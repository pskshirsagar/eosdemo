<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAssociation extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intArCascadeId;
    protected $m_intArCascadeReferenceId;
    protected $m_intArOriginId;
    protected $m_intArOriginCategoryId;
    protected $m_intArOriginGroupId;
    protected $m_intArOriginReferenceId;
    protected $m_intCompanyMediaFileId;
    protected $m_strRemotePrimaryKey;
    protected $m_strExternalVideoUrl;
    protected $m_strDescription;
    protected $m_strNotes;
    protected $m_intHideDescription;
    protected $m_strPetRestrictions;
    protected $m_intPetWeightLimit;
    protected $m_intPetMaxAllowed;
    protected $m_intAverageAmount;
    protected $m_intIsDefault;
    protected $m_intIsFeatured;
    protected $m_intIsOptional;
    protected $m_intIsPublished;
    protected $m_intIsResponsible;
    protected $m_intIsIncludedInRent;
    protected $m_intHideRates;
    protected $m_intOrderNum;
    protected $m_strImportedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;
    protected $m_intMappingId;

    public function __construct() {
        parent::__construct();

        $this->m_intHideDescription = '0';
        $this->m_intPetWeightLimit = '0';
        $this->m_intPetMaxAllowed = '0';
        $this->m_intAverageAmount = '0';
        $this->m_intIsDefault = '0';
        $this->m_intIsFeatured = '0';
        $this->m_intIsOptional = '1';
        $this->m_intIsPublished = '1';
        $this->m_intIsResponsible = '0';
        $this->m_intIsIncludedInRent = '0';
        $this->m_intHideRates = '0';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->m_intArCascadeId = trim( $arrValues['ar_cascade_id'] ); else if( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
        if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->m_intArCascadeReferenceId = trim( $arrValues['ar_cascade_reference_id'] ); else if( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
        if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->m_intArOriginId = trim( $arrValues['ar_origin_id'] ); else if( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
        if( isset( $arrValues['ar_origin_category_id'] ) && $boolDirectSet ) $this->m_intArOriginCategoryId = trim( $arrValues['ar_origin_category_id'] ); else if( isset( $arrValues['ar_origin_category_id'] ) ) $this->setArOriginCategoryId( $arrValues['ar_origin_category_id'] );
        if( isset( $arrValues['ar_origin_group_id'] ) && $boolDirectSet ) $this->m_intArOriginGroupId = trim( $arrValues['ar_origin_group_id'] ); else if( isset( $arrValues['ar_origin_group_id'] ) ) $this->setArOriginGroupId( $arrValues['ar_origin_group_id'] );
        if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->m_intArOriginReferenceId = trim( $arrValues['ar_origin_reference_id'] ); else if( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
        if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->m_intCompanyMediaFileId = trim( $arrValues['company_media_file_id'] ); else if( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->m_strRemotePrimaryKey = trim( stripcslashes( $arrValues['remote_primary_key'] ) ); else if( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
        if( isset( $arrValues['external_video_url'] ) && $boolDirectSet ) $this->m_strExternalVideoUrl = trim( stripcslashes( $arrValues['external_video_url'] ) ); else if( isset( $arrValues['external_video_url'] ) ) $this->setExternalVideoUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_video_url'] ) : $arrValues['external_video_url'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->m_strNotes = trim( stripcslashes( $arrValues['notes'] ) ); else if( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
        if( isset( $arrValues['hide_description'] ) && $boolDirectSet ) $this->m_intHideDescription = trim( $arrValues['hide_description'] ); else if( isset( $arrValues['hide_description'] ) ) $this->setHideDescription( $arrValues['hide_description'] );
        if( isset( $arrValues['pet_restrictions'] ) && $boolDirectSet ) $this->m_strPetRestrictions = trim( stripcslashes( $arrValues['pet_restrictions'] ) ); else if( isset( $arrValues['pet_restrictions'] ) ) $this->setPetRestrictions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pet_restrictions'] ) : $arrValues['pet_restrictions'] );
        if( isset( $arrValues['pet_weight_limit'] ) && $boolDirectSet ) $this->m_intPetWeightLimit = trim( $arrValues['pet_weight_limit'] ); else if( isset( $arrValues['pet_weight_limit'] ) ) $this->setPetWeightLimit( $arrValues['pet_weight_limit'] );
        if( isset( $arrValues['pet_max_allowed'] ) && $boolDirectSet ) $this->m_intPetMaxAllowed = trim( $arrValues['pet_max_allowed'] ); else if( isset( $arrValues['pet_max_allowed'] ) ) $this->setPetMaxAllowed( $arrValues['pet_max_allowed'] );
        if( isset( $arrValues['average_amount'] ) && $boolDirectSet ) $this->m_intAverageAmount = trim( $arrValues['average_amount'] ); else if( isset( $arrValues['average_amount'] ) ) $this->setAverageAmount( $arrValues['average_amount'] );
        if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->m_intIsDefault = trim( $arrValues['is_default'] ); else if( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
        if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->m_intIsFeatured = trim( $arrValues['is_featured'] ); else if( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( $arrValues['is_featured'] );
        if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->m_intIsOptional = trim( $arrValues['is_optional'] ); else if( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( $arrValues['is_optional'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['is_responsible'] ) && $boolDirectSet ) $this->m_intIsResponsible = trim( $arrValues['is_responsible'] ); else if( isset( $arrValues['is_responsible'] ) ) $this->setIsResponsible( $arrValues['is_responsible'] );
        if( isset( $arrValues['is_included_in_rent'] ) && $boolDirectSet ) $this->m_intIsIncludedInRent = trim( $arrValues['is_included_in_rent'] ); else if( isset( $arrValues['is_included_in_rent'] ) ) $this->setIsIncludedInRent( $arrValues['is_included_in_rent'] );
        if( isset( $arrValues['hide_rates'] ) && $boolDirectSet ) $this->m_intHideRates = trim( $arrValues['hide_rates'] ); else if( isset( $arrValues['hide_rates'] ) ) $this->setHideRates( $arrValues['hide_rates'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->m_strImportedOn = trim( $arrValues['imported_on'] ); else if( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['mapping_id'] ) && $boolDirectSet ) $this->m_intMappingId = trim( $arrValues['mapping_id'] ); else if( isset( $arrValues['mapping_id'] ) ) $this->setMappingId( $arrValues['mapping_id'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setArCascadeId( $intArCascadeId ) {
        $this->m_intArCascadeId = CStrings::strToIntDef( $intArCascadeId, NULL, false );
    }

    public function getArCascadeId() {
        return $this->m_intArCascadeId;
    }

    public function sqlArCascadeId() {
        return ( true == isset( $this->m_intArCascadeId ) ) ? (string) $this->m_intArCascadeId : 'NULL';
    }

    public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
        $this->m_intArCascadeReferenceId = CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false );
    }

    public function getArCascadeReferenceId() {
        return $this->m_intArCascadeReferenceId;
    }

    public function sqlArCascadeReferenceId() {
        return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? (string) $this->m_intArCascadeReferenceId : 'NULL';
    }

    public function setArOriginId( $intArOriginId ) {
        $this->m_intArOriginId = CStrings::strToIntDef( $intArOriginId, NULL, false );
    }

    public function getArOriginId() {
        return $this->m_intArOriginId;
    }

    public function sqlArOriginId() {
        return ( true == isset( $this->m_intArOriginId ) ) ? (string) $this->m_intArOriginId : 'NULL';
    }

    public function setArOriginCategoryId( $intArOriginCategoryId ) {
        $this->m_intArOriginCategoryId = CStrings::strToIntDef( $intArOriginCategoryId, NULL, false );
    }

    public function getArOriginCategoryId() {
        return $this->m_intArOriginCategoryId;
    }

    public function sqlArOriginCategoryId() {
        return ( true == isset( $this->m_intArOriginCategoryId ) ) ? (string) $this->m_intArOriginCategoryId : 'NULL';
    }

    public function setArOriginGroupId( $intArOriginGroupId ) {
        $this->m_intArOriginGroupId = CStrings::strToIntDef( $intArOriginGroupId, NULL, false );
    }

    public function getArOriginGroupId() {
        return $this->m_intArOriginGroupId;
    }

    public function sqlArOriginGroupId() {
        return ( true == isset( $this->m_intArOriginGroupId ) ) ? (string) $this->m_intArOriginGroupId : 'NULL';
    }

    public function setArOriginReferenceId( $intArOriginReferenceId ) {
        $this->m_intArOriginReferenceId = CStrings::strToIntDef( $intArOriginReferenceId, NULL, false );
    }

    public function getArOriginReferenceId() {
        return $this->m_intArOriginReferenceId;
    }

    public function sqlArOriginReferenceId() {
        return ( true == isset( $this->m_intArOriginReferenceId ) ) ? (string) $this->m_intArOriginReferenceId : 'NULL';
    }

    public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
        $this->m_intCompanyMediaFileId = CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false );
    }

    public function getCompanyMediaFileId() {
        return $this->m_intCompanyMediaFileId;
    }

    public function sqlCompanyMediaFileId() {
        return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? (string) $this->m_intCompanyMediaFileId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->m_strRemotePrimaryKey = CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
    }

    public function setExternalVideoUrl( $strExternalVideoUrl ) {
        $this->m_strExternalVideoUrl = CStrings::strTrimDef( $strExternalVideoUrl, 4096, NULL, true );
    }

    public function getExternalVideoUrl() {
        return $this->m_strExternalVideoUrl;
    }

    public function sqlExternalVideoUrl() {
        return ( true == isset( $this->m_strExternalVideoUrl ) ) ? '\'' . addslashes( $this->m_strExternalVideoUrl ) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setNotes( $strNotes ) {
        $this->m_strNotes = CStrings::strTrimDef( $strNotes, 240, NULL, true );
    }

    public function getNotes() {
        return $this->m_strNotes;
    }

    public function sqlNotes() {
        return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
    }

    public function setHideDescription( $intHideDescription ) {
        $this->m_intHideDescription = CStrings::strToIntDef( $intHideDescription, NULL, false );
    }

    public function getHideDescription() {
        return $this->m_intHideDescription;
    }

    public function sqlHideDescription() {
        return ( true == isset( $this->m_intHideDescription ) ) ? (string) $this->m_intHideDescription : '0';
    }

    public function setPetRestrictions( $strPetRestrictions ) {
        $this->m_strPetRestrictions = CStrings::strTrimDef( $strPetRestrictions, -1, NULL, true );
    }

    public function getPetRestrictions() {
        return $this->m_strPetRestrictions;
    }

    public function sqlPetRestrictions() {
        return ( true == isset( $this->m_strPetRestrictions ) ) ? '\'' . addslashes( $this->m_strPetRestrictions ) . '\'' : 'NULL';
    }

    public function setPetWeightLimit( $intPetWeightLimit ) {
        $this->m_intPetWeightLimit = CStrings::strToIntDef( $intPetWeightLimit, NULL, false );
    }

    public function getPetWeightLimit() {
        return $this->m_intPetWeightLimit;
    }

    public function sqlPetWeightLimit() {
        return ( true == isset( $this->m_intPetWeightLimit ) ) ? (string) $this->m_intPetWeightLimit : '0';
    }

    public function setPetMaxAllowed( $intPetMaxAllowed ) {
        $this->m_intPetMaxAllowed = CStrings::strToIntDef( $intPetMaxAllowed, NULL, false );
    }

    public function getPetMaxAllowed() {
        return $this->m_intPetMaxAllowed;
    }

    public function sqlPetMaxAllowed() {
        return ( true == isset( $this->m_intPetMaxAllowed ) ) ? (string) $this->m_intPetMaxAllowed : '0';
    }

    public function setAverageAmount( $intAverageAmount ) {
        $this->m_intAverageAmount = CStrings::strToIntDef( $intAverageAmount, NULL, false );
    }

    public function getAverageAmount() {
        return $this->m_intAverageAmount;
    }

    public function sqlAverageAmount() {
        return ( true == isset( $this->m_intAverageAmount ) ) ? (string) $this->m_intAverageAmount : '0';
    }

    public function setIsDefault( $intIsDefault ) {
        $this->m_intIsDefault = CStrings::strToIntDef( $intIsDefault, NULL, false );
    }

    public function getIsDefault() {
        return $this->m_intIsDefault;
    }

    public function sqlIsDefault() {
        return ( true == isset( $this->m_intIsDefault ) ) ? (string) $this->m_intIsDefault : '0';
    }

    public function setIsFeatured( $intIsFeatured ) {
        $this->m_intIsFeatured = CStrings::strToIntDef( $intIsFeatured, NULL, false );
    }

    public function getIsFeatured() {
        return $this->m_intIsFeatured;
    }

    public function sqlIsFeatured() {
        return ( true == isset( $this->m_intIsFeatured ) ) ? (string) $this->m_intIsFeatured : '0';
    }

    public function setIsOptional( $intIsOptional ) {
        $this->m_intIsOptional = CStrings::strToIntDef( $intIsOptional, NULL, false );
    }

    public function getIsOptional() {
        return $this->m_intIsOptional;
    }

    public function sqlIsOptional() {
        return ( true == isset( $this->m_intIsOptional ) ) ? (string) $this->m_intIsOptional : '1';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setIsResponsible( $intIsResponsible ) {
        $this->m_intIsResponsible = CStrings::strToIntDef( $intIsResponsible, NULL, false );
    }

    public function getIsResponsible() {
        return $this->m_intIsResponsible;
    }

    public function sqlIsResponsible() {
        return ( true == isset( $this->m_intIsResponsible ) ) ? (string) $this->m_intIsResponsible : '0';
    }

    public function setIsIncludedInRent( $intIsIncludedInRent ) {
        $this->m_intIsIncludedInRent = CStrings::strToIntDef( $intIsIncludedInRent, NULL, false );
    }

    public function getIsIncludedInRent() {
        return $this->m_intIsIncludedInRent;
    }

    public function sqlIsIncludedInRent() {
        return ( true == isset( $this->m_intIsIncludedInRent ) ) ? (string) $this->m_intIsIncludedInRent : '0';
    }

    public function setHideRates( $intHideRates ) {
        $this->m_intHideRates = CStrings::strToIntDef( $intHideRates, NULL, false );
    }

    public function getHideRates() {
        return $this->m_intHideRates;
    }

    public function sqlHideRates() {
        return ( true == isset( $this->m_intHideRates ) ) ? (string) $this->m_intHideRates : '0';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function setImportedOn( $strImportedOn ) {
        $this->m_strImportedOn = CStrings::strTrimDef( $strImportedOn, -1, NULL, true );
    }

    public function getImportedOn() {
        return $this->m_strImportedOn;
    }

    public function sqlImportedOn() {
        return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function setMappingId( $intMappingId ) {
        $this->m_intMappingId = CStrings::strToIntDef( $intMappingId, NULL, false );
    }

    public function getMappingId() {
        return $this->m_intMappingId;
    }

    public function sqlMappingId() {
        return ( true == isset( $this->m_intMappingId ) ) ? (string) $this->m_intMappingId : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.property_associations_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.property_associations( id, cid, property_id, ar_cascade_id, ar_cascade_reference_id, ar_origin_id, ar_origin_category_id, ar_origin_group_id, ar_origin_reference_id, company_media_file_id, remote_primary_key, external_video_url, description, notes, hide_description, pet_restrictions, pet_weight_limit, pet_max_allowed, average_amount, is_default, is_featured, is_optional, is_published, is_responsible, is_included_in_rent, hide_rates, order_num, imported_on, updated_by, updated_on, created_by, created_on, mapping_id )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlArCascadeId() . ', ' . 		                $this->sqlArCascadeReferenceId() . ', ' . 		                $this->sqlArOriginId() . ', ' . 		                $this->sqlArOriginCategoryId() . ', ' . 		                $this->sqlArOriginGroupId() . ', ' . 		                $this->sqlArOriginReferenceId() . ', ' . 		                $this->sqlCompanyMediaFileId() . ', ' . 		                $this->sqlRemotePrimaryKey() . ', ' . 		                $this->sqlExternalVideoUrl() . ', ' . 		                $this->sqlDescription() . ', ' . 		                $this->sqlNotes() . ', ' . 		                $this->sqlHideDescription() . ', ' . 		                $this->sqlPetRestrictions() . ', ' . 		                $this->sqlPetWeightLimit() . ', ' . 		                $this->sqlPetMaxAllowed() . ', ' . 		                $this->sqlAverageAmount() . ', ' . 		                $this->sqlIsDefault() . ', ' . 		                $this->sqlIsFeatured() . ', ' . 		                $this->sqlIsOptional() . ', ' . 		                $this->sqlIsPublished() . ', ' . 		                $this->sqlIsResponsible() . ', ' . 		                $this->sqlIsIncludedInRent() . ', ' . 		                $this->sqlHideRates() . ', ' . 		                $this->sqlOrderNum() . ', ' . 		                $this->sqlImportedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ', ' . 		                $this->sqlMappingId() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.property_associations
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArCascadeId() ) != $this->getOriginalValueByFieldName ( 'ar_cascade_id' ) ) { $arrstrOriginalValueChanges['ar_cascade_id'] = $this->sqlArCascadeId(); $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArCascadeReferenceId() ) != $this->getOriginalValueByFieldName ( 'ar_cascade_reference_id' ) ) { $arrstrOriginalValueChanges['ar_cascade_reference_id'] = $this->sqlArCascadeReferenceId(); $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArOriginId() ) != $this->getOriginalValueByFieldName ( 'ar_origin_id' ) ) { $arrstrOriginalValueChanges['ar_origin_id'] = $this->sqlArOriginId(); $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_category_id = ' . $this->sqlArOriginCategoryId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArOriginCategoryId() ) != $this->getOriginalValueByFieldName ( 'ar_origin_category_id' ) ) { $arrstrOriginalValueChanges['ar_origin_category_id'] = $this->sqlArOriginCategoryId(); $strSql .= ' ar_origin_category_id = ' . $this->sqlArOriginCategoryId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_group_id = ' . $this->sqlArOriginGroupId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArOriginGroupId() ) != $this->getOriginalValueByFieldName ( 'ar_origin_group_id' ) ) { $arrstrOriginalValueChanges['ar_origin_group_id'] = $this->sqlArOriginGroupId(); $strSql .= ' ar_origin_group_id = ' . $this->sqlArOriginGroupId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArOriginReferenceId() ) != $this->getOriginalValueByFieldName ( 'ar_origin_reference_id' ) ) { $arrstrOriginalValueChanges['ar_origin_reference_id'] = $this->sqlArOriginReferenceId(); $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyMediaFileId() ) != $this->getOriginalValueByFieldName ( 'company_media_file_id' ) ) { $arrstrOriginalValueChanges['company_media_file_id'] = $this->sqlCompanyMediaFileId(); $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRemotePrimaryKey() ) != $this->getOriginalValueByFieldName ( 'remote_primary_key' ) ) { $arrstrOriginalValueChanges['remote_primary_key'] = $this->sqlRemotePrimaryKey(); $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlExternalVideoUrl() ) != $this->getOriginalValueByFieldName ( 'external_video_url' ) ) { $arrstrOriginalValueChanges['external_video_url'] = $this->sqlExternalVideoUrl(); $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName ( 'description' ) ) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNotes() ) != $this->getOriginalValueByFieldName ( 'notes' ) ) { $arrstrOriginalValueChanges['notes'] = $this->sqlNotes(); $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_description = ' . $this->sqlHideDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHideDescription() ) != $this->getOriginalValueByFieldName ( 'hide_description' ) ) { $arrstrOriginalValueChanges['hide_description'] = $this->sqlHideDescription(); $strSql .= ' hide_description = ' . $this->sqlHideDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_restrictions = ' . $this->sqlPetRestrictions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPetRestrictions() ) != $this->getOriginalValueByFieldName ( 'pet_restrictions' ) ) { $arrstrOriginalValueChanges['pet_restrictions'] = $this->sqlPetRestrictions(); $strSql .= ' pet_restrictions = ' . $this->sqlPetRestrictions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_weight_limit = ' . $this->sqlPetWeightLimit() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPetWeightLimit() ) != $this->getOriginalValueByFieldName ( 'pet_weight_limit' ) ) { $arrstrOriginalValueChanges['pet_weight_limit'] = $this->sqlPetWeightLimit(); $strSql .= ' pet_weight_limit = ' . $this->sqlPetWeightLimit() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_max_allowed = ' . $this->sqlPetMaxAllowed() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPetMaxAllowed() ) != $this->getOriginalValueByFieldName ( 'pet_max_allowed' ) ) { $arrstrOriginalValueChanges['pet_max_allowed'] = $this->sqlPetMaxAllowed(); $strSql .= ' pet_max_allowed = ' . $this->sqlPetMaxAllowed() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_amount = ' . $this->sqlAverageAmount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAverageAmount() ) != $this->getOriginalValueByFieldName ( 'average_amount' ) ) { $arrstrOriginalValueChanges['average_amount'] = $this->sqlAverageAmount(); $strSql .= ' average_amount = ' . $this->sqlAverageAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsDefault() ) != $this->getOriginalValueByFieldName ( 'is_default' ) ) { $arrstrOriginalValueChanges['is_default'] = $this->sqlIsDefault(); $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsFeatured() ) != $this->getOriginalValueByFieldName ( 'is_featured' ) ) { $arrstrOriginalValueChanges['is_featured'] = $this->sqlIsFeatured(); $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsOptional() ) != $this->getOriginalValueByFieldName ( 'is_optional' ) ) { $arrstrOriginalValueChanges['is_optional'] = $this->sqlIsOptional(); $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible = ' . $this->sqlIsResponsible() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsResponsible() ) != $this->getOriginalValueByFieldName ( 'is_responsible' ) ) { $arrstrOriginalValueChanges['is_responsible'] = $this->sqlIsResponsible(); $strSql .= ' is_responsible = ' . $this->sqlIsResponsible() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_included_in_rent = ' . $this->sqlIsIncludedInRent() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsIncludedInRent() ) != $this->getOriginalValueByFieldName ( 'is_included_in_rent' ) ) { $arrstrOriginalValueChanges['is_included_in_rent'] = $this->sqlIsIncludedInRent(); $strSql .= ' is_included_in_rent = ' . $this->sqlIsIncludedInRent() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_rates = ' . $this->sqlHideRates() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHideRates() ) != $this->getOriginalValueByFieldName ( 'hide_rates' ) ) { $arrstrOriginalValueChanges['hide_rates'] = $this->sqlHideRates(); $strSql .= ' hide_rates = ' . $this->sqlHideRates() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName ( 'order_num' ) ) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlImportedOn() ) != $this->getOriginalValueByFieldName ( 'imported_on' ) ) { $arrstrOriginalValueChanges['imported_on'] = $this->sqlImportedOn(); $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMappingId() ) != $this->getOriginalValueByFieldName ( 'mapping_id' ) ) { $arrstrOriginalValueChanges['mapping_id'] = $this->sqlMappingId(); $strSql .= ' mapping_id = ' . $this->sqlMappingId() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.property_associations WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.property_associations_id_seq', $objDatabase );
    }

}
?>