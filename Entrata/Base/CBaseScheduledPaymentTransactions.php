<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTransactions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledPaymentTransactions extends CEosPluralBase {

	/**
	 * @return CScheduledPaymentTransaction[]
	 */
	public static function fetchScheduledPaymentTransactions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledPaymentTransaction', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledPaymentTransaction
	 */
	public static function fetchScheduledPaymentTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledPaymentTransaction', $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_payment_transactions', $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransaction( sprintf( 'SELECT * FROM scheduled_payment_transactions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransactions( sprintf( 'SELECT * FROM scheduled_payment_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionsByScheduledPaymentTransTypeIdByCid( $intScheduledPaymentTransTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransactions( sprintf( 'SELECT * FROM scheduled_payment_transactions WHERE scheduled_payment_trans_type_id = %d AND cid = %d', ( int ) $intScheduledPaymentTransTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionsByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransactions( sprintf( 'SELECT * FROM scheduled_payment_transactions WHERE scheduled_payment_id = %d AND cid = %d', ( int ) $intScheduledPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentTransactionsByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentTransactions( sprintf( 'SELECT * FROM scheduled_payment_transactions WHERE ar_payment_id = %d AND cid = %d', ( int ) $intArPaymentId, ( int ) $intCid ), $objDatabase );
	}

}
?>