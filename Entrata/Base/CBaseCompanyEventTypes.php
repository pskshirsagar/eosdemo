<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEventTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyEventTypes extends CEosPluralBase {

	/**
	 * @return CCompanyEventType[]
	 */
	public static function fetchCompanyEventTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyEventType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEventType
	 */
	public static function fetchCompanyEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEventType::class, $objDatabase );
	}

	public static function fetchCompanyEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_event_types', $objDatabase );
	}

	public static function fetchCompanyEventTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyEventType( sprintf( 'SELECT * FROM company_event_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>