<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientKeyValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationClientKeyValues extends CEosPluralBase {

	/**
	 * @return CIntegrationClientKeyValue[]
	 */
	public static function fetchIntegrationClientKeyValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CIntegrationClientKeyValue::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationClientKeyValue
	 */
	public static function fetchIntegrationClientKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationClientKeyValue::class, $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_client_key_values', $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM integration_client_key_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByIntegrationClientIdByCid( $intIntegrationClientId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE integration_client_id = %d AND cid = %d', ( int ) $intIntegrationClientId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>