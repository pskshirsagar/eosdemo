<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDetails extends CEosPluralBase {

	/**
	 * @return CCustomerDetail[]
	 */
	public static function fetchCustomerDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerDetail
	 */
	public static function fetchCustomerDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerDetail::class, $objDatabase );
	}

	public static function fetchCustomerDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_details', $objDatabase );
	}

	public static function fetchCustomerDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerDetail( sprintf( 'SELECT * FROM customer_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerDetails( sprintf( 'SELECT * FROM customer_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerDetailsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerDetails( sprintf( 'SELECT * FROM customer_details WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerDetailsByInvoiceDeliveryTypeIdByCid( $intInvoiceDeliveryTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerDetails( sprintf( 'SELECT * FROM customer_details WHERE invoice_delivery_type_id = %d AND cid = %d', $intInvoiceDeliveryTypeId, $intCid ), $objDatabase );
	}

}
?>