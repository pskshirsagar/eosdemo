<?php

class CBaseArCodeTypeAllowance extends CEosSingularBase {

    protected $m_intId;
    protected $m_intArCodeTypeId;
    protected $m_intReferenceId;
    protected $m_intIsAllowed;
    protected $m_intIsGlAccountUsageType;
    protected $m_intIsGlAccountType;
    protected $m_intIsCreditAllowance;

    public function __construct() {
        parent::__construct();

        $this->m_intIsAllowed = '0';
        $this->m_intIsGlAccountUsageType = '0';
        $this->m_intIsGlAccountType = '0';
        $this->m_intIsCreditAllowance = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->m_intArCodeTypeId = trim( $arrValues['ar_code_type_id'] ); else if( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
        if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->m_intReferenceId = trim( $arrValues['reference_id'] ); else if( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
        if( isset( $arrValues['is_allowed'] ) && $boolDirectSet ) $this->m_intIsAllowed = trim( $arrValues['is_allowed'] ); else if( isset( $arrValues['is_allowed'] ) ) $this->setIsAllowed( $arrValues['is_allowed'] );
        if( isset( $arrValues['is_gl_account_usage_type'] ) && $boolDirectSet ) $this->m_intIsGlAccountUsageType = trim( $arrValues['is_gl_account_usage_type'] ); else if( isset( $arrValues['is_gl_account_usage_type'] ) ) $this->setIsGlAccountUsageType( $arrValues['is_gl_account_usage_type'] );
        if( isset( $arrValues['is_gl_account_type'] ) && $boolDirectSet ) $this->m_intIsGlAccountType = trim( $arrValues['is_gl_account_type'] ); else if( isset( $arrValues['is_gl_account_type'] ) ) $this->setIsGlAccountType( $arrValues['is_gl_account_type'] );
        if( isset( $arrValues['is_credit_allowance'] ) && $boolDirectSet ) $this->m_intIsCreditAllowance = trim( $arrValues['is_credit_allowance'] ); else if( isset( $arrValues['is_credit_allowance'] ) ) $this->setIsCreditAllowance( $arrValues['is_credit_allowance'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setArCodeTypeId( $intArCodeTypeId ) {
        $this->m_intArCodeTypeId = CStrings::strToIntDef( $intArCodeTypeId, NULL, false );
    }

    public function getArCodeTypeId() {
        return $this->m_intArCodeTypeId;
    }

    public function sqlArCodeTypeId() {
        return ( true == isset( $this->m_intArCodeTypeId ) ) ? (string) $this->m_intArCodeTypeId : 'NULL';
    }

    public function setReferenceId( $intReferenceId ) {
        $this->m_intReferenceId = CStrings::strToIntDef( $intReferenceId, NULL, false );
    }

    public function getReferenceId() {
        return $this->m_intReferenceId;
    }

    public function sqlReferenceId() {
        return ( true == isset( $this->m_intReferenceId ) ) ? (string) $this->m_intReferenceId : 'NULL';
    }

    public function setIsAllowed( $intIsAllowed ) {
        $this->m_intIsAllowed = CStrings::strToIntDef( $intIsAllowed, NULL, false );
    }

    public function getIsAllowed() {
        return $this->m_intIsAllowed;
    }

    public function sqlIsAllowed() {
        return ( true == isset( $this->m_intIsAllowed ) ) ? (string) $this->m_intIsAllowed : '0';
    }

    public function setIsGlAccountUsageType( $intIsGlAccountUsageType ) {
        $this->m_intIsGlAccountUsageType = CStrings::strToIntDef( $intIsGlAccountUsageType, NULL, false );
    }

    public function getIsGlAccountUsageType() {
        return $this->m_intIsGlAccountUsageType;
    }

    public function sqlIsGlAccountUsageType() {
        return ( true == isset( $this->m_intIsGlAccountUsageType ) ) ? (string) $this->m_intIsGlAccountUsageType : '0';
    }

    public function setIsGlAccountType( $intIsGlAccountType ) {
        $this->m_intIsGlAccountType = CStrings::strToIntDef( $intIsGlAccountType, NULL, false );
    }

    public function getIsGlAccountType() {
        return $this->m_intIsGlAccountType;
    }

    public function sqlIsGlAccountType() {
        return ( true == isset( $this->m_intIsGlAccountType ) ) ? (string) $this->m_intIsGlAccountType : '0';
    }

    public function setIsCreditAllowance( $intIsCreditAllowance ) {
        $this->m_intIsCreditAllowance = CStrings::strToIntDef( $intIsCreditAllowance, NULL, false );
    }

    public function getIsCreditAllowance() {
        return $this->m_intIsCreditAllowance;
    }

    public function sqlIsCreditAllowance() {
        return ( true == isset( $this->m_intIsCreditAllowance ) ) ? (string) $this->m_intIsCreditAllowance : '0';
    }

}
?>