<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplateSlotImages extends CEosPluralBase {

	/**
	 * @return CEmailTemplateSlotImage[]
	 */
	public static function fetchEmailTemplateSlotImages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CEmailTemplateSlotImage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplateSlotImage
	 */
	public static function fetchEmailTemplateSlotImage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailTemplateSlotImage', $objDatabase );
	}

	public static function fetchEmailTemplateSlotImageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_slot_images', $objDatabase );
	}

	public static function fetchEmailTemplateSlotImageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImage( sprintf( 'SELECT * FROM email_template_slot_images WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE scheduled_email_id = %d AND cid = %d', ( int ) $intScheduledEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByScheduledTaskEmailIdByCid( $intScheduledTaskEmailId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE scheduled_task_email_id = %d AND cid = %d', ( int ) $intScheduledTaskEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE campaign_id = %d AND cid = %d', ( int ) $intCampaignId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByEmailTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE system_message_template_id = %d AND cid = %d', ( int ) $intSystemMessageTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByEmailTemplateSlotIdByCid( $intEmailTemplateSlotId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE system_message_template_slot_id = %d AND cid = %d', ( int ) $intEmailTemplateSlotId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotImagesByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateSlotImages( sprintf( 'SELECT * FROM email_template_slot_images WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>