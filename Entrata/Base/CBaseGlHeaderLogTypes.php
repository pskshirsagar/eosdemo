<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderLogTypes
 * Do not add any new functions to this class.
 */

class CBaseGlHeaderLogTypes extends CEosPluralBase {

	/**
	 * @return CGlHeaderLogType[]
	 */
	public static function fetchGlHeaderLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlHeaderLogType::class, $objDatabase );
	}

	/**
	 * @return CGlHeaderLogType
	 */
	public static function fetchGlHeaderLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlHeaderLogType::class, $objDatabase );
	}

	public static function fetchGlHeaderLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_log_types', $objDatabase );
	}

	public static function fetchGlHeaderLogTypeById( $intId, $objDatabase ) {
		return self::fetchGlHeaderLogType( sprintf( 'SELECT * FROM gl_header_log_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>