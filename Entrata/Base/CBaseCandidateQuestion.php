<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateQuestion extends CEosSingularBase {

	const TABLE_NAME = 'public.candidate_questions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCandidateId;
	protected $m_intIsLegallyAuthorized;
	protected $m_intHasConviction;
	protected $m_strConvictionDescription;
	protected $m_intWordsPerMinute;
	protected $m_intKnowsTenKey;
	protected $m_strComputerSkills;
	protected $m_strOtherSpecialSkills;
	protected $m_fltMinimumSalary;
	protected $m_intMinimumWeeklyHours;
	protected $m_intMaximumWeeklyHours;
	protected $m_intAge;
	protected $m_intHasMilitaryExperience;
	protected $m_strMilitaryBranch;
	protected $m_strMilitaryTraining;
	protected $m_intHasGed;
	protected $m_intHasHsDiploma;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['candidate_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateId', trim( $arrValues['candidate_id'] ) ); elseif( isset( $arrValues['candidate_id'] ) ) $this->setCandidateId( $arrValues['candidate_id'] );
		if( isset( $arrValues['is_legally_authorized'] ) && $boolDirectSet ) $this->set( 'm_intIsLegallyAuthorized', trim( $arrValues['is_legally_authorized'] ) ); elseif( isset( $arrValues['is_legally_authorized'] ) ) $this->setIsLegallyAuthorized( $arrValues['is_legally_authorized'] );
		if( isset( $arrValues['has_conviction'] ) && $boolDirectSet ) $this->set( 'm_intHasConviction', trim( $arrValues['has_conviction'] ) ); elseif( isset( $arrValues['has_conviction'] ) ) $this->setHasConviction( $arrValues['has_conviction'] );
		if( isset( $arrValues['conviction_description'] ) && $boolDirectSet ) $this->set( 'm_strConvictionDescription', trim( stripcslashes( $arrValues['conviction_description'] ) ) ); elseif( isset( $arrValues['conviction_description'] ) ) $this->setConvictionDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['conviction_description'] ) : $arrValues['conviction_description'] );
		if( isset( $arrValues['words_per_minute'] ) && $boolDirectSet ) $this->set( 'm_intWordsPerMinute', trim( $arrValues['words_per_minute'] ) ); elseif( isset( $arrValues['words_per_minute'] ) ) $this->setWordsPerMinute( $arrValues['words_per_minute'] );
		if( isset( $arrValues['knows_ten_key'] ) && $boolDirectSet ) $this->set( 'm_intKnowsTenKey', trim( $arrValues['knows_ten_key'] ) ); elseif( isset( $arrValues['knows_ten_key'] ) ) $this->setKnowsTenKey( $arrValues['knows_ten_key'] );
		if( isset( $arrValues['computer_skills'] ) && $boolDirectSet ) $this->set( 'm_strComputerSkills', trim( stripcslashes( $arrValues['computer_skills'] ) ) ); elseif( isset( $arrValues['computer_skills'] ) ) $this->setComputerSkills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['computer_skills'] ) : $arrValues['computer_skills'] );
		if( isset( $arrValues['other_special_skills'] ) && $boolDirectSet ) $this->set( 'm_strOtherSpecialSkills', trim( stripcslashes( $arrValues['other_special_skills'] ) ) ); elseif( isset( $arrValues['other_special_skills'] ) ) $this->setOtherSpecialSkills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['other_special_skills'] ) : $arrValues['other_special_skills'] );
		if( isset( $arrValues['minimum_salary'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumSalary', trim( $arrValues['minimum_salary'] ) ); elseif( isset( $arrValues['minimum_salary'] ) ) $this->setMinimumSalary( $arrValues['minimum_salary'] );
		if( isset( $arrValues['minimum_weekly_hours'] ) && $boolDirectSet ) $this->set( 'm_intMinimumWeeklyHours', trim( $arrValues['minimum_weekly_hours'] ) ); elseif( isset( $arrValues['minimum_weekly_hours'] ) ) $this->setMinimumWeeklyHours( $arrValues['minimum_weekly_hours'] );
		if( isset( $arrValues['maximum_weekly_hours'] ) && $boolDirectSet ) $this->set( 'm_intMaximumWeeklyHours', trim( $arrValues['maximum_weekly_hours'] ) ); elseif( isset( $arrValues['maximum_weekly_hours'] ) ) $this->setMaximumWeeklyHours( $arrValues['maximum_weekly_hours'] );
		if( isset( $arrValues['age'] ) && $boolDirectSet ) $this->set( 'm_intAge', trim( $arrValues['age'] ) ); elseif( isset( $arrValues['age'] ) ) $this->setAge( $arrValues['age'] );
		if( isset( $arrValues['has_military_experience'] ) && $boolDirectSet ) $this->set( 'm_intHasMilitaryExperience', trim( $arrValues['has_military_experience'] ) ); elseif( isset( $arrValues['has_military_experience'] ) ) $this->setHasMilitaryExperience( $arrValues['has_military_experience'] );
		if( isset( $arrValues['military_branch'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryBranch', trim( stripcslashes( $arrValues['military_branch'] ) ) ); elseif( isset( $arrValues['military_branch'] ) ) $this->setMilitaryBranch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_branch'] ) : $arrValues['military_branch'] );
		if( isset( $arrValues['military_training'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryTraining', trim( stripcslashes( $arrValues['military_training'] ) ) ); elseif( isset( $arrValues['military_training'] ) ) $this->setMilitaryTraining( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_training'] ) : $arrValues['military_training'] );
		if( isset( $arrValues['has_ged'] ) && $boolDirectSet ) $this->set( 'm_intHasGed', trim( $arrValues['has_ged'] ) ); elseif( isset( $arrValues['has_ged'] ) ) $this->setHasGed( $arrValues['has_ged'] );
		if( isset( $arrValues['has_hs_diploma'] ) && $boolDirectSet ) $this->set( 'm_intHasHsDiploma', trim( $arrValues['has_hs_diploma'] ) ); elseif( isset( $arrValues['has_hs_diploma'] ) ) $this->setHasHsDiploma( $arrValues['has_hs_diploma'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCandidateId( $intCandidateId ) {
		$this->set( 'm_intCandidateId', CStrings::strToIntDef( $intCandidateId, NULL, false ) );
	}

	public function getCandidateId() {
		return $this->m_intCandidateId;
	}

	public function sqlCandidateId() {
		return ( true == isset( $this->m_intCandidateId ) ) ? ( string ) $this->m_intCandidateId : 'NULL';
	}

	public function setIsLegallyAuthorized( $intIsLegallyAuthorized ) {
		$this->set( 'm_intIsLegallyAuthorized', CStrings::strToIntDef( $intIsLegallyAuthorized, NULL, false ) );
	}

	public function getIsLegallyAuthorized() {
		return $this->m_intIsLegallyAuthorized;
	}

	public function sqlIsLegallyAuthorized() {
		return ( true == isset( $this->m_intIsLegallyAuthorized ) ) ? ( string ) $this->m_intIsLegallyAuthorized : 'NULL';
	}

	public function setHasConviction( $intHasConviction ) {
		$this->set( 'm_intHasConviction', CStrings::strToIntDef( $intHasConviction, NULL, false ) );
	}

	public function getHasConviction() {
		return $this->m_intHasConviction;
	}

	public function sqlHasConviction() {
		return ( true == isset( $this->m_intHasConviction ) ) ? ( string ) $this->m_intHasConviction : 'NULL';
	}

	public function setConvictionDescription( $strConvictionDescription ) {
		$this->set( 'm_strConvictionDescription', CStrings::strTrimDef( $strConvictionDescription, 2000, NULL, true ) );
	}

	public function getConvictionDescription() {
		return $this->m_strConvictionDescription;
	}

	public function sqlConvictionDescription() {
		return ( true == isset( $this->m_strConvictionDescription ) ) ? '\'' . addslashes( $this->m_strConvictionDescription ) . '\'' : 'NULL';
	}

	public function setWordsPerMinute( $intWordsPerMinute ) {
		$this->set( 'm_intWordsPerMinute', CStrings::strToIntDef( $intWordsPerMinute, NULL, false ) );
	}

	public function getWordsPerMinute() {
		return $this->m_intWordsPerMinute;
	}

	public function sqlWordsPerMinute() {
		return ( true == isset( $this->m_intWordsPerMinute ) ) ? ( string ) $this->m_intWordsPerMinute : 'NULL';
	}

	public function setKnowsTenKey( $intKnowsTenKey ) {
		$this->set( 'm_intKnowsTenKey', CStrings::strToIntDef( $intKnowsTenKey, NULL, false ) );
	}

	public function getKnowsTenKey() {
		return $this->m_intKnowsTenKey;
	}

	public function sqlKnowsTenKey() {
		return ( true == isset( $this->m_intKnowsTenKey ) ) ? ( string ) $this->m_intKnowsTenKey : 'NULL';
	}

	public function setComputerSkills( $strComputerSkills ) {
		$this->set( 'm_strComputerSkills', CStrings::strTrimDef( $strComputerSkills, 2000, NULL, true ) );
	}

	public function getComputerSkills() {
		return $this->m_strComputerSkills;
	}

	public function sqlComputerSkills() {
		return ( true == isset( $this->m_strComputerSkills ) ) ? '\'' . addslashes( $this->m_strComputerSkills ) . '\'' : 'NULL';
	}

	public function setOtherSpecialSkills( $strOtherSpecialSkills ) {
		$this->set( 'm_strOtherSpecialSkills', CStrings::strTrimDef( $strOtherSpecialSkills, 2000, NULL, true ) );
	}

	public function getOtherSpecialSkills() {
		return $this->m_strOtherSpecialSkills;
	}

	public function sqlOtherSpecialSkills() {
		return ( true == isset( $this->m_strOtherSpecialSkills ) ) ? '\'' . addslashes( $this->m_strOtherSpecialSkills ) . '\'' : 'NULL';
	}

	public function setMinimumSalary( $fltMinimumSalary ) {
		$this->set( 'm_fltMinimumSalary', CStrings::strToFloatDef( $fltMinimumSalary, NULL, false, 2 ) );
	}

	public function getMinimumSalary() {
		return $this->m_fltMinimumSalary;
	}

	public function sqlMinimumSalary() {
		return ( true == isset( $this->m_fltMinimumSalary ) ) ? ( string ) $this->m_fltMinimumSalary : 'NULL';
	}

	public function setMinimumWeeklyHours( $intMinimumWeeklyHours ) {
		$this->set( 'm_intMinimumWeeklyHours', CStrings::strToIntDef( $intMinimumWeeklyHours, NULL, false ) );
	}

	public function getMinimumWeeklyHours() {
		return $this->m_intMinimumWeeklyHours;
	}

	public function sqlMinimumWeeklyHours() {
		return ( true == isset( $this->m_intMinimumWeeklyHours ) ) ? ( string ) $this->m_intMinimumWeeklyHours : 'NULL';
	}

	public function setMaximumWeeklyHours( $intMaximumWeeklyHours ) {
		$this->set( 'm_intMaximumWeeklyHours', CStrings::strToIntDef( $intMaximumWeeklyHours, NULL, false ) );
	}

	public function getMaximumWeeklyHours() {
		return $this->m_intMaximumWeeklyHours;
	}

	public function sqlMaximumWeeklyHours() {
		return ( true == isset( $this->m_intMaximumWeeklyHours ) ) ? ( string ) $this->m_intMaximumWeeklyHours : 'NULL';
	}

	public function setAge( $intAge ) {
		$this->set( 'm_intAge', CStrings::strToIntDef( $intAge, NULL, false ) );
	}

	public function getAge() {
		return $this->m_intAge;
	}

	public function sqlAge() {
		return ( true == isset( $this->m_intAge ) ) ? ( string ) $this->m_intAge : 'NULL';
	}

	public function setHasMilitaryExperience( $intHasMilitaryExperience ) {
		$this->set( 'm_intHasMilitaryExperience', CStrings::strToIntDef( $intHasMilitaryExperience, NULL, false ) );
	}

	public function getHasMilitaryExperience() {
		return $this->m_intHasMilitaryExperience;
	}

	public function sqlHasMilitaryExperience() {
		return ( true == isset( $this->m_intHasMilitaryExperience ) ) ? ( string ) $this->m_intHasMilitaryExperience : 'NULL';
	}

	public function setMilitaryBranch( $strMilitaryBranch ) {
		$this->set( 'm_strMilitaryBranch', CStrings::strTrimDef( $strMilitaryBranch, -1, NULL, true ) );
	}

	public function getMilitaryBranch() {
		return $this->m_strMilitaryBranch;
	}

	public function sqlMilitaryBranch() {
		return ( true == isset( $this->m_strMilitaryBranch ) ) ? '\'' . addslashes( $this->m_strMilitaryBranch ) . '\'' : 'NULL';
	}

	public function setMilitaryTraining( $strMilitaryTraining ) {
		$this->set( 'm_strMilitaryTraining', CStrings::strTrimDef( $strMilitaryTraining, -1, NULL, true ) );
	}

	public function getMilitaryTraining() {
		return $this->m_strMilitaryTraining;
	}

	public function sqlMilitaryTraining() {
		return ( true == isset( $this->m_strMilitaryTraining ) ) ? '\'' . addslashes( $this->m_strMilitaryTraining ) . '\'' : 'NULL';
	}

	public function setHasGed( $intHasGed ) {
		$this->set( 'm_intHasGed', CStrings::strToIntDef( $intHasGed, NULL, false ) );
	}

	public function getHasGed() {
		return $this->m_intHasGed;
	}

	public function sqlHasGed() {
		return ( true == isset( $this->m_intHasGed ) ) ? ( string ) $this->m_intHasGed : 'NULL';
	}

	public function setHasHsDiploma( $intHasHsDiploma ) {
		$this->set( 'm_intHasHsDiploma', CStrings::strToIntDef( $intHasHsDiploma, NULL, false ) );
	}

	public function getHasHsDiploma() {
		return $this->m_intHasHsDiploma;
	}

	public function sqlHasHsDiploma() {
		return ( true == isset( $this->m_intHasHsDiploma ) ) ? ( string ) $this->m_intHasHsDiploma : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, candidate_id, is_legally_authorized, has_conviction, conviction_description, words_per_minute, knows_ten_key, computer_skills, other_special_skills, minimum_salary, minimum_weekly_hours, maximum_weekly_hours, age, has_military_experience, military_branch, military_training, has_ged, has_hs_diploma, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCandidateId() . ', ' .
 						$this->sqlIsLegallyAuthorized() . ', ' .
 						$this->sqlHasConviction() . ', ' .
 						$this->sqlConvictionDescription() . ', ' .
 						$this->sqlWordsPerMinute() . ', ' .
 						$this->sqlKnowsTenKey() . ', ' .
 						$this->sqlComputerSkills() . ', ' .
 						$this->sqlOtherSpecialSkills() . ', ' .
 						$this->sqlMinimumSalary() . ', ' .
 						$this->sqlMinimumWeeklyHours() . ', ' .
 						$this->sqlMaximumWeeklyHours() . ', ' .
 						$this->sqlAge() . ', ' .
 						$this->sqlHasMilitaryExperience() . ', ' .
 						$this->sqlMilitaryBranch() . ', ' .
 						$this->sqlMilitaryTraining() . ', ' .
 						$this->sqlHasGed() . ', ' .
 						$this->sqlHasHsDiploma() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; } elseif( true == array_key_exists( 'CandidateId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_legally_authorized = ' . $this->sqlIsLegallyAuthorized() . ','; } elseif( true == array_key_exists( 'IsLegallyAuthorized', $this->getChangedColumns() ) ) { $strSql .= ' is_legally_authorized = ' . $this->sqlIsLegallyAuthorized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_conviction = ' . $this->sqlHasConviction() . ','; } elseif( true == array_key_exists( 'HasConviction', $this->getChangedColumns() ) ) { $strSql .= ' has_conviction = ' . $this->sqlHasConviction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conviction_description = ' . $this->sqlConvictionDescription() . ','; } elseif( true == array_key_exists( 'ConvictionDescription', $this->getChangedColumns() ) ) { $strSql .= ' conviction_description = ' . $this->sqlConvictionDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' words_per_minute = ' . $this->sqlWordsPerMinute() . ','; } elseif( true == array_key_exists( 'WordsPerMinute', $this->getChangedColumns() ) ) { $strSql .= ' words_per_minute = ' . $this->sqlWordsPerMinute() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' knows_ten_key = ' . $this->sqlKnowsTenKey() . ','; } elseif( true == array_key_exists( 'KnowsTenKey', $this->getChangedColumns() ) ) { $strSql .= ' knows_ten_key = ' . $this->sqlKnowsTenKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' computer_skills = ' . $this->sqlComputerSkills() . ','; } elseif( true == array_key_exists( 'ComputerSkills', $this->getChangedColumns() ) ) { $strSql .= ' computer_skills = ' . $this->sqlComputerSkills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_special_skills = ' . $this->sqlOtherSpecialSkills() . ','; } elseif( true == array_key_exists( 'OtherSpecialSkills', $this->getChangedColumns() ) ) { $strSql .= ' other_special_skills = ' . $this->sqlOtherSpecialSkills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_salary = ' . $this->sqlMinimumSalary() . ','; } elseif( true == array_key_exists( 'MinimumSalary', $this->getChangedColumns() ) ) { $strSql .= ' minimum_salary = ' . $this->sqlMinimumSalary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_weekly_hours = ' . $this->sqlMinimumWeeklyHours() . ','; } elseif( true == array_key_exists( 'MinimumWeeklyHours', $this->getChangedColumns() ) ) { $strSql .= ' minimum_weekly_hours = ' . $this->sqlMinimumWeeklyHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_weekly_hours = ' . $this->sqlMaximumWeeklyHours() . ','; } elseif( true == array_key_exists( 'MaximumWeeklyHours', $this->getChangedColumns() ) ) { $strSql .= ' maximum_weekly_hours = ' . $this->sqlMaximumWeeklyHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' age = ' . $this->sqlAge() . ','; } elseif( true == array_key_exists( 'Age', $this->getChangedColumns() ) ) { $strSql .= ' age = ' . $this->sqlAge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_military_experience = ' . $this->sqlHasMilitaryExperience() . ','; } elseif( true == array_key_exists( 'HasMilitaryExperience', $this->getChangedColumns() ) ) { $strSql .= ' has_military_experience = ' . $this->sqlHasMilitaryExperience() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_branch = ' . $this->sqlMilitaryBranch() . ','; } elseif( true == array_key_exists( 'MilitaryBranch', $this->getChangedColumns() ) ) { $strSql .= ' military_branch = ' . $this->sqlMilitaryBranch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_training = ' . $this->sqlMilitaryTraining() . ','; } elseif( true == array_key_exists( 'MilitaryTraining', $this->getChangedColumns() ) ) { $strSql .= ' military_training = ' . $this->sqlMilitaryTraining() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_ged = ' . $this->sqlHasGed() . ','; } elseif( true == array_key_exists( 'HasGed', $this->getChangedColumns() ) ) { $strSql .= ' has_ged = ' . $this->sqlHasGed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_hs_diploma = ' . $this->sqlHasHsDiploma() . ','; } elseif( true == array_key_exists( 'HasHsDiploma', $this->getChangedColumns() ) ) { $strSql .= ' has_hs_diploma = ' . $this->sqlHasHsDiploma() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'candidate_id' => $this->getCandidateId(),
			'is_legally_authorized' => $this->getIsLegallyAuthorized(),
			'has_conviction' => $this->getHasConviction(),
			'conviction_description' => $this->getConvictionDescription(),
			'words_per_minute' => $this->getWordsPerMinute(),
			'knows_ten_key' => $this->getKnowsTenKey(),
			'computer_skills' => $this->getComputerSkills(),
			'other_special_skills' => $this->getOtherSpecialSkills(),
			'minimum_salary' => $this->getMinimumSalary(),
			'minimum_weekly_hours' => $this->getMinimumWeeklyHours(),
			'maximum_weekly_hours' => $this->getMaximumWeeklyHours(),
			'age' => $this->getAge(),
			'has_military_experience' => $this->getHasMilitaryExperience(),
			'military_branch' => $this->getMilitaryBranch(),
			'military_training' => $this->getMilitaryTraining(),
			'has_ged' => $this->getHasGed(),
			'has_hs_diploma' => $this->getHasHsDiploma(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>