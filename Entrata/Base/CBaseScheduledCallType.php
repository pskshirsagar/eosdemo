<?php

class CBaseScheduledCallType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.scheduled_call_types';

	protected $m_intId;
	protected $m_intCallFrequencyId;
	protected $m_intCallPriorityId;
	protected $m_strBeginSendTime;
	protected $m_strEndSendTime;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFrequencyId', trim( $arrValues['call_frequency_id'] ) ); elseif( isset( $arrValues['call_frequency_id'] ) ) $this->setCallFrequencyId( $arrValues['call_frequency_id'] );
		if( isset( $arrValues['call_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPriorityId', trim( $arrValues['call_priority_id'] ) ); elseif( isset( $arrValues['call_priority_id'] ) ) $this->setCallPriorityId( $arrValues['call_priority_id'] );
		if( isset( $arrValues['begin_send_time'] ) && $boolDirectSet ) $this->set( 'm_strBeginSendTime', trim( $arrValues['begin_send_time'] ) ); elseif( isset( $arrValues['begin_send_time'] ) ) $this->setBeginSendTime( $arrValues['begin_send_time'] );
		if( isset( $arrValues['end_send_time'] ) && $boolDirectSet ) $this->set( 'm_strEndSendTime', trim( $arrValues['end_send_time'] ) ); elseif( isset( $arrValues['end_send_time'] ) ) $this->setEndSendTime( $arrValues['end_send_time'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallFrequencyId( $intCallFrequencyId ) {
		$this->set( 'm_intCallFrequencyId', CStrings::strToIntDef( $intCallFrequencyId, NULL, false ) );
	}

	public function getCallFrequencyId() {
		return $this->m_intCallFrequencyId;
	}

	public function sqlCallFrequencyId() {
		return ( true == isset( $this->m_intCallFrequencyId ) ) ? ( string ) $this->m_intCallFrequencyId : 'NULL';
	}

	public function setCallPriorityId( $intCallPriorityId ) {
		$this->set( 'm_intCallPriorityId', CStrings::strToIntDef( $intCallPriorityId, NULL, false ) );
	}

	public function getCallPriorityId() {
		return $this->m_intCallPriorityId;
	}

	public function sqlCallPriorityId() {
		return ( true == isset( $this->m_intCallPriorityId ) ) ? ( string ) $this->m_intCallPriorityId : 'NULL';
	}

	public function setBeginSendTime( $strBeginSendTime ) {
		$this->set( 'm_strBeginSendTime', CStrings::strTrimDef( $strBeginSendTime, NULL, NULL, true ) );
	}

	public function getBeginSendTime() {
		return $this->m_strBeginSendTime;
	}

	public function sqlBeginSendTime() {
		return ( true == isset( $this->m_strBeginSendTime ) ) ? '\'' . $this->m_strBeginSendTime . '\'' : 'NULL';
	}

	public function setEndSendTime( $strEndSendTime ) {
		$this->set( 'm_strEndSendTime', CStrings::strTrimDef( $strEndSendTime, NULL, NULL, true ) );
	}

	public function getEndSendTime() {
		return $this->m_strEndSendTime;
	}

	public function sqlEndSendTime() {
		return ( true == isset( $this->m_strEndSendTime ) ) ? '\'' . $this->m_strEndSendTime . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_frequency_id' => $this->getCallFrequencyId(),
			'call_priority_id' => $this->getCallPriorityId(),
			'begin_send_time' => $this->getBeginSendTime(),
			'end_send_time' => $this->getEndSendTime(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>