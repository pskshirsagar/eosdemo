<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccountTree extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.gl_account_trees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlTreeId;
	protected $m_intGlBranchId;
	protected $m_intGlAccountId;
	protected $m_intGlAccountTreeId;
	protected $m_intGroupingGlAccountId;
	protected $m_intGlGroupTypeId;
	protected $m_strName;
	protected $m_strAccountNumber;
	protected $m_strFormattedAccountNumber;
	protected $m_strSecondaryNumber;
	protected $m_strDescription;
	protected $m_intHideIfZero;
	protected $m_intIsSystem;
	protected $m_intIsDefault;
	protected $m_intHideAccountBalanceFromReports;
	protected $m_intOrderNum;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intGlGroupId;
	protected $m_intGlAccountTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intHideIfZero = '0';
		$this->m_intIsSystem = '0';
		$this->m_intIsDefault = '0';
		$this->m_intHideAccountBalanceFromReports = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeId', trim( $arrValues['gl_tree_id'] ) ); elseif( isset( $arrValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrValues['gl_tree_id'] );
		if( isset( $arrValues['gl_branch_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBranchId', trim( $arrValues['gl_branch_id'] ) ); elseif( isset( $arrValues['gl_branch_id'] ) ) $this->setGlBranchId( $arrValues['gl_branch_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['gl_account_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTreeId', trim( $arrValues['gl_account_tree_id'] ) ); elseif( isset( $arrValues['gl_account_tree_id'] ) ) $this->setGlAccountTreeId( $arrValues['gl_account_tree_id'] );
		if( isset( $arrValues['grouping_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupingGlAccountId', trim( $arrValues['grouping_gl_account_id'] ) ); elseif( isset( $arrValues['grouping_gl_account_id'] ) ) $this->setGroupingGlAccountId( $arrValues['grouping_gl_account_id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( $arrValues['account_number'] ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( $arrValues['account_number'] );
		if( isset( $arrValues['formatted_account_number'] ) && $boolDirectSet ) $this->set( 'm_strFormattedAccountNumber', trim( $arrValues['formatted_account_number'] ) ); elseif( isset( $arrValues['formatted_account_number'] ) ) $this->setFormattedAccountNumber( $arrValues['formatted_account_number'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( $arrValues['secondary_number'] ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrValues['secondary_number'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['hide_if_zero'] ) && $boolDirectSet ) $this->set( 'm_intHideIfZero', trim( $arrValues['hide_if_zero'] ) ); elseif( isset( $arrValues['hide_if_zero'] ) ) $this->setHideIfZero( $arrValues['hide_if_zero'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['hide_account_balance_from_reports'] ) && $boolDirectSet ) $this->set( 'm_intHideAccountBalanceFromReports', trim( $arrValues['hide_account_balance_from_reports'] ) ); elseif( isset( $arrValues['hide_account_balance_from_reports'] ) ) $this->setHideAccountBalanceFromReports( $arrValues['hide_account_balance_from_reports'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupId', trim( $arrValues['gl_group_id'] ) ); elseif( isset( $arrValues['gl_group_id'] ) ) $this->setGlGroupId( $arrValues['gl_group_id'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlTreeId( $intGlTreeId ) {
		$this->set( 'm_intGlTreeId', CStrings::strToIntDef( $intGlTreeId, NULL, false ) );
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function sqlGlTreeId() {
		return ( true == isset( $this->m_intGlTreeId ) ) ? ( string ) $this->m_intGlTreeId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlBranchId( $intGlBranchId ) {
		$this->set( 'm_intGlBranchId', CStrings::strToIntDef( $intGlBranchId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlBranchId() {
		return $this->m_intGlBranchId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlBranchId() {
		return ( true == isset( $this->m_intGlBranchId ) ) ? ( string ) $this->m_intGlBranchId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setGlAccountTreeId( $intGlAccountTreeId ) {
		$this->set( 'm_intGlAccountTreeId', CStrings::strToIntDef( $intGlAccountTreeId, NULL, false ) );
	}

	public function getGlAccountTreeId() {
		return $this->m_intGlAccountTreeId;
	}

	public function sqlGlAccountTreeId() {
		return ( true == isset( $this->m_intGlAccountTreeId ) ) ? ( string ) $this->m_intGlAccountTreeId : 'NULL';
	}

	public function setGroupingGlAccountId( $intGroupingGlAccountId ) {
		$this->set( 'm_intGroupingGlAccountId', CStrings::strToIntDef( $intGroupingGlAccountId, NULL, false ) );
	}

	public function getGroupingGlAccountId() {
		return $this->m_intGroupingGlAccountId;
	}

	public function sqlGroupingGlAccountId() {
		return ( true == isset( $this->m_intGroupingGlAccountId ) ) ? ( string ) $this->m_intGroupingGlAccountId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 80, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 80, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumber ) : '\'' . addslashes( $this->m_strAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setFormattedAccountNumber( $strFormattedAccountNumber ) {
		$this->set( 'm_strFormattedAccountNumber', CStrings::strTrimDef( $strFormattedAccountNumber, 100, NULL, true ) );
	}

	public function getFormattedAccountNumber() {
		return $this->m_strFormattedAccountNumber;
	}

	public function sqlFormattedAccountNumber() {
		return ( true == isset( $this->m_strFormattedAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFormattedAccountNumber ) : '\'' . addslashes( $this->m_strFormattedAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 50, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryNumber ) : '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setHideIfZero( $intHideIfZero ) {
		$this->set( 'm_intHideIfZero', CStrings::strToIntDef( $intHideIfZero, NULL, false ) );
	}

	public function getHideIfZero() {
		return $this->m_intHideIfZero;
	}

	public function sqlHideIfZero() {
		return ( true == isset( $this->m_intHideIfZero ) ) ? ( string ) $this->m_intHideIfZero : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setHideAccountBalanceFromReports( $intHideAccountBalanceFromReports ) {
		$this->set( 'm_intHideAccountBalanceFromReports', CStrings::strToIntDef( $intHideAccountBalanceFromReports, NULL, false ) );
	}

	public function getHideAccountBalanceFromReports() {
		return $this->m_intHideAccountBalanceFromReports;
	}

	public function sqlHideAccountBalanceFromReports() {
		return ( true == isset( $this->m_intHideAccountBalanceFromReports ) ) ? ( string ) $this->m_intHideAccountBalanceFromReports : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGlGroupId( $intGlGroupId ) {
		$this->set( 'm_intGlGroupId', CStrings::strToIntDef( $intGlGroupId, NULL, false ) );
	}

	public function getGlGroupId() {
		return $this->m_intGlGroupId;
	}

	public function sqlGlGroupId() {
		return ( true == isset( $this->m_intGlGroupId ) ) ? ( string ) $this->m_intGlGroupId : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_tree_id, gl_branch_id, gl_account_id, gl_account_tree_id, grouping_gl_account_id, gl_group_type_id, name, account_number, formatted_account_number, secondary_number, description, hide_if_zero, is_system, is_default, hide_account_balance_from_reports, order_num, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, details, gl_group_id, gl_account_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlTreeId() . ', ' .
						$this->sqlGlBranchId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlGlAccountTreeId() . ', ' .
						$this->sqlGroupingGlAccountId() . ', ' .
						$this->sqlGlGroupTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAccountNumber() . ', ' .
						$this->sqlFormattedAccountNumber() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlHideIfZero() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlHideAccountBalanceFromReports() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlGlGroupId() . ', ' .
						$this->sqlGlAccountTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId(). ',' ; } elseif( true == array_key_exists( 'GlBranchId', $this->getChangedColumns() ) ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_tree_id = ' . $this->sqlGlAccountTreeId(). ',' ; } elseif( true == array_key_exists( 'GlAccountTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_tree_id = ' . $this->sqlGlAccountTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grouping_gl_account_id = ' . $this->sqlGroupingGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GroupingGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' grouping_gl_account_id = ' . $this->sqlGroupingGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId(). ',' ; } elseif( true == array_key_exists( 'GlGroupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber(). ',' ; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' formatted_account_number = ' . $this->sqlFormattedAccountNumber(). ',' ; } elseif( true == array_key_exists( 'FormattedAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' formatted_account_number = ' . $this->sqlFormattedAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber(). ',' ; } elseif( true == array_key_exists( 'SecondaryNumber', $this->getChangedColumns() ) ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_if_zero = ' . $this->sqlHideIfZero(). ',' ; } elseif( true == array_key_exists( 'HideIfZero', $this->getChangedColumns() ) ) { $strSql .= ' hide_if_zero = ' . $this->sqlHideIfZero() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_account_balance_from_reports = ' . $this->sqlHideAccountBalanceFromReports(). ',' ; } elseif( true == array_key_exists( 'HideAccountBalanceFromReports', $this->getChangedColumns() ) ) { $strSql .= ' hide_account_balance_from_reports = ' . $this->sqlHideAccountBalanceFromReports() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId(). ',' ; } elseif( true == array_key_exists( 'GlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'GlAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_tree_id' => $this->getGlTreeId(),
			'gl_branch_id' => $this->getGlBranchId(),
			'gl_account_id' => $this->getGlAccountId(),
			'gl_account_tree_id' => $this->getGlAccountTreeId(),
			'grouping_gl_account_id' => $this->getGroupingGlAccountId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'name' => $this->getName(),
			'account_number' => $this->getAccountNumber(),
			'formatted_account_number' => $this->getFormattedAccountNumber(),
			'secondary_number' => $this->getSecondaryNumber(),
			'description' => $this->getDescription(),
			'hide_if_zero' => $this->getHideIfZero(),
			'is_system' => $this->getIsSystem(),
			'is_default' => $this->getIsDefault(),
			'hide_account_balance_from_reports' => $this->getHideAccountBalanceFromReports(),
			'order_num' => $this->getOrderNum(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'gl_group_id' => $this->getGlGroupId(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>