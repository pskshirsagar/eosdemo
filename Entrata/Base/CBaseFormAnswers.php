<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFormAnswers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFormAnswers extends CEosPluralBase {

	/**
	 * @return CFormAnswer[]
	 */
	public static function fetchFormAnswers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFormAnswer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFormAnswer
	 */
	public static function fetchFormAnswer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFormAnswer', $objDatabase );
	}

	public static function fetchFormAnswerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'form_answers', $objDatabase );
	}

	public static function fetchFormAnswerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFormAnswer( sprintf( 'SELECT * FROM form_answers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormAnswersByCid( $intCid, $objDatabase ) {
		return self::fetchFormAnswers( sprintf( 'SELECT * FROM form_answers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormAnswersByFormIdByCid( $intFormId, $intCid, $objDatabase ) {
		return self::fetchFormAnswers( sprintf( 'SELECT * FROM form_answers WHERE form_id = %d AND cid = %d', ( int ) $intFormId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormAnswersByDocumentComponentIdByCid( $intDocumentComponentId, $intCid, $objDatabase ) {
		return self::fetchFormAnswers( sprintf( 'SELECT * FROM form_answers WHERE document_component_id = %d AND cid = %d', ( int ) $intDocumentComponentId, ( int ) $intCid ), $objDatabase );
	}

}
?>