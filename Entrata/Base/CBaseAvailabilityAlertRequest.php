<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAvailabilityAlertRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.availability_alert_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intApplicationDuplicateId;
	protected $m_intMessageOperatorId;
	protected $m_intTimeZoneId;
	protected $m_intAvailabilityAlertFrequencyId;
	protected $m_strEmailAddress;
	protected $m_strCellNumber;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strAvailableOnStartDate;
	protected $m_strAvailableOnEndDate;
	protected $m_intSendStartHour;
	protected $m_intSendStopHour;
	protected $m_strLastEmailAlertedOn;
	protected $m_strLastTextAlertedOn;
	protected $m_intIsEmailAlert;
	protected $m_intIsTextAlert;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsEmailAlert = '0';
		$this->m_intIsTextAlert = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['application_duplicate_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationDuplicateId', trim( $arrValues['application_duplicate_id'] ) ); elseif( isset( $arrValues['application_duplicate_id'] ) ) $this->setApplicationDuplicateId( $arrValues['application_duplicate_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['availability_alert_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intAvailabilityAlertFrequencyId', trim( $arrValues['availability_alert_frequency_id'] ) ); elseif( isset( $arrValues['availability_alert_frequency_id'] ) ) $this->setAvailabilityAlertFrequencyId( $arrValues['availability_alert_frequency_id'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['cell_number'] ) && $boolDirectSet ) $this->set( 'm_strCellNumber', trim( stripcslashes( $arrValues['cell_number'] ) ) ); elseif( isset( $arrValues['cell_number'] ) ) $this->setCellNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cell_number'] ) : $arrValues['cell_number'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['available_on_start_date'] ) && $boolDirectSet ) $this->set( 'm_strAvailableOnStartDate', trim( $arrValues['available_on_start_date'] ) ); elseif( isset( $arrValues['available_on_start_date'] ) ) $this->setAvailableOnStartDate( $arrValues['available_on_start_date'] );
		if( isset( $arrValues['available_on_end_date'] ) && $boolDirectSet ) $this->set( 'm_strAvailableOnEndDate', trim( $arrValues['available_on_end_date'] ) ); elseif( isset( $arrValues['available_on_end_date'] ) ) $this->setAvailableOnEndDate( $arrValues['available_on_end_date'] );
		if( isset( $arrValues['send_start_hour'] ) && $boolDirectSet ) $this->set( 'm_intSendStartHour', trim( $arrValues['send_start_hour'] ) ); elseif( isset( $arrValues['send_start_hour'] ) ) $this->setSendStartHour( $arrValues['send_start_hour'] );
		if( isset( $arrValues['send_stop_hour'] ) && $boolDirectSet ) $this->set( 'm_intSendStopHour', trim( $arrValues['send_stop_hour'] ) ); elseif( isset( $arrValues['send_stop_hour'] ) ) $this->setSendStopHour( $arrValues['send_stop_hour'] );
		if( isset( $arrValues['last_email_alerted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastEmailAlertedOn', trim( $arrValues['last_email_alerted_on'] ) ); elseif( isset( $arrValues['last_email_alerted_on'] ) ) $this->setLastEmailAlertedOn( $arrValues['last_email_alerted_on'] );
		if( isset( $arrValues['last_text_alerted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastTextAlertedOn', trim( $arrValues['last_text_alerted_on'] ) ); elseif( isset( $arrValues['last_text_alerted_on'] ) ) $this->setLastTextAlertedOn( $arrValues['last_text_alerted_on'] );
		if( isset( $arrValues['is_email_alert'] ) && $boolDirectSet ) $this->set( 'm_intIsEmailAlert', trim( $arrValues['is_email_alert'] ) ); elseif( isset( $arrValues['is_email_alert'] ) ) $this->setIsEmailAlert( $arrValues['is_email_alert'] );
		if( isset( $arrValues['is_text_alert'] ) && $boolDirectSet ) $this->set( 'm_intIsTextAlert', trim( $arrValues['is_text_alert'] ) ); elseif( isset( $arrValues['is_text_alert'] ) ) $this->setIsTextAlert( $arrValues['is_text_alert'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApplicationDuplicateId( $intApplicationDuplicateId ) {
		$this->set( 'm_intApplicationDuplicateId', CStrings::strToIntDef( $intApplicationDuplicateId, NULL, false ) );
	}

	public function getApplicationDuplicateId() {
		return $this->m_intApplicationDuplicateId;
	}

	public function sqlApplicationDuplicateId() {
		return ( true == isset( $this->m_intApplicationDuplicateId ) ) ? ( string ) $this->m_intApplicationDuplicateId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setAvailabilityAlertFrequencyId( $intAvailabilityAlertFrequencyId ) {
		$this->set( 'm_intAvailabilityAlertFrequencyId', CStrings::strToIntDef( $intAvailabilityAlertFrequencyId, NULL, false ) );
	}

	public function getAvailabilityAlertFrequencyId() {
		return $this->m_intAvailabilityAlertFrequencyId;
	}

	public function sqlAvailabilityAlertFrequencyId() {
		return ( true == isset( $this->m_intAvailabilityAlertFrequencyId ) ) ? ( string ) $this->m_intAvailabilityAlertFrequencyId : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setCellNumber( $strCellNumber ) {
		$this->set( 'm_strCellNumber', CStrings::strTrimDef( $strCellNumber, 30, NULL, true ) );
	}

	public function getCellNumber() {
		return $this->m_strCellNumber;
	}

	public function sqlCellNumber() {
		return ( true == isset( $this->m_strCellNumber ) ) ? '\'' . addslashes( $this->m_strCellNumber ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setAvailableOnStartDate( $strAvailableOnStartDate ) {
		$this->set( 'm_strAvailableOnStartDate', CStrings::strTrimDef( $strAvailableOnStartDate, -1, NULL, true ) );
	}

	public function getAvailableOnStartDate() {
		return $this->m_strAvailableOnStartDate;
	}

	public function sqlAvailableOnStartDate() {
		return ( true == isset( $this->m_strAvailableOnStartDate ) ) ? '\'' . $this->m_strAvailableOnStartDate . '\'' : 'NULL';
	}

	public function setAvailableOnEndDate( $strAvailableOnEndDate ) {
		$this->set( 'm_strAvailableOnEndDate', CStrings::strTrimDef( $strAvailableOnEndDate, -1, NULL, true ) );
	}

	public function getAvailableOnEndDate() {
		return $this->m_strAvailableOnEndDate;
	}

	public function sqlAvailableOnEndDate() {
		return ( true == isset( $this->m_strAvailableOnEndDate ) ) ? '\'' . $this->m_strAvailableOnEndDate . '\'' : 'NULL';
	}

	public function setSendStartHour( $intSendStartHour ) {
		$this->set( 'm_intSendStartHour', CStrings::strToIntDef( $intSendStartHour, NULL, false ) );
	}

	public function getSendStartHour() {
		return $this->m_intSendStartHour;
	}

	public function sqlSendStartHour() {
		return ( true == isset( $this->m_intSendStartHour ) ) ? ( string ) $this->m_intSendStartHour : 'NULL';
	}

	public function setSendStopHour( $intSendStopHour ) {
		$this->set( 'm_intSendStopHour', CStrings::strToIntDef( $intSendStopHour, NULL, false ) );
	}

	public function getSendStopHour() {
		return $this->m_intSendStopHour;
	}

	public function sqlSendStopHour() {
		return ( true == isset( $this->m_intSendStopHour ) ) ? ( string ) $this->m_intSendStopHour : 'NULL';
	}

	public function setLastEmailAlertedOn( $strLastEmailAlertedOn ) {
		$this->set( 'm_strLastEmailAlertedOn', CStrings::strTrimDef( $strLastEmailAlertedOn, -1, NULL, true ) );
	}

	public function getLastEmailAlertedOn() {
		return $this->m_strLastEmailAlertedOn;
	}

	public function sqlLastEmailAlertedOn() {
		return ( true == isset( $this->m_strLastEmailAlertedOn ) ) ? '\'' . $this->m_strLastEmailAlertedOn . '\'' : 'NULL';
	}

	public function setLastTextAlertedOn( $strLastTextAlertedOn ) {
		$this->set( 'm_strLastTextAlertedOn', CStrings::strTrimDef( $strLastTextAlertedOn, -1, NULL, true ) );
	}

	public function getLastTextAlertedOn() {
		return $this->m_strLastTextAlertedOn;
	}

	public function sqlLastTextAlertedOn() {
		return ( true == isset( $this->m_strLastTextAlertedOn ) ) ? '\'' . $this->m_strLastTextAlertedOn . '\'' : 'NULL';
	}

	public function setIsEmailAlert( $intIsEmailAlert ) {
		$this->set( 'm_intIsEmailAlert', CStrings::strToIntDef( $intIsEmailAlert, NULL, false ) );
	}

	public function getIsEmailAlert() {
		return $this->m_intIsEmailAlert;
	}

	public function sqlIsEmailAlert() {
		return ( true == isset( $this->m_intIsEmailAlert ) ) ? ( string ) $this->m_intIsEmailAlert : '0';
	}

	public function setIsTextAlert( $intIsTextAlert ) {
		$this->set( 'm_intIsTextAlert', CStrings::strToIntDef( $intIsTextAlert, NULL, false ) );
	}

	public function getIsTextAlert() {
		return $this->m_intIsTextAlert;
	}

	public function sqlIsTextAlert() {
		return ( true == isset( $this->m_intIsTextAlert ) ) ? ( string ) $this->m_intIsTextAlert : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, application_id, application_duplicate_id, message_operator_id, time_zone_id, availability_alert_frequency_id, email_address, cell_number, start_date, end_date, available_on_start_date, available_on_end_date, send_start_hour, send_stop_hour, last_email_alerted_on, last_text_alerted_on, is_email_alert, is_text_alert, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicantId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlApplicationDuplicateId() . ', ' .
 						$this->sqlMessageOperatorId() . ', ' .
 						$this->sqlTimeZoneId() . ', ' .
 						$this->sqlAvailabilityAlertFrequencyId() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlCellNumber() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlAvailableOnStartDate() . ', ' .
 						$this->sqlAvailableOnEndDate() . ', ' .
 						$this->sqlSendStartHour() . ', ' .
 						$this->sqlSendStopHour() . ', ' .
 						$this->sqlLastEmailAlertedOn() . ', ' .
 						$this->sqlLastTextAlertedOn() . ', ' .
 						$this->sqlIsEmailAlert() . ', ' .
 						$this->sqlIsTextAlert() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_duplicate_id = ' . $this->sqlApplicationDuplicateId() . ','; } elseif( true == array_key_exists( 'ApplicationDuplicateId', $this->getChangedColumns() ) ) { $strSql .= ' application_duplicate_id = ' . $this->sqlApplicationDuplicateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; } elseif( true == array_key_exists( 'MessageOperatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' availability_alert_frequency_id = ' . $this->sqlAvailabilityAlertFrequencyId() . ','; } elseif( true == array_key_exists( 'AvailabilityAlertFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' availability_alert_frequency_id = ' . $this->sqlAvailabilityAlertFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cell_number = ' . $this->sqlCellNumber() . ','; } elseif( true == array_key_exists( 'CellNumber', $this->getChangedColumns() ) ) { $strSql .= ' cell_number = ' . $this->sqlCellNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on_start_date = ' . $this->sqlAvailableOnStartDate() . ','; } elseif( true == array_key_exists( 'AvailableOnStartDate', $this->getChangedColumns() ) ) { $strSql .= ' available_on_start_date = ' . $this->sqlAvailableOnStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on_end_date = ' . $this->sqlAvailableOnEndDate() . ','; } elseif( true == array_key_exists( 'AvailableOnEndDate', $this->getChangedColumns() ) ) { $strSql .= ' available_on_end_date = ' . $this->sqlAvailableOnEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_start_hour = ' . $this->sqlSendStartHour() . ','; } elseif( true == array_key_exists( 'SendStartHour', $this->getChangedColumns() ) ) { $strSql .= ' send_start_hour = ' . $this->sqlSendStartHour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_stop_hour = ' . $this->sqlSendStopHour() . ','; } elseif( true == array_key_exists( 'SendStopHour', $this->getChangedColumns() ) ) { $strSql .= ' send_stop_hour = ' . $this->sqlSendStopHour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_email_alerted_on = ' . $this->sqlLastEmailAlertedOn() . ','; } elseif( true == array_key_exists( 'LastEmailAlertedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_email_alerted_on = ' . $this->sqlLastEmailAlertedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_text_alerted_on = ' . $this->sqlLastTextAlertedOn() . ','; } elseif( true == array_key_exists( 'LastTextAlertedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_text_alerted_on = ' . $this->sqlLastTextAlertedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_email_alert = ' . $this->sqlIsEmailAlert() . ','; } elseif( true == array_key_exists( 'IsEmailAlert', $this->getChangedColumns() ) ) { $strSql .= ' is_email_alert = ' . $this->sqlIsEmailAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_text_alert = ' . $this->sqlIsTextAlert() . ','; } elseif( true == array_key_exists( 'IsTextAlert', $this->getChangedColumns() ) ) { $strSql .= ' is_text_alert = ' . $this->sqlIsTextAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'application_duplicate_id' => $this->getApplicationDuplicateId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'availability_alert_frequency_id' => $this->getAvailabilityAlertFrequencyId(),
			'email_address' => $this->getEmailAddress(),
			'cell_number' => $this->getCellNumber(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'available_on_start_date' => $this->getAvailableOnStartDate(),
			'available_on_end_date' => $this->getAvailableOnEndDate(),
			'send_start_hour' => $this->getSendStartHour(),
			'send_stop_hour' => $this->getSendStopHour(),
			'last_email_alerted_on' => $this->getLastEmailAlertedOn(),
			'last_text_alerted_on' => $this->getLastTextAlertedOn(),
			'is_email_alert' => $this->getIsEmailAlert(),
			'is_text_alert' => $this->getIsTextAlert(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>