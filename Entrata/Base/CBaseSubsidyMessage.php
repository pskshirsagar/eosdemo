<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyMessage extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_messages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyRequestId;
	protected $m_intSubsidyMessageTypeId;
	protected $m_intSubsidyMessageSubTypeId;
	protected $m_intSubsidyMessageStatusTypeId;
	protected $m_intSubsidyMessageId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSender;
	protected $m_arrstrRecipients;
	protected $m_strSubject;
	protected $m_strMessage;
	protected $m_strSentToImaxDate;
	protected $m_strSentToTracsDate;
	protected $m_boolIsArchived;
	protected $m_boolHasBeenViewed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsArchived = false;
		$this->m_boolHasBeenViewed = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_request_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyRequestId', trim( $arrValues['subsidy_request_id'] ) ); elseif( isset( $arrValues['subsidy_request_id'] ) ) $this->setSubsidyRequestId( $arrValues['subsidy_request_id'] );
		if( isset( $arrValues['subsidy_message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMessageTypeId', trim( $arrValues['subsidy_message_type_id'] ) ); elseif( isset( $arrValues['subsidy_message_type_id'] ) ) $this->setSubsidyMessageTypeId( $arrValues['subsidy_message_type_id'] );
		if( isset( $arrValues['subsidy_message_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMessageSubTypeId', trim( $arrValues['subsidy_message_sub_type_id'] ) ); elseif( isset( $arrValues['subsidy_message_sub_type_id'] ) ) $this->setSubsidyMessageSubTypeId( $arrValues['subsidy_message_sub_type_id'] );
		if( isset( $arrValues['subsidy_message_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMessageStatusTypeId', trim( $arrValues['subsidy_message_status_type_id'] ) ); elseif( isset( $arrValues['subsidy_message_status_type_id'] ) ) $this->setSubsidyMessageStatusTypeId( $arrValues['subsidy_message_status_type_id'] );
		if( isset( $arrValues['subsidy_message_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMessageId', trim( $arrValues['subsidy_message_id'] ) ); elseif( isset( $arrValues['subsidy_message_id'] ) ) $this->setSubsidyMessageId( $arrValues['subsidy_message_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['sender'] ) && $boolDirectSet ) $this->set( 'm_strSender', trim( stripcslashes( $arrValues['sender'] ) ) ); elseif( isset( $arrValues['sender'] ) ) $this->setSender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sender'] ) : $arrValues['sender'] );
		if( isset( $arrValues['recipients'] ) && $boolDirectSet ) $this->set( 'm_arrstrRecipients', trim( $arrValues['recipients'] ) ); elseif( isset( $arrValues['recipients'] ) ) $this->setRecipients( $arrValues['recipients'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['sent_to_imax_date'] ) && $boolDirectSet ) $this->set( 'm_strSentToImaxDate', trim( $arrValues['sent_to_imax_date'] ) ); elseif( isset( $arrValues['sent_to_imax_date'] ) ) $this->setSentToImaxDate( $arrValues['sent_to_imax_date'] );
		if( isset( $arrValues['sent_to_tracs_date'] ) && $boolDirectSet ) $this->set( 'm_strSentToTracsDate', trim( $arrValues['sent_to_tracs_date'] ) ); elseif( isset( $arrValues['sent_to_tracs_date'] ) ) $this->setSentToTracsDate( $arrValues['sent_to_tracs_date'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchived', trim( stripcslashes( $arrValues['is_archived'] ) ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived'] ) : $arrValues['is_archived'] );
		if( isset( $arrValues['has_been_viewed'] ) && $boolDirectSet ) $this->set( 'm_boolHasBeenViewed', trim( stripcslashes( $arrValues['has_been_viewed'] ) ) ); elseif( isset( $arrValues['has_been_viewed'] ) ) $this->setHasBeenViewed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_been_viewed'] ) : $arrValues['has_been_viewed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyRequestId( $intSubsidyRequestId ) {
		$this->set( 'm_intSubsidyRequestId', CStrings::strToIntDef( $intSubsidyRequestId, NULL, false ) );
	}

	public function getSubsidyRequestId() {
		return $this->m_intSubsidyRequestId;
	}

	public function sqlSubsidyRequestId() {
		return ( true == isset( $this->m_intSubsidyRequestId ) ) ? ( string ) $this->m_intSubsidyRequestId : 'NULL';
	}

	public function setSubsidyMessageTypeId( $intSubsidyMessageTypeId ) {
		$this->set( 'm_intSubsidyMessageTypeId', CStrings::strToIntDef( $intSubsidyMessageTypeId, NULL, false ) );
	}

	public function getSubsidyMessageTypeId() {
		return $this->m_intSubsidyMessageTypeId;
	}

	public function sqlSubsidyMessageTypeId() {
		return ( true == isset( $this->m_intSubsidyMessageTypeId ) ) ? ( string ) $this->m_intSubsidyMessageTypeId : 'NULL';
	}

	public function setSubsidyMessageSubTypeId( $intSubsidyMessageSubTypeId ) {
		$this->set( 'm_intSubsidyMessageSubTypeId', CStrings::strToIntDef( $intSubsidyMessageSubTypeId, NULL, false ) );
	}

	public function getSubsidyMessageSubTypeId() {
		return $this->m_intSubsidyMessageSubTypeId;
	}

	public function sqlSubsidyMessageSubTypeId() {
		return ( true == isset( $this->m_intSubsidyMessageSubTypeId ) ) ? ( string ) $this->m_intSubsidyMessageSubTypeId : 'NULL';
	}

	public function setSubsidyMessageStatusTypeId( $intSubsidyMessageStatusTypeId ) {
		$this->set( 'm_intSubsidyMessageStatusTypeId', CStrings::strToIntDef( $intSubsidyMessageStatusTypeId, NULL, false ) );
	}

	public function getSubsidyMessageStatusTypeId() {
		return $this->m_intSubsidyMessageStatusTypeId;
	}

	public function sqlSubsidyMessageStatusTypeId() {
		return ( true == isset( $this->m_intSubsidyMessageStatusTypeId ) ) ? ( string ) $this->m_intSubsidyMessageStatusTypeId : 'NULL';
	}

	public function setSubsidyMessageId( $intSubsidyMessageId ) {
		$this->set( 'm_intSubsidyMessageId', CStrings::strToIntDef( $intSubsidyMessageId, NULL, false ) );
	}

	public function getSubsidyMessageId() {
		return $this->m_intSubsidyMessageId;
	}

	public function sqlSubsidyMessageId() {
		return ( true == isset( $this->m_intSubsidyMessageId ) ) ? ( string ) $this->m_intSubsidyMessageId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 240, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setSender( $strSender ) {
		$this->set( 'm_strSender', CStrings::strTrimDef( $strSender, 240, NULL, true ) );
	}

	public function getSender() {
		return $this->m_strSender;
	}

	public function sqlSender() {
		return ( true == isset( $this->m_strSender ) ) ? '\'' . addslashes( $this->m_strSender ) . '\'' : 'NULL';
	}

	public function setRecipients( $arrstrRecipients ) {
		$this->set( 'm_arrstrRecipients', CStrings::strToArrIntDef( $arrstrRecipients, NULL ) );
	}

	public function getRecipients() {
		return $this->m_arrstrRecipients;
	}

	public function sqlRecipients() {
		return ( true == isset( $this->m_arrstrRecipients ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrRecipients, NULL ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setSentToImaxDate( $strSentToImaxDate ) {
		$this->set( 'm_strSentToImaxDate', CStrings::strTrimDef( $strSentToImaxDate, -1, NULL, true ) );
	}

	public function getSentToImaxDate() {
		return $this->m_strSentToImaxDate;
	}

	public function sqlSentToImaxDate() {
		return ( true == isset( $this->m_strSentToImaxDate ) ) ? '\'' . $this->m_strSentToImaxDate . '\'' : 'NOW()';
	}

	public function setSentToTracsDate( $strSentToTracsDate ) {
		$this->set( 'm_strSentToTracsDate', CStrings::strTrimDef( $strSentToTracsDate, -1, NULL, true ) );
	}

	public function getSentToTracsDate() {
		return $this->m_strSentToTracsDate;
	}

	public function sqlSentToTracsDate() {
		return ( true == isset( $this->m_strSentToTracsDate ) ) ? '\'' . $this->m_strSentToTracsDate . '\'' : 'NULL';
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_boolIsArchived ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchived ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasBeenViewed( $boolHasBeenViewed ) {
		$this->set( 'm_boolHasBeenViewed', CStrings::strToBool( $boolHasBeenViewed ) );
	}

	public function getHasBeenViewed() {
		return $this->m_boolHasBeenViewed;
	}

	public function sqlHasBeenViewed() {
		return ( true == isset( $this->m_boolHasBeenViewed ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasBeenViewed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_request_id, subsidy_message_type_id, subsidy_message_sub_type_id, subsidy_message_status_type_id, subsidy_message_id, remote_primary_key, sender, recipients, subject, message, sent_to_imax_date, sent_to_tracs_date, is_archived, has_been_viewed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidyRequestId() . ', ' .
 						$this->sqlSubsidyMessageTypeId() . ', ' .
 						$this->sqlSubsidyMessageSubTypeId() . ', ' .
 						$this->sqlSubsidyMessageStatusTypeId() . ', ' .
 						$this->sqlSubsidyMessageId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlSender() . ', ' .
 						$this->sqlRecipients() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlSentToImaxDate() . ', ' .
 						$this->sqlSentToTracsDate() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlHasBeenViewed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_request_id = ' . $this->sqlSubsidyRequestId() . ','; } elseif( true == array_key_exists( 'SubsidyRequestId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_request_id = ' . $this->sqlSubsidyRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_message_type_id = ' . $this->sqlSubsidyMessageTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyMessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_message_type_id = ' . $this->sqlSubsidyMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_message_sub_type_id = ' . $this->sqlSubsidyMessageSubTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyMessageSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_message_sub_type_id = ' . $this->sqlSubsidyMessageSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_message_status_type_id = ' . $this->sqlSubsidyMessageStatusTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyMessageStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_message_status_type_id = ' . $this->sqlSubsidyMessageStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_message_id = ' . $this->sqlSubsidyMessageId() . ','; } elseif( true == array_key_exists( 'SubsidyMessageId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_message_id = ' . $this->sqlSubsidyMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sender = ' . $this->sqlSender() . ','; } elseif( true == array_key_exists( 'Sender', $this->getChangedColumns() ) ) { $strSql .= ' sender = ' . $this->sqlSender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipients = ' . $this->sqlRecipients() . ','; } elseif( true == array_key_exists( 'Recipients', $this->getChangedColumns() ) ) { $strSql .= ' recipients = ' . $this->sqlRecipients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_to_imax_date = ' . $this->sqlSentToImaxDate() . ','; } elseif( true == array_key_exists( 'SentToImaxDate', $this->getChangedColumns() ) ) { $strSql .= ' sent_to_imax_date = ' . $this->sqlSentToImaxDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_to_tracs_date = ' . $this->sqlSentToTracsDate() . ','; } elseif( true == array_key_exists( 'SentToTracsDate', $this->getChangedColumns() ) ) { $strSql .= ' sent_to_tracs_date = ' . $this->sqlSentToTracsDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_been_viewed = ' . $this->sqlHasBeenViewed() . ','; } elseif( true == array_key_exists( 'HasBeenViewed', $this->getChangedColumns() ) ) { $strSql .= ' has_been_viewed = ' . $this->sqlHasBeenViewed() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_request_id' => $this->getSubsidyRequestId(),
			'subsidy_message_type_id' => $this->getSubsidyMessageTypeId(),
			'subsidy_message_sub_type_id' => $this->getSubsidyMessageSubTypeId(),
			'subsidy_message_status_type_id' => $this->getSubsidyMessageStatusTypeId(),
			'subsidy_message_id' => $this->getSubsidyMessageId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'sender' => $this->getSender(),
			'recipients' => $this->getRecipients(),
			'subject' => $this->getSubject(),
			'message' => $this->getMessage(),
			'sent_to_imax_date' => $this->getSentToImaxDate(),
			'sent_to_tracs_date' => $this->getSentToTracsDate(),
			'is_archived' => $this->getIsArchived(),
			'has_been_viewed' => $this->getHasBeenViewed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>