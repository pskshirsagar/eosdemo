<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CConstraintConditions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseConstraintConditions extends CEosPluralBase {

	/**
	 * @return CConstraintCondition[]
	 */
	public static function fetchConstraintConditions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CConstraintCondition', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CConstraintCondition
	 */
	public static function fetchConstraintCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConstraintCondition', $objDatabase );
	}

	public static function fetchConstraintConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'constraint_conditions', $objDatabase );
	}

	public static function fetchConstraintConditionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchConstraintCondition( sprintf( 'SELECT * FROM constraint_conditions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchConstraintConditions( sprintf( 'SELECT * FROM constraint_conditions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintConditionsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchConstraintConditions( sprintf( 'SELECT * FROM constraint_conditions WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintConditionsByRevenueConstraintIdByCid( $intRevenueConstraintId, $intCid, $objDatabase ) {
		return self::fetchConstraintConditions( sprintf( 'SELECT * FROM constraint_conditions WHERE revenue_constraint_id = %d AND cid = %d', ( int ) $intRevenueConstraintId, ( int ) $intCid ), $objDatabase );
	}

}
?>