<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFloorplanUnitTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFloorplanUnitTypes extends CEosPluralBase {

	/**
	 * @return CFloorplanUnitType[]
	 */
	public static function fetchFloorplanUnitTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFloorplanUnitType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFloorplanUnitType
	 */
	public static function fetchFloorplanUnitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFloorplanUnitType', $objDatabase );
	}

	public static function fetchFloorplanUnitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'floorplan_unit_types', $objDatabase );
	}

	public static function fetchFloorplanUnitTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFloorplanUnitType( sprintf( 'SELECT * FROM floorplan_unit_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanUnitTypesByCid( $intCid, $objDatabase ) {
		return self::fetchFloorplanUnitTypes( sprintf( 'SELECT * FROM floorplan_unit_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanUnitTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFloorplanUnitTypes( sprintf( 'SELECT * FROM floorplan_unit_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanUnitTypesByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchFloorplanUnitTypes( sprintf( 'SELECT * FROM floorplan_unit_types WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanUnitTypesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchFloorplanUnitTypes( sprintf( 'SELECT * FROM floorplan_unit_types WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>