<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReturnTypeOptions
 * Do not add any new functions to this class.
 */

class CBaseDefaultReturnTypeOptions extends CEosPluralBase {

	/**
	 * @return CDefaultReturnTypeOption[]
	 */
	public static function fetchDefaultReturnTypeOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultReturnTypeOption', $objDatabase );
	}

	/**
	 * @return CDefaultReturnTypeOption
	 */
	public static function fetchDefaultReturnTypeOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultReturnTypeOption', $objDatabase );
	}

	public static function fetchDefaultReturnTypeOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_return_type_options', $objDatabase );
	}

	public static function fetchDefaultReturnTypeOptionById( $intId, $objDatabase ) {
		return self::fetchDefaultReturnTypeOption( sprintf( 'SELECT * FROM default_return_type_options WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultReturnTypeOptionsByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchDefaultReturnTypeOptions( sprintf( 'SELECT * FROM default_return_type_options WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

}
?>