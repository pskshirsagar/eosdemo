<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationQueueStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseIntegrationQueueStatusTypes extends CEosPluralBase {

	/**
	 * @return CIntegrationQueueStatusType[]
	 */
	public static function fetchIntegrationQueueStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationQueueStatusType::class, $objDatabase );
	}

	/**
	 * @return CIntegrationQueueStatusType
	 */
	public static function fetchIntegrationQueueStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationQueueStatusType::class, $objDatabase );
	}

	public static function fetchIntegrationQueueStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_queue_status_types', $objDatabase );
	}

	public static function fetchIntegrationQueueStatusTypeById( $intId, $objDatabase ) {
		return self::fetchIntegrationQueueStatusType( sprintf( 'SELECT * FROM integration_queue_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>