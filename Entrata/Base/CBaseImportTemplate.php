<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportTemplate extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.import_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSourceCid;
	protected $m_intPropertyId;
	protected $m_strSourcePropertyId;
	protected $m_intImportTypeId;
	protected $m_intImportStatusTypeId;
	protected $m_intCompetitorId;
	protected $m_strName;
	protected $m_strToken;
	protected $m_strPassword;
	protected $m_boolHasResidents;
	protected $m_intIsSimpleCsvUpload;
	protected $m_arrstrLeaseStatusTypes;
	protected $m_boolAmountDue;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsActive;
	protected $m_intInitiatedBy;
	protected $m_strInitiatedOn;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImportDataTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasResidents = false;
		$this->m_intIsSimpleCsvUpload = '0';
		$this->m_boolAmountDue = false;
		$this->m_boolIsActive = true;
		$this->m_intImportDataTypeId = '2';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['source_cid'] ) && $boolDirectSet ) $this->set( 'm_intSourceCid', trim( $arrValues['source_cid'] ) ); elseif( isset( $arrValues['source_cid'] ) ) $this->setSourceCid( $arrValues['source_cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['source_property_id'] ) && $boolDirectSet ) $this->set( 'm_strSourcePropertyId', trim( stripcslashes( $arrValues['source_property_id'] ) ) ); elseif( isset( $arrValues['source_property_id'] ) ) $this->setSourcePropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source_property_id'] ) : $arrValues['source_property_id'] );
		if( isset( $arrValues['import_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTypeId', trim( $arrValues['import_type_id'] ) ); elseif( isset( $arrValues['import_type_id'] ) ) $this->setImportTypeId( $arrValues['import_type_id'] );
		if( isset( $arrValues['import_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportStatusTypeId', trim( $arrValues['import_status_type_id'] ) ); elseif( isset( $arrValues['import_status_type_id'] ) ) $this->setImportStatusTypeId( $arrValues['import_status_type_id'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['token'] ) && $boolDirectSet ) $this->set( 'm_strToken', trim( stripcslashes( $arrValues['token'] ) ) ); elseif( isset( $arrValues['token'] ) ) $this->setToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['token'] ) : $arrValues['token'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['has_residents'] ) && $boolDirectSet ) $this->set( 'm_boolHasResidents', trim( stripcslashes( $arrValues['has_residents'] ) ) ); elseif( isset( $arrValues['has_residents'] ) ) $this->setHasResidents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_residents'] ) : $arrValues['has_residents'] );
		if( isset( $arrValues['is_simple_csv_upload'] ) && $boolDirectSet ) $this->set( 'm_intIsSimpleCsvUpload', trim( $arrValues['is_simple_csv_upload'] ) ); elseif( isset( $arrValues['is_simple_csv_upload'] ) ) $this->setIsSimpleCsvUpload( $arrValues['is_simple_csv_upload'] );
		if( isset( $arrValues['lease_status_types'] ) && $boolDirectSet ) $this->set( 'm_arrstrLeaseStatusTypes', trim( $arrValues['lease_status_types'] ) ); elseif( isset( $arrValues['lease_status_types'] ) ) $this->setLeaseStatusTypes( $arrValues['lease_status_types'] );
		if( isset( $arrValues['amount_due'] ) && $boolDirectSet ) $this->set( 'm_boolAmountDue', trim( stripcslashes( $arrValues['amount_due'] ) ) ); elseif( isset( $arrValues['amount_due'] ) ) $this->setAmountDue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amount_due'] ) : $arrValues['amount_due'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['initiated_by'] ) && $boolDirectSet ) $this->set( 'm_intInitiatedBy', trim( $arrValues['initiated_by'] ) ); elseif( isset( $arrValues['initiated_by'] ) ) $this->setInitiatedBy( $arrValues['initiated_by'] );
		if( isset( $arrValues['initiated_on'] ) && $boolDirectSet ) $this->set( 'm_strInitiatedOn', trim( $arrValues['initiated_on'] ) ); elseif( isset( $arrValues['initiated_on'] ) ) $this->setInitiatedOn( $arrValues['initiated_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportDataTypeId', trim( $arrValues['import_data_type_id'] ) ); elseif( isset( $arrValues['import_data_type_id'] ) ) $this->setImportDataTypeId( $arrValues['import_data_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSourceCid( $intSourceCid ) {
		$this->set( 'm_intSourceCid', CStrings::strToIntDef( $intSourceCid, NULL, false ) );
	}

	public function getSourceCid() {
		return $this->m_intSourceCid;
	}

	public function sqlSourceCid() {
		return ( true == isset( $this->m_intSourceCid ) ) ? ( string ) $this->m_intSourceCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSourcePropertyId( $strSourcePropertyId ) {
		$this->set( 'm_strSourcePropertyId', CStrings::strTrimDef( $strSourcePropertyId, 200, NULL, true ) );
	}

	public function getSourcePropertyId() {
		return $this->m_strSourcePropertyId;
	}

	public function sqlSourcePropertyId() {
		return ( true == isset( $this->m_strSourcePropertyId ) ) ? '\'' . addslashes( $this->m_strSourcePropertyId ) . '\'' : 'NULL';
	}

	public function setImportTypeId( $intImportTypeId ) {
		$this->set( 'm_intImportTypeId', CStrings::strToIntDef( $intImportTypeId, NULL, false ) );
	}

	public function getImportTypeId() {
		return $this->m_intImportTypeId;
	}

	public function sqlImportTypeId() {
		return ( true == isset( $this->m_intImportTypeId ) ) ? ( string ) $this->m_intImportTypeId : 'NULL';
	}

	public function setImportStatusTypeId( $intImportStatusTypeId ) {
		$this->set( 'm_intImportStatusTypeId', CStrings::strToIntDef( $intImportStatusTypeId, NULL, false ) );
	}

	public function getImportStatusTypeId() {
		return $this->m_intImportStatusTypeId;
	}

	public function sqlImportStatusTypeId() {
		return ( true == isset( $this->m_intImportStatusTypeId ) ) ? ( string ) $this->m_intImportStatusTypeId : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setToken( $strToken ) {
		$this->set( 'm_strToken', CStrings::strTrimDef( $strToken, 244, NULL, true ) );
	}

	public function getToken() {
		return $this->m_strToken;
	}

	public function sqlToken() {
		return ( true == isset( $this->m_strToken ) ) ? '\'' . addslashes( $this->m_strToken ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 244, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setHasResidents( $boolHasResidents ) {
		$this->set( 'm_boolHasResidents', CStrings::strToBool( $boolHasResidents ) );
	}

	public function getHasResidents() {
		return $this->m_boolHasResidents;
	}

	public function sqlHasResidents() {
		return ( true == isset( $this->m_boolHasResidents ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasResidents ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSimpleCsvUpload( $intIsSimpleCsvUpload ) {
		$this->set( 'm_intIsSimpleCsvUpload', CStrings::strToIntDef( $intIsSimpleCsvUpload, NULL, false ) );
	}

	public function getIsSimpleCsvUpload() {
		return $this->m_intIsSimpleCsvUpload;
	}

	public function sqlIsSimpleCsvUpload() {
		return ( true == isset( $this->m_intIsSimpleCsvUpload ) ) ? ( string ) $this->m_intIsSimpleCsvUpload : '0';
	}

	public function setLeaseStatusTypes( $arrstrLeaseStatusTypes ) {
		$this->set( 'm_arrstrLeaseStatusTypes', CStrings::strToArrIntDef( $arrstrLeaseStatusTypes, NULL ) );
	}

	public function getLeaseStatusTypes() {
		return $this->m_arrstrLeaseStatusTypes;
	}

	public function sqlLeaseStatusTypes() {
		return ( true == isset( $this->m_arrstrLeaseStatusTypes ) && true == valArr( $this->m_arrstrLeaseStatusTypes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrLeaseStatusTypes, NULL ) . '\'' : 'NULL';
	}

	public function setAmountDue( $boolAmountDue ) {
		$this->set( 'm_boolAmountDue', CStrings::strToBool( $boolAmountDue ) );
	}

	public function getAmountDue() {
		return $this->m_boolAmountDue;
	}

	public function sqlAmountDue() {
		return ( true == isset( $this->m_boolAmountDue ) ) ? '\'' . ( true == ( bool ) $this->m_boolAmountDue ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInitiatedBy( $intInitiatedBy ) {
		$this->set( 'm_intInitiatedBy', CStrings::strToIntDef( $intInitiatedBy, NULL, false ) );
	}

	public function getInitiatedBy() {
		return $this->m_intInitiatedBy;
	}

	public function sqlInitiatedBy() {
		return ( true == isset( $this->m_intInitiatedBy ) ) ? ( string ) $this->m_intInitiatedBy : 'NULL';
	}

	public function setInitiatedOn( $strInitiatedOn ) {
		$this->set( 'm_strInitiatedOn', CStrings::strTrimDef( $strInitiatedOn, -1, NULL, true ) );
	}

	public function getInitiatedOn() {
		return $this->m_strInitiatedOn;
	}

	public function sqlInitiatedOn() {
		return ( true == isset( $this->m_strInitiatedOn ) ) ? '\'' . $this->m_strInitiatedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImportDataTypeId( $intImportDataTypeId ) {
		$this->set( 'm_intImportDataTypeId', CStrings::strToIntDef( $intImportDataTypeId, NULL, false ) );
	}

	public function getImportDataTypeId() {
		return $this->m_intImportDataTypeId;
	}

	public function sqlImportDataTypeId() {
		return ( true == isset( $this->m_intImportDataTypeId ) ) ? ( string ) $this->m_intImportDataTypeId : '2';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, source_cid, property_id, source_property_id, import_type_id, import_status_type_id, competitor_id, name, token, password, has_residents, is_simple_csv_upload, lease_status_types, amount_due, details, is_active, initiated_by, initiated_on, completed_on, updated_by, updated_on, created_by, created_on, import_data_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlSourceCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSourcePropertyId() . ', ' .
						$this->sqlImportTypeId() . ', ' .
						$this->sqlImportStatusTypeId() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlToken() . ', ' .
						$this->sqlPassword() . ', ' .
						$this->sqlHasResidents() . ', ' .
						$this->sqlIsSimpleCsvUpload() . ', ' .
						$this->sqlLeaseStatusTypes() . ', ' .
						$this->sqlAmountDue() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlInitiatedBy() . ', ' .
						$this->sqlInitiatedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlImportDataTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid(). ',' ; } elseif( true == array_key_exists( 'SourceCid', $this->getChangedColumns() ) ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId(). ',' ; } elseif( true == array_key_exists( 'SourcePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token = ' . $this->sqlToken(). ',' ; } elseif( true == array_key_exists( 'Token', $this->getChangedColumns() ) ) { $strSql .= ' token = ' . $this->sqlToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword(). ',' ; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_residents = ' . $this->sqlHasResidents(). ',' ; } elseif( true == array_key_exists( 'HasResidents', $this->getChangedColumns() ) ) { $strSql .= ' has_residents = ' . $this->sqlHasResidents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_simple_csv_upload = ' . $this->sqlIsSimpleCsvUpload(). ',' ; } elseif( true == array_key_exists( 'IsSimpleCsvUpload', $this->getChangedColumns() ) ) { $strSql .= ' is_simple_csv_upload = ' . $this->sqlIsSimpleCsvUpload() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_types = ' . $this->sqlLeaseStatusTypes(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_types = ' . $this->sqlLeaseStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_due = ' . $this->sqlAmountDue(). ',' ; } elseif( true == array_key_exists( 'AmountDue', $this->getChangedColumns() ) ) { $strSql .= ' amount_due = ' . $this->sqlAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy(). ',' ; } elseif( true == array_key_exists( 'InitiatedBy', $this->getChangedColumns() ) ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn(). ',' ; } elseif( true == array_key_exists( 'InitiatedOn', $this->getChangedColumns() ) ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'source_cid' => $this->getSourceCid(),
			'property_id' => $this->getPropertyId(),
			'source_property_id' => $this->getSourcePropertyId(),
			'import_type_id' => $this->getImportTypeId(),
			'import_status_type_id' => $this->getImportStatusTypeId(),
			'competitor_id' => $this->getCompetitorId(),
			'name' => $this->getName(),
			'token' => $this->getToken(),
			'password' => $this->getPassword(),
			'has_residents' => $this->getHasResidents(),
			'is_simple_csv_upload' => $this->getIsSimpleCsvUpload(),
			'lease_status_types' => $this->getLeaseStatusTypes(),
			'amount_due' => $this->getAmountDue(),
			'details' => $this->getDetails(),
			'is_active' => $this->getIsActive(),
			'initiated_by' => $this->getInitiatedBy(),
			'initiated_on' => $this->getInitiatedOn(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'import_data_type_id' => $this->getImportDataTypeId()
		);
	}

}
?>