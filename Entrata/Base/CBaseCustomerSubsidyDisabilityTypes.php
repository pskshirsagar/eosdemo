<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDisabilityTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidyDisabilityTypes extends CEosPluralBase {

	/**
	 * @return CCustomerSubsidyDisabilityType[]
	 */
	public static function fetchCustomerSubsidyDisabilityTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerSubsidyDisabilityType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSubsidyDisabilityType
	 */
	public static function fetchCustomerSubsidyDisabilityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerSubsidyDisabilityType', $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_subsidy_disability_types', $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDisabilityType( sprintf( 'SELECT * FROM customer_subsidy_disability_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDisabilityTypes( sprintf( 'SELECT * FROM customer_subsidy_disability_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityTypesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDisabilityTypes( sprintf( 'SELECT * FROM customer_subsidy_disability_types WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDisabilityTypesBySubsidyDisabilityTypeIdByCid( $intSubsidyDisabilityTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDisabilityTypes( sprintf( 'SELECT * FROM customer_subsidy_disability_types WHERE subsidy_disability_type_id = %d AND cid = %d', ( int ) $intSubsidyDisabilityTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>