<?php

class CBaseCustomerPlaidItem extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_plaid_items';

	protected $m_intId;
	protected $m_intCustomerId;
	protected $m_strItemIdEncrypted;
	protected $m_strInstitutionId;
	protected $m_strName;
	protected $m_strWebhook;
	protected $m_strConsentExpirationTime;
	protected $m_strError;
	protected $m_jsonError;
	protected $m_strLastWebhookSentAt;
	protected $m_strLastWebhookCodeSent;
	protected $m_strPublicToken;
	protected $m_strAccessTokenEncrypted;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strItemIdBindex;
	protected $m_intCid;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['item_id_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strItemIdEncrypted', trim( $arrValues['item_id_encrypted'] ) ); elseif( isset( $arrValues['item_id_encrypted'] ) ) $this->setItemIdEncrypted( $arrValues['item_id_encrypted'] );
		if( isset( $arrValues['institution_id'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionId', trim( $arrValues['institution_id'] ) ); elseif( isset( $arrValues['institution_id'] ) ) $this->setInstitutionId( $arrValues['institution_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['webhook'] ) && $boolDirectSet ) $this->set( 'm_strWebhook', trim( $arrValues['webhook'] ) ); elseif( isset( $arrValues['webhook'] ) ) $this->setWebhook( $arrValues['webhook'] );
		if( isset( $arrValues['consent_expiration_time'] ) && $boolDirectSet ) $this->set( 'm_strConsentExpirationTime', trim( $arrValues['consent_expiration_time'] ) ); elseif( isset( $arrValues['consent_expiration_time'] ) ) $this->setConsentExpirationTime( $arrValues['consent_expiration_time'] );
		if( isset( $arrValues['error'] ) ) $this->set( 'm_strError', trim( $arrValues['error'] ) );
		if( isset( $arrValues['last_webhook_sent_at'] ) && $boolDirectSet ) $this->set( 'm_strLastWebhookSentAt', trim( $arrValues['last_webhook_sent_at'] ) ); elseif( isset( $arrValues['last_webhook_sent_at'] ) ) $this->setLastWebhookSentAt( $arrValues['last_webhook_sent_at'] );
		if( isset( $arrValues['last_webhook_code_sent'] ) && $boolDirectSet ) $this->set( 'm_strLastWebhookCodeSent', trim( $arrValues['last_webhook_code_sent'] ) ); elseif( isset( $arrValues['last_webhook_code_sent'] ) ) $this->setLastWebhookCodeSent( $arrValues['last_webhook_code_sent'] );
		if( isset( $arrValues['public_token'] ) && $boolDirectSet ) $this->set( 'm_strPublicToken', trim( $arrValues['public_token'] ) ); elseif( isset( $arrValues['public_token'] ) ) $this->setPublicToken( $arrValues['public_token'] );
		if( isset( $arrValues['access_token_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAccessTokenEncrypted', trim( $arrValues['access_token_encrypted'] ) ); elseif( isset( $arrValues['access_token_encrypted'] ) ) $this->setAccessTokenEncrypted( $arrValues['access_token_encrypted'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['item_id_bindex'] ) && $boolDirectSet ) $this->set( 'm_strItemIdBindex', trim( $arrValues['item_id_bindex'] ) ); elseif( isset( $arrValues['item_id_bindex'] ) ) $this->setItemIdBindex( $arrValues['item_id_bindex'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setItemIdEncrypted( $strItemIdEncrypted ) {
		$this->set( 'm_strItemIdEncrypted', CStrings::strTrimDef( $strItemIdEncrypted, 1000, NULL, true ) );
	}

	public function getItemIdEncrypted() {
		return $this->m_strItemIdEncrypted;
	}

	public function sqlItemIdEncrypted() {
		return ( true == isset( $this->m_strItemIdEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strItemIdEncrypted ) : '\'' . addslashes( $this->m_strItemIdEncrypted ) . '\'' ) : 'NULL';
	}

	public function setInstitutionId( $strInstitutionId ) {
		$this->set( 'm_strInstitutionId', CStrings::strTrimDef( $strInstitutionId, 240, NULL, true ) );
	}

	public function getInstitutionId() {
		return $this->m_strInstitutionId;
	}

	public function sqlInstitutionId() {
		return ( true == isset( $this->m_strInstitutionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstitutionId ) : '\'' . addslashes( $this->m_strInstitutionId ) . '\'' ) : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setWebhook( $strWebhook ) {
		$this->set( 'm_strWebhook', CStrings::strTrimDef( $strWebhook, 240, NULL, true ) );
	}

	public function getWebhook() {
		return $this->m_strWebhook;
	}

	public function sqlWebhook() {
		return ( true == isset( $this->m_strWebhook ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebhook ) : '\'' . addslashes( $this->m_strWebhook ) . '\'' ) : 'NULL';
	}

	public function setConsentExpirationTime( $strConsentExpirationTime ) {
		$this->set( 'm_strConsentExpirationTime', CStrings::strTrimDef( $strConsentExpirationTime, -1, NULL, true ) );
	}

	public function getConsentExpirationTime() {
		return $this->m_strConsentExpirationTime;
	}

	public function sqlConsentExpirationTime() {
		return ( true == isset( $this->m_strConsentExpirationTime ) ) ? '\'' . $this->m_strConsentExpirationTime . '\'' : 'NULL';
	}

	public function setError( $jsonError ) {
		if( true == valObj( $jsonError, 'stdClass' ) ) {
			$this->set( 'm_jsonError', $jsonError );
		} elseif( true == valJsonString( $jsonError ) ) {
			$this->set( 'm_jsonError', CStrings::strToJson( $jsonError ) );
		} else {
			$this->set( 'm_jsonError', NULL );
		}
		unset( $this->m_strError );
	}

	public function getError() {
		if( true == isset( $this->m_strError ) ) {
			$this->m_jsonError = CStrings::strToJson( $this->m_strError );
			unset( $this->m_strError );
		}
		return $this->m_jsonError;
	}

	public function sqlError() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getError() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getError() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getError() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setLastWebhookSentAt( $strLastWebhookSentAt ) {
		$this->set( 'm_strLastWebhookSentAt', CStrings::strTrimDef( $strLastWebhookSentAt, -1, NULL, true ) );
	}

	public function getLastWebhookSentAt() {
		return $this->m_strLastWebhookSentAt;
	}

	public function sqlLastWebhookSentAt() {
		return ( true == isset( $this->m_strLastWebhookSentAt ) ) ? '\'' . $this->m_strLastWebhookSentAt . '\'' : 'NULL';
	}

	public function setLastWebhookCodeSent( $strLastWebhookCodeSent ) {
		$this->set( 'm_strLastWebhookCodeSent', CStrings::strTrimDef( $strLastWebhookCodeSent, 240, NULL, true ) );
	}

	public function getLastWebhookCodeSent() {
		return $this->m_strLastWebhookCodeSent;
	}

	public function sqlLastWebhookCodeSent() {
		return ( true == isset( $this->m_strLastWebhookCodeSent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLastWebhookCodeSent ) : '\'' . addslashes( $this->m_strLastWebhookCodeSent ) . '\'' ) : 'NULL';
	}

	public function setPublicToken( $strPublicToken ) {
		$this->set( 'm_strPublicToken', CStrings::strTrimDef( $strPublicToken, 240, NULL, true ) );
	}

	public function getPublicToken() {
		return $this->m_strPublicToken;
	}

	public function sqlPublicToken() {
		return ( true == isset( $this->m_strPublicToken ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPublicToken ) : '\'' . addslashes( $this->m_strPublicToken ) . '\'' ) : 'NULL';
	}

	public function setAccessTokenEncrypted( $strAccessTokenEncrypted ) {
		$this->set( 'm_strAccessTokenEncrypted', CStrings::strTrimDef( $strAccessTokenEncrypted, 1000, NULL, true ) );
	}

	public function getAccessTokenEncrypted() {
		return $this->m_strAccessTokenEncrypted;
	}

	public function sqlAccessTokenEncrypted() {
		return ( true == isset( $this->m_strAccessTokenEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccessTokenEncrypted ) : '\'' . addslashes( $this->m_strAccessTokenEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setItemIdBindex( $strItemIdBindex ) {
		$this->set( 'm_strItemIdBindex', CStrings::strTrimDef( $strItemIdBindex, 240, NULL, true ) );
	}

	public function getItemIdBindex() {
		return $this->m_strItemIdBindex;
	}

	public function sqlItemIdBindex() {
		return ( true == isset( $this->m_strItemIdBindex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strItemIdBindex ) : '\'' . addslashes( $this->m_strItemIdBindex ) . '\'' ) : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, customer_id, item_id_encrypted, institution_id, name, webhook, consent_expiration_time, error, last_webhook_sent_at, last_webhook_code_sent, public_token, access_token_encrypted, deleted_on, deleted_by, updated_on, updated_by, created_on, created_by, item_id_bindex, cid )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlItemIdEncrypted() . ', ' .
						$this->sqlInstitutionId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlWebhook() . ', ' .
						$this->sqlConsentExpirationTime() . ', ' .
						$this->sqlError() . ', ' .
						$this->sqlLastWebhookSentAt() . ', ' .
						$this->sqlLastWebhookCodeSent() . ', ' .
						$this->sqlPublicToken() . ', ' .
						$this->sqlAccessTokenEncrypted() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlItemIdBindex() . ', ' .
						$this->sqlCid() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_id_encrypted = ' . $this->sqlItemIdEncrypted(). ',' ; } elseif( true == array_key_exists( 'ItemIdEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' item_id_encrypted = ' . $this->sqlItemIdEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_id = ' . $this->sqlInstitutionId(). ',' ; } elseif( true == array_key_exists( 'InstitutionId', $this->getChangedColumns() ) ) { $strSql .= ' institution_id = ' . $this->sqlInstitutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' webhook = ' . $this->sqlWebhook(). ',' ; } elseif( true == array_key_exists( 'Webhook', $this->getChangedColumns() ) ) { $strSql .= ' webhook = ' . $this->sqlWebhook() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consent_expiration_time = ' . $this->sqlConsentExpirationTime(). ',' ; } elseif( true == array_key_exists( 'ConsentExpirationTime', $this->getChangedColumns() ) ) { $strSql .= ' consent_expiration_time = ' . $this->sqlConsentExpirationTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error = ' . $this->sqlError(). ',' ; } elseif( true == array_key_exists( 'Error', $this->getChangedColumns() ) ) { $strSql .= ' error = ' . $this->sqlError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_webhook_sent_at = ' . $this->sqlLastWebhookSentAt(). ',' ; } elseif( true == array_key_exists( 'LastWebhookSentAt', $this->getChangedColumns() ) ) { $strSql .= ' last_webhook_sent_at = ' . $this->sqlLastWebhookSentAt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_webhook_code_sent = ' . $this->sqlLastWebhookCodeSent(). ',' ; } elseif( true == array_key_exists( 'LastWebhookCodeSent', $this->getChangedColumns() ) ) { $strSql .= ' last_webhook_code_sent = ' . $this->sqlLastWebhookCodeSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' public_token = ' . $this->sqlPublicToken(). ',' ; } elseif( true == array_key_exists( 'PublicToken', $this->getChangedColumns() ) ) { $strSql .= ' public_token = ' . $this->sqlPublicToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' access_token_encrypted = ' . $this->sqlAccessTokenEncrypted(). ',' ; } elseif( true == array_key_exists( 'AccessTokenEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' access_token_encrypted = ' . $this->sqlAccessTokenEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_id_bindex = ' . $this->sqlItemIdBindex(). ',' ; } elseif( true == array_key_exists( 'ItemIdBindex', $this->getChangedColumns() ) ) { $strSql .= ' item_id_bindex = ' . $this->sqlItemIdBindex() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'customer_id' => $this->getCustomerId(),
			'item_id_encrypted' => $this->getItemIdEncrypted(),
			'institution_id' => $this->getInstitutionId(),
			'name' => $this->getName(),
			'webhook' => $this->getWebhook(),
			'consent_expiration_time' => $this->getConsentExpirationTime(),
			'error' => $this->getError(),
			'last_webhook_sent_at' => $this->getLastWebhookSentAt(),
			'last_webhook_code_sent' => $this->getLastWebhookCodeSent(),
			'public_token' => $this->getPublicToken(),
			'access_token_encrypted' => $this->getAccessTokenEncrypted(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'item_id_bindex' => $this->getItemIdBindex(),
			'cid' => $this->getCid()
		);
	}

}
?>
