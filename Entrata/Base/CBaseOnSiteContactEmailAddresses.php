<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COnSiteContactEmailAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOnSiteContactEmailAddresses extends CEosPluralBase {

	/**
	 * @return COnSiteContactEmailAddress[]
	 */
	public static function fetchOnSiteContactEmailAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COnSiteContactEmailAddress', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COnSiteContactEmailAddress
	 */
	public static function fetchOnSiteContactEmailAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COnSiteContactEmailAddress', $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'on_site_contact_email_addresses', $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactEmailAddress( sprintf( 'SELECT * FROM on_site_contact_email_addresses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchOnSiteContactEmailAddresses( sprintf( 'SELECT * FROM on_site_contact_email_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactEmailAddresses( sprintf( 'SELECT * FROM on_site_contact_email_addresses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressesByPropertyOnSiteContactIdByCid( $intPropertyOnSiteContactId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactEmailAddresses( sprintf( 'SELECT * FROM on_site_contact_email_addresses WHERE property_on_site_contact_id = %d AND cid = %d', ( int ) $intPropertyOnSiteContactId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactEmailAddressesByEmailAddressTypeIdByCid( $intEmailAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactEmailAddresses( sprintf( 'SELECT * FROM on_site_contact_email_addresses WHERE email_address_type_id = %d AND cid = %d', ( int ) $intEmailAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>