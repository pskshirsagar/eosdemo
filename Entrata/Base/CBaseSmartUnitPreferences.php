<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartUnitPreferences
 * Do not add any new functions to this class.
 */

class CBaseSmartUnitPreferences extends CEosPluralBase {

	/**
	 * @return CSmartUnitPreference[]
	 */
	public static function fetchSmartUnitPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSmartUnitPreference::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSmartUnitPreference
	 */
	public static function fetchSmartUnitPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSmartUnitPreference::class, $objDatabase );
	}

	public static function fetchSmartUnitPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'smart_unit_preferences', $objDatabase );
	}

	public static function fetchSmartUnitPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSmartUnitPreference( sprintf( 'SELECT * FROM smart_unit_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSmartUnitPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchSmartUnitPreferences( sprintf( 'SELECT * FROM smart_unit_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSmartUnitPreferencesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchSmartUnitPreferences( sprintf( 'SELECT * FROM smart_unit_preferences WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

}
?>