<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEventAssociations extends CEosPluralBase {

	/**
	 * @return CCalendarEventAssociation[]
	 */
	public static function fetchCalendarEventAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCalendarEventAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCalendarEventAssociation
	 */
	public static function fetchCalendarEventAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCalendarEventAssociation', $objDatabase );
	}

	public static function fetchCalendarEventAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calendar_event_associations', $objDatabase );
	}

	public static function fetchCalendarEventAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventAssociation( sprintf( 'SELECT * FROM calendar_event_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchCalendarEventAssociations( sprintf( 'SELECT * FROM calendar_event_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventAssociationsByCalendarEventIdByCid( $intCalendarEventId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventAssociations( sprintf( 'SELECT * FROM calendar_event_associations WHERE calendar_event_id = %d AND cid = %d', ( int ) $intCalendarEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventAssociations( sprintf( 'SELECT * FROM calendar_event_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventAssociationsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventAssociations( sprintf( 'SELECT * FROM calendar_event_associations WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>