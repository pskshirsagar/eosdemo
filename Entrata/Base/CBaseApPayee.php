<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayee extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ap_payees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intApPayeeTermId;
	protected $m_intDefaultApRemittanceId;
	protected $m_intGlAccountId;
	protected $m_intVendorId;
	protected $m_intApPayeeTypeId;
	protected $m_intApPayeeStatusTypeId;
	protected $m_intCategorizationTypeId;
	protected $m_intConfidentialityAllowanceTypeId;
	protected $m_intCamAllowanceTypeId;
	protected $m_intVendorCategoryTypeId;
	protected $m_intComplianceRulsetId;
	protected $m_intReferenceId;
	protected $m_intCustomerId;
	protected $m_strSecondaryNumber;
	protected $m_strRemotePrimaryKey;
	protected $m_strSystemCode;
	protected $m_strCompanyName;
	protected $m_strUsername;
	protected $m_strWebsiteUrl;
	protected $m_strNotes;
	protected $m_strSalesTaxDescription;
	protected $m_strShippingDescription;
	protected $m_strDiscountDescription;
	protected $m_boolRequirePoForInvoice;
	protected $m_boolIsCategorizationQueued;
	protected $m_boolIsConsolidated;
	protected $m_boolIsSystem;
	protected $m_boolIsOnSite;
	protected $m_boolIsUtilities;
	protected $m_boolIsCaptureInvoiceTotalOnly;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCompetitorId;
	protected $m_intPsProductId;

	public function __construct() {
		parent::__construct();

		$this->m_intApPayeeStatusTypeId = '1';
		$this->m_intCategorizationTypeId = '1';
		$this->m_intConfidentialityAllowanceTypeId = '1';
		$this->m_intCamAllowanceTypeId = '1';
		$this->m_strSalesTaxDescription = 'Sales Tax; Tax;';
		$this->m_strShippingDescription = 'Shipping';
		$this->m_strDiscountDescription = 'Discount';
		$this->m_boolRequirePoForInvoice = false;
		$this->m_boolIsCategorizationQueued = false;
		$this->m_boolIsConsolidated = false;
		$this->m_boolIsSystem = false;
		$this->m_boolIsOnSite = false;
		$this->m_boolIsUtilities = false;
		$this->m_boolIsCaptureInvoiceTotalOnly = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['ap_payee_term_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeTermId', trim( $arrValues['ap_payee_term_id'] ) ); elseif( isset( $arrValues['ap_payee_term_id'] ) ) $this->setApPayeeTermId( $arrValues['ap_payee_term_id'] );
		if( isset( $arrValues['default_ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultApRemittanceId', trim( $arrValues['default_ap_remittance_id'] ) ); elseif( isset( $arrValues['default_ap_remittance_id'] ) ) $this->setDefaultApRemittanceId( $arrValues['default_ap_remittance_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['ap_payee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeTypeId', trim( $arrValues['ap_payee_type_id'] ) ); elseif( isset( $arrValues['ap_payee_type_id'] ) ) $this->setApPayeeTypeId( $arrValues['ap_payee_type_id'] );
		if( isset( $arrValues['ap_payee_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeStatusTypeId', trim( $arrValues['ap_payee_status_type_id'] ) ); elseif( isset( $arrValues['ap_payee_status_type_id'] ) ) $this->setApPayeeStatusTypeId( $arrValues['ap_payee_status_type_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['confidentiality_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConfidentialityAllowanceTypeId', trim( $arrValues['confidentiality_allowance_type_id'] ) ); elseif( isset( $arrValues['confidentiality_allowance_type_id'] ) ) $this->setConfidentialityAllowanceTypeId( $arrValues['confidentiality_allowance_type_id'] );
		if( isset( $arrValues['cam_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCamAllowanceTypeId', trim( $arrValues['cam_allowance_type_id'] ) ); elseif( isset( $arrValues['cam_allowance_type_id'] ) ) $this->setCamAllowanceTypeId( $arrValues['cam_allowance_type_id'] );
		if( isset( $arrValues['vendor_category_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorCategoryTypeId', trim( $arrValues['vendor_category_type_id'] ) ); elseif( isset( $arrValues['vendor_category_type_id'] ) ) $this->setVendorCategoryTypeId( $arrValues['vendor_category_type_id'] );
		if( isset( $arrValues['compliance_rulset_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceRulsetId', trim( $arrValues['compliance_rulset_id'] ) ); elseif( isset( $arrValues['compliance_rulset_id'] ) ) $this->setComplianceRulsetId( $arrValues['compliance_rulset_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( $arrValues['secondary_number'] ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrValues['secondary_number'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( $arrValues['system_code'] ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( $arrValues['website_url'] ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( $arrValues['website_url'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['sales_tax_description'] ) && $boolDirectSet ) $this->set( 'm_strSalesTaxDescription', trim( $arrValues['sales_tax_description'] ) ); elseif( isset( $arrValues['sales_tax_description'] ) ) $this->setSalesTaxDescription( $arrValues['sales_tax_description'] );
		if( isset( $arrValues['shipping_description'] ) && $boolDirectSet ) $this->set( 'm_strShippingDescription', trim( $arrValues['shipping_description'] ) ); elseif( isset( $arrValues['shipping_description'] ) ) $this->setShippingDescription( $arrValues['shipping_description'] );
		if( isset( $arrValues['discount_description'] ) && $boolDirectSet ) $this->set( 'm_strDiscountDescription', trim( $arrValues['discount_description'] ) ); elseif( isset( $arrValues['discount_description'] ) ) $this->setDiscountDescription( $arrValues['discount_description'] );
		if( isset( $arrValues['require_po_for_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePoForInvoice', trim( stripcslashes( $arrValues['require_po_for_invoice'] ) ) ); elseif( isset( $arrValues['require_po_for_invoice'] ) ) $this->setRequirePoForInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_po_for_invoice'] ) : $arrValues['require_po_for_invoice'] );
		if( isset( $arrValues['is_categorization_queued'] ) && $boolDirectSet ) $this->set( 'm_boolIsCategorizationQueued', trim( stripcslashes( $arrValues['is_categorization_queued'] ) ) ); elseif( isset( $arrValues['is_categorization_queued'] ) ) $this->setIsCategorizationQueued( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_categorization_queued'] ) : $arrValues['is_categorization_queued'] );
		if( isset( $arrValues['is_consolidated'] ) && $boolDirectSet ) $this->set( 'm_boolIsConsolidated', trim( stripcslashes( $arrValues['is_consolidated'] ) ) ); elseif( isset( $arrValues['is_consolidated'] ) ) $this->setIsConsolidated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_consolidated'] ) : $arrValues['is_consolidated'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['is_on_site'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnSite', trim( stripcslashes( $arrValues['is_on_site'] ) ) ); elseif( isset( $arrValues['is_on_site'] ) ) $this->setIsOnSite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_site'] ) : $arrValues['is_on_site'] );
		if( isset( $arrValues['is_utilities'] ) && $boolDirectSet ) $this->set( 'm_boolIsUtilities', trim( stripcslashes( $arrValues['is_utilities'] ) ) ); elseif( isset( $arrValues['is_utilities'] ) ) $this->setIsUtilities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_utilities'] ) : $arrValues['is_utilities'] );
		if( isset( $arrValues['is_capture_invoice_total_only'] ) && $boolDirectSet ) $this->set( 'm_boolIsCaptureInvoiceTotalOnly', trim( stripcslashes( $arrValues['is_capture_invoice_total_only'] ) ) ); elseif( isset( $arrValues['is_capture_invoice_total_only'] ) ) $this->setIsCaptureInvoiceTotalOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_capture_invoice_total_only'] ) : $arrValues['is_capture_invoice_total_only'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setApPayeeTermId( $intApPayeeTermId ) {
		$this->set( 'm_intApPayeeTermId', CStrings::strToIntDef( $intApPayeeTermId, NULL, false ) );
	}

	public function getApPayeeTermId() {
		return $this->m_intApPayeeTermId;
	}

	public function sqlApPayeeTermId() {
		return ( true == isset( $this->m_intApPayeeTermId ) ) ? ( string ) $this->m_intApPayeeTermId : 'NULL';
	}

	public function setDefaultApRemittanceId( $intDefaultApRemittanceId ) {
		$this->set( 'm_intDefaultApRemittanceId', CStrings::strToIntDef( $intDefaultApRemittanceId, NULL, false ) );
	}

	public function getDefaultApRemittanceId() {
		return $this->m_intDefaultApRemittanceId;
	}

	public function sqlDefaultApRemittanceId() {
		return ( true == isset( $this->m_intDefaultApRemittanceId ) ) ? ( string ) $this->m_intDefaultApRemittanceId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setApPayeeTypeId( $intApPayeeTypeId ) {
		$this->set( 'm_intApPayeeTypeId', CStrings::strToIntDef( $intApPayeeTypeId, NULL, false ) );
	}

	public function getApPayeeTypeId() {
		return $this->m_intApPayeeTypeId;
	}

	public function sqlApPayeeTypeId() {
		return ( true == isset( $this->m_intApPayeeTypeId ) ) ? ( string ) $this->m_intApPayeeTypeId : 'NULL';
	}

	public function setApPayeeStatusTypeId( $intApPayeeStatusTypeId ) {
		$this->set( 'm_intApPayeeStatusTypeId', CStrings::strToIntDef( $intApPayeeStatusTypeId, NULL, false ) );
	}

	public function getApPayeeStatusTypeId() {
		return $this->m_intApPayeeStatusTypeId;
	}

	public function sqlApPayeeStatusTypeId() {
		return ( true == isset( $this->m_intApPayeeStatusTypeId ) ) ? ( string ) $this->m_intApPayeeStatusTypeId : '1';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '1';
	}

	public function setConfidentialityAllowanceTypeId( $intConfidentialityAllowanceTypeId ) {
		$this->set( 'm_intConfidentialityAllowanceTypeId', CStrings::strToIntDef( $intConfidentialityAllowanceTypeId, NULL, false ) );
	}

	public function getConfidentialityAllowanceTypeId() {
		return $this->m_intConfidentialityAllowanceTypeId;
	}

	public function sqlConfidentialityAllowanceTypeId() {
		return ( true == isset( $this->m_intConfidentialityAllowanceTypeId ) ) ? ( string ) $this->m_intConfidentialityAllowanceTypeId : '1';
	}

	public function setCamAllowanceTypeId( $intCamAllowanceTypeId ) {
		$this->set( 'm_intCamAllowanceTypeId', CStrings::strToIntDef( $intCamAllowanceTypeId, NULL, false ) );
	}

	public function getCamAllowanceTypeId() {
		return $this->m_intCamAllowanceTypeId;
	}

	public function sqlCamAllowanceTypeId() {
		return ( true == isset( $this->m_intCamAllowanceTypeId ) ) ? ( string ) $this->m_intCamAllowanceTypeId : '1';
	}

	public function setVendorCategoryTypeId( $intVendorCategoryTypeId ) {
		$this->set( 'm_intVendorCategoryTypeId', CStrings::strToIntDef( $intVendorCategoryTypeId, NULL, false ) );
	}

	public function getVendorCategoryTypeId() {
		return $this->m_intVendorCategoryTypeId;
	}

	public function sqlVendorCategoryTypeId() {
		return ( true == isset( $this->m_intVendorCategoryTypeId ) ) ? ( string ) $this->m_intVendorCategoryTypeId : 'NULL';
	}

	public function setComplianceRulsetId( $intComplianceRulsetId ) {
		$this->set( 'm_intComplianceRulsetId', CStrings::strToIntDef( $intComplianceRulsetId, NULL, false ) );
	}

	public function getComplianceRulsetId() {
		return $this->m_intComplianceRulsetId;
	}

	public function sqlComplianceRulsetId() {
		return ( true == isset( $this->m_intComplianceRulsetId ) ) ? ( string ) $this->m_intComplianceRulsetId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 64, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryNumber ) : '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' ) : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystemCode ) : '\'' . addslashes( $this->m_strSystemCode ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 150, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 255, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebsiteUrl ) : '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setSalesTaxDescription( $strSalesTaxDescription ) {
		$this->set( 'm_strSalesTaxDescription', CStrings::strTrimDef( $strSalesTaxDescription, 500, NULL, true ) );
	}

	public function getSalesTaxDescription() {
		return $this->m_strSalesTaxDescription;
	}

	public function sqlSalesTaxDescription() {
		return ( true == isset( $this->m_strSalesTaxDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSalesTaxDescription ) : '\'' . addslashes( $this->m_strSalesTaxDescription ) . '\'' ) : '\'Sales Tax; Tax;\'';
	}

	public function setShippingDescription( $strShippingDescription ) {
		$this->set( 'm_strShippingDescription', CStrings::strTrimDef( $strShippingDescription, 500, NULL, true ) );
	}

	public function getShippingDescription() {
		return $this->m_strShippingDescription;
	}

	public function sqlShippingDescription() {
		return ( true == isset( $this->m_strShippingDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strShippingDescription ) : '\'' . addslashes( $this->m_strShippingDescription ) . '\'' ) : '\'Shipping\'';
	}

	public function setDiscountDescription( $strDiscountDescription ) {
		$this->set( 'm_strDiscountDescription', CStrings::strTrimDef( $strDiscountDescription, 500, NULL, true ) );
	}

	public function getDiscountDescription() {
		return $this->m_strDiscountDescription;
	}

	public function sqlDiscountDescription() {
		return ( true == isset( $this->m_strDiscountDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDiscountDescription ) : '\'' . addslashes( $this->m_strDiscountDescription ) . '\'' ) : '\'Discount\'';
	}

	public function setRequirePoForInvoice( $boolRequirePoForInvoice ) {
		$this->set( 'm_boolRequirePoForInvoice', CStrings::strToBool( $boolRequirePoForInvoice ) );
	}

	public function getRequirePoForInvoice() {
		return $this->m_boolRequirePoForInvoice;
	}

	public function sqlRequirePoForInvoice() {
		return ( true == isset( $this->m_boolRequirePoForInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePoForInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCategorizationQueued( $boolIsCategorizationQueued ) {
		$this->set( 'm_boolIsCategorizationQueued', CStrings::strToBool( $boolIsCategorizationQueued ) );
	}

	public function getIsCategorizationQueued() {
		return $this->m_boolIsCategorizationQueued;
	}

	public function sqlIsCategorizationQueued() {
		return ( true == isset( $this->m_boolIsCategorizationQueued ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCategorizationQueued ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsConsolidated( $boolIsConsolidated ) {
		$this->set( 'm_boolIsConsolidated', CStrings::strToBool( $boolIsConsolidated ) );
	}

	public function getIsConsolidated() {
		return $this->m_boolIsConsolidated;
	}

	public function sqlIsConsolidated() {
		return ( true == isset( $this->m_boolIsConsolidated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConsolidated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOnSite( $boolIsOnSite ) {
		$this->set( 'm_boolIsOnSite', CStrings::strToBool( $boolIsOnSite ) );
	}

	public function getIsOnSite() {
		return $this->m_boolIsOnSite;
	}

	public function sqlIsOnSite() {
		return ( true == isset( $this->m_boolIsOnSite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnSite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUtilities( $boolIsUtilities ) {
		$this->set( 'm_boolIsUtilities', CStrings::strToBool( $boolIsUtilities ) );
	}

	public function getIsUtilities() {
		return $this->m_boolIsUtilities;
	}

	public function sqlIsUtilities() {
		return ( true == isset( $this->m_boolIsUtilities ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUtilities ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCaptureInvoiceTotalOnly( $boolIsCaptureInvoiceTotalOnly ) {
		$this->set( 'm_boolIsCaptureInvoiceTotalOnly', CStrings::strToBool( $boolIsCaptureInvoiceTotalOnly ) );
	}

	public function getIsCaptureInvoiceTotalOnly() {
		return $this->m_boolIsCaptureInvoiceTotalOnly;
	}

	public function sqlIsCaptureInvoiceTotalOnly() {
		return ( true == isset( $this->m_boolIsCaptureInvoiceTotalOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCaptureInvoiceTotalOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_database_id, ap_payee_term_id, default_ap_remittance_id, gl_account_id, vendor_id, ap_payee_type_id, ap_payee_status_type_id, categorization_type_id, confidentiality_allowance_type_id, cam_allowance_type_id, vendor_category_type_id, compliance_rulset_id, reference_id, customer_id, secondary_number, remote_primary_key, system_code, company_name, username, website_url, notes, sales_tax_description, shipping_description, discount_description, require_po_for_invoice, is_categorization_queued, is_consolidated, is_system, is_on_site, is_utilities, is_capture_invoice_total_only, updated_by, updated_on, created_by, created_on, details, competitor_id, ps_product_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlApPayeeTermId() . ', ' .
						$this->sqlDefaultApRemittanceId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlVendorId() . ', ' .
						$this->sqlApPayeeTypeId() . ', ' .
						$this->sqlApPayeeStatusTypeId() . ', ' .
						$this->sqlCategorizationTypeId() . ', ' .
						$this->sqlConfidentialityAllowanceTypeId() . ', ' .
						$this->sqlCamAllowanceTypeId() . ', ' .
						$this->sqlVendorCategoryTypeId() . ', ' .
						$this->sqlComplianceRulsetId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlWebsiteUrl() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlSalesTaxDescription() . ', ' .
						$this->sqlShippingDescription() . ', ' .
						$this->sqlDiscountDescription() . ', ' .
						$this->sqlRequirePoForInvoice() . ', ' .
						$this->sqlIsCategorizationQueued() . ', ' .
						$this->sqlIsConsolidated() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsOnSite() . ', ' .
						$this->sqlIsUtilities() . ', ' .
						$this->sqlIsCaptureInvoiceTotalOnly() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlPsProductId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeTermId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_ap_remittance_id = ' . $this->sqlDefaultApRemittanceId(). ',' ; } elseif( true == array_key_exists( 'DefaultApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' default_ap_remittance_id = ' . $this->sqlDefaultApRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_type_id = ' . $this->sqlApPayeeTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_type_id = ' . $this->sqlApPayeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_status_type_id = ' . $this->sqlApPayeeStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_status_type_id = ' . $this->sqlApPayeeStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId(). ',' ; } elseif( true == array_key_exists( 'CategorizationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confidentiality_allowance_type_id = ' . $this->sqlConfidentialityAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'ConfidentialityAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' confidentiality_allowance_type_id = ' . $this->sqlConfidentialityAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cam_allowance_type_id = ' . $this->sqlCamAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'CamAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cam_allowance_type_id = ' . $this->sqlCamAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_category_type_id = ' . $this->sqlVendorCategoryTypeId(). ',' ; } elseif( true == array_key_exists( 'VendorCategoryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_category_type_id = ' . $this->sqlVendorCategoryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_rulset_id = ' . $this->sqlComplianceRulsetId(). ',' ; } elseif( true == array_key_exists( 'ComplianceRulsetId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_rulset_id = ' . $this->sqlComplianceRulsetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber(). ',' ; } elseif( true == array_key_exists( 'SecondaryNumber', $this->getChangedColumns() ) ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl(). ',' ; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_tax_description = ' . $this->sqlSalesTaxDescription(). ',' ; } elseif( true == array_key_exists( 'SalesTaxDescription', $this->getChangedColumns() ) ) { $strSql .= ' sales_tax_description = ' . $this->sqlSalesTaxDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shipping_description = ' . $this->sqlShippingDescription(). ',' ; } elseif( true == array_key_exists( 'ShippingDescription', $this->getChangedColumns() ) ) { $strSql .= ' shipping_description = ' . $this->sqlShippingDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_description = ' . $this->sqlDiscountDescription(). ',' ; } elseif( true == array_key_exists( 'DiscountDescription', $this->getChangedColumns() ) ) { $strSql .= ' discount_description = ' . $this->sqlDiscountDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_po_for_invoice = ' . $this->sqlRequirePoForInvoice(). ',' ; } elseif( true == array_key_exists( 'RequirePoForInvoice', $this->getChangedColumns() ) ) { $strSql .= ' require_po_for_invoice = ' . $this->sqlRequirePoForInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued(). ',' ; } elseif( true == array_key_exists( 'IsCategorizationQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_consolidated = ' . $this->sqlIsConsolidated(). ',' ; } elseif( true == array_key_exists( 'IsConsolidated', $this->getChangedColumns() ) ) { $strSql .= ' is_consolidated = ' . $this->sqlIsConsolidated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite(). ',' ; } elseif( true == array_key_exists( 'IsOnSite', $this->getChangedColumns() ) ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_utilities = ' . $this->sqlIsUtilities(). ',' ; } elseif( true == array_key_exists( 'IsUtilities', $this->getChangedColumns() ) ) { $strSql .= ' is_utilities = ' . $this->sqlIsUtilities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_capture_invoice_total_only = ' . $this->sqlIsCaptureInvoiceTotalOnly(). ',' ; } elseif( true == array_key_exists( 'IsCaptureInvoiceTotalOnly', $this->getChangedColumns() ) ) { $strSql .= ' is_capture_invoice_total_only = ' . $this->sqlIsCaptureInvoiceTotalOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'ap_payee_term_id' => $this->getApPayeeTermId(),
			'default_ap_remittance_id' => $this->getDefaultApRemittanceId(),
			'gl_account_id' => $this->getGlAccountId(),
			'vendor_id' => $this->getVendorId(),
			'ap_payee_type_id' => $this->getApPayeeTypeId(),
			'ap_payee_status_type_id' => $this->getApPayeeStatusTypeId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'confidentiality_allowance_type_id' => $this->getConfidentialityAllowanceTypeId(),
			'cam_allowance_type_id' => $this->getCamAllowanceTypeId(),
			'vendor_category_type_id' => $this->getVendorCategoryTypeId(),
			'compliance_rulset_id' => $this->getComplianceRulsetId(),
			'reference_id' => $this->getReferenceId(),
			'customer_id' => $this->getCustomerId(),
			'secondary_number' => $this->getSecondaryNumber(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'system_code' => $this->getSystemCode(),
			'company_name' => $this->getCompanyName(),
			'username' => $this->getUsername(),
			'website_url' => $this->getWebsiteUrl(),
			'notes' => $this->getNotes(),
			'sales_tax_description' => $this->getSalesTaxDescription(),
			'shipping_description' => $this->getShippingDescription(),
			'discount_description' => $this->getDiscountDescription(),
			'require_po_for_invoice' => $this->getRequirePoForInvoice(),
			'is_categorization_queued' => $this->getIsCategorizationQueued(),
			'is_consolidated' => $this->getIsConsolidated(),
			'is_system' => $this->getIsSystem(),
			'is_on_site' => $this->getIsOnSite(),
			'is_utilities' => $this->getIsUtilities(),
			'is_capture_invoice_total_only' => $this->getIsCaptureInvoiceTotalOnly(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'competitor_id' => $this->getCompetitorId(),
			'ps_product_id' => $this->getPsProductId()
		);
	}

}
?>