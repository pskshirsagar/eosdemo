<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledBlackoutDates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledBlackoutDates extends CEosPluralBase {

	/**
	 * @return CScheduledBlackoutDate[]
	 */
	public static function fetchScheduledBlackoutDates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledBlackoutDate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledBlackoutDate
	 */
	public static function fetchScheduledBlackoutDate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledBlackoutDate', $objDatabase );
	}

	public static function fetchScheduledBlackoutDateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_blackout_dates', $objDatabase );
	}

	public static function fetchScheduledBlackoutDateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledBlackoutDate( sprintf( 'SELECT * FROM scheduled_blackout_dates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledBlackoutDatesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledBlackoutDates( sprintf( 'SELECT * FROM scheduled_blackout_dates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledBlackoutDatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScheduledBlackoutDates( sprintf( 'SELECT * FROM scheduled_blackout_dates WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledBlackoutDatesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchScheduledBlackoutDates( sprintf( 'SELECT * FROM scheduled_blackout_dates WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>