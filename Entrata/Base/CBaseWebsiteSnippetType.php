<?php

class CBaseWebsiteSnippetType extends CEosSingularBase {

	const TABLE_NAME = 'public.website_snippet_types';

	protected $m_intId;
	protected $m_strSnippetType;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['snippet_type'] ) && $boolDirectSet ) $this->set( 'm_strSnippetType', trim( stripcslashes( $arrValues['snippet_type'] ) ) ); elseif( isset( $arrValues['snippet_type'] ) ) $this->setSnippetType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['snippet_type'] ) : $arrValues['snippet_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSnippetType( $strSnippetType ) {
		$this->set( 'm_strSnippetType', CStrings::strTrimDef( $strSnippetType, 30, NULL, true ) );
	}

	public function getSnippetType() {
		return $this->m_strSnippetType;
	}

	public function sqlSnippetType() {
		return ( true == isset( $this->m_strSnippetType ) ) ? '\'' . addslashes( $this->m_strSnippetType ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'snippet_type' => $this->getSnippetType()
		);
	}

}
?>