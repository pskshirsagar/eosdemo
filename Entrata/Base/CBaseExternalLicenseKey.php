<?php

class CBaseExternalLicenseKey extends CEosSingularBase {

	const TABLE_NAME = 'public.external_license_keys';

	protected $m_intId;
	protected $m_strVendorName;
	protected $m_strInterfaceEntityName;
	protected $m_strInterfaceEntityLicenseKey;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_name'] ) && $boolDirectSet ) $this->set( 'm_strVendorName', trim( $arrValues['vendor_name'] ) ); elseif( isset( $arrValues['vendor_name'] ) ) $this->setVendorName( $arrValues['vendor_name'] );
		if( isset( $arrValues['interface_entity_name'] ) && $boolDirectSet ) $this->set( 'm_strInterfaceEntityName', trim( $arrValues['interface_entity_name'] ) ); elseif( isset( $arrValues['interface_entity_name'] ) ) $this->setInterfaceEntityName( $arrValues['interface_entity_name'] );
		if( isset( $arrValues['interface_entity_license_key'] ) && $boolDirectSet ) $this->set( 'm_strInterfaceEntityLicenseKey', trim( $arrValues['interface_entity_license_key'] ) ); elseif( isset( $arrValues['interface_entity_license_key'] ) ) $this->setInterfaceEntityLicenseKey( $arrValues['interface_entity_license_key'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorName( $strVendorName ) {
		$this->set( 'm_strVendorName', CStrings::strTrimDef( $strVendorName, -1, NULL, true ) );
	}

	public function getVendorName() {
		return $this->m_strVendorName;
	}

	public function sqlVendorName() {
		return ( true == isset( $this->m_strVendorName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorName ) : '\'' . addslashes( $this->m_strVendorName ) . '\'' ) : 'NULL';
	}

	public function setInterfaceEntityName( $strInterfaceEntityName ) {
		$this->set( 'm_strInterfaceEntityName', CStrings::strTrimDef( $strInterfaceEntityName, -1, NULL, true ) );
	}

	public function getInterfaceEntityName() {
		return $this->m_strInterfaceEntityName;
	}

	public function sqlInterfaceEntityName() {
		return ( true == isset( $this->m_strInterfaceEntityName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInterfaceEntityName ) : '\'' . addslashes( $this->m_strInterfaceEntityName ) . '\'' ) : 'NULL';
	}

	public function setInterfaceEntityLicenseKey( $strInterfaceEntityLicenseKey ) {
		$this->set( 'm_strInterfaceEntityLicenseKey', CStrings::strTrimDef( $strInterfaceEntityLicenseKey, -1, NULL, true ) );
	}

	public function getInterfaceEntityLicenseKey() {
		return $this->m_strInterfaceEntityLicenseKey;
	}

	public function sqlInterfaceEntityLicenseKey() {
		return ( true == isset( $this->m_strInterfaceEntityLicenseKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInterfaceEntityLicenseKey ) : '\'' . addslashes( $this->m_strInterfaceEntityLicenseKey ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_name, interface_entity_name, interface_entity_license_key, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVendorName() . ', ' .
						$this->sqlInterfaceEntityName() . ', ' .
						$this->sqlInterfaceEntityLicenseKey() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName(). ',' ; } elseif( true == array_key_exists( 'VendorName', $this->getChangedColumns() ) ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interface_entity_name = ' . $this->sqlInterfaceEntityName(). ',' ; } elseif( true == array_key_exists( 'InterfaceEntityName', $this->getChangedColumns() ) ) { $strSql .= ' interface_entity_name = ' . $this->sqlInterfaceEntityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interface_entity_license_key = ' . $this->sqlInterfaceEntityLicenseKey(). ',' ; } elseif( true == array_key_exists( 'InterfaceEntityLicenseKey', $this->getChangedColumns() ) ) { $strSql .= ' interface_entity_license_key = ' . $this->sqlInterfaceEntityLicenseKey() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_name' => $this->getVendorName(),
			'interface_entity_name' => $this->getInterfaceEntityName(),
			'interface_entity_license_key' => $this->getInterfaceEntityLicenseKey(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>