<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledApExportBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_ap_export_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyTransmissionVendorId;
	protected $m_intApExportBatchTypeId;
	protected $m_intFrequencyId;
	protected $m_intFrequencyInterval;
	protected $m_strWeekDays;
	protected $m_strMonthDays;
	protected $m_strScheduledStartOn;
	protected $m_strScheduledEndOn;
	protected $m_intNumberOfOccurrences;
	protected $m_strLastPostedDate;
	protected $m_strNextPostDate;
	protected $m_boolIsZipFiles;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_boolIsIncludeReversals;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsZipFiles = false;
		$this->m_boolIsIncludeReversals = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransmissionVendorId', trim( $arrValues['company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['company_transmission_vendor_id'] ) ) $this->setCompanyTransmissionVendorId( $arrValues['company_transmission_vendor_id'] );
		if( isset( $arrValues['ap_export_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApExportBatchTypeId', trim( $arrValues['ap_export_batch_type_id'] ) ); elseif( isset( $arrValues['ap_export_batch_type_id'] ) ) $this->setApExportBatchTypeId( $arrValues['ap_export_batch_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['frequency_interval'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyInterval', trim( $arrValues['frequency_interval'] ) ); elseif( isset( $arrValues['frequency_interval'] ) ) $this->setFrequencyInterval( $arrValues['frequency_interval'] );
		if( isset( $arrValues['week_days'] ) && $boolDirectSet ) $this->set( 'm_strWeekDays', trim( stripcslashes( $arrValues['week_days'] ) ) ); elseif( isset( $arrValues['week_days'] ) ) $this->setWeekDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['week_days'] ) : $arrValues['week_days'] );
		if( isset( $arrValues['month_days'] ) && $boolDirectSet ) $this->set( 'm_strMonthDays', trim( stripcslashes( $arrValues['month_days'] ) ) ); elseif( isset( $arrValues['month_days'] ) ) $this->setMonthDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['month_days'] ) : $arrValues['month_days'] );
		if( isset( $arrValues['scheduled_start_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartOn', trim( $arrValues['scheduled_start_on'] ) ); elseif( isset( $arrValues['scheduled_start_on'] ) ) $this->setScheduledStartOn( $arrValues['scheduled_start_on'] );
		if( isset( $arrValues['scheduled_end_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndOn', trim( $arrValues['scheduled_end_on'] ) ); elseif( isset( $arrValues['scheduled_end_on'] ) ) $this->setScheduledEndOn( $arrValues['scheduled_end_on'] );
		if( isset( $arrValues['number_of_occurrences'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOccurrences', trim( $arrValues['number_of_occurrences'] ) ); elseif( isset( $arrValues['number_of_occurrences'] ) ) $this->setNumberOfOccurrences( $arrValues['number_of_occurrences'] );
		if( isset( $arrValues['last_posted_date'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedDate', trim( $arrValues['last_posted_date'] ) ); elseif( isset( $arrValues['last_posted_date'] ) ) $this->setLastPostedDate( $arrValues['last_posted_date'] );
		if( isset( $arrValues['next_post_date'] ) && $boolDirectSet ) $this->set( 'm_strNextPostDate', trim( $arrValues['next_post_date'] ) ); elseif( isset( $arrValues['next_post_date'] ) ) $this->setNextPostDate( $arrValues['next_post_date'] );
		if( isset( $arrValues['is_zip_files'] ) && $boolDirectSet ) $this->set( 'm_boolIsZipFiles', trim( stripcslashes( $arrValues['is_zip_files'] ) ) ); elseif( isset( $arrValues['is_zip_files'] ) ) $this->setIsZipFiles( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_zip_files'] ) : $arrValues['is_zip_files'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['is_include_reversals'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeReversals', trim( stripcslashes( $arrValues['is_include_reversals'] ) ) ); elseif( isset( $arrValues['is_include_reversals'] ) ) $this->setIsIncludeReversals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_reversals'] ) : $arrValues['is_include_reversals'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyTransmissionVendorId( $intCompanyTransmissionVendorId ) {
		$this->set( 'm_intCompanyTransmissionVendorId', CStrings::strToIntDef( $intCompanyTransmissionVendorId, NULL, false ) );
	}

	public function getCompanyTransmissionVendorId() {
		return $this->m_intCompanyTransmissionVendorId;
	}

	public function sqlCompanyTransmissionVendorId() {
		return ( true == isset( $this->m_intCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intCompanyTransmissionVendorId : 'NULL';
	}

	public function setApExportBatchTypeId( $intApExportBatchTypeId ) {
		$this->set( 'm_intApExportBatchTypeId', CStrings::strToIntDef( $intApExportBatchTypeId, NULL, false ) );
	}

	public function getApExportBatchTypeId() {
		return $this->m_intApExportBatchTypeId;
	}

	public function sqlApExportBatchTypeId() {
		return ( true == isset( $this->m_intApExportBatchTypeId ) ) ? ( string ) $this->m_intApExportBatchTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->set( 'm_intFrequencyInterval', CStrings::strToIntDef( $intFrequencyInterval, NULL, false ) );
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function sqlFrequencyInterval() {
		return ( true == isset( $this->m_intFrequencyInterval ) ) ? ( string ) $this->m_intFrequencyInterval : 'NULL';
	}

	public function setWeekDays( $strWeekDays ) {
		$this->set( 'm_strWeekDays', CStrings::strTrimDef( $strWeekDays, 240, NULL, true ) );
	}

	public function getWeekDays() {
		return $this->m_strWeekDays;
	}

	public function sqlWeekDays() {
		return ( true == isset( $this->m_strWeekDays ) ) ? '\'' . addslashes( $this->m_strWeekDays ) . '\'' : 'NULL';
	}

	public function setMonthDays( $strMonthDays ) {
		$this->set( 'm_strMonthDays', CStrings::strTrimDef( $strMonthDays, 240, NULL, true ) );
	}

	public function getMonthDays() {
		return $this->m_strMonthDays;
	}

	public function sqlMonthDays() {
		return ( true == isset( $this->m_strMonthDays ) ) ? '\'' . addslashes( $this->m_strMonthDays ) . '\'' : 'NULL';
	}

	public function setScheduledStartOn( $strScheduledStartOn ) {
		$this->set( 'm_strScheduledStartOn', CStrings::strTrimDef( $strScheduledStartOn, -1, NULL, true ) );
	}

	public function getScheduledStartOn() {
		return $this->m_strScheduledStartOn;
	}

	public function sqlScheduledStartOn() {
		return ( true == isset( $this->m_strScheduledStartOn ) ) ? '\'' . $this->m_strScheduledStartOn . '\'' : 'NOW()';
	}

	public function setScheduledEndOn( $strScheduledEndOn ) {
		$this->set( 'm_strScheduledEndOn', CStrings::strTrimDef( $strScheduledEndOn, -1, NULL, true ) );
	}

	public function getScheduledEndOn() {
		return $this->m_strScheduledEndOn;
	}

	public function sqlScheduledEndOn() {
		return ( true == isset( $this->m_strScheduledEndOn ) ) ? '\'' . $this->m_strScheduledEndOn . '\'' : 'NULL';
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->set( 'm_intNumberOfOccurrences', CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false ) );
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function sqlNumberOfOccurrences() {
		return ( true == isset( $this->m_intNumberOfOccurrences ) ) ? ( string ) $this->m_intNumberOfOccurrences : 'NULL';
	}

	public function setLastPostedDate( $strLastPostedDate ) {
		$this->set( 'm_strLastPostedDate', CStrings::strTrimDef( $strLastPostedDate, -1, NULL, true ) );
	}

	public function getLastPostedDate() {
		return $this->m_strLastPostedDate;
	}

	public function sqlLastPostedDate() {
		return ( true == isset( $this->m_strLastPostedDate ) ) ? '\'' . $this->m_strLastPostedDate . '\'' : 'NULL';
	}

	public function setNextPostDate( $strNextPostDate ) {
		$this->set( 'm_strNextPostDate', CStrings::strTrimDef( $strNextPostDate, -1, NULL, true ) );
	}

	public function getNextPostDate() {
		return $this->m_strNextPostDate;
	}

	public function sqlNextPostDate() {
		return ( true == isset( $this->m_strNextPostDate ) ) ? '\'' . $this->m_strNextPostDate . '\'' : 'NULL';
	}

	public function setIsZipFiles( $boolIsZipFiles ) {
		$this->set( 'm_boolIsZipFiles', CStrings::strToBool( $boolIsZipFiles ) );
	}

	public function getIsZipFiles() {
		return $this->m_boolIsZipFiles;
	}

	public function sqlIsZipFiles() {
		return ( true == isset( $this->m_boolIsZipFiles ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsZipFiles ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setIsIncludeReversals( $boolIsIncludeReversals ) {
		$this->set( 'm_boolIsIncludeReversals', CStrings::strToBool( $boolIsIncludeReversals ) );
	}

	public function getIsIncludeReversals() {
		return $this->m_boolIsIncludeReversals;
	}

	public function sqlIsIncludeReversals() {
		return ( true == isset( $this->m_boolIsIncludeReversals ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeReversals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_transmission_vendor_id, ap_export_batch_type_id, frequency_id, frequency_interval, week_days, month_days, scheduled_start_on, scheduled_end_on, number_of_occurrences, last_posted_date, next_post_date, is_zip_files, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, is_include_reversals )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyTransmissionVendorId() . ', ' .
 						$this->sqlApExportBatchTypeId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlFrequencyInterval() . ', ' .
 						$this->sqlWeekDays() . ', ' .
 						$this->sqlMonthDays() . ', ' .
 						$this->sqlScheduledStartOn() . ', ' .
 						$this->sqlScheduledEndOn() . ', ' .
 						$this->sqlNumberOfOccurrences() . ', ' .
 						$this->sqlLastPostedDate() . ', ' .
 						$this->sqlNextPostDate() . ', ' .
 						$this->sqlIsZipFiles() . ', ' .
 						$this->sqlDisabledBy() . ', ' .
 						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
 						$this->sqlIsIncludeReversals() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'CompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_export_batch_type_id = ' . $this->sqlApExportBatchTypeId() . ','; } elseif( true == array_key_exists( 'ApExportBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_export_batch_type_id = ' . $this->sqlApExportBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; } elseif( true == array_key_exists( 'FrequencyInterval', $this->getChangedColumns() ) ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; } elseif( true == array_key_exists( 'WeekDays', $this->getChangedColumns() ) ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; } elseif( true == array_key_exists( 'MonthDays', $this->getChangedColumns() ) ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_start_on = ' . $this->sqlScheduledStartOn() . ','; } elseif( true == array_key_exists( 'ScheduledStartOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_start_on = ' . $this->sqlScheduledStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_on = ' . $this->sqlScheduledEndOn() . ','; } elseif( true == array_key_exists( 'ScheduledEndOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_on = ' . $this->sqlScheduledEndOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; } elseif( true == array_key_exists( 'NumberOfOccurrences', $this->getChangedColumns() ) ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; } elseif( true == array_key_exists( 'LastPostedDate', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; } elseif( true == array_key_exists( 'NextPostDate', $this->getChangedColumns() ) ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_zip_files = ' . $this->sqlIsZipFiles() . ','; } elseif( true == array_key_exists( 'IsZipFiles', $this->getChangedColumns() ) ) { $strSql .= ' is_zip_files = ' . $this->sqlIsZipFiles() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_reversals = ' . $this->sqlIsIncludeReversals() . ','; } elseif( true == array_key_exists( 'IsIncludeReversals', $this->getChangedColumns() ) ) { $strSql .= ' is_include_reversals = ' . $this->sqlIsIncludeReversals() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_transmission_vendor_id' => $this->getCompanyTransmissionVendorId(),
			'ap_export_batch_type_id' => $this->getApExportBatchTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'frequency_interval' => $this->getFrequencyInterval(),
			'week_days' => $this->getWeekDays(),
			'month_days' => $this->getMonthDays(),
			'scheduled_start_on' => $this->getScheduledStartOn(),
			'scheduled_end_on' => $this->getScheduledEndOn(),
			'number_of_occurrences' => $this->getNumberOfOccurrences(),
			'last_posted_date' => $this->getLastPostedDate(),
			'next_post_date' => $this->getNextPostDate(),
			'is_zip_files' => $this->getIsZipFiles(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'is_include_reversals' => $this->getIsIncludeReversals()
		);
	}

}
?>