<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestResponses extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestResponse[]
	 */
	public static function fetchMaintenanceRequestResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestResponse::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestResponse
	 */
	public static function fetchMaintenanceRequestResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestResponse::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_responses', $objDatabase );
	}

	public static function fetchMaintenanceRequestResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponse( sprintf( 'SELECT * FROM maintenance_request_responses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponses( sprintf( 'SELECT * FROM maintenance_request_responses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponses( sprintf( 'SELECT * FROM maintenance_request_responses WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestResponsesByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponses( sprintf( 'SELECT * FROM maintenance_request_responses WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestResponsesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponses( sprintf( 'SELECT * FROM maintenance_request_responses WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestResponsesByMaintenanceRequestResponseTypeIdByCid( $intMaintenanceRequestResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestResponses( sprintf( 'SELECT * FROM maintenance_request_responses WHERE maintenance_request_response_type_id = %d AND cid = %d', $intMaintenanceRequestResponseTypeId, $intCid ), $objDatabase );
	}

}
?>