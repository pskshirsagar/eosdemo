<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountItemMonths
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccountItemMonths extends CEosPluralBase {

	/**
	 * @return CBudgetTabGlAccountItemMonth[]
	 */
	public static function fetchBudgetTabGlAccountItemMonths( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetTabGlAccountItemMonth::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetTabGlAccountItemMonth
	 */
	public static function fetchBudgetTabGlAccountItemMonth( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetTabGlAccountItemMonth::class, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_tab_gl_account_item_months', $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItemMonth( sprintf( 'SELECT * FROM budget_tab_gl_account_item_months WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItemMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_item_months WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItemMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_item_months WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthsByBudgetTabGlAccountItemIdByCid( $intBudgetTabGlAccountItemId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItemMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_item_months WHERE budget_tab_gl_account_item_id = %d AND cid = %d', $intBudgetTabGlAccountItemId, $intCid ), $objDatabase );
	}

}
?>