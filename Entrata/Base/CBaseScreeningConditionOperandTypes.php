<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOperandTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningConditionOperandTypes extends CEosPluralBase {

	/**
	 * @return CScreeningConditionOperandType[]
	 */
	public static function fetchScreeningConditionOperandTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningConditionOperandType::class, $objDatabase );
	}

	/**
	 * @return CScreeningConditionOperandType
	 */
	public static function fetchScreeningConditionOperandType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningConditionOperandType::class, $objDatabase );
	}

	public static function fetchScreeningConditionOperandTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_operand_types', $objDatabase );
	}

	public static function fetchScreeningConditionOperandTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningConditionOperandType( sprintf( 'SELECT * FROM screening_condition_operand_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>