<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsite extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.websites';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteTemplateId;
	protected $m_intWebsiteDisplayTypeId;
	protected $m_intPreviewWebsiteTemplateId;
	protected $m_strSubDomain;
	protected $m_strName;
	protected $m_strTitle;
	protected $m_strKeywords;
	protected $m_strCaption;
	protected $m_strContent;
	protected $m_strDescription;
	protected $m_strCompanyLogo;
	protected $m_strTemplateColor;
	protected $m_intOrderNum;
	protected $m_intBlockRentReminders;
	protected $m_intIsDisabled;
	protected $m_intLastReviewedBy;
	protected $m_strLastReviewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintWebsiteTypes;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intBlockRentReminders = '0';
		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_template_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateId', trim( $arrValues['website_template_id'] ) ); elseif( isset( $arrValues['website_template_id'] ) ) $this->setWebsiteTemplateId( $arrValues['website_template_id'] );
		if( isset( $arrValues['website_display_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteDisplayTypeId', trim( $arrValues['website_display_type_id'] ) ); elseif( isset( $arrValues['website_display_type_id'] ) ) $this->setWebsiteDisplayTypeId( $arrValues['website_display_type_id'] );
		if( isset( $arrValues['preview_website_template_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviewWebsiteTemplateId', trim( $arrValues['preview_website_template_id'] ) ); elseif( isset( $arrValues['preview_website_template_id'] ) ) $this->setPreviewWebsiteTemplateId( $arrValues['preview_website_template_id'] );
		if( isset( $arrValues['sub_domain'] ) && $boolDirectSet ) $this->set( 'm_strSubDomain', trim( $arrValues['sub_domain'] ) ); elseif( isset( $arrValues['sub_domain'] ) ) $this->setSubDomain( $arrValues['sub_domain'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['keywords'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strKeywords', trim( $arrValues['keywords'] ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( $arrValues['keywords'] );
		if( isset( $arrValues['caption'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCaption', trim( $arrValues['caption'] ) ); elseif( isset( $arrValues['caption'] ) ) $this->setCaption( $arrValues['caption'] );
		if( isset( $arrValues['content'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strContent', trim( $arrValues['content'] ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( $arrValues['content'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['company_logo'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCompanyLogo', trim( $arrValues['company_logo'] ) ); elseif( isset( $arrValues['company_logo'] ) ) $this->setCompanyLogo( $arrValues['company_logo'] );
		if( isset( $arrValues['template_color'] ) && $boolDirectSet ) $this->set( 'm_strTemplateColor', trim( $arrValues['template_color'] ) ); elseif( isset( $arrValues['template_color'] ) ) $this->setTemplateColor( $arrValues['template_color'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['block_rent_reminders'] ) && $boolDirectSet ) $this->set( 'm_intBlockRentReminders', trim( $arrValues['block_rent_reminders'] ) ); elseif( isset( $arrValues['block_rent_reminders'] ) ) $this->setBlockRentReminders( $arrValues['block_rent_reminders'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['last_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastReviewedBy', trim( $arrValues['last_reviewed_by'] ) ); elseif( isset( $arrValues['last_reviewed_by'] ) ) $this->setLastReviewedBy( $arrValues['last_reviewed_by'] );
		if( isset( $arrValues['last_reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastReviewedOn', trim( $arrValues['last_reviewed_on'] ) ); elseif( isset( $arrValues['last_reviewed_on'] ) ) $this->setLastReviewedOn( $arrValues['last_reviewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['website_types'] ) && $boolDirectSet ) $this->set( 'm_arrintWebsiteTypes', trim( $arrValues['website_types'] ) ); elseif( isset( $arrValues['website_types'] ) ) $this->setWebsiteTypes( $arrValues['website_types'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteTemplateId( $intWebsiteTemplateId ) {
		$this->set( 'm_intWebsiteTemplateId', CStrings::strToIntDef( $intWebsiteTemplateId, NULL, false ) );
	}

	public function getWebsiteTemplateId() {
		return $this->m_intWebsiteTemplateId;
	}

	public function sqlWebsiteTemplateId() {
		return ( true == isset( $this->m_intWebsiteTemplateId ) ) ? ( string ) $this->m_intWebsiteTemplateId : 'NULL';
	}

	public function setWebsiteDisplayTypeId( $intWebsiteDisplayTypeId ) {
		$this->set( 'm_intWebsiteDisplayTypeId', CStrings::strToIntDef( $intWebsiteDisplayTypeId, NULL, false ) );
	}

	public function getWebsiteDisplayTypeId() {
		return $this->m_intWebsiteDisplayTypeId;
	}

	public function sqlWebsiteDisplayTypeId() {
		return ( true == isset( $this->m_intWebsiteDisplayTypeId ) ) ? ( string ) $this->m_intWebsiteDisplayTypeId : 'NULL';
	}

	public function setPreviewWebsiteTemplateId( $intPreviewWebsiteTemplateId ) {
		$this->set( 'm_intPreviewWebsiteTemplateId', CStrings::strToIntDef( $intPreviewWebsiteTemplateId, NULL, false ) );
	}

	public function getPreviewWebsiteTemplateId() {
		return $this->m_intPreviewWebsiteTemplateId;
	}

	public function sqlPreviewWebsiteTemplateId() {
		return ( true == isset( $this->m_intPreviewWebsiteTemplateId ) ) ? ( string ) $this->m_intPreviewWebsiteTemplateId : 'NULL';
	}

	public function setSubDomain( $strSubDomain ) {
		$this->set( 'm_strSubDomain', CStrings::strTrimDef( $strSubDomain, 240, NULL, true ) );
	}

	public function getSubDomain() {
		return $this->m_strSubDomain;
	}

	public function sqlSubDomain() {
		return ( true == isset( $this->m_strSubDomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubDomain ) : '\'' . addslashes( $this->m_strSubDomain ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setKeywords( $strKeywords, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 4096, NULL, true ), $strLocaleCode );
	}

	public function getKeywords( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strKeywords', $strLocaleCode );
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKeywords ) : '\'' . addslashes( $this->m_strKeywords ) . '\'' ) : 'NULL';
	}

	public function setCaption( $strCaption, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCaption', CStrings::strTrimDef( $strCaption, 50, NULL, true ), $strLocaleCode );
	}

	public function getCaption( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCaption', $strLocaleCode );
	}

	public function sqlCaption() {
		return ( true == isset( $this->m_strCaption ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCaption ) : '\'' . addslashes( $this->m_strCaption ) . '\'' ) : 'NULL';
	}

	public function setContent( $strContent, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ), $strLocaleCode );
	}

	public function getContent( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strContent', $strLocaleCode );
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContent ) : '\'' . addslashes( $this->m_strContent ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 320, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setCompanyLogo( $strCompanyLogo, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCompanyLogo', CStrings::strTrimDef( $strCompanyLogo, 50, NULL, true ), $strLocaleCode );
	}

	public function getCompanyLogo( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCompanyLogo', $strLocaleCode );
	}

	public function sqlCompanyLogo() {
		return ( true == isset( $this->m_strCompanyLogo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyLogo ) : '\'' . addslashes( $this->m_strCompanyLogo ) . '\'' ) : 'NULL';
	}

	public function setTemplateColor( $strTemplateColor ) {
		$this->set( 'm_strTemplateColor', CStrings::strTrimDef( $strTemplateColor, 2000, NULL, true ) );
	}

	public function getTemplateColor() {
		return $this->m_strTemplateColor;
	}

	public function sqlTemplateColor() {
		return ( true == isset( $this->m_strTemplateColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateColor ) : '\'' . addslashes( $this->m_strTemplateColor ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setBlockRentReminders( $intBlockRentReminders ) {
		$this->set( 'm_intBlockRentReminders', CStrings::strToIntDef( $intBlockRentReminders, NULL, false ) );
	}

	public function getBlockRentReminders() {
		return $this->m_intBlockRentReminders;
	}

	public function sqlBlockRentReminders() {
		return ( true == isset( $this->m_intBlockRentReminders ) ) ? ( string ) $this->m_intBlockRentReminders : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setLastReviewedBy( $intLastReviewedBy ) {
		$this->set( 'm_intLastReviewedBy', CStrings::strToIntDef( $intLastReviewedBy, NULL, false ) );
	}

	public function getLastReviewedBy() {
		return $this->m_intLastReviewedBy;
	}

	public function sqlLastReviewedBy() {
		return ( true == isset( $this->m_intLastReviewedBy ) ) ? ( string ) $this->m_intLastReviewedBy : 'NULL';
	}

	public function setLastReviewedOn( $strLastReviewedOn ) {
		$this->set( 'm_strLastReviewedOn', CStrings::strTrimDef( $strLastReviewedOn, -1, NULL, true ) );
	}

	public function getLastReviewedOn() {
		return $this->m_strLastReviewedOn;
	}

	public function sqlLastReviewedOn() {
		return ( true == isset( $this->m_strLastReviewedOn ) ) ? '\'' . $this->m_strLastReviewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDetails( $jsonDetails ) {
		if( true == valObj( $jsonDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDetails', $jsonDetails );
		} elseif( true == valJsonString( $jsonDetails ) ) {
			$this->set( 'm_jsonDetails', CStrings::strToJson( $jsonDetails ) );
		} else {
			$this->set( 'm_jsonDetails', NULL ); 
		}
		unset( $this->m_strDetails );
	}

	public function getDetails() {
		if( true == isset( $this->m_strDetails ) ) {
			$this->m_jsonDetails = CStrings::strToJson( $this->m_strDetails );
			unset( $this->m_strDetails );
		}
		$this->loadUserDefinedFields();
		return $this->m_jsonDetails;
	}

	public function sqlDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setWebsiteTypes( $arrintWebsiteTypes ) {
		$this->set( 'm_arrintWebsiteTypes', CStrings::strToArrIntDef( $arrintWebsiteTypes, NULL ) );
	}

	public function getWebsiteTypes() {
		return $this->m_arrintWebsiteTypes;
	}

	public function sqlWebsiteTypes() {
		return ( true == isset( $this->m_arrintWebsiteTypes ) && true == valArr( $this->m_arrintWebsiteTypes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintWebsiteTypes, NULL ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_template_id, website_display_type_id, preview_website_template_id, sub_domain, name, title, keywords, caption, content, description, company_logo, template_color, order_num, block_rent_reminders, is_disabled, last_reviewed_by, last_reviewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, website_types )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteTemplateId() . ', ' .
						$this->sqlWebsiteDisplayTypeId() . ', ' .
						$this->sqlPreviewWebsiteTemplateId() . ', ' .
						$this->sqlSubDomain() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlCaption() . ', ' .
						$this->sqlContent() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlCompanyLogo() . ', ' .
						$this->sqlTemplateColor() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlBlockRentReminders() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlLastReviewedBy() . ', ' .
						$this->sqlLastReviewedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlWebsiteTypes() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_display_type_id = ' . $this->sqlWebsiteDisplayTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsiteDisplayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_display_type_id = ' . $this->sqlWebsiteDisplayTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preview_website_template_id = ' . $this->sqlPreviewWebsiteTemplateId(). ',' ; } elseif( true == array_key_exists( 'PreviewWebsiteTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' preview_website_template_id = ' . $this->sqlPreviewWebsiteTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain(). ',' ; } elseif( true == array_key_exists( 'SubDomain', $this->getChangedColumns() ) ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caption = ' . $this->sqlCaption(). ',' ; } elseif( true == array_key_exists( 'Caption', $this->getChangedColumns() ) ) { $strSql .= ' caption = ' . $this->sqlCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent(). ',' ; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_logo = ' . $this->sqlCompanyLogo(). ',' ; } elseif( true == array_key_exists( 'CompanyLogo', $this->getChangedColumns() ) ) { $strSql .= ' company_logo = ' . $this->sqlCompanyLogo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_color = ' . $this->sqlTemplateColor(). ',' ; } elseif( true == array_key_exists( 'TemplateColor', $this->getChangedColumns() ) ) { $strSql .= ' template_color = ' . $this->sqlTemplateColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_rent_reminders = ' . $this->sqlBlockRentReminders(). ',' ; } elseif( true == array_key_exists( 'BlockRentReminders', $this->getChangedColumns() ) ) { $strSql .= ' block_rent_reminders = ' . $this->sqlBlockRentReminders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy(). ',' ; } elseif( true == array_key_exists( 'LastReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reviewed_on = ' . $this->sqlLastReviewedOn(). ',' ; } elseif( true == array_key_exists( 'LastReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_reviewed_on = ' . $this->sqlLastReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_types = ' . $this->sqlWebsiteTypes(). ',' ; } elseif( true == array_key_exists( 'WebsiteTypes', $this->getChangedColumns() ) ) { $strSql .= ' website_types = ' . $this->sqlWebsiteTypes() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_template_id' => $this->getWebsiteTemplateId(),
			'website_display_type_id' => $this->getWebsiteDisplayTypeId(),
			'preview_website_template_id' => $this->getPreviewWebsiteTemplateId(),
			'sub_domain' => $this->getSubDomain(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'keywords' => $this->getKeywords(),
			'caption' => $this->getCaption(),
			'content' => $this->getContent(),
			'description' => $this->getDescription(),
			'company_logo' => $this->getCompanyLogo(),
			'template_color' => $this->getTemplateColor(),
			'order_num' => $this->getOrderNum(),
			'block_rent_reminders' => $this->getBlockRentReminders(),
			'is_disabled' => $this->getIsDisabled(),
			'last_reviewed_by' => $this->getLastReviewedBy(),
			'last_reviewed_on' => $this->getLastReviewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'website_types' => $this->getWebsiteTypes()
		);
	}

}
?>