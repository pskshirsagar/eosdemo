<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyEmployeeContactTypes extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeContactType[]
	 */
	public static function fetchCompanyEmployeeContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyEmployeeContactType::class, $objDatabase );
	}

	/**
	 * @return CCompanyEmployeeContactType
	 */
	public static function fetchCompanyEmployeeContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEmployeeContactType::class, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_contact_types', $objDatabase );
	}

	public static function fetchCompanyEmployeeContactTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyEmployeeContactType( sprintf( 'SELECT * FROM company_employee_contact_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>