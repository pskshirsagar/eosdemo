<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidates extends CEosPluralBase {

	/**
	 * @return CCandidate[]
	 */
	public static function fetchCandidates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidate
	 */
	public static function fetchCandidate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidate', $objDatabase );
	}

	public static function fetchCandidateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidates', $objDatabase );
	}

	public static function fetchCandidateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidate( sprintf( 'SELECT * FROM candidates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCid( $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCareerApplicationIdByCid( $intCareerApplicationId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE career_application_id = %d AND cid = %d', ( int ) $intCareerApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCareerIdByCid( $intCareerId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE career_id = %d AND cid = %d', ( int ) $intCareerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCandidateStatusTypeIdByCid( $intCandidateStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE candidate_status_type_id = %d AND cid = %d', ( int ) $intCandidateStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidatesByCareerSourceIdByCid( $intCareerSourceId, $intCid, $objDatabase ) {
		return self::fetchCandidates( sprintf( 'SELECT * FROM candidates WHERE career_source_id = %d AND cid = %d', ( int ) $intCareerSourceId, ( int ) $intCid ), $objDatabase );
	}

}
?>