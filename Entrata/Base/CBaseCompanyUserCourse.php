<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserCourse extends CEosSingularBase {

	const TABLE_NAME = 'public.company_user_courses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intCompanyUserAssessmentId;
	protected $m_intCourseHelpResourceId;
	protected $m_intHelpResourceId;
	protected $m_strCompletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intSectionHelpResourceId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['company_user_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserAssessmentId', trim( $arrValues['company_user_assessment_id'] ) ); elseif( isset( $arrValues['company_user_assessment_id'] ) ) $this->setCompanyUserAssessmentId( $arrValues['company_user_assessment_id'] );
		if( isset( $arrValues['course_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intCourseHelpResourceId', trim( $arrValues['course_help_resource_id'] ) ); elseif( isset( $arrValues['course_help_resource_id'] ) ) $this->setCourseHelpResourceId( $arrValues['course_help_resource_id'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['section_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intSectionHelpResourceId', trim( $arrValues['section_help_resource_id'] ) ); elseif( isset( $arrValues['section_help_resource_id'] ) ) $this->setSectionHelpResourceId( $arrValues['section_help_resource_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setCompanyUserAssessmentId( $intCompanyUserAssessmentId ) {
		$this->set( 'm_intCompanyUserAssessmentId', CStrings::strToIntDef( $intCompanyUserAssessmentId, NULL, false ) );
	}

	public function getCompanyUserAssessmentId() {
		return $this->m_intCompanyUserAssessmentId;
	}

	public function sqlCompanyUserAssessmentId() {
		return ( true == isset( $this->m_intCompanyUserAssessmentId ) ) ? ( string ) $this->m_intCompanyUserAssessmentId : 'NULL';
	}

	public function setCourseHelpResourceId( $intCourseHelpResourceId ) {
		$this->set( 'm_intCourseHelpResourceId', CStrings::strToIntDef( $intCourseHelpResourceId, NULL, false ) );
	}

	public function getCourseHelpResourceId() {
		return $this->m_intCourseHelpResourceId;
	}

	public function sqlCourseHelpResourceId() {
		return ( true == isset( $this->m_intCourseHelpResourceId ) ) ? ( string ) $this->m_intCourseHelpResourceId : 'NULL';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setSectionHelpResourceId( $intSectionHelpResourceId ) {
		$this->set( 'm_intSectionHelpResourceId', CStrings::strToIntDef( $intSectionHelpResourceId, NULL, false ) );
	}

	public function getSectionHelpResourceId() {
		return $this->m_intSectionHelpResourceId;
	}

	public function sqlSectionHelpResourceId() {
		return ( true == isset( $this->m_intSectionHelpResourceId ) ) ? ( string ) $this->m_intSectionHelpResourceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, company_user_assessment_id, course_help_resource_id, help_resource_id, completed_on, created_by, created_on, updated_by, updated_on, section_help_resource_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlCompanyUserAssessmentId() . ', ' .
						$this->sqlCourseHelpResourceId() . ', ' .
						$this->sqlHelpResourceId() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlSectionHelpResourceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_help_resource_id = ' . $this->sqlCourseHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'CourseHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' course_help_resource_id = ' . $this->sqlCourseHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' section_help_resource_id = ' . $this->sqlSectionHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'SectionHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' section_help_resource_id = ' . $this->sqlSectionHelpResourceId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'company_user_assessment_id' => $this->getCompanyUserAssessmentId(),
			'course_help_resource_id' => $this->getCourseHelpResourceId(),
			'help_resource_id' => $this->getHelpResourceId(),
			'completed_on' => $this->getCompletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'section_help_resource_id' => $this->getSectionHelpResourceId()
		);
	}

}
?>