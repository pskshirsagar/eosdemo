<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportDocuments extends CEosPluralBase {

    public static function fetchReportDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CReportDocument', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchReportDocument( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CReportDocument', $objDatabase );
    }

    public static function fetchReportDocumentCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'report_documents', $objDatabase );
    }

    public static function fetchReportDocumentByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchReportDocument( sprintf( 'SELECT * FROM report_documents WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchReportDocumentsByCid( $intCid, $objDatabase ) {
        return self::fetchReportDocuments( sprintf( 'SELECT * FROM report_documents WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchReportDocumentsByReportHistoryIdByCid( $intReportHistoryId, $intCid, $objDatabase ) {
        return self::fetchReportDocuments( sprintf( 'SELECT * FROM report_documents WHERE report_history_id = %d AND cid = %d', (int) $intReportHistoryId, (int) $intCid ), $objDatabase );
    }

    public static function fetchReportDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
        return self::fetchReportDocuments( sprintf( 'SELECT * FROM report_documents WHERE document_id = %d AND cid = %d', (int) $intDocumentId, (int) $intCid ), $objDatabase );
    }

    public static function fetchReportDocumentsByReportDocumentTypeIdByCid( $intReportDocumentTypeId, $intCid, $objDatabase ) {
        return self::fetchReportDocuments( sprintf( 'SELECT * FROM report_documents WHERE report_document_type_id = %d AND cid = %d', (int) $intReportDocumentTypeId, (int) $intCid ), $objDatabase );
    }

}
?>