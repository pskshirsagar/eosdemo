<?php

class CBaseArTrigger extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ar_triggers';

	protected $m_intId;
	protected $m_intArTriggerTypeId;
	protected $m_intFrequencyId;
	protected $m_intUtilityTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolAllowFlexibleLeaseCaching;
	protected $m_boolAllowRateOffsets;
	protected $m_boolAllowArCodeMapping;
	protected $m_boolRequireCustomerRelationship;
	protected $m_boolRequireLeaseIntervalType;
	protected $m_boolIsOptional;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowFlexibleLeaseCaching = false;
		$this->m_boolAllowRateOffsets = false;
		$this->m_boolAllowArCodeMapping = false;
		$this->m_boolRequireCustomerRelationship = false;
		$this->m_boolRequireLeaseIntervalType = false;
		$this->m_boolIsOptional = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ar_trigger_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerTypeId', trim( $arrValues['ar_trigger_type_id'] ) ); elseif( isset( $arrValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrValues['ar_trigger_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['allow_flexible_lease_caching'] ) && $boolDirectSet ) $this->set( 'm_boolAllowFlexibleLeaseCaching', trim( stripcslashes( $arrValues['allow_flexible_lease_caching'] ) ) ); elseif( isset( $arrValues['allow_flexible_lease_caching'] ) ) $this->setAllowFlexibleLeaseCaching( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_flexible_lease_caching'] ) : $arrValues['allow_flexible_lease_caching'] );
		if( isset( $arrValues['allow_rate_offsets'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRateOffsets', trim( stripcslashes( $arrValues['allow_rate_offsets'] ) ) ); elseif( isset( $arrValues['allow_rate_offsets'] ) ) $this->setAllowRateOffsets( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_rate_offsets'] ) : $arrValues['allow_rate_offsets'] );
		if( isset( $arrValues['allow_ar_code_mapping'] ) && $boolDirectSet ) $this->set( 'm_boolAllowArCodeMapping', trim( stripcslashes( $arrValues['allow_ar_code_mapping'] ) ) ); elseif( isset( $arrValues['allow_ar_code_mapping'] ) ) $this->setAllowArCodeMapping( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_ar_code_mapping'] ) : $arrValues['allow_ar_code_mapping'] );
		if( isset( $arrValues['require_customer_relationship'] ) && $boolDirectSet ) $this->set( 'm_boolRequireCustomerRelationship', trim( stripcslashes( $arrValues['require_customer_relationship'] ) ) ); elseif( isset( $arrValues['require_customer_relationship'] ) ) $this->setRequireCustomerRelationship( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_customer_relationship'] ) : $arrValues['require_customer_relationship'] );
		if( isset( $arrValues['require_lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_boolRequireLeaseIntervalType', trim( stripcslashes( $arrValues['require_lease_interval_type'] ) ) ); elseif( isset( $arrValues['require_lease_interval_type'] ) ) $this->setRequireLeaseIntervalType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_lease_interval_type'] ) : $arrValues['require_lease_interval_type'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptional', trim( stripcslashes( $arrValues['is_optional'] ) ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optional'] ) : $arrValues['is_optional'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->set( 'm_intArTriggerTypeId', CStrings::strToIntDef( $intArTriggerTypeId, NULL, false ) );
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function sqlArTriggerTypeId() {
		return ( true == isset( $this->m_intArTriggerTypeId ) ) ? ( string ) $this->m_intArTriggerTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setAllowFlexibleLeaseCaching( $boolAllowFlexibleLeaseCaching ) {
		$this->set( 'm_boolAllowFlexibleLeaseCaching', CStrings::strToBool( $boolAllowFlexibleLeaseCaching ) );
	}

	public function getAllowFlexibleLeaseCaching() {
		return $this->m_boolAllowFlexibleLeaseCaching;
	}

	public function sqlAllowFlexibleLeaseCaching() {
		return ( true == isset( $this->m_boolAllowFlexibleLeaseCaching ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowFlexibleLeaseCaching ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRateOffsets( $boolAllowRateOffsets ) {
		$this->set( 'm_boolAllowRateOffsets', CStrings::strToBool( $boolAllowRateOffsets ) );
	}

	public function getAllowRateOffsets() {
		return $this->m_boolAllowRateOffsets;
	}

	public function sqlAllowRateOffsets() {
		return ( true == isset( $this->m_boolAllowRateOffsets ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRateOffsets ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowArCodeMapping( $boolAllowArCodeMapping ) {
		$this->set( 'm_boolAllowArCodeMapping', CStrings::strToBool( $boolAllowArCodeMapping ) );
	}

	public function getAllowArCodeMapping() {
		return $this->m_boolAllowArCodeMapping;
	}

	public function sqlAllowArCodeMapping() {
		return ( true == isset( $this->m_boolAllowArCodeMapping ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowArCodeMapping ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireCustomerRelationship( $boolRequireCustomerRelationship ) {
		$this->set( 'm_boolRequireCustomerRelationship', CStrings::strToBool( $boolRequireCustomerRelationship ) );
	}

	public function getRequireCustomerRelationship() {
		return $this->m_boolRequireCustomerRelationship;
	}

	public function sqlRequireCustomerRelationship() {
		return ( true == isset( $this->m_boolRequireCustomerRelationship ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireCustomerRelationship ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireLeaseIntervalType( $boolRequireLeaseIntervalType ) {
		$this->set( 'm_boolRequireLeaseIntervalType', CStrings::strToBool( $boolRequireLeaseIntervalType ) );
	}

	public function getRequireLeaseIntervalType() {
		return $this->m_boolRequireLeaseIntervalType;
	}

	public function sqlRequireLeaseIntervalType() {
		return ( true == isset( $this->m_boolRequireLeaseIntervalType ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireLeaseIntervalType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->set( 'm_boolIsOptional', CStrings::strToBool( $boolIsOptional ) );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_boolIsOptional ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptional ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ar_trigger_type_id' => $this->getArTriggerTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'allow_flexible_lease_caching' => $this->getAllowFlexibleLeaseCaching(),
			'allow_rate_offsets' => $this->getAllowRateOffsets(),
			'allow_ar_code_mapping' => $this->getAllowArCodeMapping(),
			'require_customer_relationship' => $this->getRequireCustomerRelationship(),
			'require_lease_interval_type' => $this->getRequireLeaseIntervalType(),
			'is_optional' => $this->getIsOptional(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>