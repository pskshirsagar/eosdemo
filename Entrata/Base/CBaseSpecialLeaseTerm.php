<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpecialLeaseTerm extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intSpecialId;
    protected $m_intLeaseTermId;
    protected $m_intLeaseStartWindowId;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['special_id'] ) && $boolDirectSet ) $this->m_intSpecialId = trim( $arrValues['special_id'] ); else if( isset( $arrValues['special_id'] ) ) $this->setSpecialId( $arrValues['special_id'] );
        if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->m_intLeaseTermId = trim( $arrValues['lease_term_id'] ); else if( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
        if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->m_intLeaseStartWindowId = trim( $arrValues['lease_start_window_id'] ); else if( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setSpecialId( $intSpecialId ) {
        $this->m_intSpecialId = CStrings::strToIntDef( $intSpecialId, NULL, false );
    }

    public function getSpecialId() {
        return $this->m_intSpecialId;
    }

    public function sqlSpecialId() {
        return ( true == isset( $this->m_intSpecialId ) ) ? (string) $this->m_intSpecialId : 'NULL';
    }

    public function setLeaseTermId( $intLeaseTermId ) {
        $this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId, NULL, false );
    }

    public function getLeaseTermId() {
        return $this->m_intLeaseTermId;
    }

    public function sqlLeaseTermId() {
        return ( true == isset( $this->m_intLeaseTermId ) ) ? (string) $this->m_intLeaseTermId : 'NULL';
    }

    public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
        $this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false );
    }

    public function getLeaseStartWindowId() {
        return $this->m_intLeaseStartWindowId;
    }

    public function sqlLeaseStartWindowId() {
        return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? (string) $this->m_intLeaseStartWindowId : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.special_lease_terms_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.special_lease_terms( id, cid, special_id, lease_term_id, lease_start_window_id, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlSpecialId() . ', ' . 		                $this->sqlLeaseTermId() . ', ' . 		                $this->sqlLeaseStartWindowId() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.special_lease_terms
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_id = ' . $this->sqlSpecialId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSpecialId() ) != $this->getOriginalValueByFieldName ( 'special_id' ) ) { $arrstrOriginalValueChanges['special_id'] = $this->sqlSpecialId(); $strSql .= ' special_id = ' . $this->sqlSpecialId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseTermId() ) != $this->getOriginalValueByFieldName ( 'lease_term_id' ) ) { $arrstrOriginalValueChanges['lease_term_id'] = $this->sqlLeaseTermId(); $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseStartWindowId() ) != $this->getOriginalValueByFieldName ( 'lease_start_window_id' ) ) { $arrstrOriginalValueChanges['lease_start_window_id'] = $this->sqlLeaseStartWindowId(); $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.special_lease_terms WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.special_lease_terms_id_seq', $objDatabase );
    }

}
?>