<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.gl_groups';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlGroupTypeId;
	protected $m_intGlTreeId;
	protected $m_intParentGlBranchId;
	protected $m_intOriginGlBranchId;
	protected $m_intDefaultGlGroupId;
	protected $m_intGlBranchTypeId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strTotalGroupName;
	protected $m_strDescription;
	protected $m_strTotalLabel;
	protected $m_strRemotePrimaryKey;
	protected $m_intHideOnReports;
	protected $m_intIsTotaled;
	protected $m_intIsUnderlined;
	protected $m_intIsBold;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_boolShowGroupName;
	protected $m_boolShowTotalGroupName;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intParentGlGroupId;
	protected $m_intOriginGlGroupId;
	protected $m_strSummaryRowName;
	protected $m_boolShowSummaryRowName;
	protected $m_intGlAccountTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intGlGroupTypeId = '1';
		$this->m_intGlBranchTypeId = '1';
		$this->m_strRemotePrimaryKey = NULL;
		$this->m_intHideOnReports = '0';
		$this->m_intIsTotaled = '0';
		$this->m_intIsUnderlined = '0';
		$this->m_intIsBold = '0';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolShowGroupName = true;
		$this->m_boolShowTotalGroupName = true;
		$this->m_boolShowSummaryRowName = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeId', trim( $arrValues['gl_tree_id'] ) ); elseif( isset( $arrValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrValues['gl_tree_id'] );
		if( isset( $arrValues['parent_gl_branch_id'] ) && $boolDirectSet ) $this->set( 'm_intParentGlBranchId', trim( $arrValues['parent_gl_branch_id'] ) ); elseif( isset( $arrValues['parent_gl_branch_id'] ) ) $this->setParentGlBranchId( $arrValues['parent_gl_branch_id'] );
		if( isset( $arrValues['origin_gl_branch_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginGlBranchId', trim( $arrValues['origin_gl_branch_id'] ) ); elseif( isset( $arrValues['origin_gl_branch_id'] ) ) $this->setOriginGlBranchId( $arrValues['origin_gl_branch_id'] );
		if( isset( $arrValues['default_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlGroupId', trim( $arrValues['default_gl_group_id'] ) ); elseif( isset( $arrValues['default_gl_group_id'] ) ) $this->setDefaultGlGroupId( $arrValues['default_gl_group_id'] );
		if( isset( $arrValues['gl_branch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBranchTypeId', trim( $arrValues['gl_branch_type_id'] ) ); elseif( isset( $arrValues['gl_branch_type_id'] ) ) $this->setGlBranchTypeId( $arrValues['gl_branch_type_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( $arrValues['system_code'] ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['total_group_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTotalGroupName', trim( $arrValues['total_group_name'] ) ); elseif( isset( $arrValues['total_group_name'] ) ) $this->setTotalGroupName( $arrValues['total_group_name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['total_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTotalLabel', trim( $arrValues['total_label'] ) ); elseif( isset( $arrValues['total_label'] ) ) $this->setTotalLabel( $arrValues['total_label'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['hide_on_reports'] ) && $boolDirectSet ) $this->set( 'm_intHideOnReports', trim( $arrValues['hide_on_reports'] ) ); elseif( isset( $arrValues['hide_on_reports'] ) ) $this->setHideOnReports( $arrValues['hide_on_reports'] );
		if( isset( $arrValues['is_totaled'] ) && $boolDirectSet ) $this->set( 'm_intIsTotaled', trim( $arrValues['is_totaled'] ) ); elseif( isset( $arrValues['is_totaled'] ) ) $this->setIsTotaled( $arrValues['is_totaled'] );
		if( isset( $arrValues['is_underlined'] ) && $boolDirectSet ) $this->set( 'm_intIsUnderlined', trim( $arrValues['is_underlined'] ) ); elseif( isset( $arrValues['is_underlined'] ) ) $this->setIsUnderlined( $arrValues['is_underlined'] );
		if( isset( $arrValues['is_bold'] ) && $boolDirectSet ) $this->set( 'm_intIsBold', trim( $arrValues['is_bold'] ) ); elseif( isset( $arrValues['is_bold'] ) ) $this->setIsBold( $arrValues['is_bold'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['show_group_name'] ) && $boolDirectSet ) $this->set( 'm_boolShowGroupName', trim( stripcslashes( $arrValues['show_group_name'] ) ) ); elseif( isset( $arrValues['show_group_name'] ) ) $this->setShowGroupName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_group_name'] ) : $arrValues['show_group_name'] );
		if( isset( $arrValues['show_total_group_name'] ) && $boolDirectSet ) $this->set( 'm_boolShowTotalGroupName', trim( stripcslashes( $arrValues['show_total_group_name'] ) ) ); elseif( isset( $arrValues['show_total_group_name'] ) ) $this->setShowTotalGroupName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_total_group_name'] ) : $arrValues['show_total_group_name'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['parent_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intParentGlGroupId', trim( $arrValues['parent_gl_group_id'] ) ); elseif( isset( $arrValues['parent_gl_group_id'] ) ) $this->setParentGlGroupId( $arrValues['parent_gl_group_id'] );
		if( isset( $arrValues['origin_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginGlGroupId', trim( $arrValues['origin_gl_group_id'] ) ); elseif( isset( $arrValues['origin_gl_group_id'] ) ) $this->setOriginGlGroupId( $arrValues['origin_gl_group_id'] );
		if( isset( $arrValues['summary_row_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSummaryRowName', trim( $arrValues['summary_row_name'] ) ); elseif( isset( $arrValues['summary_row_name'] ) ) $this->setSummaryRowName( $arrValues['summary_row_name'] );
		if( isset( $arrValues['show_summary_row_name'] ) && $boolDirectSet ) $this->set( 'm_boolShowSummaryRowName', trim( stripcslashes( $arrValues['show_summary_row_name'] ) ) ); elseif( isset( $arrValues['show_summary_row_name'] ) ) $this->setShowSummaryRowName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_summary_row_name'] ) : $arrValues['show_summary_row_name'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : '1';
	}

	public function setGlTreeId( $intGlTreeId ) {
		$this->set( 'm_intGlTreeId', CStrings::strToIntDef( $intGlTreeId, NULL, false ) );
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function sqlGlTreeId() {
		return ( true == isset( $this->m_intGlTreeId ) ) ? ( string ) $this->m_intGlTreeId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setParentGlBranchId( $intParentGlBranchId ) {
		$this->set( 'm_intParentGlBranchId', CStrings::strToIntDef( $intParentGlBranchId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getParentGlBranchId() {
		return $this->m_intParentGlBranchId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlParentGlBranchId() {
		return ( true == isset( $this->m_intParentGlBranchId ) ) ? ( string ) $this->m_intParentGlBranchId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setOriginGlBranchId( $intOriginGlBranchId ) {
		$this->set( 'm_intOriginGlBranchId', CStrings::strToIntDef( $intOriginGlBranchId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getOriginGlBranchId() {
		return $this->m_intOriginGlBranchId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlOriginGlBranchId() {
		return ( true == isset( $this->m_intOriginGlBranchId ) ) ? ( string ) $this->m_intOriginGlBranchId : 'NULL';
	}

	public function setDefaultGlGroupId( $intDefaultGlGroupId ) {
		$this->set( 'm_intDefaultGlGroupId', CStrings::strToIntDef( $intDefaultGlGroupId, NULL, false ) );
	}

	public function getDefaultGlGroupId() {
		return $this->m_intDefaultGlGroupId;
	}

	public function sqlDefaultGlGroupId() {
		return ( true == isset( $this->m_intDefaultGlGroupId ) ) ? ( string ) $this->m_intDefaultGlGroupId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlBranchTypeId( $intGlBranchTypeId ) {
		$this->set( 'm_intGlBranchTypeId', CStrings::strToIntDef( $intGlBranchTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlBranchTypeId() {
		return $this->m_intGlBranchTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlBranchTypeId() {
		return ( true == isset( $this->m_intGlBranchTypeId ) ) ? ( string ) $this->m_intGlBranchTypeId : '1';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystemCode ) : '\'' . addslashes( $this->m_strSystemCode ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setTotalGroupName( $strTotalGroupName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTotalGroupName', CStrings::strTrimDef( $strTotalGroupName, 100, NULL, true ), $strLocaleCode );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getTotalGroupName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTotalGroupName', $strLocaleCode );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlTotalGroupName() {
		return ( true == isset( $this->m_strTotalGroupName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTotalGroupName ) : '\'' . addslashes( $this->m_strTotalGroupName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setTotalLabel( $strTotalLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTotalLabel', CStrings::strTrimDef( $strTotalLabel, 50, NULL, true ), $strLocaleCode );
	}

	public function getTotalLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTotalLabel', $strLocaleCode );
	}

	public function sqlTotalLabel() {
		return ( true == isset( $this->m_strTotalLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTotalLabel ) : '\'' . addslashes( $this->m_strTotalLabel ) . '\'' ) : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : '\'NULL\'';
	}

	public function setHideOnReports( $intHideOnReports ) {
		$this->set( 'm_intHideOnReports', CStrings::strToIntDef( $intHideOnReports, NULL, false ) );
	}

	public function getHideOnReports() {
		return $this->m_intHideOnReports;
	}

	public function sqlHideOnReports() {
		return ( true == isset( $this->m_intHideOnReports ) ) ? ( string ) $this->m_intHideOnReports : '0';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setIsTotaled( $intIsTotaled ) {
		$this->set( 'm_intIsTotaled', CStrings::strToIntDef( $intIsTotaled, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getIsTotaled() {
		return $this->m_intIsTotaled;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlIsTotaled() {
		return ( true == isset( $this->m_intIsTotaled ) ) ? ( string ) $this->m_intIsTotaled : '0';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setIsUnderlined( $intIsUnderlined ) {
		$this->set( 'm_intIsUnderlined', CStrings::strToIntDef( $intIsUnderlined, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getIsUnderlined() {
		return $this->m_intIsUnderlined;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlIsUnderlined() {
		return ( true == isset( $this->m_intIsUnderlined ) ) ? ( string ) $this->m_intIsUnderlined : '0';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setIsBold( $intIsBold ) {
		$this->set( 'm_intIsBold', CStrings::strToIntDef( $intIsBold, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getIsBold() {
		return $this->m_intIsBold;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlIsBold() {
		return ( true == isset( $this->m_intIsBold ) ) ? ( string ) $this->m_intIsBold : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setShowGroupName( $boolShowGroupName ) {
		$this->set( 'm_boolShowGroupName', CStrings::strToBool( $boolShowGroupName ) );
	}

	public function getShowGroupName() {
		return $this->m_boolShowGroupName;
	}

	public function sqlShowGroupName() {
		return ( true == isset( $this->m_boolShowGroupName ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowGroupName ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setShowTotalGroupName( $boolShowTotalGroupName ) {
		$this->set( 'm_boolShowTotalGroupName', CStrings::strToBool( $boolShowTotalGroupName ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getShowTotalGroupName() {
		return $this->m_boolShowTotalGroupName;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlShowTotalGroupName() {
		return ( true == isset( $this->m_boolShowTotalGroupName ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowTotalGroupName ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setParentGlGroupId( $intParentGlGroupId ) {
		$this->set( 'm_intParentGlGroupId', CStrings::strToIntDef( $intParentGlGroupId, NULL, false ) );
	}

	public function getParentGlGroupId() {
		return $this->m_intParentGlGroupId;
	}

	public function sqlParentGlGroupId() {
		return ( true == isset( $this->m_intParentGlGroupId ) ) ? ( string ) $this->m_intParentGlGroupId : 'NULL';
	}

	public function setOriginGlGroupId( $intOriginGlGroupId ) {
		$this->set( 'm_intOriginGlGroupId', CStrings::strToIntDef( $intOriginGlGroupId, NULL, false ) );
	}

	public function getOriginGlGroupId() {
		return $this->m_intOriginGlGroupId;
	}

	public function sqlOriginGlGroupId() {
		return ( true == isset( $this->m_intOriginGlGroupId ) ) ? ( string ) $this->m_intOriginGlGroupId : 'NULL';
	}

	public function setSummaryRowName( $strSummaryRowName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSummaryRowName', CStrings::strTrimDef( $strSummaryRowName, -1, NULL, true ), $strLocaleCode );
	}

	public function getSummaryRowName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSummaryRowName', $strLocaleCode );
	}

	public function sqlSummaryRowName() {
		return ( true == isset( $this->m_strSummaryRowName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSummaryRowName ) : '\'' . addslashes( $this->m_strSummaryRowName ) . '\'' ) : 'NULL';
	}

	public function setShowSummaryRowName( $boolShowSummaryRowName ) {
		$this->set( 'm_boolShowSummaryRowName', CStrings::strToBool( $boolShowSummaryRowName ) );
	}

	public function getShowSummaryRowName() {
		return $this->m_boolShowSummaryRowName;
	}

	public function sqlShowSummaryRowName() {
		return ( true == isset( $this->m_boolShowSummaryRowName ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowSummaryRowName ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_group_type_id, gl_tree_id, parent_gl_branch_id, origin_gl_branch_id, default_gl_group_id, gl_branch_type_id, system_code, name, total_group_name, description, total_label, remote_primary_key, hide_on_reports, is_totaled, is_underlined, is_bold, is_system, order_num, show_group_name, show_total_group_name, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, details, parent_gl_group_id, origin_gl_group_id, summary_row_name, show_summary_row_name, gl_account_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlGroupTypeId() . ', ' .
						$this->sqlGlTreeId() . ', ' .
						$this->sqlParentGlBranchId() . ', ' .
						$this->sqlOriginGlBranchId() . ', ' .
						$this->sqlDefaultGlGroupId() . ', ' .
						$this->sqlGlBranchTypeId() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTotalGroupName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlTotalLabel() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlHideOnReports() . ', ' .
						$this->sqlIsTotaled() . ', ' .
						$this->sqlIsUnderlined() . ', ' .
						$this->sqlIsBold() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlShowGroupName() . ', ' .
						$this->sqlShowTotalGroupName() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlParentGlGroupId() . ', ' .
						$this->sqlOriginGlGroupId() . ', ' .
						$this->sqlSummaryRowName() . ', ' .
						$this->sqlShowSummaryRowName() . ', ' .
						$this->sqlGlAccountTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId(). ',' ; } elseif( true == array_key_exists( 'GlGroupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_gl_branch_id = ' . $this->sqlParentGlBranchId(). ',' ; } elseif( true == array_key_exists( 'ParentGlBranchId', $this->getChangedColumns() ) ) { $strSql .= ' parent_gl_branch_id = ' . $this->sqlParentGlBranchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_gl_branch_id = ' . $this->sqlOriginGlBranchId(). ',' ; } elseif( true == array_key_exists( 'OriginGlBranchId', $this->getChangedColumns() ) ) { $strSql .= ' origin_gl_branch_id = ' . $this->sqlOriginGlBranchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_gl_group_id = ' . $this->sqlDefaultGlGroupId(). ',' ; } elseif( true == array_key_exists( 'DefaultGlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' default_gl_group_id = ' . $this->sqlDefaultGlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_branch_type_id = ' . $this->sqlGlBranchTypeId(). ',' ; } elseif( true == array_key_exists( 'GlBranchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_branch_type_id = ' . $this->sqlGlBranchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_group_name = ' . $this->sqlTotalGroupName(). ',' ; } elseif( true == array_key_exists( 'TotalGroupName', $this->getChangedColumns() ) ) { $strSql .= ' total_group_name = ' . $this->sqlTotalGroupName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_label = ' . $this->sqlTotalLabel(). ',' ; } elseif( true == array_key_exists( 'TotalLabel', $this->getChangedColumns() ) ) { $strSql .= ' total_label = ' . $this->sqlTotalLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_on_reports = ' . $this->sqlHideOnReports(). ',' ; } elseif( true == array_key_exists( 'HideOnReports', $this->getChangedColumns() ) ) { $strSql .= ' hide_on_reports = ' . $this->sqlHideOnReports() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_totaled = ' . $this->sqlIsTotaled(). ',' ; } elseif( true == array_key_exists( 'IsTotaled', $this->getChangedColumns() ) ) { $strSql .= ' is_totaled = ' . $this->sqlIsTotaled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_underlined = ' . $this->sqlIsUnderlined(). ',' ; } elseif( true == array_key_exists( 'IsUnderlined', $this->getChangedColumns() ) ) { $strSql .= ' is_underlined = ' . $this->sqlIsUnderlined() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bold = ' . $this->sqlIsBold(). ',' ; } elseif( true == array_key_exists( 'IsBold', $this->getChangedColumns() ) ) { $strSql .= ' is_bold = ' . $this->sqlIsBold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_group_name = ' . $this->sqlShowGroupName(). ',' ; } elseif( true == array_key_exists( 'ShowGroupName', $this->getChangedColumns() ) ) { $strSql .= ' show_group_name = ' . $this->sqlShowGroupName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_total_group_name = ' . $this->sqlShowTotalGroupName(). ',' ; } elseif( true == array_key_exists( 'ShowTotalGroupName', $this->getChangedColumns() ) ) { $strSql .= ' show_total_group_name = ' . $this->sqlShowTotalGroupName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_gl_group_id = ' . $this->sqlParentGlGroupId(). ',' ; } elseif( true == array_key_exists( 'ParentGlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' parent_gl_group_id = ' . $this->sqlParentGlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_gl_group_id = ' . $this->sqlOriginGlGroupId(). ',' ; } elseif( true == array_key_exists( 'OriginGlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' origin_gl_group_id = ' . $this->sqlOriginGlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_row_name = ' . $this->sqlSummaryRowName(). ',' ; } elseif( true == array_key_exists( 'SummaryRowName', $this->getChangedColumns() ) ) { $strSql .= ' summary_row_name = ' . $this->sqlSummaryRowName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_summary_row_name = ' . $this->sqlShowSummaryRowName(). ',' ; } elseif( true == array_key_exists( 'ShowSummaryRowName', $this->getChangedColumns() ) ) { $strSql .= ' show_summary_row_name = ' . $this->sqlShowSummaryRowName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'GlAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'gl_tree_id' => $this->getGlTreeId(),
			'parent_gl_branch_id' => $this->getParentGlBranchId(),
			'origin_gl_branch_id' => $this->getOriginGlBranchId(),
			'default_gl_group_id' => $this->getDefaultGlGroupId(),
			'gl_branch_type_id' => $this->getGlBranchTypeId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'total_group_name' => $this->getTotalGroupName(),
			'description' => $this->getDescription(),
			'total_label' => $this->getTotalLabel(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'hide_on_reports' => $this->getHideOnReports(),
			'is_totaled' => $this->getIsTotaled(),
			'is_underlined' => $this->getIsUnderlined(),
			'is_bold' => $this->getIsBold(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'show_group_name' => $this->getShowGroupName(),
			'show_total_group_name' => $this->getShowTotalGroupName(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'parent_gl_group_id' => $this->getParentGlGroupId(),
			'origin_gl_group_id' => $this->getOriginGlGroupId(),
			'summary_row_name' => $this->getSummaryRowName(),
			'show_summary_row_name' => $this->getShowSummaryRowName(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>