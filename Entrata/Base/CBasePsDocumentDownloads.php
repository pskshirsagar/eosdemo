<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPsDocumentDownloads
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePsDocumentDownloads extends CEosPluralBase {

	/**
	 * @return CPsDocumentDownload[]
	 */
	public static function fetchPsDocumentDownloads( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPsDocumentDownload', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPsDocumentDownload
	 */
	public static function fetchPsDocumentDownload( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsDocumentDownload', $objDatabase );
	}

	public static function fetchPsDocumentDownloadCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_document_downloads', $objDatabase );
	}

	public static function fetchPsDocumentDownloadByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPsDocumentDownload( sprintf( 'SELECT * FROM ps_document_downloads WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsDocumentDownloadsByCid( $intCid, $objDatabase ) {
		return self::fetchPsDocumentDownloads( sprintf( 'SELECT * FROM ps_document_downloads WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsDocumentDownloadsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchPsDocumentDownloads( sprintf( 'SELECT * FROM ps_document_downloads WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsDocumentDownloadsByPsDocumentIdByCid( $intPsDocumentId, $intCid, $objDatabase ) {
		return self::fetchPsDocumentDownloads( sprintf( 'SELECT * FROM ps_document_downloads WHERE ps_document_id = %d AND cid = %d', ( int ) $intPsDocumentId, ( int ) $intCid ), $objDatabase );
	}

}
?>