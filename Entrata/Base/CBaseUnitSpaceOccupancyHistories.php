<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceOccupancyHistories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceOccupancyHistories extends CEosPluralBase {

	/**
	 * @return CUnitSpaceOccupancyHistory[]
	 */
	public static function fetchUnitSpaceOccupancyHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitSpaceOccupancyHistory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceOccupancyHistory
	 */
	public static function fetchUnitSpaceOccupancyHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitSpaceOccupancyHistory', $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_occupancy_histories', $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistory( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByOriginatingLeaseIdByCid( $intOriginatingLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE originating_lease_id = %d AND cid = %d', ( int ) $intOriginatingLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByLeaseActionIdByCid( $intLeaseActionId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceOccupancyHistories( sprintf( 'SELECT * FROM unit_space_occupancy_histories WHERE lease_action_id = %d AND cid = %d', ( int ) $intLeaseActionId, ( int ) $intCid ), $objDatabase );
	}

}
?>