<?php

class CBaseAssetLedgerTypes extends CEosPluralBase {

	public static function fetchAssetLedgerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAssetLedgerType', $objDatabase );
	}

	public static function fetchAssetLedgerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetLedgerType', $objDatabase );
	}

	public static function fetchAssetLedgerTypeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_ledger_types', $objDatabase );
	}

	public static function fetchAssetLedgerTypeById( $intId, $objDatabase ) {
		return self::fetchAssetLedgerType( sprintf( 'SELECT * FROM asset_ledger_types WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>