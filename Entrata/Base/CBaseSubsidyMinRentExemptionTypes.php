<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMinRentExemptionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyMinRentExemptionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyMinRentExemptionType[]
	 */
	public static function fetchSubsidyMinRentExemptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyMinRentExemptionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyMinRentExemptionType
	 */
	public static function fetchSubsidyMinRentExemptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyMinRentExemptionType::class, $objDatabase );
	}

	public static function fetchSubsidyMinRentExemptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_min_rent_exemption_types', $objDatabase );
	}

	public static function fetchSubsidyMinRentExemptionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyMinRentExemptionType( sprintf( 'SELECT * FROM subsidy_min_rent_exemption_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>