<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallTypeChunks
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCallTypeChunks extends CEosPluralBase {

	/**
	 * @return CCallTypeChunk[]
	 */
	public static function fetchCallTypeChunks( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCallTypeChunk', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCallTypeChunk
	 */
	public static function fetchCallTypeChunk( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallTypeChunk', $objDatabase );
	}

	public static function fetchCallTypeChunkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_type_chunks', $objDatabase );
	}

	public static function fetchCallTypeChunkByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCallTypeChunk( sprintf( 'SELECT * FROM call_type_chunks WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTypeChunksByCid( $intCid, $objDatabase ) {
		return self::fetchCallTypeChunks( sprintf( 'SELECT * FROM call_type_chunks WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTypeChunksByScheduledCallTypeIdByCid( $intScheduledCallTypeId, $intCid, $objDatabase ) {
		return self::fetchCallTypeChunks( sprintf( 'SELECT * FROM call_type_chunks WHERE scheduled_call_type_id = %d AND cid = %d', ( int ) $intScheduledCallTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTypeChunksByCallChunkTypeIdByCid( $intCallChunkTypeId, $intCid, $objDatabase ) {
		return self::fetchCallTypeChunks( sprintf( 'SELECT * FROM call_type_chunks WHERE call_chunk_type_id = %d AND cid = %d', ( int ) $intCallChunkTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTypeChunksByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCallTypeChunks( sprintf( 'SELECT * FROM call_type_chunks WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>