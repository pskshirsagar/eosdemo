<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItems extends CEosPluralBase {

    const TABLE_RENTABLE_ITEMS = 'public.rentable_items';

    public static function fetchRentableItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CRentableItem', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchRentableItem( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRentableItem', $objDatabase );
    }

    public static function fetchRentableItemCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'rentable_items', $objDatabase );
    }

    public static function fetchRentableItemByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchRentableItem( sprintf( 'SELECT * FROM rentable_items WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemsByCid( $intCid, $objDatabase ) {
        return self::fetchRentableItems( sprintf( 'SELECT * FROM rentable_items WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchRentableItems( sprintf( 'SELECT * FROM rentable_items WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
        return self::fetchRentableItems( sprintf( 'SELECT * FROM rentable_items WHERE unit_space_id = %d AND cid = %d', (int) $intUnitSpaceId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemsByRentableItemOptionIdByCid( $intRentableItemOptionId, $intCid, $objDatabase ) {
        return self::fetchRentableItems( sprintf( 'SELECT * FROM rentable_items WHERE rentable_item_option_id = %d AND cid = %d', (int) $intRentableItemOptionId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemsByUnitSpaceStatusTypeIdByCid( $intUnitSpaceStatusTypeId, $intCid, $objDatabase ) {
        return self::fetchRentableItems( sprintf( 'SELECT * FROM rentable_items WHERE unit_space_status_type_id = %d AND cid = %d', (int) $intUnitSpaceStatusTypeId, (int) $intCid ), $objDatabase );
    }

}
?>