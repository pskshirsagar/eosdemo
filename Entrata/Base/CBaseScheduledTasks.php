<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTasks
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTasks extends CEosPluralBase {

	/**
	 * @return CScheduledTask[]
	 */
	public static function fetchScheduledTasks( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScheduledTask::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledTask
	 */
	public static function fetchScheduledTask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledTask::class, $objDatabase );
	}

	public static function fetchScheduledTaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_tasks', $objDatabase );
	}

	public static function fetchScheduledTaskByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledTask( sprintf( 'SELECT * FROM scheduled_tasks WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByDefaultScheduledTaskIdByCid( $intDefaultScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE default_scheduled_task_id = %d AND cid = %d', $intDefaultScheduledTaskId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByScheduledTaskTypeIdByCid( $intScheduledTaskTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE scheduled_task_type_id = %d AND cid = %d', $intScheduledTaskTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByScheduleTypeIdByCid( $intScheduleTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE schedule_type_id = %d AND cid = %d', $intScheduleTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE scheduled_task_id = %d AND cid = %d', $intScheduledTaskId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByScheduledTaskGroupIdByCid( $intScheduledTaskGroupId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE scheduled_task_group_id = %d AND cid = %d', $intScheduledTaskGroupId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE event_type_id = %d AND cid = %d', $intEventTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledTasksByEventSubTypeIdByCid( $intEventSubTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTasks( sprintf( 'SELECT * FROM scheduled_tasks WHERE event_sub_type_id = %d AND cid = %d', $intEventSubTypeId, $intCid ), $objDatabase );
	}

}
?>