<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBuildingTypes
 * Do not add any new functions to this class.
 */

class CBaseBuildingTypes extends CEosPluralBase {

	/**
	 * @return CBuildingType[]
	 */
	public static function fetchBuildingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBuildingType::class, $objDatabase );
	}

	/**
	 * @return CBuildingType
	 */
	public static function fetchBuildingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBuildingType::class, $objDatabase );
	}

	public static function fetchBuildingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'building_types', $objDatabase );
	}

	public static function fetchBuildingTypeById( $intId, $objDatabase ) {
		return self::fetchBuildingType( sprintf( 'SELECT * FROM building_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>