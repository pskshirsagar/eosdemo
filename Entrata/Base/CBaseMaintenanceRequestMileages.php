<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestMileages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestMileages extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestMileage[]
	 */
	public static function fetchMaintenanceRequestMileages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestMileage::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestMileage
	 */
	public static function fetchMaintenanceRequestMileage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestMileage::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestMileageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_mileages', $objDatabase );
	}

	public static function fetchMaintenanceRequestMileageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileage( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMileagesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileages( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMileagesByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileages( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMileagesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileages( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMileagesByVehicleIdByCid( $intVehicleId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileages( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE vehicle_id = %d AND cid = %d', $intVehicleId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMileagesByInvoiceDetailIdByCid( $intInvoiceDetailId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMileages( sprintf( 'SELECT * FROM maintenance_request_mileages WHERE invoice_detail_id = %d AND cid = %d', $intInvoiceDetailId, $intCid ), $objDatabase );
	}

}
?>