<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportAccessLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.report_access_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReportId;
	protected $m_intReportVersionId;
	protected $m_intReportInstanceId;
	protected $m_intReportNewInstanceId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCompanyUserId;
	protected $m_strAccessedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['report_id'] ) && $boolDirectSet ) $this->set( 'm_intReportId', trim( $arrValues['report_id'] ) ); elseif( isset( $arrValues['report_id'] ) ) $this->setReportId( $arrValues['report_id'] );
		if( isset( $arrValues['report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intReportVersionId', trim( $arrValues['report_version_id'] ) ); elseif( isset( $arrValues['report_version_id'] ) ) $this->setReportVersionId( $arrValues['report_version_id'] );
		if( isset( $arrValues['report_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intReportInstanceId', trim( $arrValues['report_instance_id'] ) ); elseif( isset( $arrValues['report_instance_id'] ) ) $this->setReportInstanceId( $arrValues['report_instance_id'] );
		if( isset( $arrValues['report_new_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intReportNewInstanceId', trim( $arrValues['report_new_instance_id'] ) ); elseif( isset( $arrValues['report_new_instance_id'] ) ) $this->setReportNewInstanceId( $arrValues['report_new_instance_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['accessed_on'] ) && $boolDirectSet ) $this->set( 'm_strAccessedOn', trim( $arrValues['accessed_on'] ) ); elseif( isset( $arrValues['accessed_on'] ) ) $this->setAccessedOn( $arrValues['accessed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReportId( $intReportId ) {
		$this->set( 'm_intReportId', CStrings::strToIntDef( $intReportId, NULL, false ) );
	}

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function sqlReportId() {
		return ( true == isset( $this->m_intReportId ) ) ? ( string ) $this->m_intReportId : 'NULL';
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->set( 'm_intReportVersionId', CStrings::strToIntDef( $intReportVersionId, NULL, false ) );
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function sqlReportVersionId() {
		return ( true == isset( $this->m_intReportVersionId ) ) ? ( string ) $this->m_intReportVersionId : 'NULL';
	}

	public function setReportInstanceId( $intReportInstanceId ) {
		$this->set( 'm_intReportInstanceId', CStrings::strToIntDef( $intReportInstanceId, NULL, false ) );
	}

	public function getReportInstanceId() {
		return $this->m_intReportInstanceId;
	}

	public function sqlReportInstanceId() {
		return ( true == isset( $this->m_intReportInstanceId ) ) ? ( string ) $this->m_intReportInstanceId : 'NULL';
	}

	public function setReportNewInstanceId( $intReportNewInstanceId ) {
		$this->set( 'm_intReportNewInstanceId', CStrings::strToIntDef( $intReportNewInstanceId, NULL, false ) );
	}

	public function getReportNewInstanceId() {
		return $this->m_intReportNewInstanceId;
	}

	public function sqlReportNewInstanceId() {
		return ( true == isset( $this->m_intReportNewInstanceId ) ) ? ( string ) $this->m_intReportNewInstanceId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setAccessedOn( $strAccessedOn ) {
		$this->set( 'm_strAccessedOn', CStrings::strTrimDef( $strAccessedOn, -1, NULL, true ) );
	}

	public function getAccessedOn() {
		return $this->m_strAccessedOn;
	}

	public function sqlAccessedOn() {
		return ( true == isset( $this->m_strAccessedOn ) ) ? '\'' . $this->m_strAccessedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, report_id, report_version_id, report_instance_id, report_new_instance_id, details, company_user_id, accessed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReportId() . ', ' .
						$this->sqlReportVersionId() . ', ' .
						$this->sqlReportInstanceId() . ', ' .
						$this->sqlReportNewInstanceId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlAccessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_id = ' . $this->sqlReportId(). ',' ; } elseif( true == array_key_exists( 'ReportId', $this->getChangedColumns() ) ) { $strSql .= ' report_id = ' . $this->sqlReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId(). ',' ; } elseif( true == array_key_exists( 'ReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_instance_id = ' . $this->sqlReportInstanceId(). ',' ; } elseif( true == array_key_exists( 'ReportInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' report_instance_id = ' . $this->sqlReportInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId(). ',' ; } elseif( true == array_key_exists( 'ReportNewInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() ; } elseif( true == array_key_exists( 'AccessedOn', $this->getChangedColumns() ) ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'report_id' => $this->getReportId(),
			'report_version_id' => $this->getReportVersionId(),
			'report_instance_id' => $this->getReportInstanceId(),
			'report_new_instance_id' => $this->getReportNewInstanceId(),
			'details' => $this->getDetails(),
			'company_user_id' => $this->getCompanyUserId(),
			'accessed_on' => $this->getAccessedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>