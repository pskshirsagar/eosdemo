<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryVendors
 * Do not add any new functions to this class.
 */

class CBaseAncillaryVendors extends CEosPluralBase {

	/**
	 * @return CAncillaryVendor[]
	 */
	public static function fetchAncillaryVendors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAncillaryVendor::class, $objDatabase );
	}

	/**
	 * @return CAncillaryVendor
	 */
	public static function fetchAncillaryVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAncillaryVendor::class, $objDatabase );
	}

	public static function fetchAncillaryVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_vendors', $objDatabase );
	}

	public static function fetchAncillaryVendorById( $intId, $objDatabase ) {
		return self::fetchAncillaryVendor( sprintf( 'SELECT * FROM ancillary_vendors WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchAncillaryVendorsByAncillaryTypeId( $intAncillaryTypeId, $objDatabase ) {
		return self::fetchAncillaryVendors( sprintf( 'SELECT * FROM ancillary_vendors WHERE ancillary_type_id = %d', $intAncillaryTypeId ), $objDatabase );
	}

}
?>