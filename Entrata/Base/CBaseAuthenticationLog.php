<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAuthenticationLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.authentication_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intApplicantId;
	protected $m_intCompanyUserId;
	protected $m_intEmployeeId;
	protected $m_intApPayeeId;
	protected $m_intPsProductId;
	protected $m_intAuthenticationLogTypeId;
	protected $m_strPreviousPassword;
	protected $m_strLoginDatetime;
	protected $m_strLogoutDatetime;
	protected $m_strIpAddress;
	protected $m_strSessionId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['authentication_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAuthenticationLogTypeId', trim( $arrValues['authentication_log_type_id'] ) ); elseif( isset( $arrValues['authentication_log_type_id'] ) ) $this->setAuthenticationLogTypeId( $arrValues['authentication_log_type_id'] );
		if( isset( $arrValues['previous_password'] ) && $boolDirectSet ) $this->set( 'm_strPreviousPassword', trim( $arrValues['previous_password'] ) ); elseif( isset( $arrValues['previous_password'] ) ) $this->setPreviousPassword( $arrValues['previous_password'] );
		if( isset( $arrValues['login_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLoginDatetime', trim( $arrValues['login_datetime'] ) ); elseif( isset( $arrValues['login_datetime'] ) ) $this->setLoginDatetime( $arrValues['login_datetime'] );
		if( isset( $arrValues['logout_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogoutDatetime', trim( $arrValues['logout_datetime'] ) ); elseif( isset( $arrValues['logout_datetime'] ) ) $this->setLogoutDatetime( $arrValues['logout_datetime'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['session_id'] ) && $boolDirectSet ) $this->set( 'm_strSessionId', trim( $arrValues['session_id'] ) ); elseif( isset( $arrValues['session_id'] ) ) $this->setSessionId( $arrValues['session_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setAuthenticationLogTypeId( $intAuthenticationLogTypeId ) {
		$this->set( 'm_intAuthenticationLogTypeId', CStrings::strToIntDef( $intAuthenticationLogTypeId, NULL, false ) );
	}

	public function getAuthenticationLogTypeId() {
		return $this->m_intAuthenticationLogTypeId;
	}

	public function sqlAuthenticationLogTypeId() {
		return ( true == isset( $this->m_intAuthenticationLogTypeId ) ) ? ( string ) $this->m_intAuthenticationLogTypeId : 'NULL';
	}

	public function setPreviousPassword( $strPreviousPassword ) {
		$this->set( 'm_strPreviousPassword', CStrings::strTrimDef( $strPreviousPassword, 240, NULL, true ) );
	}

	public function getPreviousPassword() {
		return $this->m_strPreviousPassword;
	}

	public function sqlPreviousPassword() {
		return ( true == isset( $this->m_strPreviousPassword ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousPassword ) : '\'' . addslashes( $this->m_strPreviousPassword ) . '\'' ) : 'NULL';
	}

	public function setLoginDatetime( $strLoginDatetime ) {
		$this->set( 'm_strLoginDatetime', CStrings::strTrimDef( $strLoginDatetime, -1, NULL, true ) );
	}

	public function getLoginDatetime() {
		return $this->m_strLoginDatetime;
	}

	public function sqlLoginDatetime() {
		return ( true == isset( $this->m_strLoginDatetime ) ) ? '\'' . $this->m_strLoginDatetime . '\'' : 'NULL';
	}

	public function setLogoutDatetime( $strLogoutDatetime ) {
		$this->set( 'm_strLogoutDatetime', CStrings::strTrimDef( $strLogoutDatetime, -1, NULL, true ) );
	}

	public function getLogoutDatetime() {
		return $this->m_strLogoutDatetime;
	}

	public function sqlLogoutDatetime() {
		return ( true == isset( $this->m_strLogoutDatetime ) ) ? '\'' . $this->m_strLogoutDatetime . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setSessionId( $strSessionId ) {
		$this->set( 'm_strSessionId', CStrings::strTrimDef( $strSessionId, 50, NULL, true ) );
	}

	public function getSessionId() {
		return $this->m_strSessionId;
	}

	public function sqlSessionId() {
		return ( true == isset( $this->m_strSessionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSessionId ) : '\'' . addslashes( $this->m_strSessionId ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, applicant_id, company_user_id, employee_id, ap_payee_id, ps_product_id, authentication_log_type_id, previous_password, login_datetime, logout_datetime, ip_address, session_id, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlAuthenticationLogTypeId() . ', ' .
						$this->sqlPreviousPassword() . ', ' .
						$this->sqlLoginDatetime() . ', ' .
						$this->sqlLogoutDatetime() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlSessionId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' authentication_log_type_id = ' . $this->sqlAuthenticationLogTypeId(). ',' ; } elseif( true == array_key_exists( 'AuthenticationLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' authentication_log_type_id = ' . $this->sqlAuthenticationLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_password = ' . $this->sqlPreviousPassword(). ',' ; } elseif( true == array_key_exists( 'PreviousPassword', $this->getChangedColumns() ) ) { $strSql .= ' previous_password = ' . $this->sqlPreviousPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_datetime = ' . $this->sqlLoginDatetime(). ',' ; } elseif( true == array_key_exists( 'LoginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' login_datetime = ' . $this->sqlLoginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logout_datetime = ' . $this->sqlLogoutDatetime(). ',' ; } elseif( true == array_key_exists( 'LogoutDatetime', $this->getChangedColumns() ) ) { $strSql .= ' logout_datetime = ' . $this->sqlLogoutDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_id = ' . $this->sqlSessionId(). ',' ; } elseif( true == array_key_exists( 'SessionId', $this->getChangedColumns() ) ) { $strSql .= ' session_id = ' . $this->sqlSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'applicant_id' => $this->getApplicantId(),
			'company_user_id' => $this->getCompanyUserId(),
			'employee_id' => $this->getEmployeeId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ps_product_id' => $this->getPsProductId(),
			'authentication_log_type_id' => $this->getAuthenticationLogTypeId(),
			'previous_password' => $this->getPreviousPassword(),
			'login_datetime' => $this->getLoginDatetime(),
			'logout_datetime' => $this->getLogoutDatetime(),
			'ip_address' => $this->getIpAddress(),
			'session_id' => $this->getSessionId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>