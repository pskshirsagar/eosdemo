<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLateFeeFormulaCodes extends CEosPluralBase {

	/**
	 * @return CLateFeeFormulaCode[]
	 */
	public static function fetchLateFeeFormulaCodes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLateFeeFormulaCode', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLateFeeFormulaCode
	 */
	public static function fetchLateFeeFormulaCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLateFeeFormulaCode', $objDatabase );
	}

	public static function fetchLateFeeFormulaCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'late_fee_formula_codes', $objDatabase );
	}

	public static function fetchLateFeeFormulaCodeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLateFeeFormulaCode( sprintf( 'SELECT * FROM late_fee_formula_codes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLateFeeFormulaCodesByCid( $intCid, $objDatabase ) {
		return self::fetchLateFeeFormulaCodes( sprintf( 'SELECT * FROM late_fee_formula_codes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLateFeeFormulaCodesByLateFeeFormulaIdByCid( $intLateFeeFormulaId, $intCid, $objDatabase ) {
		return self::fetchLateFeeFormulaCodes( sprintf( 'SELECT * FROM late_fee_formula_codes WHERE late_fee_formula_id = %d AND cid = %d', ( int ) $intLateFeeFormulaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLateFeeFormulaCodesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchLateFeeFormulaCodes( sprintf( 'SELECT * FROM late_fee_formula_codes WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>