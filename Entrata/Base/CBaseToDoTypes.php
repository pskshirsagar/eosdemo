<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CToDoTypes
 * Do not add any new functions to this class.
 */

class CBaseToDoTypes extends CEosPluralBase {

	/**
	 * @return CToDoType[]
	 */
	public static function fetchToDoTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CToDoType::class, $objDatabase );
	}

	/**
	 * @return CToDoType
	 */
	public static function fetchToDoType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CToDoType::class, $objDatabase );
	}

	public static function fetchToDoTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'to_do_types', $objDatabase );
	}

	public static function fetchToDoTypeById( $intId, $objDatabase ) {
		return self::fetchToDoType( sprintf( 'SELECT * FROM to_do_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>