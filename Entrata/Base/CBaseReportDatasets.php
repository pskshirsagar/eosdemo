<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportDatasets
 * Do not add any new functions to this class.
 */

class CBaseReportDatasets extends CEosPluralBase {

	/**
	 * @return CReportDataset[]
	 */
	public static function fetchReportDatasets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportDataset::class, $objDatabase );
	}

	/**
	 * @return CReportDataset
	 */
	public static function fetchReportDataset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportDataset::class, $objDatabase );
	}

	public static function fetchReportDatasetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_datasets', $objDatabase );
	}

	public static function fetchReportDatasetById( $intId, $objDatabase ) {
		return self::fetchReportDataset( sprintf( 'SELECT * FROM report_datasets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportDatasetsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchReportDatasets( sprintf( 'SELECT * FROM report_datasets WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

	public static function fetchReportDatasetsByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		return self::fetchReportDatasets( sprintf( 'SELECT * FROM report_datasets WHERE default_report_version_id = %d', ( int ) $intDefaultReportVersionId ), $objDatabase );
	}

}
?>