<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledEmailFilter extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scheduled_email_filters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intScheduledEmailTypeId;
	protected $m_strBlockedRecipients;
	protected $m_strName;
	protected $m_strProperties;
	protected $m_strPropertyBuildings;
	protected $m_strUnitNumber;
	protected $m_strPropertyFloorplans;
	protected $m_strPropertyFloors;
	protected $m_strUnitTypes;
	protected $m_strLeasingAgents;
	protected $m_strLeadSources;
	protected $m_strLeaseIntervalTypes;
	protected $m_strCustomerTypes;
	protected $m_strProducts;
	protected $m_strStatusTypes;
	protected $m_strSubStatusTypes;
	protected $m_strDesiredBedrooms;
	protected $m_strDesiredBathrooms;
	protected $m_strPetTypes;
	protected $m_strEventTypes;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strMoveInDateMin;
	protected $m_strMoveInDateMax;
	protected $m_strMinMoveOutDate;
	protected $m_strMaxMoveOutDate;
	protected $m_strLeaseExpirationDateMin;
	protected $m_strLeaseExpirationDateMax;
	protected $m_strLeaseRenewalDateMin;
	protected $m_strLeaseRenewalDateMax;
	protected $m_strApplicationCreatedOnDateMin;
	protected $m_strApplicationCreatedOnDateMax;
	protected $m_fltBalanceDueMin;
	protected $m_fltBalanceDueMax;
	protected $m_fltCreditBalanceMin;
	protected $m_fltCreditBalanceMax;
	protected $m_fltDesiredRentMin;
	protected $m_fltDesiredRentMax;
	protected $m_strCustomerVehicle;
	protected $m_intDelinquencies;
	protected $m_strApplicationTypes;
	protected $m_intDaysCreated;
	protected $m_intIsRenterInsurance;
	protected $m_intIsBalanceDue;
	protected $m_intIsResidentPortalUsername;
	protected $m_intIsVehiclePermitNumber;
	protected $m_intIsShared;
	protected $m_intIsPublished;
	protected $m_boolIsDedupe;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsBalanceDue = '0';
		$this->m_intIsShared = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolIsDedupe = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['scheduled_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailTypeId', trim( $arrValues['scheduled_email_type_id'] ) ); elseif( isset( $arrValues['scheduled_email_type_id'] ) ) $this->setScheduledEmailTypeId( $arrValues['scheduled_email_type_id'] );
		if( isset( $arrValues['blocked_recipients'] ) && $boolDirectSet ) $this->set( 'm_strBlockedRecipients', trim( stripcslashes( $arrValues['blocked_recipients'] ) ) ); elseif( isset( $arrValues['blocked_recipients'] ) ) $this->setBlockedRecipients( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['blocked_recipients'] ) : $arrValues['blocked_recipients'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['properties'] ) && $boolDirectSet ) $this->set( 'm_strProperties', trim( stripcslashes( $arrValues['properties'] ) ) ); elseif( isset( $arrValues['properties'] ) ) $this->setProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['properties'] ) : $arrValues['properties'] );
		if( isset( $arrValues['property_buildings'] ) && $boolDirectSet ) $this->set( 'm_strPropertyBuildings', trim( stripcslashes( $arrValues['property_buildings'] ) ) ); elseif( isset( $arrValues['property_buildings'] ) ) $this->setPropertyBuildings( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_buildings'] ) : $arrValues['property_buildings'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['property_floorplans'] ) && $boolDirectSet ) $this->set( 'm_strPropertyFloorplans', trim( stripcslashes( $arrValues['property_floorplans'] ) ) ); elseif( isset( $arrValues['property_floorplans'] ) ) $this->setPropertyFloorplans( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_floorplans'] ) : $arrValues['property_floorplans'] );
		if( isset( $arrValues['property_floors'] ) && $boolDirectSet ) $this->set( 'm_strPropertyFloors', trim( stripcslashes( $arrValues['property_floors'] ) ) ); elseif( isset( $arrValues['property_floors'] ) ) $this->setPropertyFloors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_floors'] ) : $arrValues['property_floors'] );
		if( isset( $arrValues['unit_types'] ) && $boolDirectSet ) $this->set( 'm_strUnitTypes', trim( stripcslashes( $arrValues['unit_types'] ) ) ); elseif( isset( $arrValues['unit_types'] ) ) $this->setUnitTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_types'] ) : $arrValues['unit_types'] );
		if( isset( $arrValues['leasing_agents'] ) && $boolDirectSet ) $this->set( 'm_strLeasingAgents', trim( stripcslashes( $arrValues['leasing_agents'] ) ) ); elseif( isset( $arrValues['leasing_agents'] ) ) $this->setLeasingAgents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leasing_agents'] ) : $arrValues['leasing_agents'] );
		if( isset( $arrValues['lead_sources'] ) && $boolDirectSet ) $this->set( 'm_strLeadSources', trim( stripcslashes( $arrValues['lead_sources'] ) ) ); elseif( isset( $arrValues['lead_sources'] ) ) $this->setLeadSources( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_sources'] ) : $arrValues['lead_sources'] );
		if( isset( $arrValues['lease_interval_types'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalTypes', trim( stripcslashes( $arrValues['lease_interval_types'] ) ) ); elseif( isset( $arrValues['lease_interval_types'] ) ) $this->setLeaseIntervalTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_interval_types'] ) : $arrValues['lease_interval_types'] );
		if( isset( $arrValues['customer_types'] ) && $boolDirectSet ) $this->set( 'm_strCustomerTypes', trim( stripcslashes( $arrValues['customer_types'] ) ) ); elseif( isset( $arrValues['customer_types'] ) ) $this->setCustomerTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_types'] ) : $arrValues['customer_types'] );
		if( isset( $arrValues['products'] ) && $boolDirectSet ) $this->set( 'm_strProducts', trim( stripcslashes( $arrValues['products'] ) ) ); elseif( isset( $arrValues['products'] ) ) $this->setProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['products'] ) : $arrValues['products'] );
		if( isset( $arrValues['status_types'] ) && $boolDirectSet ) $this->set( 'm_strStatusTypes', trim( stripcslashes( $arrValues['status_types'] ) ) ); elseif( isset( $arrValues['status_types'] ) ) $this->setStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_types'] ) : $arrValues['status_types'] );
		if( isset( $arrValues['sub_status_types'] ) && $boolDirectSet ) $this->set( 'm_strSubStatusTypes', trim( stripcslashes( $arrValues['sub_status_types'] ) ) ); elseif( isset( $arrValues['sub_status_types'] ) ) $this->setSubStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sub_status_types'] ) : $arrValues['sub_status_types'] );
		if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_strDesiredBedrooms', trim( stripcslashes( $arrValues['desired_bedrooms'] ) ) ); elseif( isset( $arrValues['desired_bedrooms'] ) ) $this->setDesiredBedrooms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bedrooms'] ) : $arrValues['desired_bedrooms'] );
		if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_strDesiredBathrooms', trim( stripcslashes( $arrValues['desired_bathrooms'] ) ) ); elseif( isset( $arrValues['desired_bathrooms'] ) ) $this->setDesiredBathrooms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bathrooms'] ) : $arrValues['desired_bathrooms'] );
		if( isset( $arrValues['pet_types'] ) && $boolDirectSet ) $this->set( 'm_strPetTypes', trim( stripcslashes( $arrValues['pet_types'] ) ) ); elseif( isset( $arrValues['pet_types'] ) ) $this->setPetTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pet_types'] ) : $arrValues['pet_types'] );
		if( isset( $arrValues['event_types'] ) && $boolDirectSet ) $this->set( 'm_strEventTypes', trim( stripcslashes( $arrValues['event_types'] ) ) ); elseif( isset( $arrValues['event_types'] ) ) $this->setEventTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_types'] ) : $arrValues['event_types'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['move_in_date_min'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDateMin', trim( $arrValues['move_in_date_min'] ) ); elseif( isset( $arrValues['move_in_date_min'] ) ) $this->setMoveInDateMin( $arrValues['move_in_date_min'] );
		if( isset( $arrValues['move_in_date_max'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDateMax', trim( $arrValues['move_in_date_max'] ) ); elseif( isset( $arrValues['move_in_date_max'] ) ) $this->setMoveInDateMax( $arrValues['move_in_date_max'] );
		if( isset( $arrValues['min_move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMinMoveOutDate', trim( $arrValues['min_move_out_date'] ) ); elseif( isset( $arrValues['min_move_out_date'] ) ) $this->setMinMoveOutDate( $arrValues['min_move_out_date'] );
		if( isset( $arrValues['max_move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMaxMoveOutDate', trim( $arrValues['max_move_out_date'] ) ); elseif( isset( $arrValues['max_move_out_date'] ) ) $this->setMaxMoveOutDate( $arrValues['max_move_out_date'] );
		if( isset( $arrValues['lease_expiration_date_min'] ) && $boolDirectSet ) $this->set( 'm_strLeaseExpirationDateMin', trim( $arrValues['lease_expiration_date_min'] ) ); elseif( isset( $arrValues['lease_expiration_date_min'] ) ) $this->setLeaseExpirationDateMin( $arrValues['lease_expiration_date_min'] );
		if( isset( $arrValues['lease_expiration_date_max'] ) && $boolDirectSet ) $this->set( 'm_strLeaseExpirationDateMax', trim( $arrValues['lease_expiration_date_max'] ) ); elseif( isset( $arrValues['lease_expiration_date_max'] ) ) $this->setLeaseExpirationDateMax( $arrValues['lease_expiration_date_max'] );
		if( isset( $arrValues['lease_renewal_date_min'] ) && $boolDirectSet ) $this->set( 'm_strLeaseRenewalDateMin', trim( $arrValues['lease_renewal_date_min'] ) ); elseif( isset( $arrValues['lease_renewal_date_min'] ) ) $this->setLeaseRenewalDateMin( $arrValues['lease_renewal_date_min'] );
		if( isset( $arrValues['lease_renewal_date_max'] ) && $boolDirectSet ) $this->set( 'm_strLeaseRenewalDateMax', trim( $arrValues['lease_renewal_date_max'] ) ); elseif( isset( $arrValues['lease_renewal_date_max'] ) ) $this->setLeaseRenewalDateMax( $arrValues['lease_renewal_date_max'] );
		if( isset( $arrValues['application_created_on_date_min'] ) && $boolDirectSet ) $this->set( 'm_strApplicationCreatedOnDateMin', trim( $arrValues['application_created_on_date_min'] ) ); elseif( isset( $arrValues['application_created_on_date_min'] ) ) $this->setApplicationCreatedOnDateMin( $arrValues['application_created_on_date_min'] );
		if( isset( $arrValues['application_created_on_date_max'] ) && $boolDirectSet ) $this->set( 'm_strApplicationCreatedOnDateMax', trim( $arrValues['application_created_on_date_max'] ) ); elseif( isset( $arrValues['application_created_on_date_max'] ) ) $this->setApplicationCreatedOnDateMax( $arrValues['application_created_on_date_max'] );
		if( isset( $arrValues['balance_due_min'] ) && $boolDirectSet ) $this->set( 'm_fltBalanceDueMin', trim( $arrValues['balance_due_min'] ) ); elseif( isset( $arrValues['balance_due_min'] ) ) $this->setBalanceDueMin( $arrValues['balance_due_min'] );
		if( isset( $arrValues['balance_due_max'] ) && $boolDirectSet ) $this->set( 'm_fltBalanceDueMax', trim( $arrValues['balance_due_max'] ) ); elseif( isset( $arrValues['balance_due_max'] ) ) $this->setBalanceDueMax( $arrValues['balance_due_max'] );
		if( isset( $arrValues['credit_balance_min'] ) && $boolDirectSet ) $this->set( 'm_fltCreditBalanceMin', trim( $arrValues['credit_balance_min'] ) ); elseif( isset( $arrValues['credit_balance_min'] ) ) $this->setCreditBalanceMin( $arrValues['credit_balance_min'] );
		if( isset( $arrValues['credit_balance_max'] ) && $boolDirectSet ) $this->set( 'm_fltCreditBalanceMax', trim( $arrValues['credit_balance_max'] ) ); elseif( isset( $arrValues['credit_balance_max'] ) ) $this->setCreditBalanceMax( $arrValues['credit_balance_max'] );
		if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMin', trim( $arrValues['desired_rent_min'] ) ); elseif( isset( $arrValues['desired_rent_min'] ) ) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
		if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMax', trim( $arrValues['desired_rent_max'] ) ); elseif( isset( $arrValues['desired_rent_max'] ) ) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
		if( isset( $arrValues['customer_vehicle'] ) && $boolDirectSet ) $this->set( 'm_strCustomerVehicle', trim( stripcslashes( $arrValues['customer_vehicle'] ) ) ); elseif( isset( $arrValues['customer_vehicle'] ) ) $this->setCustomerVehicle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_vehicle'] ) : $arrValues['customer_vehicle'] );
		if( isset( $arrValues['delinquencies'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencies', trim( $arrValues['delinquencies'] ) ); elseif( isset( $arrValues['delinquencies'] ) ) $this->setDelinquencies( $arrValues['delinquencies'] );
		if( isset( $arrValues['application_types'] ) && $boolDirectSet ) $this->set( 'm_strApplicationTypes', trim( stripcslashes( $arrValues['application_types'] ) ) ); elseif( isset( $arrValues['application_types'] ) ) $this->setApplicationTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_types'] ) : $arrValues['application_types'] );
		if( isset( $arrValues['days_created'] ) && $boolDirectSet ) $this->set( 'm_intDaysCreated', trim( $arrValues['days_created'] ) ); elseif( isset( $arrValues['days_created'] ) ) $this->setDaysCreated( $arrValues['days_created'] );
		if( isset( $arrValues['is_renter_insurance'] ) && $boolDirectSet ) $this->set( 'm_intIsRenterInsurance', trim( $arrValues['is_renter_insurance'] ) ); elseif( isset( $arrValues['is_renter_insurance'] ) ) $this->setIsRenterInsurance( $arrValues['is_renter_insurance'] );
		if( isset( $arrValues['is_balance_due'] ) && $boolDirectSet ) $this->set( 'm_intIsBalanceDue', trim( $arrValues['is_balance_due'] ) ); elseif( isset( $arrValues['is_balance_due'] ) ) $this->setIsBalanceDue( $arrValues['is_balance_due'] );
		if( isset( $arrValues['is_resident_portal_username'] ) && $boolDirectSet ) $this->set( 'm_intIsResidentPortalUsername', trim( $arrValues['is_resident_portal_username'] ) ); elseif( isset( $arrValues['is_resident_portal_username'] ) ) $this->setIsResidentPortalUsername( $arrValues['is_resident_portal_username'] );
		if( isset( $arrValues['is_vehicle_permit_number'] ) && $boolDirectSet ) $this->set( 'm_intIsVehiclePermitNumber', trim( $arrValues['is_vehicle_permit_number'] ) ); elseif( isset( $arrValues['is_vehicle_permit_number'] ) ) $this->setIsVehiclePermitNumber( $arrValues['is_vehicle_permit_number'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_intIsShared', trim( $arrValues['is_shared'] ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( $arrValues['is_shared'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_dedupe'] ) && $boolDirectSet ) $this->set( 'm_boolIsDedupe', trim( stripcslashes( $arrValues['is_dedupe'] ) ) ); elseif( isset( $arrValues['is_dedupe'] ) ) $this->setIsDedupe( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dedupe'] ) : $arrValues['is_dedupe'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setScheduledEmailTypeId( $intScheduledEmailTypeId ) {
		$this->set( 'm_intScheduledEmailTypeId', CStrings::strToIntDef( $intScheduledEmailTypeId, NULL, false ) );
	}

	public function getScheduledEmailTypeId() {
		return $this->m_intScheduledEmailTypeId;
	}

	public function sqlScheduledEmailTypeId() {
		return ( true == isset( $this->m_intScheduledEmailTypeId ) ) ? ( string ) $this->m_intScheduledEmailTypeId : 'NULL';
	}

	public function setBlockedRecipients( $strBlockedRecipients ) {
		$this->set( 'm_strBlockedRecipients', CStrings::strTrimDef( $strBlockedRecipients, -1, NULL, true ) );
	}

	public function getBlockedRecipients() {
		return $this->m_strBlockedRecipients;
	}

	public function sqlBlockedRecipients() {
		return ( true == isset( $this->m_strBlockedRecipients ) ) ? '\'' . addslashes( $this->m_strBlockedRecipients ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 200, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setProperties( $strProperties ) {
		$this->set( 'm_strProperties', CStrings::strTrimDef( $strProperties, -1, NULL, true ) );
	}

	public function getProperties() {
		return $this->m_strProperties;
	}

	public function sqlProperties() {
		return ( true == isset( $this->m_strProperties ) ) ? '\'' . addslashes( $this->m_strProperties ) . '\'' : 'NULL';
	}

	public function setPropertyBuildings( $strPropertyBuildings ) {
		$this->set( 'm_strPropertyBuildings', CStrings::strTrimDef( $strPropertyBuildings, -1, NULL, true ) );
	}

	public function getPropertyBuildings() {
		return $this->m_strPropertyBuildings;
	}

	public function sqlPropertyBuildings() {
		return ( true == isset( $this->m_strPropertyBuildings ) ) ? '\'' . addslashes( $this->m_strPropertyBuildings ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, -1, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setPropertyFloorplans( $strPropertyFloorplans ) {
		$this->set( 'm_strPropertyFloorplans', CStrings::strTrimDef( $strPropertyFloorplans, -1, NULL, true ) );
	}

	public function getPropertyFloorplans() {
		return $this->m_strPropertyFloorplans;
	}

	public function sqlPropertyFloorplans() {
		return ( true == isset( $this->m_strPropertyFloorplans ) ) ? '\'' . addslashes( $this->m_strPropertyFloorplans ) . '\'' : 'NULL';
	}

	public function setPropertyFloors( $strPropertyFloors ) {
		$this->set( 'm_strPropertyFloors', CStrings::strTrimDef( $strPropertyFloors, -1, NULL, true ) );
	}

	public function getPropertyFloors() {
		return $this->m_strPropertyFloors;
	}

	public function sqlPropertyFloors() {
		return ( true == isset( $this->m_strPropertyFloors ) ) ? '\'' . addslashes( $this->m_strPropertyFloors ) . '\'' : 'NULL';
	}

	public function setUnitTypes( $strUnitTypes ) {
		$this->set( 'm_strUnitTypes', CStrings::strTrimDef( $strUnitTypes, -1, NULL, true ) );
	}

	public function getUnitTypes() {
		return $this->m_strUnitTypes;
	}

	public function sqlUnitTypes() {
		return ( true == isset( $this->m_strUnitTypes ) ) ? '\'' . addslashes( $this->m_strUnitTypes ) . '\'' : 'NULL';
	}

	public function setLeasingAgents( $strLeasingAgents ) {
		$this->set( 'm_strLeasingAgents', CStrings::strTrimDef( $strLeasingAgents, -1, NULL, true ) );
	}

	public function getLeasingAgents() {
		return $this->m_strLeasingAgents;
	}

	public function sqlLeasingAgents() {
		return ( true == isset( $this->m_strLeasingAgents ) ) ? '\'' . addslashes( $this->m_strLeasingAgents ) . '\'' : 'NULL';
	}

	public function setLeadSources( $strLeadSources ) {
		$this->set( 'm_strLeadSources', CStrings::strTrimDef( $strLeadSources, -1, NULL, true ) );
	}

	public function getLeadSources() {
		return $this->m_strLeadSources;
	}

	public function sqlLeadSources() {
		return ( true == isset( $this->m_strLeadSources ) ) ? '\'' . addslashes( $this->m_strLeadSources ) . '\'' : 'NULL';
	}

	public function setLeaseIntervalTypes( $strLeaseIntervalTypes ) {
		$this->set( 'm_strLeaseIntervalTypes', CStrings::strTrimDef( $strLeaseIntervalTypes, -1, NULL, true ) );
	}

	public function getLeaseIntervalTypes() {
		return $this->m_strLeaseIntervalTypes;
	}

	public function sqlLeaseIntervalTypes() {
		return ( true == isset( $this->m_strLeaseIntervalTypes ) ) ? '\'' . addslashes( $this->m_strLeaseIntervalTypes ) . '\'' : 'NULL';
	}

	public function setCustomerTypes( $strCustomerTypes ) {
		$this->set( 'm_strCustomerTypes', CStrings::strTrimDef( $strCustomerTypes, -1, NULL, true ) );
	}

	public function getCustomerTypes() {
		return $this->m_strCustomerTypes;
	}

	public function sqlCustomerTypes() {
		return ( true == isset( $this->m_strCustomerTypes ) ) ? '\'' . addslashes( $this->m_strCustomerTypes ) . '\'' : 'NULL';
	}

	public function setProducts( $strProducts ) {
		$this->set( 'm_strProducts', CStrings::strTrimDef( $strProducts, -1, NULL, true ) );
	}

	public function getProducts() {
		return $this->m_strProducts;
	}

	public function sqlProducts() {
		return ( true == isset( $this->m_strProducts ) ) ? '\'' . addslashes( $this->m_strProducts ) . '\'' : 'NULL';
	}

	public function setStatusTypes( $strStatusTypes ) {
		$this->set( 'm_strStatusTypes', CStrings::strTrimDef( $strStatusTypes, -1, NULL, true ) );
	}

	public function getStatusTypes() {
		return $this->m_strStatusTypes;
	}

	public function sqlStatusTypes() {
		return ( true == isset( $this->m_strStatusTypes ) ) ? '\'' . addslashes( $this->m_strStatusTypes ) . '\'' : 'NULL';
	}

	public function setSubStatusTypes( $strSubStatusTypes ) {
		$this->set( 'm_strSubStatusTypes', CStrings::strTrimDef( $strSubStatusTypes, -1, NULL, true ) );
	}

	public function getSubStatusTypes() {
		return $this->m_strSubStatusTypes;
	}

	public function sqlSubStatusTypes() {
		return ( true == isset( $this->m_strSubStatusTypes ) ) ? '\'' . addslashes( $this->m_strSubStatusTypes ) . '\'' : 'NULL';
	}

	public function setDesiredBedrooms( $strDesiredBedrooms ) {
		$this->set( 'm_strDesiredBedrooms', CStrings::strTrimDef( $strDesiredBedrooms, -1, NULL, true ) );
	}

	public function getDesiredBedrooms() {
		return $this->m_strDesiredBedrooms;
	}

	public function sqlDesiredBedrooms() {
		return ( true == isset( $this->m_strDesiredBedrooms ) ) ? '\'' . addslashes( $this->m_strDesiredBedrooms ) . '\'' : 'NULL';
	}

	public function setDesiredBathrooms( $strDesiredBathrooms ) {
		$this->set( 'm_strDesiredBathrooms', CStrings::strTrimDef( $strDesiredBathrooms, -1, NULL, true ) );
	}

	public function getDesiredBathrooms() {
		return $this->m_strDesiredBathrooms;
	}

	public function sqlDesiredBathrooms() {
		return ( true == isset( $this->m_strDesiredBathrooms ) ) ? '\'' . addslashes( $this->m_strDesiredBathrooms ) . '\'' : 'NULL';
	}

	public function setPetTypes( $strPetTypes ) {
		$this->set( 'm_strPetTypes', CStrings::strTrimDef( $strPetTypes, -1, NULL, true ) );
	}

	public function getPetTypes() {
		return $this->m_strPetTypes;
	}

	public function sqlPetTypes() {
		return ( true == isset( $this->m_strPetTypes ) ) ? '\'' . addslashes( $this->m_strPetTypes ) . '\'' : 'NULL';
	}

	public function setEventTypes( $strEventTypes ) {
		$this->set( 'm_strEventTypes', CStrings::strTrimDef( $strEventTypes, -1, NULL, true ) );
	}

	public function getEventTypes() {
		return $this->m_strEventTypes;
	}

	public function sqlEventTypes() {
		return ( true == isset( $this->m_strEventTypes ) ) ? '\'' . addslashes( $this->m_strEventTypes ) . '\'' : 'NULL';
	}

	public function setMoveInDateMin( $strMoveInDateMin ) {
		$this->set( 'm_strMoveInDateMin', CStrings::strTrimDef( $strMoveInDateMin, -1, NULL, true ) );
	}

	public function getMoveInDateMin() {
		return $this->m_strMoveInDateMin;
	}

	public function sqlMoveInDateMin() {
		return ( true == isset( $this->m_strMoveInDateMin ) ) ? '\'' . $this->m_strMoveInDateMin . '\'' : 'NULL';
	}

	public function setMoveInDateMax( $strMoveInDateMax ) {
		$this->set( 'm_strMoveInDateMax', CStrings::strTrimDef( $strMoveInDateMax, -1, NULL, true ) );
	}

	public function getMoveInDateMax() {
		return $this->m_strMoveInDateMax;
	}

	public function sqlMoveInDateMax() {
		return ( true == isset( $this->m_strMoveInDateMax ) ) ? '\'' . $this->m_strMoveInDateMax . '\'' : 'NULL';
	}

	public function setMinMoveOutDate( $strMinMoveOutDate ) {
		$this->set( 'm_strMinMoveOutDate', CStrings::strTrimDef( $strMinMoveOutDate, -1, NULL, true ) );
	}

	public function getMinMoveOutDate() {
		return $this->m_strMinMoveOutDate;
	}

	public function sqlMinMoveOutDate() {
		return ( true == isset( $this->m_strMinMoveOutDate ) ) ? '\'' . $this->m_strMinMoveOutDate . '\'' : 'NULL';
	}

	public function setMaxMoveOutDate( $strMaxMoveOutDate ) {
		$this->set( 'm_strMaxMoveOutDate', CStrings::strTrimDef( $strMaxMoveOutDate, -1, NULL, true ) );
	}

	public function getMaxMoveOutDate() {
		return $this->m_strMaxMoveOutDate;
	}

	public function sqlMaxMoveOutDate() {
		return ( true == isset( $this->m_strMaxMoveOutDate ) ) ? '\'' . $this->m_strMaxMoveOutDate . '\'' : 'NULL';
	}

	public function setLeaseExpirationDateMin( $strLeaseExpirationDateMin ) {
		$this->set( 'm_strLeaseExpirationDateMin', CStrings::strTrimDef( $strLeaseExpirationDateMin, -1, NULL, true ) );
	}

	public function getLeaseExpirationDateMin() {
		return $this->m_strLeaseExpirationDateMin;
	}

	public function sqlLeaseExpirationDateMin() {
		return ( true == isset( $this->m_strLeaseExpirationDateMin ) ) ? '\'' . $this->m_strLeaseExpirationDateMin . '\'' : 'NULL';
	}

	public function setLeaseExpirationDateMax( $strLeaseExpirationDateMax ) {
		$this->set( 'm_strLeaseExpirationDateMax', CStrings::strTrimDef( $strLeaseExpirationDateMax, -1, NULL, true ) );
	}

	public function getLeaseExpirationDateMax() {
		return $this->m_strLeaseExpirationDateMax;
	}

	public function sqlLeaseExpirationDateMax() {
		return ( true == isset( $this->m_strLeaseExpirationDateMax ) ) ? '\'' . $this->m_strLeaseExpirationDateMax . '\'' : 'NULL';
	}

	public function setLeaseRenewalDateMin( $strLeaseRenewalDateMin ) {
		$this->set( 'm_strLeaseRenewalDateMin', CStrings::strTrimDef( $strLeaseRenewalDateMin, -1, NULL, true ) );
	}

	public function getLeaseRenewalDateMin() {
		return $this->m_strLeaseRenewalDateMin;
	}

	public function sqlLeaseRenewalDateMin() {
		return ( true == isset( $this->m_strLeaseRenewalDateMin ) ) ? '\'' . $this->m_strLeaseRenewalDateMin . '\'' : 'NULL';
	}

	public function setLeaseRenewalDateMax( $strLeaseRenewalDateMax ) {
		$this->set( 'm_strLeaseRenewalDateMax', CStrings::strTrimDef( $strLeaseRenewalDateMax, -1, NULL, true ) );
	}

	public function getLeaseRenewalDateMax() {
		return $this->m_strLeaseRenewalDateMax;
	}

	public function sqlLeaseRenewalDateMax() {
		return ( true == isset( $this->m_strLeaseRenewalDateMax ) ) ? '\'' . $this->m_strLeaseRenewalDateMax . '\'' : 'NULL';
	}

	public function setApplicationCreatedOnDateMin( $strApplicationCreatedOnDateMin ) {
		$this->set( 'm_strApplicationCreatedOnDateMin', CStrings::strTrimDef( $strApplicationCreatedOnDateMin, -1, NULL, true ) );
	}

	public function getApplicationCreatedOnDateMin() {
		return $this->m_strApplicationCreatedOnDateMin;
	}

	public function sqlApplicationCreatedOnDateMin() {
		return ( true == isset( $this->m_strApplicationCreatedOnDateMin ) ) ? '\'' . $this->m_strApplicationCreatedOnDateMin . '\'' : 'NULL';
	}

	public function setApplicationCreatedOnDateMax( $strApplicationCreatedOnDateMax ) {
		$this->set( 'm_strApplicationCreatedOnDateMax', CStrings::strTrimDef( $strApplicationCreatedOnDateMax, -1, NULL, true ) );
	}

	public function getApplicationCreatedOnDateMax() {
		return $this->m_strApplicationCreatedOnDateMax;
	}

	public function sqlApplicationCreatedOnDateMax() {
		return ( true == isset( $this->m_strApplicationCreatedOnDateMax ) ) ? '\'' . $this->m_strApplicationCreatedOnDateMax . '\'' : 'NULL';
	}

	public function setBalanceDueMin( $fltBalanceDueMin ) {
		$this->set( 'm_fltBalanceDueMin', CStrings::strToFloatDef( $fltBalanceDueMin, NULL, false, 2 ) );
	}

	public function getBalanceDueMin() {
		return $this->m_fltBalanceDueMin;
	}

	public function sqlBalanceDueMin() {
		return ( true == isset( $this->m_fltBalanceDueMin ) ) ? ( string ) $this->m_fltBalanceDueMin : 'NULL';
	}

	public function setBalanceDueMax( $fltBalanceDueMax ) {
		$this->set( 'm_fltBalanceDueMax', CStrings::strToFloatDef( $fltBalanceDueMax, NULL, false, 2 ) );
	}

	public function getBalanceDueMax() {
		return $this->m_fltBalanceDueMax;
	}

	public function sqlBalanceDueMax() {
		return ( true == isset( $this->m_fltBalanceDueMax ) ) ? ( string ) $this->m_fltBalanceDueMax : 'NULL';
	}

	public function setCreditBalanceMin( $fltCreditBalanceMin ) {
		$this->set( 'm_fltCreditBalanceMin', CStrings::strToFloatDef( $fltCreditBalanceMin, NULL, false, 2 ) );
	}

	public function getCreditBalanceMin() {
		return $this->m_fltCreditBalanceMin;
	}

	public function sqlCreditBalanceMin() {
		return ( true == isset( $this->m_fltCreditBalanceMin ) ) ? ( string ) $this->m_fltCreditBalanceMin : 'NULL';
	}

	public function setCreditBalanceMax( $fltCreditBalanceMax ) {
		$this->set( 'm_fltCreditBalanceMax', CStrings::strToFloatDef( $fltCreditBalanceMax, NULL, false, 2 ) );
	}

	public function getCreditBalanceMax() {
		return $this->m_fltCreditBalanceMax;
	}

	public function sqlCreditBalanceMax() {
		return ( true == isset( $this->m_fltCreditBalanceMax ) ) ? ( string ) $this->m_fltCreditBalanceMax : 'NULL';
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		$this->set( 'm_fltDesiredRentMin', CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2 ) );
	}

	public function getDesiredRentMin() {
		return $this->m_fltDesiredRentMin;
	}

	public function sqlDesiredRentMin() {
		return ( true == isset( $this->m_fltDesiredRentMin ) ) ? ( string ) $this->m_fltDesiredRentMin : 'NULL';
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		$this->set( 'm_fltDesiredRentMax', CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2 ) );
	}

	public function getDesiredRentMax() {
		return $this->m_fltDesiredRentMax;
	}

	public function sqlDesiredRentMax() {
		return ( true == isset( $this->m_fltDesiredRentMax ) ) ? ( string ) $this->m_fltDesiredRentMax : 'NULL';
	}

	public function setCustomerVehicle( $strCustomerVehicle ) {
		$this->set( 'm_strCustomerVehicle', CStrings::strTrimDef( $strCustomerVehicle, 50, NULL, true ) );
	}

	public function getCustomerVehicle() {
		return $this->m_strCustomerVehicle;
	}

	public function sqlCustomerVehicle() {
		return ( true == isset( $this->m_strCustomerVehicle ) ) ? '\'' . addslashes( $this->m_strCustomerVehicle ) . '\'' : 'NULL';
	}

	public function setDelinquencies( $intDelinquencies ) {
		$this->set( 'm_intDelinquencies', CStrings::strToIntDef( $intDelinquencies, NULL, false ) );
	}

	public function getDelinquencies() {
		return $this->m_intDelinquencies;
	}

	public function sqlDelinquencies() {
		return ( true == isset( $this->m_intDelinquencies ) ) ? ( string ) $this->m_intDelinquencies : 'NULL';
	}

	public function setApplicationTypes( $strApplicationTypes ) {
		$this->set( 'm_strApplicationTypes', CStrings::strTrimDef( $strApplicationTypes, -1, NULL, true ) );
	}

	public function getApplicationTypes() {
		return $this->m_strApplicationTypes;
	}

	public function sqlApplicationTypes() {
		return ( true == isset( $this->m_strApplicationTypes ) ) ? '\'' . addslashes( $this->m_strApplicationTypes ) . '\'' : 'NULL';
	}

	public function setDaysCreated( $intDaysCreated ) {
		$this->set( 'm_intDaysCreated', CStrings::strToIntDef( $intDaysCreated, NULL, false ) );
	}

	public function getDaysCreated() {
		return $this->m_intDaysCreated;
	}

	public function sqlDaysCreated() {
		return ( true == isset( $this->m_intDaysCreated ) ) ? ( string ) $this->m_intDaysCreated : 'NULL';
	}

	public function setIsRenterInsurance( $intIsRenterInsurance ) {
		$this->set( 'm_intIsRenterInsurance', CStrings::strToIntDef( $intIsRenterInsurance, NULL, false ) );
	}

	public function getIsRenterInsurance() {
		return $this->m_intIsRenterInsurance;
	}

	public function sqlIsRenterInsurance() {
		return ( true == isset( $this->m_intIsRenterInsurance ) ) ? ( string ) $this->m_intIsRenterInsurance : 'NULL';
	}

	public function setIsBalanceDue( $intIsBalanceDue ) {
		$this->set( 'm_intIsBalanceDue', CStrings::strToIntDef( $intIsBalanceDue, NULL, false ) );
	}

	public function getIsBalanceDue() {
		return $this->m_intIsBalanceDue;
	}

	public function sqlIsBalanceDue() {
		return ( true == isset( $this->m_intIsBalanceDue ) ) ? ( string ) $this->m_intIsBalanceDue : '0';
	}

	public function setIsResidentPortalUsername( $intIsResidentPortalUsername ) {
		$this->set( 'm_intIsResidentPortalUsername', CStrings::strToIntDef( $intIsResidentPortalUsername, NULL, false ) );
	}

	public function getIsResidentPortalUsername() {
		return $this->m_intIsResidentPortalUsername;
	}

	public function sqlIsResidentPortalUsername() {
		return ( true == isset( $this->m_intIsResidentPortalUsername ) ) ? ( string ) $this->m_intIsResidentPortalUsername : 'NULL';
	}

	public function setIsVehiclePermitNumber( $intIsVehiclePermitNumber ) {
		$this->set( 'm_intIsVehiclePermitNumber', CStrings::strToIntDef( $intIsVehiclePermitNumber, NULL, false ) );
	}

	public function getIsVehiclePermitNumber() {
		return $this->m_intIsVehiclePermitNumber;
	}

	public function sqlIsVehiclePermitNumber() {
		return ( true == isset( $this->m_intIsVehiclePermitNumber ) ) ? ( string ) $this->m_intIsVehiclePermitNumber : 'NULL';
	}

	public function setIsShared( $intIsShared ) {
		$this->set( 'm_intIsShared', CStrings::strToIntDef( $intIsShared, NULL, false ) );
	}

	public function getIsShared() {
		return $this->m_intIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_intIsShared ) ) ? ( string ) $this->m_intIsShared : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsDedupe( $boolIsDedupe ) {
		$this->set( 'm_boolIsDedupe', CStrings::strToBool( $boolIsDedupe ) );
	}

	public function getIsDedupe() {
		return $this->m_boolIsDedupe;
	}

	public function sqlIsDedupe() {
		return ( true == isset( $this->m_boolIsDedupe ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDedupe ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, scheduled_email_type_id, blocked_recipients, name, properties, property_buildings, unit_number, property_floorplans, property_floors, unit_types, leasing_agents, lead_sources, lease_interval_types, customer_types, products, status_types, sub_status_types, desired_bedrooms, desired_bathrooms, pet_types, event_types, details, move_in_date_min, move_in_date_max, min_move_out_date, max_move_out_date, lease_expiration_date_min, lease_expiration_date_max, lease_renewal_date_min, lease_renewal_date_max, application_created_on_date_min, application_created_on_date_max, balance_due_min, balance_due_max, credit_balance_min, credit_balance_max, desired_rent_min, desired_rent_max, customer_vehicle, delinquencies, application_types, days_created, is_renter_insurance, is_balance_due, is_resident_portal_username, is_vehicle_permit_number, is_shared, is_published, is_dedupe, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlScheduledEmailTypeId() . ', ' .
						$this->sqlBlockedRecipients() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlProperties() . ', ' .
						$this->sqlPropertyBuildings() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlPropertyFloorplans() . ', ' .
						$this->sqlPropertyFloors() . ', ' .
						$this->sqlUnitTypes() . ', ' .
						$this->sqlLeasingAgents() . ', ' .
						$this->sqlLeadSources() . ', ' .
						$this->sqlLeaseIntervalTypes() . ', ' .
						$this->sqlCustomerTypes() . ', ' .
						$this->sqlProducts() . ', ' .
						$this->sqlStatusTypes() . ', ' .
						$this->sqlSubStatusTypes() . ', ' .
						$this->sqlDesiredBedrooms() . ', ' .
						$this->sqlDesiredBathrooms() . ', ' .
						$this->sqlPetTypes() . ', ' .
						$this->sqlEventTypes() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMoveInDateMin() . ', ' .
						$this->sqlMoveInDateMax() . ', ' .
						$this->sqlMinMoveOutDate() . ', ' .
						$this->sqlMaxMoveOutDate() . ', ' .
						$this->sqlLeaseExpirationDateMin() . ', ' .
						$this->sqlLeaseExpirationDateMax() . ', ' .
						$this->sqlLeaseRenewalDateMin() . ', ' .
						$this->sqlLeaseRenewalDateMax() . ', ' .
						$this->sqlApplicationCreatedOnDateMin() . ', ' .
						$this->sqlApplicationCreatedOnDateMax() . ', ' .
						$this->sqlBalanceDueMin() . ', ' .
						$this->sqlBalanceDueMax() . ', ' .
						$this->sqlCreditBalanceMin() . ', ' .
						$this->sqlCreditBalanceMax() . ', ' .
						$this->sqlDesiredRentMin() . ', ' .
						$this->sqlDesiredRentMax() . ', ' .
						$this->sqlCustomerVehicle() . ', ' .
						$this->sqlDelinquencies() . ', ' .
						$this->sqlApplicationTypes() . ', ' .
						$this->sqlDaysCreated() . ', ' .
						$this->sqlIsRenterInsurance() . ', ' .
						$this->sqlIsBalanceDue() . ', ' .
						$this->sqlIsResidentPortalUsername() . ', ' .
						$this->sqlIsVehiclePermitNumber() . ', ' .
						$this->sqlIsShared() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsDedupe() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_type_id = ' . $this->sqlScheduledEmailTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_type_id = ' . $this->sqlScheduledEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blocked_recipients = ' . $this->sqlBlockedRecipients(). ',' ; } elseif( true == array_key_exists( 'BlockedRecipients', $this->getChangedColumns() ) ) { $strSql .= ' blocked_recipients = ' . $this->sqlBlockedRecipients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' properties = ' . $this->sqlProperties(). ',' ; } elseif( true == array_key_exists( 'Properties', $this->getChangedColumns() ) ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_buildings = ' . $this->sqlPropertyBuildings(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildings', $this->getChangedColumns() ) ) { $strSql .= ' property_buildings = ' . $this->sqlPropertyBuildings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplans', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floors = ' . $this->sqlPropertyFloors(). ',' ; } elseif( true == array_key_exists( 'PropertyFloors', $this->getChangedColumns() ) ) { $strSql .= ' property_floors = ' . $this->sqlPropertyFloors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_types = ' . $this->sqlUnitTypes(). ',' ; } elseif( true == array_key_exists( 'UnitTypes', $this->getChangedColumns() ) ) { $strSql .= ' unit_types = ' . $this->sqlUnitTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_agents = ' . $this->sqlLeasingAgents(). ',' ; } elseif( true == array_key_exists( 'LeasingAgents', $this->getChangedColumns() ) ) { $strSql .= ' leasing_agents = ' . $this->sqlLeasingAgents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_sources = ' . $this->sqlLeadSources(). ',' ; } elseif( true == array_key_exists( 'LeadSources', $this->getChangedColumns() ) ) { $strSql .= ' lead_sources = ' . $this->sqlLeadSources() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypes', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_types = ' . $this->sqlCustomerTypes(). ',' ; } elseif( true == array_key_exists( 'CustomerTypes', $this->getChangedColumns() ) ) { $strSql .= ' customer_types = ' . $this->sqlCustomerTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' products = ' . $this->sqlProducts(). ',' ; } elseif( true == array_key_exists( 'Products', $this->getChangedColumns() ) ) { $strSql .= ' products = ' . $this->sqlProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_types = ' . $this->sqlStatusTypes(). ',' ; } elseif( true == array_key_exists( 'StatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' status_types = ' . $this->sqlStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_status_types = ' . $this->sqlSubStatusTypes(). ',' ; } elseif( true == array_key_exists( 'SubStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' sub_status_types = ' . $this->sqlSubStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_types = ' . $this->sqlPetTypes(). ',' ; } elseif( true == array_key_exists( 'PetTypes', $this->getChangedColumns() ) ) { $strSql .= ' pet_types = ' . $this->sqlPetTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_types = ' . $this->sqlEventTypes(). ',' ; } elseif( true == array_key_exists( 'EventTypes', $this->getChangedColumns() ) ) { $strSql .= ' event_types = ' . $this->sqlEventTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin(). ',' ; } elseif( true == array_key_exists( 'MoveInDateMin', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax(). ',' ; } elseif( true == array_key_exists( 'MoveInDateMax', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_move_out_date = ' . $this->sqlMinMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MinMoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' min_move_out_date = ' . $this->sqlMinMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_move_out_date = ' . $this->sqlMaxMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MaxMoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' max_move_out_date = ' . $this->sqlMaxMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_expiration_date_min = ' . $this->sqlLeaseExpirationDateMin(). ',' ; } elseif( true == array_key_exists( 'LeaseExpirationDateMin', $this->getChangedColumns() ) ) { $strSql .= ' lease_expiration_date_min = ' . $this->sqlLeaseExpirationDateMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_expiration_date_max = ' . $this->sqlLeaseExpirationDateMax(). ',' ; } elseif( true == array_key_exists( 'LeaseExpirationDateMax', $this->getChangedColumns() ) ) { $strSql .= ' lease_expiration_date_max = ' . $this->sqlLeaseExpirationDateMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_renewal_date_min = ' . $this->sqlLeaseRenewalDateMin(). ',' ; } elseif( true == array_key_exists( 'LeaseRenewalDateMin', $this->getChangedColumns() ) ) { $strSql .= ' lease_renewal_date_min = ' . $this->sqlLeaseRenewalDateMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_renewal_date_max = ' . $this->sqlLeaseRenewalDateMax(). ',' ; } elseif( true == array_key_exists( 'LeaseRenewalDateMax', $this->getChangedColumns() ) ) { $strSql .= ' lease_renewal_date_max = ' . $this->sqlLeaseRenewalDateMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_created_on_date_min = ' . $this->sqlApplicationCreatedOnDateMin(). ',' ; } elseif( true == array_key_exists( 'ApplicationCreatedOnDateMin', $this->getChangedColumns() ) ) { $strSql .= ' application_created_on_date_min = ' . $this->sqlApplicationCreatedOnDateMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_created_on_date_max = ' . $this->sqlApplicationCreatedOnDateMax(). ',' ; } elseif( true == array_key_exists( 'ApplicationCreatedOnDateMax', $this->getChangedColumns() ) ) { $strSql .= ' application_created_on_date_max = ' . $this->sqlApplicationCreatedOnDateMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' balance_due_min = ' . $this->sqlBalanceDueMin(). ',' ; } elseif( true == array_key_exists( 'BalanceDueMin', $this->getChangedColumns() ) ) { $strSql .= ' balance_due_min = ' . $this->sqlBalanceDueMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' balance_due_max = ' . $this->sqlBalanceDueMax(). ',' ; } elseif( true == array_key_exists( 'BalanceDueMax', $this->getChangedColumns() ) ) { $strSql .= ' balance_due_max = ' . $this->sqlBalanceDueMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_balance_min = ' . $this->sqlCreditBalanceMin(). ',' ; } elseif( true == array_key_exists( 'CreditBalanceMin', $this->getChangedColumns() ) ) { $strSql .= ' credit_balance_min = ' . $this->sqlCreditBalanceMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_balance_max = ' . $this->sqlCreditBalanceMax(). ',' ; } elseif( true == array_key_exists( 'CreditBalanceMax', $this->getChangedColumns() ) ) { $strSql .= ' credit_balance_max = ' . $this->sqlCreditBalanceMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMin', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMax', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_vehicle = ' . $this->sqlCustomerVehicle(). ',' ; } elseif( true == array_key_exists( 'CustomerVehicle', $this->getChangedColumns() ) ) { $strSql .= ' customer_vehicle = ' . $this->sqlCustomerVehicle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquencies = ' . $this->sqlDelinquencies(). ',' ; } elseif( true == array_key_exists( 'Delinquencies', $this->getChangedColumns() ) ) { $strSql .= ' delinquencies = ' . $this->sqlDelinquencies() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_types = ' . $this->sqlApplicationTypes(). ',' ; } elseif( true == array_key_exists( 'ApplicationTypes', $this->getChangedColumns() ) ) { $strSql .= ' application_types = ' . $this->sqlApplicationTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_created = ' . $this->sqlDaysCreated(). ',' ; } elseif( true == array_key_exists( 'DaysCreated', $this->getChangedColumns() ) ) { $strSql .= ' days_created = ' . $this->sqlDaysCreated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renter_insurance = ' . $this->sqlIsRenterInsurance(). ',' ; } elseif( true == array_key_exists( 'IsRenterInsurance', $this->getChangedColumns() ) ) { $strSql .= ' is_renter_insurance = ' . $this->sqlIsRenterInsurance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_balance_due = ' . $this->sqlIsBalanceDue(). ',' ; } elseif( true == array_key_exists( 'IsBalanceDue', $this->getChangedColumns() ) ) { $strSql .= ' is_balance_due = ' . $this->sqlIsBalanceDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_portal_username = ' . $this->sqlIsResidentPortalUsername(). ',' ; } elseif( true == array_key_exists( 'IsResidentPortalUsername', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_portal_username = ' . $this->sqlIsResidentPortalUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vehicle_permit_number = ' . $this->sqlIsVehiclePermitNumber(). ',' ; } elseif( true == array_key_exists( 'IsVehiclePermitNumber', $this->getChangedColumns() ) ) { $strSql .= ' is_vehicle_permit_number = ' . $this->sqlIsVehiclePermitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared(). ',' ; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dedupe = ' . $this->sqlIsDedupe(). ',' ; } elseif( true == array_key_exists( 'IsDedupe', $this->getChangedColumns() ) ) { $strSql .= ' is_dedupe = ' . $this->sqlIsDedupe() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'scheduled_email_type_id' => $this->getScheduledEmailTypeId(),
			'blocked_recipients' => $this->getBlockedRecipients(),
			'name' => $this->getName(),
			'properties' => $this->getProperties(),
			'property_buildings' => $this->getPropertyBuildings(),
			'unit_number' => $this->getUnitNumber(),
			'property_floorplans' => $this->getPropertyFloorplans(),
			'property_floors' => $this->getPropertyFloors(),
			'unit_types' => $this->getUnitTypes(),
			'leasing_agents' => $this->getLeasingAgents(),
			'lead_sources' => $this->getLeadSources(),
			'lease_interval_types' => $this->getLeaseIntervalTypes(),
			'customer_types' => $this->getCustomerTypes(),
			'products' => $this->getProducts(),
			'status_types' => $this->getStatusTypes(),
			'sub_status_types' => $this->getSubStatusTypes(),
			'desired_bedrooms' => $this->getDesiredBedrooms(),
			'desired_bathrooms' => $this->getDesiredBathrooms(),
			'pet_types' => $this->getPetTypes(),
			'event_types' => $this->getEventTypes(),
			'details' => $this->getDetails(),
			'move_in_date_min' => $this->getMoveInDateMin(),
			'move_in_date_max' => $this->getMoveInDateMax(),
			'min_move_out_date' => $this->getMinMoveOutDate(),
			'max_move_out_date' => $this->getMaxMoveOutDate(),
			'lease_expiration_date_min' => $this->getLeaseExpirationDateMin(),
			'lease_expiration_date_max' => $this->getLeaseExpirationDateMax(),
			'lease_renewal_date_min' => $this->getLeaseRenewalDateMin(),
			'lease_renewal_date_max' => $this->getLeaseRenewalDateMax(),
			'application_created_on_date_min' => $this->getApplicationCreatedOnDateMin(),
			'application_created_on_date_max' => $this->getApplicationCreatedOnDateMax(),
			'balance_due_min' => $this->getBalanceDueMin(),
			'balance_due_max' => $this->getBalanceDueMax(),
			'credit_balance_min' => $this->getCreditBalanceMin(),
			'credit_balance_max' => $this->getCreditBalanceMax(),
			'desired_rent_min' => $this->getDesiredRentMin(),
			'desired_rent_max' => $this->getDesiredRentMax(),
			'customer_vehicle' => $this->getCustomerVehicle(),
			'delinquencies' => $this->getDelinquencies(),
			'application_types' => $this->getApplicationTypes(),
			'days_created' => $this->getDaysCreated(),
			'is_renter_insurance' => $this->getIsRenterInsurance(),
			'is_balance_due' => $this->getIsBalanceDue(),
			'is_resident_portal_username' => $this->getIsResidentPortalUsername(),
			'is_vehicle_permit_number' => $this->getIsVehiclePermitNumber(),
			'is_shared' => $this->getIsShared(),
			'is_published' => $this->getIsPublished(),
			'is_dedupe' => $this->getIsDedupe(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>