<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryMacPeriodStatuses
 * Do not add any new functions to this class.
 */

class CBaseMilitaryMacPeriodStatuses extends CEosPluralBase {

	/**
	 * @return CMilitaryMacPeriodStatus[]
	 */
	public static function fetchMilitaryMacPeriodStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryMacPeriodStatus::class, $objDatabase );
	}

	/**
	 * @return CMilitaryMacPeriodStatus
	 */
	public static function fetchMilitaryMacPeriodStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryMacPeriodStatus::class, $objDatabase );
	}

	public static function fetchMilitaryMacPeriodStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_mac_period_statuses', $objDatabase );
	}

	public static function fetchMilitaryMacPeriodStatusById( $intId, $objDatabase ) {
		return self::fetchMilitaryMacPeriodStatus( sprintf( 'SELECT * FROM military_mac_period_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>