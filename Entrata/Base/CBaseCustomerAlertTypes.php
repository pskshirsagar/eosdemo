<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAlertTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerAlertTypes extends CEosPluralBase {

	/**
	 * @return CCustomerAlertType[]
	 */
	public static function fetchCustomerAlertTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerAlertType::class, $objDatabase );
	}

	/**
	 * @return CCustomerAlertType
	 */
	public static function fetchCustomerAlertType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAlertType::class, $objDatabase );
	}

	public static function fetchCustomerAlertTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_alert_types', $objDatabase );
	}

	public static function fetchCustomerAlertTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerAlertType( sprintf( 'SELECT * FROM customer_alert_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>