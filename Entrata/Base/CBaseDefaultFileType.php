<?php

class CBaseDefaultFileType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_file_types';

	protected $m_intId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_arrintEntityTypeIds;
	protected $m_intShowInPortal;
	protected $m_intIsPublished;
	protected $m_intIsHidden;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intFileTypeGroupId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intShowInPortal = '0';
		$this->m_intIsPublished = '0';
		$this->m_intIsHidden = '0';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';
		$this->m_intFileTypeGroupId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['entity_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintEntityTypeIds', trim( $arrValues['entity_type_ids'] ) ); elseif( isset( $arrValues['entity_type_ids'] ) ) $this->setEntityTypeIds( $arrValues['entity_type_ids'] );
		if( isset( $arrValues['show_in_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowInPortal', trim( $arrValues['show_in_portal'] ) ); elseif( isset( $arrValues['show_in_portal'] ) ) $this->setShowInPortal( $arrValues['show_in_portal'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_intIsHidden', trim( $arrValues['is_hidden'] ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( $arrValues['is_hidden'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['file_type_group_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeGroupId', trim( $arrValues['file_type_group_id'] ) ); elseif( isset( $arrValues['file_type_group_id'] ) ) $this->setFileTypeGroupId( $arrValues['file_type_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setEntityTypeIds( $arrintEntityTypeIds ) {
		$this->set( 'm_arrintEntityTypeIds', CStrings::strToArrIntDef( $arrintEntityTypeIds, NULL ) );
	}

	public function getEntityTypeIds() {
		return $this->m_arrintEntityTypeIds;
	}

	public function sqlEntityTypeIds() {
		return ( true == isset( $this->m_arrintEntityTypeIds ) && true == valArr( $this->m_arrintEntityTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintEntityTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setShowInPortal( $intShowInPortal ) {
		$this->set( 'm_intShowInPortal', CStrings::strToIntDef( $intShowInPortal, NULL, false ) );
	}

	public function getShowInPortal() {
		return $this->m_intShowInPortal;
	}

	public function sqlShowInPortal() {
		return ( true == isset( $this->m_intShowInPortal ) ) ? ( string ) $this->m_intShowInPortal : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsHidden( $intIsHidden ) {
		$this->set( 'm_intIsHidden', CStrings::strToIntDef( $intIsHidden, NULL, false ) );
	}

	public function getIsHidden() {
		return $this->m_intIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_intIsHidden ) ) ? ( string ) $this->m_intIsHidden : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setFileTypeGroupId( $intFileTypeGroupId ) {
		$this->set( 'm_intFileTypeGroupId', CStrings::strToIntDef( $intFileTypeGroupId, NULL, false ) );
	}

	public function getFileTypeGroupId() {
		return $this->m_intFileTypeGroupId;
	}

	public function sqlFileTypeGroupId() {
		return ( true == isset( $this->m_intFileTypeGroupId ) ) ? ( string ) $this->m_intFileTypeGroupId : '1';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'entity_type_ids' => $this->getEntityTypeIds(),
			'show_in_portal' => $this->getShowInPortal(),
			'is_published' => $this->getIsPublished(),
			'is_hidden' => $this->getIsHidden(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'file_type_group_id' => $this->getFileTypeGroupId()
		);
	}

}
?>