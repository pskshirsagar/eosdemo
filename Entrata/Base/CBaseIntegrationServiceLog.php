<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationServiceLog extends CEosSingularBase {

	const TABLE_NAME = 'public.integration_service_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intIntegrationServiceId;
	protected $m_intSuccessCount;
	protected $m_intFailureCount;
	protected $m_strLogDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSuccessCount = '0';
		$this->m_intFailureCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['integration_service_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationServiceId', trim( $arrValues['integration_service_id'] ) ); elseif( isset( $arrValues['integration_service_id'] ) ) $this->setIntegrationServiceId( $arrValues['integration_service_id'] );
		if( isset( $arrValues['success_count'] ) && $boolDirectSet ) $this->set( 'm_intSuccessCount', trim( $arrValues['success_count'] ) ); elseif( isset( $arrValues['success_count'] ) ) $this->setSuccessCount( $arrValues['success_count'] );
		if( isset( $arrValues['failure_count'] ) && $boolDirectSet ) $this->set( 'm_intFailureCount', trim( $arrValues['failure_count'] ) ); elseif( isset( $arrValues['failure_count'] ) ) $this->setFailureCount( $arrValues['failure_count'] );
		if( isset( $arrValues['log_date'] ) && $boolDirectSet ) $this->set( 'm_strLogDate', trim( $arrValues['log_date'] ) ); elseif( isset( $arrValues['log_date'] ) ) $this->setLogDate( $arrValues['log_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setIntegrationServiceId( $intIntegrationServiceId ) {
		$this->set( 'm_intIntegrationServiceId', CStrings::strToIntDef( $intIntegrationServiceId, NULL, false ) );
	}

	public function getIntegrationServiceId() {
		return $this->m_intIntegrationServiceId;
	}

	public function sqlIntegrationServiceId() {
		return ( true == isset( $this->m_intIntegrationServiceId ) ) ? ( string ) $this->m_intIntegrationServiceId : 'NULL';
	}

	public function setSuccessCount( $intSuccessCount ) {
		$this->set( 'm_intSuccessCount', CStrings::strToIntDef( $intSuccessCount, NULL, false ) );
	}

	public function getSuccessCount() {
		return $this->m_intSuccessCount;
	}

	public function sqlSuccessCount() {
		return ( true == isset( $this->m_intSuccessCount ) ) ? ( string ) $this->m_intSuccessCount : '0';
	}

	public function setFailureCount( $intFailureCount ) {
		$this->set( 'm_intFailureCount', CStrings::strToIntDef( $intFailureCount, NULL, false ) );
	}

	public function getFailureCount() {
		return $this->m_intFailureCount;
	}

	public function sqlFailureCount() {
		return ( true == isset( $this->m_intFailureCount ) ) ? ( string ) $this->m_intFailureCount : '0';
	}

	public function setLogDate( $strLogDate ) {
		$this->set( 'm_strLogDate', CStrings::strTrimDef( $strLogDate, -1, NULL, true ) );
	}

	public function getLogDate() {
		return $this->m_strLogDate;
	}

	public function sqlLogDate() {
		return ( true == isset( $this->m_strLogDate ) ) ? '\'' . $this->m_strLogDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_database_id, integration_service_id, success_count, failure_count, log_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIntegrationDatabaseId() . ', ' .
 						$this->sqlIntegrationServiceId() . ', ' .
 						$this->sqlSuccessCount() . ', ' .
 						$this->sqlFailureCount() . ', ' .
 						$this->sqlLogDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; } elseif( true == array_key_exists( 'IntegrationServiceId', $this->getChangedColumns() ) ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' success_count = ' . $this->sqlSuccessCount() . ','; } elseif( true == array_key_exists( 'SuccessCount', $this->getChangedColumns() ) ) { $strSql .= ' success_count = ' . $this->sqlSuccessCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failure_count = ' . $this->sqlFailureCount() . ','; } elseif( true == array_key_exists( 'FailureCount', $this->getChangedColumns() ) ) { $strSql .= ' failure_count = ' . $this->sqlFailureCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; } elseif( true == array_key_exists( 'LogDate', $this->getChangedColumns() ) ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'integration_service_id' => $this->getIntegrationServiceId(),
			'success_count' => $this->getSuccessCount(),
			'failure_count' => $this->getFailureCount(),
			'log_date' => $this->getLogDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>