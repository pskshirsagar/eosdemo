<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMobileApplicationProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMobileApplicationProperties extends CEosPluralBase {

	/**
	 * @return CMobileApplicationProperty[]
	 */
	public static function fetchMobileApplicationProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMobileApplicationProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMobileApplicationProperty
	 */
	public static function fetchMobileApplicationProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMobileApplicationProperty', $objDatabase );
	}

	public static function fetchMobileApplicationPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mobile_application_properties', $objDatabase );
	}

	public static function fetchMobileApplicationPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMobileApplicationProperty( sprintf( 'SELECT * FROM mobile_application_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMobileApplicationPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchMobileApplicationProperties( sprintf( 'SELECT * FROM mobile_application_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMobileApplicationPropertiesByMobileApplicationIdByCid( $intMobileApplicationId, $intCid, $objDatabase ) {
		return self::fetchMobileApplicationProperties( sprintf( 'SELECT * FROM mobile_application_properties WHERE mobile_application_id = %d AND cid = %d', ( int ) $intMobileApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMobileApplicationPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMobileApplicationProperties( sprintf( 'SELECT * FROM mobile_application_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>