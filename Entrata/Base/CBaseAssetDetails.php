<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetDetails extends CEosPluralBase {

	/**
	 * @return CAssetDetail[]
	 */
	public static function fetchAssetDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetDetail
	 */
	public static function fetchAssetDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetDetail', $objDatabase );
	}

	public static function fetchAssetDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_details', $objDatabase );
	}

	public static function fetchAssetDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetDetail( sprintf( 'SELECT * FROM asset_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE asset_id = %d AND cid = %d', ( int ) $intAssetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByAssetConditionIdByCid( $intAssetConditionId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE asset_condition_id = %d AND cid = %d', ( int ) $intAssetConditionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByTaxRecoveryPeriodIdByCid( $intTaxRecoveryPeriodId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE tax_recovery_period_id = %d AND cid = %d', ( int ) $intTaxRecoveryPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDetailsByDepreciationCategoryIdByCid( $intDepreciationCategoryId, $intCid, $objDatabase ) {
		return self::fetchAssetDetails( sprintf( 'SELECT * FROM asset_details WHERE depreciation_category_id = %d AND cid = %d', ( int ) $intDepreciationCategoryId, ( int ) $intCid ), $objDatabase );
	}

}
?>