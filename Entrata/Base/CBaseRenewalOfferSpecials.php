<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base class again, please do so by checking the composite key checkbox. ***
 */

class CBaseRenewalOfferSpecials extends CEosPluralBase {

    const TABLE_RENEWAL_OFFER_SPECIALS = 'public.renewal_offer_specials';

    public static function fetchRenewalOfferSpecials( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRenewalOfferSpecial', $objDatabase );
    }

    public static function fetchRenewalOfferSpecial( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRenewalOfferSpecial', $objDatabase );
    }

    public static function fetchRenewalOfferSpecialCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'renewal_offer_specials', $objDatabase );
    }

    public static function fetchRenewalOfferSpecialByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchRenewalOfferSpecial( sprintf( 'SELECT * FROM renewal_offer_specials WHERE id = %d AND cid = %d ', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRenewalOfferSpecialsByCid( $intCid, $objDatabase ) {
        return self::fetchRenewalOfferSpecials( sprintf( 'SELECT * FROM renewal_offer_specials WHERE cid = %d ', (int) $intCid ), $objDatabase );
    }

    public static function fetchRenewalOfferSpecialsByRenewalOfferIdByCid( $intRenewalOfferId, $intCid, $objDatabase ) {
        return self::fetchRenewalOfferSpecials( sprintf( 'SELECT * FROM renewal_offer_specials WHERE renewal_offer_id = %d AND cid = %d', (int) $intRenewalOfferId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRenewalOfferSpecialsByCompanySpecialIdByCid( $intCompanySpecialId, $intCid, $objDatabase ) {
        return self::fetchRenewalOfferSpecials( sprintf( 'SELECT * FROM renewal_offer_specials WHERE company_special_id = %d AND cid = %d', (int) $intCompanySpecialId, (int) $intCid ), $objDatabase );
    }
}
?>