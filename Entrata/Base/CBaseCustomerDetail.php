<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intInvoiceDeliveryTypeId;
	protected $m_strStudentIdNumber;
	protected $m_strParentNameFirst;
	protected $m_strParentNameLast;
	protected $m_strParentEmailAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intHasNameLastMatronymic;
	protected $m_strCitizenshipCountryCode;
	protected $m_strBankName;

	public function __construct() {
		parent::__construct();

		$this->m_intHasNameLastMatronymic = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['invoice_delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDeliveryTypeId', trim( $arrValues['invoice_delivery_type_id'] ) ); elseif( isset( $arrValues['invoice_delivery_type_id'] ) ) $this->setInvoiceDeliveryTypeId( $arrValues['invoice_delivery_type_id'] );
		if( isset( $arrValues['student_id_number'] ) && $boolDirectSet ) $this->set( 'm_strStudentIdNumber', trim( stripcslashes( $arrValues['student_id_number'] ) ) ); elseif( isset( $arrValues['student_id_number'] ) ) $this->setStudentIdNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['student_id_number'] ) : $arrValues['student_id_number'] );
		if( isset( $arrValues['parent_name_first'] ) && $boolDirectSet ) $this->set( 'm_strParentNameFirst', trim( stripcslashes( $arrValues['parent_name_first'] ) ) ); elseif( isset( $arrValues['parent_name_first'] ) ) $this->setParentNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parent_name_first'] ) : $arrValues['parent_name_first'] );
		if( isset( $arrValues['parent_name_last'] ) && $boolDirectSet ) $this->set( 'm_strParentNameLast', trim( stripcslashes( $arrValues['parent_name_last'] ) ) ); elseif( isset( $arrValues['parent_name_last'] ) ) $this->setParentNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parent_name_last'] ) : $arrValues['parent_name_last'] );
		if( isset( $arrValues['parent_email_address'] ) && $boolDirectSet ) $this->set( 'm_strParentEmailAddress', trim( stripcslashes( $arrValues['parent_email_address'] ) ) ); elseif( isset( $arrValues['parent_email_address'] ) ) $this->setParentEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parent_email_address'] ) : $arrValues['parent_email_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['has_name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_intHasNameLastMatronymic', trim( $arrValues['has_name_last_matronymic'] ) ); elseif( isset( $arrValues['has_name_last_matronymic'] ) ) $this->setHasNameLastMatronymic( $arrValues['has_name_last_matronymic'] );
		if( isset( $arrValues['citizenship_country_code'] ) && $boolDirectSet ) $this->set( 'm_strCitizenshipCountryCode', trim( stripcslashes( $arrValues['citizenship_country_code'] ) ) ); elseif( isset( $arrValues['citizenship_country_code'] ) ) $this->setCitizenshipCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['citizenship_country_code'] ) : $arrValues['citizenship_country_code'] );
		if( isset( $arrValues['bank_name'] ) && $boolDirectSet ) $this->set( 'm_strBankName', trim( stripcslashes( $arrValues['bank_name'] ) ) ); elseif( isset( $arrValues['bank_name'] ) ) $this->setBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_name'] ) : $arrValues['bank_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setInvoiceDeliveryTypeId( $intInvoiceDeliveryTypeId ) {
		$this->set( 'm_intInvoiceDeliveryTypeId', CStrings::strToIntDef( $intInvoiceDeliveryTypeId, NULL, false ) );
	}

	public function getInvoiceDeliveryTypeId() {
		return $this->m_intInvoiceDeliveryTypeId;
	}

	public function sqlInvoiceDeliveryTypeId() {
		return ( true == isset( $this->m_intInvoiceDeliveryTypeId ) ) ? ( string ) $this->m_intInvoiceDeliveryTypeId : 'NULL';
	}

	public function setStudentIdNumber( $strStudentIdNumber ) {
		$this->set( 'm_strStudentIdNumber', CStrings::strTrimDef( $strStudentIdNumber, 25, NULL, true ) );
	}

	public function getStudentIdNumber() {
		return $this->m_strStudentIdNumber;
	}

	public function sqlStudentIdNumber() {
		return ( true == isset( $this->m_strStudentIdNumber ) ) ? '\'' . addslashes( $this->m_strStudentIdNumber ) . '\'' : 'NULL';
	}

	public function setParentNameFirst( $strParentNameFirst ) {
		$this->set( 'm_strParentNameFirst', CStrings::strTrimDef( $strParentNameFirst, 50, NULL, true ) );
	}

	public function getParentNameFirst() {
		return $this->m_strParentNameFirst;
	}

	public function sqlParentNameFirst() {
		return ( true == isset( $this->m_strParentNameFirst ) ) ? '\'' . addslashes( $this->m_strParentNameFirst ) . '\'' : 'NULL';
	}

	public function setParentNameLast( $strParentNameLast ) {
		$this->set( 'm_strParentNameLast', CStrings::strTrimDef( $strParentNameLast, 50, NULL, true ) );
	}

	public function getParentNameLast() {
		return $this->m_strParentNameLast;
	}

	public function sqlParentNameLast() {
		return ( true == isset( $this->m_strParentNameLast ) ) ? '\'' . addslashes( $this->m_strParentNameLast ) . '\'' : 'NULL';
	}

	public function setParentEmailAddress( $strParentEmailAddress ) {
		$this->set( 'm_strParentEmailAddress', CStrings::strTrimDef( $strParentEmailAddress, 240, NULL, true ) );
	}

	public function getParentEmailAddress() {
		return $this->m_strParentEmailAddress;
	}

	public function sqlParentEmailAddress() {
		return ( true == isset( $this->m_strParentEmailAddress ) ) ? '\'' . addslashes( $this->m_strParentEmailAddress ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setHasNameLastMatronymic( $intHasNameLastMatronymic ) {
		$this->set( 'm_intHasNameLastMatronymic', CStrings::strToIntDef( $intHasNameLastMatronymic, NULL, false ) );
	}

	public function getHasNameLastMatronymic() {
		return $this->m_intHasNameLastMatronymic;
	}

	public function sqlHasNameLastMatronymic() {
		return ( true == isset( $this->m_intHasNameLastMatronymic ) ) ? ( string ) $this->m_intHasNameLastMatronymic : '1';
	}

	public function setCitizenshipCountryCode( $strCitizenshipCountryCode ) {
		$this->set( 'm_strCitizenshipCountryCode', CStrings::strTrimDef( $strCitizenshipCountryCode, 2, NULL, true ) );
	}

	public function getCitizenshipCountryCode() {
		return $this->m_strCitizenshipCountryCode;
	}

	public function sqlCitizenshipCountryCode() {
		return ( true == isset( $this->m_strCitizenshipCountryCode ) ) ? '\'' . addslashes( $this->m_strCitizenshipCountryCode ) . '\'' : 'NULL';
	}

	public function setBankName( $strBankName ) {
		$this->set( 'm_strBankName', CStrings::strTrimDef( $strBankName, 50, NULL, true ) );
	}

	public function getBankName() {
		return $this->m_strBankName;
	}

	public function sqlBankName() {
		return ( true == isset( $this->m_strBankName ) ) ? '\'' . addslashes( $this->m_strBankName ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, invoice_delivery_type_id, student_id_number, parent_name_first, parent_name_last, parent_email_address, updated_by, updated_on, created_by, created_on, has_name_last_matronymic, citizenship_country_code, bank_name )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlInvoiceDeliveryTypeId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlParentNameFirst() . ', ' .
						$this->sqlParentNameLast() . ', ' .
						$this->sqlParentEmailAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
						$this->sqlCitizenshipCountryCode() . ', ' .
						$this->sqlBankName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_delivery_type_id = ' . $this->sqlInvoiceDeliveryTypeId(). ',' ; } elseif( true == array_key_exists( 'InvoiceDeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_delivery_type_id = ' . $this->sqlInvoiceDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' student_id_number = ' . $this->sqlStudentIdNumber(). ',' ; } elseif( true == array_key_exists( 'StudentIdNumber', $this->getChangedColumns() ) ) { $strSql .= ' student_id_number = ' . $this->sqlStudentIdNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_name_first = ' . $this->sqlParentNameFirst(). ',' ; } elseif( true == array_key_exists( 'ParentNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' parent_name_first = ' . $this->sqlParentNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_name_last = ' . $this->sqlParentNameLast(). ',' ; } elseif( true == array_key_exists( 'ParentNameLast', $this->getChangedColumns() ) ) { $strSql .= ' parent_name_last = ' . $this->sqlParentNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_email_address = ' . $this->sqlParentEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ParentEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' parent_email_address = ' . $this->sqlParentEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_name_last_matronymic = ' . $this->sqlHasNameLastMatronymic(). ',' ; } elseif( true == array_key_exists( 'HasNameLastMatronymic', $this->getChangedColumns() ) ) { $strSql .= ' has_name_last_matronymic = ' . $this->sqlHasNameLastMatronymic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' citizenship_country_code = ' . $this->sqlCitizenshipCountryCode(). ',' ; } elseif( true == array_key_exists( 'CitizenshipCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' citizenship_country_code = ' . $this->sqlCitizenshipCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_name = ' . $this->sqlBankName(). ',' ; } elseif( true == array_key_exists( 'BankName', $this->getChangedColumns() ) ) { $strSql .= ' bank_name = ' . $this->sqlBankName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'invoice_delivery_type_id' => $this->getInvoiceDeliveryTypeId(),
			'student_id_number' => $this->getStudentIdNumber(),
			'parent_name_first' => $this->getParentNameFirst(),
			'parent_name_last' => $this->getParentNameLast(),
			'parent_email_address' => $this->getParentEmailAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'has_name_last_matronymic' => $this->getHasNameLastMatronymic(),
			'citizenship_country_code' => $this->getCitizenshipCountryCode(),
			'bank_name' => $this->getBankName()
		);
	}

}
?>