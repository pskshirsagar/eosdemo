<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.periods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strPostMonth;
	protected $m_intCalendarDays;
	protected $m_intFinancialDays;
	protected $m_strArStartDate;
	protected $m_strApStartDate;
	protected $m_strGlStartDate;
	protected $m_strArEndDate;
	protected $m_strApEndDate;
	protected $m_strGlEndDate;
	protected $m_strArLockedOn;
	protected $m_strApLockedOn;
	protected $m_strGlLockedOn;
	protected $m_strArSoftLockedOn;
	protected $m_strApSoftLockedOn;
	protected $m_strGlSoftLockedOn;
	protected $m_strVacancyCachedOn;
	protected $m_intRebuildDifferentialCache;
	protected $m_intCacheInitialized;
	protected $m_intHasCache;
	protected $m_intHasDisabledProration;
	protected $m_intUseMarketRentInGpr;
	protected $m_intHasFinancialData;
	protected $m_strGprBookBasis;
	protected $m_intIsArRollback;
	protected $m_intIsApRollback;
	protected $m_intIsGlRollback;
	protected $m_intIsGprPosted;
	protected $m_intIsInitialImport;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRebuildDifferentialCache = '0';
		$this->m_intCacheInitialized = '0';
		$this->m_intHasCache = '0';
		$this->m_intHasDisabledProration = '0';
		$this->m_intUseMarketRentInGpr = '1';
		$this->m_intHasFinancialData = '1';
		$this->m_strGprBookBasis = 'accrual';
		$this->m_intIsArRollback = '0';
		$this->m_intIsApRollback = '0';
		$this->m_intIsGlRollback = '0';
		$this->m_intIsGprPosted = '0';
		$this->m_intIsInitialImport = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['calendar_days'] ) && $boolDirectSet ) $this->set( 'm_intCalendarDays', trim( $arrValues['calendar_days'] ) ); elseif( isset( $arrValues['calendar_days'] ) ) $this->setCalendarDays( $arrValues['calendar_days'] );
		if( isset( $arrValues['financial_days'] ) && $boolDirectSet ) $this->set( 'm_intFinancialDays', trim( $arrValues['financial_days'] ) ); elseif( isset( $arrValues['financial_days'] ) ) $this->setFinancialDays( $arrValues['financial_days'] );
		if( isset( $arrValues['ar_start_date'] ) && $boolDirectSet ) $this->set( 'm_strArStartDate', trim( $arrValues['ar_start_date'] ) ); elseif( isset( $arrValues['ar_start_date'] ) ) $this->setArStartDate( $arrValues['ar_start_date'] );
		if( isset( $arrValues['ap_start_date'] ) && $boolDirectSet ) $this->set( 'm_strApStartDate', trim( $arrValues['ap_start_date'] ) ); elseif( isset( $arrValues['ap_start_date'] ) ) $this->setApStartDate( $arrValues['ap_start_date'] );
		if( isset( $arrValues['gl_start_date'] ) && $boolDirectSet ) $this->set( 'm_strGlStartDate', trim( $arrValues['gl_start_date'] ) ); elseif( isset( $arrValues['gl_start_date'] ) ) $this->setGlStartDate( $arrValues['gl_start_date'] );
		if( isset( $arrValues['ar_end_date'] ) && $boolDirectSet ) $this->set( 'm_strArEndDate', trim( $arrValues['ar_end_date'] ) ); elseif( isset( $arrValues['ar_end_date'] ) ) $this->setArEndDate( $arrValues['ar_end_date'] );
		if( isset( $arrValues['ap_end_date'] ) && $boolDirectSet ) $this->set( 'm_strApEndDate', trim( $arrValues['ap_end_date'] ) ); elseif( isset( $arrValues['ap_end_date'] ) ) $this->setApEndDate( $arrValues['ap_end_date'] );
		if( isset( $arrValues['gl_end_date'] ) && $boolDirectSet ) $this->set( 'm_strGlEndDate', trim( $arrValues['gl_end_date'] ) ); elseif( isset( $arrValues['gl_end_date'] ) ) $this->setGlEndDate( $arrValues['gl_end_date'] );
		if( isset( $arrValues['ar_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strArLockedOn', trim( $arrValues['ar_locked_on'] ) ); elseif( isset( $arrValues['ar_locked_on'] ) ) $this->setArLockedOn( $arrValues['ar_locked_on'] );
		if( isset( $arrValues['ap_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strApLockedOn', trim( $arrValues['ap_locked_on'] ) ); elseif( isset( $arrValues['ap_locked_on'] ) ) $this->setApLockedOn( $arrValues['ap_locked_on'] );
		if( isset( $arrValues['gl_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strGlLockedOn', trim( $arrValues['gl_locked_on'] ) ); elseif( isset( $arrValues['gl_locked_on'] ) ) $this->setGlLockedOn( $arrValues['gl_locked_on'] );
		if( isset( $arrValues['ar_soft_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strArSoftLockedOn', trim( $arrValues['ar_soft_locked_on'] ) ); elseif( isset( $arrValues['ar_soft_locked_on'] ) ) $this->setArSoftLockedOn( $arrValues['ar_soft_locked_on'] );
		if( isset( $arrValues['ap_soft_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strApSoftLockedOn', trim( $arrValues['ap_soft_locked_on'] ) ); elseif( isset( $arrValues['ap_soft_locked_on'] ) ) $this->setApSoftLockedOn( $arrValues['ap_soft_locked_on'] );
		if( isset( $arrValues['gl_soft_locked_on'] ) && $boolDirectSet ) $this->set( 'm_strGlSoftLockedOn', trim( $arrValues['gl_soft_locked_on'] ) ); elseif( isset( $arrValues['gl_soft_locked_on'] ) ) $this->setGlSoftLockedOn( $arrValues['gl_soft_locked_on'] );
		if( isset( $arrValues['vacancy_cached_on'] ) && $boolDirectSet ) $this->set( 'm_strVacancyCachedOn', trim( $arrValues['vacancy_cached_on'] ) ); elseif( isset( $arrValues['vacancy_cached_on'] ) ) $this->setVacancyCachedOn( $arrValues['vacancy_cached_on'] );
		if( isset( $arrValues['rebuild_differential_cache'] ) && $boolDirectSet ) $this->set( 'm_intRebuildDifferentialCache', trim( $arrValues['rebuild_differential_cache'] ) ); elseif( isset( $arrValues['rebuild_differential_cache'] ) ) $this->setRebuildDifferentialCache( $arrValues['rebuild_differential_cache'] );
		if( isset( $arrValues['cache_initialized'] ) && $boolDirectSet ) $this->set( 'm_intCacheInitialized', trim( $arrValues['cache_initialized'] ) ); elseif( isset( $arrValues['cache_initialized'] ) ) $this->setCacheInitialized( $arrValues['cache_initialized'] );
		if( isset( $arrValues['has_cache'] ) && $boolDirectSet ) $this->set( 'm_intHasCache', trim( $arrValues['has_cache'] ) ); elseif( isset( $arrValues['has_cache'] ) ) $this->setHasCache( $arrValues['has_cache'] );
		if( isset( $arrValues['has_disabled_proration'] ) && $boolDirectSet ) $this->set( 'm_intHasDisabledProration', trim( $arrValues['has_disabled_proration'] ) ); elseif( isset( $arrValues['has_disabled_proration'] ) ) $this->setHasDisabledProration( $arrValues['has_disabled_proration'] );
		if( isset( $arrValues['use_market_rent_in_gpr'] ) && $boolDirectSet ) $this->set( 'm_intUseMarketRentInGpr', trim( $arrValues['use_market_rent_in_gpr'] ) ); elseif( isset( $arrValues['use_market_rent_in_gpr'] ) ) $this->setUseMarketRentInGpr( $arrValues['use_market_rent_in_gpr'] );
		if( isset( $arrValues['has_financial_data'] ) && $boolDirectSet ) $this->set( 'm_intHasFinancialData', trim( $arrValues['has_financial_data'] ) ); elseif( isset( $arrValues['has_financial_data'] ) ) $this->setHasFinancialData( $arrValues['has_financial_data'] );
		if( isset( $arrValues['gpr_book_basis'] ) && $boolDirectSet ) $this->set( 'm_strGprBookBasis', trim( stripcslashes( $arrValues['gpr_book_basis'] ) ) ); elseif( isset( $arrValues['gpr_book_basis'] ) ) $this->setGprBookBasis( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gpr_book_basis'] ) : $arrValues['gpr_book_basis'] );
		if( isset( $arrValues['is_ar_rollback'] ) && $boolDirectSet ) $this->set( 'm_intIsArRollback', trim( $arrValues['is_ar_rollback'] ) ); elseif( isset( $arrValues['is_ar_rollback'] ) ) $this->setIsArRollback( $arrValues['is_ar_rollback'] );
		if( isset( $arrValues['is_ap_rollback'] ) && $boolDirectSet ) $this->set( 'm_intIsApRollback', trim( $arrValues['is_ap_rollback'] ) ); elseif( isset( $arrValues['is_ap_rollback'] ) ) $this->setIsApRollback( $arrValues['is_ap_rollback'] );
		if( isset( $arrValues['is_gl_rollback'] ) && $boolDirectSet ) $this->set( 'm_intIsGlRollback', trim( $arrValues['is_gl_rollback'] ) ); elseif( isset( $arrValues['is_gl_rollback'] ) ) $this->setIsGlRollback( $arrValues['is_gl_rollback'] );
		if( isset( $arrValues['is_gpr_posted'] ) && $boolDirectSet ) $this->set( 'm_intIsGprPosted', trim( $arrValues['is_gpr_posted'] ) ); elseif( isset( $arrValues['is_gpr_posted'] ) ) $this->setIsGprPosted( $arrValues['is_gpr_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_intIsInitialImport', trim( $arrValues['is_initial_import'] ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( $arrValues['is_initial_import'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setCalendarDays( $intCalendarDays ) {
		$this->set( 'm_intCalendarDays', CStrings::strToIntDef( $intCalendarDays, NULL, false ) );
	}

	public function getCalendarDays() {
		return $this->m_intCalendarDays;
	}

	public function sqlCalendarDays() {
		return ( true == isset( $this->m_intCalendarDays ) ) ? ( string ) $this->m_intCalendarDays : 'NULL';
	}

	public function setFinancialDays( $intFinancialDays ) {
		$this->set( 'm_intFinancialDays', CStrings::strToIntDef( $intFinancialDays, NULL, false ) );
	}

	public function getFinancialDays() {
		return $this->m_intFinancialDays;
	}

	public function sqlFinancialDays() {
		return ( true == isset( $this->m_intFinancialDays ) ) ? ( string ) $this->m_intFinancialDays : 'NULL';
	}

	public function setArStartDate( $strArStartDate ) {
		$this->set( 'm_strArStartDate', CStrings::strTrimDef( $strArStartDate, -1, NULL, true ) );
	}

	public function getArStartDate() {
		return $this->m_strArStartDate;
	}

	public function sqlArStartDate() {
		return ( true == isset( $this->m_strArStartDate ) ) ? '\'' . $this->m_strArStartDate . '\'' : 'NOW()';
	}

	public function setApStartDate( $strApStartDate ) {
		$this->set( 'm_strApStartDate', CStrings::strTrimDef( $strApStartDate, -1, NULL, true ) );
	}

	public function getApStartDate() {
		return $this->m_strApStartDate;
	}

	public function sqlApStartDate() {
		return ( true == isset( $this->m_strApStartDate ) ) ? '\'' . $this->m_strApStartDate . '\'' : 'NOW()';
	}

	public function setGlStartDate( $strGlStartDate ) {
		$this->set( 'm_strGlStartDate', CStrings::strTrimDef( $strGlStartDate, -1, NULL, true ) );
	}

	public function getGlStartDate() {
		return $this->m_strGlStartDate;
	}

	public function sqlGlStartDate() {
		return ( true == isset( $this->m_strGlStartDate ) ) ? '\'' . $this->m_strGlStartDate . '\'' : 'NOW()';
	}

	public function setArEndDate( $strArEndDate ) {
		$this->set( 'm_strArEndDate', CStrings::strTrimDef( $strArEndDate, -1, NULL, true ) );
	}

	public function getArEndDate() {
		return $this->m_strArEndDate;
	}

	public function sqlArEndDate() {
		return ( true == isset( $this->m_strArEndDate ) ) ? '\'' . $this->m_strArEndDate . '\'' : 'NOW()';
	}

	public function setApEndDate( $strApEndDate ) {
		$this->set( 'm_strApEndDate', CStrings::strTrimDef( $strApEndDate, -1, NULL, true ) );
	}

	public function getApEndDate() {
		return $this->m_strApEndDate;
	}

	public function sqlApEndDate() {
		return ( true == isset( $this->m_strApEndDate ) ) ? '\'' . $this->m_strApEndDate . '\'' : 'NOW()';
	}

	public function setGlEndDate( $strGlEndDate ) {
		$this->set( 'm_strGlEndDate', CStrings::strTrimDef( $strGlEndDate, -1, NULL, true ) );
	}

	public function getGlEndDate() {
		return $this->m_strGlEndDate;
	}

	public function sqlGlEndDate() {
		return ( true == isset( $this->m_strGlEndDate ) ) ? '\'' . $this->m_strGlEndDate . '\'' : 'NOW()';
	}

	public function setArLockedOn( $strArLockedOn ) {
		$this->set( 'm_strArLockedOn', CStrings::strTrimDef( $strArLockedOn, -1, NULL, true ) );
	}

	public function getArLockedOn() {
		return $this->m_strArLockedOn;
	}

	public function sqlArLockedOn() {
		return ( true == isset( $this->m_strArLockedOn ) ) ? '\'' . $this->m_strArLockedOn . '\'' : 'NULL';
	}

	public function setApLockedOn( $strApLockedOn ) {
		$this->set( 'm_strApLockedOn', CStrings::strTrimDef( $strApLockedOn, -1, NULL, true ) );
	}

	public function getApLockedOn() {
		return $this->m_strApLockedOn;
	}

	public function sqlApLockedOn() {
		return ( true == isset( $this->m_strApLockedOn ) ) ? '\'' . $this->m_strApLockedOn . '\'' : 'NULL';
	}

	public function setGlLockedOn( $strGlLockedOn ) {
		$this->set( 'm_strGlLockedOn', CStrings::strTrimDef( $strGlLockedOn, -1, NULL, true ) );
	}

	public function getGlLockedOn() {
		return $this->m_strGlLockedOn;
	}

	public function sqlGlLockedOn() {
		return ( true == isset( $this->m_strGlLockedOn ) ) ? '\'' . $this->m_strGlLockedOn . '\'' : 'NULL';
	}

	public function setArSoftLockedOn( $strArSoftLockedOn ) {
		$this->set( 'm_strArSoftLockedOn', CStrings::strTrimDef( $strArSoftLockedOn, -1, NULL, true ) );
	}

	public function getArSoftLockedOn() {
		return $this->m_strArSoftLockedOn;
	}

	public function sqlArSoftLockedOn() {
		return ( true == isset( $this->m_strArSoftLockedOn ) ) ? '\'' . $this->m_strArSoftLockedOn . '\'' : 'NULL';
	}

	public function setApSoftLockedOn( $strApSoftLockedOn ) {
		$this->set( 'm_strApSoftLockedOn', CStrings::strTrimDef( $strApSoftLockedOn, -1, NULL, true ) );
	}

	public function getApSoftLockedOn() {
		return $this->m_strApSoftLockedOn;
	}

	public function sqlApSoftLockedOn() {
		return ( true == isset( $this->m_strApSoftLockedOn ) ) ? '\'' . $this->m_strApSoftLockedOn . '\'' : 'NULL';
	}

	public function setGlSoftLockedOn( $strGlSoftLockedOn ) {
		$this->set( 'm_strGlSoftLockedOn', CStrings::strTrimDef( $strGlSoftLockedOn, -1, NULL, true ) );
	}

	public function getGlSoftLockedOn() {
		return $this->m_strGlSoftLockedOn;
	}

	public function sqlGlSoftLockedOn() {
		return ( true == isset( $this->m_strGlSoftLockedOn ) ) ? '\'' . $this->m_strGlSoftLockedOn . '\'' : 'NULL';
	}

	public function setVacancyCachedOn( $strVacancyCachedOn ) {
		$this->set( 'm_strVacancyCachedOn', CStrings::strTrimDef( $strVacancyCachedOn, -1, NULL, true ) );
	}

	public function getVacancyCachedOn() {
		return $this->m_strVacancyCachedOn;
	}

	public function sqlVacancyCachedOn() {
		return ( true == isset( $this->m_strVacancyCachedOn ) ) ? '\'' . $this->m_strVacancyCachedOn . '\'' : 'NULL';
	}

	public function setRebuildDifferentialCache( $intRebuildDifferentialCache ) {
		$this->set( 'm_intRebuildDifferentialCache', CStrings::strToIntDef( $intRebuildDifferentialCache, NULL, false ) );
	}

	public function getRebuildDifferentialCache() {
		return $this->m_intRebuildDifferentialCache;
	}

	public function sqlRebuildDifferentialCache() {
		return ( true == isset( $this->m_intRebuildDifferentialCache ) ) ? ( string ) $this->m_intRebuildDifferentialCache : '0';
	}

	public function setCacheInitialized( $intCacheInitialized ) {
		$this->set( 'm_intCacheInitialized', CStrings::strToIntDef( $intCacheInitialized, NULL, false ) );
	}

	public function getCacheInitialized() {
		return $this->m_intCacheInitialized;
	}

	public function sqlCacheInitialized() {
		return ( true == isset( $this->m_intCacheInitialized ) ) ? ( string ) $this->m_intCacheInitialized : '0';
	}

	public function setHasCache( $intHasCache ) {
		$this->set( 'm_intHasCache', CStrings::strToIntDef( $intHasCache, NULL, false ) );
	}

	public function getHasCache() {
		return $this->m_intHasCache;
	}

	public function sqlHasCache() {
		return ( true == isset( $this->m_intHasCache ) ) ? ( string ) $this->m_intHasCache : '0';
	}

	public function setHasDisabledProration( $intHasDisabledProration ) {
		$this->set( 'm_intHasDisabledProration', CStrings::strToIntDef( $intHasDisabledProration, NULL, false ) );
	}

	public function getHasDisabledProration() {
		return $this->m_intHasDisabledProration;
	}

	public function sqlHasDisabledProration() {
		return ( true == isset( $this->m_intHasDisabledProration ) ) ? ( string ) $this->m_intHasDisabledProration : '0';
	}

	public function setUseMarketRentInGpr( $intUseMarketRentInGpr ) {
		$this->set( 'm_intUseMarketRentInGpr', CStrings::strToIntDef( $intUseMarketRentInGpr, NULL, false ) );
	}

	public function getUseMarketRentInGpr() {
		return $this->m_intUseMarketRentInGpr;
	}

	public function sqlUseMarketRentInGpr() {
		return ( true == isset( $this->m_intUseMarketRentInGpr ) ) ? ( string ) $this->m_intUseMarketRentInGpr : '1';
	}

	public function setHasFinancialData( $intHasFinancialData ) {
		$this->set( 'm_intHasFinancialData', CStrings::strToIntDef( $intHasFinancialData, NULL, false ) );
	}

	public function getHasFinancialData() {
		return $this->m_intHasFinancialData;
	}

	public function sqlHasFinancialData() {
		return ( true == isset( $this->m_intHasFinancialData ) ) ? ( string ) $this->m_intHasFinancialData : '1';
	}

	public function setGprBookBasis( $strGprBookBasis ) {
		$this->set( 'm_strGprBookBasis', CStrings::strTrimDef( $strGprBookBasis, 7, NULL, true ) );
	}

	public function getGprBookBasis() {
		return $this->m_strGprBookBasis;
	}

	public function sqlGprBookBasis() {
		return ( true == isset( $this->m_strGprBookBasis ) ) ? '\'' . addslashes( $this->m_strGprBookBasis ) . '\'' : '\'accrual\'';
	}

	public function setIsArRollback( $intIsArRollback ) {
		$this->set( 'm_intIsArRollback', CStrings::strToIntDef( $intIsArRollback, NULL, false ) );
	}

	public function getIsArRollback() {
		return $this->m_intIsArRollback;
	}

	public function sqlIsArRollback() {
		return ( true == isset( $this->m_intIsArRollback ) ) ? ( string ) $this->m_intIsArRollback : '0';
	}

	public function setIsApRollback( $intIsApRollback ) {
		$this->set( 'm_intIsApRollback', CStrings::strToIntDef( $intIsApRollback, NULL, false ) );
	}

	public function getIsApRollback() {
		return $this->m_intIsApRollback;
	}

	public function sqlIsApRollback() {
		return ( true == isset( $this->m_intIsApRollback ) ) ? ( string ) $this->m_intIsApRollback : '0';
	}

	public function setIsGlRollback( $intIsGlRollback ) {
		$this->set( 'm_intIsGlRollback', CStrings::strToIntDef( $intIsGlRollback, NULL, false ) );
	}

	public function getIsGlRollback() {
		return $this->m_intIsGlRollback;
	}

	public function sqlIsGlRollback() {
		return ( true == isset( $this->m_intIsGlRollback ) ) ? ( string ) $this->m_intIsGlRollback : '0';
	}

	public function setIsGprPosted( $intIsGprPosted ) {
		$this->set( 'm_intIsGprPosted', CStrings::strToIntDef( $intIsGprPosted, NULL, false ) );
	}

	public function getIsGprPosted() {
		return $this->m_intIsGprPosted;
	}

	public function sqlIsGprPosted() {
		return ( true == isset( $this->m_intIsGprPosted ) ) ? ( string ) $this->m_intIsGprPosted : '0';
	}

	public function setIsInitialImport( $intIsInitialImport ) {
		$this->set( 'm_intIsInitialImport', CStrings::strToIntDef( $intIsInitialImport, NULL, false ) );
	}

	public function getIsInitialImport() {
		return $this->m_intIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_intIsInitialImport ) ) ? ( string ) $this->m_intIsInitialImport : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, post_month, calendar_days, financial_days, ar_start_date, ap_start_date, gl_start_date, ar_end_date, ap_end_date, gl_end_date, ar_locked_on, ap_locked_on, gl_locked_on, ar_soft_locked_on, ap_soft_locked_on, gl_soft_locked_on, vacancy_cached_on, rebuild_differential_cache, cache_initialized, has_cache, has_disabled_proration, use_market_rent_in_gpr, has_financial_data, gpr_book_basis, is_ar_rollback, is_ap_rollback, is_gl_rollback, is_gpr_posted, is_initial_import, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlCalendarDays() . ', ' .
 						$this->sqlFinancialDays() . ', ' .
 						$this->sqlArStartDate() . ', ' .
 						$this->sqlApStartDate() . ', ' .
 						$this->sqlGlStartDate() . ', ' .
 						$this->sqlArEndDate() . ', ' .
 						$this->sqlApEndDate() . ', ' .
 						$this->sqlGlEndDate() . ', ' .
 						$this->sqlArLockedOn() . ', ' .
 						$this->sqlApLockedOn() . ', ' .
 						$this->sqlGlLockedOn() . ', ' .
 						$this->sqlArSoftLockedOn() . ', ' .
 						$this->sqlApSoftLockedOn() . ', ' .
 						$this->sqlGlSoftLockedOn() . ', ' .
 						$this->sqlVacancyCachedOn() . ', ' .
 						$this->sqlRebuildDifferentialCache() . ', ' .
 						$this->sqlCacheInitialized() . ', ' .
 						$this->sqlHasCache() . ', ' .
 						$this->sqlHasDisabledProration() . ', ' .
 						$this->sqlUseMarketRentInGpr() . ', ' .
 						$this->sqlHasFinancialData() . ', ' .
 						$this->sqlGprBookBasis() . ', ' .
 						$this->sqlIsArRollback() . ', ' .
 						$this->sqlIsApRollback() . ', ' .
 						$this->sqlIsGlRollback() . ', ' .
 						$this->sqlIsGprPosted() . ', ' .
 						$this->sqlIsInitialImport() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_days = ' . $this->sqlCalendarDays() . ','; } elseif( true == array_key_exists( 'CalendarDays', $this->getChangedColumns() ) ) { $strSql .= ' calendar_days = ' . $this->sqlCalendarDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_days = ' . $this->sqlFinancialDays() . ','; } elseif( true == array_key_exists( 'FinancialDays', $this->getChangedColumns() ) ) { $strSql .= ' financial_days = ' . $this->sqlFinancialDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_start_date = ' . $this->sqlArStartDate() . ','; } elseif( true == array_key_exists( 'ArStartDate', $this->getChangedColumns() ) ) { $strSql .= ' ar_start_date = ' . $this->sqlArStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_start_date = ' . $this->sqlApStartDate() . ','; } elseif( true == array_key_exists( 'ApStartDate', $this->getChangedColumns() ) ) { $strSql .= ' ap_start_date = ' . $this->sqlApStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_start_date = ' . $this->sqlGlStartDate() . ','; } elseif( true == array_key_exists( 'GlStartDate', $this->getChangedColumns() ) ) { $strSql .= ' gl_start_date = ' . $this->sqlGlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_end_date = ' . $this->sqlArEndDate() . ','; } elseif( true == array_key_exists( 'ArEndDate', $this->getChangedColumns() ) ) { $strSql .= ' ar_end_date = ' . $this->sqlArEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_end_date = ' . $this->sqlApEndDate() . ','; } elseif( true == array_key_exists( 'ApEndDate', $this->getChangedColumns() ) ) { $strSql .= ' ap_end_date = ' . $this->sqlApEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_end_date = ' . $this->sqlGlEndDate() . ','; } elseif( true == array_key_exists( 'GlEndDate', $this->getChangedColumns() ) ) { $strSql .= ' gl_end_date = ' . $this->sqlGlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_locked_on = ' . $this->sqlArLockedOn() . ','; } elseif( true == array_key_exists( 'ArLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' ar_locked_on = ' . $this->sqlArLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_locked_on = ' . $this->sqlApLockedOn() . ','; } elseif( true == array_key_exists( 'ApLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' ap_locked_on = ' . $this->sqlApLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_locked_on = ' . $this->sqlGlLockedOn() . ','; } elseif( true == array_key_exists( 'GlLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' gl_locked_on = ' . $this->sqlGlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_soft_locked_on = ' . $this->sqlArSoftLockedOn() . ','; } elseif( true == array_key_exists( 'ArSoftLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' ar_soft_locked_on = ' . $this->sqlArSoftLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_soft_locked_on = ' . $this->sqlApSoftLockedOn() . ','; } elseif( true == array_key_exists( 'ApSoftLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' ap_soft_locked_on = ' . $this->sqlApSoftLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_soft_locked_on = ' . $this->sqlGlSoftLockedOn() . ','; } elseif( true == array_key_exists( 'GlSoftLockedOn', $this->getChangedColumns() ) ) { $strSql .= ' gl_soft_locked_on = ' . $this->sqlGlSoftLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_cached_on = ' . $this->sqlVacancyCachedOn() . ','; } elseif( true == array_key_exists( 'VacancyCachedOn', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_cached_on = ' . $this->sqlVacancyCachedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rebuild_differential_cache = ' . $this->sqlRebuildDifferentialCache() . ','; } elseif( true == array_key_exists( 'RebuildDifferentialCache', $this->getChangedColumns() ) ) { $strSql .= ' rebuild_differential_cache = ' . $this->sqlRebuildDifferentialCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cache_initialized = ' . $this->sqlCacheInitialized() . ','; } elseif( true == array_key_exists( 'CacheInitialized', $this->getChangedColumns() ) ) { $strSql .= ' cache_initialized = ' . $this->sqlCacheInitialized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_cache = ' . $this->sqlHasCache() . ','; } elseif( true == array_key_exists( 'HasCache', $this->getChangedColumns() ) ) { $strSql .= ' has_cache = ' . $this->sqlHasCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_disabled_proration = ' . $this->sqlHasDisabledProration() . ','; } elseif( true == array_key_exists( 'HasDisabledProration', $this->getChangedColumns() ) ) { $strSql .= ' has_disabled_proration = ' . $this->sqlHasDisabledProration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_market_rent_in_gpr = ' . $this->sqlUseMarketRentInGpr() . ','; } elseif( true == array_key_exists( 'UseMarketRentInGpr', $this->getChangedColumns() ) ) { $strSql .= ' use_market_rent_in_gpr = ' . $this->sqlUseMarketRentInGpr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_financial_data = ' . $this->sqlHasFinancialData() . ','; } elseif( true == array_key_exists( 'HasFinancialData', $this->getChangedColumns() ) ) { $strSql .= ' has_financial_data = ' . $this->sqlHasFinancialData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gpr_book_basis = ' . $this->sqlGprBookBasis() . ','; } elseif( true == array_key_exists( 'GprBookBasis', $this->getChangedColumns() ) ) { $strSql .= ' gpr_book_basis = ' . $this->sqlGprBookBasis() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ar_rollback = ' . $this->sqlIsArRollback() . ','; } elseif( true == array_key_exists( 'IsArRollback', $this->getChangedColumns() ) ) { $strSql .= ' is_ar_rollback = ' . $this->sqlIsArRollback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ap_rollback = ' . $this->sqlIsApRollback() . ','; } elseif( true == array_key_exists( 'IsApRollback', $this->getChangedColumns() ) ) { $strSql .= ' is_ap_rollback = ' . $this->sqlIsApRollback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gl_rollback = ' . $this->sqlIsGlRollback() . ','; } elseif( true == array_key_exists( 'IsGlRollback', $this->getChangedColumns() ) ) { $strSql .= ' is_gl_rollback = ' . $this->sqlIsGlRollback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gpr_posted = ' . $this->sqlIsGprPosted() . ','; } elseif( true == array_key_exists( 'IsGprPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_gpr_posted = ' . $this->sqlIsGprPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'post_month' => $this->getPostMonth(),
			'calendar_days' => $this->getCalendarDays(),
			'financial_days' => $this->getFinancialDays(),
			'ar_start_date' => $this->getArStartDate(),
			'ap_start_date' => $this->getApStartDate(),
			'gl_start_date' => $this->getGlStartDate(),
			'ar_end_date' => $this->getArEndDate(),
			'ap_end_date' => $this->getApEndDate(),
			'gl_end_date' => $this->getGlEndDate(),
			'ar_locked_on' => $this->getArLockedOn(),
			'ap_locked_on' => $this->getApLockedOn(),
			'gl_locked_on' => $this->getGlLockedOn(),
			'ar_soft_locked_on' => $this->getArSoftLockedOn(),
			'ap_soft_locked_on' => $this->getApSoftLockedOn(),
			'gl_soft_locked_on' => $this->getGlSoftLockedOn(),
			'vacancy_cached_on' => $this->getVacancyCachedOn(),
			'rebuild_differential_cache' => $this->getRebuildDifferentialCache(),
			'cache_initialized' => $this->getCacheInitialized(),
			'has_cache' => $this->getHasCache(),
			'has_disabled_proration' => $this->getHasDisabledProration(),
			'use_market_rent_in_gpr' => $this->getUseMarketRentInGpr(),
			'has_financial_data' => $this->getHasFinancialData(),
			'gpr_book_basis' => $this->getGprBookBasis(),
			'is_ar_rollback' => $this->getIsArRollback(),
			'is_ap_rollback' => $this->getIsApRollback(),
			'is_gl_rollback' => $this->getIsGlRollback(),
			'is_gpr_posted' => $this->getIsGprPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>