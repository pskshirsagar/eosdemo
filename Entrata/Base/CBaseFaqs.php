<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFaqs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFaqs extends CEosPluralBase {

	/**
	 * @return CFaq[]
	 */
	public static function fetchFaqs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFaq', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFaq
	 */
	public static function fetchFaq( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFaq', $objDatabase );
	}

	public static function fetchFaqCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'faqs', $objDatabase );
	}

	public static function fetchFaqByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFaq( sprintf( 'SELECT * FROM faqs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFaqsByCid( $intCid, $objDatabase ) {
		return self::fetchFaqs( sprintf( 'SELECT * FROM faqs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFaqsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchFaqs( sprintf( 'SELECT * FROM faqs WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>