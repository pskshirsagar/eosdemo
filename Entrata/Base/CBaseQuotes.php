<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CQuotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuotes extends CEosPluralBase {

	/**
	 * @return CQuote[]
	 */
	public static function fetchQuotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CQuote::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CQuote
	 */
	public static function fetchQuote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CQuote::class, $objDatabase );
	}

	public static function fetchQuoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'quotes', $objDatabase );
	}

	public static function fetchQuoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchQuote( sprintf( 'SELECT * FROM quotes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByCid( $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByApprovalCustomerIdByCid( $intApprovalCustomerId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE approval_customer_id = %d AND cid = %d', ( int ) $intApprovalCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByLeasingTierSpecialIdByCid( $intLeasingTierSpecialId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE leasing_tier_special_id = %d AND cid = %d', ( int ) $intLeasingTierSpecialId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesBySelectedQuoteLeaseTermIdByCid( $intSelectedQuoteLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE selected_quote_lease_term_id = %d AND cid = %d', ( int ) $intSelectedQuoteLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByEmailEventIdByCid( $intEmailEventId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE email_event_id = %d AND cid = %d', ( int ) $intEmailEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuotesByMappingRenewalTierIdByCid( $intMappingRenewalTierId, $intCid, $objDatabase ) {
		return self::fetchQuotes( sprintf( 'SELECT * FROM quotes WHERE mapping_renewal_tier_id = %d AND cid = %d', ( int ) $intMappingRenewalTierId, ( int ) $intCid ), $objDatabase );
	}

}
?>