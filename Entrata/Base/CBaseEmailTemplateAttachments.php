<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmailTemplateAttachments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplateAttachments extends CEosPluralBase {

	/**
	 * @return CEmailTemplateAttachment[]
	 */
	public static function fetchEmailTemplateAttachments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CEmailTemplateAttachment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplateAttachment
	 */
	public static function fetchEmailTemplateAttachment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmailTemplateAttachment::class, $objDatabase );
	}

	public static function fetchEmailTemplateAttachmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_attachments', $objDatabase );
	}

	public static function fetchEmailTemplateAttachmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateAttachment( sprintf( 'SELECT * FROM email_template_attachments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateAttachmentsByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplateAttachments( sprintf( 'SELECT * FROM email_template_attachments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateAttachmentsByEmailTemplateIdByCid( $intEmailTemplateId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateAttachments( sprintf( 'SELECT * FROM email_template_attachments WHERE email_template_id = %d AND cid = %d', ( int ) $intEmailTemplateId, ( int ) $intCid ), $objDatabase );
	}

}
?>