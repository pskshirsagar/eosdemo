<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountMonthNoteTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetGlAccountMonthNoteTypes extends CEosPluralBase {

	/**
	 * @return CBudgetGlAccountMonthNoteType[]
	 */
	public static function fetchBudgetGlAccountMonthNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetGlAccountMonthNoteType::class, $objDatabase );
	}

	/**
	 * @return CBudgetGlAccountMonthNoteType
	 */
	public static function fetchBudgetGlAccountMonthNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetGlAccountMonthNoteType::class, $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_gl_account_month_note_types', $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNoteTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNoteType( sprintf( 'SELECT * FROM budget_gl_account_month_note_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>