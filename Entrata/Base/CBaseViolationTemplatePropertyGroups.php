<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationTemplatePropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationTemplatePropertyGroups extends CEosPluralBase {

	/**
	 * @return CViolationTemplatePropertyGroup[]
	 */
	public static function fetchViolationTemplatePropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolationTemplatePropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolationTemplatePropertyGroup
	 */
	public static function fetchViolationTemplatePropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationTemplatePropertyGroup::class, $objDatabase );
	}

	public static function fetchViolationTemplatePropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_template_property_groups', $objDatabase );
	}

	public static function fetchViolationTemplatePropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplatePropertyGroup( sprintf( 'SELECT * FROM violation_template_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplatePropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchViolationTemplatePropertyGroups( sprintf( 'SELECT * FROM violation_template_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplatePropertyGroupsByViolationTemplateIdByCid( $intViolationTemplateId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplatePropertyGroups( sprintf( 'SELECT * FROM violation_template_property_groups WHERE violation_template_id = %d AND cid = %d', ( int ) $intViolationTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplatePropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplatePropertyGroups( sprintf( 'SELECT * FROM violation_template_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>