<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenanceTemplates
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenanceTemplates extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenanceTemplate[]
	 */
	public static function fetchDefaultMaintenanceTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenanceTemplate::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenanceTemplate
	 */
	public static function fetchDefaultMaintenanceTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenanceTemplate::class, $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_templates', $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplateById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplate( sprintf( 'SELECT * FROM default_maintenance_templates WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByDefaultMaintenanceTemplateId( $intDefaultMaintenanceTemplateId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE default_maintenance_template_id = %d', $intDefaultMaintenanceTemplateId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByDefaultMaintenancePriorityId( $intDefaultMaintenancePriorityId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE default_maintenance_priority_id = %d', $intDefaultMaintenancePriorityId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByDefaultMaintenanceStatusId( $intDefaultMaintenanceStatusId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE default_maintenance_status_id = %d', $intDefaultMaintenanceStatusId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByDefaultMaintenanceLocationId( $intDefaultMaintenanceLocationId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE default_maintenance_location_id = %d', $intDefaultMaintenanceLocationId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByDefaultMaintenanceProblemId( $intDefaultMaintenanceProblemId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE default_maintenance_problem_id = %d', $intDefaultMaintenanceProblemId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceTemplatesByMaintenanceRequestTypeId( $intMaintenanceRequestTypeId, $objDatabase ) {
		return self::fetchDefaultMaintenanceTemplates( sprintf( 'SELECT * FROM default_maintenance_templates WHERE maintenance_request_type_id = %d', $intMaintenanceRequestTypeId ), $objDatabase );
	}

}
?>