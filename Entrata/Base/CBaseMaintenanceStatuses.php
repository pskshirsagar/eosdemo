<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceStatuses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceStatuses extends CEosPluralBase {

	/**
	 * @return CMaintenanceStatus[]
	 */
	public static function fetchMaintenanceStatuses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceStatus::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceStatus
	 */
	public static function fetchMaintenanceStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceStatus::class, $objDatabase );
	}

	public static function fetchMaintenanceStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_statuses', $objDatabase );
	}

	public static function fetchMaintenanceStatusByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceStatus( sprintf( 'SELECT * FROM maintenance_statuses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceStatusesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceStatuses( sprintf( 'SELECT * FROM maintenance_statuses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceStatusesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceStatuses( sprintf( 'SELECT * FROM maintenance_statuses WHERE integration_database_id = %d AND cid = %d', $intIntegrationDatabaseId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceStatusesByMaintenanceStatusTypeIdByCid( $intMaintenanceStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceStatuses( sprintf( 'SELECT * FROM maintenance_statuses WHERE maintenance_status_type_id = %d AND cid = %d', $intMaintenanceStatusTypeId, $intCid ), $objDatabase );
	}

}
?>