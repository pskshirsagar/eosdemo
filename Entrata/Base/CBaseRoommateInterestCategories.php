<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterestCategories
 * Do not add any new functions to this class.
 */

class CBaseRoommateInterestCategories extends CEosPluralBase {

	/**
	 * @return CRoommateInterestCategory[]
	 */
	public static function fetchRoommateInterestCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRoommateInterestCategory::class, $objDatabase );
	}

	/**
	 * @return CRoommateInterestCategory
	 */
	public static function fetchRoommateInterestCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRoommateInterestCategory::class, $objDatabase );
	}

	public static function fetchRoommateInterestCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roommate_interest_categories', $objDatabase );
	}

	public static function fetchRoommateInterestCategoryById( $intId, $objDatabase ) {
		return self::fetchRoommateInterestCategory( sprintf( 'SELECT * FROM roommate_interest_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>