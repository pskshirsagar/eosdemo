<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeLease extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intApPayeeId;
    protected $m_intReimbursedPropertyId;
    protected $m_intPropertyId;
    protected $m_intOwnerId;
    protected $m_intLeaseId;
    protected $m_boolIsOwner;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_boolIsOwner = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->m_intApPayeeId = trim( $arrValues['ap_payee_id'] ); else if( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
        if( isset( $arrValues['reimbursed_property_id'] ) && $boolDirectSet ) $this->m_intReimbursedPropertyId = trim( $arrValues['reimbursed_property_id'] ); else if( isset( $arrValues['reimbursed_property_id'] ) ) $this->setReimbursedPropertyId( $arrValues['reimbursed_property_id'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->m_intOwnerId = trim( $arrValues['owner_id'] ); else if( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
        if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->m_intLeaseId = trim( $arrValues['lease_id'] ); else if( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
        if( isset( $arrValues['is_owner'] ) && $boolDirectSet ) $this->m_boolIsOwner = trim( stripcslashes( $arrValues['is_owner'] ) ); else if( isset( $arrValues['is_owner'] ) ) $this->setIsOwner( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_owner'] ) : $arrValues['is_owner'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setApPayeeId( $intApPayeeId ) {
        $this->m_intApPayeeId = CStrings::strToIntDef( $intApPayeeId, NULL, false );
    }

    public function getApPayeeId() {
        return $this->m_intApPayeeId;
    }

    public function sqlApPayeeId() {
        return ( true == isset( $this->m_intApPayeeId ) ) ? (string) $this->m_intApPayeeId : 'NULL';
    }

    public function setReimbursedPropertyId( $intReimbursedPropertyId ) {
        $this->m_intReimbursedPropertyId = CStrings::strToIntDef( $intReimbursedPropertyId, NULL, false );
    }

    public function getReimbursedPropertyId() {
        return $this->m_intReimbursedPropertyId;
    }

    public function sqlReimbursedPropertyId() {
        return ( true == isset( $this->m_intReimbursedPropertyId ) ) ? (string) $this->m_intReimbursedPropertyId : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setOwnerId( $intOwnerId ) {
        $this->m_intOwnerId = CStrings::strToIntDef( $intOwnerId, NULL, false );
    }

    public function getOwnerId() {
        return $this->m_intOwnerId;
    }

    public function sqlOwnerId() {
        return ( true == isset( $this->m_intOwnerId ) ) ? (string) $this->m_intOwnerId : 'NULL';
    }

    public function setLeaseId( $intLeaseId ) {
        $this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
    }

    public function getLeaseId() {
        return $this->m_intLeaseId;
    }

    public function sqlLeaseId() {
        return ( true == isset( $this->m_intLeaseId ) ) ? (string) $this->m_intLeaseId : 'NULL';
    }

    public function setIsOwner( $boolIsOwner ) {
        $this->m_boolIsOwner = CStrings::strToBool( $boolIsOwner );
    }

    public function getIsOwner() {
        return $this->m_boolIsOwner;
    }

    public function sqlIsOwner() {
        return ( true == isset( $this->m_boolIsOwner ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOwner ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.ap_payee_leases_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.ap_payee_leases					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlApPayeeId() . ', ' . 		                $this->sqlReimbursedPropertyId() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlOwnerId() . ', ' . 		                $this->sqlLeaseId() . ', ' . 		                $this->sqlIsOwner() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.ap_payee_leases
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApPayeeId() ) != $this->getOriginalValueByFieldName ( 'ap_payee_id' ) ) { $arrstrOriginalValueChanges['ap_payee_id'] = $this->sqlApPayeeId(); $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursed_property_id = ' . $this->sqlReimbursedPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlReimbursedPropertyId() ) != $this->getOriginalValueByFieldName ( 'reimbursed_property_id' ) ) { $arrstrOriginalValueChanges['reimbursed_property_id'] = $this->sqlReimbursedPropertyId(); $strSql .= ' reimbursed_property_id = ' . $this->sqlReimbursedPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOwnerId() ) != $this->getOriginalValueByFieldName ( 'owner_id' ) ) { $arrstrOriginalValueChanges['owner_id'] = $this->sqlOwnerId(); $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseId() ) != $this->getOriginalValueByFieldName ( 'lease_id' ) ) { $arrstrOriginalValueChanges['lease_id'] = $this->sqlLeaseId(); $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_owner = ' . $this->sqlIsOwner() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsOwner() ) != $this->getOriginalValueByFieldName ( 'is_owner' ) ) { $arrstrOriginalValueChanges['is_owner'] = $this->sqlIsOwner(); $strSql .= ' is_owner = ' . $this->sqlIsOwner() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.ap_payee_leases WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.ap_payee_leases_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'ap_payee_id' => $this->getApPayeeId(),
        	'reimbursed_property_id' => $this->getReimbursedPropertyId(),
        	'property_id' => $this->getPropertyId(),
        	'owner_id' => $this->getOwnerId(),
        	'lease_id' => $this->getLeaseId(),
        	'is_owner' => $this->getIsOwner(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>