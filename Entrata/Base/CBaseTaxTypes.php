<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxTypes
 * Do not add any new functions to this class.
 */

class CBaseTaxTypes extends CEosPluralBase {

	/**
	 * @return CTaxType[]
	 */
	public static function fetchTaxTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaxType::class, $objDatabase );
	}

	/**
	 * @return CTaxType
	 */
	public static function fetchTaxType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxType::class, $objDatabase );
	}

	public static function fetchTaxTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tax_types', $objDatabase );
	}

	public static function fetchTaxTypeById( $intId, $objDatabase ) {
		return self::fetchTaxType( sprintf( 'SELECT * FROM tax_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>