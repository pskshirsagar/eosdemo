<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCamPeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCamPeriods extends CEosPluralBase {

	/**
	 * @return CPropertyCamPeriod[]
	 */
	public static function fetchPropertyCamPeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCamPeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCamPeriod
	 */
	public static function fetchPropertyCamPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCamPeriod', $objDatabase );
	}

	public static function fetchPropertyCamPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_cam_periods', $objDatabase );
	}

	public static function fetchPropertyCamPeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPeriod( sprintf( 'SELECT * FROM property_cam_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCamPeriods( sprintf( 'SELECT * FROM property_cam_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPeriods( sprintf( 'SELECT * FROM property_cam_periods WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPeriodsByCamPeriodIdByCid( $intCamPeriodId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPeriods( sprintf( 'SELECT * FROM property_cam_periods WHERE cam_period_id = %d AND cid = %d', ( int ) $intCamPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>