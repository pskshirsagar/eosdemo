<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySettingLog extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_setting_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertySettingKeyId;
	protected $m_intPropertyId;
	protected $m_strAction;
	protected $m_strNewValue;
	protected $m_strNewText;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_setting_key_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertySettingKeyId', trim( $arrValues['property_setting_key_id'] ) ); elseif( isset( $arrValues['property_setting_key_id'] ) ) $this->setPropertySettingKeyId( $arrValues['property_setting_key_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['new_value'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNewValue', trim( stripcslashes( $arrValues['new_value'] ) ) ); elseif( isset( $arrValues['new_value'] ) ) $this->setNewValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_value'] ) : $arrValues['new_value'] );
		if( isset( $arrValues['new_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNewText', trim( stripcslashes( $arrValues['new_text'] ) ) ); elseif( isset( $arrValues['new_text'] ) ) $this->setNewText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_text'] ) : $arrValues['new_text'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertySettingKeyId( $intPropertySettingKeyId ) {
		$this->set( 'm_intPropertySettingKeyId', CStrings::strToIntDef( $intPropertySettingKeyId, NULL, false ) );
	}

	public function getPropertySettingKeyId() {
		return $this->m_intPropertySettingKeyId;
	}

	public function sqlPropertySettingKeyId() {
		return ( true == isset( $this->m_intPropertySettingKeyId ) ) ? ( string ) $this->m_intPropertySettingKeyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setNewValue( $strNewValue, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNewValue', CStrings::strTrimDef( $strNewValue, -1, NULL, true ), $strLocaleCode );
	}

	public function getNewValue( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNewValue', $strLocaleCode );
	}

	public function sqlNewValue() {
		return ( true == isset( $this->m_strNewValue ) ) ? '\'' . addslashes( $this->m_strNewValue ) . '\'' : 'NULL';
	}

	public function setNewText( $strNewText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNewText', CStrings::strTrimDef( $strNewText, -1, NULL, true ), $strLocaleCode );
	}

	public function getNewText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNewText', $strLocaleCode );
	}

	public function sqlNewText() {
		return ( true == isset( $this->m_strNewText ) ) ? '\'' . addslashes( $this->m_strNewText ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_setting_key_id, property_id, action, new_value, new_text, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertySettingKeyId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlAction() . ', ' .
						$this->sqlNewValue() . ', ' .
						$this->sqlNewText() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId(). ',' ; } elseif( true == array_key_exists( 'PropertySettingKeyId', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction(). ',' ; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_value = ' . $this->sqlNewValue(). ',' ; } elseif( true == array_key_exists( 'NewValue', $this->getChangedColumns() ) ) { $strSql .= ' new_value = ' . $this->sqlNewValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_text = ' . $this->sqlNewText(). ',' ; } elseif( true == array_key_exists( 'NewText', $this->getChangedColumns() ) ) { $strSql .= ' new_text = ' . $this->sqlNewText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_setting_key_id' => $this->getPropertySettingKeyId(),
			'property_id' => $this->getPropertyId(),
			'action' => $this->getAction(),
			'new_value' => $this->getNewValue(),
			'new_text' => $this->getNewText(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>