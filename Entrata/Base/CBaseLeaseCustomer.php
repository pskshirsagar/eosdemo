<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCustomer extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_customers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intCustomerRelationshipId;
	protected $m_fltRefundPercent;
	protected $m_strRemotePrimaryKey;
	protected $m_intIsUngrouped;
	protected $m_intAllowAccessToUnit;
	protected $m_intIncludeOnFmoStatement;
	protected $m_fltGuaranteePercent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintResponsibleLedgerFilterIds;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsUngrouped = '0';
		$this->m_intAllowAccessToUnit = '0';
		$this->m_intIncludeOnFmoStatement = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipId', trim( $arrValues['customer_relationship_id'] ) ); elseif( isset( $arrValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrValues['customer_relationship_id'] );
		if( isset( $arrValues['refund_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRefundPercent', trim( $arrValues['refund_percent'] ) ); elseif( isset( $arrValues['refund_percent'] ) ) $this->setRefundPercent( $arrValues['refund_percent'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['is_ungrouped'] ) && $boolDirectSet ) $this->set( 'm_intIsUngrouped', trim( $arrValues['is_ungrouped'] ) ); elseif( isset( $arrValues['is_ungrouped'] ) ) $this->setIsUngrouped( $arrValues['is_ungrouped'] );
		if( isset( $arrValues['allow_access_to_unit'] ) && $boolDirectSet ) $this->set( 'm_intAllowAccessToUnit', trim( $arrValues['allow_access_to_unit'] ) ); elseif( isset( $arrValues['allow_access_to_unit'] ) ) $this->setAllowAccessToUnit( $arrValues['allow_access_to_unit'] );
		if( isset( $arrValues['include_on_fmo_statement'] ) && $boolDirectSet ) $this->set( 'm_intIncludeOnFmoStatement', trim( $arrValues['include_on_fmo_statement'] ) ); elseif( isset( $arrValues['include_on_fmo_statement'] ) ) $this->setIncludeOnFmoStatement( $arrValues['include_on_fmo_statement'] );
		if( isset( $arrValues['guarantee_percent'] ) && $boolDirectSet ) $this->set( 'm_fltGuaranteePercent', trim( $arrValues['guarantee_percent'] ) ); elseif( isset( $arrValues['guarantee_percent'] ) ) $this->setGuaranteePercent( $arrValues['guarantee_percent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['responsible_ledger_filter_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintResponsibleLedgerFilterIds', trim( $arrValues['responsible_ledger_filter_ids'] ) ); elseif( isset( $arrValues['responsible_ledger_filter_ids'] ) ) $this->setResponsibleLedgerFilterIds( $arrValues['responsible_ledger_filter_ids'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->set( 'm_intCustomerRelationshipId', CStrings::strToIntDef( $intCustomerRelationshipId, NULL, false ) );
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function sqlCustomerRelationshipId() {
		return ( true == isset( $this->m_intCustomerRelationshipId ) ) ? ( string ) $this->m_intCustomerRelationshipId : 'NULL';
	}

	public function setRefundPercent( $fltRefundPercent ) {
		$this->set( 'm_fltRefundPercent', CStrings::strToFloatDef( $fltRefundPercent, NULL, false, 2 ) );
	}

	public function getRefundPercent() {
		return $this->m_fltRefundPercent;
	}

	public function sqlRefundPercent() {
		return ( true == isset( $this->m_fltRefundPercent ) ) ? ( string ) $this->m_fltRefundPercent : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setIsUngrouped( $intIsUngrouped ) {
		$this->set( 'm_intIsUngrouped', CStrings::strToIntDef( $intIsUngrouped, NULL, false ) );
	}

	public function getIsUngrouped() {
		return $this->m_intIsUngrouped;
	}

	public function sqlIsUngrouped() {
		return ( true == isset( $this->m_intIsUngrouped ) ) ? ( string ) $this->m_intIsUngrouped : '0';
	}

	public function setAllowAccessToUnit( $intAllowAccessToUnit ) {
		$this->set( 'm_intAllowAccessToUnit', CStrings::strToIntDef( $intAllowAccessToUnit, NULL, false ) );
	}

	public function getAllowAccessToUnit() {
		return $this->m_intAllowAccessToUnit;
	}

	public function sqlAllowAccessToUnit() {
		return ( true == isset( $this->m_intAllowAccessToUnit ) ) ? ( string ) $this->m_intAllowAccessToUnit : '0';
	}

	public function setIncludeOnFmoStatement( $intIncludeOnFmoStatement ) {
		$this->set( 'm_intIncludeOnFmoStatement', CStrings::strToIntDef( $intIncludeOnFmoStatement, NULL, false ) );
	}

	public function getIncludeOnFmoStatement() {
		return $this->m_intIncludeOnFmoStatement;
	}

	public function sqlIncludeOnFmoStatement() {
		return ( true == isset( $this->m_intIncludeOnFmoStatement ) ) ? ( string ) $this->m_intIncludeOnFmoStatement : '0';
	}

	public function setGuaranteePercent( $fltGuaranteePercent ) {
		$this->set( 'm_fltGuaranteePercent', CStrings::strToFloatDef( $fltGuaranteePercent, NULL, false, 2 ) );
	}

	public function getGuaranteePercent() {
		return $this->m_fltGuaranteePercent;
	}

	public function sqlGuaranteePercent() {
		return ( true == isset( $this->m_fltGuaranteePercent ) ) ? ( string ) $this->m_fltGuaranteePercent : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setResponsibleLedgerFilterIds( $arrintResponsibleLedgerFilterIds ) {
		$this->set( 'm_arrintResponsibleLedgerFilterIds', CStrings::strToArrIntDef( $arrintResponsibleLedgerFilterIds, NULL ) );
	}

	public function getResponsibleLedgerFilterIds() {
		return $this->m_arrintResponsibleLedgerFilterIds;
	}

	public function sqlResponsibleLedgerFilterIds() {
		return ( true == isset( $this->m_arrintResponsibleLedgerFilterIds ) && true == valArr( $this->m_arrintResponsibleLedgerFilterIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintResponsibleLedgerFilterIds, NULL ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, customer_id, lease_status_type_id, customer_type_id, customer_relationship_id, refund_percent, remote_primary_key, is_ungrouped, allow_access_to_unit, include_on_fmo_statement, guarantee_percent, updated_by, updated_on, created_by, created_on, responsible_ledger_filter_ids, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlCustomerTypeId() . ', ' .
						$this->sqlCustomerRelationshipId() . ', ' .
						$this->sqlRefundPercent() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlIsUngrouped() . ', ' .
						$this->sqlAllowAccessToUnit() . ', ' .
						$this->sqlIncludeOnFmoStatement() . ', ' .
						$this->sqlGuaranteePercent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlResponsibleLedgerFilterIds() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId(). ',' ; } elseif( true == array_key_exists( 'CustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_percent = ' . $this->sqlRefundPercent(). ',' ; } elseif( true == array_key_exists( 'RefundPercent', $this->getChangedColumns() ) ) { $strSql .= ' refund_percent = ' . $this->sqlRefundPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ungrouped = ' . $this->sqlIsUngrouped(). ',' ; } elseif( true == array_key_exists( 'IsUngrouped', $this->getChangedColumns() ) ) { $strSql .= ' is_ungrouped = ' . $this->sqlIsUngrouped() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_access_to_unit = ' . $this->sqlAllowAccessToUnit(). ',' ; } elseif( true == array_key_exists( 'AllowAccessToUnit', $this->getChangedColumns() ) ) { $strSql .= ' allow_access_to_unit = ' . $this->sqlAllowAccessToUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_on_fmo_statement = ' . $this->sqlIncludeOnFmoStatement(). ',' ; } elseif( true == array_key_exists( 'IncludeOnFmoStatement', $this->getChangedColumns() ) ) { $strSql .= ' include_on_fmo_statement = ' . $this->sqlIncludeOnFmoStatement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guarantee_percent = ' . $this->sqlGuaranteePercent(). ',' ; } elseif( true == array_key_exists( 'GuaranteePercent', $this->getChangedColumns() ) ) { $strSql .= ' guarantee_percent = ' . $this->sqlGuaranteePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responsible_ledger_filter_ids = ' . $this->sqlResponsibleLedgerFilterIds(). ',' ; } elseif( true == array_key_exists( 'ResponsibleLedgerFilterIds', $this->getChangedColumns() ) ) { $strSql .= ' responsible_ledger_filter_ids = ' . $this->sqlResponsibleLedgerFilterIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'customer_relationship_id' => $this->getCustomerRelationshipId(),
			'refund_percent' => $this->getRefundPercent(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'is_ungrouped' => $this->getIsUngrouped(),
			'allow_access_to_unit' => $this->getAllowAccessToUnit(),
			'include_on_fmo_statement' => $this->getIncludeOnFmoStatement(),
			'guarantee_percent' => $this->getGuaranteePercent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'responsible_ledger_filter_ids' => $this->getResponsibleLedgerFilterIds(),
			'details' => $this->getDetails()
		);
	}

}
?>