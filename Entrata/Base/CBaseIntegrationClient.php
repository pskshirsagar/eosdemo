<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationClient extends CEosSingularBase {

	const TABLE_NAME = 'public.integration_clients';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intIntegrationClientTypeId;
	protected $m_intIntegrationClientStatusTypeId;
	protected $m_intIntegrationSyncTypeId;
	protected $m_intIntegrationServiceId;
	protected $m_intIntegrationPeriodId;
	protected $m_strServiceUri;
	protected $m_strLastSyncOn;
	protected $m_strHourlyRunValues;
	protected $m_strSyncFromDate;
	protected $m_strExpiresOn;
	protected $m_intOrderNum;
	protected $m_boolIsPropertySpecific;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPropertySpecific = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['integration_client_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientTypeId', trim( $arrValues['integration_client_type_id'] ) ); elseif( isset( $arrValues['integration_client_type_id'] ) ) $this->setIntegrationClientTypeId( $arrValues['integration_client_type_id'] );
		if( isset( $arrValues['integration_client_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientStatusTypeId', trim( $arrValues['integration_client_status_type_id'] ) ); elseif( isset( $arrValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrValues['integration_client_status_type_id'] );
		if( isset( $arrValues['integration_sync_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationSyncTypeId', trim( $arrValues['integration_sync_type_id'] ) ); elseif( isset( $arrValues['integration_sync_type_id'] ) ) $this->setIntegrationSyncTypeId( $arrValues['integration_sync_type_id'] );
		if( isset( $arrValues['integration_service_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationServiceId', trim( $arrValues['integration_service_id'] ) ); elseif( isset( $arrValues['integration_service_id'] ) ) $this->setIntegrationServiceId( $arrValues['integration_service_id'] );
		if( isset( $arrValues['integration_period_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationPeriodId', trim( $arrValues['integration_period_id'] ) ); elseif( isset( $arrValues['integration_period_id'] ) ) $this->setIntegrationPeriodId( $arrValues['integration_period_id'] );
		if( isset( $arrValues['service_uri'] ) && $boolDirectSet ) $this->set( 'm_strServiceUri', trim( stripcslashes( $arrValues['service_uri'] ) ) ); elseif( isset( $arrValues['service_uri'] ) ) $this->setServiceUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_uri'] ) : $arrValues['service_uri'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['hourly_run_values'] ) && $boolDirectSet ) $this->set( 'm_strHourlyRunValues', trim( stripcslashes( $arrValues['hourly_run_values'] ) ) ); elseif( isset( $arrValues['hourly_run_values'] ) ) $this->setHourlyRunValues( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hourly_run_values'] ) : $arrValues['hourly_run_values'] );
		if( isset( $arrValues['sync_from_date'] ) && $boolDirectSet ) $this->set( 'm_strSyncFromDate', trim( $arrValues['sync_from_date'] ) ); elseif( isset( $arrValues['sync_from_date'] ) ) $this->setSyncFromDate( $arrValues['sync_from_date'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_property_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsPropertySpecific', trim( stripcslashes( $arrValues['is_property_specific'] ) ) ); elseif( isset( $arrValues['is_property_specific'] ) ) $this->setIsPropertySpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property_specific'] ) : $arrValues['is_property_specific'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setIntegrationClientTypeId( $intIntegrationClientTypeId ) {
		$this->set( 'm_intIntegrationClientTypeId', CStrings::strToIntDef( $intIntegrationClientTypeId, NULL, false ) );
	}

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
	}

	public function sqlIntegrationClientTypeId() {
		return ( true == isset( $this->m_intIntegrationClientTypeId ) ) ? ( string ) $this->m_intIntegrationClientTypeId : 'NULL';
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		$this->set( 'm_intIntegrationClientStatusTypeId', CStrings::strToIntDef( $intIntegrationClientStatusTypeId, NULL, false ) );
	}

	public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

	public function sqlIntegrationClientStatusTypeId() {
		return ( true == isset( $this->m_intIntegrationClientStatusTypeId ) ) ? ( string ) $this->m_intIntegrationClientStatusTypeId : 'NULL';
	}

	public function setIntegrationSyncTypeId( $intIntegrationSyncTypeId ) {
		$this->set( 'm_intIntegrationSyncTypeId', CStrings::strToIntDef( $intIntegrationSyncTypeId, NULL, false ) );
	}

	public function getIntegrationSyncTypeId() {
		return $this->m_intIntegrationSyncTypeId;
	}

	public function sqlIntegrationSyncTypeId() {
		return ( true == isset( $this->m_intIntegrationSyncTypeId ) ) ? ( string ) $this->m_intIntegrationSyncTypeId : 'NULL';
	}

	public function setIntegrationServiceId( $intIntegrationServiceId ) {
		$this->set( 'm_intIntegrationServiceId', CStrings::strToIntDef( $intIntegrationServiceId, NULL, false ) );
	}

	public function getIntegrationServiceId() {
		return $this->m_intIntegrationServiceId;
	}

	public function sqlIntegrationServiceId() {
		return ( true == isset( $this->m_intIntegrationServiceId ) ) ? ( string ) $this->m_intIntegrationServiceId : 'NULL';
	}

	public function setIntegrationPeriodId( $intIntegrationPeriodId ) {
		$this->set( 'm_intIntegrationPeriodId', CStrings::strToIntDef( $intIntegrationPeriodId, NULL, false ) );
	}

	public function getIntegrationPeriodId() {
		return $this->m_intIntegrationPeriodId;
	}

	public function sqlIntegrationPeriodId() {
		return ( true == isset( $this->m_intIntegrationPeriodId ) ) ? ( string ) $this->m_intIntegrationPeriodId : 'NULL';
	}

	public function setServiceUri( $strServiceUri ) {
		$this->set( 'm_strServiceUri', CStrings::strTrimDef( $strServiceUri, 4096, NULL, true ) );
	}

	public function getServiceUri() {
		return $this->m_strServiceUri;
	}

	public function sqlServiceUri() {
		return ( true == isset( $this->m_strServiceUri ) ) ? '\'' . addslashes( $this->m_strServiceUri ) . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setHourlyRunValues( $strHourlyRunValues ) {
		$this->set( 'm_strHourlyRunValues', CStrings::strTrimDef( $strHourlyRunValues, 4096, NULL, true ) );
	}

	public function getHourlyRunValues() {
		return $this->m_strHourlyRunValues;
	}

	public function sqlHourlyRunValues() {
		return ( true == isset( $this->m_strHourlyRunValues ) ) ? '\'' . addslashes( $this->m_strHourlyRunValues ) . '\'' : 'NULL';
	}

	public function setSyncFromDate( $strSyncFromDate ) {
		$this->set( 'm_strSyncFromDate', CStrings::strTrimDef( $strSyncFromDate, -1, NULL, true ) );
	}

	public function getSyncFromDate() {
		return $this->m_strSyncFromDate;
	}

	public function sqlSyncFromDate() {
		return ( true == isset( $this->m_strSyncFromDate ) ) ? '\'' . $this->m_strSyncFromDate . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsPropertySpecific( $boolIsPropertySpecific ) {
		$this->set( 'm_boolIsPropertySpecific', CStrings::strToBool( $boolIsPropertySpecific ) );
	}

	public function getIsPropertySpecific() {
		return $this->m_boolIsPropertySpecific;
	}

	public function sqlIsPropertySpecific() {
		return ( true == isset( $this->m_boolIsPropertySpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPropertySpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_database_id, integration_client_type_id, integration_client_status_type_id, integration_sync_type_id, integration_service_id, integration_period_id, service_uri, last_sync_on, hourly_run_values, sync_from_date, expires_on, order_num, is_property_specific, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIntegrationDatabaseId() . ', ' .
 						$this->sqlIntegrationClientTypeId() . ', ' .
 						$this->sqlIntegrationClientStatusTypeId() . ', ' .
 						$this->sqlIntegrationSyncTypeId() . ', ' .
 						$this->sqlIntegrationServiceId() . ', ' .
 						$this->sqlIntegrationPeriodId() . ', ' .
 						$this->sqlServiceUri() . ', ' .
 						$this->sqlLastSyncOn() . ', ' .
 						$this->sqlHourlyRunValues() . ', ' .
 						$this->sqlSyncFromDate() . ', ' .
 						$this->sqlExpiresOn() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlIsPropertySpecific() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_type_id = ' . $this->sqlIntegrationClientTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationClientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_type_id = ' . $this->sqlIntegrationClientTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_status_type_id = ' . $this->sqlIntegrationClientStatusTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationClientStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_status_type_id = ' . $this->sqlIntegrationClientStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_sync_type_id = ' . $this->sqlIntegrationSyncTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationSyncTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_sync_type_id = ' . $this->sqlIntegrationSyncTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; } elseif( true == array_key_exists( 'IntegrationServiceId', $this->getChangedColumns() ) ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_period_id = ' . $this->sqlIntegrationPeriodId() . ','; } elseif( true == array_key_exists( 'IntegrationPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' integration_period_id = ' . $this->sqlIntegrationPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_uri = ' . $this->sqlServiceUri() . ','; } elseif( true == array_key_exists( 'ServiceUri', $this->getChangedColumns() ) ) { $strSql .= ' service_uri = ' . $this->sqlServiceUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_run_values = ' . $this->sqlHourlyRunValues() . ','; } elseif( true == array_key_exists( 'HourlyRunValues', $this->getChangedColumns() ) ) { $strSql .= ' hourly_run_values = ' . $this->sqlHourlyRunValues() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_from_date = ' . $this->sqlSyncFromDate() . ','; } elseif( true == array_key_exists( 'SyncFromDate', $this->getChangedColumns() ) ) { $strSql .= ' sync_from_date = ' . $this->sqlSyncFromDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_property_specific = ' . $this->sqlIsPropertySpecific() . ','; } elseif( true == array_key_exists( 'IsPropertySpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_property_specific = ' . $this->sqlIsPropertySpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'integration_client_type_id' => $this->getIntegrationClientTypeId(),
			'integration_client_status_type_id' => $this->getIntegrationClientStatusTypeId(),
			'integration_sync_type_id' => $this->getIntegrationSyncTypeId(),
			'integration_service_id' => $this->getIntegrationServiceId(),
			'integration_period_id' => $this->getIntegrationPeriodId(),
			'service_uri' => $this->getServiceUri(),
			'last_sync_on' => $this->getLastSyncOn(),
			'hourly_run_values' => $this->getHourlyRunValues(),
			'sync_from_date' => $this->getSyncFromDate(),
			'expires_on' => $this->getExpiresOn(),
			'order_num' => $this->getOrderNum(),
			'is_property_specific' => $this->getIsPropertySpecific(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>