<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySellingPointCategories
 * Do not add any new functions to this class.
 */

class CBasePropertySellingPointCategories extends CEosPluralBase {

	/**
	 * @return CPropertySellingPointCategory[]
	 */
	public static function fetchPropertySellingPointCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertySellingPointCategory::class, $objDatabase );
	}

	/**
	 * @return CPropertySellingPointCategory
	 */
	public static function fetchPropertySellingPointCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertySellingPointCategory::class, $objDatabase );
	}

	public static function fetchPropertySellingPointCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_selling_point_categories', $objDatabase );
	}

	public static function fetchPropertySellingPointCategoryById( $intId, $objDatabase ) {
		return self::fetchPropertySellingPointCategory( sprintf( 'SELECT * FROM property_selling_point_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>