<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxableEntities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTaxableEntities extends CEosPluralBase {

	/**
	 * @return CTaxableEntity[]
	 */
	public static function fetchTaxableEntities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTaxableEntity::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTaxableEntity
	 */
	public static function fetchTaxableEntity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxableEntity::class, $objDatabase );
	}

	public static function fetchTaxableEntityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'taxable_entities', $objDatabase );
	}

	public static function fetchTaxableEntityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntity( sprintf( 'SELECT * FROM taxable_entities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntitiesByCid( $intCid, $objDatabase ) {
		return self::fetchTaxableEntities( sprintf( 'SELECT * FROM taxable_entities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntities( sprintf( 'SELECT * FROM taxable_entities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntitiesByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntities( sprintf( 'SELECT * FROM taxable_entities WHERE gl_tree_id = %d AND cid = %d', $intGlTreeId, $intCid ), $objDatabase );
	}

}
?>