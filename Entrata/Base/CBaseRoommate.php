<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoommate extends CEosSingularBase {

	const TABLE_NAME = 'public.roommates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRoommateGroupId;
	protected $m_intParentLeaseId;
	protected $m_intLeaseId;
	protected $m_strEmailAddress;
	protected $m_intIsInvitationEmailSent;
	protected $m_intConfirmedBy;
	protected $m_strConfirmedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsInvitationRejected;
	protected $m_intApplicationId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsInvitationEmailSent = '0';
		$this->m_boolIsInvitationRejected = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['roommate_group_id'] ) && $boolDirectSet ) $this->set( 'm_intRoommateGroupId', trim( $arrValues['roommate_group_id'] ) ); elseif( isset( $arrValues['roommate_group_id'] ) ) $this->setRoommateGroupId( $arrValues['roommate_group_id'] );
		if( isset( $arrValues['parent_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intParentLeaseId', trim( $arrValues['parent_lease_id'] ) ); elseif( isset( $arrValues['parent_lease_id'] ) ) $this->setParentLeaseId( $arrValues['parent_lease_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['is_invitation_email_sent'] ) && $boolDirectSet ) $this->set( 'm_intIsInvitationEmailSent', trim( $arrValues['is_invitation_email_sent'] ) ); elseif( isset( $arrValues['is_invitation_email_sent'] ) ) $this->setIsInvitationEmailSent( $arrValues['is_invitation_email_sent'] );
		if( isset( $arrValues['confirmed_by'] ) && $boolDirectSet ) $this->set( 'm_intConfirmedBy', trim( $arrValues['confirmed_by'] ) ); elseif( isset( $arrValues['confirmed_by'] ) ) $this->setConfirmedBy( $arrValues['confirmed_by'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_invitation_rejected'] ) && $boolDirectSet ) $this->set( 'm_boolIsInvitationRejected', trim( stripcslashes( $arrValues['is_invitation_rejected'] ) ) ); elseif( isset( $arrValues['is_invitation_rejected'] ) ) $this->setIsInvitationRejected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_invitation_rejected'] ) : $arrValues['is_invitation_rejected'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRoommateGroupId( $intRoommateGroupId ) {
		$this->set( 'm_intRoommateGroupId', CStrings::strToIntDef( $intRoommateGroupId, NULL, false ) );
	}

	public function getRoommateGroupId() {
		return $this->m_intRoommateGroupId;
	}

	public function sqlRoommateGroupId() {
		return ( true == isset( $this->m_intRoommateGroupId ) ) ? ( string ) $this->m_intRoommateGroupId : 'NULL';
	}

	public function setParentLeaseId( $intParentLeaseId ) {
		$this->set( 'm_intParentLeaseId', CStrings::strToIntDef( $intParentLeaseId, NULL, false ) );
	}

	public function getParentLeaseId() {
		return $this->m_intParentLeaseId;
	}

	public function sqlParentLeaseId() {
		return ( true == isset( $this->m_intParentLeaseId ) ) ? ( string ) $this->m_intParentLeaseId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setIsInvitationEmailSent( $intIsInvitationEmailSent ) {
		$this->set( 'm_intIsInvitationEmailSent', CStrings::strToIntDef( $intIsInvitationEmailSent, NULL, false ) );
	}

	public function getIsInvitationEmailSent() {
		return $this->m_intIsInvitationEmailSent;
	}

	public function sqlIsInvitationEmailSent() {
		return ( true == isset( $this->m_intIsInvitationEmailSent ) ) ? ( string ) $this->m_intIsInvitationEmailSent : '0';
	}

	public function setConfirmedBy( $intConfirmedBy ) {
		$this->set( 'm_intConfirmedBy', CStrings::strToIntDef( $intConfirmedBy, NULL, false ) );
	}

	public function getConfirmedBy() {
		return $this->m_intConfirmedBy;
	}

	public function sqlConfirmedBy() {
		return ( true == isset( $this->m_intConfirmedBy ) ) ? ( string ) $this->m_intConfirmedBy : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsInvitationRejected( $boolIsInvitationRejected ) {
		$this->set( 'm_boolIsInvitationRejected', CStrings::strToBool( $boolIsInvitationRejected ) );
	}

	public function getIsInvitationRejected() {
		return $this->m_boolIsInvitationRejected;
	}

	public function sqlIsInvitationRejected() {
		return ( true == isset( $this->m_boolIsInvitationRejected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInvitationRejected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, roommate_group_id, parent_lease_id, lease_id, email_address, is_invitation_email_sent, confirmed_by, confirmed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_invitation_rejected, application_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlRoommateGroupId() . ', ' .
						$this->sqlParentLeaseId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlIsInvitationEmailSent() . ', ' .
						$this->sqlConfirmedBy() . ', ' .
						$this->sqlConfirmedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsInvitationRejected() . ', ' .
						$this->sqlApplicationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' roommate_group_id = ' . $this->sqlRoommateGroupId(). ',' ; } elseif( true == array_key_exists( 'RoommateGroupId', $this->getChangedColumns() ) ) { $strSql .= ' roommate_group_id = ' . $this->sqlRoommateGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_lease_id = ' . $this->sqlParentLeaseId(). ',' ; } elseif( true == array_key_exists( 'ParentLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' parent_lease_id = ' . $this->sqlParentLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_invitation_email_sent = ' . $this->sqlIsInvitationEmailSent(). ',' ; } elseif( true == array_key_exists( 'IsInvitationEmailSent', $this->getChangedColumns() ) ) { $strSql .= ' is_invitation_email_sent = ' . $this->sqlIsInvitationEmailSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy(). ',' ; } elseif( true == array_key_exists( 'ConfirmedBy', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_invitation_rejected = ' . $this->sqlIsInvitationRejected(). ',' ; } elseif( true == array_key_exists( 'IsInvitationRejected', $this->getChangedColumns() ) ) { $strSql .= ' is_invitation_rejected = ' . $this->sqlIsInvitationRejected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'roommate_group_id' => $this->getRoommateGroupId(),
			'parent_lease_id' => $this->getParentLeaseId(),
			'lease_id' => $this->getLeaseId(),
			'email_address' => $this->getEmailAddress(),
			'is_invitation_email_sent' => $this->getIsInvitationEmailSent(),
			'confirmed_by' => $this->getConfirmedBy(),
			'confirmed_on' => $this->getConfirmedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_invitation_rejected' => $this->getIsInvitationRejected(),
			'application_id' => $this->getApplicationId()
		);
	}

}
?>