<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyArRule extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intArOriginId;
    protected $m_intArCodeTypeId;
    protected $m_intArTriggerId;
    protected $m_intArActionId;
    protected $m_intStoreLeaseTerms;
    protected $m_intStoreLeaseStartWindows;
    protected $m_intStoreUnitOptions;
    protected $m_intUseNonStandardFrequencies;
    protected $m_intUseEffectiveDates;
    protected $m_intUseRateOffsets;
    protected $m_intUseArTiers;
    protected $m_intUseArMultipliers;
    protected $m_intUsePercentages;
    protected $m_intUseUnitOptions;
    protected $m_intUseMinMaxes;
    protected $m_intRequireCustomerRelationship;
    protected $m_intSeparateProspectVsRenewal;
    protected $m_intAllowDeactivationDate;
    protected $m_intRequireTriggerTypeCodes;
    protected $m_intRequireTriggerCodes;
    protected $m_intIsEnabled;
    protected $m_intOrderNum;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intStoreLeaseTerms = '0';
        $this->m_intStoreLeaseStartWindows = '0';
        $this->m_intStoreUnitOptions = '0';
        $this->m_intUseNonStandardFrequencies = '0';
        $this->m_intUseEffectiveDates = '0';
        $this->m_intUseRateOffsets = '0';
        $this->m_intUseArTiers = '0';
        $this->m_intUseArMultipliers = '0';
        $this->m_intUsePercentages = '0';
        $this->m_intUseUnitOptions = '0';
        $this->m_intUseMinMaxes = '0';
        $this->m_intRequireCustomerRelationship = '0';
        $this->m_intSeparateProspectVsRenewal = '0';
        $this->m_intAllowDeactivationDate = '0';
        $this->m_intRequireTriggerTypeCodes = '0';
        $this->m_intRequireTriggerCodes = '0';
        $this->m_intIsEnabled = '0';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->m_intArOriginId = trim( $arrValues['ar_origin_id'] ); else if( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
        if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->m_intArCodeTypeId = trim( $arrValues['ar_code_type_id'] ); else if( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
        if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->m_intArTriggerId = trim( $arrValues['ar_trigger_id'] ); else if( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
        if( isset( $arrValues['ar_action_id'] ) && $boolDirectSet ) $this->m_intArActionId = trim( $arrValues['ar_action_id'] ); else if( isset( $arrValues['ar_action_id'] ) ) $this->setArActionId( $arrValues['ar_action_id'] );
        if( isset( $arrValues['store_lease_terms'] ) && $boolDirectSet ) $this->m_intStoreLeaseTerms = trim( $arrValues['store_lease_terms'] ); else if( isset( $arrValues['store_lease_terms'] ) ) $this->setStoreLeaseTerms( $arrValues['store_lease_terms'] );
        if( isset( $arrValues['store_lease_start_windows'] ) && $boolDirectSet ) $this->m_intStoreLeaseStartWindows = trim( $arrValues['store_lease_start_windows'] ); else if( isset( $arrValues['store_lease_start_windows'] ) ) $this->setStoreLeaseStartWindows( $arrValues['store_lease_start_windows'] );
        if( isset( $arrValues['store_unit_options'] ) && $boolDirectSet ) $this->m_intStoreUnitOptions = trim( $arrValues['store_unit_options'] ); else if( isset( $arrValues['store_unit_options'] ) ) $this->setStoreUnitOptions( $arrValues['store_unit_options'] );
        if( isset( $arrValues['use_non_standard_frequencies'] ) && $boolDirectSet ) $this->m_intUseNonStandardFrequencies = trim( $arrValues['use_non_standard_frequencies'] ); else if( isset( $arrValues['use_non_standard_frequencies'] ) ) $this->setUseNonStandardFrequencies( $arrValues['use_non_standard_frequencies'] );
        if( isset( $arrValues['use_effective_dates'] ) && $boolDirectSet ) $this->m_intUseEffectiveDates = trim( $arrValues['use_effective_dates'] ); else if( isset( $arrValues['use_effective_dates'] ) ) $this->setUseEffectiveDates( $arrValues['use_effective_dates'] );
        if( isset( $arrValues['use_rate_offsets'] ) && $boolDirectSet ) $this->m_intUseRateOffsets = trim( $arrValues['use_rate_offsets'] ); else if( isset( $arrValues['use_rate_offsets'] ) ) $this->setUseRateOffsets( $arrValues['use_rate_offsets'] );
        if( isset( $arrValues['use_ar_tiers'] ) && $boolDirectSet ) $this->m_intUseArTiers = trim( $arrValues['use_ar_tiers'] ); else if( isset( $arrValues['use_ar_tiers'] ) ) $this->setUseArTiers( $arrValues['use_ar_tiers'] );
        if( isset( $arrValues['use_ar_multipliers'] ) && $boolDirectSet ) $this->m_intUseArMultipliers = trim( $arrValues['use_ar_multipliers'] ); else if( isset( $arrValues['use_ar_multipliers'] ) ) $this->setUseArMultipliers( $arrValues['use_ar_multipliers'] );
        if( isset( $arrValues['use_percentages'] ) && $boolDirectSet ) $this->m_intUsePercentages = trim( $arrValues['use_percentages'] ); else if( isset( $arrValues['use_percentages'] ) ) $this->setUsePercentages( $arrValues['use_percentages'] );
        if( isset( $arrValues['use_unit_options'] ) && $boolDirectSet ) $this->m_intUseUnitOptions = trim( $arrValues['use_unit_options'] ); else if( isset( $arrValues['use_unit_options'] ) ) $this->setUseUnitOptions( $arrValues['use_unit_options'] );
        if( isset( $arrValues['use_min_maxes'] ) && $boolDirectSet ) $this->m_intUseMinMaxes = trim( $arrValues['use_min_maxes'] ); else if( isset( $arrValues['use_min_maxes'] ) ) $this->setUseMinMaxes( $arrValues['use_min_maxes'] );
        if( isset( $arrValues['require_customer_relationship'] ) && $boolDirectSet ) $this->m_intRequireCustomerRelationship = trim( $arrValues['require_customer_relationship'] ); else if( isset( $arrValues['require_customer_relationship'] ) ) $this->setRequireCustomerRelationship( $arrValues['require_customer_relationship'] );
        if( isset( $arrValues['separate_prospect_vs_renewal'] ) && $boolDirectSet ) $this->m_intSeparateProspectVsRenewal = trim( $arrValues['separate_prospect_vs_renewal'] ); else if( isset( $arrValues['separate_prospect_vs_renewal'] ) ) $this->setSeparateProspectVsRenewal( $arrValues['separate_prospect_vs_renewal'] );
        if( isset( $arrValues['allow_deactivation_date'] ) && $boolDirectSet ) $this->m_intAllowDeactivationDate = trim( $arrValues['allow_deactivation_date'] ); else if( isset( $arrValues['allow_deactivation_date'] ) ) $this->setAllowDeactivationDate( $arrValues['allow_deactivation_date'] );
        if( isset( $arrValues['require_trigger_type_codes'] ) && $boolDirectSet ) $this->m_intRequireTriggerTypeCodes = trim( $arrValues['require_trigger_type_codes'] ); else if( isset( $arrValues['require_trigger_type_codes'] ) ) $this->setRequireTriggerTypeCodes( $arrValues['require_trigger_type_codes'] );
        if( isset( $arrValues['require_trigger_codes'] ) && $boolDirectSet ) $this->m_intRequireTriggerCodes = trim( $arrValues['require_trigger_codes'] ); else if( isset( $arrValues['require_trigger_codes'] ) ) $this->setRequireTriggerCodes( $arrValues['require_trigger_codes'] );
        if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->m_intIsEnabled = trim( $arrValues['is_enabled'] ); else if( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( $arrValues['is_enabled'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setArOriginId( $intArOriginId ) {
        $this->m_intArOriginId = CStrings::strToIntDef( $intArOriginId, NULL, false );
    }

    public function getArOriginId() {
        return $this->m_intArOriginId;
    }

    public function sqlArOriginId() {
        return ( true == isset( $this->m_intArOriginId ) ) ? (string) $this->m_intArOriginId : 'NULL';
    }

    public function setArCodeTypeId( $intArCodeTypeId ) {
        $this->m_intArCodeTypeId = CStrings::strToIntDef( $intArCodeTypeId, NULL, false );
    }

    public function getArCodeTypeId() {
        return $this->m_intArCodeTypeId;
    }

    public function sqlArCodeTypeId() {
        return ( true == isset( $this->m_intArCodeTypeId ) ) ? (string) $this->m_intArCodeTypeId : 'NULL';
    }

    public function setArTriggerId( $intArTriggerId ) {
        $this->m_intArTriggerId = CStrings::strToIntDef( $intArTriggerId, NULL, false );
    }

    public function getArTriggerId() {
        return $this->m_intArTriggerId;
    }

    public function sqlArTriggerId() {
        return ( true == isset( $this->m_intArTriggerId ) ) ? (string) $this->m_intArTriggerId : 'NULL';
    }

    public function setArActionId( $intArActionId ) {
        $this->m_intArActionId = CStrings::strToIntDef( $intArActionId, NULL, false );
    }

    public function getArActionId() {
        return $this->m_intArActionId;
    }

    public function sqlArActionId() {
        return ( true == isset( $this->m_intArActionId ) ) ? (string) $this->m_intArActionId : 'NULL';
    }

    public function setStoreLeaseTerms( $intStoreLeaseTerms ) {
        $this->m_intStoreLeaseTerms = CStrings::strToIntDef( $intStoreLeaseTerms, NULL, false );
    }

    public function getStoreLeaseTerms() {
        return $this->m_intStoreLeaseTerms;
    }

    public function sqlStoreLeaseTerms() {
        return ( true == isset( $this->m_intStoreLeaseTerms ) ) ? (string) $this->m_intStoreLeaseTerms : '0';
    }

    public function setStoreLeaseStartWindows( $intStoreLeaseStartWindows ) {
        $this->m_intStoreLeaseStartWindows = CStrings::strToIntDef( $intStoreLeaseStartWindows, NULL, false );
    }

    public function getStoreLeaseStartWindows() {
        return $this->m_intStoreLeaseStartWindows;
    }

    public function sqlStoreLeaseStartWindows() {
        return ( true == isset( $this->m_intStoreLeaseStartWindows ) ) ? (string) $this->m_intStoreLeaseStartWindows : '0';
    }

    public function setStoreUnitOptions( $intStoreUnitOptions ) {
        $this->m_intStoreUnitOptions = CStrings::strToIntDef( $intStoreUnitOptions, NULL, false );
    }

    public function getStoreUnitOptions() {
        return $this->m_intStoreUnitOptions;
    }

    public function sqlStoreUnitOptions() {
        return ( true == isset( $this->m_intStoreUnitOptions ) ) ? (string) $this->m_intStoreUnitOptions : '0';
    }

    public function setUseNonStandardFrequencies( $intUseNonStandardFrequencies ) {
        $this->m_intUseNonStandardFrequencies = CStrings::strToIntDef( $intUseNonStandardFrequencies, NULL, false );
    }

    public function getUseNonStandardFrequencies() {
        return $this->m_intUseNonStandardFrequencies;
    }

    public function sqlUseNonStandardFrequencies() {
        return ( true == isset( $this->m_intUseNonStandardFrequencies ) ) ? (string) $this->m_intUseNonStandardFrequencies : '0';
    }

    public function setUseEffectiveDates( $intUseEffectiveDates ) {
        $this->m_intUseEffectiveDates = CStrings::strToIntDef( $intUseEffectiveDates, NULL, false );
    }

    public function getUseEffectiveDates() {
        return $this->m_intUseEffectiveDates;
    }

    public function sqlUseEffectiveDates() {
        return ( true == isset( $this->m_intUseEffectiveDates ) ) ? (string) $this->m_intUseEffectiveDates : '0';
    }

    public function setUseRateOffsets( $intUseRateOffsets ) {
        $this->m_intUseRateOffsets = CStrings::strToIntDef( $intUseRateOffsets, NULL, false );
    }

    public function getUseRateOffsets() {
        return $this->m_intUseRateOffsets;
    }

    public function sqlUseRateOffsets() {
        return ( true == isset( $this->m_intUseRateOffsets ) ) ? (string) $this->m_intUseRateOffsets : '0';
    }

    public function setUseArTiers( $intUseArTiers ) {
        $this->m_intUseArTiers = CStrings::strToIntDef( $intUseArTiers, NULL, false );
    }

    public function getUseArTiers() {
        return $this->m_intUseArTiers;
    }

    public function sqlUseArTiers() {
        return ( true == isset( $this->m_intUseArTiers ) ) ? (string) $this->m_intUseArTiers : '0';
    }

    public function setUseArMultipliers( $intUseArMultipliers ) {
        $this->m_intUseArMultipliers = CStrings::strToIntDef( $intUseArMultipliers, NULL, false );
    }

    public function getUseArMultipliers() {
        return $this->m_intUseArMultipliers;
    }

    public function sqlUseArMultipliers() {
        return ( true == isset( $this->m_intUseArMultipliers ) ) ? (string) $this->m_intUseArMultipliers : '0';
    }

    public function setUsePercentages( $intUsePercentages ) {
        $this->m_intUsePercentages = CStrings::strToIntDef( $intUsePercentages, NULL, false );
    }

    public function getUsePercentages() {
        return $this->m_intUsePercentages;
    }

    public function sqlUsePercentages() {
        return ( true == isset( $this->m_intUsePercentages ) ) ? (string) $this->m_intUsePercentages : '0';
    }

    public function setUseUnitOptions( $intUseUnitOptions ) {
        $this->m_intUseUnitOptions = CStrings::strToIntDef( $intUseUnitOptions, NULL, false );
    }

    public function getUseUnitOptions() {
        return $this->m_intUseUnitOptions;
    }

    public function sqlUseUnitOptions() {
        return ( true == isset( $this->m_intUseUnitOptions ) ) ? (string) $this->m_intUseUnitOptions : '0';
    }

    public function setUseMinMaxes( $intUseMinMaxes ) {
        $this->m_intUseMinMaxes = CStrings::strToIntDef( $intUseMinMaxes, NULL, false );
    }

    public function getUseMinMaxes() {
        return $this->m_intUseMinMaxes;
    }

    public function sqlUseMinMaxes() {
        return ( true == isset( $this->m_intUseMinMaxes ) ) ? (string) $this->m_intUseMinMaxes : '0';
    }

    public function setRequireCustomerRelationship( $intRequireCustomerRelationship ) {
        $this->m_intRequireCustomerRelationship = CStrings::strToIntDef( $intRequireCustomerRelationship, NULL, false );
    }

    public function getRequireCustomerRelationship() {
        return $this->m_intRequireCustomerRelationship;
    }

    public function sqlRequireCustomerRelationship() {
        return ( true == isset( $this->m_intRequireCustomerRelationship ) ) ? (string) $this->m_intRequireCustomerRelationship : '0';
    }

    public function setSeparateProspectVsRenewal( $intSeparateProspectVsRenewal ) {
        $this->m_intSeparateProspectVsRenewal = CStrings::strToIntDef( $intSeparateProspectVsRenewal, NULL, false );
    }

    public function getSeparateProspectVsRenewal() {
        return $this->m_intSeparateProspectVsRenewal;
    }

    public function sqlSeparateProspectVsRenewal() {
        return ( true == isset( $this->m_intSeparateProspectVsRenewal ) ) ? (string) $this->m_intSeparateProspectVsRenewal : '0';
    }

    public function setAllowDeactivationDate( $intAllowDeactivationDate ) {
        $this->m_intAllowDeactivationDate = CStrings::strToIntDef( $intAllowDeactivationDate, NULL, false );
    }

    public function getAllowDeactivationDate() {
        return $this->m_intAllowDeactivationDate;
    }

    public function sqlAllowDeactivationDate() {
        return ( true == isset( $this->m_intAllowDeactivationDate ) ) ? (string) $this->m_intAllowDeactivationDate : '0';
    }

    public function setRequireTriggerTypeCodes( $intRequireTriggerTypeCodes ) {
        $this->m_intRequireTriggerTypeCodes = CStrings::strToIntDef( $intRequireTriggerTypeCodes, NULL, false );
    }

    public function getRequireTriggerTypeCodes() {
        return $this->m_intRequireTriggerTypeCodes;
    }

    public function sqlRequireTriggerTypeCodes() {
        return ( true == isset( $this->m_intRequireTriggerTypeCodes ) ) ? (string) $this->m_intRequireTriggerTypeCodes : '0';
    }

    public function setRequireTriggerCodes( $intRequireTriggerCodes ) {
        $this->m_intRequireTriggerCodes = CStrings::strToIntDef( $intRequireTriggerCodes, NULL, false );
    }

    public function getRequireTriggerCodes() {
        return $this->m_intRequireTriggerCodes;
    }

    public function sqlRequireTriggerCodes() {
        return ( true == isset( $this->m_intRequireTriggerCodes ) ) ? (string) $this->m_intRequireTriggerCodes : '0';
    }

    public function setIsEnabled( $intIsEnabled ) {
        $this->m_intIsEnabled = CStrings::strToIntDef( $intIsEnabled, NULL, false );
    }

    public function getIsEnabled() {
        return $this->m_intIsEnabled;
    }

    public function sqlIsEnabled() {
        return ( true == isset( $this->m_intIsEnabled ) ) ? (string) $this->m_intIsEnabled : '0';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.property_ar_rules_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.property_ar_rules( id, cid, property_id, ar_origin_id, ar_code_type_id, ar_trigger_id, ar_action_id, store_lease_terms, store_lease_start_windows, store_unit_options, use_non_standard_frequencies, use_effective_dates, use_rate_offsets, use_ar_tiers, use_ar_multipliers, use_percentages, use_unit_options, use_min_maxes, require_customer_relationship, separate_prospect_vs_renewal, allow_deactivation_date, require_trigger_type_codes, require_trigger_codes, is_enabled, order_num, updated_by, updated_on, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlArOriginId() . ', ' . 		                $this->sqlArCodeTypeId() . ', ' . 		                $this->sqlArTriggerId() . ', ' . 		                $this->sqlArActionId() . ', ' . 		                $this->sqlStoreLeaseTerms() . ', ' . 		                $this->sqlStoreLeaseStartWindows() . ', ' . 		                $this->sqlStoreUnitOptions() . ', ' . 		                $this->sqlUseNonStandardFrequencies() . ', ' . 		                $this->sqlUseEffectiveDates() . ', ' . 		                $this->sqlUseRateOffsets() . ', ' . 		                $this->sqlUseArTiers() . ', ' . 		                $this->sqlUseArMultipliers() . ', ' . 		                $this->sqlUsePercentages() . ', ' . 		                $this->sqlUseUnitOptions() . ', ' . 		                $this->sqlUseMinMaxes() . ', ' . 		                $this->sqlRequireCustomerRelationship() . ', ' . 		                $this->sqlSeparateProspectVsRenewal() . ', ' . 		                $this->sqlAllowDeactivationDate() . ', ' . 		                $this->sqlRequireTriggerTypeCodes() . ', ' . 		                $this->sqlRequireTriggerCodes() . ', ' . 		                $this->sqlIsEnabled() . ', ' . 		                $this->sqlOrderNum() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.property_ar_rules
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArOriginId() ) != $this->getOriginalValueByFieldName ( 'ar_origin_id' ) ) { $arrstrOriginalValueChanges['ar_origin_id'] = $this->sqlArOriginId(); $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArCodeTypeId() ) != $this->getOriginalValueByFieldName ( 'ar_code_type_id' ) ) { $arrstrOriginalValueChanges['ar_code_type_id'] = $this->sqlArCodeTypeId(); $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArTriggerId() ) != $this->getOriginalValueByFieldName ( 'ar_trigger_id' ) ) { $arrstrOriginalValueChanges['ar_trigger_id'] = $this->sqlArTriggerId(); $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_action_id = ' . $this->sqlArActionId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArActionId() ) != $this->getOriginalValueByFieldName ( 'ar_action_id' ) ) { $arrstrOriginalValueChanges['ar_action_id'] = $this->sqlArActionId(); $strSql .= ' ar_action_id = ' . $this->sqlArActionId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_lease_terms = ' . $this->sqlStoreLeaseTerms() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStoreLeaseTerms() ) != $this->getOriginalValueByFieldName ( 'store_lease_terms' ) ) { $arrstrOriginalValueChanges['store_lease_terms'] = $this->sqlStoreLeaseTerms(); $strSql .= ' store_lease_terms = ' . $this->sqlStoreLeaseTerms() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_lease_start_windows = ' . $this->sqlStoreLeaseStartWindows() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStoreLeaseStartWindows() ) != $this->getOriginalValueByFieldName ( 'store_lease_start_windows' ) ) { $arrstrOriginalValueChanges['store_lease_start_windows'] = $this->sqlStoreLeaseStartWindows(); $strSql .= ' store_lease_start_windows = ' . $this->sqlStoreLeaseStartWindows() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_unit_options = ' . $this->sqlStoreUnitOptions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStoreUnitOptions() ) != $this->getOriginalValueByFieldName ( 'store_unit_options' ) ) { $arrstrOriginalValueChanges['store_unit_options'] = $this->sqlStoreUnitOptions(); $strSql .= ' store_unit_options = ' . $this->sqlStoreUnitOptions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_non_standard_frequencies = ' . $this->sqlUseNonStandardFrequencies() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseNonStandardFrequencies() ) != $this->getOriginalValueByFieldName ( 'use_non_standard_frequencies' ) ) { $arrstrOriginalValueChanges['use_non_standard_frequencies'] = $this->sqlUseNonStandardFrequencies(); $strSql .= ' use_non_standard_frequencies = ' . $this->sqlUseNonStandardFrequencies() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_effective_dates = ' . $this->sqlUseEffectiveDates() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseEffectiveDates() ) != $this->getOriginalValueByFieldName ( 'use_effective_dates' ) ) { $arrstrOriginalValueChanges['use_effective_dates'] = $this->sqlUseEffectiveDates(); $strSql .= ' use_effective_dates = ' . $this->sqlUseEffectiveDates() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_rate_offsets = ' . $this->sqlUseRateOffsets() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseRateOffsets() ) != $this->getOriginalValueByFieldName ( 'use_rate_offsets' ) ) { $arrstrOriginalValueChanges['use_rate_offsets'] = $this->sqlUseRateOffsets(); $strSql .= ' use_rate_offsets = ' . $this->sqlUseRateOffsets() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_ar_tiers = ' . $this->sqlUseArTiers() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseArTiers() ) != $this->getOriginalValueByFieldName ( 'use_ar_tiers' ) ) { $arrstrOriginalValueChanges['use_ar_tiers'] = $this->sqlUseArTiers(); $strSql .= ' use_ar_tiers = ' . $this->sqlUseArTiers() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_ar_multipliers = ' . $this->sqlUseArMultipliers() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseArMultipliers() ) != $this->getOriginalValueByFieldName ( 'use_ar_multipliers' ) ) { $arrstrOriginalValueChanges['use_ar_multipliers'] = $this->sqlUseArMultipliers(); $strSql .= ' use_ar_multipliers = ' . $this->sqlUseArMultipliers() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_percentages = ' . $this->sqlUsePercentages() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUsePercentages() ) != $this->getOriginalValueByFieldName ( 'use_percentages' ) ) { $arrstrOriginalValueChanges['use_percentages'] = $this->sqlUsePercentages(); $strSql .= ' use_percentages = ' . $this->sqlUsePercentages() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_unit_options = ' . $this->sqlUseUnitOptions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseUnitOptions() ) != $this->getOriginalValueByFieldName ( 'use_unit_options' ) ) { $arrstrOriginalValueChanges['use_unit_options'] = $this->sqlUseUnitOptions(); $strSql .= ' use_unit_options = ' . $this->sqlUseUnitOptions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_min_maxes = ' . $this->sqlUseMinMaxes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseMinMaxes() ) != $this->getOriginalValueByFieldName ( 'use_min_maxes' ) ) { $arrstrOriginalValueChanges['use_min_maxes'] = $this->sqlUseMinMaxes(); $strSql .= ' use_min_maxes = ' . $this->sqlUseMinMaxes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_customer_relationship = ' . $this->sqlRequireCustomerRelationship() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRequireCustomerRelationship() ) != $this->getOriginalValueByFieldName ( 'require_customer_relationship' ) ) { $arrstrOriginalValueChanges['require_customer_relationship'] = $this->sqlRequireCustomerRelationship(); $strSql .= ' require_customer_relationship = ' . $this->sqlRequireCustomerRelationship() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' separate_prospect_vs_renewal = ' . $this->sqlSeparateProspectVsRenewal() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSeparateProspectVsRenewal() ) != $this->getOriginalValueByFieldName ( 'separate_prospect_vs_renewal' ) ) { $arrstrOriginalValueChanges['separate_prospect_vs_renewal'] = $this->sqlSeparateProspectVsRenewal(); $strSql .= ' separate_prospect_vs_renewal = ' . $this->sqlSeparateProspectVsRenewal() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_deactivation_date = ' . $this->sqlAllowDeactivationDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAllowDeactivationDate() ) != $this->getOriginalValueByFieldName ( 'allow_deactivation_date' ) ) { $arrstrOriginalValueChanges['allow_deactivation_date'] = $this->sqlAllowDeactivationDate(); $strSql .= ' allow_deactivation_date = ' . $this->sqlAllowDeactivationDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_trigger_type_codes = ' . $this->sqlRequireTriggerTypeCodes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRequireTriggerTypeCodes() ) != $this->getOriginalValueByFieldName ( 'require_trigger_type_codes' ) ) { $arrstrOriginalValueChanges['require_trigger_type_codes'] = $this->sqlRequireTriggerTypeCodes(); $strSql .= ' require_trigger_type_codes = ' . $this->sqlRequireTriggerTypeCodes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_trigger_codes = ' . $this->sqlRequireTriggerCodes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRequireTriggerCodes() ) != $this->getOriginalValueByFieldName ( 'require_trigger_codes' ) ) { $arrstrOriginalValueChanges['require_trigger_codes'] = $this->sqlRequireTriggerCodes(); $strSql .= ' require_trigger_codes = ' . $this->sqlRequireTriggerCodes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsEnabled() ) != $this->getOriginalValueByFieldName ( 'is_enabled' ) ) { $arrstrOriginalValueChanges['is_enabled'] = $this->sqlIsEnabled(); $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName ( 'order_num' ) ) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.property_ar_rules WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.property_ar_rules_id_seq', $objDatabase );
    }

}
?>