<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgets extends CEosPluralBase {

	/**
	 * @return CBudget[]
	 */
	public static function fetchBudgets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudget::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudget
	 */
	public static function fetchBudget( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudget::class, $objDatabase );
	}

	public static function fetchBudgetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budgets', $objDatabase );
	}

	public static function fetchBudgetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudget( sprintf( 'SELECT * FROM budgets WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgets( sprintf( 'SELECT * FROM budgets WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetsByBudgetStatusTypeIdByCid( $intBudgetStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgets( sprintf( 'SELECT * FROM budgets WHERE budget_status_type_id = %d AND cid = %d', $intBudgetStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchBudgets( sprintf( 'SELECT * FROM budgets WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		return self::fetchBudgets( sprintf( 'SELECT * FROM budgets WHERE budget_workbook_id = %d AND cid = %d', $intBudgetWorkbookId, $intCid ), $objDatabase );
	}

}
?>