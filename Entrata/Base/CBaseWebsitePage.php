<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsitePage extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.website_pages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteDocumentId;
	protected $m_intWebsitePageTypeId;
	protected $m_strPageName;
	protected $m_strPageTitle;
	protected $m_strKeywords;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsPublished;
	protected $m_strVersion;
	protected $m_strRoute;
	protected $m_strRedirectUrl;
	protected $m_strTarget;
	protected $m_intOrderNum;
	protected $m_strCanonicalUrl;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_document_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteDocumentId', trim( $arrValues['website_document_id'] ) ); elseif( isset( $arrValues['website_document_id'] ) ) $this->setWebsiteDocumentId( $arrValues['website_document_id'] );
		if( isset( $arrValues['website_page_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsitePageTypeId', trim( $arrValues['website_page_type_id'] ) ); elseif( isset( $arrValues['website_page_type_id'] ) ) $this->setWebsitePageTypeId( $arrValues['website_page_type_id'] );
		if( isset( $arrValues['page_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPageName', trim( $arrValues['page_name'] ) ); elseif( isset( $arrValues['page_name'] ) ) $this->setPageName( $arrValues['page_name'] );
		if( isset( $arrValues['page_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPageTitle', trim( $arrValues['page_title'] ) ); elseif( isset( $arrValues['page_title'] ) ) $this->setPageTitle( $arrValues['page_title'] );
		if( isset( $arrValues['keywords'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strKeywords', trim( $arrValues['keywords'] ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( $arrValues['keywords'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['version'] ) && $boolDirectSet ) $this->set( 'm_strVersion', trim( $arrValues['version'] ) ); elseif( isset( $arrValues['version'] ) ) $this->setVersion( $arrValues['version'] );
		if( isset( $arrValues['route'] ) && $boolDirectSet ) $this->set( 'm_strRoute', trim( $arrValues['route'] ) ); elseif( isset( $arrValues['route'] ) ) $this->setRoute( $arrValues['route'] );
		if( isset( $arrValues['redirect_url'] ) && $boolDirectSet ) $this->set( 'm_strRedirectUrl', trim( $arrValues['redirect_url'] ) ); elseif( isset( $arrValues['redirect_url'] ) ) $this->setRedirectUrl( $arrValues['redirect_url'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( $arrValues['target'] ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( $arrValues['target'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['canonical_url'] ) && $boolDirectSet ) $this->set( 'm_strCanonicalUrl', trim( $arrValues['canonical_url'] ) ); elseif( isset( $arrValues['canonical_url'] ) ) $this->setCanonicalUrl( $arrValues['canonical_url'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteDocumentId( $intWebsiteDocumentId ) {
		$this->set( 'm_intWebsiteDocumentId', CStrings::strToIntDef( $intWebsiteDocumentId, NULL, false ) );
	}

	public function getWebsiteDocumentId() {
		return $this->m_intWebsiteDocumentId;
	}

	public function sqlWebsiteDocumentId() {
		return ( true == isset( $this->m_intWebsiteDocumentId ) ) ? ( string ) $this->m_intWebsiteDocumentId : 'NULL';
	}

	public function setWebsitePageTypeId( $intWebsitePageTypeId ) {
		$this->set( 'm_intWebsitePageTypeId', CStrings::strToIntDef( $intWebsitePageTypeId, NULL, false ) );
	}

	public function getWebsitePageTypeId() {
		return $this->m_intWebsitePageTypeId;
	}

	public function sqlWebsitePageTypeId() {
		return ( true == isset( $this->m_intWebsitePageTypeId ) ) ? ( string ) $this->m_intWebsitePageTypeId : 'NULL';
	}

	public function setPageName( $strPageName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPageName', CStrings::strTrimDef( $strPageName, -1, NULL, true ), $strLocaleCode );
	}

	public function getPageName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPageName', $strLocaleCode );
	}

	public function sqlPageName() {
		return ( true == isset( $this->m_strPageName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPageName ) : '\'' . addslashes( $this->m_strPageName ) . '\'' ) : 'NULL';
	}

	public function setPageTitle( $strPageTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPageTitle', CStrings::strTrimDef( $strPageTitle, -1, NULL, true ), $strLocaleCode );
	}

	public function getPageTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPageTitle', $strLocaleCode );
	}

	public function sqlPageTitle() {
		return ( true == isset( $this->m_strPageTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPageTitle ) : '\'' . addslashes( $this->m_strPageTitle ) . '\'' ) : 'NULL';
	}

	public function setKeywords( $strKeywords, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, -1, NULL, true ), $strLocaleCode );
	}

	public function getKeywords( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strKeywords', $strLocaleCode );
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKeywords ) : '\'' . addslashes( $this->m_strKeywords ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setVersion( $strVersion ) {
		$this->set( 'm_strVersion', CStrings::strTrimDef( $strVersion, -1, NULL, true ) );
	}

	public function getVersion() {
		return $this->m_strVersion;
	}

	public function sqlVersion() {
		return ( true == isset( $this->m_strVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVersion ) : '\'' . addslashes( $this->m_strVersion ) . '\'' ) : 'NULL';
	}

	public function setRoute( $strRoute ) {
		$this->set( 'm_strRoute', CStrings::strTrimDef( $strRoute, -1, NULL, true ) );
	}

	public function getRoute() {
		return $this->m_strRoute;
	}

	public function sqlRoute() {
		return ( true == isset( $this->m_strRoute ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRoute ) : '\'' . addslashes( $this->m_strRoute ) . '\'' ) : 'NULL';
	}

	public function setRedirectUrl( $strRedirectUrl ) {
		$this->set( 'm_strRedirectUrl', CStrings::strTrimDef( $strRedirectUrl, -1, NULL, true ) );
	}

	public function getRedirectUrl() {
		return $this->m_strRedirectUrl;
	}

	public function sqlRedirectUrl() {
		return ( true == isset( $this->m_strRedirectUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRedirectUrl ) : '\'' . addslashes( $this->m_strRedirectUrl ) . '\'' ) : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, -1, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTarget ) : '\'' . addslashes( $this->m_strTarget ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCanonicalUrl( $strCanonicalUrl ) {
		$this->set( 'm_strCanonicalUrl', CStrings::strTrimDef( $strCanonicalUrl, -1, NULL, true ) );
	}

	public function getCanonicalUrl() {
		return $this->m_strCanonicalUrl;
	}

	public function sqlCanonicalUrl() {
		return ( true == isset( $this->m_strCanonicalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCanonicalUrl ) : '\'' . addslashes( $this->m_strCanonicalUrl ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_document_id, website_page_type_id, page_name, page_title, keywords, description, details, is_published, version, route, redirect_url, target, order_num, canonical_url, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlWebsiteDocumentId() . ', ' .
						$this->sqlWebsitePageTypeId() . ', ' .
						$this->sqlPageName() . ', ' .
						$this->sqlPageTitle() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlVersion() . ', ' .
						$this->sqlRoute() . ', ' .
						$this->sqlRedirectUrl() . ', ' .
						$this->sqlTarget() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCanonicalUrl() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_document_id = ' . $this->sqlWebsiteDocumentId(). ',' ; } elseif( true == array_key_exists( 'WebsiteDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' website_document_id = ' . $this->sqlWebsiteDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_page_type_id = ' . $this->sqlWebsitePageTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsitePageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_page_type_id = ' . $this->sqlWebsitePageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_name = ' . $this->sqlPageName(). ',' ; } elseif( true == array_key_exists( 'PageName', $this->getChangedColumns() ) ) { $strSql .= ' page_name = ' . $this->sqlPageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_title = ' . $this->sqlPageTitle(). ',' ; } elseif( true == array_key_exists( 'PageTitle', $this->getChangedColumns() ) ) { $strSql .= ' page_title = ' . $this->sqlPageTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version = ' . $this->sqlVersion(). ',' ; } elseif( true == array_key_exists( 'Version', $this->getChangedColumns() ) ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route = ' . $this->sqlRoute(). ',' ; } elseif( true == array_key_exists( 'Route', $this->getChangedColumns() ) ) { $strSql .= ' route = ' . $this->sqlRoute() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl(). ',' ; } elseif( true == array_key_exists( 'RedirectUrl', $this->getChangedColumns() ) ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget(). ',' ; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' canonical_url = ' . $this->sqlCanonicalUrl(). ',' ; } elseif( true == array_key_exists( 'CanonicalUrl', $this->getChangedColumns() ) ) { $strSql .= ' canonical_url = ' . $this->sqlCanonicalUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_document_id' => $this->getWebsiteDocumentId(),
			'website_page_type_id' => $this->getWebsitePageTypeId(),
			'page_name' => $this->getPageName(),
			'page_title' => $this->getPageTitle(),
			'keywords' => $this->getKeywords(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'is_published' => $this->getIsPublished(),
			'version' => $this->getVersion(),
			'route' => $this->getRoute(),
			'redirect_url' => $this->getRedirectUrl(),
			'target' => $this->getTarget(),
			'order_num' => $this->getOrderNum(),
			'canonical_url' => $this->getCanonicalUrl(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>