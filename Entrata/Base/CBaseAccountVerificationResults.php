<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountVerificationResults
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountVerificationResults extends CEosPluralBase {

	/**
	 * @return CAccountVerificationResult[]
	 */
	public static function fetchAccountVerificationResults( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountVerificationResult::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountVerificationResult
	 */
	public static function fetchAccountVerificationResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountVerificationResult::class, $objDatabase );
	}

	public static function fetchAccountVerificationResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'account_verification_results', $objDatabase );
	}

	public static function fetchAccountVerificationResultByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResult( sprintf( 'SELECT * FROM account_verification_results WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByCid( $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE company_merchant_account_id = %d AND cid = %d', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountVerificationResultsByGiactRequestLogIdByCid( $intGiactRequestLogId, $intCid, $objDatabase ) {
		return self::fetchAccountVerificationResults( sprintf( 'SELECT * FROM account_verification_results WHERE giact_request_log_id = %d AND cid = %d', ( int ) $intGiactRequestLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>