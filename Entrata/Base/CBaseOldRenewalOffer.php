<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOldRenewalOffer extends CEosSingularBase {

	const TABLE_NAME = 'public.old_renewal_offers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intPropertyRenewalOfferSettingId;
	protected $m_intPropertyLeasePeriodId;
	protected $m_intConcessionArCodeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strOfferDatetime;
	protected $m_intMinLeaseTerm;
	protected $m_intMaxLeaseTerm;
	protected $m_strIncentive;
	protected $m_strIncentiveDescription;
	protected $m_fltIncentiveAmount;
	protected $m_fltConcessionAmount;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['property_renewal_offer_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyRenewalOfferSettingId', trim( $arrValues['property_renewal_offer_setting_id'] ) ); elseif( isset( $arrValues['property_renewal_offer_setting_id'] ) ) $this->setPropertyRenewalOfferSettingId( $arrValues['property_renewal_offer_setting_id'] );
		if( isset( $arrValues['property_lease_period_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyLeasePeriodId', trim( $arrValues['property_lease_period_id'] ) ); elseif( isset( $arrValues['property_lease_period_id'] ) ) $this->setPropertyLeasePeriodId( $arrValues['property_lease_period_id'] );
		if( isset( $arrValues['concession_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intConcessionArCodeId', trim( $arrValues['concession_ar_code_id'] ) ); elseif( isset( $arrValues['concession_ar_code_id'] ) ) $this->setConcessionArCodeId( $arrValues['concession_ar_code_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['offer_datetime'] ) && $boolDirectSet ) $this->set( 'm_strOfferDatetime', trim( $arrValues['offer_datetime'] ) ); elseif( isset( $arrValues['offer_datetime'] ) ) $this->setOfferDatetime( $arrValues['offer_datetime'] );
		if( isset( $arrValues['min_lease_term'] ) && $boolDirectSet ) $this->set( 'm_intMinLeaseTerm', trim( $arrValues['min_lease_term'] ) ); elseif( isset( $arrValues['min_lease_term'] ) ) $this->setMinLeaseTerm( $arrValues['min_lease_term'] );
		if( isset( $arrValues['max_lease_term'] ) && $boolDirectSet ) $this->set( 'm_intMaxLeaseTerm', trim( $arrValues['max_lease_term'] ) ); elseif( isset( $arrValues['max_lease_term'] ) ) $this->setMaxLeaseTerm( $arrValues['max_lease_term'] );
		if( isset( $arrValues['incentive'] ) && $boolDirectSet ) $this->set( 'm_strIncentive', trim( stripcslashes( $arrValues['incentive'] ) ) ); elseif( isset( $arrValues['incentive'] ) ) $this->setIncentive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['incentive'] ) : $arrValues['incentive'] );
		if( isset( $arrValues['incentive_description'] ) && $boolDirectSet ) $this->set( 'm_strIncentiveDescription', trim( stripcslashes( $arrValues['incentive_description'] ) ) ); elseif( isset( $arrValues['incentive_description'] ) ) $this->setIncentiveDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['incentive_description'] ) : $arrValues['incentive_description'] );
		if( isset( $arrValues['incentive_amount'] ) && $boolDirectSet ) $this->set( 'm_fltIncentiveAmount', trim( $arrValues['incentive_amount'] ) ); elseif( isset( $arrValues['incentive_amount'] ) ) $this->setIncentiveAmount( $arrValues['incentive_amount'] );
		if( isset( $arrValues['concession_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConcessionAmount', trim( $arrValues['concession_amount'] ) ); elseif( isset( $arrValues['concession_amount'] ) ) $this->setConcessionAmount( $arrValues['concession_amount'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPropertyRenewalOfferSettingId( $intPropertyRenewalOfferSettingId ) {
		$this->set( 'm_intPropertyRenewalOfferSettingId', CStrings::strToIntDef( $intPropertyRenewalOfferSettingId, NULL, false ) );
	}

	public function getPropertyRenewalOfferSettingId() {
		return $this->m_intPropertyRenewalOfferSettingId;
	}

	public function sqlPropertyRenewalOfferSettingId() {
		return ( true == isset( $this->m_intPropertyRenewalOfferSettingId ) ) ? ( string ) $this->m_intPropertyRenewalOfferSettingId : 'NULL';
	}

	public function setPropertyLeasePeriodId( $intPropertyLeasePeriodId ) {
		$this->set( 'm_intPropertyLeasePeriodId', CStrings::strToIntDef( $intPropertyLeasePeriodId, NULL, false ) );
	}

	public function getPropertyLeasePeriodId() {
		return $this->m_intPropertyLeasePeriodId;
	}

	public function sqlPropertyLeasePeriodId() {
		return ( true == isset( $this->m_intPropertyLeasePeriodId ) ) ? ( string ) $this->m_intPropertyLeasePeriodId : 'NULL';
	}

	public function setConcessionArCodeId( $intConcessionArCodeId ) {
		$this->set( 'm_intConcessionArCodeId', CStrings::strToIntDef( $intConcessionArCodeId, NULL, false ) );
	}

	public function getConcessionArCodeId() {
		return $this->m_intConcessionArCodeId;
	}

	public function sqlConcessionArCodeId() {
		return ( true == isset( $this->m_intConcessionArCodeId ) ) ? ( string ) $this->m_intConcessionArCodeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setOfferDatetime( $strOfferDatetime ) {
		$this->set( 'm_strOfferDatetime', CStrings::strTrimDef( $strOfferDatetime, -1, NULL, true ) );
	}

	public function getOfferDatetime() {
		return $this->m_strOfferDatetime;
	}

	public function sqlOfferDatetime() {
		return ( true == isset( $this->m_strOfferDatetime ) ) ? '\'' . $this->m_strOfferDatetime . '\'' : 'NOW()';
	}

	public function setMinLeaseTerm( $intMinLeaseTerm ) {
		$this->set( 'm_intMinLeaseTerm', CStrings::strToIntDef( $intMinLeaseTerm, NULL, false ) );
	}

	public function getMinLeaseTerm() {
		return $this->m_intMinLeaseTerm;
	}

	public function sqlMinLeaseTerm() {
		return ( true == isset( $this->m_intMinLeaseTerm ) ) ? ( string ) $this->m_intMinLeaseTerm : 'NULL';
	}

	public function setMaxLeaseTerm( $intMaxLeaseTerm ) {
		$this->set( 'm_intMaxLeaseTerm', CStrings::strToIntDef( $intMaxLeaseTerm, NULL, false ) );
	}

	public function getMaxLeaseTerm() {
		return $this->m_intMaxLeaseTerm;
	}

	public function sqlMaxLeaseTerm() {
		return ( true == isset( $this->m_intMaxLeaseTerm ) ) ? ( string ) $this->m_intMaxLeaseTerm : 'NULL';
	}

	public function setIncentive( $strIncentive ) {
		$this->set( 'm_strIncentive', CStrings::strTrimDef( $strIncentive, 100, NULL, true ) );
	}

	public function getIncentive() {
		return $this->m_strIncentive;
	}

	public function sqlIncentive() {
		return ( true == isset( $this->m_strIncentive ) ) ? '\'' . addslashes( $this->m_strIncentive ) . '\'' : 'NULL';
	}

	public function setIncentiveDescription( $strIncentiveDescription ) {
		$this->set( 'm_strIncentiveDescription', CStrings::strTrimDef( $strIncentiveDescription, 240, NULL, true ) );
	}

	public function getIncentiveDescription() {
		return $this->m_strIncentiveDescription;
	}

	public function sqlIncentiveDescription() {
		return ( true == isset( $this->m_strIncentiveDescription ) ) ? '\'' . addslashes( $this->m_strIncentiveDescription ) . '\'' : 'NULL';
	}

	public function setIncentiveAmount( $fltIncentiveAmount ) {
		$this->set( 'm_fltIncentiveAmount', CStrings::strToFloatDef( $fltIncentiveAmount, NULL, false, 2 ) );
	}

	public function getIncentiveAmount() {
		return $this->m_fltIncentiveAmount;
	}

	public function sqlIncentiveAmount() {
		return ( true == isset( $this->m_fltIncentiveAmount ) ) ? ( string ) $this->m_fltIncentiveAmount : 'NULL';
	}

	public function setConcessionAmount( $fltConcessionAmount ) {
		$this->set( 'm_fltConcessionAmount', CStrings::strToFloatDef( $fltConcessionAmount, NULL, false, 2 ) );
	}

	public function getConcessionAmount() {
		return $this->m_fltConcessionAmount;
	}

	public function sqlConcessionAmount() {
		return ( true == isset( $this->m_fltConcessionAmount ) ) ? ( string ) $this->m_fltConcessionAmount : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, property_renewal_offer_setting_id, property_lease_period_id, concession_ar_code_id, remote_primary_key, offer_datetime, min_lease_term, max_lease_term, incentive, incentive_description, incentive_amount, concession_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPropertyRenewalOfferSettingId() . ', ' .
 						$this->sqlPropertyLeasePeriodId() . ', ' .
 						$this->sqlConcessionArCodeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlOfferDatetime() . ', ' .
 						$this->sqlMinLeaseTerm() . ', ' .
 						$this->sqlMaxLeaseTerm() . ', ' .
 						$this->sqlIncentive() . ', ' .
 						$this->sqlIncentiveDescription() . ', ' .
 						$this->sqlIncentiveAmount() . ', ' .
 						$this->sqlConcessionAmount() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_renewal_offer_setting_id = ' . $this->sqlPropertyRenewalOfferSettingId() . ','; } elseif( true == array_key_exists( 'PropertyRenewalOfferSettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_renewal_offer_setting_id = ' . $this->sqlPropertyRenewalOfferSettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_lease_period_id = ' . $this->sqlPropertyLeasePeriodId() . ','; } elseif( true == array_key_exists( 'PropertyLeasePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' property_lease_period_id = ' . $this->sqlPropertyLeasePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_ar_code_id = ' . $this->sqlConcessionArCodeId() . ','; } elseif( true == array_key_exists( 'ConcessionArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' concession_ar_code_id = ' . $this->sqlConcessionArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_datetime = ' . $this->sqlOfferDatetime() . ','; } elseif( true == array_key_exists( 'OfferDatetime', $this->getChangedColumns() ) ) { $strSql .= ' offer_datetime = ' . $this->sqlOfferDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_lease_term = ' . $this->sqlMinLeaseTerm() . ','; } elseif( true == array_key_exists( 'MinLeaseTerm', $this->getChangedColumns() ) ) { $strSql .= ' min_lease_term = ' . $this->sqlMinLeaseTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_lease_term = ' . $this->sqlMaxLeaseTerm() . ','; } elseif( true == array_key_exists( 'MaxLeaseTerm', $this->getChangedColumns() ) ) { $strSql .= ' max_lease_term = ' . $this->sqlMaxLeaseTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive = ' . $this->sqlIncentive() . ','; } elseif( true == array_key_exists( 'Incentive', $this->getChangedColumns() ) ) { $strSql .= ' incentive = ' . $this->sqlIncentive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_description = ' . $this->sqlIncentiveDescription() . ','; } elseif( true == array_key_exists( 'IncentiveDescription', $this->getChangedColumns() ) ) { $strSql .= ' incentive_description = ' . $this->sqlIncentiveDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' incentive_amount = ' . $this->sqlIncentiveAmount() . ','; } elseif( true == array_key_exists( 'IncentiveAmount', $this->getChangedColumns() ) ) { $strSql .= ' incentive_amount = ' . $this->sqlIncentiveAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_amount = ' . $this->sqlConcessionAmount() . ','; } elseif( true == array_key_exists( 'ConcessionAmount', $this->getChangedColumns() ) ) { $strSql .= ' concession_amount = ' . $this->sqlConcessionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'property_renewal_offer_setting_id' => $this->getPropertyRenewalOfferSettingId(),
			'property_lease_period_id' => $this->getPropertyLeasePeriodId(),
			'concession_ar_code_id' => $this->getConcessionArCodeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'offer_datetime' => $this->getOfferDatetime(),
			'min_lease_term' => $this->getMinLeaseTerm(),
			'max_lease_term' => $this->getMaxLeaseTerm(),
			'incentive' => $this->getIncentive(),
			'incentive_description' => $this->getIncentiveDescription(),
			'incentive_amount' => $this->getIncentiveAmount(),
			'concession_amount' => $this->getConcessionAmount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>