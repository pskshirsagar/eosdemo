<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApprovalPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApprovalPreferences extends CEosPluralBase {

	/**
	 * @return CApprovalPreference[]
	 */
	public static function fetchApprovalPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApprovalPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApprovalPreference
	 */
	public static function fetchApprovalPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApprovalPreference', $objDatabase );
	}

	public static function fetchApprovalPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'approval_preferences', $objDatabase );
	}

	public static function fetchApprovalPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApprovalPreference( sprintf( 'SELECT * FROM approval_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApprovalPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchApprovalPreferences( sprintf( 'SELECT * FROM approval_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApprovalPreferenceByRouteTypeIdByCid( $intRouteTypeId, $intCid, $objDatabase ) {
		return self::fetchApprovalPreference( sprintf( 'SELECT * FROM approval_preferences WHERE route_type_id = %d AND cid = %d', ( int ) $intRouteTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApprovalPreferencesByRouteTypeIdByCid( $intRouteTypeId, $intCid, $objDatabase ) {
		return self::fetchApprovalPreferences( sprintf( 'SELECT * FROM approval_preferences WHERE route_type_id = %d AND cid = %d', ( int ) $intRouteTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApprovalPreferencesByDefaultCompanyGroupIdByCid( $intDefaultCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchApprovalPreferences( sprintf( 'SELECT * FROM approval_preferences WHERE default_company_group_id = %d AND cid = %d', ( int ) $intDefaultCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApprovalPreferencesByDefaultCompanyUserIdByCid( $intDefaultCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchApprovalPreferences( sprintf( 'SELECT * FROM approval_preferences WHERE default_company_user_id = %d AND cid = %d', ( int ) $intDefaultCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>