<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOrganizationContract extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.organization_contracts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intOrganizationId;
	protected $m_intOrganizationContractStatusTypeId;
	protected $m_intOrganizationContractFinancialResponsibilityTypeId;
	protected $m_intCompanyApplicationId;
	protected $m_intLeaseStartWindowId;
	protected $m_strName;
	protected $m_strReservationCode;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intPrimaryCustomerRelationshipId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['organization_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationId', trim( $arrValues['organization_id'] ) ); elseif( isset( $arrValues['organization_id'] ) ) $this->setOrganizationId( $arrValues['organization_id'] );
		if( isset( $arrValues['organization_contract_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractStatusTypeId', trim( $arrValues['organization_contract_status_type_id'] ) ); elseif( isset( $arrValues['organization_contract_status_type_id'] ) ) $this->setOrganizationContractStatusTypeId( $arrValues['organization_contract_status_type_id'] );
		if( isset( $arrValues['organization_contract_financial_responsibility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractFinancialResponsibilityTypeId', trim( $arrValues['organization_contract_financial_responsibility_type_id'] ) ); elseif( isset( $arrValues['organization_contract_financial_responsibility_type_id'] ) ) $this->setOrganizationContractFinancialResponsibilityTypeId( $arrValues['organization_contract_financial_responsibility_type_id'] );
		if( isset( $arrValues['company_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyApplicationId', trim( $arrValues['company_application_id'] ) ); elseif( isset( $arrValues['company_application_id'] ) ) $this->setCompanyApplicationId( $arrValues['company_application_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['reservation_code'] ) && $boolDirectSet ) $this->set( 'm_strReservationCode', trim( stripcslashes( $arrValues['reservation_code'] ) ) ); elseif( isset( $arrValues['reservation_code'] ) ) $this->setReservationCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reservation_code'] ) : $arrValues['reservation_code'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['primary_customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerRelationshipId', trim( $arrValues['primary_customer_relationship_id'] ) ); elseif( isset( $arrValues['primary_customer_relationship_id'] ) ) $this->setPrimaryCustomerRelationshipId( $arrValues['primary_customer_relationship_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setOrganizationId( $intOrganizationId ) {
		$this->set( 'm_intOrganizationId', CStrings::strToIntDef( $intOrganizationId, NULL, false ) );
	}

	public function getOrganizationId() {
		return $this->m_intOrganizationId;
	}

	public function sqlOrganizationId() {
		return ( true == isset( $this->m_intOrganizationId ) ) ? ( string ) $this->m_intOrganizationId : 'NULL';
	}

	public function setOrganizationContractStatusTypeId( $intOrganizationContractStatusTypeId ) {
		$this->set( 'm_intOrganizationContractStatusTypeId', CStrings::strToIntDef( $intOrganizationContractStatusTypeId, NULL, false ) );
	}

	public function getOrganizationContractStatusTypeId() {
		return $this->m_intOrganizationContractStatusTypeId;
	}

	public function sqlOrganizationContractStatusTypeId() {
		return ( true == isset( $this->m_intOrganizationContractStatusTypeId ) ) ? ( string ) $this->m_intOrganizationContractStatusTypeId : 'NULL';
	}

	public function setOrganizationContractFinancialResponsibilityTypeId( $intOrganizationContractFinancialResponsibilityTypeId ) {
		$this->set( 'm_intOrganizationContractFinancialResponsibilityTypeId', CStrings::strToIntDef( $intOrganizationContractFinancialResponsibilityTypeId, NULL, false ) );
	}

	public function getOrganizationContractFinancialResponsibilityTypeId() {
		return $this->m_intOrganizationContractFinancialResponsibilityTypeId;
	}

	public function sqlOrganizationContractFinancialResponsibilityTypeId() {
		return ( true == isset( $this->m_intOrganizationContractFinancialResponsibilityTypeId ) ) ? ( string ) $this->m_intOrganizationContractFinancialResponsibilityTypeId : 'NULL';
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->set( 'm_intCompanyApplicationId', CStrings::strToIntDef( $intCompanyApplicationId, NULL, false ) );
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function sqlCompanyApplicationId() {
		return ( true == isset( $this->m_intCompanyApplicationId ) ) ? ( string ) $this->m_intCompanyApplicationId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 256, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setReservationCode( $strReservationCode ) {
		$this->set( 'm_strReservationCode', CStrings::strTrimDef( $strReservationCode, 256, NULL, true ) );
	}

	public function getReservationCode() {
		return $this->m_strReservationCode;
	}

	public function sqlReservationCode() {
		return ( true == isset( $this->m_strReservationCode ) ) ? '\'' . addslashes( $this->m_strReservationCode ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPrimaryCustomerRelationshipId( $intPrimaryCustomerRelationshipId ) {
		$this->set( 'm_intPrimaryCustomerRelationshipId', CStrings::strToIntDef( $intPrimaryCustomerRelationshipId, NULL, false ) );
	}

	public function sqlPrimaryCustomerRelationshipId() {
		return ( true == isset( $this->m_intPrimaryCustomerRelationshipId ) ) ? ( string ) $this->m_intPrimaryCustomerRelationshipId : 'NULL';
	}

	public function getPrimaryCustomerRelationshipId() {
		return $this->m_intPrimaryCustomerRelationshipId;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, organization_id, organization_contract_status_type_id, organization_contract_financial_responsibility_type_id, company_application_id, lease_start_window_id, name, reservation_code, updated_by, updated_on, created_by, created_on, details, primary_customer_relationship_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlOrganizationId() . ', ' .
						$this->sqlOrganizationContractStatusTypeId() . ', ' .
						$this->sqlOrganizationContractFinancialResponsibilityTypeId() . ', ' .
						$this->sqlCompanyApplicationId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlReservationCode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlPrimaryCustomerRelationshipId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_id = ' . $this->sqlOrganizationId(). ',' ; } elseif( true == array_key_exists( 'OrganizationId', $this->getChangedColumns() ) ) { $strSql .= ' organization_id = ' . $this->sqlOrganizationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_status_type_id = ' . $this->sqlOrganizationContractStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_status_type_id = ' . $this->sqlOrganizationContractStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_financial_responsibility_type_id = ' . $this->sqlOrganizationContractFinancialResponsibilityTypeId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractFinancialResponsibilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_financial_responsibility_type_id = ' . $this->sqlOrganizationContractFinancialResponsibilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId(). ',' ; } elseif( true == array_key_exists( 'CompanyApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_code = ' . $this->sqlReservationCode(). ',' ; } elseif( true == array_key_exists( 'ReservationCode', $this->getChangedColumns() ) ) { $strSql .= ' reservation_code = ' . $this->sqlReservationCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_customer_relationship_id = ' . $this->sqlPrimaryCustomerRelationshipId(). ',' ; } elseif( true == array_key_exists( 'PrimaryCustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' primary_customer_relationship_id = ' . $this->sqlPrimaryCustomerRelationshipId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'organization_id' => $this->getOrganizationId(),
			'organization_contract_status_type_id' => $this->getOrganizationContractStatusTypeId(),
			'organization_contract_financial_responsibility_type_id' => $this->getOrganizationContractFinancialResponsibilityTypeId(),
			'company_application_id' => $this->getCompanyApplicationId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'name' => $this->getName(),
			'reservation_code' => $this->getReservationCode(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'primary_customer_relationship_id' => $this->getPrimaryCustomerRelationshipId()
		);
	}

}
?>