<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForcedPlacedPolicies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseForcedPlacedPolicies extends CEosPluralBase {

	/**
	 * @return CForcedPlacedPolicy[]
	 */
	public static function fetchForcedPlacedPolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CForcedPlacedPolicy', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CForcedPlacedPolicy
	 */
	public static function fetchForcedPlacedPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CForcedPlacedPolicy', $objDatabase );
	}

	public static function fetchForcedPlacedPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'forced_placed_policies', $objDatabase );
	}

	public static function fetchForcedPlacedPolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicy( sprintf( 'SELECT * FROM forced_placed_policies WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE resident_insurance_policy_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchForcedPlacedPoliciesByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchForcedPlacedPolicies( sprintf( 'SELECT * FROM forced_placed_policies WHERE ar_transaction_id = %d AND cid = %d', ( int ) $intArTransactionId, ( int ) $intCid ), $objDatabase );
	}

}
?>