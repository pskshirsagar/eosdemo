<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmailTemplatePropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplatePropertyGroups extends CEosPluralBase {

	/**
	 * @return CEmailTemplatePropertyGroup[]
	 */
	public static function fetchEmailTemplatePropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CEmailTemplatePropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplatePropertyGroup
	 */
	public static function fetchEmailTemplatePropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmailTemplatePropertyGroup::class, $objDatabase );
	}

	public static function fetchEmailTemplatePropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_property_groups', $objDatabase );
	}

	public static function fetchEmailTemplatePropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplatePropertyGroup( sprintf( 'SELECT * FROM email_template_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatePropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplatePropertyGroups( sprintf( 'SELECT * FROM email_template_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatePropertyGroupsByEmailTemplateIdByCid( $intEmailTemplateId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplatePropertyGroups( sprintf( 'SELECT * FROM email_template_property_groups WHERE email_template_id = %d AND cid = %d', ( int ) $intEmailTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatePropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplatePropertyGroups( sprintf( 'SELECT * FROM email_template_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>