<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAssociationTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentAssociationTypes extends CEosPluralBase {

	/**
	 * @return CDocumentAssociationType[]
	 */
	public static function fetchDocumentAssociationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDocumentAssociationType::class, $objDatabase );
	}

	/**
	 * @return CDocumentAssociationType
	 */
	public static function fetchDocumentAssociationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentAssociationType::class, $objDatabase );
	}

	public static function fetchDocumentAssociationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_association_types', $objDatabase );
	}

	public static function fetchDocumentAssociationTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentAssociationType( sprintf( 'SELECT * FROM document_association_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>