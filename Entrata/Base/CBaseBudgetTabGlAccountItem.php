<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccountItem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.budget_tab_gl_account_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetId;
	protected $m_intBudgetTabGlAccountId;
	protected $m_intBudgetDataSourceTypeId;
	protected $m_strItemName;
	protected $m_fltUnitPrice;
	protected $m_strFormula;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltUnitPrice = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['budget_tab_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabGlAccountId', trim( $arrValues['budget_tab_gl_account_id'] ) ); elseif( isset( $arrValues['budget_tab_gl_account_id'] ) ) $this->setBudgetTabGlAccountId( $arrValues['budget_tab_gl_account_id'] );
		if( isset( $arrValues['budget_data_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceTypeId', trim( $arrValues['budget_data_source_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_type_id'] ) ) $this->setBudgetDataSourceTypeId( $arrValues['budget_data_source_type_id'] );
		if( isset( $arrValues['item_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strItemName', trim( stripcslashes( $arrValues['item_name'] ) ) ); elseif( isset( $arrValues['item_name'] ) ) $this->setItemName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_name'] ) : $arrValues['item_name'] );
		if( isset( $arrValues['unit_price'] ) && $boolDirectSet ) $this->set( 'm_fltUnitPrice', trim( $arrValues['unit_price'] ) ); elseif( isset( $arrValues['unit_price'] ) ) $this->setUnitPrice( $arrValues['unit_price'] );
		if( isset( $arrValues['formula'] ) && $boolDirectSet ) $this->set( 'm_strFormula', trim( stripcslashes( $arrValues['formula'] ) ) ); elseif( isset( $arrValues['formula'] ) ) $this->setFormula( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['formula'] ) : $arrValues['formula'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setBudgetTabGlAccountId( $intBudgetTabGlAccountId ) {
		$this->set( 'm_intBudgetTabGlAccountId', CStrings::strToIntDef( $intBudgetTabGlAccountId, NULL, false ) );
	}

	public function getBudgetTabGlAccountId() {
		return $this->m_intBudgetTabGlAccountId;
	}

	public function sqlBudgetTabGlAccountId() {
		return ( true == isset( $this->m_intBudgetTabGlAccountId ) ) ? ( string ) $this->m_intBudgetTabGlAccountId : 'NULL';
	}

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->set( 'm_intBudgetDataSourceTypeId', CStrings::strToIntDef( $intBudgetDataSourceTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function sqlBudgetDataSourceTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceTypeId : 'NULL';
	}

	public function setItemName( $strItemName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strItemName', CStrings::strTrimDef( $strItemName, 100, NULL, true ), $strLocaleCode );
	}

	public function getItemName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strItemName', $strLocaleCode );
	}

	public function sqlItemName() {
		return ( true == isset( $this->m_strItemName ) ) ? '\'' . addslashes( $this->m_strItemName ) . '\'' : 'NULL';
	}

	public function setUnitPrice( $fltUnitPrice ) {
		$this->set( 'm_fltUnitPrice', CStrings::strToFloatDef( $fltUnitPrice, NULL, false, 2 ) );
	}

	public function getUnitPrice() {
		return $this->m_fltUnitPrice;
	}

	public function sqlUnitPrice() {
		return ( true == isset( $this->m_fltUnitPrice ) ) ? ( string ) $this->m_fltUnitPrice : '0';
	}

	public function setFormula( $strFormula ) {
		$this->set( 'm_strFormula', CStrings::strTrimDef( $strFormula, -1, NULL, true ) );
	}

	public function getFormula() {
		return $this->m_strFormula;
	}

	public function sqlFormula() {
		return ( true == isset( $this->m_strFormula ) ) ? '\'' . addslashes( $this->m_strFormula ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_id, budget_tab_gl_account_id, budget_data_source_type_id, item_name, unit_price, formula, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetId() . ', ' .
						$this->sqlBudgetTabGlAccountId() . ', ' .
						$this->sqlBudgetDataSourceTypeId() . ', ' .
						$this->sqlItemName() . ', ' .
						$this->sqlUnitPrice() . ', ' .
						$this->sqlFormula() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId(). ',' ; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId(). ',' ; } elseif( true == array_key_exists( 'BudgetTabGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_gl_account_id = ' . $this->sqlBudgetTabGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_name = ' . $this->sqlItemName(). ',' ; } elseif( true == array_key_exists( 'ItemName', $this->getChangedColumns() ) ) { $strSql .= ' item_name = ' . $this->sqlItemName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_price = ' . $this->sqlUnitPrice(). ',' ; } elseif( true == array_key_exists( 'UnitPrice', $this->getChangedColumns() ) ) { $strSql .= ' unit_price = ' . $this->sqlUnitPrice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' formula = ' . $this->sqlFormula(). ',' ; } elseif( true == array_key_exists( 'Formula', $this->getChangedColumns() ) ) { $strSql .= ' formula = ' . $this->sqlFormula() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_id' => $this->getBudgetId(),
			'budget_tab_gl_account_id' => $this->getBudgetTabGlAccountId(),
			'budget_data_source_type_id' => $this->getBudgetDataSourceTypeId(),
			'item_name' => $this->getItemName(),
			'unit_price' => $this->getUnitPrice(),
			'formula' => $this->getFormula(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>