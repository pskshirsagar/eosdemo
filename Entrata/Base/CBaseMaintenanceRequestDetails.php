<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestDetails extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestDetail[]
	 */
	public static function fetchMaintenanceRequestDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestDetail
	 */
	public static function fetchMaintenanceRequestDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestDetail::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_details', $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetail( sprintf( 'SELECT * FROM maintenance_request_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByMainPhoneNumberTypeIdByCid( $intMainPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE main_phone_number_type_id = %d AND cid = %d', $intMainPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByAltPhoneNumberTypeIdByCid( $intAltPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE alt_phone_number_type_id = %d AND cid = %d', $intAltPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByMainMessageOperatorIdByCid( $intMainMessageOperatorId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE main_message_operator_id = %d AND cid = %d', $intMainMessageOperatorId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByAltMessageOperatorIdByCid( $intAltMessageOperatorId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE alt_message_operator_id = %d AND cid = %d', $intAltMessageOperatorId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByMaintenanceExceptionIdByCid( $intMaintenanceExceptionId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE maintenance_exception_id = %d AND cid = %d', $intMaintenanceExceptionId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestDetailsByMaintenanceRequestMediumIdByCid( $intMaintenanceRequestMediumId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestDetails( sprintf( 'SELECT * FROM maintenance_request_details WHERE maintenance_request_medium_id = %d AND cid = %d', $intMaintenanceRequestMediumId, $intCid ), $objDatabase );
	}

}
?>