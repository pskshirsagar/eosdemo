<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlobalUserLease extends CEosSingularBase {

	const TABLE_NAME = 'public.global_user_leases';

	protected $m_intId;
	protected $m_intGlobalUserId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intLeaseCustomerId;
	protected $m_strLeaseStatusName;
	protected $m_intLeaseStatusTypeId;
	protected $m_intPropertyUnitId;
	protected $m_strUnitNumber;
	protected $m_strBuildingName;
	protected $m_boolUnitIsSmart;
	protected $m_strHaTermsAgreedOn;
	protected $m_strHaTermsAgreedVersion;
	protected $m_boolIsLoginDisabled;
	protected $m_strLoginDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUnitSpaceId;

	public function __construct() {
		parent::__construct();

		$this->m_boolUnitIsSmart = false;
		$this->m_boolIsLoginDisabled = false;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['global_user_id'] ) && $boolDirectSet ) $this->set( 'm_intGlobalUserId', trim( $arrValues['global_user_id'] ) ); elseif( isset( $arrValues['global_user_id'] ) ) $this->setGlobalUserId( $arrValues['global_user_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCustomerId', trim( $arrValues['lease_customer_id'] ) ); elseif( isset( $arrValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrValues['lease_customer_id'] );
		if( isset( $arrValues['lease_status_name'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStatusName', trim( $arrValues['lease_status_name'] ) ); elseif( isset( $arrValues['lease_status_name'] ) ) $this->setLeaseStatusName( $arrValues['lease_status_name'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( $arrValues['unit_number'] ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( $arrValues['unit_number'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['unit_is_smart'] ) && $boolDirectSet ) $this->set( 'm_boolUnitIsSmart', trim( stripcslashes( $arrValues['unit_is_smart'] ) ) ); elseif( isset( $arrValues['unit_is_smart'] ) ) $this->setUnitIsSmart( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_is_smart'] ) : $arrValues['unit_is_smart'] );
		if( isset( $arrValues['ha_terms_agreed_on'] ) && $boolDirectSet ) $this->set( 'm_strHaTermsAgreedOn', trim( $arrValues['ha_terms_agreed_on'] ) ); elseif( isset( $arrValues['ha_terms_agreed_on'] ) ) $this->setHaTermsAgreedOn( $arrValues['ha_terms_agreed_on'] );
		if( isset( $arrValues['ha_terms_agreed_version'] ) && $boolDirectSet ) $this->set( 'm_strHaTermsAgreedVersion', trim( $arrValues['ha_terms_agreed_version'] ) ); elseif( isset( $arrValues['ha_terms_agreed_version'] ) ) $this->setHaTermsAgreedVersion( $arrValues['ha_terms_agreed_version'] );
		if( isset( $arrValues['is_login_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsLoginDisabled', trim( stripcslashes( $arrValues['is_login_disabled'] ) ) ); elseif( isset( $arrValues['is_login_disabled'] ) ) $this->setIsLoginDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_login_disabled'] ) : $arrValues['is_login_disabled'] );
		if( isset( $arrValues['login_disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strLoginDisabledOn', trim( $arrValues['login_disabled_on'] ) ); elseif( isset( $arrValues['login_disabled_on'] ) ) $this->setLoginDisabledOn( $arrValues['login_disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setGlobalUserId( $intGlobalUserId ) {
		$this->set( 'm_intGlobalUserId', CStrings::strToIntDef( $intGlobalUserId, NULL, false ) );
	}

	public function getGlobalUserId() {
		return $this->m_intGlobalUserId;
	}

	public function sqlGlobalUserId() {
		return ( true == isset( $this->m_intGlobalUserId ) ) ? ( string ) $this->m_intGlobalUserId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->set( 'm_intLeaseCustomerId', CStrings::strToIntDef( $intLeaseCustomerId, NULL, false ) );
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function sqlLeaseCustomerId() {
		return ( true == isset( $this->m_intLeaseCustomerId ) ) ? ( string ) $this->m_intLeaseCustomerId : 'NULL';
	}

	public function setLeaseStatusName( $strLeaseStatusName ) {
		$this->set( 'm_strLeaseStatusName', CStrings::strTrimDef( $strLeaseStatusName, 50, NULL, true ) );
	}

	public function getLeaseStatusName() {
		return $this->m_strLeaseStatusName;
	}

	public function sqlLeaseStatusName() {
		return ( true == isset( $this->m_strLeaseStatusName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseStatusName ) : '\'' . addslashes( $this->m_strLeaseStatusName ) . '\'' ) : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumber ) : '\'' . addslashes( $this->m_strUnitNumber ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setUnitIsSmart( $boolUnitIsSmart ) {
		$this->set( 'm_boolUnitIsSmart', CStrings::strToBool( $boolUnitIsSmart ) );
	}

	public function getUnitIsSmart() {
		return $this->m_boolUnitIsSmart;
	}

	public function sqlUnitIsSmart() {
		return ( true == isset( $this->m_boolUnitIsSmart ) ) ? '\'' . ( true == ( bool ) $this->m_boolUnitIsSmart ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHaTermsAgreedOn( $strHaTermsAgreedOn ) {
		$this->set( 'm_strHaTermsAgreedOn', CStrings::strTrimDef( $strHaTermsAgreedOn, -1, NULL, true ) );
	}

	public function getHaTermsAgreedOn() {
		return $this->m_strHaTermsAgreedOn;
	}

	public function sqlHaTermsAgreedOn() {
		return ( true == isset( $this->m_strHaTermsAgreedOn ) ) ? '\'' . $this->m_strHaTermsAgreedOn . '\'' : 'NULL';
	}

	public function setHaTermsAgreedVersion( $strHaTermsAgreedVersion ) {
		$this->set( 'm_strHaTermsAgreedVersion', CStrings::strTrimDef( $strHaTermsAgreedVersion, 10, NULL, true ) );
	}

	public function getHaTermsAgreedVersion() {
		return $this->m_strHaTermsAgreedVersion;
	}

	public function sqlHaTermsAgreedVersion() {
		return ( true == isset( $this->m_strHaTermsAgreedVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHaTermsAgreedVersion ) : '\'' . addslashes( $this->m_strHaTermsAgreedVersion ) . '\'' ) : 'NULL';
	}

	public function setIsLoginDisabled( $boolIsLoginDisabled ) {
		$this->set( 'm_boolIsLoginDisabled', CStrings::strToBool( $boolIsLoginDisabled ) );
	}

	public function getIsLoginDisabled() {
		return $this->m_boolIsLoginDisabled;
	}

	public function sqlIsLoginDisabled() {
		return ( true == isset( $this->m_boolIsLoginDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLoginDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLoginDisabledOn( $strLoginDisabledOn ) {
		$this->set( 'm_strLoginDisabledOn', CStrings::strTrimDef( $strLoginDisabledOn, -1, NULL, true ) );
	}

	public function getLoginDisabledOn() {
		return $this->m_strLoginDisabledOn;
	}

	public function sqlLoginDisabledOn() {
		return ( true == isset( $this->m_strLoginDisabledOn ) ) ? '\'' . $this->m_strLoginDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, global_user_id, cid, property_id, customer_id, lease_id, lease_customer_id, lease_status_name, lease_status_type_id, property_unit_id, unit_number, building_name, unit_is_smart, ha_terms_agreed_on, ha_terms_agreed_version, is_login_disabled, login_disabled_on, updated_by, updated_on, created_by, created_on, unit_space_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlGlobalUserId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseCustomerId() . ', ' .
						$this->sqlLeaseStatusName() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitIsSmart() . ', ' .
						$this->sqlHaTermsAgreedOn() . ', ' .
						$this->sqlHaTermsAgreedVersion() . ', ' .
						$this->sqlIsLoginDisabled() . ', ' .
						$this->sqlLoginDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUnitSpaceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' global_user_id = ' . $this->sqlGlobalUserId(). ',' ; } elseif( true == array_key_exists( 'GlobalUserId', $this->getChangedColumns() ) ) { $strSql .= ' global_user_id = ' . $this->sqlGlobalUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId(). ',' ; } elseif( true == array_key_exists( 'LeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_name = ' . $this->sqlLeaseStatusName(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusName', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_name = ' . $this->sqlLeaseStatusName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_is_smart = ' . $this->sqlUnitIsSmart(). ',' ; } elseif( true == array_key_exists( 'UnitIsSmart', $this->getChangedColumns() ) ) { $strSql .= ' unit_is_smart = ' . $this->sqlUnitIsSmart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ha_terms_agreed_on = ' . $this->sqlHaTermsAgreedOn(). ',' ; } elseif( true == array_key_exists( 'HaTermsAgreedOn', $this->getChangedColumns() ) ) { $strSql .= ' ha_terms_agreed_on = ' . $this->sqlHaTermsAgreedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ha_terms_agreed_version = ' . $this->sqlHaTermsAgreedVersion(). ',' ; } elseif( true == array_key_exists( 'HaTermsAgreedVersion', $this->getChangedColumns() ) ) { $strSql .= ' ha_terms_agreed_version = ' . $this->sqlHaTermsAgreedVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_login_disabled = ' . $this->sqlIsLoginDisabled(). ',' ; } elseif( true == array_key_exists( 'IsLoginDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_login_disabled = ' . $this->sqlIsLoginDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_disabled_on = ' . $this->sqlLoginDisabledOn(). ',' ; } elseif( true == array_key_exists( 'LoginDisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' login_disabled_on = ' . $this->sqlLoginDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'global_user_id' => $this->getGlobalUserId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'lease_customer_id' => $this->getLeaseCustomerId(),
			'lease_status_name' => $this->getLeaseStatusName(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_number' => $this->getUnitNumber(),
			'building_name' => $this->getBuildingName(),
			'unit_is_smart' => $this->getUnitIsSmart(),
			'ha_terms_agreed_on' => $this->getHaTermsAgreedOn(),
			'ha_terms_agreed_version' => $this->getHaTermsAgreedVersion(),
			'is_login_disabled' => $this->getIsLoginDisabled(),
			'login_disabled_on' => $this->getLoginDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'unit_space_id' => $this->getUnitSpaceId()
		);
	}

}
?>