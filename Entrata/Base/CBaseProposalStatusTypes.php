<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposalStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseProposalStatusTypes extends CEosPluralBase {

	/**
	 * @return CProposalStatusType[]
	 */
	public static function fetchProposalStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CProposalStatusType::class, $objDatabase );
	}

	/**
	 * @return CProposalStatusType
	 */
	public static function fetchProposalStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProposalStatusType::class, $objDatabase );
	}

	public static function fetchProposalStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'proposal_status_types', $objDatabase );
	}

	public static function fetchProposalStatusTypeById( $intId, $objDatabase ) {
		return self::fetchProposalStatusType( sprintf( 'SELECT * FROM proposal_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>