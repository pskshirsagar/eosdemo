<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenanceProblems
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenanceProblems extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenanceProblem[]
	 */
	public static function fetchDefaultMaintenanceProblems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenanceProblem::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenanceProblem
	 */
	public static function fetchDefaultMaintenanceProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenanceProblem::class, $objDatabase );
	}

	public static function fetchDefaultMaintenanceProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_problems', $objDatabase );
	}

	public static function fetchDefaultMaintenanceProblemById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenanceProblem( sprintf( 'SELECT * FROM default_maintenance_problems WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceProblemsByMaintenanceProblemId( $intMaintenanceProblemId, $objDatabase ) {
		return self::fetchDefaultMaintenanceProblems( sprintf( 'SELECT * FROM default_maintenance_problems WHERE maintenance_problem_id = %d', $intMaintenanceProblemId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceProblemsByMaintenanceProblemTypeId( $intMaintenanceProblemTypeId, $objDatabase ) {
		return self::fetchDefaultMaintenanceProblems( sprintf( 'SELECT * FROM default_maintenance_problems WHERE maintenance_problem_type_id = %d', $intMaintenanceProblemTypeId ), $objDatabase );
	}

}
?>