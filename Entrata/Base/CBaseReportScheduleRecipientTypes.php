<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleRecipientTypes
 * Do not add any new functions to this class.
 */

class CBaseReportScheduleRecipientTypes extends CEosPluralBase {

	/**
	 * @return CReportScheduleRecipientType[]
	 */
	public static function fetchReportScheduleRecipientTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportScheduleRecipientType::class, $objDatabase );
	}

	/**
	 * @return CReportScheduleRecipientType
	 */
	public static function fetchReportScheduleRecipientType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportScheduleRecipientType::class, $objDatabase );
	}

	public static function fetchReportScheduleRecipientTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_schedule_recipient_types', $objDatabase );
	}

	public static function fetchReportScheduleRecipientTypeById( $intId, $objDatabase ) {
		return self::fetchReportScheduleRecipientType( sprintf( 'SELECT * FROM report_schedule_recipient_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>