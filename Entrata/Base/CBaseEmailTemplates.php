<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmailTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplates extends CEosPluralBase {

	/**
	 * @return CEmailTemplate[]
	 */
	public static function fetchEmailTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CEmailTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplate
	 */
	public static function fetchEmailTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmailTemplate::class, $objDatabase );
	}

	public static function fetchEmailTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_templates', $objDatabase );
	}

	public static function fetchEmailTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplate( sprintf( 'SELECT * FROM email_templates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplates( sprintf( 'SELECT * FROM email_templates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatesByScheduledEmailTypeIdByCid( $intScheduledEmailTypeId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplates( sprintf( 'SELECT * FROM email_templates WHERE scheduled_email_type_id = %d AND cid = %d', $intScheduledEmailTypeId, $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatesByEmailTemplateFolderIdByCid( $intEmailTemplateFolderId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplates( sprintf( 'SELECT * FROM email_templates WHERE email_template_folder_id = %d AND cid = %d', $intEmailTemplateFolderId, $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatesBySystemMessageTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplates( sprintf( 'SELECT * FROM email_templates WHERE system_message_template_id = %d AND cid = %d', $intSystemMessageTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplatesByHeaderFooterOptionTypeIdByCid( $intHeaderFooterOptionTypeId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplates( sprintf( 'SELECT * FROM email_templates WHERE header_footer_option_type_id = %d AND cid = %d', $intHeaderFooterOptionTypeId, $intCid ), $objDatabase );
	}

}
?>