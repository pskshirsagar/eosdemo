<?php

class CBaseDefaultSpaceConfiguration extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_space_configurations';

	protected $m_intId;
	protected $m_strName;
	protected $m_intUnitSpaceCount;
	protected $m_boolIsStudent;
	protected $m_boolIsDefault;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintOccupancyTypeIds;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intUnitSpaceCount = '0';
		$this->m_boolIsStudent = false;
		$this->m_boolIsDefault = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceCount', trim( $arrValues['unit_space_count'] ) ); elseif( isset( $arrValues['unit_space_count'] ) ) $this->setUnitSpaceCount( $arrValues['unit_space_count'] );
		if( isset( $arrValues['is_student'] ) && $boolDirectSet ) $this->set( 'm_boolIsStudent', trim( stripcslashes( $arrValues['is_student'] ) ) ); elseif( isset( $arrValues['is_student'] ) ) $this->setIsStudent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_student'] ) : $arrValues['is_student'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setUnitSpaceCount( $intUnitSpaceCount ) {
		$this->set( 'm_intUnitSpaceCount', CStrings::strToIntDef( $intUnitSpaceCount, NULL, false ) );
	}

	public function getUnitSpaceCount() {
		return $this->m_intUnitSpaceCount;
	}

	public function sqlUnitSpaceCount() {
		return ( true == isset( $this->m_intUnitSpaceCount ) ) ? ( string ) $this->m_intUnitSpaceCount : '0';
	}

	public function setIsStudent( $boolIsStudent ) {
		$this->set( 'm_boolIsStudent', CStrings::strToBool( $boolIsStudent ) );
	}

	public function getIsStudent() {
		return $this->m_boolIsStudent;
	}

	public function sqlIsStudent() {
		return ( true == isset( $this->m_boolIsStudent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStudent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'unit_space_count' => $this->getUnitSpaceCount(),
			'is_student' => $this->getIsStudent(),
			'is_default' => $this->getIsDefault(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds()
		);
	}

}
?>