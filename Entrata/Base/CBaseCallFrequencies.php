<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallFrequencies
 * Do not add any new functions to this class.
 */

class CBaseCallFrequencies extends CEosPluralBase {

	/**
	 * @return CCallFrequency[]
	 */
	public static function fetchCallFrequencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallFrequency::class, $objDatabase );
	}

	/**
	 * @return CCallFrequency
	 */
	public static function fetchCallFrequency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallFrequency::class, $objDatabase );
	}

	public static function fetchCallFrequencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_frequencies', $objDatabase );
	}

	public static function fetchCallFrequencyById( $intId, $objDatabase ) {
		return self::fetchCallFrequency( sprintf( 'SELECT * FROM call_frequencies WHERE id = %d', $intId ), $objDatabase );
	}

}
?>