<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClAd extends CEosSingularBase {

	const TABLE_NAME = 'public.cl_ads';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intClTemplateId;
	protected $m_intTotalClicks;
	protected $m_strAdDescription;
	protected $m_strAdTitle;
	protected $m_strIpAddress;
	protected $m_strDoneOn;
	protected $m_intFinalizedBy;
	protected $m_strFinalizedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalClicks = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['cl_template_id'] ) && $boolDirectSet ) $this->set( 'm_intClTemplateId', trim( $arrValues['cl_template_id'] ) ); elseif( isset( $arrValues['cl_template_id'] ) ) $this->setClTemplateId( $arrValues['cl_template_id'] );
		if( isset( $arrValues['total_clicks'] ) && $boolDirectSet ) $this->set( 'm_intTotalClicks', trim( $arrValues['total_clicks'] ) ); elseif( isset( $arrValues['total_clicks'] ) ) $this->setTotalClicks( $arrValues['total_clicks'] );
		if( isset( $arrValues['ad_description'] ) && $boolDirectSet ) $this->set( 'm_strAdDescription', trim( stripcslashes( $arrValues['ad_description'] ) ) ); elseif( isset( $arrValues['ad_description'] ) ) $this->setAdDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ad_description'] ) : $arrValues['ad_description'] );
		if( isset( $arrValues['ad_title'] ) && $boolDirectSet ) $this->set( 'm_strAdTitle', trim( stripcslashes( $arrValues['ad_title'] ) ) ); elseif( isset( $arrValues['ad_title'] ) ) $this->setAdTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ad_title'] ) : $arrValues['ad_title'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['done_on'] ) && $boolDirectSet ) $this->set( 'm_strDoneOn', trim( $arrValues['done_on'] ) ); elseif( isset( $arrValues['done_on'] ) ) $this->setDoneOn( $arrValues['done_on'] );
		if( isset( $arrValues['finalized_by'] ) && $boolDirectSet ) $this->set( 'm_intFinalizedBy', trim( $arrValues['finalized_by'] ) ); elseif( isset( $arrValues['finalized_by'] ) ) $this->setFinalizedBy( $arrValues['finalized_by'] );
		if( isset( $arrValues['finalized_on'] ) && $boolDirectSet ) $this->set( 'm_strFinalizedOn', trim( $arrValues['finalized_on'] ) ); elseif( isset( $arrValues['finalized_on'] ) ) $this->setFinalizedOn( $arrValues['finalized_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setClTemplateId( $intClTemplateId ) {
		$this->set( 'm_intClTemplateId', CStrings::strToIntDef( $intClTemplateId, NULL, false ) );
	}

	public function getClTemplateId() {
		return $this->m_intClTemplateId;
	}

	public function sqlClTemplateId() {
		return ( true == isset( $this->m_intClTemplateId ) ) ? ( string ) $this->m_intClTemplateId : 'NULL';
	}

	public function setTotalClicks( $intTotalClicks ) {
		$this->set( 'm_intTotalClicks', CStrings::strToIntDef( $intTotalClicks, NULL, false ) );
	}

	public function getTotalClicks() {
		return $this->m_intTotalClicks;
	}

	public function sqlTotalClicks() {
		return ( true == isset( $this->m_intTotalClicks ) ) ? ( string ) $this->m_intTotalClicks : '0';
	}

	public function setAdDescription( $strAdDescription ) {
		$this->set( 'm_strAdDescription', CStrings::strTrimDef( $strAdDescription, -1, NULL, true ) );
	}

	public function getAdDescription() {
		return $this->m_strAdDescription;
	}

	public function sqlAdDescription() {
		return ( true == isset( $this->m_strAdDescription ) ) ? '\'' . addslashes( $this->m_strAdDescription ) . '\'' : 'NULL';
	}

	public function setAdTitle( $strAdTitle ) {
		$this->set( 'm_strAdTitle', CStrings::strTrimDef( $strAdTitle, -1, NULL, true ) );
	}

	public function getAdTitle() {
		return $this->m_strAdTitle;
	}

	public function sqlAdTitle() {
		return ( true == isset( $this->m_strAdTitle ) ) ? '\'' . addslashes( $this->m_strAdTitle ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setDoneOn( $strDoneOn ) {
		$this->set( 'm_strDoneOn', CStrings::strTrimDef( $strDoneOn, -1, NULL, true ) );
	}

	public function getDoneOn() {
		return $this->m_strDoneOn;
	}

	public function sqlDoneOn() {
		return ( true == isset( $this->m_strDoneOn ) ) ? '\'' . $this->m_strDoneOn . '\'' : 'NULL';
	}

	public function setFinalizedBy( $intFinalizedBy ) {
		$this->set( 'm_intFinalizedBy', CStrings::strToIntDef( $intFinalizedBy, NULL, false ) );
	}

	public function getFinalizedBy() {
		return $this->m_intFinalizedBy;
	}

	public function sqlFinalizedBy() {
		return ( true == isset( $this->m_intFinalizedBy ) ) ? ( string ) $this->m_intFinalizedBy : 'NULL';
	}

	public function setFinalizedOn( $strFinalizedOn ) {
		$this->set( 'm_strFinalizedOn', CStrings::strTrimDef( $strFinalizedOn, -1, NULL, true ) );
	}

	public function getFinalizedOn() {
		return $this->m_strFinalizedOn;
	}

	public function sqlFinalizedOn() {
		return ( true == isset( $this->m_strFinalizedOn ) ) ? '\'' . $this->m_strFinalizedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, cl_template_id, total_clicks, ad_description, ad_title, ip_address, done_on, finalized_by, finalized_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlClTemplateId() . ', ' .
 						$this->sqlTotalClicks() . ', ' .
 						$this->sqlAdDescription() . ', ' .
 						$this->sqlAdTitle() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlDoneOn() . ', ' .
 						$this->sqlFinalizedBy() . ', ' .
 						$this->sqlFinalizedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cl_template_id = ' . $this->sqlClTemplateId() . ','; } elseif( true == array_key_exists( 'ClTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' cl_template_id = ' . $this->sqlClTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_clicks = ' . $this->sqlTotalClicks() . ','; } elseif( true == array_key_exists( 'TotalClicks', $this->getChangedColumns() ) ) { $strSql .= ' total_clicks = ' . $this->sqlTotalClicks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_description = ' . $this->sqlAdDescription() . ','; } elseif( true == array_key_exists( 'AdDescription', $this->getChangedColumns() ) ) { $strSql .= ' ad_description = ' . $this->sqlAdDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_title = ' . $this->sqlAdTitle() . ','; } elseif( true == array_key_exists( 'AdTitle', $this->getChangedColumns() ) ) { $strSql .= ' ad_title = ' . $this->sqlAdTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' done_on = ' . $this->sqlDoneOn() . ','; } elseif( true == array_key_exists( 'DoneOn', $this->getChangedColumns() ) ) { $strSql .= ' done_on = ' . $this->sqlDoneOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' finalized_by = ' . $this->sqlFinalizedBy() . ','; } elseif( true == array_key_exists( 'FinalizedBy', $this->getChangedColumns() ) ) { $strSql .= ' finalized_by = ' . $this->sqlFinalizedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' finalized_on = ' . $this->sqlFinalizedOn() . ','; } elseif( true == array_key_exists( 'FinalizedOn', $this->getChangedColumns() ) ) { $strSql .= ' finalized_on = ' . $this->sqlFinalizedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'cl_template_id' => $this->getClTemplateId(),
			'total_clicks' => $this->getTotalClicks(),
			'ad_description' => $this->getAdDescription(),
			'ad_title' => $this->getAdTitle(),
			'ip_address' => $this->getIpAddress(),
			'done_on' => $this->getDoneOn(),
			'finalized_by' => $this->getFinalizedBy(),
			'finalized_on' => $this->getFinalizedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>