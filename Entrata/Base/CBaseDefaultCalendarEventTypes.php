<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCalendarEventTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultCalendarEventTypes extends CEosPluralBase {

	/**
	 * @return CDefaultCalendarEventType[]
	 */
	public static function fetchDefaultCalendarEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCalendarEventType::class, $objDatabase );
	}

	/**
	 * @return CDefaultCalendarEventType
	 */
	public static function fetchDefaultCalendarEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCalendarEventType::class, $objDatabase );
	}

	public static function fetchDefaultCalendarEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_calendar_event_types', $objDatabase );
	}

	public static function fetchDefaultCalendarEventTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultCalendarEventType( sprintf( 'SELECT * FROM default_calendar_event_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultCalendarEventTypesByCalendarEventCategoryId( $intCalendarEventCategoryId, $objDatabase ) {
		return self::fetchDefaultCalendarEventTypes( sprintf( 'SELECT * FROM default_calendar_event_types WHERE calendar_event_category_id = %d', $intCalendarEventCategoryId ), $objDatabase );
	}

	public static function fetchDefaultCalendarEventTypesByEventTypeId( $intEventTypeId, $objDatabase ) {
		return self::fetchDefaultCalendarEventTypes( sprintf( 'SELECT * FROM default_calendar_event_types WHERE event_type_id = %d', $intEventTypeId ), $objDatabase );
	}

}
?>