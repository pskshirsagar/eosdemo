<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.asset_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAssetId;
	protected $m_intUnitSpaceId;
	protected $m_intApPayeeId;
	protected $m_intMaintenanceLocationId;
	protected $m_intAssetConditionId;
	protected $m_intTaxRecoveryPeriodId;
	protected $m_intDepreciationCategoryId;
	protected $m_strAssetNumber;
	protected $m_strWarrantyEndDate;
	protected $m_strMake;
	protected $m_strModel;
	protected $m_strColor;
	protected $m_fltSalvageValue;
	protected $m_intUsefulLifeMonths;
	protected $m_strDepreciationStartDate;
	protected $m_boolIsSection179;
	protected $m_fltReplacementCost;
	protected $m_fltProceedsFromSale;
	protected $m_strNotes;
	protected $m_intPurchasedBy;
	protected $m_strPurchasedOn;
	protected $m_intPlacedInServiceBy;
	protected $m_strPlacedInServiceOn;
	protected $m_intLastInspectedBy;
	protected $m_strLastInspectedOn;
	protected $m_intRetiredBy;
	protected $m_strRetiredOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intReturnedBy;
	protected $m_strReturnedOn;
	protected $m_intServiceLifeMonths;

	public function __construct() {
		parent::__construct();

		$this->m_fltSalvageValue = '0';
		$this->m_intUsefulLifeMonths = '1';
		$this->m_boolIsSection179 = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['asset_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetId', trim( $arrValues['asset_id'] ) ); elseif( isset( $arrValues['asset_id'] ) ) $this->setAssetId( $arrValues['asset_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationId', trim( $arrValues['maintenance_location_id'] ) ); elseif( isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( isset( $arrValues['asset_condition_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetConditionId', trim( $arrValues['asset_condition_id'] ) ); elseif( isset( $arrValues['asset_condition_id'] ) ) $this->setAssetConditionId( $arrValues['asset_condition_id'] );
		if( isset( $arrValues['tax_recovery_period_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRecoveryPeriodId', trim( $arrValues['tax_recovery_period_id'] ) ); elseif( isset( $arrValues['tax_recovery_period_id'] ) ) $this->setTaxRecoveryPeriodId( $arrValues['tax_recovery_period_id'] );
		if( isset( $arrValues['depreciation_category_id'] ) && $boolDirectSet ) $this->set( 'm_intDepreciationCategoryId', trim( $arrValues['depreciation_category_id'] ) ); elseif( isset( $arrValues['depreciation_category_id'] ) ) $this->setDepreciationCategoryId( $arrValues['depreciation_category_id'] );
		if( isset( $arrValues['asset_number'] ) && $boolDirectSet ) $this->set( 'm_strAssetNumber', trim( $arrValues['asset_number'] ) ); elseif( isset( $arrValues['asset_number'] ) ) $this->setAssetNumber( $arrValues['asset_number'] );
		if( isset( $arrValues['warranty_end_date'] ) && $boolDirectSet ) $this->set( 'm_strWarrantyEndDate', trim( $arrValues['warranty_end_date'] ) ); elseif( isset( $arrValues['warranty_end_date'] ) ) $this->setWarrantyEndDate( $arrValues['warranty_end_date'] );
		if( isset( $arrValues['make'] ) && $boolDirectSet ) $this->set( 'm_strMake', trim( $arrValues['make'] ) ); elseif( isset( $arrValues['make'] ) ) $this->setMake( $arrValues['make'] );
		if( isset( $arrValues['model'] ) && $boolDirectSet ) $this->set( 'm_strModel', trim( $arrValues['model'] ) ); elseif( isset( $arrValues['model'] ) ) $this->setModel( $arrValues['model'] );
		if( isset( $arrValues['color'] ) && $boolDirectSet ) $this->set( 'm_strColor', trim( $arrValues['color'] ) ); elseif( isset( $arrValues['color'] ) ) $this->setColor( $arrValues['color'] );
		if( isset( $arrValues['salvage_value'] ) && $boolDirectSet ) $this->set( 'm_fltSalvageValue', trim( $arrValues['salvage_value'] ) ); elseif( isset( $arrValues['salvage_value'] ) ) $this->setSalvageValue( $arrValues['salvage_value'] );
		if( isset( $arrValues['useful_life_months'] ) && $boolDirectSet ) $this->set( 'm_intUsefulLifeMonths', trim( $arrValues['useful_life_months'] ) ); elseif( isset( $arrValues['useful_life_months'] ) ) $this->setUsefulLifeMonths( $arrValues['useful_life_months'] );
		if( isset( $arrValues['depreciation_start_date'] ) && $boolDirectSet ) $this->set( 'm_strDepreciationStartDate', trim( $arrValues['depreciation_start_date'] ) ); elseif( isset( $arrValues['depreciation_start_date'] ) ) $this->setDepreciationStartDate( $arrValues['depreciation_start_date'] );
		if( isset( $arrValues['is_section_179'] ) && $boolDirectSet ) $this->set( 'm_boolIsSection179', trim( stripcslashes( $arrValues['is_section_179'] ) ) ); elseif( isset( $arrValues['is_section_179'] ) ) $this->setIsSection179( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_section_179'] ) : $arrValues['is_section_179'] );
		if( isset( $arrValues['replacement_cost'] ) && $boolDirectSet ) $this->set( 'm_fltReplacementCost', trim( $arrValues['replacement_cost'] ) ); elseif( isset( $arrValues['replacement_cost'] ) ) $this->setReplacementCost( $arrValues['replacement_cost'] );
		if( isset( $arrValues['proceeds_from_sale'] ) && $boolDirectSet ) $this->set( 'm_fltProceedsFromSale', trim( $arrValues['proceeds_from_sale'] ) ); elseif( isset( $arrValues['proceeds_from_sale'] ) ) $this->setProceedsFromSale( $arrValues['proceeds_from_sale'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['purchased_by'] ) && $boolDirectSet ) $this->set( 'm_intPurchasedBy', trim( $arrValues['purchased_by'] ) ); elseif( isset( $arrValues['purchased_by'] ) ) $this->setPurchasedBy( $arrValues['purchased_by'] );
		if( isset( $arrValues['purchased_on'] ) && $boolDirectSet ) $this->set( 'm_strPurchasedOn', trim( $arrValues['purchased_on'] ) ); elseif( isset( $arrValues['purchased_on'] ) ) $this->setPurchasedOn( $arrValues['purchased_on'] );
		if( isset( $arrValues['placed_in_service_by'] ) && $boolDirectSet ) $this->set( 'm_intPlacedInServiceBy', trim( $arrValues['placed_in_service_by'] ) ); elseif( isset( $arrValues['placed_in_service_by'] ) ) $this->setPlacedInServiceBy( $arrValues['placed_in_service_by'] );
		if( isset( $arrValues['placed_in_service_on'] ) && $boolDirectSet ) $this->set( 'm_strPlacedInServiceOn', trim( $arrValues['placed_in_service_on'] ) ); elseif( isset( $arrValues['placed_in_service_on'] ) ) $this->setPlacedInServiceOn( $arrValues['placed_in_service_on'] );
		if( isset( $arrValues['last_inspected_by'] ) && $boolDirectSet ) $this->set( 'm_intLastInspectedBy', trim( $arrValues['last_inspected_by'] ) ); elseif( isset( $arrValues['last_inspected_by'] ) ) $this->setLastInspectedBy( $arrValues['last_inspected_by'] );
		if( isset( $arrValues['last_inspected_on'] ) && $boolDirectSet ) $this->set( 'm_strLastInspectedOn', trim( $arrValues['last_inspected_on'] ) ); elseif( isset( $arrValues['last_inspected_on'] ) ) $this->setLastInspectedOn( $arrValues['last_inspected_on'] );
		if( isset( $arrValues['retired_by'] ) && $boolDirectSet ) $this->set( 'm_intRetiredBy', trim( $arrValues['retired_by'] ) ); elseif( isset( $arrValues['retired_by'] ) ) $this->setRetiredBy( $arrValues['retired_by'] );
		if( isset( $arrValues['retired_on'] ) && $boolDirectSet ) $this->set( 'm_strRetiredOn', trim( $arrValues['retired_on'] ) ); elseif( isset( $arrValues['retired_on'] ) ) $this->setRetiredOn( $arrValues['retired_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['returned_by'] ) && $boolDirectSet ) $this->set( 'm_intReturnedBy', trim( $arrValues['returned_by'] ) ); elseif( isset( $arrValues['returned_by'] ) ) $this->setReturnedBy( $arrValues['returned_by'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['service_life_months'] ) && $boolDirectSet ) $this->set( 'm_intServiceLifeMonths', trim( $arrValues['service_life_months'] ) ); elseif( isset( $arrValues['service_life_months'] ) ) $this->setServiceLifeMonths( $arrValues['service_life_months'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAssetId( $intAssetId ) {
		$this->set( 'm_intAssetId', CStrings::strToIntDef( $intAssetId, NULL, false ) );
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function sqlAssetId() {
		return ( true == isset( $this->m_intAssetId ) ) ? ( string ) $this->m_intAssetId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->set( 'm_intMaintenanceLocationId', CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false ) );
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function sqlMaintenanceLocationId() {
		return ( true == isset( $this->m_intMaintenanceLocationId ) ) ? ( string ) $this->m_intMaintenanceLocationId : 'NULL';
	}

	public function setAssetConditionId( $intAssetConditionId ) {
		$this->set( 'm_intAssetConditionId', CStrings::strToIntDef( $intAssetConditionId, NULL, false ) );
	}

	public function getAssetConditionId() {
		return $this->m_intAssetConditionId;
	}

	public function sqlAssetConditionId() {
		return ( true == isset( $this->m_intAssetConditionId ) ) ? ( string ) $this->m_intAssetConditionId : 'NULL';
	}

	public function setTaxRecoveryPeriodId( $intTaxRecoveryPeriodId ) {
		$this->set( 'm_intTaxRecoveryPeriodId', CStrings::strToIntDef( $intTaxRecoveryPeriodId, NULL, false ) );
	}

	public function getTaxRecoveryPeriodId() {
		return $this->m_intTaxRecoveryPeriodId;
	}

	public function sqlTaxRecoveryPeriodId() {
		return ( true == isset( $this->m_intTaxRecoveryPeriodId ) ) ? ( string ) $this->m_intTaxRecoveryPeriodId : 'NULL';
	}

	public function setDepreciationCategoryId( $intDepreciationCategoryId ) {
		$this->set( 'm_intDepreciationCategoryId', CStrings::strToIntDef( $intDepreciationCategoryId, NULL, false ) );
	}

	public function getDepreciationCategoryId() {
		return $this->m_intDepreciationCategoryId;
	}

	public function sqlDepreciationCategoryId() {
		return ( true == isset( $this->m_intDepreciationCategoryId ) ) ? ( string ) $this->m_intDepreciationCategoryId : 'NULL';
	}

	public function setAssetNumber( $strAssetNumber ) {
		$this->set( 'm_strAssetNumber', CStrings::strTrimDef( $strAssetNumber, 50, NULL, true ) );
	}

	public function getAssetNumber() {
		return $this->m_strAssetNumber;
	}

	public function sqlAssetNumber() {
		return ( true == isset( $this->m_strAssetNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAssetNumber ) : '\'' . addslashes( $this->m_strAssetNumber ) . '\'' ) : 'NULL';
	}

	public function setWarrantyEndDate( $strWarrantyEndDate ) {
		$this->set( 'm_strWarrantyEndDate', CStrings::strTrimDef( $strWarrantyEndDate, -1, NULL, true ) );
	}

	public function getWarrantyEndDate() {
		return $this->m_strWarrantyEndDate;
	}

	public function sqlWarrantyEndDate() {
		return ( true == isset( $this->m_strWarrantyEndDate ) ) ? '\'' . $this->m_strWarrantyEndDate . '\'' : 'NULL';
	}

	public function setMake( $strMake ) {
		$this->set( 'm_strMake', CStrings::strTrimDef( $strMake, 100, NULL, true ) );
	}

	public function getMake() {
		return $this->m_strMake;
	}

	public function sqlMake() {
		return ( true == isset( $this->m_strMake ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMake ) : '\'' . addslashes( $this->m_strMake ) . '\'' ) : 'NULL';
	}

	public function setModel( $strModel ) {
		$this->set( 'm_strModel', CStrings::strTrimDef( $strModel, 100, NULL, true ) );
	}

	public function getModel() {
		return $this->m_strModel;
	}

	public function sqlModel() {
		return ( true == isset( $this->m_strModel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strModel ) : '\'' . addslashes( $this->m_strModel ) . '\'' ) : 'NULL';
	}

	public function setColor( $strColor ) {
		$this->set( 'm_strColor', CStrings::strTrimDef( $strColor, 100, NULL, true ) );
	}

	public function getColor() {
		return $this->m_strColor;
	}

	public function sqlColor() {
		return ( true == isset( $this->m_strColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strColor ) : '\'' . addslashes( $this->m_strColor ) . '\'' ) : 'NULL';
	}

	public function setSalvageValue( $fltSalvageValue ) {
		$this->set( 'm_fltSalvageValue', CStrings::strToFloatDef( $fltSalvageValue, NULL, false, 2 ) );
	}

	public function getSalvageValue() {
		return $this->m_fltSalvageValue;
	}

	public function sqlSalvageValue() {
		return ( true == isset( $this->m_fltSalvageValue ) ) ? ( string ) $this->m_fltSalvageValue : '0';
	}

	public function setUsefulLifeMonths( $intUsefulLifeMonths ) {
		$this->set( 'm_intUsefulLifeMonths', CStrings::strToIntDef( $intUsefulLifeMonths, NULL, false ) );
	}

	public function getUsefulLifeMonths() {
		return $this->m_intUsefulLifeMonths;
	}

	public function sqlUsefulLifeMonths() {
		return ( true == isset( $this->m_intUsefulLifeMonths ) ) ? ( string ) $this->m_intUsefulLifeMonths : '1';
	}

	public function setDepreciationStartDate( $strDepreciationStartDate ) {
		$this->set( 'm_strDepreciationStartDate', CStrings::strTrimDef( $strDepreciationStartDate, -1, NULL, true ) );
	}

	public function getDepreciationStartDate() {
		return $this->m_strDepreciationStartDate;
	}

	public function sqlDepreciationStartDate() {
		return ( true == isset( $this->m_strDepreciationStartDate ) ) ? '\'' . $this->m_strDepreciationStartDate . '\'' : 'NULL';
	}

	public function setIsSection179( $boolIsSection179 ) {
		$this->set( 'm_boolIsSection179', CStrings::strToBool( $boolIsSection179 ) );
	}

	public function getIsSection179() {
		return $this->m_boolIsSection179;
	}

	public function sqlIsSection179() {
		return ( true == isset( $this->m_boolIsSection179 ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSection179 ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReplacementCost( $fltReplacementCost ) {
		$this->set( 'm_fltReplacementCost', CStrings::strToFloatDef( $fltReplacementCost, NULL, false, 2 ) );
	}

	public function getReplacementCost() {
		return $this->m_fltReplacementCost;
	}

	public function sqlReplacementCost() {
		return ( true == isset( $this->m_fltReplacementCost ) ) ? ( string ) $this->m_fltReplacementCost : 'NULL';
	}

	public function setProceedsFromSale( $fltProceedsFromSale ) {
		$this->set( 'm_fltProceedsFromSale', CStrings::strToFloatDef( $fltProceedsFromSale, NULL, false, 2 ) );
	}

	public function getProceedsFromSale() {
		return $this->m_fltProceedsFromSale;
	}

	public function sqlProceedsFromSale() {
		return ( true == isset( $this->m_fltProceedsFromSale ) ) ? ( string ) $this->m_fltProceedsFromSale : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 240, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setPurchasedBy( $intPurchasedBy ) {
		$this->set( 'm_intPurchasedBy', CStrings::strToIntDef( $intPurchasedBy, NULL, false ) );
	}

	public function getPurchasedBy() {
		return $this->m_intPurchasedBy;
	}

	public function sqlPurchasedBy() {
		return ( true == isset( $this->m_intPurchasedBy ) ) ? ( string ) $this->m_intPurchasedBy : 'NULL';
	}

	public function setPurchasedOn( $strPurchasedOn ) {
		$this->set( 'm_strPurchasedOn', CStrings::strTrimDef( $strPurchasedOn, -1, NULL, true ) );
	}

	public function getPurchasedOn() {
		return $this->m_strPurchasedOn;
	}

	public function sqlPurchasedOn() {
		return ( true == isset( $this->m_strPurchasedOn ) ) ? '\'' . $this->m_strPurchasedOn . '\'' : 'NOW()';
	}

	public function setPlacedInServiceBy( $intPlacedInServiceBy ) {
		$this->set( 'm_intPlacedInServiceBy', CStrings::strToIntDef( $intPlacedInServiceBy, NULL, false ) );
	}

	public function getPlacedInServiceBy() {
		return $this->m_intPlacedInServiceBy;
	}

	public function sqlPlacedInServiceBy() {
		return ( true == isset( $this->m_intPlacedInServiceBy ) ) ? ( string ) $this->m_intPlacedInServiceBy : 'NULL';
	}

	public function setPlacedInServiceOn( $strPlacedInServiceOn ) {
		$this->set( 'm_strPlacedInServiceOn', CStrings::strTrimDef( $strPlacedInServiceOn, -1, NULL, true ) );
	}

	public function getPlacedInServiceOn() {
		return $this->m_strPlacedInServiceOn;
	}

	public function sqlPlacedInServiceOn() {
		return ( true == isset( $this->m_strPlacedInServiceOn ) ) ? '\'' . $this->m_strPlacedInServiceOn . '\'' : 'NULL';
	}

	public function setLastInspectedBy( $intLastInspectedBy ) {
		$this->set( 'm_intLastInspectedBy', CStrings::strToIntDef( $intLastInspectedBy, NULL, false ) );
	}

	public function getLastInspectedBy() {
		return $this->m_intLastInspectedBy;
	}

	public function sqlLastInspectedBy() {
		return ( true == isset( $this->m_intLastInspectedBy ) ) ? ( string ) $this->m_intLastInspectedBy : 'NULL';
	}

	public function setLastInspectedOn( $strLastInspectedOn ) {
		$this->set( 'm_strLastInspectedOn', CStrings::strTrimDef( $strLastInspectedOn, -1, NULL, true ) );
	}

	public function getLastInspectedOn() {
		return $this->m_strLastInspectedOn;
	}

	public function sqlLastInspectedOn() {
		return ( true == isset( $this->m_strLastInspectedOn ) ) ? '\'' . $this->m_strLastInspectedOn . '\'' : 'NULL';
	}

	public function setRetiredBy( $intRetiredBy ) {
		$this->set( 'm_intRetiredBy', CStrings::strToIntDef( $intRetiredBy, NULL, false ) );
	}

	public function getRetiredBy() {
		return $this->m_intRetiredBy;
	}

	public function sqlRetiredBy() {
		return ( true == isset( $this->m_intRetiredBy ) ) ? ( string ) $this->m_intRetiredBy : 'NULL';
	}

	public function setRetiredOn( $strRetiredOn ) {
		$this->set( 'm_strRetiredOn', CStrings::strTrimDef( $strRetiredOn, -1, NULL, true ) );
	}

	public function getRetiredOn() {
		return $this->m_strRetiredOn;
	}

	public function sqlRetiredOn() {
		return ( true == isset( $this->m_strRetiredOn ) ) ? '\'' . $this->m_strRetiredOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReturnedBy( $intReturnedBy ) {
		$this->set( 'm_intReturnedBy', CStrings::strToIntDef( $intReturnedBy, NULL, false ) );
	}

	public function getReturnedBy() {
		return $this->m_intReturnedBy;
	}

	public function sqlReturnedBy() {
		return ( true == isset( $this->m_intReturnedBy ) ) ? ( string ) $this->m_intReturnedBy : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setServiceLifeMonths( $intServiceLifeMonths ) {
		$this->set( 'm_intServiceLifeMonths', CStrings::strToIntDef( $intServiceLifeMonths, NULL, false ) );
	}

	public function getServiceLifeMonths() {
		return $this->m_intServiceLifeMonths;
	}

	public function sqlServiceLifeMonths() {
		return ( true == isset( $this->m_intServiceLifeMonths ) ) ? ( string ) $this->m_intServiceLifeMonths : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, asset_id, unit_space_id, ap_payee_id, maintenance_location_id, asset_condition_id, tax_recovery_period_id, depreciation_category_id, asset_number, warranty_end_date, make, model, color, salvage_value, useful_life_months, depreciation_start_date, is_section_179, replacement_cost, proceeds_from_sale, notes, purchased_by, purchased_on, placed_in_service_by, placed_in_service_on, last_inspected_by, last_inspected_on, retired_by, retired_on, updated_by, updated_on, created_by, created_on, returned_by, returned_on, service_life_months )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAssetId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlMaintenanceLocationId() . ', ' .
						$this->sqlAssetConditionId() . ', ' .
						$this->sqlTaxRecoveryPeriodId() . ', ' .
						$this->sqlDepreciationCategoryId() . ', ' .
						$this->sqlAssetNumber() . ', ' .
						$this->sqlWarrantyEndDate() . ', ' .
						$this->sqlMake() . ', ' .
						$this->sqlModel() . ', ' .
						$this->sqlColor() . ', ' .
						$this->sqlSalvageValue() . ', ' .
						$this->sqlUsefulLifeMonths() . ', ' .
						$this->sqlDepreciationStartDate() . ', ' .
						$this->sqlIsSection179() . ', ' .
						$this->sqlReplacementCost() . ', ' .
						$this->sqlProceedsFromSale() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlPurchasedBy() . ', ' .
						$this->sqlPurchasedOn() . ', ' .
						$this->sqlPlacedInServiceBy() . ', ' .
						$this->sqlPlacedInServiceOn() . ', ' .
						$this->sqlLastInspectedBy() . ', ' .
						$this->sqlLastInspectedOn() . ', ' .
						$this->sqlRetiredBy() . ', ' .
						$this->sqlRetiredOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlReturnedBy() . ', ' .
						$this->sqlReturnedOn() . ', ' .
						$this->sqlServiceLifeMonths() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_id = ' . $this->sqlAssetId(). ',' ; } elseif( true == array_key_exists( 'AssetId', $this->getChangedColumns() ) ) { $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_condition_id = ' . $this->sqlAssetConditionId(). ',' ; } elseif( true == array_key_exists( 'AssetConditionId', $this->getChangedColumns() ) ) { $strSql .= ' asset_condition_id = ' . $this->sqlAssetConditionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_recovery_period_id = ' . $this->sqlTaxRecoveryPeriodId(). ',' ; } elseif( true == array_key_exists( 'TaxRecoveryPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' tax_recovery_period_id = ' . $this->sqlTaxRecoveryPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' depreciation_category_id = ' . $this->sqlDepreciationCategoryId(). ',' ; } elseif( true == array_key_exists( 'DepreciationCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' depreciation_category_id = ' . $this->sqlDepreciationCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_number = ' . $this->sqlAssetNumber(). ',' ; } elseif( true == array_key_exists( 'AssetNumber', $this->getChangedColumns() ) ) { $strSql .= ' asset_number = ' . $this->sqlAssetNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' warranty_end_date = ' . $this->sqlWarrantyEndDate(). ',' ; } elseif( true == array_key_exists( 'WarrantyEndDate', $this->getChangedColumns() ) ) { $strSql .= ' warranty_end_date = ' . $this->sqlWarrantyEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' make = ' . $this->sqlMake(). ',' ; } elseif( true == array_key_exists( 'Make', $this->getChangedColumns() ) ) { $strSql .= ' make = ' . $this->sqlMake() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' model = ' . $this->sqlModel(). ',' ; } elseif( true == array_key_exists( 'Model', $this->getChangedColumns() ) ) { $strSql .= ' model = ' . $this->sqlModel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color = ' . $this->sqlColor(). ',' ; } elseif( true == array_key_exists( 'Color', $this->getChangedColumns() ) ) { $strSql .= ' color = ' . $this->sqlColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' salvage_value = ' . $this->sqlSalvageValue(). ',' ; } elseif( true == array_key_exists( 'SalvageValue', $this->getChangedColumns() ) ) { $strSql .= ' salvage_value = ' . $this->sqlSalvageValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' useful_life_months = ' . $this->sqlUsefulLifeMonths(). ',' ; } elseif( true == array_key_exists( 'UsefulLifeMonths', $this->getChangedColumns() ) ) { $strSql .= ' useful_life_months = ' . $this->sqlUsefulLifeMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' depreciation_start_date = ' . $this->sqlDepreciationStartDate(). ',' ; } elseif( true == array_key_exists( 'DepreciationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' depreciation_start_date = ' . $this->sqlDepreciationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_section_179 = ' . $this->sqlIsSection179(). ',' ; } elseif( true == array_key_exists( 'IsSection179', $this->getChangedColumns() ) ) { $strSql .= ' is_section_179 = ' . $this->sqlIsSection179() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_cost = ' . $this->sqlReplacementCost(). ',' ; } elseif( true == array_key_exists( 'ReplacementCost', $this->getChangedColumns() ) ) { $strSql .= ' replacement_cost = ' . $this->sqlReplacementCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proceeds_from_sale = ' . $this->sqlProceedsFromSale(). ',' ; } elseif( true == array_key_exists( 'ProceedsFromSale', $this->getChangedColumns() ) ) { $strSql .= ' proceeds_from_sale = ' . $this->sqlProceedsFromSale() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_by = ' . $this->sqlPurchasedBy(). ',' ; } elseif( true == array_key_exists( 'PurchasedBy', $this->getChangedColumns() ) ) { $strSql .= ' purchased_by = ' . $this->sqlPurchasedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_on = ' . $this->sqlPurchasedOn(). ',' ; } elseif( true == array_key_exists( 'PurchasedOn', $this->getChangedColumns() ) ) { $strSql .= ' purchased_on = ' . $this->sqlPurchasedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' placed_in_service_by = ' . $this->sqlPlacedInServiceBy(). ',' ; } elseif( true == array_key_exists( 'PlacedInServiceBy', $this->getChangedColumns() ) ) { $strSql .= ' placed_in_service_by = ' . $this->sqlPlacedInServiceBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' placed_in_service_on = ' . $this->sqlPlacedInServiceOn(). ',' ; } elseif( true == array_key_exists( 'PlacedInServiceOn', $this->getChangedColumns() ) ) { $strSql .= ' placed_in_service_on = ' . $this->sqlPlacedInServiceOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_inspected_by = ' . $this->sqlLastInspectedBy(). ',' ; } elseif( true == array_key_exists( 'LastInspectedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_inspected_by = ' . $this->sqlLastInspectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_inspected_on = ' . $this->sqlLastInspectedOn(). ',' ; } elseif( true == array_key_exists( 'LastInspectedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_inspected_on = ' . $this->sqlLastInspectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retired_by = ' . $this->sqlRetiredBy(). ',' ; } elseif( true == array_key_exists( 'RetiredBy', $this->getChangedColumns() ) ) { $strSql .= ' retired_by = ' . $this->sqlRetiredBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retired_on = ' . $this->sqlRetiredOn(). ',' ; } elseif( true == array_key_exists( 'RetiredOn', $this->getChangedColumns() ) ) { $strSql .= ' retired_on = ' . $this->sqlRetiredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_by = ' . $this->sqlReturnedBy(). ',' ; } elseif( true == array_key_exists( 'ReturnedBy', $this->getChangedColumns() ) ) { $strSql .= ' returned_by = ' . $this->sqlReturnedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_life_months = ' . $this->sqlServiceLifeMonths(). ',' ; } elseif( true == array_key_exists( 'ServiceLifeMonths', $this->getChangedColumns() ) ) { $strSql .= ' service_life_months = ' . $this->sqlServiceLifeMonths() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'asset_id' => $this->getAssetId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'maintenance_location_id' => $this->getMaintenanceLocationId(),
			'asset_condition_id' => $this->getAssetConditionId(),
			'tax_recovery_period_id' => $this->getTaxRecoveryPeriodId(),
			'depreciation_category_id' => $this->getDepreciationCategoryId(),
			'asset_number' => $this->getAssetNumber(),
			'warranty_end_date' => $this->getWarrantyEndDate(),
			'make' => $this->getMake(),
			'model' => $this->getModel(),
			'color' => $this->getColor(),
			'salvage_value' => $this->getSalvageValue(),
			'useful_life_months' => $this->getUsefulLifeMonths(),
			'depreciation_start_date' => $this->getDepreciationStartDate(),
			'is_section_179' => $this->getIsSection179(),
			'replacement_cost' => $this->getReplacementCost(),
			'proceeds_from_sale' => $this->getProceedsFromSale(),
			'notes' => $this->getNotes(),
			'purchased_by' => $this->getPurchasedBy(),
			'purchased_on' => $this->getPurchasedOn(),
			'placed_in_service_by' => $this->getPlacedInServiceBy(),
			'placed_in_service_on' => $this->getPlacedInServiceOn(),
			'last_inspected_by' => $this->getLastInspectedBy(),
			'last_inspected_on' => $this->getLastInspectedOn(),
			'retired_by' => $this->getRetiredBy(),
			'retired_on' => $this->getRetiredOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'returned_by' => $this->getReturnedBy(),
			'returned_on' => $this->getReturnedOn(),
			'service_life_months' => $this->getServiceLifeMonths()
		);
	}

}
?>