<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApCodeCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApCodeCategories extends CEosPluralBase {

	/**
	 * @return CApCodeCategory[]
	 */
	public static function fetchApCodeCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApCodeCategory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApCodeCategory
	 */
	public static function fetchApCodeCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApCodeCategory::class, $objDatabase );
	}

	public static function fetchApCodeCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_code_categories', $objDatabase );
	}

	public static function fetchApCodeCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategory( sprintf( 'SELECT * FROM ap_code_categories WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByDefaultApCodeCategoryIdByCid( $intDefaultApCodeCategoryId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE default_ap_code_category_id = %d AND cid = %d', $intDefaultApCodeCategoryId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByApCodeTypeIdByCid( $intApCodeTypeId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE ap_code_type_id = %d AND cid = %d', $intApCodeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByDebitGlAccountIdByCid( $intDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE debit_gl_account_id = %d AND cid = %d', $intDebitGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByCreditGlAccountIdByCid( $intCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE credit_gl_account_id = %d AND cid = %d', $intCreditGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByConsumptionGlAccountIdByCid( $intConsumptionGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE consumption_gl_account_id = %d AND cid = %d', $intConsumptionGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApCodeCategoriesByVendorProductCategoryIdByCid( $intVendorProductCategoryId, $intCid, $objDatabase ) {
		return self::fetchApCodeCategories( sprintf( 'SELECT * FROM ap_code_categories WHERE vendor_product_category_id = %d AND cid = %d', $intVendorProductCategoryId, $intCid ), $objDatabase );
	}

}
?>