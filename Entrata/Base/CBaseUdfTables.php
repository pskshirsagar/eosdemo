<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUdfTables
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUdfTables extends CEosPluralBase {

	/**
	 * @return CUdfTable[]
	 */
	public static function fetchUdfTables( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUdfTable::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUdfTable
	 */
	public static function fetchUdfTable( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUdfTable::class, $objDatabase );
	}

	public static function fetchUdfTableCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'udf_tables', $objDatabase );
	}

	public static function fetchUdfTableByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUdfTable( sprintf( 'SELECT * FROM udf_tables WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUdfTablesByCid( $intCid, $objDatabase ) {
		return self::fetchUdfTables( sprintf( 'SELECT * FROM udf_tables WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>