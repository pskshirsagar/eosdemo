<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyTaxCreditSetting extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.subsidy_tax_credit_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intRentAssistanceArCodeId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIncludeFosterAdultInHouseholdSize;
	protected $m_boolIncludeFosterChildInHouseholdSize;
	protected $m_boolIncludeFosterAdultIncome;
	protected $m_boolIncludeFosterChildIncome;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIncludeFosterAdultInHouseholdSize = false;
		$this->m_boolIncludeFosterChildInHouseholdSize = false;
		$this->m_boolIncludeFosterAdultIncome = false;
		$this->m_boolIncludeFosterChildIncome = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['rent_assistance_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRentAssistanceArCodeId', trim( $arrValues['rent_assistance_ar_code_id'] ) ); elseif( isset( $arrValues['rent_assistance_ar_code_id'] ) ) $this->setRentAssistanceArCodeId( $arrValues['rent_assistance_ar_code_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['include_foster_adult_in_household_size'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFosterAdultInHouseholdSize', trim( stripcslashes( $arrValues['include_foster_adult_in_household_size'] ) ) ); elseif( isset( $arrValues['include_foster_adult_in_household_size'] ) ) $this->setIncludeFosterAdultInHouseholdSize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_foster_adult_in_household_size'] ) : $arrValues['include_foster_adult_in_household_size'] );
		if( isset( $arrValues['include_foster_child_in_household_size'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFosterChildInHouseholdSize', trim( stripcslashes( $arrValues['include_foster_child_in_household_size'] ) ) ); elseif( isset( $arrValues['include_foster_child_in_household_size'] ) ) $this->setIncludeFosterChildInHouseholdSize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_foster_child_in_household_size'] ) : $arrValues['include_foster_child_in_household_size'] );
		if( isset( $arrValues['include_foster_adult_income'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFosterAdultIncome', trim( stripcslashes( $arrValues['include_foster_adult_income'] ) ) ); elseif( isset( $arrValues['include_foster_adult_income'] ) ) $this->setIncludeFosterAdultIncome( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_foster_adult_income'] ) : $arrValues['include_foster_adult_income'] );
		if( isset( $arrValues['include_foster_child_income'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFosterChildIncome', trim( stripcslashes( $arrValues['include_foster_child_income'] ) ) ); elseif( isset( $arrValues['include_foster_child_income'] ) ) $this->setIncludeFosterChildIncome( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_foster_child_income'] ) : $arrValues['include_foster_child_income'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRentAssistanceArCodeId( $intRentAssistanceArCodeId ) {
		$this->set( 'm_intRentAssistanceArCodeId', CStrings::strToIntDef( $intRentAssistanceArCodeId, NULL, false ) );
	}

	public function getRentAssistanceArCodeId() {
		return $this->m_intRentAssistanceArCodeId;
	}

	public function sqlRentAssistanceArCodeId() {
		return ( true == isset( $this->m_intRentAssistanceArCodeId ) ) ? ( string ) $this->m_intRentAssistanceArCodeId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIncludeFosterAdultInHouseholdSize( $boolIncludeFosterAdultInHouseholdSize ) {
		$this->set( 'm_boolIncludeFosterAdultInHouseholdSize', CStrings::strToBool( $boolIncludeFosterAdultInHouseholdSize ) );
	}

	public function getIncludeFosterAdultInHouseholdSize() {
		return $this->m_boolIncludeFosterAdultInHouseholdSize;
	}

	public function sqlIncludeFosterAdultInHouseholdSize() {
		return ( true == isset( $this->m_boolIncludeFosterAdultInHouseholdSize ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFosterAdultInHouseholdSize ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeFosterChildInHouseholdSize( $boolIncludeFosterChildInHouseholdSize ) {
		$this->set( 'm_boolIncludeFosterChildInHouseholdSize', CStrings::strToBool( $boolIncludeFosterChildInHouseholdSize ) );
	}

	public function getIncludeFosterChildInHouseholdSize() {
		return $this->m_boolIncludeFosterChildInHouseholdSize;
	}

	public function sqlIncludeFosterChildInHouseholdSize() {
		return ( true == isset( $this->m_boolIncludeFosterChildInHouseholdSize ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFosterChildInHouseholdSize ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeFosterAdultIncome( $boolIncludeFosterAdultIncome ) {
		$this->set( 'm_boolIncludeFosterAdultIncome', CStrings::strToBool( $boolIncludeFosterAdultIncome ) );
	}

	public function getIncludeFosterAdultIncome() {
		return $this->m_boolIncludeFosterAdultIncome;
	}

	public function sqlIncludeFosterAdultIncome() {
		return ( true == isset( $this->m_boolIncludeFosterAdultIncome ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFosterAdultIncome ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeFosterChildIncome( $boolIncludeFosterChildIncome ) {
		$this->set( 'm_boolIncludeFosterChildIncome', CStrings::strToBool( $boolIncludeFosterChildIncome ) );
	}

	public function getIncludeFosterChildIncome() {
		return $this->m_boolIncludeFosterChildIncome;
	}

	public function sqlIncludeFosterChildIncome() {
		return ( true == isset( $this->m_boolIncludeFosterChildIncome ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFosterChildIncome ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, rent_assistance_ar_code_id, updated_by, updated_on, created_by, created_on, include_foster_adult_in_household_size, include_foster_child_in_household_size, include_foster_adult_income, include_foster_child_income, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlRentAssistanceArCodeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIncludeFosterAdultInHouseholdSize() . ', ' .
						$this->sqlIncludeFosterChildInHouseholdSize() . ', ' .
						$this->sqlIncludeFosterAdultIncome() . ', ' .
						$this->sqlIncludeFosterChildIncome() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_assistance_ar_code_id = ' . $this->sqlRentAssistanceArCodeId(). ',' ; } elseif( true == array_key_exists( 'RentAssistanceArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' rent_assistance_ar_code_id = ' . $this->sqlRentAssistanceArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_foster_adult_in_household_size = ' . $this->sqlIncludeFosterAdultInHouseholdSize(). ',' ; } elseif( true == array_key_exists( 'IncludeFosterAdultInHouseholdSize', $this->getChangedColumns() ) ) { $strSql .= ' include_foster_adult_in_household_size = ' . $this->sqlIncludeFosterAdultInHouseholdSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_foster_child_in_household_size = ' . $this->sqlIncludeFosterChildInHouseholdSize(). ',' ; } elseif( true == array_key_exists( 'IncludeFosterChildInHouseholdSize', $this->getChangedColumns() ) ) { $strSql .= ' include_foster_child_in_household_size = ' . $this->sqlIncludeFosterChildInHouseholdSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_foster_adult_income = ' . $this->sqlIncludeFosterAdultIncome(). ',' ; } elseif( true == array_key_exists( 'IncludeFosterAdultIncome', $this->getChangedColumns() ) ) { $strSql .= ' include_foster_adult_income = ' . $this->sqlIncludeFosterAdultIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_foster_child_income = ' . $this->sqlIncludeFosterChildIncome(). ',' ; } elseif( true == array_key_exists( 'IncludeFosterChildIncome', $this->getChangedColumns() ) ) { $strSql .= ' include_foster_child_income = ' . $this->sqlIncludeFosterChildIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'rent_assistance_ar_code_id' => $this->getRentAssistanceArCodeId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'include_foster_adult_in_household_size' => $this->getIncludeFosterAdultInHouseholdSize(),
			'include_foster_child_in_household_size' => $this->getIncludeFosterChildInHouseholdSize(),
			'include_foster_adult_income' => $this->getIncludeFosterAdultIncome(),
			'include_foster_child_income' => $this->getIncludeFosterChildIncome(),
			'details' => $this->getDetails()
		);
	}

}
?>