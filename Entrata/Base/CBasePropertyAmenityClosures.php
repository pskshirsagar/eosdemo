<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAmenityClosures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAmenityClosures extends CEosPluralBase {

	/**
	 * @return CPropertyAmenityClosure[]
	 */
	public static function fetchPropertyAmenityClosures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyAmenityClosure', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAmenityClosure
	 */
	public static function fetchPropertyAmenityClosure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyAmenityClosure', $objDatabase );
	}

	public static function fetchPropertyAmenityClosureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_amenity_closures', $objDatabase );
	}

	public static function fetchPropertyAmenityClosureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityClosure( sprintf( 'SELECT * FROM property_amenity_closures WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityClosuresByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityClosures( sprintf( 'SELECT * FROM property_amenity_closures WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityClosuresByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityClosures( sprintf( 'SELECT * FROM property_amenity_closures WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityClosuresByPropertyAmenityIdByCid( $intPropertyAmenityId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityClosures( sprintf( 'SELECT * FROM property_amenity_closures WHERE property_amenity_id = %d AND cid = %d', ( int ) $intPropertyAmenityId, ( int ) $intCid ), $objDatabase );
	}

}
?>