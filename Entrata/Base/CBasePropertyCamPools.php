<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCamPools
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCamPools extends CEosPluralBase {

	/**
	 * @return CPropertyCamPool[]
	 */
	public static function fetchPropertyCamPools( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCamPool', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCamPool
	 */
	public static function fetchPropertyCamPool( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCamPool', $objDatabase );
	}

	public static function fetchPropertyCamPoolCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_cam_pools', $objDatabase );
	}

	public static function fetchPropertyCamPoolByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPool( sprintf( 'SELECT * FROM property_cam_pools WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPoolsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCamPools( sprintf( 'SELECT * FROM property_cam_pools WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPoolsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPools( sprintf( 'SELECT * FROM property_cam_pools WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamPoolsByCamPoolTypeIdByCid( $intCamPoolTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamPools( sprintf( 'SELECT * FROM property_cam_pools WHERE cam_pool_type_id = %d AND cid = %d', ( int ) $intCamPoolTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>