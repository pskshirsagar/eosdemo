<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationInterests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationInterests extends CEosPluralBase {

	/**
	 * @return CApplicationInterest[]
	 */
	public static function fetchApplicationInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationInterest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationInterest
	 */
	public static function fetchApplicationInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationInterest', $objDatabase );
	}

	public static function fetchApplicationInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_interests', $objDatabase );
	}

	public static function fetchApplicationInterestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterest( sprintf( 'SELECT * FROM application_interests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByAddOnIdByCid( $intAddOnId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE add_on_id = %d AND cid = %d', ( int ) $intAddOnId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationInterestsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationInterests( sprintf( 'SELECT * FROM application_interests WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>