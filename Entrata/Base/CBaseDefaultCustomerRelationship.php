<?php

class CBaseDefaultCustomerRelationship extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_customer_relationships';

	protected $m_intId;
	protected $m_intDefaultCustomerRelationshipGroupId;
	protected $m_intCustomerTypeId;
	protected $m_strName;
	protected $m_strMarketingName;
	protected $m_boolIsSystem;
	protected $m_boolShowInternally;
	protected $m_boolShowExternally;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSystem = false;
		$this->m_boolShowInternally = true;
		$this->m_boolShowExternally = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_customer_relationship_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCustomerRelationshipGroupId', trim( $arrValues['default_customer_relationship_group_id'] ) ); elseif( isset( $arrValues['default_customer_relationship_group_id'] ) ) $this->setDefaultCustomerRelationshipGroupId( $arrValues['default_customer_relationship_group_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['marketing_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMarketingName', trim( stripcslashes( $arrValues['marketing_name'] ) ) ); elseif( isset( $arrValues['marketing_name'] ) ) $this->setMarketingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_name'] ) : $arrValues['marketing_name'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['show_internally'] ) && $boolDirectSet ) $this->set( 'm_boolShowInternally', trim( stripcslashes( $arrValues['show_internally'] ) ) ); elseif( isset( $arrValues['show_internally'] ) ) $this->setShowInternally( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_internally'] ) : $arrValues['show_internally'] );
		if( isset( $arrValues['show_externally'] ) && $boolDirectSet ) $this->set( 'm_boolShowExternally', trim( stripcslashes( $arrValues['show_externally'] ) ) ); elseif( isset( $arrValues['show_externally'] ) ) $this->setShowExternally( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_externally'] ) : $arrValues['show_externally'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultCustomerRelationshipGroupId( $intDefaultCustomerRelationshipGroupId ) {
		$this->set( 'm_intDefaultCustomerRelationshipGroupId', CStrings::strToIntDef( $intDefaultCustomerRelationshipGroupId, NULL, false ) );
	}

	public function getDefaultCustomerRelationshipGroupId() {
		return $this->m_intDefaultCustomerRelationshipGroupId;
	}

	public function sqlDefaultCustomerRelationshipGroupId() {
		return ( true == isset( $this->m_intDefaultCustomerRelationshipGroupId ) ) ? ( string ) $this->m_intDefaultCustomerRelationshipGroupId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setMarketingName( $strMarketingName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMarketingName', CStrings::strTrimDef( $strMarketingName, 240, NULL, true ), $strLocaleCode );
	}

	public function getMarketingName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMarketingName', $strLocaleCode );
	}

	public function sqlMarketingName() {
		return ( true == isset( $this->m_strMarketingName ) ) ? '\'' . addslashes( $this->m_strMarketingName ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInternally( $boolShowInternally ) {
		$this->set( 'm_boolShowInternally', CStrings::strToBool( $boolShowInternally ) );
	}

	public function getShowInternally() {
		return $this->m_boolShowInternally;
	}

	public function sqlShowInternally() {
		return ( true == isset( $this->m_boolShowInternally ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInternally ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowExternally( $boolShowExternally ) {
		$this->set( 'm_boolShowExternally', CStrings::strToBool( $boolShowExternally ) );
	}

	public function getShowExternally() {
		return $this->m_boolShowExternally;
	}

	public function sqlShowExternally() {
		return ( true == isset( $this->m_boolShowExternally ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowExternally ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_customer_relationship_group_id' => $this->getDefaultCustomerRelationshipGroupId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'name' => $this->getName(),
			'marketing_name' => $this->getMarketingName(),
			'is_system' => $this->getIsSystem(),
			'show_internally' => $this->getShowInternally(),
			'show_externally' => $this->getShowExternally(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>