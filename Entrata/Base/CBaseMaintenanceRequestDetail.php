<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestDetail extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_request_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestId;
	protected $m_intMainPhoneNumberTypeId;
	protected $m_intAltPhoneNumberTypeId;
	protected $m_strProblemDescription;
	protected $m_strAdditionalInfo;
	protected $m_strLocationSpecifics;
	protected $m_intPermissionToEnter;
	protected $m_strRequestedDatetime;
	protected $m_strScheduledStartDatetime;
	protected $m_strScheduledEndDatetime;
	protected $m_strActualStartDatetime;
	protected $m_strCompletedDatetime;
	protected $m_boolIgnoreResponseTimes;
	protected $m_strIgnoreResponseReason;
	protected $m_strCustomerNameFirst;
	protected $m_strCustomerNameFirstEncrypted;
	protected $m_strCustomerNameLast;
	protected $m_strCustomerNameLastEncrypted;
	protected $m_strUnitNumber;
	protected $m_strMainPhoneNumber;
	protected $m_strMainPhoneNumberEncrypted;
	protected $m_strAltPhoneNumber;
	protected $m_strAltPhoneNumberEncrypted;
	protected $m_strEmailAddress;
	protected $m_strRequiredResponseDatetime;
	protected $m_intResponseDuration;
	protected $m_strSmsConfirmedOn;
	protected $m_intIsPet;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMaintenanceExceptionId;
	protected $m_boolIsRequestedAm;
	protected $m_strRequestedDate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIgnoreResponseTimes = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['main_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMainPhoneNumberTypeId', trim( $arrValues['main_phone_number_type_id'] ) ); elseif( isset( $arrValues['main_phone_number_type_id'] ) ) $this->setMainPhoneNumberTypeId( $arrValues['main_phone_number_type_id'] );
		if( isset( $arrValues['alt_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAltPhoneNumberTypeId', trim( $arrValues['alt_phone_number_type_id'] ) ); elseif( isset( $arrValues['alt_phone_number_type_id'] ) ) $this->setAltPhoneNumberTypeId( $arrValues['alt_phone_number_type_id'] );
		if( isset( $arrValues['problem_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strProblemDescription', trim( stripcslashes( $arrValues['problem_description'] ) ) ); elseif( isset( $arrValues['problem_description'] ) ) $this->setProblemDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['problem_description'] ) : $arrValues['problem_description'] );
		if( isset( $arrValues['additional_info'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAdditionalInfo', trim( stripcslashes( $arrValues['additional_info'] ) ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_info'] ) : $arrValues['additional_info'] );
		if( isset( $arrValues['location_specifics'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLocationSpecifics', trim( stripcslashes( $arrValues['location_specifics'] ) ) ); elseif( isset( $arrValues['location_specifics'] ) ) $this->setLocationSpecifics( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['location_specifics'] ) : $arrValues['location_specifics'] );
		if( isset( $arrValues['permission_to_enter'] ) && $boolDirectSet ) $this->set( 'm_intPermissionToEnter', trim( $arrValues['permission_to_enter'] ) ); elseif( isset( $arrValues['permission_to_enter'] ) ) $this->setPermissionToEnter( $arrValues['permission_to_enter'] );
		if( isset( $arrValues['scheduled_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartDatetime', trim( $arrValues['scheduled_start_datetime'] ) ); elseif( isset( $arrValues['scheduled_start_datetime'] ) ) $this->setScheduledStartDatetime( $arrValues['scheduled_start_datetime'] );
		if( isset( $arrValues['scheduled_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndDatetime', trim( $arrValues['scheduled_end_datetime'] ) ); elseif( isset( $arrValues['scheduled_end_datetime'] ) ) $this->setScheduledEndDatetime( $arrValues['scheduled_end_datetime'] );
		if( isset( $arrValues['actual_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDatetime', trim( $arrValues['actual_start_datetime'] ) ); elseif( isset( $arrValues['actual_start_datetime'] ) ) $this->setActualStartDatetime( $arrValues['actual_start_datetime'] );
		if( isset( $arrValues['completed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCompletedDatetime', trim( $arrValues['completed_datetime'] ) ); elseif( isset( $arrValues['completed_datetime'] ) ) $this->setCompletedDatetime( $arrValues['completed_datetime'] );
		if( isset( $arrValues['ignore_response_times'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreResponseTimes', trim( stripcslashes( $arrValues['ignore_response_times'] ) ) ); elseif( isset( $arrValues['ignore_response_times'] ) ) $this->setIgnoreResponseTimes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_response_times'] ) : $arrValues['ignore_response_times'] );
		if( isset( $arrValues['ignore_response_reason'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strIgnoreResponseReason', trim( stripcslashes( $arrValues['ignore_response_reason'] ) ) ); elseif( isset( $arrValues['ignore_response_reason'] ) ) $this->setIgnoreResponseReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_response_reason'] ) : $arrValues['ignore_response_reason'] );
		if( isset( $arrValues['customer_name_first'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomerNameFirst', trim( stripcslashes( $arrValues['customer_name_first'] ) ) ); elseif( isset( $arrValues['customer_name_first'] ) ) $this->setCustomerNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_first'] ) : $arrValues['customer_name_first'] );
		if( isset( $arrValues['customer_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameFirstEncrypted', trim( stripcslashes( $arrValues['customer_name_first_encrypted'] ) ) ); elseif( isset( $arrValues['customer_name_first_encrypted'] ) ) $this->setCustomerNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_first_encrypted'] ) : $arrValues['customer_name_first_encrypted'] );
		if( isset( $arrValues['customer_name_last'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomerNameLast', trim( stripcslashes( $arrValues['customer_name_last'] ) ) ); elseif( isset( $arrValues['customer_name_last'] ) ) $this->setCustomerNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_last'] ) : $arrValues['customer_name_last'] );
		if( isset( $arrValues['customer_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameLastEncrypted', trim( stripcslashes( $arrValues['customer_name_last_encrypted'] ) ) ); elseif( isset( $arrValues['customer_name_last_encrypted'] ) ) $this->setCustomerNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_last_encrypted'] ) : $arrValues['customer_name_last_encrypted'] );
		if( isset( $arrValues['unit_number'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['main_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strMainPhoneNumber', trim( stripcslashes( $arrValues['main_phone_number'] ) ) ); elseif( isset( $arrValues['main_phone_number'] ) ) $this->setMainPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['main_phone_number'] ) : $arrValues['main_phone_number'] );
		if( isset( $arrValues['main_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strMainPhoneNumberEncrypted', trim( stripcslashes( $arrValues['main_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['main_phone_number_encrypted'] ) ) $this->setMainPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['main_phone_number_encrypted'] ) : $arrValues['main_phone_number_encrypted'] );
		if( isset( $arrValues['alt_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAltPhoneNumber', trim( stripcslashes( $arrValues['alt_phone_number'] ) ) ); elseif( isset( $arrValues['alt_phone_number'] ) ) $this->setAltPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alt_phone_number'] ) : $arrValues['alt_phone_number'] );
		if( isset( $arrValues['alt_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAltPhoneNumberEncrypted', trim( stripcslashes( $arrValues['alt_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['alt_phone_number_encrypted'] ) ) $this->setAltPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alt_phone_number_encrypted'] ) : $arrValues['alt_phone_number_encrypted'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['required_response_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequiredResponseDatetime', trim( $arrValues['required_response_datetime'] ) ); elseif( isset( $arrValues['required_response_datetime'] ) ) $this->setRequiredResponseDatetime( $arrValues['required_response_datetime'] );
		if( isset( $arrValues['response_duration'] ) && $boolDirectSet ) $this->set( 'm_intResponseDuration', trim( $arrValues['response_duration'] ) ); elseif( isset( $arrValues['response_duration'] ) ) $this->setResponseDuration( $arrValues['response_duration'] );
		if( isset( $arrValues['sms_confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsConfirmedOn', trim( $arrValues['sms_confirmed_on'] ) ); elseif( isset( $arrValues['sms_confirmed_on'] ) ) $this->setSmsConfirmedOn( $arrValues['sms_confirmed_on'] );
		if( isset( $arrValues['is_pet'] ) && $boolDirectSet ) $this->set( 'm_intIsPet', trim( $arrValues['is_pet'] ) ); elseif( isset( $arrValues['is_pet'] ) ) $this->setIsPet( $arrValues['is_pet'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['maintenance_exception_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceExceptionId', trim( $arrValues['maintenance_exception_id'] ) ); elseif( isset( $arrValues['maintenance_exception_id'] ) ) $this->setMaintenanceExceptionId( $arrValues['maintenance_exception_id'] );
		if( isset( $arrValues['is_requested_am'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequestedAm', trim( stripcslashes( $arrValues['is_requested_am'] ) ) ); elseif( isset( $arrValues['is_requested_am'] ) ) $this->setIsRequestedAm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_requested_am'] ) : $arrValues['is_requested_am'] );
		if( isset( $arrValues['requested_date'] ) && $boolDirectSet ) $this->set( 'm_strRequestedDate', trim( $arrValues['requested_date'] ) ); elseif( isset( $arrValues['requested_date'] ) ) $this->setRequestedDate( $arrValues['requested_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setMainPhoneNumberTypeId( $intMainPhoneNumberTypeId ) {
		$this->set( 'm_intMainPhoneNumberTypeId', CStrings::strToIntDef( $intMainPhoneNumberTypeId, NULL, false ) );
	}

	public function getMainPhoneNumberTypeId() {
		return $this->m_intMainPhoneNumberTypeId;
	}

	public function sqlMainPhoneNumberTypeId() {
		return ( true == isset( $this->m_intMainPhoneNumberTypeId ) ) ? ( string ) $this->m_intMainPhoneNumberTypeId : 'NULL';
	}

	public function setAltPhoneNumberTypeId( $intAltPhoneNumberTypeId ) {
		$this->set( 'm_intAltPhoneNumberTypeId', CStrings::strToIntDef( $intAltPhoneNumberTypeId, NULL, false ) );
	}

	public function getAltPhoneNumberTypeId() {
		return $this->m_intAltPhoneNumberTypeId;
	}

	public function sqlAltPhoneNumberTypeId() {
		return ( true == isset( $this->m_intAltPhoneNumberTypeId ) ) ? ( string ) $this->m_intAltPhoneNumberTypeId : 'NULL';
	}

	public function setProblemDescription( $strProblemDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strProblemDescription', CStrings::strTrimDef( $strProblemDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getProblemDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strProblemDescription', $strLocaleCode );
	}

	public function sqlProblemDescription() {
		return ( true == isset( $this->m_strProblemDescription ) ) ? '\'' . addslashes( $this->m_strProblemDescription ) . '\'' : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ), $strLocaleCode );
	}

	public function getAdditionalInfo( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAdditionalInfo', $strLocaleCode );
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' : 'NULL';
	}

	public function setLocationSpecifics( $strLocationSpecifics, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLocationSpecifics', CStrings::strTrimDef( $strLocationSpecifics, -1, NULL, true ), $strLocaleCode );
	}

	public function getLocationSpecifics( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLocationSpecifics', $strLocaleCode );
	}

	public function sqlLocationSpecifics() {
		return ( true == isset( $this->m_strLocationSpecifics ) ) ? '\'' . addslashes( $this->m_strLocationSpecifics ) . '\'' : 'NULL';
	}

	public function setPermissionToEnter( $intPermissionToEnter ) {
		$this->set( 'm_intPermissionToEnter', CStrings::strToIntDef( $intPermissionToEnter, NULL, false ) );
	}

	public function getPermissionToEnter() {
		return $this->m_intPermissionToEnter;
	}

	public function sqlPermissionToEnter() {
		return ( true == isset( $this->m_intPermissionToEnter ) ) ? ( string ) $this->m_intPermissionToEnter : 'NULL';
	}

	public function setScheduledStartDatetime( $strScheduledStartDatetime ) {
		$this->set( 'm_strScheduledStartDatetime', CStrings::strTrimDef( $strScheduledStartDatetime, -1, NULL, true ) );
	}

	public function getScheduledStartDatetime() {
		return $this->m_strScheduledStartDatetime;
	}

	public function sqlScheduledStartDatetime() {
		return ( true == isset( $this->m_strScheduledStartDatetime ) ) ? '\'' . $this->m_strScheduledStartDatetime . '\'' : 'NULL';
	}

	public function setScheduledEndDatetime( $strScheduledEndDatetime ) {
		$this->set( 'm_strScheduledEndDatetime', CStrings::strTrimDef( $strScheduledEndDatetime, -1, NULL, true ) );
	}

	public function getScheduledEndDatetime() {
		return $this->m_strScheduledEndDatetime;
	}

	public function sqlScheduledEndDatetime() {
		return ( true == isset( $this->m_strScheduledEndDatetime ) ) ? '\'' . $this->m_strScheduledEndDatetime . '\'' : 'NULL';
	}

	public function setActualStartDatetime( $strActualStartDatetime ) {
		$this->set( 'm_strActualStartDatetime', CStrings::strTrimDef( $strActualStartDatetime, -1, NULL, true ) );
	}

	public function getActualStartDatetime() {
		return $this->m_strActualStartDatetime;
	}

	public function sqlActualStartDatetime() {
		return ( true == isset( $this->m_strActualStartDatetime ) ) ? '\'' . $this->m_strActualStartDatetime . '\'' : 'NOW()';
	}

	public function setCompletedDatetime( $strCompletedDatetime ) {
		$this->set( 'm_strCompletedDatetime', CStrings::strTrimDef( $strCompletedDatetime, -1, NULL, true ) );
	}

	public function getCompletedDatetime() {
		return $this->m_strCompletedDatetime;
	}

	public function sqlCompletedDatetime() {
		return ( true == isset( $this->m_strCompletedDatetime ) ) ? '\'' . $this->m_strCompletedDatetime . '\'' : 'NULL';
	}

	public function setIgnoreResponseTimes( $boolIgnoreResponseTimes ) {
		$this->set( 'm_boolIgnoreResponseTimes', CStrings::strToBool( $boolIgnoreResponseTimes ) );
	}

	public function getIgnoreResponseTimes() {
		return $this->m_boolIgnoreResponseTimes;
	}

	public function sqlIgnoreResponseTimes() {
		return ( true == isset( $this->m_boolIgnoreResponseTimes ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreResponseTimes ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIgnoreResponseReason( $strIgnoreResponseReason, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strIgnoreResponseReason', CStrings::strTrimDef( $strIgnoreResponseReason, 255, NULL, true ), $strLocaleCode );
	}

	public function getIgnoreResponseReason( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strIgnoreResponseReason', $strLocaleCode );
	}

	public function sqlIgnoreResponseReason() {
		return ( true == isset( $this->m_strIgnoreResponseReason ) ) ? '\'' . addslashes( $this->m_strIgnoreResponseReason ) . '\'' : 'NULL';
	}

	public function setCustomerNameFirst( $strCustomerNameFirst, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomerNameFirst', CStrings::strTrimDef( $strCustomerNameFirst, 50, NULL, true ), $strLocaleCode );
	}

	public function getCustomerNameFirst( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomerNameFirst', $strLocaleCode );
	}

	public function sqlCustomerNameFirst() {
		return ( true == isset( $this->m_strCustomerNameFirst ) ) ? '\'' . addslashes( $this->m_strCustomerNameFirst ) . '\'' : 'NULL';
	}

	public function setCustomerNameFirstEncrypted( $strCustomerNameFirstEncrypted ) {
		$this->set( 'm_strCustomerNameFirstEncrypted', CStrings::strTrimDef( $strCustomerNameFirstEncrypted, -1, NULL, true ) );
	}

	public function getCustomerNameFirstEncrypted() {
		return $this->m_strCustomerNameFirstEncrypted;
	}

	public function sqlCustomerNameFirstEncrypted() {
		return ( true == isset( $this->m_strCustomerNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setCustomerNameLast( $strCustomerNameLast, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomerNameLast', CStrings::strTrimDef( $strCustomerNameLast, 50, NULL, true ), $strLocaleCode );
	}

	public function getCustomerNameLast( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomerNameLast', $strLocaleCode );
	}

	public function sqlCustomerNameLast() {
		return ( true == isset( $this->m_strCustomerNameLast ) ) ? '\'' . addslashes( $this->m_strCustomerNameLast ) . '\'' : 'NULL';
	}

	public function setCustomerNameLastEncrypted( $strCustomerNameLastEncrypted ) {
		$this->set( 'm_strCustomerNameLastEncrypted', CStrings::strTrimDef( $strCustomerNameLastEncrypted, -1, NULL, true ) );
	}

	public function getCustomerNameLastEncrypted() {
		return $this->m_strCustomerNameLastEncrypted;
	}

	public function sqlCustomerNameLastEncrypted() {
		return ( true == isset( $this->m_strCustomerNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitNumber( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitNumber', $strLocaleCode );
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setMainPhoneNumber( $strMainPhoneNumber ) {
		$this->set( 'm_strMainPhoneNumber', CStrings::strTrimDef( $strMainPhoneNumber, 30, NULL, true ) );
	}

	public function getMainPhoneNumber() {
		return $this->m_strMainPhoneNumber;
	}

	public function sqlMainPhoneNumber() {
		return ( true == isset( $this->m_strMainPhoneNumber ) ) ? '\'' . addslashes( $this->m_strMainPhoneNumber ) . '\'' : 'NULL';
	}

	public function setMainPhoneNumberEncrypted( $strMainPhoneNumberEncrypted ) {
		$this->set( 'm_strMainPhoneNumberEncrypted', CStrings::strTrimDef( $strMainPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getMainPhoneNumberEncrypted() {
		return $this->m_strMainPhoneNumberEncrypted;
	}

	public function sqlMainPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strMainPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strMainPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setAltPhoneNumber( $strAltPhoneNumber ) {
		$this->set( 'm_strAltPhoneNumber', CStrings::strTrimDef( $strAltPhoneNumber, 30, NULL, true ) );
	}

	public function getAltPhoneNumber() {
		return $this->m_strAltPhoneNumber;
	}

	public function sqlAltPhoneNumber() {
		return ( true == isset( $this->m_strAltPhoneNumber ) ) ? '\'' . addslashes( $this->m_strAltPhoneNumber ) . '\'' : 'NULL';
	}

	public function setAltPhoneNumberEncrypted( $strAltPhoneNumberEncrypted ) {
		$this->set( 'm_strAltPhoneNumberEncrypted', CStrings::strTrimDef( $strAltPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getAltPhoneNumberEncrypted() {
		return $this->m_strAltPhoneNumberEncrypted;
	}

	public function sqlAltPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strAltPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strAltPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setRequiredResponseDatetime( $strRequiredResponseDatetime ) {
		$this->set( 'm_strRequiredResponseDatetime', CStrings::strTrimDef( $strRequiredResponseDatetime, -1, NULL, true ) );
	}

	public function getRequiredResponseDatetime() {
		return $this->m_strRequiredResponseDatetime;
	}

	public function sqlRequiredResponseDatetime() {
		return ( true == isset( $this->m_strRequiredResponseDatetime ) ) ? '\'' . $this->m_strRequiredResponseDatetime . '\'' : 'NULL';
	}

	public function setResponseDuration( $intResponseDuration ) {
		$this->set( 'm_intResponseDuration', CStrings::strToIntDef( $intResponseDuration, NULL, false ) );
	}

	public function getResponseDuration() {
		return $this->m_intResponseDuration;
	}

	public function sqlResponseDuration() {
		return ( true == isset( $this->m_intResponseDuration ) ) ? ( string ) $this->m_intResponseDuration : 'NULL';
	}

	public function setSmsConfirmedOn( $strSmsConfirmedOn ) {
		$this->set( 'm_strSmsConfirmedOn', CStrings::strTrimDef( $strSmsConfirmedOn, -1, NULL, true ) );
	}

	public function getSmsConfirmedOn() {
		return $this->m_strSmsConfirmedOn;
	}

	public function sqlSmsConfirmedOn() {
		return ( true == isset( $this->m_strSmsConfirmedOn ) ) ? '\'' . $this->m_strSmsConfirmedOn . '\'' : 'NULL';
	}

	public function setIsPet( $intIsPet ) {
		$this->set( 'm_intIsPet', CStrings::strToIntDef( $intIsPet, NULL, false ) );
	}

	public function getIsPet() {
		return $this->m_intIsPet;
	}

	public function sqlIsPet() {
		return ( true == isset( $this->m_intIsPet ) ) ? ( string ) $this->m_intIsPet : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setMaintenanceExceptionId( $intMaintenanceExceptionId ) {
		$this->set( 'm_intMaintenanceExceptionId', CStrings::strToIntDef( $intMaintenanceExceptionId, NULL, false ) );
	}

	public function getMaintenanceExceptionId() {
		return $this->m_intMaintenanceExceptionId;
	}

	public function sqlMaintenanceExceptionId() {
		return ( true == isset( $this->m_intMaintenanceExceptionId ) ) ? ( string ) $this->m_intMaintenanceExceptionId : 'NULL';
	}

	public function setIsRequestedAm( $boolIsRequestedAm ) {
		$this->set( 'm_boolIsRequestedAm', CStrings::strToBool( $boolIsRequestedAm ) );
	}

	public function getIsRequestedAm() {
		return $this->m_boolIsRequestedAm;
	}

	public function sqlIsRequestedAm() {
		return ( true == isset( $this->m_boolIsRequestedAm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequestedAm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequestedDate( $strRequestedDate ) {
		$this->set( 'm_strRequestedDate', CStrings::strTrimDef( $strRequestedDate, -1, NULL, true ) );
	}

	public function getRequestedDate() {
		return $this->m_strRequestedDate;
	}

	public function sqlRequestedDate() {
		return ( true == isset( $this->m_strRequestedDate ) ) ? '\'' . $this->m_strRequestedDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_id, main_phone_number_type_id, alt_phone_number_type_id, problem_description, additional_info, location_specifics, permission_to_enter, scheduled_start_datetime, scheduled_end_datetime, actual_start_datetime, completed_datetime, ignore_response_times, ignore_response_reason, customer_name_first, customer_name_first_encrypted, customer_name_last, customer_name_last_encrypted, unit_number, main_phone_number, main_phone_number_encrypted, alt_phone_number, alt_phone_number_encrypted, email_address, required_response_datetime, response_duration, sms_confirmed_on, is_pet, updated_by, updated_on, created_by, created_on, details, maintenance_exception_id, is_requested_am, requested_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlMainPhoneNumberTypeId() . ', ' .
						$this->sqlAltPhoneNumberTypeId() . ', ' .
						$this->sqlProblemDescription() . ', ' .
						$this->sqlAdditionalInfo() . ', ' .
						$this->sqlLocationSpecifics() . ', ' .
						$this->sqlPermissionToEnter() . ', ' .
						$this->sqlScheduledStartDatetime() . ', ' .
						$this->sqlScheduledEndDatetime() . ', ' .
						$this->sqlActualStartDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ', ' .
						$this->sqlIgnoreResponseTimes() . ', ' .
						$this->sqlIgnoreResponseReason() . ', ' .
						$this->sqlCustomerNameFirst() . ', ' .
						$this->sqlCustomerNameFirstEncrypted() . ', ' .
						$this->sqlCustomerNameLast() . ', ' .
						$this->sqlCustomerNameLastEncrypted() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlMainPhoneNumber() . ', ' .
						$this->sqlMainPhoneNumberEncrypted() . ', ' .
						$this->sqlAltPhoneNumber() . ', ' .
						$this->sqlAltPhoneNumberEncrypted() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlRequiredResponseDatetime() . ', ' .
						$this->sqlResponseDuration() . ', ' .
						$this->sqlSmsConfirmedOn() . ', ' .
						$this->sqlIsPet() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMaintenanceExceptionId() . ', ' .
						$this->sqlIsRequestedAm() . ', ' .
						$this->sqlRequestedDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_phone_number_type_id = ' . $this->sqlMainPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'MainPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' main_phone_number_type_id = ' . $this->sqlMainPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_phone_number_type_id = ' . $this->sqlAltPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'AltPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' alt_phone_number_type_id = ' . $this->sqlAltPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' problem_description = ' . $this->sqlProblemDescription(). ',' ; } elseif( true == array_key_exists( 'ProblemDescription', $this->getChangedColumns() ) ) { $strSql .= ' problem_description = ' . $this->sqlProblemDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo(). ',' ; } elseif( true == array_key_exists( 'AdditionalInfo', $this->getChangedColumns() ) ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location_specifics = ' . $this->sqlLocationSpecifics(). ',' ; } elseif( true == array_key_exists( 'LocationSpecifics', $this->getChangedColumns() ) ) { $strSql .= ' location_specifics = ' . $this->sqlLocationSpecifics() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permission_to_enter = ' . $this->sqlPermissionToEnter(). ',' ; } elseif( true == array_key_exists( 'PermissionToEnter', $this->getChangedColumns() ) ) { $strSql .= ' permission_to_enter = ' . $this->sqlPermissionToEnter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_start_datetime = ' . $this->sqlScheduledStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_start_datetime = ' . $this->sqlScheduledStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_datetime = ' . $this->sqlActualStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ActualStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_datetime = ' . $this->sqlActualStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime(). ',' ; } elseif( true == array_key_exists( 'CompletedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_response_times = ' . $this->sqlIgnoreResponseTimes(). ',' ; } elseif( true == array_key_exists( 'IgnoreResponseTimes', $this->getChangedColumns() ) ) { $strSql .= ' ignore_response_times = ' . $this->sqlIgnoreResponseTimes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_response_reason = ' . $this->sqlIgnoreResponseReason(). ',' ; } elseif( true == array_key_exists( 'IgnoreResponseReason', $this->getChangedColumns() ) ) { $strSql .= ' ignore_response_reason = ' . $this->sqlIgnoreResponseReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_first = ' . $this->sqlCustomerNameFirst(). ',' ; } elseif( true == array_key_exists( 'CustomerNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_first = ' . $this->sqlCustomerNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_first_encrypted = ' . $this->sqlCustomerNameFirstEncrypted(). ',' ; } elseif( true == array_key_exists( 'CustomerNameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_first_encrypted = ' . $this->sqlCustomerNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_last = ' . $this->sqlCustomerNameLast(). ',' ; } elseif( true == array_key_exists( 'CustomerNameLast', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_last = ' . $this->sqlCustomerNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_last_encrypted = ' . $this->sqlCustomerNameLastEncrypted(). ',' ; } elseif( true == array_key_exists( 'CustomerNameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_last_encrypted = ' . $this->sqlCustomerNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_phone_number = ' . $this->sqlMainPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'MainPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' main_phone_number = ' . $this->sqlMainPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_phone_number_encrypted = ' . $this->sqlMainPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'MainPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' main_phone_number_encrypted = ' . $this->sqlMainPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_phone_number = ' . $this->sqlAltPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'AltPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' alt_phone_number = ' . $this->sqlAltPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_phone_number_encrypted = ' . $this->sqlAltPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'AltPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' alt_phone_number_encrypted = ' . $this->sqlAltPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_response_datetime = ' . $this->sqlRequiredResponseDatetime(). ',' ; } elseif( true == array_key_exists( 'RequiredResponseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' required_response_datetime = ' . $this->sqlRequiredResponseDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_duration = ' . $this->sqlResponseDuration(). ',' ; } elseif( true == array_key_exists( 'ResponseDuration', $this->getChangedColumns() ) ) { $strSql .= ' response_duration = ' . $this->sqlResponseDuration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_confirmed_on = ' . $this->sqlSmsConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'SmsConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' sms_confirmed_on = ' . $this->sqlSmsConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pet = ' . $this->sqlIsPet(). ',' ; } elseif( true == array_key_exists( 'IsPet', $this->getChangedColumns() ) ) { $strSql .= ' is_pet = ' . $this->sqlIsPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_exception_id = ' . $this->sqlMaintenanceExceptionId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceExceptionId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_exception_id = ' . $this->sqlMaintenanceExceptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requested_am = ' . $this->sqlIsRequestedAm(). ',' ; } elseif( true == array_key_exists( 'IsRequestedAm', $this->getChangedColumns() ) ) { $strSql .= ' is_requested_am = ' . $this->sqlIsRequestedAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_date = ' . $this->sqlRequestedDate(). ',' ; } elseif( true == array_key_exists( 'RequestedDate', $this->getChangedColumns() ) ) { $strSql .= ' requested_date = ' . $this->sqlRequestedDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'main_phone_number_type_id' => $this->getMainPhoneNumberTypeId(),
			'alt_phone_number_type_id' => $this->getAltPhoneNumberTypeId(),
			'problem_description' => $this->getProblemDescription(),
			'additional_info' => $this->getAdditionalInfo(),
			'location_specifics' => $this->getLocationSpecifics(),
			'permission_to_enter' => $this->getPermissionToEnter(),
			'scheduled_start_datetime' => $this->getScheduledStartDatetime(),
			'scheduled_end_datetime' => $this->getScheduledEndDatetime(),
			'actual_start_datetime' => $this->getActualStartDatetime(),
			'completed_datetime' => $this->getCompletedDatetime(),
			'ignore_response_times' => $this->getIgnoreResponseTimes(),
			'ignore_response_reason' => $this->getIgnoreResponseReason(),
			'customer_name_first' => $this->getCustomerNameFirst(),
			'customer_name_first_encrypted' => $this->getCustomerNameFirstEncrypted(),
			'customer_name_last' => $this->getCustomerNameLast(),
			'customer_name_last_encrypted' => $this->getCustomerNameLastEncrypted(),
			'unit_number' => $this->getUnitNumber(),
			'main_phone_number' => $this->getMainPhoneNumber(),
			'main_phone_number_encrypted' => $this->getMainPhoneNumberEncrypted(),
			'alt_phone_number' => $this->getAltPhoneNumber(),
			'alt_phone_number_encrypted' => $this->getAltPhoneNumberEncrypted(),
			'email_address' => $this->getEmailAddress(),
			'required_response_datetime' => $this->getRequiredResponseDatetime(),
			'response_duration' => $this->getResponseDuration(),
			'sms_confirmed_on' => $this->getSmsConfirmedOn(),
			'is_pet' => $this->getIsPet(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'maintenance_exception_id' => $this->getMaintenanceExceptionId(),
			'is_requested_am' => $this->getIsRequestedAm(),
			'requested_date' => $this->getRequestedDate()
		);
	}

}
?>