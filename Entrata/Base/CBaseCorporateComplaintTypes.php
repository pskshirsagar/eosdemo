<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateComplaintTypes
 * Do not add any new functions to this class.
 */

class CBaseCorporateComplaintTypes extends CEosPluralBase {

	/**
	 * @return CCorporateComplaintType[]
	 */
	public static function fetchCorporateComplaintTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCorporateComplaintType::class, $objDatabase );
	}

	/**
	 * @return CCorporateComplaintType
	 */
	public static function fetchCorporateComplaintType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCorporateComplaintType::class, $objDatabase );
	}

	public static function fetchCorporateComplaintTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_complaint_types', $objDatabase );
	}

	public static function fetchCorporateComplaintTypeById( $intId, $objDatabase ) {
		return self::fetchCorporateComplaintType( sprintf( 'SELECT * FROM corporate_complaint_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>