<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeLeases extends CEosPluralBase {

    public static function fetchApPayeeLeases( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CApPayeeLease', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchApPayeeLease( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApPayeeLease', $objDatabase );
    }

    public static function fetchApPayeeLeaseCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ap_payee_leases', $objDatabase );
    }

    public static function fetchApPayeeLeaseByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLease( sprintf( 'SELECT * FROM ap_payee_leases WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByCid( $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE ap_payee_id = %d AND cid = %d', (int) $intApPayeeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByReimbursedPropertyIdByCid( $intReimbursedPropertyId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE reimbursed_property_id = %d AND cid = %d', (int) $intReimbursedPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByOwnerIdByCid( $intOwnerId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE owner_id = %d AND cid = %d', (int) $intOwnerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApPayeeLeasesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
        return self::fetchApPayeeLeases( sprintf( 'SELECT * FROM ap_payee_leases WHERE lease_id = %d AND cid = %d', (int) $intLeaseId, (int) $intCid ), $objDatabase );
    }

}
?>