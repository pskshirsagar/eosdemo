<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetDataSourceTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetDataSourceTypes extends CEosPluralBase {

	/**
	 * @return CBudgetDataSourceType[]
	 */
	public static function fetchBudgetDataSourceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetDataSourceType::class, $objDatabase );
	}

	/**
	 * @return CBudgetDataSourceType
	 */
	public static function fetchBudgetDataSourceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetDataSourceType::class, $objDatabase );
	}

	public static function fetchBudgetDataSourceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_data_source_types', $objDatabase );
	}

	public static function fetchBudgetDataSourceTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetDataSourceType( sprintf( 'SELECT * FROM budget_data_source_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>