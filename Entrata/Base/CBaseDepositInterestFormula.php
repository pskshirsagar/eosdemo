<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDepositInterestFormula extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_strName;
    protected $m_intInterestStartDays;
    protected $m_intInterestQualificationDays;
    protected $m_intInterestGraceDays;
    protected $m_intInterestPeriodForfeitDays;
    protected $m_boolDenyInterestForSkips;
    protected $m_boolGenerateRefundCheck;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intInterestStartDays = '0';
        $this->m_intInterestQualificationDays = '0';
        $this->m_intInterestGraceDays = '0';
        $this->m_intInterestPeriodForfeitDays = '0';
        $this->m_boolDenyInterestForSkips = false;
        $this->m_boolGenerateRefundCheck = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['interest_start_days'] ) && $boolDirectSet ) $this->m_intInterestStartDays = trim( $arrValues['interest_start_days'] ); else if( isset( $arrValues['interest_start_days'] ) ) $this->setInterestStartDays( $arrValues['interest_start_days'] );
        if( isset( $arrValues['interest_qualification_days'] ) && $boolDirectSet ) $this->m_intInterestQualificationDays = trim( $arrValues['interest_qualification_days'] ); else if( isset( $arrValues['interest_qualification_days'] ) ) $this->setInterestQualificationDays( $arrValues['interest_qualification_days'] );
        if( isset( $arrValues['interest_grace_days'] ) && $boolDirectSet ) $this->m_intInterestGraceDays = trim( $arrValues['interest_grace_days'] ); else if( isset( $arrValues['interest_grace_days'] ) ) $this->setInterestGraceDays( $arrValues['interest_grace_days'] );
        if( isset( $arrValues['interest_period_forfeit_days'] ) && $boolDirectSet ) $this->m_intInterestPeriodForfeitDays = trim( $arrValues['interest_period_forfeit_days'] ); else if( isset( $arrValues['interest_period_forfeit_days'] ) ) $this->setInterestPeriodForfeitDays( $arrValues['interest_period_forfeit_days'] );
        if( isset( $arrValues['deny_interest_for_skips'] ) && $boolDirectSet ) $this->m_boolDenyInterestForSkips = trim( stripcslashes( $arrValues['deny_interest_for_skips'] ) ); else if( isset( $arrValues['deny_interest_for_skips'] ) ) $this->setDenyInterestForSkips( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deny_interest_for_skips'] ) : $arrValues['deny_interest_for_skips'] );
        if( isset( $arrValues['generate_refund_check'] ) && $boolDirectSet ) $this->m_boolGenerateRefundCheck = trim( stripcslashes( $arrValues['generate_refund_check'] ) ); else if( isset( $arrValues['generate_refund_check'] ) ) $this->setGenerateRefundCheck( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['generate_refund_check'] ) : $arrValues['generate_refund_check'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setInterestStartDays( $intInterestStartDays ) {
        $this->m_intInterestStartDays = CStrings::strToIntDef( $intInterestStartDays, NULL, false );
    }

    public function getInterestStartDays() {
        return $this->m_intInterestStartDays;
    }

    public function sqlInterestStartDays() {
        return ( true == isset( $this->m_intInterestStartDays ) ) ? (string) $this->m_intInterestStartDays : '0';
    }

    public function setInterestQualificationDays( $intInterestQualificationDays ) {
        $this->m_intInterestQualificationDays = CStrings::strToIntDef( $intInterestQualificationDays, NULL, false );
    }

    public function getInterestQualificationDays() {
        return $this->m_intInterestQualificationDays;
    }

    public function sqlInterestQualificationDays() {
        return ( true == isset( $this->m_intInterestQualificationDays ) ) ? (string) $this->m_intInterestQualificationDays : '0';
    }

    public function setInterestGraceDays( $intInterestGraceDays ) {
        $this->m_intInterestGraceDays = CStrings::strToIntDef( $intInterestGraceDays, NULL, false );
    }

    public function getInterestGraceDays() {
        return $this->m_intInterestGraceDays;
    }

    public function sqlInterestGraceDays() {
        return ( true == isset( $this->m_intInterestGraceDays ) ) ? (string) $this->m_intInterestGraceDays : '0';
    }

    public function setInterestPeriodForfeitDays( $intInterestPeriodForfeitDays ) {
        $this->m_intInterestPeriodForfeitDays = CStrings::strToIntDef( $intInterestPeriodForfeitDays, NULL, false );
    }

    public function getInterestPeriodForfeitDays() {
        return $this->m_intInterestPeriodForfeitDays;
    }

    public function sqlInterestPeriodForfeitDays() {
        return ( true == isset( $this->m_intInterestPeriodForfeitDays ) ) ? (string) $this->m_intInterestPeriodForfeitDays : '0';
    }

    public function setDenyInterestForSkips( $boolDenyInterestForSkips ) {
        $this->m_boolDenyInterestForSkips = CStrings::strToBool( $boolDenyInterestForSkips );
    }

    public function getDenyInterestForSkips() {
        return $this->m_boolDenyInterestForSkips;
    }

    public function sqlDenyInterestForSkips() {
        return ( true == isset( $this->m_boolDenyInterestForSkips ) ) ? '\'' . ( true == ( bool ) $this->m_boolDenyInterestForSkips ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setGenerateRefundCheck( $boolGenerateRefundCheck ) {
        $this->m_boolGenerateRefundCheck = CStrings::strToBool( $boolGenerateRefundCheck );
    }

    public function getGenerateRefundCheck() {
        return $this->m_boolGenerateRefundCheck;
    }

    public function sqlGenerateRefundCheck() {
        return ( true == isset( $this->m_boolGenerateRefundCheck ) ) ? '\'' . ( true == ( bool ) $this->m_boolGenerateRefundCheck ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.deposit_interest_formulas_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.deposit_interest_formulas( id, cid, name, interest_start_days, interest_qualification_days, interest_grace_days, interest_period_forfeit_days, deny_interest_for_skips, generate_refund_check, updated_by, updated_on, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlName() . ', ' . 		                $this->sqlInterestStartDays() . ', ' . 		                $this->sqlInterestQualificationDays() . ', ' . 		                $this->sqlInterestGraceDays() . ', ' . 		                $this->sqlInterestPeriodForfeitDays() . ', ' . 		                $this->sqlDenyInterestForSkips() . ', ' . 		                $this->sqlGenerateRefundCheck() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.deposit_interest_formulas
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_start_days = ' . $this->sqlInterestStartDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInterestStartDays() ) != $this->getOriginalValueByFieldName ( 'interest_start_days' ) ) { $arrstrOriginalValueChanges['interest_start_days'] = $this->sqlInterestStartDays(); $strSql .= ' interest_start_days = ' . $this->sqlInterestStartDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_qualification_days = ' . $this->sqlInterestQualificationDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInterestQualificationDays() ) != $this->getOriginalValueByFieldName ( 'interest_qualification_days' ) ) { $arrstrOriginalValueChanges['interest_qualification_days'] = $this->sqlInterestQualificationDays(); $strSql .= ' interest_qualification_days = ' . $this->sqlInterestQualificationDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_grace_days = ' . $this->sqlInterestGraceDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInterestGraceDays() ) != $this->getOriginalValueByFieldName ( 'interest_grace_days' ) ) { $arrstrOriginalValueChanges['interest_grace_days'] = $this->sqlInterestGraceDays(); $strSql .= ' interest_grace_days = ' . $this->sqlInterestGraceDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_period_forfeit_days = ' . $this->sqlInterestPeriodForfeitDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInterestPeriodForfeitDays() ) != $this->getOriginalValueByFieldName ( 'interest_period_forfeit_days' ) ) { $arrstrOriginalValueChanges['interest_period_forfeit_days'] = $this->sqlInterestPeriodForfeitDays(); $strSql .= ' interest_period_forfeit_days = ' . $this->sqlInterestPeriodForfeitDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deny_interest_for_skips = ' . $this->sqlDenyInterestForSkips() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDenyInterestForSkips() ) != $this->getOriginalValueByFieldName ( 'deny_interest_for_skips' ) ) { $arrstrOriginalValueChanges['deny_interest_for_skips'] = $this->sqlDenyInterestForSkips(); $strSql .= ' deny_interest_for_skips = ' . $this->sqlDenyInterestForSkips() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' generate_refund_check = ' . $this->sqlGenerateRefundCheck() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlGenerateRefundCheck() ) != $this->getOriginalValueByFieldName ( 'generate_refund_check' ) ) { $arrstrOriginalValueChanges['generate_refund_check'] = $this->sqlGenerateRefundCheck(); $strSql .= ' generate_refund_check = ' . $this->sqlGenerateRefundCheck() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.deposit_interest_formulas WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.deposit_interest_formulas_id_seq', $objDatabase );
    }

}
?>