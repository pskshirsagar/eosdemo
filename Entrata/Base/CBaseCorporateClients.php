<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateClients
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateClients extends CEosPluralBase {

	/**
	 * @return CCorporateClient[]
	 */
	public static function fetchCorporateClients( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCorporateClient', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporateClient
	 */
	public static function fetchCorporateClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCorporateClient', $objDatabase );
	}

	public static function fetchCorporateClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_clients', $objDatabase );
	}

	public static function fetchCorporateClientByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporateClient( sprintf( 'SELECT * FROM corporate_clients WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateClientsByCid( $intCid, $objDatabase ) {
		return self::fetchCorporateClients( sprintf( 'SELECT * FROM corporate_clients WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>