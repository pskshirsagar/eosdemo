<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIpsChecklistQuestionPropertyDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ips_checklist_question_property_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intIpsChecklistQuestionId;
	protected $m_boolIsCompleted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsClientSpecific;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strAnswerText;
	protected $m_boolIsAnswerVerified;
	protected $m_intAnswerVerifiedBy;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCompleted = false;
		$this->m_boolIsClientSpecific = false;
		$this->m_boolIsAnswerVerified = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ips_checklist_question_id'] ) && $boolDirectSet ) $this->set( 'm_intIpsChecklistQuestionId', trim( $arrValues['ips_checklist_question_id'] ) ); elseif( isset( $arrValues['ips_checklist_question_id'] ) ) $this->setIpsChecklistQuestionId( $arrValues['ips_checklist_question_id'] );
		if( isset( $arrValues['is_completed'] ) && $boolDirectSet ) $this->set( 'm_boolIsCompleted', trim( stripcslashes( $arrValues['is_completed'] ) ) ); elseif( isset( $arrValues['is_completed'] ) ) $this->setIsCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_completed'] ) : $arrValues['is_completed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_client_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientSpecific', trim( stripcslashes( $arrValues['is_client_specific'] ) ) ); elseif( isset( $arrValues['is_client_specific'] ) ) $this->setIsClientSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_specific'] ) : $arrValues['is_client_specific'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['answer_text'] ) && $boolDirectSet ) $this->set( 'm_strAnswerText', trim( stripcslashes( $arrValues['answer_text'] ) ) ); elseif( isset( $arrValues['answer_text'] ) ) $this->setAnswerText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer_text'] ) : $arrValues['answer_text'] );
		if( isset( $arrValues['is_answer_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsAnswerVerified', trim( stripcslashes( $arrValues['is_answer_verified'] ) ) ); elseif( isset( $arrValues['is_answer_verified'] ) ) $this->setIsAnswerVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_answer_verified'] ) : $arrValues['is_answer_verified'] );
		if( isset( $arrValues['answer_verified_by'] ) && $boolDirectSet ) $this->set( 'm_intAnswerVerifiedBy', trim( $arrValues['answer_verified_by'] ) ); elseif( isset( $arrValues['answer_verified_by'] ) ) $this->setAnswerVerifiedBy( $arrValues['answer_verified_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setIpsChecklistQuestionId( $intIpsChecklistQuestionId ) {
		$this->set( 'm_intIpsChecklistQuestionId', CStrings::strToIntDef( $intIpsChecklistQuestionId, NULL, false ) );
	}

	public function getIpsChecklistQuestionId() {
		return $this->m_intIpsChecklistQuestionId;
	}

	public function sqlIpsChecklistQuestionId() {
		return ( true == isset( $this->m_intIpsChecklistQuestionId ) ) ? ( string ) $this->m_intIpsChecklistQuestionId : 'NULL';
	}

	public function setIsCompleted( $boolIsCompleted ) {
		$this->set( 'm_boolIsCompleted', CStrings::strToBool( $boolIsCompleted ) );
	}

	public function getIsCompleted() {
		return $this->m_boolIsCompleted;
	}

	public function sqlIsCompleted() {
		return ( true == isset( $this->m_boolIsCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsClientSpecific( $boolIsClientSpecific ) {
		$this->set( 'm_boolIsClientSpecific', CStrings::strToBool( $boolIsClientSpecific ) );
	}

	public function getIsClientSpecific() {
		return $this->m_boolIsClientSpecific;
	}

	public function sqlIsClientSpecific() {
		return ( true == isset( $this->m_boolIsClientSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAnswerText( $strAnswerText ) {
		$this->set( 'm_strAnswerText', CStrings::strTrimDef( $strAnswerText, -1, NULL, true ) );
	}

	public function getAnswerText() {
		return $this->m_strAnswerText;
	}

	public function sqlAnswerText() {
		return ( true == isset( $this->m_strAnswerText ) ) ? '\'' . addslashes( $this->m_strAnswerText ) . '\'' : 'NULL';
	}

	public function setIsAnswerVerified( $boolIsAnswerVerified ) {
		$this->set( 'm_boolIsAnswerVerified', CStrings::strToBool( $boolIsAnswerVerified ) );
	}

	public function getIsAnswerVerified() {
		return $this->m_boolIsAnswerVerified;
	}

	public function sqlIsAnswerVerified() {
		return ( true == isset( $this->m_boolIsAnswerVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAnswerVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAnswerVerifiedBy( $intAnswerVerifiedBy ) {
		$this->set( 'm_intAnswerVerifiedBy', CStrings::strToIntDef( $intAnswerVerifiedBy, NULL, false ) );
	}

	public function getAnswerVerifiedBy() {
		return $this->m_intAnswerVerifiedBy;
	}

	public function sqlAnswerVerifiedBy() {
		return ( true == isset( $this->m_intAnswerVerifiedBy ) ) ? ( string ) $this->m_intAnswerVerifiedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ips_checklist_question_id, is_completed, updated_by, updated_on, created_by, created_on, is_client_specific, details, answer_text, is_answer_verified, answer_verified_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlIpsChecklistQuestionId() . ', ' .
						$this->sqlIsCompleted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsClientSpecific() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlAnswerText() . ', ' .
						$this->sqlIsAnswerVerified() . ', ' .
						$this->sqlAnswerVerifiedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ips_checklist_question_id = ' . $this->sqlIpsChecklistQuestionId(). ',' ; } elseif( true == array_key_exists( 'IpsChecklistQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' ips_checklist_question_id = ' . $this->sqlIpsChecklistQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted(). ',' ; } elseif( true == array_key_exists( 'IsCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific(). ',' ; } elseif( true == array_key_exists( 'IsClientSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer_text = ' . $this->sqlAnswerText(). ',' ; } elseif( true == array_key_exists( 'AnswerText', $this->getChangedColumns() ) ) { $strSql .= ' answer_text = ' . $this->sqlAnswerText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_answer_verified = ' . $this->sqlIsAnswerVerified(). ',' ; } elseif( true == array_key_exists( 'IsAnswerVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_answer_verified = ' . $this->sqlIsAnswerVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer_verified_by = ' . $this->sqlAnswerVerifiedBy(). ',' ; } elseif( true == array_key_exists( 'AnswerVerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' answer_verified_by = ' . $this->sqlAnswerVerifiedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ips_checklist_question_id' => $this->getIpsChecklistQuestionId(),
			'is_completed' => $this->getIsCompleted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_client_specific' => $this->getIsClientSpecific(),
			'details' => $this->getDetails(),
			'answer_text' => $this->getAnswerText(),
			'is_answer_verified' => $this->getIsAnswerVerified(),
			'answer_verified_by' => $this->getAnswerVerifiedBy()
		);
	}

}
?>