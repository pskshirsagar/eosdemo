<?php

class CBaseApReimbursementTypes extends CEosPluralBase {

    public static function fetchApReimbursementTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CApReimbursementType', $objDatabase );
    }

    public static function fetchApReimbursementType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApReimbursementType', $objDatabase );
    }

    public static function fetchApReimbursementTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ap_reimbursement_types', $objDatabase );
    }

    public static function fetchApReimbursementTypeById( $intId, $objDatabase ) {
        return self::fetchApReimbursementType( sprintf( 'SELECT * FROM ap_reimbursement_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>