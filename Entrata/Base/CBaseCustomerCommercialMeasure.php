<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerCommercialMeasure extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.customer_commercial_measures';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intCommercialSaleBreakpointsId;
	protected $m_strMeasureMonth;
	protected $m_strMeasureDate;
	protected $m_fltGrossSales;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['commercial_sale_breakpoints_id'] ) && $boolDirectSet ) $this->set( 'm_intCommercialSaleBreakpointsId', trim( $arrValues['commercial_sale_breakpoints_id'] ) ); elseif( isset( $arrValues['commercial_sale_breakpoints_id'] ) ) $this->setCommercialSaleBreakpointsId( $arrValues['commercial_sale_breakpoints_id'] );
		if( isset( $arrValues['measure_month'] ) && $boolDirectSet ) $this->set( 'm_strMeasureMonth', trim( $arrValues['measure_month'] ) ); elseif( isset( $arrValues['measure_month'] ) ) $this->setMeasureMonth( $arrValues['measure_month'] );
		if( isset( $arrValues['measure_date'] ) && $boolDirectSet ) $this->set( 'm_strMeasureDate', trim( $arrValues['measure_date'] ) ); elseif( isset( $arrValues['measure_date'] ) ) $this->setMeasureDate( $arrValues['measure_date'] );
		if( isset( $arrValues['gross_sales'] ) && $boolDirectSet ) $this->set( 'm_fltGrossSales', trim( $arrValues['gross_sales'] ) ); elseif( isset( $arrValues['gross_sales'] ) ) $this->setGrossSales( $arrValues['gross_sales'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCommercialSaleBreakpointsId( $intCommercialSaleBreakpointsId ) {
		$this->set( 'm_intCommercialSaleBreakpointsId', CStrings::strToIntDef( $intCommercialSaleBreakpointsId, NULL, false ) );
	}

	public function getCommercialSaleBreakpointsId() {
		return $this->m_intCommercialSaleBreakpointsId;
	}

	public function sqlCommercialSaleBreakpointsId() {
		return ( true == isset( $this->m_intCommercialSaleBreakpointsId ) ) ? ( string ) $this->m_intCommercialSaleBreakpointsId : 'NULL';
	}

	public function setMeasureMonth( $strMeasureMonth ) {
		$this->set( 'm_strMeasureMonth', CStrings::strTrimDef( $strMeasureMonth, -1, NULL, true ) );
	}

	public function getMeasureMonth() {
		return $this->m_strMeasureMonth;
	}

	public function sqlMeasureMonth() {
		return ( true == isset( $this->m_strMeasureMonth ) ) ? '\'' . $this->m_strMeasureMonth . '\'' : 'NOW()';
	}

	public function setMeasureDate( $strMeasureDate ) {
		$this->set( 'm_strMeasureDate', CStrings::strTrimDef( $strMeasureDate, -1, NULL, true ) );
	}

	public function getMeasureDate() {
		return $this->m_strMeasureDate;
	}

	public function sqlMeasureDate() {
		return ( true == isset( $this->m_strMeasureDate ) ) ? '\'' . $this->m_strMeasureDate . '\'' : 'NOW()';
	}

	public function setGrossSales( $fltGrossSales ) {
		$this->set( 'm_fltGrossSales', CStrings::strToFloatDef( $fltGrossSales, NULL, false, 2 ) );
	}

	public function getGrossSales() {
		return $this->m_fltGrossSales;
	}

	public function sqlGrossSales() {
		return ( true == isset( $this->m_fltGrossSales ) ) ? ( string ) $this->m_fltGrossSales : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, lease_id, commercial_sale_breakpoints_id, measure_month, measure_date, gross_sales, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCommercialSaleBreakpointsId() . ', ' .
		          $this->sqlMeasureMonth() . ', ' .
		          $this->sqlMeasureDate() . ', ' .
		          $this->sqlGrossSales() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_sale_breakpoints_id = ' . $this->sqlCommercialSaleBreakpointsId(). ',' ; } elseif( true == array_key_exists( 'CommercialSaleBreakpointsId', $this->getChangedColumns() ) ) { $strSql .= ' commercial_sale_breakpoints_id = ' . $this->sqlCommercialSaleBreakpointsId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' measure_month = ' . $this->sqlMeasureMonth(). ',' ; } elseif( true == array_key_exists( 'MeasureMonth', $this->getChangedColumns() ) ) { $strSql .= ' measure_month = ' . $this->sqlMeasureMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' measure_date = ' . $this->sqlMeasureDate(). ',' ; } elseif( true == array_key_exists( 'MeasureDate', $this->getChangedColumns() ) ) { $strSql .= ' measure_date = ' . $this->sqlMeasureDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_sales = ' . $this->sqlGrossSales(). ',' ; } elseif( true == array_key_exists( 'GrossSales', $this->getChangedColumns() ) ) { $strSql .= ' gross_sales = ' . $this->sqlGrossSales() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'commercial_sale_breakpoints_id' => $this->getCommercialSaleBreakpointsId(),
			'measure_month' => $this->getMeasureMonth(),
			'measure_date' => $this->getMeasureDate(),
			'gross_sales' => $this->getGrossSales(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>