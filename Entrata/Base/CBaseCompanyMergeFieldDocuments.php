<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldDocuments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeFieldDocuments extends CEosPluralBase {

	/**
	 * @return CCompanyMergeFieldDocument[]
	 */
	public static function fetchCompanyMergeFieldDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyMergeFieldDocument::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyMergeFieldDocument
	 */
	public static function fetchCompanyMergeFieldDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyMergeFieldDocument::class, $objDatabase );
	}

	public static function fetchCompanyMergeFieldDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merge_field_documents', $objDatabase );
	}

	public static function fetchCompanyMergeFieldDocumentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldDocument( sprintf( 'SELECT * FROM company_merge_field_documents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldDocuments( sprintf( 'SELECT * FROM company_merge_field_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldDocumentsByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldDocuments( sprintf( 'SELECT * FROM company_merge_field_documents WHERE company_merge_field_id = %d AND cid = %d', ( int ) $intCompanyMergeFieldId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldDocuments( sprintf( 'SELECT * FROM company_merge_field_documents WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

}
?>