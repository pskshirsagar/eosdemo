<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeTemplates extends CEosPluralBase {

	/**
	 * @return CFeeTemplate[]
	 */
	public static function fetchFeeTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFeeTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFeeTemplate
	 */
	public static function fetchFeeTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeTemplate::class, $objDatabase );
	}

	public static function fetchFeeTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_templates', $objDatabase );
	}

	public static function fetchFeeTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplate( sprintf( 'SELECT * FROM fee_templates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByApPaymentTypeIdByCid( $intApPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE ap_payment_type_id = %d AND cid = %d', $intApPaymentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByFeeTypeIdByCid( $intFeeTypeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE fee_type_id = %d AND cid = %d', $intFeeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByApRoutingTagIdByCid( $intApRoutingTagId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE ap_routing_tag_id = %d AND cid = %d', $intApRoutingTagId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatesByFeeCalculationTypeIdByCid( $intFeeCalculationTypeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplates( sprintf( 'SELECT * FROM fee_templates WHERE fee_calculation_type_id = %d AND cid = %d', $intFeeCalculationTypeId, $intCid ), $objDatabase );
	}

}
?>