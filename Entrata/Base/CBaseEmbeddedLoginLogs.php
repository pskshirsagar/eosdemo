<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmbeddedLoginLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmbeddedLoginLogs extends CEosPluralBase {

	/**
	 * @return CEmbeddedLoginLog[]
	 */
	public static function fetchEmbeddedLoginLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CEmbeddedLoginLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmbeddedLoginLog
	 */
	public static function fetchEmbeddedLoginLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmbeddedLoginLog', $objDatabase );
	}

	public static function fetchEmbeddedLoginLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'embedded_login_logs', $objDatabase );
	}

	public static function fetchEmbeddedLoginLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmbeddedLoginLog( sprintf( 'SELECT * FROM embedded_login_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmbeddedLoginLogsByCid( $intCid, $objDatabase ) {
		return self::fetchEmbeddedLoginLogs( sprintf( 'SELECT * FROM embedded_login_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmbeddedLoginLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchEmbeddedLoginLogs( sprintf( 'SELECT * FROM embedded_login_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>