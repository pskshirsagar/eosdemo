<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConnectionTypes
 * Do not add any new functions to this class.
 */

class CBaseTransmissionConnectionTypes extends CEosPluralBase {

	/**
	 * @return CTransmissionConnectionType[]
	 */
	public static function fetchTransmissionConnectionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransmissionConnectionType::class, $objDatabase );
	}

	/**
	 * @return CTransmissionConnectionType
	 */
	public static function fetchTransmissionConnectionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransmissionConnectionType::class, $objDatabase );
	}

	public static function fetchTransmissionConnectionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transmission_connection_types', $objDatabase );
	}

	public static function fetchTransmissionConnectionTypeById( $intId, $objDatabase ) {
		return self::fetchTransmissionConnectionType( sprintf( 'SELECT * FROM transmission_connection_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>