<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialClauseKeyTypes
 * Do not add any new functions to this class.
 */

class CBaseCommercialClauseKeyTypes extends CEosPluralBase {

	/**
	 * @return CCommercialClauseKeyType[]
	 */
	public static function fetchCommercialClauseKeyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCommercialClauseKeyType::class, $objDatabase );
	}

	/**
	 * @return CCommercialClauseKeyType
	 */
	public static function fetchCommercialClauseKeyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCommercialClauseKeyType::class, $objDatabase );
	}

	public static function fetchCommercialClauseKeyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_clause_key_types', $objDatabase );
	}

	public static function fetchCommercialClauseKeyTypeById( $intId, $objDatabase ) {
		return self::fetchCommercialClauseKeyType( sprintf( 'SELECT * FROM commercial_clause_key_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>