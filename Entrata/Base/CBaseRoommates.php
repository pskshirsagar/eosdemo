<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoommates extends CEosPluralBase {

	/**
	 * @return CRoommate[]
	 */
	public static function fetchRoommates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRoommate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRoommate
	 */
	public static function fetchRoommate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRoommate', $objDatabase );
	}

	public static function fetchRoommateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roommates', $objDatabase );
	}

	public static function fetchRoommateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRoommate( sprintf( 'SELECT * FROM roommates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommatesByCid( $intCid, $objDatabase ) {
		return self::fetchRoommates( sprintf( 'SELECT * FROM roommates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommatesByRoommateGroupIdByCid( $intRoommateGroupId, $intCid, $objDatabase ) {
		return self::fetchRoommates( sprintf( 'SELECT * FROM roommates WHERE roommate_group_id = %d AND cid = %d', ( int ) $intRoommateGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommatesByParentLeaseIdByCid( $intParentLeaseId, $intCid, $objDatabase ) {
		return self::fetchRoommates( sprintf( 'SELECT * FROM roommates WHERE parent_lease_id = %d AND cid = %d', ( int ) $intParentLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRoommatesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchRoommates( sprintf( 'SELECT * FROM roommates WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>