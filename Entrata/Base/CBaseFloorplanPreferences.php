<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFloorplanPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFloorplanPreferences extends CEosPluralBase {

	/**
	 * @return CFloorplanPreference[]
	 */
	public static function fetchFloorplanPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFloorplanPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFloorplanPreference
	 */
	public static function fetchFloorplanPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFloorplanPreference', $objDatabase );
	}

	public static function fetchFloorplanPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'floorplan_preferences', $objDatabase );
	}

	public static function fetchFloorplanPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFloorplanPreference( sprintf( 'SELECT * FROM floorplan_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchFloorplanPreferences( sprintf( 'SELECT * FROM floorplan_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFloorplanPreferencesByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchFloorplanPreferences( sprintf( 'SELECT * FROM floorplan_preferences WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

}
?>