<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountMonthNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetGlAccountMonthNotes extends CEosPluralBase {

	/**
	 * @return CBudgetGlAccountMonthNote[]
	 */
	public static function fetchBudgetGlAccountMonthNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CBudgetGlAccountMonthNote', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetGlAccountMonthNote
	 */
	public static function fetchBudgetGlAccountMonthNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBudgetGlAccountMonthNote', $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_gl_account_month_notes', $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNote( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNotesByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNotes( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNotesByBudgetGlAccountMonthNoteTypeIdByCid( $intBudgetGlAccountMonthNoteTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNotes( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE budget_gl_account_month_note_type_id = %d AND cid = %d', ( int ) $intBudgetGlAccountMonthNoteTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNotesByBudgetGlAccountMonthIdByCid( $intBudgetGlAccountMonthId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNotes( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE budget_gl_account_month_id = %d AND cid = %d', ( int ) $intBudgetGlAccountMonthId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNotesByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNotes( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE budget_id = %d AND cid = %d', ( int ) $intBudgetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthNotesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonthNotes( sprintf( 'SELECT * FROM budget_gl_account_month_notes WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>