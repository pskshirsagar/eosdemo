<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyConciergeServices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyConciergeServices extends CEosPluralBase {

	/**
	 * @return CPropertyConciergeService[]
	 */
	public static function fetchPropertyConciergeServices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyConciergeService', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyConciergeService
	 */
	public static function fetchPropertyConciergeService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyConciergeService', $objDatabase );
	}

	public static function fetchPropertyConciergeServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_concierge_services', $objDatabase );
	}

	public static function fetchPropertyConciergeServiceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyConciergeService( sprintf( 'SELECT * FROM property_concierge_services WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyConciergeServicesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyConciergeServices( sprintf( 'SELECT * FROM property_concierge_services WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyConciergeServicesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyConciergeServices( sprintf( 'SELECT * FROM property_concierge_services WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyConciergeServicesByCompanyConciergeServiceIdByCid( $intCompanyConciergeServiceId, $intCid, $objDatabase ) {
		return self::fetchPropertyConciergeServices( sprintf( 'SELECT * FROM property_concierge_services WHERE company_concierge_service_id = %d AND cid = %d', ( int ) $intCompanyConciergeServiceId, ( int ) $intCid ), $objDatabase );
	}

}
?>