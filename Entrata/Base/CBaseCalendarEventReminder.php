<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEventReminder extends CEosSingularBase {

	const TABLE_NAME = 'public.calendar_event_reminders';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intCalendarEventId;
	protected $m_intReminderTypeId;
	protected $m_intIntervalTypeId;
	protected $m_intIntervalCount;
	protected $m_strReminderDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIntervalTypeId = '1';
		$this->m_intIntervalCount = '30';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_intCalendarEventId', trim( $arrValues['calendar_event_id'] ) ); elseif( isset( $arrValues['calendar_event_id'] ) ) $this->setCalendarEventId( $arrValues['calendar_event_id'] );
		if( isset( $arrValues['reminder_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReminderTypeId', trim( $arrValues['reminder_type_id'] ) ); elseif( isset( $arrValues['reminder_type_id'] ) ) $this->setReminderTypeId( $arrValues['reminder_type_id'] );
		if( isset( $arrValues['interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntervalTypeId', trim( $arrValues['interval_type_id'] ) ); elseif( isset( $arrValues['interval_type_id'] ) ) $this->setIntervalTypeId( $arrValues['interval_type_id'] );
		if( isset( $arrValues['interval_count'] ) && $boolDirectSet ) $this->set( 'm_intIntervalCount', trim( $arrValues['interval_count'] ) ); elseif( isset( $arrValues['interval_count'] ) ) $this->setIntervalCount( $arrValues['interval_count'] );
		if( isset( $arrValues['reminder_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReminderDatetime', trim( $arrValues['reminder_datetime'] ) ); elseif( isset( $arrValues['reminder_datetime'] ) ) $this->setReminderDatetime( $arrValues['reminder_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setCalendarEventId( $intCalendarEventId ) {
		$this->set( 'm_intCalendarEventId', CStrings::strToIntDef( $intCalendarEventId, NULL, false ) );
	}

	public function getCalendarEventId() {
		return $this->m_intCalendarEventId;
	}

	public function sqlCalendarEventId() {
		return ( true == isset( $this->m_intCalendarEventId ) ) ? ( string ) $this->m_intCalendarEventId : 'NULL';
	}

	public function setReminderTypeId( $intReminderTypeId ) {
		$this->set( 'm_intReminderTypeId', CStrings::strToIntDef( $intReminderTypeId, NULL, false ) );
	}

	public function getReminderTypeId() {
		return $this->m_intReminderTypeId;
	}

	public function sqlReminderTypeId() {
		return ( true == isset( $this->m_intReminderTypeId ) ) ? ( string ) $this->m_intReminderTypeId : 'NULL';
	}

	public function setIntervalTypeId( $intIntervalTypeId ) {
		$this->set( 'm_intIntervalTypeId', CStrings::strToIntDef( $intIntervalTypeId, NULL, false ) );
	}

	public function getIntervalTypeId() {
		return $this->m_intIntervalTypeId;
	}

	public function sqlIntervalTypeId() {
		return ( true == isset( $this->m_intIntervalTypeId ) ) ? ( string ) $this->m_intIntervalTypeId : '1';
	}

	public function setIntervalCount( $intIntervalCount ) {
		$this->set( 'm_intIntervalCount', CStrings::strToIntDef( $intIntervalCount, NULL, false ) );
	}

	public function getIntervalCount() {
		return $this->m_intIntervalCount;
	}

	public function sqlIntervalCount() {
		return ( true == isset( $this->m_intIntervalCount ) ) ? ( string ) $this->m_intIntervalCount : '30';
	}

	public function setReminderDatetime( $strReminderDatetime ) {
		$this->set( 'm_strReminderDatetime', CStrings::strTrimDef( $strReminderDatetime, -1, NULL, true ) );
	}

	public function getReminderDatetime() {
		return $this->m_strReminderDatetime;
	}

	public function sqlReminderDatetime() {
		return ( true == isset( $this->m_strReminderDatetime ) ) ? '\'' . $this->m_strReminderDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, calendar_event_id, reminder_type_id, interval_type_id, interval_count, reminder_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlCalendarEventId() . ', ' .
 						$this->sqlReminderTypeId() . ', ' .
 						$this->sqlIntervalTypeId() . ', ' .
 						$this->sqlIntervalCount() . ', ' .
 						$this->sqlReminderDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_event_id = ' . $this->sqlCalendarEventId() . ','; } elseif( true == array_key_exists( 'CalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' calendar_event_id = ' . $this->sqlCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_type_id = ' . $this->sqlReminderTypeId() . ','; } elseif( true == array_key_exists( 'ReminderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reminder_type_id = ' . $this->sqlReminderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_type_id = ' . $this->sqlIntervalTypeId() . ','; } elseif( true == array_key_exists( 'IntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' interval_type_id = ' . $this->sqlIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_count = ' . $this->sqlIntervalCount() . ','; } elseif( true == array_key_exists( 'IntervalCount', $this->getChangedColumns() ) ) { $strSql .= ' interval_count = ' . $this->sqlIntervalCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; } elseif( true == array_key_exists( 'ReminderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'calendar_event_id' => $this->getCalendarEventId(),
			'reminder_type_id' => $this->getReminderTypeId(),
			'interval_type_id' => $this->getIntervalTypeId(),
			'interval_count' => $this->getIntervalCount(),
			'reminder_datetime' => $this->getReminderDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>