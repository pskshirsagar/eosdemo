<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomAdvertisements
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomAdvertisements extends CEosPluralBase {

	/**
	 * @return CCustomAdvertisement[]
	 */
	public static function fetchCustomAdvertisements( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomAdvertisement::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomAdvertisement
	 */
	public static function fetchCustomAdvertisement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomAdvertisement::class, $objDatabase );
	}

	public static function fetchCustomAdvertisementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'custom_advertisements', $objDatabase );
	}

	public static function fetchCustomAdvertisementByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisement( sprintf( 'SELECT * FROM custom_advertisements WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomAdvertisementsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisements( sprintf( 'SELECT * FROM custom_advertisements WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomAdvertisementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisements( sprintf( 'SELECT * FROM custom_advertisements WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCustomAdvertisementsByCustomAdTypeIdByCid( $intCustomAdTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisements( sprintf( 'SELECT * FROM custom_advertisements WHERE custom_ad_type_id = %d AND cid = %d', $intCustomAdTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomAdvertisementsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisements( sprintf( 'SELECT * FROM custom_advertisements WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

}
?>