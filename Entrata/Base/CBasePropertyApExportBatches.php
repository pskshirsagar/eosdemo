<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApExportBatches extends CEosPluralBase {

	/**
	 * @return CPropertyApExportBatch[]
	 */
	public static function fetchPropertyApExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyApExportBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApExportBatch
	 */
	public static function fetchPropertyApExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyApExportBatch', $objDatabase );
	}

	public static function fetchPropertyApExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_ap_export_batches', $objDatabase );
	}

	public static function fetchPropertyApExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApExportBatch( sprintf( 'SELECT * FROM property_ap_export_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApExportBatches( sprintf( 'SELECT * FROM property_ap_export_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApExportBatchesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApExportBatches( sprintf( 'SELECT * FROM property_ap_export_batches WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApExportBatchesByScheduledApExportBatchIdByCid( $intScheduledApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchPropertyApExportBatches( sprintf( 'SELECT * FROM property_ap_export_batches WHERE scheduled_ap_export_batch_id = %d AND cid = %d', ( int ) $intScheduledApExportBatchId, ( int ) $intCid ), $objDatabase );
	}

}
?>