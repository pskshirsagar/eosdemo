<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueScheduledChargeTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueScheduledChargeTypes extends CEosPluralBase {

	/**
	 * @return CRevenueScheduledChargeType[]
	 */
	public static function fetchRevenueScheduledChargeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueScheduledChargeType::class, $objDatabase );
	}

	/**
	 * @return CRevenueScheduledChargeType
	 */
	public static function fetchRevenueScheduledChargeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueScheduledChargeType::class, $objDatabase );
	}

	public static function fetchRevenueScheduledChargeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_scheduled_charge_types', $objDatabase );
	}

	public static function fetchRevenueScheduledChargeTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueScheduledChargeType( sprintf( 'SELECT * FROM revenue_scheduled_charge_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>