<?php

class CBaseRpMenuItem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.rp_menu_items';

	protected $m_intId;
	protected $m_intPropertySettingKeyId;
	protected $m_intMenuItemTypeId;
	protected $m_strTitle;
	protected $m_strHoverText;
	protected $m_intParentId;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_setting_key_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertySettingKeyId', trim( $arrValues['property_setting_key_id'] ) ); elseif( isset( $arrValues['property_setting_key_id'] ) ) $this->setPropertySettingKeyId( $arrValues['property_setting_key_id'] );
		if( isset( $arrValues['menu_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMenuItemTypeId', trim( $arrValues['menu_item_type_id'] ) ); elseif( isset( $arrValues['menu_item_type_id'] ) ) $this->setMenuItemTypeId( $arrValues['menu_item_type_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['hover_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strHoverText', trim( $arrValues['hover_text'] ) ); elseif( isset( $arrValues['hover_text'] ) ) $this->setHoverText( $arrValues['hover_text'] );
		if( isset( $arrValues['parent_id'] ) && $boolDirectSet ) $this->set( 'm_intParentId', trim( $arrValues['parent_id'] ) ); elseif( isset( $arrValues['parent_id'] ) ) $this->setParentId( $arrValues['parent_id'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertySettingKeyId( $intPropertySettingKeyId ) {
		$this->set( 'm_intPropertySettingKeyId', CStrings::strToIntDef( $intPropertySettingKeyId, NULL, false ) );
	}

	public function getPropertySettingKeyId() {
		return $this->m_intPropertySettingKeyId;
	}

	public function sqlPropertySettingKeyId() {
		return ( true == isset( $this->m_intPropertySettingKeyId ) ) ? ( string ) $this->m_intPropertySettingKeyId : 'NULL';
	}

	public function setMenuItemTypeId( $intMenuItemTypeId ) {
		$this->set( 'm_intMenuItemTypeId', CStrings::strToIntDef( $intMenuItemTypeId, NULL, false ) );
	}

	public function getMenuItemTypeId() {
		return $this->m_intMenuItemTypeId;
	}

	public function sqlMenuItemTypeId() {
		return ( true == isset( $this->m_intMenuItemTypeId ) ) ? ( string ) $this->m_intMenuItemTypeId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 100, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setHoverText( $strHoverText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strHoverText', CStrings::strTrimDef( $strHoverText, 100, NULL, true ), $strLocaleCode );
	}

	public function getHoverText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strHoverText', $strLocaleCode );
	}

	public function sqlHoverText() {
		return ( true == isset( $this->m_strHoverText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHoverText ) : '\'' . addslashes( $this->m_strHoverText ) . '\'' ) : 'NULL';
	}

	public function setParentId( $intParentId ) {
		$this->set( 'm_intParentId', CStrings::strToIntDef( $intParentId, NULL, false ) );
	}

	public function getParentId() {
		return $this->m_intParentId;
	}

	public function sqlParentId() {
		return ( true == isset( $this->m_intParentId ) ) ? ( string ) $this->m_intParentId : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_setting_key_id, menu_item_type_id, title, hover_text, parent_id, order_num, details, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPropertySettingKeyId() . ', ' .
						$this->sqlMenuItemTypeId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlHoverText() . ', ' .
						$this->sqlParentId() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId(). ',' ; } elseif( true == array_key_exists( 'PropertySettingKeyId', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' menu_item_type_id = ' . $this->sqlMenuItemTypeId(). ',' ; } elseif( true == array_key_exists( 'MenuItemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' menu_item_type_id = ' . $this->sqlMenuItemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hover_text = ' . $this->sqlHoverText(). ',' ; } elseif( true == array_key_exists( 'HoverText', $this->getChangedColumns() ) ) { $strSql .= ' hover_text = ' . $this->sqlHoverText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_id = ' . $this->sqlParentId(). ',' ; } elseif( true == array_key_exists( 'ParentId', $this->getChangedColumns() ) ) { $strSql .= ' parent_id = ' . $this->sqlParentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_setting_key_id' => $this->getPropertySettingKeyId(),
			'menu_item_type_id' => $this->getMenuItemTypeId(),
			'title' => $this->getTitle(),
			'hover_text' => $this->getHoverText(),
			'parent_id' => $this->getParentId(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>