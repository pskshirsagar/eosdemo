<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceVacancyLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceVacancyLogs extends CEosPluralBase {

	/**
	 * @return CUnitSpaceVacancyLog[]
	 */
	public static function fetchUnitSpaceVacancyLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitSpaceVacancyLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceVacancyLog
	 */
	public static function fetchUnitSpaceVacancyLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitSpaceVacancyLog', $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_vacancy_logs', $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceVacancyLog( sprintf( 'SELECT * FROM unit_space_vacancy_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceVacancyLogs( sprintf( 'SELECT * FROM unit_space_vacancy_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceVacancyLogs( sprintf( 'SELECT * FROM unit_space_vacancy_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceVacancyLogs( sprintf( 'SELECT * FROM unit_space_vacancy_logs WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceVacancyLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceVacancyLogs( sprintf( 'SELECT * FROM unit_space_vacancy_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>