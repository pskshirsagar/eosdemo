<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBankAccountLog extends CEosSingularBase {

	const TABLE_NAME = 'public.property_bank_account_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intBankAccountLogId;
	protected $m_intBankApCodeId;
	protected $m_boolIsReimbursedProperty;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsReimbursedProperty = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['bank_account_log_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountLogId', trim( $arrValues['bank_account_log_id'] ) ); elseif( isset( $arrValues['bank_account_log_id'] ) ) $this->setBankAccountLogId( $arrValues['bank_account_log_id'] );
		if( isset( $arrValues['bank_ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intBankApCodeId', trim( $arrValues['bank_ap_code_id'] ) ); elseif( isset( $arrValues['bank_ap_code_id'] ) ) $this->setBankApCodeId( $arrValues['bank_ap_code_id'] );
		if( isset( $arrValues['is_reimbursed_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsReimbursedProperty', trim( stripcslashes( $arrValues['is_reimbursed_property'] ) ) ); elseif( isset( $arrValues['is_reimbursed_property'] ) ) $this->setIsReimbursedProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reimbursed_property'] ) : $arrValues['is_reimbursed_property'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBankAccountLogId( $intBankAccountLogId ) {
		$this->set( 'm_intBankAccountLogId', CStrings::strToIntDef( $intBankAccountLogId, NULL, false ) );
	}

	public function getBankAccountLogId() {
		return $this->m_intBankAccountLogId;
	}

	public function sqlBankAccountLogId() {
		return ( true == isset( $this->m_intBankAccountLogId ) ) ? ( string ) $this->m_intBankAccountLogId : 'NULL';
	}

	public function setBankApCodeId( $intBankApCodeId ) {
		$this->set( 'm_intBankApCodeId', CStrings::strToIntDef( $intBankApCodeId, NULL, false ) );
	}

	public function getBankApCodeId() {
		return $this->m_intBankApCodeId;
	}

	public function sqlBankApCodeId() {
		return ( true == isset( $this->m_intBankApCodeId ) ) ? ( string ) $this->m_intBankApCodeId : 'NULL';
	}

	public function setIsReimbursedProperty( $boolIsReimbursedProperty ) {
		$this->set( 'm_boolIsReimbursedProperty', CStrings::strToBool( $boolIsReimbursedProperty ) );
	}

	public function getIsReimbursedProperty() {
		return $this->m_boolIsReimbursedProperty;
	}

	public function sqlIsReimbursedProperty() {
		return ( true == isset( $this->m_boolIsReimbursedProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReimbursedProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, bank_account_log_id, bank_ap_code_id, is_reimbursed_property, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlBankAccountLogId() . ', ' .
 						$this->sqlBankApCodeId() . ', ' .
 						$this->sqlIsReimbursedProperty() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_log_id = ' . $this->sqlBankAccountLogId() . ','; } elseif( true == array_key_exists( 'BankAccountLogId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_log_id = ' . $this->sqlBankAccountLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_ap_code_id = ' . $this->sqlBankApCodeId() . ','; } elseif( true == array_key_exists( 'BankApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' bank_ap_code_id = ' . $this->sqlBankApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reimbursed_property = ' . $this->sqlIsReimbursedProperty() . ','; } elseif( true == array_key_exists( 'IsReimbursedProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_reimbursed_property = ' . $this->sqlIsReimbursedProperty() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'bank_account_log_id' => $this->getBankAccountLogId(),
			'bank_ap_code_id' => $this->getBankApCodeId(),
			'is_reimbursed_property' => $this->getIsReimbursedProperty(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>