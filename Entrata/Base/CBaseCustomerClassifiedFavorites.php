<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifiedFavorites
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerClassifiedFavorites extends CEosPluralBase {

	/**
	 * @return CCustomerClassifiedFavorite[]
	 */
	public static function fetchCustomerClassifiedFavorites( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerClassifiedFavorite::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerClassifiedFavorite
	 */
	public static function fetchCustomerClassifiedFavorite( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerClassifiedFavorite::class, $objDatabase );
	}

	public static function fetchCustomerClassifiedFavoriteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_classified_favorites', $objDatabase );
	}

	public static function fetchCustomerClassifiedFavoriteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedFavorite( sprintf( 'SELECT * FROM customer_classified_favorites WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedFavoritesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedFavorites( sprintf( 'SELECT * FROM customer_classified_favorites WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedFavoritesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedFavorites( sprintf( 'SELECT * FROM customer_classified_favorites WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedFavoritesByCustomerClassifiedIdByCid( $intCustomerClassifiedId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedFavorites( sprintf( 'SELECT * FROM customer_classified_favorites WHERE customer_classified_id = %d AND cid = %d', ( int ) $intCustomerClassifiedId, ( int ) $intCid ), $objDatabase );
	}

}
?>