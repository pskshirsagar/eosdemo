<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReviewTransmissionVendor extends CEosSingularBase {

	const TABLE_NAME = 'public.review_transmission_vendors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReviewId;
	protected $m_intTransmissionVendorId;
	protected $m_strRemoteReviewKey;
	protected $m_intReviewTypeId;
	protected $m_intRemotePageKey;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['review_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewId', trim( $arrValues['review_id'] ) ); elseif( isset( $arrValues['review_id'] ) ) $this->setReviewId( $arrValues['review_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['remote_review_key'] ) && $boolDirectSet ) $this->set( 'm_strRemoteReviewKey', trim( stripcslashes( $arrValues['remote_review_key'] ) ) ); elseif( isset( $arrValues['remote_review_key'] ) ) $this->setRemoteReviewKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_review_key'] ) : $arrValues['remote_review_key'] );
		if( isset( $arrValues['review_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewTypeId', trim( $arrValues['review_type_id'] ) ); elseif( isset( $arrValues['review_type_id'] ) ) $this->setReviewTypeId( $arrValues['review_type_id'] );
		if( isset( $arrValues['remote_page_key'] ) && $boolDirectSet ) $this->set( 'm_intRemotePageKey', trim( $arrValues['remote_page_key'] ) ); elseif( isset( $arrValues['remote_page_key'] ) ) $this->setRemotePageKey( $arrValues['remote_page_key'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReviewId( $intReviewId ) {
		$this->set( 'm_intReviewId', CStrings::strToIntDef( $intReviewId, NULL, false ) );
	}

	public function getReviewId() {
		return $this->m_intReviewId;
	}

	public function sqlReviewId() {
		return ( true == isset( $this->m_intReviewId ) ) ? ( string ) $this->m_intReviewId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setRemoteReviewKey( $strRemoteReviewKey ) {
		$this->set( 'm_strRemoteReviewKey', CStrings::strTrimDef( $strRemoteReviewKey, 240, NULL, true ) );
	}

	public function getRemoteReviewKey() {
		return $this->m_strRemoteReviewKey;
	}

	public function sqlRemoteReviewKey() {
		return ( true == isset( $this->m_strRemoteReviewKey ) ) ? '\'' . addslashes( $this->m_strRemoteReviewKey ) . '\'' : 'NULL';
	}

	public function setReviewTypeId( $intReviewTypeId ) {
		$this->set( 'm_intReviewTypeId', CStrings::strToIntDef( $intReviewTypeId, NULL, false ) );
	}

	public function getReviewTypeId() {
		return $this->m_intReviewTypeId;
	}

	public function sqlReviewTypeId() {
		return ( true == isset( $this->m_intReviewTypeId ) ) ? ( string ) $this->m_intReviewTypeId : 'NULL';
	}

	public function setRemotePageKey( $intRemotePageKey ) {
		$this->set( 'm_intRemotePageKey', CStrings::strToIntDef( $intRemotePageKey, NULL, false ) );
	}

	public function getRemotePageKey() {
		return $this->m_intRemotePageKey;
	}

	public function sqlRemotePageKey() {
		return ( true == isset( $this->m_intRemotePageKey ) ) ? ( string ) $this->m_intRemotePageKey : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, review_id, transmission_vendor_id, remote_review_key, review_type_id, remote_page_key, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlReviewId() . ', ' .
 						$this->sqlTransmissionVendorId() . ', ' .
 						$this->sqlRemoteReviewKey() . ', ' .
 						$this->sqlReviewTypeId() . ', ' .
 						$this->sqlRemotePageKey() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_id = ' . $this->sqlReviewId() . ','; } elseif( true == array_key_exists( 'ReviewId', $this->getChangedColumns() ) ) { $strSql .= ' review_id = ' . $this->sqlReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_review_key = ' . $this->sqlRemoteReviewKey() . ','; } elseif( true == array_key_exists( 'RemoteReviewKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_review_key = ' . $this->sqlRemoteReviewKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_type_id = ' . $this->sqlReviewTypeId() . ','; } elseif( true == array_key_exists( 'ReviewTypeId', $this->getChangedColumns() ) ) { $strSql .= ' review_type_id = ' . $this->sqlReviewTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_page_key = ' . $this->sqlRemotePageKey() . ','; } elseif( true == array_key_exists( 'RemotePageKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_page_key = ' . $this->sqlRemotePageKey() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'review_id' => $this->getReviewId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'remote_review_key' => $this->getRemoteReviewKey(),
			'review_type_id' => $this->getReviewTypeId(),
			'remote_page_key' => $this->getRemotePageKey(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>