<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledPaymentDetails extends CEosPluralBase {

	/**
	 * @return CScheduledPaymentDetail[]
	 */
	public static function fetchScheduledPaymentDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledPaymentDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledPaymentDetail
	 */
	public static function fetchScheduledPaymentDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledPaymentDetail', $objDatabase );
	}

	public static function fetchScheduledPaymentDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_payment_details', $objDatabase );
	}

	public static function fetchScheduledPaymentDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentDetail( sprintf( 'SELECT * FROM scheduled_payment_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentDetails( sprintf( 'SELECT * FROM scheduled_payment_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentDetailsByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentDetails( sprintf( 'SELECT * FROM scheduled_payment_details WHERE scheduled_payment_id = %d AND cid = %d', ( int ) $intScheduledPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentDetailsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledPaymentDetails( sprintf( 'SELECT * FROM scheduled_payment_details WHERE check_account_type_id = %d AND cid = %d', ( int ) $intCheckAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>