<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApHeaders extends CEosPluralBase {

	/**
	 * @return CApHeader[]
	 */
	public static function fetchApHeaders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApHeader::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApHeader
	 */
	public static function fetchApHeader( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApHeader::class, $objDatabase );
	}

	public static function fetchApHeaderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_headers', $objDatabase );
	}

	public static function fetchApHeaderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApHeader( sprintf( 'SELECT * FROM ap_headers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByCid( $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApHeaderTypeIdByCid( $intApHeaderTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_header_type_id = %d AND cid = %d', ( int ) $intApHeaderTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApHeaderSubTypeIdByCid( $intApHeaderSubTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_header_sub_type_id = %d AND cid = %d', ( int ) $intApHeaderSubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApHeaderModeIdByCid( $intApHeaderModeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_header_mode_id = %d AND cid = %d', ( int ) $intApHeaderModeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPhysicalStatusTypeIdByCid( $intApPhysicalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_physical_status_type_id = %d AND cid = %d', ( int ) $intApPhysicalStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApFinancialStatusTypeIdByCid( $intApFinancialStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_financial_status_type_id = %d AND cid = %d', ( int ) $intApFinancialStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByOrderHeaderIdByCid( $intOrderHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE order_header_id = %d AND cid = %d', ( int ) $intOrderHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_payee_account_id = %d AND cid = %d', ( int ) $intApPayeeAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApRemittanceIdByCid( $intApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_remittance_id = %d AND cid = %d', ( int ) $intApRemittanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPayeeTermIdByCid( $intApPayeeTermId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_payee_term_id = %d AND cid = %d', ( int ) $intApPayeeTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByInterCoApPayeeIdByCid( $intInterCoApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE inter_co_ap_payee_id = %d AND cid = %d', ( int ) $intInterCoApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByInterCoApPayeeLocationIdByCid( $intInterCoApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE inter_co_ap_payee_location_id = %d AND cid = %d', ( int ) $intInterCoApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByInterCoApPayeeAccountIdByCid( $intInterCoApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE inter_co_ap_payee_account_id = %d AND cid = %d', ( int ) $intInterCoApPayeeAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByInterCoApRemittanceIdByCid( $intInterCoApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE inter_co_ap_remittance_id = %d AND cid = %d', ( int ) $intInterCoApRemittanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE gl_transaction_type_id = %d AND cid = %d', ( int ) $intGlTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_contract_id = %d AND cid = %d', ( int ) $intApContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkJobPhaseIdByCid( $intBulkJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_job_phase_id = %d AND cid = %d', ( int ) $intBulkJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkApContractIdByCid( $intBulkApContractId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_ap_contract_id = %d AND cid = %d', ( int ) $intBulkApContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkBankAccountIdByCid( $intBulkBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_bank_account_id = %d AND cid = %d', ( int ) $intBulkBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkPropertyIdByCid( $intBulkPropertyId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_property_id = %d AND cid = %d', ( int ) $intBulkPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkApFormulaIdByCid( $intBulkApFormulaId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_ap_formula_id = %d AND cid = %d', ( int ) $intBulkApFormulaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkApCodeIdByCid( $intBulkApCodeId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_ap_code_id = %d AND cid = %d', ( int ) $intBulkApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkUnitNumberIdByCid( $intBulkUnitNumberId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_unit_number_id = %d AND cid = %d', ( int ) $intBulkUnitNumberId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkCompanyDepartmentIdByCid( $intBulkCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_company_department_id = %d AND cid = %d', ( int ) $intBulkCompanyDepartmentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkGlDimensionIdByCid( $intBulkGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_gl_dimension_id = %d AND cid = %d', ( int ) $intBulkGlDimensionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkPropertyBuildingIdByCid( $intBulkPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_property_building_id = %d AND cid = %d', ( int ) $intBulkPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBulkBudgetApHeaderIdByCid( $intBulkBudgetApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE bulk_budget_ap_header_id = %d AND cid = %d', ( int ) $intBulkBudgetApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByReversalApHeaderIdByCid( $intReversalApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE reversal_ap_header_id = %d AND cid = %d', ( int ) $intReversalApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByPaymentApHeaderIdByCid( $intPaymentApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE payment_ap_header_id = %d AND cid = %d', ( int ) $intPaymentApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByScheduledApHeaderIdByCid( $intScheduledApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE scheduled_ap_header_id = %d AND cid = %d', ( int ) $intScheduledApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByTemplateApHeaderIdByCid( $intTemplateApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE template_ap_header_id = %d AND cid = %d', ( int ) $intTemplateApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByReimbursementApHeaderIdByCid( $intReimbursementApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE reimbursement_ap_header_id = %d AND cid = %d', ( int ) $intReimbursementApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByLeaseCustomerIdByCid( $intLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE lease_customer_id = %d AND cid = %d', ( int ) $intLeaseCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByRefundArTransactionIdByCid( $intRefundArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE refund_ar_transaction_id = %d AND cid = %d', ( int ) $intRefundArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApBatchIdByCid( $intApBatchId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_batch_id = %d AND cid = %d', ( int ) $intApBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE accounting_export_batch_id = %d AND cid = %d', ( int ) $intAccountingExportBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByComplianceJobIdByCid( $intComplianceJobId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE compliance_job_id = %d AND cid = %d', ( int ) $intComplianceJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByApRoutingTagIdByCid( $intApRoutingTagId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE ap_routing_tag_id = %d AND cid = %d', ( int ) $intApRoutingTagId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApHeadersByBudgetChangeOrderIdByCid( $intBudgetChangeOrderId, $intCid, $objDatabase ) {
		return self::fetchApHeaders( sprintf( 'SELECT * FROM ap_headers WHERE budget_change_order_id = %d AND cid = %d', ( int ) $intBudgetChangeOrderId, ( int ) $intCid ), $objDatabase );
	}

}
?>