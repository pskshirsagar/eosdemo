<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInputTypes
 * Do not add any new functions to this class.
 */

class CBaseInputTypes extends CEosPluralBase {

	/**
	 * @return CInputType[]
	 */
	public static function fetchInputTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInputType::class, $objDatabase );
	}

	/**
	 * @return CInputType
	 */
	public static function fetchInputType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInputType::class, $objDatabase );
	}

	public static function fetchInputTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'input_types', $objDatabase );
	}

	public static function fetchInputTypeById( $intId, $objDatabase ) {
		return self::fetchInputType( sprintf( 'SELECT * FROM input_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>