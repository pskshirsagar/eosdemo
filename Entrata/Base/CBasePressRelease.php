<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePressRelease extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.press_releases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPressReleaseCategoryId;
	protected $m_strTitle;
	protected $m_strTeaser;
	protected $m_strRedirectUrl;
	protected $m_strBody;
	protected $m_strReleaseDatetime;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['press_release_category_id'] ) && $boolDirectSet ) $this->set( 'm_intPressReleaseCategoryId', trim( $arrValues['press_release_category_id'] ) ); elseif( isset( $arrValues['press_release_category_id'] ) ) $this->setPressReleaseCategoryId( $arrValues['press_release_category_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['teaser'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTeaser', trim( stripcslashes( $arrValues['teaser'] ) ) ); elseif( isset( $arrValues['teaser'] ) ) $this->setTeaser( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['teaser'] ) : $arrValues['teaser'] );
		if( isset( $arrValues['redirect_url'] ) && $boolDirectSet ) $this->set( 'm_strRedirectUrl', trim( stripcslashes( $arrValues['redirect_url'] ) ) ); elseif( isset( $arrValues['redirect_url'] ) ) $this->setRedirectUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['redirect_url'] ) : $arrValues['redirect_url'] );
		if( isset( $arrValues['body'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBody', trim( stripcslashes( $arrValues['body'] ) ) ); elseif( isset( $arrValues['body'] ) ) $this->setBody( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['body'] ) : $arrValues['body'] );
		if( isset( $arrValues['release_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReleaseDatetime', trim( $arrValues['release_datetime'] ) ); elseif( isset( $arrValues['release_datetime'] ) ) $this->setReleaseDatetime( $arrValues['release_datetime'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPressReleaseCategoryId( $intPressReleaseCategoryId ) {
		$this->set( 'm_intPressReleaseCategoryId', CStrings::strToIntDef( $intPressReleaseCategoryId, NULL, false ) );
	}

	public function getPressReleaseCategoryId() {
		return $this->m_intPressReleaseCategoryId;
	}

	public function sqlPressReleaseCategoryId() {
		return ( true == isset( $this->m_intPressReleaseCategoryId ) ) ? ( string ) $this->m_intPressReleaseCategoryId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 2000, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setTeaser( $strTeaser, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTeaser', CStrings::strTrimDef( $strTeaser, 2000, NULL, true ), $strLocaleCode );
	}

	public function getTeaser( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTeaser', $strLocaleCode );
	}

	public function sqlTeaser() {
		return ( true == isset( $this->m_strTeaser ) ) ? '\'' . addslashes( $this->m_strTeaser ) . '\'' : 'NULL';
	}

	public function setRedirectUrl( $strRedirectUrl ) {
		$this->set( 'm_strRedirectUrl', CStrings::strTrimDef( $strRedirectUrl, 4096, NULL, true ) );
	}

	public function getRedirectUrl() {
		return $this->m_strRedirectUrl;
	}

	public function sqlRedirectUrl() {
		return ( true == isset( $this->m_strRedirectUrl ) ) ? '\'' . addslashes( $this->m_strRedirectUrl ) . '\'' : 'NULL';
	}

	public function setBody( $strBody, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBody', CStrings::strTrimDef( $strBody, -1, NULL, true ), $strLocaleCode );
	}

	public function getBody( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBody', $strLocaleCode );
	}

	public function sqlBody() {
		return ( true == isset( $this->m_strBody ) ) ? '\'' . addslashes( $this->m_strBody ) . '\'' : 'NULL';
	}

	public function setReleaseDatetime( $strReleaseDatetime ) {
		$this->set( 'm_strReleaseDatetime', CStrings::strTrimDef( $strReleaseDatetime, -1, NULL, true ) );
	}

	public function getReleaseDatetime() {
		return $this->m_strReleaseDatetime;
	}

	public function sqlReleaseDatetime() {
		return ( true == isset( $this->m_strReleaseDatetime ) ) ? '\'' . $this->m_strReleaseDatetime . '\'' : 'NOW()';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, press_release_category_id, title, teaser, redirect_url, body, release_datetime, is_published, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPressReleaseCategoryId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlTeaser() . ', ' .
						$this->sqlRedirectUrl() . ', ' .
						$this->sqlBody() . ', ' .
						$this->sqlReleaseDatetime() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' press_release_category_id = ' . $this->sqlPressReleaseCategoryId(). ',' ; } elseif( true == array_key_exists( 'PressReleaseCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' press_release_category_id = ' . $this->sqlPressReleaseCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' teaser = ' . $this->sqlTeaser(). ',' ; } elseif( true == array_key_exists( 'Teaser', $this->getChangedColumns() ) ) { $strSql .= ' teaser = ' . $this->sqlTeaser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl(). ',' ; } elseif( true == array_key_exists( 'RedirectUrl', $this->getChangedColumns() ) ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' body = ' . $this->sqlBody(). ',' ; } elseif( true == array_key_exists( 'Body', $this->getChangedColumns() ) ) { $strSql .= ' body = ' . $this->sqlBody() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime(). ',' ; } elseif( true == array_key_exists( 'ReleaseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'press_release_category_id' => $this->getPressReleaseCategoryId(),
			'title' => $this->getTitle(),
			'teaser' => $this->getTeaser(),
			'redirect_url' => $this->getRedirectUrl(),
			'body' => $this->getBody(),
			'release_datetime' => $this->getReleaseDatetime(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>