<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeSubAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeSubAccounts extends CEosPluralBase {

	/**
	 * @return CApPayeeSubAccount[]
	 */
	public static function fetchApPayeeSubAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayeeSubAccount', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeSubAccount
	 */
	public static function fetchApPayeeSubAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayeeSubAccount', $objDatabase );
	}

	public static function fetchApPayeeSubAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_sub_accounts', $objDatabase );
	}

	public static function fetchApPayeeSubAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccount( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeSubAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccounts( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeSubAccountsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccounts( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeSubAccountsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccounts( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeSubAccountsByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccounts( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE ap_payee_account_id = %d AND cid = %d', ( int ) $intApPayeeAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeSubAccountsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeSubAccounts( sprintf( 'SELECT * FROM ap_payee_sub_accounts WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>