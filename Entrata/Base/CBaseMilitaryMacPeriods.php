<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryMacPeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMilitaryMacPeriods extends CEosPluralBase {

	/**
	 * @return CMilitaryMacPeriod[]
	 */
	public static function fetchMilitaryMacPeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMilitaryMacPeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMilitaryMacPeriod
	 */
	public static function fetchMilitaryMacPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMilitaryMacPeriod', $objDatabase );
	}

	public static function fetchMilitaryMacPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_mac_periods', $objDatabase );
	}

	public static function fetchMilitaryMacPeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMilitaryMacPeriod( sprintf( 'SELECT * FROM military_mac_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMilitaryMacPeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchMilitaryMacPeriods( sprintf( 'SELECT * FROM military_mac_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMilitaryMacPeriodsByMilitaryInstallationIdByCid( $intMilitaryInstallationId, $intCid, $objDatabase ) {
		return self::fetchMilitaryMacPeriods( sprintf( 'SELECT * FROM military_mac_periods WHERE military_installation_id = %d AND cid = %d', ( int ) $intMilitaryInstallationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMilitaryMacPeriodsByMilitaryMacPeriodStatusIdByCid( $intMilitaryMacPeriodStatusId, $intCid, $objDatabase ) {
		return self::fetchMilitaryMacPeriods( sprintf( 'SELECT * FROM military_mac_periods WHERE military_mac_period_status_id = %d AND cid = %d', ( int ) $intMilitaryMacPeriodStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>