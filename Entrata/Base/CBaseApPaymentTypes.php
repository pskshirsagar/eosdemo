<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentTypes
 * Do not add any new functions to this class.
 */

class CBaseApPaymentTypes extends CEosPluralBase {

	/**
	 * @return CApPaymentType[]
	 */
	public static function fetchApPaymentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApPaymentType::class, $objDatabase );
	}

	/**
	 * @return CApPaymentType
	 */
	public static function fetchApPaymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPaymentType::class, $objDatabase );
	}

	public static function fetchApPaymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payment_types', $objDatabase );
	}

	public static function fetchApPaymentTypeById( $intId, $objDatabase ) {
		return self::fetchApPaymentType( sprintf( 'SELECT * FROM ap_payment_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>