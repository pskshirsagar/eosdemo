<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.budget_tab_gl_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetId;
	protected $m_intBudgetTabId;
	protected $m_intGlAccountId;
	protected $m_intBudgetDataSourceTypeId;
	protected $m_intBudgetDataSourceSubTypeId;
	protected $m_strFormula;
	protected $m_fltAdjustmentPercent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAdjustmentPercent = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['budget_tab_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetTabId', trim( $arrValues['budget_tab_id'] ) ); elseif( isset( $arrValues['budget_tab_id'] ) ) $this->setBudgetTabId( $arrValues['budget_tab_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['budget_data_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceTypeId', trim( $arrValues['budget_data_source_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_type_id'] ) ) $this->setBudgetDataSourceTypeId( $arrValues['budget_data_source_type_id'] );
		if( isset( $arrValues['budget_data_source_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceSubTypeId', trim( $arrValues['budget_data_source_sub_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_sub_type_id'] ) ) $this->setBudgetDataSourceSubTypeId( $arrValues['budget_data_source_sub_type_id'] );
		if( isset( $arrValues['formula'] ) && $boolDirectSet ) $this->set( 'm_strFormula', trim( stripcslashes( $arrValues['formula'] ) ) ); elseif( isset( $arrValues['formula'] ) ) $this->setFormula( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['formula'] ) : $arrValues['formula'] );
		if( isset( $arrValues['adjustment_percent'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentPercent', trim( $arrValues['adjustment_percent'] ) ); elseif( isset( $arrValues['adjustment_percent'] ) ) $this->setAdjustmentPercent( $arrValues['adjustment_percent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setBudgetTabId( $intBudgetTabId ) {
		$this->set( 'm_intBudgetTabId', CStrings::strToIntDef( $intBudgetTabId, NULL, false ) );
	}

	public function getBudgetTabId() {
		return $this->m_intBudgetTabId;
	}

	public function sqlBudgetTabId() {
		return ( true == isset( $this->m_intBudgetTabId ) ) ? ( string ) $this->m_intBudgetTabId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->set( 'm_intBudgetDataSourceTypeId', CStrings::strToIntDef( $intBudgetDataSourceTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function sqlBudgetDataSourceTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceTypeId : 'NULL';
	}

	public function setBudgetDataSourceSubTypeId( $intBudgetDataSourceSubTypeId ) {
		$this->set( 'm_intBudgetDataSourceSubTypeId', CStrings::strToIntDef( $intBudgetDataSourceSubTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceSubTypeId() {
		return $this->m_intBudgetDataSourceSubTypeId;
	}

	public function sqlBudgetDataSourceSubTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceSubTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceSubTypeId : 'NULL';
	}

	public function setFormula( $strFormula ) {
		$this->set( 'm_strFormula', CStrings::strTrimDef( $strFormula, -1, NULL, true ) );
	}

	public function getFormula() {
		return $this->m_strFormula;
	}

	public function sqlFormula() {
		return ( true == isset( $this->m_strFormula ) ) ? '\'' . addslashes( $this->m_strFormula ) . '\'' : 'NULL';
	}

	public function setAdjustmentPercent( $fltAdjustmentPercent ) {
		$this->set( 'm_fltAdjustmentPercent', CStrings::strToFloatDef( $fltAdjustmentPercent, NULL, false, 4 ) );
	}

	public function getAdjustmentPercent() {
		return $this->m_fltAdjustmentPercent;
	}

	public function sqlAdjustmentPercent() {
		return ( true == isset( $this->m_fltAdjustmentPercent ) ) ? ( string ) $this->m_fltAdjustmentPercent : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_id, budget_tab_id, gl_account_id, budget_data_source_type_id, budget_data_source_sub_type_id, formula, adjustment_percent, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetId() . ', ' .
						$this->sqlBudgetTabId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlBudgetDataSourceTypeId() . ', ' .
						$this->sqlBudgetDataSourceSubTypeId() . ', ' .
						$this->sqlFormula() . ', ' .
						$this->sqlAdjustmentPercent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_tab_id = ' . $this->sqlBudgetTabId() . ','; } elseif( true == array_key_exists( 'BudgetTabId', $this->getChangedColumns() ) ) { $strSql .= ' budget_tab_id = ' . $this->sqlBudgetTabId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; } elseif( true == array_key_exists( 'BudgetDataSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId() . ','; } elseif( true == array_key_exists( 'BudgetDataSourceSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' formula = ' . $this->sqlFormula() . ','; } elseif( true == array_key_exists( 'Formula', $this->getChangedColumns() ) ) { $strSql .= ' formula = ' . $this->sqlFormula() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent() . ','; } elseif( true == array_key_exists( 'AdjustmentPercent', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_id' => $this->getBudgetId(),
			'budget_tab_id' => $this->getBudgetTabId(),
			'gl_account_id' => $this->getGlAccountId(),
			'budget_data_source_type_id' => $this->getBudgetDataSourceTypeId(),
			'budget_data_source_sub_type_id' => $this->getBudgetDataSourceSubTypeId(),
			'formula' => $this->getFormula(),
			'adjustment_percent' => $this->getAdjustmentPercent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>