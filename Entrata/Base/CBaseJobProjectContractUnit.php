<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjectContractUnit extends CEosSingularBase {

	const TABLE_NAME = 'public.job_project_contract_units';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobProjectId;
	protected $m_intPropertyUnitId;
	protected $m_intContractId;
	protected $m_intApCodeId;
	protected $m_intMaintenanceRequestCount;
	protected $m_intAmenityCount;
	protected $m_intPoCount;
	protected $m_intInvoiceCount;
	protected $m_intInvoiceDrawCount;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMaintenanceRequestCount = '0';
		$this->m_intAmenityCount = '0';
		$this->m_intPoCount = '0';
		$this->m_intInvoiceCount = '0';
		$this->m_intInvoiceDrawCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_project_id'] ) && $boolDirectSet ) $this->set( 'm_intJobProjectId', trim( $arrValues['job_project_id'] ) ); elseif( isset( $arrValues['job_project_id'] ) ) $this->setJobProjectId( $arrValues['job_project_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['maintenance_request_count'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestCount', trim( $arrValues['maintenance_request_count'] ) ); elseif( isset( $arrValues['maintenance_request_count'] ) ) $this->setMaintenanceRequestCount( $arrValues['maintenance_request_count'] );
		if( isset( $arrValues['amenity_count'] ) && $boolDirectSet ) $this->set( 'm_intAmenityCount', trim( $arrValues['amenity_count'] ) ); elseif( isset( $arrValues['amenity_count'] ) ) $this->setAmenityCount( $arrValues['amenity_count'] );
		if( isset( $arrValues['po_count'] ) && $boolDirectSet ) $this->set( 'm_intPoCount', trim( $arrValues['po_count'] ) ); elseif( isset( $arrValues['po_count'] ) ) $this->setPoCount( $arrValues['po_count'] );
		if( isset( $arrValues['invoice_count'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceCount', trim( $arrValues['invoice_count'] ) ); elseif( isset( $arrValues['invoice_count'] ) ) $this->setInvoiceCount( $arrValues['invoice_count'] );
		if( isset( $arrValues['invoice_draw_count'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDrawCount', trim( $arrValues['invoice_draw_count'] ) ); elseif( isset( $arrValues['invoice_draw_count'] ) ) $this->setInvoiceDrawCount( $arrValues['invoice_draw_count'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobProjectId( $intJobProjectId ) {
		$this->set( 'm_intJobProjectId', CStrings::strToIntDef( $intJobProjectId, NULL, false ) );
	}

	public function getJobProjectId() {
		return $this->m_intJobProjectId;
	}

	public function sqlJobProjectId() {
		return ( true == isset( $this->m_intJobProjectId ) ) ? ( string ) $this->m_intJobProjectId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setMaintenanceRequestCount( $intMaintenanceRequestCount ) {
		$this->set( 'm_intMaintenanceRequestCount', CStrings::strToIntDef( $intMaintenanceRequestCount, NULL, false ) );
	}

	public function getMaintenanceRequestCount() {
		return $this->m_intMaintenanceRequestCount;
	}

	public function sqlMaintenanceRequestCount() {
		return ( true == isset( $this->m_intMaintenanceRequestCount ) ) ? ( string ) $this->m_intMaintenanceRequestCount : '0';
	}

	public function setAmenityCount( $intAmenityCount ) {
		$this->set( 'm_intAmenityCount', CStrings::strToIntDef( $intAmenityCount, NULL, false ) );
	}

	public function getAmenityCount() {
		return $this->m_intAmenityCount;
	}

	public function sqlAmenityCount() {
		return ( true == isset( $this->m_intAmenityCount ) ) ? ( string ) $this->m_intAmenityCount : '0';
	}

	public function setPoCount( $intPoCount ) {
		$this->set( 'm_intPoCount', CStrings::strToIntDef( $intPoCount, NULL, false ) );
	}

	public function getPoCount() {
		return $this->m_intPoCount;
	}

	public function sqlPoCount() {
		return ( true == isset( $this->m_intPoCount ) ) ? ( string ) $this->m_intPoCount : '0';
	}

	public function setInvoiceCount( $intInvoiceCount ) {
		$this->set( 'm_intInvoiceCount', CStrings::strToIntDef( $intInvoiceCount, NULL, false ) );
	}

	public function getInvoiceCount() {
		return $this->m_intInvoiceCount;
	}

	public function sqlInvoiceCount() {
		return ( true == isset( $this->m_intInvoiceCount ) ) ? ( string ) $this->m_intInvoiceCount : '0';
	}

	public function setInvoiceDrawCount( $intInvoiceDrawCount ) {
		$this->set( 'm_intInvoiceDrawCount', CStrings::strToIntDef( $intInvoiceDrawCount, NULL, false ) );
	}

	public function getInvoiceDrawCount() {
		return $this->m_intInvoiceDrawCount;
	}

	public function sqlInvoiceDrawCount() {
		return ( true == isset( $this->m_intInvoiceDrawCount ) ) ? ( string ) $this->m_intInvoiceDrawCount : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_project_id, property_unit_id, contract_id, ap_code_id, maintenance_request_count, amenity_count, po_count, invoice_count, invoice_draw_count, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlJobProjectId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlApCodeId() . ', ' .
 						$this->sqlMaintenanceRequestCount() . ', ' .
 						$this->sqlAmenityCount() . ', ' .
 						$this->sqlPoCount() . ', ' .
 						$this->sqlInvoiceCount() . ', ' .
 						$this->sqlInvoiceDrawCount() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_project_id = ' . $this->sqlJobProjectId() . ','; } elseif( true == array_key_exists( 'JobProjectId', $this->getChangedColumns() ) ) { $strSql .= ' job_project_id = ' . $this->sqlJobProjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_count = ' . $this->sqlMaintenanceRequestCount() . ','; } elseif( true == array_key_exists( 'MaintenanceRequestCount', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_count = ' . $this->sqlMaintenanceRequestCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_count = ' . $this->sqlAmenityCount() . ','; } elseif( true == array_key_exists( 'AmenityCount', $this->getChangedColumns() ) ) { $strSql .= ' amenity_count = ' . $this->sqlAmenityCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_count = ' . $this->sqlPoCount() . ','; } elseif( true == array_key_exists( 'PoCount', $this->getChangedColumns() ) ) { $strSql .= ' po_count = ' . $this->sqlPoCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_count = ' . $this->sqlInvoiceCount() . ','; } elseif( true == array_key_exists( 'InvoiceCount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_count = ' . $this->sqlInvoiceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_draw_count = ' . $this->sqlInvoiceDrawCount() . ','; } elseif( true == array_key_exists( 'InvoiceDrawCount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_draw_count = ' . $this->sqlInvoiceDrawCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_project_id' => $this->getJobProjectId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'contract_id' => $this->getContractId(),
			'ap_code_id' => $this->getApCodeId(),
			'maintenance_request_count' => $this->getMaintenanceRequestCount(),
			'amenity_count' => $this->getAmenityCount(),
			'po_count' => $this->getPoCount(),
			'invoice_count' => $this->getInvoiceCount(),
			'invoice_draw_count' => $this->getInvoiceDrawCount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>