<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccounts extends CEosPluralBase {

	/**
	 * @return CGlAccount[]
	 */
	public static function fetchGlAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlAccount', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlAccount
	 */
	public static function fetchGlAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlAccount', $objDatabase );
	}

	public static function fetchGlAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_accounts', $objDatabase );
	}

	public static function fetchGlAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlAccount( sprintf( 'SELECT * FROM gl_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByGlChartIdByCid( $intGlChartId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE gl_chart_id = %d AND cid = %d', ( int ) $intGlChartId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByGlAccountTypeIdByCid( $intGlAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE gl_account_type_id = %d AND cid = %d', ( int ) $intGlAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByDefaultGlAccountIdByCid( $intDefaultGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE default_gl_account_id = %d AND cid = %d', ( int ) $intDefaultGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByCashFlowTypeIdByCid( $intCashFlowTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE cash_flow_type_id = %d AND cid = %d', ( int ) $intCashFlowTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByGlAccountUsageTypeIdByCid( $intGlAccountUsageTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE gl_account_usage_type_id = %d AND cid = %d', ( int ) $intGlAccountUsageTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByCamAllowanceTypeIdByCid( $intCamAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE cam_allowance_type_id = %d AND cid = %d', ( int ) $intCamAllowanceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountsByCategorizationTypeIdByCid( $intCategorizationTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccounts( sprintf( 'SELECT * FROM gl_accounts WHERE categorization_type_id = %d AND cid = %d', ( int ) $intCategorizationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>