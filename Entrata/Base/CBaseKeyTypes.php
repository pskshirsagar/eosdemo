<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CKeyTypes
 * Do not add any new functions to this class.
 */

class CBaseKeyTypes extends CEosPluralBase {

	/**
	 * @return CKeyType[]
	 */
	public static function fetchKeyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CKeyType::class, $objDatabase );
	}

	/**
	 * @return CKeyType
	 */
	public static function fetchKeyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CKeyType::class, $objDatabase );
	}

	public static function fetchKeyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'key_types', $objDatabase );
	}

	public static function fetchKeyTypeById( $intId, $objDatabase ) {
		return self::fetchKeyType( sprintf( 'SELECT * FROM key_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>