<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileMergeValue extends CEosSingularBase {

	const TABLE_NAME = 'public.file_merge_values';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intLeaseId;
	protected $m_intDocumentId;
	protected $m_intDocumentAddendaId;
	protected $m_intFileId;
	protected $m_intBlockFileMergeValueId;
	protected $m_strField;
	protected $m_strValue;
	protected $m_intRow;
	protected $m_intIsBlockField;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsBlockField = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['document_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentAddendaId', trim( $arrValues['document_addenda_id'] ) ); elseif( isset( $arrValues['document_addenda_id'] ) ) $this->setDocumentAddendaId( $arrValues['document_addenda_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['block_file_merge_value_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockFileMergeValueId', trim( $arrValues['block_file_merge_value_id'] ) ); elseif( isset( $arrValues['block_file_merge_value_id'] ) ) $this->setBlockFileMergeValueId( $arrValues['block_file_merge_value_id'] );
		if( isset( $arrValues['field'] ) && $boolDirectSet ) $this->set( 'm_strField', trim( stripcslashes( $arrValues['field'] ) ) ); elseif( isset( $arrValues['field'] ) ) $this->setField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['field'] ) : $arrValues['field'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( stripcslashes( $arrValues['value'] ) ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
		if( isset( $arrValues['row'] ) && $boolDirectSet ) $this->set( 'm_intRow', trim( $arrValues['row'] ) ); elseif( isset( $arrValues['row'] ) ) $this->setRow( $arrValues['row'] );
		if( isset( $arrValues['is_block_field'] ) && $boolDirectSet ) $this->set( 'm_intIsBlockField', trim( $arrValues['is_block_field'] ) ); elseif( isset( $arrValues['is_block_field'] ) ) $this->setIsBlockField( $arrValues['is_block_field'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDocumentAddendaId( $intDocumentAddendaId ) {
		$this->set( 'm_intDocumentAddendaId', CStrings::strToIntDef( $intDocumentAddendaId, NULL, false ) );
	}

	public function getDocumentAddendaId() {
		return $this->m_intDocumentAddendaId;
	}

	public function sqlDocumentAddendaId() {
		return ( true == isset( $this->m_intDocumentAddendaId ) ) ? ( string ) $this->m_intDocumentAddendaId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setBlockFileMergeValueId( $intBlockFileMergeValueId ) {
		$this->set( 'm_intBlockFileMergeValueId', CStrings::strToIntDef( $intBlockFileMergeValueId, NULL, false ) );
	}

	public function getBlockFileMergeValueId() {
		return $this->m_intBlockFileMergeValueId;
	}

	public function sqlBlockFileMergeValueId() {
		return ( true == isset( $this->m_intBlockFileMergeValueId ) ) ? ( string ) $this->m_intBlockFileMergeValueId : 'NULL';
	}

	public function setField( $strField ) {
		$this->set( 'm_strField', CStrings::strTrimDef( $strField, 64, NULL, true ) );
	}

	public function getField() {
		return $this->m_strField;
	}

	public function sqlField() {
		return ( true == isset( $this->m_strField ) ) ? '\'' . addslashes( $this->m_strField ) . '\'' : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
	}

	public function setRow( $intRow ) {
		$this->set( 'm_intRow', CStrings::strToIntDef( $intRow, NULL, false ) );
	}

	public function getRow() {
		return $this->m_intRow;
	}

	public function sqlRow() {
		return ( true == isset( $this->m_intRow ) ) ? ( string ) $this->m_intRow : 'NULL';
	}

	public function setIsBlockField( $intIsBlockField ) {
		$this->set( 'm_intIsBlockField', CStrings::strToIntDef( $intIsBlockField, NULL, false ) );
	}

	public function getIsBlockField() {
		return $this->m_intIsBlockField;
	}

	public function sqlIsBlockField() {
		return ( true == isset( $this->m_intIsBlockField ) ) ? ( string ) $this->m_intIsBlockField : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, lease_id, document_id, document_addenda_id, file_id, block_file_merge_value_id, field, value, row, is_block_field, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlDocumentAddendaId() . ', ' .
 						$this->sqlFileId() . ', ' .
 						$this->sqlBlockFileMergeValueId() . ', ' .
 						$this->sqlField() . ', ' .
 						$this->sqlValue() . ', ' .
 						$this->sqlRow() . ', ' .
 						$this->sqlIsBlockField() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId() . ','; } elseif( true == array_key_exists( 'DocumentAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_file_merge_value_id = ' . $this->sqlBlockFileMergeValueId() . ','; } elseif( true == array_key_exists( 'BlockFileMergeValueId', $this->getChangedColumns() ) ) { $strSql .= ' block_file_merge_value_id = ' . $this->sqlBlockFileMergeValueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field = ' . $this->sqlField() . ','; } elseif( true == array_key_exists( 'Field', $this->getChangedColumns() ) ) { $strSql .= ' field = ' . $this->sqlField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' row = ' . $this->sqlRow() . ','; } elseif( true == array_key_exists( 'Row', $this->getChangedColumns() ) ) { $strSql .= ' row = ' . $this->sqlRow() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField() . ','; } elseif( true == array_key_exists( 'IsBlockField', $this->getChangedColumns() ) ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'lease_id' => $this->getLeaseId(),
			'document_id' => $this->getDocumentId(),
			'document_addenda_id' => $this->getDocumentAddendaId(),
			'file_id' => $this->getFileId(),
			'block_file_merge_value_id' => $this->getBlockFileMergeValueId(),
			'field' => $this->getField(),
			'value' => $this->getValue(),
			'row' => $this->getRow(),
			'is_block_field' => $this->getIsBlockField(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>