<?php

class CBaseDefaultBadge extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_badges';

	protected $m_intId;
	protected $m_intDefaultActivityPointId;
	protected $m_strFileHandle;
	protected $m_strName;
	protected $m_strBaseDescription;
	protected $m_strProDescription;
	protected $m_intBaseCriteria;
	protected $m_intProCriteria;
	protected $m_intIsPenalty;
	protected $m_intDisplayToAll;
	protected $m_intDisplayToCustomer;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPenalty = '0';
		$this->m_intDisplayToCustomer = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_activity_point_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultActivityPointId', trim( $arrValues['default_activity_point_id'] ) ); elseif( isset( $arrValues['default_activity_point_id'] ) ) $this->setDefaultActivityPointId( $arrValues['default_activity_point_id'] );
		if( isset( $arrValues['file_handle'] ) && $boolDirectSet ) $this->set( 'm_strFileHandle', trim( stripcslashes( $arrValues['file_handle'] ) ) ); elseif( isset( $arrValues['file_handle'] ) ) $this->setFileHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_handle'] ) : $arrValues['file_handle'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['base_description'] ) && $boolDirectSet ) $this->set( 'm_strBaseDescription', trim( stripcslashes( $arrValues['base_description'] ) ) ); elseif( isset( $arrValues['base_description'] ) ) $this->setBaseDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_description'] ) : $arrValues['base_description'] );
		if( isset( $arrValues['pro_description'] ) && $boolDirectSet ) $this->set( 'm_strProDescription', trim( stripcslashes( $arrValues['pro_description'] ) ) ); elseif( isset( $arrValues['pro_description'] ) ) $this->setProDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pro_description'] ) : $arrValues['pro_description'] );
		if( isset( $arrValues['base_criteria'] ) && $boolDirectSet ) $this->set( 'm_intBaseCriteria', trim( $arrValues['base_criteria'] ) ); elseif( isset( $arrValues['base_criteria'] ) ) $this->setBaseCriteria( $arrValues['base_criteria'] );
		if( isset( $arrValues['pro_criteria'] ) && $boolDirectSet ) $this->set( 'm_intProCriteria', trim( $arrValues['pro_criteria'] ) ); elseif( isset( $arrValues['pro_criteria'] ) ) $this->setProCriteria( $arrValues['pro_criteria'] );
		if( isset( $arrValues['is_penalty'] ) && $boolDirectSet ) $this->set( 'm_intIsPenalty', trim( $arrValues['is_penalty'] ) ); elseif( isset( $arrValues['is_penalty'] ) ) $this->setIsPenalty( $arrValues['is_penalty'] );
		if( isset( $arrValues['display_to_all'] ) && $boolDirectSet ) $this->set( 'm_intDisplayToAll', trim( $arrValues['display_to_all'] ) ); elseif( isset( $arrValues['display_to_all'] ) ) $this->setDisplayToAll( $arrValues['display_to_all'] );
		if( isset( $arrValues['display_to_customer'] ) && $boolDirectSet ) $this->set( 'm_intDisplayToCustomer', trim( $arrValues['display_to_customer'] ) ); elseif( isset( $arrValues['display_to_customer'] ) ) $this->setDisplayToCustomer( $arrValues['display_to_customer'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultActivityPointId( $intDefaultActivityPointId ) {
		$this->set( 'm_intDefaultActivityPointId', CStrings::strToIntDef( $intDefaultActivityPointId, NULL, false ) );
	}

	public function getDefaultActivityPointId() {
		return $this->m_intDefaultActivityPointId;
	}

	public function sqlDefaultActivityPointId() {
		return ( true == isset( $this->m_intDefaultActivityPointId ) ) ? ( string ) $this->m_intDefaultActivityPointId : 'NULL';
	}

	public function setFileHandle( $strFileHandle ) {
		$this->set( 'm_strFileHandle', CStrings::strTrimDef( $strFileHandle, 50, NULL, true ) );
	}

	public function getFileHandle() {
		return $this->m_strFileHandle;
	}

	public function sqlFileHandle() {
		return ( true == isset( $this->m_strFileHandle ) ) ? '\'' . addslashes( $this->m_strFileHandle ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBaseDescription( $strBaseDescription ) {
		$this->set( 'm_strBaseDescription', CStrings::strTrimDef( $strBaseDescription, -1, NULL, true ) );
	}

	public function getBaseDescription() {
		return $this->m_strBaseDescription;
	}

	public function sqlBaseDescription() {
		return ( true == isset( $this->m_strBaseDescription ) ) ? '\'' . addslashes( $this->m_strBaseDescription ) . '\'' : 'NULL';
	}

	public function setProDescription( $strProDescription ) {
		$this->set( 'm_strProDescription', CStrings::strTrimDef( $strProDescription, -1, NULL, true ) );
	}

	public function getProDescription() {
		return $this->m_strProDescription;
	}

	public function sqlProDescription() {
		return ( true == isset( $this->m_strProDescription ) ) ? '\'' . addslashes( $this->m_strProDescription ) . '\'' : 'NULL';
	}

	public function setBaseCriteria( $intBaseCriteria ) {
		$this->set( 'm_intBaseCriteria', CStrings::strToIntDef( $intBaseCriteria, NULL, false ) );
	}

	public function getBaseCriteria() {
		return $this->m_intBaseCriteria;
	}

	public function sqlBaseCriteria() {
		return ( true == isset( $this->m_intBaseCriteria ) ) ? ( string ) $this->m_intBaseCriteria : 'NULL';
	}

	public function setProCriteria( $intProCriteria ) {
		$this->set( 'm_intProCriteria', CStrings::strToIntDef( $intProCriteria, NULL, false ) );
	}

	public function getProCriteria() {
		return $this->m_intProCriteria;
	}

	public function sqlProCriteria() {
		return ( true == isset( $this->m_intProCriteria ) ) ? ( string ) $this->m_intProCriteria : 'NULL';
	}

	public function setIsPenalty( $intIsPenalty ) {
		$this->set( 'm_intIsPenalty', CStrings::strToIntDef( $intIsPenalty, NULL, false ) );
	}

	public function getIsPenalty() {
		return $this->m_intIsPenalty;
	}

	public function sqlIsPenalty() {
		return ( true == isset( $this->m_intIsPenalty ) ) ? ( string ) $this->m_intIsPenalty : '0';
	}

	public function setDisplayToAll( $intDisplayToAll ) {
		$this->set( 'm_intDisplayToAll', CStrings::strToIntDef( $intDisplayToAll, NULL, false ) );
	}

	public function getDisplayToAll() {
		return $this->m_intDisplayToAll;
	}

	public function sqlDisplayToAll() {
		return ( true == isset( $this->m_intDisplayToAll ) ) ? ( string ) $this->m_intDisplayToAll : 'NULL';
	}

	public function setDisplayToCustomer( $intDisplayToCustomer ) {
		$this->set( 'm_intDisplayToCustomer', CStrings::strToIntDef( $intDisplayToCustomer, NULL, false ) );
	}

	public function getDisplayToCustomer() {
		return $this->m_intDisplayToCustomer;
	}

	public function sqlDisplayToCustomer() {
		return ( true == isset( $this->m_intDisplayToCustomer ) ) ? ( string ) $this->m_intDisplayToCustomer : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_activity_point_id' => $this->getDefaultActivityPointId(),
			'file_handle' => $this->getFileHandle(),
			'name' => $this->getName(),
			'base_description' => $this->getBaseDescription(),
			'pro_description' => $this->getProDescription(),
			'base_criteria' => $this->getBaseCriteria(),
			'pro_criteria' => $this->getProCriteria(),
			'is_penalty' => $this->getIsPenalty(),
			'display_to_all' => $this->getDisplayToAll(),
			'display_to_customer' => $this->getDisplayToCustomer(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>