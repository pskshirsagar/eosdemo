<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentAddenda extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.document_addendas';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDocumentId;
	protected $m_intMasterDocumentId;
	protected $m_intCompanyApplicationId;
	protected $m_strExternalKey;
	protected $m_strRemotePrimaryKey;
	protected $m_intRequireConfirmation;
	protected $m_intAttachToEmail;
	protected $m_intIsForPrimaryApplicant;
	protected $m_intIsForCoApplicant;
	protected $m_intIsForCoSigner;
	protected $m_intIsOptional;
	protected $m_boolIsAutoGenerate;
	protected $m_intIsDefaultSelected;
	protected $m_intIsHousehold;
	protected $m_intIsPublished;
	protected $m_boolIsHidden;
	protected $m_boolShowInPortal;
	protected $m_intRequireSign;
	protected $m_boolRequireCountersign;
	protected $m_arrintCountersignCompanyGroupIds;
	protected $m_intExcludeOtherSignersInfo;
	protected $m_intOrderNum;
	protected $m_strActivatedOn;
	protected $m_strDeactivatedOn;
	protected $m_strLastSyncOn;
	protected $m_strArchivedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intRequireConfirmation = '1';
		$this->m_intAttachToEmail = '1';
		$this->m_intIsForPrimaryApplicant = '1';
		$this->m_intIsForCoApplicant = '1';
		$this->m_intIsForCoSigner = '1';
		$this->m_intIsOptional = '0';
		$this->m_boolIsAutoGenerate = false;
		$this->m_intIsDefaultSelected = '0';
		$this->m_intIsHousehold = '1';
		$this->m_intIsPublished = '1';
		$this->m_boolIsHidden = false;
		$this->m_boolShowInPortal = false;
		$this->m_intRequireSign = '0';
		$this->m_boolRequireCountersign = false;
		$this->m_intExcludeOtherSignersInfo = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['master_document_id'] ) && $boolDirectSet ) $this->set( 'm_intMasterDocumentId', trim( $arrValues['master_document_id'] ) ); elseif( isset( $arrValues['master_document_id'] ) ) $this->setMasterDocumentId( $arrValues['master_document_id'] );
		if( isset( $arrValues['company_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyApplicationId', trim( $arrValues['company_application_id'] ) ); elseif( isset( $arrValues['company_application_id'] ) ) $this->setCompanyApplicationId( $arrValues['company_application_id'] );
		if( isset( $arrValues['external_key'] ) && $boolDirectSet ) $this->set( 'm_strExternalKey', trim( stripcslashes( $arrValues['external_key'] ) ) ); elseif( isset( $arrValues['external_key'] ) ) $this->setExternalKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_key'] ) : $arrValues['external_key'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['require_confirmation'] ) && $boolDirectSet ) $this->set( 'm_intRequireConfirmation', trim( $arrValues['require_confirmation'] ) ); elseif( isset( $arrValues['require_confirmation'] ) ) $this->setRequireConfirmation( $arrValues['require_confirmation'] );
		if( isset( $arrValues['attach_to_email'] ) && $boolDirectSet ) $this->set( 'm_intAttachToEmail', trim( $arrValues['attach_to_email'] ) ); elseif( isset( $arrValues['attach_to_email'] ) ) $this->setAttachToEmail( $arrValues['attach_to_email'] );
		if( isset( $arrValues['is_for_primary_applicant'] ) && $boolDirectSet ) $this->set( 'm_intIsForPrimaryApplicant', trim( $arrValues['is_for_primary_applicant'] ) ); elseif( isset( $arrValues['is_for_primary_applicant'] ) ) $this->setIsForPrimaryApplicant( $arrValues['is_for_primary_applicant'] );
		if( isset( $arrValues['is_for_co_applicant'] ) && $boolDirectSet ) $this->set( 'm_intIsForCoApplicant', trim( $arrValues['is_for_co_applicant'] ) ); elseif( isset( $arrValues['is_for_co_applicant'] ) ) $this->setIsForCoApplicant( $arrValues['is_for_co_applicant'] );
		if( isset( $arrValues['is_for_co_signer'] ) && $boolDirectSet ) $this->set( 'm_intIsForCoSigner', trim( $arrValues['is_for_co_signer'] ) ); elseif( isset( $arrValues['is_for_co_signer'] ) ) $this->setIsForCoSigner( $arrValues['is_for_co_signer'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_intIsOptional', trim( $arrValues['is_optional'] ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( $arrValues['is_optional'] );
		if( isset( $arrValues['is_auto_generate'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoGenerate', trim( stripcslashes( $arrValues['is_auto_generate'] ) ) ); elseif( isset( $arrValues['is_auto_generate'] ) ) $this->setIsAutoGenerate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_generate'] ) : $arrValues['is_auto_generate'] );
		if( isset( $arrValues['is_default_selected'] ) && $boolDirectSet ) $this->set( 'm_intIsDefaultSelected', trim( $arrValues['is_default_selected'] ) ); elseif( isset( $arrValues['is_default_selected'] ) ) $this->setIsDefaultSelected( $arrValues['is_default_selected'] );
		if( isset( $arrValues['is_household'] ) && $boolDirectSet ) $this->set( 'm_intIsHousehold', trim( $arrValues['is_household'] ) ); elseif( isset( $arrValues['is_household'] ) ) $this->setIsHousehold( $arrValues['is_household'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_boolIsHidden', trim( stripcslashes( $arrValues['is_hidden'] ) ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hidden'] ) : $arrValues['is_hidden'] );
		if( isset( $arrValues['show_in_portal'] ) && $boolDirectSet ) $this->set( 'm_boolShowInPortal', trim( stripcslashes( $arrValues['show_in_portal'] ) ) ); elseif( isset( $arrValues['show_in_portal'] ) ) $this->setShowInPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_portal'] ) : $arrValues['show_in_portal'] );
		if( isset( $arrValues['require_sign'] ) && $boolDirectSet ) $this->set( 'm_intRequireSign', trim( $arrValues['require_sign'] ) ); elseif( isset( $arrValues['require_sign'] ) ) $this->setRequireSign( $arrValues['require_sign'] );
		if( isset( $arrValues['require_countersign'] ) && $boolDirectSet ) $this->set( 'm_boolRequireCountersign', trim( stripcslashes( $arrValues['require_countersign'] ) ) ); elseif( isset( $arrValues['require_countersign'] ) ) $this->setRequireCountersign( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_countersign'] ) : $arrValues['require_countersign'] );
		if( isset( $arrValues['countersign_company_group_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintCountersignCompanyGroupIds', trim( $arrValues['countersign_company_group_ids'] ) ); elseif( isset( $arrValues['countersign_company_group_ids'] ) ) $this->setCountersignCompanyGroupIds( $arrValues['countersign_company_group_ids'] );
		if( isset( $arrValues['exclude_other_signers_info'] ) && $boolDirectSet ) $this->set( 'm_intExcludeOtherSignersInfo', trim( $arrValues['exclude_other_signers_info'] ) ); elseif( isset( $arrValues['exclude_other_signers_info'] ) ) $this->setExcludeOtherSignersInfo( $arrValues['exclude_other_signers_info'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['activated_on'] ) && $boolDirectSet ) $this->set( 'm_strActivatedOn', trim( $arrValues['activated_on'] ) ); elseif( isset( $arrValues['activated_on'] ) ) $this->setActivatedOn( $arrValues['activated_on'] );
		if( isset( $arrValues['deactivated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeactivatedOn', trim( $arrValues['deactivated_on'] ) ); elseif( isset( $arrValues['deactivated_on'] ) ) $this->setDeactivatedOn( $arrValues['deactivated_on'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['archived_on'] ) && $boolDirectSet ) $this->set( 'm_strArchivedOn', trim( $arrValues['archived_on'] ) ); elseif( isset( $arrValues['archived_on'] ) ) $this->setArchivedOn( $arrValues['archived_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setMasterDocumentId( $intMasterDocumentId ) {
		$this->set( 'm_intMasterDocumentId', CStrings::strToIntDef( $intMasterDocumentId, NULL, false ) );
	}

	public function getMasterDocumentId() {
		return $this->m_intMasterDocumentId;
	}

	public function sqlMasterDocumentId() {
		return ( true == isset( $this->m_intMasterDocumentId ) ) ? ( string ) $this->m_intMasterDocumentId : 'NULL';
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->set( 'm_intCompanyApplicationId', CStrings::strToIntDef( $intCompanyApplicationId, NULL, false ) );
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function sqlCompanyApplicationId() {
		return ( true == isset( $this->m_intCompanyApplicationId ) ) ? ( string ) $this->m_intCompanyApplicationId : 'NULL';
	}

	public function setExternalKey( $strExternalKey ) {
		$this->set( 'm_strExternalKey', CStrings::strTrimDef( $strExternalKey, 255, NULL, true ) );
	}

	public function getExternalKey() {
		return $this->m_strExternalKey;
	}

	public function sqlExternalKey() {
		return ( true == isset( $this->m_strExternalKey ) ) ? '\'' . addslashes( $this->m_strExternalKey ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setRequireConfirmation( $intRequireConfirmation ) {
		$this->set( 'm_intRequireConfirmation', CStrings::strToIntDef( $intRequireConfirmation, NULL, false ) );
	}

	public function getRequireConfirmation() {
		return $this->m_intRequireConfirmation;
	}

	public function sqlRequireConfirmation() {
		return ( true == isset( $this->m_intRequireConfirmation ) ) ? ( string ) $this->m_intRequireConfirmation : '1';
	}

	public function setAttachToEmail( $intAttachToEmail ) {
		$this->set( 'm_intAttachToEmail', CStrings::strToIntDef( $intAttachToEmail, NULL, false ) );
	}

	public function getAttachToEmail() {
		return $this->m_intAttachToEmail;
	}

	public function sqlAttachToEmail() {
		return ( true == isset( $this->m_intAttachToEmail ) ) ? ( string ) $this->m_intAttachToEmail : '1';
	}

	public function setIsForPrimaryApplicant( $intIsForPrimaryApplicant ) {
		$this->set( 'm_intIsForPrimaryApplicant', CStrings::strToIntDef( $intIsForPrimaryApplicant, NULL, false ) );
	}

	public function getIsForPrimaryApplicant() {
		return $this->m_intIsForPrimaryApplicant;
	}

	public function sqlIsForPrimaryApplicant() {
		return ( true == isset( $this->m_intIsForPrimaryApplicant ) ) ? ( string ) $this->m_intIsForPrimaryApplicant : '1';
	}

	public function setIsForCoApplicant( $intIsForCoApplicant ) {
		$this->set( 'm_intIsForCoApplicant', CStrings::strToIntDef( $intIsForCoApplicant, NULL, false ) );
	}

	public function getIsForCoApplicant() {
		return $this->m_intIsForCoApplicant;
	}

	public function sqlIsForCoApplicant() {
		return ( true == isset( $this->m_intIsForCoApplicant ) ) ? ( string ) $this->m_intIsForCoApplicant : '1';
	}

	public function setIsForCoSigner( $intIsForCoSigner ) {
		$this->set( 'm_intIsForCoSigner', CStrings::strToIntDef( $intIsForCoSigner, NULL, false ) );
	}

	public function getIsForCoSigner() {
		return $this->m_intIsForCoSigner;
	}

	public function sqlIsForCoSigner() {
		return ( true == isset( $this->m_intIsForCoSigner ) ) ? ( string ) $this->m_intIsForCoSigner : '1';
	}

	public function setIsOptional( $intIsOptional ) {
		$this->set( 'm_intIsOptional', CStrings::strToIntDef( $intIsOptional, NULL, false ) );
	}

	public function getIsOptional() {
		return $this->m_intIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_intIsOptional ) ) ? ( string ) $this->m_intIsOptional : '0';
	}

	public function setIsAutoGenerate( $boolIsAutoGenerate ) {
		$this->set( 'm_boolIsAutoGenerate', CStrings::strToBool( $boolIsAutoGenerate ) );
	}

	public function getIsAutoGenerate() {
		return $this->m_boolIsAutoGenerate;
	}

	public function sqlIsAutoGenerate() {
		return ( true == isset( $this->m_boolIsAutoGenerate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoGenerate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefaultSelected( $intIsDefaultSelected ) {
		$this->set( 'm_intIsDefaultSelected', CStrings::strToIntDef( $intIsDefaultSelected, NULL, false ) );
	}

	public function getIsDefaultSelected() {
		return $this->m_intIsDefaultSelected;
	}

	public function sqlIsDefaultSelected() {
		return ( true == isset( $this->m_intIsDefaultSelected ) ) ? ( string ) $this->m_intIsDefaultSelected : '0';
	}

	public function setIsHousehold( $intIsHousehold ) {
		$this->set( 'm_intIsHousehold', CStrings::strToIntDef( $intIsHousehold, NULL, false ) );
	}

	public function getIsHousehold() {
		return $this->m_intIsHousehold;
	}

	public function sqlIsHousehold() {
		return ( true == isset( $this->m_intIsHousehold ) ) ? ( string ) $this->m_intIsHousehold : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsHidden( $boolIsHidden ) {
		$this->set( 'm_boolIsHidden', CStrings::strToBool( $boolIsHidden ) );
	}

	public function getIsHidden() {
		return $this->m_boolIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_boolIsHidden ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHidden ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInPortal( $boolShowInPortal ) {
		$this->set( 'm_boolShowInPortal', CStrings::strToBool( $boolShowInPortal ) );
	}

	public function getShowInPortal() {
		return $this->m_boolShowInPortal;
	}

	public function sqlShowInPortal() {
		return ( true == isset( $this->m_boolShowInPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireSign( $intRequireSign ) {
		$this->set( 'm_intRequireSign', CStrings::strToIntDef( $intRequireSign, NULL, false ) );
	}

	public function getRequireSign() {
		return $this->m_intRequireSign;
	}

	public function sqlRequireSign() {
		return ( true == isset( $this->m_intRequireSign ) ) ? ( string ) $this->m_intRequireSign : '0';
	}

	public function setRequireCountersign( $boolRequireCountersign ) {
		$this->set( 'm_boolRequireCountersign', CStrings::strToBool( $boolRequireCountersign ) );
	}

	public function getRequireCountersign() {
		return $this->m_boolRequireCountersign;
	}

	public function sqlRequireCountersign() {
		return ( true == isset( $this->m_boolRequireCountersign ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireCountersign ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCountersignCompanyGroupIds( $arrintCountersignCompanyGroupIds ) {
		$this->set( 'm_arrintCountersignCompanyGroupIds', CStrings::strToArrIntDef( $arrintCountersignCompanyGroupIds, NULL ) );
	}

	public function getCountersignCompanyGroupIds() {
		return $this->m_arrintCountersignCompanyGroupIds;
	}

	public function sqlCountersignCompanyGroupIds() {
		return ( true == isset( $this->m_arrintCountersignCompanyGroupIds ) && true == valArr( $this->m_arrintCountersignCompanyGroupIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCountersignCompanyGroupIds, NULL ) . '\'' : 'NULL';
	}

	public function setExcludeOtherSignersInfo( $intExcludeOtherSignersInfo ) {
		$this->set( 'm_intExcludeOtherSignersInfo', CStrings::strToIntDef( $intExcludeOtherSignersInfo, NULL, false ) );
	}

	public function getExcludeOtherSignersInfo() {
		return $this->m_intExcludeOtherSignersInfo;
	}

	public function sqlExcludeOtherSignersInfo() {
		return ( true == isset( $this->m_intExcludeOtherSignersInfo ) ) ? ( string ) $this->m_intExcludeOtherSignersInfo : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setActivatedOn( $strActivatedOn ) {
		$this->set( 'm_strActivatedOn', CStrings::strTrimDef( $strActivatedOn, -1, NULL, true ) );
	}

	public function getActivatedOn() {
		return $this->m_strActivatedOn;
	}

	public function sqlActivatedOn() {
		return ( true == isset( $this->m_strActivatedOn ) ) ? '\'' . $this->m_strActivatedOn . '\'' : 'NULL';
	}

	public function setDeactivatedOn( $strDeactivatedOn ) {
		$this->set( 'm_strDeactivatedOn', CStrings::strTrimDef( $strDeactivatedOn, -1, NULL, true ) );
	}

	public function getDeactivatedOn() {
		return $this->m_strDeactivatedOn;
	}

	public function sqlDeactivatedOn() {
		return ( true == isset( $this->m_strDeactivatedOn ) ) ? '\'' . $this->m_strDeactivatedOn . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setArchivedOn( $strArchivedOn ) {
		$this->set( 'm_strArchivedOn', CStrings::strTrimDef( $strArchivedOn, -1, NULL, true ) );
	}

	public function getArchivedOn() {
		return $this->m_strArchivedOn;
	}

	public function sqlArchivedOn() {
		return ( true == isset( $this->m_strArchivedOn ) ) ? '\'' . $this->m_strArchivedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, document_id, master_document_id, company_application_id, external_key, remote_primary_key, require_confirmation, attach_to_email, is_for_primary_applicant, is_for_co_applicant, is_for_co_signer, is_optional, is_auto_generate, is_default_selected, is_household, is_published, is_hidden, show_in_portal, require_sign, require_countersign, countersign_company_group_ids, exclude_other_signers_info, order_num, activated_on, deactivated_on, last_sync_on, archived_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlMasterDocumentId() . ', ' .
						$this->sqlCompanyApplicationId() . ', ' .
						$this->sqlExternalKey() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlRequireConfirmation() . ', ' .
						$this->sqlAttachToEmail() . ', ' .
						$this->sqlIsForPrimaryApplicant() . ', ' .
						$this->sqlIsForCoApplicant() . ', ' .
						$this->sqlIsForCoSigner() . ', ' .
						$this->sqlIsOptional() . ', ' .
						$this->sqlIsAutoGenerate() . ', ' .
						$this->sqlIsDefaultSelected() . ', ' .
						$this->sqlIsHousehold() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsHidden() . ', ' .
						$this->sqlShowInPortal() . ', ' .
						$this->sqlRequireSign() . ', ' .
						$this->sqlRequireCountersign() . ', ' .
						$this->sqlCountersignCompanyGroupIds() . ', ' .
						$this->sqlExcludeOtherSignersInfo() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlActivatedOn() . ', ' .
						$this->sqlDeactivatedOn() . ', ' .
						$this->sqlLastSyncOn() . ', ' .
						$this->sqlArchivedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' master_document_id = ' . $this->sqlMasterDocumentId(). ',' ; } elseif( true == array_key_exists( 'MasterDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' master_document_id = ' . $this->sqlMasterDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId(). ',' ; } elseif( true == array_key_exists( 'CompanyApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_key = ' . $this->sqlExternalKey(). ',' ; } elseif( true == array_key_exists( 'ExternalKey', $this->getChangedColumns() ) ) { $strSql .= ' external_key = ' . $this->sqlExternalKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_confirmation = ' . $this->sqlRequireConfirmation(). ',' ; } elseif( true == array_key_exists( 'RequireConfirmation', $this->getChangedColumns() ) ) { $strSql .= ' require_confirmation = ' . $this->sqlRequireConfirmation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attach_to_email = ' . $this->sqlAttachToEmail(). ',' ; } elseif( true == array_key_exists( 'AttachToEmail', $this->getChangedColumns() ) ) { $strSql .= ' attach_to_email = ' . $this->sqlAttachToEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_primary_applicant = ' . $this->sqlIsForPrimaryApplicant(). ',' ; } elseif( true == array_key_exists( 'IsForPrimaryApplicant', $this->getChangedColumns() ) ) { $strSql .= ' is_for_primary_applicant = ' . $this->sqlIsForPrimaryApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_co_applicant = ' . $this->sqlIsForCoApplicant(). ',' ; } elseif( true == array_key_exists( 'IsForCoApplicant', $this->getChangedColumns() ) ) { $strSql .= ' is_for_co_applicant = ' . $this->sqlIsForCoApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_co_signer = ' . $this->sqlIsForCoSigner(). ',' ; } elseif( true == array_key_exists( 'IsForCoSigner', $this->getChangedColumns() ) ) { $strSql .= ' is_for_co_signer = ' . $this->sqlIsForCoSigner() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_generate = ' . $this->sqlIsAutoGenerate(). ',' ; } elseif( true == array_key_exists( 'IsAutoGenerate', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_generate = ' . $this->sqlIsAutoGenerate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_selected = ' . $this->sqlIsDefaultSelected(). ',' ; } elseif( true == array_key_exists( 'IsDefaultSelected', $this->getChangedColumns() ) ) { $strSql .= ' is_default_selected = ' . $this->sqlIsDefaultSelected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_household = ' . $this->sqlIsHousehold(). ',' ; } elseif( true == array_key_exists( 'IsHousehold', $this->getChangedColumns() ) ) { $strSql .= ' is_household = ' . $this->sqlIsHousehold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden(). ',' ; } elseif( true == array_key_exists( 'IsHidden', $this->getChangedColumns() ) ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_portal = ' . $this->sqlShowInPortal(). ',' ; } elseif( true == array_key_exists( 'ShowInPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_in_portal = ' . $this->sqlShowInPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_sign = ' . $this->sqlRequireSign(). ',' ; } elseif( true == array_key_exists( 'RequireSign', $this->getChangedColumns() ) ) { $strSql .= ' require_sign = ' . $this->sqlRequireSign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_countersign = ' . $this->sqlRequireCountersign(). ',' ; } elseif( true == array_key_exists( 'RequireCountersign', $this->getChangedColumns() ) ) { $strSql .= ' require_countersign = ' . $this->sqlRequireCountersign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' countersign_company_group_ids = ' . $this->sqlCountersignCompanyGroupIds(). ',' ; } elseif( true == array_key_exists( 'CountersignCompanyGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' countersign_company_group_ids = ' . $this->sqlCountersignCompanyGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_other_signers_info = ' . $this->sqlExcludeOtherSignersInfo(). ',' ; } elseif( true == array_key_exists( 'ExcludeOtherSignersInfo', $this->getChangedColumns() ) ) { $strSql .= ' exclude_other_signers_info = ' . $this->sqlExcludeOtherSignersInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn(). ',' ; } elseif( true == array_key_exists( 'ActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivated_on = ' . $this->sqlDeactivatedOn(). ',' ; } elseif( true == array_key_exists( 'DeactivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deactivated_on = ' . $this->sqlDeactivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn(). ',' ; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn(). ',' ; } elseif( true == array_key_exists( 'ArchivedOn', $this->getChangedColumns() ) ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'document_id' => $this->getDocumentId(),
			'master_document_id' => $this->getMasterDocumentId(),
			'company_application_id' => $this->getCompanyApplicationId(),
			'external_key' => $this->getExternalKey(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'require_confirmation' => $this->getRequireConfirmation(),
			'attach_to_email' => $this->getAttachToEmail(),
			'is_for_primary_applicant' => $this->getIsForPrimaryApplicant(),
			'is_for_co_applicant' => $this->getIsForCoApplicant(),
			'is_for_co_signer' => $this->getIsForCoSigner(),
			'is_optional' => $this->getIsOptional(),
			'is_auto_generate' => $this->getIsAutoGenerate(),
			'is_default_selected' => $this->getIsDefaultSelected(),
			'is_household' => $this->getIsHousehold(),
			'is_published' => $this->getIsPublished(),
			'is_hidden' => $this->getIsHidden(),
			'show_in_portal' => $this->getShowInPortal(),
			'require_sign' => $this->getRequireSign(),
			'require_countersign' => $this->getRequireCountersign(),
			'countersign_company_group_ids' => $this->getCountersignCompanyGroupIds(),
			'exclude_other_signers_info' => $this->getExcludeOtherSignersInfo(),
			'order_num' => $this->getOrderNum(),
			'activated_on' => $this->getActivatedOn(),
			'deactivated_on' => $this->getDeactivatedOn(),
			'last_sync_on' => $this->getLastSyncOn(),
			'archived_on' => $this->getArchivedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>