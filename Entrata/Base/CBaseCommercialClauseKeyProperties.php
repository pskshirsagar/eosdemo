<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialClauseKeyProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialClauseKeyProperties extends CEosPluralBase {

	/**
	 * @return CCommercialClauseKeyProperty[]
	 */
	public static function fetchCommercialClauseKeyProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCommercialClauseKeyProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialClauseKeyProperty
	 */
	public static function fetchCommercialClauseKeyProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommercialClauseKeyProperty', $objDatabase );
	}

	public static function fetchCommercialClauseKeyPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_clause_key_properties', $objDatabase );
	}

	public static function fetchCommercialClauseKeyPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeyProperty( sprintf( 'SELECT * FROM commercial_clause_key_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeyPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeyProperties( sprintf( 'SELECT * FROM commercial_clause_key_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeyPropertiesByCommercialClauseKeyIdByCid( $intCommercialClauseKeyId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeyProperties( sprintf( 'SELECT * FROM commercial_clause_key_properties WHERE commercial_clause_key_id = %d AND cid = %d', ( int ) $intCommercialClauseKeyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeyPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeyProperties( sprintf( 'SELECT * FROM commercial_clause_key_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>