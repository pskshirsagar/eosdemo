<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlGroups extends CEosPluralBase {

	/**
	 * @return CGlGroup[]
	 */
	public static function fetchGlGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlGroup
	 */
	public static function fetchGlGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlGroup::class, $objDatabase );
	}

	public static function fetchGlGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_groups', $objDatabase );
	}

	public static function fetchGlGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlGroup( sprintf( 'SELECT * FROM gl_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByGlAccountTypeIdByCid( $intGlAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE gl_account_type_id = %d AND cid = %d', $intGlAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE gl_tree_id = %d AND cid = %d', $intGlTreeId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByParentGlGroupIdByCid( $intParentGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE parent_gl_group_id = %d AND cid = %d', $intParentGlGroupId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByOriginGlGroupIdByCid( $intOriginGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE origin_gl_group_id = %d AND cid = %d', $intOriginGlGroupId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByDefaultGlGroupIdByCid( $intDefaultGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE default_gl_group_id = %d AND cid = %d', $intDefaultGlGroupId, $intCid ), $objDatabase );
	}

	public static function fetchGlGroupsByGlGroupTypeIdByCid( $intGlGroupTypeId, $intCid, $objDatabase ) {
		return self::fetchGlGroups( sprintf( 'SELECT * FROM gl_groups WHERE gl_group_type_id = %d AND cid = %d', $intGlGroupTypeId, $intCid ), $objDatabase );
	}

}
?>