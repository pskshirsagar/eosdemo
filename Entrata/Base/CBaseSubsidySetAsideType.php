<?php

class CBaseSubsidySetAsideType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.subsidy_set_aside_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltQualifiedUnitsPercent;
	protected $m_fltMedianIncomePercent;
	protected $m_boolIsPublished;
	protected $m_boolIsCustom;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_fltQualifiedUnitsPercent = '0.00';
		$this->m_fltMedianIncomePercent = '0.00';
		$this->m_boolIsPublished = true;
		$this->m_boolIsCustom = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['qualified_units_percent'] ) && $boolDirectSet ) $this->set( 'm_fltQualifiedUnitsPercent', trim( $arrValues['qualified_units_percent'] ) ); elseif( isset( $arrValues['qualified_units_percent'] ) ) $this->setQualifiedUnitsPercent( $arrValues['qualified_units_percent'] );
		if( isset( $arrValues['median_income_percent'] ) && $boolDirectSet ) $this->set( 'm_fltMedianIncomePercent', trim( $arrValues['median_income_percent'] ) ); elseif( isset( $arrValues['median_income_percent'] ) ) $this->setMedianIncomePercent( $arrValues['median_income_percent'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_custom'] ) && $boolDirectSet ) $this->set( 'm_boolIsCustom', trim( stripcslashes( $arrValues['is_custom'] ) ) ); elseif( isset( $arrValues['is_custom'] ) ) $this->setIsCustom( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_custom'] ) : $arrValues['is_custom'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setQualifiedUnitsPercent( $fltQualifiedUnitsPercent ) {
		$this->set( 'm_fltQualifiedUnitsPercent', CStrings::strToFloatDef( $fltQualifiedUnitsPercent, NULL, false, 2 ) );
	}

	public function getQualifiedUnitsPercent() {
		return $this->m_fltQualifiedUnitsPercent;
	}

	public function sqlQualifiedUnitsPercent() {
		return ( true == isset( $this->m_fltQualifiedUnitsPercent ) ) ? ( string ) $this->m_fltQualifiedUnitsPercent : '0.00';
	}

	public function setMedianIncomePercent( $fltMedianIncomePercent ) {
		$this->set( 'm_fltMedianIncomePercent', CStrings::strToFloatDef( $fltMedianIncomePercent, NULL, false, 2 ) );
	}

	public function getMedianIncomePercent() {
		return $this->m_fltMedianIncomePercent;
	}

	public function sqlMedianIncomePercent() {
		return ( true == isset( $this->m_fltMedianIncomePercent ) ) ? ( string ) $this->m_fltMedianIncomePercent : '0.00';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCustom( $boolIsCustom ) {
		$this->set( 'm_boolIsCustom', CStrings::strToBool( $boolIsCustom ) );
	}

	public function getIsCustom() {
		return $this->m_boolIsCustom;
	}

	public function sqlIsCustom() {
		return ( true == isset( $this->m_boolIsCustom ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCustom ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'qualified_units_percent' => $this->getQualifiedUnitsPercent(),
			'median_income_percent' => $this->getMedianIncomePercent(),
			'is_published' => $this->getIsPublished(),
			'is_custom' => $this->getIsCustom(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>