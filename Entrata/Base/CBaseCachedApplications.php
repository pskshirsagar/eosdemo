<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedApplications extends CEosPluralBase {

	/**
	 * @return CCachedApplication[]
	 */
	public static function fetchCachedApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplication::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedApplication
	 */
	public static function fetchCachedApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplication::class, $objDatabase );
	}

	public static function fetchCachedApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_applications', $objDatabase );
	}

	public static function fetchCachedApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedApplication( sprintf( 'SELECT * FROM cached_applications WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_interval_id = %d AND cid = %d', $intLeaseIntervalId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseIntervalTypeIdByCid( $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_interval_type_id = %d AND cid = %d', $intLeaseIntervalTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_status_type_id = %d AND cid = %d', $intLeaseStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_type_id = %d AND cid = %d', $intLeaseTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByApplicationStageIdByCid( $intApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE application_stage_id = %d AND cid = %d', $intApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByApplicationStatusIdByCid( $intApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE application_status_id = %d AND cid = %d', $intApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByApplicationStepIdByCid( $intApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE application_step_id = %d AND cid = %d', $intApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByMaxApplicationStageIdByCid( $intMaxApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE max_application_stage_id = %d AND cid = %d', $intMaxApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByMaxApplicationStatusIdByCid( $intMaxApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE max_application_status_id = %d AND cid = %d', $intMaxApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByMaxApplicationStepIdByCid( $intMaxApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE max_application_step_id = %d AND cid = %d', $intMaxApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCancellationListTypeIdByCid( $intCancellationListTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE cancellation_list_type_id = %d AND cid = %d', $intCancellationListTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCancellationListItemIdByCid( $intCancellationListItemId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE cancellation_list_item_id = %d AND cid = %d', $intCancellationListItemId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPrimaryApplicantIdByCid( $intPrimaryApplicantId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE primary_applicant_id = %d AND cid = %d', $intPrimaryApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByUnitKindIdByCid( $intUnitKindId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE unit_kind_id = %d AND cid = %d', $intUnitKindId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE space_configuration_id = %d AND cid = %d', $intSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByDesiredSpaceConfigurationIdByCid( $intDesiredSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE desired_space_configuration_id = %d AND cid = %d', $intDesiredSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE screening_id = %d AND cid = %d', $intScreeningId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByApplicationPsProductIdByCid( $intApplicationPsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE application_ps_product_id = %d AND cid = %d', $intApplicationPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeasePsProductIdByCid( $intLeasePsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_ps_product_id = %d AND cid = %d', $intLeasePsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeasingAgentIdByCid( $intLeasingAgentId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE leasing_agent_id = %d AND cid = %d', $intLeasingAgentId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByInternetListingServiceIdByCid( $intInternetListingServiceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE internet_listing_service_id = %d AND cid = %d', $intInternetListingServiceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByOriginatingLeadSourceIdByCid( $intOriginatingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE originating_lead_source_id = %d AND cid = %d', $intOriginatingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByConvertingLeadSourceIdByCid( $intConvertingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE converting_lead_source_id = %d AND cid = %d', $intConvertingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE company_application_id = %d AND cid = %d', $intCompanyApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLateFeeFormulaIdByCid( $intLateFeeFormulaId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE late_fee_formula_id = %d AND cid = %d', $intLateFeeFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCombinedApplicationIdByCid( $intCombinedApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE combined_application_id = %d AND cid = %d', $intCombinedApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE quote_id = %d AND cid = %d', $intQuoteId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsBySemAdGroupIdByCid( $intSemAdGroupId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE sem_ad_group_id = %d AND cid = %d', $intSemAdGroupId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsBySemKeywordIdByCid( $intSemKeywordId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE sem_keyword_id = %d AND cid = %d', $intSemKeywordId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsBySemSourceIdByCid( $intSemSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE sem_source_id = %d AND cid = %d', $intSemSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE call_id = %d AND cid = %d', $intCallId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE website_id = %d AND cid = %d', $intWebsiteId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByCraigslistPostIdByCid( $intCraigslistPostId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE craigslist_post_id = %d AND cid = %d', $intCraigslistPostId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByClAdIdByCid( $intClAdId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE cl_ad_id = %d AND cid = %d', $intClAdId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByTrafficCookieIdByCid( $intTrafficCookieId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE traffic_cookie_id = %d AND cid = %d', $intTrafficCookieId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByFirstEventIdByCid( $intFirstEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE first_event_id = %d AND cid = %d', $intFirstEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByFirstEventMediumIdByCid( $intFirstEventMediumId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE first_event_medium_id = %d AND cid = %d', $intFirstEventMediumId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByFirstLeasingCenterEventIdByCid( $intFirstLeasingCenterEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE first_leasing_center_event_id = %d AND cid = %d', $intFirstLeasingCenterEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByResponseEventIdByCid( $intResponseEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE response_event_id = %d AND cid = %d', $intResponseEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLastEventIdByCid( $intLastEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE last_event_id = %d AND cid = %d', $intLastEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByReferringLeaseCustomerIdByCid( $intReferringLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE referring_lease_customer_id = %d AND cid = %d', $intReferringLeaseCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByLeaseSignatureTypeIdByCid( $intLeaseSignatureTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE lease_signature_type_id = %d AND cid = %d', $intLeaseSignatureTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByInsurancePolicyIdByCid( $intInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE insurance_policy_id = %d AND cid = %d', $intInsurancePolicyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByInsurancePolicyStatusTypeIdByCid( $intInsurancePolicyStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE insurance_policy_status_type_id = %d AND cid = %d', $intInsurancePolicyStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByWaitListIdByCid( $intWaitListId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE wait_list_id = %d AND cid = %d', $intWaitListId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByWaitListApplicationIdByCid( $intWaitListApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE wait_list_application_id = %d AND cid = %d', $intWaitListApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationsByUnitPriorApplicationIdByCid( $intUnitPriorApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications WHERE unit_prior_application_id = %d AND cid = %d', $intUnitPriorApplicationId, $intCid ), $objDatabase );
	}

}
?>