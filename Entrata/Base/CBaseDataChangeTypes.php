<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataChangeTypes
 * Do not add any new functions to this class.
 */

class CBaseDataChangeTypes extends CEosPluralBase {

	/**
	 * @return CDataChangeType[]
	 */
	public static function fetchDataChangeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDataChangeType::class, $objDatabase );
	}

	/**
	 * @return CDataChangeType
	 */
	public static function fetchDataChangeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDataChangeType::class, $objDatabase );
	}

	public static function fetchDataChangeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_change_types', $objDatabase );
	}

	public static function fetchDataChangeTypeById( $intId, $objDatabase ) {
		return self::fetchDataChangeType( sprintf( 'SELECT * FROM data_change_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>