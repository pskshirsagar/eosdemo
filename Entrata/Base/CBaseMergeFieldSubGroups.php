<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldSubGroups
 * Do not add any new functions to this class.
 */

class CBaseMergeFieldSubGroups extends CEosPluralBase {

	/**
	 * @return CMergeFieldSubGroup[]
	 */
	public static function fetchMergeFieldSubGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMergeFieldSubGroup::class, $objDatabase );
	}

	/**
	 * @return CMergeFieldSubGroup
	 */
	public static function fetchMergeFieldSubGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMergeFieldSubGroup::class, $objDatabase );
	}

	public static function fetchMergeFieldSubGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merge_field_sub_groups', $objDatabase );
	}

	public static function fetchMergeFieldSubGroupById( $intId, $objDatabase ) {
		return self::fetchMergeFieldSubGroup( sprintf( 'SELECT * FROM merge_field_sub_groups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMergeFieldSubGroupsByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {
		return self::fetchMergeFieldSubGroups( sprintf( 'SELECT * FROM merge_field_sub_groups WHERE merge_field_group_id = %d', $intMergeFieldGroupId ), $objDatabase );
	}

}
?>