<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestFilter extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_request_filters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_strName;
	protected $m_strSearchKeyword;
	protected $m_strPropertyGroupIds;
	protected $m_strMaintenancePriorityIds;
	protected $m_strMaintenanceStatusIds;
	protected $m_strMaintenanceLocationIds;
	protected $m_strMaintenanceProblemIds;
	protected $m_strMaintenanceRequestTypeIds;
	protected $m_strUnitSpaceStatusTypeIds;
	protected $m_strApPayeeIds;
	protected $m_strPropertyBuildingIds;
	protected $m_strUnitTypeIds;
	protected $m_strOrderByField;
	protected $m_intOrderByType;
	protected $m_strCreatedOnFrom;
	protected $m_strCreatedOnTo;
	protected $m_strCompletedOnFrom;
	protected $m_strCompletedOnTo;
	protected $m_strScheduledOnFrom;
	protected $m_strScheduledOnTo;
	protected $m_strScheduledEndFrom;
	protected $m_strScheduledEndTo;
	protected $m_strMoveInFrom;
	protected $m_strMoveInTo;
	protected $m_strMoveOutFrom;
	protected $m_strMoveOutTo;
	protected $m_strDueOnFrom;
	protected $m_strDueOnTo;
	protected $m_strSubtaskScheduledEndFrom;
	protected $m_strSubtaskScheduledEndTo;
	protected $m_strPsProductIds;
	protected $m_strCompanyEmployeeIds;
	protected $m_intIsShowSubTasks;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsShowSubTasks = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestTypeId', trim( $arrValues['maintenance_request_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrValues['maintenance_request_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['search_keyword'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSearchKeyword', trim( stripcslashes( $arrValues['search_keyword'] ) ) ); elseif( isset( $arrValues['search_keyword'] ) ) $this->setSearchKeyword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['search_keyword'] ) : $arrValues['search_keyword'] );
		if( isset( $arrValues['property_group_ids'] ) && $boolDirectSet ) $this->set( 'm_strPropertyGroupIds', trim( stripcslashes( $arrValues['property_group_ids'] ) ) ); elseif( isset( $arrValues['property_group_ids'] ) ) $this->setPropertyGroupIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_group_ids'] ) : $arrValues['property_group_ids'] );
		if( isset( $arrValues['maintenance_priority_ids'] ) && $boolDirectSet ) $this->set( 'm_strMaintenancePriorityIds', trim( stripcslashes( $arrValues['maintenance_priority_ids'] ) ) ); elseif( isset( $arrValues['maintenance_priority_ids'] ) ) $this->setMaintenancePriorityIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_priority_ids'] ) : $arrValues['maintenance_priority_ids'] );
		if( isset( $arrValues['maintenance_status_ids'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceStatusIds', trim( stripcslashes( $arrValues['maintenance_status_ids'] ) ) ); elseif( isset( $arrValues['maintenance_status_ids'] ) ) $this->setMaintenanceStatusIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_status_ids'] ) : $arrValues['maintenance_status_ids'] );
		if( isset( $arrValues['maintenance_location_ids'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceLocationIds', trim( stripcslashes( $arrValues['maintenance_location_ids'] ) ) ); elseif( isset( $arrValues['maintenance_location_ids'] ) ) $this->setMaintenanceLocationIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_location_ids'] ) : $arrValues['maintenance_location_ids'] );
		if( isset( $arrValues['maintenance_problem_ids'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceProblemIds', trim( stripcslashes( $arrValues['maintenance_problem_ids'] ) ) ); elseif( isset( $arrValues['maintenance_problem_ids'] ) ) $this->setMaintenanceProblemIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_problem_ids'] ) : $arrValues['maintenance_problem_ids'] );
		if( isset( $arrValues['maintenance_request_type_ids'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceRequestTypeIds', trim( stripcslashes( $arrValues['maintenance_request_type_ids'] ) ) ); elseif( isset( $arrValues['maintenance_request_type_ids'] ) ) $this->setMaintenanceRequestTypeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_request_type_ids'] ) : $arrValues['maintenance_request_type_ids'] );
		if( isset( $arrValues['unit_space_status_type_ids'] ) && $boolDirectSet ) $this->set( 'm_strUnitSpaceStatusTypeIds', trim( stripcslashes( $arrValues['unit_space_status_type_ids'] ) ) ); elseif( isset( $arrValues['unit_space_status_type_ids'] ) ) $this->setUnitSpaceStatusTypeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_status_type_ids'] ) : $arrValues['unit_space_status_type_ids'] );
		if( isset( $arrValues['ap_payee_ids'] ) && $boolDirectSet ) $this->set( 'm_strApPayeeIds', trim( stripcslashes( $arrValues['ap_payee_ids'] ) ) ); elseif( isset( $arrValues['ap_payee_ids'] ) ) $this->setApPayeeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ap_payee_ids'] ) : $arrValues['ap_payee_ids'] );
		if( isset( $arrValues['property_building_ids'] ) && $boolDirectSet ) $this->set( 'm_strPropertyBuildingIds', trim( stripcslashes( $arrValues['property_building_ids'] ) ) ); elseif( isset( $arrValues['property_building_ids'] ) ) $this->setPropertyBuildingIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_building_ids'] ) : $arrValues['property_building_ids'] );
		if( isset( $arrValues['unit_type_ids'] ) && $boolDirectSet ) $this->set( 'm_strUnitTypeIds', trim( stripcslashes( $arrValues['unit_type_ids'] ) ) ); elseif( isset( $arrValues['unit_type_ids'] ) ) $this->setUnitTypeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_type_ids'] ) : $arrValues['unit_type_ids'] );
		if( isset( $arrValues['order_by_field'] ) && $boolDirectSet ) $this->set( 'm_strOrderByField', trim( stripcslashes( $arrValues['order_by_field'] ) ) ); elseif( isset( $arrValues['order_by_field'] ) ) $this->setOrderByField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['order_by_field'] ) : $arrValues['order_by_field'] );
		if( isset( $arrValues['order_by_type'] ) && $boolDirectSet ) $this->set( 'm_intOrderByType', trim( $arrValues['order_by_type'] ) ); elseif( isset( $arrValues['order_by_type'] ) ) $this->setOrderByType( $arrValues['order_by_type'] );
		if( isset( $arrValues['created_on_from'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOnFrom', trim( $arrValues['created_on_from'] ) ); elseif( isset( $arrValues['created_on_from'] ) ) $this->setCreatedOnFrom( $arrValues['created_on_from'] );
		if( isset( $arrValues['created_on_to'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOnTo', trim( $arrValues['created_on_to'] ) ); elseif( isset( $arrValues['created_on_to'] ) ) $this->setCreatedOnTo( $arrValues['created_on_to'] );
		if( isset( $arrValues['completed_on_from'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOnFrom', trim( $arrValues['completed_on_from'] ) ); elseif( isset( $arrValues['completed_on_from'] ) ) $this->setCompletedOnFrom( $arrValues['completed_on_from'] );
		if( isset( $arrValues['completed_on_to'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOnTo', trim( $arrValues['completed_on_to'] ) ); elseif( isset( $arrValues['completed_on_to'] ) ) $this->setCompletedOnTo( $arrValues['completed_on_to'] );
		if( isset( $arrValues['scheduled_on_from'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOnFrom', trim( $arrValues['scheduled_on_from'] ) ); elseif( isset( $arrValues['scheduled_on_from'] ) ) $this->setScheduledOnFrom( $arrValues['scheduled_on_from'] );
		if( isset( $arrValues['scheduled_on_to'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOnTo', trim( $arrValues['scheduled_on_to'] ) ); elseif( isset( $arrValues['scheduled_on_to'] ) ) $this->setScheduledOnTo( $arrValues['scheduled_on_to'] );
		if( isset( $arrValues['scheduled_end_from'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndFrom', trim( $arrValues['scheduled_end_from'] ) ); elseif( isset( $arrValues['scheduled_end_from'] ) ) $this->setScheduledEndFrom( $arrValues['scheduled_end_from'] );
		if( isset( $arrValues['scheduled_end_to'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndTo', trim( $arrValues['scheduled_end_to'] ) ); elseif( isset( $arrValues['scheduled_end_to'] ) ) $this->setScheduledEndTo( $arrValues['scheduled_end_to'] );
		if( isset( $arrValues['move_in_from'] ) && $boolDirectSet ) $this->set( 'm_strMoveInFrom', trim( $arrValues['move_in_from'] ) ); elseif( isset( $arrValues['move_in_from'] ) ) $this->setMoveInFrom( $arrValues['move_in_from'] );
		if( isset( $arrValues['move_in_to'] ) && $boolDirectSet ) $this->set( 'm_strMoveInTo', trim( $arrValues['move_in_to'] ) ); elseif( isset( $arrValues['move_in_to'] ) ) $this->setMoveInTo( $arrValues['move_in_to'] );
		if( isset( $arrValues['move_out_from'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutFrom', trim( $arrValues['move_out_from'] ) ); elseif( isset( $arrValues['move_out_from'] ) ) $this->setMoveOutFrom( $arrValues['move_out_from'] );
		if( isset( $arrValues['move_out_to'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutTo', trim( $arrValues['move_out_to'] ) ); elseif( isset( $arrValues['move_out_to'] ) ) $this->setMoveOutTo( $arrValues['move_out_to'] );
		if( isset( $arrValues['due_on_from'] ) && $boolDirectSet ) $this->set( 'm_strDueOnFrom', trim( $arrValues['due_on_from'] ) ); elseif( isset( $arrValues['due_on_from'] ) ) $this->setDueOnFrom( $arrValues['due_on_from'] );
		if( isset( $arrValues['due_on_to'] ) && $boolDirectSet ) $this->set( 'm_strDueOnTo', trim( $arrValues['due_on_to'] ) ); elseif( isset( $arrValues['due_on_to'] ) ) $this->setDueOnTo( $arrValues['due_on_to'] );
		if( isset( $arrValues['subtask_scheduled_end_from'] ) && $boolDirectSet ) $this->set( 'm_strSubtaskScheduledEndFrom', trim( $arrValues['subtask_scheduled_end_from'] ) ); elseif( isset( $arrValues['subtask_scheduled_end_from'] ) ) $this->setSubtaskScheduledEndFrom( $arrValues['subtask_scheduled_end_from'] );
		if( isset( $arrValues['subtask_scheduled_end_to'] ) && $boolDirectSet ) $this->set( 'm_strSubtaskScheduledEndTo', trim( $arrValues['subtask_scheduled_end_to'] ) ); elseif( isset( $arrValues['subtask_scheduled_end_to'] ) ) $this->setSubtaskScheduledEndTo( $arrValues['subtask_scheduled_end_to'] );
		if( isset( $arrValues['ps_product_ids'] ) && $boolDirectSet ) $this->set( 'm_strPsProductIds', trim( stripcslashes( $arrValues['ps_product_ids'] ) ) ); elseif( isset( $arrValues['ps_product_ids'] ) ) $this->setPsProductIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_product_ids'] ) : $arrValues['ps_product_ids'] );
		if( isset( $arrValues['company_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEmployeeIds', trim( stripcslashes( $arrValues['company_employee_ids'] ) ) ); elseif( isset( $arrValues['company_employee_ids'] ) ) $this->setCompanyEmployeeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_employee_ids'] ) : $arrValues['company_employee_ids'] );
		if( isset( $arrValues['is_show_sub_tasks'] ) && $boolDirectSet ) $this->set( 'm_intIsShowSubTasks', trim( $arrValues['is_show_sub_tasks'] ) ); elseif( isset( $arrValues['is_show_sub_tasks'] ) ) $this->setIsShowSubTasks( $arrValues['is_show_sub_tasks'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->set( 'm_intMaintenanceRequestTypeId', CStrings::strToIntDef( $intMaintenanceRequestTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function sqlMaintenanceRequestTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSearchKeyword( $strSearchKeyword, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSearchKeyword', CStrings::strTrimDef( $strSearchKeyword, 1000, NULL, true ), $strLocaleCode );
	}

	public function getSearchKeyword( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSearchKeyword', $strLocaleCode );
	}

	public function sqlSearchKeyword() {
		return ( true == isset( $this->m_strSearchKeyword ) ) ? '\'' . addslashes( $this->m_strSearchKeyword ) . '\'' : 'NULL';
	}

	public function setPropertyGroupIds( $strPropertyGroupIds ) {
		$this->set( 'm_strPropertyGroupIds', CStrings::strTrimDef( $strPropertyGroupIds, -1, NULL, true ) );
	}

	public function getPropertyGroupIds() {
		return $this->m_strPropertyGroupIds;
	}

	public function sqlPropertyGroupIds() {
		return ( true == isset( $this->m_strPropertyGroupIds ) ) ? '\'' . addslashes( $this->m_strPropertyGroupIds ) . '\'' : 'NULL';
	}

	public function setMaintenancePriorityIds( $strMaintenancePriorityIds ) {
		$this->set( 'm_strMaintenancePriorityIds', CStrings::strTrimDef( $strMaintenancePriorityIds, 5000, NULL, true ) );
	}

	public function getMaintenancePriorityIds() {
		return $this->m_strMaintenancePriorityIds;
	}

	public function sqlMaintenancePriorityIds() {
		return ( true == isset( $this->m_strMaintenancePriorityIds ) ) ? '\'' . addslashes( $this->m_strMaintenancePriorityIds ) . '\'' : 'NULL';
	}

	public function setMaintenanceStatusIds( $strMaintenanceStatusIds ) {
		$this->set( 'm_strMaintenanceStatusIds', CStrings::strTrimDef( $strMaintenanceStatusIds, 2000, NULL, true ) );
	}

	public function getMaintenanceStatusIds() {
		return $this->m_strMaintenanceStatusIds;
	}

	public function sqlMaintenanceStatusIds() {
		return ( true == isset( $this->m_strMaintenanceStatusIds ) ) ? '\'' . addslashes( $this->m_strMaintenanceStatusIds ) . '\'' : 'NULL';
	}

	public function setMaintenanceLocationIds( $strMaintenanceLocationIds ) {
		$this->set( 'm_strMaintenanceLocationIds', CStrings::strTrimDef( $strMaintenanceLocationIds, -1, NULL, true ) );
	}

	public function getMaintenanceLocationIds() {
		return $this->m_strMaintenanceLocationIds;
	}

	public function sqlMaintenanceLocationIds() {
		return ( true == isset( $this->m_strMaintenanceLocationIds ) ) ? '\'' . addslashes( $this->m_strMaintenanceLocationIds ) . '\'' : 'NULL';
	}

	public function setMaintenanceProblemIds( $strMaintenanceProblemIds ) {
		$this->set( 'm_strMaintenanceProblemIds', CStrings::strTrimDef( $strMaintenanceProblemIds, -1, NULL, true ) );
	}

	public function getMaintenanceProblemIds() {
		return $this->m_strMaintenanceProblemIds;
	}

	public function sqlMaintenanceProblemIds() {
		return ( true == isset( $this->m_strMaintenanceProblemIds ) ) ? '\'' . addslashes( $this->m_strMaintenanceProblemIds ) . '\'' : 'NULL';
	}

	public function setMaintenanceRequestTypeIds( $strMaintenanceRequestTypeIds ) {
		$this->set( 'm_strMaintenanceRequestTypeIds', CStrings::strTrimDef( $strMaintenanceRequestTypeIds, 30, NULL, true ) );
	}

	public function getMaintenanceRequestTypeIds() {
		return $this->m_strMaintenanceRequestTypeIds;
	}

	public function sqlMaintenanceRequestTypeIds() {
		return ( true == isset( $this->m_strMaintenanceRequestTypeIds ) ) ? '\'' . addslashes( $this->m_strMaintenanceRequestTypeIds ) . '\'' : 'NULL';
	}

	public function setUnitSpaceStatusTypeIds( $strUnitSpaceStatusTypeIds ) {
		$this->set( 'm_strUnitSpaceStatusTypeIds', CStrings::strTrimDef( $strUnitSpaceStatusTypeIds, 30, NULL, true ) );
	}

	public function getUnitSpaceStatusTypeIds() {
		return $this->m_strUnitSpaceStatusTypeIds;
	}

	public function sqlUnitSpaceStatusTypeIds() {
		return ( true == isset( $this->m_strUnitSpaceStatusTypeIds ) ) ? '\'' . addslashes( $this->m_strUnitSpaceStatusTypeIds ) . '\'' : 'NULL';
	}

	public function setApPayeeIds( $strApPayeeIds ) {
		$this->set( 'm_strApPayeeIds', CStrings::strTrimDef( $strApPayeeIds, -1, NULL, true ) );
	}

	public function getApPayeeIds() {
		return $this->m_strApPayeeIds;
	}

	public function sqlApPayeeIds() {
		return ( true == isset( $this->m_strApPayeeIds ) ) ? '\'' . addslashes( $this->m_strApPayeeIds ) . '\'' : 'NULL';
	}

	public function setPropertyBuildingIds( $strPropertyBuildingIds ) {
		$this->set( 'm_strPropertyBuildingIds', CStrings::strTrimDef( $strPropertyBuildingIds, -1, NULL, true ) );
	}

	public function getPropertyBuildingIds() {
		return $this->m_strPropertyBuildingIds;
	}

	public function sqlPropertyBuildingIds() {
		return ( true == isset( $this->m_strPropertyBuildingIds ) ) ? '\'' . addslashes( $this->m_strPropertyBuildingIds ) . '\'' : 'NULL';
	}

	public function setUnitTypeIds( $strUnitTypeIds ) {
		$this->set( 'm_strUnitTypeIds', CStrings::strTrimDef( $strUnitTypeIds, -1, NULL, true ) );
	}

	public function getUnitTypeIds() {
		return $this->m_strUnitTypeIds;
	}

	public function sqlUnitTypeIds() {
		return ( true == isset( $this->m_strUnitTypeIds ) ) ? '\'' . addslashes( $this->m_strUnitTypeIds ) . '\'' : 'NULL';
	}

	public function setOrderByField( $strOrderByField ) {
		$this->set( 'm_strOrderByField', CStrings::strTrimDef( $strOrderByField, 240, NULL, true ) );
	}

	public function getOrderByField() {
		return $this->m_strOrderByField;
	}

	public function sqlOrderByField() {
		return ( true == isset( $this->m_strOrderByField ) ) ? '\'' . addslashes( $this->m_strOrderByField ) . '\'' : 'NULL';
	}

	public function setOrderByType( $intOrderByType ) {
		$this->set( 'm_intOrderByType', CStrings::strToIntDef( $intOrderByType, NULL, false ) );
	}

	public function getOrderByType() {
		return $this->m_intOrderByType;
	}

	public function sqlOrderByType() {
		return ( true == isset( $this->m_intOrderByType ) ) ? ( string ) $this->m_intOrderByType : 'NULL';
	}

	public function setCreatedOnFrom( $strCreatedOnFrom ) {
		$this->set( 'm_strCreatedOnFrom', CStrings::strTrimDef( $strCreatedOnFrom, -1, NULL, true ) );
	}

	public function getCreatedOnFrom() {
		return $this->m_strCreatedOnFrom;
	}

	public function sqlCreatedOnFrom() {
		return ( true == isset( $this->m_strCreatedOnFrom ) ) ? '\'' . $this->m_strCreatedOnFrom . '\'' : 'NULL';
	}

	public function setCreatedOnTo( $strCreatedOnTo ) {
		$this->set( 'm_strCreatedOnTo', CStrings::strTrimDef( $strCreatedOnTo, -1, NULL, true ) );
	}

	public function getCreatedOnTo() {
		return $this->m_strCreatedOnTo;
	}

	public function sqlCreatedOnTo() {
		return ( true == isset( $this->m_strCreatedOnTo ) ) ? '\'' . $this->m_strCreatedOnTo . '\'' : 'NULL';
	}

	public function setCompletedOnFrom( $strCompletedOnFrom ) {
		$this->set( 'm_strCompletedOnFrom', CStrings::strTrimDef( $strCompletedOnFrom, -1, NULL, true ) );
	}

	public function getCompletedOnFrom() {
		return $this->m_strCompletedOnFrom;
	}

	public function sqlCompletedOnFrom() {
		return ( true == isset( $this->m_strCompletedOnFrom ) ) ? '\'' . $this->m_strCompletedOnFrom . '\'' : 'NULL';
	}

	public function setCompletedOnTo( $strCompletedOnTo ) {
		$this->set( 'm_strCompletedOnTo', CStrings::strTrimDef( $strCompletedOnTo, -1, NULL, true ) );
	}

	public function getCompletedOnTo() {
		return $this->m_strCompletedOnTo;
	}

	public function sqlCompletedOnTo() {
		return ( true == isset( $this->m_strCompletedOnTo ) ) ? '\'' . $this->m_strCompletedOnTo . '\'' : 'NULL';
	}

	public function setScheduledOnFrom( $strScheduledOnFrom ) {
		$this->set( 'm_strScheduledOnFrom', CStrings::strTrimDef( $strScheduledOnFrom, -1, NULL, true ) );
	}

	public function getScheduledOnFrom() {
		return $this->m_strScheduledOnFrom;
	}

	public function sqlScheduledOnFrom() {
		return ( true == isset( $this->m_strScheduledOnFrom ) ) ? '\'' . $this->m_strScheduledOnFrom . '\'' : 'NULL';
	}

	public function setScheduledOnTo( $strScheduledOnTo ) {
		$this->set( 'm_strScheduledOnTo', CStrings::strTrimDef( $strScheduledOnTo, -1, NULL, true ) );
	}

	public function getScheduledOnTo() {
		return $this->m_strScheduledOnTo;
	}

	public function sqlScheduledOnTo() {
		return ( true == isset( $this->m_strScheduledOnTo ) ) ? '\'' . $this->m_strScheduledOnTo . '\'' : 'NULL';
	}

	public function setScheduledEndFrom( $strScheduledEndFrom ) {
		$this->set( 'm_strScheduledEndFrom', CStrings::strTrimDef( $strScheduledEndFrom, -1, NULL, true ) );
	}

	public function getScheduledEndFrom() {
		return $this->m_strScheduledEndFrom;
	}

	public function sqlScheduledEndFrom() {
		return ( true == isset( $this->m_strScheduledEndFrom ) ) ? '\'' . $this->m_strScheduledEndFrom . '\'' : 'NULL';
	}

	public function setScheduledEndTo( $strScheduledEndTo ) {
		$this->set( 'm_strScheduledEndTo', CStrings::strTrimDef( $strScheduledEndTo, -1, NULL, true ) );
	}

	public function getScheduledEndTo() {
		return $this->m_strScheduledEndTo;
	}

	public function sqlScheduledEndTo() {
		return ( true == isset( $this->m_strScheduledEndTo ) ) ? '\'' . $this->m_strScheduledEndTo . '\'' : 'NULL';
	}

	public function setMoveInFrom( $strMoveInFrom ) {
		$this->set( 'm_strMoveInFrom', CStrings::strTrimDef( $strMoveInFrom, -1, NULL, true ) );
	}

	public function getMoveInFrom() {
		return $this->m_strMoveInFrom;
	}

	public function sqlMoveInFrom() {
		return ( true == isset( $this->m_strMoveInFrom ) ) ? '\'' . $this->m_strMoveInFrom . '\'' : 'NULL';
	}

	public function setMoveInTo( $strMoveInTo ) {
		$this->set( 'm_strMoveInTo', CStrings::strTrimDef( $strMoveInTo, -1, NULL, true ) );
	}

	public function getMoveInTo() {
		return $this->m_strMoveInTo;
	}

	public function sqlMoveInTo() {
		return ( true == isset( $this->m_strMoveInTo ) ) ? '\'' . $this->m_strMoveInTo . '\'' : 'NULL';
	}

	public function setMoveOutFrom( $strMoveOutFrom ) {
		$this->set( 'm_strMoveOutFrom', CStrings::strTrimDef( $strMoveOutFrom, -1, NULL, true ) );
	}

	public function getMoveOutFrom() {
		return $this->m_strMoveOutFrom;
	}

	public function sqlMoveOutFrom() {
		return ( true == isset( $this->m_strMoveOutFrom ) ) ? '\'' . $this->m_strMoveOutFrom . '\'' : 'NULL';
	}

	public function setMoveOutTo( $strMoveOutTo ) {
		$this->set( 'm_strMoveOutTo', CStrings::strTrimDef( $strMoveOutTo, -1, NULL, true ) );
	}

	public function getMoveOutTo() {
		return $this->m_strMoveOutTo;
	}

	public function sqlMoveOutTo() {
		return ( true == isset( $this->m_strMoveOutTo ) ) ? '\'' . $this->m_strMoveOutTo . '\'' : 'NULL';
	}

	public function setDueOnFrom( $strDueOnFrom ) {
		$this->set( 'm_strDueOnFrom', CStrings::strTrimDef( $strDueOnFrom, -1, NULL, true ) );
	}

	public function getDueOnFrom() {
		return $this->m_strDueOnFrom;
	}

	public function sqlDueOnFrom() {
		return ( true == isset( $this->m_strDueOnFrom ) ) ? '\'' . $this->m_strDueOnFrom . '\'' : 'NULL';
	}

	public function setDueOnTo( $strDueOnTo ) {
		$this->set( 'm_strDueOnTo', CStrings::strTrimDef( $strDueOnTo, -1, NULL, true ) );
	}

	public function getDueOnTo() {
		return $this->m_strDueOnTo;
	}

	public function sqlDueOnTo() {
		return ( true == isset( $this->m_strDueOnTo ) ) ? '\'' . $this->m_strDueOnTo . '\'' : 'NULL';
	}

	public function setSubtaskScheduledEndFrom( $strSubtaskScheduledEndFrom ) {
		$this->set( 'm_strSubtaskScheduledEndFrom', CStrings::strTrimDef( $strSubtaskScheduledEndFrom, -1, NULL, true ) );
	}

	public function getSubtaskScheduledEndFrom() {
		return $this->m_strSubtaskScheduledEndFrom;
	}

	public function sqlSubtaskScheduledEndFrom() {
		return ( true == isset( $this->m_strSubtaskScheduledEndFrom ) ) ? '\'' . $this->m_strSubtaskScheduledEndFrom . '\'' : 'NULL';
	}

	public function setSubtaskScheduledEndTo( $strSubtaskScheduledEndTo ) {
		$this->set( 'm_strSubtaskScheduledEndTo', CStrings::strTrimDef( $strSubtaskScheduledEndTo, -1, NULL, true ) );
	}

	public function getSubtaskScheduledEndTo() {
		return $this->m_strSubtaskScheduledEndTo;
	}

	public function sqlSubtaskScheduledEndTo() {
		return ( true == isset( $this->m_strSubtaskScheduledEndTo ) ) ? '\'' . $this->m_strSubtaskScheduledEndTo . '\'' : 'NULL';
	}

	public function setPsProductIds( $strPsProductIds ) {
		$this->set( 'm_strPsProductIds', CStrings::strTrimDef( $strPsProductIds, 2000, NULL, true ) );
	}

	public function getPsProductIds() {
		return $this->m_strPsProductIds;
	}

	public function sqlPsProductIds() {
		return ( true == isset( $this->m_strPsProductIds ) ) ? '\'' . addslashes( $this->m_strPsProductIds ) . '\'' : 'NULL';
	}

	public function setCompanyEmployeeIds( $strCompanyEmployeeIds ) {
		$this->set( 'm_strCompanyEmployeeIds', CStrings::strTrimDef( $strCompanyEmployeeIds, -1, NULL, true ) );
	}

	public function getCompanyEmployeeIds() {
		return $this->m_strCompanyEmployeeIds;
	}

	public function sqlCompanyEmployeeIds() {
		return ( true == isset( $this->m_strCompanyEmployeeIds ) ) ? '\'' . addslashes( $this->m_strCompanyEmployeeIds ) . '\'' : 'NULL';
	}

	public function setIsShowSubTasks( $intIsShowSubTasks ) {
		$this->set( 'm_intIsShowSubTasks', CStrings::strToIntDef( $intIsShowSubTasks, NULL, false ) );
	}

	public function getIsShowSubTasks() {
		return $this->m_intIsShowSubTasks;
	}

	public function sqlIsShowSubTasks() {
		return ( true == isset( $this->m_intIsShowSubTasks ) ) ? ( string ) $this->m_intIsShowSubTasks : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_type_id, name, search_keyword, property_group_ids, maintenance_priority_ids, maintenance_status_ids, maintenance_location_ids, maintenance_problem_ids, maintenance_request_type_ids, unit_space_status_type_ids, ap_payee_ids, property_building_ids, unit_type_ids, order_by_field, order_by_type, created_on_from, created_on_to, completed_on_from, completed_on_to, scheduled_on_from, scheduled_on_to, scheduled_end_from, scheduled_end_to, move_in_from, move_in_to, move_out_from, move_out_to, due_on_from, due_on_to, subtask_scheduled_end_from, subtask_scheduled_end_to, ps_product_ids, company_employee_ids, is_show_sub_tasks, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaintenanceRequestTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlSearchKeyword() . ', ' .
						$this->sqlPropertyGroupIds() . ', ' .
						$this->sqlMaintenancePriorityIds() . ', ' .
						$this->sqlMaintenanceStatusIds() . ', ' .
						$this->sqlMaintenanceLocationIds() . ', ' .
						$this->sqlMaintenanceProblemIds() . ', ' .
						$this->sqlMaintenanceRequestTypeIds() . ', ' .
						$this->sqlUnitSpaceStatusTypeIds() . ', ' .
						$this->sqlApPayeeIds() . ', ' .
						$this->sqlPropertyBuildingIds() . ', ' .
						$this->sqlUnitTypeIds() . ', ' .
						$this->sqlOrderByField() . ', ' .
						$this->sqlOrderByType() . ', ' .
						$this->sqlCreatedOnFrom() . ', ' .
						$this->sqlCreatedOnTo() . ', ' .
						$this->sqlCompletedOnFrom() . ', ' .
						$this->sqlCompletedOnTo() . ', ' .
						$this->sqlScheduledOnFrom() . ', ' .
						$this->sqlScheduledOnTo() . ', ' .
						$this->sqlScheduledEndFrom() . ', ' .
						$this->sqlScheduledEndTo() . ', ' .
						$this->sqlMoveInFrom() . ', ' .
						$this->sqlMoveInTo() . ', ' .
						$this->sqlMoveOutFrom() . ', ' .
						$this->sqlMoveOutTo() . ', ' .
						$this->sqlDueOnFrom() . ', ' .
						$this->sqlDueOnTo() . ', ' .
						$this->sqlSubtaskScheduledEndFrom() . ', ' .
						$this->sqlSubtaskScheduledEndTo() . ', ' .
						$this->sqlPsProductIds() . ', ' .
						$this->sqlCompanyEmployeeIds() . ', ' .
						$this->sqlIsShowSubTasks() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_keyword = ' . $this->sqlSearchKeyword(). ',' ; } elseif( true == array_key_exists( 'SearchKeyword', $this->getChangedColumns() ) ) { $strSql .= ' search_keyword = ' . $this->sqlSearchKeyword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds(). ',' ; } elseif( true == array_key_exists( 'PropertyGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_priority_ids = ' . $this->sqlMaintenancePriorityIds(). ',' ; } elseif( true == array_key_exists( 'MaintenancePriorityIds', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_priority_ids = ' . $this->sqlMaintenancePriorityIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_status_ids = ' . $this->sqlMaintenanceStatusIds(). ',' ; } elseif( true == array_key_exists( 'MaintenanceStatusIds', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_status_ids = ' . $this->sqlMaintenanceStatusIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_ids = ' . $this->sqlMaintenanceLocationIds(). ',' ; } elseif( true == array_key_exists( 'MaintenanceLocationIds', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_ids = ' . $this->sqlMaintenanceLocationIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_ids = ' . $this->sqlMaintenanceProblemIds(). ',' ; } elseif( true == array_key_exists( 'MaintenanceProblemIds', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_ids = ' . $this->sqlMaintenanceProblemIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_type_ids = ' . $this->sqlMaintenanceRequestTypeIds(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_type_ids = ' . $this->sqlMaintenanceRequestTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_status_type_ids = ' . $this->sqlUnitSpaceStatusTypeIds(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceStatusTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_status_type_ids = ' . $this->sqlUnitSpaceStatusTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_ids = ' . $this->sqlApPayeeIds(). ',' ; } elseif( true == array_key_exists( 'ApPayeeIds', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_ids = ' . $this->sqlApPayeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_ids = ' . $this->sqlPropertyBuildingIds(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingIds', $this->getChangedColumns() ) ) { $strSql .= ' property_building_ids = ' . $this->sqlPropertyBuildingIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_ids = ' . $this->sqlUnitTypeIds(). ',' ; } elseif( true == array_key_exists( 'UnitTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_ids = ' . $this->sqlUnitTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_by_field = ' . $this->sqlOrderByField(). ',' ; } elseif( true == array_key_exists( 'OrderByField', $this->getChangedColumns() ) ) { $strSql .= ' order_by_field = ' . $this->sqlOrderByField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_by_type = ' . $this->sqlOrderByType(). ',' ; } elseif( true == array_key_exists( 'OrderByType', $this->getChangedColumns() ) ) { $strSql .= ' order_by_type = ' . $this->sqlOrderByType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_on_from = ' . $this->sqlCreatedOnFrom(). ',' ; } elseif( true == array_key_exists( 'CreatedOnFrom', $this->getChangedColumns() ) ) { $strSql .= ' created_on_from = ' . $this->sqlCreatedOnFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_on_to = ' . $this->sqlCreatedOnTo(). ',' ; } elseif( true == array_key_exists( 'CreatedOnTo', $this->getChangedColumns() ) ) { $strSql .= ' created_on_to = ' . $this->sqlCreatedOnTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on_from = ' . $this->sqlCompletedOnFrom(). ',' ; } elseif( true == array_key_exists( 'CompletedOnFrom', $this->getChangedColumns() ) ) { $strSql .= ' completed_on_from = ' . $this->sqlCompletedOnFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on_to = ' . $this->sqlCompletedOnTo(). ',' ; } elseif( true == array_key_exists( 'CompletedOnTo', $this->getChangedColumns() ) ) { $strSql .= ' completed_on_to = ' . $this->sqlCompletedOnTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on_from = ' . $this->sqlScheduledOnFrom(). ',' ; } elseif( true == array_key_exists( 'ScheduledOnFrom', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on_from = ' . $this->sqlScheduledOnFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on_to = ' . $this->sqlScheduledOnTo(). ',' ; } elseif( true == array_key_exists( 'ScheduledOnTo', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on_to = ' . $this->sqlScheduledOnTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_from = ' . $this->sqlScheduledEndFrom(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndFrom', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_from = ' . $this->sqlScheduledEndFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_to = ' . $this->sqlScheduledEndTo(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndTo', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_to = ' . $this->sqlScheduledEndTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_from = ' . $this->sqlMoveInFrom(). ',' ; } elseif( true == array_key_exists( 'MoveInFrom', $this->getChangedColumns() ) ) { $strSql .= ' move_in_from = ' . $this->sqlMoveInFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_to = ' . $this->sqlMoveInTo(). ',' ; } elseif( true == array_key_exists( 'MoveInTo', $this->getChangedColumns() ) ) { $strSql .= ' move_in_to = ' . $this->sqlMoveInTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_from = ' . $this->sqlMoveOutFrom(). ',' ; } elseif( true == array_key_exists( 'MoveOutFrom', $this->getChangedColumns() ) ) { $strSql .= ' move_out_from = ' . $this->sqlMoveOutFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_to = ' . $this->sqlMoveOutTo(). ',' ; } elseif( true == array_key_exists( 'MoveOutTo', $this->getChangedColumns() ) ) { $strSql .= ' move_out_to = ' . $this->sqlMoveOutTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_on_from = ' . $this->sqlDueOnFrom(). ',' ; } elseif( true == array_key_exists( 'DueOnFrom', $this->getChangedColumns() ) ) { $strSql .= ' due_on_from = ' . $this->sqlDueOnFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_on_to = ' . $this->sqlDueOnTo(). ',' ; } elseif( true == array_key_exists( 'DueOnTo', $this->getChangedColumns() ) ) { $strSql .= ' due_on_to = ' . $this->sqlDueOnTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subtask_scheduled_end_from = ' . $this->sqlSubtaskScheduledEndFrom(). ',' ; } elseif( true == array_key_exists( 'SubtaskScheduledEndFrom', $this->getChangedColumns() ) ) { $strSql .= ' subtask_scheduled_end_from = ' . $this->sqlSubtaskScheduledEndFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subtask_scheduled_end_to = ' . $this->sqlSubtaskScheduledEndTo(). ',' ; } elseif( true == array_key_exists( 'SubtaskScheduledEndTo', $this->getChangedColumns() ) ) { $strSql .= ' subtask_scheduled_end_to = ' . $this->sqlSubtaskScheduledEndTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_ids = ' . $this->sqlPsProductIds(). ',' ; } elseif( true == array_key_exists( 'PsProductIds', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_ids = ' . $this->sqlPsProductIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_ids = ' . $this->sqlCompanyEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_ids = ' . $this->sqlCompanyEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_sub_tasks = ' . $this->sqlIsShowSubTasks(). ',' ; } elseif( true == array_key_exists( 'IsShowSubTasks', $this->getChangedColumns() ) ) { $strSql .= ' is_show_sub_tasks = ' . $this->sqlIsShowSubTasks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_type_id' => $this->getMaintenanceRequestTypeId(),
			'name' => $this->getName(),
			'search_keyword' => $this->getSearchKeyword(),
			'property_group_ids' => $this->getPropertyGroupIds(),
			'maintenance_priority_ids' => $this->getMaintenancePriorityIds(),
			'maintenance_status_ids' => $this->getMaintenanceStatusIds(),
			'maintenance_location_ids' => $this->getMaintenanceLocationIds(),
			'maintenance_problem_ids' => $this->getMaintenanceProblemIds(),
			'maintenance_request_type_ids' => $this->getMaintenanceRequestTypeIds(),
			'unit_space_status_type_ids' => $this->getUnitSpaceStatusTypeIds(),
			'ap_payee_ids' => $this->getApPayeeIds(),
			'property_building_ids' => $this->getPropertyBuildingIds(),
			'unit_type_ids' => $this->getUnitTypeIds(),
			'order_by_field' => $this->getOrderByField(),
			'order_by_type' => $this->getOrderByType(),
			'created_on_from' => $this->getCreatedOnFrom(),
			'created_on_to' => $this->getCreatedOnTo(),
			'completed_on_from' => $this->getCompletedOnFrom(),
			'completed_on_to' => $this->getCompletedOnTo(),
			'scheduled_on_from' => $this->getScheduledOnFrom(),
			'scheduled_on_to' => $this->getScheduledOnTo(),
			'scheduled_end_from' => $this->getScheduledEndFrom(),
			'scheduled_end_to' => $this->getScheduledEndTo(),
			'move_in_from' => $this->getMoveInFrom(),
			'move_in_to' => $this->getMoveInTo(),
			'move_out_from' => $this->getMoveOutFrom(),
			'move_out_to' => $this->getMoveOutTo(),
			'due_on_from' => $this->getDueOnFrom(),
			'due_on_to' => $this->getDueOnTo(),
			'subtask_scheduled_end_from' => $this->getSubtaskScheduledEndFrom(),
			'subtask_scheduled_end_to' => $this->getSubtaskScheduledEndTo(),
			'ps_product_ids' => $this->getPsProductIds(),
			'company_employee_ids' => $this->getCompanyEmployeeIds(),
			'is_show_sub_tasks' => $this->getIsShowSubTasks(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>