<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoneygramLocations
 * Do not add any new functions to this class.
 */

class CBaseMoneygramLocations extends CEosPluralBase {

	/**
	 * @return CMoneygramLocation[]
	 */
	public static function fetchMoneygramLocations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMoneygramLocation', $objDatabase );
	}

	/**
	 * @return CMoneygramLocation
	 */
	public static function fetchMoneygramLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMoneygramLocation', $objDatabase );
	}

	public static function fetchMoneygramLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'moneygram_locations', $objDatabase );
	}

	public static function fetchMoneygramLocationById( $intId, $objDatabase ) {
		return self::fetchMoneygramLocation( sprintf( 'SELECT * FROM moneygram_locations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>