<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyChargeSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_charge_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseTermStructureId;
	protected $m_intLeaseStartStructureId;
	protected $m_intAcceleratedRentTypeId;
	protected $m_intBaseRentRenewalOptionId;
	protected $m_intOtherFeesRenewalOptionId;
	protected $m_intLeaseEndBillingTypeId;
	protected $m_intRoundTypeId;
	protected $m_intMonthToMonthTypeId;
	protected $m_intMonthToMonthRentArCodeId;
	protected $m_boolEnforceUnitSpaceUnions;
	protected $m_boolProrateCharges;
	protected $m_boolIgnoreMonthToMonthIntervals;
	protected $m_boolUse30DayMonth;
	protected $m_boolTemporaryChargeGlPostDay;
	protected $m_boolChargeMoveInDay;
	protected $m_boolChargeMoveOutDay;
	protected $m_boolImmediateMoveInProrateCharges;
	protected $m_boolImmediateMoveInUse30DayMonth;
	protected $m_boolImmediateMoveInChargeMoveInDay;
	protected $m_boolWriteOffAcceleratedRent;
	protected $m_boolAutoPostScheduledCharges;
	protected $m_fltBaseRentRenewalFactor;
	protected $m_fltOtherFeesRenewalFactor;
	protected $m_intScheduledChargeAutoPostDay;
	protected $m_intScheduledChargeAutoPostThrough;
	protected $m_boolPostThroughNextMonth;
	protected $m_boolShowRentAmountOnQuickView;
	protected $m_boolStartRenewalChargesOnFirst;
	protected $m_boolStartMtmChargesOnFirst;
	protected $m_boolRequireInstallments;
	protected $m_intAvailableSpaceLimitDays;
	protected $m_intOnNoticeSpaceLimitDays;
	protected $m_intEarlyDisplayAllowanceDays;
	protected $m_boolShowAvailableNotReadySpaces;
	protected $m_boolShowNoticeAvailableSpaces;
	protected $m_boolAllowRateOffsets;
	protected $m_boolAllowLateFeeOverrides;
	protected $m_boolAllowAssetRates;
	protected $m_boolAllowEquityRates;
	protected $m_boolAllowExpenseRates;
	protected $m_boolAllowLiabilityRates;
	protected $m_boolAllowPreQualificationTrigger;
	protected $m_boolAllowApplicationApprovalTrigger;
	protected $m_boolAllowLeaseCompletedTrigger;
	protected $m_boolAllowLeaseApprovalTrigger;
	protected $m_boolAllowLastLeaseMonthTrigger;
	protected $m_boolAllowNoticeTrigger;
	protected $m_boolAllowAnniversaryOfMoveInTrigger;
	protected $m_boolAllowEndOfYearTrigger;
	protected $m_boolAllowHourlyTrigger;
	protected $m_boolAllowDailyTrigger;
	protected $m_boolAllowWeeklyTrigger;
	protected $m_boolAllowEveryTwoWeeksTrigger;
	protected $m_boolAllowTwicePerMonthTrigger;
	protected $m_boolAllowSpecificMonthsTrigger;
	protected $m_boolAllowQuarterlyTrigger;
	protected $m_boolAllowTwicePerYearTrigger;
	protected $m_boolAllowYearlyTrigger;
	protected $m_boolAllowScreeningApprovedTrigger;
	protected $m_boolPostAccelRentInCurrentPostMonth;
	protected $m_boolAllowRepaymentAgreements;
	protected $m_boolRefundAcceleratedRentAtMoveIn;
	protected $m_boolIncludeOtherInAdvertisedRent;
	protected $m_intRepaymentMaximumAllowedMonths;
	protected $m_intRepaymentAgreementArCodeId;
	protected $m_intRepaymentAllocationOrder;
	protected $m_boolRepaymentChargeLateFeesRetroactively;
	protected $m_intDefaultMtmMultiplierLeaseTermId;
	protected $m_fltDefaultMtmMultiplier;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolAllowRepaymentsForActiveLeases;
	protected $m_boolUseCurrentDateForFloorplanRange;
	protected $m_boolIncludeTaxInAdvertisedRent;
	protected $m_boolBillFutureLeases;
	protected $m_boolBillEarlyMoveIn;
	protected $m_boolUseRatesForEarlyMoveIns;
	protected $m_boolEndChargesAtNotice;
	protected $m_boolAllocateTaxesProportionately;
	protected $m_boolBillEarlyMoveInsImmediately;
	protected $m_boolAllowNightlyTrigger;
	protected $m_boolAllowRenewalOfferAcceptedTrigger;
	protected $m_boolAllowServiceSelectedTrigger;
	protected $m_boolAllowServiceFulfilledTrigger;
	protected $m_intProrationMethodTypeId;
	protected $m_boolCreateMtmCharges;
	protected $m_boolUseCurrentPostMonthForRepayments;
	protected $m_fltRepaymentMaxAmount;
	protected $m_intRepaymentMaxAmountArFormulaId;
	protected $m_intRepaymentMaxAmountArFormulaReferenceId;
	protected $m_fltRepaymentMaxPercent;
	protected $m_boolProrateStraightLineAdjustmentPosting;

	public function __construct() {
		parent::__construct();

		$this->m_intLeaseTermStructureId = '1';
		$this->m_intLeaseStartStructureId = '1';
		$this->m_intAcceleratedRentTypeId = '1';
		$this->m_intBaseRentRenewalOptionId = '3';
		$this->m_intOtherFeesRenewalOptionId = '1';
		$this->m_intLeaseEndBillingTypeId = '1';
		$this->m_intRoundTypeId = '1';
		$this->m_intMonthToMonthTypeId = '1';
		$this->m_boolEnforceUnitSpaceUnions = false;
		$this->m_boolProrateCharges = true;
		$this->m_boolIgnoreMonthToMonthIntervals = false;
		$this->m_boolUse30DayMonth = false;
		$this->m_boolTemporaryChargeGlPostDay = true;
		$this->m_boolChargeMoveInDay = true;
		$this->m_boolChargeMoveOutDay = true;
		$this->m_boolImmediateMoveInProrateCharges = true;
		$this->m_boolImmediateMoveInUse30DayMonth = false;
		$this->m_boolImmediateMoveInChargeMoveInDay = true;
		$this->m_boolWriteOffAcceleratedRent = false;
		$this->m_boolAutoPostScheduledCharges = false;
		$this->m_fltBaseRentRenewalFactor = '0';
		$this->m_fltOtherFeesRenewalFactor = '0';
		$this->m_intScheduledChargeAutoPostDay = '1';
		$this->m_intScheduledChargeAutoPostThrough = '31';
		$this->m_boolPostThroughNextMonth = false;
		$this->m_boolShowRentAmountOnQuickView = false;
		$this->m_boolStartRenewalChargesOnFirst = false;
		$this->m_boolStartMtmChargesOnFirst = false;
		$this->m_boolRequireInstallments = false;
		$this->m_intAvailableSpaceLimitDays = '10000';
		$this->m_intOnNoticeSpaceLimitDays = '10000';
		$this->m_intEarlyDisplayAllowanceDays = '0';
		$this->m_boolShowAvailableNotReadySpaces = true;
		$this->m_boolShowNoticeAvailableSpaces = true;
		$this->m_boolAllowRateOffsets = false;
		$this->m_boolAllowLateFeeOverrides = false;
		$this->m_boolAllowAssetRates = false;
		$this->m_boolAllowEquityRates = false;
		$this->m_boolAllowExpenseRates = false;
		$this->m_boolAllowLiabilityRates = false;
		$this->m_boolAllowPreQualificationTrigger = false;
		$this->m_boolAllowApplicationApprovalTrigger = false;
		$this->m_boolAllowLeaseCompletedTrigger = false;
		$this->m_boolAllowLeaseApprovalTrigger = false;
		$this->m_boolAllowLastLeaseMonthTrigger = false;
		$this->m_boolAllowNoticeTrigger = false;
		$this->m_boolAllowAnniversaryOfMoveInTrigger = false;
		$this->m_boolAllowEndOfYearTrigger = false;
		$this->m_boolAllowHourlyTrigger = false;
		$this->m_boolAllowDailyTrigger = false;
		$this->m_boolAllowWeeklyTrigger = false;
		$this->m_boolAllowEveryTwoWeeksTrigger = false;
		$this->m_boolAllowTwicePerMonthTrigger = false;
		$this->m_boolAllowSpecificMonthsTrigger = false;
		$this->m_boolAllowQuarterlyTrigger = false;
		$this->m_boolAllowTwicePerYearTrigger = false;
		$this->m_boolAllowYearlyTrigger = false;
		$this->m_boolAllowScreeningApprovedTrigger = false;
		$this->m_boolPostAccelRentInCurrentPostMonth = true;
		$this->m_boolAllowRepaymentAgreements = false;
		$this->m_boolRefundAcceleratedRentAtMoveIn = false;
		$this->m_boolIncludeOtherInAdvertisedRent = false;
		$this->m_intRepaymentMaximumAllowedMonths = '0';
		$this->m_intRepaymentAllocationOrder = '0';
		$this->m_boolRepaymentChargeLateFeesRetroactively = false;
		$this->m_fltDefaultMtmMultiplier = '1';
		$this->m_boolAllowRepaymentsForActiveLeases = false;
		$this->m_boolUseCurrentDateForFloorplanRange = true;
		$this->m_boolIncludeTaxInAdvertisedRent = false;
		$this->m_boolBillFutureLeases = false;
		$this->m_boolBillEarlyMoveIn = false;
		$this->m_boolUseRatesForEarlyMoveIns = false;
		$this->m_boolEndChargesAtNotice = true;
		$this->m_boolAllocateTaxesProportionately = false;
		$this->m_boolBillEarlyMoveInsImmediately = false;
		$this->m_boolAllowNightlyTrigger = false;
		$this->m_boolAllowRenewalOfferAcceptedTrigger = false;
		$this->m_boolAllowServiceSelectedTrigger = false;
		$this->m_boolAllowServiceFulfilledTrigger = false;
		$this->m_intProrationMethodTypeId = '1';
		$this->m_boolCreateMtmCharges = true;
		$this->m_boolUseCurrentPostMonthForRepayments = true;
		$this->m_fltRepaymentMaxAmount = '0';
		$this->m_fltRepaymentMaxPercent = '0';
		$this->m_boolProrateStraightLineAdjustmentPosting = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_term_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermStructureId', trim( $arrValues['lease_term_structure_id'] ) ); elseif( isset( $arrValues['lease_term_structure_id'] ) ) $this->setLeaseTermStructureId( $arrValues['lease_term_structure_id'] );
		if( isset( $arrValues['lease_start_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartStructureId', trim( $arrValues['lease_start_structure_id'] ) ); elseif( isset( $arrValues['lease_start_structure_id'] ) ) $this->setLeaseStartStructureId( $arrValues['lease_start_structure_id'] );
		if( isset( $arrValues['accelerated_rent_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAcceleratedRentTypeId', trim( $arrValues['accelerated_rent_type_id'] ) ); elseif( isset( $arrValues['accelerated_rent_type_id'] ) ) $this->setAcceleratedRentTypeId( $arrValues['accelerated_rent_type_id'] );
		if( isset( $arrValues['base_rent_renewal_option_id'] ) && $boolDirectSet ) $this->set( 'm_intBaseRentRenewalOptionId', trim( $arrValues['base_rent_renewal_option_id'] ) ); elseif( isset( $arrValues['base_rent_renewal_option_id'] ) ) $this->setBaseRentRenewalOptionId( $arrValues['base_rent_renewal_option_id'] );
		if( isset( $arrValues['other_fees_renewal_option_id'] ) && $boolDirectSet ) $this->set( 'm_intOtherFeesRenewalOptionId', trim( $arrValues['other_fees_renewal_option_id'] ) ); elseif( isset( $arrValues['other_fees_renewal_option_id'] ) ) $this->setOtherFeesRenewalOptionId( $arrValues['other_fees_renewal_option_id'] );
		if( isset( $arrValues['lease_end_billing_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseEndBillingTypeId', trim( $arrValues['lease_end_billing_type_id'] ) ); elseif( isset( $arrValues['lease_end_billing_type_id'] ) ) $this->setLeaseEndBillingTypeId( $arrValues['lease_end_billing_type_id'] );
		if( isset( $arrValues['round_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRoundTypeId', trim( $arrValues['round_type_id'] ) ); elseif( isset( $arrValues['round_type_id'] ) ) $this->setRoundTypeId( $arrValues['round_type_id'] );
		if( isset( $arrValues['month_to_month_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMonthToMonthTypeId', trim( $arrValues['month_to_month_type_id'] ) ); elseif( isset( $arrValues['month_to_month_type_id'] ) ) $this->setMonthToMonthTypeId( $arrValues['month_to_month_type_id'] );
		if( isset( $arrValues['month_to_month_rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intMonthToMonthRentArCodeId', trim( $arrValues['month_to_month_rent_ar_code_id'] ) ); elseif( isset( $arrValues['month_to_month_rent_ar_code_id'] ) ) $this->setMonthToMonthRentArCodeId( $arrValues['month_to_month_rent_ar_code_id'] );
		if( isset( $arrValues['enforce_unit_space_unions'] ) && $boolDirectSet ) $this->set( 'm_boolEnforceUnitSpaceUnions', trim( stripcslashes( $arrValues['enforce_unit_space_unions'] ) ) ); elseif( isset( $arrValues['enforce_unit_space_unions'] ) ) $this->setEnforceUnitSpaceUnions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['enforce_unit_space_unions'] ) : $arrValues['enforce_unit_space_unions'] );
		if( isset( $arrValues['prorate_charges'] ) && $boolDirectSet ) $this->set( 'm_boolProrateCharges', trim( stripcslashes( $arrValues['prorate_charges'] ) ) ); elseif( isset( $arrValues['prorate_charges'] ) ) $this->setProrateCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prorate_charges'] ) : $arrValues['prorate_charges'] );
		if( isset( $arrValues['ignore_month_to_month_intervals'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreMonthToMonthIntervals', trim( stripcslashes( $arrValues['ignore_month_to_month_intervals'] ) ) ); elseif( isset( $arrValues['ignore_month_to_month_intervals'] ) ) $this->setIgnoreMonthToMonthIntervals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_month_to_month_intervals'] ) : $arrValues['ignore_month_to_month_intervals'] );
		if( isset( $arrValues['use_30_day_month'] ) && $boolDirectSet ) $this->set( 'm_boolUse30DayMonth', trim( stripcslashes( $arrValues['use_30_day_month'] ) ) ); elseif( isset( $arrValues['use_30_day_month'] ) ) $this->setUse30DayMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_30_day_month'] ) : $arrValues['use_30_day_month'] );
		if( isset( $arrValues['temporary_charge_gl_post_day'] ) && $boolDirectSet ) $this->set( 'm_boolTemporaryChargeGlPostDay', trim( stripcslashes( $arrValues['temporary_charge_gl_post_day'] ) ) ); elseif( isset( $arrValues['temporary_charge_gl_post_day'] ) ) $this->setTemporaryChargeGlPostDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['temporary_charge_gl_post_day'] ) : $arrValues['temporary_charge_gl_post_day'] );
		if( isset( $arrValues['charge_move_in_day'] ) && $boolDirectSet ) $this->set( 'm_boolChargeMoveInDay', trim( stripcslashes( $arrValues['charge_move_in_day'] ) ) ); elseif( isset( $arrValues['charge_move_in_day'] ) ) $this->setChargeMoveInDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_move_in_day'] ) : $arrValues['charge_move_in_day'] );
		if( isset( $arrValues['charge_move_out_day'] ) && $boolDirectSet ) $this->set( 'm_boolChargeMoveOutDay', trim( stripcslashes( $arrValues['charge_move_out_day'] ) ) ); elseif( isset( $arrValues['charge_move_out_day'] ) ) $this->setChargeMoveOutDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_move_out_day'] ) : $arrValues['charge_move_out_day'] );
		if( isset( $arrValues['immediate_move_in_prorate_charges'] ) && $boolDirectSet ) $this->set( 'm_boolImmediateMoveInProrateCharges', trim( stripcslashes( $arrValues['immediate_move_in_prorate_charges'] ) ) ); elseif( isset( $arrValues['immediate_move_in_prorate_charges'] ) ) $this->setImmediateMoveInProrateCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['immediate_move_in_prorate_charges'] ) : $arrValues['immediate_move_in_prorate_charges'] );
		if( isset( $arrValues['immediate_move_in_use_30_day_month'] ) && $boolDirectSet ) $this->set( 'm_boolImmediateMoveInUse30DayMonth', trim( stripcslashes( $arrValues['immediate_move_in_use_30_day_month'] ) ) ); elseif( isset( $arrValues['immediate_move_in_use_30_day_month'] ) ) $this->setImmediateMoveInUse30DayMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['immediate_move_in_use_30_day_month'] ) : $arrValues['immediate_move_in_use_30_day_month'] );
		if( isset( $arrValues['immediate_move_in_charge_move_in_day'] ) && $boolDirectSet ) $this->set( 'm_boolImmediateMoveInChargeMoveInDay', trim( stripcslashes( $arrValues['immediate_move_in_charge_move_in_day'] ) ) ); elseif( isset( $arrValues['immediate_move_in_charge_move_in_day'] ) ) $this->setImmediateMoveInChargeMoveInDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['immediate_move_in_charge_move_in_day'] ) : $arrValues['immediate_move_in_charge_move_in_day'] );
		if( isset( $arrValues['write_off_accelerated_rent'] ) && $boolDirectSet ) $this->set( 'm_boolWriteOffAcceleratedRent', trim( stripcslashes( $arrValues['write_off_accelerated_rent'] ) ) ); elseif( isset( $arrValues['write_off_accelerated_rent'] ) ) $this->setWriteOffAcceleratedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['write_off_accelerated_rent'] ) : $arrValues['write_off_accelerated_rent'] );
		if( isset( $arrValues['auto_post_scheduled_charges'] ) && $boolDirectSet ) $this->set( 'm_boolAutoPostScheduledCharges', trim( stripcslashes( $arrValues['auto_post_scheduled_charges'] ) ) ); elseif( isset( $arrValues['auto_post_scheduled_charges'] ) ) $this->setAutoPostScheduledCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_post_scheduled_charges'] ) : $arrValues['auto_post_scheduled_charges'] );
		if( isset( $arrValues['base_rent_renewal_factor'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRentRenewalFactor', trim( $arrValues['base_rent_renewal_factor'] ) ); elseif( isset( $arrValues['base_rent_renewal_factor'] ) ) $this->setBaseRentRenewalFactor( $arrValues['base_rent_renewal_factor'] );
		if( isset( $arrValues['other_fees_renewal_factor'] ) && $boolDirectSet ) $this->set( 'm_fltOtherFeesRenewalFactor', trim( $arrValues['other_fees_renewal_factor'] ) ); elseif( isset( $arrValues['other_fees_renewal_factor'] ) ) $this->setOtherFeesRenewalFactor( $arrValues['other_fees_renewal_factor'] );
		if( isset( $arrValues['scheduled_charge_auto_post_day'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeAutoPostDay', trim( $arrValues['scheduled_charge_auto_post_day'] ) ); elseif( isset( $arrValues['scheduled_charge_auto_post_day'] ) ) $this->setScheduledChargeAutoPostDay( $arrValues['scheduled_charge_auto_post_day'] );
		if( isset( $arrValues['scheduled_charge_auto_post_through'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeAutoPostThrough', trim( $arrValues['scheduled_charge_auto_post_through'] ) ); elseif( isset( $arrValues['scheduled_charge_auto_post_through'] ) ) $this->setScheduledChargeAutoPostThrough( $arrValues['scheduled_charge_auto_post_through'] );
		if( isset( $arrValues['post_through_next_month'] ) && $boolDirectSet ) $this->set( 'm_boolPostThroughNextMonth', trim( stripcslashes( $arrValues['post_through_next_month'] ) ) ); elseif( isset( $arrValues['post_through_next_month'] ) ) $this->setPostThroughNextMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_through_next_month'] ) : $arrValues['post_through_next_month'] );
		if( isset( $arrValues['show_rent_amount_on_quick_view'] ) && $boolDirectSet ) $this->set( 'm_boolShowRentAmountOnQuickView', trim( stripcslashes( $arrValues['show_rent_amount_on_quick_view'] ) ) ); elseif( isset( $arrValues['show_rent_amount_on_quick_view'] ) ) $this->setShowRentAmountOnQuickView( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_rent_amount_on_quick_view'] ) : $arrValues['show_rent_amount_on_quick_view'] );
		if( isset( $arrValues['start_renewal_charges_on_first'] ) && $boolDirectSet ) $this->set( 'm_boolStartRenewalChargesOnFirst', trim( stripcslashes( $arrValues['start_renewal_charges_on_first'] ) ) ); elseif( isset( $arrValues['start_renewal_charges_on_first'] ) ) $this->setStartRenewalChargesOnFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['start_renewal_charges_on_first'] ) : $arrValues['start_renewal_charges_on_first'] );
		if( isset( $arrValues['start_mtm_charges_on_first'] ) && $boolDirectSet ) $this->set( 'm_boolStartMtmChargesOnFirst', trim( stripcslashes( $arrValues['start_mtm_charges_on_first'] ) ) ); elseif( isset( $arrValues['start_mtm_charges_on_first'] ) ) $this->setStartMtmChargesOnFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['start_mtm_charges_on_first'] ) : $arrValues['start_mtm_charges_on_first'] );
		if( isset( $arrValues['require_installments'] ) && $boolDirectSet ) $this->set( 'm_boolRequireInstallments', trim( stripcslashes( $arrValues['require_installments'] ) ) ); elseif( isset( $arrValues['require_installments'] ) ) $this->setRequireInstallments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_installments'] ) : $arrValues['require_installments'] );
		if( isset( $arrValues['available_space_limit_days'] ) && $boolDirectSet ) $this->set( 'm_intAvailableSpaceLimitDays', trim( $arrValues['available_space_limit_days'] ) ); elseif( isset( $arrValues['available_space_limit_days'] ) ) $this->setAvailableSpaceLimitDays( $arrValues['available_space_limit_days'] );
		if( isset( $arrValues['on_notice_space_limit_days'] ) && $boolDirectSet ) $this->set( 'm_intOnNoticeSpaceLimitDays', trim( $arrValues['on_notice_space_limit_days'] ) ); elseif( isset( $arrValues['on_notice_space_limit_days'] ) ) $this->setOnNoticeSpaceLimitDays( $arrValues['on_notice_space_limit_days'] );
		if( isset( $arrValues['early_display_allowance_days'] ) && $boolDirectSet ) $this->set( 'm_intEarlyDisplayAllowanceDays', trim( $arrValues['early_display_allowance_days'] ) ); elseif( isset( $arrValues['early_display_allowance_days'] ) ) $this->setEarlyDisplayAllowanceDays( $arrValues['early_display_allowance_days'] );
		if( isset( $arrValues['show_available_not_ready_spaces'] ) && $boolDirectSet ) $this->set( 'm_boolShowAvailableNotReadySpaces', trim( stripcslashes( $arrValues['show_available_not_ready_spaces'] ) ) ); elseif( isset( $arrValues['show_available_not_ready_spaces'] ) ) $this->setShowAvailableNotReadySpaces( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_available_not_ready_spaces'] ) : $arrValues['show_available_not_ready_spaces'] );
		if( isset( $arrValues['show_notice_available_spaces'] ) && $boolDirectSet ) $this->set( 'm_boolShowNoticeAvailableSpaces', trim( stripcslashes( $arrValues['show_notice_available_spaces'] ) ) ); elseif( isset( $arrValues['show_notice_available_spaces'] ) ) $this->setShowNoticeAvailableSpaces( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_notice_available_spaces'] ) : $arrValues['show_notice_available_spaces'] );
		if( isset( $arrValues['allow_rate_offsets'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRateOffsets', trim( stripcslashes( $arrValues['allow_rate_offsets'] ) ) ); elseif( isset( $arrValues['allow_rate_offsets'] ) ) $this->setAllowRateOffsets( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_rate_offsets'] ) : $arrValues['allow_rate_offsets'] );
		if( isset( $arrValues['allow_late_fee_overrides'] ) && $boolDirectSet ) $this->set( 'm_boolAllowLateFeeOverrides', trim( stripcslashes( $arrValues['allow_late_fee_overrides'] ) ) ); elseif( isset( $arrValues['allow_late_fee_overrides'] ) ) $this->setAllowLateFeeOverrides( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_late_fee_overrides'] ) : $arrValues['allow_late_fee_overrides'] );
		if( isset( $arrValues['allow_asset_rates'] ) && $boolDirectSet ) $this->set( 'm_boolAllowAssetRates', trim( stripcslashes( $arrValues['allow_asset_rates'] ) ) ); elseif( isset( $arrValues['allow_asset_rates'] ) ) $this->setAllowAssetRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_asset_rates'] ) : $arrValues['allow_asset_rates'] );
		if( isset( $arrValues['allow_equity_rates'] ) && $boolDirectSet ) $this->set( 'm_boolAllowEquityRates', trim( stripcslashes( $arrValues['allow_equity_rates'] ) ) ); elseif( isset( $arrValues['allow_equity_rates'] ) ) $this->setAllowEquityRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_equity_rates'] ) : $arrValues['allow_equity_rates'] );
		if( isset( $arrValues['allow_expense_rates'] ) && $boolDirectSet ) $this->set( 'm_boolAllowExpenseRates', trim( stripcslashes( $arrValues['allow_expense_rates'] ) ) ); elseif( isset( $arrValues['allow_expense_rates'] ) ) $this->setAllowExpenseRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_expense_rates'] ) : $arrValues['allow_expense_rates'] );
		if( isset( $arrValues['allow_liability_rates'] ) && $boolDirectSet ) $this->set( 'm_boolAllowLiabilityRates', trim( stripcslashes( $arrValues['allow_liability_rates'] ) ) ); elseif( isset( $arrValues['allow_liability_rates'] ) ) $this->setAllowLiabilityRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_liability_rates'] ) : $arrValues['allow_liability_rates'] );
		if( isset( $arrValues['allow_pre_qualification_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPreQualificationTrigger', trim( stripcslashes( $arrValues['allow_pre_qualification_trigger'] ) ) ); elseif( isset( $arrValues['allow_pre_qualification_trigger'] ) ) $this->setAllowPreQualificationTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pre_qualification_trigger'] ) : $arrValues['allow_pre_qualification_trigger'] );
		if( isset( $arrValues['allow_application_approval_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowApplicationApprovalTrigger', trim( stripcslashes( $arrValues['allow_application_approval_trigger'] ) ) ); elseif( isset( $arrValues['allow_application_approval_trigger'] ) ) $this->setAllowApplicationApprovalTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_application_approval_trigger'] ) : $arrValues['allow_application_approval_trigger'] );
		if( isset( $arrValues['allow_lease_completed_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowLeaseCompletedTrigger', trim( stripcslashes( $arrValues['allow_lease_completed_trigger'] ) ) ); elseif( isset( $arrValues['allow_lease_completed_trigger'] ) ) $this->setAllowLeaseCompletedTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_lease_completed_trigger'] ) : $arrValues['allow_lease_completed_trigger'] );
		if( isset( $arrValues['allow_lease_approval_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowLeaseApprovalTrigger', trim( stripcslashes( $arrValues['allow_lease_approval_trigger'] ) ) ); elseif( isset( $arrValues['allow_lease_approval_trigger'] ) ) $this->setAllowLeaseApprovalTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_lease_approval_trigger'] ) : $arrValues['allow_lease_approval_trigger'] );
		if( isset( $arrValues['allow_last_lease_month_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowLastLeaseMonthTrigger', trim( stripcslashes( $arrValues['allow_last_lease_month_trigger'] ) ) ); elseif( isset( $arrValues['allow_last_lease_month_trigger'] ) ) $this->setAllowLastLeaseMonthTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_last_lease_month_trigger'] ) : $arrValues['allow_last_lease_month_trigger'] );
		if( isset( $arrValues['allow_notice_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNoticeTrigger', trim( stripcslashes( $arrValues['allow_notice_trigger'] ) ) ); elseif( isset( $arrValues['allow_notice_trigger'] ) ) $this->setAllowNoticeTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_notice_trigger'] ) : $arrValues['allow_notice_trigger'] );
		if( isset( $arrValues['allow_anniversary_of_move_in_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowAnniversaryOfMoveInTrigger', trim( stripcslashes( $arrValues['allow_anniversary_of_move_in_trigger'] ) ) ); elseif( isset( $arrValues['allow_anniversary_of_move_in_trigger'] ) ) $this->setAllowAnniversaryOfMoveInTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_anniversary_of_move_in_trigger'] ) : $arrValues['allow_anniversary_of_move_in_trigger'] );
		if( isset( $arrValues['allow_end_of_year_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowEndOfYearTrigger', trim( stripcslashes( $arrValues['allow_end_of_year_trigger'] ) ) ); elseif( isset( $arrValues['allow_end_of_year_trigger'] ) ) $this->setAllowEndOfYearTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_end_of_year_trigger'] ) : $arrValues['allow_end_of_year_trigger'] );
		if( isset( $arrValues['allow_hourly_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowHourlyTrigger', trim( stripcslashes( $arrValues['allow_hourly_trigger'] ) ) ); elseif( isset( $arrValues['allow_hourly_trigger'] ) ) $this->setAllowHourlyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_hourly_trigger'] ) : $arrValues['allow_hourly_trigger'] );
		if( isset( $arrValues['allow_daily_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowDailyTrigger', trim( stripcslashes( $arrValues['allow_daily_trigger'] ) ) ); elseif( isset( $arrValues['allow_daily_trigger'] ) ) $this->setAllowDailyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_daily_trigger'] ) : $arrValues['allow_daily_trigger'] );
		if( isset( $arrValues['allow_weekly_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowWeeklyTrigger', trim( stripcslashes( $arrValues['allow_weekly_trigger'] ) ) ); elseif( isset( $arrValues['allow_weekly_trigger'] ) ) $this->setAllowWeeklyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_weekly_trigger'] ) : $arrValues['allow_weekly_trigger'] );
		if( isset( $arrValues['allow_every_two_weeks_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowEveryTwoWeeksTrigger', trim( stripcslashes( $arrValues['allow_every_two_weeks_trigger'] ) ) ); elseif( isset( $arrValues['allow_every_two_weeks_trigger'] ) ) $this->setAllowEveryTwoWeeksTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_every_two_weeks_trigger'] ) : $arrValues['allow_every_two_weeks_trigger'] );
		if( isset( $arrValues['allow_twice_per_month_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowTwicePerMonthTrigger', trim( stripcslashes( $arrValues['allow_twice_per_month_trigger'] ) ) ); elseif( isset( $arrValues['allow_twice_per_month_trigger'] ) ) $this->setAllowTwicePerMonthTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_twice_per_month_trigger'] ) : $arrValues['allow_twice_per_month_trigger'] );
		if( isset( $arrValues['allow_specific_months_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowSpecificMonthsTrigger', trim( stripcslashes( $arrValues['allow_specific_months_trigger'] ) ) ); elseif( isset( $arrValues['allow_specific_months_trigger'] ) ) $this->setAllowSpecificMonthsTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_specific_months_trigger'] ) : $arrValues['allow_specific_months_trigger'] );
		if( isset( $arrValues['allow_quarterly_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowQuarterlyTrigger', trim( stripcslashes( $arrValues['allow_quarterly_trigger'] ) ) ); elseif( isset( $arrValues['allow_quarterly_trigger'] ) ) $this->setAllowQuarterlyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_quarterly_trigger'] ) : $arrValues['allow_quarterly_trigger'] );
		if( isset( $arrValues['allow_twice_per_year_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowTwicePerYearTrigger', trim( stripcslashes( $arrValues['allow_twice_per_year_trigger'] ) ) ); elseif( isset( $arrValues['allow_twice_per_year_trigger'] ) ) $this->setAllowTwicePerYearTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_twice_per_year_trigger'] ) : $arrValues['allow_twice_per_year_trigger'] );
		if( isset( $arrValues['allow_yearly_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowYearlyTrigger', trim( stripcslashes( $arrValues['allow_yearly_trigger'] ) ) ); elseif( isset( $arrValues['allow_yearly_trigger'] ) ) $this->setAllowYearlyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_yearly_trigger'] ) : $arrValues['allow_yearly_trigger'] );
		if( isset( $arrValues['allow_screening_approved_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowScreeningApprovedTrigger', trim( stripcslashes( $arrValues['allow_screening_approved_trigger'] ) ) ); elseif( isset( $arrValues['allow_screening_approved_trigger'] ) ) $this->setAllowScreeningApprovedTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_screening_approved_trigger'] ) : $arrValues['allow_screening_approved_trigger'] );
		if( isset( $arrValues['post_accel_rent_in_current_post_month'] ) && $boolDirectSet ) $this->set( 'm_boolPostAccelRentInCurrentPostMonth', trim( stripcslashes( $arrValues['post_accel_rent_in_current_post_month'] ) ) ); elseif( isset( $arrValues['post_accel_rent_in_current_post_month'] ) ) $this->setPostAccelRentInCurrentPostMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_accel_rent_in_current_post_month'] ) : $arrValues['post_accel_rent_in_current_post_month'] );
		if( isset( $arrValues['allow_repayment_agreements'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRepaymentAgreements', trim( stripcslashes( $arrValues['allow_repayment_agreements'] ) ) ); elseif( isset( $arrValues['allow_repayment_agreements'] ) ) $this->setAllowRepaymentAgreements( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_repayment_agreements'] ) : $arrValues['allow_repayment_agreements'] );
		if( isset( $arrValues['refund_accelerated_rent_at_move_in'] ) && $boolDirectSet ) $this->set( 'm_boolRefundAcceleratedRentAtMoveIn', trim( stripcslashes( $arrValues['refund_accelerated_rent_at_move_in'] ) ) ); elseif( isset( $arrValues['refund_accelerated_rent_at_move_in'] ) ) $this->setRefundAcceleratedRentAtMoveIn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['refund_accelerated_rent_at_move_in'] ) : $arrValues['refund_accelerated_rent_at_move_in'] );
		if( isset( $arrValues['include_other_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeOtherInAdvertisedRent', trim( stripcslashes( $arrValues['include_other_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_other_in_advertised_rent'] ) ) $this->setIncludeOtherInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_other_in_advertised_rent'] ) : $arrValues['include_other_in_advertised_rent'] );
		if( isset( $arrValues['repayment_maximum_allowed_months'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentMaximumAllowedMonths', trim( $arrValues['repayment_maximum_allowed_months'] ) ); elseif( isset( $arrValues['repayment_maximum_allowed_months'] ) ) $this->setRepaymentMaximumAllowedMonths( $arrValues['repayment_maximum_allowed_months'] );
		if( isset( $arrValues['repayment_agreement_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentAgreementArCodeId', trim( $arrValues['repayment_agreement_ar_code_id'] ) ); elseif( isset( $arrValues['repayment_agreement_ar_code_id'] ) ) $this->setRepaymentAgreementArCodeId( $arrValues['repayment_agreement_ar_code_id'] );
		if( isset( $arrValues['repayment_allocation_order'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentAllocationOrder', trim( $arrValues['repayment_allocation_order'] ) ); elseif( isset( $arrValues['repayment_allocation_order'] ) ) $this->setRepaymentAllocationOrder( $arrValues['repayment_allocation_order'] );
		if( isset( $arrValues['repayment_charge_late_fees_retroactively'] ) && $boolDirectSet ) $this->set( 'm_boolRepaymentChargeLateFeesRetroactively', trim( stripcslashes( $arrValues['repayment_charge_late_fees_retroactively'] ) ) ); elseif( isset( $arrValues['repayment_charge_late_fees_retroactively'] ) ) $this->setRepaymentChargeLateFeesRetroactively( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['repayment_charge_late_fees_retroactively'] ) : $arrValues['repayment_charge_late_fees_retroactively'] );
		if( isset( $arrValues['default_mtm_multiplier_lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMtmMultiplierLeaseTermId', trim( $arrValues['default_mtm_multiplier_lease_term_id'] ) ); elseif( isset( $arrValues['default_mtm_multiplier_lease_term_id'] ) ) $this->setDefaultMtmMultiplierLeaseTermId( $arrValues['default_mtm_multiplier_lease_term_id'] );
		if( isset( $arrValues['default_mtm_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultMtmMultiplier', trim( $arrValues['default_mtm_multiplier'] ) ); elseif( isset( $arrValues['default_mtm_multiplier'] ) ) $this->setDefaultMtmMultiplier( $arrValues['default_mtm_multiplier'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['allow_repayments_for_active_leases'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRepaymentsForActiveLeases', trim( stripcslashes( $arrValues['allow_repayments_for_active_leases'] ) ) ); elseif( isset( $arrValues['allow_repayments_for_active_leases'] ) ) $this->setAllowRepaymentsForActiveLeases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_repayments_for_active_leases'] ) : $arrValues['allow_repayments_for_active_leases'] );
		if( isset( $arrValues['use_current_date_for_floorplan_range'] ) && $boolDirectSet ) $this->set( 'm_boolUseCurrentDateForFloorplanRange', trim( stripcslashes( $arrValues['use_current_date_for_floorplan_range'] ) ) ); elseif( isset( $arrValues['use_current_date_for_floorplan_range'] ) ) $this->setUseCurrentDateForFloorplanRange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_current_date_for_floorplan_range'] ) : $arrValues['use_current_date_for_floorplan_range'] );
		if( isset( $arrValues['include_tax_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeTaxInAdvertisedRent', trim( stripcslashes( $arrValues['include_tax_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_tax_in_advertised_rent'] ) ) $this->setIncludeTaxInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_tax_in_advertised_rent'] ) : $arrValues['include_tax_in_advertised_rent'] );
		if( isset( $arrValues['bill_future_leases'] ) && $boolDirectSet ) $this->set( 'm_boolBillFutureLeases', trim( stripcslashes( $arrValues['bill_future_leases'] ) ) ); elseif( isset( $arrValues['bill_future_leases'] ) ) $this->setBillFutureLeases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bill_future_leases'] ) : $arrValues['bill_future_leases'] );
		if( isset( $arrValues['bill_early_move_in'] ) && $boolDirectSet ) $this->set( 'm_boolBillEarlyMoveIn', trim( stripcslashes( $arrValues['bill_early_move_in'] ) ) ); elseif( isset( $arrValues['bill_early_move_in'] ) ) $this->setBillEarlyMoveIn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bill_early_move_in'] ) : $arrValues['bill_early_move_in'] );
		if( isset( $arrValues['use_rates_for_early_move_ins'] ) && $boolDirectSet ) $this->set( 'm_boolUseRatesForEarlyMoveIns', trim( stripcslashes( $arrValues['use_rates_for_early_move_ins'] ) ) ); elseif( isset( $arrValues['use_rates_for_early_move_ins'] ) ) $this->setUseRatesForEarlyMoveIns( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_rates_for_early_move_ins'] ) : $arrValues['use_rates_for_early_move_ins'] );
		if( isset( $arrValues['end_charges_at_notice'] ) && $boolDirectSet ) $this->set( 'm_boolEndChargesAtNotice', trim( stripcslashes( $arrValues['end_charges_at_notice'] ) ) ); elseif( isset( $arrValues['end_charges_at_notice'] ) ) $this->setEndChargesAtNotice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['end_charges_at_notice'] ) : $arrValues['end_charges_at_notice'] );
		if( isset( $arrValues['allocate_taxes_proportionately'] ) && $boolDirectSet ) $this->set( 'm_boolAllocateTaxesProportionately', trim( stripcslashes( $arrValues['allocate_taxes_proportionately'] ) ) ); elseif( isset( $arrValues['allocate_taxes_proportionately'] ) ) $this->setAllocateTaxesProportionately( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allocate_taxes_proportionately'] ) : $arrValues['allocate_taxes_proportionately'] );
		if( isset( $arrValues['bill_early_move_ins_immediately'] ) && $boolDirectSet ) $this->set( 'm_boolBillEarlyMoveInsImmediately', trim( stripcslashes( $arrValues['bill_early_move_ins_immediately'] ) ) ); elseif( isset( $arrValues['bill_early_move_ins_immediately'] ) ) $this->setBillEarlyMoveInsImmediately( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bill_early_move_ins_immediately'] ) : $arrValues['bill_early_move_ins_immediately'] );
		if( isset( $arrValues['allow_nightly_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNightlyTrigger', trim( stripcslashes( $arrValues['allow_nightly_trigger'] ) ) ); elseif( isset( $arrValues['allow_nightly_trigger'] ) ) $this->setAllowNightlyTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_nightly_trigger'] ) : $arrValues['allow_nightly_trigger'] );
		if( isset( $arrValues['allow_renewal_offer_accepted_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRenewalOfferAcceptedTrigger', trim( stripcslashes( $arrValues['allow_renewal_offer_accepted_trigger'] ) ) ); elseif( isset( $arrValues['allow_renewal_offer_accepted_trigger'] ) ) $this->setAllowRenewalOfferAcceptedTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_renewal_offer_accepted_trigger'] ) : $arrValues['allow_renewal_offer_accepted_trigger'] );
		if( isset( $arrValues['allow_service_selected_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowServiceSelectedTrigger', trim( stripcslashes( $arrValues['allow_service_selected_trigger'] ) ) ); elseif( isset( $arrValues['allow_service_selected_trigger'] ) ) $this->setAllowServiceSelectedTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_service_selected_trigger'] ) : $arrValues['allow_service_selected_trigger'] );
		if( isset( $arrValues['allow_service_fulfilled_trigger'] ) && $boolDirectSet ) $this->set( 'm_boolAllowServiceFulfilledTrigger', trim( stripcslashes( $arrValues['allow_service_fulfilled_trigger'] ) ) ); elseif( isset( $arrValues['allow_service_fulfilled_trigger'] ) ) $this->setAllowServiceFulfilledTrigger( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_service_fulfilled_trigger'] ) : $arrValues['allow_service_fulfilled_trigger'] );
		if( isset( $arrValues['proration_method_type_id'] ) && $boolDirectSet ) $this->set( 'm_intProrationMethodTypeId', trim( $arrValues['proration_method_type_id'] ) ); elseif( isset( $arrValues['proration_method_type_id'] ) ) $this->setProrationMethodTypeId( $arrValues['proration_method_type_id'] );
		if( isset( $arrValues['create_mtm_charges'] ) && $boolDirectSet ) $this->set( 'm_boolCreateMtmCharges', trim( stripcslashes( $arrValues['create_mtm_charges'] ) ) ); elseif( isset( $arrValues['create_mtm_charges'] ) ) $this->setCreateMtmCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['create_mtm_charges'] ) : $arrValues['create_mtm_charges'] );
		if( isset( $arrValues['use_current_post_month_for_repayments'] ) && $boolDirectSet ) $this->set( 'm_boolUseCurrentPostMonthForRepayments', trim( stripcslashes( $arrValues['use_current_post_month_for_repayments'] ) ) ); elseif( isset( $arrValues['use_current_post_month_for_repayments'] ) ) $this->setUseCurrentPostMonthForRepayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_current_post_month_for_repayments'] ) : $arrValues['use_current_post_month_for_repayments'] );
		if( isset( $arrValues['repayment_max_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRepaymentMaxAmount', trim( $arrValues['repayment_max_amount'] ) ); elseif( isset( $arrValues['repayment_max_amount'] ) ) $this->setRepaymentMaxAmount( $arrValues['repayment_max_amount'] );
		if( isset( $arrValues['repayment_max_amount_ar_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentMaxAmountArFormulaId', trim( $arrValues['repayment_max_amount_ar_formula_id'] ) ); elseif( isset( $arrValues['repayment_max_amount_ar_formula_id'] ) ) $this->setRepaymentMaxAmountArFormulaId( $arrValues['repayment_max_amount_ar_formula_id'] );
		if( isset( $arrValues['repayment_max_amount_ar_formula_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentMaxAmountArFormulaReferenceId', trim( $arrValues['repayment_max_amount_ar_formula_reference_id'] ) ); elseif( isset( $arrValues['repayment_max_amount_ar_formula_reference_id'] ) ) $this->setRepaymentMaxAmountArFormulaReferenceId( $arrValues['repayment_max_amount_ar_formula_reference_id'] );
		if( isset( $arrValues['repayment_max_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRepaymentMaxPercent', trim( $arrValues['repayment_max_percent'] ) ); elseif( isset( $arrValues['repayment_max_percent'] ) ) $this->setRepaymentMaxPercent( $arrValues['repayment_max_percent'] );
		if( isset( $arrValues['prorate_straight_line_adjustment_posting'] ) && $boolDirectSet ) $this->set( 'm_boolProrateStraightLineAdjustmentPosting', trim( stripcslashes( $arrValues['prorate_straight_line_adjustment_posting'] ) ) ); elseif( isset( $arrValues['prorate_straight_line_adjustment_posting'] ) ) $this->setProrateStraightLineAdjustmentPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prorate_straight_line_adjustment_posting'] ) : $arrValues['prorate_straight_line_adjustment_posting'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseTermStructureId( $intLeaseTermStructureId ) {
		$this->set( 'm_intLeaseTermStructureId', CStrings::strToIntDef( $intLeaseTermStructureId, NULL, false ) );
	}

	public function getLeaseTermStructureId() {
		return $this->m_intLeaseTermStructureId;
	}

	public function sqlLeaseTermStructureId() {
		return ( true == isset( $this->m_intLeaseTermStructureId ) ) ? ( string ) $this->m_intLeaseTermStructureId : '1';
	}

	public function setLeaseStartStructureId( $intLeaseStartStructureId ) {
		$this->set( 'm_intLeaseStartStructureId', CStrings::strToIntDef( $intLeaseStartStructureId, NULL, false ) );
	}

	public function getLeaseStartStructureId() {
		return $this->m_intLeaseStartStructureId;
	}

	public function sqlLeaseStartStructureId() {
		return ( true == isset( $this->m_intLeaseStartStructureId ) ) ? ( string ) $this->m_intLeaseStartStructureId : '1';
	}

	public function setAcceleratedRentTypeId( $intAcceleratedRentTypeId ) {
		$this->set( 'm_intAcceleratedRentTypeId', CStrings::strToIntDef( $intAcceleratedRentTypeId, NULL, false ) );
	}

	public function getAcceleratedRentTypeId() {
		return $this->m_intAcceleratedRentTypeId;
	}

	public function sqlAcceleratedRentTypeId() {
		return ( true == isset( $this->m_intAcceleratedRentTypeId ) ) ? ( string ) $this->m_intAcceleratedRentTypeId : '1';
	}

	public function setBaseRentRenewalOptionId( $intBaseRentRenewalOptionId ) {
		$this->set( 'm_intBaseRentRenewalOptionId', CStrings::strToIntDef( $intBaseRentRenewalOptionId, NULL, false ) );
	}

	public function getBaseRentRenewalOptionId() {
		return $this->m_intBaseRentRenewalOptionId;
	}

	public function sqlBaseRentRenewalOptionId() {
		return ( true == isset( $this->m_intBaseRentRenewalOptionId ) ) ? ( string ) $this->m_intBaseRentRenewalOptionId : '3';
	}

	public function setOtherFeesRenewalOptionId( $intOtherFeesRenewalOptionId ) {
		$this->set( 'm_intOtherFeesRenewalOptionId', CStrings::strToIntDef( $intOtherFeesRenewalOptionId, NULL, false ) );
	}

	public function getOtherFeesRenewalOptionId() {
		return $this->m_intOtherFeesRenewalOptionId;
	}

	public function sqlOtherFeesRenewalOptionId() {
		return ( true == isset( $this->m_intOtherFeesRenewalOptionId ) ) ? ( string ) $this->m_intOtherFeesRenewalOptionId : '1';
	}

	public function setLeaseEndBillingTypeId( $intLeaseEndBillingTypeId ) {
		$this->set( 'm_intLeaseEndBillingTypeId', CStrings::strToIntDef( $intLeaseEndBillingTypeId, NULL, false ) );
	}

	public function getLeaseEndBillingTypeId() {
		return $this->m_intLeaseEndBillingTypeId;
	}

	public function sqlLeaseEndBillingTypeId() {
		return ( true == isset( $this->m_intLeaseEndBillingTypeId ) ) ? ( string ) $this->m_intLeaseEndBillingTypeId : '1';
	}

	public function setRoundTypeId( $intRoundTypeId ) {
		$this->set( 'm_intRoundTypeId', CStrings::strToIntDef( $intRoundTypeId, NULL, false ) );
	}

	public function getRoundTypeId() {
		return $this->m_intRoundTypeId;
	}

	public function sqlRoundTypeId() {
		return ( true == isset( $this->m_intRoundTypeId ) ) ? ( string ) $this->m_intRoundTypeId : '1';
	}

	public function setMonthToMonthTypeId( $intMonthToMonthTypeId ) {
		$this->set( 'm_intMonthToMonthTypeId', CStrings::strToIntDef( $intMonthToMonthTypeId, NULL, false ) );
	}

	public function getMonthToMonthTypeId() {
		return $this->m_intMonthToMonthTypeId;
	}

	public function sqlMonthToMonthTypeId() {
		return ( true == isset( $this->m_intMonthToMonthTypeId ) ) ? ( string ) $this->m_intMonthToMonthTypeId : '1';
	}

	public function setMonthToMonthRentArCodeId( $intMonthToMonthRentArCodeId ) {
		$this->set( 'm_intMonthToMonthRentArCodeId', CStrings::strToIntDef( $intMonthToMonthRentArCodeId, NULL, false ) );
	}

	public function getMonthToMonthRentArCodeId() {
		return $this->m_intMonthToMonthRentArCodeId;
	}

	public function sqlMonthToMonthRentArCodeId() {
		return ( true == isset( $this->m_intMonthToMonthRentArCodeId ) ) ? ( string ) $this->m_intMonthToMonthRentArCodeId : 'NULL';
	}

	public function setEnforceUnitSpaceUnions( $boolEnforceUnitSpaceUnions ) {
		$this->set( 'm_boolEnforceUnitSpaceUnions', CStrings::strToBool( $boolEnforceUnitSpaceUnions ) );
	}

	public function getEnforceUnitSpaceUnions() {
		return $this->m_boolEnforceUnitSpaceUnions;
	}

	public function sqlEnforceUnitSpaceUnions() {
		return ( true == isset( $this->m_boolEnforceUnitSpaceUnions ) ) ? '\'' . ( true == ( bool ) $this->m_boolEnforceUnitSpaceUnions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProrateCharges( $boolProrateCharges ) {
		$this->set( 'm_boolProrateCharges', CStrings::strToBool( $boolProrateCharges ) );
	}

	public function getProrateCharges() {
		return $this->m_boolProrateCharges;
	}

	public function sqlProrateCharges() {
		return ( true == isset( $this->m_boolProrateCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolProrateCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIgnoreMonthToMonthIntervals( $boolIgnoreMonthToMonthIntervals ) {
		$this->set( 'm_boolIgnoreMonthToMonthIntervals', CStrings::strToBool( $boolIgnoreMonthToMonthIntervals ) );
	}

	public function getIgnoreMonthToMonthIntervals() {
		return $this->m_boolIgnoreMonthToMonthIntervals;
	}

	public function sqlIgnoreMonthToMonthIntervals() {
		return ( true == isset( $this->m_boolIgnoreMonthToMonthIntervals ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreMonthToMonthIntervals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUse30DayMonth( $boolUse30DayMonth ) {
		$this->set( 'm_boolUse30DayMonth', CStrings::strToBool( $boolUse30DayMonth ) );
	}

	public function getUse30DayMonth() {
		return $this->m_boolUse30DayMonth;
	}

	public function sqlUse30DayMonth() {
		return ( true == isset( $this->m_boolUse30DayMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolUse30DayMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTemporaryChargeGlPostDay( $boolTemporaryChargeGlPostDay ) {
		$this->set( 'm_boolTemporaryChargeGlPostDay', CStrings::strToBool( $boolTemporaryChargeGlPostDay ) );
	}

	public function getTemporaryChargeGlPostDay() {
		return $this->m_boolTemporaryChargeGlPostDay;
	}

	public function sqlTemporaryChargeGlPostDay() {
		return ( true == isset( $this->m_boolTemporaryChargeGlPostDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolTemporaryChargeGlPostDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeMoveInDay( $boolChargeMoveInDay ) {
		$this->set( 'm_boolChargeMoveInDay', CStrings::strToBool( $boolChargeMoveInDay ) );
	}

	public function getChargeMoveInDay() {
		return $this->m_boolChargeMoveInDay;
	}

	public function sqlChargeMoveInDay() {
		return ( true == isset( $this->m_boolChargeMoveInDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeMoveInDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeMoveOutDay( $boolChargeMoveOutDay ) {
		$this->set( 'm_boolChargeMoveOutDay', CStrings::strToBool( $boolChargeMoveOutDay ) );
	}

	public function getChargeMoveOutDay() {
		return $this->m_boolChargeMoveOutDay;
	}

	public function sqlChargeMoveOutDay() {
		return ( true == isset( $this->m_boolChargeMoveOutDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeMoveOutDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImmediateMoveInProrateCharges( $boolImmediateMoveInProrateCharges ) {
		$this->set( 'm_boolImmediateMoveInProrateCharges', CStrings::strToBool( $boolImmediateMoveInProrateCharges ) );
	}

	public function getImmediateMoveInProrateCharges() {
		return $this->m_boolImmediateMoveInProrateCharges;
	}

	public function sqlImmediateMoveInProrateCharges() {
		return ( true == isset( $this->m_boolImmediateMoveInProrateCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolImmediateMoveInProrateCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImmediateMoveInUse30DayMonth( $boolImmediateMoveInUse30DayMonth ) {
		$this->set( 'm_boolImmediateMoveInUse30DayMonth', CStrings::strToBool( $boolImmediateMoveInUse30DayMonth ) );
	}

	public function getImmediateMoveInUse30DayMonth() {
		return $this->m_boolImmediateMoveInUse30DayMonth;
	}

	public function sqlImmediateMoveInUse30DayMonth() {
		return ( true == isset( $this->m_boolImmediateMoveInUse30DayMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolImmediateMoveInUse30DayMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImmediateMoveInChargeMoveInDay( $boolImmediateMoveInChargeMoveInDay ) {
		$this->set( 'm_boolImmediateMoveInChargeMoveInDay', CStrings::strToBool( $boolImmediateMoveInChargeMoveInDay ) );
	}

	public function getImmediateMoveInChargeMoveInDay() {
		return $this->m_boolImmediateMoveInChargeMoveInDay;
	}

	public function sqlImmediateMoveInChargeMoveInDay() {
		return ( true == isset( $this->m_boolImmediateMoveInChargeMoveInDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolImmediateMoveInChargeMoveInDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWriteOffAcceleratedRent( $boolWriteOffAcceleratedRent ) {
		$this->set( 'm_boolWriteOffAcceleratedRent', CStrings::strToBool( $boolWriteOffAcceleratedRent ) );
	}

	public function getWriteOffAcceleratedRent() {
		return $this->m_boolWriteOffAcceleratedRent;
	}

	public function sqlWriteOffAcceleratedRent() {
		return ( true == isset( $this->m_boolWriteOffAcceleratedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolWriteOffAcceleratedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoPostScheduledCharges( $boolAutoPostScheduledCharges ) {
		$this->set( 'm_boolAutoPostScheduledCharges', CStrings::strToBool( $boolAutoPostScheduledCharges ) );
	}

	public function getAutoPostScheduledCharges() {
		return $this->m_boolAutoPostScheduledCharges;
	}

	public function sqlAutoPostScheduledCharges() {
		return ( true == isset( $this->m_boolAutoPostScheduledCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoPostScheduledCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBaseRentRenewalFactor( $fltBaseRentRenewalFactor ) {
		$this->set( 'm_fltBaseRentRenewalFactor', CStrings::strToFloatDef( $fltBaseRentRenewalFactor, NULL, false, 6 ) );
	}

	public function getBaseRentRenewalFactor() {
		return $this->m_fltBaseRentRenewalFactor;
	}

	public function sqlBaseRentRenewalFactor() {
		return ( true == isset( $this->m_fltBaseRentRenewalFactor ) ) ? ( string ) $this->m_fltBaseRentRenewalFactor : '0';
	}

	public function setOtherFeesRenewalFactor( $fltOtherFeesRenewalFactor ) {
		$this->set( 'm_fltOtherFeesRenewalFactor', CStrings::strToFloatDef( $fltOtherFeesRenewalFactor, NULL, false, 6 ) );
	}

	public function getOtherFeesRenewalFactor() {
		return $this->m_fltOtherFeesRenewalFactor;
	}

	public function sqlOtherFeesRenewalFactor() {
		return ( true == isset( $this->m_fltOtherFeesRenewalFactor ) ) ? ( string ) $this->m_fltOtherFeesRenewalFactor : '0';
	}

	public function setScheduledChargeAutoPostDay( $intScheduledChargeAutoPostDay ) {
		$this->set( 'm_intScheduledChargeAutoPostDay', CStrings::strToIntDef( $intScheduledChargeAutoPostDay, NULL, false ) );
	}

	public function getScheduledChargeAutoPostDay() {
		return $this->m_intScheduledChargeAutoPostDay;
	}

	public function sqlScheduledChargeAutoPostDay() {
		return ( true == isset( $this->m_intScheduledChargeAutoPostDay ) ) ? ( string ) $this->m_intScheduledChargeAutoPostDay : '1';
	}

	public function setScheduledChargeAutoPostThrough( $intScheduledChargeAutoPostThrough ) {
		$this->set( 'm_intScheduledChargeAutoPostThrough', CStrings::strToIntDef( $intScheduledChargeAutoPostThrough, NULL, false ) );
	}

	public function getScheduledChargeAutoPostThrough() {
		return $this->m_intScheduledChargeAutoPostThrough;
	}

	public function sqlScheduledChargeAutoPostThrough() {
		return ( true == isset( $this->m_intScheduledChargeAutoPostThrough ) ) ? ( string ) $this->m_intScheduledChargeAutoPostThrough : '31';
	}

	public function setPostThroughNextMonth( $boolPostThroughNextMonth ) {
		$this->set( 'm_boolPostThroughNextMonth', CStrings::strToBool( $boolPostThroughNextMonth ) );
	}

	public function getPostThroughNextMonth() {
		return $this->m_boolPostThroughNextMonth;
	}

	public function sqlPostThroughNextMonth() {
		return ( true == isset( $this->m_boolPostThroughNextMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostThroughNextMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowRentAmountOnQuickView( $boolShowRentAmountOnQuickView ) {
		$this->set( 'm_boolShowRentAmountOnQuickView', CStrings::strToBool( $boolShowRentAmountOnQuickView ) );
	}

	public function getShowRentAmountOnQuickView() {
		return $this->m_boolShowRentAmountOnQuickView;
	}

	public function sqlShowRentAmountOnQuickView() {
		return ( true == isset( $this->m_boolShowRentAmountOnQuickView ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowRentAmountOnQuickView ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartRenewalChargesOnFirst( $boolStartRenewalChargesOnFirst ) {
		$this->set( 'm_boolStartRenewalChargesOnFirst', CStrings::strToBool( $boolStartRenewalChargesOnFirst ) );
	}

	public function getStartRenewalChargesOnFirst() {
		return $this->m_boolStartRenewalChargesOnFirst;
	}

	public function sqlStartRenewalChargesOnFirst() {
		return ( true == isset( $this->m_boolStartRenewalChargesOnFirst ) ) ? '\'' . ( true == ( bool ) $this->m_boolStartRenewalChargesOnFirst ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartMtmChargesOnFirst( $boolStartMtmChargesOnFirst ) {
		$this->set( 'm_boolStartMtmChargesOnFirst', CStrings::strToBool( $boolStartMtmChargesOnFirst ) );
	}

	public function getStartMtmChargesOnFirst() {
		return $this->m_boolStartMtmChargesOnFirst;
	}

	public function sqlStartMtmChargesOnFirst() {
		return ( true == isset( $this->m_boolStartMtmChargesOnFirst ) ) ? '\'' . ( true == ( bool ) $this->m_boolStartMtmChargesOnFirst ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireInstallments( $boolRequireInstallments ) {
		$this->set( 'm_boolRequireInstallments', CStrings::strToBool( $boolRequireInstallments ) );
	}

	public function getRequireInstallments() {
		return $this->m_boolRequireInstallments;
	}

	public function sqlRequireInstallments() {
		return ( true == isset( $this->m_boolRequireInstallments ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireInstallments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAvailableSpaceLimitDays( $intAvailableSpaceLimitDays ) {
		$this->set( 'm_intAvailableSpaceLimitDays', CStrings::strToIntDef( $intAvailableSpaceLimitDays, NULL, false ) );
	}

	public function getAvailableSpaceLimitDays() {
		return $this->m_intAvailableSpaceLimitDays;
	}

	public function sqlAvailableSpaceLimitDays() {
		return ( true == isset( $this->m_intAvailableSpaceLimitDays ) ) ? ( string ) $this->m_intAvailableSpaceLimitDays : '10000';
	}

	public function setOnNoticeSpaceLimitDays( $intOnNoticeSpaceLimitDays ) {
		$this->set( 'm_intOnNoticeSpaceLimitDays', CStrings::strToIntDef( $intOnNoticeSpaceLimitDays, NULL, false ) );
	}

	public function getOnNoticeSpaceLimitDays() {
		return $this->m_intOnNoticeSpaceLimitDays;
	}

	public function sqlOnNoticeSpaceLimitDays() {
		return ( true == isset( $this->m_intOnNoticeSpaceLimitDays ) ) ? ( string ) $this->m_intOnNoticeSpaceLimitDays : '10000';
	}

	public function setEarlyDisplayAllowanceDays( $intEarlyDisplayAllowanceDays ) {
		$this->set( 'm_intEarlyDisplayAllowanceDays', CStrings::strToIntDef( $intEarlyDisplayAllowanceDays, NULL, false ) );
	}

	public function getEarlyDisplayAllowanceDays() {
		return $this->m_intEarlyDisplayAllowanceDays;
	}

	public function sqlEarlyDisplayAllowanceDays() {
		return ( true == isset( $this->m_intEarlyDisplayAllowanceDays ) ) ? ( string ) $this->m_intEarlyDisplayAllowanceDays : '0';
	}

	public function setShowAvailableNotReadySpaces( $boolShowAvailableNotReadySpaces ) {
		$this->set( 'm_boolShowAvailableNotReadySpaces', CStrings::strToBool( $boolShowAvailableNotReadySpaces ) );
	}

	public function getShowAvailableNotReadySpaces() {
		return $this->m_boolShowAvailableNotReadySpaces;
	}

	public function sqlShowAvailableNotReadySpaces() {
		return ( true == isset( $this->m_boolShowAvailableNotReadySpaces ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowAvailableNotReadySpaces ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowNoticeAvailableSpaces( $boolShowNoticeAvailableSpaces ) {
		$this->set( 'm_boolShowNoticeAvailableSpaces', CStrings::strToBool( $boolShowNoticeAvailableSpaces ) );
	}

	public function getShowNoticeAvailableSpaces() {
		return $this->m_boolShowNoticeAvailableSpaces;
	}

	public function sqlShowNoticeAvailableSpaces() {
		return ( true == isset( $this->m_boolShowNoticeAvailableSpaces ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowNoticeAvailableSpaces ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRateOffsets( $boolAllowRateOffsets ) {
		$this->set( 'm_boolAllowRateOffsets', CStrings::strToBool( $boolAllowRateOffsets ) );
	}

	public function getAllowRateOffsets() {
		return $this->m_boolAllowRateOffsets;
	}

	public function sqlAllowRateOffsets() {
		return ( true == isset( $this->m_boolAllowRateOffsets ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRateOffsets ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowLateFeeOverrides( $boolAllowLateFeeOverrides ) {
		$this->set( 'm_boolAllowLateFeeOverrides', CStrings::strToBool( $boolAllowLateFeeOverrides ) );
	}

	public function getAllowLateFeeOverrides() {
		return $this->m_boolAllowLateFeeOverrides;
	}

	public function sqlAllowLateFeeOverrides() {
		return ( true == isset( $this->m_boolAllowLateFeeOverrides ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowLateFeeOverrides ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowAssetRates( $boolAllowAssetRates ) {
		$this->set( 'm_boolAllowAssetRates', CStrings::strToBool( $boolAllowAssetRates ) );
	}

	public function getAllowAssetRates() {
		return $this->m_boolAllowAssetRates;
	}

	public function sqlAllowAssetRates() {
		return ( true == isset( $this->m_boolAllowAssetRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowAssetRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowEquityRates( $boolAllowEquityRates ) {
		$this->set( 'm_boolAllowEquityRates', CStrings::strToBool( $boolAllowEquityRates ) );
	}

	public function getAllowEquityRates() {
		return $this->m_boolAllowEquityRates;
	}

	public function sqlAllowEquityRates() {
		return ( true == isset( $this->m_boolAllowEquityRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowEquityRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowExpenseRates( $boolAllowExpenseRates ) {
		$this->set( 'm_boolAllowExpenseRates', CStrings::strToBool( $boolAllowExpenseRates ) );
	}

	public function getAllowExpenseRates() {
		return $this->m_boolAllowExpenseRates;
	}

	public function sqlAllowExpenseRates() {
		return ( true == isset( $this->m_boolAllowExpenseRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowExpenseRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowLiabilityRates( $boolAllowLiabilityRates ) {
		$this->set( 'm_boolAllowLiabilityRates', CStrings::strToBool( $boolAllowLiabilityRates ) );
	}

	public function getAllowLiabilityRates() {
		return $this->m_boolAllowLiabilityRates;
	}

	public function sqlAllowLiabilityRates() {
		return ( true == isset( $this->m_boolAllowLiabilityRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowLiabilityRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPreQualificationTrigger( $boolAllowPreQualificationTrigger ) {
		$this->set( 'm_boolAllowPreQualificationTrigger', CStrings::strToBool( $boolAllowPreQualificationTrigger ) );
	}

	public function getAllowPreQualificationTrigger() {
		return $this->m_boolAllowPreQualificationTrigger;
	}

	public function sqlAllowPreQualificationTrigger() {
		return ( true == isset( $this->m_boolAllowPreQualificationTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPreQualificationTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowApplicationApprovalTrigger( $boolAllowApplicationApprovalTrigger ) {
		$this->set( 'm_boolAllowApplicationApprovalTrigger', CStrings::strToBool( $boolAllowApplicationApprovalTrigger ) );
	}

	public function getAllowApplicationApprovalTrigger() {
		return $this->m_boolAllowApplicationApprovalTrigger;
	}

	public function sqlAllowApplicationApprovalTrigger() {
		return ( true == isset( $this->m_boolAllowApplicationApprovalTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowApplicationApprovalTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowLeaseCompletedTrigger( $boolAllowLeaseCompletedTrigger ) {
		$this->set( 'm_boolAllowLeaseCompletedTrigger', CStrings::strToBool( $boolAllowLeaseCompletedTrigger ) );
	}

	public function getAllowLeaseCompletedTrigger() {
		return $this->m_boolAllowLeaseCompletedTrigger;
	}

	public function sqlAllowLeaseCompletedTrigger() {
		return ( true == isset( $this->m_boolAllowLeaseCompletedTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowLeaseCompletedTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowLeaseApprovalTrigger( $boolAllowLeaseApprovalTrigger ) {
		$this->set( 'm_boolAllowLeaseApprovalTrigger', CStrings::strToBool( $boolAllowLeaseApprovalTrigger ) );
	}

	public function getAllowLeaseApprovalTrigger() {
		return $this->m_boolAllowLeaseApprovalTrigger;
	}

	public function sqlAllowLeaseApprovalTrigger() {
		return ( true == isset( $this->m_boolAllowLeaseApprovalTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowLeaseApprovalTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowLastLeaseMonthTrigger( $boolAllowLastLeaseMonthTrigger ) {
		$this->set( 'm_boolAllowLastLeaseMonthTrigger', CStrings::strToBool( $boolAllowLastLeaseMonthTrigger ) );
	}

	public function getAllowLastLeaseMonthTrigger() {
		return $this->m_boolAllowLastLeaseMonthTrigger;
	}

	public function sqlAllowLastLeaseMonthTrigger() {
		return ( true == isset( $this->m_boolAllowLastLeaseMonthTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowLastLeaseMonthTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNoticeTrigger( $boolAllowNoticeTrigger ) {
		$this->set( 'm_boolAllowNoticeTrigger', CStrings::strToBool( $boolAllowNoticeTrigger ) );
	}

	public function getAllowNoticeTrigger() {
		return $this->m_boolAllowNoticeTrigger;
	}

	public function sqlAllowNoticeTrigger() {
		return ( true == isset( $this->m_boolAllowNoticeTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNoticeTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowAnniversaryOfMoveInTrigger( $boolAllowAnniversaryOfMoveInTrigger ) {
		$this->set( 'm_boolAllowAnniversaryOfMoveInTrigger', CStrings::strToBool( $boolAllowAnniversaryOfMoveInTrigger ) );
	}

	public function getAllowAnniversaryOfMoveInTrigger() {
		return $this->m_boolAllowAnniversaryOfMoveInTrigger;
	}

	public function sqlAllowAnniversaryOfMoveInTrigger() {
		return ( true == isset( $this->m_boolAllowAnniversaryOfMoveInTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowAnniversaryOfMoveInTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowEndOfYearTrigger( $boolAllowEndOfYearTrigger ) {
		$this->set( 'm_boolAllowEndOfYearTrigger', CStrings::strToBool( $boolAllowEndOfYearTrigger ) );
	}

	public function getAllowEndOfYearTrigger() {
		return $this->m_boolAllowEndOfYearTrigger;
	}

	public function sqlAllowEndOfYearTrigger() {
		return ( true == isset( $this->m_boolAllowEndOfYearTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowEndOfYearTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowHourlyTrigger( $boolAllowHourlyTrigger ) {
		$this->set( 'm_boolAllowHourlyTrigger', CStrings::strToBool( $boolAllowHourlyTrigger ) );
	}

	public function getAllowHourlyTrigger() {
		return $this->m_boolAllowHourlyTrigger;
	}

	public function sqlAllowHourlyTrigger() {
		return ( true == isset( $this->m_boolAllowHourlyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowHourlyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowDailyTrigger( $boolAllowDailyTrigger ) {
		$this->set( 'm_boolAllowDailyTrigger', CStrings::strToBool( $boolAllowDailyTrigger ) );
	}

	public function getAllowDailyTrigger() {
		return $this->m_boolAllowDailyTrigger;
	}

	public function sqlAllowDailyTrigger() {
		return ( true == isset( $this->m_boolAllowDailyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowDailyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowWeeklyTrigger( $boolAllowWeeklyTrigger ) {
		$this->set( 'm_boolAllowWeeklyTrigger', CStrings::strToBool( $boolAllowWeeklyTrigger ) );
	}

	public function getAllowWeeklyTrigger() {
		return $this->m_boolAllowWeeklyTrigger;
	}

	public function sqlAllowWeeklyTrigger() {
		return ( true == isset( $this->m_boolAllowWeeklyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowWeeklyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowEveryTwoWeeksTrigger( $boolAllowEveryTwoWeeksTrigger ) {
		$this->set( 'm_boolAllowEveryTwoWeeksTrigger', CStrings::strToBool( $boolAllowEveryTwoWeeksTrigger ) );
	}

	public function getAllowEveryTwoWeeksTrigger() {
		return $this->m_boolAllowEveryTwoWeeksTrigger;
	}

	public function sqlAllowEveryTwoWeeksTrigger() {
		return ( true == isset( $this->m_boolAllowEveryTwoWeeksTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowEveryTwoWeeksTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowTwicePerMonthTrigger( $boolAllowTwicePerMonthTrigger ) {
		$this->set( 'm_boolAllowTwicePerMonthTrigger', CStrings::strToBool( $boolAllowTwicePerMonthTrigger ) );
	}

	public function getAllowTwicePerMonthTrigger() {
		return $this->m_boolAllowTwicePerMonthTrigger;
	}

	public function sqlAllowTwicePerMonthTrigger() {
		return ( true == isset( $this->m_boolAllowTwicePerMonthTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowTwicePerMonthTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowSpecificMonthsTrigger( $boolAllowSpecificMonthsTrigger ) {
		$this->set( 'm_boolAllowSpecificMonthsTrigger', CStrings::strToBool( $boolAllowSpecificMonthsTrigger ) );
	}

	public function getAllowSpecificMonthsTrigger() {
		return $this->m_boolAllowSpecificMonthsTrigger;
	}

	public function sqlAllowSpecificMonthsTrigger() {
		return ( true == isset( $this->m_boolAllowSpecificMonthsTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowSpecificMonthsTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowQuarterlyTrigger( $boolAllowQuarterlyTrigger ) {
		$this->set( 'm_boolAllowQuarterlyTrigger', CStrings::strToBool( $boolAllowQuarterlyTrigger ) );
	}

	public function getAllowQuarterlyTrigger() {
		return $this->m_boolAllowQuarterlyTrigger;
	}

	public function sqlAllowQuarterlyTrigger() {
		return ( true == isset( $this->m_boolAllowQuarterlyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowQuarterlyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowTwicePerYearTrigger( $boolAllowTwicePerYearTrigger ) {
		$this->set( 'm_boolAllowTwicePerYearTrigger', CStrings::strToBool( $boolAllowTwicePerYearTrigger ) );
	}

	public function getAllowTwicePerYearTrigger() {
		return $this->m_boolAllowTwicePerYearTrigger;
	}

	public function sqlAllowTwicePerYearTrigger() {
		return ( true == isset( $this->m_boolAllowTwicePerYearTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowTwicePerYearTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowYearlyTrigger( $boolAllowYearlyTrigger ) {
		$this->set( 'm_boolAllowYearlyTrigger', CStrings::strToBool( $boolAllowYearlyTrigger ) );
	}

	public function getAllowYearlyTrigger() {
		return $this->m_boolAllowYearlyTrigger;
	}

	public function sqlAllowYearlyTrigger() {
		return ( true == isset( $this->m_boolAllowYearlyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowYearlyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowScreeningApprovedTrigger( $boolAllowScreeningApprovedTrigger ) {
		$this->set( 'm_boolAllowScreeningApprovedTrigger', CStrings::strToBool( $boolAllowScreeningApprovedTrigger ) );
	}

	public function getAllowScreeningApprovedTrigger() {
		return $this->m_boolAllowScreeningApprovedTrigger;
	}

	public function sqlAllowScreeningApprovedTrigger() {
		return ( true == isset( $this->m_boolAllowScreeningApprovedTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowScreeningApprovedTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostAccelRentInCurrentPostMonth( $boolPostAccelRentInCurrentPostMonth ) {
		$this->set( 'm_boolPostAccelRentInCurrentPostMonth', CStrings::strToBool( $boolPostAccelRentInCurrentPostMonth ) );
	}

	public function getPostAccelRentInCurrentPostMonth() {
		return $this->m_boolPostAccelRentInCurrentPostMonth;
	}

	public function sqlPostAccelRentInCurrentPostMonth() {
		return ( true == isset( $this->m_boolPostAccelRentInCurrentPostMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostAccelRentInCurrentPostMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRepaymentAgreements( $boolAllowRepaymentAgreements ) {
		$this->set( 'm_boolAllowRepaymentAgreements', CStrings::strToBool( $boolAllowRepaymentAgreements ) );
	}

	public function getAllowRepaymentAgreements() {
		return $this->m_boolAllowRepaymentAgreements;
	}

	public function sqlAllowRepaymentAgreements() {
		return ( true == isset( $this->m_boolAllowRepaymentAgreements ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRepaymentAgreements ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRefundAcceleratedRentAtMoveIn( $boolRefundAcceleratedRentAtMoveIn ) {
		$this->set( 'm_boolRefundAcceleratedRentAtMoveIn', CStrings::strToBool( $boolRefundAcceleratedRentAtMoveIn ) );
	}

	public function getRefundAcceleratedRentAtMoveIn() {
		return $this->m_boolRefundAcceleratedRentAtMoveIn;
	}

	public function sqlRefundAcceleratedRentAtMoveIn() {
		return ( true == isset( $this->m_boolRefundAcceleratedRentAtMoveIn ) ) ? '\'' . ( true == ( bool ) $this->m_boolRefundAcceleratedRentAtMoveIn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeOtherInAdvertisedRent( $boolIncludeOtherInAdvertisedRent ) {
		$this->set( 'm_boolIncludeOtherInAdvertisedRent', CStrings::strToBool( $boolIncludeOtherInAdvertisedRent ) );
	}

	public function getIncludeOtherInAdvertisedRent() {
		return $this->m_boolIncludeOtherInAdvertisedRent;
	}

	public function sqlIncludeOtherInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeOtherInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeOtherInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRepaymentMaximumAllowedMonths( $intRepaymentMaximumAllowedMonths ) {
		$this->set( 'm_intRepaymentMaximumAllowedMonths', CStrings::strToIntDef( $intRepaymentMaximumAllowedMonths, NULL, false ) );
	}

	public function getRepaymentMaximumAllowedMonths() {
		return $this->m_intRepaymentMaximumAllowedMonths;
	}

	public function sqlRepaymentMaximumAllowedMonths() {
		return ( true == isset( $this->m_intRepaymentMaximumAllowedMonths ) ) ? ( string ) $this->m_intRepaymentMaximumAllowedMonths : '0';
	}

	public function setRepaymentAgreementArCodeId( $intRepaymentAgreementArCodeId ) {
		$this->set( 'm_intRepaymentAgreementArCodeId', CStrings::strToIntDef( $intRepaymentAgreementArCodeId, NULL, false ) );
	}

	public function getRepaymentAgreementArCodeId() {
		return $this->m_intRepaymentAgreementArCodeId;
	}

	public function sqlRepaymentAgreementArCodeId() {
		return ( true == isset( $this->m_intRepaymentAgreementArCodeId ) ) ? ( string ) $this->m_intRepaymentAgreementArCodeId : 'NULL';
	}

	public function setRepaymentAllocationOrder( $intRepaymentAllocationOrder ) {
		$this->set( 'm_intRepaymentAllocationOrder', CStrings::strToIntDef( $intRepaymentAllocationOrder, NULL, false ) );
	}

	public function getRepaymentAllocationOrder() {
		return $this->m_intRepaymentAllocationOrder;
	}

	public function sqlRepaymentAllocationOrder() {
		return ( true == isset( $this->m_intRepaymentAllocationOrder ) ) ? ( string ) $this->m_intRepaymentAllocationOrder : '0';
	}

	public function setRepaymentChargeLateFeesRetroactively( $boolRepaymentChargeLateFeesRetroactively ) {
		$this->set( 'm_boolRepaymentChargeLateFeesRetroactively', CStrings::strToBool( $boolRepaymentChargeLateFeesRetroactively ) );
	}

	public function getRepaymentChargeLateFeesRetroactively() {
		return $this->m_boolRepaymentChargeLateFeesRetroactively;
	}

	public function sqlRepaymentChargeLateFeesRetroactively() {
		return ( true == isset( $this->m_boolRepaymentChargeLateFeesRetroactively ) ) ? '\'' . ( true == ( bool ) $this->m_boolRepaymentChargeLateFeesRetroactively ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDefaultMtmMultiplierLeaseTermId( $intDefaultMtmMultiplierLeaseTermId ) {
		$this->set( 'm_intDefaultMtmMultiplierLeaseTermId', CStrings::strToIntDef( $intDefaultMtmMultiplierLeaseTermId, NULL, false ) );
	}

	public function getDefaultMtmMultiplierLeaseTermId() {
		return $this->m_intDefaultMtmMultiplierLeaseTermId;
	}

	public function sqlDefaultMtmMultiplierLeaseTermId() {
		return ( true == isset( $this->m_intDefaultMtmMultiplierLeaseTermId ) ) ? ( string ) $this->m_intDefaultMtmMultiplierLeaseTermId : 'NULL';
	}

	public function setDefaultMtmMultiplier( $fltDefaultMtmMultiplier ) {
		$this->set( 'm_fltDefaultMtmMultiplier', CStrings::strToFloatDef( $fltDefaultMtmMultiplier, NULL, false, 4 ) );
	}

	public function getDefaultMtmMultiplier() {
		return $this->m_fltDefaultMtmMultiplier;
	}

	public function sqlDefaultMtmMultiplier() {
		return ( true == isset( $this->m_fltDefaultMtmMultiplier ) ) ? ( string ) $this->m_fltDefaultMtmMultiplier : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAllowRepaymentsForActiveLeases( $boolAllowRepaymentsForActiveLeases ) {
		$this->set( 'm_boolAllowRepaymentsForActiveLeases', CStrings::strToBool( $boolAllowRepaymentsForActiveLeases ) );
	}

	public function getAllowRepaymentsForActiveLeases() {
		return $this->m_boolAllowRepaymentsForActiveLeases;
	}

	public function sqlAllowRepaymentsForActiveLeases() {
		return ( true == isset( $this->m_boolAllowRepaymentsForActiveLeases ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRepaymentsForActiveLeases ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseCurrentDateForFloorplanRange( $boolUseCurrentDateForFloorplanRange ) {
		$this->set( 'm_boolUseCurrentDateForFloorplanRange', CStrings::strToBool( $boolUseCurrentDateForFloorplanRange ) );
	}

	public function getUseCurrentDateForFloorplanRange() {
		return $this->m_boolUseCurrentDateForFloorplanRange;
	}

	public function sqlUseCurrentDateForFloorplanRange() {
		return ( true == isset( $this->m_boolUseCurrentDateForFloorplanRange ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseCurrentDateForFloorplanRange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeTaxInAdvertisedRent( $boolIncludeTaxInAdvertisedRent ) {
		$this->set( 'm_boolIncludeTaxInAdvertisedRent', CStrings::strToBool( $boolIncludeTaxInAdvertisedRent ) );
	}

	public function getIncludeTaxInAdvertisedRent() {
		return $this->m_boolIncludeTaxInAdvertisedRent;
	}

	public function sqlIncludeTaxInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeTaxInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeTaxInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillFutureLeases( $boolBillFutureLeases ) {
		$this->set( 'm_boolBillFutureLeases', CStrings::strToBool( $boolBillFutureLeases ) );
	}

	public function getBillFutureLeases() {
		return $this->m_boolBillFutureLeases;
	}

	public function sqlBillFutureLeases() {
		return ( true == isset( $this->m_boolBillFutureLeases ) ) ? '\'' . ( true == ( bool ) $this->m_boolBillFutureLeases ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillEarlyMoveIn( $boolBillEarlyMoveIn ) {
		$this->set( 'm_boolBillEarlyMoveIn', CStrings::strToBool( $boolBillEarlyMoveIn ) );
	}

	public function getBillEarlyMoveIn() {
		return $this->m_boolBillEarlyMoveIn;
	}

	public function sqlBillEarlyMoveIn() {
		return ( true == isset( $this->m_boolBillEarlyMoveIn ) ) ? '\'' . ( true == ( bool ) $this->m_boolBillEarlyMoveIn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseRatesForEarlyMoveIns( $boolUseRatesForEarlyMoveIns ) {
		$this->set( 'm_boolUseRatesForEarlyMoveIns', CStrings::strToBool( $boolUseRatesForEarlyMoveIns ) );
	}

	public function getUseRatesForEarlyMoveIns() {
		return $this->m_boolUseRatesForEarlyMoveIns;
	}

	public function sqlUseRatesForEarlyMoveIns() {
		return ( true == isset( $this->m_boolUseRatesForEarlyMoveIns ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseRatesForEarlyMoveIns ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEndChargesAtNotice( $boolEndChargesAtNotice ) {
		$this->set( 'm_boolEndChargesAtNotice', CStrings::strToBool( $boolEndChargesAtNotice ) );
	}

	public function getEndChargesAtNotice() {
		return $this->m_boolEndChargesAtNotice;
	}

	public function sqlEndChargesAtNotice() {
		return ( true == isset( $this->m_boolEndChargesAtNotice ) ) ? '\'' . ( true == ( bool ) $this->m_boolEndChargesAtNotice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllocateTaxesProportionately( $boolAllocateTaxesProportionately ) {
		$this->set( 'm_boolAllocateTaxesProportionately', CStrings::strToBool( $boolAllocateTaxesProportionately ) );
	}

	public function getAllocateTaxesProportionately() {
		return $this->m_boolAllocateTaxesProportionately;
	}

	public function sqlAllocateTaxesProportionately() {
		return ( true == isset( $this->m_boolAllocateTaxesProportionately ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllocateTaxesProportionately ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillEarlyMoveInsImmediately( $boolBillEarlyMoveInsImmediately ) {
		$this->set( 'm_boolBillEarlyMoveInsImmediately', CStrings::strToBool( $boolBillEarlyMoveInsImmediately ) );
	}

	public function getBillEarlyMoveInsImmediately() {
		return $this->m_boolBillEarlyMoveInsImmediately;
	}

	public function sqlBillEarlyMoveInsImmediately() {
		return ( true == isset( $this->m_boolBillEarlyMoveInsImmediately ) ) ? '\'' . ( true == ( bool ) $this->m_boolBillEarlyMoveInsImmediately ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNightlyTrigger( $boolAllowNightlyTrigger ) {
		$this->set( 'm_boolAllowNightlyTrigger', CStrings::strToBool( $boolAllowNightlyTrigger ) );
	}

	public function getAllowNightlyTrigger() {
		return $this->m_boolAllowNightlyTrigger;
	}

	public function sqlAllowNightlyTrigger() {
		return ( true == isset( $this->m_boolAllowNightlyTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNightlyTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRenewalOfferAcceptedTrigger( $boolAllowRenewalOfferAcceptedTrigger ) {
		$this->set( 'm_boolAllowRenewalOfferAcceptedTrigger', CStrings::strToBool( $boolAllowRenewalOfferAcceptedTrigger ) );
	}

	public function getAllowRenewalOfferAcceptedTrigger() {
		return $this->m_boolAllowRenewalOfferAcceptedTrigger;
	}

	public function sqlAllowRenewalOfferAcceptedTrigger() {
		return ( true == isset( $this->m_boolAllowRenewalOfferAcceptedTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRenewalOfferAcceptedTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowServiceSelectedTrigger( $boolAllowServiceSelectedTrigger ) {
		$this->set( 'm_boolAllowServiceSelectedTrigger', CStrings::strToBool( $boolAllowServiceSelectedTrigger ) );
	}

	public function getAllowServiceSelectedTrigger() {
		return $this->m_boolAllowServiceSelectedTrigger;
	}

	public function sqlAllowServiceSelectedTrigger() {
		return ( true == isset( $this->m_boolAllowServiceSelectedTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowServiceSelectedTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowServiceFulfilledTrigger( $boolAllowServiceFulfilledTrigger ) {
		$this->set( 'm_boolAllowServiceFulfilledTrigger', CStrings::strToBool( $boolAllowServiceFulfilledTrigger ) );
	}

	public function getAllowServiceFulfilledTrigger() {
		return $this->m_boolAllowServiceFulfilledTrigger;
	}

	public function sqlAllowServiceFulfilledTrigger() {
		return ( true == isset( $this->m_boolAllowServiceFulfilledTrigger ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowServiceFulfilledTrigger ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProrationMethodTypeId( $intProrationMethodTypeId ) {
		$this->set( 'm_intProrationMethodTypeId', CStrings::strToIntDef( $intProrationMethodTypeId, NULL, false ) );
	}

	public function getProrationMethodTypeId() {
		return $this->m_intProrationMethodTypeId;
	}

	public function sqlProrationMethodTypeId() {
		return ( true == isset( $this->m_intProrationMethodTypeId ) ) ? ( string ) $this->m_intProrationMethodTypeId : '1';
	}

	public function setCreateMtmCharges( $boolCreateMtmCharges ) {
		$this->set( 'm_boolCreateMtmCharges', CStrings::strToBool( $boolCreateMtmCharges ) );
	}

	public function getCreateMtmCharges() {
		return $this->m_boolCreateMtmCharges;
	}

	public function sqlCreateMtmCharges() {
		return ( true == isset( $this->m_boolCreateMtmCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolCreateMtmCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseCurrentPostMonthForRepayments( $boolUseCurrentPostMonthForRepayments ) {
		$this->set( 'm_boolUseCurrentPostMonthForRepayments', CStrings::strToBool( $boolUseCurrentPostMonthForRepayments ) );
	}

	public function getUseCurrentPostMonthForRepayments() {
		return $this->m_boolUseCurrentPostMonthForRepayments;
	}

	public function sqlUseCurrentPostMonthForRepayments() {
		return ( true == isset( $this->m_boolUseCurrentPostMonthForRepayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseCurrentPostMonthForRepayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRepaymentMaxAmount( $fltRepaymentMaxAmount ) {
		$this->set( 'm_fltRepaymentMaxAmount', CStrings::strToFloatDef( $fltRepaymentMaxAmount, NULL, false, 2 ) );
	}

	public function getRepaymentMaxAmount() {
		return $this->m_fltRepaymentMaxAmount;
	}

	public function sqlRepaymentMaxAmount() {
		return ( true == isset( $this->m_fltRepaymentMaxAmount ) ) ? ( string ) $this->m_fltRepaymentMaxAmount : '0';
	}

	public function setRepaymentMaxAmountArFormulaId( $intRepaymentMaxAmountArFormulaId ) {
		$this->set( 'm_intRepaymentMaxAmountArFormulaId', CStrings::strToIntDef( $intRepaymentMaxAmountArFormulaId, NULL, false ) );
	}

	public function getRepaymentMaxAmountArFormulaId() {
		return $this->m_intRepaymentMaxAmountArFormulaId;
	}

	public function sqlRepaymentMaxAmountArFormulaId() {
		return ( true == isset( $this->m_intRepaymentMaxAmountArFormulaId ) ) ? ( string ) $this->m_intRepaymentMaxAmountArFormulaId : 'NULL';
	}

	public function setRepaymentMaxAmountArFormulaReferenceId( $intRepaymentMaxAmountArFormulaReferenceId ) {
		$this->set( 'm_intRepaymentMaxAmountArFormulaReferenceId', CStrings::strToIntDef( $intRepaymentMaxAmountArFormulaReferenceId, NULL, false ) );
	}

	public function getRepaymentMaxAmountArFormulaReferenceId() {
		return $this->m_intRepaymentMaxAmountArFormulaReferenceId;
	}

	public function sqlRepaymentMaxAmountArFormulaReferenceId() {
		return ( true == isset( $this->m_intRepaymentMaxAmountArFormulaReferenceId ) ) ? ( string ) $this->m_intRepaymentMaxAmountArFormulaReferenceId : 'NULL';
	}

	public function setRepaymentMaxPercent( $fltRepaymentMaxPercent ) {
		$this->set( 'm_fltRepaymentMaxPercent', CStrings::strToFloatDef( $fltRepaymentMaxPercent, NULL, false, 6 ) );
	}

	public function getRepaymentMaxPercent() {
		return $this->m_fltRepaymentMaxPercent;
	}

	public function sqlRepaymentMaxPercent() {
		return ( true == isset( $this->m_fltRepaymentMaxPercent ) ) ? ( string ) $this->m_fltRepaymentMaxPercent : '0';
	}

	public function setProrateStraightLineAdjustmentPosting( $boolProrateStraightLineAdjustmentPosting ) {
		$this->set( 'm_boolProrateStraightLineAdjustmentPosting', CStrings::strToBool( $boolProrateStraightLineAdjustmentPosting ) );
	}

	public function getProrateStraightLineAdjustmentPosting() {
		return $this->m_boolProrateStraightLineAdjustmentPosting;
	}

	public function sqlProrateStraightLineAdjustmentPosting() {
		return ( true == isset( $this->m_boolProrateStraightLineAdjustmentPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolProrateStraightLineAdjustmentPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_term_structure_id, lease_start_structure_id, accelerated_rent_type_id, base_rent_renewal_option_id, other_fees_renewal_option_id, lease_end_billing_type_id, round_type_id, month_to_month_type_id, month_to_month_rent_ar_code_id, enforce_unit_space_unions, prorate_charges, ignore_month_to_month_intervals, use_30_day_month, temporary_charge_gl_post_day, charge_move_in_day, charge_move_out_day, immediate_move_in_prorate_charges, immediate_move_in_use_30_day_month, immediate_move_in_charge_move_in_day, write_off_accelerated_rent, auto_post_scheduled_charges, base_rent_renewal_factor, other_fees_renewal_factor, scheduled_charge_auto_post_day, scheduled_charge_auto_post_through, post_through_next_month, show_rent_amount_on_quick_view, start_renewal_charges_on_first, start_mtm_charges_on_first, require_installments, available_space_limit_days, on_notice_space_limit_days, early_display_allowance_days, show_available_not_ready_spaces, show_notice_available_spaces, allow_rate_offsets, allow_late_fee_overrides, allow_asset_rates, allow_equity_rates, allow_expense_rates, allow_liability_rates, allow_pre_qualification_trigger, allow_application_approval_trigger, allow_lease_completed_trigger, allow_lease_approval_trigger, allow_last_lease_month_trigger, allow_notice_trigger, allow_anniversary_of_move_in_trigger, allow_end_of_year_trigger, allow_hourly_trigger, allow_daily_trigger, allow_weekly_trigger, allow_every_two_weeks_trigger, allow_twice_per_month_trigger, allow_specific_months_trigger, allow_quarterly_trigger, allow_twice_per_year_trigger, allow_yearly_trigger, allow_screening_approved_trigger, post_accel_rent_in_current_post_month, allow_repayment_agreements, refund_accelerated_rent_at_move_in, include_other_in_advertised_rent, repayment_maximum_allowed_months, repayment_agreement_ar_code_id, repayment_allocation_order, repayment_charge_late_fees_retroactively, default_mtm_multiplier_lease_term_id, default_mtm_multiplier, updated_by, updated_on, created_by, created_on, allow_repayments_for_active_leases, use_current_date_for_floorplan_range, include_tax_in_advertised_rent, bill_future_leases, bill_early_move_in, use_rates_for_early_move_ins, end_charges_at_notice, allocate_taxes_proportionately, bill_early_move_ins_immediately, allow_nightly_trigger, allow_renewal_offer_accepted_trigger, allow_service_selected_trigger, allow_service_fulfilled_trigger, proration_method_type_id, create_mtm_charges, use_current_post_month_for_repayments, repayment_max_amount, repayment_max_amount_ar_formula_id, repayment_max_amount_ar_formula_reference_id, repayment_max_percent, prorate_straight_line_adjustment_posting )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlLeaseTermStructureId() . ', ' .
			$this->sqlLeaseStartStructureId() . ', ' .
			$this->sqlAcceleratedRentTypeId() . ', ' .
			$this->sqlBaseRentRenewalOptionId() . ', ' .
			$this->sqlOtherFeesRenewalOptionId() . ', ' .
			$this->sqlLeaseEndBillingTypeId() . ', ' .
			$this->sqlRoundTypeId() . ', ' .
			$this->sqlMonthToMonthTypeId() . ', ' .
			$this->sqlMonthToMonthRentArCodeId() . ', ' .
			$this->sqlEnforceUnitSpaceUnions() . ', ' .
			$this->sqlProrateCharges() . ', ' .
			$this->sqlIgnoreMonthToMonthIntervals() . ', ' .
			$this->sqlUse30DayMonth() . ', ' .
			$this->sqlTemporaryChargeGlPostDay() . ', ' .
			$this->sqlChargeMoveInDay() . ', ' .
			$this->sqlChargeMoveOutDay() . ', ' .
			$this->sqlImmediateMoveInProrateCharges() . ', ' .
			$this->sqlImmediateMoveInUse30DayMonth() . ', ' .
			$this->sqlImmediateMoveInChargeMoveInDay() . ', ' .
			$this->sqlWriteOffAcceleratedRent() . ', ' .
			$this->sqlAutoPostScheduledCharges() . ', ' .
			$this->sqlBaseRentRenewalFactor() . ', ' .
			$this->sqlOtherFeesRenewalFactor() . ', ' .
			$this->sqlScheduledChargeAutoPostDay() . ', ' .
			$this->sqlScheduledChargeAutoPostThrough() . ', ' .
			$this->sqlPostThroughNextMonth() . ', ' .
			$this->sqlShowRentAmountOnQuickView() . ', ' .
			$this->sqlStartRenewalChargesOnFirst() . ', ' .
			$this->sqlStartMtmChargesOnFirst() . ', ' .
			$this->sqlRequireInstallments() . ', ' .
			$this->sqlAvailableSpaceLimitDays() . ', ' .
			$this->sqlOnNoticeSpaceLimitDays() . ', ' .
			$this->sqlEarlyDisplayAllowanceDays() . ', ' .
			$this->sqlShowAvailableNotReadySpaces() . ', ' .
			$this->sqlShowNoticeAvailableSpaces() . ', ' .
			$this->sqlAllowRateOffsets() . ', ' .
			$this->sqlAllowLateFeeOverrides() . ', ' .
			$this->sqlAllowAssetRates() . ', ' .
			$this->sqlAllowEquityRates() . ', ' .
			$this->sqlAllowExpenseRates() . ', ' .
			$this->sqlAllowLiabilityRates() . ', ' .
			$this->sqlAllowPreQualificationTrigger() . ', ' .
			$this->sqlAllowApplicationApprovalTrigger() . ', ' .
			$this->sqlAllowLeaseCompletedTrigger() . ', ' .
			$this->sqlAllowLeaseApprovalTrigger() . ', ' .
			$this->sqlAllowLastLeaseMonthTrigger() . ', ' .
			$this->sqlAllowNoticeTrigger() . ', ' .
			$this->sqlAllowAnniversaryOfMoveInTrigger() . ', ' .
			$this->sqlAllowEndOfYearTrigger() . ', ' .
			$this->sqlAllowHourlyTrigger() . ', ' .
			$this->sqlAllowDailyTrigger() . ', ' .
			$this->sqlAllowWeeklyTrigger() . ', ' .
			$this->sqlAllowEveryTwoWeeksTrigger() . ', ' .
			$this->sqlAllowTwicePerMonthTrigger() . ', ' .
			$this->sqlAllowSpecificMonthsTrigger() . ', ' .
			$this->sqlAllowQuarterlyTrigger() . ', ' .
			$this->sqlAllowTwicePerYearTrigger() . ', ' .
			$this->sqlAllowYearlyTrigger() . ', ' .
			$this->sqlAllowScreeningApprovedTrigger() . ', ' .
			$this->sqlPostAccelRentInCurrentPostMonth() . ', ' .
			$this->sqlAllowRepaymentAgreements() . ', ' .
			$this->sqlRefundAcceleratedRentAtMoveIn() . ', ' .
			$this->sqlIncludeOtherInAdvertisedRent() . ', ' .
			$this->sqlRepaymentMaximumAllowedMonths() . ', ' .
			$this->sqlRepaymentAgreementArCodeId() . ', ' .
			$this->sqlRepaymentAllocationOrder() . ', ' .
			$this->sqlRepaymentChargeLateFeesRetroactively() . ', ' .
			$this->sqlDefaultMtmMultiplierLeaseTermId() . ', ' .
			$this->sqlDefaultMtmMultiplier() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ', ' .
			$this->sqlAllowRepaymentsForActiveLeases() . ', ' .
			$this->sqlUseCurrentDateForFloorplanRange() . ', ' .
			$this->sqlIncludeTaxInAdvertisedRent() . ', ' .
			$this->sqlBillFutureLeases() . ', ' .
			$this->sqlBillEarlyMoveIn() . ', ' .
			$this->sqlUseRatesForEarlyMoveIns() . ', ' .
			$this->sqlEndChargesAtNotice() . ', ' .
			$this->sqlAllocateTaxesProportionately() . ', ' .
			$this->sqlBillEarlyMoveInsImmediately() . ', ' .
			$this->sqlAllowNightlyTrigger() . ', ' .
			$this->sqlAllowRenewalOfferAcceptedTrigger() . ', ' .
			$this->sqlAllowServiceSelectedTrigger() . ', ' .
			$this->sqlAllowServiceFulfilledTrigger() . ', ' .
			$this->sqlProrationMethodTypeId() . ', ' .
			$this->sqlCreateMtmCharges() . ', ' .
			$this->sqlUseCurrentPostMonthForRepayments() . ', ' .
			$this->sqlRepaymentMaxAmount() . ', ' .
			$this->sqlRepaymentMaxAmountArFormulaId() . ', ' .
			$this->sqlRepaymentMaxAmountArFormulaReferenceId() . ', ' .
			$this->sqlRepaymentMaxPercent() . ', ' .
			$this->sqlProrateStraightLineAdjustmentPosting() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_structure_id = ' . $this->sqlLeaseTermStructureId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermStructureId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_structure_id = ' . $this->sqlLeaseTermStructureId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_structure_id = ' . $this->sqlLeaseStartStructureId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartStructureId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_structure_id = ' . $this->sqlLeaseStartStructureId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accelerated_rent_type_id = ' . $this->sqlAcceleratedRentTypeId(). ',' ; } elseif( true == array_key_exists( 'AcceleratedRentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' accelerated_rent_type_id = ' . $this->sqlAcceleratedRentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent_renewal_option_id = ' . $this->sqlBaseRentRenewalOptionId(). ',' ; } elseif( true == array_key_exists( 'BaseRentRenewalOptionId', $this->getChangedColumns() ) ) { $strSql .= ' base_rent_renewal_option_id = ' . $this->sqlBaseRentRenewalOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_fees_renewal_option_id = ' . $this->sqlOtherFeesRenewalOptionId(). ',' ; } elseif( true == array_key_exists( 'OtherFeesRenewalOptionId', $this->getChangedColumns() ) ) { $strSql .= ' other_fees_renewal_option_id = ' . $this->sqlOtherFeesRenewalOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_billing_type_id = ' . $this->sqlLeaseEndBillingTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseEndBillingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_billing_type_id = ' . $this->sqlLeaseEndBillingTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' round_type_id = ' . $this->sqlRoundTypeId(). ',' ; } elseif( true == array_key_exists( 'RoundTypeId', $this->getChangedColumns() ) ) { $strSql .= ' round_type_id = ' . $this->sqlRoundTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_type_id = ' . $this->sqlMonthToMonthTypeId(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthTypeId', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_type_id = ' . $this->sqlMonthToMonthTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_rent_ar_code_id = ' . $this->sqlMonthToMonthRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthRentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_rent_ar_code_id = ' . $this->sqlMonthToMonthRentArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enforce_unit_space_unions = ' . $this->sqlEnforceUnitSpaceUnions(). ',' ; } elseif( true == array_key_exists( 'EnforceUnitSpaceUnions', $this->getChangedColumns() ) ) { $strSql .= ' enforce_unit_space_unions = ' . $this->sqlEnforceUnitSpaceUnions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prorate_charges = ' . $this->sqlProrateCharges(). ',' ; } elseif( true == array_key_exists( 'ProrateCharges', $this->getChangedColumns() ) ) { $strSql .= ' prorate_charges = ' . $this->sqlProrateCharges() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_month_to_month_intervals = ' . $this->sqlIgnoreMonthToMonthIntervals(). ',' ; } elseif( true == array_key_exists( 'IgnoreMonthToMonthIntervals', $this->getChangedColumns() ) ) { $strSql .= ' ignore_month_to_month_intervals = ' . $this->sqlIgnoreMonthToMonthIntervals() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_30_day_month = ' . $this->sqlUse30DayMonth(). ',' ; } elseif( true == array_key_exists( 'Use30DayMonth', $this->getChangedColumns() ) ) { $strSql .= ' use_30_day_month = ' . $this->sqlUse30DayMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temporary_charge_gl_post_day = ' . $this->sqlTemporaryChargeGlPostDay(). ',' ; } elseif( true == array_key_exists( 'TemporaryChargeGlPostDay', $this->getChangedColumns() ) ) { $strSql .= ' temporary_charge_gl_post_day = ' . $this->sqlTemporaryChargeGlPostDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_move_in_day = ' . $this->sqlChargeMoveInDay(). ',' ; } elseif( true == array_key_exists( 'ChargeMoveInDay', $this->getChangedColumns() ) ) { $strSql .= ' charge_move_in_day = ' . $this->sqlChargeMoveInDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_move_out_day = ' . $this->sqlChargeMoveOutDay(). ',' ; } elseif( true == array_key_exists( 'ChargeMoveOutDay', $this->getChangedColumns() ) ) { $strSql .= ' charge_move_out_day = ' . $this->sqlChargeMoveOutDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_move_in_prorate_charges = ' . $this->sqlImmediateMoveInProrateCharges(). ',' ; } elseif( true == array_key_exists( 'ImmediateMoveInProrateCharges', $this->getChangedColumns() ) ) { $strSql .= ' immediate_move_in_prorate_charges = ' . $this->sqlImmediateMoveInProrateCharges() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_move_in_use_30_day_month = ' . $this->sqlImmediateMoveInUse30DayMonth(). ',' ; } elseif( true == array_key_exists( 'ImmediateMoveInUse30DayMonth', $this->getChangedColumns() ) ) { $strSql .= ' immediate_move_in_use_30_day_month = ' . $this->sqlImmediateMoveInUse30DayMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_move_in_charge_move_in_day = ' . $this->sqlImmediateMoveInChargeMoveInDay(). ',' ; } elseif( true == array_key_exists( 'ImmediateMoveInChargeMoveInDay', $this->getChangedColumns() ) ) { $strSql .= ' immediate_move_in_charge_move_in_day = ' . $this->sqlImmediateMoveInChargeMoveInDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' write_off_accelerated_rent = ' . $this->sqlWriteOffAcceleratedRent(). ',' ; } elseif( true == array_key_exists( 'WriteOffAcceleratedRent', $this->getChangedColumns() ) ) { $strSql .= ' write_off_accelerated_rent = ' . $this->sqlWriteOffAcceleratedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_scheduled_charges = ' . $this->sqlAutoPostScheduledCharges(). ',' ; } elseif( true == array_key_exists( 'AutoPostScheduledCharges', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_scheduled_charges = ' . $this->sqlAutoPostScheduledCharges() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent_renewal_factor = ' . $this->sqlBaseRentRenewalFactor(). ',' ; } elseif( true == array_key_exists( 'BaseRentRenewalFactor', $this->getChangedColumns() ) ) { $strSql .= ' base_rent_renewal_factor = ' . $this->sqlBaseRentRenewalFactor() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_fees_renewal_factor = ' . $this->sqlOtherFeesRenewalFactor(). ',' ; } elseif( true == array_key_exists( 'OtherFeesRenewalFactor', $this->getChangedColumns() ) ) { $strSql .= ' other_fees_renewal_factor = ' . $this->sqlOtherFeesRenewalFactor() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_auto_post_day = ' . $this->sqlScheduledChargeAutoPostDay(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeAutoPostDay', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_auto_post_day = ' . $this->sqlScheduledChargeAutoPostDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_auto_post_through = ' . $this->sqlScheduledChargeAutoPostThrough(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeAutoPostThrough', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_auto_post_through = ' . $this->sqlScheduledChargeAutoPostThrough() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_through_next_month = ' . $this->sqlPostThroughNextMonth(). ',' ; } elseif( true == array_key_exists( 'PostThroughNextMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_through_next_month = ' . $this->sqlPostThroughNextMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_rent_amount_on_quick_view = ' . $this->sqlShowRentAmountOnQuickView(). ',' ; } elseif( true == array_key_exists( 'ShowRentAmountOnQuickView', $this->getChangedColumns() ) ) { $strSql .= ' show_rent_amount_on_quick_view = ' . $this->sqlShowRentAmountOnQuickView() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_renewal_charges_on_first = ' . $this->sqlStartRenewalChargesOnFirst(). ',' ; } elseif( true == array_key_exists( 'StartRenewalChargesOnFirst', $this->getChangedColumns() ) ) { $strSql .= ' start_renewal_charges_on_first = ' . $this->sqlStartRenewalChargesOnFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_mtm_charges_on_first = ' . $this->sqlStartMtmChargesOnFirst(). ',' ; } elseif( true == array_key_exists( 'StartMtmChargesOnFirst', $this->getChangedColumns() ) ) { $strSql .= ' start_mtm_charges_on_first = ' . $this->sqlStartMtmChargesOnFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_installments = ' . $this->sqlRequireInstallments(). ',' ; } elseif( true == array_key_exists( 'RequireInstallments', $this->getChangedColumns() ) ) { $strSql .= ' require_installments = ' . $this->sqlRequireInstallments() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_space_limit_days = ' . $this->sqlAvailableSpaceLimitDays(). ',' ; } elseif( true == array_key_exists( 'AvailableSpaceLimitDays', $this->getChangedColumns() ) ) { $strSql .= ' available_space_limit_days = ' . $this->sqlAvailableSpaceLimitDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_notice_space_limit_days = ' . $this->sqlOnNoticeSpaceLimitDays(). ',' ; } elseif( true == array_key_exists( 'OnNoticeSpaceLimitDays', $this->getChangedColumns() ) ) { $strSql .= ' on_notice_space_limit_days = ' . $this->sqlOnNoticeSpaceLimitDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' early_display_allowance_days = ' . $this->sqlEarlyDisplayAllowanceDays(). ',' ; } elseif( true == array_key_exists( 'EarlyDisplayAllowanceDays', $this->getChangedColumns() ) ) { $strSql .= ' early_display_allowance_days = ' . $this->sqlEarlyDisplayAllowanceDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_available_not_ready_spaces = ' . $this->sqlShowAvailableNotReadySpaces(). ',' ; } elseif( true == array_key_exists( 'ShowAvailableNotReadySpaces', $this->getChangedColumns() ) ) { $strSql .= ' show_available_not_ready_spaces = ' . $this->sqlShowAvailableNotReadySpaces() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_notice_available_spaces = ' . $this->sqlShowNoticeAvailableSpaces(). ',' ; } elseif( true == array_key_exists( 'ShowNoticeAvailableSpaces', $this->getChangedColumns() ) ) { $strSql .= ' show_notice_available_spaces = ' . $this->sqlShowNoticeAvailableSpaces() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_rate_offsets = ' . $this->sqlAllowRateOffsets(). ',' ; } elseif( true == array_key_exists( 'AllowRateOffsets', $this->getChangedColumns() ) ) { $strSql .= ' allow_rate_offsets = ' . $this->sqlAllowRateOffsets() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_late_fee_overrides = ' . $this->sqlAllowLateFeeOverrides(). ',' ; } elseif( true == array_key_exists( 'AllowLateFeeOverrides', $this->getChangedColumns() ) ) { $strSql .= ' allow_late_fee_overrides = ' . $this->sqlAllowLateFeeOverrides() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_asset_rates = ' . $this->sqlAllowAssetRates(). ',' ; } elseif( true == array_key_exists( 'AllowAssetRates', $this->getChangedColumns() ) ) { $strSql .= ' allow_asset_rates = ' . $this->sqlAllowAssetRates() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_equity_rates = ' . $this->sqlAllowEquityRates(). ',' ; } elseif( true == array_key_exists( 'AllowEquityRates', $this->getChangedColumns() ) ) { $strSql .= ' allow_equity_rates = ' . $this->sqlAllowEquityRates() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_expense_rates = ' . $this->sqlAllowExpenseRates(). ',' ; } elseif( true == array_key_exists( 'AllowExpenseRates', $this->getChangedColumns() ) ) { $strSql .= ' allow_expense_rates = ' . $this->sqlAllowExpenseRates() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_liability_rates = ' . $this->sqlAllowLiabilityRates(). ',' ; } elseif( true == array_key_exists( 'AllowLiabilityRates', $this->getChangedColumns() ) ) { $strSql .= ' allow_liability_rates = ' . $this->sqlAllowLiabilityRates() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pre_qualification_trigger = ' . $this->sqlAllowPreQualificationTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowPreQualificationTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_pre_qualification_trigger = ' . $this->sqlAllowPreQualificationTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_application_approval_trigger = ' . $this->sqlAllowApplicationApprovalTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowApplicationApprovalTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_application_approval_trigger = ' . $this->sqlAllowApplicationApprovalTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_lease_completed_trigger = ' . $this->sqlAllowLeaseCompletedTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowLeaseCompletedTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_lease_completed_trigger = ' . $this->sqlAllowLeaseCompletedTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_lease_approval_trigger = ' . $this->sqlAllowLeaseApprovalTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowLeaseApprovalTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_lease_approval_trigger = ' . $this->sqlAllowLeaseApprovalTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_last_lease_month_trigger = ' . $this->sqlAllowLastLeaseMonthTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowLastLeaseMonthTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_last_lease_month_trigger = ' . $this->sqlAllowLastLeaseMonthTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_notice_trigger = ' . $this->sqlAllowNoticeTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowNoticeTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_notice_trigger = ' . $this->sqlAllowNoticeTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_anniversary_of_move_in_trigger = ' . $this->sqlAllowAnniversaryOfMoveInTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowAnniversaryOfMoveInTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_anniversary_of_move_in_trigger = ' . $this->sqlAllowAnniversaryOfMoveInTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_end_of_year_trigger = ' . $this->sqlAllowEndOfYearTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowEndOfYearTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_end_of_year_trigger = ' . $this->sqlAllowEndOfYearTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_hourly_trigger = ' . $this->sqlAllowHourlyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowHourlyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_hourly_trigger = ' . $this->sqlAllowHourlyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_daily_trigger = ' . $this->sqlAllowDailyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowDailyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_daily_trigger = ' . $this->sqlAllowDailyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_weekly_trigger = ' . $this->sqlAllowWeeklyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowWeeklyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_weekly_trigger = ' . $this->sqlAllowWeeklyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_every_two_weeks_trigger = ' . $this->sqlAllowEveryTwoWeeksTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowEveryTwoWeeksTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_every_two_weeks_trigger = ' . $this->sqlAllowEveryTwoWeeksTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_twice_per_month_trigger = ' . $this->sqlAllowTwicePerMonthTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowTwicePerMonthTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_twice_per_month_trigger = ' . $this->sqlAllowTwicePerMonthTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_specific_months_trigger = ' . $this->sqlAllowSpecificMonthsTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowSpecificMonthsTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_specific_months_trigger = ' . $this->sqlAllowSpecificMonthsTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_quarterly_trigger = ' . $this->sqlAllowQuarterlyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowQuarterlyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_quarterly_trigger = ' . $this->sqlAllowQuarterlyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_twice_per_year_trigger = ' . $this->sqlAllowTwicePerYearTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowTwicePerYearTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_twice_per_year_trigger = ' . $this->sqlAllowTwicePerYearTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_yearly_trigger = ' . $this->sqlAllowYearlyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowYearlyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_yearly_trigger = ' . $this->sqlAllowYearlyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_screening_approved_trigger = ' . $this->sqlAllowScreeningApprovedTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowScreeningApprovedTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_screening_approved_trigger = ' . $this->sqlAllowScreeningApprovedTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_accel_rent_in_current_post_month = ' . $this->sqlPostAccelRentInCurrentPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostAccelRentInCurrentPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_accel_rent_in_current_post_month = ' . $this->sqlPostAccelRentInCurrentPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_repayment_agreements = ' . $this->sqlAllowRepaymentAgreements(). ',' ; } elseif( true == array_key_exists( 'AllowRepaymentAgreements', $this->getChangedColumns() ) ) { $strSql .= ' allow_repayment_agreements = ' . $this->sqlAllowRepaymentAgreements() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_accelerated_rent_at_move_in = ' . $this->sqlRefundAcceleratedRentAtMoveIn(). ',' ; } elseif( true == array_key_exists( 'RefundAcceleratedRentAtMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' refund_accelerated_rent_at_move_in = ' . $this->sqlRefundAcceleratedRentAtMoveIn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeOtherInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_maximum_allowed_months = ' . $this->sqlRepaymentMaximumAllowedMonths(). ',' ; } elseif( true == array_key_exists( 'RepaymentMaximumAllowedMonths', $this->getChangedColumns() ) ) { $strSql .= ' repayment_maximum_allowed_months = ' . $this->sqlRepaymentMaximumAllowedMonths() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_agreement_ar_code_id = ' . $this->sqlRepaymentAgreementArCodeId(). ',' ; } elseif( true == array_key_exists( 'RepaymentAgreementArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' repayment_agreement_ar_code_id = ' . $this->sqlRepaymentAgreementArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_allocation_order = ' . $this->sqlRepaymentAllocationOrder(). ',' ; } elseif( true == array_key_exists( 'RepaymentAllocationOrder', $this->getChangedColumns() ) ) { $strSql .= ' repayment_allocation_order = ' . $this->sqlRepaymentAllocationOrder() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_charge_late_fees_retroactively = ' . $this->sqlRepaymentChargeLateFeesRetroactively(). ',' ; } elseif( true == array_key_exists( 'RepaymentChargeLateFeesRetroactively', $this->getChangedColumns() ) ) { $strSql .= ' repayment_charge_late_fees_retroactively = ' . $this->sqlRepaymentChargeLateFeesRetroactively() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_mtm_multiplier_lease_term_id = ' . $this->sqlDefaultMtmMultiplierLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'DefaultMtmMultiplierLeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' default_mtm_multiplier_lease_term_id = ' . $this->sqlDefaultMtmMultiplierLeaseTermId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_mtm_multiplier = ' . $this->sqlDefaultMtmMultiplier(). ',' ; } elseif( true == array_key_exists( 'DefaultMtmMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' default_mtm_multiplier = ' . $this->sqlDefaultMtmMultiplier() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_repayments_for_active_leases = ' . $this->sqlAllowRepaymentsForActiveLeases(). ',' ; } elseif( true == array_key_exists( 'AllowRepaymentsForActiveLeases', $this->getChangedColumns() ) ) { $strSql .= ' allow_repayments_for_active_leases = ' . $this->sqlAllowRepaymentsForActiveLeases() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_current_date_for_floorplan_range = ' . $this->sqlUseCurrentDateForFloorplanRange(). ',' ; } elseif( true == array_key_exists( 'UseCurrentDateForFloorplanRange', $this->getChangedColumns() ) ) { $strSql .= ' use_current_date_for_floorplan_range = ' . $this->sqlUseCurrentDateForFloorplanRange() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeTaxInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_future_leases = ' . $this->sqlBillFutureLeases(). ',' ; } elseif( true == array_key_exists( 'BillFutureLeases', $this->getChangedColumns() ) ) { $strSql .= ' bill_future_leases = ' . $this->sqlBillFutureLeases() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_early_move_in = ' . $this->sqlBillEarlyMoveIn(). ',' ; } elseif( true == array_key_exists( 'BillEarlyMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' bill_early_move_in = ' . $this->sqlBillEarlyMoveIn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_rates_for_early_move_ins = ' . $this->sqlUseRatesForEarlyMoveIns(). ',' ; } elseif( true == array_key_exists( 'UseRatesForEarlyMoveIns', $this->getChangedColumns() ) ) { $strSql .= ' use_rates_for_early_move_ins = ' . $this->sqlUseRatesForEarlyMoveIns() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_charges_at_notice = ' . $this->sqlEndChargesAtNotice(). ',' ; } elseif( true == array_key_exists( 'EndChargesAtNotice', $this->getChangedColumns() ) ) { $strSql .= ' end_charges_at_notice = ' . $this->sqlEndChargesAtNotice() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocate_taxes_proportionately = ' . $this->sqlAllocateTaxesProportionately(). ',' ; } elseif( true == array_key_exists( 'AllocateTaxesProportionately', $this->getChangedColumns() ) ) { $strSql .= ' allocate_taxes_proportionately = ' . $this->sqlAllocateTaxesProportionately() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_early_move_ins_immediately = ' . $this->sqlBillEarlyMoveInsImmediately(). ',' ; } elseif( true == array_key_exists( 'BillEarlyMoveInsImmediately', $this->getChangedColumns() ) ) { $strSql .= ' bill_early_move_ins_immediately = ' . $this->sqlBillEarlyMoveInsImmediately() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_nightly_trigger = ' . $this->sqlAllowNightlyTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowNightlyTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_nightly_trigger = ' . $this->sqlAllowNightlyTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_renewal_offer_accepted_trigger = ' . $this->sqlAllowRenewalOfferAcceptedTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowRenewalOfferAcceptedTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_renewal_offer_accepted_trigger = ' . $this->sqlAllowRenewalOfferAcceptedTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_service_selected_trigger = ' . $this->sqlAllowServiceSelectedTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowServiceSelectedTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_service_selected_trigger = ' . $this->sqlAllowServiceSelectedTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_service_fulfilled_trigger = ' . $this->sqlAllowServiceFulfilledTrigger(). ',' ; } elseif( true == array_key_exists( 'AllowServiceFulfilledTrigger', $this->getChangedColumns() ) ) { $strSql .= ' allow_service_fulfilled_trigger = ' . $this->sqlAllowServiceFulfilledTrigger() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proration_method_type_id = ' . $this->sqlProrationMethodTypeId(). ',' ; } elseif( true == array_key_exists( 'ProrationMethodTypeId', $this->getChangedColumns() ) ) { $strSql .= ' proration_method_type_id = ' . $this->sqlProrationMethodTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' create_mtm_charges = ' . $this->sqlCreateMtmCharges(). ',' ; } elseif( true == array_key_exists( 'CreateMtmCharges', $this->getChangedColumns() ) ) { $strSql .= ' create_mtm_charges = ' . $this->sqlCreateMtmCharges() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_current_post_month_for_repayments = ' . $this->sqlUseCurrentPostMonthForRepayments(). ',' ; } elseif( true == array_key_exists( 'UseCurrentPostMonthForRepayments', $this->getChangedColumns() ) ) { $strSql .= ' use_current_post_month_for_repayments = ' . $this->sqlUseCurrentPostMonthForRepayments() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_max_amount = ' . $this->sqlRepaymentMaxAmount(). ',' ; } elseif( true == array_key_exists( 'RepaymentMaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' repayment_max_amount = ' . $this->sqlRepaymentMaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_max_amount_ar_formula_id = ' . $this->sqlRepaymentMaxAmountArFormulaId(). ',' ; } elseif( true == array_key_exists( 'RepaymentMaxAmountArFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' repayment_max_amount_ar_formula_id = ' . $this->sqlRepaymentMaxAmountArFormulaId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_max_amount_ar_formula_reference_id = ' . $this->sqlRepaymentMaxAmountArFormulaReferenceId(). ',' ; } elseif( true == array_key_exists( 'RepaymentMaxAmountArFormulaReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' repayment_max_amount_ar_formula_reference_id = ' . $this->sqlRepaymentMaxAmountArFormulaReferenceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_max_percent = ' . $this->sqlRepaymentMaxPercent(). ',' ; } elseif( true == array_key_exists( 'RepaymentMaxPercent', $this->getChangedColumns() ) ) { $strSql .= ' repayment_max_percent = ' . $this->sqlRepaymentMaxPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prorate_straight_line_adjustment_posting = ' . $this->sqlProrateStraightLineAdjustmentPosting(). ',' ; } elseif( true == array_key_exists( 'ProrateStraightLineAdjustmentPosting', $this->getChangedColumns() ) ) { $strSql .= ' prorate_straight_line_adjustment_posting = ' . $this->sqlProrateStraightLineAdjustmentPosting() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_term_structure_id' => $this->getLeaseTermStructureId(),
			'lease_start_structure_id' => $this->getLeaseStartStructureId(),
			'accelerated_rent_type_id' => $this->getAcceleratedRentTypeId(),
			'base_rent_renewal_option_id' => $this->getBaseRentRenewalOptionId(),
			'other_fees_renewal_option_id' => $this->getOtherFeesRenewalOptionId(),
			'lease_end_billing_type_id' => $this->getLeaseEndBillingTypeId(),
			'round_type_id' => $this->getRoundTypeId(),
			'month_to_month_type_id' => $this->getMonthToMonthTypeId(),
			'month_to_month_rent_ar_code_id' => $this->getMonthToMonthRentArCodeId(),
			'enforce_unit_space_unions' => $this->getEnforceUnitSpaceUnions(),
			'prorate_charges' => $this->getProrateCharges(),
			'ignore_month_to_month_intervals' => $this->getIgnoreMonthToMonthIntervals(),
			'use_30_day_month' => $this->getUse30DayMonth(),
			'temporary_charge_gl_post_day' => $this->getTemporaryChargeGlPostDay(),
			'charge_move_in_day' => $this->getChargeMoveInDay(),
			'charge_move_out_day' => $this->getChargeMoveOutDay(),
			'immediate_move_in_prorate_charges' => $this->getImmediateMoveInProrateCharges(),
			'immediate_move_in_use_30_day_month' => $this->getImmediateMoveInUse30DayMonth(),
			'immediate_move_in_charge_move_in_day' => $this->getImmediateMoveInChargeMoveInDay(),
			'write_off_accelerated_rent' => $this->getWriteOffAcceleratedRent(),
			'auto_post_scheduled_charges' => $this->getAutoPostScheduledCharges(),
			'base_rent_renewal_factor' => $this->getBaseRentRenewalFactor(),
			'other_fees_renewal_factor' => $this->getOtherFeesRenewalFactor(),
			'scheduled_charge_auto_post_day' => $this->getScheduledChargeAutoPostDay(),
			'scheduled_charge_auto_post_through' => $this->getScheduledChargeAutoPostThrough(),
			'post_through_next_month' => $this->getPostThroughNextMonth(),
			'show_rent_amount_on_quick_view' => $this->getShowRentAmountOnQuickView(),
			'start_renewal_charges_on_first' => $this->getStartRenewalChargesOnFirst(),
			'start_mtm_charges_on_first' => $this->getStartMtmChargesOnFirst(),
			'require_installments' => $this->getRequireInstallments(),
			'available_space_limit_days' => $this->getAvailableSpaceLimitDays(),
			'on_notice_space_limit_days' => $this->getOnNoticeSpaceLimitDays(),
			'early_display_allowance_days' => $this->getEarlyDisplayAllowanceDays(),
			'show_available_not_ready_spaces' => $this->getShowAvailableNotReadySpaces(),
			'show_notice_available_spaces' => $this->getShowNoticeAvailableSpaces(),
			'allow_rate_offsets' => $this->getAllowRateOffsets(),
			'allow_late_fee_overrides' => $this->getAllowLateFeeOverrides(),
			'allow_asset_rates' => $this->getAllowAssetRates(),
			'allow_equity_rates' => $this->getAllowEquityRates(),
			'allow_expense_rates' => $this->getAllowExpenseRates(),
			'allow_liability_rates' => $this->getAllowLiabilityRates(),
			'allow_pre_qualification_trigger' => $this->getAllowPreQualificationTrigger(),
			'allow_application_approval_trigger' => $this->getAllowApplicationApprovalTrigger(),
			'allow_lease_completed_trigger' => $this->getAllowLeaseCompletedTrigger(),
			'allow_lease_approval_trigger' => $this->getAllowLeaseApprovalTrigger(),
			'allow_last_lease_month_trigger' => $this->getAllowLastLeaseMonthTrigger(),
			'allow_notice_trigger' => $this->getAllowNoticeTrigger(),
			'allow_anniversary_of_move_in_trigger' => $this->getAllowAnniversaryOfMoveInTrigger(),
			'allow_end_of_year_trigger' => $this->getAllowEndOfYearTrigger(),
			'allow_hourly_trigger' => $this->getAllowHourlyTrigger(),
			'allow_daily_trigger' => $this->getAllowDailyTrigger(),
			'allow_weekly_trigger' => $this->getAllowWeeklyTrigger(),
			'allow_every_two_weeks_trigger' => $this->getAllowEveryTwoWeeksTrigger(),
			'allow_twice_per_month_trigger' => $this->getAllowTwicePerMonthTrigger(),
			'allow_specific_months_trigger' => $this->getAllowSpecificMonthsTrigger(),
			'allow_quarterly_trigger' => $this->getAllowQuarterlyTrigger(),
			'allow_twice_per_year_trigger' => $this->getAllowTwicePerYearTrigger(),
			'allow_yearly_trigger' => $this->getAllowYearlyTrigger(),
			'allow_screening_approved_trigger' => $this->getAllowScreeningApprovedTrigger(),
			'post_accel_rent_in_current_post_month' => $this->getPostAccelRentInCurrentPostMonth(),
			'allow_repayment_agreements' => $this->getAllowRepaymentAgreements(),
			'refund_accelerated_rent_at_move_in' => $this->getRefundAcceleratedRentAtMoveIn(),
			'include_other_in_advertised_rent' => $this->getIncludeOtherInAdvertisedRent(),
			'repayment_maximum_allowed_months' => $this->getRepaymentMaximumAllowedMonths(),
			'repayment_agreement_ar_code_id' => $this->getRepaymentAgreementArCodeId(),
			'repayment_allocation_order' => $this->getRepaymentAllocationOrder(),
			'repayment_charge_late_fees_retroactively' => $this->getRepaymentChargeLateFeesRetroactively(),
			'default_mtm_multiplier_lease_term_id' => $this->getDefaultMtmMultiplierLeaseTermId(),
			'default_mtm_multiplier' => $this->getDefaultMtmMultiplier(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'allow_repayments_for_active_leases' => $this->getAllowRepaymentsForActiveLeases(),
			'use_current_date_for_floorplan_range' => $this->getUseCurrentDateForFloorplanRange(),
			'include_tax_in_advertised_rent' => $this->getIncludeTaxInAdvertisedRent(),
			'bill_future_leases' => $this->getBillFutureLeases(),
			'bill_early_move_in' => $this->getBillEarlyMoveIn(),
			'use_rates_for_early_move_ins' => $this->getUseRatesForEarlyMoveIns(),
			'end_charges_at_notice' => $this->getEndChargesAtNotice(),
			'allocate_taxes_proportionately' => $this->getAllocateTaxesProportionately(),
			'bill_early_move_ins_immediately' => $this->getBillEarlyMoveInsImmediately(),
			'allow_nightly_trigger' => $this->getAllowNightlyTrigger(),
			'allow_renewal_offer_accepted_trigger' => $this->getAllowRenewalOfferAcceptedTrigger(),
			'allow_service_selected_trigger' => $this->getAllowServiceSelectedTrigger(),
			'allow_service_fulfilled_trigger' => $this->getAllowServiceFulfilledTrigger(),
			'proration_method_type_id' => $this->getProrationMethodTypeId(),
			'create_mtm_charges' => $this->getCreateMtmCharges(),
			'use_current_post_month_for_repayments' => $this->getUseCurrentPostMonthForRepayments(),
			'repayment_max_amount' => $this->getRepaymentMaxAmount(),
			'repayment_max_amount_ar_formula_id' => $this->getRepaymentMaxAmountArFormulaId(),
			'repayment_max_amount_ar_formula_reference_id' => $this->getRepaymentMaxAmountArFormulaReferenceId(),
			'repayment_max_percent' => $this->getRepaymentMaxPercent(),
			'prorate_straight_line_adjustment_posting' => $this->getProrateStraightLineAdjustmentPosting()
		);
	}

}
?>