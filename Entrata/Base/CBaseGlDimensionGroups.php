<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDimensionGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlDimensionGroups extends CEosPluralBase {

	/**
	 * @return CGlDimensionGroup[]
	 */
	public static function fetchGlDimensionGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlDimensionGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlDimensionGroup
	 */
	public static function fetchGlDimensionGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlDimensionGroup', $objDatabase );
	}

	public static function fetchGlDimensionGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_dimension_groups', $objDatabase );
	}

	public static function fetchGlDimensionGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlDimensionGroup( sprintf( 'SELECT * FROM gl_dimension_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlDimensionGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchGlDimensionGroups( sprintf( 'SELECT * FROM gl_dimension_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlDimensionGroupsByDefaultGlDimensionGroupIdByCid( $intDefaultGlDimensionGroupId, $intCid, $objDatabase ) {
		return self::fetchGlDimensionGroups( sprintf( 'SELECT * FROM gl_dimension_groups WHERE default_gl_dimension_group_id = %d AND cid = %d', ( int ) $intDefaultGlDimensionGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>