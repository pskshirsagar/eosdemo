<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConditionTypes
 * Do not add any new functions to this class.
 */

class CBaseTransmissionConditionTypes extends CEosPluralBase {

	/**
	 * @return CTransmissionConditionType[]
	 */
	public static function fetchTransmissionConditionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransmissionConditionType::class, $objDatabase );
	}

	/**
	 * @return CTransmissionConditionType
	 */
	public static function fetchTransmissionConditionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransmissionConditionType::class, $objDatabase );
	}

	public static function fetchTransmissionConditionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transmission_condition_types', $objDatabase );
	}

	public static function fetchTransmissionConditionTypeById( $intId, $objDatabase ) {
		return self::fetchTransmissionConditionType( sprintf( 'SELECT * FROM transmission_condition_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>