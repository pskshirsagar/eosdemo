<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COnSiteContactTypes
 * Do not add any new functions to this class.
 */

class CBaseOnSiteContactTypes extends CEosPluralBase {

	/**
	 * @return COnSiteContactType[]
	 */
	public static function fetchOnSiteContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COnSiteContactType::class, $objDatabase );
	}

	/**
	 * @return COnSiteContactType
	 */
	public static function fetchOnSiteContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COnSiteContactType::class, $objDatabase );
	}

	public static function fetchOnSiteContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'on_site_contact_types', $objDatabase );
	}

	public static function fetchOnSiteContactTypeById( $intId, $objDatabase ) {
		return self::fetchOnSiteContactType( sprintf( 'SELECT * FROM on_site_contact_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>