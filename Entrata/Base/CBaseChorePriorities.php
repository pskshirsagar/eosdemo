<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChorePriorities
 * Do not add any new functions to this class.
 */

class CBaseChorePriorities extends CEosPluralBase {

	/**
	 * @return CChorePriority[]
	 */
	public static function fetchChorePriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChorePriority::class, $objDatabase );
	}

	/**
	 * @return CChorePriority
	 */
	public static function fetchChorePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChorePriority::class, $objDatabase );
	}

	public static function fetchChorePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_priorities', $objDatabase );
	}

	public static function fetchChorePriorityById( $intId, $objDatabase ) {
		return self::fetchChorePriority( sprintf( 'SELECT * FROM chore_priorities WHERE id = %d', $intId ), $objDatabase );
	}

}
?>