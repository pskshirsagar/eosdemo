<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyIdentificationTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyIdentificationTypes extends CEosPluralBase {

	/**
	 * @return CCompanyIdentificationType[]
	 */
	public static function fetchCompanyIdentificationTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyIdentificationType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyIdentificationType
	 */
	public static function fetchCompanyIdentificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyIdentificationType::class, $objDatabase );
	}

	public static function fetchCompanyIdentificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_identification_types', $objDatabase );
	}

	public static function fetchCompanyIdentificationTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyIdentificationType( sprintf( 'SELECT * FROM company_identification_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyIdentificationTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyIdentificationTypes( sprintf( 'SELECT * FROM company_identification_types WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>