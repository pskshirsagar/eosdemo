<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationVersions
 * Do not add any new functions to this class.
 */

class CBaseIntegrationVersions extends CEosPluralBase {

	/**
	 * @return CIntegrationVersion[]
	 */
	public static function fetchIntegrationVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIntegrationVersion', $objDatabase );
	}

	/**
	 * @return CIntegrationVersion
	 */
	public static function fetchIntegrationVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationVersion', $objDatabase );
	}

	public static function fetchIntegrationVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_versions', $objDatabase );
	}

	public static function fetchIntegrationVersionById( $intId, $objDatabase ) {
		return self::fetchIntegrationVersion( sprintf( 'SELECT * FROM integration_versions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIntegrationVersionsByIntegrationClientTypeId( $intIntegrationClientTypeId, $objDatabase ) {
		return self::fetchIntegrationVersions( sprintf( 'SELECT * FROM integration_versions WHERE integration_client_type_id = %d', ( int ) $intIntegrationClientTypeId ), $objDatabase );
	}

}
?>