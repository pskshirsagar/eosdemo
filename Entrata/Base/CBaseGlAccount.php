<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlChartId;
	protected $m_intGlGroupTypeId;
	protected $m_intDefaultGlAccountId;
	protected $m_intCashFlowTypeId;
	protected $m_intGlAccountTypeId;
	protected $m_intApCodeId;
	protected $m_intCamAllowanceTypeId;
	protected $m_intCategorizationTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSystemCode;
	protected $m_boolRestrictForAp;
	protected $m_boolRestrictForGl;
	protected $m_boolIsConfidential;
	protected $m_boolIsCategorizationQueued;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intGlAccountUsageTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intCamAllowanceTypeId = '1';
		$this->m_intCategorizationTypeId = '1';
		$this->m_boolRestrictForAp = false;
		$this->m_boolRestrictForGl = false;
		$this->m_boolIsConfidential = false;
		$this->m_boolIsCategorizationQueued = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intGlChartId', trim( $arrValues['gl_chart_id'] ) ); elseif( isset( $arrValues['gl_chart_id'] ) ) $this->setGlChartId( $arrValues['gl_chart_id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlAccountId', trim( $arrValues['default_gl_account_id'] ) ); elseif( isset( $arrValues['default_gl_account_id'] ) ) $this->setDefaultGlAccountId( $arrValues['default_gl_account_id'] );
		if( isset( $arrValues['cash_flow_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCashFlowTypeId', trim( $arrValues['cash_flow_type_id'] ) ); elseif( isset( $arrValues['cash_flow_type_id'] ) ) $this->setCashFlowTypeId( $arrValues['cash_flow_type_id'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['cam_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCamAllowanceTypeId', trim( $arrValues['cam_allowance_type_id'] ) ); elseif( isset( $arrValues['cam_allowance_type_id'] ) ) $this->setCamAllowanceTypeId( $arrValues['cam_allowance_type_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( $arrValues['system_code'] ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
		if( isset( $arrValues['restrict_for_ap'] ) && $boolDirectSet ) $this->set( 'm_boolRestrictForAp', trim( stripcslashes( $arrValues['restrict_for_ap'] ) ) ); elseif( isset( $arrValues['restrict_for_ap'] ) ) $this->setRestrictForAp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['restrict_for_ap'] ) : $arrValues['restrict_for_ap'] );
		if( isset( $arrValues['restrict_for_gl'] ) && $boolDirectSet ) $this->set( 'm_boolRestrictForGl', trim( stripcslashes( $arrValues['restrict_for_gl'] ) ) ); elseif( isset( $arrValues['restrict_for_gl'] ) ) $this->setRestrictForGl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['restrict_for_gl'] ) : $arrValues['restrict_for_gl'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['is_categorization_queued'] ) && $boolDirectSet ) $this->set( 'm_boolIsCategorizationQueued', trim( stripcslashes( $arrValues['is_categorization_queued'] ) ) ); elseif( isset( $arrValues['is_categorization_queued'] ) ) $this->setIsCategorizationQueued( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_categorization_queued'] ) : $arrValues['is_categorization_queued'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['gl_account_usage_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountUsageTypeId', trim( $arrValues['gl_account_usage_type_id'] ) ); elseif( isset( $arrValues['gl_account_usage_type_id'] ) ) $this->setGlAccountUsageTypeId( $arrValues['gl_account_usage_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlChartId( $intGlChartId ) {
		$this->set( 'm_intGlChartId', CStrings::strToIntDef( $intGlChartId, NULL, false ) );
	}

	public function getGlChartId() {
		return $this->m_intGlChartId;
	}

	public function sqlGlChartId() {
		return ( true == isset( $this->m_intGlChartId ) ) ? ( string ) $this->m_intGlChartId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setDefaultGlAccountId( $intDefaultGlAccountId ) {
		$this->set( 'm_intDefaultGlAccountId', CStrings::strToIntDef( $intDefaultGlAccountId, NULL, false ) );
	}

	public function getDefaultGlAccountId() {
		return $this->m_intDefaultGlAccountId;
	}

	public function sqlDefaultGlAccountId() {
		return ( true == isset( $this->m_intDefaultGlAccountId ) ) ? ( string ) $this->m_intDefaultGlAccountId : 'NULL';
	}

	public function setCashFlowTypeId( $intCashFlowTypeId ) {
		$this->set( 'm_intCashFlowTypeId', CStrings::strToIntDef( $intCashFlowTypeId, NULL, false ) );
	}

	public function getCashFlowTypeId() {
		return $this->m_intCashFlowTypeId;
	}

	public function sqlCashFlowTypeId() {
		return ( true == isset( $this->m_intCashFlowTypeId ) ) ? ( string ) $this->m_intCashFlowTypeId : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setCamAllowanceTypeId( $intCamAllowanceTypeId ) {
		$this->set( 'm_intCamAllowanceTypeId', CStrings::strToIntDef( $intCamAllowanceTypeId, NULL, false ) );
	}

	public function getCamAllowanceTypeId() {
		return $this->m_intCamAllowanceTypeId;
	}

	public function sqlCamAllowanceTypeId() {
		return ( true == isset( $this->m_intCamAllowanceTypeId ) ) ? ( string ) $this->m_intCamAllowanceTypeId : '1';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '1';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystemCode ) : '\'' . addslashes( $this->m_strSystemCode ) . '\'' ) : 'NULL';
	}

	public function setRestrictForAp( $boolRestrictForAp ) {
		$this->set( 'm_boolRestrictForAp', CStrings::strToBool( $boolRestrictForAp ) );
	}

	public function getRestrictForAp() {
		return $this->m_boolRestrictForAp;
	}

	public function sqlRestrictForAp() {
		return ( true == isset( $this->m_boolRestrictForAp ) ) ? '\'' . ( true == ( bool ) $this->m_boolRestrictForAp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRestrictForGl( $boolRestrictForGl ) {
		$this->set( 'm_boolRestrictForGl', CStrings::strToBool( $boolRestrictForGl ) );
	}

	public function getRestrictForGl() {
		return $this->m_boolRestrictForGl;
	}

	public function sqlRestrictForGl() {
		return ( true == isset( $this->m_boolRestrictForGl ) ) ? '\'' . ( true == ( bool ) $this->m_boolRestrictForGl ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCategorizationQueued( $boolIsCategorizationQueued ) {
		$this->set( 'm_boolIsCategorizationQueued', CStrings::strToBool( $boolIsCategorizationQueued ) );
	}

	public function getIsCategorizationQueued() {
		return $this->m_boolIsCategorizationQueued;
	}

	public function sqlIsCategorizationQueued() {
		return ( true == isset( $this->m_boolIsCategorizationQueued ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCategorizationQueued ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGlAccountUsageTypeId( $intGlAccountUsageTypeId ) {
		$this->set( 'm_intGlAccountUsageTypeId', CStrings::strToIntDef( $intGlAccountUsageTypeId, NULL, false ) );
	}

	public function getGlAccountUsageTypeId() {
		return $this->m_intGlAccountUsageTypeId;
	}

	public function sqlGlAccountUsageTypeId() {
		return ( true == isset( $this->m_intGlAccountUsageTypeId ) ) ? ( string ) $this->m_intGlAccountUsageTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_chart_id, gl_group_type_id, default_gl_account_id, cash_flow_type_id, gl_account_type_id, ap_code_id, cam_allowance_type_id, categorization_type_id, remote_primary_key, system_code, restrict_for_ap, restrict_for_gl, is_confidential, is_categorization_queued, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, gl_account_usage_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlChartId() . ', ' .
						$this->sqlGlGroupTypeId() . ', ' .
						$this->sqlDefaultGlAccountId() . ', ' .
						$this->sqlCashFlowTypeId() . ', ' .
						$this->sqlGlAccountTypeId() . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlCamAllowanceTypeId() . ', ' .
						$this->sqlCategorizationTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlRestrictForAp() . ', ' .
						$this->sqlRestrictForGl() . ', ' .
						$this->sqlIsConfidential() . ', ' .
						$this->sqlIsCategorizationQueued() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlGlAccountUsageTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId(). ',' ; } elseif( true == array_key_exists( 'GlChartId', $this->getChangedColumns() ) ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId(). ',' ; } elseif( true == array_key_exists( 'GlGroupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_type_id = ' . $this->sqlGlGroupTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_gl_account_id = ' . $this->sqlDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' default_gl_account_id = ' . $this->sqlDefaultGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_flow_type_id = ' . $this->sqlCashFlowTypeId(). ',' ; } elseif( true == array_key_exists( 'CashFlowTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cash_flow_type_id = ' . $this->sqlCashFlowTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'GlAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_type_id = ' . $this->sqlGlAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cam_allowance_type_id = ' . $this->sqlCamAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'CamAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cam_allowance_type_id = ' . $this->sqlCamAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId(). ',' ; } elseif( true == array_key_exists( 'CategorizationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restrict_for_ap = ' . $this->sqlRestrictForAp(). ',' ; } elseif( true == array_key_exists( 'RestrictForAp', $this->getChangedColumns() ) ) { $strSql .= ' restrict_for_ap = ' . $this->sqlRestrictForAp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restrict_for_gl = ' . $this->sqlRestrictForGl(). ',' ; } elseif( true == array_key_exists( 'RestrictForGl', $this->getChangedColumns() ) ) { $strSql .= ' restrict_for_gl = ' . $this->sqlRestrictForGl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued(). ',' ; } elseif( true == array_key_exists( 'IsCategorizationQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_usage_type_id = ' . $this->sqlGlAccountUsageTypeId(). ',' ; } elseif( true == array_key_exists( 'GlAccountUsageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_usage_type_id = ' . $this->sqlGlAccountUsageTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_chart_id' => $this->getGlChartId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'default_gl_account_id' => $this->getDefaultGlAccountId(),
			'cash_flow_type_id' => $this->getCashFlowTypeId(),
			'gl_account_type_id' => $this->getGlAccountTypeId(),
			'ap_code_id' => $this->getApCodeId(),
			'cam_allowance_type_id' => $this->getCamAllowanceTypeId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'system_code' => $this->getSystemCode(),
			'restrict_for_ap' => $this->getRestrictForAp(),
			'restrict_for_gl' => $this->getRestrictForGl(),
			'is_confidential' => $this->getIsConfidential(),
			'is_categorization_queued' => $this->getIsCategorizationQueued(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'gl_account_usage_type_id' => $this->getGlAccountUsageTypeId()
		);
	}

}
?>