<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataCleanupTypes
 * Do not add any new functions to this class.
 */

class CBaseDataCleanupTypes extends CEosPluralBase {

	/**
	 * @return CDataCleanupType[]
	 */
	public static function fetchDataCleanupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDataCleanupType::class, $objDatabase );
	}

	/**
	 * @return CDataCleanupType
	 */
	public static function fetchDataCleanupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDataCleanupType::class, $objDatabase );
	}

	public static function fetchDataCleanupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_cleanup_types', $objDatabase );
	}

	public static function fetchDataCleanupTypeById( $intId, $objDatabase ) {
		return self::fetchDataCleanupType( sprintf( 'SELECT * FROM data_cleanup_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>