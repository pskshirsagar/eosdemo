<?php

class CBaseSpecialTargets extends CEosPluralBase {

    const TABLE_SPECIAL_TARGETS = 'public.special_targets';

    public static function fetchSpecialTargets( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CSpecialTarget', $objDatabase );
    }

    public static function fetchSpecialTarget( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CSpecialTarget', $objDatabase );
    }

    public static function fetchSpecialTargetCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'special_targets', $objDatabase );
    }

    public static function fetchSpecialTargetById( $intId, $objDatabase ) {
        return self::fetchSpecialTarget( sprintf( 'SELECT * FROM special_targets WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>