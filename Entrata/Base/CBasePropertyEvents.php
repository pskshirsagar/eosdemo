<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEvents extends CEosPluralBase {

	/**
	 * @return CPropertyEvent[]
	 */
	public static function fetchPropertyEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyEvent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyEvent
	 */
	public static function fetchPropertyEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyEvent', $objDatabase );
	}

	public static function fetchPropertyEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_events', $objDatabase );
	}

	public static function fetchPropertyEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyEvent( sprintf( 'SELECT * FROM property_events WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEventsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyEvents( sprintf( 'SELECT * FROM property_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEventsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyEvents( sprintf( 'SELECT * FROM property_events WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEventsByCompanyEventIdByCid( $intCompanyEventId, $intCid, $objDatabase ) {
		return self::fetchPropertyEvents( sprintf( 'SELECT * FROM property_events WHERE company_event_id = %d AND cid = %d', ( int ) $intCompanyEventId, ( int ) $intCid ), $objDatabase );
	}

}
?>