<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitLists
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWaitLists extends CEosPluralBase {

	/**
	 * @return CWaitList[]
	 */
	public static function fetchWaitLists( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CWaitList', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CWaitList
	 */
	public static function fetchWaitList( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CWaitList', $objDatabase );
	}

	public static function fetchWaitListCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'wait_lists', $objDatabase );
	}

	public static function fetchWaitListByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchWaitList( sprintf( 'SELECT * FROM wait_lists WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListsByCid( $intCid, $objDatabase ) {
		return self::fetchWaitLists( sprintf( 'SELECT * FROM wait_lists WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchWaitLists( sprintf( 'SELECT * FROM wait_lists WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchWaitLists( sprintf( 'SELECT * FROM wait_lists WHERE occupancy_type_id = %d AND cid = %d', ( int ) $intOccupancyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListsByApplicationStageStatusIdByCid( $intApplicationStageStatusId, $intCid, $objDatabase ) {
		return self::fetchWaitLists( sprintf( 'SELECT * FROM wait_lists WHERE application_stage_status_id = %d AND cid = %d', ( int ) $intApplicationStageStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>