<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantResults
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicantResults extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantResult[]
	 */
	public static function fetchScreeningApplicantResults( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicantResult', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningApplicantResult
	 */
	public static function fetchScreeningApplicantResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicantResult', $objDatabase );
	}

	public static function fetchScreeningApplicantResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_results', $objDatabase );
	}

	public static function fetchScreeningApplicantResultByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResult( sprintf( 'SELECT * FROM screening_applicant_results WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByScreeningApplicationRequestIdByCid( $intScreeningApplicationRequestId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE screening_application_request_id = %d AND cid = %d', ( int ) $intScreeningApplicationRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE screening_recommendation_type_id = %d AND cid = %d', ( int ) $intScreeningRecommendationTypeId, ( int ) $intCid ), $objDatabase );
	}

    public static function fetchScreeningApplicantResultsByApplicantIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase ) {
        return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE applicant_id = %d AND application_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
    }


	public static function fetchScreeningApplicantResultsByRequestStatusTypeIdByCid( $intRequestStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResults( sprintf( 'SELECT * FROM screening_applicant_results WHERE request_status_type_id = %d AND cid = %d', ( int ) $intRequestStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>