<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyConciergeServices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyConciergeServices extends CEosPluralBase {

	/**
	 * @return CCompanyConciergeService[]
	 */
	public static function fetchCompanyConciergeServices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyConciergeService::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyConciergeService
	 */
	public static function fetchCompanyConciergeService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyConciergeService::class, $objDatabase );
	}

	public static function fetchCompanyConciergeServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_concierge_services', $objDatabase );
	}

	public static function fetchCompanyConciergeServiceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyConciergeService( sprintf( 'SELECT * FROM company_concierge_services WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyConciergeServicesByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCompanyConciergeServices( sprintf( 'SELECT * FROM company_concierge_services WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyConciergeServicesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyConciergeServices( sprintf( 'SELECT * FROM company_concierge_services WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>