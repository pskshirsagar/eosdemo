<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncements
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAnnouncements extends CEosPluralBase {

	/**
	 * @return CAnnouncement[]
	 */
	public static function fetchAnnouncements( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAnnouncement::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAnnouncement
	 */
	public static function fetchAnnouncement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAnnouncement::class, $objDatabase );
	}

	public static function fetchAnnouncementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'announcements', $objDatabase );
	}

	public static function fetchAnnouncementByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAnnouncement( sprintf( 'SELECT * FROM announcements WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementsByCid( $intCid, $objDatabase ) {
		return self::fetchAnnouncements( sprintf( 'SELECT * FROM announcements WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementsByAnnouncementTypeIdByCid( $intAnnouncementTypeId, $intCid, $objDatabase ) {
		return self::fetchAnnouncements( sprintf( 'SELECT * FROM announcements WHERE announcement_type_id = %d AND cid = %d', $intAnnouncementTypeId, $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchAnnouncements( sprintf( 'SELECT * FROM announcements WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchAnnouncementsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchAnnouncements( sprintf( 'SELECT * FROM announcements WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

}
?>