<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserDetails extends CEosPluralBase {

	/**
	 * @return CCompanyUserDetail[]
	 */
	public static function fetchCompanyUserDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserDetail
	 */
	public static function fetchCompanyUserDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserDetail', $objDatabase );
	}

	public static function fetchCompanyUserDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_details', $objDatabase );
	}

	public static function fetchCompanyUserDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDetail( sprintf( 'SELECT * FROM company_user_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserDetails( sprintf( 'SELECT * FROM company_user_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDetailsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDetails( sprintf( 'SELECT * FROM company_user_details WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDetailsByVacancyStatsEmailFrequencyIdByCid( $intVacancyStatsEmailFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDetails( sprintf( 'SELECT * FROM company_user_details WHERE vacancy_stats_email_frequency_id = %d AND cid = %d', ( int ) $intVacancyStatsEmailFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>