<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyMessageDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_message_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyMessageSubTypeId;
	protected $m_intReferenceId;
	protected $m_intSubmissionSubsidyMessageId;
	protected $m_intResultSubsidyMessageId;
	protected $m_strResultMessage;
	protected $m_intNumErrors;
	protected $m_strFieldErrors;
	protected $m_jsonFieldErrors;
	protected $m_arrintSubmissionRecordNumbers;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intTransactionTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_message_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyMessageSubTypeId', trim( $arrValues['subsidy_message_sub_type_id'] ) ); elseif( isset( $arrValues['subsidy_message_sub_type_id'] ) ) $this->setSubsidyMessageSubTypeId( $arrValues['subsidy_message_sub_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['submission_subsidy_message_id'] ) && $boolDirectSet ) $this->set( 'm_intSubmissionSubsidyMessageId', trim( $arrValues['submission_subsidy_message_id'] ) ); elseif( isset( $arrValues['submission_subsidy_message_id'] ) ) $this->setSubmissionSubsidyMessageId( $arrValues['submission_subsidy_message_id'] );
		if( isset( $arrValues['result_subsidy_message_id'] ) && $boolDirectSet ) $this->set( 'm_intResultSubsidyMessageId', trim( $arrValues['result_subsidy_message_id'] ) ); elseif( isset( $arrValues['result_subsidy_message_id'] ) ) $this->setResultSubsidyMessageId( $arrValues['result_subsidy_message_id'] );
		if( isset( $arrValues['result_message'] ) && $boolDirectSet ) $this->set( 'm_strResultMessage', trim( stripcslashes( $arrValues['result_message'] ) ) ); elseif( isset( $arrValues['result_message'] ) ) $this->setResultMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['result_message'] ) : $arrValues['result_message'] );
		if( isset( $arrValues['num_errors'] ) && $boolDirectSet ) $this->set( 'm_intNumErrors', trim( $arrValues['num_errors'] ) ); elseif( isset( $arrValues['num_errors'] ) ) $this->setNumErrors( $arrValues['num_errors'] );
		if( isset( $arrValues['field_errors'] ) ) $this->set( 'm_strFieldErrors', trim( stripcslashes( $arrValues['field_errors'] ) ) );
		if( isset( $arrValues['submission_record_numbers'] ) && $boolDirectSet ) $this->set( 'm_arrintSubmissionRecordNumbers', trim( $arrValues['submission_record_numbers'] ) ); elseif( isset( $arrValues['submission_record_numbers'] ) ) $this->setSubmissionRecordNumbers( $arrValues['submission_record_numbers'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyMessageSubTypeId( $intSubsidyMessageSubTypeId ) {
		$this->set( 'm_intSubsidyMessageSubTypeId', CStrings::strToIntDef( $intSubsidyMessageSubTypeId, NULL, false ) );
	}

	public function getSubsidyMessageSubTypeId() {
		return $this->m_intSubsidyMessageSubTypeId;
	}

	public function sqlSubsidyMessageSubTypeId() {
		return ( true == isset( $this->m_intSubsidyMessageSubTypeId ) ) ? ( string ) $this->m_intSubsidyMessageSubTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setSubmissionSubsidyMessageId( $intSubmissionSubsidyMessageId ) {
		$this->set( 'm_intSubmissionSubsidyMessageId', CStrings::strToIntDef( $intSubmissionSubsidyMessageId, NULL, false ) );
	}

	public function getSubmissionSubsidyMessageId() {
		return $this->m_intSubmissionSubsidyMessageId;
	}

	public function sqlSubmissionSubsidyMessageId() {
		return ( true == isset( $this->m_intSubmissionSubsidyMessageId ) ) ? ( string ) $this->m_intSubmissionSubsidyMessageId : 'NULL';
	}

	public function setResultSubsidyMessageId( $intResultSubsidyMessageId ) {
		$this->set( 'm_intResultSubsidyMessageId', CStrings::strToIntDef( $intResultSubsidyMessageId, NULL, false ) );
	}

	public function getResultSubsidyMessageId() {
		return $this->m_intResultSubsidyMessageId;
	}

	public function sqlResultSubsidyMessageId() {
		return ( true == isset( $this->m_intResultSubsidyMessageId ) ) ? ( string ) $this->m_intResultSubsidyMessageId : 'NULL';
	}

	public function setResultMessage( $strResultMessage ) {
		$this->set( 'm_strResultMessage', CStrings::strTrimDef( $strResultMessage, -1, NULL, true ) );
	}

	public function getResultMessage() {
		return $this->m_strResultMessage;
	}

	public function sqlResultMessage() {
		return ( true == isset( $this->m_strResultMessage ) ) ? '\'' . addslashes( $this->m_strResultMessage ) . '\'' : 'NULL';
	}

	public function setNumErrors( $intNumErrors ) {
		$this->set( 'm_intNumErrors', CStrings::strToIntDef( $intNumErrors, NULL, false ) );
	}

	public function getNumErrors() {
		return $this->m_intNumErrors;
	}

	public function sqlNumErrors() {
		return ( true == isset( $this->m_intNumErrors ) ) ? ( string ) $this->m_intNumErrors : 'NULL';
	}

	public function setFieldErrors( $strFieldErrors ) {
		$this->set( 'm_strFieldErrors', CStrings::strTrimDef( $strFieldErrors, -1, NULL, true ) );
	}

	public function getFieldErrors() {
		return $this->m_strFieldErrors;
	}

	public function sqlFieldErrors() {
		return ( true == isset( $this->m_strFieldErrors ) ) ? '\'' . addslashes( $this->m_strFieldErrors ) . '\'' : 'NULL';
	}

	public function setSubmissionRecordNumbers( $arrintSubmissionRecordNumbers ) {
		$this->set( 'm_arrintSubmissionRecordNumbers', CStrings::strToArrIntDef( $arrintSubmissionRecordNumbers, NULL ) );
	}

	public function getSubmissionRecordNumbers() {
		return $this->m_arrintSubmissionRecordNumbers;
	}

	public function sqlSubmissionRecordNumbers() {
		return ( true == isset( $this->m_arrintSubmissionRecordNumbers ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintSubmissionRecordNumbers, NULL ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getTransactionTypeId() {
		return $this->m_intTransactionTypeId;
	}

	public function setTransactionTypeId( $intTransactionTypeId ) {
		$this->m_intTransactionTypeId = $intTransactionTypeId;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_message_sub_type_id, reference_id, submission_subsidy_message_id, result_subsidy_message_id, result_message, num_errors, field_errors, submission_record_numbers, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidyMessageSubTypeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlSubmissionSubsidyMessageId() . ', ' .
 						$this->sqlResultSubsidyMessageId() . ', ' .
 						$this->sqlResultMessage() . ', ' .
 						$this->sqlNumErrors() . ', ' .
 						$this->sqlFieldErrors() . ', ' .
 						$this->sqlSubmissionRecordNumbers() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_message_sub_type_id = ' . $this->sqlSubsidyMessageSubTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyMessageSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_message_sub_type_id = ' . $this->sqlSubsidyMessageSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submission_subsidy_message_id = ' . $this->sqlSubmissionSubsidyMessageId() . ','; } elseif( true == array_key_exists( 'SubmissionSubsidyMessageId', $this->getChangedColumns() ) ) { $strSql .= ' submission_subsidy_message_id = ' . $this->sqlSubmissionSubsidyMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_subsidy_message_id = ' . $this->sqlResultSubsidyMessageId() . ','; } elseif( true == array_key_exists( 'ResultSubsidyMessageId', $this->getChangedColumns() ) ) { $strSql .= ' result_subsidy_message_id = ' . $this->sqlResultSubsidyMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_message = ' . $this->sqlResultMessage() . ','; } elseif( true == array_key_exists( 'ResultMessage', $this->getChangedColumns() ) ) { $strSql .= ' result_message = ' . $this->sqlResultMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_errors = ' . $this->sqlNumErrors() . ','; } elseif( true == array_key_exists( 'NumErrors', $this->getChangedColumns() ) ) { $strSql .= ' num_errors = ' . $this->sqlNumErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field_errors = ' . $this->sqlFieldErrors() . ','; } elseif( true == array_key_exists( 'FieldErrors', $this->getChangedColumns() ) ) { $strSql .= ' field_errors = ' . $this->sqlFieldErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submission_record_numbers = ' . $this->sqlSubmissionRecordNumbers() . ','; } elseif( true == array_key_exists( 'SubmissionRecordNumbers', $this->getChangedColumns() ) ) { $strSql .= ' submission_record_numbers = ' . $this->sqlSubmissionRecordNumbers() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_message_sub_type_id' => $this->getSubsidyMessageSubTypeId(),
			'reference_id' => $this->getReferenceId(),
			'submission_subsidy_message_id' => $this->getSubmissionSubsidyMessageId(),
			'result_subsidy_message_id' => $this->getResultSubsidyMessageId(),
			'result_message' => $this->getResultMessage(),
			'num_errors' => $this->getNumErrors(),
			'field_errors' => $this->getFieldErrors(),
			'submission_record_numbers' => $this->getSubmissionRecordNumbers(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>