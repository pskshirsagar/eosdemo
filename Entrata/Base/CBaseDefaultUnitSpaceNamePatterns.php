<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultUnitSpaceNamePatterns
 * Do not add any new functions to this class.
 */

class CBaseDefaultUnitSpaceNamePatterns extends CEosPluralBase {

	/**
	 * @return CDefaultUnitSpaceNamePattern[]
	 */
	public static function fetchDefaultUnitSpaceNamePatterns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultUnitSpaceNamePattern::class, $objDatabase );
	}

	/**
	 * @return CDefaultUnitSpaceNamePattern
	 */
	public static function fetchDefaultUnitSpaceNamePattern( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultUnitSpaceNamePattern::class, $objDatabase );
	}

	public static function fetchDefaultUnitSpaceNamePatternCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_unit_space_name_patterns', $objDatabase );
	}

	public static function fetchDefaultUnitSpaceNamePatternById( $intId, $objDatabase ) {
		return self::fetchDefaultUnitSpaceNamePattern( sprintf( 'SELECT * FROM default_unit_space_name_patterns WHERE id = %d', $intId ), $objDatabase );
	}

}
?>