<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployee extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.company_employees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intEmployeeStatusTypeId;
	protected $m_intCompanyManagerId;
	protected $m_intApPayeeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strNameMaiden;
	protected $m_strTitle;
	protected $m_intRoleReferenceId;
	protected $m_intIsEmployee;
	protected $m_strBirthDate;
	protected $m_strTaxNumberEncrypted;
	protected $m_strEmailAddress;
	protected $m_strSecondaryEmailAddress;
	protected $m_intPhoneNumber1TypeId;
	protected $m_strPhoneNumber1;
	protected $m_strPhoneNumber1Extension;
	protected $m_intPhoneNumber1IsPublished;
	protected $m_intPhoneNumber2TypeId;
	protected $m_strPhoneNumber2;
	protected $m_strPhoneNumber2Extension;
	protected $m_intPhoneNumber2IsPublished;
	protected $m_strDateStarted;
	protected $m_strDateTerminated;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strEmployeeCode;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strPreferredLocaleCode;

	public function __construct() {
		parent::__construct();

		$this->m_intIsEmployee = '1';
		$this->m_intPhoneNumber1IsPublished = '1';
		$this->m_intPhoneNumber2IsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['employee_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeStatusTypeId', trim( $arrValues['employee_status_type_id'] ) ); elseif( isset( $arrValues['employee_status_type_id'] ) ) $this->setEmployeeStatusTypeId( $arrValues['employee_status_type_id'] );
		if( isset( $arrValues['company_manager_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyManagerId', trim( $arrValues['company_manager_id'] ) ); elseif( isset( $arrValues['company_manager_id'] ) ) $this->setCompanyManagerId( $arrValues['company_manager_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['role_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intRoleReferenceId', trim( $arrValues['role_reference_id'] ) ); elseif( isset( $arrValues['role_reference_id'] ) ) $this->setRoleReferenceId( $arrValues['role_reference_id'] );
		if( isset( $arrValues['is_employee'] ) && $boolDirectSet ) $this->set( 'm_intIsEmployee', trim( $arrValues['is_employee'] ) ); elseif( isset( $arrValues['is_employee'] ) ) $this->setIsEmployee( $arrValues['is_employee'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['secondary_email_address'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryEmailAddress', trim( $arrValues['secondary_email_address'] ) ); elseif( isset( $arrValues['secondary_email_address'] ) ) $this->setSecondaryEmailAddress( $arrValues['secondary_email_address'] );
		if( isset( $arrValues['phone_number1_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber1TypeId', trim( $arrValues['phone_number1_type_id'] ) ); elseif( isset( $arrValues['phone_number1_type_id'] ) ) $this->setPhoneNumber1TypeId( $arrValues['phone_number1_type_id'] );
		if( isset( $arrValues['phone_number1'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber1', trim( $arrValues['phone_number1'] ) ); elseif( isset( $arrValues['phone_number1'] ) ) $this->setPhoneNumber1( $arrValues['phone_number1'] );
		if( isset( $arrValues['phone_number1_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber1Extension', trim( $arrValues['phone_number1_extension'] ) ); elseif( isset( $arrValues['phone_number1_extension'] ) ) $this->setPhoneNumber1Extension( $arrValues['phone_number1_extension'] );
		if( isset( $arrValues['phone_number1_is_published'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber1IsPublished', trim( $arrValues['phone_number1_is_published'] ) ); elseif( isset( $arrValues['phone_number1_is_published'] ) ) $this->setPhoneNumber1IsPublished( $arrValues['phone_number1_is_published'] );
		if( isset( $arrValues['phone_number2_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber2TypeId', trim( $arrValues['phone_number2_type_id'] ) ); elseif( isset( $arrValues['phone_number2_type_id'] ) ) $this->setPhoneNumber2TypeId( $arrValues['phone_number2_type_id'] );
		if( isset( $arrValues['phone_number2'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber2', trim( $arrValues['phone_number2'] ) ); elseif( isset( $arrValues['phone_number2'] ) ) $this->setPhoneNumber2( $arrValues['phone_number2'] );
		if( isset( $arrValues['phone_number2_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber2Extension', trim( $arrValues['phone_number2_extension'] ) ); elseif( isset( $arrValues['phone_number2_extension'] ) ) $this->setPhoneNumber2Extension( $arrValues['phone_number2_extension'] );
		if( isset( $arrValues['phone_number2_is_published'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber2IsPublished', trim( $arrValues['phone_number2_is_published'] ) ); elseif( isset( $arrValues['phone_number2_is_published'] ) ) $this->setPhoneNumber2IsPublished( $arrValues['phone_number2_is_published'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['employee_code'] ) && $boolDirectSet ) $this->set( 'm_strEmployeeCode', trim( $arrValues['employee_code'] ) ); elseif( isset( $arrValues['employee_code'] ) ) $this->setEmployeeCode( $arrValues['employee_code'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['preferred_locale_code'] ) && $boolDirectSet ) $this->set( 'm_strPreferredLocaleCode', trim( $arrValues['preferred_locale_code'] ) ); elseif( isset( $arrValues['preferred_locale_code'] ) ) $this->setPreferredLocaleCode( $arrValues['preferred_locale_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->set( 'm_intEmployeeStatusTypeId', CStrings::strToIntDef( $intEmployeeStatusTypeId, NULL, false ) );
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function sqlEmployeeStatusTypeId() {
		return ( true == isset( $this->m_intEmployeeStatusTypeId ) ) ? ( string ) $this->m_intEmployeeStatusTypeId : 'NULL';
	}

	public function setCompanyManagerId( $intCompanyManagerId ) {
		$this->set( 'm_intCompanyManagerId', CStrings::strToIntDef( $intCompanyManagerId, NULL, false ) );
	}

	public function getCompanyManagerId() {
		return $this->m_intCompanyManagerId;
	}

	public function sqlCompanyManagerId() {
		return ( true == isset( $this->m_intCompanyManagerId ) ) ? ( string ) $this->m_intCompanyManagerId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 50, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setRoleReferenceId( $intRoleReferenceId ) {
		$this->set( 'm_intRoleReferenceId', CStrings::strToIntDef( $intRoleReferenceId, NULL, false ) );
	}

	public function getRoleReferenceId() {
		return $this->m_intRoleReferenceId;
	}

	public function sqlRoleReferenceId() {
		return ( true == isset( $this->m_intRoleReferenceId ) ) ? ( string ) $this->m_intRoleReferenceId : 'NULL';
	}

	public function setIsEmployee( $intIsEmployee ) {
		$this->set( 'm_intIsEmployee', CStrings::strToIntDef( $intIsEmployee, NULL, false ) );
	}

	public function getIsEmployee() {
		return $this->m_intIsEmployee;
	}

	public function sqlIsEmployee() {
		return ( true == isset( $this->m_intIsEmployee ) ) ? ( string ) $this->m_intIsEmployee : '1';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		if( true == $this->m_boolInitialized ) { 
			$strTaxNumberEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER ); 
		 } 
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function getDecryptedTaxNumberEncrypted() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		 }
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTaxNumberEncrypted(),CONFIG_SODIUM_KEY_TAX_NUMBER, ['legacy_secret_key' => CONFIG_KEY_TAX_NUMBER,'is_base64_encoded '=> true, 'should_rtrim_nulls' => true ]	);
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setSecondaryEmailAddress( $strSecondaryEmailAddress ) {
		$this->set( 'm_strSecondaryEmailAddress', CStrings::strTrimDef( $strSecondaryEmailAddress, 240, NULL, true ) );
	}

	public function getSecondaryEmailAddress() {
		return $this->m_strSecondaryEmailAddress;
	}

	public function sqlSecondaryEmailAddress() {
		return ( true == isset( $this->m_strSecondaryEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryEmailAddress ) : '\'' . addslashes( $this->m_strSecondaryEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber1TypeId( $intPhoneNumber1TypeId ) {
		$this->set( 'm_intPhoneNumber1TypeId', CStrings::strToIntDef( $intPhoneNumber1TypeId, NULL, false ) );
	}

	public function getPhoneNumber1TypeId() {
		return $this->m_intPhoneNumber1TypeId;
	}

	public function sqlPhoneNumber1TypeId() {
		return ( true == isset( $this->m_intPhoneNumber1TypeId ) ) ? ( string ) $this->m_intPhoneNumber1TypeId : 'NULL';
	}

	public function setPhoneNumber1( $strPhoneNumber1 ) {
		$this->set( 'm_strPhoneNumber1', CStrings::strTrimDef( $strPhoneNumber1, 30, NULL, true ) );
	}

	public function getPhoneNumber1() {
		return $this->m_strPhoneNumber1;
	}

	public function sqlPhoneNumber1() {
		return ( true == isset( $this->m_strPhoneNumber1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber1 ) : '\'' . addslashes( $this->m_strPhoneNumber1 ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber1Extension( $strPhoneNumber1Extension ) {
		$this->set( 'm_strPhoneNumber1Extension', CStrings::strTrimDef( $strPhoneNumber1Extension, 20, NULL, true ) );
	}

	public function getPhoneNumber1Extension() {
		return $this->m_strPhoneNumber1Extension;
	}

	public function sqlPhoneNumber1Extension() {
		return ( true == isset( $this->m_strPhoneNumber1Extension ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber1Extension ) : '\'' . addslashes( $this->m_strPhoneNumber1Extension ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber1IsPublished( $intPhoneNumber1IsPublished ) {
		$this->set( 'm_intPhoneNumber1IsPublished', CStrings::strToIntDef( $intPhoneNumber1IsPublished, NULL, false ) );
	}

	public function getPhoneNumber1IsPublished() {
		return $this->m_intPhoneNumber1IsPublished;
	}

	public function sqlPhoneNumber1IsPublished() {
		return ( true == isset( $this->m_intPhoneNumber1IsPublished ) ) ? ( string ) $this->m_intPhoneNumber1IsPublished : '1';
	}

	public function setPhoneNumber2TypeId( $intPhoneNumber2TypeId ) {
		$this->set( 'm_intPhoneNumber2TypeId', CStrings::strToIntDef( $intPhoneNumber2TypeId, NULL, false ) );
	}

	public function getPhoneNumber2TypeId() {
		return $this->m_intPhoneNumber2TypeId;
	}

	public function sqlPhoneNumber2TypeId() {
		return ( true == isset( $this->m_intPhoneNumber2TypeId ) ) ? ( string ) $this->m_intPhoneNumber2TypeId : 'NULL';
	}

	public function setPhoneNumber2( $strPhoneNumber2 ) {
		$this->set( 'm_strPhoneNumber2', CStrings::strTrimDef( $strPhoneNumber2, 30, NULL, true ) );
	}

	public function getPhoneNumber2() {
		return $this->m_strPhoneNumber2;
	}

	public function sqlPhoneNumber2() {
		return ( true == isset( $this->m_strPhoneNumber2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber2 ) : '\'' . addslashes( $this->m_strPhoneNumber2 ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber2Extension( $strPhoneNumber2Extension ) {
		$this->set( 'm_strPhoneNumber2Extension', CStrings::strTrimDef( $strPhoneNumber2Extension, 20, NULL, true ) );
	}

	public function getPhoneNumber2Extension() {
		return $this->m_strPhoneNumber2Extension;
	}

	public function sqlPhoneNumber2Extension() {
		return ( true == isset( $this->m_strPhoneNumber2Extension ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber2Extension ) : '\'' . addslashes( $this->m_strPhoneNumber2Extension ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber2IsPublished( $intPhoneNumber2IsPublished ) {
		$this->set( 'm_intPhoneNumber2IsPublished', CStrings::strToIntDef( $intPhoneNumber2IsPublished, NULL, false ) );
	}

	public function getPhoneNumber2IsPublished() {
		return $this->m_intPhoneNumber2IsPublished;
	}

	public function sqlPhoneNumber2IsPublished() {
		return ( true == isset( $this->m_intPhoneNumber2IsPublished ) ) ? ( string ) $this->m_intPhoneNumber2IsPublished : '1';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEmployeeCode( $strEmployeeCode ) {
		$this->set( 'm_strEmployeeCode', CStrings::strTrimDef( $strEmployeeCode, 30, NULL, true ) );
	}

	public function getEmployeeCode() {
		return $this->m_strEmployeeCode;
	}

	public function sqlEmployeeCode() {
		return ( true == isset( $this->m_strEmployeeCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmployeeCode ) : '\'' . addslashes( $this->m_strEmployeeCode ) . '\'' ) : 'NULL';
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->set( 'm_strPreferredLocaleCode', CStrings::strTrimDef( $strPreferredLocaleCode, 30, NULL, true ) );
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function sqlPreferredLocaleCode() {
		return ( true == isset( $this->m_strPreferredLocaleCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredLocaleCode ) : '\'' . addslashes( $this->m_strPreferredLocaleCode ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, employee_status_type_id, company_manager_id, ap_payee_id, remote_primary_key, name_prefix, name_first, name_middle, name_last, name_suffix, name_maiden, title, role_reference_id, is_employee, birth_date, tax_number_encrypted, email_address, secondary_email_address, phone_number1_type_id, phone_number1, phone_number1_extension, phone_number1_is_published, phone_number2_type_id, phone_number2, phone_number2_extension, phone_number2_is_published, date_started, date_terminated, updated_by, updated_on, created_by, created_on, employee_code, details, preferred_locale_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlEmployeeStatusTypeId() . ', ' .
						$this->sqlCompanyManagerId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlRoleReferenceId() . ', ' .
						$this->sqlIsEmployee() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlSecondaryEmailAddress() . ', ' .
						$this->sqlPhoneNumber1TypeId() . ', ' .
						$this->sqlPhoneNumber1() . ', ' .
						$this->sqlPhoneNumber1Extension() . ', ' .
						$this->sqlPhoneNumber1IsPublished() . ', ' .
						$this->sqlPhoneNumber2TypeId() . ', ' .
						$this->sqlPhoneNumber2() . ', ' .
						$this->sqlPhoneNumber2Extension() . ', ' .
						$this->sqlPhoneNumber2IsPublished() . ', ' .
						$this->sqlDateStarted() . ', ' .
						$this->sqlDateTerminated() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlEmployeeCode() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlPreferredLocaleCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_manager_id = ' . $this->sqlCompanyManagerId(). ',' ; } elseif( true == array_key_exists( 'CompanyManagerId', $this->getChangedColumns() ) ) { $strSql .= ' company_manager_id = ' . $this->sqlCompanyManagerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden(). ',' ; } elseif( true == array_key_exists( 'NameMaiden', $this->getChangedColumns() ) ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' role_reference_id = ' . $this->sqlRoleReferenceId(). ',' ; } elseif( true == array_key_exists( 'RoleReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' role_reference_id = ' . $this->sqlRoleReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_employee = ' . $this->sqlIsEmployee(). ',' ; } elseif( true == array_key_exists( 'IsEmployee', $this->getChangedColumns() ) ) { $strSql .= ' is_employee = ' . $this->sqlIsEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_email_address = ' . $this->sqlSecondaryEmailAddress(). ',' ; } elseif( true == array_key_exists( 'SecondaryEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' secondary_email_address = ' . $this->sqlSecondaryEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1_type_id = ' . $this->sqlPhoneNumber1TypeId(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber1TypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1_type_id = ' . $this->sqlPhoneNumber1TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1 = ' . $this->sqlPhoneNumber1(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber1', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1 = ' . $this->sqlPhoneNumber1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1_extension = ' . $this->sqlPhoneNumber1Extension(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber1Extension', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1_extension = ' . $this->sqlPhoneNumber1Extension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1_is_published = ' . $this->sqlPhoneNumber1IsPublished(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber1IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1_is_published = ' . $this->sqlPhoneNumber1IsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2_type_id = ' . $this->sqlPhoneNumber2TypeId(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber2TypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2_type_id = ' . $this->sqlPhoneNumber2TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2 = ' . $this->sqlPhoneNumber2(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber2', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2 = ' . $this->sqlPhoneNumber2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2_extension = ' . $this->sqlPhoneNumber2Extension(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber2Extension', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2_extension = ' . $this->sqlPhoneNumber2Extension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2_is_published = ' . $this->sqlPhoneNumber2IsPublished(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber2IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2_is_published = ' . $this->sqlPhoneNumber2IsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted(). ',' ; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated(). ',' ; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_code = ' . $this->sqlEmployeeCode(). ',' ; } elseif( true == array_key_exists( 'EmployeeCode', $this->getChangedColumns() ) ) { $strSql .= ' employee_code = ' . $this->sqlEmployeeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_locale_code = ' . $this->sqlPreferredLocaleCode(). ',' ; } elseif( true == array_key_exists( 'PreferredLocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' preferred_locale_code = ' . $this->sqlPreferredLocaleCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'employee_status_type_id' => $this->getEmployeeStatusTypeId(),
			'company_manager_id' => $this->getCompanyManagerId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'name_maiden' => $this->getNameMaiden(),
			'title' => $this->getTitle(),
			'role_reference_id' => $this->getRoleReferenceId(),
			'is_employee' => $this->getIsEmployee(),
			'birth_date' => $this->getBirthDate(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'email_address' => $this->getEmailAddress(),
			'secondary_email_address' => $this->getSecondaryEmailAddress(),
			'phone_number1_type_id' => $this->getPhoneNumber1TypeId(),
			'phone_number1' => $this->getPhoneNumber1(),
			'phone_number1_extension' => $this->getPhoneNumber1Extension(),
			'phone_number1_is_published' => $this->getPhoneNumber1IsPublished(),
			'phone_number2_type_id' => $this->getPhoneNumber2TypeId(),
			'phone_number2' => $this->getPhoneNumber2(),
			'phone_number2_extension' => $this->getPhoneNumber2Extension(),
			'phone_number2_is_published' => $this->getPhoneNumber2IsPublished(),
			'date_started' => $this->getDateStarted(),
			'date_terminated' => $this->getDateTerminated(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'employee_code' => $this->getEmployeeCode(),
			'details' => $this->getDetails(),
			'preferred_locale_code' => $this->getPreferredLocaleCode()
		);
	}

}
?>