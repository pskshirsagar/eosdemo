<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataChange extends CEosSingularBase {

	const TABLE_NAME = 'public.data_changes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDataChangeTypeId;
	protected $m_intOldReferenceId;
	protected $m_intNewReferenceId;
	protected $m_strOldReferenceName;
	protected $m_strNewReferenceName;
	protected $m_intNumberOfTransactionsAffected;
	protected $m_strChangeDatetime;
	protected $m_strProcessedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intNumberOfTransactionsAffected = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['data_change_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataChangeTypeId', trim( $arrValues['data_change_type_id'] ) ); elseif( isset( $arrValues['data_change_type_id'] ) ) $this->setDataChangeTypeId( $arrValues['data_change_type_id'] );
		if( isset( $arrValues['old_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intOldReferenceId', trim( $arrValues['old_reference_id'] ) ); elseif( isset( $arrValues['old_reference_id'] ) ) $this->setOldReferenceId( $arrValues['old_reference_id'] );
		if( isset( $arrValues['new_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intNewReferenceId', trim( $arrValues['new_reference_id'] ) ); elseif( isset( $arrValues['new_reference_id'] ) ) $this->setNewReferenceId( $arrValues['new_reference_id'] );
		if( isset( $arrValues['old_reference_name'] ) && $boolDirectSet ) $this->set( 'm_strOldReferenceName', trim( stripcslashes( $arrValues['old_reference_name'] ) ) ); elseif( isset( $arrValues['old_reference_name'] ) ) $this->setOldReferenceName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_reference_name'] ) : $arrValues['old_reference_name'] );
		if( isset( $arrValues['new_reference_name'] ) && $boolDirectSet ) $this->set( 'm_strNewReferenceName', trim( stripcslashes( $arrValues['new_reference_name'] ) ) ); elseif( isset( $arrValues['new_reference_name'] ) ) $this->setNewReferenceName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_reference_name'] ) : $arrValues['new_reference_name'] );
		if( isset( $arrValues['number_of_transactions_affected'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfTransactionsAffected', trim( $arrValues['number_of_transactions_affected'] ) ); elseif( isset( $arrValues['number_of_transactions_affected'] ) ) $this->setNumberOfTransactionsAffected( $arrValues['number_of_transactions_affected'] );
		if( isset( $arrValues['change_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChangeDatetime', trim( $arrValues['change_datetime'] ) ); elseif( isset( $arrValues['change_datetime'] ) ) $this->setChangeDatetime( $arrValues['change_datetime'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDataChangeTypeId( $intDataChangeTypeId ) {
		$this->set( 'm_intDataChangeTypeId', CStrings::strToIntDef( $intDataChangeTypeId, NULL, false ) );
	}

	public function getDataChangeTypeId() {
		return $this->m_intDataChangeTypeId;
	}

	public function sqlDataChangeTypeId() {
		return ( true == isset( $this->m_intDataChangeTypeId ) ) ? ( string ) $this->m_intDataChangeTypeId : 'NULL';
	}

	public function setOldReferenceId( $intOldReferenceId ) {
		$this->set( 'm_intOldReferenceId', CStrings::strToIntDef( $intOldReferenceId, NULL, false ) );
	}

	public function getOldReferenceId() {
		return $this->m_intOldReferenceId;
	}

	public function sqlOldReferenceId() {
		return ( true == isset( $this->m_intOldReferenceId ) ) ? ( string ) $this->m_intOldReferenceId : 'NULL';
	}

	public function setNewReferenceId( $intNewReferenceId ) {
		$this->set( 'm_intNewReferenceId', CStrings::strToIntDef( $intNewReferenceId, NULL, false ) );
	}

	public function getNewReferenceId() {
		return $this->m_intNewReferenceId;
	}

	public function sqlNewReferenceId() {
		return ( true == isset( $this->m_intNewReferenceId ) ) ? ( string ) $this->m_intNewReferenceId : 'NULL';
	}

	public function setOldReferenceName( $strOldReferenceName ) {
		$this->set( 'm_strOldReferenceName', CStrings::strTrimDef( $strOldReferenceName, 100, NULL, true ) );
	}

	public function getOldReferenceName() {
		return $this->m_strOldReferenceName;
	}

	public function sqlOldReferenceName() {
		return ( true == isset( $this->m_strOldReferenceName ) ) ? '\'' . addslashes( $this->m_strOldReferenceName ) . '\'' : 'NULL';
	}

	public function setNewReferenceName( $strNewReferenceName ) {
		$this->set( 'm_strNewReferenceName', CStrings::strTrimDef( $strNewReferenceName, 100, NULL, true ) );
	}

	public function getNewReferenceName() {
		return $this->m_strNewReferenceName;
	}

	public function sqlNewReferenceName() {
		return ( true == isset( $this->m_strNewReferenceName ) ) ? '\'' . addslashes( $this->m_strNewReferenceName ) . '\'' : 'NULL';
	}

	public function setNumberOfTransactionsAffected( $intNumberOfTransactionsAffected ) {
		$this->set( 'm_intNumberOfTransactionsAffected', CStrings::strToIntDef( $intNumberOfTransactionsAffected, NULL, false ) );
	}

	public function getNumberOfTransactionsAffected() {
		return $this->m_intNumberOfTransactionsAffected;
	}

	public function sqlNumberOfTransactionsAffected() {
		return ( true == isset( $this->m_intNumberOfTransactionsAffected ) ) ? ( string ) $this->m_intNumberOfTransactionsAffected : '0';
	}

	public function setChangeDatetime( $strChangeDatetime ) {
		$this->set( 'm_strChangeDatetime', CStrings::strTrimDef( $strChangeDatetime, -1, NULL, true ) );
	}

	public function getChangeDatetime() {
		return $this->m_strChangeDatetime;
	}

	public function sqlChangeDatetime() {
		return ( true == isset( $this->m_strChangeDatetime ) ) ? '\'' . $this->m_strChangeDatetime . '\'' : 'NOW()';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, data_change_type_id, old_reference_id, new_reference_id, old_reference_name, new_reference_name, number_of_transactions_affected, change_datetime, processed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDataChangeTypeId() . ', ' .
 						$this->sqlOldReferenceId() . ', ' .
 						$this->sqlNewReferenceId() . ', ' .
 						$this->sqlOldReferenceName() . ', ' .
 						$this->sqlNewReferenceName() . ', ' .
 						$this->sqlNumberOfTransactionsAffected() . ', ' .
 						$this->sqlChangeDatetime() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_change_type_id = ' . $this->sqlDataChangeTypeId() . ','; } elseif( true == array_key_exists( 'DataChangeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_change_type_id = ' . $this->sqlDataChangeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_reference_id = ' . $this->sqlOldReferenceId() . ','; } elseif( true == array_key_exists( 'OldReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' old_reference_id = ' . $this->sqlOldReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_reference_id = ' . $this->sqlNewReferenceId() . ','; } elseif( true == array_key_exists( 'NewReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' new_reference_id = ' . $this->sqlNewReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_reference_name = ' . $this->sqlOldReferenceName() . ','; } elseif( true == array_key_exists( 'OldReferenceName', $this->getChangedColumns() ) ) { $strSql .= ' old_reference_name = ' . $this->sqlOldReferenceName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_reference_name = ' . $this->sqlNewReferenceName() . ','; } elseif( true == array_key_exists( 'NewReferenceName', $this->getChangedColumns() ) ) { $strSql .= ' new_reference_name = ' . $this->sqlNewReferenceName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_transactions_affected = ' . $this->sqlNumberOfTransactionsAffected() . ','; } elseif( true == array_key_exists( 'NumberOfTransactionsAffected', $this->getChangedColumns() ) ) { $strSql .= ' number_of_transactions_affected = ' . $this->sqlNumberOfTransactionsAffected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_datetime = ' . $this->sqlChangeDatetime() . ','; } elseif( true == array_key_exists( 'ChangeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' change_datetime = ' . $this->sqlChangeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'data_change_type_id' => $this->getDataChangeTypeId(),
			'old_reference_id' => $this->getOldReferenceId(),
			'new_reference_id' => $this->getNewReferenceId(),
			'old_reference_name' => $this->getOldReferenceName(),
			'new_reference_name' => $this->getNewReferenceName(),
			'number_of_transactions_affected' => $this->getNumberOfTransactionsAffected(),
			'change_datetime' => $this->getChangeDatetime(),
			'processed_on' => $this->getProcessedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>