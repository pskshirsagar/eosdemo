<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyIncomeExceptionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyIncomeExceptionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyIncomeExceptionType[]
	 */
	public static function fetchSubsidyIncomeExceptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyIncomeExceptionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyIncomeExceptionType
	 */
	public static function fetchSubsidyIncomeExceptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyIncomeExceptionType::class, $objDatabase );
	}

	public static function fetchSubsidyIncomeExceptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_income_exception_types', $objDatabase );
	}

	public static function fetchSubsidyIncomeExceptionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyIncomeExceptionType( sprintf( 'SELECT * FROM subsidy_income_exception_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>