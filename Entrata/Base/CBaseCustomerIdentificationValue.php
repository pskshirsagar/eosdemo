<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerIdentificationValue extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.customer_identification_values';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCustomerId;
	protected $m_intCompanyIdentificationTypeId;
	protected $m_strEncryptedValue;
	protected $m_strMaskedValue;
	protected $m_intFileId;
	protected $m_strIdExpirationDate;
	protected $m_strIdIssueDate;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_identification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyIdentificationTypeId', trim( $arrValues['company_identification_type_id'] ) ); elseif( isset( $arrValues['company_identification_type_id'] ) ) $this->setCompanyIdentificationTypeId( $arrValues['company_identification_type_id'] );
		if( isset( $arrValues['encrypted_value'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedValue', trim( $arrValues['encrypted_value'] ) ); elseif( isset( $arrValues['encrypted_value'] ) ) $this->setEncryptedValue( $arrValues['encrypted_value'] );
		if( isset( $arrValues['masked_value'] ) && $boolDirectSet ) $this->set( 'm_strMaskedValue', trim( $arrValues['masked_value'] ) ); elseif( isset( $arrValues['masked_value'] ) ) $this->setMaskedValue( $arrValues['masked_value'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['id_expiration_date'] ) && $boolDirectSet ) $this->set( 'm_strIdExpirationDate', trim( $arrValues['id_expiration_date'] ) ); elseif( isset( $arrValues['id_expiration_date'] ) ) $this->setIdExpirationDate( $arrValues['id_expiration_date'] );
		if( isset( $arrValues['id_issue_date'] ) && $boolDirectSet ) $this->set( 'm_strIdIssueDate', trim( $arrValues['id_issue_date'] ) ); elseif( isset( $arrValues['id_issue_date'] ) ) $this->setIdIssueDate( $arrValues['id_issue_date'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId ) {
		$this->set( 'm_intCompanyIdentificationTypeId', CStrings::strToIntDef( $intCompanyIdentificationTypeId, NULL, false ) );
	}

	public function getCompanyIdentificationTypeId() {
		return $this->m_intCompanyIdentificationTypeId;
	}

	public function sqlCompanyIdentificationTypeId() {
		return ( true == isset( $this->m_intCompanyIdentificationTypeId ) ) ? ( string ) $this->m_intCompanyIdentificationTypeId : 'NULL';
	}

	public function setEncryptedValue( $strEncryptedValue ) {
		$this->set( 'm_strEncryptedValue', CStrings::strTrimDef( $strEncryptedValue, -1, NULL, true ) );
	}

	public function getEncryptedValue() {
		return $this->m_strEncryptedValue;
	}

	public function sqlEncryptedValue() {
		return ( true == isset( $this->m_strEncryptedValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEncryptedValue ) : '\'' . addslashes( $this->m_strEncryptedValue ) . '\'' ) : 'NULL';
	}

	public function setMaskedValue( $strMaskedValue ) {
		$this->set( 'm_strMaskedValue', CStrings::strTrimDef( $strMaskedValue, -1, NULL, true ) );
	}

	public function getMaskedValue() {
		return $this->m_strMaskedValue;
	}

	public function sqlMaskedValue() {
		return ( true == isset( $this->m_strMaskedValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaskedValue ) : '\'' . addslashes( $this->m_strMaskedValue ) . '\'' ) : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setIdExpirationDate( $strIdExpirationDate ) {
		$this->set( 'm_strIdExpirationDate', CStrings::strTrimDef( $strIdExpirationDate, -1, NULL, true ) );
	}

	public function getIdExpirationDate() {
		return $this->m_strIdExpirationDate;
	}

	public function sqlIdExpirationDate() {
		return ( true == isset( $this->m_strIdExpirationDate ) ) ? '\'' . $this->m_strIdExpirationDate . '\'' : 'NULL';
	}

	public function setIdIssueDate( $strIdIssueDate ) {
		$this->set( 'm_strIdIssueDate', CStrings::strTrimDef( $strIdIssueDate, -1, NULL, true ) );
	}

	public function getIdIssueDate() {
		return $this->m_strIdIssueDate;
	}

	public function sqlIdIssueDate() {
		return ( true == isset( $this->m_strIdIssueDate ) ) ? '\'' . $this->m_strIdIssueDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, updated_by, updated_on, created_by, created_on, customer_id, company_identification_type_id, encrypted_value, masked_value, file_id, id_expiration_date, id_issue_date, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlCompanyIdentificationTypeId() . ', ' .
		          $this->sqlEncryptedValue() . ', ' .
		          $this->sqlMaskedValue() . ', ' .
		          $this->sqlFileId() . ', ' .
		          $this->sqlIdExpirationDate() . ', ' .
		          $this->sqlIdIssueDate() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyIdentificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_value = ' . $this->sqlEncryptedValue(). ',' ; } elseif( true == array_key_exists( 'EncryptedValue', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_value = ' . $this->sqlEncryptedValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' masked_value = ' . $this->sqlMaskedValue(). ',' ; } elseif( true == array_key_exists( 'MaskedValue', $this->getChangedColumns() ) ) { $strSql .= ' masked_value = ' . $this->sqlMaskedValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' id_expiration_date = ' . $this->sqlIdExpirationDate(). ',' ; } elseif( true == array_key_exists( 'IdExpirationDate', $this->getChangedColumns() ) ) { $strSql .= ' id_expiration_date = ' . $this->sqlIdExpirationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' id_issue_date = ' . $this->sqlIdIssueDate(). ',' ; } elseif( true == array_key_exists( 'IdIssueDate', $this->getChangedColumns() ) ) { $strSql .= ' id_issue_date = ' . $this->sqlIdIssueDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'customer_id' => $this->getCustomerId(),
			'company_identification_type_id' => $this->getCompanyIdentificationTypeId(),
			'encrypted_value' => $this->getEncryptedValue(),
			'masked_value' => $this->getMaskedValue(),
			'file_id' => $this->getFileId(),
			'id_expiration_date' => $this->getIdExpirationDate(),
			'id_issue_date' => $this->getIdIssueDate(),
			'details' => $this->getDetails()
		);
	}

}
?>