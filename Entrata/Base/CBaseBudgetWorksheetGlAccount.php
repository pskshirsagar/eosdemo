<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorksheetGlAccount extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.budget_worksheet_gl_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetWorksheetId;
	protected $m_intGlAccountId;
	protected $m_intBudgetDataSourceTypeId;
	protected $m_intBudgetDataSourceSubTypeId;
	protected $m_strFormula;
	protected $m_fltAdjustmentPercent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPropertyId;
	protected $m_intBudgetWorksheetDataTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltAdjustmentPercent = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_intBudgetWorksheetDataTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_worksheet_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorksheetId', trim( $arrValues['budget_worksheet_id'] ) ); elseif( isset( $arrValues['budget_worksheet_id'] ) ) $this->setBudgetWorksheetId( $arrValues['budget_worksheet_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['budget_data_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceTypeId', trim( $arrValues['budget_data_source_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_type_id'] ) ) $this->setBudgetDataSourceTypeId( $arrValues['budget_data_source_type_id'] );
		if( isset( $arrValues['budget_data_source_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceSubTypeId', trim( $arrValues['budget_data_source_sub_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_sub_type_id'] ) ) $this->setBudgetDataSourceSubTypeId( $arrValues['budget_data_source_sub_type_id'] );
		if( isset( $arrValues['formula'] ) && $boolDirectSet ) $this->set( 'm_strFormula', trim( $arrValues['formula'] ) ); elseif( isset( $arrValues['formula'] ) ) $this->setFormula( $arrValues['formula'] );
		if( isset( $arrValues['adjustment_percent'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentPercent', trim( $arrValues['adjustment_percent'] ) ); elseif( isset( $arrValues['adjustment_percent'] ) ) $this->setAdjustmentPercent( $arrValues['adjustment_percent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['budget_worksheet_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorksheetDataTypeId', trim( $arrValues['budget_worksheet_data_type_id'] ) ); elseif( isset( $arrValues['budget_worksheet_data_type_id'] ) ) $this->setBudgetWorksheetDataTypeId( $arrValues['budget_worksheet_data_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetWorksheetId( $intBudgetWorksheetId ) {
		$this->set( 'm_intBudgetWorksheetId', CStrings::strToIntDef( $intBudgetWorksheetId, NULL, false ) );
	}

	public function getBudgetWorksheetId() {
		return $this->m_intBudgetWorksheetId;
	}

	public function sqlBudgetWorksheetId() {
		return ( true == isset( $this->m_intBudgetWorksheetId ) ) ? ( string ) $this->m_intBudgetWorksheetId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->set( 'm_intBudgetDataSourceTypeId', CStrings::strToIntDef( $intBudgetDataSourceTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function sqlBudgetDataSourceTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceTypeId : 'NULL';
	}

	public function setBudgetDataSourceSubTypeId( $intBudgetDataSourceSubTypeId ) {
		$this->set( 'm_intBudgetDataSourceSubTypeId', CStrings::strToIntDef( $intBudgetDataSourceSubTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceSubTypeId() {
		return $this->m_intBudgetDataSourceSubTypeId;
	}

	public function sqlBudgetDataSourceSubTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceSubTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceSubTypeId : 'NULL';
	}

	public function setFormula( $strFormula ) {
		$this->set( 'm_strFormula', CStrings::strTrimDef( $strFormula, -1, NULL, true ) );
	}

	public function getFormula() {
		return $this->m_strFormula;
	}

	public function sqlFormula() {
		return ( true == isset( $this->m_strFormula ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFormula ) : '\'' . addslashes( $this->m_strFormula ) . '\'' ) : 'NULL';
	}

	public function setAdjustmentPercent( $fltAdjustmentPercent ) {
		$this->set( 'm_fltAdjustmentPercent', CStrings::strToFloatDef( $fltAdjustmentPercent, NULL, false, 4 ) );
	}

	public function getAdjustmentPercent() {
		return $this->m_fltAdjustmentPercent;
	}

	public function sqlAdjustmentPercent() {
		return ( true == isset( $this->m_fltAdjustmentPercent ) ) ? ( string ) $this->m_fltAdjustmentPercent : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBudgetWorksheetDataTypeId( $intBudgetWorksheetDataTypeId ) {
		$this->set( 'm_intBudgetWorksheetDataTypeId', CStrings::strToIntDef( $intBudgetWorksheetDataTypeId, NULL, false ) );
	}

	public function getBudgetWorksheetDataTypeId() {
		return $this->m_intBudgetWorksheetDataTypeId;
	}

	public function sqlBudgetWorksheetDataTypeId() {
		return ( true == isset( $this->m_intBudgetWorksheetDataTypeId ) ) ? ( string ) $this->m_intBudgetWorksheetDataTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_worksheet_id, gl_account_id, budget_data_source_type_id, budget_data_source_sub_type_id, formula, adjustment_percent, updated_by, updated_on, created_by, created_on, property_id, budget_worksheet_data_type_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetWorksheetId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlBudgetDataSourceTypeId() . ', ' .
						$this->sqlBudgetDataSourceSubTypeId() . ', ' .
						$this->sqlFormula() . ', ' .
						$this->sqlAdjustmentPercent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlBudgetWorksheetDataTypeId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_worksheet_id = ' . $this->sqlBudgetWorksheetId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorksheetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_worksheet_id = ' . $this->sqlBudgetWorksheetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' formula = ' . $this->sqlFormula(). ',' ; } elseif( true == array_key_exists( 'Formula', $this->getChangedColumns() ) ) { $strSql .= ' formula = ' . $this->sqlFormula() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent(). ',' ; } elseif( true == array_key_exists( 'AdjustmentPercent', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_worksheet_data_type_id = ' . $this->sqlBudgetWorksheetDataTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorksheetDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_worksheet_data_type_id = ' . $this->sqlBudgetWorksheetDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_worksheet_id' => $this->getBudgetWorksheetId(),
			'gl_account_id' => $this->getGlAccountId(),
			'budget_data_source_type_id' => $this->getBudgetDataSourceTypeId(),
			'budget_data_source_sub_type_id' => $this->getBudgetDataSourceSubTypeId(),
			'formula' => $this->getFormula(),
			'adjustment_percent' => $this->getAdjustmentPercent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'property_id' => $this->getPropertyId(),
			'budget_worksheet_data_type_id' => $this->getBudgetWorksheetDataTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>