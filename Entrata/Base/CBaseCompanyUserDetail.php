<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.company_user_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intVacancyStatsEmailFrequencyId;
	protected $m_strLeadsPulledOn;
	protected $m_strVacancyUserTermsAgreedOn;
	protected $m_strVacancySalesTermsAgreedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['vacancy_stats_email_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intVacancyStatsEmailFrequencyId', trim( $arrValues['vacancy_stats_email_frequency_id'] ) ); elseif( isset( $arrValues['vacancy_stats_email_frequency_id'] ) ) $this->setVacancyStatsEmailFrequencyId( $arrValues['vacancy_stats_email_frequency_id'] );
		if( isset( $arrValues['leads_pulled_on'] ) && $boolDirectSet ) $this->set( 'm_strLeadsPulledOn', trim( $arrValues['leads_pulled_on'] ) ); elseif( isset( $arrValues['leads_pulled_on'] ) ) $this->setLeadsPulledOn( $arrValues['leads_pulled_on'] );
		if( isset( $arrValues['vacancy_user_terms_agreed_on'] ) && $boolDirectSet ) $this->set( 'm_strVacancyUserTermsAgreedOn', trim( $arrValues['vacancy_user_terms_agreed_on'] ) ); elseif( isset( $arrValues['vacancy_user_terms_agreed_on'] ) ) $this->setVacancyUserTermsAgreedOn( $arrValues['vacancy_user_terms_agreed_on'] );
		if( isset( $arrValues['vacancy_sales_terms_agreed_on'] ) && $boolDirectSet ) $this->set( 'm_strVacancySalesTermsAgreedOn', trim( $arrValues['vacancy_sales_terms_agreed_on'] ) ); elseif( isset( $arrValues['vacancy_sales_terms_agreed_on'] ) ) $this->setVacancySalesTermsAgreedOn( $arrValues['vacancy_sales_terms_agreed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setVacancyStatsEmailFrequencyId( $intVacancyStatsEmailFrequencyId ) {
		$this->set( 'm_intVacancyStatsEmailFrequencyId', CStrings::strToIntDef( $intVacancyStatsEmailFrequencyId, NULL, false ) );
	}

	public function getVacancyStatsEmailFrequencyId() {
		return $this->m_intVacancyStatsEmailFrequencyId;
	}

	public function sqlVacancyStatsEmailFrequencyId() {
		return ( true == isset( $this->m_intVacancyStatsEmailFrequencyId ) ) ? ( string ) $this->m_intVacancyStatsEmailFrequencyId : 'NULL';
	}

	public function setLeadsPulledOn( $strLeadsPulledOn ) {
		$this->set( 'm_strLeadsPulledOn', CStrings::strTrimDef( $strLeadsPulledOn, -1, NULL, true ) );
	}

	public function getLeadsPulledOn() {
		return $this->m_strLeadsPulledOn;
	}

	public function sqlLeadsPulledOn() {
		return ( true == isset( $this->m_strLeadsPulledOn ) ) ? '\'' . $this->m_strLeadsPulledOn . '\'' : 'NULL';
	}

	public function setVacancyUserTermsAgreedOn( $strVacancyUserTermsAgreedOn ) {
		$this->set( 'm_strVacancyUserTermsAgreedOn', CStrings::strTrimDef( $strVacancyUserTermsAgreedOn, -1, NULL, true ) );
	}

	public function getVacancyUserTermsAgreedOn() {
		return $this->m_strVacancyUserTermsAgreedOn;
	}

	public function sqlVacancyUserTermsAgreedOn() {
		return ( true == isset( $this->m_strVacancyUserTermsAgreedOn ) ) ? '\'' . $this->m_strVacancyUserTermsAgreedOn . '\'' : 'NULL';
	}

	public function setVacancySalesTermsAgreedOn( $strVacancySalesTermsAgreedOn ) {
		$this->set( 'm_strVacancySalesTermsAgreedOn', CStrings::strTrimDef( $strVacancySalesTermsAgreedOn, -1, NULL, true ) );
	}

	public function getVacancySalesTermsAgreedOn() {
		return $this->m_strVacancySalesTermsAgreedOn;
	}

	public function sqlVacancySalesTermsAgreedOn() {
		return ( true == isset( $this->m_strVacancySalesTermsAgreedOn ) ) ? '\'' . $this->m_strVacancySalesTermsAgreedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, vacancy_stats_email_frequency_id, leads_pulled_on, vacancy_user_terms_agreed_on, vacancy_sales_terms_agreed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlVacancyStatsEmailFrequencyId() . ', ' .
 						$this->sqlLeadsPulledOn() . ', ' .
 						$this->sqlVacancyUserTermsAgreedOn() . ', ' .
 						$this->sqlVacancySalesTermsAgreedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_stats_email_frequency_id = ' . $this->sqlVacancyStatsEmailFrequencyId() . ','; } elseif( true == array_key_exists( 'VacancyStatsEmailFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_stats_email_frequency_id = ' . $this->sqlVacancyStatsEmailFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_pulled_on = ' . $this->sqlLeadsPulledOn() . ','; } elseif( true == array_key_exists( 'LeadsPulledOn', $this->getChangedColumns() ) ) { $strSql .= ' leads_pulled_on = ' . $this->sqlLeadsPulledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_user_terms_agreed_on = ' . $this->sqlVacancyUserTermsAgreedOn() . ','; } elseif( true == array_key_exists( 'VacancyUserTermsAgreedOn', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_user_terms_agreed_on = ' . $this->sqlVacancyUserTermsAgreedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_sales_terms_agreed_on = ' . $this->sqlVacancySalesTermsAgreedOn() . ','; } elseif( true == array_key_exists( 'VacancySalesTermsAgreedOn', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_sales_terms_agreed_on = ' . $this->sqlVacancySalesTermsAgreedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'vacancy_stats_email_frequency_id' => $this->getVacancyStatsEmailFrequencyId(),
			'leads_pulled_on' => $this->getLeadsPulledOn(),
			'vacancy_user_terms_agreed_on' => $this->getVacancyUserTermsAgreedOn(),
			'vacancy_sales_terms_agreed_on' => $this->getVacancySalesTermsAgreedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>