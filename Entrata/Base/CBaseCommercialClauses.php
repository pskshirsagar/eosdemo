<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialClauses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialClauses extends CEosPluralBase {

	/**
	 * @return CCommercialClause[]
	 */
	public static function fetchCommercialClauses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCommercialClause::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialClause
	 */
	public static function fetchCommercialClause( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCommercialClause::class, $objDatabase );
	}

	public static function fetchCommercialClauseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_clauses', $objDatabase );
	}

	public static function fetchCommercialClauseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialClause( sprintf( 'SELECT * FROM commercial_clauses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialClausesByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialClauses( sprintf( 'SELECT * FROM commercial_clauses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCommercialClausesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauses( sprintf( 'SELECT * FROM commercial_clauses WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialClausesByCommercialClauseKeyIdByCid( $intCommercialClauseKeyId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauses( sprintf( 'SELECT * FROM commercial_clauses WHERE commercial_clause_key_id = %d AND cid = %d', $intCommercialClauseKeyId, $intCid ), $objDatabase );
	}

}
?>