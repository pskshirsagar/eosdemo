<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceTemplates extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceTemplate[]
	 */
	public static function fetchPropertyMaintenanceTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMaintenanceTemplate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceTemplate
	 */
	public static function fetchPropertyMaintenanceTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMaintenanceTemplate', $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_templates', $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplate( sprintf( 'SELECT * FROM property_maintenance_templates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplates( sprintf( 'SELECT * FROM property_maintenance_templates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplatesByMaintenanceTemplateIdByCid( $intMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplates( sprintf( 'SELECT * FROM property_maintenance_templates WHERE maintenance_template_id = %d AND cid = %d', ( int ) $intMaintenanceTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplates( sprintf( 'SELECT * FROM property_maintenance_templates WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplatesByPropertyMaintenanceTemplateIdByCid( $intPropertyMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplates( sprintf( 'SELECT * FROM property_maintenance_templates WHERE property_maintenance_template_id = %d AND cid = %d', ( int ) $intPropertyMaintenanceTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceTemplatesByMaintenanceGroupTypeIdByCid( $intMaintenanceGroupTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceTemplates( sprintf( 'SELECT * FROM property_maintenance_templates WHERE maintenance_group_type_id = %d AND cid = %d', ( int ) $intMaintenanceGroupTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>