<?php

class CBaseDefaultApPayeeTerm extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_ap_payee_terms';

	protected $m_intId;
	protected $m_strName;
	protected $m_strSystemCode;
	protected $m_fltPercentage;
	protected $m_intDays;
	protected $m_intDiscountPeriodDays;
	protected $m_boolIsFromInvoiceDate;
	protected $m_boolIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_fltPercentage = '0';
		$this->m_intDays = '0';
		$this->m_boolIsFromInvoiceDate = ( 1 );
		$this->m_boolIsSystem = ( 0 );
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['percentage'] ) && $boolDirectSet ) $this->set( 'm_fltPercentage', trim( $arrValues['percentage'] ) ); elseif( isset( $arrValues['percentage'] ) ) $this->setPercentage( $arrValues['percentage'] );
		if( isset( $arrValues['days'] ) && $boolDirectSet ) $this->set( 'm_intDays', trim( $arrValues['days'] ) ); elseif( isset( $arrValues['days'] ) ) $this->setDays( $arrValues['days'] );
		if( isset( $arrValues['discount_period_days'] ) && $boolDirectSet ) $this->set( 'm_intDiscountPeriodDays', trim( $arrValues['discount_period_days'] ) ); elseif( isset( $arrValues['discount_period_days'] ) ) $this->setDiscountPeriodDays( $arrValues['discount_period_days'] );
		if( isset( $arrValues['is_from_invoice_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromInvoiceDate', trim( stripcslashes( $arrValues['is_from_invoice_date'] ) ) ); elseif( isset( $arrValues['is_from_invoice_date'] ) ) $this->setIsFromInvoiceDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_invoice_date'] ) : $arrValues['is_from_invoice_date'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setPercentage( $fltPercentage ) {
		$this->set( 'm_fltPercentage', CStrings::strToFloatDef( $fltPercentage, NULL, false, 2 ) );
	}

	public function getPercentage() {
		return $this->m_fltPercentage;
	}

	public function sqlPercentage() {
		return ( true == isset( $this->m_fltPercentage ) ) ? ( string ) $this->m_fltPercentage : '0';
	}

	public function setDays( $intDays ) {
		$this->set( 'm_intDays', CStrings::strToIntDef( $intDays, NULL, false ) );
	}

	public function getDays() {
		return $this->m_intDays;
	}

	public function sqlDays() {
		return ( true == isset( $this->m_intDays ) ) ? ( string ) $this->m_intDays : '0';
	}

	public function setDiscountPeriodDays( $intDiscountPeriodDays ) {
		$this->set( 'm_intDiscountPeriodDays', CStrings::strToIntDef( $intDiscountPeriodDays, NULL, false ) );
	}

	public function getDiscountPeriodDays() {
		return $this->m_intDiscountPeriodDays;
	}

	public function sqlDiscountPeriodDays() {
		return ( true == isset( $this->m_intDiscountPeriodDays ) ) ? ( string ) $this->m_intDiscountPeriodDays : 'NULL';
	}

	public function setIsFromInvoiceDate( $boolIsFromInvoiceDate ) {
		$this->set( 'm_boolIsFromInvoiceDate', CStrings::strToBool( $boolIsFromInvoiceDate ) );
	}

	public function getIsFromInvoiceDate() {
		return $this->m_boolIsFromInvoiceDate;
	}

	public function sqlIsFromInvoiceDate() {
		return ( true == isset( $this->m_boolIsFromInvoiceDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromInvoiceDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'system_code' => $this->getSystemCode(),
			'percentage' => $this->getPercentage(),
			'days' => $this->getDays(),
			'discount_period_days' => $this->getDiscountPeriodDays(),
			'is_from_invoice_date' => $this->getIsFromInvoiceDate(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>