<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduleTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduleTypes extends CEosPluralBase {

	/**
	 * @return CScheduleType[]
	 */
	public static function fetchScheduleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduleType::class, $objDatabase );
	}

	/**
	 * @return CScheduleType
	 */
	public static function fetchScheduleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduleType::class, $objDatabase );
	}

	public static function fetchScheduleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'schedule_types', $objDatabase );
	}

	public static function fetchScheduleTypeById( $intId, $objDatabase ) {
		return self::fetchScheduleType( sprintf( 'SELECT * FROM schedule_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>