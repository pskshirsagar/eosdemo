<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReviewDetailAttribute extends CEosSingularBase {

	const TABLE_NAME = 'public.review_detail_attributes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyReviewAttributeId;
	protected $m_intReviewDetailId;
	protected $m_strValue;
	protected $m_intAttributeAssociatedBy;
	protected $m_strAttributeAssociatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAttributeAssociatedBy = '0';
		$this->m_intUpdatedBy = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_review_attribute_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyReviewAttributeId', trim( $arrValues['property_review_attribute_id'] ) ); elseif( isset( $arrValues['property_review_attribute_id'] ) ) $this->setPropertyReviewAttributeId( $arrValues['property_review_attribute_id'] );
		if( isset( $arrValues['review_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewDetailId', trim( $arrValues['review_detail_id'] ) ); elseif( isset( $arrValues['review_detail_id'] ) ) $this->setReviewDetailId( $arrValues['review_detail_id'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( stripcslashes( $arrValues['value'] ) ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
		if( isset( $arrValues['attribute_associated_by'] ) && $boolDirectSet ) $this->set( 'm_intAttributeAssociatedBy', trim( $arrValues['attribute_associated_by'] ) ); elseif( isset( $arrValues['attribute_associated_by'] ) ) $this->setAttributeAssociatedBy( $arrValues['attribute_associated_by'] );
		if( isset( $arrValues['attribute_associated_on'] ) && $boolDirectSet ) $this->set( 'm_strAttributeAssociatedOn', trim( $arrValues['attribute_associated_on'] ) ); elseif( isset( $arrValues['attribute_associated_on'] ) ) $this->setAttributeAssociatedOn( $arrValues['attribute_associated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyReviewAttributeId( $intPropertyReviewAttributeId ) {
		$this->set( 'm_intPropertyReviewAttributeId', CStrings::strToIntDef( $intPropertyReviewAttributeId, NULL, false ) );
	}

	public function getPropertyReviewAttributeId() {
		return $this->m_intPropertyReviewAttributeId;
	}

	public function sqlPropertyReviewAttributeId() {
		return ( true == isset( $this->m_intPropertyReviewAttributeId ) ) ? ( string ) $this->m_intPropertyReviewAttributeId : 'NULL';
	}

	public function setReviewDetailId( $intReviewDetailId ) {
		$this->set( 'm_intReviewDetailId', CStrings::strToIntDef( $intReviewDetailId, NULL, false ) );
	}

	public function getReviewDetailId() {
		return $this->m_intReviewDetailId;
	}

	public function sqlReviewDetailId() {
		return ( true == isset( $this->m_intReviewDetailId ) ) ? ( string ) $this->m_intReviewDetailId : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, 5, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
	}

	public function setAttributeAssociatedBy( $intAttributeAssociatedBy ) {
		$this->set( 'm_intAttributeAssociatedBy', CStrings::strToIntDef( $intAttributeAssociatedBy, NULL, false ) );
	}

	public function getAttributeAssociatedBy() {
		return $this->m_intAttributeAssociatedBy;
	}

	public function sqlAttributeAssociatedBy() {
		return ( true == isset( $this->m_intAttributeAssociatedBy ) ) ? ( string ) $this->m_intAttributeAssociatedBy : '0';
	}

	public function setAttributeAssociatedOn( $strAttributeAssociatedOn ) {
		$this->set( 'm_strAttributeAssociatedOn', CStrings::strTrimDef( $strAttributeAssociatedOn, -1, NULL, true ) );
	}

	public function getAttributeAssociatedOn() {
		return $this->m_strAttributeAssociatedOn;
	}

	public function sqlAttributeAssociatedOn() {
		return ( true == isset( $this->m_strAttributeAssociatedOn ) ) ? '\'' . $this->m_strAttributeAssociatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '0';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_review_attribute_id, review_detail_id, value, attribute_associated_by, attribute_associated_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyReviewAttributeId() . ', ' .
		          $this->sqlReviewDetailId() . ', ' .
		          $this->sqlValue() . ', ' .
		          $this->sqlAttributeAssociatedBy() . ', ' .
		          $this->sqlAttributeAssociatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_review_attribute_id = ' . $this->sqlPropertyReviewAttributeId() . ','; } elseif( true == array_key_exists( 'PropertyReviewAttributeId', $this->getChangedColumns() ) ) { $strSql .= ' property_review_attribute_id = ' . $this->sqlPropertyReviewAttributeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId() . ','; } elseif( true == array_key_exists( 'ReviewDetailId', $this->getChangedColumns() ) ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attribute_associated_by = ' . $this->sqlAttributeAssociatedBy() . ','; } elseif( true == array_key_exists( 'AttributeAssociatedBy', $this->getChangedColumns() ) ) { $strSql .= ' attribute_associated_by = ' . $this->sqlAttributeAssociatedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attribute_associated_on = ' . $this->sqlAttributeAssociatedOn() . ','; } elseif( true == array_key_exists( 'AttributeAssociatedOn', $this->getChangedColumns() ) ) { $strSql .= ' attribute_associated_on = ' . $this->sqlAttributeAssociatedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_review_attribute_id' => $this->getPropertyReviewAttributeId(),
			'review_detail_id' => $this->getReviewDetailId(),
			'value' => $this->getValue(),
			'attribute_associated_by' => $this->getAttributeAssociatedBy(),
			'attribute_associated_on' => $this->getAttributeAssociatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>