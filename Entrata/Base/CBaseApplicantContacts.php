<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantContacts extends CEosPluralBase {

	/**
	 * @return CApplicantContact[]
	 */
	public static function fetchApplicantContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantContact', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantContact
	 */
	public static function fetchApplicantContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantContact', $objDatabase );
	}

	public static function fetchApplicantContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_contacts', $objDatabase );
	}

	public static function fetchApplicantContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantContact( sprintf( 'SELECT * FROM applicant_contacts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantContactsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
	return self::fetchApplicantContacts( sprintf( 'SELECT * FROM applicant_contacts WHERE id IN ( %s ) AND cid = %d', implode( ',', $arrintIds ), ( int ) $intCid ), $objDatabase );
	}


	public static function fetchApplicantContactsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantContacts( sprintf( 'SELECT * FROM applicant_contacts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantContactsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantContacts( sprintf( 'SELECT * FROM applicant_contacts WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantContactsByCustomerContactTypeIdByCid( $intCustomerContactTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantContacts( sprintf( 'SELECT * FROM applicant_contacts WHERE customer_contact_type_id = %d AND cid = %d', ( int ) $intCustomerContactTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantContactsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchApplicantContacts( sprintf( 'SELECT * FROM applicant_contacts WHERE call_id = %d AND cid = %d', ( int ) $intCallId, ( int ) $intCid ), $objDatabase );
	}

}
?>