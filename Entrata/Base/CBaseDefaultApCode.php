<?php

class CBaseDefaultApCode extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_ap_codes';

	protected $m_intId;
	protected $m_intApCodeTypeId;
	protected $m_intApDefaultGlAccountId;
	protected $m_intItemDefaultGlAccountId;
	protected $m_intWipDefaultGlAccountId;
	protected $m_intConsumptionDefaultGlAccountId;
	protected $m_intDefaultArCodeId;
	protected $m_intDefaultJobApCodeId;
	protected $m_strName;
	protected $m_strItemNumber;
	protected $m_strDescription;
	protected $m_fltDefaultAmount;
	protected $m_intOrderNum;
	protected $m_boolPostToWip;
	protected $m_boolIsAutoAdded;
	protected $m_boolIsReserved;
	protected $m_boolIsSystem;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intApCodeTypeId = '1';
		$this->m_fltDefaultAmount = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolPostToWip = true;
		$this->m_boolIsAutoAdded = false;
		$this->m_boolIsReserved = false;
		$this->m_boolIsSystem = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ap_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeTypeId', trim( $arrValues['ap_code_type_id'] ) ); elseif( isset( $arrValues['ap_code_type_id'] ) ) $this->setApCodeTypeId( $arrValues['ap_code_type_id'] );
		if( isset( $arrValues['ap_default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApDefaultGlAccountId', trim( $arrValues['ap_default_gl_account_id'] ) ); elseif( isset( $arrValues['ap_default_gl_account_id'] ) ) $this->setApDefaultGlAccountId( $arrValues['ap_default_gl_account_id'] );
		if( isset( $arrValues['item_default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intItemDefaultGlAccountId', trim( $arrValues['item_default_gl_account_id'] ) ); elseif( isset( $arrValues['item_default_gl_account_id'] ) ) $this->setItemDefaultGlAccountId( $arrValues['item_default_gl_account_id'] );
		if( isset( $arrValues['wip_default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intWipDefaultGlAccountId', trim( $arrValues['wip_default_gl_account_id'] ) ); elseif( isset( $arrValues['wip_default_gl_account_id'] ) ) $this->setWipDefaultGlAccountId( $arrValues['wip_default_gl_account_id'] );
		if( isset( $arrValues['consumption_default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumptionDefaultGlAccountId', trim( $arrValues['consumption_default_gl_account_id'] ) ); elseif( isset( $arrValues['consumption_default_gl_account_id'] ) ) $this->setConsumptionDefaultGlAccountId( $arrValues['consumption_default_gl_account_id'] );
		if( isset( $arrValues['default_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultArCodeId', trim( $arrValues['default_ar_code_id'] ) ); elseif( isset( $arrValues['default_ar_code_id'] ) ) $this->setDefaultArCodeId( $arrValues['default_ar_code_id'] );
		if( isset( $arrValues['default_job_ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultJobApCodeId', trim( $arrValues['default_job_ap_code_id'] ) ); elseif( isset( $arrValues['default_job_ap_code_id'] ) ) $this->setDefaultJobApCodeId( $arrValues['default_job_ap_code_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['item_number'] ) && $boolDirectSet ) $this->set( 'm_strItemNumber', trim( stripcslashes( $arrValues['item_number'] ) ) ); elseif( isset( $arrValues['item_number'] ) ) $this->setItemNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_number'] ) : $arrValues['item_number'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['post_to_wip'] ) && $boolDirectSet ) $this->set( 'm_boolPostToWip', trim( stripcslashes( $arrValues['post_to_wip'] ) ) ); elseif( isset( $arrValues['post_to_wip'] ) ) $this->setPostToWip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_to_wip'] ) : $arrValues['post_to_wip'] );
		if( isset( $arrValues['is_auto_added'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoAdded', trim( stripcslashes( $arrValues['is_auto_added'] ) ) ); elseif( isset( $arrValues['is_auto_added'] ) ) $this->setIsAutoAdded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_added'] ) : $arrValues['is_auto_added'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_boolIsReserved', trim( stripcslashes( $arrValues['is_reserved'] ) ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reserved'] ) : $arrValues['is_reserved'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setApCodeTypeId( $intApCodeTypeId ) {
		$this->set( 'm_intApCodeTypeId', CStrings::strToIntDef( $intApCodeTypeId, NULL, false ) );
	}

	public function getApCodeTypeId() {
		return $this->m_intApCodeTypeId;
	}

	public function sqlApCodeTypeId() {
		return ( true == isset( $this->m_intApCodeTypeId ) ) ? ( string ) $this->m_intApCodeTypeId : '1';
	}

	public function setApDefaultGlAccountId( $intApDefaultGlAccountId ) {
		$this->set( 'm_intApDefaultGlAccountId', CStrings::strToIntDef( $intApDefaultGlAccountId, NULL, false ) );
	}

	public function getApDefaultGlAccountId() {
		return $this->m_intApDefaultGlAccountId;
	}

	public function sqlApDefaultGlAccountId() {
		return ( true == isset( $this->m_intApDefaultGlAccountId ) ) ? ( string ) $this->m_intApDefaultGlAccountId : 'NULL';
	}

	public function setItemDefaultGlAccountId( $intItemDefaultGlAccountId ) {
		$this->set( 'm_intItemDefaultGlAccountId', CStrings::strToIntDef( $intItemDefaultGlAccountId, NULL, false ) );
	}

	public function getItemDefaultGlAccountId() {
		return $this->m_intItemDefaultGlAccountId;
	}

	public function sqlItemDefaultGlAccountId() {
		return ( true == isset( $this->m_intItemDefaultGlAccountId ) ) ? ( string ) $this->m_intItemDefaultGlAccountId : 'NULL';
	}

	public function setWipDefaultGlAccountId( $intWipDefaultGlAccountId ) {
		$this->set( 'm_intWipDefaultGlAccountId', CStrings::strToIntDef( $intWipDefaultGlAccountId, NULL, false ) );
	}

	public function getWipDefaultGlAccountId() {
		return $this->m_intWipDefaultGlAccountId;
	}

	public function sqlWipDefaultGlAccountId() {
		return ( true == isset( $this->m_intWipDefaultGlAccountId ) ) ? ( string ) $this->m_intWipDefaultGlAccountId : 'NULL';
	}

	public function setConsumptionDefaultGlAccountId( $intConsumptionDefaultGlAccountId ) {
		$this->set( 'm_intConsumptionDefaultGlAccountId', CStrings::strToIntDef( $intConsumptionDefaultGlAccountId, NULL, false ) );
	}

	public function getConsumptionDefaultGlAccountId() {
		return $this->m_intConsumptionDefaultGlAccountId;
	}

	public function sqlConsumptionDefaultGlAccountId() {
		return ( true == isset( $this->m_intConsumptionDefaultGlAccountId ) ) ? ( string ) $this->m_intConsumptionDefaultGlAccountId : 'NULL';
	}

	public function setDefaultArCodeId( $intDefaultArCodeId ) {
		$this->set( 'm_intDefaultArCodeId', CStrings::strToIntDef( $intDefaultArCodeId, NULL, false ) );
	}

	public function getDefaultArCodeId() {
		return $this->m_intDefaultArCodeId;
	}

	public function sqlDefaultArCodeId() {
		return ( true == isset( $this->m_intDefaultArCodeId ) ) ? ( string ) $this->m_intDefaultArCodeId : 'NULL';
	}

	public function setDefaultJobApCodeId( $intDefaultJobApCodeId ) {
		$this->set( 'm_intDefaultJobApCodeId', CStrings::strToIntDef( $intDefaultJobApCodeId, NULL, false ) );
	}

	public function getDefaultJobApCodeId() {
		return $this->m_intDefaultJobApCodeId;
	}

	public function sqlDefaultJobApCodeId() {
		return ( true == isset( $this->m_intDefaultJobApCodeId ) ) ? ( string ) $this->m_intDefaultJobApCodeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setItemNumber( $strItemNumber ) {
		$this->set( 'm_strItemNumber', CStrings::strTrimDef( $strItemNumber, 50, NULL, true ) );
	}

	public function getItemNumber() {
		return $this->m_strItemNumber;
	}

	public function sqlItemNumber() {
		return ( true == isset( $this->m_strItemNumber ) ) ? '\'' . addslashes( $this->m_strItemNumber ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 4 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPostToWip( $boolPostToWip ) {
		$this->set( 'm_boolPostToWip', CStrings::strToBool( $boolPostToWip ) );
	}

	public function getPostToWip() {
		return $this->m_boolPostToWip;
	}

	public function sqlPostToWip() {
		return ( true == isset( $this->m_boolPostToWip ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostToWip ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoAdded( $boolIsAutoAdded ) {
		$this->set( 'm_boolIsAutoAdded', CStrings::strToBool( $boolIsAutoAdded ) );
	}

	public function getIsAutoAdded() {
		return $this->m_boolIsAutoAdded;
	}

	public function sqlIsAutoAdded() {
		return ( true == isset( $this->m_boolIsAutoAdded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoAdded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->set( 'm_boolIsReserved', CStrings::strToBool( $boolIsReserved ) );
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_boolIsReserved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReserved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ap_code_type_id, ap_default_gl_account_id, item_default_gl_account_id, wip_default_gl_account_id, consumption_default_gl_account_id, default_ar_code_id, default_job_ap_code_id, name, item_number, description, default_amount, order_num, post_to_wip, is_auto_added, is_reserved, is_system, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlApCodeTypeId() . ', ' .
						$this->sqlApDefaultGlAccountId() . ', ' .
						$this->sqlItemDefaultGlAccountId() . ', ' .
						$this->sqlWipDefaultGlAccountId() . ', ' .
						$this->sqlConsumptionDefaultGlAccountId() . ', ' .
						$this->sqlDefaultArCodeId() . ', ' .
						$this->sqlDefaultJobApCodeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlItemNumber() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDefaultAmount() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlPostToWip() . ', ' .
						$this->sqlIsAutoAdded() . ', ' .
						$this->sqlIsReserved() . ', ' .
						$this->sqlIsSystem() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_default_gl_account_id = ' . $this->sqlApDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApDefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_default_gl_account_id = ' . $this->sqlApDefaultGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_default_gl_account_id = ' . $this->sqlItemDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ItemDefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' item_default_gl_account_id = ' . $this->sqlItemDefaultGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wip_default_gl_account_id = ' . $this->sqlWipDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'WipDefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' wip_default_gl_account_id = ' . $this->sqlWipDefaultGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_default_gl_account_id = ' . $this->sqlConsumptionDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ConsumptionDefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' consumption_default_gl_account_id = ' . $this->sqlConsumptionDefaultGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_ar_code_id = ' . $this->sqlDefaultArCodeId(). ',' ; } elseif( true == array_key_exists( 'DefaultArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' default_ar_code_id = ' . $this->sqlDefaultArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_job_ap_code_id = ' . $this->sqlDefaultJobApCodeId(). ',' ; } elseif( true == array_key_exists( 'DefaultJobApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' default_job_ap_code_id = ' . $this->sqlDefaultJobApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_number = ' . $this->sqlItemNumber(). ',' ; } elseif( true == array_key_exists( 'ItemNumber', $this->getChangedColumns() ) ) { $strSql .= ' item_number = ' . $this->sqlItemNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount(). ',' ; } elseif( true == array_key_exists( 'DefaultAmount', $this->getChangedColumns() ) ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_to_wip = ' . $this->sqlPostToWip(). ',' ; } elseif( true == array_key_exists( 'PostToWip', $this->getChangedColumns() ) ) { $strSql .= ' post_to_wip = ' . $this->sqlPostToWip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_added = ' . $this->sqlIsAutoAdded(). ',' ; } elseif( true == array_key_exists( 'IsAutoAdded', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_added = ' . $this->sqlIsAutoAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved(). ',' ; } elseif( true == array_key_exists( 'IsReserved', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ap_code_type_id' => $this->getApCodeTypeId(),
			'ap_default_gl_account_id' => $this->getApDefaultGlAccountId(),
			'item_default_gl_account_id' => $this->getItemDefaultGlAccountId(),
			'wip_default_gl_account_id' => $this->getWipDefaultGlAccountId(),
			'consumption_default_gl_account_id' => $this->getConsumptionDefaultGlAccountId(),
			'default_ar_code_id' => $this->getDefaultArCodeId(),
			'default_job_ap_code_id' => $this->getDefaultJobApCodeId(),
			'name' => $this->getName(),
			'item_number' => $this->getItemNumber(),
			'description' => $this->getDescription(),
			'default_amount' => $this->getDefaultAmount(),
			'order_num' => $this->getOrderNum(),
			'post_to_wip' => $this->getPostToWip(),
			'is_auto_added' => $this->getIsAutoAdded(),
			'is_reserved' => $this->getIsReserved(),
			'is_system' => $this->getIsSystem(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>