<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionForms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionForms extends CEosPluralBase {

	/**
	 * @return CInspectionForm[]
	 */
	public static function fetchInspectionForms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInspectionForm::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionForm
	 */
	public static function fetchInspectionForm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspectionForm::class, $objDatabase );
	}

	public static function fetchInspectionFormCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_forms', $objDatabase );
	}

	public static function fetchInspectionFormByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionForm( sprintf( 'SELECT * FROM inspection_forms WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormsByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionForms( sprintf( 'SELECT * FROM inspection_forms WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		return self::fetchInspectionForms( sprintf( 'SELECT * FROM inspection_forms WHERE inspection_form_id = %d AND cid = %d', $intInspectionFormId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormsByMaintenanceLocationTypeIdByCid( $intMaintenanceLocationTypeId, $intCid, $objDatabase ) {
		return self::fetchInspectionForms( sprintf( 'SELECT * FROM inspection_forms WHERE maintenance_location_type_id = %d AND cid = %d', $intMaintenanceLocationTypeId, $intCid ), $objDatabase );
	}

}
?>