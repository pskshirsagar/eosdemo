<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCertificationStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCertificationStatusType[]
	 */
	public static function fetchSubsidyCertificationStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCertificationStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCertificationStatusType
	 */
	public static function fetchSubsidyCertificationStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCertificationStatusType::class, $objDatabase );
	}

	public static function fetchSubsidyCertificationStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certification_status_types', $objDatabase );
	}

	public static function fetchSubsidyCertificationStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCertificationStatusType( sprintf( 'SELECT * FROM subsidy_certification_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>