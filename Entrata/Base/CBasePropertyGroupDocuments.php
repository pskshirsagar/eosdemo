<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupDocuments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGroupDocuments extends CEosPluralBase {

	/**
	 * @return CPropertyGroupDocument[]
	 */
	public static function fetchPropertyGroupDocuments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyGroupDocument', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGroupDocument
	 */
	public static function fetchPropertyGroupDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyGroupDocument', $objDatabase );
	}

	public static function fetchPropertyGroupDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_group_documents', $objDatabase );
	}

	public static function fetchPropertyGroupDocumentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupDocument( sprintf( 'SELECT * FROM property_group_documents WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGroupDocuments( sprintf( 'SELECT * FROM property_group_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupDocuments( sprintf( 'SELECT * FROM property_group_documents WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupDocuments( sprintf( 'SELECT * FROM property_group_documents WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentAssociationTypeIdByCid( $intDocumentAssociationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupDocuments( sprintf( 'SELECT * FROM property_group_documents WHERE document_association_type_id = %d AND cid = %d', ( int ) $intDocumentAssociationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>