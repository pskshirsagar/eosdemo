<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateContactTypes
 * Do not add any new functions to this class.
 */

class CBaseCandidateContactTypes extends CEosPluralBase {

	/**
	 * @return CCandidateContactType[]
	 */
	public static function fetchCandidateContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCandidateContactType::class, $objDatabase );
	}

	/**
	 * @return CCandidateContactType
	 */
	public static function fetchCandidateContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCandidateContactType::class, $objDatabase );
	}

	public static function fetchCandidateContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_contact_types', $objDatabase );
	}

	public static function fetchCandidateContactTypeById( $intId, $objDatabase ) {
		return self::fetchCandidateContactType( sprintf( 'SELECT * FROM candidate_contact_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>