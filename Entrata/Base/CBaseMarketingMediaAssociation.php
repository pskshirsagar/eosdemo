<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMarketingMediaAssociation extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.marketing_media_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMarketingMediaId;
	protected $m_intMarketingMediaTypeId;
	protected $m_intMarketingMediaSubTypeId;
	protected $m_intReferenceId;
	protected $m_intMarketingStorageTypeId;
	protected $m_strMarketingStorageTypeReference;
	protected $m_boolIsOptimized;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strMediaCaption;
	protected $m_strMediaDescription;
	protected $m_strMediaAlt;
	protected $m_strLinkText;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_boolIsOptimized = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['marketing_media_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingMediaId', trim( $arrValues['marketing_media_id'] ) ); elseif( isset( $arrValues['marketing_media_id'] ) ) $this->setMarketingMediaId( $arrValues['marketing_media_id'] );
		if( isset( $arrValues['marketing_media_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingMediaTypeId', trim( $arrValues['marketing_media_type_id'] ) ); elseif( isset( $arrValues['marketing_media_type_id'] ) ) $this->setMarketingMediaTypeId( $arrValues['marketing_media_type_id'] );
		if( isset( $arrValues['marketing_media_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingMediaSubTypeId', trim( $arrValues['marketing_media_sub_type_id'] ) ); elseif( isset( $arrValues['marketing_media_sub_type_id'] ) ) $this->setMarketingMediaSubTypeId( $arrValues['marketing_media_sub_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['marketing_storage_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingStorageTypeId', trim( $arrValues['marketing_storage_type_id'] ) ); elseif( isset( $arrValues['marketing_storage_type_id'] ) ) $this->setMarketingStorageTypeId( $arrValues['marketing_storage_type_id'] );
		if( isset( $arrValues['marketing_storage_type_reference'] ) && $boolDirectSet ) $this->set( 'm_strMarketingStorageTypeReference', trim( $arrValues['marketing_storage_type_reference'] ) ); elseif( isset( $arrValues['marketing_storage_type_reference'] ) ) $this->setMarketingStorageTypeReference( $arrValues['marketing_storage_type_reference'] );
		if( isset( $arrValues['is_optimized'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptimized', trim( stripcslashes( $arrValues['is_optimized'] ) ) ); elseif( isset( $arrValues['is_optimized'] ) ) $this->setIsOptimized( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optimized'] ) : $arrValues['is_optimized'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['media_caption'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaCaption', trim( $arrValues['media_caption'] ) ); elseif( isset( $arrValues['media_caption'] ) ) $this->setMediaCaption( $arrValues['media_caption'] );
		if( isset( $arrValues['media_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaDescription', trim( $arrValues['media_description'] ) ); elseif( isset( $arrValues['media_description'] ) ) $this->setMediaDescription( $arrValues['media_description'] );
		if( isset( $arrValues['media_alt'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMediaAlt', trim( $arrValues['media_alt'] ) ); elseif( isset( $arrValues['media_alt'] ) ) $this->setMediaAlt( $arrValues['media_alt'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( $arrValues['link_text'] ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( $arrValues['link_text'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setMarketingMediaId( $intMarketingMediaId ) {
		$this->set( 'm_intMarketingMediaId', CStrings::strToIntDef( $intMarketingMediaId, NULL, false ) );
	}

	public function getMarketingMediaId() {
		return $this->m_intMarketingMediaId;
	}

	public function sqlMarketingMediaId() {
		return ( true == isset( $this->m_intMarketingMediaId ) ) ? ( string ) $this->m_intMarketingMediaId : 'NULL';
	}

	public function setMarketingMediaTypeId( $intMarketingMediaTypeId ) {
		$this->set( 'm_intMarketingMediaTypeId', CStrings::strToIntDef( $intMarketingMediaTypeId, NULL, false ) );
	}

	public function getMarketingMediaTypeId() {
		return $this->m_intMarketingMediaTypeId;
	}

	public function sqlMarketingMediaTypeId() {
		return ( true == isset( $this->m_intMarketingMediaTypeId ) ) ? ( string ) $this->m_intMarketingMediaTypeId : 'NULL';
	}

	public function setMarketingMediaSubTypeId( $intMarketingMediaSubTypeId ) {
		$this->set( 'm_intMarketingMediaSubTypeId', CStrings::strToIntDef( $intMarketingMediaSubTypeId, NULL, false ) );
	}

	public function getMarketingMediaSubTypeId() {
		return $this->m_intMarketingMediaSubTypeId;
	}

	public function sqlMarketingMediaSubTypeId() {
		return ( true == isset( $this->m_intMarketingMediaSubTypeId ) ) ? ( string ) $this->m_intMarketingMediaSubTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setMarketingStorageTypeId( $intMarketingStorageTypeId ) {
		$this->set( 'm_intMarketingStorageTypeId', CStrings::strToIntDef( $intMarketingStorageTypeId, NULL, false ) );
	}

	public function getMarketingStorageTypeId() {
		return $this->m_intMarketingStorageTypeId;
	}

	public function sqlMarketingStorageTypeId() {
		return ( true == isset( $this->m_intMarketingStorageTypeId ) ) ? ( string ) $this->m_intMarketingStorageTypeId : 'NULL';
	}

	public function setMarketingStorageTypeReference( $strMarketingStorageTypeReference ) {
		$this->set( 'm_strMarketingStorageTypeReference', CStrings::strTrimDef( $strMarketingStorageTypeReference, 4096, NULL, true ) );
	}

	public function getMarketingStorageTypeReference() {
		return $this->m_strMarketingStorageTypeReference;
	}

	public function sqlMarketingStorageTypeReference() {
		return ( true == isset( $this->m_strMarketingStorageTypeReference ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingStorageTypeReference ) : '\'' . addslashes( $this->m_strMarketingStorageTypeReference ) . '\'' ) : 'NULL';
	}

	public function setIsOptimized( $boolIsOptimized ) {
		$this->set( 'm_boolIsOptimized', CStrings::strToBool( $boolIsOptimized ) );
	}

	public function getIsOptimized() {
		return $this->m_boolIsOptimized;
	}

	public function sqlIsOptimized() {
		return ( true == isset( $this->m_boolIsOptimized ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptimized ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setMediaCaption( $strMediaCaption, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaCaption', CStrings::strTrimDef( $strMediaCaption, -1, NULL, true ), $strLocaleCode );
	}

	public function getMediaCaption( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaCaption', $strLocaleCode );
	}

	public function sqlMediaCaption() {
		return ( true == isset( $this->m_strMediaCaption ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMediaCaption ) : '\'' . addslashes( $this->m_strMediaCaption ) . '\'' ) : 'NULL';
	}

	public function setMediaDescription( $strMediaDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaDescription', CStrings::strTrimDef( $strMediaDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getMediaDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaDescription', $strLocaleCode );
	}

	public function sqlMediaDescription() {
		return ( true == isset( $this->m_strMediaDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMediaDescription ) : '\'' . addslashes( $this->m_strMediaDescription ) . '\'' ) : 'NULL';
	}

	public function setMediaAlt( $strMediaAlt, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMediaAlt', CStrings::strTrimDef( $strMediaAlt, -1, NULL, true ), $strLocaleCode );
	}

	public function getMediaAlt( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMediaAlt', $strLocaleCode );
	}

	public function sqlMediaAlt() {
		return ( true == isset( $this->m_strMediaAlt ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMediaAlt ) : '\'' . addslashes( $this->m_strMediaAlt ) . '\'' ) : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, -1, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkText ) : '\'' . addslashes( $this->m_strLinkText ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, marketing_media_id, marketing_media_type_id, marketing_media_sub_type_id, reference_id, marketing_storage_type_id, marketing_storage_type_reference, is_optimized, order_num, details, created_by, created_on, updated_by, updated_on, deleted_by, deleted_on, media_caption, media_description, media_alt, link_text )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMarketingMediaId() . ', ' .
						$this->sqlMarketingMediaTypeId() . ', ' .
						$this->sqlMarketingMediaSubTypeId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlMarketingStorageTypeId() . ', ' .
						$this->sqlMarketingStorageTypeReference() . ', ' .
						$this->sqlIsOptimized() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlMediaCaption() . ', ' .
						$this->sqlMediaDescription() . ', ' .
						$this->sqlMediaAlt() . ', ' .
						$this->sqlLinkText() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_media_id = ' . $this->sqlMarketingMediaId(). ',' ; } elseif( true == array_key_exists( 'MarketingMediaId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_media_id = ' . $this->sqlMarketingMediaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_media_type_id = ' . $this->sqlMarketingMediaTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketingMediaTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_media_type_id = ' . $this->sqlMarketingMediaTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_media_sub_type_id = ' . $this->sqlMarketingMediaSubTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketingMediaSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_media_sub_type_id = ' . $this->sqlMarketingMediaSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_storage_type_id = ' . $this->sqlMarketingStorageTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketingStorageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_storage_type_id = ' . $this->sqlMarketingStorageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_storage_type_reference = ' . $this->sqlMarketingStorageTypeReference(). ',' ; } elseif( true == array_key_exists( 'MarketingStorageTypeReference', $this->getChangedColumns() ) ) { $strSql .= ' marketing_storage_type_reference = ' . $this->sqlMarketingStorageTypeReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optimized = ' . $this->sqlIsOptimized(). ',' ; } elseif( true == array_key_exists( 'IsOptimized', $this->getChangedColumns() ) ) { $strSql .= ' is_optimized = ' . $this->sqlIsOptimized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption(). ',' ; } elseif( true == array_key_exists( 'MediaCaption', $this->getChangedColumns() ) ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription(). ',' ; } elseif( true == array_key_exists( 'MediaDescription', $this->getChangedColumns() ) ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt(). ',' ; } elseif( true == array_key_exists( 'MediaAlt', $this->getChangedColumns() ) ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'marketing_media_id' => $this->getMarketingMediaId(),
			'marketing_media_type_id' => $this->getMarketingMediaTypeId(),
			'marketing_media_sub_type_id' => $this->getMarketingMediaSubTypeId(),
			'reference_id' => $this->getReferenceId(),
			'marketing_storage_type_id' => $this->getMarketingStorageTypeId(),
			'marketing_storage_type_reference' => $this->getMarketingStorageTypeReference(),
			'is_optimized' => $this->getIsOptimized(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'media_caption' => $this->getMediaCaption(),
			'media_description' => $this->getMediaDescription(),
			'media_alt' => $this->getMediaAlt(),
			'link_text' => $this->getLinkText()
		);
	}

}
?>