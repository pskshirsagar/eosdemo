<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerDataTypes extends CEosPluralBase {

	/**
	 * @return CCustomerDataType[]
	 */
	public static function fetchCustomerDataTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerDataType::class, $objDatabase );
	}

	/**
	 * @return CCustomerDataType
	 */
	public static function fetchCustomerDataType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerDataType::class, $objDatabase );
	}

	public static function fetchCustomerDataTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_data_types', $objDatabase );
	}

	public static function fetchCustomerDataTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerDataType( sprintf( 'SELECT * FROM customer_data_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>