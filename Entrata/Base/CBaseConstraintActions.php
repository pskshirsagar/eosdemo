<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CConstraintActions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseConstraintActions extends CEosPluralBase {

	/**
	 * @return CConstraintAction[]
	 */
	public static function fetchConstraintActions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CConstraintAction', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CConstraintAction
	 */
	public static function fetchConstraintAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConstraintAction', $objDatabase );
	}

	public static function fetchConstraintActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'constraint_actions', $objDatabase );
	}

	public static function fetchConstraintActionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchConstraintAction( sprintf( 'SELECT * FROM constraint_actions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintActionsByCid( $intCid, $objDatabase ) {
		return self::fetchConstraintActions( sprintf( 'SELECT * FROM constraint_actions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintActionsByRevenueConstraintIdByCid( $intRevenueConstraintId, $intCid, $objDatabase ) {
		return self::fetchConstraintActions( sprintf( 'SELECT * FROM constraint_actions WHERE revenue_constraint_id = %d AND cid = %d', ( int ) $intRevenueConstraintId, ( int ) $intCid ), $objDatabase );
	}

}
?>