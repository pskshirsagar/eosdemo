<?php

class CBaseGlExportBatchTypes extends CEosPluralBase {

	/**
	 * @return CGlExportBatchType[]
	 */
	public static function fetchGlExportBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlExportBatchType::class, $objDatabase );
	}

	/**
	 * @return CGlExportBatchType
	 */
	public static function fetchGlExportBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlExportBatchType::class, $objDatabase );
	}

	public static function fetchGlExportBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_export_batch_types', $objDatabase );
	}

	public static function fetchGlExportBatchTypeById( $intId, $objDatabase ) {
		return self::fetchGlExportBatchType( sprintf( 'SELECT * FROM gl_export_batch_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>