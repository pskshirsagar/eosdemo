<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultOccupations
 * Do not add any new functions to this class.
 */

class CBaseDefaultOccupations extends CEosPluralBase {

	/**
	 * @return CDefaultOccupation[]
	 */
	public static function fetchDefaultOccupations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultOccupation::class, $objDatabase );
	}

	/**
	 * @return CDefaultOccupation
	 */
	public static function fetchDefaultOccupation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultOccupation::class, $objDatabase );
	}

	public static function fetchDefaultOccupationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_occupations', $objDatabase );
	}

	public static function fetchDefaultOccupationById( $intId, $objDatabase ) {
		return self::fetchDefaultOccupation( sprintf( 'SELECT * FROM default_occupations WHERE id = %d', $intId ), $objDatabase );
	}

}
?>