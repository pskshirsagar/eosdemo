<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFormAnswer extends CEosSingularBase {

	const TABLE_NAME = 'public.form_answers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFormId;
	protected $m_intDocumentComponentId;
	protected $m_strQuestion;
	protected $m_strAnswer;
	protected $m_strTag;
	protected $m_intIsSecure;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSecure = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['form_id'] ) && $boolDirectSet ) $this->set( 'm_intFormId', trim( $arrValues['form_id'] ) ); elseif( isset( $arrValues['form_id'] ) ) $this->setFormId( $arrValues['form_id'] );
		if( isset( $arrValues['document_component_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentComponentId', trim( $arrValues['document_component_id'] ) ); elseif( isset( $arrValues['document_component_id'] ) ) $this->setDocumentComponentId( $arrValues['document_component_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['tag'] ) && $boolDirectSet ) $this->set( 'm_strTag', trim( stripcslashes( $arrValues['tag'] ) ) ); elseif( isset( $arrValues['tag'] ) ) $this->setTag( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tag'] ) : $arrValues['tag'] );
		if( isset( $arrValues['is_secure'] ) && $boolDirectSet ) $this->set( 'm_intIsSecure', trim( $arrValues['is_secure'] ) ); elseif( isset( $arrValues['is_secure'] ) ) $this->setIsSecure( $arrValues['is_secure'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFormId( $intFormId ) {
		$this->set( 'm_intFormId', CStrings::strToIntDef( $intFormId, NULL, false ) );
	}

	public function getFormId() {
		return $this->m_intFormId;
	}

	public function sqlFormId() {
		return ( true == isset( $this->m_intFormId ) ) ? ( string ) $this->m_intFormId : 'NULL';
	}

	public function setDocumentComponentId( $intDocumentComponentId ) {
		$this->set( 'm_intDocumentComponentId', CStrings::strToIntDef( $intDocumentComponentId, NULL, false ) );
	}

	public function getDocumentComponentId() {
		return $this->m_intDocumentComponentId;
	}

	public function sqlDocumentComponentId() {
		return ( true == isset( $this->m_intDocumentComponentId ) ) ? ( string ) $this->m_intDocumentComponentId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, 240, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, -1, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setTag( $strTag ) {
		$this->set( 'm_strTag', CStrings::strTrimDef( $strTag, -1, NULL, true ) );
	}

	public function getTag() {
		return $this->m_strTag;
	}

	public function sqlTag() {
		return ( true == isset( $this->m_strTag ) ) ? '\'' . addslashes( $this->m_strTag ) . '\'' : 'NULL';
	}

	public function setIsSecure( $intIsSecure ) {
		$this->set( 'm_intIsSecure', CStrings::strToIntDef( $intIsSecure, NULL, false ) );
	}

	public function getIsSecure() {
		return $this->m_intIsSecure;
	}

	public function sqlIsSecure() {
		return ( true == isset( $this->m_intIsSecure ) ) ? ( string ) $this->m_intIsSecure : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, form_id, document_component_id, question, answer, tag, is_secure, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlFormId() . ', ' .
 						$this->sqlDocumentComponentId() . ', ' .
 						$this->sqlQuestion() . ', ' .
 						$this->sqlAnswer() . ', ' .
 						$this->sqlTag() . ', ' .
 						$this->sqlIsSecure() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_id = ' . $this->sqlFormId() . ','; } elseif( true == array_key_exists( 'FormId', $this->getChangedColumns() ) ) { $strSql .= ' form_id = ' . $this->sqlFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_component_id = ' . $this->sqlDocumentComponentId() . ','; } elseif( true == array_key_exists( 'DocumentComponentId', $this->getChangedColumns() ) ) { $strSql .= ' document_component_id = ' . $this->sqlDocumentComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tag = ' . $this->sqlTag() . ','; } elseif( true == array_key_exists( 'Tag', $this->getChangedColumns() ) ) { $strSql .= ' tag = ' . $this->sqlTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_secure = ' . $this->sqlIsSecure() . ','; } elseif( true == array_key_exists( 'IsSecure', $this->getChangedColumns() ) ) { $strSql .= ' is_secure = ' . $this->sqlIsSecure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'form_id' => $this->getFormId(),
			'document_component_id' => $this->getDocumentComponentId(),
			'question' => $this->getQuestion(),
			'answer' => $this->getAnswer(),
			'tag' => $this->getTag(),
			'is_secure' => $this->getIsSecure(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>