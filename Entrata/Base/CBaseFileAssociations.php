<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileAssociations extends CEosPluralBase {

	/**
	 * @return CFileAssociation[]
	 */
	public static function fetchFileAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileAssociation
	 */
	public static function fetchFileAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileAssociation::class, $objDatabase );
	}

	public static function fetchFileAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_associations', $objDatabase );
	}

	public static function fetchFileAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileAssociation( sprintf( 'SELECT * FROM file_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE quote_id = %d AND cid = %d', ( int ) $intQuoteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCustomerPetIdByCid( $intCustomerPetId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE customer_pet_id = %d AND cid = %d', ( int ) $intCustomerPetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ar_transaction_id = %d AND cid = %d', ( int ) $intArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByInspectionResponseIdByCid( $intInspectionResponseId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE inspection_response_id = %d AND cid = %d', ( int ) $intInspectionResponseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCompanyUserReportHistoryIdByCid( $intCompanyUserReportHistoryId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE company_user_report_history_id = %d AND cid = %d', ( int ) $intCompanyUserReportHistoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ap_header_id = %d AND cid = %d', ( int ) $intApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByApPaymentBatchIdByCid( $intApPaymentBatchId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE ap_payment_batch_id = %d AND cid = %d', ( int ) $intApPaymentBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCheckComponentTypeIdByCid( $intCheckComponentTypeId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE check_component_type_id = %d AND cid = %d', ( int ) $intCheckComponentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE gl_header_id = %d AND cid = %d', ( int ) $intGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE gl_reconciliation_id = %d AND cid = %d', ( int ) $intGlReconciliationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE gl_header_schedule_id = %d AND cid = %d', ( int ) $intGlHeaderScheduleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByChoreIdByCid( $intChoreId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE chore_id = %d AND cid = %d', ( int ) $intChoreId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByVpDocumentIdByCid( $intVpDocumentId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE vp_document_id = %d AND cid = %d', ( int ) $intVpDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCompanyOwnerIdByCid( $intCompanyOwnerId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE company_owner_id = %d AND cid = %d', ( int ) $intCompanyOwnerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsBySystemMessageIdByCid( $intSystemMessageId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE system_message_id = %d AND cid = %d', ( int ) $intSystemMessageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE campaign_id = %d AND cid = %d', ( int ) $intCampaignId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE scheduled_email_id = %d AND cid = %d', ( int ) $intScheduledEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByScheduledTaskEmailIdByCid( $intScheduledTaskEmailId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE scheduled_task_email_id = %d AND cid = %d', ( int ) $intScheduledTaskEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByScheduledChargeIdByCid( $intScheduledChargeId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE scheduled_charge_id = %d AND cid = %d', ( int ) $intScheduledChargeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByEsaFileIdByCid( $intEsaFileId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE esa_file_id = %d AND cid = %d', ( int ) $intEsaFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByCustomerDataVerificationIdByCid( $intCustomerDataVerificationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE customer_data_verification_id = %d AND cid = %d', ( int ) $intCustomerDataVerificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByBudgetChangeOrderIdByCid( $intBudgetChangeOrderId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE budget_change_order_id = %d AND cid = %d', ( int ) $intBudgetChangeOrderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsBySubsidyCertificationIdByCid( $intSubsidyCertificationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE subsidy_certification_id = %d AND cid = %d', ( int ) $intSubsidyCertificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsBySubsidyHapRequestIdByCid( $intSubsidyHapRequestId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE subsidy_hap_request_id = %d AND cid = %d', ( int ) $intSubsidyHapRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsBySubsidySpecialClaimIdByCid( $intSubsidySpecialClaimId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE subsidy_special_claim_id = %d AND cid = %d', ( int ) $intSubsidySpecialClaimId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByMilitaryMacPeriodIdByCid( $intMilitaryMacPeriodId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE military_mac_period_id = %d AND cid = %d', ( int ) $intMilitaryMacPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByViolationIdByCid( $intViolationId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE violation_id = %d AND cid = %d', ( int ) $intViolationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE resident_insurance_policy_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileAssociationsByOwnerIdByCid( $intOwnerId, $intCid, $objDatabase ) {
		return self::fetchFileAssociations( sprintf( 'SELECT * FROM file_associations WHERE owner_id = %d AND cid = %d', ( int ) $intOwnerId, ( int ) $intCid ), $objDatabase );
	}

}
?>