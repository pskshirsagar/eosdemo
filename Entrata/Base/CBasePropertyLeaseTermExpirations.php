<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLeaseTermExpirations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyLeaseTermExpirations extends CEosPluralBase {

	/**
	 * @return CPropertyLeaseTermExpiration[]
	 */
	public static function fetchPropertyLeaseTermExpirations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyLeaseTermExpiration', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyLeaseTermExpiration
	 */
	public static function fetchPropertyLeaseTermExpiration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyLeaseTermExpiration', $objDatabase );
	}

	public static function fetchPropertyLeaseTermExpirationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_lease_term_expirations', $objDatabase );
	}

	public static function fetchPropertyLeaseTermExpirationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeaseTermExpiration( sprintf( 'SELECT * FROM property_lease_term_expirations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeaseTermExpirationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyLeaseTermExpirations( sprintf( 'SELECT * FROM property_lease_term_expirations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeaseTermExpirationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeaseTermExpirations( sprintf( 'SELECT * FROM property_lease_term_expirations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>