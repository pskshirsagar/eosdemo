<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetConditions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetConditions extends CEosPluralBase {

	/**
	 * @return CAssetCondition[]
	 */
	public static function fetchAssetConditions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAssetCondition::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetCondition
	 */
	public static function fetchAssetCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAssetCondition::class, $objDatabase );
	}

	public static function fetchAssetConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_conditions', $objDatabase );
	}

	public static function fetchAssetConditionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetCondition( sprintf( 'SELECT * FROM asset_conditions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAssetConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchAssetConditions( sprintf( 'SELECT * FROM asset_conditions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAssetConditionsByDefaultAssetConditionIdByCid( $intDefaultAssetConditionId, $intCid, $objDatabase ) {
		return self::fetchAssetConditions( sprintf( 'SELECT * FROM asset_conditions WHERE default_asset_condition_id = %d AND cid = %d', $intDefaultAssetConditionId, $intCid ), $objDatabase );
	}

}
?>