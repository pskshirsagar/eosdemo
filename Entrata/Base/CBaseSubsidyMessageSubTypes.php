<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessageSubTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyMessageSubTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyMessageSubType[]
	 */
	public static function fetchSubsidyMessageSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyMessageSubType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyMessageSubType
	 */
	public static function fetchSubsidyMessageSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyMessageSubType::class, $objDatabase );
	}

	public static function fetchSubsidyMessageSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_message_sub_types', $objDatabase );
	}

	public static function fetchSubsidyMessageSubTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyMessageSubType( sprintf( 'SELECT * FROM subsidy_message_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>