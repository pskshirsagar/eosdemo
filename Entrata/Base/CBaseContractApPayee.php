<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContractApPayee extends CEosSingularBase {

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_intApPayeeId;
	protected $m_fltBidAmount;
	protected $m_strBidDescription;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltBidAmount = '0';
		$this->m_strAcceptedOn = 'now()';
		$this->m_strDeniedOn = 'now()';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->m_intContractId = trim( $arrValues['contract_id'] ); else if( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->m_intApPayeeId = trim( $arrValues['ap_payee_id'] ); else if( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['bid_amount'] ) && $boolDirectSet ) $this->m_fltBidAmount = trim( $arrValues['bid_amount'] ); else if( isset( $arrValues['bid_amount'] ) ) $this->setBidAmount( $arrValues['bid_amount'] );
		if( isset( $arrValues['bid_description'] ) && $boolDirectSet ) $this->m_strBidDescription = trim( stripcslashes( $arrValues['bid_description'] ) ); else if( isset( $arrValues['bid_description'] ) ) $this->setBidDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bid_description'] ) : $arrValues['bid_description'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->m_intAcceptedBy = trim( $arrValues['accepted_by'] ); else if( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->m_strAcceptedOn = trim( $arrValues['accepted_on'] ); else if( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->m_intDeniedBy = trim( $arrValues['denied_by'] ); else if( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->m_strDeniedOn = trim( $arrValues['denied_on'] ); else if( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->m_intContractId = CStrings::strToIntDef( $intContractId, NULL, false );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? (string) $this->m_intContractId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = CStrings::strToIntDef( $intApPayeeId, NULL, false );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? (string) $this->m_intApPayeeId : 'NULL';
	}

	public function setBidAmount( $fltBidAmount ) {
		$this->m_fltBidAmount = CStrings::strToFloatDef( $fltBidAmount, NULL, false, 2 );
	}

	public function getBidAmount() {
		return $this->m_fltBidAmount;
	}

	public function sqlBidAmount() {
		return ( true == isset( $this->m_fltBidAmount ) ) ? (string) $this->m_fltBidAmount : '0';
	}

	public function setBidDescription( $strBidDescription ) {
		$this->m_strBidDescription = CStrings::strTrimDef( $strBidDescription, -1, NULL, true );
	}

	public function getBidDescription() {
		return $this->m_strBidDescription;
	}

	public function sqlBidDescription() {
		return ( true == isset( $this->m_strBidDescription ) ) ? '\'' . addslashes( $this->m_strBidDescription ) . '\'' : 'NULL';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->m_intAcceptedBy = CStrings::strToIntDef( $intAcceptedBy, NULL, false );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? (string) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->m_strAcceptedOn = CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->m_intDeniedBy = CStrings::strToIntDef( $intDeniedBy, NULL, false );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? (string) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->m_strDeniedOn = CStrings::strTrimDef( $strDeniedOn, -1, NULL, true );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.contract_ap_payees_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.contract_ap_payees
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlBidAmount() . ', ' .
 						$this->sqlBidDescription() . ', ' .
 						$this->sqlAcceptedBy() . ', ' .
 						$this->sqlAcceptedOn() . ', ' .
 						$this->sqlDeniedBy() . ', ' .
 						$this->sqlDeniedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.contract_ap_payees
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContractId() ) != $this->getOriginalValueByFieldName ( 'contract_id' ) ) { $arrstrOriginalValueChanges['contract_id'] = $this->sqlContractId(); $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApPayeeId() ) != $this->getOriginalValueByFieldName ( 'ap_payee_id' ) ) { $arrstrOriginalValueChanges['ap_payee_id'] = $this->sqlApPayeeId(); $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bid_amount = ' . $this->sqlBidAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlBidAmount() ), $this->getOriginalValueByFieldName ( 'bid_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['bid_amount'] = $this->sqlBidAmount(); $strSql .= ' bid_amount = ' . $this->sqlBidAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bid_description = ' . $this->sqlBidDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlBidDescription() ) != $this->getOriginalValueByFieldName ( 'bid_description' ) ) { $arrstrOriginalValueChanges['bid_description'] = $this->sqlBidDescription(); $strSql .= ' bid_description = ' . $this->sqlBidDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAcceptedBy() ) != $this->getOriginalValueByFieldName ( 'accepted_by' ) ) { $arrstrOriginalValueChanges['accepted_by'] = $this->sqlAcceptedBy(); $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAcceptedOn() ) != $this->getOriginalValueByFieldName ( 'accepted_on' ) ) { $arrstrOriginalValueChanges['accepted_on'] = $this->sqlAcceptedOn(); $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeniedBy() ) != $this->getOriginalValueByFieldName ( 'denied_by' ) ) { $arrstrOriginalValueChanges['denied_by'] = $this->sqlDeniedBy(); $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeniedOn() ) != $this->getOriginalValueByFieldName ( 'denied_on' ) ) { $arrstrOriginalValueChanges['denied_on'] = $this->sqlDeniedOn(); $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.contract_ap_payees WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.contract_ap_payees_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'bid_amount' => $this->getBidAmount(),
			'bid_description' => $this->getBidDescription(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>