<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.calendar_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intCalendarEventCategoryId;
	protected $m_intCalendarEventTypeId;
	protected $m_intEventTypeId;
	protected $m_intEventId;
	protected $m_intParentCalendarEventId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intLeaseId;
	protected $m_intLeaseCustomerId;
	protected $m_intReferenceId;
	protected $m_strTitle;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strScheduleDetails;
	protected $m_strEventDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['calendar_event_category_id'] ) && $boolDirectSet ) $this->set( 'm_intCalendarEventCategoryId', trim( $arrValues['calendar_event_category_id'] ) ); elseif( isset( $arrValues['calendar_event_category_id'] ) ) $this->setCalendarEventCategoryId( $arrValues['calendar_event_category_id'] );
		if( isset( $arrValues['calendar_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCalendarEventTypeId', trim( $arrValues['calendar_event_type_id'] ) ); elseif( isset( $arrValues['calendar_event_type_id'] ) ) $this->setCalendarEventTypeId( $arrValues['calendar_event_type_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['parent_calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_intParentCalendarEventId', trim( $arrValues['parent_calendar_event_id'] ) ); elseif( isset( $arrValues['parent_calendar_event_id'] ) ) $this->setParentCalendarEventId( $arrValues['parent_calendar_event_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCustomerId', trim( $arrValues['lease_customer_id'] ) ); elseif( isset( $arrValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrValues['lease_customer_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['schedule_details'] ) && $boolDirectSet ) $this->set( 'm_strScheduleDetails', trim( stripcslashes( $arrValues['schedule_details'] ) ) ); elseif( isset( $arrValues['schedule_details'] ) ) $this->setScheduleDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schedule_details'] ) : $arrValues['schedule_details'] );
		if( isset( $arrValues['event_details'] ) && $boolDirectSet ) $this->set( 'm_strEventDetails', trim( stripcslashes( $arrValues['event_details'] ) ) ); elseif( isset( $arrValues['event_details'] ) ) $this->setEventDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_details'] ) : $arrValues['event_details'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setCalendarEventCategoryId( $intCalendarEventCategoryId ) {
		$this->set( 'm_intCalendarEventCategoryId', CStrings::strToIntDef( $intCalendarEventCategoryId, NULL, false ) );
	}

	public function getCalendarEventCategoryId() {
		return $this->m_intCalendarEventCategoryId;
	}

	public function sqlCalendarEventCategoryId() {
		return ( true == isset( $this->m_intCalendarEventCategoryId ) ) ? ( string ) $this->m_intCalendarEventCategoryId : 'NULL';
	}

	public function setCalendarEventTypeId( $intCalendarEventTypeId ) {
		$this->set( 'm_intCalendarEventTypeId', CStrings::strToIntDef( $intCalendarEventTypeId, NULL, false ) );
	}

	public function getCalendarEventTypeId() {
		return $this->m_intCalendarEventTypeId;
	}

	public function sqlCalendarEventTypeId() {
		return ( true == isset( $this->m_intCalendarEventTypeId ) ) ? ( string ) $this->m_intCalendarEventTypeId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setParentCalendarEventId( $intParentCalendarEventId ) {
		$this->set( 'm_intParentCalendarEventId', CStrings::strToIntDef( $intParentCalendarEventId, NULL, false ) );
	}

	public function getParentCalendarEventId() {
		return $this->m_intParentCalendarEventId;
	}

	public function sqlParentCalendarEventId() {
		return ( true == isset( $this->m_intParentCalendarEventId ) ) ? ( string ) $this->m_intParentCalendarEventId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->set( 'm_intLeaseCustomerId', CStrings::strToIntDef( $intLeaseCustomerId, NULL, false ) );
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function sqlLeaseCustomerId() {
		return ( true == isset( $this->m_intLeaseCustomerId ) ) ? ( string ) $this->m_intLeaseCustomerId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NOW()';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setScheduleDetails( $strScheduleDetails ) {
		$this->set( 'm_strScheduleDetails', CStrings::strTrimDef( $strScheduleDetails, -1, NULL, true ) );
	}

	public function getScheduleDetails() {
		return $this->m_strScheduleDetails;
	}

	public function sqlScheduleDetails() {
		return ( true == isset( $this->m_strScheduleDetails ) ) ? '\'' . addslashes( $this->m_strScheduleDetails ) . '\'' : 'NULL';
	}

	public function setEventDetails( $strEventDetails ) {
		$this->set( 'm_strEventDetails', CStrings::strTrimDef( $strEventDetails, -1, NULL, true ) );
	}

	public function getEventDetails() {
		return $this->m_strEventDetails;
	}

	public function sqlEventDetails() {
		return ( true == isset( $this->m_strEventDetails ) ) ? '\'' . addslashes( $this->m_strEventDetails ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, calendar_event_category_id, calendar_event_type_id, event_type_id, event_id, parent_calendar_event_id, applicant_id, application_id, lease_id, lease_customer_id, reference_id, title, start_datetime, end_datetime, schedule_details, event_details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlCalendarEventCategoryId() . ', ' .
 						$this->sqlCalendarEventTypeId() . ', ' .
 						$this->sqlEventTypeId() . ', ' .
 						$this->sqlEventId() . ', ' .
 						$this->sqlParentCalendarEventId() . ', ' .
 						$this->sqlApplicantId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlLeaseCustomerId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlScheduleDetails() . ', ' .
 						$this->sqlEventDetails() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_event_category_id = ' . $this->sqlCalendarEventCategoryId() . ','; } elseif( true == array_key_exists( 'CalendarEventCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' calendar_event_category_id = ' . $this->sqlCalendarEventCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_event_type_id = ' . $this->sqlCalendarEventTypeId() . ','; } elseif( true == array_key_exists( 'CalendarEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' calendar_event_type_id = ' . $this->sqlCalendarEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_calendar_event_id = ' . $this->sqlParentCalendarEventId() . ','; } elseif( true == array_key_exists( 'ParentCalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' parent_calendar_event_id = ' . $this->sqlParentCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() . ','; } elseif( true == array_key_exists( 'LeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails() . ','; } elseif( true == array_key_exists( 'ScheduleDetails', $this->getChangedColumns() ) ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_details = ' . $this->sqlEventDetails() . ','; } elseif( true == array_key_exists( 'EventDetails', $this->getChangedColumns() ) ) { $strSql .= ' event_details = ' . $this->sqlEventDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'calendar_event_category_id' => $this->getCalendarEventCategoryId(),
			'calendar_event_type_id' => $this->getCalendarEventTypeId(),
			'event_type_id' => $this->getEventTypeId(),
			'event_id' => $this->getEventId(),
			'parent_calendar_event_id' => $this->getParentCalendarEventId(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'lease_id' => $this->getLeaseId(),
			'lease_customer_id' => $this->getLeaseCustomerId(),
			'reference_id' => $this->getReferenceId(),
			'title' => $this->getTitle(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'schedule_details' => $this->getScheduleDetails(),
			'event_details' => $this->getEventDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>