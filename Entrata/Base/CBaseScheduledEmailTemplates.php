<?php

class CBaseScheduledEmailTemplates extends CEosPluralBase {

	/**
	 * @return CScheduledEmailTemplate[]
	 */
	public static function fetchScheduledEmailTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScheduledEmailTemplate', $objDatabase );
	}

	/**
	 * @return CScheduledEmailTemplate
	 */
	public static function fetchScheduledEmailTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledEmailTemplate', $objDatabase );
	}

	public static function fetchScheduledEmailTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_email_templates', $objDatabase );
	}

	public static function fetchScheduledEmailTemplateById( $intId, $objDatabase ) {
		return self::fetchScheduledEmailTemplate( sprintf( 'SELECT * FROM scheduled_email_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>