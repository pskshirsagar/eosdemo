<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeDepartments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployeeDepartments extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeDepartment[]
	 */
	public static function fetchCompanyEmployeeDepartments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyEmployeeDepartment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployeeDepartment
	 */
	public static function fetchCompanyEmployeeDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyEmployeeDepartment', $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_departments', $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeDepartment( sprintf( 'SELECT * FROM company_employee_departments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeDepartments( sprintf( 'SELECT * FROM company_employee_departments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeDepartments( sprintf( 'SELECT * FROM company_employee_departments WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentsByCompanyDepartmentIdByCid( $intCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeDepartments( sprintf( 'SELECT * FROM company_employee_departments WHERE company_department_id = %d AND cid = %d', ( int ) $intCompanyDepartmentId, ( int ) $intCid ), $objDatabase );
	}

}
?>