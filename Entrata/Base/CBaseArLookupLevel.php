<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArLookupLevel extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_lookup_levels';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intArLookupTableId;
	protected $m_intMinItemCount;
	protected $m_intMaxItemCount;
	protected $m_fltRateAmount;
	protected $m_fltRatePercent;
	protected $m_boolIsDefault;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMinItemCount = '0';
		$this->m_intMaxItemCount = '0';
		$this->m_fltRateAmount = '0';
		$this->m_boolIsDefault = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ar_lookup_table_id'] ) && $boolDirectSet ) $this->set( 'm_intArLookupTableId', trim( $arrValues['ar_lookup_table_id'] ) ); elseif( isset( $arrValues['ar_lookup_table_id'] ) ) $this->setArLookupTableId( $arrValues['ar_lookup_table_id'] );
		if( isset( $arrValues['min_item_count'] ) && $boolDirectSet ) $this->set( 'm_intMinItemCount', trim( $arrValues['min_item_count'] ) ); elseif( isset( $arrValues['min_item_count'] ) ) $this->setMinItemCount( $arrValues['min_item_count'] );
		if( isset( $arrValues['max_item_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxItemCount', trim( $arrValues['max_item_count'] ) ); elseif( isset( $arrValues['max_item_count'] ) ) $this->setMaxItemCount( $arrValues['max_item_count'] );
		if( isset( $arrValues['rate_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRateAmount', trim( $arrValues['rate_amount'] ) ); elseif( isset( $arrValues['rate_amount'] ) ) $this->setRateAmount( $arrValues['rate_amount'] );
		if( isset( $arrValues['rate_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRatePercent', trim( $arrValues['rate_percent'] ) ); elseif( isset( $arrValues['rate_percent'] ) ) $this->setRatePercent( $arrValues['rate_percent'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setArLookupTableId( $intArLookupTableId ) {
		$this->set( 'm_intArLookupTableId', CStrings::strToIntDef( $intArLookupTableId, NULL, false ) );
	}

	public function getArLookupTableId() {
		return $this->m_intArLookupTableId;
	}

	public function sqlArLookupTableId() {
		return ( true == isset( $this->m_intArLookupTableId ) ) ? ( string ) $this->m_intArLookupTableId : 'NULL';
	}

	public function setMinItemCount( $intMinItemCount ) {
		$this->set( 'm_intMinItemCount', CStrings::strToIntDef( $intMinItemCount, NULL, false ) );
	}

	public function getMinItemCount() {
		return $this->m_intMinItemCount;
	}

	public function sqlMinItemCount() {
		return ( true == isset( $this->m_intMinItemCount ) ) ? ( string ) $this->m_intMinItemCount : '0';
	}

	public function setMaxItemCount( $intMaxItemCount ) {
		$this->set( 'm_intMaxItemCount', CStrings::strToIntDef( $intMaxItemCount, NULL, false ) );
	}

	public function getMaxItemCount() {
		return $this->m_intMaxItemCount;
	}

	public function sqlMaxItemCount() {
		return ( true == isset( $this->m_intMaxItemCount ) ) ? ( string ) $this->m_intMaxItemCount : '0';
	}

	public function setRateAmount( $fltRateAmount ) {
		$this->set( 'm_fltRateAmount', CStrings::strToFloatDef( $fltRateAmount, NULL, false, 2 ) );
	}

	public function getRateAmount() {
		return $this->m_fltRateAmount;
	}

	public function sqlRateAmount() {
		return ( true == isset( $this->m_fltRateAmount ) ) ? ( string ) $this->m_fltRateAmount : '0';
	}

	public function setRatePercent( $fltRatePercent ) {
		$this->set( 'm_fltRatePercent', CStrings::strToFloatDef( $fltRatePercent, NULL, false, 6 ) );
	}

	public function getRatePercent() {
		return $this->m_fltRatePercent;
	}

	public function sqlRatePercent() {
		return ( true == isset( $this->m_fltRatePercent ) ) ? ( string ) $this->m_fltRatePercent : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ar_lookup_table_id, min_item_count, max_item_count, rate_amount, rate_percent, is_default, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlArLookupTableId() . ', ' .
 						$this->sqlMinItemCount() . ', ' .
 						$this->sqlMaxItemCount() . ', ' .
 						$this->sqlRateAmount() . ', ' .
 						$this->sqlRatePercent() . ', ' .
 						$this->sqlIsDefault() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_lookup_table_id = ' . $this->sqlArLookupTableId() . ','; } elseif( true == array_key_exists( 'ArLookupTableId', $this->getChangedColumns() ) ) { $strSql .= ' ar_lookup_table_id = ' . $this->sqlArLookupTableId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_item_count = ' . $this->sqlMinItemCount() . ','; } elseif( true == array_key_exists( 'MinItemCount', $this->getChangedColumns() ) ) { $strSql .= ' min_item_count = ' . $this->sqlMinItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; } elseif( true == array_key_exists( 'MaxItemCount', $this->getChangedColumns() ) ) { $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_amount = ' . $this->sqlRateAmount() . ','; } elseif( true == array_key_exists( 'RateAmount', $this->getChangedColumns() ) ) { $strSql .= ' rate_amount = ' . $this->sqlRateAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_percent = ' . $this->sqlRatePercent() . ','; } elseif( true == array_key_exists( 'RatePercent', $this->getChangedColumns() ) ) { $strSql .= ' rate_percent = ' . $this->sqlRatePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ar_lookup_table_id' => $this->getArLookupTableId(),
			'min_item_count' => $this->getMinItemCount(),
			'max_item_count' => $this->getMaxItemCount(),
			'rate_amount' => $this->getRateAmount(),
			'rate_percent' => $this->getRatePercent(),
			'is_default' => $this->getIsDefault(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>