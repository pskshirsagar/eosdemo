<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 *
 * @FIXME: These classes are temporarily manually edited for i18n release deadline.
 *          Please talk to ssontakke@entrata.com if you need to regenerate this class.
 */
class CBaseApplicantAddress extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.applicant_addresses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intAddressTypeId;
	protected $m_intCompanyResidenceTypeId;
	protected $m_intTimeZoneId;
	protected $m_intMoveOutReasonListItemId;
	protected $m_strCommunityName;
	protected $m_strReasonForLeaving;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_fltMonthlyPaymentAmount;
	protected $m_strPaymentRecipient;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strCounty;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_boolIsVerified;
	protected $m_intIsOwner;
	protected $m_intIsNonUsAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVerified = false;
		$this->m_intIsNonUsAddress = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strCounty' => 'dependentLocality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['address_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddressTypeId', trim( $arrValues['address_type_id'] ) ); elseif( isset( $arrValues['address_type_id'] ) ) $this->setAddressTypeId( $arrValues['address_type_id'] );
		if( isset( $arrValues['company_residence_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyResidenceTypeId', trim( $arrValues['company_residence_type_id'] ) ); elseif( isset( $arrValues['company_residence_type_id'] ) ) $this->setCompanyResidenceTypeId( $arrValues['company_residence_type_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['move_out_reason_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutReasonListItemId', trim( $arrValues['move_out_reason_list_item_id'] ) ); elseif( isset( $arrValues['move_out_reason_list_item_id'] ) ) $this->setMoveOutReasonListItemId( $arrValues['move_out_reason_list_item_id'] );
		if( isset( $arrValues['community_name'] ) && $boolDirectSet ) $this->set( 'm_strCommunityName', trim( stripcslashes( $arrValues['community_name'] ) ) ); elseif( isset( $arrValues['community_name'] ) ) $this->setCommunityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['community_name'] ) : $arrValues['community_name'] );
		if( isset( $arrValues['reason_for_leaving'] ) && $boolDirectSet ) $this->set( 'm_strReasonForLeaving', trim( stripcslashes( $arrValues['reason_for_leaving'] ) ) ); elseif( isset( $arrValues['reason_for_leaving'] ) ) $this->setReasonForLeaving( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_for_leaving'] ) : $arrValues['reason_for_leaving'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['monthly_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyPaymentAmount', trim( $arrValues['monthly_payment_amount'] ) ); elseif( isset( $arrValues['monthly_payment_amount'] ) ) $this->setMonthlyPaymentAmount( $arrValues['monthly_payment_amount'] );
		if( isset( $arrValues['payment_recipient'] ) && $boolDirectSet ) $this->set( 'm_strPaymentRecipient', trim( stripcslashes( $arrValues['payment_recipient'] ) ) ); elseif( isset( $arrValues['payment_recipient'] ) ) $this->setPaymentRecipient( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_recipient'] ) : $arrValues['payment_recipient'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['county'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCounty', trim( stripcslashes( $arrValues['county'] ) ) ); elseif( isset( $arrValues['county'] ) ) $this->setCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county'] ) : $arrValues['county'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerified', trim( stripcslashes( $arrValues['is_verified'] ) ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
		if( isset( $arrValues['is_owner'] ) && $boolDirectSet ) $this->set( 'm_intIsOwner', trim( $arrValues['is_owner'] ) ); elseif( isset( $arrValues['is_owner'] ) ) $this->setIsOwner( $arrValues['is_owner'] );
		if( isset( $arrValues['is_non_us_address'] ) && $boolDirectSet ) $this->set( 'm_intIsNonUsAddress', trim( $arrValues['is_non_us_address'] ) ); elseif( isset( $arrValues['is_non_us_address'] ) ) $this->setIsNonUsAddress( $arrValues['is_non_us_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );

		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->set( 'm_intAddressTypeId', CStrings::strToIntDef( $intAddressTypeId, NULL, false ) );
	}

	public function getAddressTypeId() {
		return $this->m_intAddressTypeId;
	}

	public function sqlAddressTypeId() {
		return ( true == isset( $this->m_intAddressTypeId ) ) ? ( string ) $this->m_intAddressTypeId : 'NULL';
	}

	public function setCompanyResidenceTypeId( $intCompanyResidenceTypeId ) {
		$this->set( 'm_intCompanyResidenceTypeId', CStrings::strToIntDef( $intCompanyResidenceTypeId, NULL, false ) );
	}

	public function getCompanyResidenceTypeId() {
		return $this->m_intCompanyResidenceTypeId;
	}

	public function sqlCompanyResidenceTypeId() {
		return ( true == isset( $this->m_intCompanyResidenceTypeId ) ) ? ( string ) $this->m_intCompanyResidenceTypeId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setMoveOutReasonListItemId( $intMoveOutReasonListItemId ) {
		$this->set( 'm_intMoveOutReasonListItemId', CStrings::strToIntDef( $intMoveOutReasonListItemId, NULL, false ) );
	}

	public function getMoveOutReasonListItemId() {
		return $this->m_intMoveOutReasonListItemId;
	}

	public function sqlMoveOutReasonListItemId() {
		return ( true == isset( $this->m_intMoveOutReasonListItemId ) ) ? ( string ) $this->m_intMoveOutReasonListItemId : 'NULL';
	}

	public function setCommunityName( $strCommunityName ) {
		$this->set( 'm_strCommunityName', CStrings::strTrimDef( $strCommunityName, 50, NULL, true ) );
	}

	public function getCommunityName() {
		return $this->m_strCommunityName;
	}

	public function sqlCommunityName() {
		return ( true == isset( $this->m_strCommunityName ) ) ? '\'' . addslashes( $this->m_strCommunityName ) . '\'' : 'NULL';
	}

	public function setReasonForLeaving( $strReasonForLeaving ) {
		$this->set( 'm_strReasonForLeaving', CStrings::strTrimDef( $strReasonForLeaving, 240, NULL, true ) );
	}

	public function getReasonForLeaving() {
		return $this->m_strReasonForLeaving;
	}

	public function sqlReasonForLeaving() {
		return ( true == isset( $this->m_strReasonForLeaving ) ) ? '\'' . addslashes( $this->m_strReasonForLeaving ) . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setMonthlyPaymentAmount( $fltMonthlyPaymentAmount ) {
		$this->set( 'm_fltMonthlyPaymentAmount', CStrings::strToFloatDef( $fltMonthlyPaymentAmount, NULL, false, 2 ) );
	}

	public function getMonthlyPaymentAmount() {
		return $this->m_fltMonthlyPaymentAmount;
	}

	public function sqlMonthlyPaymentAmount() {
		return ( true == isset( $this->m_fltMonthlyPaymentAmount ) ) ? ( string ) $this->m_fltMonthlyPaymentAmount : 'NULL';
	}

	public function setPaymentRecipient( $strPaymentRecipient ) {
		$this->set( 'm_strPaymentRecipient', CStrings::strTrimDef( $strPaymentRecipient, 50, NULL, true ) );
	}

	public function getPaymentRecipient() {
		return $this->m_strPaymentRecipient;
	}

	public function sqlPaymentRecipient() {
		return ( true == isset( $this->m_strPaymentRecipient ) ) ? '\'' . addslashes( $this->m_strPaymentRecipient ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setCounty( $strCounty ) {
		$this->setPostalAddressField( 'dependentLocality', $strCounty, $strAddressKey = 'default', 'm_strCounty' );
	}

	public function getCounty() {
		return $this->getPostalAddressField( 'dependentLocality', 'default', 'm_strCounty' );
	}

	public function sqlCounty() {
		return ( true == isset( $this->m_strCounty ) ) ? '\'' . addslashes( $this->m_strCounty ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->set( 'm_boolIsVerified', CStrings::strToBool( $boolIsVerified ) );
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOwner( $intIsOwner ) {
		$this->set( 'm_intIsOwner', CStrings::strToIntDef( $intIsOwner, NULL, false ) );
	}

	public function getIsOwner() {
		return $this->m_intIsOwner;
	}

	public function sqlIsOwner() {
		return ( true == isset( $this->m_intIsOwner ) ) ? ( string ) $this->m_intIsOwner : 'NULL';
	}

	public function setIsNonUsAddress( $intIsNonUsAddress ) {
		$this->set( 'm_intIsNonUsAddress', CStrings::strToIntDef( $intIsNonUsAddress, NULL, false ) );
	}

	public function getIsNonUsAddress() {
		return $this->m_intIsNonUsAddress;
	}

	public function sqlIsNonUsAddress() {
		return ( true == isset( $this->m_intIsNonUsAddress ) ) ? ( string ) $this->m_intIsNonUsAddress : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, address_type_id, company_residence_type_id, time_zone_id, move_out_reason_list_item_id, community_name, reason_for_leaving, move_in_date, move_out_date, monthly_payment_amount, payment_recipient, street_line1, street_line2, street_line3, city, county, state_code, province, postal_code, country_code, is_verified, is_owner, is_non_us_address, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlAddressTypeId() . ', ' .
						$this->sqlCompanyResidenceTypeId() . ', ' .
						$this->sqlTimeZoneId() . ', ' .
						$this->sqlMoveOutReasonListItemId() . ', ' .
						$this->sqlCommunityName() . ', ' .
						$this->sqlReasonForLeaving() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlMonthlyPaymentAmount() . ', ' .
						$this->sqlPaymentRecipient() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlStreetLine2() . ', ' .
						$this->sqlStreetLine3() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlCounty() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlIsVerified() . ', ' .
						$this->sqlIsOwner() . ', ' .
						$this->sqlIsNonUsAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; } elseif( true == array_key_exists( 'AddressTypeId', $this->getChangedColumns() ) ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_residence_type_id = ' . $this->sqlCompanyResidenceTypeId() . ','; } elseif( true == array_key_exists( 'CompanyResidenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_residence_type_id = ' . $this->sqlCompanyResidenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId() . ','; } elseif( true == array_key_exists( 'MoveOutReasonListItemId', $this->getChangedColumns() ) ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' community_name = ' . $this->sqlCommunityName() . ','; } elseif( true == array_key_exists( 'CommunityName', $this->getChangedColumns() ) ) { $strSql .= ' community_name = ' . $this->sqlCommunityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; } elseif( true == array_key_exists( 'ReasonForLeaving', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount() . ','; } elseif( true == array_key_exists( 'MonthlyPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_recipient = ' . $this->sqlPaymentRecipient() . ','; } elseif( true == array_key_exists( 'PaymentRecipient', $this->getChangedColumns() ) ) { $strSql .= ' payment_recipient = ' . $this->sqlPaymentRecipient() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; } elseif( true == array_key_exists( 'County', $this->getChangedColumns() ) ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_owner = ' . $this->sqlIsOwner() . ','; } elseif( true == array_key_exists( 'IsOwner', $this->getChangedColumns() ) ) { $strSql .= ' is_owner = ' . $this->sqlIsOwner() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_non_us_address = ' . $this->sqlIsNonUsAddress() . ','; } elseif( true == array_key_exists( 'IsNonUsAddress', $this->getChangedColumns() ) ) { $strSql .= ' is_non_us_address = ' . $this->sqlIsNonUsAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'address_type_id' => $this->getAddressTypeId(),
			'company_residence_type_id' => $this->getCompanyResidenceTypeId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'move_out_reason_list_item_id' => $this->getMoveOutReasonListItemId(),
			'community_name' => $this->getCommunityName(),
			'reason_for_leaving' => $this->getReasonForLeaving(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'monthly_payment_amount' => $this->getMonthlyPaymentAmount(),
			'payment_recipient' => $this->getPaymentRecipient(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'county' => $this->getCounty(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'is_verified' => $this->getIsVerified(),
			'is_owner' => $this->getIsOwner(),
			'is_non_us_address' => $this->getIsNonUsAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>