<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitTypeLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitTypeLogs extends CEosPluralBase {

	/**
	 * @return CUnitTypeLog[]
	 */
	public static function fetchUnitTypeLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitTypeLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitTypeLog
	 */
	public static function fetchUnitTypeLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitTypeLog', $objDatabase );
	}

	public static function fetchUnitTypeLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_type_logs', $objDatabase );
	}

	public static function fetchUnitTypeLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLog( sprintf( 'SELECT * FROM unit_type_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByOptimizationStructureIdByCid( $intOptimizationStructureId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE optimization_structure_id = %d AND cid = %d', ( int ) $intOptimizationStructureId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByUnitExclusionReasonTypeIdByCid( $intUnitExclusionReasonTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE unit_exclusion_reason_type_id = %d AND cid = %d', ( int ) $intUnitExclusionReasonTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeLogs( sprintf( 'SELECT * FROM unit_type_logs WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>