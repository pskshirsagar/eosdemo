<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectContracts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjectContracts extends CEosPluralBase {

	/**
	 * @return CJobProjectContract[]
	 */
	public static function fetchJobProjectContracts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobProjectContract', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobProjectContract
	 */
	public static function fetchJobProjectContract( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobProjectContract', $objDatabase );
	}

	public static function fetchJobProjectContractCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_project_contracts', $objDatabase );
	}

	public static function fetchJobProjectContractByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContract( sprintf( 'SELECT * FROM job_project_contracts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractsByCid( $intCid, $objDatabase ) {
		return self::fetchJobProjectContracts( sprintf( 'SELECT * FROM job_project_contracts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractsByJobProjectIdByCid( $intJobProjectId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContracts( sprintf( 'SELECT * FROM job_project_contracts WHERE job_project_id = %d AND cid = %d', ( int ) $intJobProjectId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractsByContractIdByCid( $intContractId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContracts( sprintf( 'SELECT * FROM job_project_contracts WHERE contract_id = %d AND cid = %d', ( int ) $intContractId, ( int ) $intCid ), $objDatabase );
	}

}
?>