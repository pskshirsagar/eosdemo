<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenancePriorities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenancePriorities extends CEosPluralBase {

	/**
	 * @return CMaintenancePriority[]
	 */
	public static function fetchMaintenancePriorities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenancePriority::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenancePriority
	 */
	public static function fetchMaintenancePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenancePriority::class, $objDatabase );
	}

	public static function fetchMaintenancePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_priorities', $objDatabase );
	}

	public static function fetchMaintenancePriorityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenancePriority( sprintf( 'SELECT * FROM maintenance_priorities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenancePrioritiesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenancePriorities( sprintf( 'SELECT * FROM maintenance_priorities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenancePrioritiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchMaintenancePriorities( sprintf( 'SELECT * FROM maintenance_priorities WHERE integration_database_id = %d AND cid = %d', $intIntegrationDatabaseId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenancePrioritiesByMaintenancePriorityTypeIdByCid( $intMaintenancePriorityTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenancePriorities( sprintf( 'SELECT * FROM maintenance_priorities WHERE maintenance_priority_type_id = %d AND cid = %d', $intMaintenancePriorityTypeId, $intCid ), $objDatabase );
	}

}
?>