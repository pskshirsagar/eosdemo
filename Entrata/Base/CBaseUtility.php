<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUtility extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.utilities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityTypeId;
	protected $m_intCategorizationTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsCategorizationQueued;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intCategorizedBy;
	protected $m_strCategorizedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMappingId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intUtilityTypeId = '1';
		$this->m_intCategorizationTypeId = '3';
		$this->m_boolIsCategorizationQueued = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_categorization_queued'] ) && $boolDirectSet ) $this->set( 'm_boolIsCategorizationQueued', trim( stripcslashes( $arrValues['is_categorization_queued'] ) ) ); elseif( isset( $arrValues['is_categorization_queued'] ) ) $this->setIsCategorizationQueued( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_categorization_queued'] ) : $arrValues['is_categorization_queued'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['categorized_by'] ) && $boolDirectSet ) $this->set( 'm_intCategorizedBy', trim( $arrValues['categorized_by'] ) ); elseif( isset( $arrValues['categorized_by'] ) ) $this->setCategorizedBy( $arrValues['categorized_by'] );
		if( isset( $arrValues['categorized_on'] ) && $boolDirectSet ) $this->set( 'm_strCategorizedOn', trim( $arrValues['categorized_on'] ) ); elseif( isset( $arrValues['categorized_on'] ) ) $this->setCategorizedOn( $arrValues['categorized_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingId', trim( $arrValues['mapping_id'] ) ); elseif( isset( $arrValues['mapping_id'] ) ) $this->setMappingId( $arrValues['mapping_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : '1';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '3';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 10, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsCategorizationQueued( $boolIsCategorizationQueued ) {
		$this->set( 'm_boolIsCategorizationQueued', CStrings::strToBool( $boolIsCategorizationQueued ) );
	}

	public function getIsCategorizationQueued() {
		return $this->m_boolIsCategorizationQueued;
	}

	public function sqlIsCategorizationQueued() {
		return ( true == isset( $this->m_boolIsCategorizationQueued ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCategorizationQueued ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCategorizedBy( $intCategorizedBy ) {
		$this->set( 'm_intCategorizedBy', CStrings::strToIntDef( $intCategorizedBy, NULL, false ) );
	}

	public function getCategorizedBy() {
		return $this->m_intCategorizedBy;
	}

	public function sqlCategorizedBy() {
		return ( true == isset( $this->m_intCategorizedBy ) ) ? ( string ) $this->m_intCategorizedBy : 'NULL';
	}

	public function setCategorizedOn( $strCategorizedOn ) {
		$this->set( 'm_strCategorizedOn', CStrings::strTrimDef( $strCategorizedOn, -1, NULL, true ) );
	}

	public function getCategorizedOn() {
		return $this->m_strCategorizedOn;
	}

	public function sqlCategorizedOn() {
		return ( true == isset( $this->m_strCategorizedOn ) ) ? '\'' . $this->m_strCategorizedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMappingId( $intMappingId ) {
		$this->set( 'm_intMappingId', CStrings::strToIntDef( $intMappingId, NULL, false ) );
	}

	public function getMappingId() {
		return $this->m_intMappingId;
	}

	public function sqlMappingId() {
		return ( true == isset( $this->m_intMappingId ) ) ? ( string ) $this->m_intMappingId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_type_id, categorization_type_id, remote_primary_key, name, description, is_categorization_queued, is_published, order_num, categorized_by, categorized_on, updated_by, updated_on, created_by, created_on, mapping_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUtilityTypeId() . ', ' .
						$this->sqlCategorizationTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsCategorizationQueued() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCategorizedBy() . ', ' .
						$this->sqlCategorizedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMappingId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId(). ',' ; } elseif( true == array_key_exists( 'CategorizationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued(). ',' ; } elseif( true == array_key_exists( 'IsCategorizationQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_categorization_queued = ' . $this->sqlIsCategorizationQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorized_by = ' . $this->sqlCategorizedBy(). ',' ; } elseif( true == array_key_exists( 'CategorizedBy', $this->getChangedColumns() ) ) { $strSql .= ' categorized_by = ' . $this->sqlCategorizedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorized_on = ' . $this->sqlCategorizedOn(). ',' ; } elseif( true == array_key_exists( 'CategorizedOn', $this->getChangedColumns() ) ) { $strSql .= ' categorized_on = ' . $this->sqlCategorizedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId(). ',' ; } elseif( true == array_key_exists( 'MappingId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_categorization_queued' => $this->getIsCategorizationQueued(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'categorized_by' => $this->getCategorizedBy(),
			'categorized_on' => $this->getCategorizedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'mapping_id' => $this->getMappingId(),
			'details' => $this->getDetails()
		);
	}

}
?>