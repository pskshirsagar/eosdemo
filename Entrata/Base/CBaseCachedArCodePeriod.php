<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedArCodePeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_ar_code_periods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPeriodId;
	protected $m_intPropertyId;
	protected $m_intGlGroupTypeId;
	protected $m_intArCodeTypeId;
	protected $m_intArCodeId;
	protected $m_intArOriginId;
	protected $m_intArTriggerTypeId;
	protected $m_intArTriggerId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_strPostMonth;
	protected $m_strArCodeName;
	protected $m_strDebitGlAccountName;
	protected $m_strCreditGlAccountName;
	protected $m_strDebitGlAccountNumber;
	protected $m_strCreditGlAccountNumber;
	protected $m_fltWriteOffAmount;
	protected $m_fltOutOfPeriodAmount;
	protected $m_fltScheduledAmount;
	protected $m_fltAdjustedAmount;
	protected $m_fltTotalCreditAmount;
	protected $m_fltTotalAmount;
	protected $m_fltWriteOffAllocations;
	protected $m_fltOutOfPeriodAllocations;
	protected $m_fltScheduledAllocations;
	protected $m_fltAdjustedAllocations;
	protected $m_fltTotalCurrentCreditAllocations;
	protected $m_fltTotalCreditAllocations;
	protected $m_fltTotalCurrentAllocations;
	protected $m_fltTotalAllocations;
	protected $m_fltCurrentPaymentAllocations;
	protected $m_fltPrePaymentAllocations;
	protected $m_fltPaymentAllocations;
	protected $m_fltPaymentInKindAllocations;
	protected $m_fltRentAllocations;
	protected $m_fltOtherIncomeAllocations;
	protected $m_fltExpenseAllocations;
	protected $m_fltAssetAllocations;
	protected $m_fltEquityAllocations;
	protected $m_fltOtherChargeAllocations;
	protected $m_fltDepositAllocations;
	protected $m_fltDepositCreditAllocations;
	protected $m_fltRefundAllocations;
	protected $m_fltOtherLiabilityAllocations;
	protected $m_fltReceivablesAllocations;
	protected $m_fltPostToCashRentAllocations;
	protected $m_fltPostToCashOtherIncomeAllocations;
	protected $m_fltPostToCashExpenseAllocations;
	protected $m_fltPostToCashAssetAllocations;
	protected $m_fltPostToCashEquityAllocations;
	protected $m_fltPostToCashOtherChargeAllocations;
	protected $m_fltTotalCashBasisAllocations;
	protected $m_fltScheduledChargeTotal;
	protected $m_fltScheduledChargeProratedTotal;
	protected $m_intTransactionCount;
	protected $m_fltAllocationCount;
	protected $m_fltScheduledChargeCount;
	protected $m_intArCodeGroupId;
	protected $m_boolPostToCash;
	protected $m_boolIsPaymentInKind;
	protected $m_boolIsDepositCredit;
	protected $m_intGlAccountTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_fltWriteOffAmount = '0';
		$this->m_fltOutOfPeriodAmount = '0';
		$this->m_fltScheduledAmount = '0';
		$this->m_fltAdjustedAmount = '0';
		$this->m_fltTotalCreditAmount = '0';
		$this->m_fltTotalAmount = '0';
		$this->m_fltWriteOffAllocations = '0';
		$this->m_fltOutOfPeriodAllocations = '0';
		$this->m_fltScheduledAllocations = '0';
		$this->m_fltAdjustedAllocations = '0';
		$this->m_fltTotalCurrentCreditAllocations = '0';
		$this->m_fltTotalCreditAllocations = '0';
		$this->m_fltTotalCurrentAllocations = '0';
		$this->m_fltTotalAllocations = '0';
		$this->m_fltCurrentPaymentAllocations = '0';
		$this->m_fltPrePaymentAllocations = '0';
		$this->m_fltPaymentAllocations = '0';
		$this->m_fltPaymentInKindAllocations = '0';
		$this->m_fltRentAllocations = '0';
		$this->m_fltOtherIncomeAllocations = '0';
		$this->m_fltExpenseAllocations = '0';
		$this->m_fltAssetAllocations = '0';
		$this->m_fltEquityAllocations = '0';
		$this->m_fltOtherChargeAllocations = '0';
		$this->m_fltDepositAllocations = '0';
		$this->m_fltDepositCreditAllocations = '0';
		$this->m_fltRefundAllocations = '0';
		$this->m_fltOtherLiabilityAllocations = '0';
		$this->m_fltReceivablesAllocations = '0';
		$this->m_fltPostToCashRentAllocations = '0';
		$this->m_fltPostToCashOtherIncomeAllocations = '0';
		$this->m_fltPostToCashExpenseAllocations = '0';
		$this->m_fltPostToCashAssetAllocations = '0';
		$this->m_fltPostToCashEquityAllocations = '0';
		$this->m_fltPostToCashOtherChargeAllocations = '0';
		$this->m_fltTotalCashBasisAllocations = '0';
		$this->m_fltScheduledChargeTotal = '0';
		$this->m_fltScheduledChargeProratedTotal = '0';
		$this->m_intTransactionCount = '0';
		$this->m_fltAllocationCount = '0';
		$this->m_fltScheduledChargeCount = '0';
		$this->m_boolPostToCash = false;
		$this->m_boolIsPaymentInKind = false;
		$this->m_boolIsDepositCredit = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_trigger_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerTypeId', trim( $arrValues['ar_trigger_type_id'] ) ); elseif( isset( $arrValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrValues['ar_trigger_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['ar_code_name'] ) && $boolDirectSet ) $this->set( 'm_strArCodeName', trim( $arrValues['ar_code_name'] ) ); elseif( isset( $arrValues['ar_code_name'] ) ) $this->setArCodeName( $arrValues['ar_code_name'] );
		if( isset( $arrValues['debit_gl_account_name'] ) && $boolDirectSet ) $this->set( 'm_strDebitGlAccountName', trim( $arrValues['debit_gl_account_name'] ) ); elseif( isset( $arrValues['debit_gl_account_name'] ) ) $this->setDebitGlAccountName( $arrValues['debit_gl_account_name'] );
		if( isset( $arrValues['credit_gl_account_name'] ) && $boolDirectSet ) $this->set( 'm_strCreditGlAccountName', trim( $arrValues['credit_gl_account_name'] ) ); elseif( isset( $arrValues['credit_gl_account_name'] ) ) $this->setCreditGlAccountName( $arrValues['credit_gl_account_name'] );
		if( isset( $arrValues['debit_gl_account_number'] ) && $boolDirectSet ) $this->set( 'm_strDebitGlAccountNumber', trim( $arrValues['debit_gl_account_number'] ) ); elseif( isset( $arrValues['debit_gl_account_number'] ) ) $this->setDebitGlAccountNumber( $arrValues['debit_gl_account_number'] );
		if( isset( $arrValues['credit_gl_account_number'] ) && $boolDirectSet ) $this->set( 'm_strCreditGlAccountNumber', trim( $arrValues['credit_gl_account_number'] ) ); elseif( isset( $arrValues['credit_gl_account_number'] ) ) $this->setCreditGlAccountNumber( $arrValues['credit_gl_account_number'] );
		if( isset( $arrValues['write_off_amount'] ) && $boolDirectSet ) $this->set( 'm_fltWriteOffAmount', trim( $arrValues['write_off_amount'] ) ); elseif( isset( $arrValues['write_off_amount'] ) ) $this->setWriteOffAmount( $arrValues['write_off_amount'] );
		if( isset( $arrValues['out_of_period_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOutOfPeriodAmount', trim( $arrValues['out_of_period_amount'] ) ); elseif( isset( $arrValues['out_of_period_amount'] ) ) $this->setOutOfPeriodAmount( $arrValues['out_of_period_amount'] );
		if( isset( $arrValues['scheduled_amount'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledAmount', trim( $arrValues['scheduled_amount'] ) ); elseif( isset( $arrValues['scheduled_amount'] ) ) $this->setScheduledAmount( $arrValues['scheduled_amount'] );
		if( isset( $arrValues['adjusted_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustedAmount', trim( $arrValues['adjusted_amount'] ) ); elseif( isset( $arrValues['adjusted_amount'] ) ) $this->setAdjustedAmount( $arrValues['adjusted_amount'] );
		if( isset( $arrValues['total_credit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCreditAmount', trim( $arrValues['total_credit_amount'] ) ); elseif( isset( $arrValues['total_credit_amount'] ) ) $this->setTotalCreditAmount( $arrValues['total_credit_amount'] );
		if( isset( $arrValues['total_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmount', trim( $arrValues['total_amount'] ) ); elseif( isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
		if( isset( $arrValues['write_off_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltWriteOffAllocations', trim( $arrValues['write_off_allocations'] ) ); elseif( isset( $arrValues['write_off_allocations'] ) ) $this->setWriteOffAllocations( $arrValues['write_off_allocations'] );
		if( isset( $arrValues['out_of_period_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltOutOfPeriodAllocations', trim( $arrValues['out_of_period_allocations'] ) ); elseif( isset( $arrValues['out_of_period_allocations'] ) ) $this->setOutOfPeriodAllocations( $arrValues['out_of_period_allocations'] );
		if( isset( $arrValues['scheduled_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledAllocations', trim( $arrValues['scheduled_allocations'] ) ); elseif( isset( $arrValues['scheduled_allocations'] ) ) $this->setScheduledAllocations( $arrValues['scheduled_allocations'] );
		if( isset( $arrValues['adjusted_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustedAllocations', trim( $arrValues['adjusted_allocations'] ) ); elseif( isset( $arrValues['adjusted_allocations'] ) ) $this->setAdjustedAllocations( $arrValues['adjusted_allocations'] );
		if( isset( $arrValues['total_current_credit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCurrentCreditAllocations', trim( $arrValues['total_current_credit_allocations'] ) ); elseif( isset( $arrValues['total_current_credit_allocations'] ) ) $this->setTotalCurrentCreditAllocations( $arrValues['total_current_credit_allocations'] );
		if( isset( $arrValues['total_credit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCreditAllocations', trim( $arrValues['total_credit_allocations'] ) ); elseif( isset( $arrValues['total_credit_allocations'] ) ) $this->setTotalCreditAllocations( $arrValues['total_credit_allocations'] );
		if( isset( $arrValues['total_current_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCurrentAllocations', trim( $arrValues['total_current_allocations'] ) ); elseif( isset( $arrValues['total_current_allocations'] ) ) $this->setTotalCurrentAllocations( $arrValues['total_current_allocations'] );
		if( isset( $arrValues['total_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAllocations', trim( $arrValues['total_allocations'] ) ); elseif( isset( $arrValues['total_allocations'] ) ) $this->setTotalAllocations( $arrValues['total_allocations'] );
		if( isset( $arrValues['current_payment_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentPaymentAllocations', trim( $arrValues['current_payment_allocations'] ) ); elseif( isset( $arrValues['current_payment_allocations'] ) ) $this->setCurrentPaymentAllocations( $arrValues['current_payment_allocations'] );
		if( isset( $arrValues['pre_payment_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPrePaymentAllocations', trim( $arrValues['pre_payment_allocations'] ) ); elseif( isset( $arrValues['pre_payment_allocations'] ) ) $this->setPrePaymentAllocations( $arrValues['pre_payment_allocations'] );
		if( isset( $arrValues['payment_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAllocations', trim( $arrValues['payment_allocations'] ) ); elseif( isset( $arrValues['payment_allocations'] ) ) $this->setPaymentAllocations( $arrValues['payment_allocations'] );
		if( isset( $arrValues['payment_in_kind_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentInKindAllocations', trim( $arrValues['payment_in_kind_allocations'] ) ); elseif( isset( $arrValues['payment_in_kind_allocations'] ) ) $this->setPaymentInKindAllocations( $arrValues['payment_in_kind_allocations'] );
		if( isset( $arrValues['rent_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocations', trim( $arrValues['rent_allocations'] ) ); elseif( isset( $arrValues['rent_allocations'] ) ) $this->setRentAllocations( $arrValues['rent_allocations'] );
		if( isset( $arrValues['other_income_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeAllocations', trim( $arrValues['other_income_allocations'] ) ); elseif( isset( $arrValues['other_income_allocations'] ) ) $this->setOtherIncomeAllocations( $arrValues['other_income_allocations'] );
		if( isset( $arrValues['expense_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltExpenseAllocations', trim( $arrValues['expense_allocations'] ) ); elseif( isset( $arrValues['expense_allocations'] ) ) $this->setExpenseAllocations( $arrValues['expense_allocations'] );
		if( isset( $arrValues['asset_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltAssetAllocations', trim( $arrValues['asset_allocations'] ) ); elseif( isset( $arrValues['asset_allocations'] ) ) $this->setAssetAllocations( $arrValues['asset_allocations'] );
		if( isset( $arrValues['equity_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltEquityAllocations', trim( $arrValues['equity_allocations'] ) ); elseif( isset( $arrValues['equity_allocations'] ) ) $this->setEquityAllocations( $arrValues['equity_allocations'] );
		if( isset( $arrValues['other_charge_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltOtherChargeAllocations', trim( $arrValues['other_charge_allocations'] ) ); elseif( isset( $arrValues['other_charge_allocations'] ) ) $this->setOtherChargeAllocations( $arrValues['other_charge_allocations'] );
		if( isset( $arrValues['deposit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltDepositAllocations', trim( $arrValues['deposit_allocations'] ) ); elseif( isset( $arrValues['deposit_allocations'] ) ) $this->setDepositAllocations( $arrValues['deposit_allocations'] );
		if( isset( $arrValues['deposit_credit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditAllocations', trim( $arrValues['deposit_credit_allocations'] ) ); elseif( isset( $arrValues['deposit_credit_allocations'] ) ) $this->setDepositCreditAllocations( $arrValues['deposit_credit_allocations'] );
		if( isset( $arrValues['refund_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltRefundAllocations', trim( $arrValues['refund_allocations'] ) ); elseif( isset( $arrValues['refund_allocations'] ) ) $this->setRefundAllocations( $arrValues['refund_allocations'] );
		if( isset( $arrValues['other_liability_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltOtherLiabilityAllocations', trim( $arrValues['other_liability_allocations'] ) ); elseif( isset( $arrValues['other_liability_allocations'] ) ) $this->setOtherLiabilityAllocations( $arrValues['other_liability_allocations'] );
		if( isset( $arrValues['receivables_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltReceivablesAllocations', trim( $arrValues['receivables_allocations'] ) ); elseif( isset( $arrValues['receivables_allocations'] ) ) $this->setReceivablesAllocations( $arrValues['receivables_allocations'] );
		if( isset( $arrValues['post_to_cash_rent_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashRentAllocations', trim( $arrValues['post_to_cash_rent_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_rent_allocations'] ) ) $this->setPostToCashRentAllocations( $arrValues['post_to_cash_rent_allocations'] );
		if( isset( $arrValues['post_to_cash_other_income_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashOtherIncomeAllocations', trim( $arrValues['post_to_cash_other_income_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_other_income_allocations'] ) ) $this->setPostToCashOtherIncomeAllocations( $arrValues['post_to_cash_other_income_allocations'] );
		if( isset( $arrValues['post_to_cash_expense_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashExpenseAllocations', trim( $arrValues['post_to_cash_expense_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_expense_allocations'] ) ) $this->setPostToCashExpenseAllocations( $arrValues['post_to_cash_expense_allocations'] );
		if( isset( $arrValues['post_to_cash_asset_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashAssetAllocations', trim( $arrValues['post_to_cash_asset_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_asset_allocations'] ) ) $this->setPostToCashAssetAllocations( $arrValues['post_to_cash_asset_allocations'] );
		if( isset( $arrValues['post_to_cash_equity_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashEquityAllocations', trim( $arrValues['post_to_cash_equity_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_equity_allocations'] ) ) $this->setPostToCashEquityAllocations( $arrValues['post_to_cash_equity_allocations'] );
		if( isset( $arrValues['post_to_cash_other_charge_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltPostToCashOtherChargeAllocations', trim( $arrValues['post_to_cash_other_charge_allocations'] ) ); elseif( isset( $arrValues['post_to_cash_other_charge_allocations'] ) ) $this->setPostToCashOtherChargeAllocations( $arrValues['post_to_cash_other_charge_allocations'] );
		if( isset( $arrValues['total_cash_basis_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCashBasisAllocations', trim( $arrValues['total_cash_basis_allocations'] ) ); elseif( isset( $arrValues['total_cash_basis_allocations'] ) ) $this->setTotalCashBasisAllocations( $arrValues['total_cash_basis_allocations'] );
		if( isset( $arrValues['scheduled_charge_total'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledChargeTotal', trim( $arrValues['scheduled_charge_total'] ) ); elseif( isset( $arrValues['scheduled_charge_total'] ) ) $this->setScheduledChargeTotal( $arrValues['scheduled_charge_total'] );
		if( isset( $arrValues['scheduled_charge_prorated_total'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledChargeProratedTotal', trim( $arrValues['scheduled_charge_prorated_total'] ) ); elseif( isset( $arrValues['scheduled_charge_prorated_total'] ) ) $this->setScheduledChargeProratedTotal( $arrValues['scheduled_charge_prorated_total'] );
		if( isset( $arrValues['transaction_count'] ) && $boolDirectSet ) $this->set( 'm_intTransactionCount', trim( $arrValues['transaction_count'] ) ); elseif( isset( $arrValues['transaction_count'] ) ) $this->setTransactionCount( $arrValues['transaction_count'] );
		if( isset( $arrValues['allocation_count'] ) && $boolDirectSet ) $this->set( 'm_fltAllocationCount', trim( $arrValues['allocation_count'] ) ); elseif( isset( $arrValues['allocation_count'] ) ) $this->setAllocationCount( $arrValues['allocation_count'] );
		if( isset( $arrValues['scheduled_charge_count'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledChargeCount', trim( $arrValues['scheduled_charge_count'] ) ); elseif( isset( $arrValues['scheduled_charge_count'] ) ) $this->setScheduledChargeCount( $arrValues['scheduled_charge_count'] );
		if( isset( $arrValues['ar_code_group_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeGroupId', trim( $arrValues['ar_code_group_id'] ) ); elseif( isset( $arrValues['ar_code_group_id'] ) ) $this->setArCodeGroupId( $arrValues['ar_code_group_id'] );
		if( isset( $arrValues['post_to_cash'] ) && $boolDirectSet ) $this->set( 'm_boolPostToCash', trim( stripcslashes( $arrValues['post_to_cash'] ) ) ); elseif( isset( $arrValues['post_to_cash'] ) ) $this->setPostToCash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_to_cash'] ) : $arrValues['post_to_cash'] );
		if( isset( $arrValues['is_payment_in_kind'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaymentInKind', trim( stripcslashes( $arrValues['is_payment_in_kind'] ) ) ); elseif( isset( $arrValues['is_payment_in_kind'] ) ) $this->setIsPaymentInKind( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payment_in_kind'] ) : $arrValues['is_payment_in_kind'] );
		if( isset( $arrValues['is_deposit_credit'] ) && $boolDirectSet ) $this->set( 'm_boolIsDepositCredit', trim( stripcslashes( $arrValues['is_deposit_credit'] ) ) ); elseif( isset( $arrValues['is_deposit_credit'] ) ) $this->setIsDepositCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deposit_credit'] ) : $arrValues['is_deposit_credit'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function sqlArCodeTypeId() {
		return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->set( 'm_intArTriggerTypeId', CStrings::strToIntDef( $intArTriggerTypeId, NULL, false ) );
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function sqlArTriggerTypeId() {
		return ( true == isset( $this->m_intArTriggerTypeId ) ) ? ( string ) $this->m_intArTriggerTypeId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setArCodeName( $strArCodeName ) {
		$this->set( 'm_strArCodeName', CStrings::strTrimDef( $strArCodeName, -1, NULL, true ) );
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function sqlArCodeName() {
		return ( true == isset( $this->m_strArCodeName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strArCodeName ) : '\'' . addslashes( $this->m_strArCodeName ) . '\'' ) : 'NULL';
	}

	public function setDebitGlAccountName( $strDebitGlAccountName ) {
		$this->set( 'm_strDebitGlAccountName', CStrings::strTrimDef( $strDebitGlAccountName, -1, NULL, true ) );
	}

	public function getDebitGlAccountName() {
		return $this->m_strDebitGlAccountName;
	}

	public function sqlDebitGlAccountName() {
		return ( true == isset( $this->m_strDebitGlAccountName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDebitGlAccountName ) : '\'' . addslashes( $this->m_strDebitGlAccountName ) . '\'' ) : 'NULL';
	}

	public function setCreditGlAccountName( $strCreditGlAccountName ) {
		$this->set( 'm_strCreditGlAccountName', CStrings::strTrimDef( $strCreditGlAccountName, -1, NULL, true ) );
	}

	public function getCreditGlAccountName() {
		return $this->m_strCreditGlAccountName;
	}

	public function sqlCreditGlAccountName() {
		return ( true == isset( $this->m_strCreditGlAccountName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCreditGlAccountName ) : '\'' . addslashes( $this->m_strCreditGlAccountName ) . '\'' ) : 'NULL';
	}

	public function setDebitGlAccountNumber( $strDebitGlAccountNumber ) {
		$this->set( 'm_strDebitGlAccountNumber', CStrings::strTrimDef( $strDebitGlAccountNumber, -1, NULL, true ) );
	}

	public function getDebitGlAccountNumber() {
		return $this->m_strDebitGlAccountNumber;
	}

	public function sqlDebitGlAccountNumber() {
		return ( true == isset( $this->m_strDebitGlAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDebitGlAccountNumber ) : '\'' . addslashes( $this->m_strDebitGlAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setCreditGlAccountNumber( $strCreditGlAccountNumber ) {
		$this->set( 'm_strCreditGlAccountNumber', CStrings::strTrimDef( $strCreditGlAccountNumber, -1, NULL, true ) );
	}

	public function getCreditGlAccountNumber() {
		return $this->m_strCreditGlAccountNumber;
	}

	public function sqlCreditGlAccountNumber() {
		return ( true == isset( $this->m_strCreditGlAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCreditGlAccountNumber ) : '\'' . addslashes( $this->m_strCreditGlAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setWriteOffAmount( $fltWriteOffAmount ) {
		$this->set( 'm_fltWriteOffAmount', CStrings::strToFloatDef( $fltWriteOffAmount, NULL, false, 2 ) );
	}

	public function getWriteOffAmount() {
		return $this->m_fltWriteOffAmount;
	}

	public function sqlWriteOffAmount() {
		return ( true == isset( $this->m_fltWriteOffAmount ) ) ? ( string ) $this->m_fltWriteOffAmount : '0';
	}

	public function setOutOfPeriodAmount( $fltOutOfPeriodAmount ) {
		$this->set( 'm_fltOutOfPeriodAmount', CStrings::strToFloatDef( $fltOutOfPeriodAmount, NULL, false, 2 ) );
	}

	public function getOutOfPeriodAmount() {
		return $this->m_fltOutOfPeriodAmount;
	}

	public function sqlOutOfPeriodAmount() {
		return ( true == isset( $this->m_fltOutOfPeriodAmount ) ) ? ( string ) $this->m_fltOutOfPeriodAmount : '0';
	}

	public function setScheduledAmount( $fltScheduledAmount ) {
		$this->set( 'm_fltScheduledAmount', CStrings::strToFloatDef( $fltScheduledAmount, NULL, false, 2 ) );
	}

	public function getScheduledAmount() {
		return $this->m_fltScheduledAmount;
	}

	public function sqlScheduledAmount() {
		return ( true == isset( $this->m_fltScheduledAmount ) ) ? ( string ) $this->m_fltScheduledAmount : '0';
	}

	public function setAdjustedAmount( $fltAdjustedAmount ) {
		$this->set( 'm_fltAdjustedAmount', CStrings::strToFloatDef( $fltAdjustedAmount, NULL, false, 2 ) );
	}

	public function getAdjustedAmount() {
		return $this->m_fltAdjustedAmount;
	}

	public function sqlAdjustedAmount() {
		return ( true == isset( $this->m_fltAdjustedAmount ) ) ? ( string ) $this->m_fltAdjustedAmount : '0';
	}

	public function setTotalCreditAmount( $fltTotalCreditAmount ) {
		$this->set( 'm_fltTotalCreditAmount', CStrings::strToFloatDef( $fltTotalCreditAmount, NULL, false, 2 ) );
	}

	public function getTotalCreditAmount() {
		return $this->m_fltTotalCreditAmount;
	}

	public function sqlTotalCreditAmount() {
		return ( true == isset( $this->m_fltTotalCreditAmount ) ) ? ( string ) $this->m_fltTotalCreditAmount : '0';
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->set( 'm_fltTotalAmount', CStrings::strToFloatDef( $fltTotalAmount, NULL, false, 2 ) );
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function sqlTotalAmount() {
		return ( true == isset( $this->m_fltTotalAmount ) ) ? ( string ) $this->m_fltTotalAmount : '0';
	}

	public function setWriteOffAllocations( $fltWriteOffAllocations ) {
		$this->set( 'm_fltWriteOffAllocations', CStrings::strToFloatDef( $fltWriteOffAllocations, NULL, false, 2 ) );
	}

	public function getWriteOffAllocations() {
		return $this->m_fltWriteOffAllocations;
	}

	public function sqlWriteOffAllocations() {
		return ( true == isset( $this->m_fltWriteOffAllocations ) ) ? ( string ) $this->m_fltWriteOffAllocations : '0';
	}

	public function setOutOfPeriodAllocations( $fltOutOfPeriodAllocations ) {
		$this->set( 'm_fltOutOfPeriodAllocations', CStrings::strToFloatDef( $fltOutOfPeriodAllocations, NULL, false, 2 ) );
	}

	public function getOutOfPeriodAllocations() {
		return $this->m_fltOutOfPeriodAllocations;
	}

	public function sqlOutOfPeriodAllocations() {
		return ( true == isset( $this->m_fltOutOfPeriodAllocations ) ) ? ( string ) $this->m_fltOutOfPeriodAllocations : '0';
	}

	public function setScheduledAllocations( $fltScheduledAllocations ) {
		$this->set( 'm_fltScheduledAllocations', CStrings::strToFloatDef( $fltScheduledAllocations, NULL, false, 2 ) );
	}

	public function getScheduledAllocations() {
		return $this->m_fltScheduledAllocations;
	}

	public function sqlScheduledAllocations() {
		return ( true == isset( $this->m_fltScheduledAllocations ) ) ? ( string ) $this->m_fltScheduledAllocations : '0';
	}

	public function setAdjustedAllocations( $fltAdjustedAllocations ) {
		$this->set( 'm_fltAdjustedAllocations', CStrings::strToFloatDef( $fltAdjustedAllocations, NULL, false, 2 ) );
	}

	public function getAdjustedAllocations() {
		return $this->m_fltAdjustedAllocations;
	}

	public function sqlAdjustedAllocations() {
		return ( true == isset( $this->m_fltAdjustedAllocations ) ) ? ( string ) $this->m_fltAdjustedAllocations : '0';
	}

	public function setTotalCurrentCreditAllocations( $fltTotalCurrentCreditAllocations ) {
		$this->set( 'm_fltTotalCurrentCreditAllocations', CStrings::strToFloatDef( $fltTotalCurrentCreditAllocations, NULL, false, 2 ) );
	}

	public function getTotalCurrentCreditAllocations() {
		return $this->m_fltTotalCurrentCreditAllocations;
	}

	public function sqlTotalCurrentCreditAllocations() {
		return ( true == isset( $this->m_fltTotalCurrentCreditAllocations ) ) ? ( string ) $this->m_fltTotalCurrentCreditAllocations : '0';
	}

	public function setTotalCreditAllocations( $fltTotalCreditAllocations ) {
		$this->set( 'm_fltTotalCreditAllocations', CStrings::strToFloatDef( $fltTotalCreditAllocations, NULL, false, 2 ) );
	}

	public function getTotalCreditAllocations() {
		return $this->m_fltTotalCreditAllocations;
	}

	public function sqlTotalCreditAllocations() {
		return ( true == isset( $this->m_fltTotalCreditAllocations ) ) ? ( string ) $this->m_fltTotalCreditAllocations : '0';
	}

	public function setTotalCurrentAllocations( $fltTotalCurrentAllocations ) {
		$this->set( 'm_fltTotalCurrentAllocations', CStrings::strToFloatDef( $fltTotalCurrentAllocations, NULL, false, 2 ) );
	}

	public function getTotalCurrentAllocations() {
		return $this->m_fltTotalCurrentAllocations;
	}

	public function sqlTotalCurrentAllocations() {
		return ( true == isset( $this->m_fltTotalCurrentAllocations ) ) ? ( string ) $this->m_fltTotalCurrentAllocations : '0';
	}

	public function setTotalAllocations( $fltTotalAllocations ) {
		$this->set( 'm_fltTotalAllocations', CStrings::strToFloatDef( $fltTotalAllocations, NULL, false, 2 ) );
	}

	public function getTotalAllocations() {
		return $this->m_fltTotalAllocations;
	}

	public function sqlTotalAllocations() {
		return ( true == isset( $this->m_fltTotalAllocations ) ) ? ( string ) $this->m_fltTotalAllocations : '0';
	}

	public function setCurrentPaymentAllocations( $fltCurrentPaymentAllocations ) {
		$this->set( 'm_fltCurrentPaymentAllocations', CStrings::strToFloatDef( $fltCurrentPaymentAllocations, NULL, false, 2 ) );
	}

	public function getCurrentPaymentAllocations() {
		return $this->m_fltCurrentPaymentAllocations;
	}

	public function sqlCurrentPaymentAllocations() {
		return ( true == isset( $this->m_fltCurrentPaymentAllocations ) ) ? ( string ) $this->m_fltCurrentPaymentAllocations : '0';
	}

	public function setPrePaymentAllocations( $fltPrePaymentAllocations ) {
		$this->set( 'm_fltPrePaymentAllocations', CStrings::strToFloatDef( $fltPrePaymentAllocations, NULL, false, 2 ) );
	}

	public function getPrePaymentAllocations() {
		return $this->m_fltPrePaymentAllocations;
	}

	public function sqlPrePaymentAllocations() {
		return ( true == isset( $this->m_fltPrePaymentAllocations ) ) ? ( string ) $this->m_fltPrePaymentAllocations : '0';
	}

	public function setPaymentAllocations( $fltPaymentAllocations ) {
		$this->set( 'm_fltPaymentAllocations', CStrings::strToFloatDef( $fltPaymentAllocations, NULL, false, 2 ) );
	}

	public function getPaymentAllocations() {
		return $this->m_fltPaymentAllocations;
	}

	public function sqlPaymentAllocations() {
		return ( true == isset( $this->m_fltPaymentAllocations ) ) ? ( string ) $this->m_fltPaymentAllocations : '0';
	}

	public function setPaymentInKindAllocations( $fltPaymentInKindAllocations ) {
		$this->set( 'm_fltPaymentInKindAllocations', CStrings::strToFloatDef( $fltPaymentInKindAllocations, NULL, false, 2 ) );
	}

	public function getPaymentInKindAllocations() {
		return $this->m_fltPaymentInKindAllocations;
	}

	public function sqlPaymentInKindAllocations() {
		return ( true == isset( $this->m_fltPaymentInKindAllocations ) ) ? ( string ) $this->m_fltPaymentInKindAllocations : '0';
	}

	public function setRentAllocations( $fltRentAllocations ) {
		$this->set( 'm_fltRentAllocations', CStrings::strToFloatDef( $fltRentAllocations, NULL, false, 2 ) );
	}

	public function getRentAllocations() {
		return $this->m_fltRentAllocations;
	}

	public function sqlRentAllocations() {
		return ( true == isset( $this->m_fltRentAllocations ) ) ? ( string ) $this->m_fltRentAllocations : '0';
	}

	public function setOtherIncomeAllocations( $fltOtherIncomeAllocations ) {
		$this->set( 'm_fltOtherIncomeAllocations', CStrings::strToFloatDef( $fltOtherIncomeAllocations, NULL, false, 2 ) );
	}

	public function getOtherIncomeAllocations() {
		return $this->m_fltOtherIncomeAllocations;
	}

	public function sqlOtherIncomeAllocations() {
		return ( true == isset( $this->m_fltOtherIncomeAllocations ) ) ? ( string ) $this->m_fltOtherIncomeAllocations : '0';
	}

	public function setExpenseAllocations( $fltExpenseAllocations ) {
		$this->set( 'm_fltExpenseAllocations', CStrings::strToFloatDef( $fltExpenseAllocations, NULL, false, 2 ) );
	}

	public function getExpenseAllocations() {
		return $this->m_fltExpenseAllocations;
	}

	public function sqlExpenseAllocations() {
		return ( true == isset( $this->m_fltExpenseAllocations ) ) ? ( string ) $this->m_fltExpenseAllocations : '0';
	}

	public function setAssetAllocations( $fltAssetAllocations ) {
		$this->set( 'm_fltAssetAllocations', CStrings::strToFloatDef( $fltAssetAllocations, NULL, false, 2 ) );
	}

	public function getAssetAllocations() {
		return $this->m_fltAssetAllocations;
	}

	public function sqlAssetAllocations() {
		return ( true == isset( $this->m_fltAssetAllocations ) ) ? ( string ) $this->m_fltAssetAllocations : '0';
	}

	public function setEquityAllocations( $fltEquityAllocations ) {
		$this->set( 'm_fltEquityAllocations', CStrings::strToFloatDef( $fltEquityAllocations, NULL, false, 2 ) );
	}

	public function getEquityAllocations() {
		return $this->m_fltEquityAllocations;
	}

	public function sqlEquityAllocations() {
		return ( true == isset( $this->m_fltEquityAllocations ) ) ? ( string ) $this->m_fltEquityAllocations : '0';
	}

	public function setOtherChargeAllocations( $fltOtherChargeAllocations ) {
		$this->set( 'm_fltOtherChargeAllocations', CStrings::strToFloatDef( $fltOtherChargeAllocations, NULL, false, 2 ) );
	}

	public function getOtherChargeAllocations() {
		return $this->m_fltOtherChargeAllocations;
	}

	public function sqlOtherChargeAllocations() {
		return ( true == isset( $this->m_fltOtherChargeAllocations ) ) ? ( string ) $this->m_fltOtherChargeAllocations : '0';
	}

	public function setDepositAllocations( $fltDepositAllocations ) {
		$this->set( 'm_fltDepositAllocations', CStrings::strToFloatDef( $fltDepositAllocations, NULL, false, 2 ) );
	}

	public function getDepositAllocations() {
		return $this->m_fltDepositAllocations;
	}

	public function sqlDepositAllocations() {
		return ( true == isset( $this->m_fltDepositAllocations ) ) ? ( string ) $this->m_fltDepositAllocations : '0';
	}

	public function setDepositCreditAllocations( $fltDepositCreditAllocations ) {
		$this->set( 'm_fltDepositCreditAllocations', CStrings::strToFloatDef( $fltDepositCreditAllocations, NULL, false, 2 ) );
	}

	public function getDepositCreditAllocations() {
		return $this->m_fltDepositCreditAllocations;
	}

	public function sqlDepositCreditAllocations() {
		return ( true == isset( $this->m_fltDepositCreditAllocations ) ) ? ( string ) $this->m_fltDepositCreditAllocations : '0';
	}

	public function setRefundAllocations( $fltRefundAllocations ) {
		$this->set( 'm_fltRefundAllocations', CStrings::strToFloatDef( $fltRefundAllocations, NULL, false, 2 ) );
	}

	public function getRefundAllocations() {
		return $this->m_fltRefundAllocations;
	}

	public function sqlRefundAllocations() {
		return ( true == isset( $this->m_fltRefundAllocations ) ) ? ( string ) $this->m_fltRefundAllocations : '0';
	}

	public function setOtherLiabilityAllocations( $fltOtherLiabilityAllocations ) {
		$this->set( 'm_fltOtherLiabilityAllocations', CStrings::strToFloatDef( $fltOtherLiabilityAllocations, NULL, false, 2 ) );
	}

	public function getOtherLiabilityAllocations() {
		return $this->m_fltOtherLiabilityAllocations;
	}

	public function sqlOtherLiabilityAllocations() {
		return ( true == isset( $this->m_fltOtherLiabilityAllocations ) ) ? ( string ) $this->m_fltOtherLiabilityAllocations : '0';
	}

	public function setReceivablesAllocations( $fltReceivablesAllocations ) {
		$this->set( 'm_fltReceivablesAllocations', CStrings::strToFloatDef( $fltReceivablesAllocations, NULL, false, 2 ) );
	}

	public function getReceivablesAllocations() {
		return $this->m_fltReceivablesAllocations;
	}

	public function sqlReceivablesAllocations() {
		return ( true == isset( $this->m_fltReceivablesAllocations ) ) ? ( string ) $this->m_fltReceivablesAllocations : '0';
	}

	public function setPostToCashRentAllocations( $fltPostToCashRentAllocations ) {
		$this->set( 'm_fltPostToCashRentAllocations', CStrings::strToFloatDef( $fltPostToCashRentAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashRentAllocations() {
		return $this->m_fltPostToCashRentAllocations;
	}

	public function sqlPostToCashRentAllocations() {
		return ( true == isset( $this->m_fltPostToCashRentAllocations ) ) ? ( string ) $this->m_fltPostToCashRentAllocations : '0';
	}

	public function setPostToCashOtherIncomeAllocations( $fltPostToCashOtherIncomeAllocations ) {
		$this->set( 'm_fltPostToCashOtherIncomeAllocations', CStrings::strToFloatDef( $fltPostToCashOtherIncomeAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashOtherIncomeAllocations() {
		return $this->m_fltPostToCashOtherIncomeAllocations;
	}

	public function sqlPostToCashOtherIncomeAllocations() {
		return ( true == isset( $this->m_fltPostToCashOtherIncomeAllocations ) ) ? ( string ) $this->m_fltPostToCashOtherIncomeAllocations : '0';
	}

	public function setPostToCashExpenseAllocations( $fltPostToCashExpenseAllocations ) {
		$this->set( 'm_fltPostToCashExpenseAllocations', CStrings::strToFloatDef( $fltPostToCashExpenseAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashExpenseAllocations() {
		return $this->m_fltPostToCashExpenseAllocations;
	}

	public function sqlPostToCashExpenseAllocations() {
		return ( true == isset( $this->m_fltPostToCashExpenseAllocations ) ) ? ( string ) $this->m_fltPostToCashExpenseAllocations : '0';
	}

	public function setPostToCashAssetAllocations( $fltPostToCashAssetAllocations ) {
		$this->set( 'm_fltPostToCashAssetAllocations', CStrings::strToFloatDef( $fltPostToCashAssetAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashAssetAllocations() {
		return $this->m_fltPostToCashAssetAllocations;
	}

	public function sqlPostToCashAssetAllocations() {
		return ( true == isset( $this->m_fltPostToCashAssetAllocations ) ) ? ( string ) $this->m_fltPostToCashAssetAllocations : '0';
	}

	public function setPostToCashEquityAllocations( $fltPostToCashEquityAllocations ) {
		$this->set( 'm_fltPostToCashEquityAllocations', CStrings::strToFloatDef( $fltPostToCashEquityAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashEquityAllocations() {
		return $this->m_fltPostToCashEquityAllocations;
	}

	public function sqlPostToCashEquityAllocations() {
		return ( true == isset( $this->m_fltPostToCashEquityAllocations ) ) ? ( string ) $this->m_fltPostToCashEquityAllocations : '0';
	}

	public function setPostToCashOtherChargeAllocations( $fltPostToCashOtherChargeAllocations ) {
		$this->set( 'm_fltPostToCashOtherChargeAllocations', CStrings::strToFloatDef( $fltPostToCashOtherChargeAllocations, NULL, false, 2 ) );
	}

	public function getPostToCashOtherChargeAllocations() {
		return $this->m_fltPostToCashOtherChargeAllocations;
	}

	public function sqlPostToCashOtherChargeAllocations() {
		return ( true == isset( $this->m_fltPostToCashOtherChargeAllocations ) ) ? ( string ) $this->m_fltPostToCashOtherChargeAllocations : '0';
	}

	public function setTotalCashBasisAllocations( $fltTotalCashBasisAllocations ) {
		$this->set( 'm_fltTotalCashBasisAllocations', CStrings::strToFloatDef( $fltTotalCashBasisAllocations, NULL, false, 2 ) );
	}

	public function getTotalCashBasisAllocations() {
		return $this->m_fltTotalCashBasisAllocations;
	}

	public function sqlTotalCashBasisAllocations() {
		return ( true == isset( $this->m_fltTotalCashBasisAllocations ) ) ? ( string ) $this->m_fltTotalCashBasisAllocations : '0';
	}

	public function setScheduledChargeTotal( $fltScheduledChargeTotal ) {
		$this->set( 'm_fltScheduledChargeTotal', CStrings::strToFloatDef( $fltScheduledChargeTotal, NULL, false, 2 ) );
	}

	public function getScheduledChargeTotal() {
		return $this->m_fltScheduledChargeTotal;
	}

	public function sqlScheduledChargeTotal() {
		return ( true == isset( $this->m_fltScheduledChargeTotal ) ) ? ( string ) $this->m_fltScheduledChargeTotal : '0';
	}

	public function setScheduledChargeProratedTotal( $fltScheduledChargeProratedTotal ) {
		$this->set( 'm_fltScheduledChargeProratedTotal', CStrings::strToFloatDef( $fltScheduledChargeProratedTotal, NULL, false, 2 ) );
	}

	public function getScheduledChargeProratedTotal() {
		return $this->m_fltScheduledChargeProratedTotal;
	}

	public function sqlScheduledChargeProratedTotal() {
		return ( true == isset( $this->m_fltScheduledChargeProratedTotal ) ) ? ( string ) $this->m_fltScheduledChargeProratedTotal : '0';
	}

	public function setTransactionCount( $intTransactionCount ) {
		$this->set( 'm_intTransactionCount', CStrings::strToIntDef( $intTransactionCount, NULL, false ) );
	}

	public function getTransactionCount() {
		return $this->m_intTransactionCount;
	}

	public function sqlTransactionCount() {
		return ( true == isset( $this->m_intTransactionCount ) ) ? ( string ) $this->m_intTransactionCount : '0';
	}

	public function setAllocationCount( $fltAllocationCount ) {
		$this->set( 'm_fltAllocationCount', CStrings::strToFloatDef( $fltAllocationCount, NULL, false, 2 ) );
	}

	public function getAllocationCount() {
		return $this->m_fltAllocationCount;
	}

	public function sqlAllocationCount() {
		return ( true == isset( $this->m_fltAllocationCount ) ) ? ( string ) $this->m_fltAllocationCount : '0';
	}

	public function setScheduledChargeCount( $fltScheduledChargeCount ) {
		$this->set( 'm_fltScheduledChargeCount', CStrings::strToFloatDef( $fltScheduledChargeCount, NULL, false, 2 ) );
	}

	public function getScheduledChargeCount() {
		return $this->m_fltScheduledChargeCount;
	}

	public function sqlScheduledChargeCount() {
		return ( true == isset( $this->m_fltScheduledChargeCount ) ) ? ( string ) $this->m_fltScheduledChargeCount : '0';
	}

	public function setArCodeGroupId( $intArCodeGroupId ) {
		$this->set( 'm_intArCodeGroupId', CStrings::strToIntDef( $intArCodeGroupId, NULL, false ) );
	}

	public function getArCodeGroupId() {
		return $this->m_intArCodeGroupId;
	}

	public function sqlArCodeGroupId() {
		return ( true == isset( $this->m_intArCodeGroupId ) ) ? ( string ) $this->m_intArCodeGroupId : 'NULL';
	}

	public function setPostToCash( $boolPostToCash ) {
		$this->set( 'm_boolPostToCash', CStrings::strToBool( $boolPostToCash ) );
	}

	public function getPostToCash() {
		return $this->m_boolPostToCash;
	}

	public function sqlPostToCash() {
		return ( true == isset( $this->m_boolPostToCash ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostToCash ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPaymentInKind( $boolIsPaymentInKind ) {
		$this->set( 'm_boolIsPaymentInKind', CStrings::strToBool( $boolIsPaymentInKind ) );
	}

	public function getIsPaymentInKind() {
		return $this->m_boolIsPaymentInKind;
	}

	public function sqlIsPaymentInKind() {
		return ( true == isset( $this->m_boolIsPaymentInKind ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentInKind ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDepositCredit( $boolIsDepositCredit ) {
		$this->set( 'm_boolIsDepositCredit', CStrings::strToBool( $boolIsDepositCredit ) );
	}

	public function getIsDepositCredit() {
		return $this->m_boolIsDepositCredit;
	}

	public function sqlIsDepositCredit() {
		return ( true == isset( $this->m_boolIsDepositCredit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDepositCredit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'period_id' => $this->getPeriodId(),
			'property_id' => $this->getPropertyId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'ar_code_type_id' => $this->getArCodeTypeId(),
			'ar_code_id' => $this->getArCodeId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_trigger_type_id' => $this->getArTriggerTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'post_month' => $this->getPostMonth(),
			'ar_code_name' => $this->getArCodeName(),
			'debit_gl_account_name' => $this->getDebitGlAccountName(),
			'credit_gl_account_name' => $this->getCreditGlAccountName(),
			'debit_gl_account_number' => $this->getDebitGlAccountNumber(),
			'credit_gl_account_number' => $this->getCreditGlAccountNumber(),
			'write_off_amount' => $this->getWriteOffAmount(),
			'out_of_period_amount' => $this->getOutOfPeriodAmount(),
			'scheduled_amount' => $this->getScheduledAmount(),
			'adjusted_amount' => $this->getAdjustedAmount(),
			'total_credit_amount' => $this->getTotalCreditAmount(),
			'total_amount' => $this->getTotalAmount(),
			'write_off_allocations' => $this->getWriteOffAllocations(),
			'out_of_period_allocations' => $this->getOutOfPeriodAllocations(),
			'scheduled_allocations' => $this->getScheduledAllocations(),
			'adjusted_allocations' => $this->getAdjustedAllocations(),
			'total_current_credit_allocations' => $this->getTotalCurrentCreditAllocations(),
			'total_credit_allocations' => $this->getTotalCreditAllocations(),
			'total_current_allocations' => $this->getTotalCurrentAllocations(),
			'total_allocations' => $this->getTotalAllocations(),
			'current_payment_allocations' => $this->getCurrentPaymentAllocations(),
			'pre_payment_allocations' => $this->getPrePaymentAllocations(),
			'payment_allocations' => $this->getPaymentAllocations(),
			'payment_in_kind_allocations' => $this->getPaymentInKindAllocations(),
			'rent_allocations' => $this->getRentAllocations(),
			'other_income_allocations' => $this->getOtherIncomeAllocations(),
			'expense_allocations' => $this->getExpenseAllocations(),
			'asset_allocations' => $this->getAssetAllocations(),
			'equity_allocations' => $this->getEquityAllocations(),
			'other_charge_allocations' => $this->getOtherChargeAllocations(),
			'deposit_allocations' => $this->getDepositAllocations(),
			'deposit_credit_allocations' => $this->getDepositCreditAllocations(),
			'refund_allocations' => $this->getRefundAllocations(),
			'other_liability_allocations' => $this->getOtherLiabilityAllocations(),
			'receivables_allocations' => $this->getReceivablesAllocations(),
			'post_to_cash_rent_allocations' => $this->getPostToCashRentAllocations(),
			'post_to_cash_other_income_allocations' => $this->getPostToCashOtherIncomeAllocations(),
			'post_to_cash_expense_allocations' => $this->getPostToCashExpenseAllocations(),
			'post_to_cash_asset_allocations' => $this->getPostToCashAssetAllocations(),
			'post_to_cash_equity_allocations' => $this->getPostToCashEquityAllocations(),
			'post_to_cash_other_charge_allocations' => $this->getPostToCashOtherChargeAllocations(),
			'total_cash_basis_allocations' => $this->getTotalCashBasisAllocations(),
			'scheduled_charge_total' => $this->getScheduledChargeTotal(),
			'scheduled_charge_prorated_total' => $this->getScheduledChargeProratedTotal(),
			'transaction_count' => $this->getTransactionCount(),
			'allocation_count' => $this->getAllocationCount(),
			'scheduled_charge_count' => $this->getScheduledChargeCount(),
			'ar_code_group_id' => $this->getArCodeGroupId(),
			'post_to_cash' => $this->getPostToCash(),
			'is_payment_in_kind' => $this->getIsPaymentInKind(),
			'is_deposit_credit' => $this->getIsDepositCredit(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>