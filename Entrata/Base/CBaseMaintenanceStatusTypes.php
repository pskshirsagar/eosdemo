<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceStatusTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceStatusType[]
	 */
	public static function fetchMaintenanceStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceStatusType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceStatusType
	 */
	public static function fetchMaintenanceStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceStatusType::class, $objDatabase );
	}

	public static function fetchMaintenanceStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_status_types', $objDatabase );
	}

	public static function fetchMaintenanceStatusTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceStatusType( sprintf( 'SELECT * FROM maintenance_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>