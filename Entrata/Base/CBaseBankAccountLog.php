<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankAccountLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.bank_account_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBankAccountId;
	protected $m_intBankAccountTypeId;
	protected $m_intBankLoginTypeId;
	protected $m_intCheckTemplateId;
	protected $m_intCheckAccountTypeId;
	protected $m_intTransmissionVendorId;
	protected $m_intApAchTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strAccountName;
	protected $m_strCheckBankName;
	protected $m_strBankContactNameFirstEncrypted;
	protected $m_strBankContactNameLastEncrypted;
	protected $m_strBankStreetLine1;
	protected $m_strBankStreetLine2;
	protected $m_strBankStateCode;
	protected $m_strBankPostalCode;
	protected $m_strBankCity;
	protected $m_strAction;
	protected $m_strBankPhoneNumberEncrypted;
	protected $m_strCheckNameOnAccount;
	protected $m_strPayerStreetLine1;
	protected $m_strPayerStreetLine2;
	protected $m_strPayerCity;
	protected $m_strPayerStateCode;
	protected $m_strPayerPostalCode;
	protected $m_strPayerPhoneNumberEncrypted;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckFractionalRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckAccountNumberMasked;
	protected $m_strCheckReorderReminderDate;
	protected $m_intCheckReorderReminderNumber;
	protected $m_intNextCheckNumber;
	protected $m_fltMinimumBalance;
	protected $m_fltPrintSignatureLimitAmount;
	protected $m_fltSecondSignatureLimitAmount;
	protected $m_fltReserveAmount;
	protected $m_intPrintInvoiceDetails;
	protected $m_strLoginUri;
	protected $m_strLoginUsername;
	protected $m_strLoginPasswordEncrypted;
	protected $m_strImmediateDestinationName;
	protected $m_strImmediateDestinationId;
	protected $m_strImmediateOriginId;
	protected $m_strImmediateOriginName;
	protected $m_strCompanyName;
	protected $m_strCompanyId;
	protected $m_strOriginatingDfi;
	protected $m_strGiactVerificationResponse;
	protected $m_boolIsVerified;
	protected $m_intIsCreditCardAccount;
	protected $m_intIsStubStubCheckFormat;
	protected $m_intIsSystem;
	protected $m_intIsDisabled;
	protected $m_intIsBankRecOnPostMonth;
	protected $m_intIsCashBasis;
	protected $m_intIsAchEnable;
	protected $m_boolIsBalancedAchFile;
	protected $m_boolIsShowVendorAccountNumber;
	protected $m_strVerifiedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strWfpmCeoCompanyId;
	protected $m_strCheckLayoutId;
	protected $m_strOriginatingBankId;
	protected $m_strAchCompanyId;
	protected $m_strAchSecCode;
	protected $m_intSftpCompanyTransmissionVendorId;
	protected $m_intSecureReferenceNumber;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcFirstName;
	protected $m_strCcLastName;
	protected $m_intIsWfpmEnable;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intApPayeeAccountId;
	protected $m_boolIsCreateInvoiceAtPayment;
	protected $m_boolIsSeparateBatchPerPayment;
	protected $m_boolIsUseSubledgers;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsAutoGenerateReimbursementInvoice;
	protected $m_strSenderId;
	protected $m_strClientId;

	public function __construct() {
		parent::__construct();

		$this->m_intApAchTypeId = '1';
		$this->m_intNextCheckNumber = '0';
		$this->m_fltMinimumBalance = '0';
		$this->m_fltReserveAmount = '0';
		$this->m_intPrintInvoiceDetails = '1';
		$this->m_boolIsVerified = false;
		$this->m_intIsCreditCardAccount = '0';
		$this->m_intIsStubStubCheckFormat = '1';
		$this->m_intIsSystem = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsBankRecOnPostMonth = '0';
		$this->m_intIsCashBasis = '1';
		$this->m_boolIsBalancedAchFile = false;
		$this->m_boolIsShowVendorAccountNumber = true;
		$this->m_boolIsCreateInvoiceAtPayment = false;
		$this->m_boolIsSeparateBatchPerPayment = true;
		$this->m_boolIsUseSubledgers = false;
		$this->m_boolIsAutoGenerateReimbursementInvoice = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['bank_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountTypeId', trim( $arrValues['bank_account_type_id'] ) ); elseif( isset( $arrValues['bank_account_type_id'] ) ) $this->setBankAccountTypeId( $arrValues['bank_account_type_id'] );
		if( isset( $arrValues['bank_login_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBankLoginTypeId', trim( $arrValues['bank_login_type_id'] ) ); elseif( isset( $arrValues['bank_login_type_id'] ) ) $this->setBankLoginTypeId( $arrValues['bank_login_type_id'] );
		if( isset( $arrValues['check_template_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckTemplateId', trim( $arrValues['check_template_id'] ) ); elseif( isset( $arrValues['check_template_id'] ) ) $this->setCheckTemplateId( $arrValues['check_template_id'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['ap_ach_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApAchTypeId', trim( $arrValues['ap_ach_type_id'] ) ); elseif( isset( $arrValues['ap_ach_type_id'] ) ) $this->setApAchTypeId( $arrValues['ap_ach_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['account_name'] ) && $boolDirectSet ) $this->set( 'm_strAccountName', trim( $arrValues['account_name'] ) ); elseif( isset( $arrValues['account_name'] ) ) $this->setAccountName( $arrValues['account_name'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['bank_contact_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankContactNameFirstEncrypted', trim( $arrValues['bank_contact_name_first_encrypted'] ) ); elseif( isset( $arrValues['bank_contact_name_first_encrypted'] ) ) $this->setBankContactNameFirstEncrypted( $arrValues['bank_contact_name_first_encrypted'] );
		if( isset( $arrValues['bank_contact_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankContactNameLastEncrypted', trim( $arrValues['bank_contact_name_last_encrypted'] ) ); elseif( isset( $arrValues['bank_contact_name_last_encrypted'] ) ) $this->setBankContactNameLastEncrypted( $arrValues['bank_contact_name_last_encrypted'] );
		if( isset( $arrValues['bank_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBankStreetLine1', trim( $arrValues['bank_street_line1'] ) ); elseif( isset( $arrValues['bank_street_line1'] ) ) $this->setBankStreetLine1( $arrValues['bank_street_line1'] );
		if( isset( $arrValues['bank_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strBankStreetLine2', trim( $arrValues['bank_street_line2'] ) ); elseif( isset( $arrValues['bank_street_line2'] ) ) $this->setBankStreetLine2( $arrValues['bank_street_line2'] );
		if( isset( $arrValues['bank_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBankStateCode', trim( $arrValues['bank_state_code'] ) ); elseif( isset( $arrValues['bank_state_code'] ) ) $this->setBankStateCode( $arrValues['bank_state_code'] );
		if( isset( $arrValues['bank_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBankPostalCode', trim( $arrValues['bank_postal_code'] ) ); elseif( isset( $arrValues['bank_postal_code'] ) ) $this->setBankPostalCode( $arrValues['bank_postal_code'] );
		if( isset( $arrValues['bank_city'] ) && $boolDirectSet ) $this->set( 'm_strBankCity', trim( $arrValues['bank_city'] ) ); elseif( isset( $arrValues['bank_city'] ) ) $this->setBankCity( $arrValues['bank_city'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( $arrValues['action'] ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( $arrValues['action'] );
		if( isset( $arrValues['bank_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankPhoneNumberEncrypted', trim( $arrValues['bank_phone_number_encrypted'] ) ); elseif( isset( $arrValues['bank_phone_number_encrypted'] ) ) $this->setBankPhoneNumberEncrypted( $arrValues['bank_phone_number_encrypted'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( $arrValues['check_name_on_account'] ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrValues['check_name_on_account'] );
		if( isset( $arrValues['payer_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strPayerStreetLine1', trim( $arrValues['payer_street_line1'] ) ); elseif( isset( $arrValues['payer_street_line1'] ) ) $this->setPayerStreetLine1( $arrValues['payer_street_line1'] );
		if( isset( $arrValues['payer_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strPayerStreetLine2', trim( $arrValues['payer_street_line2'] ) ); elseif( isset( $arrValues['payer_street_line2'] ) ) $this->setPayerStreetLine2( $arrValues['payer_street_line2'] );
		if( isset( $arrValues['payer_city'] ) && $boolDirectSet ) $this->set( 'm_strPayerCity', trim( $arrValues['payer_city'] ) ); elseif( isset( $arrValues['payer_city'] ) ) $this->setPayerCity( $arrValues['payer_city'] );
		if( isset( $arrValues['payer_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPayerStateCode', trim( $arrValues['payer_state_code'] ) ); elseif( isset( $arrValues['payer_state_code'] ) ) $this->setPayerStateCode( $arrValues['payer_state_code'] );
		if( isset( $arrValues['payer_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPayerPostalCode', trim( $arrValues['payer_postal_code'] ) ); elseif( isset( $arrValues['payer_postal_code'] ) ) $this->setPayerPostalCode( $arrValues['payer_postal_code'] );
		if( isset( $arrValues['payer_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPayerPhoneNumberEncrypted', trim( $arrValues['payer_phone_number_encrypted'] ) ); elseif( isset( $arrValues['payer_phone_number_encrypted'] ) ) $this->setPayerPhoneNumberEncrypted( $arrValues['payer_phone_number_encrypted'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_fractional_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckFractionalRoutingNumber', trim( $arrValues['check_fractional_routing_number'] ) ); elseif( isset( $arrValues['check_fractional_routing_number'] ) ) $this->setCheckFractionalRoutingNumber( $arrValues['check_fractional_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_account_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberMasked', trim( $arrValues['check_account_number_masked'] ) ); elseif( isset( $arrValues['check_account_number_masked'] ) ) $this->setCheckAccountNumberMasked( $arrValues['check_account_number_masked'] );
		if( isset( $arrValues['check_reorder_reminder_date'] ) && $boolDirectSet ) $this->set( 'm_strCheckReorderReminderDate', trim( $arrValues['check_reorder_reminder_date'] ) ); elseif( isset( $arrValues['check_reorder_reminder_date'] ) ) $this->setCheckReorderReminderDate( $arrValues['check_reorder_reminder_date'] );
		if( isset( $arrValues['check_reorder_reminder_number'] ) && $boolDirectSet ) $this->set( 'm_intCheckReorderReminderNumber', trim( $arrValues['check_reorder_reminder_number'] ) ); elseif( isset( $arrValues['check_reorder_reminder_number'] ) ) $this->setCheckReorderReminderNumber( $arrValues['check_reorder_reminder_number'] );
		if( isset( $arrValues['next_check_number'] ) && $boolDirectSet ) $this->set( 'm_intNextCheckNumber', trim( $arrValues['next_check_number'] ) ); elseif( isset( $arrValues['next_check_number'] ) ) $this->setNextCheckNumber( $arrValues['next_check_number'] );
		if( isset( $arrValues['minimum_balance'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumBalance', trim( $arrValues['minimum_balance'] ) ); elseif( isset( $arrValues['minimum_balance'] ) ) $this->setMinimumBalance( $arrValues['minimum_balance'] );
		if( isset( $arrValues['print_signature_limit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPrintSignatureLimitAmount', trim( $arrValues['print_signature_limit_amount'] ) ); elseif( isset( $arrValues['print_signature_limit_amount'] ) ) $this->setPrintSignatureLimitAmount( $arrValues['print_signature_limit_amount'] );
		if( isset( $arrValues['second_signature_limit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSecondSignatureLimitAmount', trim( $arrValues['second_signature_limit_amount'] ) ); elseif( isset( $arrValues['second_signature_limit_amount'] ) ) $this->setSecondSignatureLimitAmount( $arrValues['second_signature_limit_amount'] );
		if( isset( $arrValues['reserve_amount'] ) && $boolDirectSet ) $this->set( 'm_fltReserveAmount', trim( $arrValues['reserve_amount'] ) ); elseif( isset( $arrValues['reserve_amount'] ) ) $this->setReserveAmount( $arrValues['reserve_amount'] );
		if( isset( $arrValues['print_invoice_details'] ) && $boolDirectSet ) $this->set( 'm_intPrintInvoiceDetails', trim( $arrValues['print_invoice_details'] ) ); elseif( isset( $arrValues['print_invoice_details'] ) ) $this->setPrintInvoiceDetails( $arrValues['print_invoice_details'] );
		if( isset( $arrValues['login_uri'] ) && $boolDirectSet ) $this->set( 'm_strLoginUri', trim( $arrValues['login_uri'] ) ); elseif( isset( $arrValues['login_uri'] ) ) $this->setLoginUri( $arrValues['login_uri'] );
		if( isset( $arrValues['login_username'] ) && $boolDirectSet ) $this->set( 'm_strLoginUsername', trim( $arrValues['login_username'] ) ); elseif( isset( $arrValues['login_username'] ) ) $this->setLoginUsername( $arrValues['login_username'] );
		if( isset( $arrValues['login_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strLoginPasswordEncrypted', trim( $arrValues['login_password_encrypted'] ) ); elseif( isset( $arrValues['login_password_encrypted'] ) ) $this->setLoginPasswordEncrypted( $arrValues['login_password_encrypted'] );
		if( isset( $arrValues['immediate_destination_name'] ) && $boolDirectSet ) $this->set( 'm_strImmediateDestinationName', trim( $arrValues['immediate_destination_name'] ) ); elseif( isset( $arrValues['immediate_destination_name'] ) ) $this->setImmediateDestinationName( $arrValues['immediate_destination_name'] );
		if( isset( $arrValues['immediate_destination_id'] ) && $boolDirectSet ) $this->set( 'm_strImmediateDestinationId', trim( $arrValues['immediate_destination_id'] ) ); elseif( isset( $arrValues['immediate_destination_id'] ) ) $this->setImmediateDestinationId( $arrValues['immediate_destination_id'] );
		if( isset( $arrValues['immediate_origin_id'] ) && $boolDirectSet ) $this->set( 'm_strImmediateOriginId', trim( $arrValues['immediate_origin_id'] ) ); elseif( isset( $arrValues['immediate_origin_id'] ) ) $this->setImmediateOriginId( $arrValues['immediate_origin_id'] );
		if( isset( $arrValues['immediate_origin_name'] ) && $boolDirectSet ) $this->set( 'm_strImmediateOriginName', trim( $arrValues['immediate_origin_name'] ) ); elseif( isset( $arrValues['immediate_origin_name'] ) ) $this->setImmediateOriginName( $arrValues['immediate_origin_name'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_strCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['originating_dfi'] ) && $boolDirectSet ) $this->set( 'm_strOriginatingDfi', trim( $arrValues['originating_dfi'] ) ); elseif( isset( $arrValues['originating_dfi'] ) ) $this->setOriginatingDfi( $arrValues['originating_dfi'] );
		if( isset( $arrValues['giact_verification_response'] ) && $boolDirectSet ) $this->set( 'm_strGiactVerificationResponse', trim( $arrValues['giact_verification_response'] ) ); elseif( isset( $arrValues['giact_verification_response'] ) ) $this->setGiactVerificationResponse( $arrValues['giact_verification_response'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerified', trim( stripcslashes( $arrValues['is_verified'] ) ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
		if( isset( $arrValues['is_credit_card_account'] ) && $boolDirectSet ) $this->set( 'm_intIsCreditCardAccount', trim( $arrValues['is_credit_card_account'] ) ); elseif( isset( $arrValues['is_credit_card_account'] ) ) $this->setIsCreditCardAccount( $arrValues['is_credit_card_account'] );
		if( isset( $arrValues['is_stub_stub_check_format'] ) && $boolDirectSet ) $this->set( 'm_intIsStubStubCheckFormat', trim( $arrValues['is_stub_stub_check_format'] ) ); elseif( isset( $arrValues['is_stub_stub_check_format'] ) ) $this->setIsStubStubCheckFormat( $arrValues['is_stub_stub_check_format'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_bank_rec_on_post_month'] ) && $boolDirectSet ) $this->set( 'm_intIsBankRecOnPostMonth', trim( $arrValues['is_bank_rec_on_post_month'] ) ); elseif( isset( $arrValues['is_bank_rec_on_post_month'] ) ) $this->setIsBankRecOnPostMonth( $arrValues['is_bank_rec_on_post_month'] );
		if( isset( $arrValues['is_cash_basis'] ) && $boolDirectSet ) $this->set( 'm_intIsCashBasis', trim( $arrValues['is_cash_basis'] ) ); elseif( isset( $arrValues['is_cash_basis'] ) ) $this->setIsCashBasis( $arrValues['is_cash_basis'] );
		if( isset( $arrValues['is_ach_enable'] ) && $boolDirectSet ) $this->set( 'm_intIsAchEnable', trim( $arrValues['is_ach_enable'] ) ); elseif( isset( $arrValues['is_ach_enable'] ) ) $this->setIsAchEnable( $arrValues['is_ach_enable'] );
		if( isset( $arrValues['is_balanced_ach_file'] ) && $boolDirectSet ) $this->set( 'm_boolIsBalancedAchFile', trim( stripcslashes( $arrValues['is_balanced_ach_file'] ) ) ); elseif( isset( $arrValues['is_balanced_ach_file'] ) ) $this->setIsBalancedAchFile( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_balanced_ach_file'] ) : $arrValues['is_balanced_ach_file'] );
		if( isset( $arrValues['is_show_vendor_account_number'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowVendorAccountNumber', trim( stripcslashes( $arrValues['is_show_vendor_account_number'] ) ) ); elseif( isset( $arrValues['is_show_vendor_account_number'] ) ) $this->setIsShowVendorAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_vendor_account_number'] ) : $arrValues['is_show_vendor_account_number'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['wfpm_ceo_company_id'] ) && $boolDirectSet ) $this->set( 'm_strWfpmCeoCompanyId', trim( $arrValues['wfpm_ceo_company_id'] ) ); elseif( isset( $arrValues['wfpm_ceo_company_id'] ) ) $this->setWfpmCeoCompanyId( $arrValues['wfpm_ceo_company_id'] );
		if( isset( $arrValues['check_layout_id'] ) && $boolDirectSet ) $this->set( 'm_strCheckLayoutId', trim( $arrValues['check_layout_id'] ) ); elseif( isset( $arrValues['check_layout_id'] ) ) $this->setCheckLayoutId( $arrValues['check_layout_id'] );
		if( isset( $arrValues['originating_bank_id'] ) && $boolDirectSet ) $this->set( 'm_strOriginatingBankId', trim( $arrValues['originating_bank_id'] ) ); elseif( isset( $arrValues['originating_bank_id'] ) ) $this->setOriginatingBankId( $arrValues['originating_bank_id'] );
		if( isset( $arrValues['ach_company_id'] ) && $boolDirectSet ) $this->set( 'm_strAchCompanyId', trim( $arrValues['ach_company_id'] ) ); elseif( isset( $arrValues['ach_company_id'] ) ) $this->setAchCompanyId( $arrValues['ach_company_id'] );
		if( isset( $arrValues['ach_sec_code'] ) && $boolDirectSet ) $this->set( 'm_strAchSecCode', trim( $arrValues['ach_sec_code'] ) ); elseif( isset( $arrValues['ach_sec_code'] ) ) $this->setAchSecCode( $arrValues['ach_sec_code'] );
		if( isset( $arrValues['sftp_company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intSftpCompanyTransmissionVendorId', trim( $arrValues['sftp_company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['sftp_company_transmission_vendor_id'] ) ) $this->setSftpCompanyTransmissionVendorId( $arrValues['sftp_company_transmission_vendor_id'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( $arrValues['cc_card_number_encrypted'] ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( $arrValues['cc_exp_date_month'] ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( $arrValues['cc_exp_date_year'] ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_first_name'] ) && $boolDirectSet ) $this->set( 'm_strCcFirstName', trim( $arrValues['cc_first_name'] ) ); elseif( isset( $arrValues['cc_first_name'] ) ) $this->setCcFirstName( $arrValues['cc_first_name'] );
		if( isset( $arrValues['cc_last_name'] ) && $boolDirectSet ) $this->set( 'm_strCcLastName', trim( $arrValues['cc_last_name'] ) ); elseif( isset( $arrValues['cc_last_name'] ) ) $this->setCcLastName( $arrValues['cc_last_name'] );
		if( isset( $arrValues['is_wfpm_enable'] ) && $boolDirectSet ) $this->set( 'm_intIsWfpmEnable', trim( $arrValues['is_wfpm_enable'] ) ); elseif( isset( $arrValues['is_wfpm_enable'] ) ) $this->setIsWfpmEnable( $arrValues['is_wfpm_enable'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['is_create_invoice_at_payment'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreateInvoiceAtPayment', trim( stripcslashes( $arrValues['is_create_invoice_at_payment'] ) ) ); elseif( isset( $arrValues['is_create_invoice_at_payment'] ) ) $this->setIsCreateInvoiceAtPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_create_invoice_at_payment'] ) : $arrValues['is_create_invoice_at_payment'] );
		if( isset( $arrValues['is_separate_batch_per_payment'] ) && $boolDirectSet ) $this->set( 'm_boolIsSeparateBatchPerPayment', trim( stripcslashes( $arrValues['is_separate_batch_per_payment'] ) ) ); elseif( isset( $arrValues['is_separate_batch_per_payment'] ) ) $this->setIsSeparateBatchPerPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_separate_batch_per_payment'] ) : $arrValues['is_separate_batch_per_payment'] );
		if( isset( $arrValues['is_use_subledgers'] ) && $boolDirectSet ) $this->set( 'm_boolIsUseSubledgers', trim( stripcslashes( $arrValues['is_use_subledgers'] ) ) ); elseif( isset( $arrValues['is_use_subledgers'] ) ) $this->setIsUseSubledgers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_use_subledgers'] ) : $arrValues['is_use_subledgers'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_auto_generate_reimbursement_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoGenerateReimbursementInvoice', trim( stripcslashes( $arrValues['is_auto_generate_reimbursement_invoice'] ) ) ); elseif( isset( $arrValues['is_auto_generate_reimbursement_invoice'] ) ) $this->setIsAutoGenerateReimbursementInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_generate_reimbursement_invoice'] ) : $arrValues['is_auto_generate_reimbursement_invoice'] );
		if( isset( $arrValues['sender_id'] ) && $boolDirectSet ) $this->set( 'm_strSenderId', trim( $arrValues['sender_id'] ) ); elseif( isset( $arrValues['sender_id'] ) ) $this->setSenderId( $arrValues['sender_id'] );
		if( isset( $arrValues['client_id'] ) && $boolDirectSet ) $this->set( 'm_strClientId', trim( $arrValues['client_id'] ) ); elseif( isset( $arrValues['client_id'] ) ) $this->setClientId( $arrValues['client_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setBankAccountTypeId( $intBankAccountTypeId ) {
		$this->set( 'm_intBankAccountTypeId', CStrings::strToIntDef( $intBankAccountTypeId, NULL, false ) );
	}

	public function getBankAccountTypeId() {
		return $this->m_intBankAccountTypeId;
	}

	public function sqlBankAccountTypeId() {
		return ( true == isset( $this->m_intBankAccountTypeId ) ) ? ( string ) $this->m_intBankAccountTypeId : 'NULL';
	}

	public function setBankLoginTypeId( $intBankLoginTypeId ) {
		$this->set( 'm_intBankLoginTypeId', CStrings::strToIntDef( $intBankLoginTypeId, NULL, false ) );
	}

	public function getBankLoginTypeId() {
		return $this->m_intBankLoginTypeId;
	}

	public function sqlBankLoginTypeId() {
		return ( true == isset( $this->m_intBankLoginTypeId ) ) ? ( string ) $this->m_intBankLoginTypeId : 'NULL';
	}

	public function setCheckTemplateId( $intCheckTemplateId ) {
		$this->set( 'm_intCheckTemplateId', CStrings::strToIntDef( $intCheckTemplateId, NULL, false ) );
	}

	public function getCheckTemplateId() {
		return $this->m_intCheckTemplateId;
	}

	public function sqlCheckTemplateId() {
		return ( true == isset( $this->m_intCheckTemplateId ) ) ? ( string ) $this->m_intCheckTemplateId : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setApAchTypeId( $intApAchTypeId ) {
		$this->set( 'm_intApAchTypeId', CStrings::strToIntDef( $intApAchTypeId, NULL, false ) );
	}

	public function getApAchTypeId() {
		return $this->m_intApAchTypeId;
	}

	public function sqlApAchTypeId() {
		return ( true == isset( $this->m_intApAchTypeId ) ) ? ( string ) $this->m_intApAchTypeId : '1';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setAccountName( $strAccountName ) {
		$this->set( 'm_strAccountName', CStrings::strTrimDef( $strAccountName, 50, NULL, true ) );
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function sqlAccountName() {
		return ( true == isset( $this->m_strAccountName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountName ) : '\'' . addslashes( $this->m_strAccountName ) . '\'' ) : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 50, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setBankContactNameFirstEncrypted( $strBankContactNameFirstEncrypted ) {
		$this->set( 'm_strBankContactNameFirstEncrypted', CStrings::strTrimDef( $strBankContactNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getBankContactNameFirstEncrypted() {
		return $this->m_strBankContactNameFirstEncrypted;
	}

	public function sqlBankContactNameFirstEncrypted() {
		return ( true == isset( $this->m_strBankContactNameFirstEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankContactNameFirstEncrypted ) : '\'' . addslashes( $this->m_strBankContactNameFirstEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBankContactNameLastEncrypted( $strBankContactNameLastEncrypted ) {
		$this->set( 'm_strBankContactNameLastEncrypted', CStrings::strTrimDef( $strBankContactNameLastEncrypted, 240, NULL, true ) );
	}

	public function getBankContactNameLastEncrypted() {
		return $this->m_strBankContactNameLastEncrypted;
	}

	public function sqlBankContactNameLastEncrypted() {
		return ( true == isset( $this->m_strBankContactNameLastEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankContactNameLastEncrypted ) : '\'' . addslashes( $this->m_strBankContactNameLastEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBankStreetLine1( $strBankStreetLine1 ) {
		$this->set( 'm_strBankStreetLine1', CStrings::strTrimDef( $strBankStreetLine1, 100, NULL, true ) );
	}

	public function getBankStreetLine1() {
		return $this->m_strBankStreetLine1;
	}

	public function sqlBankStreetLine1() {
		return ( true == isset( $this->m_strBankStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankStreetLine1 ) : '\'' . addslashes( $this->m_strBankStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setBankStreetLine2( $strBankStreetLine2 ) {
		$this->set( 'm_strBankStreetLine2', CStrings::strTrimDef( $strBankStreetLine2, 100, NULL, true ) );
	}

	public function getBankStreetLine2() {
		return $this->m_strBankStreetLine2;
	}

	public function sqlBankStreetLine2() {
		return ( true == isset( $this->m_strBankStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankStreetLine2 ) : '\'' . addslashes( $this->m_strBankStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setBankStateCode( $strBankStateCode ) {
		$this->set( 'm_strBankStateCode', CStrings::strTrimDef( $strBankStateCode, 2, NULL, true ) );
	}

	public function getBankStateCode() {
		return $this->m_strBankStateCode;
	}

	public function sqlBankStateCode() {
		return ( true == isset( $this->m_strBankStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankStateCode ) : '\'' . addslashes( $this->m_strBankStateCode ) . '\'' ) : 'NULL';
	}

	public function setBankPostalCode( $strBankPostalCode ) {
		$this->set( 'm_strBankPostalCode', CStrings::strTrimDef( $strBankPostalCode, 20, NULL, true ) );
	}

	public function getBankPostalCode() {
		return $this->m_strBankPostalCode;
	}

	public function sqlBankPostalCode() {
		return ( true == isset( $this->m_strBankPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankPostalCode ) : '\'' . addslashes( $this->m_strBankPostalCode ) . '\'' ) : 'NULL';
	}

	public function setBankCity( $strBankCity ) {
		$this->set( 'm_strBankCity', CStrings::strTrimDef( $strBankCity, 50, NULL, true ) );
	}

	public function getBankCity() {
		return $this->m_strBankCity;
	}

	public function sqlBankCity() {
		return ( true == isset( $this->m_strBankCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankCity ) : '\'' . addslashes( $this->m_strBankCity ) . '\'' ) : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAction ) : '\'' . addslashes( $this->m_strAction ) . '\'' ) : 'NULL';
	}

	public function setBankPhoneNumberEncrypted( $strBankPhoneNumberEncrypted ) {
		$this->set( 'm_strBankPhoneNumberEncrypted', CStrings::strTrimDef( $strBankPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankPhoneNumberEncrypted() {
		return $this->m_strBankPhoneNumberEncrypted;
	}

	public function sqlBankPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strBankPhoneNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankPhoneNumberEncrypted ) : '\'' . addslashes( $this->m_strBankPhoneNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccount ) : '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setPayerStreetLine1( $strPayerStreetLine1 ) {
		$this->set( 'm_strPayerStreetLine1', CStrings::strTrimDef( $strPayerStreetLine1, 100, NULL, true ) );
	}

	public function getPayerStreetLine1() {
		return $this->m_strPayerStreetLine1;
	}

	public function sqlPayerStreetLine1() {
		return ( true == isset( $this->m_strPayerStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerStreetLine1 ) : '\'' . addslashes( $this->m_strPayerStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setPayerStreetLine2( $strPayerStreetLine2 ) {
		$this->set( 'm_strPayerStreetLine2', CStrings::strTrimDef( $strPayerStreetLine2, 100, NULL, true ) );
	}

	public function getPayerStreetLine2() {
		return $this->m_strPayerStreetLine2;
	}

	public function sqlPayerStreetLine2() {
		return ( true == isset( $this->m_strPayerStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerStreetLine2 ) : '\'' . addslashes( $this->m_strPayerStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setPayerCity( $strPayerCity ) {
		$this->set( 'm_strPayerCity', CStrings::strTrimDef( $strPayerCity, 50, NULL, true ) );
	}

	public function getPayerCity() {
		return $this->m_strPayerCity;
	}

	public function sqlPayerCity() {
		return ( true == isset( $this->m_strPayerCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerCity ) : '\'' . addslashes( $this->m_strPayerCity ) . '\'' ) : 'NULL';
	}

	public function setPayerStateCode( $strPayerStateCode ) {
		$this->set( 'm_strPayerStateCode', CStrings::strTrimDef( $strPayerStateCode, 2, NULL, true ) );
	}

	public function getPayerStateCode() {
		return $this->m_strPayerStateCode;
	}

	public function sqlPayerStateCode() {
		return ( true == isset( $this->m_strPayerStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerStateCode ) : '\'' . addslashes( $this->m_strPayerStateCode ) . '\'' ) : 'NULL';
	}

	public function setPayerPostalCode( $strPayerPostalCode ) {
		$this->set( 'm_strPayerPostalCode', CStrings::strTrimDef( $strPayerPostalCode, 20, NULL, true ) );
	}

	public function getPayerPostalCode() {
		return $this->m_strPayerPostalCode;
	}

	public function sqlPayerPostalCode() {
		return ( true == isset( $this->m_strPayerPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerPostalCode ) : '\'' . addslashes( $this->m_strPayerPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPayerPhoneNumberEncrypted( $strPayerPhoneNumberEncrypted ) {
		$this->set( 'm_strPayerPhoneNumberEncrypted', CStrings::strTrimDef( $strPayerPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getPayerPhoneNumberEncrypted() {
		return $this->m_strPayerPhoneNumberEncrypted;
	}

	public function sqlPayerPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strPayerPhoneNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayerPhoneNumberEncrypted ) : '\'' . addslashes( $this->m_strPayerPhoneNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 50, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckFractionalRoutingNumber( $strCheckFractionalRoutingNumber ) {
		$this->set( 'm_strCheckFractionalRoutingNumber', CStrings::strTrimDef( $strCheckFractionalRoutingNumber, 50, NULL, true ) );
	}

	public function getCheckFractionalRoutingNumber() {
		return $this->m_strCheckFractionalRoutingNumber;
	}

	public function sqlCheckFractionalRoutingNumber() {
		return ( true == isset( $this->m_strCheckFractionalRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckFractionalRoutingNumber ) : '\'' . addslashes( $this->m_strCheckFractionalRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberMasked( $strCheckAccountNumberMasked ) {
		$this->set( 'm_strCheckAccountNumberMasked', CStrings::strTrimDef( $strCheckAccountNumberMasked, 50, NULL, true ) );
	}

	public function getCheckAccountNumberMasked() {
		return $this->m_strCheckAccountNumberMasked;
	}

	public function sqlCheckAccountNumberMasked() {
		return ( true == isset( $this->m_strCheckAccountNumberMasked ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberMasked ) : '\'' . addslashes( $this->m_strCheckAccountNumberMasked ) . '\'' ) : 'NULL';
	}

	public function setCheckReorderReminderDate( $strCheckReorderReminderDate ) {
		$this->set( 'm_strCheckReorderReminderDate', CStrings::strTrimDef( $strCheckReorderReminderDate, -1, NULL, true ) );
	}

	public function getCheckReorderReminderDate() {
		return $this->m_strCheckReorderReminderDate;
	}

	public function sqlCheckReorderReminderDate() {
		return ( true == isset( $this->m_strCheckReorderReminderDate ) ) ? '\'' . $this->m_strCheckReorderReminderDate . '\'' : 'NULL';
	}

	public function setCheckReorderReminderNumber( $intCheckReorderReminderNumber ) {
		$this->set( 'm_intCheckReorderReminderNumber', CStrings::strToIntDef( $intCheckReorderReminderNumber, NULL, false ) );
	}

	public function getCheckReorderReminderNumber() {
		return $this->m_intCheckReorderReminderNumber;
	}

	public function sqlCheckReorderReminderNumber() {
		return ( true == isset( $this->m_intCheckReorderReminderNumber ) ) ? ( string ) $this->m_intCheckReorderReminderNumber : 'NULL';
	}

	public function setNextCheckNumber( $intNextCheckNumber ) {
		$this->set( 'm_intNextCheckNumber', CStrings::strToIntDef( $intNextCheckNumber, NULL, false ) );
	}

	public function getNextCheckNumber() {
		return $this->m_intNextCheckNumber;
	}

	public function sqlNextCheckNumber() {
		return ( true == isset( $this->m_intNextCheckNumber ) ) ? ( string ) $this->m_intNextCheckNumber : '0';
	}

	public function setMinimumBalance( $fltMinimumBalance ) {
		$this->set( 'm_fltMinimumBalance', CStrings::strToFloatDef( $fltMinimumBalance, NULL, false, 2 ) );
	}

	public function getMinimumBalance() {
		return $this->m_fltMinimumBalance;
	}

	public function sqlMinimumBalance() {
		return ( true == isset( $this->m_fltMinimumBalance ) ) ? ( string ) $this->m_fltMinimumBalance : '0';
	}

	public function setPrintSignatureLimitAmount( $fltPrintSignatureLimitAmount ) {
		$this->set( 'm_fltPrintSignatureLimitAmount', CStrings::strToFloatDef( $fltPrintSignatureLimitAmount, NULL, false, 2 ) );
	}

	public function getPrintSignatureLimitAmount() {
		return $this->m_fltPrintSignatureLimitAmount;
	}

	public function sqlPrintSignatureLimitAmount() {
		return ( true == isset( $this->m_fltPrintSignatureLimitAmount ) ) ? ( string ) $this->m_fltPrintSignatureLimitAmount : 'NULL';
	}

	public function setSecondSignatureLimitAmount( $fltSecondSignatureLimitAmount ) {
		$this->set( 'm_fltSecondSignatureLimitAmount', CStrings::strToFloatDef( $fltSecondSignatureLimitAmount, NULL, false, 2 ) );
	}

	public function getSecondSignatureLimitAmount() {
		return $this->m_fltSecondSignatureLimitAmount;
	}

	public function sqlSecondSignatureLimitAmount() {
		return ( true == isset( $this->m_fltSecondSignatureLimitAmount ) ) ? ( string ) $this->m_fltSecondSignatureLimitAmount : 'NULL';
	}

	public function setReserveAmount( $fltReserveAmount ) {
		$this->set( 'm_fltReserveAmount', CStrings::strToFloatDef( $fltReserveAmount, NULL, false, 2 ) );
	}

	public function getReserveAmount() {
		return $this->m_fltReserveAmount;
	}

	public function sqlReserveAmount() {
		return ( true == isset( $this->m_fltReserveAmount ) ) ? ( string ) $this->m_fltReserveAmount : '0';
	}

	public function setPrintInvoiceDetails( $intPrintInvoiceDetails ) {
		$this->set( 'm_intPrintInvoiceDetails', CStrings::strToIntDef( $intPrintInvoiceDetails, NULL, false ) );
	}

	public function getPrintInvoiceDetails() {
		return $this->m_intPrintInvoiceDetails;
	}

	public function sqlPrintInvoiceDetails() {
		return ( true == isset( $this->m_intPrintInvoiceDetails ) ) ? ( string ) $this->m_intPrintInvoiceDetails : '1';
	}

	public function setLoginUri( $strLoginUri ) {
		$this->set( 'm_strLoginUri', CStrings::strTrimDef( $strLoginUri, 2000, NULL, true ) );
	}

	public function getLoginUri() {
		return $this->m_strLoginUri;
	}

	public function sqlLoginUri() {
		return ( true == isset( $this->m_strLoginUri ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLoginUri ) : '\'' . addslashes( $this->m_strLoginUri ) . '\'' ) : 'NULL';
	}

	public function setLoginUsername( $strLoginUsername ) {
		$this->set( 'm_strLoginUsername', CStrings::strTrimDef( $strLoginUsername, 50, NULL, true ) );
	}

	public function getLoginUsername() {
		return $this->m_strLoginUsername;
	}

	public function sqlLoginUsername() {
		return ( true == isset( $this->m_strLoginUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLoginUsername ) : '\'' . addslashes( $this->m_strLoginUsername ) . '\'' ) : 'NULL';
	}

	public function setLoginPasswordEncrypted( $strLoginPasswordEncrypted ) {
		$this->set( 'm_strLoginPasswordEncrypted', CStrings::strTrimDef( $strLoginPasswordEncrypted, 200, NULL, true ) );
	}

	public function getLoginPasswordEncrypted() {
		return $this->m_strLoginPasswordEncrypted;
	}

	public function sqlLoginPasswordEncrypted() {
		return ( true == isset( $this->m_strLoginPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLoginPasswordEncrypted ) : '\'' . addslashes( $this->m_strLoginPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setImmediateDestinationName( $strImmediateDestinationName ) {
		$this->set( 'm_strImmediateDestinationName', CStrings::strTrimDef( $strImmediateDestinationName, 23, NULL, true ) );
	}

	public function getImmediateDestinationName() {
		return $this->m_strImmediateDestinationName;
	}

	public function sqlImmediateDestinationName() {
		return ( true == isset( $this->m_strImmediateDestinationName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strImmediateDestinationName ) : '\'' . addslashes( $this->m_strImmediateDestinationName ) . '\'' ) : 'NULL';
	}

	public function setImmediateDestinationId( $strImmediateDestinationId ) {
		$this->set( 'm_strImmediateDestinationId', CStrings::strTrimDef( $strImmediateDestinationId, 10, NULL, true ) );
	}

	public function getImmediateDestinationId() {
		return $this->m_strImmediateDestinationId;
	}

	public function sqlImmediateDestinationId() {
		return ( true == isset( $this->m_strImmediateDestinationId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strImmediateDestinationId ) : '\'' . addslashes( $this->m_strImmediateDestinationId ) . '\'' ) : 'NULL';
	}

	public function setImmediateOriginId( $strImmediateOriginId ) {
		$this->set( 'm_strImmediateOriginId', CStrings::strTrimDef( $strImmediateOriginId, 10, NULL, true ) );
	}

	public function getImmediateOriginId() {
		return $this->m_strImmediateOriginId;
	}

	public function sqlImmediateOriginId() {
		return ( true == isset( $this->m_strImmediateOriginId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strImmediateOriginId ) : '\'' . addslashes( $this->m_strImmediateOriginId ) . '\'' ) : 'NULL';
	}

	public function setImmediateOriginName( $strImmediateOriginName ) {
		$this->set( 'm_strImmediateOriginName', CStrings::strTrimDef( $strImmediateOriginName, 23, NULL, true ) );
	}

	public function getImmediateOriginName() {
		return $this->m_strImmediateOriginName;
	}

	public function sqlImmediateOriginName() {
		return ( true == isset( $this->m_strImmediateOriginName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strImmediateOriginName ) : '\'' . addslashes( $this->m_strImmediateOriginName ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 16, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setCompanyId( $strCompanyId ) {
		$this->set( 'm_strCompanyId', CStrings::strTrimDef( $strCompanyId, 10, NULL, true ) );
	}

	public function getCompanyId() {
		return $this->m_strCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_strCompanyId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyId ) : '\'' . addslashes( $this->m_strCompanyId ) . '\'' ) : 'NULL';
	}

	public function setOriginatingDfi( $strOriginatingDfi ) {
		$this->set( 'm_strOriginatingDfi', CStrings::strTrimDef( $strOriginatingDfi, 9, NULL, true ) );
	}

	public function getOriginatingDfi() {
		return $this->m_strOriginatingDfi;
	}

	public function sqlOriginatingDfi() {
		return ( true == isset( $this->m_strOriginatingDfi ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginatingDfi ) : '\'' . addslashes( $this->m_strOriginatingDfi ) . '\'' ) : 'NULL';
	}

	public function setGiactVerificationResponse( $strGiactVerificationResponse ) {
		$this->set( 'm_strGiactVerificationResponse', CStrings::strTrimDef( $strGiactVerificationResponse, 500, NULL, true ) );
	}

	public function getGiactVerificationResponse() {
		return $this->m_strGiactVerificationResponse;
	}

	public function sqlGiactVerificationResponse() {
		return ( true == isset( $this->m_strGiactVerificationResponse ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGiactVerificationResponse ) : '\'' . addslashes( $this->m_strGiactVerificationResponse ) . '\'' ) : 'NULL';
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->set( 'm_boolIsVerified', CStrings::strToBool( $boolIsVerified ) );
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCreditCardAccount( $intIsCreditCardAccount ) {
		$this->set( 'm_intIsCreditCardAccount', CStrings::strToIntDef( $intIsCreditCardAccount, NULL, false ) );
	}

	public function getIsCreditCardAccount() {
		return $this->m_intIsCreditCardAccount;
	}

	public function sqlIsCreditCardAccount() {
		return ( true == isset( $this->m_intIsCreditCardAccount ) ) ? ( string ) $this->m_intIsCreditCardAccount : '0';
	}

	public function setIsStubStubCheckFormat( $intIsStubStubCheckFormat ) {
		$this->set( 'm_intIsStubStubCheckFormat', CStrings::strToIntDef( $intIsStubStubCheckFormat, NULL, false ) );
	}

	public function getIsStubStubCheckFormat() {
		return $this->m_intIsStubStubCheckFormat;
	}

	public function sqlIsStubStubCheckFormat() {
		return ( true == isset( $this->m_intIsStubStubCheckFormat ) ) ? ( string ) $this->m_intIsStubStubCheckFormat : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsBankRecOnPostMonth( $intIsBankRecOnPostMonth ) {
		$this->set( 'm_intIsBankRecOnPostMonth', CStrings::strToIntDef( $intIsBankRecOnPostMonth, NULL, false ) );
	}

	public function getIsBankRecOnPostMonth() {
		return $this->m_intIsBankRecOnPostMonth;
	}

	public function sqlIsBankRecOnPostMonth() {
		return ( true == isset( $this->m_intIsBankRecOnPostMonth ) ) ? ( string ) $this->m_intIsBankRecOnPostMonth : '0';
	}

	public function setIsCashBasis( $intIsCashBasis ) {
		$this->set( 'm_intIsCashBasis', CStrings::strToIntDef( $intIsCashBasis, NULL, false ) );
	}

	public function getIsCashBasis() {
		return $this->m_intIsCashBasis;
	}

	public function sqlIsCashBasis() {
		return ( true == isset( $this->m_intIsCashBasis ) ) ? ( string ) $this->m_intIsCashBasis : '1';
	}

	public function setIsAchEnable( $intIsAchEnable ) {
		$this->set( 'm_intIsAchEnable', CStrings::strToIntDef( $intIsAchEnable, NULL, false ) );
	}

	public function getIsAchEnable() {
		return $this->m_intIsAchEnable;
	}

	public function sqlIsAchEnable() {
		return ( true == isset( $this->m_intIsAchEnable ) ) ? ( string ) $this->m_intIsAchEnable : 'NULL';
	}

	public function setIsBalancedAchFile( $boolIsBalancedAchFile ) {
		$this->set( 'm_boolIsBalancedAchFile', CStrings::strToBool( $boolIsBalancedAchFile ) );
	}

	public function getIsBalancedAchFile() {
		return $this->m_boolIsBalancedAchFile;
	}

	public function sqlIsBalancedAchFile() {
		return ( true == isset( $this->m_boolIsBalancedAchFile ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBalancedAchFile ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsShowVendorAccountNumber( $boolIsShowVendorAccountNumber ) {
		$this->set( 'm_boolIsShowVendorAccountNumber', CStrings::strToBool( $boolIsShowVendorAccountNumber ) );
	}

	public function getIsShowVendorAccountNumber() {
		return $this->m_boolIsShowVendorAccountNumber;
	}

	public function sqlIsShowVendorAccountNumber() {
		return ( true == isset( $this->m_boolIsShowVendorAccountNumber ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowVendorAccountNumber ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWfpmCeoCompanyId( $strWfpmCeoCompanyId ) {
		$this->set( 'm_strWfpmCeoCompanyId', CStrings::strTrimDef( $strWfpmCeoCompanyId, 15, NULL, true ) );
	}

	public function getWfpmCeoCompanyId() {
		return $this->m_strWfpmCeoCompanyId;
	}

	public function sqlWfpmCeoCompanyId() {
		return ( true == isset( $this->m_strWfpmCeoCompanyId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWfpmCeoCompanyId ) : '\'' . addslashes( $this->m_strWfpmCeoCompanyId ) . '\'' ) : 'NULL';
	}

	public function setCheckLayoutId( $strCheckLayoutId ) {
		$this->set( 'm_strCheckLayoutId', CStrings::strTrimDef( $strCheckLayoutId, 18, NULL, true ) );
	}

	public function getCheckLayoutId() {
		return $this->m_strCheckLayoutId;
	}

	public function sqlCheckLayoutId() {
		return ( true == isset( $this->m_strCheckLayoutId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckLayoutId ) : '\'' . addslashes( $this->m_strCheckLayoutId ) . '\'' ) : 'NULL';
	}

	public function setOriginatingBankId( $strOriginatingBankId ) {
		$this->set( 'm_strOriginatingBankId', CStrings::strTrimDef( $strOriginatingBankId, 11, NULL, true ) );
	}

	public function getOriginatingBankId() {
		return $this->m_strOriginatingBankId;
	}

	public function sqlOriginatingBankId() {
		return ( true == isset( $this->m_strOriginatingBankId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOriginatingBankId ) : '\'' . addslashes( $this->m_strOriginatingBankId ) . '\'' ) : 'NULL';
	}

	public function setAchCompanyId( $strAchCompanyId ) {
		$this->set( 'm_strAchCompanyId', CStrings::strTrimDef( $strAchCompanyId, 10, NULL, true ) );
	}

	public function getAchCompanyId() {
		return $this->m_strAchCompanyId;
	}

	public function sqlAchCompanyId() {
		return ( true == isset( $this->m_strAchCompanyId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAchCompanyId ) : '\'' . addslashes( $this->m_strAchCompanyId ) . '\'' ) : 'NULL';
	}

	public function setAchSecCode( $strAchSecCode ) {
		$this->set( 'm_strAchSecCode', CStrings::strTrimDef( $strAchSecCode, 4, NULL, true ) );
	}

	public function getAchSecCode() {
		return $this->m_strAchSecCode;
	}

	public function sqlAchSecCode() {
		return ( true == isset( $this->m_strAchSecCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAchSecCode ) : '\'' . addslashes( $this->m_strAchSecCode ) . '\'' ) : 'NULL';
	}

	public function setSftpCompanyTransmissionVendorId( $intSftpCompanyTransmissionVendorId ) {
		$this->set( 'm_intSftpCompanyTransmissionVendorId', CStrings::strToIntDef( $intSftpCompanyTransmissionVendorId, NULL, false ) );
	}

	public function getSftpCompanyTransmissionVendorId() {
		return $this->m_intSftpCompanyTransmissionVendorId;
	}

	public function sqlSftpCompanyTransmissionVendorId() {
		return ( true == isset( $this->m_intSftpCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intSftpCompanyTransmissionVendorId : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcCardNumberEncrypted ) : '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 4, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateMonth ) : '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateYear ) : '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' ) : 'NULL';
	}

	public function setCcFirstName( $strCcFirstName ) {
		$this->set( 'm_strCcFirstName', CStrings::strTrimDef( $strCcFirstName, 50, NULL, true ) );
	}

	public function getCcFirstName() {
		return $this->m_strCcFirstName;
	}

	public function sqlCcFirstName() {
		return ( true == isset( $this->m_strCcFirstName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcFirstName ) : '\'' . addslashes( $this->m_strCcFirstName ) . '\'' ) : 'NULL';
	}

	public function setCcLastName( $strCcLastName ) {
		$this->set( 'm_strCcLastName', CStrings::strTrimDef( $strCcLastName, 50, NULL, true ) );
	}

	public function getCcLastName() {
		return $this->m_strCcLastName;
	}

	public function sqlCcLastName() {
		return ( true == isset( $this->m_strCcLastName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcLastName ) : '\'' . addslashes( $this->m_strCcLastName ) . '\'' ) : 'NULL';
	}

	public function setIsWfpmEnable( $intIsWfpmEnable ) {
		$this->set( 'm_intIsWfpmEnable', CStrings::strToIntDef( $intIsWfpmEnable, NULL, false ) );
	}

	public function getIsWfpmEnable() {
		return $this->m_intIsWfpmEnable;
	}

	public function sqlIsWfpmEnable() {
		return ( true == isset( $this->m_intIsWfpmEnable ) ) ? ( string ) $this->m_intIsWfpmEnable : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setIsCreateInvoiceAtPayment( $boolIsCreateInvoiceAtPayment ) {
		$this->set( 'm_boolIsCreateInvoiceAtPayment', CStrings::strToBool( $boolIsCreateInvoiceAtPayment ) );
	}

	public function getIsCreateInvoiceAtPayment() {
		return $this->m_boolIsCreateInvoiceAtPayment;
	}

	public function sqlIsCreateInvoiceAtPayment() {
		return ( true == isset( $this->m_boolIsCreateInvoiceAtPayment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreateInvoiceAtPayment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSeparateBatchPerPayment( $boolIsSeparateBatchPerPayment ) {
		$this->set( 'm_boolIsSeparateBatchPerPayment', CStrings::strToBool( $boolIsSeparateBatchPerPayment ) );
	}

	public function getIsSeparateBatchPerPayment() {
		return $this->m_boolIsSeparateBatchPerPayment;
	}

	public function sqlIsSeparateBatchPerPayment() {
		return ( true == isset( $this->m_boolIsSeparateBatchPerPayment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSeparateBatchPerPayment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUseSubledgers( $boolIsUseSubledgers ) {
		$this->set( 'm_boolIsUseSubledgers', CStrings::strToBool( $boolIsUseSubledgers ) );
	}

	public function getIsUseSubledgers() {
		return $this->m_boolIsUseSubledgers;
	}

	public function sqlIsUseSubledgers() {
		return ( true == isset( $this->m_boolIsUseSubledgers ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUseSubledgers ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoGenerateReimbursementInvoice( $boolIsAutoGenerateReimbursementInvoice ) {
		$this->set( 'm_boolIsAutoGenerateReimbursementInvoice', CStrings::strToBool( $boolIsAutoGenerateReimbursementInvoice ) );
	}

	public function getIsAutoGenerateReimbursementInvoice() {
		return $this->m_boolIsAutoGenerateReimbursementInvoice;
	}

	public function sqlIsAutoGenerateReimbursementInvoice() {
		return ( true == isset( $this->m_boolIsAutoGenerateReimbursementInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoGenerateReimbursementInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSenderId( $strSenderId ) {
		$this->set( 'm_strSenderId', CStrings::strTrimDef( $strSenderId, -1, NULL, true ) );
	}

	public function getSenderId() {
		return $this->m_strSenderId;
	}

	public function sqlSenderId() {
		return ( true == isset( $this->m_strSenderId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSenderId ) : '\'' . addslashes( $this->m_strSenderId ) . '\'' ) : 'NULL';
	}

	public function setClientId( $strClientId ) {
		$this->set( 'm_strClientId', CStrings::strTrimDef( $strClientId, -1, NULL, true ) );
	}

	public function getClientId() {
		return $this->m_strClientId;
	}

	public function sqlClientId() {
		return ( true == isset( $this->m_strClientId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strClientId ) : '\'' . addslashes( $this->m_strClientId ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, bank_account_id, bank_account_type_id, bank_login_type_id, check_template_id, check_account_type_id, transmission_vendor_id, ap_ach_type_id, remote_primary_key, account_name, check_bank_name, bank_contact_name_first_encrypted, bank_contact_name_last_encrypted, bank_street_line1, bank_street_line2, bank_state_code, bank_postal_code, bank_city, action, bank_phone_number_encrypted, check_name_on_account, payer_street_line1, payer_street_line2, payer_city, payer_state_code, payer_postal_code, payer_phone_number_encrypted, check_routing_number, check_fractional_routing_number, check_account_number_encrypted, check_account_number_masked, check_reorder_reminder_date, check_reorder_reminder_number, next_check_number, minimum_balance, print_signature_limit_amount, second_signature_limit_amount, reserve_amount, print_invoice_details, login_uri, login_username, login_password_encrypted, immediate_destination_name, immediate_destination_id, immediate_origin_id, immediate_origin_name, company_name, company_id, originating_dfi, giact_verification_response, is_verified, is_credit_card_account, is_stub_stub_check_format, is_system, is_disabled, is_bank_rec_on_post_month, is_cash_basis, is_ach_enable, is_balanced_ach_file, is_show_vendor_account_number, verified_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, wfpm_ceo_company_id, check_layout_id, originating_bank_id, ach_company_id, ach_sec_code, sftp_company_transmission_vendor_id, secure_reference_number, cc_card_number_encrypted, cc_exp_date_month, cc_exp_date_year, cc_first_name, cc_last_name, is_wfpm_enable, ap_payee_id, ap_payee_location_id, ap_payee_account_id, is_create_invoice_at_payment, is_separate_batch_per_payment, is_use_subledgers, details, is_auto_generate_reimbursement_invoice, sender_id, client_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlBankAccountTypeId() . ', ' .
						$this->sqlBankLoginTypeId() . ', ' .
						$this->sqlCheckTemplateId() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlApAchTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlAccountName() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlBankContactNameFirstEncrypted() . ', ' .
						$this->sqlBankContactNameLastEncrypted() . ', ' .
						$this->sqlBankStreetLine1() . ', ' .
						$this->sqlBankStreetLine2() . ', ' .
						$this->sqlBankStateCode() . ', ' .
						$this->sqlBankPostalCode() . ', ' .
						$this->sqlBankCity() . ', ' .
						$this->sqlAction() . ', ' .
						$this->sqlBankPhoneNumberEncrypted() . ', ' .
						$this->sqlCheckNameOnAccount() . ', ' .
						$this->sqlPayerStreetLine1() . ', ' .
						$this->sqlPayerStreetLine2() . ', ' .
						$this->sqlPayerCity() . ', ' .
						$this->sqlPayerStateCode() . ', ' .
						$this->sqlPayerPostalCode() . ', ' .
						$this->sqlPayerPhoneNumberEncrypted() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckFractionalRoutingNumber() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlCheckAccountNumberMasked() . ', ' .
						$this->sqlCheckReorderReminderDate() . ', ' .
						$this->sqlCheckReorderReminderNumber() . ', ' .
						$this->sqlNextCheckNumber() . ', ' .
						$this->sqlMinimumBalance() . ', ' .
						$this->sqlPrintSignatureLimitAmount() . ', ' .
						$this->sqlSecondSignatureLimitAmount() . ', ' .
						$this->sqlReserveAmount() . ', ' .
						$this->sqlPrintInvoiceDetails() . ', ' .
						$this->sqlLoginUri() . ', ' .
						$this->sqlLoginUsername() . ', ' .
						$this->sqlLoginPasswordEncrypted() . ', ' .
						$this->sqlImmediateDestinationName() . ', ' .
						$this->sqlImmediateDestinationId() . ', ' .
						$this->sqlImmediateOriginId() . ', ' .
						$this->sqlImmediateOriginName() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlCompanyId() . ', ' .
						$this->sqlOriginatingDfi() . ', ' .
						$this->sqlGiactVerificationResponse() . ', ' .
						$this->sqlIsVerified() . ', ' .
						$this->sqlIsCreditCardAccount() . ', ' .
						$this->sqlIsStubStubCheckFormat() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsBankRecOnPostMonth() . ', ' .
						$this->sqlIsCashBasis() . ', ' .
						$this->sqlIsAchEnable() . ', ' .
						$this->sqlIsBalancedAchFile() . ', ' .
						$this->sqlIsShowVendorAccountNumber() . ', ' .
						$this->sqlVerifiedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlWfpmCeoCompanyId() . ', ' .
						$this->sqlCheckLayoutId() . ', ' .
						$this->sqlOriginatingBankId() . ', ' .
						$this->sqlAchCompanyId() . ', ' .
						$this->sqlAchSecCode() . ', ' .
						$this->sqlSftpCompanyTransmissionVendorId() . ', ' .
						$this->sqlSecureReferenceNumber() . ', ' .
						$this->sqlCcCardNumberEncrypted() . ', ' .
						$this->sqlCcExpDateMonth() . ', ' .
						$this->sqlCcExpDateYear() . ', ' .
						$this->sqlCcFirstName() . ', ' .
						$this->sqlCcLastName() . ', ' .
						$this->sqlIsWfpmEnable() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlIsCreateInvoiceAtPayment() . ', ' .
						$this->sqlIsSeparateBatchPerPayment() . ', ' .
						$this->sqlIsUseSubledgers() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsAutoGenerateReimbursementInvoice() . ', ' .
						$this->sqlSenderId() . ', ' .
						$this->sqlClientId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_type_id = ' . $this->sqlBankAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'BankAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_type_id = ' . $this->sqlBankAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_login_type_id = ' . $this->sqlBankLoginTypeId(). ',' ; } elseif( true == array_key_exists( 'BankLoginTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bank_login_type_id = ' . $this->sqlBankLoginTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_template_id = ' . $this->sqlCheckTemplateId(). ',' ; } elseif( true == array_key_exists( 'CheckTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' check_template_id = ' . $this->sqlCheckTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_ach_type_id = ' . $this->sqlApAchTypeId(). ',' ; } elseif( true == array_key_exists( 'ApAchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_ach_type_id = ' . $this->sqlApAchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_name = ' . $this->sqlAccountName(). ',' ; } elseif( true == array_key_exists( 'AccountName', $this->getChangedColumns() ) ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_contact_name_first_encrypted = ' . $this->sqlBankContactNameFirstEncrypted(). ',' ; } elseif( true == array_key_exists( 'BankContactNameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_contact_name_first_encrypted = ' . $this->sqlBankContactNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_contact_name_last_encrypted = ' . $this->sqlBankContactNameLastEncrypted(). ',' ; } elseif( true == array_key_exists( 'BankContactNameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_contact_name_last_encrypted = ' . $this->sqlBankContactNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_street_line1 = ' . $this->sqlBankStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BankStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' bank_street_line1 = ' . $this->sqlBankStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_street_line2 = ' . $this->sqlBankStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BankStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' bank_street_line2 = ' . $this->sqlBankStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_state_code = ' . $this->sqlBankStateCode(). ',' ; } elseif( true == array_key_exists( 'BankStateCode', $this->getChangedColumns() ) ) { $strSql .= ' bank_state_code = ' . $this->sqlBankStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_postal_code = ' . $this->sqlBankPostalCode(). ',' ; } elseif( true == array_key_exists( 'BankPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' bank_postal_code = ' . $this->sqlBankPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_city = ' . $this->sqlBankCity(). ',' ; } elseif( true == array_key_exists( 'BankCity', $this->getChangedColumns() ) ) { $strSql .= ' bank_city = ' . $this->sqlBankCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction(). ',' ; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_phone_number_encrypted = ' . $this->sqlBankPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'BankPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_phone_number_encrypted = ' . $this->sqlBankPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_street_line1 = ' . $this->sqlPayerStreetLine1(). ',' ; } elseif( true == array_key_exists( 'PayerStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' payer_street_line1 = ' . $this->sqlPayerStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_street_line2 = ' . $this->sqlPayerStreetLine2(). ',' ; } elseif( true == array_key_exists( 'PayerStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' payer_street_line2 = ' . $this->sqlPayerStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_city = ' . $this->sqlPayerCity(). ',' ; } elseif( true == array_key_exists( 'PayerCity', $this->getChangedColumns() ) ) { $strSql .= ' payer_city = ' . $this->sqlPayerCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_state_code = ' . $this->sqlPayerStateCode(). ',' ; } elseif( true == array_key_exists( 'PayerStateCode', $this->getChangedColumns() ) ) { $strSql .= ' payer_state_code = ' . $this->sqlPayerStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_postal_code = ' . $this->sqlPayerPostalCode(). ',' ; } elseif( true == array_key_exists( 'PayerPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' payer_postal_code = ' . $this->sqlPayerPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_phone_number_encrypted = ' . $this->sqlPayerPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'PayerPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' payer_phone_number_encrypted = ' . $this->sqlPayerPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_fractional_routing_number = ' . $this->sqlCheckFractionalRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckFractionalRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_fractional_routing_number = ' . $this->sqlCheckFractionalRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_masked = ' . $this->sqlCheckAccountNumberMasked(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_masked = ' . $this->sqlCheckAccountNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_reorder_reminder_date = ' . $this->sqlCheckReorderReminderDate(). ',' ; } elseif( true == array_key_exists( 'CheckReorderReminderDate', $this->getChangedColumns() ) ) { $strSql .= ' check_reorder_reminder_date = ' . $this->sqlCheckReorderReminderDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_reorder_reminder_number = ' . $this->sqlCheckReorderReminderNumber(). ',' ; } elseif( true == array_key_exists( 'CheckReorderReminderNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_reorder_reminder_number = ' . $this->sqlCheckReorderReminderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_check_number = ' . $this->sqlNextCheckNumber(). ',' ; } elseif( true == array_key_exists( 'NextCheckNumber', $this->getChangedColumns() ) ) { $strSql .= ' next_check_number = ' . $this->sqlNextCheckNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_balance = ' . $this->sqlMinimumBalance(). ',' ; } elseif( true == array_key_exists( 'MinimumBalance', $this->getChangedColumns() ) ) { $strSql .= ' minimum_balance = ' . $this->sqlMinimumBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' print_signature_limit_amount = ' . $this->sqlPrintSignatureLimitAmount(). ',' ; } elseif( true == array_key_exists( 'PrintSignatureLimitAmount', $this->getChangedColumns() ) ) { $strSql .= ' print_signature_limit_amount = ' . $this->sqlPrintSignatureLimitAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' second_signature_limit_amount = ' . $this->sqlSecondSignatureLimitAmount(). ',' ; } elseif( true == array_key_exists( 'SecondSignatureLimitAmount', $this->getChangedColumns() ) ) { $strSql .= ' second_signature_limit_amount = ' . $this->sqlSecondSignatureLimitAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserve_amount = ' . $this->sqlReserveAmount(). ',' ; } elseif( true == array_key_exists( 'ReserveAmount', $this->getChangedColumns() ) ) { $strSql .= ' reserve_amount = ' . $this->sqlReserveAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' print_invoice_details = ' . $this->sqlPrintInvoiceDetails(). ',' ; } elseif( true == array_key_exists( 'PrintInvoiceDetails', $this->getChangedColumns() ) ) { $strSql .= ' print_invoice_details = ' . $this->sqlPrintInvoiceDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_uri = ' . $this->sqlLoginUri(). ',' ; } elseif( true == array_key_exists( 'LoginUri', $this->getChangedColumns() ) ) { $strSql .= ' login_uri = ' . $this->sqlLoginUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_username = ' . $this->sqlLoginUsername(). ',' ; } elseif( true == array_key_exists( 'LoginUsername', $this->getChangedColumns() ) ) { $strSql .= ' login_username = ' . $this->sqlLoginUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_password_encrypted = ' . $this->sqlLoginPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'LoginPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' login_password_encrypted = ' . $this->sqlLoginPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_destination_name = ' . $this->sqlImmediateDestinationName(). ',' ; } elseif( true == array_key_exists( 'ImmediateDestinationName', $this->getChangedColumns() ) ) { $strSql .= ' immediate_destination_name = ' . $this->sqlImmediateDestinationName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_destination_id = ' . $this->sqlImmediateDestinationId(). ',' ; } elseif( true == array_key_exists( 'ImmediateDestinationId', $this->getChangedColumns() ) ) { $strSql .= ' immediate_destination_id = ' . $this->sqlImmediateDestinationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_origin_id = ' . $this->sqlImmediateOriginId(). ',' ; } elseif( true == array_key_exists( 'ImmediateOriginId', $this->getChangedColumns() ) ) { $strSql .= ' immediate_origin_id = ' . $this->sqlImmediateOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' immediate_origin_name = ' . $this->sqlImmediateOriginName(). ',' ; } elseif( true == array_key_exists( 'ImmediateOriginName', $this->getChangedColumns() ) ) { $strSql .= ' immediate_origin_name = ' . $this->sqlImmediateOriginName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId(). ',' ; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_dfi = ' . $this->sqlOriginatingDfi(). ',' ; } elseif( true == array_key_exists( 'OriginatingDfi', $this->getChangedColumns() ) ) { $strSql .= ' originating_dfi = ' . $this->sqlOriginatingDfi() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' giact_verification_response = ' . $this->sqlGiactVerificationResponse(). ',' ; } elseif( true == array_key_exists( 'GiactVerificationResponse', $this->getChangedColumns() ) ) { $strSql .= ' giact_verification_response = ' . $this->sqlGiactVerificationResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified(). ',' ; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_card_account = ' . $this->sqlIsCreditCardAccount(). ',' ; } elseif( true == array_key_exists( 'IsCreditCardAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_card_account = ' . $this->sqlIsCreditCardAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_stub_stub_check_format = ' . $this->sqlIsStubStubCheckFormat(). ',' ; } elseif( true == array_key_exists( 'IsStubStubCheckFormat', $this->getChangedColumns() ) ) { $strSql .= ' is_stub_stub_check_format = ' . $this->sqlIsStubStubCheckFormat() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bank_rec_on_post_month = ' . $this->sqlIsBankRecOnPostMonth(). ',' ; } elseif( true == array_key_exists( 'IsBankRecOnPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_bank_rec_on_post_month = ' . $this->sqlIsBankRecOnPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis(). ',' ; } elseif( true == array_key_exists( 'IsCashBasis', $this->getChangedColumns() ) ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ach_enable = ' . $this->sqlIsAchEnable(). ',' ; } elseif( true == array_key_exists( 'IsAchEnable', $this->getChangedColumns() ) ) { $strSql .= ' is_ach_enable = ' . $this->sqlIsAchEnable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_balanced_ach_file = ' . $this->sqlIsBalancedAchFile(). ',' ; } elseif( true == array_key_exists( 'IsBalancedAchFile', $this->getChangedColumns() ) ) { $strSql .= ' is_balanced_ach_file = ' . $this->sqlIsBalancedAchFile() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_vendor_account_number = ' . $this->sqlIsShowVendorAccountNumber(). ',' ; } elseif( true == array_key_exists( 'IsShowVendorAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' is_show_vendor_account_number = ' . $this->sqlIsShowVendorAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wfpm_ceo_company_id = ' . $this->sqlWfpmCeoCompanyId(). ',' ; } elseif( true == array_key_exists( 'WfpmCeoCompanyId', $this->getChangedColumns() ) ) { $strSql .= ' wfpm_ceo_company_id = ' . $this->sqlWfpmCeoCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_layout_id = ' . $this->sqlCheckLayoutId(). ',' ; } elseif( true == array_key_exists( 'CheckLayoutId', $this->getChangedColumns() ) ) { $strSql .= ' check_layout_id = ' . $this->sqlCheckLayoutId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_bank_id = ' . $this->sqlOriginatingBankId(). ',' ; } elseif( true == array_key_exists( 'OriginatingBankId', $this->getChangedColumns() ) ) { $strSql .= ' originating_bank_id = ' . $this->sqlOriginatingBankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_company_id = ' . $this->sqlAchCompanyId(). ',' ; } elseif( true == array_key_exists( 'AchCompanyId', $this->getChangedColumns() ) ) { $strSql .= ' ach_company_id = ' . $this->sqlAchCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_sec_code = ' . $this->sqlAchSecCode(). ',' ; } elseif( true == array_key_exists( 'AchSecCode', $this->getChangedColumns() ) ) { $strSql .= ' ach_sec_code = ' . $this->sqlAchSecCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sftp_company_transmission_vendor_id = ' . $this->sqlSftpCompanyTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'SftpCompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' sftp_company_transmission_vendor_id = ' . $this->sqlSftpCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CcCardNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth(). ',' ; } elseif( true == array_key_exists( 'CcExpDateMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear(). ',' ; } elseif( true == array_key_exists( 'CcExpDateYear', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_first_name = ' . $this->sqlCcFirstName(). ',' ; } elseif( true == array_key_exists( 'CcFirstName', $this->getChangedColumns() ) ) { $strSql .= ' cc_first_name = ' . $this->sqlCcFirstName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_last_name = ' . $this->sqlCcLastName(). ',' ; } elseif( true == array_key_exists( 'CcLastName', $this->getChangedColumns() ) ) { $strSql .= ' cc_last_name = ' . $this->sqlCcLastName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_wfpm_enable = ' . $this->sqlIsWfpmEnable(). ',' ; } elseif( true == array_key_exists( 'IsWfpmEnable', $this->getChangedColumns() ) ) { $strSql .= ' is_wfpm_enable = ' . $this->sqlIsWfpmEnable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_create_invoice_at_payment = ' . $this->sqlIsCreateInvoiceAtPayment(). ',' ; } elseif( true == array_key_exists( 'IsCreateInvoiceAtPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_create_invoice_at_payment = ' . $this->sqlIsCreateInvoiceAtPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_separate_batch_per_payment = ' . $this->sqlIsSeparateBatchPerPayment(). ',' ; } elseif( true == array_key_exists( 'IsSeparateBatchPerPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_separate_batch_per_payment = ' . $this->sqlIsSeparateBatchPerPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_use_subledgers = ' . $this->sqlIsUseSubledgers(). ',' ; } elseif( true == array_key_exists( 'IsUseSubledgers', $this->getChangedColumns() ) ) { $strSql .= ' is_use_subledgers = ' . $this->sqlIsUseSubledgers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_generate_reimbursement_invoice = ' . $this->sqlIsAutoGenerateReimbursementInvoice(). ',' ; } elseif( true == array_key_exists( 'IsAutoGenerateReimbursementInvoice', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_generate_reimbursement_invoice = ' . $this->sqlIsAutoGenerateReimbursementInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sender_id = ' . $this->sqlSenderId(). ',' ; } elseif( true == array_key_exists( 'SenderId', $this->getChangedColumns() ) ) { $strSql .= ' sender_id = ' . $this->sqlSenderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_id = ' . $this->sqlClientId(). ',' ; } elseif( true == array_key_exists( 'ClientId', $this->getChangedColumns() ) ) { $strSql .= ' client_id = ' . $this->sqlClientId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'bank_account_id' => $this->getBankAccountId(),
			'bank_account_type_id' => $this->getBankAccountTypeId(),
			'bank_login_type_id' => $this->getBankLoginTypeId(),
			'check_template_id' => $this->getCheckTemplateId(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'ap_ach_type_id' => $this->getApAchTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'account_name' => $this->getAccountName(),
			'check_bank_name' => $this->getCheckBankName(),
			'bank_contact_name_first_encrypted' => $this->getBankContactNameFirstEncrypted(),
			'bank_contact_name_last_encrypted' => $this->getBankContactNameLastEncrypted(),
			'bank_street_line1' => $this->getBankStreetLine1(),
			'bank_street_line2' => $this->getBankStreetLine2(),
			'bank_state_code' => $this->getBankStateCode(),
			'bank_postal_code' => $this->getBankPostalCode(),
			'bank_city' => $this->getBankCity(),
			'action' => $this->getAction(),
			'bank_phone_number_encrypted' => $this->getBankPhoneNumberEncrypted(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'payer_street_line1' => $this->getPayerStreetLine1(),
			'payer_street_line2' => $this->getPayerStreetLine2(),
			'payer_city' => $this->getPayerCity(),
			'payer_state_code' => $this->getPayerStateCode(),
			'payer_postal_code' => $this->getPayerPostalCode(),
			'payer_phone_number_encrypted' => $this->getPayerPhoneNumberEncrypted(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_fractional_routing_number' => $this->getCheckFractionalRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_account_number_masked' => $this->getCheckAccountNumberMasked(),
			'check_reorder_reminder_date' => $this->getCheckReorderReminderDate(),
			'check_reorder_reminder_number' => $this->getCheckReorderReminderNumber(),
			'next_check_number' => $this->getNextCheckNumber(),
			'minimum_balance' => $this->getMinimumBalance(),
			'print_signature_limit_amount' => $this->getPrintSignatureLimitAmount(),
			'second_signature_limit_amount' => $this->getSecondSignatureLimitAmount(),
			'reserve_amount' => $this->getReserveAmount(),
			'print_invoice_details' => $this->getPrintInvoiceDetails(),
			'login_uri' => $this->getLoginUri(),
			'login_username' => $this->getLoginUsername(),
			'login_password_encrypted' => $this->getLoginPasswordEncrypted(),
			'immediate_destination_name' => $this->getImmediateDestinationName(),
			'immediate_destination_id' => $this->getImmediateDestinationId(),
			'immediate_origin_id' => $this->getImmediateOriginId(),
			'immediate_origin_name' => $this->getImmediateOriginName(),
			'company_name' => $this->getCompanyName(),
			'company_id' => $this->getCompanyId(),
			'originating_dfi' => $this->getOriginatingDfi(),
			'giact_verification_response' => $this->getGiactVerificationResponse(),
			'is_verified' => $this->getIsVerified(),
			'is_credit_card_account' => $this->getIsCreditCardAccount(),
			'is_stub_stub_check_format' => $this->getIsStubStubCheckFormat(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'is_bank_rec_on_post_month' => $this->getIsBankRecOnPostMonth(),
			'is_cash_basis' => $this->getIsCashBasis(),
			'is_ach_enable' => $this->getIsAchEnable(),
			'is_balanced_ach_file' => $this->getIsBalancedAchFile(),
			'is_show_vendor_account_number' => $this->getIsShowVendorAccountNumber(),
			'verified_on' => $this->getVerifiedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'wfpm_ceo_company_id' => $this->getWfpmCeoCompanyId(),
			'check_layout_id' => $this->getCheckLayoutId(),
			'originating_bank_id' => $this->getOriginatingBankId(),
			'ach_company_id' => $this->getAchCompanyId(),
			'ach_sec_code' => $this->getAchSecCode(),
			'sftp_company_transmission_vendor_id' => $this->getSftpCompanyTransmissionVendorId(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_first_name' => $this->getCcFirstName(),
			'cc_last_name' => $this->getCcLastName(),
			'is_wfpm_enable' => $this->getIsWfpmEnable(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'is_create_invoice_at_payment' => $this->getIsCreateInvoiceAtPayment(),
			'is_separate_batch_per_payment' => $this->getIsSeparateBatchPerPayment(),
			'is_use_subledgers' => $this->getIsUseSubledgers(),
			'details' => $this->getDetails(),
			'is_auto_generate_reimbursement_invoice' => $this->getIsAutoGenerateReimbursementInvoice(),
			'sender_id' => $this->getSenderId(),
			'client_id' => $this->getClientId()
		);
	}

}
?>