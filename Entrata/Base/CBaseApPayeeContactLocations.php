<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeContactLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeContactLocations extends CEosPluralBase {

	/**
	 * @return CApPayeeContactLocation[]
	 */
	public static function fetchApPayeeContactLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayeeContactLocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeContactLocation
	 */
	public static function fetchApPayeeContactLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayeeContactLocation', $objDatabase );
	}

	public static function fetchApPayeeContactLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_contact_locations', $objDatabase );
	}

	public static function fetchApPayeeContactLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContactLocation( sprintf( 'SELECT * FROM ap_payee_contact_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeContactLocations( sprintf( 'SELECT * FROM ap_payee_contact_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactLocationsByApPayeeContactIdByCid( $intApPayeeContactId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContactLocations( sprintf( 'SELECT * FROM ap_payee_contact_locations WHERE ap_payee_contact_id = %d AND cid = %d', ( int ) $intApPayeeContactId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactLocationsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContactLocations( sprintf( 'SELECT * FROM ap_payee_contact_locations WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>