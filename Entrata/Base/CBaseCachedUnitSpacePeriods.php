<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedUnitSpacePeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedUnitSpacePeriods extends CEosPluralBase {

	/**
	 * @return CCachedUnitSpacePeriod[]
	 */
	public static function fetchCachedUnitSpacePeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCachedUnitSpacePeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedUnitSpacePeriod
	 */
	public static function fetchCachedUnitSpacePeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCachedUnitSpacePeriod', $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_unit_space_periods', $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriod( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitSpaceRateLogIdByCid( $intUnitSpaceRateLogId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_space_rate_log_id = %d AND cid = %d', ( int ) $intUnitSpaceRateLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitSpaceLogIdByCid( $intUnitSpaceLogId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_space_log_id = %d AND cid = %d', ( int ) $intUnitSpaceLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitSpaceStatusTypeIdByCid( $intUnitSpaceStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_space_status_type_id = %d AND cid = %d', ( int ) $intUnitSpaceStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByUnitExclusionReasonTypeIdByCid( $intUnitExclusionReasonTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE unit_exclusion_reason_type_id = %d AND cid = %d', ( int ) $intUnitExclusionReasonTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByGroupUnitSpaceIdByCid( $intGroupUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE group_unit_space_id = %d AND cid = %d', ( int ) $intGroupUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByApplicationLeaseIdByCid( $intApplicationLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE application_lease_id = %d AND cid = %d', ( int ) $intApplicationLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByHoldLeaseIdByCid( $intHoldLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE hold_lease_id = %d AND cid = %d', ( int ) $intHoldLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByFutureLeaseIdByCid( $intFutureLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE future_lease_id = %d AND cid = %d', ( int ) $intFutureLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByCurrentLeaseIdByCid( $intCurrentLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE current_lease_id = %d AND cid = %d', ( int ) $intCurrentLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedUnitSpacePeriodsByPastLeaseIdByCid( $intPastLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedUnitSpacePeriods( sprintf( 'SELECT * FROM cached_unit_space_periods WHERE past_lease_id = %d AND cid = %d', ( int ) $intPastLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>