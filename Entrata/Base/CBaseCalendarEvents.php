<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEvents extends CEosPluralBase {

	/**
	 * @return CCalendarEvent[]
	 */
	public static function fetchCalendarEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCalendarEvent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCalendarEvent
	 */
	public static function fetchCalendarEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCalendarEvent', $objDatabase );
	}

	public static function fetchCalendarEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calendar_events', $objDatabase );
	}

	public static function fetchCalendarEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvent( sprintf( 'SELECT * FROM calendar_events WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByCid( $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByCalendarEventCategoryIdByCid( $intCalendarEventCategoryId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE calendar_event_category_id = %d AND cid = %d', ( int ) $intCalendarEventCategoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByCalendarEventTypeIdByCid( $intCalendarEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE calendar_event_type_id = %d AND cid = %d', ( int ) $intCalendarEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByEventIdByCid( $intEventId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE event_id = %d AND cid = %d', ( int ) $intEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByParentCalendarEventIdByCid( $intParentCalendarEventId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE parent_calendar_event_id = %d AND cid = %d', ( int ) $intParentCalendarEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByLeaseCustomerIdByCid( $intLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE lease_customer_id = %d AND cid = %d', ( int ) $intLeaseCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchCalendarEvents( sprintf( 'SELECT * FROM calendar_events WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>