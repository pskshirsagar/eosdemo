<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomAdvertisement extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.custom_advertisements';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomAdTypeId;
	protected $m_intCompanyMediaFileId;
	protected $m_strTitle;
	protected $m_strTitleTextColor;
	protected $m_strTitleBgColor;
	protected $m_strBodyText;
	protected $m_strBodyTextColor;
	protected $m_strBodyBgColor;
	protected $m_strLinkText;
	protected $m_strLinkTextColor;
	protected $m_strLinkUrl;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['custom_ad_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomAdTypeId', trim( $arrValues['custom_ad_type_id'] ) ); elseif( isset( $arrValues['custom_ad_type_id'] ) ) $this->setCustomAdTypeId( $arrValues['custom_ad_type_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['title_text_color'] ) && $boolDirectSet ) $this->set( 'm_strTitleTextColor', trim( stripcslashes( $arrValues['title_text_color'] ) ) ); elseif( isset( $arrValues['title_text_color'] ) ) $this->setTitleTextColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title_text_color'] ) : $arrValues['title_text_color'] );
		if( isset( $arrValues['title_bg_color'] ) && $boolDirectSet ) $this->set( 'm_strTitleBgColor', trim( stripcslashes( $arrValues['title_bg_color'] ) ) ); elseif( isset( $arrValues['title_bg_color'] ) ) $this->setTitleBgColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title_bg_color'] ) : $arrValues['title_bg_color'] );
		if( isset( $arrValues['body_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBodyText', trim( stripcslashes( $arrValues['body_text'] ) ) ); elseif( isset( $arrValues['body_text'] ) ) $this->setBodyText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['body_text'] ) : $arrValues['body_text'] );
		if( isset( $arrValues['body_text_color'] ) && $boolDirectSet ) $this->set( 'm_strBodyTextColor', trim( stripcslashes( $arrValues['body_text_color'] ) ) ); elseif( isset( $arrValues['body_text_color'] ) ) $this->setBodyTextColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['body_text_color'] ) : $arrValues['body_text_color'] );
		if( isset( $arrValues['body_bg_color'] ) && $boolDirectSet ) $this->set( 'm_strBodyBgColor', trim( stripcslashes( $arrValues['body_bg_color'] ) ) ); elseif( isset( $arrValues['body_bg_color'] ) ) $this->setBodyBgColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['body_bg_color'] ) : $arrValues['body_bg_color'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( stripcslashes( $arrValues['link_text'] ) ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_text'] ) : $arrValues['link_text'] );
		if( isset( $arrValues['link_text_color'] ) && $boolDirectSet ) $this->set( 'm_strLinkTextColor', trim( stripcslashes( $arrValues['link_text_color'] ) ) ); elseif( isset( $arrValues['link_text_color'] ) ) $this->setLinkTextColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_text_color'] ) : $arrValues['link_text_color'] );
		if( isset( $arrValues['link_url'] ) && $boolDirectSet ) $this->set( 'm_strLinkUrl', trim( stripcslashes( $arrValues['link_url'] ) ) ); elseif( isset( $arrValues['link_url'] ) ) $this->setLinkUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_url'] ) : $arrValues['link_url'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomAdTypeId( $intCustomAdTypeId ) {
		$this->set( 'm_intCustomAdTypeId', CStrings::strToIntDef( $intCustomAdTypeId, NULL, false ) );
	}

	public function getCustomAdTypeId() {
		return $this->m_intCustomAdTypeId;
	}

	public function sqlCustomAdTypeId() {
		return ( true == isset( $this->m_intCustomAdTypeId ) ) ? ( string ) $this->m_intCustomAdTypeId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 100, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setTitleTextColor( $strTitleTextColor ) {
		$this->set( 'm_strTitleTextColor', CStrings::strTrimDef( $strTitleTextColor, 7, NULL, true ) );
	}

	public function getTitleTextColor() {
		return $this->m_strTitleTextColor;
	}

	public function sqlTitleTextColor() {
		return ( true == isset( $this->m_strTitleTextColor ) ) ? '\'' . addslashes( $this->m_strTitleTextColor ) . '\'' : 'NULL';
	}

	public function setTitleBgColor( $strTitleBgColor ) {
		$this->set( 'm_strTitleBgColor', CStrings::strTrimDef( $strTitleBgColor, 7, NULL, true ) );
	}

	public function getTitleBgColor() {
		return $this->m_strTitleBgColor;
	}

	public function sqlTitleBgColor() {
		return ( true == isset( $this->m_strTitleBgColor ) ) ? '\'' . addslashes( $this->m_strTitleBgColor ) . '\'' : 'NULL';
	}

	public function setBodyText( $strBodyText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBodyText', CStrings::strTrimDef( $strBodyText, -1, NULL, true ), $strLocaleCode );
	}

	public function getBodyText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBodyText', $strLocaleCode );
	}

	public function sqlBodyText() {
		return ( true == isset( $this->m_strBodyText ) ) ? '\'' . addslashes( $this->m_strBodyText ) . '\'' : 'NULL';
	}

	public function setBodyTextColor( $strBodyTextColor ) {
		$this->set( 'm_strBodyTextColor', CStrings::strTrimDef( $strBodyTextColor, 7, NULL, true ) );
	}

	public function getBodyTextColor() {
		return $this->m_strBodyTextColor;
	}

	public function sqlBodyTextColor() {
		return ( true == isset( $this->m_strBodyTextColor ) ) ? '\'' . addslashes( $this->m_strBodyTextColor ) . '\'' : 'NULL';
	}

	public function setBodyBgColor( $strBodyBgColor ) {
		$this->set( 'm_strBodyBgColor', CStrings::strTrimDef( $strBodyBgColor, 7, NULL, true ) );
	}

	public function getBodyBgColor() {
		return $this->m_strBodyBgColor;
	}

	public function sqlBodyBgColor() {
		return ( true == isset( $this->m_strBodyBgColor ) ) ? '\'' . addslashes( $this->m_strBodyBgColor ) . '\'' : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, 100, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? '\'' . addslashes( $this->m_strLinkText ) . '\'' : 'NULL';
	}

	public function setLinkTextColor( $strLinkTextColor ) {
		$this->set( 'm_strLinkTextColor', CStrings::strTrimDef( $strLinkTextColor, 7, NULL, true ) );
	}

	public function getLinkTextColor() {
		return $this->m_strLinkTextColor;
	}

	public function sqlLinkTextColor() {
		return ( true == isset( $this->m_strLinkTextColor ) ) ? '\'' . addslashes( $this->m_strLinkTextColor ) . '\'' : 'NULL';
	}

	public function setLinkUrl( $strLinkUrl ) {
		$this->set( 'm_strLinkUrl', CStrings::strTrimDef( $strLinkUrl, 100, NULL, true ) );
	}

	public function getLinkUrl() {
		return $this->m_strLinkUrl;
	}

	public function sqlLinkUrl() {
		return ( true == isset( $this->m_strLinkUrl ) ) ? '\'' . addslashes( $this->m_strLinkUrl ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, custom_ad_type_id, company_media_file_id, title, title_text_color, title_bg_color, body_text, body_text_color, body_bg_color, link_text, link_text_color, link_url, is_published, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomAdTypeId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlTitleTextColor() . ', ' .
						$this->sqlTitleBgColor() . ', ' .
						$this->sqlBodyText() . ', ' .
						$this->sqlBodyTextColor() . ', ' .
						$this->sqlBodyBgColor() . ', ' .
						$this->sqlLinkText() . ', ' .
						$this->sqlLinkTextColor() . ', ' .
						$this->sqlLinkUrl() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_ad_type_id = ' . $this->sqlCustomAdTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomAdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' custom_ad_type_id = ' . $this->sqlCustomAdTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title_text_color = ' . $this->sqlTitleTextColor(). ',' ; } elseif( true == array_key_exists( 'TitleTextColor', $this->getChangedColumns() ) ) { $strSql .= ' title_text_color = ' . $this->sqlTitleTextColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title_bg_color = ' . $this->sqlTitleBgColor(). ',' ; } elseif( true == array_key_exists( 'TitleBgColor', $this->getChangedColumns() ) ) { $strSql .= ' title_bg_color = ' . $this->sqlTitleBgColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' body_text = ' . $this->sqlBodyText(). ',' ; } elseif( true == array_key_exists( 'BodyText', $this->getChangedColumns() ) ) { $strSql .= ' body_text = ' . $this->sqlBodyText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' body_text_color = ' . $this->sqlBodyTextColor(). ',' ; } elseif( true == array_key_exists( 'BodyTextColor', $this->getChangedColumns() ) ) { $strSql .= ' body_text_color = ' . $this->sqlBodyTextColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' body_bg_color = ' . $this->sqlBodyBgColor(). ',' ; } elseif( true == array_key_exists( 'BodyBgColor', $this->getChangedColumns() ) ) { $strSql .= ' body_bg_color = ' . $this->sqlBodyBgColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text_color = ' . $this->sqlLinkTextColor(). ',' ; } elseif( true == array_key_exists( 'LinkTextColor', $this->getChangedColumns() ) ) { $strSql .= ' link_text_color = ' . $this->sqlLinkTextColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl(). ',' ; } elseif( true == array_key_exists( 'LinkUrl', $this->getChangedColumns() ) ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'custom_ad_type_id' => $this->getCustomAdTypeId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'title' => $this->getTitle(),
			'title_text_color' => $this->getTitleTextColor(),
			'title_bg_color' => $this->getTitleBgColor(),
			'body_text' => $this->getBodyText(),
			'body_text_color' => $this->getBodyTextColor(),
			'body_bg_color' => $this->getBodyBgColor(),
			'link_text' => $this->getLinkText(),
			'link_text_color' => $this->getLinkTextColor(),
			'link_url' => $this->getLinkUrl(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>