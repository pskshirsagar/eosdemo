<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCalls
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCalls extends CEosPluralBase {

	/**
	 * @return CScheduledCall[]
	 */
	public static function fetchScheduledCalls( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledCall', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledCall
	 */
	public static function fetchScheduledCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledCall', $objDatabase );
	}

	public static function fetchScheduledCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_calls', $objDatabase );
	}

	public static function fetchScheduledCallByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledCall( sprintf( 'SELECT * FROM scheduled_calls WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledCalls( sprintf( 'SELECT * FROM scheduled_calls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallsByScheduledCallTypeIdByCid( $intScheduledCallTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledCalls( sprintf( 'SELECT * FROM scheduled_calls WHERE scheduled_call_type_id = %d AND cid = %d', ( int ) $intScheduledCallTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallsByCallFrequencyIdByCid( $intCallFrequencyId, $intCid, $objDatabase ) {
		return self::fetchScheduledCalls( sprintf( 'SELECT * FROM scheduled_calls WHERE call_frequency_id = %d AND cid = %d', ( int ) $intCallFrequencyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallsByCallPriorityIdByCid( $intCallPriorityId, $intCid, $objDatabase ) {
		return self::fetchScheduledCalls( sprintf( 'SELECT * FROM scheduled_calls WHERE call_priority_id = %d AND cid = %d', ( int ) $intCallPriorityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallsByVoiceTypeIdByCid( $intVoiceTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledCalls( sprintf( 'SELECT * FROM scheduled_calls WHERE voice_type_id = %d AND cid = %d', ( int ) $intVoiceTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>