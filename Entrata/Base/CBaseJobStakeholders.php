<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobStakeholders extends CEosPluralBase {

	/**
	 * @return CJobStakeholder[]
	 */
	public static function fetchJobStakeholders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobStakeholder::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobStakeholder
	 */
	public static function fetchJobStakeholder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobStakeholder::class, $objDatabase );
	}

	public static function fetchJobStakeholderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_stakeholders', $objDatabase );
	}

	public static function fetchJobStakeholderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobStakeholder( sprintf( 'SELECT * FROM job_stakeholders WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchJobStakeholdersByCid( $intCid, $objDatabase ) {
		return self::fetchJobStakeholders( sprintf( 'SELECT * FROM job_stakeholders WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchJobStakeholdersByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobStakeholders( sprintf( 'SELECT * FROM job_stakeholders WHERE job_id = %d AND cid = %d', $intJobId, $intCid ), $objDatabase );
	}

	public static function fetchJobStakeholdersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchJobStakeholders( sprintf( 'SELECT * FROM job_stakeholders WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>