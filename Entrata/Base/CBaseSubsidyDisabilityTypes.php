<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyDisabilityTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyDisabilityTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyDisabilityType[]
	 */
	public static function fetchSubsidyDisabilityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyDisabilityType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyDisabilityType
	 */
	public static function fetchSubsidyDisabilityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyDisabilityType::class, $objDatabase );
	}

	public static function fetchSubsidyDisabilityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_disability_types', $objDatabase );
	}

	public static function fetchSubsidyDisabilityTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyDisabilityType( sprintf( 'SELECT * FROM subsidy_disability_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>