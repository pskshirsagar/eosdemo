<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionForm extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.inspection_forms';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInspectionFormId;
	protected $m_strName;
	protected $m_strCustomText;
	protected $m_strTermsAndConditions;
	protected $m_intIncludeApproveAll;
	protected $m_intIncludeCustomText;
	protected $m_intDoNotUseActions;
	protected $m_intIsCompleted;
	protected $m_intIsNewFormSetup;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMaintenanceLocationTypeId;
	protected $m_intIncludeBottomText;

	public function __construct() {
		parent::__construct();

		$this->m_intIncludeApproveAll = '1';
		$this->m_intIncludeCustomText = '1';
		$this->m_intDoNotUseActions = '0';
		$this->m_intIsCompleted = '0';
		$this->m_intIsNewFormSetup = '0';
		$this->m_intMaintenanceLocationTypeId = '2';
		$this->m_intIncludeBottomText = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['inspection_form_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionFormId', trim( $arrValues['inspection_form_id'] ) ); elseif( isset( $arrValues['inspection_form_id'] ) ) $this->setInspectionFormId( $arrValues['inspection_form_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['custom_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomText', trim( $arrValues['custom_text'] ) ); elseif( isset( $arrValues['custom_text'] ) ) $this->setCustomText( $arrValues['custom_text'] );
		if( isset( $arrValues['terms_and_conditions'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTermsAndConditions', trim( $arrValues['terms_and_conditions'] ) ); elseif( isset( $arrValues['terms_and_conditions'] ) ) $this->setTermsAndConditions( $arrValues['terms_and_conditions'] );
		if( isset( $arrValues['include_approve_all'] ) && $boolDirectSet ) $this->set( 'm_intIncludeApproveAll', trim( $arrValues['include_approve_all'] ) ); elseif( isset( $arrValues['include_approve_all'] ) ) $this->setIncludeApproveAll( $arrValues['include_approve_all'] );
		if( isset( $arrValues['include_custom_text'] ) && $boolDirectSet ) $this->set( 'm_intIncludeCustomText', trim( $arrValues['include_custom_text'] ) ); elseif( isset( $arrValues['include_custom_text'] ) ) $this->setIncludeCustomText( $arrValues['include_custom_text'] );
		if( isset( $arrValues['do_not_use_actions'] ) && $boolDirectSet ) $this->set( 'm_intDoNotUseActions', trim( $arrValues['do_not_use_actions'] ) ); elseif( isset( $arrValues['do_not_use_actions'] ) ) $this->setDoNotUseActions( $arrValues['do_not_use_actions'] );
		if( isset( $arrValues['is_completed'] ) && $boolDirectSet ) $this->set( 'm_intIsCompleted', trim( $arrValues['is_completed'] ) ); elseif( isset( $arrValues['is_completed'] ) ) $this->setIsCompleted( $arrValues['is_completed'] );
		if( isset( $arrValues['is_new_form_setup'] ) && $boolDirectSet ) $this->set( 'm_intIsNewFormSetup', trim( $arrValues['is_new_form_setup'] ) ); elseif( isset( $arrValues['is_new_form_setup'] ) ) $this->setIsNewFormSetup( $arrValues['is_new_form_setup'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['maintenance_location_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationTypeId', trim( $arrValues['maintenance_location_type_id'] ) ); elseif( isset( $arrValues['maintenance_location_type_id'] ) ) $this->setMaintenanceLocationTypeId( $arrValues['maintenance_location_type_id'] );
		if( isset( $arrValues['include_bottom_text'] ) && $boolDirectSet ) $this->set( 'm_intIncludeBottomText', trim( $arrValues['include_bottom_text'] ) ); elseif( isset( $arrValues['include_bottom_text'] ) ) $this->setIncludeBottomText( $arrValues['include_bottom_text'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInspectionFormId( $intInspectionFormId ) {
		$this->set( 'm_intInspectionFormId', CStrings::strToIntDef( $intInspectionFormId, NULL, false ) );
	}

	public function getInspectionFormId() {
		return $this->m_intInspectionFormId;
	}

	public function sqlInspectionFormId() {
		return ( true == isset( $this->m_intInspectionFormId ) ) ? ( string ) $this->m_intInspectionFormId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setCustomText( $strCustomText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomText', CStrings::strTrimDef( $strCustomText, -1, NULL, true ), $strLocaleCode );
	}

	public function getCustomText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomText', $strLocaleCode );
	}

	public function sqlCustomText() {
		return ( true == isset( $this->m_strCustomText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomText ) : '\'' . addslashes( $this->m_strCustomText ) . '\'' ) : 'NULL';
	}

	public function setTermsAndConditions( $strTermsAndConditions, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTermsAndConditions', CStrings::strTrimDef( $strTermsAndConditions, -1, NULL, true ), $strLocaleCode );
	}

	public function getTermsAndConditions( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTermsAndConditions', $strLocaleCode );
	}

	public function sqlTermsAndConditions() {
		return ( true == isset( $this->m_strTermsAndConditions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTermsAndConditions ) : '\'' . addslashes( $this->m_strTermsAndConditions ) . '\'' ) : 'NULL';
	}

	public function setIncludeApproveAll( $intIncludeApproveAll ) {
		$this->set( 'm_intIncludeApproveAll', CStrings::strToIntDef( $intIncludeApproveAll, NULL, false ) );
	}

	public function getIncludeApproveAll() {
		return $this->m_intIncludeApproveAll;
	}

	public function sqlIncludeApproveAll() {
		return ( true == isset( $this->m_intIncludeApproveAll ) ) ? ( string ) $this->m_intIncludeApproveAll : '1';
	}

	public function setIncludeCustomText( $intIncludeCustomText ) {
		$this->set( 'm_intIncludeCustomText', CStrings::strToIntDef( $intIncludeCustomText, NULL, false ) );
	}

	public function getIncludeCustomText() {
		return $this->m_intIncludeCustomText;
	}

	public function sqlIncludeCustomText() {
		return ( true == isset( $this->m_intIncludeCustomText ) ) ? ( string ) $this->m_intIncludeCustomText : '1';
	}

	public function setDoNotUseActions( $intDoNotUseActions ) {
		$this->set( 'm_intDoNotUseActions', CStrings::strToIntDef( $intDoNotUseActions, NULL, false ) );
	}

	public function getDoNotUseActions() {
		return $this->m_intDoNotUseActions;
	}

	public function sqlDoNotUseActions() {
		return ( true == isset( $this->m_intDoNotUseActions ) ) ? ( string ) $this->m_intDoNotUseActions : '0';
	}

	public function setIsCompleted( $intIsCompleted ) {
		$this->set( 'm_intIsCompleted', CStrings::strToIntDef( $intIsCompleted, NULL, false ) );
	}

	public function getIsCompleted() {
		return $this->m_intIsCompleted;
	}

	public function sqlIsCompleted() {
		return ( true == isset( $this->m_intIsCompleted ) ) ? ( string ) $this->m_intIsCompleted : '0';
	}

	public function setIsNewFormSetup( $intIsNewFormSetup ) {
		$this->set( 'm_intIsNewFormSetup', CStrings::strToIntDef( $intIsNewFormSetup, NULL, false ) );
	}

	public function getIsNewFormSetup() {
		return $this->m_intIsNewFormSetup;
	}

	public function sqlIsNewFormSetup() {
		return ( true == isset( $this->m_intIsNewFormSetup ) ) ? ( string ) $this->m_intIsNewFormSetup : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMaintenanceLocationTypeId( $intMaintenanceLocationTypeId ) {
		$this->set( 'm_intMaintenanceLocationTypeId', CStrings::strToIntDef( $intMaintenanceLocationTypeId, NULL, false ) );
	}

	public function getMaintenanceLocationTypeId() {
		return $this->m_intMaintenanceLocationTypeId;
	}

	public function sqlMaintenanceLocationTypeId() {
		return ( true == isset( $this->m_intMaintenanceLocationTypeId ) ) ? ( string ) $this->m_intMaintenanceLocationTypeId : '2';
	}

	public function setIncludeBottomText( $intIncludeBottomText ) {
		$this->set( 'm_intIncludeBottomText', CStrings::strToIntDef( $intIncludeBottomText, NULL, false ) );
	}

	public function getIncludeBottomText() {
		return $this->m_intIncludeBottomText;
	}

	public function sqlIncludeBottomText() {
		return ( true == isset( $this->m_intIncludeBottomText ) ) ? ( string ) $this->m_intIncludeBottomText : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, inspection_form_id, name, custom_text, terms_and_conditions, include_approve_all, include_custom_text, do_not_use_actions, is_completed, is_new_form_setup, disabled_by, disabled_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, maintenance_location_type_id, include_bottom_text )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlInspectionFormId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlCustomText() . ', ' .
						$this->sqlTermsAndConditions() . ', ' .
						$this->sqlIncludeApproveAll() . ', ' .
						$this->sqlIncludeCustomText() . ', ' .
						$this->sqlDoNotUseActions() . ', ' .
						$this->sqlIsCompleted() . ', ' .
						$this->sqlIsNewFormSetup() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMaintenanceLocationTypeId() . ', ' .
						$this->sqlIncludeBottomText() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_form_id = ' . $this->sqlInspectionFormId(). ',' ; } elseif( true == array_key_exists( 'InspectionFormId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_form_id = ' . $this->sqlInspectionFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_text = ' . $this->sqlCustomText(). ',' ; } elseif( true == array_key_exists( 'CustomText', $this->getChangedColumns() ) ) { $strSql .= ' custom_text = ' . $this->sqlCustomText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terms_and_conditions = ' . $this->sqlTermsAndConditions(). ',' ; } elseif( true == array_key_exists( 'TermsAndConditions', $this->getChangedColumns() ) ) { $strSql .= ' terms_and_conditions = ' . $this->sqlTermsAndConditions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_approve_all = ' . $this->sqlIncludeApproveAll(). ',' ; } elseif( true == array_key_exists( 'IncludeApproveAll', $this->getChangedColumns() ) ) { $strSql .= ' include_approve_all = ' . $this->sqlIncludeApproveAll() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_custom_text = ' . $this->sqlIncludeCustomText(). ',' ; } elseif( true == array_key_exists( 'IncludeCustomText', $this->getChangedColumns() ) ) { $strSql .= ' include_custom_text = ' . $this->sqlIncludeCustomText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_use_actions = ' . $this->sqlDoNotUseActions(). ',' ; } elseif( true == array_key_exists( 'DoNotUseActions', $this->getChangedColumns() ) ) { $strSql .= ' do_not_use_actions = ' . $this->sqlDoNotUseActions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted(). ',' ; } elseif( true == array_key_exists( 'IsCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new_form_setup = ' . $this->sqlIsNewFormSetup(). ',' ; } elseif( true == array_key_exists( 'IsNewFormSetup', $this->getChangedColumns() ) ) { $strSql .= ' is_new_form_setup = ' . $this->sqlIsNewFormSetup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_type_id = ' . $this->sqlMaintenanceLocationTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceLocationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_type_id = ' . $this->sqlMaintenanceLocationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_bottom_text = ' . $this->sqlIncludeBottomText(). ',' ; } elseif( true == array_key_exists( 'IncludeBottomText', $this->getChangedColumns() ) ) { $strSql .= ' include_bottom_text = ' . $this->sqlIncludeBottomText() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'inspection_form_id' => $this->getInspectionFormId(),
			'name' => $this->getName(),
			'custom_text' => $this->getCustomText(),
			'terms_and_conditions' => $this->getTermsAndConditions(),
			'include_approve_all' => $this->getIncludeApproveAll(),
			'include_custom_text' => $this->getIncludeCustomText(),
			'do_not_use_actions' => $this->getDoNotUseActions(),
			'is_completed' => $this->getIsCompleted(),
			'is_new_form_setup' => $this->getIsNewFormSetup(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'maintenance_location_type_id' => $this->getMaintenanceLocationTypeId(),
			'include_bottom_text' => $this->getIncludeBottomText()
		);
	}

}
?>