<?php

class CBaseDefaultGlTree extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_gl_trees';

	protected $m_intId;
	protected $m_intGlTreeTypeId;
	protected $m_intModeTypeId;
	protected $m_intDefaultGlChartId;
	protected $m_intRestrictedDefaultGlTreeId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strAccountNumberPattern;
	protected $m_strAccountNumberDelimiter;
	protected $m_intIsSystem;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intModeTypeId = '1';
		$this->m_intIsSystem = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['gl_tree_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeTypeId', trim( $arrValues['gl_tree_type_id'] ) ); elseif( isset( $arrValues['gl_tree_type_id'] ) ) $this->setGlTreeTypeId( $arrValues['gl_tree_type_id'] );
		if( isset( $arrValues['mode_type_id'] ) && $boolDirectSet ) $this->set( 'm_intModeTypeId', trim( $arrValues['mode_type_id'] ) ); elseif( isset( $arrValues['mode_type_id'] ) ) $this->setModeTypeId( $arrValues['mode_type_id'] );
		if( isset( $arrValues['default_gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlChartId', trim( $arrValues['default_gl_chart_id'] ) ); elseif( isset( $arrValues['default_gl_chart_id'] ) ) $this->setDefaultGlChartId( $arrValues['default_gl_chart_id'] );
		if( isset( $arrValues['restricted_default_gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intRestrictedDefaultGlTreeId', trim( $arrValues['restricted_default_gl_tree_id'] ) ); elseif( isset( $arrValues['restricted_default_gl_tree_id'] ) ) $this->setRestrictedDefaultGlTreeId( $arrValues['restricted_default_gl_tree_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['account_number_pattern'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberPattern', trim( stripcslashes( $arrValues['account_number_pattern'] ) ) ); elseif( isset( $arrValues['account_number_pattern'] ) ) $this->setAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_pattern'] ) : $arrValues['account_number_pattern'] );
		if( isset( $arrValues['account_number_delimiter'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberDelimiter', trim( stripcslashes( $arrValues['account_number_delimiter'] ) ) ); elseif( isset( $arrValues['account_number_delimiter'] ) ) $this->setAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_delimiter'] ) : $arrValues['account_number_delimiter'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setGlTreeTypeId( $intGlTreeTypeId ) {
		$this->set( 'm_intGlTreeTypeId', CStrings::strToIntDef( $intGlTreeTypeId, NULL, false ) );
	}

	public function getGlTreeTypeId() {
		return $this->m_intGlTreeTypeId;
	}

	public function sqlGlTreeTypeId() {
		return ( true == isset( $this->m_intGlTreeTypeId ) ) ? ( string ) $this->m_intGlTreeTypeId : 'NULL';
	}

	public function setModeTypeId( $intModeTypeId ) {
		$this->set( 'm_intModeTypeId', CStrings::strToIntDef( $intModeTypeId, NULL, false ) );
	}

	public function getModeTypeId() {
		return $this->m_intModeTypeId;
	}

	public function sqlModeTypeId() {
		return ( true == isset( $this->m_intModeTypeId ) ) ? ( string ) $this->m_intModeTypeId : '1';
	}

	public function setDefaultGlChartId( $intDefaultGlChartId ) {
		$this->set( 'm_intDefaultGlChartId', CStrings::strToIntDef( $intDefaultGlChartId, NULL, false ) );
	}

	public function getDefaultGlChartId() {
		return $this->m_intDefaultGlChartId;
	}

	public function sqlDefaultGlChartId() {
		return ( true == isset( $this->m_intDefaultGlChartId ) ) ? ( string ) $this->m_intDefaultGlChartId : 'NULL';
	}

	public function setRestrictedDefaultGlTreeId( $intRestrictedDefaultGlTreeId ) {
		$this->set( 'm_intRestrictedDefaultGlTreeId', CStrings::strToIntDef( $intRestrictedDefaultGlTreeId, NULL, false ) );
	}

	public function getRestrictedDefaultGlTreeId() {
		return $this->m_intRestrictedDefaultGlTreeId;
	}

	public function sqlRestrictedDefaultGlTreeId() {
		return ( true == isset( $this->m_intRestrictedDefaultGlTreeId ) ) ? ( string ) $this->m_intRestrictedDefaultGlTreeId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAccountNumberPattern( $strAccountNumberPattern ) {
		$this->set( 'm_strAccountNumberPattern', CStrings::strTrimDef( $strAccountNumberPattern, 50, NULL, true ) );
	}

	public function getAccountNumberPattern() {
		return $this->m_strAccountNumberPattern;
	}

	public function sqlAccountNumberPattern() {
		return ( true == isset( $this->m_strAccountNumberPattern ) ) ? '\'' . addslashes( $this->m_strAccountNumberPattern ) . '\'' : 'NULL';
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		$this->set( 'm_strAccountNumberDelimiter', CStrings::strTrimDef( $strAccountNumberDelimiter, 50, NULL, true ) );
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	public function sqlAccountNumberDelimiter() {
		return ( true == isset( $this->m_strAccountNumberDelimiter ) ) ? '\'' . addslashes( $this->m_strAccountNumberDelimiter ) . '\'' : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, gl_tree_type_id, mode_type_id, default_gl_chart_id, restricted_default_gl_tree_id, system_code, name, account_number_pattern, account_number_delimiter, is_system, deleted_by, deleted_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlGlTreeTypeId() . ', ' .
						$this->sqlModeTypeId() . ', ' .
						$this->sqlDefaultGlChartId() . ', ' .
						$this->sqlRestrictedDefaultGlTreeId() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAccountNumberPattern() . ', ' .
						$this->sqlAccountNumberDelimiter() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_type_id = ' . $this->sqlGlTreeTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_type_id = ' . $this->sqlGlTreeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mode_type_id = ' . $this->sqlModeTypeId(). ',' ; } elseif( true == array_key_exists( 'ModeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' mode_type_id = ' . $this->sqlModeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_gl_chart_id = ' . $this->sqlDefaultGlChartId(). ',' ; } elseif( true == array_key_exists( 'DefaultGlChartId', $this->getChangedColumns() ) ) { $strSql .= ' default_gl_chart_id = ' . $this->sqlDefaultGlChartId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restricted_default_gl_tree_id = ' . $this->sqlRestrictedDefaultGlTreeId(). ',' ; } elseif( true == array_key_exists( 'RestrictedDefaultGlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' restricted_default_gl_tree_id = ' . $this->sqlRestrictedDefaultGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_pattern = ' . $this->sqlAccountNumberPattern(). ',' ; } elseif( true == array_key_exists( 'AccountNumberPattern', $this->getChangedColumns() ) ) { $strSql .= ' account_number_pattern = ' . $this->sqlAccountNumberPattern() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_delimiter = ' . $this->sqlAccountNumberDelimiter(). ',' ; } elseif( true == array_key_exists( 'AccountNumberDelimiter', $this->getChangedColumns() ) ) { $strSql .= ' account_number_delimiter = ' . $this->sqlAccountNumberDelimiter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'gl_tree_type_id' => $this->getGlTreeTypeId(),
			'mode_type_id' => $this->getModeTypeId(),
			'default_gl_chart_id' => $this->getDefaultGlChartId(),
			'restricted_default_gl_tree_id' => $this->getRestrictedDefaultGlTreeId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'account_number_pattern' => $this->getAccountNumberPattern(),
			'account_number_delimiter' => $this->getAccountNumberDelimiter(),
			'is_system' => $this->getIsSystem(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>