<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountMonths
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccountMonths extends CEosPluralBase {

	/**
	 * @return CBudgetTabGlAccountMonth[]
	 */
	public static function fetchBudgetTabGlAccountMonths( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetTabGlAccountMonth::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetTabGlAccountMonth
	 */
	public static function fetchBudgetTabGlAccountMonth( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetTabGlAccountMonth::class, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_tab_gl_account_months', $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountMonth( sprintf( 'SELECT * FROM budget_tab_gl_account_months WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_months WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_months WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountMonthsByBudgetTabGlAccountIdByCid( $intBudgetTabGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountMonths( sprintf( 'SELECT * FROM budget_tab_gl_account_months WHERE budget_tab_gl_account_id = %d AND cid = %d', $intBudgetTabGlAccountId, $intCid ), $objDatabase );
	}

}
?>