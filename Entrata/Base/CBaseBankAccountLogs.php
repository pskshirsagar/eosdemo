<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankAccountLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankAccountLogs extends CEosPluralBase {

	/**
	 * @return CBankAccountLog[]
	 */
	public static function fetchBankAccountLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBankAccountLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBankAccountLog
	 */
	public static function fetchBankAccountLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBankAccountLog::class, $objDatabase );
	}

	public static function fetchBankAccountLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bank_account_logs', $objDatabase );
	}

	public static function fetchBankAccountLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLog( sprintf( 'SELECT * FROM bank_account_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByCid( $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE bank_account_id = %d AND cid = %d', $intBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByBankAccountTypeIdByCid( $intBankAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE bank_account_type_id = %d AND cid = %d', $intBankAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByBankLoginTypeIdByCid( $intBankLoginTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE bank_login_type_id = %d AND cid = %d', $intBankLoginTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByCheckTemplateIdByCid( $intCheckTemplateId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE check_template_id = %d AND cid = %d', $intCheckTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE check_account_type_id = %d AND cid = %d', $intCheckAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE transmission_vendor_id = %d AND cid = %d', $intTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByApAchTypeIdByCid( $intApAchTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE ap_ach_type_id = %d AND cid = %d', $intApAchTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByImmediateDestinationIdByCid( $intImmediateDestinationId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE immediate_destination_id = %d AND cid = %d', $intImmediateDestinationId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByImmediateOriginIdByCid( $intImmediateOriginId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE immediate_origin_id = %d AND cid = %d', $intImmediateOriginId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByCompanyIdByCid( $intCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE company_id = %d AND cid = %d', $intCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByWfpmCeoCompanyIdByCid( $intWfpmCeoCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE wfpm_ceo_company_id = %d AND cid = %d', $intWfpmCeoCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByCheckLayoutIdByCid( $intCheckLayoutId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE check_layout_id = %d AND cid = %d', $intCheckLayoutId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByOriginatingBankIdByCid( $intOriginatingBankId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE originating_bank_id = %d AND cid = %d', $intOriginatingBankId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByAchCompanyIdByCid( $intAchCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE ach_company_id = %d AND cid = %d', $intAchCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsBySftpCompanyTransmissionVendorIdByCid( $intSftpCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE sftp_company_transmission_vendor_id = %d AND cid = %d', $intSftpCompanyTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountLogsByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccountLogs( sprintf( 'SELECT * FROM bank_account_logs WHERE ap_payee_account_id = %d AND cid = %d', $intApPayeeAccountId, $intCid ), $objDatabase );
	}

}
?>