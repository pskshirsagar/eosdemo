<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportColumns
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportColumns extends CEosPluralBase {

	/**
	 * @return CDefaultReportColumn[]
	 */
	public static function fetchDefaultReportColumns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReportColumn::class, $objDatabase );
	}

	/**
	 * @return CDefaultReportColumn
	 */
	public static function fetchDefaultReportColumn( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReportColumn::class, $objDatabase );
	}

	public static function fetchDefaultReportColumnCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_columns', $objDatabase );
	}

	public static function fetchDefaultReportColumnById( $intId, $objDatabase ) {
		return self::fetchDefaultReportColumn( sprintf( 'SELECT * FROM default_report_columns WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultReportColumnsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchDefaultReportColumns( sprintf( 'SELECT * FROM default_report_columns WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

}
?>