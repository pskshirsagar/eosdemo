<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserGroups extends CEosPluralBase {

	/**
	 * @return CCompanyUserGroup[]
	 */
	public static function fetchCompanyUserGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserGroup
	 */
	public static function fetchCompanyUserGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserGroup', $objDatabase );
	}

	public static function fetchCompanyUserGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_groups', $objDatabase );
	}

	public static function fetchCompanyUserGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserGroup( sprintf( 'SELECT * FROM company_user_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserGroups( sprintf( 'SELECT * FROM company_user_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserGroups( sprintf( 'SELECT * FROM company_user_groups WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserGroups( sprintf( 'SELECT * FROM company_user_groups WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>