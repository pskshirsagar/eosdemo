<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserWebsites
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserWebsites extends CEosPluralBase {

	/**
	 * @return CCompanyUserWebsite[]
	 */
	public static function fetchCompanyUserWebsites( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserWebsite', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserWebsite
	 */
	public static function fetchCompanyUserWebsite( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserWebsite', $objDatabase );
	}

	public static function fetchCompanyUserWebsiteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_websites', $objDatabase );
	}

	public static function fetchCompanyUserWebsiteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserWebsite( sprintf( 'SELECT * FROM company_user_websites WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserWebsitesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserWebsites( sprintf( 'SELECT * FROM company_user_websites WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserWebsitesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserWebsites( sprintf( 'SELECT * FROM company_user_websites WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserWebsitesByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserWebsites( sprintf( 'SELECT * FROM company_user_websites WHERE website_id = %d AND cid = %d', ( int ) $intWebsiteId, ( int ) $intCid ), $objDatabase );
	}

}
?>