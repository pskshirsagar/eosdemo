<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStructureTypes
 * Do not add any new functions to this class.
 */

class CBaseStructureTypes extends CEosPluralBase {

	/**
	 * @return CStructureType[]
	 */
	public static function fetchStructureTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStructureType::class, $objDatabase );
	}

	/**
	 * @return CStructureType
	 */
	public static function fetchStructureType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStructureType::class, $objDatabase );
	}

	public static function fetchStructureTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'structure_types', $objDatabase );
	}

	public static function fetchStructureTypeById( $intId, $objDatabase ) {
		return self::fetchStructureType( sprintf( 'SELECT * FROM structure_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>