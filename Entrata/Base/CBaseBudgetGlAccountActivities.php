<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountActivities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetGlAccountActivities extends CEosPluralBase {

	/**
	 * @return CBudgetGlAccountActivity[]
	 */
	public static function fetchBudgetGlAccountActivities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetGlAccountActivity::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetGlAccountActivity
	 */
	public static function fetchBudgetGlAccountActivity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetGlAccountActivity::class, $objDatabase );
	}

	public static function fetchBudgetGlAccountActivityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_gl_account_activities', $objDatabase );
	}

	public static function fetchBudgetGlAccountActivityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivity( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetActivityTypeIdByCid( $intBudgetActivityTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_activity_type_id = %d AND cid = %d', $intBudgetActivityTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetTabIdByCid( $intBudgetTabId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_tab_id = %d AND cid = %d', $intBudgetTabId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetTabGlAccountIdByCid( $intBudgetTabGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_tab_gl_account_id = %d AND cid = %d', $intBudgetTabGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetAssumptionIdByCid( $intBudgetAssumptionId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_assumption_id = %d AND cid = %d', $intBudgetAssumptionId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountActivitiesByBudgetAssumptionMonthIdByCid( $intBudgetAssumptionMonthId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountActivities( sprintf( 'SELECT * FROM budget_gl_account_activities WHERE budget_assumption_month_id = %d AND cid = %d', $intBudgetAssumptionMonthId, $intCid ), $objDatabase );
	}

}
?>