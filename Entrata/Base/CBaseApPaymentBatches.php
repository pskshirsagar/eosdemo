<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPaymentBatches extends CEosPluralBase {

	/**
	 * @return CApPaymentBatch[]
	 */
	public static function fetchApPaymentBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPaymentBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPaymentBatch
	 */
	public static function fetchApPaymentBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPaymentBatch', $objDatabase );
	}

	public static function fetchApPaymentBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payment_batches', $objDatabase );
	}

	public static function fetchApPaymentBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPaymentBatch( sprintf( 'SELECT * FROM ap_payment_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchApPaymentBatches( sprintf( 'SELECT * FROM ap_payment_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>