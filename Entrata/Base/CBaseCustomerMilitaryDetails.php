<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMilitaryDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerMilitaryDetails extends CEosPluralBase {

	/**
	 * @return CCustomerMilitaryDetail[]
	 */
	public static function fetchCustomerMilitaryDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerMilitaryDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerMilitaryDetail
	 */
	public static function fetchCustomerMilitaryDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerMilitaryDetail', $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_military_details', $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetail( sprintf( 'SELECT * FROM customer_military_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryStatusIdByCid( $intMilitaryStatusId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_status_id = %d AND cid = %d', ( int ) $intMilitaryStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryComponentIdByCid( $intMilitaryComponentId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_component_id = %d AND cid = %d', ( int ) $intMilitaryComponentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryPayGradeIdByCid( $intMilitaryPayGradeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_pay_grade_id = %d AND cid = %d', ( int ) $intMilitaryPayGradeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryRankIdByCid( $intMilitaryRankId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_rank_id = %d AND cid = %d', ( int ) $intMilitaryRankId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryAssistanceTypeIdByCid( $intMilitaryAssistanceTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_assistance_type_id = %d AND cid = %d', ( int ) $intMilitaryAssistanceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryNonMilitaryTypeIdByCid( $intMilitaryNonMilitaryTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_non_military_type_id = %d AND cid = %d', ( int ) $intMilitaryNonMilitaryTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByFutureMilitaryInstallationIdByCid( $intFutureMilitaryInstallationId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE future_military_installation_id = %d AND cid = %d', ( int ) $intFutureMilitaryInstallationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryMaritalStatusTypeIdByCid( $intMilitaryMaritalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_marital_status_type_id = %d AND cid = %d', ( int ) $intMilitaryMaritalStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryDependentStatusTypeIdByCid( $intMilitaryDependentStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_dependent_status_type_id = %d AND cid = %d', ( int ) $intMilitaryDependentStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByMilitaryDependentRelationshipIdByCid( $intMilitaryDependentRelationshipId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE military_dependent_relationship_id = %d AND cid = %d', ( int ) $intMilitaryDependentRelationshipId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailsByExternalResidentIdByCid( $intExternalResidentId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetails( sprintf( 'SELECT * FROM customer_military_details WHERE external_resident_id = %d AND cid = %d', ( int ) $intExternalResidentId, ( int ) $intCid ), $objDatabase );
	}

}
?>