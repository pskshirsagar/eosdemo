<?php

class CBaseEntrataTabletModule extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.entrata_tablet_modules';

	protected $m_intId;
	protected $m_intEntrataTabletModuleId;
	protected $m_intModuleId;
	protected $m_intEntrataTabletModuleTypeId;
	protected $m_strName;
	protected $m_strTitle;
	protected $m_strSubTitle;
	protected $m_strUrl;
	protected $m_strIconUrl;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entrata_tablet_module_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataTabletModuleId', trim( $arrValues['entrata_tablet_module_id'] ) ); elseif( isset( $arrValues['entrata_tablet_module_id'] ) ) $this->setEntrataTabletModuleId( $arrValues['entrata_tablet_module_id'] );
		if( isset( $arrValues['module_id'] ) && $boolDirectSet ) $this->set( 'm_intModuleId', trim( $arrValues['module_id'] ) ); elseif( isset( $arrValues['module_id'] ) ) $this->setModuleId( $arrValues['module_id'] );
		if( isset( $arrValues['entrata_tablet_module_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataTabletModuleTypeId', trim( $arrValues['entrata_tablet_module_type_id'] ) ); elseif( isset( $arrValues['entrata_tablet_module_type_id'] ) ) $this->setEntrataTabletModuleTypeId( $arrValues['entrata_tablet_module_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['sub_title'] ) && $boolDirectSet ) $this->set( 'm_strSubTitle', trim( stripcslashes( $arrValues['sub_title'] ) ) ); elseif( isset( $arrValues['sub_title'] ) ) $this->setSubTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sub_title'] ) : $arrValues['sub_title'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['icon_url'] ) && $boolDirectSet ) $this->set( 'm_strIconUrl', trim( stripcslashes( $arrValues['icon_url'] ) ) ); elseif( isset( $arrValues['icon_url'] ) ) $this->setIconUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['icon_url'] ) : $arrValues['icon_url'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntrataTabletModuleId( $intEntrataTabletModuleId ) {
		$this->set( 'm_intEntrataTabletModuleId', CStrings::strToIntDef( $intEntrataTabletModuleId, NULL, false ) );
	}

	public function getEntrataTabletModuleId() {
		return $this->m_intEntrataTabletModuleId;
	}

	public function sqlEntrataTabletModuleId() {
		return ( true == isset( $this->m_intEntrataTabletModuleId ) ) ? ( string ) $this->m_intEntrataTabletModuleId : 'NULL';
	}

	public function setModuleId( $intModuleId ) {
		$this->set( 'm_intModuleId', CStrings::strToIntDef( $intModuleId, NULL, false ) );
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : 'NULL';
	}

	public function setEntrataTabletModuleTypeId( $intEntrataTabletModuleTypeId ) {
		$this->set( 'm_intEntrataTabletModuleTypeId', CStrings::strToIntDef( $intEntrataTabletModuleTypeId, NULL, false ) );
	}

	public function getEntrataTabletModuleTypeId() {
		return $this->m_intEntrataTabletModuleTypeId;
	}

	public function sqlEntrataTabletModuleTypeId() {
		return ( true == isset( $this->m_intEntrataTabletModuleTypeId ) ) ? ( string ) $this->m_intEntrataTabletModuleTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 150, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setSubTitle( $strSubTitle ) {
		$this->set( 'm_strSubTitle', CStrings::strTrimDef( $strSubTitle, 255, NULL, true ) );
	}

	public function getSubTitle() {
		return $this->m_strSubTitle;
	}

	public function sqlSubTitle() {
		return ( true == isset( $this->m_strSubTitle ) ) ? '\'' . addslashes( $this->m_strSubTitle ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 1024, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setIconUrl( $strIconUrl ) {
		$this->set( 'm_strIconUrl', CStrings::strTrimDef( $strIconUrl, 1024, NULL, true ) );
	}

	public function getIconUrl() {
		return $this->m_strIconUrl;
	}

	public function sqlIconUrl() {
		return ( true == isset( $this->m_strIconUrl ) ) ? '\'' . addslashes( $this->m_strIconUrl ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entrata_tablet_module_id' => $this->getEntrataTabletModuleId(),
			'module_id' => $this->getModuleId(),
			'entrata_tablet_module_type_id' => $this->getEntrataTabletModuleTypeId(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'sub_title' => $this->getSubTitle(),
			'url' => $this->getUrl(),
			'icon_url' => $this->getIconUrl(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>