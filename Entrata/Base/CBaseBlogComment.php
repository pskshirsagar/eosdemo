<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBlogComment extends CEosSingularBase {

	const TABLE_NAME = 'public.blog_comments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intBlogPostId;
	protected $m_intBlogCommentTypeId;
	protected $m_intBlogCommentParentId;
	protected $m_intWordpressCommentId;
	protected $m_strName;
	protected $m_strEmail;
	protected $m_boolIsReplyAllowed;
	protected $m_boolMarkedRead;
	protected $m_boolIsDisabled;
	protected $m_strBlogComment;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intBlogCommentTypeId = '1';
		$this->m_boolIsReplyAllowed = false;
		$this->m_boolMarkedRead = false;
		$this->m_boolIsDisabled = false;
		$this->m_intCreatedBy = '11';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['blog_post_id'] ) && $boolDirectSet ) $this->set( 'm_intBlogPostId', trim( $arrValues['blog_post_id'] ) ); elseif( isset( $arrValues['blog_post_id'] ) ) $this->setBlogPostId( $arrValues['blog_post_id'] );
		if( isset( $arrValues['blog_comment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBlogCommentTypeId', trim( $arrValues['blog_comment_type_id'] ) ); elseif( isset( $arrValues['blog_comment_type_id'] ) ) $this->setBlogCommentTypeId( $arrValues['blog_comment_type_id'] );
		if( isset( $arrValues['blog_comment_parent_id'] ) && $boolDirectSet ) $this->set( 'm_intBlogCommentParentId', trim( $arrValues['blog_comment_parent_id'] ) ); elseif( isset( $arrValues['blog_comment_parent_id'] ) ) $this->setBlogCommentParentId( $arrValues['blog_comment_parent_id'] );
		if( isset( $arrValues['wordpress_comment_id'] ) && $boolDirectSet ) $this->set( 'm_intWordpressCommentId', trim( $arrValues['wordpress_comment_id'] ) ); elseif( isset( $arrValues['wordpress_comment_id'] ) ) $this->setWordpressCommentId( $arrValues['wordpress_comment_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['email'] ) && $boolDirectSet ) $this->set( 'm_strEmail', trim( stripcslashes( $arrValues['email'] ) ) ); elseif( isset( $arrValues['email'] ) ) $this->setEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email'] ) : $arrValues['email'] );
		if( isset( $arrValues['is_reply_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsReplyAllowed', trim( stripcslashes( $arrValues['is_reply_allowed'] ) ) ); elseif( isset( $arrValues['is_reply_allowed'] ) ) $this->setIsReplyAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reply_allowed'] ) : $arrValues['is_reply_allowed'] );
		if( isset( $arrValues['marked_read'] ) && $boolDirectSet ) $this->set( 'm_boolMarkedRead', trim( stripcslashes( $arrValues['marked_read'] ) ) ); elseif( isset( $arrValues['marked_read'] ) ) $this->setMarkedRead( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marked_read'] ) : $arrValues['marked_read'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['blog_comment'] ) && $boolDirectSet ) $this->set( 'm_strBlogComment', trim( stripcslashes( $arrValues['blog_comment'] ) ) ); elseif( isset( $arrValues['blog_comment'] ) ) $this->setBlogComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['blog_comment'] ) : $arrValues['blog_comment'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setBlogPostId( $intBlogPostId ) {
		$this->set( 'm_intBlogPostId', CStrings::strToIntDef( $intBlogPostId, NULL, false ) );
	}

	public function getBlogPostId() {
		return $this->m_intBlogPostId;
	}

	public function sqlBlogPostId() {
		return ( true == isset( $this->m_intBlogPostId ) ) ? ( string ) $this->m_intBlogPostId : 'NULL';
	}

	public function setBlogCommentTypeId( $intBlogCommentTypeId ) {
		$this->set( 'm_intBlogCommentTypeId', CStrings::strToIntDef( $intBlogCommentTypeId, NULL, false ) );
	}

	public function getBlogCommentTypeId() {
		return $this->m_intBlogCommentTypeId;
	}

	public function sqlBlogCommentTypeId() {
		return ( true == isset( $this->m_intBlogCommentTypeId ) ) ? ( string ) $this->m_intBlogCommentTypeId : '1';
	}

	public function setBlogCommentParentId( $intBlogCommentParentId ) {
		$this->set( 'm_intBlogCommentParentId', CStrings::strToIntDef( $intBlogCommentParentId, NULL, false ) );
	}

	public function getBlogCommentParentId() {
		return $this->m_intBlogCommentParentId;
	}

	public function sqlBlogCommentParentId() {
		return ( true == isset( $this->m_intBlogCommentParentId ) ) ? ( string ) $this->m_intBlogCommentParentId : 'NULL';
	}

	public function setWordpressCommentId( $intWordpressCommentId ) {
		$this->set( 'm_intWordpressCommentId', CStrings::strToIntDef( $intWordpressCommentId, NULL, false ) );
	}

	public function getWordpressCommentId() {
		return $this->m_intWordpressCommentId;
	}

	public function sqlWordpressCommentId() {
		return ( true == isset( $this->m_intWordpressCommentId ) ) ? ( string ) $this->m_intWordpressCommentId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 256, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setEmail( $strEmail ) {
		$this->set( 'm_strEmail', CStrings::strTrimDef( $strEmail, 256, NULL, true ) );
	}

	public function getEmail() {
		return $this->m_strEmail;
	}

	public function sqlEmail() {
		return ( true == isset( $this->m_strEmail ) ) ? '\'' . addslashes( $this->m_strEmail ) . '\'' : 'NULL';
	}

	public function setIsReplyAllowed( $boolIsReplyAllowed ) {
		$this->set( 'm_boolIsReplyAllowed', CStrings::strToBool( $boolIsReplyAllowed ) );
	}

	public function getIsReplyAllowed() {
		return $this->m_boolIsReplyAllowed;
	}

	public function sqlIsReplyAllowed() {
		return ( true == isset( $this->m_boolIsReplyAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReplyAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMarkedRead( $boolMarkedRead ) {
		$this->set( 'm_boolMarkedRead', CStrings::strToBool( $boolMarkedRead ) );
	}

	public function getMarkedRead() {
		return $this->m_boolMarkedRead;
	}

	public function sqlMarkedRead() {
		return ( true == isset( $this->m_boolMarkedRead ) ) ? '\'' . ( true == ( bool ) $this->m_boolMarkedRead ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBlogComment( $strBlogComment ) {
		$this->set( 'm_strBlogComment', CStrings::strTrimDef( $strBlogComment, 256, NULL, true ) );
	}

	public function getBlogComment() {
		return $this->m_strBlogComment;
	}

	public function sqlBlogComment() {
		return ( true == isset( $this->m_strBlogComment ) ) ? '\'' . addslashes( $this->m_strBlogComment ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '11';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, blog_post_id, blog_comment_type_id, blog_comment_parent_id, wordpress_comment_id, name, email, is_reply_allowed, marked_read, is_disabled, blog_comment, approved_by, approved_on, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlWebsiteId() . ', ' .
			$this->sqlBlogPostId() . ', ' .
			$this->sqlBlogCommentTypeId() . ', ' .
			$this->sqlBlogCommentParentId() . ', ' .
			$this->sqlWordpressCommentId() . ', ' .
			$this->sqlName() . ', ' .
			$this->sqlEmail() . ', ' .
			$this->sqlIsReplyAllowed() . ', ' .
			$this->sqlMarkedRead() . ', ' .
			$this->sqlIsDisabled() . ', ' .
			$this->sqlBlogComment() . ', ' .
			$this->sqlApprovedBy() . ', ' .
			$this->sqlApprovedOn() . ', ' .
			$this->sqlDeletedBy() . ', ' .
			$this->sqlDeletedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blog_post_id = ' . $this->sqlBlogPostId() . ','; } elseif( true == array_key_exists( 'BlogPostId', $this->getChangedColumns() ) ) { $strSql .= ' blog_post_id = ' . $this->sqlBlogPostId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blog_comment_type_id = ' . $this->sqlBlogCommentTypeId() . ','; } elseif( true == array_key_exists( 'BlogCommentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' blog_comment_type_id = ' . $this->sqlBlogCommentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blog_comment_parent_id = ' . $this->sqlBlogCommentParentId() . ','; } elseif( true == array_key_exists( 'BlogCommentParentId', $this->getChangedColumns() ) ) { $strSql .= ' blog_comment_parent_id = ' . $this->sqlBlogCommentParentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wordpress_comment_id = ' . $this->sqlWordpressCommentId() . ','; } elseif( true == array_key_exists( 'WordpressCommentId', $this->getChangedColumns() ) ) { $strSql .= ' wordpress_comment_id = ' . $this->sqlWordpressCommentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; } elseif( true == array_key_exists( 'Email', $this->getChangedColumns() ) ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reply_allowed = ' . $this->sqlIsReplyAllowed() . ','; } elseif( true == array_key_exists( 'IsReplyAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_reply_allowed = ' . $this->sqlIsReplyAllowed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marked_read = ' . $this->sqlMarkedRead() . ','; } elseif( true == array_key_exists( 'MarkedRead', $this->getChangedColumns() ) ) { $strSql .= ' marked_read = ' . $this->sqlMarkedRead() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blog_comment = ' . $this->sqlBlogComment() . ','; } elseif( true == array_key_exists( 'BlogComment', $this->getChangedColumns() ) ) { $strSql .= ' blog_comment = ' . $this->sqlBlogComment() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'blog_post_id' => $this->getBlogPostId(),
			'blog_comment_type_id' => $this->getBlogCommentTypeId(),
			'blog_comment_parent_id' => $this->getBlogCommentParentId(),
			'wordpress_comment_id' => $this->getWordpressCommentId(),
			'name' => $this->getName(),
			'email' => $this->getEmail(),
			'is_reply_allowed' => $this->getIsReplyAllowed(),
			'marked_read' => $this->getMarkedRead(),
			'is_disabled' => $this->getIsDisabled(),
			'blog_comment' => $this->getBlogComment(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>