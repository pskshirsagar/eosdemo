<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerActivityPoint extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_activity_points';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intCompanyActivityPointId;
	protected $m_intPointsEarned;
	protected $m_strRewardDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_activity_point_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyActivityPointId', trim( $arrValues['company_activity_point_id'] ) ); elseif( isset( $arrValues['company_activity_point_id'] ) ) $this->setCompanyActivityPointId( $arrValues['company_activity_point_id'] );
		if( isset( $arrValues['points_earned'] ) && $boolDirectSet ) $this->set( 'm_intPointsEarned', trim( $arrValues['points_earned'] ) ); elseif( isset( $arrValues['points_earned'] ) ) $this->setPointsEarned( $arrValues['points_earned'] );
		if( isset( $arrValues['reward_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRewardDatetime', trim( $arrValues['reward_datetime'] ) ); elseif( isset( $arrValues['reward_datetime'] ) ) $this->setRewardDatetime( $arrValues['reward_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyActivityPointId( $intCompanyActivityPointId ) {
		$this->set( 'm_intCompanyActivityPointId', CStrings::strToIntDef( $intCompanyActivityPointId, NULL, false ) );
	}

	public function getCompanyActivityPointId() {
		return $this->m_intCompanyActivityPointId;
	}

	public function sqlCompanyActivityPointId() {
		return ( true == isset( $this->m_intCompanyActivityPointId ) ) ? ( string ) $this->m_intCompanyActivityPointId : 'NULL';
	}

	public function setPointsEarned( $intPointsEarned ) {
		$this->set( 'm_intPointsEarned', CStrings::strToIntDef( $intPointsEarned, NULL, false ) );
	}

	public function getPointsEarned() {
		return $this->m_intPointsEarned;
	}

	public function sqlPointsEarned() {
		return ( true == isset( $this->m_intPointsEarned ) ) ? ( string ) $this->m_intPointsEarned : 'NULL';
	}

	public function setRewardDatetime( $strRewardDatetime ) {
		$this->set( 'm_strRewardDatetime', CStrings::strTrimDef( $strRewardDatetime, -1, NULL, true ) );
	}

	public function getRewardDatetime() {
		return $this->m_strRewardDatetime;
	}

	public function sqlRewardDatetime() {
		return ( true == isset( $this->m_strRewardDatetime ) ) ? '\'' . $this->m_strRewardDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, company_activity_point_id, points_earned, reward_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlCompanyActivityPointId() . ', ' .
 						$this->sqlPointsEarned() . ', ' .
 						$this->sqlRewardDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_activity_point_id = ' . $this->sqlCompanyActivityPointId() . ','; } elseif( true == array_key_exists( 'CompanyActivityPointId', $this->getChangedColumns() ) ) { $strSql .= ' company_activity_point_id = ' . $this->sqlCompanyActivityPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' points_earned = ' . $this->sqlPointsEarned() . ','; } elseif( true == array_key_exists( 'PointsEarned', $this->getChangedColumns() ) ) { $strSql .= ' points_earned = ' . $this->sqlPointsEarned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_datetime = ' . $this->sqlRewardDatetime() . ','; } elseif( true == array_key_exists( 'RewardDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reward_datetime = ' . $this->sqlRewardDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'company_activity_point_id' => $this->getCompanyActivityPointId(),
			'points_earned' => $this->getPointsEarned(),
			'reward_datetime' => $this->getRewardDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>