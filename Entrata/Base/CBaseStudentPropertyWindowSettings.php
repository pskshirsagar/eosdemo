<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentPropertyWindowSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseStudentPropertyWindowSettings extends CEosPluralBase {

	/**
	 * @return CStudentPropertyWindowSetting[]
	 */
	public static function fetchStudentPropertyWindowSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CStudentPropertyWindowSetting::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CStudentPropertyWindowSetting
	 */
	public static function fetchStudentPropertyWindowSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStudentPropertyWindowSetting::class, $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'student_property_window_settings', $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchStudentPropertyWindowSetting( sprintf( 'SELECT * FROM student_property_window_settings WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchStudentPropertyWindowSettings( sprintf( 'SELECT * FROM student_property_window_settings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchStudentPropertyWindowSettings( sprintf( 'SELECT * FROM student_property_window_settings WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchStudentPropertyWindowSettings( sprintf( 'SELECT * FROM student_property_window_settings WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchStudentPropertyWindowSettingsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchStudentPropertyWindowSettings( sprintf( 'SELECT * FROM student_property_window_settings WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>