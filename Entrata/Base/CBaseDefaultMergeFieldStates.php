<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMergeFieldStates
 * Do not add any new functions to this class.
 */

class CBaseDefaultMergeFieldStates extends CEosPluralBase {

	/**
	 * @return CDefaultMergeFieldState[]
	 */
	public static function fetchDefaultMergeFieldStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultMergeFieldState', $objDatabase );
	}

	/**
	 * @return CDefaultMergeFieldState
	 */
	public static function fetchDefaultMergeFieldState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultMergeFieldState', $objDatabase );
	}

	public static function fetchDefaultMergeFieldStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_merge_field_states', $objDatabase );
	}

	public static function fetchDefaultMergeFieldStateById( $intId, $objDatabase ) {
		return self::fetchDefaultMergeFieldState( sprintf( 'SELECT * FROM default_merge_field_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldStatesByDefaultMergeFieldId( $intDefaultMergeFieldId, $objDatabase ) {
		return self::fetchDefaultMergeFieldStates( sprintf( 'SELECT * FROM default_merge_field_states WHERE default_merge_field_id = %d', ( int ) $intDefaultMergeFieldId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldStatesByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {
		return self::fetchDefaultMergeFieldStates( sprintf( 'SELECT * FROM default_merge_field_states WHERE merge_field_group_id = %d', ( int ) $intMergeFieldGroupId ), $objDatabase );
	}

}
?>