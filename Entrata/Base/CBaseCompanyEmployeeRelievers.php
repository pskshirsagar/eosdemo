<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeRelievers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployeeRelievers extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeReliever[]
	 */
	public static function fetchCompanyEmployeeRelievers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyEmployeeReliever', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployeeReliever
	 */
	public static function fetchCompanyEmployeeReliever( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyEmployeeReliever', $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieverCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_relievers', $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieverByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeReliever( sprintf( 'SELECT * FROM company_employee_relievers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieversByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeRelievers( sprintf( 'SELECT * FROM company_employee_relievers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieversByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeRelievers( sprintf( 'SELECT * FROM company_employee_relievers WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieversByRelieverCompanyEmployeeIdByCid( $intRelieverCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeRelievers( sprintf( 'SELECT * FROM company_employee_relievers WHERE reliever_company_employee_id = %d AND cid = %d', ( int ) $intRelieverCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>