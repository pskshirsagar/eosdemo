<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentLeasingCapLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseStudentLeasingCapLogs extends CEosPluralBase {

	/**
	 * @return CStudentLeasingCapLog[]
	 */
	public static function fetchStudentLeasingCapLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CStudentLeasingCapLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CStudentLeasingCapLog
	 */
	public static function fetchStudentLeasingCapLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStudentLeasingCapLog::class, $objDatabase );
	}

	public static function fetchStudentLeasingCapLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'student_leasing_cap_logs', $objDatabase );
	}

	public static function fetchStudentLeasingCapLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLog( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByCid( $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsBySpaceOptionIdByCid( $intSpaceOptionId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE space_option_id = %d AND cid = %d', ( int ) $intSpaceOptionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByStudentLeasingCapIdByCid( $intStudentLeasingCapId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE student_leasing_cap_id = %d AND cid = %d', ( int ) $intStudentLeasingCapId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapLogsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCapLogs( sprintf( 'SELECT * FROM student_leasing_cap_logs WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

}
?>