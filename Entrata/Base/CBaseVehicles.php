<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVehicles
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseVehicles extends CEosPluralBase {

	/**
	 * @return CVehicle[]
	 */
	public static function fetchVehicles( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CVehicle::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CVehicle
	 */
	public static function fetchVehicle( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CVehicle::class, $objDatabase );
	}

	public static function fetchVehicleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'vehicles', $objDatabase );
	}

	public static function fetchVehicleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchVehicle( sprintf( 'SELECT * FROM vehicles WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchVehiclesByCid( $intCid, $objDatabase ) {
		return self::fetchVehicles( sprintf( 'SELECT * FROM vehicles WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>