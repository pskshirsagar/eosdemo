<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChecklistItem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.checklist_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intChecklistId;
	protected $m_intChecklistItemTypeId;
	protected $m_intChecklistItemTypeOptionId;
	protected $m_intDefaultChecklistItemId;
	protected $m_intDocumentId;
	protected $m_intFileId;
	protected $m_intFileTypeId;
	protected $m_strActionLabel;
	protected $m_strItemTitle;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolShowOnPortals;
	protected $m_boolIsRequired;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowOnPortals = false;
		$this->m_boolIsRequired = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['checklist_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistId', trim( $arrValues['checklist_id'] ) ); elseif( isset( $arrValues['checklist_id'] ) ) $this->setChecklistId( $arrValues['checklist_id'] );
		if( isset( $arrValues['checklist_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistItemTypeId', trim( $arrValues['checklist_item_type_id'] ) ); elseif( isset( $arrValues['checklist_item_type_id'] ) ) $this->setChecklistItemTypeId( $arrValues['checklist_item_type_id'] );
		if( isset( $arrValues['checklist_item_type_option_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistItemTypeOptionId', trim( $arrValues['checklist_item_type_option_id'] ) ); elseif( isset( $arrValues['checklist_item_type_option_id'] ) ) $this->setChecklistItemTypeOptionId( $arrValues['checklist_item_type_option_id'] );
		if( isset( $arrValues['default_checklist_item_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultChecklistItemId', trim( $arrValues['default_checklist_item_id'] ) ); elseif( isset( $arrValues['default_checklist_item_id'] ) ) $this->setDefaultChecklistItemId( $arrValues['default_checklist_item_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['action_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strActionLabel', trim( stripcslashes( $arrValues['action_label'] ) ) ); elseif( isset( $arrValues['action_label'] ) ) $this->setActionLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_label'] ) : $arrValues['action_label'] );
		if( isset( $arrValues['item_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strItemTitle', trim( stripcslashes( $arrValues['item_title'] ) ) ); elseif( isset( $arrValues['item_title'] ) ) $this->setItemTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_title'] ) : $arrValues['item_title'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['show_on_portals'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnPortals', trim( stripcslashes( $arrValues['show_on_portals'] ) ) ); elseif( isset( $arrValues['show_on_portals'] ) ) $this->setShowOnPortals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_portals'] ) : $arrValues['show_on_portals'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequired', trim( stripcslashes( $arrValues['is_required'] ) ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required'] ) : $arrValues['is_required'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setChecklistId( $intChecklistId ) {
		$this->set( 'm_intChecklistId', CStrings::strToIntDef( $intChecklistId, NULL, false ) );
	}

	public function getChecklistId() {
		return $this->m_intChecklistId;
	}

	public function sqlChecklistId() {
		return ( true == isset( $this->m_intChecklistId ) ) ? ( string ) $this->m_intChecklistId : 'NULL';
	}

	public function setChecklistItemTypeId( $intChecklistItemTypeId ) {
		$this->set( 'm_intChecklistItemTypeId', CStrings::strToIntDef( $intChecklistItemTypeId, NULL, false ) );
	}

	public function getChecklistItemTypeId() {
		return $this->m_intChecklistItemTypeId;
	}

	public function sqlChecklistItemTypeId() {
		return ( true == isset( $this->m_intChecklistItemTypeId ) ) ? ( string ) $this->m_intChecklistItemTypeId : 'NULL';
	}

	public function setChecklistItemTypeOptionId( $intChecklistItemTypeOptionId ) {
		$this->set( 'm_intChecklistItemTypeOptionId', CStrings::strToIntDef( $intChecklistItemTypeOptionId, NULL, false ) );
	}

	public function getChecklistItemTypeOptionId() {
		return $this->m_intChecklistItemTypeOptionId;
	}

	public function sqlChecklistItemTypeOptionId() {
		return ( true == isset( $this->m_intChecklistItemTypeOptionId ) ) ? ( string ) $this->m_intChecklistItemTypeOptionId : 'NULL';
	}

	public function setDefaultChecklistItemId( $intDefaultChecklistItemId ) {
		$this->set( 'm_intDefaultChecklistItemId', CStrings::strToIntDef( $intDefaultChecklistItemId, NULL, false ) );
	}

	public function getDefaultChecklistItemId() {
		return $this->m_intDefaultChecklistItemId;
	}

	public function sqlDefaultChecklistItemId() {
		return ( true == isset( $this->m_intDefaultChecklistItemId ) ) ? ( string ) $this->m_intDefaultChecklistItemId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setActionLabel( $strActionLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strActionLabel', CStrings::strTrimDef( $strActionLabel, 50, NULL, true ), $strLocaleCode );
	}

	public function getActionLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strActionLabel', $strLocaleCode );
	}

	public function sqlActionLabel() {
		return ( true == isset( $this->m_strActionLabel ) ) ? '\'' . addslashes( $this->m_strActionLabel ) . '\'' : 'NULL';
	}

	public function setItemTitle( $strItemTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strItemTitle', CStrings::strTrimDef( $strItemTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getItemTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strItemTitle', $strLocaleCode );
	}

	public function sqlItemTitle() {
		return ( true == isset( $this->m_strItemTitle ) ) ? '\'' . addslashes( $this->m_strItemTitle ) . '\'' : 'NULL';
	}

	public function setShowOnPortals( $boolShowOnPortals ) {
		$this->set( 'm_boolShowOnPortals', CStrings::strToBool( $boolShowOnPortals ) );
	}

	public function getShowOnPortals() {
		return $this->m_boolShowOnPortals;
	}

	public function sqlShowOnPortals() {
		return ( true == isset( $this->m_boolShowOnPortals ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnPortals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->set( 'm_boolIsRequired', CStrings::strToBool( $boolIsRequired ) );
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_boolIsRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, checklist_id, checklist_item_type_id, checklist_item_type_option_id, default_checklist_item_id, document_id, file_id, file_type_id, action_label, item_title, details, show_on_portals, is_required, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlChecklistId() . ', ' .
						$this->sqlChecklistItemTypeId() . ', ' .
						$this->sqlChecklistItemTypeOptionId() . ', ' .
						$this->sqlDefaultChecklistItemId() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlFileId() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlActionLabel() . ', ' .
						$this->sqlItemTitle() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlShowOnPortals() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checklist_id = ' . $this->sqlChecklistId(). ',' ; } elseif( true == array_key_exists( 'ChecklistId', $this->getChangedColumns() ) ) { $strSql .= ' checklist_id = ' . $this->sqlChecklistId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checklist_item_type_id = ' . $this->sqlChecklistItemTypeId(). ',' ; } elseif( true == array_key_exists( 'ChecklistItemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' checklist_item_type_id = ' . $this->sqlChecklistItemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checklist_item_type_option_id = ' . $this->sqlChecklistItemTypeOptionId(). ',' ; } elseif( true == array_key_exists( 'ChecklistItemTypeOptionId', $this->getChangedColumns() ) ) { $strSql .= ' checklist_item_type_option_id = ' . $this->sqlChecklistItemTypeOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_checklist_item_id = ' . $this->sqlDefaultChecklistItemId(). ',' ; } elseif( true == array_key_exists( 'DefaultChecklistItemId', $this->getChangedColumns() ) ) { $strSql .= ' default_checklist_item_id = ' . $this->sqlDefaultChecklistItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId(). ',' ; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_label = ' . $this->sqlActionLabel(). ',' ; } elseif( true == array_key_exists( 'ActionLabel', $this->getChangedColumns() ) ) { $strSql .= ' action_label = ' . $this->sqlActionLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_title = ' . $this->sqlItemTitle(). ',' ; } elseif( true == array_key_exists( 'ItemTitle', $this->getChangedColumns() ) ) { $strSql .= ' item_title = ' . $this->sqlItemTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_portals = ' . $this->sqlShowOnPortals(). ',' ; } elseif( true == array_key_exists( 'ShowOnPortals', $this->getChangedColumns() ) ) { $strSql .= ' show_on_portals = ' . $this->sqlShowOnPortals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'checklist_id' => $this->getChecklistId(),
			'checklist_item_type_id' => $this->getChecklistItemTypeId(),
			'checklist_item_type_option_id' => $this->getChecklistItemTypeOptionId(),
			'default_checklist_item_id' => $this->getDefaultChecklistItemId(),
			'document_id' => $this->getDocumentId(),
			'file_id' => $this->getFileId(),
			'file_type_id' => $this->getFileTypeId(),
			'action_label' => $this->getActionLabel(),
			'item_title' => $this->getItemTitle(),
			'details' => $this->getDetails(),
			'show_on_portals' => $this->getShowOnPortals(),
			'is_required' => $this->getIsRequired(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>