<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseApPayeeStatusTypes extends CEosPluralBase {

	/**
	 * @return CApPayeeStatusType[]
	 */
	public static function fetchApPayeeStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApPayeeStatusType::class, $objDatabase );
	}

	/**
	 * @return CApPayeeStatusType
	 */
	public static function fetchApPayeeStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeStatusType::class, $objDatabase );
	}

	public static function fetchApPayeeStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_status_types', $objDatabase );
	}

	public static function fetchApPayeeStatusTypeById( $intId, $objDatabase ) {
		return self::fetchApPayeeStatusType( sprintf( 'SELECT * FROM ap_payee_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>