<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskEmail extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_task_emails';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSystemMessageTemplateId;
	protected $m_intHeaderFooterOptionTypeId;
	protected $m_strFromEmailAddress;
	protected $m_strSubject;
	protected $m_strCcEmailAddress;
	protected $m_strBccEmailAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTemplateId', trim( $arrValues['system_message_template_id'] ) ); elseif( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['header_footer_option_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHeaderFooterOptionTypeId', trim( $arrValues['header_footer_option_type_id'] ) ); elseif( isset( $arrValues['header_footer_option_type_id'] ) ) $this->setHeaderFooterOptionTypeId( $arrValues['header_footer_option_type_id'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( stripcslashes( $arrValues['from_email_address'] ) ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_email_address'] ) : $arrValues['from_email_address'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['cc_email_address'] ) && $boolDirectSet ) $this->set( 'm_strCcEmailAddress', trim( stripcslashes( $arrValues['cc_email_address'] ) ) ); elseif( isset( $arrValues['cc_email_address'] ) ) $this->setCcEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_email_address'] ) : $arrValues['cc_email_address'] );
		if( isset( $arrValues['bcc_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBccEmailAddress', trim( stripcslashes( $arrValues['bcc_email_address'] ) ) ); elseif( isset( $arrValues['bcc_email_address'] ) ) $this->setBccEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bcc_email_address'] ) : $arrValues['bcc_email_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->set( 'm_intSystemMessageTemplateId', CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false ) );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setHeaderFooterOptionTypeId( $intHeaderFooterOptionTypeId ) {
		$this->set( 'm_intHeaderFooterOptionTypeId', CStrings::strToIntDef( $intHeaderFooterOptionTypeId, NULL, false ) );
	}

	public function getHeaderFooterOptionTypeId() {
		return $this->m_intHeaderFooterOptionTypeId;
	}

	public function sqlHeaderFooterOptionTypeId() {
		return ( true == isset( $this->m_intHeaderFooterOptionTypeId ) ) ? ( string ) $this->m_intHeaderFooterOptionTypeId : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 240, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setCcEmailAddress( $strCcEmailAddress ) {
		$this->set( 'm_strCcEmailAddress', CStrings::strTrimDef( $strCcEmailAddress, -1, NULL, true ) );
	}

	public function getCcEmailAddress() {
		return $this->m_strCcEmailAddress;
	}

	public function sqlCcEmailAddress() {
		return ( true == isset( $this->m_strCcEmailAddress ) ) ? '\'' . addslashes( $this->m_strCcEmailAddress ) . '\'' : 'NULL';
	}

	public function setBccEmailAddress( $strBccEmailAddress ) {
		$this->set( 'm_strBccEmailAddress', CStrings::strTrimDef( $strBccEmailAddress, -1, NULL, true ) );
	}

	public function getBccEmailAddress() {
		return $this->m_strBccEmailAddress;
	}

	public function sqlBccEmailAddress() {
		return ( true == isset( $this->m_strBccEmailAddress ) ) ? '\'' . addslashes( $this->m_strBccEmailAddress ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, system_message_template_id, header_footer_option_type_id, from_email_address, subject, cc_email_address, bcc_email_address, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlSystemMessageTemplateId() . ', ' .
 						$this->sqlHeaderFooterOptionTypeId() . ', ' .
 						$this->sqlFromEmailAddress() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlCcEmailAddress() . ', ' .
 						$this->sqlBccEmailAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; } elseif( true == array_key_exists( 'SystemMessageTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_footer_option_type_id = ' . $this->sqlHeaderFooterOptionTypeId() . ','; } elseif( true == array_key_exists( 'HeaderFooterOptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' header_footer_option_type_id = ' . $this->sqlHeaderFooterOptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_email_address = ' . $this->sqlCcEmailAddress() . ','; } elseif( true == array_key_exists( 'CcEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' cc_email_address = ' . $this->sqlCcEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bcc_email_address = ' . $this->sqlBccEmailAddress() . ','; } elseif( true == array_key_exists( 'BccEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' bcc_email_address = ' . $this->sqlBccEmailAddress() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'header_footer_option_type_id' => $this->getHeaderFooterOptionTypeId(),
			'from_email_address' => $this->getFromEmailAddress(),
			'subject' => $this->getSubject(),
			'cc_email_address' => $this->getCcEmailAddress(),
			'bcc_email_address' => $this->getBccEmailAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>