<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDocumentExportRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningDocumentExportRequests extends CEosPluralBase {

	/**
	 * @return CScreeningDocumentExportRequest[]
	 */
	public static function fetchScreeningDocumentExportRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScreeningDocumentExportRequest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningDocumentExportRequest
	 */
	public static function fetchScreeningDocumentExportRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningDocumentExportRequest::class, $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_document_export_requests', $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequest( sprintf( 'SELECT * FROM screening_document_export_requests WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequests( sprintf( 'SELECT * FROM screening_document_export_requests WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestsByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequests( sprintf( 'SELECT * FROM screening_document_export_requests WHERE file_type_id = %d AND cid = %d', $intFileTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestsByScreeningDocumentExportRequestStatusTypeIdByCid( $intScreeningDocumentExportRequestStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequests( sprintf( 'SELECT * FROM screening_document_export_requests WHERE screening_document_export_request_status_type_id = %d AND cid = %d', $intScreeningDocumentExportRequestStatusTypeId, $intCid ), $objDatabase );
	}

}
?>