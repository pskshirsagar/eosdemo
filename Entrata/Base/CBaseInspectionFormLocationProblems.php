<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionFormLocationProblems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionFormLocationProblems extends CEosPluralBase {

	/**
	 * @return CInspectionFormLocationProblem[]
	 */
	public static function fetchInspectionFormLocationProblems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CInspectionFormLocationProblem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionFormLocationProblem
	 */
	public static function fetchInspectionFormLocationProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInspectionFormLocationProblem', $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_form_location_problems', $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblem( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemsByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblems( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblems( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE inspection_form_id = %d AND cid = %d', ( int ) $intInspectionFormId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemsByInspectionFormLocationIdByCid( $intInspectionFormLocationId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblems( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE inspection_form_location_id = %d AND cid = %d', ( int ) $intInspectionFormLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblems( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationProblemsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocationProblems( sprintf( 'SELECT * FROM inspection_form_location_problems WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>