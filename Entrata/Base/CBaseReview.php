<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReview extends CEosSingularBase {

	const TABLE_NAME = 'public.reviews';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intCustomerId;
	protected $m_intReviewDetailId;
	protected $m_intCompanyMediaFileId;
	protected $m_intReviewTypeId;
	protected $m_strShowUntil;
	protected $m_strFromDescription;
	protected $m_strExternalReviewId;
	protected $m_strResidentApprovedOn;
	protected $m_boolShowOnCorporateSites;
	protected $m_boolIsSyndicationAllowed;
	protected $m_boolShowOnDashboard;
	protected $m_strOriginalName;
	protected $m_strOriginalEmailAddress;
	protected $m_intCustomerLinkedBy;
	protected $m_strCustomerLinkedOn;
	protected $m_strResidentDeletedOn;
	protected $m_strResidentUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowOnDashboard = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['review_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewDetailId', trim( $arrValues['review_detail_id'] ) ); elseif( isset( $arrValues['review_detail_id'] ) ) $this->setReviewDetailId( $arrValues['review_detail_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['review_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewTypeId', trim( $arrValues['review_type_id'] ) ); elseif( isset( $arrValues['review_type_id'] ) ) $this->setReviewTypeId( $arrValues['review_type_id'] );
		if( isset( $arrValues['show_until'] ) && $boolDirectSet ) $this->set( 'm_strShowUntil', trim( $arrValues['show_until'] ) ); elseif( isset( $arrValues['show_until'] ) ) $this->setShowUntil( $arrValues['show_until'] );
		if( isset( $arrValues['from_description'] ) && $boolDirectSet ) $this->set( 'm_strFromDescription', trim( stripcslashes( $arrValues['from_description'] ) ) ); elseif( isset( $arrValues['from_description'] ) ) $this->setFromDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_description'] ) : $arrValues['from_description'] );
		if( isset( $arrValues['external_review_id'] ) && $boolDirectSet ) $this->set( 'm_strExternalReviewId', trim( stripcslashes( $arrValues['external_review_id'] ) ) ); elseif( isset( $arrValues['external_review_id'] ) ) $this->setExternalReviewId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_review_id'] ) : $arrValues['external_review_id'] );
		if( isset( $arrValues['resident_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strResidentApprovedOn', trim( $arrValues['resident_approved_on'] ) ); elseif( isset( $arrValues['resident_approved_on'] ) ) $this->setResidentApprovedOn( $arrValues['resident_approved_on'] );
		if( isset( $arrValues['show_on_corporate_sites'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnCorporateSites', trim( stripcslashes( $arrValues['show_on_corporate_sites'] ) ) ); elseif( isset( $arrValues['show_on_corporate_sites'] ) ) $this->setShowOnCorporateSites( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_corporate_sites'] ) : $arrValues['show_on_corporate_sites'] );
		if( isset( $arrValues['is_syndication_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsSyndicationAllowed', trim( stripcslashes( $arrValues['is_syndication_allowed'] ) ) ); elseif( isset( $arrValues['is_syndication_allowed'] ) ) $this->setIsSyndicationAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_syndication_allowed'] ) : $arrValues['is_syndication_allowed'] );
		if( isset( $arrValues['show_on_dashboard'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnDashboard', trim( stripcslashes( $arrValues['show_on_dashboard'] ) ) ); elseif( isset( $arrValues['show_on_dashboard'] ) ) $this->setShowOnDashboard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_dashboard'] ) : $arrValues['show_on_dashboard'] );
		if( isset( $arrValues['original_name'] ) && $boolDirectSet ) $this->set( 'm_strOriginalName', trim( stripcslashes( $arrValues['original_name'] ) ) ); elseif( isset( $arrValues['original_name'] ) ) $this->setOriginalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['original_name'] ) : $arrValues['original_name'] );
		if( isset( $arrValues['original_email_address'] ) && $boolDirectSet ) $this->set( 'm_strOriginalEmailAddress', trim( stripcslashes( $arrValues['original_email_address'] ) ) ); elseif( isset( $arrValues['original_email_address'] ) ) $this->setOriginalEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['original_email_address'] ) : $arrValues['original_email_address'] );
		if( isset( $arrValues['customer_linked_by'] ) && $boolDirectSet ) $this->set( 'm_intCustomerLinkedBy', trim( $arrValues['customer_linked_by'] ) ); elseif( isset( $arrValues['customer_linked_by'] ) ) $this->setCustomerLinkedBy( $arrValues['customer_linked_by'] );
		if( isset( $arrValues['customer_linked_on'] ) && $boolDirectSet ) $this->set( 'm_strCustomerLinkedOn', trim( $arrValues['customer_linked_on'] ) ); elseif( isset( $arrValues['customer_linked_on'] ) ) $this->setCustomerLinkedOn( $arrValues['customer_linked_on'] );
		if( isset( $arrValues['resident_deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strResidentDeletedOn', trim( $arrValues['resident_deleted_on'] ) ); elseif( isset( $arrValues['resident_deleted_on'] ) ) $this->setResidentDeletedOn( $arrValues['resident_deleted_on'] );
		if( isset( $arrValues['resident_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strResidentUpdatedOn', trim( $arrValues['resident_updated_on'] ) ); elseif( isset( $arrValues['resident_updated_on'] ) ) $this->setResidentUpdatedOn( $arrValues['resident_updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setReviewDetailId( $intReviewDetailId ) {
		$this->set( 'm_intReviewDetailId', CStrings::strToIntDef( $intReviewDetailId, NULL, false ) );
	}

	public function getReviewDetailId() {
		return $this->m_intReviewDetailId;
	}

	public function sqlReviewDetailId() {
		return ( true == isset( $this->m_intReviewDetailId ) ) ? ( string ) $this->m_intReviewDetailId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setReviewTypeId( $intReviewTypeId ) {
		$this->set( 'm_intReviewTypeId', CStrings::strToIntDef( $intReviewTypeId, NULL, false ) );
	}

	public function getReviewTypeId() {
		return $this->m_intReviewTypeId;
	}

	public function sqlReviewTypeId() {
		return ( true == isset( $this->m_intReviewTypeId ) ) ? ( string ) $this->m_intReviewTypeId : 'NULL';
	}

	public function setShowUntil( $strShowUntil ) {
		$this->set( 'm_strShowUntil', CStrings::strTrimDef( $strShowUntil, -1, NULL, true ) );
	}

	public function getShowUntil() {
		return $this->m_strShowUntil;
	}

	public function sqlShowUntil() {
		return ( true == isset( $this->m_strShowUntil ) ) ? '\'' . $this->m_strShowUntil . '\'' : 'NULL';
	}

	public function setFromDescription( $strFromDescription ) {
		$this->set( 'm_strFromDescription', CStrings::strTrimDef( $strFromDescription, 240, NULL, true ) );
	}

	public function getFromDescription() {
		return $this->m_strFromDescription;
	}

	public function sqlFromDescription() {
		return ( true == isset( $this->m_strFromDescription ) ) ? '\'' . addslashes( $this->m_strFromDescription ) . '\'' : 'NULL';
	}

	public function setExternalReviewId( $strExternalReviewId ) {
		$this->set( 'm_strExternalReviewId', CStrings::strTrimDef( $strExternalReviewId, 64, NULL, true ) );
	}

	public function getExternalReviewId() {
		return $this->m_strExternalReviewId;
	}

	public function sqlExternalReviewId() {
		return ( true == isset( $this->m_strExternalReviewId ) ) ? '\'' . addslashes( $this->m_strExternalReviewId ) . '\'' : 'NULL';
	}

	public function setResidentApprovedOn( $strResidentApprovedOn ) {
		$this->set( 'm_strResidentApprovedOn', CStrings::strTrimDef( $strResidentApprovedOn, -1, NULL, true ) );
	}

	public function getResidentApprovedOn() {
		return $this->m_strResidentApprovedOn;
	}

	public function sqlResidentApprovedOn() {
		return ( true == isset( $this->m_strResidentApprovedOn ) ) ? '\'' . $this->m_strResidentApprovedOn . '\'' : 'NULL';
	}

	public function setShowOnCorporateSites( $boolShowOnCorporateSites ) {
		$this->set( 'm_boolShowOnCorporateSites', CStrings::strToBool( $boolShowOnCorporateSites ) );
	}

	public function getShowOnCorporateSites() {
		return $this->m_boolShowOnCorporateSites;
	}

	public function sqlShowOnCorporateSites() {
		return ( true == isset( $this->m_boolShowOnCorporateSites ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnCorporateSites ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSyndicationAllowed( $boolIsSyndicationAllowed ) {
		$this->set( 'm_boolIsSyndicationAllowed', CStrings::strToBool( $boolIsSyndicationAllowed ) );
	}

	public function getIsSyndicationAllowed() {
		return $this->m_boolIsSyndicationAllowed;
	}

	public function sqlIsSyndicationAllowed() {
		return ( true == isset( $this->m_boolIsSyndicationAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSyndicationAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowOnDashboard( $boolShowOnDashboard ) {
		$this->set( 'm_boolShowOnDashboard', CStrings::strToBool( $boolShowOnDashboard ) );
	}

	public function getShowOnDashboard() {
		return $this->m_boolShowOnDashboard;
	}

	public function sqlShowOnDashboard() {
		return ( true == isset( $this->m_boolShowOnDashboard ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnDashboard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOriginalName( $strOriginalName ) {
		$this->set( 'm_strOriginalName', CStrings::strTrimDef( $strOriginalName, 240, NULL, true ) );
	}

	public function getOriginalName() {
		return $this->m_strOriginalName;
	}

	public function sqlOriginalName() {
		return ( true == isset( $this->m_strOriginalName ) ) ? '\'' . addslashes( $this->m_strOriginalName ) . '\'' : 'NULL';
	}

	public function setOriginalEmailAddress( $strOriginalEmailAddress ) {
		$this->set( 'm_strOriginalEmailAddress', CStrings::strTrimDef( $strOriginalEmailAddress, 240, NULL, true ) );
	}

	public function getOriginalEmailAddress() {
		return $this->m_strOriginalEmailAddress;
	}

	public function sqlOriginalEmailAddress() {
		return ( true == isset( $this->m_strOriginalEmailAddress ) ) ? '\'' . addslashes( $this->m_strOriginalEmailAddress ) . '\'' : 'NULL';
	}

	public function setCustomerLinkedBy( $intCustomerLinkedBy ) {
		$this->set( 'm_intCustomerLinkedBy', CStrings::strToIntDef( $intCustomerLinkedBy, NULL, false ) );
	}

	public function getCustomerLinkedBy() {
		return $this->m_intCustomerLinkedBy;
	}

	public function sqlCustomerLinkedBy() {
		return ( true == isset( $this->m_intCustomerLinkedBy ) ) ? ( string ) $this->m_intCustomerLinkedBy : 'NULL';
	}

	public function setCustomerLinkedOn( $strCustomerLinkedOn ) {
		$this->set( 'm_strCustomerLinkedOn', CStrings::strTrimDef( $strCustomerLinkedOn, -1, NULL, true ) );
	}

	public function getCustomerLinkedOn() {
		return $this->m_strCustomerLinkedOn;
	}

	public function sqlCustomerLinkedOn() {
		return ( true == isset( $this->m_strCustomerLinkedOn ) ) ? '\'' . $this->m_strCustomerLinkedOn . '\'' : 'NULL';
	}

	public function setResidentDeletedOn( $strResidentDeletedOn ) {
		$this->set( 'm_strResidentDeletedOn', CStrings::strTrimDef( $strResidentDeletedOn, -1, NULL, true ) );
	}

	public function getResidentDeletedOn() {
		return $this->m_strResidentDeletedOn;
	}

	public function sqlResidentDeletedOn() {
		return ( true == isset( $this->m_strResidentDeletedOn ) ) ? '\'' . $this->m_strResidentDeletedOn . '\'' : 'NULL';
	}

	public function setResidentUpdatedOn( $strResidentUpdatedOn ) {
		$this->set( 'm_strResidentUpdatedOn', CStrings::strTrimDef( $strResidentUpdatedOn, -1, NULL, true ) );
	}

	public function getResidentUpdatedOn() {
		return $this->m_strResidentUpdatedOn;
	}

	public function sqlResidentUpdatedOn() {
		return ( true == isset( $this->m_strResidentUpdatedOn ) ) ? '\'' . $this->m_strResidentUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, customer_id, review_detail_id, company_media_file_id, review_type_id, show_until, from_description, external_review_id, resident_approved_on, show_on_corporate_sites, is_syndication_allowed, show_on_dashboard, original_name, original_email_address, customer_linked_by, customer_linked_on, resident_deleted_on, resident_updated_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlReviewDetailId() . ', ' .
 						$this->sqlCompanyMediaFileId() . ', ' .
 						$this->sqlReviewTypeId() . ', ' .
 						$this->sqlShowUntil() . ', ' .
 						$this->sqlFromDescription() . ', ' .
 						$this->sqlExternalReviewId() . ', ' .
 						$this->sqlResidentApprovedOn() . ', ' .
 						$this->sqlShowOnCorporateSites() . ', ' .
 						$this->sqlIsSyndicationAllowed() . ', ' .
 						$this->sqlShowOnDashboard() . ', ' .
 						$this->sqlOriginalName() . ', ' .
 						$this->sqlOriginalEmailAddress() . ', ' .
 						$this->sqlCustomerLinkedBy() . ', ' .
 						$this->sqlCustomerLinkedOn() . ', ' .
 						$this->sqlResidentDeletedOn() . ', ' .
 						$this->sqlResidentUpdatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId() . ','; } elseif( true == array_key_exists( 'ReviewDetailId', $this->getChangedColumns() ) ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_type_id = ' . $this->sqlReviewTypeId() . ','; } elseif( true == array_key_exists( 'ReviewTypeId', $this->getChangedColumns() ) ) { $strSql .= ' review_type_id = ' . $this->sqlReviewTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_until = ' . $this->sqlShowUntil() . ','; } elseif( true == array_key_exists( 'ShowUntil', $this->getChangedColumns() ) ) { $strSql .= ' show_until = ' . $this->sqlShowUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_description = ' . $this->sqlFromDescription() . ','; } elseif( true == array_key_exists( 'FromDescription', $this->getChangedColumns() ) ) { $strSql .= ' from_description = ' . $this->sqlFromDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_review_id = ' . $this->sqlExternalReviewId() . ','; } elseif( true == array_key_exists( 'ExternalReviewId', $this->getChangedColumns() ) ) { $strSql .= ' external_review_id = ' . $this->sqlExternalReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_approved_on = ' . $this->sqlResidentApprovedOn() . ','; } elseif( true == array_key_exists( 'ResidentApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' resident_approved_on = ' . $this->sqlResidentApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_corporate_sites = ' . $this->sqlShowOnCorporateSites() . ','; } elseif( true == array_key_exists( 'ShowOnCorporateSites', $this->getChangedColumns() ) ) { $strSql .= ' show_on_corporate_sites = ' . $this->sqlShowOnCorporateSites() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_syndication_allowed = ' . $this->sqlIsSyndicationAllowed() . ','; } elseif( true == array_key_exists( 'IsSyndicationAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_syndication_allowed = ' . $this->sqlIsSyndicationAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_dashboard = ' . $this->sqlShowOnDashboard() . ','; } elseif( true == array_key_exists( 'ShowOnDashboard', $this->getChangedColumns() ) ) { $strSql .= ' show_on_dashboard = ' . $this->sqlShowOnDashboard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_name = ' . $this->sqlOriginalName() . ','; } elseif( true == array_key_exists( 'OriginalName', $this->getChangedColumns() ) ) { $strSql .= ' original_name = ' . $this->sqlOriginalName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_email_address = ' . $this->sqlOriginalEmailAddress() . ','; } elseif( true == array_key_exists( 'OriginalEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' original_email_address = ' . $this->sqlOriginalEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_linked_by = ' . $this->sqlCustomerLinkedBy() . ','; } elseif( true == array_key_exists( 'CustomerLinkedBy', $this->getChangedColumns() ) ) { $strSql .= ' customer_linked_by = ' . $this->sqlCustomerLinkedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_linked_on = ' . $this->sqlCustomerLinkedOn() . ','; } elseif( true == array_key_exists( 'CustomerLinkedOn', $this->getChangedColumns() ) ) { $strSql .= ' customer_linked_on = ' . $this->sqlCustomerLinkedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_deleted_on = ' . $this->sqlResidentDeletedOn() . ','; } elseif( true == array_key_exists( 'ResidentDeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' resident_deleted_on = ' . $this->sqlResidentDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_updated_on = ' . $this->sqlResidentUpdatedOn() . ','; } elseif( true == array_key_exists( 'ResidentUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' resident_updated_on = ' . $this->sqlResidentUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'customer_id' => $this->getCustomerId(),
			'review_detail_id' => $this->getReviewDetailId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'review_type_id' => $this->getReviewTypeId(),
			'show_until' => $this->getShowUntil(),
			'from_description' => $this->getFromDescription(),
			'external_review_id' => $this->getExternalReviewId(),
			'resident_approved_on' => $this->getResidentApprovedOn(),
			'show_on_corporate_sites' => $this->getShowOnCorporateSites(),
			'is_syndication_allowed' => $this->getIsSyndicationAllowed(),
			'show_on_dashboard' => $this->getShowOnDashboard(),
			'original_name' => $this->getOriginalName(),
			'original_email_address' => $this->getOriginalEmailAddress(),
			'customer_linked_by' => $this->getCustomerLinkedBy(),
			'customer_linked_on' => $this->getCustomerLinkedOn(),
			'resident_deleted_on' => $this->getResidentDeletedOn(),
			'resident_updated_on' => $this->getResidentUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>