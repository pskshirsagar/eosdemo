<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentFrequencies
 * Do not add any new functions to this class.
 */

class CBaseScheduledPaymentFrequencies extends CEosPluralBase {

	/**
	 * @return CScheduledPaymentFrequency[]
	 */
	public static function fetchScheduledPaymentFrequencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledPaymentFrequency::class, $objDatabase );
	}

	/**
	 * @return CScheduledPaymentFrequency
	 */
	public static function fetchScheduledPaymentFrequency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledPaymentFrequency::class, $objDatabase );
	}

	public static function fetchScheduledPaymentFrequencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_payment_frequencies', $objDatabase );
	}

	public static function fetchScheduledPaymentFrequencyById( $intId, $objDatabase ) {
		return self::fetchScheduledPaymentFrequency( sprintf( 'SELECT * FROM scheduled_payment_frequencies WHERE id = %d', $intId ), $objDatabase );
	}

}
?>