<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenuePostRule extends CEosSingularBase {

	const TABLE_NAME = 'public.revenue_post_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strPostRuleTarget;
	protected $m_strPostRuleMethod;
	protected $m_fltPostRuleValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPostRuleCategory;
	protected $m_strPostRuleRentSource;
	protected $m_strPostRuleRentType;
	protected $m_strPostRuleBoundary;

	public function __construct() {
		parent::__construct();

		$this->m_strPostRuleTarget = 'any';
		$this->m_strPostRuleMethod = 'percent';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['post_rule_target'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleTarget', trim( $arrValues['post_rule_target'] ) ); elseif( isset( $arrValues['post_rule_target'] ) ) $this->setPostRuleTarget( $arrValues['post_rule_target'] );
		if( isset( $arrValues['post_rule_method'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleMethod', trim( $arrValues['post_rule_method'] ) ); elseif( isset( $arrValues['post_rule_method'] ) ) $this->setPostRuleMethod( $arrValues['post_rule_method'] );
		if( isset( $arrValues['post_rule_value'] ) && $boolDirectSet ) $this->set( 'm_fltPostRuleValue', trim( $arrValues['post_rule_value'] ) ); elseif( isset( $arrValues['post_rule_value'] ) ) $this->setPostRuleValue( $arrValues['post_rule_value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['post_rule_category'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleCategory', trim( $arrValues['post_rule_category'] ) ); elseif( isset( $arrValues['post_rule_category'] ) ) $this->setPostRuleCategory( $arrValues['post_rule_category'] );
		if( isset( $arrValues['post_rule_rent_source'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleRentSource', trim( $arrValues['post_rule_rent_source'] ) ); elseif( isset( $arrValues['post_rule_rent_source'] ) ) $this->setPostRuleRentSource( $arrValues['post_rule_rent_source'] );
		if( isset( $arrValues['post_rule_rent_type'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleRentType', trim( $arrValues['post_rule_rent_type'] ) ); elseif( isset( $arrValues['post_rule_rent_type'] ) ) $this->setPostRuleRentType( $arrValues['post_rule_rent_type'] );
		if( isset( $arrValues['post_rule_boundary'] ) && $boolDirectSet ) $this->set( 'm_strPostRuleBoundary', trim( $arrValues['post_rule_boundary'] ) ); elseif( isset( $arrValues['post_rule_boundary'] ) ) $this->setPostRuleBoundary( $arrValues['post_rule_boundary'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPostRuleTarget( $strPostRuleTarget ) {
		$this->set( 'm_strPostRuleTarget', CStrings::strTrimDef( $strPostRuleTarget, 15, NULL, true ) );
	}

	public function getPostRuleTarget() {
		return $this->m_strPostRuleTarget;
	}

	public function sqlPostRuleTarget() {
		return ( true == isset( $this->m_strPostRuleTarget ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleTarget ) : '\'' . addslashes( $this->m_strPostRuleTarget ) . '\'' ) : '\'any\'';
	}

	public function setPostRuleMethod( $strPostRuleMethod ) {
		$this->set( 'm_strPostRuleMethod', CStrings::strTrimDef( $strPostRuleMethod, 10, NULL, true ) );
	}

	public function getPostRuleMethod() {
		return $this->m_strPostRuleMethod;
	}

	public function sqlPostRuleMethod() {
		return ( true == isset( $this->m_strPostRuleMethod ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleMethod ) : '\'' . addslashes( $this->m_strPostRuleMethod ) . '\'' ) : '\'percent\'';
	}

	public function setPostRuleValue( $fltPostRuleValue ) {
		$this->set( 'm_fltPostRuleValue', CStrings::strToFloatDef( $fltPostRuleValue, NULL, false, 2 ) );
	}

	public function getPostRuleValue() {
		return $this->m_fltPostRuleValue;
	}

	public function sqlPostRuleValue() {
		return ( true == isset( $this->m_fltPostRuleValue ) ) ? ( string ) $this->m_fltPostRuleValue : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPostRuleCategory( $strPostRuleCategory ) {
		$this->set( 'm_strPostRuleCategory', CStrings::strTrimDef( $strPostRuleCategory, 20, NULL, true ) );
	}

	public function getPostRuleCategory() {
		return $this->m_strPostRuleCategory;
	}

	public function sqlPostRuleCategory() {
		return ( true == isset( $this->m_strPostRuleCategory ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleCategory ) : '\'' . addslashes( $this->m_strPostRuleCategory ) . '\'' ) : 'NULL';
	}

	public function setPostRuleRentSource( $strPostRuleRentSource ) {
		$this->set( 'm_strPostRuleRentSource', CStrings::strTrimDef( $strPostRuleRentSource, 20, NULL, true ) );
	}

	public function getPostRuleRentSource() {
		return $this->m_strPostRuleRentSource;
	}

	public function sqlPostRuleRentSource() {
		return ( true == isset( $this->m_strPostRuleRentSource ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleRentSource ) : '\'' . addslashes( $this->m_strPostRuleRentSource ) . '\'' ) : 'NULL';
	}

	public function setPostRuleRentType( $strPostRuleRentType ) {
		$this->set( 'm_strPostRuleRentType', CStrings::strTrimDef( $strPostRuleRentType, 20, NULL, true ) );
	}

	public function getPostRuleRentType() {
		return $this->m_strPostRuleRentType;
	}

	public function sqlPostRuleRentType() {
		return ( true == isset( $this->m_strPostRuleRentType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleRentType ) : '\'' . addslashes( $this->m_strPostRuleRentType ) . '\'' ) : 'NULL';
	}

	public function setPostRuleBoundary( $strPostRuleBoundary ) {
		$this->set( 'm_strPostRuleBoundary', CStrings::strTrimDef( $strPostRuleBoundary, 5, NULL, true ) );
	}

	public function getPostRuleBoundary() {
		return $this->m_strPostRuleBoundary;
	}

	public function sqlPostRuleBoundary() {
		return ( true == isset( $this->m_strPostRuleBoundary ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostRuleBoundary ) : '\'' . addslashes( $this->m_strPostRuleBoundary ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, post_rule_target, post_rule_method, post_rule_value, updated_by, updated_on, created_by, created_on, post_rule_category, post_rule_rent_source, post_rule_rent_type, post_rule_boundary )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPostRuleTarget() . ', ' .
						$this->sqlPostRuleMethod() . ', ' .
						$this->sqlPostRuleValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPostRuleCategory() . ', ' .
						$this->sqlPostRuleRentSource() . ', ' .
						$this->sqlPostRuleRentType() . ', ' .
						$this->sqlPostRuleBoundary() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_target = ' . $this->sqlPostRuleTarget(). ',' ; } elseif( true == array_key_exists( 'PostRuleTarget', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_target = ' . $this->sqlPostRuleTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_method = ' . $this->sqlPostRuleMethod(). ',' ; } elseif( true == array_key_exists( 'PostRuleMethod', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_method = ' . $this->sqlPostRuleMethod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_value = ' . $this->sqlPostRuleValue(). ',' ; } elseif( true == array_key_exists( 'PostRuleValue', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_value = ' . $this->sqlPostRuleValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_category = ' . $this->sqlPostRuleCategory(). ',' ; } elseif( true == array_key_exists( 'PostRuleCategory', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_category = ' . $this->sqlPostRuleCategory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_rent_source = ' . $this->sqlPostRuleRentSource(). ',' ; } elseif( true == array_key_exists( 'PostRuleRentSource', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_rent_source = ' . $this->sqlPostRuleRentSource() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_rent_type = ' . $this->sqlPostRuleRentType(). ',' ; } elseif( true == array_key_exists( 'PostRuleRentType', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_rent_type = ' . $this->sqlPostRuleRentType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_rule_boundary = ' . $this->sqlPostRuleBoundary(). ',' ; } elseif( true == array_key_exists( 'PostRuleBoundary', $this->getChangedColumns() ) ) { $strSql .= ' post_rule_boundary = ' . $this->sqlPostRuleBoundary() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'post_rule_target' => $this->getPostRuleTarget(),
			'post_rule_method' => $this->getPostRuleMethod(),
			'post_rule_value' => $this->getPostRuleValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'post_rule_category' => $this->getPostRuleCategory(),
			'post_rule_rent_source' => $this->getPostRuleRentSource(),
			'post_rule_rent_type' => $this->getPostRuleRentType(),
			'post_rule_boundary' => $this->getPostRuleBoundary()
		);
	}

}
?>