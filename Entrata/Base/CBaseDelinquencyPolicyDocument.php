<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquencyPolicyDocument extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.delinquency_policy_documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDelinquencyPolicyId;
	protected $m_intDefaultDelinquencyPolicyDocumentId;
	protected $m_intFileTypeId;
	protected $m_intDocumentId;
	protected $m_intDelinquencyThresholdTypeId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_strEmailSubject;
	protected $m_intThresholdOffset;
	protected $m_intResendFrequency;
	protected $m_boolIsAutoSent;
	protected $m_boolIsStartEviction;
	protected $m_boolIsSendToCollections;
	protected $m_boolIsWriteOffBalance;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolEmailToResident;

	public function __construct() {
		parent::__construct();

		$this->m_intThresholdOffset = '0';
		$this->m_intResendFrequency = '0';
		$this->m_boolIsAutoSent = false;
		$this->m_boolIsStartEviction = false;
		$this->m_boolIsSendToCollections = false;
		$this->m_boolIsWriteOffBalance = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPolicyId', trim( $arrValues['delinquency_policy_id'] ) ); elseif( isset( $arrValues['delinquency_policy_id'] ) ) $this->setDelinquencyPolicyId( $arrValues['delinquency_policy_id'] );
		if( isset( $arrValues['default_delinquency_policy_document_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultDelinquencyPolicyDocumentId', trim( $arrValues['default_delinquency_policy_document_id'] ) ); elseif( isset( $arrValues['default_delinquency_policy_document_id'] ) ) $this->setDefaultDelinquencyPolicyDocumentId( $arrValues['default_delinquency_policy_document_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['delinquency_threshold_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyThresholdTypeId', trim( $arrValues['delinquency_threshold_type_id'] ) ); elseif( isset( $arrValues['delinquency_threshold_type_id'] ) ) $this->setDelinquencyThresholdTypeId( $arrValues['delinquency_threshold_type_id'] );
		if( isset( $arrValues['payment_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAllowanceTypeId', trim( $arrValues['payment_allowance_type_id'] ) ); elseif( isset( $arrValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrValues['payment_allowance_type_id'] );
		if( isset( $arrValues['email_subject'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strEmailSubject', trim( stripcslashes( $arrValues['email_subject'] ) ) ); elseif( isset( $arrValues['email_subject'] ) ) $this->setEmailSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_subject'] ) : $arrValues['email_subject'] );
		if( isset( $arrValues['threshold_offset'] ) && $boolDirectSet ) $this->set( 'm_intThresholdOffset', trim( $arrValues['threshold_offset'] ) ); elseif( isset( $arrValues['threshold_offset'] ) ) $this->setThresholdOffset( $arrValues['threshold_offset'] );
		if( isset( $arrValues['resend_frequency'] ) && $boolDirectSet ) $this->set( 'm_intResendFrequency', trim( $arrValues['resend_frequency'] ) ); elseif( isset( $arrValues['resend_frequency'] ) ) $this->setResendFrequency( $arrValues['resend_frequency'] );
		if( isset( $arrValues['is_auto_sent'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoSent', trim( stripcslashes( $arrValues['is_auto_sent'] ) ) ); elseif( isset( $arrValues['is_auto_sent'] ) ) $this->setIsAutoSent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_sent'] ) : $arrValues['is_auto_sent'] );
		if( isset( $arrValues['is_start_eviction'] ) && $boolDirectSet ) $this->set( 'm_boolIsStartEviction', trim( stripcslashes( $arrValues['is_start_eviction'] ) ) ); elseif( isset( $arrValues['is_start_eviction'] ) ) $this->setIsStartEviction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_start_eviction'] ) : $arrValues['is_start_eviction'] );
		if( isset( $arrValues['is_send_to_collections'] ) && $boolDirectSet ) $this->set( 'm_boolIsSendToCollections', trim( stripcslashes( $arrValues['is_send_to_collections'] ) ) ); elseif( isset( $arrValues['is_send_to_collections'] ) ) $this->setIsSendToCollections( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_send_to_collections'] ) : $arrValues['is_send_to_collections'] );
		if( isset( $arrValues['is_write_off_balance'] ) && $boolDirectSet ) $this->set( 'm_boolIsWriteOffBalance', trim( stripcslashes( $arrValues['is_write_off_balance'] ) ) ); elseif( isset( $arrValues['is_write_off_balance'] ) ) $this->setIsWriteOffBalance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_write_off_balance'] ) : $arrValues['is_write_off_balance'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['email_to_resident'] ) && $boolDirectSet ) $this->set( 'm_boolEmailToResident', trim( stripcslashes( $arrValues['email_to_resident'] ) ) ); elseif( isset( $arrValues['email_to_resident'] ) ) $this->setEmailToResident( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_to_resident'] ) : $arrValues['email_to_resident'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDelinquencyPolicyId( $intDelinquencyPolicyId ) {
		$this->set( 'm_intDelinquencyPolicyId', CStrings::strToIntDef( $intDelinquencyPolicyId, NULL, false ) );
	}

	public function getDelinquencyPolicyId() {
		return $this->m_intDelinquencyPolicyId;
	}

	public function sqlDelinquencyPolicyId() {
		return ( true == isset( $this->m_intDelinquencyPolicyId ) ) ? ( string ) $this->m_intDelinquencyPolicyId : 'NULL';
	}

	public function setDefaultDelinquencyPolicyDocumentId( $intDefaultDelinquencyPolicyDocumentId ) {
		$this->set( 'm_intDefaultDelinquencyPolicyDocumentId', CStrings::strToIntDef( $intDefaultDelinquencyPolicyDocumentId, NULL, false ) );
	}

	public function getDefaultDelinquencyPolicyDocumentId() {
		return $this->m_intDefaultDelinquencyPolicyDocumentId;
	}

	public function sqlDefaultDelinquencyPolicyDocumentId() {
		return ( true == isset( $this->m_intDefaultDelinquencyPolicyDocumentId ) ) ? ( string ) $this->m_intDefaultDelinquencyPolicyDocumentId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDelinquencyThresholdTypeId( $intDelinquencyThresholdTypeId ) {
		$this->set( 'm_intDelinquencyThresholdTypeId', CStrings::strToIntDef( $intDelinquencyThresholdTypeId, NULL, false ) );
	}

	public function getDelinquencyThresholdTypeId() {
		return $this->m_intDelinquencyThresholdTypeId;
	}

	public function sqlDelinquencyThresholdTypeId() {
		return ( true == isset( $this->m_intDelinquencyThresholdTypeId ) ) ? ( string ) $this->m_intDelinquencyThresholdTypeId : 'NULL';
	}

	public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
		$this->set( 'm_intPaymentAllowanceTypeId', CStrings::strToIntDef( $intPaymentAllowanceTypeId, NULL, false ) );
	}

	public function getPaymentAllowanceTypeId() {
		return $this->m_intPaymentAllowanceTypeId;
	}

	public function sqlPaymentAllowanceTypeId() {
		return ( true == isset( $this->m_intPaymentAllowanceTypeId ) ) ? ( string ) $this->m_intPaymentAllowanceTypeId : 'NULL';
	}

	public function setEmailSubject( $strEmailSubject, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strEmailSubject', CStrings::strTrimDef( $strEmailSubject, 100, NULL, true ), $strLocaleCode );
	}

	public function getEmailSubject( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strEmailSubject', $strLocaleCode );
	}

	public function sqlEmailSubject() {
		return ( true == isset( $this->m_strEmailSubject ) ) ? '\'' . addslashes( $this->m_strEmailSubject ) . '\'' : 'NULL';
	}

	public function setThresholdOffset( $intThresholdOffset ) {
		$this->set( 'm_intThresholdOffset', CStrings::strToIntDef( $intThresholdOffset, NULL, false ) );
	}

	public function getThresholdOffset() {
		return $this->m_intThresholdOffset;
	}

	public function sqlThresholdOffset() {
		return ( true == isset( $this->m_intThresholdOffset ) ) ? ( string ) $this->m_intThresholdOffset : '0';
	}

	public function setResendFrequency( $intResendFrequency ) {
		$this->set( 'm_intResendFrequency', CStrings::strToIntDef( $intResendFrequency, NULL, false ) );
	}

	public function getResendFrequency() {
		return $this->m_intResendFrequency;
	}

	public function sqlResendFrequency() {
		return ( true == isset( $this->m_intResendFrequency ) ) ? ( string ) $this->m_intResendFrequency : '0';
	}

	public function setIsAutoSent( $boolIsAutoSent ) {
		$this->set( 'm_boolIsAutoSent', CStrings::strToBool( $boolIsAutoSent ) );
	}

	public function getIsAutoSent() {
		return $this->m_boolIsAutoSent;
	}

	public function sqlIsAutoSent() {
		return ( true == isset( $this->m_boolIsAutoSent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoSent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsStartEviction( $boolIsStartEviction ) {
		$this->set( 'm_boolIsStartEviction', CStrings::strToBool( $boolIsStartEviction ) );
	}

	public function getIsStartEviction() {
		return $this->m_boolIsStartEviction;
	}

	public function sqlIsStartEviction() {
		return ( true == isset( $this->m_boolIsStartEviction ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStartEviction ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSendToCollections( $boolIsSendToCollections ) {
		$this->set( 'm_boolIsSendToCollections', CStrings::strToBool( $boolIsSendToCollections ) );
	}

	public function getIsSendToCollections() {
		return $this->m_boolIsSendToCollections;
	}

	public function sqlIsSendToCollections() {
		return ( true == isset( $this->m_boolIsSendToCollections ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSendToCollections ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsWriteOffBalance( $boolIsWriteOffBalance ) {
		$this->set( 'm_boolIsWriteOffBalance', CStrings::strToBool( $boolIsWriteOffBalance ) );
	}

	public function getIsWriteOffBalance() {
		return $this->m_boolIsWriteOffBalance;
	}

	public function sqlIsWriteOffBalance() {
		return ( true == isset( $this->m_boolIsWriteOffBalance ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsWriteOffBalance ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEmailToResident( $boolEmailToResident ) {
		$this->set( 'm_boolEmailToResident', CStrings::strToBool( $boolEmailToResident ) );
	}

	public function getEmailToResident() {
		return $this->m_boolEmailToResident;
	}

	public function sqlEmailToResident() {
		return ( true == isset( $this->m_boolEmailToResident ) ) ? '\'' . ( true == ( bool ) $this->m_boolEmailToResident ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, delinquency_policy_id, default_delinquency_policy_document_id, file_type_id, document_id, delinquency_threshold_type_id, payment_allowance_type_id, email_subject, threshold_offset, resend_frequency, is_auto_sent, is_start_eviction, is_send_to_collections, is_write_off_balance, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, email_to_resident )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDelinquencyPolicyId() . ', ' .
						$this->sqlDefaultDelinquencyPolicyDocumentId() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlDelinquencyThresholdTypeId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlEmailSubject() . ', ' .
						$this->sqlThresholdOffset() . ', ' .
						$this->sqlResendFrequency() . ', ' .
						$this->sqlIsAutoSent() . ', ' .
						$this->sqlIsStartEviction() . ', ' .
						$this->sqlIsSendToCollections() . ', ' .
						$this->sqlIsWriteOffBalance() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlEmailToResident() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId(). ',' ; } elseif( true == array_key_exists( 'DelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_delinquency_policy_document_id = ' . $this->sqlDefaultDelinquencyPolicyDocumentId(). ',' ; } elseif( true == array_key_exists( 'DefaultDelinquencyPolicyDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' default_delinquency_policy_document_id = ' . $this->sqlDefaultDelinquencyPolicyDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId(). ',' ; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_threshold_type_id = ' . $this->sqlDelinquencyThresholdTypeId(). ',' ; } elseif( true == array_key_exists( 'DelinquencyThresholdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_threshold_type_id = ' . $this->sqlDelinquencyThresholdTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject(). ',' ; } elseif( true == array_key_exists( 'EmailSubject', $this->getChangedColumns() ) ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' threshold_offset = ' . $this->sqlThresholdOffset(). ',' ; } elseif( true == array_key_exists( 'ThresholdOffset', $this->getChangedColumns() ) ) { $strSql .= ' threshold_offset = ' . $this->sqlThresholdOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resend_frequency = ' . $this->sqlResendFrequency(). ',' ; } elseif( true == array_key_exists( 'ResendFrequency', $this->getChangedColumns() ) ) { $strSql .= ' resend_frequency = ' . $this->sqlResendFrequency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_sent = ' . $this->sqlIsAutoSent(). ',' ; } elseif( true == array_key_exists( 'IsAutoSent', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_sent = ' . $this->sqlIsAutoSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_start_eviction = ' . $this->sqlIsStartEviction(). ',' ; } elseif( true == array_key_exists( 'IsStartEviction', $this->getChangedColumns() ) ) { $strSql .= ' is_start_eviction = ' . $this->sqlIsStartEviction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_to_collections = ' . $this->sqlIsSendToCollections(). ',' ; } elseif( true == array_key_exists( 'IsSendToCollections', $this->getChangedColumns() ) ) { $strSql .= ' is_send_to_collections = ' . $this->sqlIsSendToCollections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_write_off_balance = ' . $this->sqlIsWriteOffBalance(). ',' ; } elseif( true == array_key_exists( 'IsWriteOffBalance', $this->getChangedColumns() ) ) { $strSql .= ' is_write_off_balance = ' . $this->sqlIsWriteOffBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_to_resident = ' . $this->sqlEmailToResident(). ',' ; } elseif( true == array_key_exists( 'EmailToResident', $this->getChangedColumns() ) ) { $strSql .= ' email_to_resident = ' . $this->sqlEmailToResident() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'delinquency_policy_id' => $this->getDelinquencyPolicyId(),
			'default_delinquency_policy_document_id' => $this->getDefaultDelinquencyPolicyDocumentId(),
			'file_type_id' => $this->getFileTypeId(),
			'document_id' => $this->getDocumentId(),
			'delinquency_threshold_type_id' => $this->getDelinquencyThresholdTypeId(),
			'payment_allowance_type_id' => $this->getPaymentAllowanceTypeId(),
			'email_subject' => $this->getEmailSubject(),
			'threshold_offset' => $this->getThresholdOffset(),
			'resend_frequency' => $this->getResendFrequency(),
			'is_auto_sent' => $this->getIsAutoSent(),
			'is_start_eviction' => $this->getIsStartEviction(),
			'is_send_to_collections' => $this->getIsSendToCollections(),
			'is_write_off_balance' => $this->getIsWriteOffBalance(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'email_to_resident' => $this->getEmailToResident()
		);
	}

}
?>