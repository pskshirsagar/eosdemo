<?php

class CBaseMaintenanceRequestNoteTypes extends CEosPluralBase {

	public static function fetchMaintenanceRequestNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceRequestNoteType', $objDatabase );
	}

	public static function fetchMaintenanceRequestNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceRequestNoteType', $objDatabase );
	}

	public static function fetchMaintenanceRequestNoteTypeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_note_types', $objDatabase );
	}

	public static function fetchMaintenanceRequestNoteTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceRequestNoteType( sprintf( 'SELECT * FROM maintenance_request_note_types WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>