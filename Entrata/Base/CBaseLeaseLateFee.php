<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseLateFee extends CEosSingularBase {

	const TABLE_NAME = 'public.lease_late_fees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intCompanyLateFeeTypeId;
	protected $m_intLateFeeFormulaId;
	protected $m_intGraceDays;
	protected $m_fltMinimumBalanceDue;
	protected $m_fltMaximumMonthlyFeeAmount;
	protected $m_intCurrentChargesOnly;
	protected $m_intScheduledChargesOnly;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCompanyLateFeeTypeId = '1';
		$this->m_intGraceDays = '5';
		$this->m_fltMinimumBalanceDue = '0';
		$this->m_fltMaximumMonthlyFeeAmount = NULL;
		$this->m_intCurrentChargesOnly = '0';
		$this->m_intScheduledChargesOnly = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['company_late_fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyLateFeeTypeId', trim( $arrValues['company_late_fee_type_id'] ) ); elseif( isset( $arrValues['company_late_fee_type_id'] ) ) $this->setCompanyLateFeeTypeId( $arrValues['company_late_fee_type_id'] );
		if( isset( $arrValues['late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeFormulaId', trim( $arrValues['late_fee_formula_id'] ) ); elseif( isset( $arrValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrValues['late_fee_formula_id'] );
		if( isset( $arrValues['grace_days'] ) && $boolDirectSet ) $this->set( 'm_intGraceDays', trim( $arrValues['grace_days'] ) ); elseif( isset( $arrValues['grace_days'] ) ) $this->setGraceDays( $arrValues['grace_days'] );
		if( isset( $arrValues['minimum_balance_due'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumBalanceDue', trim( $arrValues['minimum_balance_due'] ) ); elseif( isset( $arrValues['minimum_balance_due'] ) ) $this->setMinimumBalanceDue( $arrValues['minimum_balance_due'] );
		if( isset( $arrValues['maximum_monthly_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaximumMonthlyFeeAmount', trim( $arrValues['maximum_monthly_fee_amount'] ) ); elseif( isset( $arrValues['maximum_monthly_fee_amount'] ) ) $this->setMaximumMonthlyFeeAmount( $arrValues['maximum_monthly_fee_amount'] );
		if( isset( $arrValues['current_charges_only'] ) && $boolDirectSet ) $this->set( 'm_intCurrentChargesOnly', trim( $arrValues['current_charges_only'] ) ); elseif( isset( $arrValues['current_charges_only'] ) ) $this->setCurrentChargesOnly( $arrValues['current_charges_only'] );
		if( isset( $arrValues['scheduled_charges_only'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargesOnly', trim( $arrValues['scheduled_charges_only'] ) ); elseif( isset( $arrValues['scheduled_charges_only'] ) ) $this->setScheduledChargesOnly( $arrValues['scheduled_charges_only'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCompanyLateFeeTypeId( $intCompanyLateFeeTypeId ) {
		$this->set( 'm_intCompanyLateFeeTypeId', CStrings::strToIntDef( $intCompanyLateFeeTypeId, NULL, false ) );
	}

	public function getCompanyLateFeeTypeId() {
		return $this->m_intCompanyLateFeeTypeId;
	}

	public function sqlCompanyLateFeeTypeId() {
		return ( true == isset( $this->m_intCompanyLateFeeTypeId ) ) ? ( string ) $this->m_intCompanyLateFeeTypeId : '1';
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->set( 'm_intLateFeeFormulaId', CStrings::strToIntDef( $intLateFeeFormulaId, NULL, false ) );
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function sqlLateFeeFormulaId() {
		return ( true == isset( $this->m_intLateFeeFormulaId ) ) ? ( string ) $this->m_intLateFeeFormulaId : 'NULL';
	}

	public function setGraceDays( $intGraceDays ) {
		$this->set( 'm_intGraceDays', CStrings::strToIntDef( $intGraceDays, NULL, false ) );
	}

	public function getGraceDays() {
		return $this->m_intGraceDays;
	}

	public function sqlGraceDays() {
		return ( true == isset( $this->m_intGraceDays ) ) ? ( string ) $this->m_intGraceDays : '5';
	}

	public function setMinimumBalanceDue( $fltMinimumBalanceDue ) {
		$this->set( 'm_fltMinimumBalanceDue', CStrings::strToFloatDef( $fltMinimumBalanceDue, NULL, false, 2 ) );
	}

	public function getMinimumBalanceDue() {
		return $this->m_fltMinimumBalanceDue;
	}

	public function sqlMinimumBalanceDue() {
		return ( true == isset( $this->m_fltMinimumBalanceDue ) ) ? ( string ) $this->m_fltMinimumBalanceDue : '0';
	}

	public function setMaximumMonthlyFeeAmount( $fltMaximumMonthlyFeeAmount ) {
		$this->set( 'm_fltMaximumMonthlyFeeAmount', CStrings::strToFloatDef( $fltMaximumMonthlyFeeAmount, NULL, false, 2 ) );
	}

	public function getMaximumMonthlyFeeAmount() {
		return $this->m_fltMaximumMonthlyFeeAmount;
	}

	public function sqlMaximumMonthlyFeeAmount() {
		return ( true == isset( $this->m_fltMaximumMonthlyFeeAmount ) ) ? ( string ) $this->m_fltMaximumMonthlyFeeAmount : 'NULL::numeric';
	}

	public function setCurrentChargesOnly( $intCurrentChargesOnly ) {
		$this->set( 'm_intCurrentChargesOnly', CStrings::strToIntDef( $intCurrentChargesOnly, NULL, false ) );
	}

	public function getCurrentChargesOnly() {
		return $this->m_intCurrentChargesOnly;
	}

	public function sqlCurrentChargesOnly() {
		return ( true == isset( $this->m_intCurrentChargesOnly ) ) ? ( string ) $this->m_intCurrentChargesOnly : '0';
	}

	public function setScheduledChargesOnly( $intScheduledChargesOnly ) {
		$this->set( 'm_intScheduledChargesOnly', CStrings::strToIntDef( $intScheduledChargesOnly, NULL, false ) );
	}

	public function getScheduledChargesOnly() {
		return $this->m_intScheduledChargesOnly;
	}

	public function sqlScheduledChargesOnly() {
		return ( true == isset( $this->m_intScheduledChargesOnly ) ) ? ( string ) $this->m_intScheduledChargesOnly : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, company_late_fee_type_id, late_fee_formula_id, grace_days, minimum_balance_due, maximum_monthly_fee_amount, current_charges_only, scheduled_charges_only, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCompanyLateFeeTypeId() . ', ' .
 						$this->sqlLateFeeFormulaId() . ', ' .
 						$this->sqlGraceDays() . ', ' .
 						$this->sqlMinimumBalanceDue() . ', ' .
 						$this->sqlMaximumMonthlyFeeAmount() . ', ' .
 						$this->sqlCurrentChargesOnly() . ', ' .
 						$this->sqlScheduledChargesOnly() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_late_fee_type_id = ' . $this->sqlCompanyLateFeeTypeId() . ','; } elseif( true == array_key_exists( 'CompanyLateFeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_late_fee_type_id = ' . $this->sqlCompanyLateFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ','; } elseif( true == array_key_exists( 'LateFeeFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays() . ','; } elseif( true == array_key_exists( 'GraceDays', $this->getChangedColumns() ) ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_balance_due = ' . $this->sqlMinimumBalanceDue() . ','; } elseif( true == array_key_exists( 'MinimumBalanceDue', $this->getChangedColumns() ) ) { $strSql .= ' minimum_balance_due = ' . $this->sqlMinimumBalanceDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_monthly_fee_amount = ' . $this->sqlMaximumMonthlyFeeAmount() . ','; } elseif( true == array_key_exists( 'MaximumMonthlyFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' maximum_monthly_fee_amount = ' . $this->sqlMaximumMonthlyFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_charges_only = ' . $this->sqlCurrentChargesOnly() . ','; } elseif( true == array_key_exists( 'CurrentChargesOnly', $this->getChangedColumns() ) ) { $strSql .= ' current_charges_only = ' . $this->sqlCurrentChargesOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charges_only = ' . $this->sqlScheduledChargesOnly() . ','; } elseif( true == array_key_exists( 'ScheduledChargesOnly', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charges_only = ' . $this->sqlScheduledChargesOnly() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'company_late_fee_type_id' => $this->getCompanyLateFeeTypeId(),
			'late_fee_formula_id' => $this->getLateFeeFormulaId(),
			'grace_days' => $this->getGraceDays(),
			'minimum_balance_due' => $this->getMinimumBalanceDue(),
			'maximum_monthly_fee_amount' => $this->getMaximumMonthlyFeeAmount(),
			'current_charges_only' => $this->getCurrentChargesOnly(),
			'scheduled_charges_only' => $this->getScheduledChargesOnly(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>