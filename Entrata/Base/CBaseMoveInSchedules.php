<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInSchedules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMoveInSchedules extends CEosPluralBase {

	/**
	 * @return CMoveInSchedule[]
	 */
	public static function fetchMoveInSchedules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMoveInSchedule::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMoveInSchedule
	 */
	public static function fetchMoveInSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoveInSchedule::class, $objDatabase );
	}

	public static function fetchMoveInScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'move_in_schedules', $objDatabase );
	}

	public static function fetchMoveInScheduleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMoveInSchedule( sprintf( 'SELECT * FROM move_in_schedules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInSchedulesByCid( $intCid, $objDatabase ) {
		return self::fetchMoveInSchedules( sprintf( 'SELECT * FROM move_in_schedules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInSchedulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMoveInSchedules( sprintf( 'SELECT * FROM move_in_schedules WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInSchedulesByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchMoveInSchedules( sprintf( 'SELECT * FROM move_in_schedules WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInSchedulesByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchMoveInSchedules( sprintf( 'SELECT * FROM move_in_schedules WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInSchedulesByMoveInScheduleTypeIdByCid( $intMoveInScheduleTypeId, $intCid, $objDatabase ) {
		return self::fetchMoveInSchedules( sprintf( 'SELECT * FROM move_in_schedules WHERE move_in_schedule_type_id = %d AND cid = %d', ( int ) $intMoveInScheduleTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>