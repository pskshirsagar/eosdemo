<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCheckTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCheckTemplates extends CEosPluralBase {

	/**
	 * @return CCheckTemplate[]
	 */
	public static function fetchCheckTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCheckTemplate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCheckTemplate
	 */
	public static function fetchCheckTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCheckTemplate', $objDatabase );
	}

	public static function fetchCheckTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'check_templates', $objDatabase );
	}

	public static function fetchCheckTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCheckTemplate( sprintf( 'SELECT * FROM check_templates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCheckTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchCheckTemplates( sprintf( 'SELECT * FROM check_templates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>