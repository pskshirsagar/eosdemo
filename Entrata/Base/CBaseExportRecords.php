<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExportRecords
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseExportRecords extends CEosPluralBase {

	/**
	 * @return CExportRecord[]
	 */
	public static function fetchExportRecords( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CExportRecord', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CExportRecord
	 */
	public static function fetchExportRecord( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CExportRecord', $objDatabase );
	}

	public static function fetchExportRecordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'export_records', $objDatabase );
	}

	public static function fetchExportRecordByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchExportRecord( sprintf( 'SELECT * FROM export_records WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchExportRecordsByCid( $intCid, $objDatabase ) {
		return self::fetchExportRecords( sprintf( 'SELECT * FROM export_records WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>