<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeFieldException extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_merge_field_exceptions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyMergeFieldId;
	protected $m_arrintPropertyGroupIds;
	protected $m_strExceptionName;
	protected $m_strDefaultValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intIsRequired;
	protected $m_intShowInMergeDisplay;
	protected $m_intAllowUserUpdate;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strViewExpression;
	protected $m_jsonViewExpression;

	public function __construct() {
		parent::__construct();

		$this->m_intIsRequired = '0';
		$this->m_intShowInMergeDisplay = '0';
		$this->m_intAllowUserUpdate = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMergeFieldId', trim( $arrValues['company_merge_field_id'] ) ); elseif( isset( $arrValues['company_merge_field_id'] ) ) $this->setCompanyMergeFieldId( $arrValues['company_merge_field_id'] );
		if( isset( $arrValues['property_group_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyGroupIds', trim( $arrValues['property_group_ids'] ) ); elseif( isset( $arrValues['property_group_ids'] ) ) $this->setPropertyGroupIds( $arrValues['property_group_ids'] );
		if( isset( $arrValues['exception_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strExceptionName', trim( stripcslashes( $arrValues['exception_name'] ) ) ); elseif( isset( $arrValues['exception_name'] ) ) $this->setExceptionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exception_name'] ) : $arrValues['exception_name'] );
		if( isset( $arrValues['default_value'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDefaultValue', trim( stripcslashes( $arrValues['default_value'] ) ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_value'] ) : $arrValues['default_value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['show_in_merge_display'] ) && $boolDirectSet ) $this->set( 'm_intShowInMergeDisplay', trim( $arrValues['show_in_merge_display'] ) ); elseif( isset( $arrValues['show_in_merge_display'] ) ) $this->setShowInMergeDisplay( $arrValues['show_in_merge_display'] );
		if( isset( $arrValues['allow_user_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowUserUpdate', trim( $arrValues['allow_user_update'] ) ); elseif( isset( $arrValues['allow_user_update'] ) ) $this->setAllowUserUpdate( $arrValues['allow_user_update'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['view_expression'] ) ) $this->set( 'm_strViewExpression', trim( $arrValues['view_expression'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyMergeFieldId( $intCompanyMergeFieldId ) {
		$this->set( 'm_intCompanyMergeFieldId', CStrings::strToIntDef( $intCompanyMergeFieldId, NULL, false ) );
	}

	public function getCompanyMergeFieldId() {
		return $this->m_intCompanyMergeFieldId;
	}

	public function sqlCompanyMergeFieldId() {
		return ( true == isset( $this->m_intCompanyMergeFieldId ) ) ? ( string ) $this->m_intCompanyMergeFieldId : 'NULL';
	}

	public function setPropertyGroupIds( $arrintPropertyGroupIds ) {
		$this->set( 'm_arrintPropertyGroupIds', CStrings::strToArrIntDef( $arrintPropertyGroupIds, NULL ) );
	}

	public function getPropertyGroupIds() {
		return $this->m_arrintPropertyGroupIds;
	}

	public function sqlPropertyGroupIds() {
		return ( true == isset( $this->m_arrintPropertyGroupIds ) && true == valArr( $this->m_arrintPropertyGroupIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyGroupIds, NULL ) . '\'' : 'NULL';
	}

	public function setExceptionName( $strExceptionName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strExceptionName', CStrings::strTrimDef( $strExceptionName, 100, NULL, true ), $strLocaleCode );
	}

	public function getExceptionName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strExceptionName', $strLocaleCode );
	}

	public function sqlExceptionName() {
		return ( true == isset( $this->m_strExceptionName ) ) ? '\'' . addslashes( $this->m_strExceptionName ) . '\'' : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ), $strLocaleCode );
	}

	public function getDefaultValue( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDefaultValue', $strLocaleCode );
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? '\'' . addslashes( $this->m_strDefaultValue ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setShowInMergeDisplay( $intShowInMergeDisplay ) {
		$this->set( 'm_intShowInMergeDisplay', CStrings::strToIntDef( $intShowInMergeDisplay, NULL, false ) );
	}

	public function getShowInMergeDisplay() {
		return $this->m_intShowInMergeDisplay;
	}

	public function sqlShowInMergeDisplay() {
		return ( true == isset( $this->m_intShowInMergeDisplay ) ) ? ( string ) $this->m_intShowInMergeDisplay : '0';
	}

	public function setAllowUserUpdate( $intAllowUserUpdate ) {
		$this->set( 'm_intAllowUserUpdate', CStrings::strToIntDef( $intAllowUserUpdate, NULL, false ) );
	}

	public function getAllowUserUpdate() {
		return $this->m_intAllowUserUpdate;
	}

	public function sqlAllowUserUpdate() {
		return ( true == isset( $this->m_intAllowUserUpdate ) ) ? ( string ) $this->m_intAllowUserUpdate : '0';
	}

	public function setViewExpression( $jsonViewExpression ) {
		if( true == valObj( $jsonViewExpression, 'stdClass' ) ) {
			$this->set( 'm_jsonViewExpression', $jsonViewExpression );
		} elseif( true == valJsonString( $jsonViewExpression ) ) {
			$this->set( 'm_jsonViewExpression', CStrings::strToJson( $jsonViewExpression ) );
		} else {
			$this->set( 'm_jsonViewExpression', NULL ); 
		}
		unset( $this->m_strViewExpression );
	}

	public function getViewExpression() {
		if( true == isset( $this->m_strViewExpression ) ) {
			$this->m_jsonViewExpression = CStrings::strToJson( $this->m_strViewExpression );
			unset( $this->m_strViewExpression );
		}
		return $this->m_jsonViewExpression;
	}

	public function sqlViewExpression() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getViewExpression() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getViewExpression() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_merge_field_id, property_group_ids, exception_name, default_value, updated_by, updated_on, created_by, created_on, is_required, show_in_merge_display, allow_user_update, details, view_expression )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyMergeFieldId() . ', ' .
						$this->sqlPropertyGroupIds() . ', ' .
						$this->sqlExceptionName() . ', ' .
						$this->sqlDefaultValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlShowInMergeDisplay() . ', ' .
						$this->sqlAllowUserUpdate() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlViewExpression() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merge_field_id = ' . $this->sqlCompanyMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'CompanyMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' company_merge_field_id = ' . $this->sqlCompanyMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds(). ',' ; } elseif( true == array_key_exists( 'PropertyGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_name = ' . $this->sqlExceptionName(). ',' ; } elseif( true == array_key_exists( 'ExceptionName', $this->getChangedColumns() ) ) { $strSql .= ' exception_name = ' . $this->sqlExceptionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue(). ',' ; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay(). ',' ; } elseif( true == array_key_exists( 'ShowInMergeDisplay', $this->getChangedColumns() ) ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowUserUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression(). ',' ; } elseif( true == array_key_exists( 'ViewExpression', $this->getChangedColumns() ) ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_merge_field_id' => $this->getCompanyMergeFieldId(),
			'property_group_ids' => $this->getPropertyGroupIds(),
			'exception_name' => $this->getExceptionName(),
			'default_value' => $this->getDefaultValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_required' => $this->getIsRequired(),
			'show_in_merge_display' => $this->getShowInMergeDisplay(),
			'allow_user_update' => $this->getAllowUserUpdate(),
			'details' => $this->getDetails(),
			'view_expression' => $this->getViewExpression()
		);
	}

}
?>