<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApp extends CEosSingularBase {

	const TABLE_NAME = 'public.company_apps';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAppId;
	protected $m_strUrl;
	protected $m_strInstallDatetime;
	protected $m_intExposeUserList;
	protected $m_intAllowAllProperties;
	protected $m_strRequestedOn;
	protected $m_strActivatedOn;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intNotified;
	protected $m_strNotifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intExposeUserList = '0';
		$this->m_intAllowAllProperties = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['app_id'] ) && $boolDirectSet ) $this->set( 'm_intAppId', trim( $arrValues['app_id'] ) ); elseif( isset( $arrValues['app_id'] ) ) $this->setAppId( $arrValues['app_id'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['install_datetime'] ) && $boolDirectSet ) $this->set( 'm_strInstallDatetime', trim( $arrValues['install_datetime'] ) ); elseif( isset( $arrValues['install_datetime'] ) ) $this->setInstallDatetime( $arrValues['install_datetime'] );
		if( isset( $arrValues['expose_user_list'] ) && $boolDirectSet ) $this->set( 'm_intExposeUserList', trim( $arrValues['expose_user_list'] ) ); elseif( isset( $arrValues['expose_user_list'] ) ) $this->setExposeUserList( $arrValues['expose_user_list'] );
		if( isset( $arrValues['allow_all_properties'] ) && $boolDirectSet ) $this->set( 'm_intAllowAllProperties', trim( $arrValues['allow_all_properties'] ) ); elseif( isset( $arrValues['allow_all_properties'] ) ) $this->setAllowAllProperties( $arrValues['allow_all_properties'] );
		if( isset( $arrValues['requested_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestedOn', trim( $arrValues['requested_on'] ) ); elseif( isset( $arrValues['requested_on'] ) ) $this->setRequestedOn( $arrValues['requested_on'] );
		if( isset( $arrValues['activated_on'] ) && $boolDirectSet ) $this->set( 'm_strActivatedOn', trim( $arrValues['activated_on'] ) ); elseif( isset( $arrValues['activated_on'] ) ) $this->setActivatedOn( $arrValues['activated_on'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['notified'] ) && $boolDirectSet ) $this->set( 'm_intNotified', trim( $arrValues['notified'] ) ); elseif( isset( $arrValues['notified'] ) ) $this->setNotified( $arrValues['notified'] );
		if( isset( $arrValues['notified_on'] ) && $boolDirectSet ) $this->set( 'm_strNotifiedOn', trim( $arrValues['notified_on'] ) ); elseif( isset( $arrValues['notified_on'] ) ) $this->setNotifiedOn( $arrValues['notified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAppId( $intAppId ) {
		$this->set( 'm_intAppId', CStrings::strToIntDef( $intAppId, NULL, false ) );
	}

	public function getAppId() {
		return $this->m_intAppId;
	}

	public function sqlAppId() {
		return ( true == isset( $this->m_intAppId ) ) ? ( string ) $this->m_intAppId : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 4096, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setInstallDatetime( $strInstallDatetime ) {
		$this->set( 'm_strInstallDatetime', CStrings::strTrimDef( $strInstallDatetime, -1, NULL, true ) );
	}

	public function getInstallDatetime() {
		return $this->m_strInstallDatetime;
	}

	public function sqlInstallDatetime() {
		return ( true == isset( $this->m_strInstallDatetime ) ) ? '\'' . $this->m_strInstallDatetime . '\'' : 'NULL';
	}

	public function setExposeUserList( $intExposeUserList ) {
		$this->set( 'm_intExposeUserList', CStrings::strToIntDef( $intExposeUserList, NULL, false ) );
	}

	public function getExposeUserList() {
		return $this->m_intExposeUserList;
	}

	public function sqlExposeUserList() {
		return ( true == isset( $this->m_intExposeUserList ) ) ? ( string ) $this->m_intExposeUserList : '0';
	}

	public function setAllowAllProperties( $intAllowAllProperties ) {
		$this->set( 'm_intAllowAllProperties', CStrings::strToIntDef( $intAllowAllProperties, NULL, false ) );
	}

	public function getAllowAllProperties() {
		return $this->m_intAllowAllProperties;
	}

	public function sqlAllowAllProperties() {
		return ( true == isset( $this->m_intAllowAllProperties ) ) ? ( string ) $this->m_intAllowAllProperties : '0';
	}

	public function setRequestedOn( $strRequestedOn ) {
		$this->set( 'm_strRequestedOn', CStrings::strTrimDef( $strRequestedOn, -1, NULL, true ) );
	}

	public function getRequestedOn() {
		return $this->m_strRequestedOn;
	}

	public function sqlRequestedOn() {
		return ( true == isset( $this->m_strRequestedOn ) ) ? '\'' . $this->m_strRequestedOn . '\'' : 'NULL';
	}

	public function setActivatedOn( $strActivatedOn ) {
		$this->set( 'm_strActivatedOn', CStrings::strTrimDef( $strActivatedOn, -1, NULL, true ) );
	}

	public function getActivatedOn() {
		return $this->m_strActivatedOn;
	}

	public function sqlActivatedOn() {
		return ( true == isset( $this->m_strActivatedOn ) ) ? '\'' . $this->m_strActivatedOn . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setNotified( $intNotified ) {
		$this->set( 'm_intNotified', CStrings::strToIntDef( $intNotified, NULL, false ) );
	}

	public function getNotified() {
		return $this->m_intNotified;
	}

	public function sqlNotified() {
		return ( true == isset( $this->m_intNotified ) ) ? ( string ) $this->m_intNotified : 'NULL';
	}

	public function setNotifiedOn( $strNotifiedOn ) {
		$this->set( 'm_strNotifiedOn', CStrings::strTrimDef( $strNotifiedOn, -1, NULL, true ) );
	}

	public function getNotifiedOn() {
		return $this->m_strNotifiedOn;
	}

	public function sqlNotifiedOn() {
		return ( true == isset( $this->m_strNotifiedOn ) ) ? '\'' . $this->m_strNotifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, app_id, url, install_datetime, expose_user_list, allow_all_properties, requested_on, activated_on, is_published, order_num, notified, notified_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAppId() . ', ' .
 						$this->sqlUrl() . ', ' .
 						$this->sqlInstallDatetime() . ', ' .
 						$this->sqlExposeUserList() . ', ' .
 						$this->sqlAllowAllProperties() . ', ' .
 						$this->sqlRequestedOn() . ', ' .
 						$this->sqlActivatedOn() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlNotified() . ', ' .
 						$this->sqlNotifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_id = ' . $this->sqlAppId() . ','; } elseif( true == array_key_exists( 'AppId', $this->getChangedColumns() ) ) { $strSql .= ' app_id = ' . $this->sqlAppId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' install_datetime = ' . $this->sqlInstallDatetime() . ','; } elseif( true == array_key_exists( 'InstallDatetime', $this->getChangedColumns() ) ) { $strSql .= ' install_datetime = ' . $this->sqlInstallDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expose_user_list = ' . $this->sqlExposeUserList() . ','; } elseif( true == array_key_exists( 'ExposeUserList', $this->getChangedColumns() ) ) { $strSql .= ' expose_user_list = ' . $this->sqlExposeUserList() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_all_properties = ' . $this->sqlAllowAllProperties() . ','; } elseif( true == array_key_exists( 'AllowAllProperties', $this->getChangedColumns() ) ) { $strSql .= ' allow_all_properties = ' . $this->sqlAllowAllProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; } elseif( true == array_key_exists( 'RequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; } elseif( true == array_key_exists( 'ActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notified = ' . $this->sqlNotified() . ','; } elseif( true == array_key_exists( 'Notified', $this->getChangedColumns() ) ) { $strSql .= ' notified = ' . $this->sqlNotified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; } elseif( true == array_key_exists( 'NotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'app_id' => $this->getAppId(),
			'url' => $this->getUrl(),
			'install_datetime' => $this->getInstallDatetime(),
			'expose_user_list' => $this->getExposeUserList(),
			'allow_all_properties' => $this->getAllowAllProperties(),
			'requested_on' => $this->getRequestedOn(),
			'activated_on' => $this->getActivatedOn(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'notified' => $this->getNotified(),
			'notified_on' => $this->getNotifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>