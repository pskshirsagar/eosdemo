<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReports
 * Do not add any new functions to this class.
 */

class CBaseDefaultReports extends CEosPluralBase {

	/**
	 * @return CDefaultReport[]
	 */
	public static function fetchDefaultReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReport::class, $objDatabase );
	}

	/**
	 * @return CDefaultReport
	 */
	public static function fetchDefaultReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReport::class, $objDatabase );
	}

	public static function fetchDefaultReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_reports', $objDatabase );
	}

	public static function fetchDefaultReportById( $intId, $objDatabase ) {
		return self::fetchDefaultReport( sprintf( 'SELECT * FROM default_reports WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultReportsByReportTypeId( $intReportTypeId, $objDatabase ) {
		return self::fetchDefaultReports( sprintf( 'SELECT * FROM default_reports WHERE report_type_id = %d', $intReportTypeId ), $objDatabase );
	}

	public static function fetchDefaultReportsByDefaultReportGroupId( $intDefaultReportGroupId, $objDatabase ) {
		return self::fetchDefaultReports( sprintf( 'SELECT * FROM default_reports WHERE default_report_group_id = %d', $intDefaultReportGroupId ), $objDatabase );
	}

}
?>