<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportHistory extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.report_histories';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReportScheduleTypeId;
	protected $m_intReportScheduleId;
	protected $m_intReportId;
	protected $m_intReportVersionId;
	protected $m_intReportFilterId;
	protected $m_intReportNewInstanceId;
	protected $m_intReportGroupId;
	protected $m_intReportNewGroupId;
	protected $m_strName;
	protected $m_strCorrelationId;
	protected $m_strStatus;
	protected $m_strErrors;
	protected $m_strFilters;
	protected $m_jsonFilters;
	protected $m_strDownloadOptions;
	protected $m_jsonDownloadOptions;
	protected $m_strQueuedDatetime;
	protected $m_strStartedDatetime;
	protected $m_strCompletedDatetime;
	protected $m_strRecipients;
	protected $m_jsonRecipients;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['report_schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportScheduleTypeId', trim( $arrValues['report_schedule_type_id'] ) ); elseif( isset( $arrValues['report_schedule_type_id'] ) ) $this->setReportScheduleTypeId( $arrValues['report_schedule_type_id'] );
		if( isset( $arrValues['report_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intReportScheduleId', trim( $arrValues['report_schedule_id'] ) ); elseif( isset( $arrValues['report_schedule_id'] ) ) $this->setReportScheduleId( $arrValues['report_schedule_id'] );
		if( isset( $arrValues['report_id'] ) && $boolDirectSet ) $this->set( 'm_intReportId', trim( $arrValues['report_id'] ) ); elseif( isset( $arrValues['report_id'] ) ) $this->setReportId( $arrValues['report_id'] );
		if( isset( $arrValues['report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intReportVersionId', trim( $arrValues['report_version_id'] ) ); elseif( isset( $arrValues['report_version_id'] ) ) $this->setReportVersionId( $arrValues['report_version_id'] );
		if( isset( $arrValues['report_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intReportFilterId', trim( $arrValues['report_filter_id'] ) ); elseif( isset( $arrValues['report_filter_id'] ) ) $this->setReportFilterId( $arrValues['report_filter_id'] );
		if( isset( $arrValues['report_new_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intReportNewInstanceId', trim( $arrValues['report_new_instance_id'] ) ); elseif( isset( $arrValues['report_new_instance_id'] ) ) $this->setReportNewInstanceId( $arrValues['report_new_instance_id'] );
		if( isset( $arrValues['report_group_id'] ) && $boolDirectSet ) $this->set( 'm_intReportGroupId', trim( $arrValues['report_group_id'] ) ); elseif( isset( $arrValues['report_group_id'] ) ) $this->setReportGroupId( $arrValues['report_group_id'] );
		if( isset( $arrValues['report_new_group_id'] ) && $boolDirectSet ) $this->set( 'm_intReportNewGroupId', trim( $arrValues['report_new_group_id'] ) ); elseif( isset( $arrValues['report_new_group_id'] ) ) $this->setReportNewGroupId( $arrValues['report_new_group_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['correlation_id'] ) && $boolDirectSet ) $this->set( 'm_strCorrelationId', trim( stripcslashes( $arrValues['correlation_id'] ) ) ); elseif( isset( $arrValues['correlation_id'] ) ) $this->setCorrelationId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['correlation_id'] ) : $arrValues['correlation_id'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['errors'] ) && $boolDirectSet ) $this->set( 'm_strErrors', trim( stripcslashes( $arrValues['errors'] ) ) ); elseif( isset( $arrValues['errors'] ) ) $this->setErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['errors'] ) : $arrValues['errors'] );
		if( isset( $arrValues['filters'] ) ) $this->set( 'm_strFilters', trim( $arrValues['filters'] ) );
		if( isset( $arrValues['download_options'] ) ) $this->set( 'm_strDownloadOptions', trim( $arrValues['download_options'] ) );
		if( isset( $arrValues['queued_datetime'] ) && $boolDirectSet ) $this->set( 'm_strQueuedDatetime', trim( $arrValues['queued_datetime'] ) ); elseif( isset( $arrValues['queued_datetime'] ) ) $this->setQueuedDatetime( $arrValues['queued_datetime'] );
		if( isset( $arrValues['started_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartedDatetime', trim( $arrValues['started_datetime'] ) ); elseif( isset( $arrValues['started_datetime'] ) ) $this->setStartedDatetime( $arrValues['started_datetime'] );
		if( isset( $arrValues['completed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCompletedDatetime', trim( $arrValues['completed_datetime'] ) ); elseif( isset( $arrValues['completed_datetime'] ) ) $this->setCompletedDatetime( $arrValues['completed_datetime'] );
		if( isset( $arrValues['recipients'] ) ) $this->set( 'm_strRecipients', trim( $arrValues['recipients'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReportScheduleTypeId( $intReportScheduleTypeId ) {
		$this->set( 'm_intReportScheduleTypeId', CStrings::strToIntDef( $intReportScheduleTypeId, NULL, false ) );
	}

	public function getReportScheduleTypeId() {
		return $this->m_intReportScheduleTypeId;
	}

	public function sqlReportScheduleTypeId() {
		return ( true == isset( $this->m_intReportScheduleTypeId ) ) ? ( string ) $this->m_intReportScheduleTypeId : 'NULL';
	}

	public function setReportScheduleId( $intReportScheduleId ) {
		$this->set( 'm_intReportScheduleId', CStrings::strToIntDef( $intReportScheduleId, NULL, false ) );
	}

	public function getReportScheduleId() {
		return $this->m_intReportScheduleId;
	}

	public function sqlReportScheduleId() {
		return ( true == isset( $this->m_intReportScheduleId ) ) ? ( string ) $this->m_intReportScheduleId : 'NULL';
	}

	public function setReportId( $intReportId ) {
		$this->set( 'm_intReportId', CStrings::strToIntDef( $intReportId, NULL, false ) );
	}

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function sqlReportId() {
		return ( true == isset( $this->m_intReportId ) ) ? ( string ) $this->m_intReportId : 'NULL';
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->set( 'm_intReportVersionId', CStrings::strToIntDef( $intReportVersionId, NULL, false ) );
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function sqlReportVersionId() {
		return ( true == isset( $this->m_intReportVersionId ) ) ? ( string ) $this->m_intReportVersionId : 'NULL';
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->set( 'm_intReportFilterId', CStrings::strToIntDef( $intReportFilterId, NULL, false ) );
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function sqlReportFilterId() {
		return ( true == isset( $this->m_intReportFilterId ) ) ? ( string ) $this->m_intReportFilterId : 'NULL';
	}

	public function setReportNewInstanceId( $intReportNewInstanceId ) {
		$this->set( 'm_intReportNewInstanceId', CStrings::strToIntDef( $intReportNewInstanceId, NULL, false ) );
	}

	public function getReportNewInstanceId() {
		return $this->m_intReportNewInstanceId;
	}

	public function sqlReportNewInstanceId() {
		return ( true == isset( $this->m_intReportNewInstanceId ) ) ? ( string ) $this->m_intReportNewInstanceId : 'NULL';
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->set( 'm_intReportGroupId', CStrings::strToIntDef( $intReportGroupId, NULL, false ) );
	}

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function sqlReportGroupId() {
		return ( true == isset( $this->m_intReportGroupId ) ) ? ( string ) $this->m_intReportGroupId : 'NULL';
	}

	public function setReportNewGroupId( $intReportNewGroupId ) {
		$this->set( 'm_intReportNewGroupId', CStrings::strToIntDef( $intReportNewGroupId, NULL, false ) );
	}

	public function getReportNewGroupId() {
		return $this->m_intReportNewGroupId;
	}

	public function sqlReportNewGroupId() {
		return ( true == isset( $this->m_intReportNewGroupId ) ) ? ( string ) $this->m_intReportNewGroupId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setCorrelationId( $strCorrelationId ) {
		$this->set( 'm_strCorrelationId', CStrings::strTrimDef( $strCorrelationId, 240, NULL, true ) );
	}

	public function getCorrelationId() {
		return $this->m_strCorrelationId;
	}

	public function sqlCorrelationId() {
		return ( true == isset( $this->m_strCorrelationId ) ) ? '\'' . addslashes( $this->m_strCorrelationId ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 240, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setErrors( $strErrors ) {
		$this->set( 'm_strErrors', CStrings::strTrimDef( $strErrors, -1, NULL, true ) );
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	public function sqlErrors() {
		return ( true == isset( $this->m_strErrors ) ) ? '\'' . addslashes( $this->m_strErrors ) . '\'' : 'NULL';
	}

	public function setFilters( $jsonFilters ) {
		if( true == valObj( $jsonFilters, 'stdClass' ) ) {
			$this->set( 'm_jsonFilters', $jsonFilters );
		} elseif( true == valJsonString( $jsonFilters ) ) {
			$this->set( 'm_jsonFilters', CStrings::strToJson( $jsonFilters ) );
		} else {
			$this->set( 'm_jsonFilters', NULL );
		}
		unset( $this->m_strFilters );
	}

	public function getFilters() {
		if( true == isset( $this->m_strFilters ) ) {
			$this->m_jsonFilters = CStrings::strToJson( $this->m_strFilters );
			unset( $this->m_strFilters );
		}
		return $this->m_jsonFilters;
	}

	public function sqlFilters() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getFilters() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getFilters() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setDownloadOptions( $jsonDownloadOptions ) {
		if( true == valObj( $jsonDownloadOptions, 'stdClass' ) ) {
			$this->set( 'm_jsonDownloadOptions', $jsonDownloadOptions );
		} elseif( true == valJsonString( $jsonDownloadOptions ) ) {
			$this->set( 'm_jsonDownloadOptions', CStrings::strToJson( $jsonDownloadOptions ) );
		} else {
			$this->set( 'm_jsonDownloadOptions', NULL );
		}
		unset( $this->m_strDownloadOptions );
	}

	public function getDownloadOptions() {
		if( true == isset( $this->m_strDownloadOptions ) ) {
			$this->m_jsonDownloadOptions = CStrings::strToJson( $this->m_strDownloadOptions );
			unset( $this->m_strDownloadOptions );
		}
		return $this->m_jsonDownloadOptions;
	}

	public function sqlDownloadOptions() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDownloadOptions() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getDownloadOptions() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setQueuedDatetime( $strQueuedDatetime ) {
		$this->set( 'm_strQueuedDatetime', CStrings::strTrimDef( $strQueuedDatetime, -1, NULL, true ) );
	}

	public function getQueuedDatetime() {
		return $this->m_strQueuedDatetime;
	}

	public function sqlQueuedDatetime() {
		return ( true == isset( $this->m_strQueuedDatetime ) ) ? '\'' . $this->m_strQueuedDatetime . '\'' : 'NULL';
	}

	public function setStartedDatetime( $strStartedDatetime ) {
		$this->set( 'm_strStartedDatetime', CStrings::strTrimDef( $strStartedDatetime, -1, NULL, true ) );
	}

	public function getStartedDatetime() {
		return $this->m_strStartedDatetime;
	}

	public function sqlStartedDatetime() {
		return ( true == isset( $this->m_strStartedDatetime ) ) ? '\'' . $this->m_strStartedDatetime . '\'' : 'NULL';
	}

	public function setCompletedDatetime( $strCompletedDatetime ) {
		$this->set( 'm_strCompletedDatetime', CStrings::strTrimDef( $strCompletedDatetime, -1, NULL, true ) );
	}

	public function getCompletedDatetime() {
		return $this->m_strCompletedDatetime;
	}

	public function sqlCompletedDatetime() {
		return ( true == isset( $this->m_strCompletedDatetime ) ) ? '\'' . $this->m_strCompletedDatetime . '\'' : 'NULL';
	}

	public function setRecipients( $jsonRecipients ) {
		if( true == valObj( $jsonRecipients, 'stdClass' ) ) {
			$this->set( 'm_jsonRecipients', $jsonRecipients );
		} elseif( true == valJsonString( $jsonRecipients ) ) {
			$this->set( 'm_jsonRecipients', CStrings::strToJson( $jsonRecipients ) );
		} else {
			$this->set( 'm_jsonRecipients', NULL );
		}
		unset( $this->m_strRecipients );
	}

	public function getRecipients() {
		if( true == isset( $this->m_strRecipients ) ) {
			$this->m_jsonRecipients = CStrings::strToJson( $this->m_strRecipients );
			unset( $this->m_strRecipients );
		}
		return $this->m_jsonRecipients;
	}

	public function sqlRecipients() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getRecipients() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getRecipients() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, report_schedule_type_id, report_schedule_id, report_id, report_version_id, report_filter_id, report_new_instance_id, report_group_id, report_new_group_id, name, correlation_id, status, errors, filters, download_options, queued_datetime, started_datetime, completed_datetime, recipients, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReportScheduleTypeId() . ', ' .
						$this->sqlReportScheduleId() . ', ' .
						$this->sqlReportId() . ', ' .
						$this->sqlReportVersionId() . ', ' .
						$this->sqlReportFilterId() . ', ' .
						$this->sqlReportNewInstanceId() . ', ' .
						$this->sqlReportGroupId() . ', ' .
						$this->sqlReportNewGroupId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlCorrelationId() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlErrors() . ', ' .
						$this->sqlFilters() . ', ' .
						$this->sqlDownloadOptions() . ', ' .
						$this->sqlQueuedDatetime() . ', ' .
						$this->sqlStartedDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ', ' .
						$this->sqlRecipients() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_schedule_type_id = ' . $this->sqlReportScheduleTypeId(). ',' ; } elseif( true == array_key_exists( 'ReportScheduleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_schedule_type_id = ' . $this->sqlReportScheduleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_schedule_id = ' . $this->sqlReportScheduleId(). ',' ; } elseif( true == array_key_exists( 'ReportScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' report_schedule_id = ' . $this->sqlReportScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_id = ' . $this->sqlReportId(). ',' ; } elseif( true == array_key_exists( 'ReportId', $this->getChangedColumns() ) ) { $strSql .= ' report_id = ' . $this->sqlReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId(). ',' ; } elseif( true == array_key_exists( 'ReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId(). ',' ; } elseif( true == array_key_exists( 'ReportFilterId', $this->getChangedColumns() ) ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId(). ',' ; } elseif( true == array_key_exists( 'ReportNewInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId(). ',' ; } elseif( true == array_key_exists( 'ReportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_new_group_id = ' . $this->sqlReportNewGroupId(). ',' ; } elseif( true == array_key_exists( 'ReportNewGroupId', $this->getChangedColumns() ) ) { $strSql .= ' report_new_group_id = ' . $this->sqlReportNewGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correlation_id = ' . $this->sqlCorrelationId(). ',' ; } elseif( true == array_key_exists( 'CorrelationId', $this->getChangedColumns() ) ) { $strSql .= ' correlation_id = ' . $this->sqlCorrelationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' errors = ' . $this->sqlErrors(). ',' ; } elseif( true == array_key_exists( 'Errors', $this->getChangedColumns() ) ) { $strSql .= ' errors = ' . $this->sqlErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filters = ' . $this->sqlFilters(). ',' ; } elseif( true == array_key_exists( 'Filters', $this->getChangedColumns() ) ) { $strSql .= ' filters = ' . $this->sqlFilters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' download_options = ' . $this->sqlDownloadOptions(). ',' ; } elseif( true == array_key_exists( 'DownloadOptions', $this->getChangedColumns() ) ) { $strSql .= ' download_options = ' . $this->sqlDownloadOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime(). ',' ; } elseif( true == array_key_exists( 'QueuedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime(). ',' ; } elseif( true == array_key_exists( 'StartedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime(). ',' ; } elseif( true == array_key_exists( 'CompletedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipients = ' . $this->sqlRecipients(). ',' ; } elseif( true == array_key_exists( 'Recipients', $this->getChangedColumns() ) ) { $strSql .= ' recipients = ' . $this->sqlRecipients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'report_schedule_type_id' => $this->getReportScheduleTypeId(),
			'report_schedule_id' => $this->getReportScheduleId(),
			'report_id' => $this->getReportId(),
			'report_version_id' => $this->getReportVersionId(),
			'report_filter_id' => $this->getReportFilterId(),
			'report_new_instance_id' => $this->getReportNewInstanceId(),
			'report_group_id' => $this->getReportGroupId(),
			'report_new_group_id' => $this->getReportNewGroupId(),
			'name' => $this->getName(),
			'correlation_id' => $this->getCorrelationId(),
			'status' => $this->getStatus(),
			'errors' => $this->getErrors(),
			'filters' => $this->getFilters(),
			'download_options' => $this->getDownloadOptions(),
			'queued_datetime' => $this->getQueuedDatetime(),
			'started_datetime' => $this->getStartedDatetime(),
			'completed_datetime' => $this->getCompletedDatetime(),
			'recipients' => $this->getRecipients(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>