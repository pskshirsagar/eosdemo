<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPaymentBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_payment_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintBankAccountIds;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_fltControlTotal;
	protected $m_intControlCount;
	protected $m_strBatchDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltControlTotal = '0';
		$this->m_intControlCount = '0';
		$this->m_strBatchDatetime = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['bank_account_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintBankAccountIds', trim( $arrValues['bank_account_ids'] ) ); elseif( isset( $arrValues['bank_account_ids'] ) ) $this->setBankAccountIds( $arrValues['bank_account_ids'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['control_total'] ) && $boolDirectSet ) $this->set( 'm_fltControlTotal', trim( $arrValues['control_total'] ) ); elseif( isset( $arrValues['control_total'] ) ) $this->setControlTotal( $arrValues['control_total'] );
		if( isset( $arrValues['control_count'] ) && $boolDirectSet ) $this->set( 'm_intControlCount', trim( $arrValues['control_count'] ) ); elseif( isset( $arrValues['control_count'] ) ) $this->setControlCount( $arrValues['control_count'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBankAccountIds( $arrintBankAccountIds ) {
		$this->set( 'm_arrintBankAccountIds', CStrings::strToArrIntDef( $arrintBankAccountIds, NULL ) );
	}

	public function getBankAccountIds() {
		return $this->m_arrintBankAccountIds;
	}

	public function sqlBankAccountIds() {
		return ( true == isset( $this->m_arrintBankAccountIds ) && true == valArr( $this->m_arrintBankAccountIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintBankAccountIds, NULL ) . '\'' : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->set( 'm_fltControlTotal', CStrings::strToFloatDef( $fltControlTotal, NULL, false, 2 ) );
	}

	public function getControlTotal() {
		return $this->m_fltControlTotal;
	}

	public function sqlControlTotal() {
		return ( true == isset( $this->m_fltControlTotal ) ) ? ( string ) $this->m_fltControlTotal : '0';
	}

	public function setControlCount( $intControlCount ) {
		$this->set( 'm_intControlCount', CStrings::strToIntDef( $intControlCount, NULL, false ) );
	}

	public function getControlCount() {
		return $this->m_intControlCount;
	}

	public function sqlControlCount() {
		return ( true == isset( $this->m_intControlCount ) ) ? ( string ) $this->m_intControlCount : '0';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, bank_account_ids, post_date, post_month, control_total, control_count, batch_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlBankAccountIds() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlControlTotal() . ', ' .
 						$this->sqlControlCount() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_ids = ' . $this->sqlBankAccountIds() . ','; } elseif( true == array_key_exists( 'BankAccountIds', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_ids = ' . $this->sqlBankAccountIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; } elseif( true == array_key_exists( 'ControlTotal', $this->getChangedColumns() ) ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_count = ' . $this->sqlControlCount() . ','; } elseif( true == array_key_exists( 'ControlCount', $this->getChangedColumns() ) ) { $strSql .= ' control_count = ' . $this->sqlControlCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'bank_account_ids' => $this->getBankAccountIds(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'control_total' => $this->getControlTotal(),
			'control_count' => $this->getControlCount(),
			'batch_datetime' => $this->getBatchDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>