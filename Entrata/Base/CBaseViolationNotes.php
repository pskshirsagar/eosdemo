<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationNotes extends CEosPluralBase {

	/**
	 * @return CViolationNote[]
	 */
	public static function fetchViolationNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolationNote::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolationNote
	 */
	public static function fetchViolationNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationNote::class, $objDatabase );
	}

	public static function fetchViolationNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_notes', $objDatabase );
	}

	public static function fetchViolationNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolationNote( sprintf( 'SELECT * FROM violation_notes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchViolationNotesByCid( $intCid, $objDatabase ) {
		return self::fetchViolationNotes( sprintf( 'SELECT * FROM violation_notes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchViolationNotesByViolationIdByCid( $intViolationId, $intCid, $objDatabase ) {
		return self::fetchViolationNotes( sprintf( 'SELECT * FROM violation_notes WHERE violation_id = %d AND cid = %d', $intViolationId, $intCid ), $objDatabase );
	}

}
?>