<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionNote extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.inspection_notes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInspectionId;
	protected $m_intInspectionFormLocationProblemId;
	protected $m_intInspectionResponseId;
	protected $m_intPropertyUnitMaintenanceLocationId;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['inspection_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionId', trim( $arrValues['inspection_id'] ) ); elseif( isset( $arrValues['inspection_id'] ) ) $this->setInspectionId( $arrValues['inspection_id'] );
		if( isset( $arrValues['inspection_form_location_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionFormLocationProblemId', trim( $arrValues['inspection_form_location_problem_id'] ) ); elseif( isset( $arrValues['inspection_form_location_problem_id'] ) ) $this->setInspectionFormLocationProblemId( $arrValues['inspection_form_location_problem_id'] );
		if( isset( $arrValues['inspection_response_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionResponseId', trim( $arrValues['inspection_response_id'] ) ); elseif( isset( $arrValues['inspection_response_id'] ) ) $this->setInspectionResponseId( $arrValues['inspection_response_id'] );
		if( isset( $arrValues['property_unit_maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitMaintenanceLocationId', trim( $arrValues['property_unit_maintenance_location_id'] ) ); elseif( isset( $arrValues['property_unit_maintenance_location_id'] ) ) $this->setPropertyUnitMaintenanceLocationId( $arrValues['property_unit_maintenance_location_id'] );
		if( isset( $arrValues['note'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInspectionId( $intInspectionId ) {
		$this->set( 'm_intInspectionId', CStrings::strToIntDef( $intInspectionId, NULL, false ) );
	}

	public function getInspectionId() {
		return $this->m_intInspectionId;
	}

	public function sqlInspectionId() {
		return ( true == isset( $this->m_intInspectionId ) ) ? ( string ) $this->m_intInspectionId : 'NULL';
	}

	public function setInspectionFormLocationProblemId( $intInspectionFormLocationProblemId ) {
		$this->set( 'm_intInspectionFormLocationProblemId', CStrings::strToIntDef( $intInspectionFormLocationProblemId, NULL, false ) );
	}

	public function getInspectionFormLocationProblemId() {
		return $this->m_intInspectionFormLocationProblemId;
	}

	public function sqlInspectionFormLocationProblemId() {
		return ( true == isset( $this->m_intInspectionFormLocationProblemId ) ) ? ( string ) $this->m_intInspectionFormLocationProblemId : 'NULL';
	}

	public function setInspectionResponseId( $intInspectionResponseId ) {
		$this->set( 'm_intInspectionResponseId', CStrings::strToIntDef( $intInspectionResponseId, NULL, false ) );
	}

	public function getInspectionResponseId() {
		return $this->m_intInspectionResponseId;
	}

	public function sqlInspectionResponseId() {
		return ( true == isset( $this->m_intInspectionResponseId ) ) ? ( string ) $this->m_intInspectionResponseId : 'NULL';
	}

	public function setPropertyUnitMaintenanceLocationId( $intPropertyUnitMaintenanceLocationId ) {
		$this->set( 'm_intPropertyUnitMaintenanceLocationId', CStrings::strToIntDef( $intPropertyUnitMaintenanceLocationId, NULL, false ) );
	}

	public function getPropertyUnitMaintenanceLocationId() {
		return $this->m_intPropertyUnitMaintenanceLocationId;
	}

	public function sqlPropertyUnitMaintenanceLocationId() {
		return ( true == isset( $this->m_intPropertyUnitMaintenanceLocationId ) ) ? ( string ) $this->m_intPropertyUnitMaintenanceLocationId : 'NULL';
	}

	public function setNote( $strNote, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ), $strLocaleCode );
	}

	public function getNote( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNote', $strLocaleCode );
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, inspection_id, inspection_form_location_problem_id, inspection_response_id, property_unit_maintenance_location_id, note, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlInspectionId() . ', ' .
						$this->sqlInspectionFormLocationProblemId() . ', ' .
						$this->sqlInspectionResponseId() . ', ' .
						$this->sqlPropertyUnitMaintenanceLocationId() . ', ' .
						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_id = ' . $this->sqlInspectionId(). ',' ; } elseif( true == array_key_exists( 'InspectionId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_id = ' . $this->sqlInspectionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_form_location_problem_id = ' . $this->sqlInspectionFormLocationProblemId(). ',' ; } elseif( true == array_key_exists( 'InspectionFormLocationProblemId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_form_location_problem_id = ' . $this->sqlInspectionFormLocationProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_response_id = ' . $this->sqlInspectionResponseId(). ',' ; } elseif( true == array_key_exists( 'InspectionResponseId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_response_id = ' . $this->sqlInspectionResponseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_maintenance_location_id = ' . $this->sqlPropertyUnitMaintenanceLocationId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitMaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_maintenance_location_id = ' . $this->sqlPropertyUnitMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'inspection_id' => $this->getInspectionId(),
			'inspection_form_location_problem_id' => $this->getInspectionFormLocationProblemId(),
			'inspection_response_id' => $this->getInspectionResponseId(),
			'property_unit_maintenance_location_id' => $this->getPropertyUnitMaintenanceLocationId(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>