<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCall extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_calls';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledCallTypeId;
	protected $m_intCallFrequencyId;
	protected $m_intCallPriorityId;
	protected $m_intVoiceTypeId;
	protected $m_strName;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strBeginSendTime;
	protected $m_strEndSendTime;
	protected $m_strWeekDays;
	protected $m_strMonthDays;
	protected $m_intIsOneTime;
	protected $m_intAllowBlock;
	protected $m_intCallToday;
	protected $m_strTestedOn;
	protected $m_strApprovedOn;
	protected $m_strLastSentOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsOneTime = '1';
		$this->m_intAllowBlock = '0';
		$this->m_intCallToday = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledCallTypeId', trim( $arrValues['scheduled_call_type_id'] ) ); elseif( isset( $arrValues['scheduled_call_type_id'] ) ) $this->setScheduledCallTypeId( $arrValues['scheduled_call_type_id'] );
		if( isset( $arrValues['call_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFrequencyId', trim( $arrValues['call_frequency_id'] ) ); elseif( isset( $arrValues['call_frequency_id'] ) ) $this->setCallFrequencyId( $arrValues['call_frequency_id'] );
		if( isset( $arrValues['call_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPriorityId', trim( $arrValues['call_priority_id'] ) ); elseif( isset( $arrValues['call_priority_id'] ) ) $this->setCallPriorityId( $arrValues['call_priority_id'] );
		if( isset( $arrValues['voice_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVoiceTypeId', trim( $arrValues['voice_type_id'] ) ); elseif( isset( $arrValues['voice_type_id'] ) ) $this->setVoiceTypeId( $arrValues['voice_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['begin_send_time'] ) && $boolDirectSet ) $this->set( 'm_strBeginSendTime', trim( $arrValues['begin_send_time'] ) ); elseif( isset( $arrValues['begin_send_time'] ) ) $this->setBeginSendTime( $arrValues['begin_send_time'] );
		if( isset( $arrValues['end_send_time'] ) && $boolDirectSet ) $this->set( 'm_strEndSendTime', trim( $arrValues['end_send_time'] ) ); elseif( isset( $arrValues['end_send_time'] ) ) $this->setEndSendTime( $arrValues['end_send_time'] );
		if( isset( $arrValues['week_days'] ) && $boolDirectSet ) $this->set( 'm_strWeekDays', trim( stripcslashes( $arrValues['week_days'] ) ) ); elseif( isset( $arrValues['week_days'] ) ) $this->setWeekDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['week_days'] ) : $arrValues['week_days'] );
		if( isset( $arrValues['month_days'] ) && $boolDirectSet ) $this->set( 'm_strMonthDays', trim( stripcslashes( $arrValues['month_days'] ) ) ); elseif( isset( $arrValues['month_days'] ) ) $this->setMonthDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['month_days'] ) : $arrValues['month_days'] );
		if( isset( $arrValues['is_one_time'] ) && $boolDirectSet ) $this->set( 'm_intIsOneTime', trim( $arrValues['is_one_time'] ) ); elseif( isset( $arrValues['is_one_time'] ) ) $this->setIsOneTime( $arrValues['is_one_time'] );
		if( isset( $arrValues['allow_block'] ) && $boolDirectSet ) $this->set( 'm_intAllowBlock', trim( $arrValues['allow_block'] ) ); elseif( isset( $arrValues['allow_block'] ) ) $this->setAllowBlock( $arrValues['allow_block'] );
		if( isset( $arrValues['call_today'] ) && $boolDirectSet ) $this->set( 'm_intCallToday', trim( $arrValues['call_today'] ) ); elseif( isset( $arrValues['call_today'] ) ) $this->setCallToday( $arrValues['call_today'] );
		if( isset( $arrValues['tested_on'] ) && $boolDirectSet ) $this->set( 'm_strTestedOn', trim( $arrValues['tested_on'] ) ); elseif( isset( $arrValues['tested_on'] ) ) $this->setTestedOn( $arrValues['tested_on'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['last_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSentOn', trim( $arrValues['last_sent_on'] ) ); elseif( isset( $arrValues['last_sent_on'] ) ) $this->setLastSentOn( $arrValues['last_sent_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledCallTypeId( $intScheduledCallTypeId ) {
		$this->set( 'm_intScheduledCallTypeId', CStrings::strToIntDef( $intScheduledCallTypeId, NULL, false ) );
	}

	public function getScheduledCallTypeId() {
		return $this->m_intScheduledCallTypeId;
	}

	public function sqlScheduledCallTypeId() {
		return ( true == isset( $this->m_intScheduledCallTypeId ) ) ? ( string ) $this->m_intScheduledCallTypeId : 'NULL';
	}

	public function setCallFrequencyId( $intCallFrequencyId ) {
		$this->set( 'm_intCallFrequencyId', CStrings::strToIntDef( $intCallFrequencyId, NULL, false ) );
	}

	public function getCallFrequencyId() {
		return $this->m_intCallFrequencyId;
	}

	public function sqlCallFrequencyId() {
		return ( true == isset( $this->m_intCallFrequencyId ) ) ? ( string ) $this->m_intCallFrequencyId : 'NULL';
	}

	public function setCallPriorityId( $intCallPriorityId ) {
		$this->set( 'm_intCallPriorityId', CStrings::strToIntDef( $intCallPriorityId, NULL, false ) );
	}

	public function getCallPriorityId() {
		return $this->m_intCallPriorityId;
	}

	public function sqlCallPriorityId() {
		return ( true == isset( $this->m_intCallPriorityId ) ) ? ( string ) $this->m_intCallPriorityId : 'NULL';
	}

	public function setVoiceTypeId( $intVoiceTypeId ) {
		$this->set( 'm_intVoiceTypeId', CStrings::strToIntDef( $intVoiceTypeId, NULL, false ) );
	}

	public function getVoiceTypeId() {
		return $this->m_intVoiceTypeId;
	}

	public function sqlVoiceTypeId() {
		return ( true == isset( $this->m_intVoiceTypeId ) ) ? ( string ) $this->m_intVoiceTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setBeginSendTime( $strBeginSendTime ) {
		$this->set( 'm_strBeginSendTime', CStrings::strTrimDef( $strBeginSendTime, -1, NULL, true ) );
	}

	public function getBeginSendTime() {
		return $this->m_strBeginSendTime;
	}

	public function sqlBeginSendTime() {
		return ( true == isset( $this->m_strBeginSendTime ) ) ? '\'' . $this->m_strBeginSendTime . '\'' : 'NULL';
	}

	public function setEndSendTime( $strEndSendTime ) {
		$this->set( 'm_strEndSendTime', CStrings::strTrimDef( $strEndSendTime, -1, NULL, true ) );
	}

	public function getEndSendTime() {
		return $this->m_strEndSendTime;
	}

	public function sqlEndSendTime() {
		return ( true == isset( $this->m_strEndSendTime ) ) ? '\'' . $this->m_strEndSendTime . '\'' : 'NULL';
	}

	public function setWeekDays( $strWeekDays ) {
		$this->set( 'm_strWeekDays', CStrings::strTrimDef( $strWeekDays, 240, NULL, true ) );
	}

	public function getWeekDays() {
		return $this->m_strWeekDays;
	}

	public function sqlWeekDays() {
		return ( true == isset( $this->m_strWeekDays ) ) ? '\'' . addslashes( $this->m_strWeekDays ) . '\'' : 'NULL';
	}

	public function setMonthDays( $strMonthDays ) {
		$this->set( 'm_strMonthDays', CStrings::strTrimDef( $strMonthDays, 240, NULL, true ) );
	}

	public function getMonthDays() {
		return $this->m_strMonthDays;
	}

	public function sqlMonthDays() {
		return ( true == isset( $this->m_strMonthDays ) ) ? '\'' . addslashes( $this->m_strMonthDays ) . '\'' : 'NULL';
	}

	public function setIsOneTime( $intIsOneTime ) {
		$this->set( 'm_intIsOneTime', CStrings::strToIntDef( $intIsOneTime, NULL, false ) );
	}

	public function getIsOneTime() {
		return $this->m_intIsOneTime;
	}

	public function sqlIsOneTime() {
		return ( true == isset( $this->m_intIsOneTime ) ) ? ( string ) $this->m_intIsOneTime : '1';
	}

	public function setAllowBlock( $intAllowBlock ) {
		$this->set( 'm_intAllowBlock', CStrings::strToIntDef( $intAllowBlock, NULL, false ) );
	}

	public function getAllowBlock() {
		return $this->m_intAllowBlock;
	}

	public function sqlAllowBlock() {
		return ( true == isset( $this->m_intAllowBlock ) ) ? ( string ) $this->m_intAllowBlock : '0';
	}

	public function setCallToday( $intCallToday ) {
		$this->set( 'm_intCallToday', CStrings::strToIntDef( $intCallToday, NULL, false ) );
	}

	public function getCallToday() {
		return $this->m_intCallToday;
	}

	public function sqlCallToday() {
		return ( true == isset( $this->m_intCallToday ) ) ? ( string ) $this->m_intCallToday : '0';
	}

	public function setTestedOn( $strTestedOn ) {
		$this->set( 'm_strTestedOn', CStrings::strTrimDef( $strTestedOn, -1, NULL, true ) );
	}

	public function getTestedOn() {
		return $this->m_strTestedOn;
	}

	public function sqlTestedOn() {
		return ( true == isset( $this->m_strTestedOn ) ) ? '\'' . $this->m_strTestedOn . '\'' : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setLastSentOn( $strLastSentOn ) {
		$this->set( 'm_strLastSentOn', CStrings::strTrimDef( $strLastSentOn, -1, NULL, true ) );
	}

	public function getLastSentOn() {
		return $this->m_strLastSentOn;
	}

	public function sqlLastSentOn() {
		return ( true == isset( $this->m_strLastSentOn ) ) ? '\'' . $this->m_strLastSentOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_call_type_id, call_frequency_id, call_priority_id, voice_type_id, name, start_date, end_date, begin_send_time, end_send_time, week_days, month_days, is_one_time, allow_block, call_today, tested_on, approved_on, last_sent_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScheduledCallTypeId() . ', ' .
 						$this->sqlCallFrequencyId() . ', ' .
 						$this->sqlCallPriorityId() . ', ' .
 						$this->sqlVoiceTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlBeginSendTime() . ', ' .
 						$this->sqlEndSendTime() . ', ' .
 						$this->sqlWeekDays() . ', ' .
 						$this->sqlMonthDays() . ', ' .
 						$this->sqlIsOneTime() . ', ' .
 						$this->sqlAllowBlock() . ', ' .
 						$this->sqlCallToday() . ', ' .
 						$this->sqlTestedOn() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlLastSentOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_call_type_id = ' . $this->sqlScheduledCallTypeId() . ','; } elseif( true == array_key_exists( 'ScheduledCallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_call_type_id = ' . $this->sqlScheduledCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_frequency_id = ' . $this->sqlCallFrequencyId() . ','; } elseif( true == array_key_exists( 'CallFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' call_frequency_id = ' . $this->sqlCallFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId() . ','; } elseif( true == array_key_exists( 'CallPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voice_type_id = ' . $this->sqlVoiceTypeId() . ','; } elseif( true == array_key_exists( 'VoiceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' voice_type_id = ' . $this->sqlVoiceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_send_time = ' . $this->sqlBeginSendTime() . ','; } elseif( true == array_key_exists( 'BeginSendTime', $this->getChangedColumns() ) ) { $strSql .= ' begin_send_time = ' . $this->sqlBeginSendTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_send_time = ' . $this->sqlEndSendTime() . ','; } elseif( true == array_key_exists( 'EndSendTime', $this->getChangedColumns() ) ) { $strSql .= ' end_send_time = ' . $this->sqlEndSendTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; } elseif( true == array_key_exists( 'WeekDays', $this->getChangedColumns() ) ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; } elseif( true == array_key_exists( 'MonthDays', $this->getChangedColumns() ) ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_one_time = ' . $this->sqlIsOneTime() . ','; } elseif( true == array_key_exists( 'IsOneTime', $this->getChangedColumns() ) ) { $strSql .= ' is_one_time = ' . $this->sqlIsOneTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_block = ' . $this->sqlAllowBlock() . ','; } elseif( true == array_key_exists( 'AllowBlock', $this->getChangedColumns() ) ) { $strSql .= ' allow_block = ' . $this->sqlAllowBlock() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_today = ' . $this->sqlCallToday() . ','; } elseif( true == array_key_exists( 'CallToday', $this->getChangedColumns() ) ) { $strSql .= ' call_today = ' . $this->sqlCallToday() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tested_on = ' . $this->sqlTestedOn() . ','; } elseif( true == array_key_exists( 'TestedOn', $this->getChangedColumns() ) ) { $strSql .= ' tested_on = ' . $this->sqlTestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; } elseif( true == array_key_exists( 'LastSentOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_call_type_id' => $this->getScheduledCallTypeId(),
			'call_frequency_id' => $this->getCallFrequencyId(),
			'call_priority_id' => $this->getCallPriorityId(),
			'voice_type_id' => $this->getVoiceTypeId(),
			'name' => $this->getName(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'begin_send_time' => $this->getBeginSendTime(),
			'end_send_time' => $this->getEndSendTime(),
			'week_days' => $this->getWeekDays(),
			'month_days' => $this->getMonthDays(),
			'is_one_time' => $this->getIsOneTime(),
			'allow_block' => $this->getAllowBlock(),
			'call_today' => $this->getCallToday(),
			'tested_on' => $this->getTestedOn(),
			'approved_on' => $this->getApprovedOn(),
			'last_sent_on' => $this->getLastSentOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>