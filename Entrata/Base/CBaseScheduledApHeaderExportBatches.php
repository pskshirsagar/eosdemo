<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApHeaderExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledApHeaderExportBatches extends CEosPluralBase {

	/**
	 * @return CScheduledApHeaderExportBatch[]
	 */
	public static function fetchScheduledApHeaderExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScheduledApHeaderExportBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledApHeaderExportBatch
	 */
	public static function fetchScheduledApHeaderExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledApHeaderExportBatch::class, $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_ap_header_export_batches', $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatch( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByApHeaderExportBatchTypeIdByCid( $intApHeaderExportBatchTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE ap_header_export_batch_type_id = %d AND cid = %d', $intApHeaderExportBatchTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByApHeaderExportFileFormatTypeIdByCid( $intApHeaderExportFileFormatTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE ap_header_export_file_format_type_id = %d AND cid = %d', $intApHeaderExportFileFormatTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE company_transmission_vendor_id = %d AND cid = %d', $intCompanyTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatches( sprintf( 'SELECT * FROM scheduled_ap_header_export_batches WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

}
?>