<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyHapRequestStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyHapRequestStatusType[]
	 */
	public static function fetchSubsidyHapRequestStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyHapRequestStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyHapRequestStatusType
	 */
	public static function fetchSubsidyHapRequestStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyHapRequestStatusType::class, $objDatabase );
	}

	public static function fetchSubsidyHapRequestStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_hap_request_status_types', $objDatabase );
	}

	public static function fetchSubsidyHapRequestStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyHapRequestStatusType( sprintf( 'SELECT * FROM subsidy_hap_request_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>