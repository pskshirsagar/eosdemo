<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApHeaderLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_header_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApHeaderId;
	protected $m_intApHeaderTypeId;
	protected $m_intApHeaderSubTypeId;
	protected $m_intApHeaderModeId;
	protected $m_intApPhysicalStatusTypeId;
	protected $m_intApFinancialStatusTypeId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intApPayeeAccountId;
	protected $m_intApRemittanceId;
	protected $m_intApPayeeTermId;
	protected $m_intGlTransactionTypeId;
	protected $m_intBulkJobPhaseId;
	protected $m_intBulkApContractId;
	protected $m_intBulkBankAccountId;
	protected $m_intBulkPropertyId;
	protected $m_intBulkApFormulaId;
	protected $m_intBulkUnitNumberId;
	protected $m_intBulkCompanyDepartmentId;
	protected $m_intBulkGlDimensionId;
	protected $m_intBulkPropertyBuildingId;
	protected $m_intBulkBudgetApHeaderId;
	protected $m_arrintApContractApHeaderIds;
	protected $m_arrintPoApHeaderIds;
	protected $m_intReversalApHeaderId;
	protected $m_intPaymentApHeaderId;
	protected $m_intScheduledApHeaderId;
	protected $m_intTemplateApHeaderId;
	protected $m_intReimbursementApHeaderId;
	protected $m_intFrequencyId;
	protected $m_intLeaseCustomerId;
	protected $m_intRefundArTransactionId;
	protected $m_intApBatchId;
	protected $m_intAccountingExportBatchId;
	protected $m_intApPaymentId;
	protected $m_intComplianceJobId;
	protected $m_intApRoutingTagId;
	protected $m_intBudgetChangeOrderId;
	protected $m_strAction;
	protected $m_strRemotePrimaryKey;
	protected $m_strTemplateName;
	protected $m_strTransactionDatetime;
	protected $m_strLogDatetime;
	protected $m_strPostDate;
	protected $m_strDueDate;
	protected $m_strPostMonth;
	protected $m_strScheduledPaymentDate;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intFrequencyInterval;
	protected $m_intDayOfWeek;
	protected $m_arrintDaysOfMonth;
	protected $m_intNumberOfOccurrences;
	protected $m_strLastPostedDate;
	protected $m_strNextPostDate;
	protected $m_strHeaderNumber;
	protected $m_strExternalUrl;
	protected $m_strAccountNumber;
	protected $m_strHeaderMemo;
	protected $m_strApprovalNote;
	protected $m_fltControlTotal;
	protected $m_fltPreApprovalAmount;
	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_fltTaxAmount;
	protected $m_fltDiscountAmount;
	protected $m_fltShippingAmount;
	protected $m_boolBulkIsConfidential;
	protected $m_boolBulkIs1099;
	protected $m_boolAutoCreatePo;
	protected $m_boolAutoApprovePo;
	protected $m_boolAutoCreateInvoice;
	protected $m_boolAutoApproveInvoice;
	protected $m_boolAutoPostInvoice;
	protected $m_boolPayWithSingleCheck;
	protected $m_boolIsDeleted;
	protected $m_boolIsReversed;
	protected $m_boolIsPrimary;
	protected $m_boolIsOnHold;
	protected $m_boolIsTemplate;
	protected $m_boolIsDisabled;
	protected $m_boolIsReimbursement;
	protected $m_boolIsBatching;
	protected $m_boolIsTemporary;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_strPostedOn;
	protected $m_strImportedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltRetentionAmountReleased;

	public function __construct() {
		parent::__construct();

		$this->m_intApHeaderTypeId = '5';
		$this->m_intApHeaderSubTypeId = '5';
		$this->m_intApHeaderModeId = '1';
		$this->m_intFrequencyId = '1';
		$this->m_intFrequencyInterval = '1';
		$this->m_fltControlTotal = '0';
		$this->m_fltPreApprovalAmount = '0';
		$this->m_fltTransactionAmount = '0';
		$this->m_fltTransactionAmountDue = '0';
		$this->m_fltTaxAmount = ( 0 );
		$this->m_fltDiscountAmount = ( 0 );
		$this->m_fltShippingAmount = ( 0 );
		$this->m_boolBulkIsConfidential = false;
		$this->m_boolBulkIs1099 = false;
		$this->m_boolAutoCreatePo = false;
		$this->m_boolAutoApprovePo = false;
		$this->m_boolAutoCreateInvoice = false;
		$this->m_boolAutoApproveInvoice = false;
		$this->m_boolAutoPostInvoice = false;
		$this->m_boolPayWithSingleCheck = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsReversed = false;
		$this->m_boolIsPrimary = false;
		$this->m_boolIsOnHold = false;
		$this->m_boolIsTemplate = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsReimbursement = false;
		$this->m_boolIsBatching = false;
		$this->m_boolIsTemporary = true;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;
		$this->m_fltRetentionAmountReleased = ( 0 );

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['ap_header_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderTypeId', trim( $arrValues['ap_header_type_id'] ) ); elseif( isset( $arrValues['ap_header_type_id'] ) ) $this->setApHeaderTypeId( $arrValues['ap_header_type_id'] );
		if( isset( $arrValues['ap_header_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderSubTypeId', trim( $arrValues['ap_header_sub_type_id'] ) ); elseif( isset( $arrValues['ap_header_sub_type_id'] ) ) $this->setApHeaderSubTypeId( $arrValues['ap_header_sub_type_id'] );
		if( isset( $arrValues['ap_header_mode_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderModeId', trim( $arrValues['ap_header_mode_id'] ) ); elseif( isset( $arrValues['ap_header_mode_id'] ) ) $this->setApHeaderModeId( $arrValues['ap_header_mode_id'] );
		if( isset( $arrValues['ap_physical_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPhysicalStatusTypeId', trim( $arrValues['ap_physical_status_type_id'] ) ); elseif( isset( $arrValues['ap_physical_status_type_id'] ) ) $this->setApPhysicalStatusTypeId( $arrValues['ap_physical_status_type_id'] );
		if( isset( $arrValues['ap_financial_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApFinancialStatusTypeId', trim( $arrValues['ap_financial_status_type_id'] ) ); elseif( isset( $arrValues['ap_financial_status_type_id'] ) ) $this->setApFinancialStatusTypeId( $arrValues['ap_financial_status_type_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intApRemittanceId', trim( $arrValues['ap_remittance_id'] ) ); elseif( isset( $arrValues['ap_remittance_id'] ) ) $this->setApRemittanceId( $arrValues['ap_remittance_id'] );
		if( isset( $arrValues['ap_payee_term_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeTermId', trim( $arrValues['ap_payee_term_id'] ) ); elseif( isset( $arrValues['ap_payee_term_id'] ) ) $this->setApPayeeTermId( $arrValues['ap_payee_term_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['bulk_job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkJobPhaseId', trim( $arrValues['bulk_job_phase_id'] ) ); elseif( isset( $arrValues['bulk_job_phase_id'] ) ) $this->setBulkJobPhaseId( $arrValues['bulk_job_phase_id'] );
		if( isset( $arrValues['bulk_ap_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkApContractId', trim( $arrValues['bulk_ap_contract_id'] ) ); elseif( isset( $arrValues['bulk_ap_contract_id'] ) ) $this->setBulkApContractId( $arrValues['bulk_ap_contract_id'] );
		if( isset( $arrValues['bulk_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkBankAccountId', trim( $arrValues['bulk_bank_account_id'] ) ); elseif( isset( $arrValues['bulk_bank_account_id'] ) ) $this->setBulkBankAccountId( $arrValues['bulk_bank_account_id'] );
		if( isset( $arrValues['bulk_property_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkPropertyId', trim( $arrValues['bulk_property_id'] ) ); elseif( isset( $arrValues['bulk_property_id'] ) ) $this->setBulkPropertyId( $arrValues['bulk_property_id'] );
		if( isset( $arrValues['bulk_ap_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkApFormulaId', trim( $arrValues['bulk_ap_formula_id'] ) ); elseif( isset( $arrValues['bulk_ap_formula_id'] ) ) $this->setBulkApFormulaId( $arrValues['bulk_ap_formula_id'] );
		if( isset( $arrValues['bulk_unit_number_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkUnitNumberId', trim( $arrValues['bulk_unit_number_id'] ) ); elseif( isset( $arrValues['bulk_unit_number_id'] ) ) $this->setBulkUnitNumberId( $arrValues['bulk_unit_number_id'] );
		if( isset( $arrValues['bulk_company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkCompanyDepartmentId', trim( $arrValues['bulk_company_department_id'] ) ); elseif( isset( $arrValues['bulk_company_department_id'] ) ) $this->setBulkCompanyDepartmentId( $arrValues['bulk_company_department_id'] );
		if( isset( $arrValues['bulk_gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkGlDimensionId', trim( $arrValues['bulk_gl_dimension_id'] ) ); elseif( isset( $arrValues['bulk_gl_dimension_id'] ) ) $this->setBulkGlDimensionId( $arrValues['bulk_gl_dimension_id'] );
		if( isset( $arrValues['bulk_property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkPropertyBuildingId', trim( $arrValues['bulk_property_building_id'] ) ); elseif( isset( $arrValues['bulk_property_building_id'] ) ) $this->setBulkPropertyBuildingId( $arrValues['bulk_property_building_id'] );
		if( isset( $arrValues['bulk_budget_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkBudgetApHeaderId', trim( $arrValues['bulk_budget_ap_header_id'] ) ); elseif( isset( $arrValues['bulk_budget_ap_header_id'] ) ) $this->setBulkBudgetApHeaderId( $arrValues['bulk_budget_ap_header_id'] );
		if( isset( $arrValues['ap_contract_ap_header_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintApContractApHeaderIds', trim( $arrValues['ap_contract_ap_header_ids'] ) ); elseif( isset( $arrValues['ap_contract_ap_header_ids'] ) ) $this->setApContractApHeaderIds( $arrValues['ap_contract_ap_header_ids'] );
		if( isset( $arrValues['po_ap_header_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPoApHeaderIds', trim( $arrValues['po_ap_header_ids'] ) ); elseif( isset( $arrValues['po_ap_header_ids'] ) ) $this->setPoApHeaderIds( $arrValues['po_ap_header_ids'] );
		if( isset( $arrValues['reversal_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReversalApHeaderId', trim( $arrValues['reversal_ap_header_id'] ) ); elseif( isset( $arrValues['reversal_ap_header_id'] ) ) $this->setReversalApHeaderId( $arrValues['reversal_ap_header_id'] );
		if( isset( $arrValues['payment_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentApHeaderId', trim( $arrValues['payment_ap_header_id'] ) ); elseif( isset( $arrValues['payment_ap_header_id'] ) ) $this->setPaymentApHeaderId( $arrValues['payment_ap_header_id'] );
		if( isset( $arrValues['scheduled_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledApHeaderId', trim( $arrValues['scheduled_ap_header_id'] ) ); elseif( isset( $arrValues['scheduled_ap_header_id'] ) ) $this->setScheduledApHeaderId( $arrValues['scheduled_ap_header_id'] );
		if( isset( $arrValues['template_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateApHeaderId', trim( $arrValues['template_ap_header_id'] ) ); elseif( isset( $arrValues['template_ap_header_id'] ) ) $this->setTemplateApHeaderId( $arrValues['template_ap_header_id'] );
		if( isset( $arrValues['reimbursement_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementApHeaderId', trim( $arrValues['reimbursement_ap_header_id'] ) ); elseif( isset( $arrValues['reimbursement_ap_header_id'] ) ) $this->setReimbursementApHeaderId( $arrValues['reimbursement_ap_header_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCustomerId', trim( $arrValues['lease_customer_id'] ) ); elseif( isset( $arrValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrValues['lease_customer_id'] );
		if( isset( $arrValues['refund_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundArTransactionId', trim( $arrValues['refund_ar_transaction_id'] ) ); elseif( isset( $arrValues['refund_ar_transaction_id'] ) ) $this->setRefundArTransactionId( $arrValues['refund_ar_transaction_id'] );
		if( isset( $arrValues['ap_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intApBatchId', trim( $arrValues['ap_batch_id'] ) ); elseif( isset( $arrValues['ap_batch_id'] ) ) $this->setApBatchId( $arrValues['ap_batch_id'] );
		if( isset( $arrValues['accounting_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportBatchId', trim( $arrValues['accounting_export_batch_id'] ) ); elseif( isset( $arrValues['accounting_export_batch_id'] ) ) $this->setAccountingExportBatchId( $arrValues['accounting_export_batch_id'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['compliance_job_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceJobId', trim( $arrValues['compliance_job_id'] ) ); elseif( isset( $arrValues['compliance_job_id'] ) ) $this->setComplianceJobId( $arrValues['compliance_job_id'] );
		if( isset( $arrValues['ap_routing_tag_id'] ) && $boolDirectSet ) $this->set( 'm_intApRoutingTagId', trim( $arrValues['ap_routing_tag_id'] ) ); elseif( isset( $arrValues['ap_routing_tag_id'] ) ) $this->setApRoutingTagId( $arrValues['ap_routing_tag_id'] );
		if( isset( $arrValues['budget_change_order_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetChangeOrderId', trim( $arrValues['budget_change_order_id'] ) ); elseif( isset( $arrValues['budget_change_order_id'] ) ) $this->setBudgetChangeOrderId( $arrValues['budget_change_order_id'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( $arrValues['action'] ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( $arrValues['action'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['template_name'] ) && $boolDirectSet ) $this->set( 'm_strTemplateName', trim( $arrValues['template_name'] ) ); elseif( isset( $arrValues['template_name'] ) ) $this->setTemplateName( $arrValues['template_name'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['scheduled_payment_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledPaymentDate', trim( $arrValues['scheduled_payment_date'] ) ); elseif( isset( $arrValues['scheduled_payment_date'] ) ) $this->setScheduledPaymentDate( $arrValues['scheduled_payment_date'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['frequency_interval'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyInterval', trim( $arrValues['frequency_interval'] ) ); elseif( isset( $arrValues['frequency_interval'] ) ) $this->setFrequencyInterval( $arrValues['frequency_interval'] );
		if( isset( $arrValues['day_of_week'] ) && $boolDirectSet ) $this->set( 'm_intDayOfWeek', trim( $arrValues['day_of_week'] ) ); elseif( isset( $arrValues['day_of_week'] ) ) $this->setDayOfWeek( $arrValues['day_of_week'] );
		if( isset( $arrValues['days_of_month'] ) && $boolDirectSet ) $this->set( 'm_arrintDaysOfMonth', trim( $arrValues['days_of_month'] ) ); elseif( isset( $arrValues['days_of_month'] ) ) $this->setDaysOfMonth( $arrValues['days_of_month'] );
		if( isset( $arrValues['number_of_occurrences'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOccurrences', trim( $arrValues['number_of_occurrences'] ) ); elseif( isset( $arrValues['number_of_occurrences'] ) ) $this->setNumberOfOccurrences( $arrValues['number_of_occurrences'] );
		if( isset( $arrValues['last_posted_date'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedDate', trim( $arrValues['last_posted_date'] ) ); elseif( isset( $arrValues['last_posted_date'] ) ) $this->setLastPostedDate( $arrValues['last_posted_date'] );
		if( isset( $arrValues['next_post_date'] ) && $boolDirectSet ) $this->set( 'm_strNextPostDate', trim( $arrValues['next_post_date'] ) ); elseif( isset( $arrValues['next_post_date'] ) ) $this->setNextPostDate( $arrValues['next_post_date'] );
		if( isset( $arrValues['header_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderNumber', trim( $arrValues['header_number'] ) ); elseif( isset( $arrValues['header_number'] ) ) $this->setHeaderNumber( $arrValues['header_number'] );
		if( isset( $arrValues['external_url'] ) && $boolDirectSet ) $this->set( 'm_strExternalUrl', trim( $arrValues['external_url'] ) ); elseif( isset( $arrValues['external_url'] ) ) $this->setExternalUrl( $arrValues['external_url'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( $arrValues['account_number'] ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( $arrValues['account_number'] );
		if( isset( $arrValues['header_memo'] ) && $boolDirectSet ) $this->set( 'm_strHeaderMemo', trim( $arrValues['header_memo'] ) ); elseif( isset( $arrValues['header_memo'] ) ) $this->setHeaderMemo( $arrValues['header_memo'] );
		if( isset( $arrValues['approval_note'] ) && $boolDirectSet ) $this->set( 'm_strApprovalNote', trim( $arrValues['approval_note'] ) ); elseif( isset( $arrValues['approval_note'] ) ) $this->setApprovalNote( $arrValues['approval_note'] );
		if( isset( $arrValues['control_total'] ) && $boolDirectSet ) $this->set( 'm_fltControlTotal', trim( $arrValues['control_total'] ) ); elseif( isset( $arrValues['control_total'] ) ) $this->setControlTotal( $arrValues['control_total'] );
		if( isset( $arrValues['pre_approval_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPreApprovalAmount', trim( $arrValues['pre_approval_amount'] ) ); elseif( isset( $arrValues['pre_approval_amount'] ) ) $this->setPreApprovalAmount( $arrValues['pre_approval_amount'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmountDue', trim( $arrValues['transaction_amount_due'] ) ); elseif( isset( $arrValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrValues['transaction_amount_due'] );
		if( isset( $arrValues['tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTaxAmount', trim( $arrValues['tax_amount'] ) ); elseif( isset( $arrValues['tax_amount'] ) ) $this->setTaxAmount( $arrValues['tax_amount'] );
		if( isset( $arrValues['discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDiscountAmount', trim( $arrValues['discount_amount'] ) ); elseif( isset( $arrValues['discount_amount'] ) ) $this->setDiscountAmount( $arrValues['discount_amount'] );
		if( isset( $arrValues['shipping_amount'] ) && $boolDirectSet ) $this->set( 'm_fltShippingAmount', trim( $arrValues['shipping_amount'] ) ); elseif( isset( $arrValues['shipping_amount'] ) ) $this->setShippingAmount( $arrValues['shipping_amount'] );
		if( isset( $arrValues['bulk_is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolBulkIsConfidential', trim( stripcslashes( $arrValues['bulk_is_confidential'] ) ) ); elseif( isset( $arrValues['bulk_is_confidential'] ) ) $this->setBulkIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bulk_is_confidential'] ) : $arrValues['bulk_is_confidential'] );
		if( isset( $arrValues['bulk_is_1099'] ) && $boolDirectSet ) $this->set( 'm_boolBulkIs1099', trim( stripcslashes( $arrValues['bulk_is_1099'] ) ) ); elseif( isset( $arrValues['bulk_is_1099'] ) ) $this->setBulkIs1099( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bulk_is_1099'] ) : $arrValues['bulk_is_1099'] );
		if( isset( $arrValues['auto_create_po'] ) && $boolDirectSet ) $this->set( 'm_boolAutoCreatePo', trim( stripcslashes( $arrValues['auto_create_po'] ) ) ); elseif( isset( $arrValues['auto_create_po'] ) ) $this->setAutoCreatePo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_create_po'] ) : $arrValues['auto_create_po'] );
		if( isset( $arrValues['auto_approve_po'] ) && $boolDirectSet ) $this->set( 'm_boolAutoApprovePo', trim( stripcslashes( $arrValues['auto_approve_po'] ) ) ); elseif( isset( $arrValues['auto_approve_po'] ) ) $this->setAutoApprovePo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_approve_po'] ) : $arrValues['auto_approve_po'] );
		if( isset( $arrValues['auto_create_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolAutoCreateInvoice', trim( stripcslashes( $arrValues['auto_create_invoice'] ) ) ); elseif( isset( $arrValues['auto_create_invoice'] ) ) $this->setAutoCreateInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_create_invoice'] ) : $arrValues['auto_create_invoice'] );
		if( isset( $arrValues['auto_approve_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolAutoApproveInvoice', trim( stripcslashes( $arrValues['auto_approve_invoice'] ) ) ); elseif( isset( $arrValues['auto_approve_invoice'] ) ) $this->setAutoApproveInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_approve_invoice'] ) : $arrValues['auto_approve_invoice'] );
		if( isset( $arrValues['auto_post_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolAutoPostInvoice', trim( stripcslashes( $arrValues['auto_post_invoice'] ) ) ); elseif( isset( $arrValues['auto_post_invoice'] ) ) $this->setAutoPostInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_post_invoice'] ) : $arrValues['auto_post_invoice'] );
		if( isset( $arrValues['pay_with_single_check'] ) && $boolDirectSet ) $this->set( 'm_boolPayWithSingleCheck', trim( stripcslashes( $arrValues['pay_with_single_check'] ) ) ); elseif( isset( $arrValues['pay_with_single_check'] ) ) $this->setPayWithSingleCheck( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pay_with_single_check'] ) : $arrValues['pay_with_single_check'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_reversed'] ) && $boolDirectSet ) $this->set( 'm_boolIsReversed', trim( stripcslashes( $arrValues['is_reversed'] ) ) ); elseif( isset( $arrValues['is_reversed'] ) ) $this->setIsReversed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reversed'] ) : $arrValues['is_reversed'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrimary', trim( stripcslashes( $arrValues['is_primary'] ) ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_primary'] ) : $arrValues['is_primary'] );
		if( isset( $arrValues['is_on_hold'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnHold', trim( stripcslashes( $arrValues['is_on_hold'] ) ) ); elseif( isset( $arrValues['is_on_hold'] ) ) $this->setIsOnHold( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_hold'] ) : $arrValues['is_on_hold'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplate', trim( stripcslashes( $arrValues['is_template'] ) ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_template'] ) : $arrValues['is_template'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_reimbursement'] ) && $boolDirectSet ) $this->set( 'm_boolIsReimbursement', trim( stripcslashes( $arrValues['is_reimbursement'] ) ) ); elseif( isset( $arrValues['is_reimbursement'] ) ) $this->setIsReimbursement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reimbursement'] ) : $arrValues['is_reimbursement'] );
		if( isset( $arrValues['is_batching'] ) && $boolDirectSet ) $this->set( 'm_boolIsBatching', trim( stripcslashes( $arrValues['is_batching'] ) ) ); elseif( isset( $arrValues['is_batching'] ) ) $this->setIsBatching( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_batching'] ) : $arrValues['is_batching'] );
		if( isset( $arrValues['is_temporary'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemporary', trim( stripcslashes( $arrValues['is_temporary'] ) ) ); elseif( isset( $arrValues['is_temporary'] ) ) $this->setIsTemporary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_temporary'] ) : $arrValues['is_temporary'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['retention_amount_released'] ) && $boolDirectSet ) $this->set( 'm_fltRetentionAmountReleased', trim( $arrValues['retention_amount_released'] ) ); elseif( isset( $arrValues['retention_amount_released'] ) ) $this->setRetentionAmountReleased( $arrValues['retention_amount_released'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setApHeaderTypeId( $intApHeaderTypeId ) {
		$this->set( 'm_intApHeaderTypeId', CStrings::strToIntDef( $intApHeaderTypeId, NULL, false ) );
	}

	public function getApHeaderTypeId() {
		return $this->m_intApHeaderTypeId;
	}

	public function sqlApHeaderTypeId() {
		return ( true == isset( $this->m_intApHeaderTypeId ) ) ? ( string ) $this->m_intApHeaderTypeId : '5';
	}

	public function setApHeaderSubTypeId( $intApHeaderSubTypeId ) {
		$this->set( 'm_intApHeaderSubTypeId', CStrings::strToIntDef( $intApHeaderSubTypeId, NULL, false ) );
	}

	public function getApHeaderSubTypeId() {
		return $this->m_intApHeaderSubTypeId;
	}

	public function sqlApHeaderSubTypeId() {
		return ( true == isset( $this->m_intApHeaderSubTypeId ) ) ? ( string ) $this->m_intApHeaderSubTypeId : '5';
	}

	public function setApHeaderModeId( $intApHeaderModeId ) {
		$this->set( 'm_intApHeaderModeId', CStrings::strToIntDef( $intApHeaderModeId, NULL, false ) );
	}

	public function getApHeaderModeId() {
		return $this->m_intApHeaderModeId;
	}

	public function sqlApHeaderModeId() {
		return ( true == isset( $this->m_intApHeaderModeId ) ) ? ( string ) $this->m_intApHeaderModeId : '1';
	}

	public function setApPhysicalStatusTypeId( $intApPhysicalStatusTypeId ) {
		$this->set( 'm_intApPhysicalStatusTypeId', CStrings::strToIntDef( $intApPhysicalStatusTypeId, NULL, false ) );
	}

	public function getApPhysicalStatusTypeId() {
		return $this->m_intApPhysicalStatusTypeId;
	}

	public function sqlApPhysicalStatusTypeId() {
		return ( true == isset( $this->m_intApPhysicalStatusTypeId ) ) ? ( string ) $this->m_intApPhysicalStatusTypeId : 'NULL';
	}

	public function setApFinancialStatusTypeId( $intApFinancialStatusTypeId ) {
		$this->set( 'm_intApFinancialStatusTypeId', CStrings::strToIntDef( $intApFinancialStatusTypeId, NULL, false ) );
	}

	public function getApFinancialStatusTypeId() {
		return $this->m_intApFinancialStatusTypeId;
	}

	public function sqlApFinancialStatusTypeId() {
		return ( true == isset( $this->m_intApFinancialStatusTypeId ) ) ? ( string ) $this->m_intApFinancialStatusTypeId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->set( 'm_intApRemittanceId', CStrings::strToIntDef( $intApRemittanceId, NULL, false ) );
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function sqlApRemittanceId() {
		return ( true == isset( $this->m_intApRemittanceId ) ) ? ( string ) $this->m_intApRemittanceId : 'NULL';
	}

	public function setApPayeeTermId( $intApPayeeTermId ) {
		$this->set( 'm_intApPayeeTermId', CStrings::strToIntDef( $intApPayeeTermId, NULL, false ) );
	}

	public function getApPayeeTermId() {
		return $this->m_intApPayeeTermId;
	}

	public function sqlApPayeeTermId() {
		return ( true == isset( $this->m_intApPayeeTermId ) ) ? ( string ) $this->m_intApPayeeTermId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setBulkJobPhaseId( $intBulkJobPhaseId ) {
		$this->set( 'm_intBulkJobPhaseId', CStrings::strToIntDef( $intBulkJobPhaseId, NULL, false ) );
	}

	public function getBulkJobPhaseId() {
		return $this->m_intBulkJobPhaseId;
	}

	public function sqlBulkJobPhaseId() {
		return ( true == isset( $this->m_intBulkJobPhaseId ) ) ? ( string ) $this->m_intBulkJobPhaseId : 'NULL';
	}

	public function setBulkApContractId( $intBulkApContractId ) {
		$this->set( 'm_intBulkApContractId', CStrings::strToIntDef( $intBulkApContractId, NULL, false ) );
	}

	public function getBulkApContractId() {
		return $this->m_intBulkApContractId;
	}

	public function sqlBulkApContractId() {
		return ( true == isset( $this->m_intBulkApContractId ) ) ? ( string ) $this->m_intBulkApContractId : 'NULL';
	}

	public function setBulkBankAccountId( $intBulkBankAccountId ) {
		$this->set( 'm_intBulkBankAccountId', CStrings::strToIntDef( $intBulkBankAccountId, NULL, false ) );
	}

	public function getBulkBankAccountId() {
		return $this->m_intBulkBankAccountId;
	}

	public function sqlBulkBankAccountId() {
		return ( true == isset( $this->m_intBulkBankAccountId ) ) ? ( string ) $this->m_intBulkBankAccountId : 'NULL';
	}

	public function setBulkPropertyId( $intBulkPropertyId ) {
		$this->set( 'm_intBulkPropertyId', CStrings::strToIntDef( $intBulkPropertyId, NULL, false ) );
	}

	public function getBulkPropertyId() {
		return $this->m_intBulkPropertyId;
	}

	public function sqlBulkPropertyId() {
		return ( true == isset( $this->m_intBulkPropertyId ) ) ? ( string ) $this->m_intBulkPropertyId : 'NULL';
	}

	public function setBulkApFormulaId( $intBulkApFormulaId ) {
		$this->set( 'm_intBulkApFormulaId', CStrings::strToIntDef( $intBulkApFormulaId, NULL, false ) );
	}

	public function getBulkApFormulaId() {
		return $this->m_intBulkApFormulaId;
	}

	public function sqlBulkApFormulaId() {
		return ( true == isset( $this->m_intBulkApFormulaId ) ) ? ( string ) $this->m_intBulkApFormulaId : 'NULL';
	}

	public function setBulkUnitNumberId( $intBulkUnitNumberId ) {
		$this->set( 'm_intBulkUnitNumberId', CStrings::strToIntDef( $intBulkUnitNumberId, NULL, false ) );
	}

	public function getBulkUnitNumberId() {
		return $this->m_intBulkUnitNumberId;
	}

	public function sqlBulkUnitNumberId() {
		return ( true == isset( $this->m_intBulkUnitNumberId ) ) ? ( string ) $this->m_intBulkUnitNumberId : 'NULL';
	}

	public function setBulkCompanyDepartmentId( $intBulkCompanyDepartmentId ) {
		$this->set( 'm_intBulkCompanyDepartmentId', CStrings::strToIntDef( $intBulkCompanyDepartmentId, NULL, false ) );
	}

	public function getBulkCompanyDepartmentId() {
		return $this->m_intBulkCompanyDepartmentId;
	}

	public function sqlBulkCompanyDepartmentId() {
		return ( true == isset( $this->m_intBulkCompanyDepartmentId ) ) ? ( string ) $this->m_intBulkCompanyDepartmentId : 'NULL';
	}

	public function setBulkGlDimensionId( $intBulkGlDimensionId ) {
		$this->set( 'm_intBulkGlDimensionId', CStrings::strToIntDef( $intBulkGlDimensionId, NULL, false ) );
	}

	public function getBulkGlDimensionId() {
		return $this->m_intBulkGlDimensionId;
	}

	public function sqlBulkGlDimensionId() {
		return ( true == isset( $this->m_intBulkGlDimensionId ) ) ? ( string ) $this->m_intBulkGlDimensionId : 'NULL';
	}

	public function setBulkPropertyBuildingId( $intBulkPropertyBuildingId ) {
		$this->set( 'm_intBulkPropertyBuildingId', CStrings::strToIntDef( $intBulkPropertyBuildingId, NULL, false ) );
	}

	public function getBulkPropertyBuildingId() {
		return $this->m_intBulkPropertyBuildingId;
	}

	public function sqlBulkPropertyBuildingId() {
		return ( true == isset( $this->m_intBulkPropertyBuildingId ) ) ? ( string ) $this->m_intBulkPropertyBuildingId : 'NULL';
	}

	public function setBulkBudgetApHeaderId( $intBulkBudgetApHeaderId ) {
		$this->set( 'm_intBulkBudgetApHeaderId', CStrings::strToIntDef( $intBulkBudgetApHeaderId, NULL, false ) );
	}

	public function getBulkBudgetApHeaderId() {
		return $this->m_intBulkBudgetApHeaderId;
	}

	public function sqlBulkBudgetApHeaderId() {
		return ( true == isset( $this->m_intBulkBudgetApHeaderId ) ) ? ( string ) $this->m_intBulkBudgetApHeaderId : 'NULL';
	}

	public function setApContractApHeaderIds( $arrintApContractApHeaderIds ) {
		$this->set( 'm_arrintApContractApHeaderIds', CStrings::strToArrIntDef( $arrintApContractApHeaderIds, NULL ) );
	}

	public function getApContractApHeaderIds() {
		return $this->m_arrintApContractApHeaderIds;
	}

	public function sqlApContractApHeaderIds() {
		return ( true == isset( $this->m_arrintApContractApHeaderIds ) && true == valArr( $this->m_arrintApContractApHeaderIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintApContractApHeaderIds, NULL ) . '\'' : 'NULL';
	}

	public function setPoApHeaderIds( $arrintPoApHeaderIds ) {
		$this->set( 'm_arrintPoApHeaderIds', CStrings::strToArrIntDef( $arrintPoApHeaderIds, NULL ) );
	}

	public function getPoApHeaderIds() {
		return $this->m_arrintPoApHeaderIds;
	}

	public function sqlPoApHeaderIds() {
		return ( true == isset( $this->m_arrintPoApHeaderIds ) && true == valArr( $this->m_arrintPoApHeaderIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPoApHeaderIds, NULL ) . '\'' : 'NULL';
	}

	public function setReversalApHeaderId( $intReversalApHeaderId ) {
		$this->set( 'm_intReversalApHeaderId', CStrings::strToIntDef( $intReversalApHeaderId, NULL, false ) );
	}

	public function getReversalApHeaderId() {
		return $this->m_intReversalApHeaderId;
	}

	public function sqlReversalApHeaderId() {
		return ( true == isset( $this->m_intReversalApHeaderId ) ) ? ( string ) $this->m_intReversalApHeaderId : 'NULL';
	}

	public function setPaymentApHeaderId( $intPaymentApHeaderId ) {
		$this->set( 'm_intPaymentApHeaderId', CStrings::strToIntDef( $intPaymentApHeaderId, NULL, false ) );
	}

	public function getPaymentApHeaderId() {
		return $this->m_intPaymentApHeaderId;
	}

	public function sqlPaymentApHeaderId() {
		return ( true == isset( $this->m_intPaymentApHeaderId ) ) ? ( string ) $this->m_intPaymentApHeaderId : 'NULL';
	}

	public function setScheduledApHeaderId( $intScheduledApHeaderId ) {
		$this->set( 'm_intScheduledApHeaderId', CStrings::strToIntDef( $intScheduledApHeaderId, NULL, false ) );
	}

	public function getScheduledApHeaderId() {
		return $this->m_intScheduledApHeaderId;
	}

	public function sqlScheduledApHeaderId() {
		return ( true == isset( $this->m_intScheduledApHeaderId ) ) ? ( string ) $this->m_intScheduledApHeaderId : 'NULL';
	}

	public function setTemplateApHeaderId( $intTemplateApHeaderId ) {
		$this->set( 'm_intTemplateApHeaderId', CStrings::strToIntDef( $intTemplateApHeaderId, NULL, false ) );
	}

	public function getTemplateApHeaderId() {
		return $this->m_intTemplateApHeaderId;
	}

	public function sqlTemplateApHeaderId() {
		return ( true == isset( $this->m_intTemplateApHeaderId ) ) ? ( string ) $this->m_intTemplateApHeaderId : 'NULL';
	}

	public function setReimbursementApHeaderId( $intReimbursementApHeaderId ) {
		$this->set( 'm_intReimbursementApHeaderId', CStrings::strToIntDef( $intReimbursementApHeaderId, NULL, false ) );
	}

	public function getReimbursementApHeaderId() {
		return $this->m_intReimbursementApHeaderId;
	}

	public function sqlReimbursementApHeaderId() {
		return ( true == isset( $this->m_intReimbursementApHeaderId ) ) ? ( string ) $this->m_intReimbursementApHeaderId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : '1';
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->set( 'm_intLeaseCustomerId', CStrings::strToIntDef( $intLeaseCustomerId, NULL, false ) );
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function sqlLeaseCustomerId() {
		return ( true == isset( $this->m_intLeaseCustomerId ) ) ? ( string ) $this->m_intLeaseCustomerId : 'NULL';
	}

	public function setRefundArTransactionId( $intRefundArTransactionId ) {
		$this->set( 'm_intRefundArTransactionId', CStrings::strToIntDef( $intRefundArTransactionId, NULL, false ) );
	}

	public function getRefundArTransactionId() {
		return $this->m_intRefundArTransactionId;
	}

	public function sqlRefundArTransactionId() {
		return ( true == isset( $this->m_intRefundArTransactionId ) ) ? ( string ) $this->m_intRefundArTransactionId : 'NULL';
	}

	public function setApBatchId( $intApBatchId ) {
		$this->set( 'm_intApBatchId', CStrings::strToIntDef( $intApBatchId, NULL, false ) );
	}

	public function getApBatchId() {
		return $this->m_intApBatchId;
	}

	public function sqlApBatchId() {
		return ( true == isset( $this->m_intApBatchId ) ) ? ( string ) $this->m_intApBatchId : 'NULL';
	}

	public function setAccountingExportBatchId( $intAccountingExportBatchId ) {
		$this->set( 'm_intAccountingExportBatchId', CStrings::strToIntDef( $intAccountingExportBatchId, NULL, false ) );
	}

	public function getAccountingExportBatchId() {
		return $this->m_intAccountingExportBatchId;
	}

	public function sqlAccountingExportBatchId() {
		return ( true == isset( $this->m_intAccountingExportBatchId ) ) ? ( string ) $this->m_intAccountingExportBatchId : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setComplianceJobId( $intComplianceJobId ) {
		$this->set( 'm_intComplianceJobId', CStrings::strToIntDef( $intComplianceJobId, NULL, false ) );
	}

	public function getComplianceJobId() {
		return $this->m_intComplianceJobId;
	}

	public function sqlComplianceJobId() {
		return ( true == isset( $this->m_intComplianceJobId ) ) ? ( string ) $this->m_intComplianceJobId : 'NULL';
	}

	public function setApRoutingTagId( $intApRoutingTagId ) {
		$this->set( 'm_intApRoutingTagId', CStrings::strToIntDef( $intApRoutingTagId, NULL, false ) );
	}

	public function getApRoutingTagId() {
		return $this->m_intApRoutingTagId;
	}

	public function sqlApRoutingTagId() {
		return ( true == isset( $this->m_intApRoutingTagId ) ) ? ( string ) $this->m_intApRoutingTagId : 'NULL';
	}

	public function setBudgetChangeOrderId( $intBudgetChangeOrderId ) {
		$this->set( 'm_intBudgetChangeOrderId', CStrings::strToIntDef( $intBudgetChangeOrderId, NULL, false ) );
	}

	public function getBudgetChangeOrderId() {
		return $this->m_intBudgetChangeOrderId;
	}

	public function sqlBudgetChangeOrderId() {
		return ( true == isset( $this->m_intBudgetChangeOrderId ) ) ? ( string ) $this->m_intBudgetChangeOrderId : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 50, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAction ) : '\'' . addslashes( $this->m_strAction ) . '\'' ) : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setTemplateName( $strTemplateName ) {
		$this->set( 'm_strTemplateName', CStrings::strTrimDef( $strTemplateName, 50, NULL, true ) );
	}

	public function getTemplateName() {
		return $this->m_strTemplateName;
	}

	public function sqlTemplateName() {
		return ( true == isset( $this->m_strTemplateName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateName ) : '\'' . addslashes( $this->m_strTemplateName ) . '\'' ) : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setScheduledPaymentDate( $strScheduledPaymentDate ) {
		$this->set( 'm_strScheduledPaymentDate', CStrings::strTrimDef( $strScheduledPaymentDate, -1, NULL, true ) );
	}

	public function getScheduledPaymentDate() {
		return $this->m_strScheduledPaymentDate;
	}

	public function sqlScheduledPaymentDate() {
		return ( true == isset( $this->m_strScheduledPaymentDate ) ) ? '\'' . $this->m_strScheduledPaymentDate . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->set( 'm_intFrequencyInterval', CStrings::strToIntDef( $intFrequencyInterval, NULL, false ) );
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function sqlFrequencyInterval() {
		return ( true == isset( $this->m_intFrequencyInterval ) ) ? ( string ) $this->m_intFrequencyInterval : '1';
	}

	public function setDayOfWeek( $intDayOfWeek ) {
		$this->set( 'm_intDayOfWeek', CStrings::strToIntDef( $intDayOfWeek, NULL, false ) );
	}

	public function getDayOfWeek() {
		return $this->m_intDayOfWeek;
	}

	public function sqlDayOfWeek() {
		return ( true == isset( $this->m_intDayOfWeek ) ) ? ( string ) $this->m_intDayOfWeek : 'NULL';
	}

	public function setDaysOfMonth( $arrintDaysOfMonth ) {
		$this->set( 'm_arrintDaysOfMonth', CStrings::strToArrIntDef( $arrintDaysOfMonth, NULL ) );
	}

	public function getDaysOfMonth() {
		return $this->m_arrintDaysOfMonth;
	}

	public function sqlDaysOfMonth() {
		return ( true == isset( $this->m_arrintDaysOfMonth ) && true == valArr( $this->m_arrintDaysOfMonth ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDaysOfMonth, NULL ) . '\'' : 'NULL';
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->set( 'm_intNumberOfOccurrences', CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false ) );
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function sqlNumberOfOccurrences() {
		return ( true == isset( $this->m_intNumberOfOccurrences ) ) ? ( string ) $this->m_intNumberOfOccurrences : 'NULL';
	}

	public function setLastPostedDate( $strLastPostedDate ) {
		$this->set( 'm_strLastPostedDate', CStrings::strTrimDef( $strLastPostedDate, -1, NULL, true ) );
	}

	public function getLastPostedDate() {
		return $this->m_strLastPostedDate;
	}

	public function sqlLastPostedDate() {
		return ( true == isset( $this->m_strLastPostedDate ) ) ? '\'' . $this->m_strLastPostedDate . '\'' : 'NULL';
	}

	public function setNextPostDate( $strNextPostDate ) {
		$this->set( 'm_strNextPostDate', CStrings::strTrimDef( $strNextPostDate, -1, NULL, true ) );
	}

	public function getNextPostDate() {
		return $this->m_strNextPostDate;
	}

	public function sqlNextPostDate() {
		return ( true == isset( $this->m_strNextPostDate ) ) ? '\'' . $this->m_strNextPostDate . '\'' : 'NULL';
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->set( 'm_strHeaderNumber', CStrings::strTrimDef( $strHeaderNumber, 2000, NULL, true ) );
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function sqlHeaderNumber() {
		return ( true == isset( $this->m_strHeaderNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHeaderNumber ) : '\'' . addslashes( $this->m_strHeaderNumber ) . '\'' ) : 'NULL';
	}

	public function setExternalUrl( $strExternalUrl ) {
		$this->set( 'm_strExternalUrl', CStrings::strTrimDef( $strExternalUrl, -1, NULL, true ) );
	}

	public function getExternalUrl() {
		return $this->m_strExternalUrl;
	}

	public function sqlExternalUrl() {
		return ( true == isset( $this->m_strExternalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalUrl ) : '\'' . addslashes( $this->m_strExternalUrl ) . '\'' ) : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumber ) : '\'' . addslashes( $this->m_strAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setHeaderMemo( $strHeaderMemo ) {
		$this->set( 'm_strHeaderMemo', CStrings::strTrimDef( $strHeaderMemo, 2000, NULL, true ) );
	}

	public function getHeaderMemo() {
		return $this->m_strHeaderMemo;
	}

	public function sqlHeaderMemo() {
		return ( true == isset( $this->m_strHeaderMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHeaderMemo ) : '\'' . addslashes( $this->m_strHeaderMemo ) . '\'' ) : 'NULL';
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->set( 'm_strApprovalNote', CStrings::strTrimDef( $strApprovalNote, 500, NULL, true ) );
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	public function sqlApprovalNote() {
		return ( true == isset( $this->m_strApprovalNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalNote ) : '\'' . addslashes( $this->m_strApprovalNote ) . '\'' ) : 'NULL';
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->set( 'm_fltControlTotal', CStrings::strToFloatDef( $fltControlTotal, NULL, false, 2 ) );
	}

	public function getControlTotal() {
		return $this->m_fltControlTotal;
	}

	public function sqlControlTotal() {
		return ( true == isset( $this->m_fltControlTotal ) ) ? ( string ) $this->m_fltControlTotal : '0';
	}

	public function setPreApprovalAmount( $fltPreApprovalAmount ) {
		$this->set( 'm_fltPreApprovalAmount', CStrings::strToFloatDef( $fltPreApprovalAmount, NULL, false, 2 ) );
	}

	public function getPreApprovalAmount() {
		return $this->m_fltPreApprovalAmount;
	}

	public function sqlPreApprovalAmount() {
		return ( true == isset( $this->m_fltPreApprovalAmount ) ) ? ( string ) $this->m_fltPreApprovalAmount : '0';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : '0';
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->set( 'm_fltTransactionAmountDue', CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 2 ) );
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function sqlTransactionAmountDue() {
		return ( true == isset( $this->m_fltTransactionAmountDue ) ) ? ( string ) $this->m_fltTransactionAmountDue : '0';
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 ) );
	}

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function sqlTaxAmount() {
		return ( true == isset( $this->m_fltTaxAmount ) ) ? ( string ) $this->m_fltTaxAmount : '( 0 )::numeric';
	}

	public function setDiscountAmount( $fltDiscountAmount ) {
		$this->set( 'm_fltDiscountAmount', CStrings::strToFloatDef( $fltDiscountAmount, NULL, false, 2 ) );
	}

	public function getDiscountAmount() {
		return $this->m_fltDiscountAmount;
	}

	public function sqlDiscountAmount() {
		return ( true == isset( $this->m_fltDiscountAmount ) ) ? ( string ) $this->m_fltDiscountAmount : '( 0 )::numeric';
	}

	public function setShippingAmount( $fltShippingAmount ) {
		$this->set( 'm_fltShippingAmount', CStrings::strToFloatDef( $fltShippingAmount, NULL, false, 2 ) );
	}

	public function getShippingAmount() {
		return $this->m_fltShippingAmount;
	}

	public function sqlShippingAmount() {
		return ( true == isset( $this->m_fltShippingAmount ) ) ? ( string ) $this->m_fltShippingAmount : '( 0 )::numeric';
	}

	public function setBulkIsConfidential( $boolBulkIsConfidential ) {
		$this->set( 'm_boolBulkIsConfidential', CStrings::strToBool( $boolBulkIsConfidential ) );
	}

	public function getBulkIsConfidential() {
		return $this->m_boolBulkIsConfidential;
	}

	public function sqlBulkIsConfidential() {
		return ( true == isset( $this->m_boolBulkIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolBulkIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBulkIs1099( $boolBulkIs1099 ) {
		$this->set( 'm_boolBulkIs1099', CStrings::strToBool( $boolBulkIs1099 ) );
	}

	public function getBulkIs1099() {
		return $this->m_boolBulkIs1099;
	}

	public function sqlBulkIs1099() {
		return ( true == isset( $this->m_boolBulkIs1099 ) ) ? '\'' . ( true == ( bool ) $this->m_boolBulkIs1099 ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoCreatePo( $boolAutoCreatePo ) {
		$this->set( 'm_boolAutoCreatePo', CStrings::strToBool( $boolAutoCreatePo ) );
	}

	public function getAutoCreatePo() {
		return $this->m_boolAutoCreatePo;
	}

	public function sqlAutoCreatePo() {
		return ( true == isset( $this->m_boolAutoCreatePo ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoCreatePo ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoApprovePo( $boolAutoApprovePo ) {
		$this->set( 'm_boolAutoApprovePo', CStrings::strToBool( $boolAutoApprovePo ) );
	}

	public function getAutoApprovePo() {
		return $this->m_boolAutoApprovePo;
	}

	public function sqlAutoApprovePo() {
		return ( true == isset( $this->m_boolAutoApprovePo ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoApprovePo ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoCreateInvoice( $boolAutoCreateInvoice ) {
		$this->set( 'm_boolAutoCreateInvoice', CStrings::strToBool( $boolAutoCreateInvoice ) );
	}

	public function getAutoCreateInvoice() {
		return $this->m_boolAutoCreateInvoice;
	}

	public function sqlAutoCreateInvoice() {
		return ( true == isset( $this->m_boolAutoCreateInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoCreateInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoApproveInvoice( $boolAutoApproveInvoice ) {
		$this->set( 'm_boolAutoApproveInvoice', CStrings::strToBool( $boolAutoApproveInvoice ) );
	}

	public function getAutoApproveInvoice() {
		return $this->m_boolAutoApproveInvoice;
	}

	public function sqlAutoApproveInvoice() {
		return ( true == isset( $this->m_boolAutoApproveInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoApproveInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoPostInvoice( $boolAutoPostInvoice ) {
		$this->set( 'm_boolAutoPostInvoice', CStrings::strToBool( $boolAutoPostInvoice ) );
	}

	public function getAutoPostInvoice() {
		return $this->m_boolAutoPostInvoice;
	}

	public function sqlAutoPostInvoice() {
		return ( true == isset( $this->m_boolAutoPostInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoPostInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPayWithSingleCheck( $boolPayWithSingleCheck ) {
		$this->set( 'm_boolPayWithSingleCheck', CStrings::strToBool( $boolPayWithSingleCheck ) );
	}

	public function getPayWithSingleCheck() {
		return $this->m_boolPayWithSingleCheck;
	}

	public function sqlPayWithSingleCheck() {
		return ( true == isset( $this->m_boolPayWithSingleCheck ) ) ? '\'' . ( true == ( bool ) $this->m_boolPayWithSingleCheck ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReversed( $boolIsReversed ) {
		$this->set( 'm_boolIsReversed', CStrings::strToBool( $boolIsReversed ) );
	}

	public function getIsReversed() {
		return $this->m_boolIsReversed;
	}

	public function sqlIsReversed() {
		return ( true == isset( $this->m_boolIsReversed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReversed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->set( 'm_boolIsPrimary', CStrings::strToBool( $boolIsPrimary ) );
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_boolIsPrimary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrimary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOnHold( $boolIsOnHold ) {
		$this->set( 'm_boolIsOnHold', CStrings::strToBool( $boolIsOnHold ) );
	}

	public function getIsOnHold() {
		return $this->m_boolIsOnHold;
	}

	public function sqlIsOnHold() {
		return ( true == isset( $this->m_boolIsOnHold ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnHold ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->set( 'm_boolIsTemplate', CStrings::strToBool( $boolIsTemplate ) );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_boolIsTemplate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReimbursement( $boolIsReimbursement ) {
		$this->set( 'm_boolIsReimbursement', CStrings::strToBool( $boolIsReimbursement ) );
	}

	public function getIsReimbursement() {
		return $this->m_boolIsReimbursement;
	}

	public function sqlIsReimbursement() {
		return ( true == isset( $this->m_boolIsReimbursement ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReimbursement ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBatching( $boolIsBatching ) {
		$this->set( 'm_boolIsBatching', CStrings::strToBool( $boolIsBatching ) );
	}

	public function getIsBatching() {
		return $this->m_boolIsBatching;
	}

	public function sqlIsBatching() {
		return ( true == isset( $this->m_boolIsBatching ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBatching ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemporary( $boolIsTemporary ) {
		$this->set( 'm_boolIsTemporary', CStrings::strToBool( $boolIsTemporary ) );
	}

	public function getIsTemporary() {
		return $this->m_boolIsTemporary;
	}

	public function sqlIsTemporary() {
		return ( true == isset( $this->m_boolIsTemporary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemporary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRetentionAmountReleased( $fltRetentionAmountReleased ) {
		$this->set( 'm_fltRetentionAmountReleased', CStrings::strToFloatDef( $fltRetentionAmountReleased, NULL, false, 2 ) );
	}

	public function getRetentionAmountReleased() {
		return $this->m_fltRetentionAmountReleased;
	}

	public function sqlRetentionAmountReleased() {
		return ( true == isset( $this->m_fltRetentionAmountReleased ) ) ? ( string ) $this->m_fltRetentionAmountReleased : '( 0 )::numeric';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_header_id, ap_header_type_id, ap_header_sub_type_id, ap_header_mode_id, ap_physical_status_type_id, ap_financial_status_type_id, ap_payee_id, ap_payee_location_id, ap_payee_account_id, ap_remittance_id, ap_payee_term_id, gl_transaction_type_id, bulk_job_phase_id, bulk_ap_contract_id, bulk_bank_account_id, bulk_property_id, bulk_ap_formula_id, bulk_unit_number_id, bulk_company_department_id, bulk_gl_dimension_id, bulk_property_building_id, bulk_budget_ap_header_id, ap_contract_ap_header_ids, po_ap_header_ids, reversal_ap_header_id, payment_ap_header_id, scheduled_ap_header_id, template_ap_header_id, reimbursement_ap_header_id, frequency_id, lease_customer_id, refund_ar_transaction_id, ap_batch_id, accounting_export_batch_id, ap_payment_id, compliance_job_id, ap_routing_tag_id, budget_change_order_id, action, remote_primary_key, template_name, transaction_datetime, log_datetime, post_date, due_date, post_month, scheduled_payment_date, start_date, end_date, frequency_interval, day_of_week, days_of_month, number_of_occurrences, last_posted_date, next_post_date, header_number, external_url, account_number, header_memo, approval_note, control_total, pre_approval_amount, transaction_amount, transaction_amount_due, tax_amount, discount_amount, shipping_amount, bulk_is_confidential, bulk_is_1099, auto_create_po, auto_approve_po, auto_create_invoice, auto_approve_invoice, auto_post_invoice, pay_with_single_check, is_deleted, is_reversed, is_primary, is_on_hold, is_template, is_disabled, is_reimbursement, is_batching, is_temporary, is_posted, is_initial_import, posted_on, imported_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, retention_amount_released )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApHeaderId() . ', ' .
						$this->sqlApHeaderTypeId() . ', ' .
						$this->sqlApHeaderSubTypeId() . ', ' .
						$this->sqlApHeaderModeId() . ', ' .
						$this->sqlApPhysicalStatusTypeId() . ', ' .
						$this->sqlApFinancialStatusTypeId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlApRemittanceId() . ', ' .
						$this->sqlApPayeeTermId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlBulkJobPhaseId() . ', ' .
						$this->sqlBulkApContractId() . ', ' .
						$this->sqlBulkBankAccountId() . ', ' .
						$this->sqlBulkPropertyId() . ', ' .
						$this->sqlBulkApFormulaId() . ', ' .
						$this->sqlBulkUnitNumberId() . ', ' .
						$this->sqlBulkCompanyDepartmentId() . ', ' .
						$this->sqlBulkGlDimensionId() . ', ' .
						$this->sqlBulkPropertyBuildingId() . ', ' .
						$this->sqlBulkBudgetApHeaderId() . ', ' .
						$this->sqlApContractApHeaderIds() . ', ' .
						$this->sqlPoApHeaderIds() . ', ' .
						$this->sqlReversalApHeaderId() . ', ' .
						$this->sqlPaymentApHeaderId() . ', ' .
						$this->sqlScheduledApHeaderId() . ', ' .
						$this->sqlTemplateApHeaderId() . ', ' .
						$this->sqlReimbursementApHeaderId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlLeaseCustomerId() . ', ' .
						$this->sqlRefundArTransactionId() . ', ' .
						$this->sqlApBatchId() . ', ' .
						$this->sqlAccountingExportBatchId() . ', ' .
						$this->sqlApPaymentId() . ', ' .
						$this->sqlComplianceJobId() . ', ' .
						$this->sqlApRoutingTagId() . ', ' .
						$this->sqlBudgetChangeOrderId() . ', ' .
						$this->sqlAction() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTemplateName() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlScheduledPaymentDate() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlFrequencyInterval() . ', ' .
						$this->sqlDayOfWeek() . ', ' .
						$this->sqlDaysOfMonth() . ', ' .
						$this->sqlNumberOfOccurrences() . ', ' .
						$this->sqlLastPostedDate() . ', ' .
						$this->sqlNextPostDate() . ', ' .
						$this->sqlHeaderNumber() . ', ' .
						$this->sqlExternalUrl() . ', ' .
						$this->sqlAccountNumber() . ', ' .
						$this->sqlHeaderMemo() . ', ' .
						$this->sqlApprovalNote() . ', ' .
						$this->sqlControlTotal() . ', ' .
						$this->sqlPreApprovalAmount() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlTransactionAmountDue() . ', ' .
						$this->sqlTaxAmount() . ', ' .
						$this->sqlDiscountAmount() . ', ' .
						$this->sqlShippingAmount() . ', ' .
						$this->sqlBulkIsConfidential() . ', ' .
						$this->sqlBulkIs1099() . ', ' .
						$this->sqlAutoCreatePo() . ', ' .
						$this->sqlAutoApprovePo() . ', ' .
						$this->sqlAutoCreateInvoice() . ', ' .
						$this->sqlAutoApproveInvoice() . ', ' .
						$this->sqlAutoPostInvoice() . ', ' .
						$this->sqlPayWithSingleCheck() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						$this->sqlIsReversed() . ', ' .
						$this->sqlIsPrimary() . ', ' .
						$this->sqlIsOnHold() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsReimbursement() . ', ' .
						$this->sqlIsBatching() . ', ' .
						$this->sqlIsTemporary() . ', ' .
						$this->sqlIsPosted() . ', ' .
						$this->sqlIsInitialImport() . ', ' .
						$this->sqlPostedOn() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRetentionAmountReleased() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_type_id = ' . $this->sqlApHeaderTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_type_id = ' . $this->sqlApHeaderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_mode_id = ' . $this->sqlApHeaderModeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderModeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_mode_id = ' . $this->sqlApHeaderModeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPhysicalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_financial_status_type_id = ' . $this->sqlApFinancialStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApFinancialStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_financial_status_type_id = ' . $this->sqlApFinancialStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId(). ',' ; } elseif( true == array_key_exists( 'ApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeTermId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_job_phase_id = ' . $this->sqlBulkJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'BulkJobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_job_phase_id = ' . $this->sqlBulkJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_ap_contract_id = ' . $this->sqlBulkApContractId(). ',' ; } elseif( true == array_key_exists( 'BulkApContractId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_ap_contract_id = ' . $this->sqlBulkApContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_bank_account_id = ' . $this->sqlBulkBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BulkBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_bank_account_id = ' . $this->sqlBulkBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_property_id = ' . $this->sqlBulkPropertyId(). ',' ; } elseif( true == array_key_exists( 'BulkPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_property_id = ' . $this->sqlBulkPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_ap_formula_id = ' . $this->sqlBulkApFormulaId(). ',' ; } elseif( true == array_key_exists( 'BulkApFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_ap_formula_id = ' . $this->sqlBulkApFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_unit_number_id = ' . $this->sqlBulkUnitNumberId(). ',' ; } elseif( true == array_key_exists( 'BulkUnitNumberId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_unit_number_id = ' . $this->sqlBulkUnitNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_company_department_id = ' . $this->sqlBulkCompanyDepartmentId(). ',' ; } elseif( true == array_key_exists( 'BulkCompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_company_department_id = ' . $this->sqlBulkCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_gl_dimension_id = ' . $this->sqlBulkGlDimensionId(). ',' ; } elseif( true == array_key_exists( 'BulkGlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_gl_dimension_id = ' . $this->sqlBulkGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_property_building_id = ' . $this->sqlBulkPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'BulkPropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_property_building_id = ' . $this->sqlBulkPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_budget_ap_header_id = ' . $this->sqlBulkBudgetApHeaderId(). ',' ; } elseif( true == array_key_exists( 'BulkBudgetApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_budget_ap_header_id = ' . $this->sqlBulkBudgetApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_ap_header_ids = ' . $this->sqlApContractApHeaderIds(). ',' ; } elseif( true == array_key_exists( 'ApContractApHeaderIds', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_ap_header_ids = ' . $this->sqlApContractApHeaderIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_ap_header_ids = ' . $this->sqlPoApHeaderIds(). ',' ; } elseif( true == array_key_exists( 'PoApHeaderIds', $this->getChangedColumns() ) ) { $strSql .= ' po_ap_header_ids = ' . $this->sqlPoApHeaderIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversal_ap_header_id = ' . $this->sqlReversalApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ReversalApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' reversal_ap_header_id = ' . $this->sqlReversalApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId(). ',' ; } elseif( true == array_key_exists( 'PaymentApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_ap_header_id = ' . $this->sqlScheduledApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ScheduledApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_ap_header_id = ' . $this->sqlScheduledApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_ap_header_id = ' . $this->sqlTemplateApHeaderId(). ',' ; } elseif( true == array_key_exists( 'TemplateApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' template_ap_header_id = ' . $this->sqlTemplateApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_ap_header_id = ' . $this->sqlReimbursementApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ReimbursementApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_ap_header_id = ' . $this->sqlReimbursementApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId(). ',' ; } elseif( true == array_key_exists( 'LeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_ar_transaction_id = ' . $this->sqlRefundArTransactionId(). ',' ; } elseif( true == array_key_exists( 'RefundArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' refund_ar_transaction_id = ' . $this->sqlRefundArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_batch_id = ' . $this->sqlApBatchId(). ',' ; } elseif( true == array_key_exists( 'ApBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ap_batch_id = ' . $this->sqlApBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId(). ',' ; } elseif( true == array_key_exists( 'AccountingExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId(). ',' ; } elseif( true == array_key_exists( 'ComplianceJobId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId(). ',' ; } elseif( true == array_key_exists( 'ApRoutingTagId', $this->getChangedColumns() ) ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_change_order_id = ' . $this->sqlBudgetChangeOrderId(). ',' ; } elseif( true == array_key_exists( 'BudgetChangeOrderId', $this->getChangedColumns() ) ) { $strSql .= ' budget_change_order_id = ' . $this->sqlBudgetChangeOrderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction(). ',' ; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_name = ' . $this->sqlTemplateName(). ',' ; } elseif( true == array_key_exists( 'TemplateName', $this->getChangedColumns() ) ) { $strSql .= ' template_name = ' . $this->sqlTemplateName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_payment_date = ' . $this->sqlScheduledPaymentDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledPaymentDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_payment_date = ' . $this->sqlScheduledPaymentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval(). ',' ; } elseif( true == array_key_exists( 'FrequencyInterval', $this->getChangedColumns() ) ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek(). ',' ; } elseif( true == array_key_exists( 'DayOfWeek', $this->getChangedColumns() ) ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_of_month = ' . $this->sqlDaysOfMonth(). ',' ; } elseif( true == array_key_exists( 'DaysOfMonth', $this->getChangedColumns() ) ) { $strSql .= ' days_of_month = ' . $this->sqlDaysOfMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences(). ',' ; } elseif( true == array_key_exists( 'NumberOfOccurrences', $this->getChangedColumns() ) ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate(). ',' ; } elseif( true == array_key_exists( 'LastPostedDate', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate(). ',' ; } elseif( true == array_key_exists( 'NextPostDate', $this->getChangedColumns() ) ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber(). ',' ; } elseif( true == array_key_exists( 'HeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl(). ',' ; } elseif( true == array_key_exists( 'ExternalUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber(). ',' ; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_memo = ' . $this->sqlHeaderMemo(). ',' ; } elseif( true == array_key_exists( 'HeaderMemo', $this->getChangedColumns() ) ) { $strSql .= ' header_memo = ' . $this->sqlHeaderMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_note = ' . $this->sqlApprovalNote(). ',' ; } elseif( true == array_key_exists( 'ApprovalNote', $this->getChangedColumns() ) ) { $strSql .= ' approval_note = ' . $this->sqlApprovalNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total = ' . $this->sqlControlTotal(). ',' ; } elseif( true == array_key_exists( 'ControlTotal', $this->getChangedColumns() ) ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount(). ',' ; } elseif( true == array_key_exists( 'PreApprovalAmount', $this->getChangedColumns() ) ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue(). ',' ; } elseif( true == array_key_exists( 'TransactionAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount(). ',' ; } elseif( true == array_key_exists( 'TaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'DiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount(). ',' ; } elseif( true == array_key_exists( 'ShippingAmount', $this->getChangedColumns() ) ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_is_confidential = ' . $this->sqlBulkIsConfidential(). ',' ; } elseif( true == array_key_exists( 'BulkIsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' bulk_is_confidential = ' . $this->sqlBulkIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_is_1099 = ' . $this->sqlBulkIs1099(). ',' ; } elseif( true == array_key_exists( 'BulkIs1099', $this->getChangedColumns() ) ) { $strSql .= ' bulk_is_1099 = ' . $this->sqlBulkIs1099() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_create_po = ' . $this->sqlAutoCreatePo(). ',' ; } elseif( true == array_key_exists( 'AutoCreatePo', $this->getChangedColumns() ) ) { $strSql .= ' auto_create_po = ' . $this->sqlAutoCreatePo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_approve_po = ' . $this->sqlAutoApprovePo(). ',' ; } elseif( true == array_key_exists( 'AutoApprovePo', $this->getChangedColumns() ) ) { $strSql .= ' auto_approve_po = ' . $this->sqlAutoApprovePo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_create_invoice = ' . $this->sqlAutoCreateInvoice(). ',' ; } elseif( true == array_key_exists( 'AutoCreateInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_create_invoice = ' . $this->sqlAutoCreateInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_approve_invoice = ' . $this->sqlAutoApproveInvoice(). ',' ; } elseif( true == array_key_exists( 'AutoApproveInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_approve_invoice = ' . $this->sqlAutoApproveInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_invoice = ' . $this->sqlAutoPostInvoice(). ',' ; } elseif( true == array_key_exists( 'AutoPostInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_invoice = ' . $this->sqlAutoPostInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pay_with_single_check = ' . $this->sqlPayWithSingleCheck(). ',' ; } elseif( true == array_key_exists( 'PayWithSingleCheck', $this->getChangedColumns() ) ) { $strSql .= ' pay_with_single_check = ' . $this->sqlPayWithSingleCheck() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed(). ',' ; } elseif( true == array_key_exists( 'IsReversed', $this->getChangedColumns() ) ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary(). ',' ; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_hold = ' . $this->sqlIsOnHold(). ',' ; } elseif( true == array_key_exists( 'IsOnHold', $this->getChangedColumns() ) ) { $strSql .= ' is_on_hold = ' . $this->sqlIsOnHold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reimbursement = ' . $this->sqlIsReimbursement(). ',' ; } elseif( true == array_key_exists( 'IsReimbursement', $this->getChangedColumns() ) ) { $strSql .= ' is_reimbursement = ' . $this->sqlIsReimbursement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_batching = ' . $this->sqlIsBatching(). ',' ; } elseif( true == array_key_exists( 'IsBatching', $this->getChangedColumns() ) ) { $strSql .= ' is_batching = ' . $this->sqlIsBatching() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_temporary = ' . $this->sqlIsTemporary(). ',' ; } elseif( true == array_key_exists( 'IsTemporary', $this->getChangedColumns() ) ) { $strSql .= ' is_temporary = ' . $this->sqlIsTemporary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted(). ',' ; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport(). ',' ; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn(). ',' ; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retention_amount_released = ' . $this->sqlRetentionAmountReleased(). ',' ; } elseif( true == array_key_exists( 'RetentionAmountReleased', $this->getChangedColumns() ) ) { $strSql .= ' retention_amount_released = ' . $this->sqlRetentionAmountReleased() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_header_id' => $this->getApHeaderId(),
			'ap_header_type_id' => $this->getApHeaderTypeId(),
			'ap_header_sub_type_id' => $this->getApHeaderSubTypeId(),
			'ap_header_mode_id' => $this->getApHeaderModeId(),
			'ap_physical_status_type_id' => $this->getApPhysicalStatusTypeId(),
			'ap_financial_status_type_id' => $this->getApFinancialStatusTypeId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'ap_remittance_id' => $this->getApRemittanceId(),
			'ap_payee_term_id' => $this->getApPayeeTermId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'bulk_job_phase_id' => $this->getBulkJobPhaseId(),
			'bulk_ap_contract_id' => $this->getBulkApContractId(),
			'bulk_bank_account_id' => $this->getBulkBankAccountId(),
			'bulk_property_id' => $this->getBulkPropertyId(),
			'bulk_ap_formula_id' => $this->getBulkApFormulaId(),
			'bulk_unit_number_id' => $this->getBulkUnitNumberId(),
			'bulk_company_department_id' => $this->getBulkCompanyDepartmentId(),
			'bulk_gl_dimension_id' => $this->getBulkGlDimensionId(),
			'bulk_property_building_id' => $this->getBulkPropertyBuildingId(),
			'bulk_budget_ap_header_id' => $this->getBulkBudgetApHeaderId(),
			'ap_contract_ap_header_ids' => $this->getApContractApHeaderIds(),
			'po_ap_header_ids' => $this->getPoApHeaderIds(),
			'reversal_ap_header_id' => $this->getReversalApHeaderId(),
			'payment_ap_header_id' => $this->getPaymentApHeaderId(),
			'scheduled_ap_header_id' => $this->getScheduledApHeaderId(),
			'template_ap_header_id' => $this->getTemplateApHeaderId(),
			'reimbursement_ap_header_id' => $this->getReimbursementApHeaderId(),
			'frequency_id' => $this->getFrequencyId(),
			'lease_customer_id' => $this->getLeaseCustomerId(),
			'refund_ar_transaction_id' => $this->getRefundArTransactionId(),
			'ap_batch_id' => $this->getApBatchId(),
			'accounting_export_batch_id' => $this->getAccountingExportBatchId(),
			'ap_payment_id' => $this->getApPaymentId(),
			'compliance_job_id' => $this->getComplianceJobId(),
			'ap_routing_tag_id' => $this->getApRoutingTagId(),
			'budget_change_order_id' => $this->getBudgetChangeOrderId(),
			'action' => $this->getAction(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'template_name' => $this->getTemplateName(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'log_datetime' => $this->getLogDatetime(),
			'post_date' => $this->getPostDate(),
			'due_date' => $this->getDueDate(),
			'post_month' => $this->getPostMonth(),
			'scheduled_payment_date' => $this->getScheduledPaymentDate(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'frequency_interval' => $this->getFrequencyInterval(),
			'day_of_week' => $this->getDayOfWeek(),
			'days_of_month' => $this->getDaysOfMonth(),
			'number_of_occurrences' => $this->getNumberOfOccurrences(),
			'last_posted_date' => $this->getLastPostedDate(),
			'next_post_date' => $this->getNextPostDate(),
			'header_number' => $this->getHeaderNumber(),
			'external_url' => $this->getExternalUrl(),
			'account_number' => $this->getAccountNumber(),
			'header_memo' => $this->getHeaderMemo(),
			'approval_note' => $this->getApprovalNote(),
			'control_total' => $this->getControlTotal(),
			'pre_approval_amount' => $this->getPreApprovalAmount(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_amount_due' => $this->getTransactionAmountDue(),
			'tax_amount' => $this->getTaxAmount(),
			'discount_amount' => $this->getDiscountAmount(),
			'shipping_amount' => $this->getShippingAmount(),
			'bulk_is_confidential' => $this->getBulkIsConfidential(),
			'bulk_is_1099' => $this->getBulkIs1099(),
			'auto_create_po' => $this->getAutoCreatePo(),
			'auto_approve_po' => $this->getAutoApprovePo(),
			'auto_create_invoice' => $this->getAutoCreateInvoice(),
			'auto_approve_invoice' => $this->getAutoApproveInvoice(),
			'auto_post_invoice' => $this->getAutoPostInvoice(),
			'pay_with_single_check' => $this->getPayWithSingleCheck(),
			'is_deleted' => $this->getIsDeleted(),
			'is_reversed' => $this->getIsReversed(),
			'is_primary' => $this->getIsPrimary(),
			'is_on_hold' => $this->getIsOnHold(),
			'is_template' => $this->getIsTemplate(),
			'is_disabled' => $this->getIsDisabled(),
			'is_reimbursement' => $this->getIsReimbursement(),
			'is_batching' => $this->getIsBatching(),
			'is_temporary' => $this->getIsTemporary(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'posted_on' => $this->getPostedOn(),
			'imported_on' => $this->getImportedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'retention_amount_released' => $this->getRetentionAmountReleased()
		);
	}

}
?>