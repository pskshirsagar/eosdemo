<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationTemplateNotices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationTemplateNotices extends CEosPluralBase {

	/**
	 * @return CViolationTemplateNotice[]
	 */
	public static function fetchViolationTemplateNotices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolationTemplateNotice::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolationTemplateNotice
	 */
	public static function fetchViolationTemplateNotice( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationTemplateNotice::class, $objDatabase );
	}

	public static function fetchViolationTemplateNoticeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_template_notices', $objDatabase );
	}

	public static function fetchViolationTemplateNoticeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotice( sprintf( 'SELECT * FROM violation_template_notices WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplateNoticesByCid( $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotices( sprintf( 'SELECT * FROM violation_template_notices WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplateNoticesByViolationTemplateIdByCid( $intViolationTemplateId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotices( sprintf( 'SELECT * FROM violation_template_notices WHERE violation_template_id = %d AND cid = %d', ( int ) $intViolationTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplateNoticesByViolationWarningTypeIdByCid( $intViolationWarningTypeId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotices( sprintf( 'SELECT * FROM violation_template_notices WHERE violation_warning_type_id = %d AND cid = %d', ( int ) $intViolationWarningTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplateNoticesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotices( sprintf( 'SELECT * FROM violation_template_notices WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplateNoticesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplateNotices( sprintf( 'SELECT * FROM violation_template_notices WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

}
?>