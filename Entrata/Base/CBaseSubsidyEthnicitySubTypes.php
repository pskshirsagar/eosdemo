<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyEthnicitySubTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyEthnicitySubTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyEthnicitySubType[]
	 */
	public static function fetchSubsidyEthnicitySubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyEthnicitySubType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyEthnicitySubType
	 */
	public static function fetchSubsidyEthnicitySubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyEthnicitySubType::class, $objDatabase );
	}

	public static function fetchSubsidyEthnicitySubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_ethnicity_sub_types', $objDatabase );
	}

	public static function fetchSubsidyEthnicitySubTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyEthnicitySubType( sprintf( 'SELECT * FROM subsidy_ethnicity_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyEthnicitySubTypesBySubsidyEthnicityTypeId( $intSubsidyEthnicityTypeId, $objDatabase ) {
		return self::fetchSubsidyEthnicitySubTypes( sprintf( 'SELECT * FROM subsidy_ethnicity_sub_types WHERE subsidy_ethnicity_type_id = %d', $intSubsidyEthnicityTypeId ), $objDatabase );
	}

}
?>