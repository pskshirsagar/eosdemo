<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheetGlAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorksheetGlAccounts extends CEosPluralBase {

	/**
	 * @return CBudgetWorksheetGlAccount[]
	 */
	public static function fetchBudgetWorksheetGlAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorksheetGlAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorksheetGlAccount
	 */
	public static function fetchBudgetWorksheetGlAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorksheetGlAccount::class, $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_worksheet_gl_accounts', $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccount( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccounts( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByBudgetWorksheetIdByCid( $intBudgetWorksheetId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccounts( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE budget_worksheet_id = %d AND cid = %d', $intBudgetWorksheetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccounts( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccounts( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByBudgetDataSourceSubTypeIdByCid( $intBudgetDataSourceSubTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetGlAccounts( sprintf( 'SELECT * FROM budget_worksheet_gl_accounts WHERE budget_data_source_sub_type_id = %d AND cid = %d', $intBudgetDataSourceSubTypeId, $intCid ), $objDatabase );
	}

}
?>