<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultDepreciationCategories
 * Do not add any new functions to this class.
 */

class CBaseDefaultDepreciationCategories extends CEosPluralBase {

	/**
	 * @return CDefaultDepreciationCategory[]
	 */
	public static function fetchDefaultDepreciationCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultDepreciationCategory::class, $objDatabase );
	}

	/**
	 * @return CDefaultDepreciationCategory
	 */
	public static function fetchDefaultDepreciationCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultDepreciationCategory::class, $objDatabase );
	}

	public static function fetchDefaultDepreciationCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_depreciation_categories', $objDatabase );
	}

	public static function fetchDefaultDepreciationCategoryById( $intId, $objDatabase ) {
		return self::fetchDefaultDepreciationCategory( sprintf( 'SELECT * FROM default_depreciation_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultDepreciationCategoriesByTaxRecoveryPeriodId( $intTaxRecoveryPeriodId, $objDatabase ) {
		return self::fetchDefaultDepreciationCategories( sprintf( 'SELECT * FROM default_depreciation_categories WHERE tax_recovery_period_id = %d', ( int ) $intTaxRecoveryPeriodId ), $objDatabase );
	}

}
?>