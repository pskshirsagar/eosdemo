<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSubmissionTypes
 * Do not add any new functions to this class.
 */

class CBaseApplicationSubmissionTypes extends CEosPluralBase {

	/**
	 * @return CApplicationSubmissionType[]
	 */
	public static function fetchApplicationSubmissionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationSubmissionType::class, $objDatabase );
	}

	/**
	 * @return CApplicationSubmissionType
	 */
	public static function fetchApplicationSubmissionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationSubmissionType::class, $objDatabase );
	}

	public static function fetchApplicationSubmissionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_submission_types', $objDatabase );
	}

	public static function fetchApplicationSubmissionTypeById( $intId, $objDatabase ) {
		return self::fetchApplicationSubmissionType( sprintf( 'SELECT * FROM application_submission_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>