<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankAccountMerchantAccountLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankAccountMerchantAccountLogs extends CEosPluralBase {

	/**
	 * @return CBankAccountMerchantAccountLog[]
	 */
	public static function fetchBankAccountMerchantAccountLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CBankAccountMerchantAccountLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBankAccountMerchantAccountLog
	 */
	public static function fetchBankAccountMerchantAccountLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBankAccountMerchantAccountLog', $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bank_account_merchant_account_logs', $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccountLog( sprintf( 'SELECT * FROM bank_account_merchant_account_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountLogsByCid( $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccountLogs( sprintf( 'SELECT * FROM bank_account_merchant_account_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountLogsByBankAccountLogIdByCid( $intBankAccountLogId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccountLogs( sprintf( 'SELECT * FROM bank_account_merchant_account_logs WHERE bank_account_log_id = %d AND cid = %d', ( int ) $intBankAccountLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountLogsByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccountLogs( sprintf( 'SELECT * FROM bank_account_merchant_account_logs WHERE merchant_account_id = %d AND cid = %d', ( int ) $intMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>