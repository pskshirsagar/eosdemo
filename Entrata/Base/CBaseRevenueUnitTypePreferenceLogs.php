<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypePreferenceLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueUnitTypePreferenceLogs extends CEosPluralBase {

	/**
	 * @return CRevenueUnitTypePreferenceLog[]
	 */
	public static function fetchRevenueUnitTypePreferenceLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueUnitTypePreferenceLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueUnitTypePreferenceLog
	 */
	public static function fetchRevenueUnitTypePreferenceLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueUnitTypePreferenceLog', $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_unit_type_preference_logs', $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferenceLog( sprintf( 'SELECT * FROM revenue_unit_type_preference_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferenceLogs( sprintf( 'SELECT * FROM revenue_unit_type_preference_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogsByPropertySettingKeyIdByCid( $intPropertySettingKeyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferenceLogs( sprintf( 'SELECT * FROM revenue_unit_type_preference_logs WHERE property_setting_key_id = %d AND cid = %d', ( int ) $intPropertySettingKeyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferenceLogs( sprintf( 'SELECT * FROM revenue_unit_type_preference_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferenceLogs( sprintf( 'SELECT * FROM revenue_unit_type_preference_logs WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>