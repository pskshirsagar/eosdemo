<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledPaymentTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_payment_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledPaymentTransTypeId;
	protected $m_intScheduledPaymentId;
	protected $m_intArPaymentId;
	protected $m_strAdditionalDescription;
	protected $m_strTransactionDatetime;
	protected $m_intIsFailure;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFailure = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_payment_trans_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledPaymentTransTypeId', trim( $arrValues['scheduled_payment_trans_type_id'] ) ); elseif( isset( $arrValues['scheduled_payment_trans_type_id'] ) ) $this->setScheduledPaymentTransTypeId( $arrValues['scheduled_payment_trans_type_id'] );
		if( isset( $arrValues['scheduled_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledPaymentId', trim( $arrValues['scheduled_payment_id'] ) ); elseif( isset( $arrValues['scheduled_payment_id'] ) ) $this->setScheduledPaymentId( $arrValues['scheduled_payment_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['additional_description'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalDescription', trim( stripcslashes( $arrValues['additional_description'] ) ) ); elseif( isset( $arrValues['additional_description'] ) ) $this->setAdditionalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_description'] ) : $arrValues['additional_description'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['is_failure'] ) && $boolDirectSet ) $this->set( 'm_intIsFailure', trim( $arrValues['is_failure'] ) ); elseif( isset( $arrValues['is_failure'] ) ) $this->setIsFailure( $arrValues['is_failure'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledPaymentTransTypeId( $intScheduledPaymentTransTypeId ) {
		$this->set( 'm_intScheduledPaymentTransTypeId', CStrings::strToIntDef( $intScheduledPaymentTransTypeId, NULL, false ) );
	}

	public function getScheduledPaymentTransTypeId() {
		return $this->m_intScheduledPaymentTransTypeId;
	}

	public function sqlScheduledPaymentTransTypeId() {
		return ( true == isset( $this->m_intScheduledPaymentTransTypeId ) ) ? ( string ) $this->m_intScheduledPaymentTransTypeId : 'NULL';
	}

	public function setScheduledPaymentId( $intScheduledPaymentId ) {
		$this->set( 'm_intScheduledPaymentId', CStrings::strToIntDef( $intScheduledPaymentId, NULL, false ) );
	}

	public function getScheduledPaymentId() {
		return $this->m_intScheduledPaymentId;
	}

	public function sqlScheduledPaymentId() {
		return ( true == isset( $this->m_intScheduledPaymentId ) ) ? ( string ) $this->m_intScheduledPaymentId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setAdditionalDescription( $strAdditionalDescription ) {
		$this->set( 'm_strAdditionalDescription', CStrings::strTrimDef( $strAdditionalDescription, 240, NULL, true ) );
	}

	public function getAdditionalDescription() {
		return $this->m_strAdditionalDescription;
	}

	public function sqlAdditionalDescription() {
		return ( true == isset( $this->m_strAdditionalDescription ) ) ? '\'' . addslashes( $this->m_strAdditionalDescription ) . '\'' : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setIsFailure( $intIsFailure ) {
		$this->set( 'm_intIsFailure', CStrings::strToIntDef( $intIsFailure, NULL, false ) );
	}

	public function getIsFailure() {
		return $this->m_intIsFailure;
	}

	public function sqlIsFailure() {
		return ( true == isset( $this->m_intIsFailure ) ) ? ( string ) $this->m_intIsFailure : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_payment_trans_type_id, scheduled_payment_id, ar_payment_id, additional_description, transaction_datetime, is_failure, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScheduledPaymentTransTypeId() . ', ' .
 						$this->sqlScheduledPaymentId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlAdditionalDescription() . ', ' .
 						$this->sqlTransactionDatetime() . ', ' .
 						$this->sqlIsFailure() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_payment_trans_type_id = ' . $this->sqlScheduledPaymentTransTypeId() . ','; } elseif( true == array_key_exists( 'ScheduledPaymentTransTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_payment_trans_type_id = ' . $this->sqlScheduledPaymentTransTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_payment_id = ' . $this->sqlScheduledPaymentId() . ','; } elseif( true == array_key_exists( 'ScheduledPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_payment_id = ' . $this->sqlScheduledPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_description = ' . $this->sqlAdditionalDescription() . ','; } elseif( true == array_key_exists( 'AdditionalDescription', $this->getChangedColumns() ) ) { $strSql .= ' additional_description = ' . $this->sqlAdditionalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failure = ' . $this->sqlIsFailure() . ','; } elseif( true == array_key_exists( 'IsFailure', $this->getChangedColumns() ) ) { $strSql .= ' is_failure = ' . $this->sqlIsFailure() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_payment_trans_type_id' => $this->getScheduledPaymentTransTypeId(),
			'scheduled_payment_id' => $this->getScheduledPaymentId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'additional_description' => $this->getAdditionalDescription(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'is_failure' => $this->getIsFailure(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>