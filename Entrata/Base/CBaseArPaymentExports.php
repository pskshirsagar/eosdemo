<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CArPaymentExports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPaymentExports extends CEosPluralBase {

	/**
	 * @return CArPaymentExport[]
	 */
	public static function fetchArPaymentExports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CArPaymentExport', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPaymentExport
	 */
	public static function fetchArPaymentExport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CArPaymentExport', $objDatabase );
	}

	public static function fetchArPaymentExportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_exports', $objDatabase );
	}

	public static function fetchArPaymentExportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchArPaymentExport( sprintf( 'SELECT * FROM ar_payment_exports WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentExportsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentExports( sprintf( 'SELECT * FROM ar_payment_exports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentExportsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchArPaymentExports( sprintf( 'SELECT * FROM ar_payment_exports WHERE company_merchant_account_id = %d AND cid = %d', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>