<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItemTypes extends CEosPluralBase {

    const TABLE_RENTABLE_ITEM_TYPES = 'public.rentable_item_types';

    public static function fetchRentableItemTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRentableItemType', $objDatabase );
    }

    public static function fetchRentableItemType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRentableItemType', $objDatabase );
    }

    public static function fetchRentableItemTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'rentable_item_types', $objDatabase );
    }

    public static function fetchRentableItemTypeById( $intId, $objDatabase ) {
        return self::fetchRentableItemType( sprintf( 'SELECT * FROM rentable_item_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>