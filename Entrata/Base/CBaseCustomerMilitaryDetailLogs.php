<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMilitaryDetailLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerMilitaryDetailLogs extends CEosPluralBase {

	/**
	 * @return CCustomerMilitaryDetailLog[]
	 */
	public static function fetchCustomerMilitaryDetailLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerMilitaryDetailLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerMilitaryDetailLog
	 */
	public static function fetchCustomerMilitaryDetailLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerMilitaryDetailLog', $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_military_detail_logs', $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLog( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByCustomerMilitaryDetailIdByCid( $intCustomerMilitaryDetailId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE customer_military_detail_id = %d AND cid = %d', ( int ) $intCustomerMilitaryDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryStatusIdByCid( $intMilitaryStatusId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_status_id = %d AND cid = %d', ( int ) $intMilitaryStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryComponentIdByCid( $intMilitaryComponentId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_component_id = %d AND cid = %d', ( int ) $intMilitaryComponentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryPayGradeIdByCid( $intMilitaryPayGradeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_pay_grade_id = %d AND cid = %d', ( int ) $intMilitaryPayGradeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryRankIdByCid( $intMilitaryRankId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_rank_id = %d AND cid = %d', ( int ) $intMilitaryRankId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryAssistanceTypeIdByCid( $intMilitaryAssistanceTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_assistance_type_id = %d AND cid = %d', ( int ) $intMilitaryAssistanceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByMilitaryNonMilitaryTypeIdByCid( $intMilitaryNonMilitaryTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE military_non_military_type_id = %d AND cid = %d', ( int ) $intMilitaryNonMilitaryTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByFutureMilitaryInstallationIdByCid( $intFutureMilitaryInstallationId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE future_military_installation_id = %d AND cid = %d', ( int ) $intFutureMilitaryInstallationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMilitaryDetailLogsByPriorCustomerMilitaryDetailLogIdByCid( $intPriorCustomerMilitaryDetailLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerMilitaryDetailLogs( sprintf( 'SELECT * FROM customer_military_detail_logs WHERE prior_customer_military_detail_log_id = %d AND cid = %d', ( int ) $intPriorCustomerMilitaryDetailLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>