<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldTypes
 * Do not add any new functions to this class.
 */

class CBaseMergeFieldTypes extends CEosPluralBase {

	/**
	 * @return CMergeFieldType[]
	 */
	public static function fetchMergeFieldTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMergeFieldType::class, $objDatabase );
	}

	/**
	 * @return CMergeFieldType
	 */
	public static function fetchMergeFieldType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMergeFieldType::class, $objDatabase );
	}

	public static function fetchMergeFieldTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merge_field_types', $objDatabase );
	}

	public static function fetchMergeFieldTypeById( $intId, $objDatabase ) {
		return self::fetchMergeFieldType( sprintf( 'SELECT * FROM merge_field_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>