<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAddOn extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.add_ons';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitSpaceId;
	protected $m_intAddOnGroupId;
	protected $m_intUnitSpaceStatusTypeId;
	protected $m_intAmenityId;
	protected $m_intMaintenanceTemplateId;
	protected $m_strRemotePrimaryKey;
	protected $m_strLookupCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intXPos;
	protected $m_intYPos;
	protected $m_strAvailableOn;
	protected $m_strHoldUntilDate;
	protected $m_intExpireMonths;
	protected $m_boolShowOnWebsite;
	protected $m_boolIsActive;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowOnWebsite = true;
		$this->m_boolIsActive = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['add_on_group_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnGroupId', trim( $arrValues['add_on_group_id'] ) ); elseif( isset( $arrValues['add_on_group_id'] ) ) $this->setAddOnGroupId( $arrValues['add_on_group_id'] );
		if( isset( $arrValues['unit_space_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceStatusTypeId', trim( $arrValues['unit_space_status_type_id'] ) ); elseif( isset( $arrValues['unit_space_status_type_id'] ) ) $this->setUnitSpaceStatusTypeId( $arrValues['unit_space_status_type_id'] );
		if( isset( $arrValues['amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityId', trim( $arrValues['amenity_id'] ) ); elseif( isset( $arrValues['amenity_id'] ) ) $this->setAmenityId( $arrValues['amenity_id'] );
		if( isset( $arrValues['maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceTemplateId', trim( $arrValues['maintenance_template_id'] ) ); elseif( isset( $arrValues['maintenance_template_id'] ) ) $this->setMaintenanceTemplateId( $arrValues['maintenance_template_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( stripcslashes( $arrValues['lookup_code'] ) ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['x_pos'] ) && $boolDirectSet ) $this->set( 'm_intXPos', trim( $arrValues['x_pos'] ) ); elseif( isset( $arrValues['x_pos'] ) ) $this->setXPos( $arrValues['x_pos'] );
		if( isset( $arrValues['y_pos'] ) && $boolDirectSet ) $this->set( 'm_intYPos', trim( $arrValues['y_pos'] ) ); elseif( isset( $arrValues['y_pos'] ) ) $this->setYPos( $arrValues['y_pos'] );
		if( isset( $arrValues['available_on'] ) && $boolDirectSet ) $this->set( 'm_strAvailableOn', trim( $arrValues['available_on'] ) ); elseif( isset( $arrValues['available_on'] ) ) $this->setAvailableOn( $arrValues['available_on'] );
		if( isset( $arrValues['hold_until_date'] ) && $boolDirectSet ) $this->set( 'm_strHoldUntilDate', trim( $arrValues['hold_until_date'] ) ); elseif( isset( $arrValues['hold_until_date'] ) ) $this->setHoldUntilDate( $arrValues['hold_until_date'] );
		if( isset( $arrValues['expire_months'] ) && $boolDirectSet ) $this->set( 'm_intExpireMonths', trim( $arrValues['expire_months'] ) ); elseif( isset( $arrValues['expire_months'] ) ) $this->setExpireMonths( $arrValues['expire_months'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setAddOnGroupId( $intAddOnGroupId ) {
		$this->set( 'm_intAddOnGroupId', CStrings::strToIntDef( $intAddOnGroupId, NULL, false ) );
	}

	public function getAddOnGroupId() {
		return $this->m_intAddOnGroupId;
	}

	public function sqlAddOnGroupId() {
		return ( true == isset( $this->m_intAddOnGroupId ) ) ? ( string ) $this->m_intAddOnGroupId : 'NULL';
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->set( 'm_intUnitSpaceStatusTypeId', CStrings::strToIntDef( $intUnitSpaceStatusTypeId, NULL, false ) );
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

	public function sqlUnitSpaceStatusTypeId() {
		return ( true == isset( $this->m_intUnitSpaceStatusTypeId ) ) ? ( string ) $this->m_intUnitSpaceStatusTypeId : 'NULL';
	}

	public function setAmenityId( $intAmenityId ) {
		$this->set( 'm_intAmenityId', CStrings::strToIntDef( $intAmenityId, NULL, false ) );
	}

	public function getAmenityId() {
		return $this->m_intAmenityId;
	}

	public function sqlAmenityId() {
		return ( true == isset( $this->m_intAmenityId ) ) ? ( string ) $this->m_intAmenityId : 'NULL';
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->set( 'm_intMaintenanceTemplateId', CStrings::strToIntDef( $intMaintenanceTemplateId, NULL, false ) );
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function sqlMaintenanceTemplateId() {
		return ( true == isset( $this->m_intMaintenanceTemplateId ) ) ? ( string ) $this->m_intMaintenanceTemplateId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 164, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 64, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? '\'' . addslashes( $this->m_strLookupCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setXPos( $intXPos ) {
		$this->set( 'm_intXPos', CStrings::strToIntDef( $intXPos, NULL, false ) );
	}

	public function getXPos() {
		return $this->m_intXPos;
	}

	public function sqlXPos() {
		return ( true == isset( $this->m_intXPos ) ) ? ( string ) $this->m_intXPos : 'NULL';
	}

	public function setYPos( $intYPos ) {
		$this->set( 'm_intYPos', CStrings::strToIntDef( $intYPos, NULL, false ) );
	}

	public function getYPos() {
		return $this->m_intYPos;
	}

	public function sqlYPos() {
		return ( true == isset( $this->m_intYPos ) ) ? ( string ) $this->m_intYPos : 'NULL';
	}

	public function setAvailableOn( $strAvailableOn ) {
		$this->set( 'm_strAvailableOn', CStrings::strTrimDef( $strAvailableOn, -1, NULL, true ) );
	}

	public function getAvailableOn() {
		return $this->m_strAvailableOn;
	}

	public function sqlAvailableOn() {
		return ( true == isset( $this->m_strAvailableOn ) ) ? '\'' . $this->m_strAvailableOn . '\'' : 'NULL';
	}

	public function setHoldUntilDate( $strHoldUntilDate ) {
		$this->set( 'm_strHoldUntilDate', CStrings::strTrimDef( $strHoldUntilDate, -1, NULL, true ) );
	}

	public function getHoldUntilDate() {
		return $this->m_strHoldUntilDate;
	}

	public function sqlHoldUntilDate() {
		return ( true == isset( $this->m_strHoldUntilDate ) ) ? '\'' . $this->m_strHoldUntilDate . '\'' : 'NULL';
	}

	public function setExpireMonths( $intExpireMonths ) {
		$this->set( 'm_intExpireMonths', CStrings::strToIntDef( $intExpireMonths, NULL, false ) );
	}

	public function getExpireMonths() {
		return $this->m_intExpireMonths;
	}

	public function sqlExpireMonths() {
		return ( true == isset( $this->m_intExpireMonths ) ) ? ( string ) $this->m_intExpireMonths : 'NULL';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_space_id, add_on_group_id, unit_space_status_type_id, amenity_id, maintenance_template_id, remote_primary_key, lookup_code, name, description, x_pos, y_pos, available_on, hold_until_date, expire_months, show_on_website, is_active, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlAddOnGroupId() . ', ' .
						$this->sqlUnitSpaceStatusTypeId() . ', ' .
						$this->sqlAmenityId() . ', ' .
						$this->sqlMaintenanceTemplateId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlLookupCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlXPos() . ', ' .
						$this->sqlYPos() . ', ' .
						$this->sqlAvailableOn() . ', ' .
						$this->sqlHoldUntilDate() . ', ' .
						$this->sqlExpireMonths() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_group_id = ' . $this->sqlAddOnGroupId(). ',' ; } elseif( true == array_key_exists( 'AddOnGroupId', $this->getChangedColumns() ) ) { $strSql .= ' add_on_group_id = ' . $this->sqlAddOnGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId(). ',' ; } elseif( true == array_key_exists( 'AmenityId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode(). ',' ; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x_pos = ' . $this->sqlXPos(). ',' ; } elseif( true == array_key_exists( 'XPos', $this->getChangedColumns() ) ) { $strSql .= ' x_pos = ' . $this->sqlXPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' y_pos = ' . $this->sqlYPos(). ',' ; } elseif( true == array_key_exists( 'YPos', $this->getChangedColumns() ) ) { $strSql .= ' y_pos = ' . $this->sqlYPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn(). ',' ; } elseif( true == array_key_exists( 'AvailableOn', $this->getChangedColumns() ) ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hold_until_date = ' . $this->sqlHoldUntilDate(). ',' ; } elseif( true == array_key_exists( 'HoldUntilDate', $this->getChangedColumns() ) ) { $strSql .= ' hold_until_date = ' . $this->sqlHoldUntilDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expire_months = ' . $this->sqlExpireMonths(). ',' ; } elseif( true == array_key_exists( 'ExpireMonths', $this->getChangedColumns() ) ) { $strSql .= ' expire_months = ' . $this->sqlExpireMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'add_on_group_id' => $this->getAddOnGroupId(),
			'unit_space_status_type_id' => $this->getUnitSpaceStatusTypeId(),
			'amenity_id' => $this->getAmenityId(),
			'maintenance_template_id' => $this->getMaintenanceTemplateId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'lookup_code' => $this->getLookupCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'x_pos' => $this->getXPos(),
			'y_pos' => $this->getYPos(),
			'available_on' => $this->getAvailableOn(),
			'hold_until_date' => $this->getHoldUntilDate(),
			'expire_months' => $this->getExpireMonths(),
			'show_on_website' => $this->getShowOnWebsite(),
			'is_active' => $this->getIsActive(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>