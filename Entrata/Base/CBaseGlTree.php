<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlTree extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.gl_trees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlTreeTypeId;
	protected $m_intModeTypeId;
	protected $m_intRestrictedGlTreeId;
	protected $m_intGlChartId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strAccountNumberPattern;
	protected $m_strAccountNumberDelimiter;
	protected $m_strRemotePrimaryKey;
	protected $m_intIsRestricted;
	protected $m_intIsCategorized;
	protected $m_intIsSystem;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intModeTypeId = '1';
		$this->m_strRemotePrimaryKey = NULL;
		$this->m_intIsRestricted = '0';
		$this->m_intIsCategorized = '0';
		$this->m_intIsSystem = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_tree_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeTypeId', trim( $arrValues['gl_tree_type_id'] ) ); elseif( isset( $arrValues['gl_tree_type_id'] ) ) $this->setGlTreeTypeId( $arrValues['gl_tree_type_id'] );
		if( isset( $arrValues['mode_type_id'] ) && $boolDirectSet ) $this->set( 'm_intModeTypeId', trim( $arrValues['mode_type_id'] ) ); elseif( isset( $arrValues['mode_type_id'] ) ) $this->setModeTypeId( $arrValues['mode_type_id'] );
		if( isset( $arrValues['restricted_gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intRestrictedGlTreeId', trim( $arrValues['restricted_gl_tree_id'] ) ); elseif( isset( $arrValues['restricted_gl_tree_id'] ) ) $this->setRestrictedGlTreeId( $arrValues['restricted_gl_tree_id'] );
		if( isset( $arrValues['gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intGlChartId', trim( $arrValues['gl_chart_id'] ) ); elseif( isset( $arrValues['gl_chart_id'] ) ) $this->setGlChartId( $arrValues['gl_chart_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['account_number_pattern'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberPattern', trim( stripcslashes( $arrValues['account_number_pattern'] ) ) ); elseif( isset( $arrValues['account_number_pattern'] ) ) $this->setAccountNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_pattern'] ) : $arrValues['account_number_pattern'] );
		if( isset( $arrValues['account_number_delimiter'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumberDelimiter', trim( stripcslashes( $arrValues['account_number_delimiter'] ) ) ); elseif( isset( $arrValues['account_number_delimiter'] ) ) $this->setAccountNumberDelimiter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_delimiter'] ) : $arrValues['account_number_delimiter'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['is_restricted'] ) && $boolDirectSet ) $this->set( 'm_intIsRestricted', trim( $arrValues['is_restricted'] ) ); elseif( isset( $arrValues['is_restricted'] ) ) $this->setIsRestricted( $arrValues['is_restricted'] );
		if( isset( $arrValues['is_categorized'] ) && $boolDirectSet ) $this->set( 'm_intIsCategorized', trim( $arrValues['is_categorized'] ) ); elseif( isset( $arrValues['is_categorized'] ) ) $this->setIsCategorized( $arrValues['is_categorized'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlTreeTypeId( $intGlTreeTypeId ) {
		$this->set( 'm_intGlTreeTypeId', CStrings::strToIntDef( $intGlTreeTypeId, NULL, false ) );
	}

	public function getGlTreeTypeId() {
		return $this->m_intGlTreeTypeId;
	}

	public function sqlGlTreeTypeId() {
		return ( true == isset( $this->m_intGlTreeTypeId ) ) ? ( string ) $this->m_intGlTreeTypeId : 'NULL';
	}

	public function setModeTypeId( $intModeTypeId ) {
		$this->set( 'm_intModeTypeId', CStrings::strToIntDef( $intModeTypeId, NULL, false ) );
	}

	public function getModeTypeId() {
		return $this->m_intModeTypeId;
	}

	public function sqlModeTypeId() {
		return ( true == isset( $this->m_intModeTypeId ) ) ? ( string ) $this->m_intModeTypeId : '1';
	}

	public function setRestrictedGlTreeId( $intRestrictedGlTreeId ) {
		$this->set( 'm_intRestrictedGlTreeId', CStrings::strToIntDef( $intRestrictedGlTreeId, NULL, false ) );
	}

	public function getRestrictedGlTreeId() {
		return $this->m_intRestrictedGlTreeId;
	}

	public function sqlRestrictedGlTreeId() {
		return ( true == isset( $this->m_intRestrictedGlTreeId ) ) ? ( string ) $this->m_intRestrictedGlTreeId : 'NULL';
	}

	public function setGlChartId( $intGlChartId ) {
		$this->set( 'm_intGlChartId', CStrings::strToIntDef( $intGlChartId, NULL, false ) );
	}

	public function getGlChartId() {
		return $this->m_intGlChartId;
	}

	public function sqlGlChartId() {
		return ( true == isset( $this->m_intGlChartId ) ) ? ( string ) $this->m_intGlChartId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAccountNumberPattern( $strAccountNumberPattern ) {
		$this->set( 'm_strAccountNumberPattern', CStrings::strTrimDef( $strAccountNumberPattern, 50, NULL, true ) );
	}

	public function getAccountNumberPattern() {
		return $this->m_strAccountNumberPattern;
	}

	public function sqlAccountNumberPattern() {
		return ( true == isset( $this->m_strAccountNumberPattern ) ) ? '\'' . addslashes( $this->m_strAccountNumberPattern ) . '\'' : 'NULL';
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		$this->set( 'm_strAccountNumberDelimiter', CStrings::strTrimDef( $strAccountNumberDelimiter, 50, NULL, true ) );
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	public function sqlAccountNumberDelimiter() {
		return ( true == isset( $this->m_strAccountNumberDelimiter ) ) ? '\'' . addslashes( $this->m_strAccountNumberDelimiter ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : '\'NULL\'';
	}

	public function setIsRestricted( $intIsRestricted ) {
		$this->set( 'm_intIsRestricted', CStrings::strToIntDef( $intIsRestricted, NULL, false ) );
	}

	public function getIsRestricted() {
		return $this->m_intIsRestricted;
	}

	public function sqlIsRestricted() {
		return ( true == isset( $this->m_intIsRestricted ) ) ? ( string ) $this->m_intIsRestricted : '0';
	}

	public function setIsCategorized( $intIsCategorized ) {
		$this->set( 'm_intIsCategorized', CStrings::strToIntDef( $intIsCategorized, NULL, false ) );
	}

	public function getIsCategorized() {
		return $this->m_intIsCategorized;
	}

	public function sqlIsCategorized() {
		return ( true == isset( $this->m_intIsCategorized ) ) ? ( string ) $this->m_intIsCategorized : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_tree_type_id, mode_type_id, restricted_gl_tree_id, gl_chart_id, system_code, name, account_number_pattern, account_number_delimiter, remote_primary_key, is_restricted, is_categorized, is_system, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlTreeTypeId() . ', ' .
						$this->sqlModeTypeId() . ', ' .
						$this->sqlRestrictedGlTreeId() . ', ' .
						$this->sqlGlChartId() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAccountNumberPattern() . ', ' .
						$this->sqlAccountNumberDelimiter() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlIsRestricted() . ', ' .
						$this->sqlIsCategorized() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_type_id = ' . $this->sqlGlTreeTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_type_id = ' . $this->sqlGlTreeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mode_type_id = ' . $this->sqlModeTypeId(). ',' ; } elseif( true == array_key_exists( 'ModeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' mode_type_id = ' . $this->sqlModeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restricted_gl_tree_id = ' . $this->sqlRestrictedGlTreeId(). ',' ; } elseif( true == array_key_exists( 'RestrictedGlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' restricted_gl_tree_id = ' . $this->sqlRestrictedGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId(). ',' ; } elseif( true == array_key_exists( 'GlChartId', $this->getChangedColumns() ) ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_pattern = ' . $this->sqlAccountNumberPattern(). ',' ; } elseif( true == array_key_exists( 'AccountNumberPattern', $this->getChangedColumns() ) ) { $strSql .= ' account_number_pattern = ' . $this->sqlAccountNumberPattern() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_delimiter = ' . $this->sqlAccountNumberDelimiter(). ',' ; } elseif( true == array_key_exists( 'AccountNumberDelimiter', $this->getChangedColumns() ) ) { $strSql .= ' account_number_delimiter = ' . $this->sqlAccountNumberDelimiter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_restricted = ' . $this->sqlIsRestricted(). ',' ; } elseif( true == array_key_exists( 'IsRestricted', $this->getChangedColumns() ) ) { $strSql .= ' is_restricted = ' . $this->sqlIsRestricted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_categorized = ' . $this->sqlIsCategorized(). ',' ; } elseif( true == array_key_exists( 'IsCategorized', $this->getChangedColumns() ) ) { $strSql .= ' is_categorized = ' . $this->sqlIsCategorized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_tree_type_id' => $this->getGlTreeTypeId(),
			'mode_type_id' => $this->getModeTypeId(),
			'restricted_gl_tree_id' => $this->getRestrictedGlTreeId(),
			'gl_chart_id' => $this->getGlChartId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'account_number_pattern' => $this->getAccountNumberPattern(),
			'account_number_delimiter' => $this->getAccountNumberDelimiter(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'is_restricted' => $this->getIsRestricted(),
			'is_categorized' => $this->getIsCategorized(),
			'is_system' => $this->getIsSystem(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>