<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerIncomeLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerIncomeLogs extends CEosPluralBase {

	/**
	 * @return CCustomerIncomeLog[]
	 */
	public static function fetchCustomerIncomeLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerIncomeLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerIncomeLog
	 */
	public static function fetchCustomerIncomeLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerIncomeLog::class, $objDatabase );
	}

	public static function fetchCustomerIncomeLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_income_logs', $objDatabase );
	}

	public static function fetchCustomerIncomeLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLog( sprintf( 'SELECT * FROM customer_income_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByCustomerIncomeIdByCid( $intCustomerIncomeId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE customer_income_id = %d AND cid = %d', $intCustomerIncomeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByIncomeTypeIdByCid( $intIncomeTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE income_type_id = %d AND cid = %d', $intIncomeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByPriorCustomerIncomeLogIdByCid( $intPriorCustomerIncomeLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE prior_customer_income_log_id = %d AND cid = %d', $intPriorCustomerIncomeLogId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeLogsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomeLogs( sprintf( 'SELECT * FROM customer_income_logs WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

}
?>