<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitOfMeasures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitOfMeasures extends CEosPluralBase {

	/**
	 * @return CUnitOfMeasure[]
	 */
	public static function fetchUnitOfMeasures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitOfMeasure::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitOfMeasure
	 */
	public static function fetchUnitOfMeasure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitOfMeasure::class, $objDatabase );
	}

	public static function fetchUnitOfMeasureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_of_measures', $objDatabase );
	}

	public static function fetchUnitOfMeasureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasure( sprintf( 'SELECT * FROM unit_of_measures WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasuresByCid( $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasures( sprintf( 'SELECT * FROM unit_of_measures WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasuresByDefaultUnitOfMeasureIdByCid( $intDefaultUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasures( sprintf( 'SELECT * FROM unit_of_measures WHERE default_unit_of_measure_id = %d AND cid = %d', $intDefaultUnitOfMeasureId, $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasuresByUnitOfMeasureTypeIdByCid( $intUnitOfMeasureTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasures( sprintf( 'SELECT * FROM unit_of_measures WHERE unit_of_measure_type_id = %d AND cid = %d', $intUnitOfMeasureTypeId, $intCid ), $objDatabase );
	}

}
?>