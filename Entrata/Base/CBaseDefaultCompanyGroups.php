<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCompanyGroups
 * Do not add any new functions to this class.
 */

class CBaseDefaultCompanyGroups extends CEosPluralBase {

	/**
	 * @return CDefaultCompanyGroup[]
	 */
	public static function fetchDefaultCompanyGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCompanyGroup::class, $objDatabase );
	}

	/**
	 * @return CDefaultCompanyGroup
	 */
	public static function fetchDefaultCompanyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCompanyGroup::class, $objDatabase );
	}

	public static function fetchDefaultCompanyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_company_groups', $objDatabase );
	}

	public static function fetchDefaultCompanyGroupById( $intId, $objDatabase ) {
		return self::fetchDefaultCompanyGroup( sprintf( 'SELECT * FROM default_company_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>