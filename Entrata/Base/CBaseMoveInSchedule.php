<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMoveInSchedule extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.move_in_schedules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intMoveInScheduleTypeId;
	protected $m_strScheduledMoveInDate;
	protected $m_strScheduledMoveInStartTime;
	protected $m_strScheduledMoveInEndTime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMoveInScheduleGroupId;
	protected $m_intAppointmentCap;
	protected $m_intDayOfWeek;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['move_in_schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInScheduleTypeId', trim( $arrValues['move_in_schedule_type_id'] ) ); elseif( isset( $arrValues['move_in_schedule_type_id'] ) ) $this->setMoveInScheduleTypeId( $arrValues['move_in_schedule_type_id'] );
		if( isset( $arrValues['scheduled_move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInDate', trim( $arrValues['scheduled_move_in_date'] ) ); elseif( isset( $arrValues['scheduled_move_in_date'] ) ) $this->setScheduledMoveInDate( $arrValues['scheduled_move_in_date'] );
		if( isset( $arrValues['scheduled_move_in_start_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInStartTime', trim( $arrValues['scheduled_move_in_start_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_start_time'] ) ) $this->setScheduledMoveInStartTime( $arrValues['scheduled_move_in_start_time'] );
		if( isset( $arrValues['scheduled_move_in_end_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInEndTime', trim( $arrValues['scheduled_move_in_end_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_end_time'] ) ) $this->setScheduledMoveInEndTime( $arrValues['scheduled_move_in_end_time'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['move_in_schedule_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInScheduleGroupId', trim( $arrValues['move_in_schedule_group_id'] ) ); elseif( isset( $arrValues['move_in_schedule_group_id'] ) ) $this->setMoveInScheduleGroupId( $arrValues['move_in_schedule_group_id'] );
		if( isset( $arrValues['appointment_cap'] ) && $boolDirectSet ) $this->set( 'm_intAppointmentCap', trim( $arrValues['appointment_cap'] ) ); elseif( isset( $arrValues['appointment_cap'] ) ) $this->setAppointmentCap( $arrValues['appointment_cap'] );
		if( isset( $arrValues['day_of_week'] ) && $boolDirectSet ) $this->set( 'm_intDayOfWeek', trim( $arrValues['day_of_week'] ) ); elseif( isset( $arrValues['day_of_week'] ) ) $this->setDayOfWeek( $arrValues['day_of_week'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setMoveInScheduleTypeId( $intMoveInScheduleTypeId ) {
		$this->set( 'm_intMoveInScheduleTypeId', CStrings::strToIntDef( $intMoveInScheduleTypeId, NULL, false ) );
	}

	public function getMoveInScheduleTypeId() {
		return $this->m_intMoveInScheduleTypeId;
	}

	public function sqlMoveInScheduleTypeId() {
		return ( true == isset( $this->m_intMoveInScheduleTypeId ) ) ? ( string ) $this->m_intMoveInScheduleTypeId : 'NULL';
	}

	public function setScheduledMoveInDate( $strScheduledMoveInDate ) {
		$this->set( 'm_strScheduledMoveInDate', CStrings::strTrimDef( $strScheduledMoveInDate, -1, NULL, true ) );
	}

	public function getScheduledMoveInDate() {
		return $this->m_strScheduledMoveInDate;
	}

	public function sqlScheduledMoveInDate() {
		return ( true == isset( $this->m_strScheduledMoveInDate ) ) ? '\'' . $this->m_strScheduledMoveInDate . '\'' : 'NOW()';
	}

	public function setScheduledMoveInStartTime( $strScheduledMoveInStartTime ) {
		$this->set( 'm_strScheduledMoveInStartTime', CStrings::strTrimDef( $strScheduledMoveInStartTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInStartTime() {
		return $this->m_strScheduledMoveInStartTime;
	}

	public function sqlScheduledMoveInStartTime() {
		return ( true == isset( $this->m_strScheduledMoveInStartTime ) ) ? '\'' . $this->m_strScheduledMoveInStartTime . '\'' : 'NULL';
	}

	public function setScheduledMoveInEndTime( $strScheduledMoveInEndTime ) {
		$this->set( 'm_strScheduledMoveInEndTime', CStrings::strTrimDef( $strScheduledMoveInEndTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInEndTime() {
		return $this->m_strScheduledMoveInEndTime;
	}

	public function sqlScheduledMoveInEndTime() {
		return ( true == isset( $this->m_strScheduledMoveInEndTime ) ) ? '\'' . $this->m_strScheduledMoveInEndTime . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMoveInScheduleGroupId( $intMoveInScheduleGroupId ) {
		$this->set( 'm_intMoveInScheduleGroupId', CStrings::strToIntDef( $intMoveInScheduleGroupId, NULL, false ) );
	}

	public function getMoveInScheduleGroupId() {
		return $this->m_intMoveInScheduleGroupId;
	}

	public function sqlMoveInScheduleGroupId() {
		return ( true == isset( $this->m_intMoveInScheduleGroupId ) ) ? ( string ) $this->m_intMoveInScheduleGroupId : 'NULL';
	}

	public function setAppointmentCap( $intAppointmentCap ) {
		$this->set( 'm_intAppointmentCap', CStrings::strToIntDef( $intAppointmentCap, NULL, false ) );
	}

	public function getAppointmentCap() {
		return $this->m_intAppointmentCap;
	}

	public function sqlAppointmentCap() {
		return ( true == isset( $this->m_intAppointmentCap ) ) ? ( string ) $this->m_intAppointmentCap : 'NULL';
	}

	public function setDayOfWeek( $intDayOfWeek ) {
		$this->set( 'm_intDayOfWeek', CStrings::strToIntDef( $intDayOfWeek, NULL, false ) );
	}

	public function getDayOfWeek() {
		return $this->m_intDayOfWeek;
	}

	public function sqlDayOfWeek() {
		return ( true == isset( $this->m_intDayOfWeek ) ) ? ( string ) $this->m_intDayOfWeek : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_term_id, lease_start_window_id, move_in_schedule_type_id, scheduled_move_in_date, scheduled_move_in_start_time, scheduled_move_in_end_time, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, move_in_schedule_group_id, appointment_cap, day_of_week )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlLeaseTermId() . ', ' .
		          $this->sqlLeaseStartWindowId() . ', ' .
		          $this->sqlMoveInScheduleTypeId() . ', ' .
		          $this->sqlScheduledMoveInDate() . ', ' .
		          $this->sqlScheduledMoveInStartTime() . ', ' .
		          $this->sqlScheduledMoveInEndTime() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlMoveInScheduleGroupId() . ', ' .
		          $this->sqlAppointmentCap() . ', ' .
		          $this->sqlDayOfWeek() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_schedule_type_id = ' . $this->sqlMoveInScheduleTypeId(). ',' ; } elseif( true == array_key_exists( 'MoveInScheduleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_schedule_type_id = ' . $this->sqlMoveInScheduleTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_date = ' . $this->sqlScheduledMoveInDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledMoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_date = ' . $this->sqlScheduledMoveInDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime(). ',' ; } elseif( true == array_key_exists( 'ScheduledMoveInStartTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime(). ',' ; } elseif( true == array_key_exists( 'ScheduledMoveInEndTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_schedule_group_id = ' . $this->sqlMoveInScheduleGroupId(). ',' ; } elseif( true == array_key_exists( 'MoveInScheduleGroupId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_schedule_group_id = ' . $this->sqlMoveInScheduleGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' appointment_cap = ' . $this->sqlAppointmentCap(). ',' ; } elseif( true == array_key_exists( 'AppointmentCap', $this->getChangedColumns() ) ) { $strSql .= ' appointment_cap = ' . $this->sqlAppointmentCap() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek(). ',' ; } elseif( true == array_key_exists( 'DayOfWeek', $this->getChangedColumns() ) ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'move_in_schedule_type_id' => $this->getMoveInScheduleTypeId(),
			'scheduled_move_in_date' => $this->getScheduledMoveInDate(),
			'scheduled_move_in_start_time' => $this->getScheduledMoveInStartTime(),
			'scheduled_move_in_end_time' => $this->getScheduledMoveInEndTime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'move_in_schedule_group_id' => $this->getMoveInScheduleGroupId(),
			'appointment_cap' => $this->getAppointmentCap(),
			'day_of_week' => $this->getDayOfWeek()
		);
	}

}
?>