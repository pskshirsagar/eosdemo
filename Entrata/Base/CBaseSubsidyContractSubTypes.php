<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyContractSubTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyContractSubTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyContractSubType[]
	 */
	public static function fetchSubsidyContractSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyContractSubType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyContractSubType
	 */
	public static function fetchSubsidyContractSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyContractSubType::class, $objDatabase );
	}

	public static function fetchSubsidyContractSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_contract_sub_types', $objDatabase );
	}

	public static function fetchSubsidyContractSubTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyContractSubType( sprintf( 'SELECT * FROM subsidy_contract_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyContractSubTypesBySubsidyTypeId( $intSubsidyTypeId, $objDatabase ) {
		return self::fetchSubsidyContractSubTypes( sprintf( 'SELECT * FROM subsidy_contract_sub_types WHERE subsidy_type_id = %d', $intSubsidyTypeId ), $objDatabase );
	}

	public static function fetchSubsidyContractSubTypesBySubsidyContractTypeId( $intSubsidyContractTypeId, $objDatabase ) {
		return self::fetchSubsidyContractSubTypes( sprintf( 'SELECT * FROM subsidy_contract_sub_types WHERE subsidy_contract_type_id = %d', $intSubsidyContractTypeId ), $objDatabase );
	}

}
?>