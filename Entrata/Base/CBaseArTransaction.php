<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArTransaction extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ar_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intCustomerId;
	protected $m_intArCodeTypeId;
	protected $m_intArCodeId;
	protected $m_intArOriginId;
	protected $m_intArOriginReferenceId;
	protected $m_intArOriginObjectId;
	protected $m_intArTriggerId;
	protected $m_intArProcessId;
	protected $m_intArPaymentId;
	protected $m_intArPaymentSplitId;
	protected $m_intAccrualDebitGlAccountId;
	protected $m_intAccrualCreditGlAccountId;
	protected $m_intCashDebitGlAccountId;
	protected $m_intCashCreditGlAccountId;
	protected $m_intArTransactionId;
	protected $m_intApAllocationId;
	protected $m_intApDetailId;
	protected $m_intScheduledChargeId;
	protected $m_intPeriodId;
	protected $m_intDependentArTransactionId;
	protected $m_strRemotePrimaryKey;
	protected $m_strTransactionDatetime;
	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_fltInitialAmountDue;
	protected $m_strReportingPostDate;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_strMemo;
	protected $m_strInternalMemo;
	protected $m_boolIsPaymentInKind;
	protected $m_boolIsDepositCredit;
	protected $m_boolIsReversal;
	protected $m_boolIsTemporary;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_boolHasDependencies;
	protected $m_boolIsDeleted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intGlDimensionId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intArInvoiceId;
	protected $m_intCompetitorId;
	protected $m_intPsProductId;
	protected $m_boolIsDeposited;

	public function __construct() {
		parent::__construct();

		$this->m_fltTransactionAmountDue = '0';
		$this->m_fltInitialAmountDue = '0';
		$this->m_boolIsPaymentInKind = false;
		$this->m_boolIsDepositCredit = false;
		$this->m_boolIsReversal = false;
		$this->m_boolIsTemporary = false;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;
		$this->m_boolHasDependencies = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsDeposited = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
		if( isset( $arrValues['ar_origin_object_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginObjectId', trim( $arrValues['ar_origin_object_id'] ) ); elseif( isset( $arrValues['ar_origin_object_id'] ) ) $this->setArOriginObjectId( $arrValues['ar_origin_object_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['ar_process_id'] ) && $boolDirectSet ) $this->set( 'm_intArProcessId', trim( $arrValues['ar_process_id'] ) ); elseif( isset( $arrValues['ar_process_id'] ) ) $this->setArProcessId( $arrValues['ar_process_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['ar_payment_split_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentSplitId', trim( $arrValues['ar_payment_split_id'] ) ); elseif( isset( $arrValues['ar_payment_split_id'] ) ) $this->setArPaymentSplitId( $arrValues['ar_payment_split_id'] );
		if( isset( $arrValues['accrual_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualDebitGlAccountId', trim( $arrValues['accrual_debit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_debit_gl_account_id'] ) ) $this->setAccrualDebitGlAccountId( $arrValues['accrual_debit_gl_account_id'] );
		if( isset( $arrValues['accrual_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualCreditGlAccountId', trim( $arrValues['accrual_credit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_credit_gl_account_id'] ) ) $this->setAccrualCreditGlAccountId( $arrValues['accrual_credit_gl_account_id'] );
		if( isset( $arrValues['cash_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashDebitGlAccountId', trim( $arrValues['cash_debit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_debit_gl_account_id'] ) ) $this->setCashDebitGlAccountId( $arrValues['cash_debit_gl_account_id'] );
		if( isset( $arrValues['cash_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashCreditGlAccountId', trim( $arrValues['cash_credit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_credit_gl_account_id'] ) ) $this->setCashCreditGlAccountId( $arrValues['cash_credit_gl_account_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['ap_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intApAllocationId', trim( $arrValues['ap_allocation_id'] ) ); elseif( isset( $arrValues['ap_allocation_id'] ) ) $this->setApAllocationId( $arrValues['ap_allocation_id'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeId', trim( $arrValues['scheduled_charge_id'] ) ); elseif( isset( $arrValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrValues['scheduled_charge_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['dependent_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intDependentArTransactionId', trim( $arrValues['dependent_ar_transaction_id'] ) ); elseif( isset( $arrValues['dependent_ar_transaction_id'] ) ) $this->setDependentArTransactionId( $arrValues['dependent_ar_transaction_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmountDue', trim( $arrValues['transaction_amount_due'] ) ); elseif( isset( $arrValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrValues['transaction_amount_due'] );
		if( isset( $arrValues['initial_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltInitialAmountDue', trim( $arrValues['initial_amount_due'] ) ); elseif( isset( $arrValues['initial_amount_due'] ) ) $this->setInitialAmountDue( $arrValues['initial_amount_due'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( $arrValues['memo'] ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( $arrValues['memo'] );
		if( isset( $arrValues['internal_memo'] ) && $boolDirectSet ) $this->set( 'm_strInternalMemo', trim( $arrValues['internal_memo'] ) ); elseif( isset( $arrValues['internal_memo'] ) ) $this->setInternalMemo( $arrValues['internal_memo'] );
		if( isset( $arrValues['is_payment_in_kind'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaymentInKind', trim( stripcslashes( $arrValues['is_payment_in_kind'] ) ) ); elseif( isset( $arrValues['is_payment_in_kind'] ) ) $this->setIsPaymentInKind( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payment_in_kind'] ) : $arrValues['is_payment_in_kind'] );
		if( isset( $arrValues['is_deposit_credit'] ) && $boolDirectSet ) $this->set( 'm_boolIsDepositCredit', trim( stripcslashes( $arrValues['is_deposit_credit'] ) ) ); elseif( isset( $arrValues['is_deposit_credit'] ) ) $this->setIsDepositCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deposit_credit'] ) : $arrValues['is_deposit_credit'] );
		if( isset( $arrValues['is_reversal'] ) && $boolDirectSet ) $this->set( 'm_boolIsReversal', trim( stripcslashes( $arrValues['is_reversal'] ) ) ); elseif( isset( $arrValues['is_reversal'] ) ) $this->setIsReversal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reversal'] ) : $arrValues['is_reversal'] );
		if( isset( $arrValues['is_temporary'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemporary', trim( stripcslashes( $arrValues['is_temporary'] ) ) ); elseif( isset( $arrValues['is_temporary'] ) ) $this->setIsTemporary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_temporary'] ) : $arrValues['is_temporary'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['has_dependencies'] ) && $boolDirectSet ) $this->set( 'm_boolHasDependencies', trim( stripcslashes( $arrValues['has_dependencies'] ) ) ); elseif( isset( $arrValues['has_dependencies'] ) ) $this->setHasDependencies( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_dependencies'] ) : $arrValues['has_dependencies'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDimensionId', trim( $arrValues['gl_dimension_id'] ) ); elseif( isset( $arrValues['gl_dimension_id'] ) ) $this->setGlDimensionId( $arrValues['gl_dimension_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['ar_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intArInvoiceId', trim( $arrValues['ar_invoice_id'] ) ); elseif( isset( $arrValues['ar_invoice_id'] ) ) $this->setArInvoiceId( $arrValues['ar_invoice_id'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['is_deposited'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeposited', trim( stripcslashes( $arrValues['is_deposited'] ) ) ); elseif( isset( $arrValues['is_deposited'] ) ) $this->setIsDeposited( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deposited'] ) : $arrValues['is_deposited'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function sqlArCodeTypeId() {
		return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function sqlArOriginReferenceId() {
		return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
	}

	public function setArOriginObjectId( $intArOriginObjectId ) {
		$this->set( 'm_intArOriginObjectId', CStrings::strToIntDef( $intArOriginObjectId, NULL, false ) );
	}

	public function getArOriginObjectId() {
		return $this->m_intArOriginObjectId;
	}

	public function sqlArOriginObjectId() {
		return ( true == isset( $this->m_intArOriginObjectId ) ) ? ( string ) $this->m_intArOriginObjectId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setArProcessId( $intArProcessId ) {
		$this->set( 'm_intArProcessId', CStrings::strToIntDef( $intArProcessId, NULL, false ) );
	}

	public function getArProcessId() {
		return $this->m_intArProcessId;
	}

	public function sqlArProcessId() {
		return ( true == isset( $this->m_intArProcessId ) ) ? ( string ) $this->m_intArProcessId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setArPaymentSplitId( $intArPaymentSplitId ) {
		$this->set( 'm_intArPaymentSplitId', CStrings::strToIntDef( $intArPaymentSplitId, NULL, false ) );
	}

	public function getArPaymentSplitId() {
		return $this->m_intArPaymentSplitId;
	}

	public function sqlArPaymentSplitId() {
		return ( true == isset( $this->m_intArPaymentSplitId ) ) ? ( string ) $this->m_intArPaymentSplitId : 'NULL';
	}

	public function setAccrualDebitGlAccountId( $intAccrualDebitGlAccountId ) {
		$this->set( 'm_intAccrualDebitGlAccountId', CStrings::strToIntDef( $intAccrualDebitGlAccountId, NULL, false ) );
	}

	public function getAccrualDebitGlAccountId() {
		return $this->m_intAccrualDebitGlAccountId;
	}

	public function sqlAccrualDebitGlAccountId() {
		return ( true == isset( $this->m_intAccrualDebitGlAccountId ) ) ? ( string ) $this->m_intAccrualDebitGlAccountId : 'NULL';
	}

	public function setAccrualCreditGlAccountId( $intAccrualCreditGlAccountId ) {
		$this->set( 'm_intAccrualCreditGlAccountId', CStrings::strToIntDef( $intAccrualCreditGlAccountId, NULL, false ) );
	}

	public function getAccrualCreditGlAccountId() {
		return $this->m_intAccrualCreditGlAccountId;
	}

	public function sqlAccrualCreditGlAccountId() {
		return ( true == isset( $this->m_intAccrualCreditGlAccountId ) ) ? ( string ) $this->m_intAccrualCreditGlAccountId : 'NULL';
	}

	public function setCashDebitGlAccountId( $intCashDebitGlAccountId ) {
		$this->set( 'm_intCashDebitGlAccountId', CStrings::strToIntDef( $intCashDebitGlAccountId, NULL, false ) );
	}

	public function getCashDebitGlAccountId() {
		return $this->m_intCashDebitGlAccountId;
	}

	public function sqlCashDebitGlAccountId() {
		return ( true == isset( $this->m_intCashDebitGlAccountId ) ) ? ( string ) $this->m_intCashDebitGlAccountId : 'NULL';
	}

	public function setCashCreditGlAccountId( $intCashCreditGlAccountId ) {
		$this->set( 'm_intCashCreditGlAccountId', CStrings::strToIntDef( $intCashCreditGlAccountId, NULL, false ) );
	}

	public function getCashCreditGlAccountId() {
		return $this->m_intCashCreditGlAccountId;
	}

	public function sqlCashCreditGlAccountId() {
		return ( true == isset( $this->m_intCashCreditGlAccountId ) ) ? ( string ) $this->m_intCashCreditGlAccountId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setApAllocationId( $intApAllocationId ) {
		$this->set( 'm_intApAllocationId', CStrings::strToIntDef( $intApAllocationId, NULL, false ) );
	}

	public function getApAllocationId() {
		return $this->m_intApAllocationId;
	}

	public function sqlApAllocationId() {
		return ( true == isset( $this->m_intApAllocationId ) ) ? ( string ) $this->m_intApAllocationId : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->set( 'm_intScheduledChargeId', CStrings::strToIntDef( $intScheduledChargeId, NULL, false ) );
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function sqlScheduledChargeId() {
		return ( true == isset( $this->m_intScheduledChargeId ) ) ? ( string ) $this->m_intScheduledChargeId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setDependentArTransactionId( $intDependentArTransactionId ) {
		$this->set( 'm_intDependentArTransactionId', CStrings::strToIntDef( $intDependentArTransactionId, NULL, false ) );
	}

	public function getDependentArTransactionId() {
		return $this->m_intDependentArTransactionId;
	}

	public function sqlDependentArTransactionId() {
		return ( true == isset( $this->m_intDependentArTransactionId ) ) ? ( string ) $this->m_intDependentArTransactionId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->set( 'm_fltTransactionAmountDue', CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 2 ) );
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function sqlTransactionAmountDue() {
		return ( true == isset( $this->m_fltTransactionAmountDue ) ) ? ( string ) $this->m_fltTransactionAmountDue : '0';
	}

	public function setInitialAmountDue( $fltInitialAmountDue ) {
		$this->set( 'm_fltInitialAmountDue', CStrings::strToFloatDef( $fltInitialAmountDue, NULL, false, 2 ) );
	}

	public function getInitialAmountDue() {
		return $this->m_fltInitialAmountDue;
	}

	public function sqlInitialAmountDue() {
		return ( true == isset( $this->m_fltInitialAmountDue ) ) ? ( string ) $this->m_fltInitialAmountDue : '0';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 2000, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMemo ) : '\'' . addslashes( $this->m_strMemo ) . '\'' ) : 'NULL';
	}

	public function setInternalMemo( $strInternalMemo ) {
		$this->set( 'm_strInternalMemo', CStrings::strTrimDef( $strInternalMemo, 2000, NULL, true ) );
	}

	public function getInternalMemo() {
		return $this->m_strInternalMemo;
	}

	public function sqlInternalMemo() {
		return ( true == isset( $this->m_strInternalMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInternalMemo ) : '\'' . addslashes( $this->m_strInternalMemo ) . '\'' ) : 'NULL';
	}

	public function setIsPaymentInKind( $boolIsPaymentInKind ) {
		$this->set( 'm_boolIsPaymentInKind', CStrings::strToBool( $boolIsPaymentInKind ) );
	}

	public function getIsPaymentInKind() {
		return $this->m_boolIsPaymentInKind;
	}

	public function sqlIsPaymentInKind() {
		return ( true == isset( $this->m_boolIsPaymentInKind ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentInKind ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDepositCredit( $boolIsDepositCredit ) {
		$this->set( 'm_boolIsDepositCredit', CStrings::strToBool( $boolIsDepositCredit ) );
	}

	public function getIsDepositCredit() {
		return $this->m_boolIsDepositCredit;
	}

	public function sqlIsDepositCredit() {
		return ( true == isset( $this->m_boolIsDepositCredit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDepositCredit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReversal( $boolIsReversal ) {
		$this->set( 'm_boolIsReversal', CStrings::strToBool( $boolIsReversal ) );
	}

	public function getIsReversal() {
		return $this->m_boolIsReversal;
	}

	public function sqlIsReversal() {
		return ( true == isset( $this->m_boolIsReversal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReversal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemporary( $boolIsTemporary ) {
		$this->set( 'm_boolIsTemporary', CStrings::strToBool( $boolIsTemporary ) );
	}

	public function getIsTemporary() {
		return $this->m_boolIsTemporary;
	}

	public function sqlIsTemporary() {
		return ( true == isset( $this->m_boolIsTemporary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemporary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasDependencies( $boolHasDependencies ) {
		$this->set( 'm_boolHasDependencies', CStrings::strToBool( $boolHasDependencies ) );
	}

	public function getHasDependencies() {
		return $this->m_boolHasDependencies;
	}

	public function sqlHasDependencies() {
		return ( true == isset( $this->m_boolHasDependencies ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasDependencies ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGlDimensionId( $intGlDimensionId ) {
		$this->set( 'm_intGlDimensionId', CStrings::strToIntDef( $intGlDimensionId, NULL, false ) );
	}

	public function getGlDimensionId() {
		return $this->m_intGlDimensionId;
	}

	public function sqlGlDimensionId() {
		return ( true == isset( $this->m_intGlDimensionId ) ) ? ( string ) $this->m_intGlDimensionId : 'NULL';
	}

	public function setArInvoiceId( $intArInvoiceId ) {
		$this->set( 'm_intArInvoiceId', CStrings::strToIntDef( $intArInvoiceId, NULL, false ) );
	}

	public function getArInvoiceId() {
		return $this->m_intArInvoiceId;
	}

	public function sqlArInvoiceId() {
		return ( true == isset( $this->m_intArInvoiceId ) ) ? ( string ) $this->m_intArInvoiceId : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setIsDeposited( $boolIsDeposited ) {
		$this->set( 'm_boolIsDeposited', CStrings::strToBool( $boolIsDeposited ) );
	}

	public function getIsDeposited() {
		return $this->m_boolIsDeposited;
	}

	public function sqlIsDeposited() {
		return ( true == isset( $this->m_boolIsDeposited ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeposited ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, lease_interval_id, customer_id, ar_code_type_id, ar_code_id, ar_origin_id, ar_origin_reference_id, ar_origin_object_id, ar_trigger_id, ar_process_id, ar_payment_id, ar_payment_split_id, accrual_debit_gl_account_id, accrual_credit_gl_account_id, cash_debit_gl_account_id, cash_credit_gl_account_id, ar_transaction_id, ap_allocation_id, ap_detail_id, scheduled_charge_id, period_id, dependent_ar_transaction_id, remote_primary_key, transaction_datetime, transaction_amount, transaction_amount_due, initial_amount_due, reporting_post_date, post_date, post_month, memo, internal_memo, is_payment_in_kind, is_deposit_credit, is_reversal, is_temporary, is_posted, is_initial_import, has_dependencies, is_deleted, updated_by, updated_on, created_by, created_on, gl_dimension_id, details, ar_invoice_id, competitor_id, ps_product_id, is_deposited )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlArCodeTypeId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlArOriginId() . ', ' .
						$this->sqlArOriginReferenceId() . ', ' .
						$this->sqlArOriginObjectId() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlArProcessId() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlArPaymentSplitId() . ', ' .
						$this->sqlAccrualDebitGlAccountId() . ', ' .
						$this->sqlAccrualCreditGlAccountId() . ', ' .
						$this->sqlCashDebitGlAccountId() . ', ' .
						$this->sqlCashCreditGlAccountId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlApAllocationId() . ', ' .
						$this->sqlApDetailId() . ', ' .
						$this->sqlScheduledChargeId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlDependentArTransactionId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlTransactionAmountDue() . ', ' .
						$this->sqlInitialAmountDue() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlInternalMemo() . ', ' .
						$this->sqlIsPaymentInKind() . ', ' .
						$this->sqlIsDepositCredit() . ', ' .
						$this->sqlIsReversal() . ', ' .
						$this->sqlIsTemporary() . ', ' .
						$this->sqlIsPosted() . ', ' .
						$this->sqlIsInitialImport() . ', ' .
						$this->sqlHasDependencies() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlGlDimensionId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlArInvoiceId() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlIsDeposited() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId(). ',' ; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId(). ',' ; } elseif( true == array_key_exists( 'ArOriginObjectId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_process_id = ' . $this->sqlArProcessId(). ',' ; } elseif( true == array_key_exists( 'ArProcessId', $this->getChangedColumns() ) ) { $strSql .= ' ar_process_id = ' . $this->sqlArProcessId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_split_id = ' . $this->sqlArPaymentSplitId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentSplitId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_split_id = ' . $this->sqlArPaymentSplitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_allocation_id = ' . $this->sqlApAllocationId(). ',' ; } elseif( true == array_key_exists( 'ApAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_allocation_id = ' . $this->sqlApAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dependent_ar_transaction_id = ' . $this->sqlDependentArTransactionId(). ',' ; } elseif( true == array_key_exists( 'DependentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' dependent_ar_transaction_id = ' . $this->sqlDependentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue(). ',' ; } elseif( true == array_key_exists( 'TransactionAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_amount_due = ' . $this->sqlInitialAmountDue(). ',' ; } elseif( true == array_key_exists( 'InitialAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' initial_amount_due = ' . $this->sqlInitialAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_memo = ' . $this->sqlInternalMemo(). ',' ; } elseif( true == array_key_exists( 'InternalMemo', $this->getChangedColumns() ) ) { $strSql .= ' internal_memo = ' . $this->sqlInternalMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_payment_in_kind = ' . $this->sqlIsPaymentInKind(). ',' ; } elseif( true == array_key_exists( 'IsPaymentInKind', $this->getChangedColumns() ) ) { $strSql .= ' is_payment_in_kind = ' . $this->sqlIsPaymentInKind() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deposit_credit = ' . $this->sqlIsDepositCredit(). ',' ; } elseif( true == array_key_exists( 'IsDepositCredit', $this->getChangedColumns() ) ) { $strSql .= ' is_deposit_credit = ' . $this->sqlIsDepositCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reversal = ' . $this->sqlIsReversal(). ',' ; } elseif( true == array_key_exists( 'IsReversal', $this->getChangedColumns() ) ) { $strSql .= ' is_reversal = ' . $this->sqlIsReversal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_temporary = ' . $this->sqlIsTemporary(). ',' ; } elseif( true == array_key_exists( 'IsTemporary', $this->getChangedColumns() ) ) { $strSql .= ' is_temporary = ' . $this->sqlIsTemporary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted(). ',' ; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport(). ',' ; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_dependencies = ' . $this->sqlHasDependencies(). ',' ; } elseif( true == array_key_exists( 'HasDependencies', $this->getChangedColumns() ) ) { $strSql .= ' has_dependencies = ' . $this->sqlHasDependencies() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId(). ',' ; } elseif( true == array_key_exists( 'GlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_invoice_id = ' . $this->sqlArInvoiceId(). ',' ; } elseif( true == array_key_exists( 'ArInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_invoice_id = ' . $this->sqlArInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deposited = ' . $this->sqlIsDeposited(). ',' ; } elseif( true == array_key_exists( 'IsDeposited', $this->getChangedColumns() ) ) { $strSql .= ' is_deposited = ' . $this->sqlIsDeposited() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'customer_id' => $this->getCustomerId(),
			'ar_code_type_id' => $this->getArCodeTypeId(),
			'ar_code_id' => $this->getArCodeId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_origin_reference_id' => $this->getArOriginReferenceId(),
			'ar_origin_object_id' => $this->getArOriginObjectId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'ar_process_id' => $this->getArProcessId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'ar_payment_split_id' => $this->getArPaymentSplitId(),
			'accrual_debit_gl_account_id' => $this->getAccrualDebitGlAccountId(),
			'accrual_credit_gl_account_id' => $this->getAccrualCreditGlAccountId(),
			'cash_debit_gl_account_id' => $this->getCashDebitGlAccountId(),
			'cash_credit_gl_account_id' => $this->getCashCreditGlAccountId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'ap_allocation_id' => $this->getApAllocationId(),
			'ap_detail_id' => $this->getApDetailId(),
			'scheduled_charge_id' => $this->getScheduledChargeId(),
			'period_id' => $this->getPeriodId(),
			'dependent_ar_transaction_id' => $this->getDependentArTransactionId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_amount_due' => $this->getTransactionAmountDue(),
			'initial_amount_due' => $this->getInitialAmountDue(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'memo' => $this->getMemo(),
			'internal_memo' => $this->getInternalMemo(),
			'is_payment_in_kind' => $this->getIsPaymentInKind(),
			'is_deposit_credit' => $this->getIsDepositCredit(),
			'is_reversal' => $this->getIsReversal(),
			'is_temporary' => $this->getIsTemporary(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'has_dependencies' => $this->getHasDependencies(),
			'is_deleted' => $this->getIsDeleted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'gl_dimension_id' => $this->getGlDimensionId(),
			'details' => $this->getDetails(),
			'ar_invoice_id' => $this->getArInvoiceId(),
			'competitor_id' => $this->getCompetitorId(),
			'ps_product_id' => $this->getPsProductId(),
			'is_deposited' => $this->getIsDeposited()
		);
	}

}
?>