<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlDetails extends CEosPluralBase {

	/**
	 * @return CGlDetail[]
	 */
	public static function fetchGlDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlDetail
	 */
	public static function fetchGlDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlDetail::class, $objDatabase );
	}

	public static function fetchGlDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_details', $objDatabase );
	}

	public static function fetchGlDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlDetail( sprintf( 'SELECT * FROM gl_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE gl_header_id = %d AND cid = %d', $intGlHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE gl_transaction_type_id = %d AND cid = %d', $intGlTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByAccrualGlAccountIdByCid( $intAccrualGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE accrual_gl_account_id = %d AND cid = %d', $intAccrualGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByCashGlAccountIdByCid( $intCashGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE cash_gl_account_id = %d AND cid = %d', $intCashGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByTemplateAmountTypeIdByCid( $intTemplateAmountTypeId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE template_amount_type_id = %d AND cid = %d', $intTemplateAmountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByTemplateBalanceFromIdByCid( $intTemplateBalanceFromId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE template_balance_from_id = %d AND cid = %d', $intTemplateBalanceFromId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE gl_reconciliation_id = %d AND cid = %d', $intGlReconciliationId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByApDetailIdByCid( $intApDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE ap_detail_id = %d AND cid = %d', $intApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByOffsettingGlDetailIdByCid( $intOffsettingGlDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE offsetting_gl_detail_id = %d AND cid = %d', $intOffsettingGlDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByCompanyDepartmentIdByCid( $intCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE company_department_id = %d AND cid = %d', $intCompanyDepartmentId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByGlDimensionIdByCid( $intGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE gl_dimension_id = %d AND cid = %d', $intGlDimensionId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE ap_code_id = %d AND cid = %d', $intApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE job_phase_id = %d AND cid = %d', $intJobPhaseId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE ap_contract_id = %d AND cid = %d', $intApContractId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE asset_id = %d AND cid = %d', $intAssetId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByReclassGlDetailIdByCid( $intReclassGlDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE reclass_gl_detail_id = %d AND cid = %d', $intReclassGlDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchGlDetails( sprintf( 'SELECT * FROM gl_details WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

}
?>