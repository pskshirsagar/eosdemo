<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestTypes
 * Do not add any new functions to this class.
 */

class CBaseGuestTypes extends CEosPluralBase {

	/**
	 * @return CGuestType[]
	 */
	public static function fetchGuestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGuestType::class, $objDatabase );
	}

	/**
	 * @return CGuestType
	 */
	public static function fetchGuestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGuestType::class, $objDatabase );
	}

	public static function fetchGuestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'guest_types', $objDatabase );
	}

	public static function fetchGuestTypeById( $intId, $objDatabase ) {
		return self::fetchGuestType( sprintf( 'SELECT * FROM guest_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>