<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmsEnrollments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSmsEnrollments extends CEosPluralBase {

	/**
	 * @return CSmsEnrollment[]
	 */
	public static function fetchSmsEnrollments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSmsEnrollment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSmsEnrollment
	 */
	public static function fetchSmsEnrollment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSmsEnrollment', $objDatabase );
	}

	public static function fetchSmsEnrollmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sms_enrollments', $objDatabase );
	}

	public static function fetchSmsEnrollmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSmsEnrollment( sprintf( 'SELECT * FROM sms_enrollments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSmsEnrollmentsByCid( $intCid, $objDatabase ) {
		return self::fetchSmsEnrollments( sprintf( 'SELECT * FROM sms_enrollments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSmsEnrollmentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchSmsEnrollments( sprintf( 'SELECT * FROM sms_enrollments WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSmsEnrollmentsByMessageTypeIdByCid( $intMessageTypeId, $intCid, $objDatabase ) {
		return self::fetchSmsEnrollments( sprintf( 'SELECT * FROM sms_enrollments WHERE message_type_id = %d AND cid = %d', ( int ) $intMessageTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>