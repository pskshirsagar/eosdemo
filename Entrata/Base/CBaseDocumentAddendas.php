<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAddendas
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentAddendas extends CEosPluralBase {

	/**
	 * @return CDocumentAddenda[]
	 */
	public static function fetchDocumentAddendas( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDocumentAddenda::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocumentAddenda
	 */
	public static function fetchDocumentAddenda( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentAddenda::class, $objDatabase );
	}

	public static function fetchDocumentAddendaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_addendas', $objDatabase );
	}

	public static function fetchDocumentAddendaByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocumentAddenda( sprintf( 'SELECT * FROM document_addendas WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentAddendasByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentAddendas( sprintf( 'SELECT * FROM document_addendas WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchDocumentAddendasByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentAddendas( sprintf( 'SELECT * FROM document_addendas WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentAddendasByMasterDocumentIdByCid( $intMasterDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentAddendas( sprintf( 'SELECT * FROM document_addendas WHERE master_document_id = %d AND cid = %d', $intMasterDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentAddendasByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchDocumentAddendas( sprintf( 'SELECT * FROM document_addendas WHERE company_application_id = %d AND cid = %d', $intCompanyApplicationId, $intCid ), $objDatabase );
	}

}
?>