<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryAssistanceTypes
 * Do not add any new functions to this class.
 */

class CBaseMilitaryAssistanceTypes extends CEosPluralBase {

	/**
	 * @return CMilitaryAssistanceType[]
	 */
	public static function fetchMilitaryAssistanceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryAssistanceType::class, $objDatabase );
	}

	/**
	 * @return CMilitaryAssistanceType
	 */
	public static function fetchMilitaryAssistanceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryAssistanceType::class, $objDatabase );
	}

	public static function fetchMilitaryAssistanceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_assistance_types', $objDatabase );
	}

	public static function fetchMilitaryAssistanceTypeById( $intId, $objDatabase ) {
		return self::fetchMilitaryAssistanceType( sprintf( 'SELECT * FROM military_assistance_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>