<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInScheduleAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMoveInScheduleAssociations extends CEosPluralBase {

	/**
	 * @return CMoveInScheduleAssociation[]
	 */
	public static function fetchMoveInScheduleAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMoveInScheduleAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMoveInScheduleAssociation
	 */
	public static function fetchMoveInScheduleAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoveInScheduleAssociation::class, $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'move_in_schedule_associations', $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociation( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByMoveInScheduleIdByCid( $intMoveInScheduleId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE move_in_schedule_id = %d AND cid = %d', ( int ) $intMoveInScheduleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE property_floor_id = %d AND cid = %d', ( int ) $intPropertyFloorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchMoveInScheduleAssociations( sprintf( 'SELECT * FROM move_in_schedule_associations WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

}
?>