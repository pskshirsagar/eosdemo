<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportVersions
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportVersions extends CEosPluralBase {

	public static function fetchDefaultReportVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultReportVersion', $objDatabase );
	}

	public static function fetchDefaultReportVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultReportVersion', $objDatabase );
	}

	public static function fetchDefaultReportVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_versions', $objDatabase );
	}

	public static function fetchDefaultReportVersionById( $intId, $objDatabase ) {
		return self::fetchDefaultReportVersion( sprintf( 'SELECT * FROM default_report_versions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultReportVersionsByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		return self::fetchDefaultReportVersions( sprintf( 'SELECT * FROM default_report_versions WHERE default_report_id = %d', ( int ) $intDefaultReportId ), $objDatabase );
	}

}
?>