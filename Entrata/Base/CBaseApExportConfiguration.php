<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApExportConfiguration extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_export_configurations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApExportBatchId;
	protected $m_strPostMonth;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_boolIsConsolidatedProperties;
	protected $m_boolIncludePendingTransactions;
	protected $m_boolIncludeAllUnexportedTransactions;
	protected $m_boolUseTransactionDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsConsolidatedProperties = true;
		$this->m_boolIncludePendingTransactions = false;
		$this->m_boolIncludeAllUnexportedTransactions = true;
		$this->m_boolUseTransactionDate = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intApExportBatchId', trim( $arrValues['ap_export_batch_id'] ) ); elseif( isset( $arrValues['ap_export_batch_id'] ) ) $this->setApExportBatchId( $arrValues['ap_export_batch_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_consolidated_properties'] ) && $boolDirectSet ) $this->set( 'm_boolIsConsolidatedProperties', trim( stripcslashes( $arrValues['is_consolidated_properties'] ) ) ); elseif( isset( $arrValues['is_consolidated_properties'] ) ) $this->setIsConsolidatedProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_consolidated_properties'] ) : $arrValues['is_consolidated_properties'] );
		if( isset( $arrValues['include_pending_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIncludePendingTransactions', trim( stripcslashes( $arrValues['include_pending_transactions'] ) ) ); elseif( isset( $arrValues['include_pending_transactions'] ) ) $this->setIncludePendingTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_pending_transactions'] ) : $arrValues['include_pending_transactions'] );
		if( isset( $arrValues['include_all_unexported_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeAllUnexportedTransactions', trim( stripcslashes( $arrValues['include_all_unexported_transactions'] ) ) ); elseif( isset( $arrValues['include_all_unexported_transactions'] ) ) $this->setIncludeAllUnexportedTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_all_unexported_transactions'] ) : $arrValues['include_all_unexported_transactions'] );
		if( isset( $arrValues['use_transaction_date'] ) && $boolDirectSet ) $this->set( 'm_boolUseTransactionDate', trim( stripcslashes( $arrValues['use_transaction_date'] ) ) ); elseif( isset( $arrValues['use_transaction_date'] ) ) $this->setUseTransactionDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_transaction_date'] ) : $arrValues['use_transaction_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApExportBatchId( $intApExportBatchId ) {
		$this->set( 'm_intApExportBatchId', CStrings::strToIntDef( $intApExportBatchId, NULL, false ) );
	}

	public function getApExportBatchId() {
		return $this->m_intApExportBatchId;
	}

	public function sqlApExportBatchId() {
		return ( true == isset( $this->m_intApExportBatchId ) ) ? ( string ) $this->m_intApExportBatchId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setIsConsolidatedProperties( $boolIsConsolidatedProperties ) {
		$this->set( 'm_boolIsConsolidatedProperties', CStrings::strToBool( $boolIsConsolidatedProperties ) );
	}

	public function getIsConsolidatedProperties() {
		return $this->m_boolIsConsolidatedProperties;
	}

	public function sqlIsConsolidatedProperties() {
		return ( true == isset( $this->m_boolIsConsolidatedProperties ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConsolidatedProperties ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludePendingTransactions( $boolIncludePendingTransactions ) {
		$this->set( 'm_boolIncludePendingTransactions', CStrings::strToBool( $boolIncludePendingTransactions ) );
	}

	public function getIncludePendingTransactions() {
		return $this->m_boolIncludePendingTransactions;
	}

	public function sqlIncludePendingTransactions() {
		return ( true == isset( $this->m_boolIncludePendingTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludePendingTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeAllUnexportedTransactions( $boolIncludeAllUnexportedTransactions ) {
		$this->set( 'm_boolIncludeAllUnexportedTransactions', CStrings::strToBool( $boolIncludeAllUnexportedTransactions ) );
	}

	public function getIncludeAllUnexportedTransactions() {
		return $this->m_boolIncludeAllUnexportedTransactions;
	}

	public function sqlIncludeAllUnexportedTransactions() {
		return ( true == isset( $this->m_boolIncludeAllUnexportedTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeAllUnexportedTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseTransactionDate( $boolUseTransactionDate ) {
		$this->set( 'm_boolUseTransactionDate', CStrings::strToBool( $boolUseTransactionDate ) );
	}

	public function getUseTransactionDate() {
		return $this->m_boolUseTransactionDate;
	}

	public function sqlUseTransactionDate() {
		return ( true == isset( $this->m_boolUseTransactionDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseTransactionDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_export_batch_id, post_month, start_date, end_date, is_consolidated_properties, include_pending_transactions, include_all_unexported_transactions, use_transaction_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApExportBatchId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlIsConsolidatedProperties() . ', ' .
 						$this->sqlIncludePendingTransactions() . ', ' .
 						$this->sqlIncludeAllUnexportedTransactions() . ', ' .
 						$this->sqlUseTransactionDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_export_batch_id = ' . $this->sqlApExportBatchId() . ','; } elseif( true == array_key_exists( 'ApExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ap_export_batch_id = ' . $this->sqlApExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_consolidated_properties = ' . $this->sqlIsConsolidatedProperties() . ','; } elseif( true == array_key_exists( 'IsConsolidatedProperties', $this->getChangedColumns() ) ) { $strSql .= ' is_consolidated_properties = ' . $this->sqlIsConsolidatedProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_pending_transactions = ' . $this->sqlIncludePendingTransactions() . ','; } elseif( true == array_key_exists( 'IncludePendingTransactions', $this->getChangedColumns() ) ) { $strSql .= ' include_pending_transactions = ' . $this->sqlIncludePendingTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_all_unexported_transactions = ' . $this->sqlIncludeAllUnexportedTransactions() . ','; } elseif( true == array_key_exists( 'IncludeAllUnexportedTransactions', $this->getChangedColumns() ) ) { $strSql .= ' include_all_unexported_transactions = ' . $this->sqlIncludeAllUnexportedTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_transaction_date = ' . $this->sqlUseTransactionDate() . ','; } elseif( true == array_key_exists( 'UseTransactionDate', $this->getChangedColumns() ) ) { $strSql .= ' use_transaction_date = ' . $this->sqlUseTransactionDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_export_batch_id' => $this->getApExportBatchId(),
			'post_month' => $this->getPostMonth(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_consolidated_properties' => $this->getIsConsolidatedProperties(),
			'include_pending_transactions' => $this->getIncludePendingTransactions(),
			'include_all_unexported_transactions' => $this->getIncludeAllUnexportedTransactions(),
			'use_transaction_date' => $this->getUseTransactionDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>