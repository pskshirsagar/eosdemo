<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceLocations extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceLocation[]
	 */
	public static function fetchPropertyMaintenanceLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMaintenanceLocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceLocation
	 */
	public static function fetchPropertyMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMaintenanceLocation', $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_locations', $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocation( sprintf( 'SELECT * FROM property_maintenance_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocations( sprintf( 'SELECT * FROM property_maintenance_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocations( sprintf( 'SELECT * FROM property_maintenance_locations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceLocationsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceLocations( sprintf( 'SELECT * FROM property_maintenance_locations WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>