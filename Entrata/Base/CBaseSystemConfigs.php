<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemConfigs
 * Do not add any new functions to this class.
 */

class CBaseSystemConfigs extends CEosPluralBase {

	/**
	 * @return CSystemConfig[]
	 */
	public static function fetchSystemConfigs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemConfig', $objDatabase );
	}

	/**
	 * @return CSystemConfig
	 */
	public static function fetchSystemConfig( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemConfig', $objDatabase );
	}

	public static function fetchSystemConfigCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_configs', $objDatabase );
	}

	public static function fetchSystemConfigById( $intId, $objDatabase ) {
		return self::fetchSystemConfig( sprintf( 'SELECT * FROM system_configs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>