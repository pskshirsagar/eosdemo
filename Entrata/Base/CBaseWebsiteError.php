<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteError extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.website_errors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteErrorTypeId;
	protected $m_strReferralSourceUrl;
	protected $m_strUrl;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteErrorTypeId', trim( $arrValues['website_error_type_id'] ) ); elseif( isset( $arrValues['website_error_type_id'] ) ) $this->setWebsiteErrorTypeId( $arrValues['website_error_type_id'] );
		if( isset( $arrValues['referral_source_url'] ) && $boolDirectSet ) $this->set( 'm_strReferralSourceUrl', trim( stripcslashes( $arrValues['referral_source_url'] ) ) ); elseif( isset( $arrValues['referral_source_url'] ) ) $this->setReferralSourceUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['referral_source_url'] ) : $arrValues['referral_source_url'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteErrorTypeId( $intWebsiteErrorTypeId ) {
		$this->set( 'm_intWebsiteErrorTypeId', CStrings::strToIntDef( $intWebsiteErrorTypeId, NULL, false ) );
	}

	public function getWebsiteErrorTypeId() {
		return $this->m_intWebsiteErrorTypeId;
	}

	public function sqlWebsiteErrorTypeId() {
		return ( true == isset( $this->m_intWebsiteErrorTypeId ) ) ? ( string ) $this->m_intWebsiteErrorTypeId : 'NULL';
	}

	public function setReferralSourceUrl( $strReferralSourceUrl ) {
		$this->set( 'm_strReferralSourceUrl', CStrings::strTrimDef( $strReferralSourceUrl, 4096, NULL, true ) );
	}

	public function getReferralSourceUrl() {
		return $this->m_strReferralSourceUrl;
	}

	public function sqlReferralSourceUrl() {
		return ( true == isset( $this->m_strReferralSourceUrl ) ) ? '\'' . addslashes( $this->m_strReferralSourceUrl ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 4096, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_error_type_id, referral_source_url, url, details, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlWebsiteErrorTypeId() . ', ' .
						$this->sqlReferralSourceUrl() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_error_type_id = ' . $this->sqlWebsiteErrorTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsiteErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_error_type_id = ' . $this->sqlWebsiteErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referral_source_url = ' . $this->sqlReferralSourceUrl(). ',' ; } elseif( true == array_key_exists( 'ReferralSourceUrl', $this->getChangedColumns() ) ) { $strSql .= ' referral_source_url = ' . $this->sqlReferralSourceUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_error_type_id' => $this->getWebsiteErrorTypeId(),
			'referral_source_url' => $this->getReferralSourceUrl(),
			'url' => $this->getUrl(),
			'details' => $this->getDetails(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>