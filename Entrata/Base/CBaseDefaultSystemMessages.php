<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultSystemMessages
 * Do not add any new functions to this class.
 */

class CBaseDefaultSystemMessages extends CEosPluralBase {

	/**
	 * @return CDefaultSystemMessage[]
	 */
	public static function fetchDefaultSystemMessages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultSystemMessage::class, $objDatabase );
	}

	/**
	 * @return CDefaultSystemMessage
	 */
	public static function fetchDefaultSystemMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultSystemMessage::class, $objDatabase );
	}

	public static function fetchDefaultSystemMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_system_messages', $objDatabase );
	}

	public static function fetchDefaultSystemMessageById( $intId, $objDatabase ) {
		return self::fetchDefaultSystemMessage( sprintf( 'SELECT * FROM default_system_messages WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultSystemMessagesBySystemMessageTypeId( $intSystemMessageTypeId, $objDatabase ) {
		return self::fetchDefaultSystemMessages( sprintf( 'SELECT * FROM default_system_messages WHERE system_message_type_id = %d', $intSystemMessageTypeId ), $objDatabase );
	}

	public static function fetchDefaultSystemMessagesBySystemMessageAudienceId( $intSystemMessageAudienceId, $objDatabase ) {
		return self::fetchDefaultSystemMessages( sprintf( 'SELECT * FROM default_system_messages WHERE system_message_audience_id = %d', $intSystemMessageAudienceId ), $objDatabase );
	}

	public static function fetchDefaultSystemMessagesBySystemMessageCategoryId( $intSystemMessageCategoryId, $objDatabase ) {
		return self::fetchDefaultSystemMessages( sprintf( 'SELECT * FROM default_system_messages WHERE system_message_category_id = %d', $intSystemMessageCategoryId ), $objDatabase );
	}

	public static function fetchDefaultSystemMessagesBySystemMessageTemplateId( $intSystemMessageTemplateId, $objDatabase ) {
		return self::fetchDefaultSystemMessages( sprintf( 'SELECT * FROM default_system_messages WHERE system_message_template_id = %d', $intSystemMessageTemplateId ), $objDatabase );
	}

}
?>