<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataChanges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataChanges extends CEosPluralBase {

	/**
	 * @return CDataChange[]
	 */
	public static function fetchDataChanges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDataChange', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDataChange
	 */
	public static function fetchDataChange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDataChange', $objDatabase );
	}

	public static function fetchDataChangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_changes', $objDatabase );
	}

	public static function fetchDataChangeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDataChange( sprintf( 'SELECT * FROM data_changes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataChangesByCid( $intCid, $objDatabase ) {
		return self::fetchDataChanges( sprintf( 'SELECT * FROM data_changes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataChangesByDataChangeTypeIdByCid( $intDataChangeTypeId, $intCid, $objDatabase ) {
		return self::fetchDataChanges( sprintf( 'SELECT * FROM data_changes WHERE data_change_type_id = %d AND cid = %d', ( int ) $intDataChangeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataChangesByOldReferenceIdByCid( $intOldReferenceId, $intCid, $objDatabase ) {
		return self::fetchDataChanges( sprintf( 'SELECT * FROM data_changes WHERE old_reference_id = %d AND cid = %d', ( int ) $intOldReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataChangesByNewReferenceIdByCid( $intNewReferenceId, $intCid, $objDatabase ) {
		return self::fetchDataChanges( sprintf( 'SELECT * FROM data_changes WHERE new_reference_id = %d AND cid = %d', ( int ) $intNewReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>