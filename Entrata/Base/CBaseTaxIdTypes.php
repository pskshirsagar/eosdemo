<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxIdTypes
 * Do not add any new functions to this class.
 */

class CBaseTaxIdTypes extends CEosPluralBase {

	/**
	 * @return CTaxIdType[]
	 */
	public static function fetchTaxIdTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaxIdType::class, $objDatabase );
	}

	/**
	 * @return CTaxIdType
	 */
	public static function fetchTaxIdType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxIdType::class, $objDatabase );
	}

	public static function fetchTaxIdTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tax_id_types', $objDatabase );
	}

	public static function fetchTaxIdTypeById( $intId, $objDatabase ) {
		return self::fetchTaxIdType( sprintf( 'SELECT * FROM tax_id_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>