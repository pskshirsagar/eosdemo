<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateChangeTypes
 * Do not add any new functions to this class.
 */

class CBaseCorporateChangeTypes extends CEosPluralBase {

	/**
	 * @return CCorporateChangeType[]
	 */
	public static function fetchCorporateChangeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCorporateChangeType::class, $objDatabase );
	}

	/**
	 * @return CCorporateChangeType
	 */
	public static function fetchCorporateChangeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCorporateChangeType::class, $objDatabase );
	}

	public static function fetchCorporateChangeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_change_types', $objDatabase );
	}

	public static function fetchCorporateChangeTypeById( $intId, $objDatabase ) {
		return self::fetchCorporateChangeType( sprintf( 'SELECT * FROM corporate_change_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>