<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.website_documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intDocumentId;
	protected $m_intShowInPrimaryNav;
	protected $m_intShowInSecondaryNav;
	protected $m_intShowInTertiaryNav;
	protected $m_intShowInMobilePortal;
	protected $m_intIsPublished;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intWebsitePageId;

	public function __construct() {
		parent::__construct();

		$this->m_intShowInPrimaryNav = '1';
		$this->m_intShowInSecondaryNav = '0';
		$this->m_intShowInTertiaryNav = '0';
		$this->m_intShowInMobilePortal = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['show_in_primary_nav'] ) && $boolDirectSet ) $this->set( 'm_intShowInPrimaryNav', trim( $arrValues['show_in_primary_nav'] ) ); elseif( isset( $arrValues['show_in_primary_nav'] ) ) $this->setShowInPrimaryNav( $arrValues['show_in_primary_nav'] );
		if( isset( $arrValues['show_in_secondary_nav'] ) && $boolDirectSet ) $this->set( 'm_intShowInSecondaryNav', trim( $arrValues['show_in_secondary_nav'] ) ); elseif( isset( $arrValues['show_in_secondary_nav'] ) ) $this->setShowInSecondaryNav( $arrValues['show_in_secondary_nav'] );
		if( isset( $arrValues['show_in_tertiary_nav'] ) && $boolDirectSet ) $this->set( 'm_intShowInTertiaryNav', trim( $arrValues['show_in_tertiary_nav'] ) ); elseif( isset( $arrValues['show_in_tertiary_nav'] ) ) $this->setShowInTertiaryNav( $arrValues['show_in_tertiary_nav'] );
		if( isset( $arrValues['show_in_mobile_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowInMobilePortal', trim( $arrValues['show_in_mobile_portal'] ) ); elseif( isset( $arrValues['show_in_mobile_portal'] ) ) $this->setShowInMobilePortal( $arrValues['show_in_mobile_portal'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['website_page_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsitePageId', trim( $arrValues['website_page_id'] ) ); elseif( isset( $arrValues['website_page_id'] ) ) $this->setWebsitePageId( $arrValues['website_page_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setShowInPrimaryNav( $intShowInPrimaryNav ) {
		$this->set( 'm_intShowInPrimaryNav', CStrings::strToIntDef( $intShowInPrimaryNav, NULL, false ) );
	}

	public function getShowInPrimaryNav() {
		return $this->m_intShowInPrimaryNav;
	}

	public function sqlShowInPrimaryNav() {
		return ( true == isset( $this->m_intShowInPrimaryNav ) ) ? ( string ) $this->m_intShowInPrimaryNav : '1';
	}

	public function setShowInSecondaryNav( $intShowInSecondaryNav ) {
		$this->set( 'm_intShowInSecondaryNav', CStrings::strToIntDef( $intShowInSecondaryNav, NULL, false ) );
	}

	public function getShowInSecondaryNav() {
		return $this->m_intShowInSecondaryNav;
	}

	public function sqlShowInSecondaryNav() {
		return ( true == isset( $this->m_intShowInSecondaryNav ) ) ? ( string ) $this->m_intShowInSecondaryNav : '0';
	}

	public function setShowInTertiaryNav( $intShowInTertiaryNav ) {
		$this->set( 'm_intShowInTertiaryNav', CStrings::strToIntDef( $intShowInTertiaryNav, NULL, false ) );
	}

	public function getShowInTertiaryNav() {
		return $this->m_intShowInTertiaryNav;
	}

	public function sqlShowInTertiaryNav() {
		return ( true == isset( $this->m_intShowInTertiaryNav ) ) ? ( string ) $this->m_intShowInTertiaryNav : '0';
	}

	public function setShowInMobilePortal( $intShowInMobilePortal ) {
		$this->set( 'm_intShowInMobilePortal', CStrings::strToIntDef( $intShowInMobilePortal, NULL, false ) );
	}

	public function getShowInMobilePortal() {
		return $this->m_intShowInMobilePortal;
	}

	public function sqlShowInMobilePortal() {
		return ( true == isset( $this->m_intShowInMobilePortal ) ) ? ( string ) $this->m_intShowInMobilePortal : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWebsitePageId( $intWebsitePageId ) {
		$this->set( 'm_intWebsitePageId', CStrings::strToIntDef( $intWebsitePageId, NULL, false ) );
	}

	public function getWebsitePageId() {
		return $this->m_intWebsitePageId;
	}

	public function sqlWebsitePageId() {
		return ( true == isset( $this->m_intWebsitePageId ) ) ? ( string ) $this->m_intWebsitePageId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, document_id, show_in_primary_nav, show_in_secondary_nav, show_in_tertiary_nav, show_in_mobile_portal, is_published, is_system, order_num, updated_by, updated_on, created_by, created_on, website_page_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlShowInPrimaryNav() . ', ' .
 						$this->sqlShowInSecondaryNav() . ', ' .
 						$this->sqlShowInTertiaryNav() . ', ' .
 						$this->sqlShowInMobilePortal() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
		                $this->sqlWebsitePageId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_primary_nav = ' . $this->sqlShowInPrimaryNav() . ','; } elseif( true == array_key_exists( 'ShowInPrimaryNav', $this->getChangedColumns() ) ) { $strSql .= ' show_in_primary_nav = ' . $this->sqlShowInPrimaryNav() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_secondary_nav = ' . $this->sqlShowInSecondaryNav() . ','; } elseif( true == array_key_exists( 'ShowInSecondaryNav', $this->getChangedColumns() ) ) { $strSql .= ' show_in_secondary_nav = ' . $this->sqlShowInSecondaryNav() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_tertiary_nav = ' . $this->sqlShowInTertiaryNav() . ','; } elseif( true == array_key_exists( 'ShowInTertiaryNav', $this->getChangedColumns() ) ) { $strSql .= ' show_in_tertiary_nav = ' . $this->sqlShowInTertiaryNav() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_mobile_portal = ' . $this->sqlShowInMobilePortal() . ','; } elseif( true == array_key_exists( 'ShowInMobilePortal', $this->getChangedColumns() ) ) { $strSql .= ' show_in_mobile_portal = ' . $this->sqlShowInMobilePortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_page_id = ' . $this->sqlWebsitePageId(). ',' ; } elseif( true == array_key_exists( 'WebsitePageId', $this->getChangedColumns() ) ) { $strSql .= ' website_page_id = ' . $this->sqlWebsitePageId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'document_id' => $this->getDocumentId(),
			'show_in_primary_nav' => $this->getShowInPrimaryNav(),
			'show_in_secondary_nav' => $this->getShowInSecondaryNav(),
			'show_in_tertiary_nav' => $this->getShowInTertiaryNav(),
			'show_in_mobile_portal' => $this->getShowInMobilePortal(),
			'is_published' => $this->getIsPublished(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'website_page_id' => $this->getWebsitePageId()
		);
	}

}
?>