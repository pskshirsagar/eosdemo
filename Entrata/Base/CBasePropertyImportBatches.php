<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyImportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyImportBatches extends CEosPluralBase {

	/**
	 * @return CPropertyImportBatch[]
	 */
	public static function fetchPropertyImportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyImportBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyImportBatch
	 */
	public static function fetchPropertyImportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyImportBatch', $objDatabase );
	}

	public static function fetchPropertyImportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_import_batches', $objDatabase );
	}

	public static function fetchPropertyImportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyImportBatch( sprintf( 'SELECT * FROM property_import_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyImportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyImportBatches( sprintf( 'SELECT * FROM property_import_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyImportBatchesByImportBatchIdByCid( $intImportBatchId, $intCid, $objDatabase ) {
		return self::fetchPropertyImportBatches( sprintf( 'SELECT * FROM property_import_batches WHERE import_batch_id = %d AND cid = %d', ( int ) $intImportBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyImportBatchesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyImportBatches( sprintf( 'SELECT * FROM property_import_batches WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>