<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypeSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueUnitTypeSettings extends CEosPluralBase {

	/**
	 * @return CRevenueUnitTypeSetting[]
	 */
	public static function fetchRevenueUnitTypeSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueUnitTypeSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueUnitTypeSetting
	 */
	public static function fetchRevenueUnitTypeSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueUnitTypeSetting', $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_unit_type_settings', $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypeSetting( sprintf( 'SELECT * FROM revenue_unit_type_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypeSettings( sprintf( 'SELECT * FROM revenue_unit_type_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypeSettings( sprintf( 'SELECT * FROM revenue_unit_type_settings WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypeSettings( sprintf( 'SELECT * FROM revenue_unit_type_settings WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>