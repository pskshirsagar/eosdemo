<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intLeaseTypeId;
	protected $m_intInsurancePolicyId;
	protected $m_intSplitBillingTypeId;
	protected $m_intAccelRentAdjustingLeaseId;
	protected $m_intDelinquencyPropertyInterestFormulaId;
	protected $m_intDelinquencyPolicyId;
	protected $m_intCollectionsDelinquencyPolicyId;
	protected $m_fltSplitBillingFee;
	protected $m_fltSplitBillingCommission;
	protected $m_intSplitBillingDay1;
	protected $m_intSplitBillingDay2;
	protected $m_strLastPayrollDeposit;
	protected $m_fltRemoteBeginningBalance;
	protected $m_fltRemoteBeginningDeposit;
	protected $m_intMaxGuestStay;
	protected $m_intNumberOfUnitKeys;
	protected $m_intNumberOfMailKeys;
	protected $m_intNumberOfOtherKeys;
	protected $m_strAlarmKey;
	protected $m_strOtherKeyType;
	protected $m_strNotes;
	protected $m_intDontChargeLateFees;
	protected $m_intReturnedPaymentsCount;
	protected $m_intLatePaymentCount;
	protected $m_intLateFeePostedBy;
	protected $m_strLateFeePostedOn;
	protected $m_intIsQueued;
	protected $m_strQueuedOn;
	protected $m_strRenewalPromptMutedOn;
	protected $m_strAccelRentAdjustedOn;
	protected $m_strRenewalPromptMutedUntil;
	protected $m_intLockSequenceNumber;
	protected $m_intRequiresDataRebuild;
	protected $m_intIsLateNoticeSent;
	protected $m_intIsSoftLocked;
	protected $m_intIsRenewalBlacklist;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strScheduledMoveInStartTime;
	protected $m_strScheduledMoveInEndTime;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intDontChargeLateFees = '0';
		$this->m_intReturnedPaymentsCount = '0';
		$this->m_intLatePaymentCount = '0';
		$this->m_intIsQueued = '0';
		$this->m_intRequiresDataRebuild = '0';
		$this->m_intIsLateNoticeSent = '0';
		$this->m_intIsRenewalBlacklist = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTypeId', trim( $arrValues['lease_type_id'] ) ); elseif( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['split_billing_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSplitBillingTypeId', trim( $arrValues['split_billing_type_id'] ) ); elseif( isset( $arrValues['split_billing_type_id'] ) ) $this->setSplitBillingTypeId( $arrValues['split_billing_type_id'] );
		if( isset( $arrValues['accel_rent_adjusting_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intAccelRentAdjustingLeaseId', trim( $arrValues['accel_rent_adjusting_lease_id'] ) ); elseif( isset( $arrValues['accel_rent_adjusting_lease_id'] ) ) $this->setAccelRentAdjustingLeaseId( $arrValues['accel_rent_adjusting_lease_id'] );
		if( isset( $arrValues['delinquency_property_interest_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPropertyInterestFormulaId', trim( $arrValues['delinquency_property_interest_formula_id'] ) ); elseif( isset( $arrValues['delinquency_property_interest_formula_id'] ) ) $this->setDelinquencyPropertyInterestFormulaId( $arrValues['delinquency_property_interest_formula_id'] );
		if( isset( $arrValues['delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPolicyId', trim( $arrValues['delinquency_policy_id'] ) ); elseif( isset( $arrValues['delinquency_policy_id'] ) ) $this->setDelinquencyPolicyId( $arrValues['delinquency_policy_id'] );
		if( isset( $arrValues['collections_delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsDelinquencyPolicyId', trim( $arrValues['collections_delinquency_policy_id'] ) ); elseif( isset( $arrValues['collections_delinquency_policy_id'] ) ) $this->setCollectionsDelinquencyPolicyId( $arrValues['collections_delinquency_policy_id'] );
		if( isset( $arrValues['split_billing_fee'] ) && $boolDirectSet ) $this->set( 'm_fltSplitBillingFee', trim( $arrValues['split_billing_fee'] ) ); elseif( isset( $arrValues['split_billing_fee'] ) ) $this->setSplitBillingFee( $arrValues['split_billing_fee'] );
		if( isset( $arrValues['split_billing_commission'] ) && $boolDirectSet ) $this->set( 'm_fltSplitBillingCommission', trim( $arrValues['split_billing_commission'] ) ); elseif( isset( $arrValues['split_billing_commission'] ) ) $this->setSplitBillingCommission( $arrValues['split_billing_commission'] );
		if( isset( $arrValues['split_billing_day1'] ) && $boolDirectSet ) $this->set( 'm_intSplitBillingDay1', trim( $arrValues['split_billing_day1'] ) ); elseif( isset( $arrValues['split_billing_day1'] ) ) $this->setSplitBillingDay1( $arrValues['split_billing_day1'] );
		if( isset( $arrValues['split_billing_day2'] ) && $boolDirectSet ) $this->set( 'm_intSplitBillingDay2', trim( $arrValues['split_billing_day2'] ) ); elseif( isset( $arrValues['split_billing_day2'] ) ) $this->setSplitBillingDay2( $arrValues['split_billing_day2'] );
		if( isset( $arrValues['last_payroll_deposit'] ) && $boolDirectSet ) $this->set( 'm_strLastPayrollDeposit', trim( $arrValues['last_payroll_deposit'] ) ); elseif( isset( $arrValues['last_payroll_deposit'] ) ) $this->setLastPayrollDeposit( $arrValues['last_payroll_deposit'] );
		if( isset( $arrValues['remote_beginning_balance'] ) && $boolDirectSet ) $this->set( 'm_fltRemoteBeginningBalance', trim( $arrValues['remote_beginning_balance'] ) ); elseif( isset( $arrValues['remote_beginning_balance'] ) ) $this->setRemoteBeginningBalance( $arrValues['remote_beginning_balance'] );
		if( isset( $arrValues['remote_beginning_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltRemoteBeginningDeposit', trim( $arrValues['remote_beginning_deposit'] ) ); elseif( isset( $arrValues['remote_beginning_deposit'] ) ) $this->setRemoteBeginningDeposit( $arrValues['remote_beginning_deposit'] );
		if( isset( $arrValues['max_guest_stay'] ) && $boolDirectSet ) $this->set( 'm_intMaxGuestStay', trim( $arrValues['max_guest_stay'] ) ); elseif( isset( $arrValues['max_guest_stay'] ) ) $this->setMaxGuestStay( $arrValues['max_guest_stay'] );
		if( isset( $arrValues['number_of_unit_keys'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnitKeys', trim( $arrValues['number_of_unit_keys'] ) ); elseif( isset( $arrValues['number_of_unit_keys'] ) ) $this->setNumberOfUnitKeys( $arrValues['number_of_unit_keys'] );
		if( isset( $arrValues['number_of_mail_keys'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfMailKeys', trim( $arrValues['number_of_mail_keys'] ) ); elseif( isset( $arrValues['number_of_mail_keys'] ) ) $this->setNumberOfMailKeys( $arrValues['number_of_mail_keys'] );
		if( isset( $arrValues['number_of_other_keys'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOtherKeys', trim( $arrValues['number_of_other_keys'] ) ); elseif( isset( $arrValues['number_of_other_keys'] ) ) $this->setNumberOfOtherKeys( $arrValues['number_of_other_keys'] );
		if( isset( $arrValues['alarm_key'] ) && $boolDirectSet ) $this->set( 'm_strAlarmKey', trim( stripcslashes( $arrValues['alarm_key'] ) ) ); elseif( isset( $arrValues['alarm_key'] ) ) $this->setAlarmKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alarm_key'] ) : $arrValues['alarm_key'] );
		if( isset( $arrValues['other_key_type'] ) && $boolDirectSet ) $this->set( 'm_strOtherKeyType', trim( stripcslashes( $arrValues['other_key_type'] ) ) ); elseif( isset( $arrValues['other_key_type'] ) ) $this->setOtherKeyType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['other_key_type'] ) : $arrValues['other_key_type'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['dont_charge_late_fees'] ) && $boolDirectSet ) $this->set( 'm_intDontChargeLateFees', trim( $arrValues['dont_charge_late_fees'] ) ); elseif( isset( $arrValues['dont_charge_late_fees'] ) ) $this->setDontChargeLateFees( $arrValues['dont_charge_late_fees'] );
		if( isset( $arrValues['returned_payments_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedPaymentsCount', trim( $arrValues['returned_payments_count'] ) ); elseif( isset( $arrValues['returned_payments_count'] ) ) $this->setReturnedPaymentsCount( $arrValues['returned_payments_count'] );
		if( isset( $arrValues['late_payment_count'] ) && $boolDirectSet ) $this->set( 'm_intLatePaymentCount', trim( $arrValues['late_payment_count'] ) ); elseif( isset( $arrValues['late_payment_count'] ) ) $this->setLatePaymentCount( $arrValues['late_payment_count'] );
		if( isset( $arrValues['late_fee_posted_by'] ) && $boolDirectSet ) $this->set( 'm_intLateFeePostedBy', trim( $arrValues['late_fee_posted_by'] ) ); elseif( isset( $arrValues['late_fee_posted_by'] ) ) $this->setLateFeePostedBy( $arrValues['late_fee_posted_by'] );
		if( isset( $arrValues['late_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLateFeePostedOn', trim( $arrValues['late_fee_posted_on'] ) ); elseif( isset( $arrValues['late_fee_posted_on'] ) ) $this->setLateFeePostedOn( $arrValues['late_fee_posted_on'] );
		if( isset( $arrValues['is_queued'] ) && $boolDirectSet ) $this->set( 'm_intIsQueued', trim( $arrValues['is_queued'] ) ); elseif( isset( $arrValues['is_queued'] ) ) $this->setIsQueued( $arrValues['is_queued'] );
		if( isset( $arrValues['queued_on'] ) && $boolDirectSet ) $this->set( 'm_strQueuedOn', trim( $arrValues['queued_on'] ) ); elseif( isset( $arrValues['queued_on'] ) ) $this->setQueuedOn( $arrValues['queued_on'] );
		if( isset( $arrValues['renewal_prompt_muted_on'] ) && $boolDirectSet ) $this->set( 'm_strRenewalPromptMutedOn', trim( $arrValues['renewal_prompt_muted_on'] ) ); elseif( isset( $arrValues['renewal_prompt_muted_on'] ) ) $this->setRenewalPromptMutedOn( $arrValues['renewal_prompt_muted_on'] );
		if( isset( $arrValues['accel_rent_adjusted_on'] ) && $boolDirectSet ) $this->set( 'm_strAccelRentAdjustedOn', trim( $arrValues['accel_rent_adjusted_on'] ) ); elseif( isset( $arrValues['accel_rent_adjusted_on'] ) ) $this->setAccelRentAdjustedOn( $arrValues['accel_rent_adjusted_on'] );
		if( isset( $arrValues['renewal_prompt_muted_until'] ) && $boolDirectSet ) $this->set( 'm_strRenewalPromptMutedUntil', trim( $arrValues['renewal_prompt_muted_until'] ) ); elseif( isset( $arrValues['renewal_prompt_muted_until'] ) ) $this->setRenewalPromptMutedUntil( $arrValues['renewal_prompt_muted_until'] );
		if( isset( $arrValues['lock_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intLockSequenceNumber', trim( $arrValues['lock_sequence_number'] ) ); elseif( isset( $arrValues['lock_sequence_number'] ) ) $this->setLockSequenceNumber( $arrValues['lock_sequence_number'] );
		if( isset( $arrValues['requires_data_rebuild'] ) && $boolDirectSet ) $this->set( 'm_intRequiresDataRebuild', trim( $arrValues['requires_data_rebuild'] ) ); elseif( isset( $arrValues['requires_data_rebuild'] ) ) $this->setRequiresDataRebuild( $arrValues['requires_data_rebuild'] );
		if( isset( $arrValues['is_late_notice_sent'] ) && $boolDirectSet ) $this->set( 'm_intIsLateNoticeSent', trim( $arrValues['is_late_notice_sent'] ) ); elseif( isset( $arrValues['is_late_notice_sent'] ) ) $this->setIsLateNoticeSent( $arrValues['is_late_notice_sent'] );
		if( isset( $arrValues['is_soft_locked'] ) && $boolDirectSet ) $this->set( 'm_intIsSoftLocked', trim( $arrValues['is_soft_locked'] ) ); elseif( isset( $arrValues['is_soft_locked'] ) ) $this->setIsSoftLocked( $arrValues['is_soft_locked'] );
		if( isset( $arrValues['is_renewal_blacklist'] ) && $boolDirectSet ) $this->set( 'm_intIsRenewalBlacklist', trim( $arrValues['is_renewal_blacklist'] ) ); elseif( isset( $arrValues['is_renewal_blacklist'] ) ) $this->setIsRenewalBlacklist( $arrValues['is_renewal_blacklist'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['scheduled_move_in_start_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInStartTime', trim( $arrValues['scheduled_move_in_start_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_start_time'] ) ) $this->setScheduledMoveInStartTime( $arrValues['scheduled_move_in_start_time'] );
		if( isset( $arrValues['scheduled_move_in_end_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInEndTime', trim( $arrValues['scheduled_move_in_end_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_end_time'] ) ) $this->setScheduledMoveInEndTime( $arrValues['scheduled_move_in_end_time'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseTypeId( $intLeaseTypeId ) {
		$this->set( 'm_intLeaseTypeId', CStrings::strToIntDef( $intLeaseTypeId, NULL, false ) );
	}

	public function getLeaseTypeId() {
		return $this->m_intLeaseTypeId;
	}

	public function sqlLeaseTypeId() {
		return ( true == isset( $this->m_intLeaseTypeId ) ) ? ( string ) $this->m_intLeaseTypeId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setSplitBillingTypeId( $intSplitBillingTypeId ) {
		$this->set( 'm_intSplitBillingTypeId', CStrings::strToIntDef( $intSplitBillingTypeId, NULL, false ) );
	}

	public function getSplitBillingTypeId() {
		return $this->m_intSplitBillingTypeId;
	}

	public function sqlSplitBillingTypeId() {
		return ( true == isset( $this->m_intSplitBillingTypeId ) ) ? ( string ) $this->m_intSplitBillingTypeId : 'NULL';
	}

	public function setAccelRentAdjustingLeaseId( $intAccelRentAdjustingLeaseId ) {
		$this->set( 'm_intAccelRentAdjustingLeaseId', CStrings::strToIntDef( $intAccelRentAdjustingLeaseId, NULL, false ) );
	}

	public function getAccelRentAdjustingLeaseId() {
		return $this->m_intAccelRentAdjustingLeaseId;
	}

	public function sqlAccelRentAdjustingLeaseId() {
		return ( true == isset( $this->m_intAccelRentAdjustingLeaseId ) ) ? ( string ) $this->m_intAccelRentAdjustingLeaseId : 'NULL';
	}

	public function setDelinquencyPropertyInterestFormulaId( $intDelinquencyPropertyInterestFormulaId ) {
		$this->set( 'm_intDelinquencyPropertyInterestFormulaId', CStrings::strToIntDef( $intDelinquencyPropertyInterestFormulaId, NULL, false ) );
	}

	public function getDelinquencyPropertyInterestFormulaId() {
		return $this->m_intDelinquencyPropertyInterestFormulaId;
	}

	public function sqlDelinquencyPropertyInterestFormulaId() {
		return ( true == isset( $this->m_intDelinquencyPropertyInterestFormulaId ) ) ? ( string ) $this->m_intDelinquencyPropertyInterestFormulaId : 'NULL';
	}

	public function setDelinquencyPolicyId( $intDelinquencyPolicyId ) {
		$this->set( 'm_intDelinquencyPolicyId', CStrings::strToIntDef( $intDelinquencyPolicyId, NULL, false ) );
	}

	public function getDelinquencyPolicyId() {
		return $this->m_intDelinquencyPolicyId;
	}

	public function sqlDelinquencyPolicyId() {
		return ( true == isset( $this->m_intDelinquencyPolicyId ) ) ? ( string ) $this->m_intDelinquencyPolicyId : 'NULL';
	}

	public function setCollectionsDelinquencyPolicyId( $intCollectionsDelinquencyPolicyId ) {
		$this->set( 'm_intCollectionsDelinquencyPolicyId', CStrings::strToIntDef( $intCollectionsDelinquencyPolicyId, NULL, false ) );
	}

	public function getCollectionsDelinquencyPolicyId() {
		return $this->m_intCollectionsDelinquencyPolicyId;
	}

	public function sqlCollectionsDelinquencyPolicyId() {
		return ( true == isset( $this->m_intCollectionsDelinquencyPolicyId ) ) ? ( string ) $this->m_intCollectionsDelinquencyPolicyId : 'NULL';
	}

	public function setSplitBillingFee( $fltSplitBillingFee ) {
		$this->set( 'm_fltSplitBillingFee', CStrings::strToFloatDef( $fltSplitBillingFee, NULL, false, 2 ) );
	}

	public function getSplitBillingFee() {
		return $this->m_fltSplitBillingFee;
	}

	public function sqlSplitBillingFee() {
		return ( true == isset( $this->m_fltSplitBillingFee ) ) ? ( string ) $this->m_fltSplitBillingFee : 'NULL';
	}

	public function setSplitBillingCommission( $fltSplitBillingCommission ) {
		$this->set( 'm_fltSplitBillingCommission', CStrings::strToFloatDef( $fltSplitBillingCommission, NULL, false, 2 ) );
	}

	public function getSplitBillingCommission() {
		return $this->m_fltSplitBillingCommission;
	}

	public function sqlSplitBillingCommission() {
		return ( true == isset( $this->m_fltSplitBillingCommission ) ) ? ( string ) $this->m_fltSplitBillingCommission : 'NULL';
	}

	public function setSplitBillingDay1( $intSplitBillingDay1 ) {
		$this->set( 'm_intSplitBillingDay1', CStrings::strToIntDef( $intSplitBillingDay1, NULL, false ) );
	}

	public function getSplitBillingDay1() {
		return $this->m_intSplitBillingDay1;
	}

	public function sqlSplitBillingDay1() {
		return ( true == isset( $this->m_intSplitBillingDay1 ) ) ? ( string ) $this->m_intSplitBillingDay1 : 'NULL';
	}

	public function setSplitBillingDay2( $intSplitBillingDay2 ) {
		$this->set( 'm_intSplitBillingDay2', CStrings::strToIntDef( $intSplitBillingDay2, NULL, false ) );
	}

	public function getSplitBillingDay2() {
		return $this->m_intSplitBillingDay2;
	}

	public function sqlSplitBillingDay2() {
		return ( true == isset( $this->m_intSplitBillingDay2 ) ) ? ( string ) $this->m_intSplitBillingDay2 : 'NULL';
	}

	public function setLastPayrollDeposit( $strLastPayrollDeposit ) {
		$this->set( 'm_strLastPayrollDeposit', CStrings::strTrimDef( $strLastPayrollDeposit, -1, NULL, true ) );
	}

	public function getLastPayrollDeposit() {
		return $this->m_strLastPayrollDeposit;
	}

	public function sqlLastPayrollDeposit() {
		return ( true == isset( $this->m_strLastPayrollDeposit ) ) ? '\'' . $this->m_strLastPayrollDeposit . '\'' : 'NULL';
	}

	public function setRemoteBeginningBalance( $fltRemoteBeginningBalance ) {
		$this->set( 'm_fltRemoteBeginningBalance', CStrings::strToFloatDef( $fltRemoteBeginningBalance, NULL, false, 2 ) );
	}

	public function getRemoteBeginningBalance() {
		return $this->m_fltRemoteBeginningBalance;
	}

	public function sqlRemoteBeginningBalance() {
		return ( true == isset( $this->m_fltRemoteBeginningBalance ) ) ? ( string ) $this->m_fltRemoteBeginningBalance : 'NULL';
	}

	public function setRemoteBeginningDeposit( $fltRemoteBeginningDeposit ) {
		$this->set( 'm_fltRemoteBeginningDeposit', CStrings::strToFloatDef( $fltRemoteBeginningDeposit, NULL, false, 2 ) );
	}

	public function getRemoteBeginningDeposit() {
		return $this->m_fltRemoteBeginningDeposit;
	}

	public function sqlRemoteBeginningDeposit() {
		return ( true == isset( $this->m_fltRemoteBeginningDeposit ) ) ? ( string ) $this->m_fltRemoteBeginningDeposit : 'NULL';
	}

	public function setMaxGuestStay( $intMaxGuestStay ) {
		$this->set( 'm_intMaxGuestStay', CStrings::strToIntDef( $intMaxGuestStay, NULL, false ) );
	}

	public function getMaxGuestStay() {
		return $this->m_intMaxGuestStay;
	}

	public function sqlMaxGuestStay() {
		return ( true == isset( $this->m_intMaxGuestStay ) ) ? ( string ) $this->m_intMaxGuestStay : 'NULL';
	}

	public function setNumberOfUnitKeys( $intNumberOfUnitKeys ) {
		$this->set( 'm_intNumberOfUnitKeys', CStrings::strToIntDef( $intNumberOfUnitKeys, NULL, false ) );
	}

	public function getNumberOfUnitKeys() {
		return $this->m_intNumberOfUnitKeys;
	}

	public function sqlNumberOfUnitKeys() {
		return ( true == isset( $this->m_intNumberOfUnitKeys ) ) ? ( string ) $this->m_intNumberOfUnitKeys : 'NULL';
	}

	public function setNumberOfMailKeys( $intNumberOfMailKeys ) {
		$this->set( 'm_intNumberOfMailKeys', CStrings::strToIntDef( $intNumberOfMailKeys, NULL, false ) );
	}

	public function getNumberOfMailKeys() {
		return $this->m_intNumberOfMailKeys;
	}

	public function sqlNumberOfMailKeys() {
		return ( true == isset( $this->m_intNumberOfMailKeys ) ) ? ( string ) $this->m_intNumberOfMailKeys : 'NULL';
	}

	public function setNumberOfOtherKeys( $intNumberOfOtherKeys ) {
		$this->set( 'm_intNumberOfOtherKeys', CStrings::strToIntDef( $intNumberOfOtherKeys, NULL, false ) );
	}

	public function getNumberOfOtherKeys() {
		return $this->m_intNumberOfOtherKeys;
	}

	public function sqlNumberOfOtherKeys() {
		return ( true == isset( $this->m_intNumberOfOtherKeys ) ) ? ( string ) $this->m_intNumberOfOtherKeys : 'NULL';
	}

	public function setAlarmKey( $strAlarmKey ) {
		$this->set( 'm_strAlarmKey', CStrings::strTrimDef( $strAlarmKey, 10, NULL, true ) );
	}

	public function getAlarmKey() {
		return $this->m_strAlarmKey;
	}

	public function sqlAlarmKey() {
		return ( true == isset( $this->m_strAlarmKey ) ) ? '\'' . addslashes( $this->m_strAlarmKey ) . '\'' : 'NULL';
	}

	public function setOtherKeyType( $strOtherKeyType ) {
		$this->set( 'm_strOtherKeyType', CStrings::strTrimDef( $strOtherKeyType, 50, NULL, true ) );
	}

	public function getOtherKeyType() {
		return $this->m_strOtherKeyType;
	}

	public function sqlOtherKeyType() {
		return ( true == isset( $this->m_strOtherKeyType ) ) ? '\'' . addslashes( $this->m_strOtherKeyType ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setDontChargeLateFees( $intDontChargeLateFees ) {
		$this->set( 'm_intDontChargeLateFees', CStrings::strToIntDef( $intDontChargeLateFees, NULL, false ) );
	}

	public function getDontChargeLateFees() {
		return $this->m_intDontChargeLateFees;
	}

	public function sqlDontChargeLateFees() {
		return ( true == isset( $this->m_intDontChargeLateFees ) ) ? ( string ) $this->m_intDontChargeLateFees : '0';
	}

	public function setReturnedPaymentsCount( $intReturnedPaymentsCount ) {
		$this->set( 'm_intReturnedPaymentsCount', CStrings::strToIntDef( $intReturnedPaymentsCount, NULL, false ) );
	}

	public function getReturnedPaymentsCount() {
		return $this->m_intReturnedPaymentsCount;
	}

	public function sqlReturnedPaymentsCount() {
		return ( true == isset( $this->m_intReturnedPaymentsCount ) ) ? ( string ) $this->m_intReturnedPaymentsCount : '0';
	}

	public function setLatePaymentCount( $intLatePaymentCount ) {
		$this->set( 'm_intLatePaymentCount', CStrings::strToIntDef( $intLatePaymentCount, NULL, false ) );
	}

	public function getLatePaymentCount() {
		return $this->m_intLatePaymentCount;
	}

	public function sqlLatePaymentCount() {
		return ( true == isset( $this->m_intLatePaymentCount ) ) ? ( string ) $this->m_intLatePaymentCount : '0';
	}

	public function setLateFeePostedBy( $intLateFeePostedBy ) {
		$this->set( 'm_intLateFeePostedBy', CStrings::strToIntDef( $intLateFeePostedBy, NULL, false ) );
	}

	public function getLateFeePostedBy() {
		return $this->m_intLateFeePostedBy;
	}

	public function sqlLateFeePostedBy() {
		return ( true == isset( $this->m_intLateFeePostedBy ) ) ? ( string ) $this->m_intLateFeePostedBy : 'NULL';
	}

	public function setLateFeePostedOn( $strLateFeePostedOn ) {
		$this->set( 'm_strLateFeePostedOn', CStrings::strTrimDef( $strLateFeePostedOn, -1, NULL, true ) );
	}

	public function getLateFeePostedOn() {
		return $this->m_strLateFeePostedOn;
	}

	public function sqlLateFeePostedOn() {
		return ( true == isset( $this->m_strLateFeePostedOn ) ) ? '\'' . $this->m_strLateFeePostedOn . '\'' : 'NULL';
	}

	public function setIsQueued( $intIsQueued ) {
		$this->set( 'm_intIsQueued', CStrings::strToIntDef( $intIsQueued, NULL, false ) );
	}

	public function getIsQueued() {
		return $this->m_intIsQueued;
	}

	public function sqlIsQueued() {
		return ( true == isset( $this->m_intIsQueued ) ) ? ( string ) $this->m_intIsQueued : '0';
	}

	public function setQueuedOn( $strQueuedOn ) {
		$this->set( 'm_strQueuedOn', CStrings::strTrimDef( $strQueuedOn, -1, NULL, true ) );
	}

	public function getQueuedOn() {
		return $this->m_strQueuedOn;
	}

	public function sqlQueuedOn() {
		return ( true == isset( $this->m_strQueuedOn ) ) ? '\'' . $this->m_strQueuedOn . '\'' : 'NULL';
	}

	public function setRenewalPromptMutedOn( $strRenewalPromptMutedOn ) {
		$this->set( 'm_strRenewalPromptMutedOn', CStrings::strTrimDef( $strRenewalPromptMutedOn, -1, NULL, true ) );
	}

	public function getRenewalPromptMutedOn() {
		return $this->m_strRenewalPromptMutedOn;
	}

	public function sqlRenewalPromptMutedOn() {
		return ( true == isset( $this->m_strRenewalPromptMutedOn ) ) ? '\'' . $this->m_strRenewalPromptMutedOn . '\'' : 'NULL';
	}

	public function setAccelRentAdjustedOn( $strAccelRentAdjustedOn ) {
		$this->set( 'm_strAccelRentAdjustedOn', CStrings::strTrimDef( $strAccelRentAdjustedOn, -1, NULL, true ) );
	}

	public function getAccelRentAdjustedOn() {
		return $this->m_strAccelRentAdjustedOn;
	}

	public function sqlAccelRentAdjustedOn() {
		return ( true == isset( $this->m_strAccelRentAdjustedOn ) ) ? '\'' . $this->m_strAccelRentAdjustedOn . '\'' : 'NULL';
	}

	public function setRenewalPromptMutedUntil( $strRenewalPromptMutedUntil ) {
		$this->set( 'm_strRenewalPromptMutedUntil', CStrings::strTrimDef( $strRenewalPromptMutedUntil, -1, NULL, true ) );
	}

	public function getRenewalPromptMutedUntil() {
		return $this->m_strRenewalPromptMutedUntil;
	}

	public function sqlRenewalPromptMutedUntil() {
		return ( true == isset( $this->m_strRenewalPromptMutedUntil ) ) ? '\'' . $this->m_strRenewalPromptMutedUntil . '\'' : 'NULL';
	}

	public function setLockSequenceNumber( $intLockSequenceNumber ) {
		$this->set( 'm_intLockSequenceNumber', CStrings::strToIntDef( $intLockSequenceNumber, NULL, false ) );
	}

	public function getLockSequenceNumber() {
		return $this->m_intLockSequenceNumber;
	}

	public function sqlLockSequenceNumber() {
		return ( true == isset( $this->m_intLockSequenceNumber ) ) ? ( string ) $this->m_intLockSequenceNumber : 'NULL';
	}

	public function setRequiresDataRebuild( $intRequiresDataRebuild ) {
		$this->set( 'm_intRequiresDataRebuild', CStrings::strToIntDef( $intRequiresDataRebuild, NULL, false ) );
	}

	public function getRequiresDataRebuild() {
		return $this->m_intRequiresDataRebuild;
	}

	public function sqlRequiresDataRebuild() {
		return ( true == isset( $this->m_intRequiresDataRebuild ) ) ? ( string ) $this->m_intRequiresDataRebuild : '0';
	}

	public function setIsLateNoticeSent( $intIsLateNoticeSent ) {
		$this->set( 'm_intIsLateNoticeSent', CStrings::strToIntDef( $intIsLateNoticeSent, NULL, false ) );
	}

	public function getIsLateNoticeSent() {
		return $this->m_intIsLateNoticeSent;
	}

	public function sqlIsLateNoticeSent() {
		return ( true == isset( $this->m_intIsLateNoticeSent ) ) ? ( string ) $this->m_intIsLateNoticeSent : '0';
	}

	public function setIsSoftLocked( $intIsSoftLocked ) {
		$this->set( 'm_intIsSoftLocked', CStrings::strToIntDef( $intIsSoftLocked, NULL, false ) );
	}

	public function getIsSoftLocked() {
		return $this->m_intIsSoftLocked;
	}

	public function sqlIsSoftLocked() {
		return ( true == isset( $this->m_intIsSoftLocked ) ) ? ( string ) $this->m_intIsSoftLocked : 'NULL';
	}

	public function setIsRenewalBlacklist( $intIsRenewalBlacklist ) {
		$this->set( 'm_intIsRenewalBlacklist', CStrings::strToIntDef( $intIsRenewalBlacklist, NULL, false ) );
	}

	public function getIsRenewalBlacklist() {
		return $this->m_intIsRenewalBlacklist;
	}

	public function sqlIsRenewalBlacklist() {
		return ( true == isset( $this->m_intIsRenewalBlacklist ) ) ? ( string ) $this->m_intIsRenewalBlacklist : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScheduledMoveInStartTime( $strScheduledMoveInStartTime ) {
		$this->set( 'm_strScheduledMoveInStartTime', CStrings::strTrimDef( $strScheduledMoveInStartTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInStartTime() {
		return $this->m_strScheduledMoveInStartTime;
	}

	public function sqlScheduledMoveInStartTime() {
		return ( true == isset( $this->m_strScheduledMoveInStartTime ) ) ? '\'' . $this->m_strScheduledMoveInStartTime . '\'' : 'NULL';
	}

	public function setScheduledMoveInEndTime( $strScheduledMoveInEndTime ) {
		$this->set( 'm_strScheduledMoveInEndTime', CStrings::strTrimDef( $strScheduledMoveInEndTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInEndTime() {
		return $this->m_strScheduledMoveInEndTime;
	}

	public function sqlScheduledMoveInEndTime() {
		return ( true == isset( $this->m_strScheduledMoveInEndTime ) ) ? '\'' . $this->m_strScheduledMoveInEndTime . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, lease_type_id, insurance_policy_id, split_billing_type_id, accel_rent_adjusting_lease_id, delinquency_property_interest_formula_id, delinquency_policy_id, collections_delinquency_policy_id, split_billing_fee, split_billing_commission, split_billing_day1, split_billing_day2, last_payroll_deposit, remote_beginning_balance, remote_beginning_deposit, max_guest_stay, number_of_unit_keys, number_of_mail_keys, number_of_other_keys, alarm_key, other_key_type, notes, dont_charge_late_fees, returned_payments_count, late_payment_count, late_fee_posted_by, late_fee_posted_on, is_queued, queued_on, renewal_prompt_muted_on, accel_rent_adjusted_on, renewal_prompt_muted_until, lock_sequence_number, requires_data_rebuild, is_late_notice_sent, is_soft_locked, is_renewal_blacklist, updated_by, updated_on, created_by, created_on, scheduled_move_in_start_time, scheduled_move_in_end_time, details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlLeaseTypeId() . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlSplitBillingTypeId() . ', ' .
 						$this->sqlAccelRentAdjustingLeaseId() . ', ' .
 						$this->sqlDelinquencyPropertyInterestFormulaId() . ', ' .
 						$this->sqlDelinquencyPolicyId() . ', ' .
 						$this->sqlCollectionsDelinquencyPolicyId() . ', ' .
 						$this->sqlSplitBillingFee() . ', ' .
 						$this->sqlSplitBillingCommission() . ', ' .
 						$this->sqlSplitBillingDay1() . ', ' .
 						$this->sqlSplitBillingDay2() . ', ' .
 						$this->sqlLastPayrollDeposit() . ', ' .
 						$this->sqlRemoteBeginningBalance() . ', ' .
 						$this->sqlRemoteBeginningDeposit() . ', ' .
 						$this->sqlMaxGuestStay() . ', ' .
 						$this->sqlNumberOfUnitKeys() . ', ' .
 						$this->sqlNumberOfMailKeys() . ', ' .
 						$this->sqlNumberOfOtherKeys() . ', ' .
 						$this->sqlAlarmKey() . ', ' .
 						$this->sqlOtherKeyType() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlDontChargeLateFees() . ', ' .
 						$this->sqlReturnedPaymentsCount() . ', ' .
 						$this->sqlLatePaymentCount() . ', ' .
 						$this->sqlLateFeePostedBy() . ', ' .
 						$this->sqlLateFeePostedOn() . ', ' .
 						$this->sqlIsQueued() . ', ' .
 						$this->sqlQueuedOn() . ', ' .
 						$this->sqlRenewalPromptMutedOn() . ', ' .
 						$this->sqlAccelRentAdjustedOn() . ', ' .
 						$this->sqlRenewalPromptMutedUntil() . ', ' .
 						$this->sqlLockSequenceNumber() . ', ' .
 						$this->sqlRequiresDataRebuild() . ', ' .
 						$this->sqlIsLateNoticeSent() . ', ' .
 						$this->sqlIsSoftLocked() . ', ' .
 						$this->sqlIsRenewalBlacklist() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlScheduledMoveInStartTime() . ', ' .
 						$this->sqlScheduledMoveInEndTime() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; } elseif( true == array_key_exists( 'LeaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_billing_type_id = ' . $this->sqlSplitBillingTypeId() . ','; } elseif( true == array_key_exists( 'SplitBillingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' split_billing_type_id = ' . $this->sqlSplitBillingTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accel_rent_adjusting_lease_id = ' . $this->sqlAccelRentAdjustingLeaseId() . ','; } elseif( true == array_key_exists( 'AccelRentAdjustingLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' accel_rent_adjusting_lease_id = ' . $this->sqlAccelRentAdjustingLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_property_interest_formula_id = ' . $this->sqlDelinquencyPropertyInterestFormulaId() . ','; } elseif( true == array_key_exists( 'DelinquencyPropertyInterestFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_property_interest_formula_id = ' . $this->sqlDelinquencyPropertyInterestFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId() . ','; } elseif( true == array_key_exists( 'DelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_delinquency_policy_id = ' . $this->sqlCollectionsDelinquencyPolicyId() . ','; } elseif( true == array_key_exists( 'CollectionsDelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' collections_delinquency_policy_id = ' . $this->sqlCollectionsDelinquencyPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_billing_fee = ' . $this->sqlSplitBillingFee() . ','; } elseif( true == array_key_exists( 'SplitBillingFee', $this->getChangedColumns() ) ) { $strSql .= ' split_billing_fee = ' . $this->sqlSplitBillingFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_billing_commission = ' . $this->sqlSplitBillingCommission() . ','; } elseif( true == array_key_exists( 'SplitBillingCommission', $this->getChangedColumns() ) ) { $strSql .= ' split_billing_commission = ' . $this->sqlSplitBillingCommission() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_billing_day1 = ' . $this->sqlSplitBillingDay1() . ','; } elseif( true == array_key_exists( 'SplitBillingDay1', $this->getChangedColumns() ) ) { $strSql .= ' split_billing_day1 = ' . $this->sqlSplitBillingDay1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_billing_day2 = ' . $this->sqlSplitBillingDay2() . ','; } elseif( true == array_key_exists( 'SplitBillingDay2', $this->getChangedColumns() ) ) { $strSql .= ' split_billing_day2 = ' . $this->sqlSplitBillingDay2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_payroll_deposit = ' . $this->sqlLastPayrollDeposit() . ','; } elseif( true == array_key_exists( 'LastPayrollDeposit', $this->getChangedColumns() ) ) { $strSql .= ' last_payroll_deposit = ' . $this->sqlLastPayrollDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_beginning_balance = ' . $this->sqlRemoteBeginningBalance() . ','; } elseif( true == array_key_exists( 'RemoteBeginningBalance', $this->getChangedColumns() ) ) { $strSql .= ' remote_beginning_balance = ' . $this->sqlRemoteBeginningBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_beginning_deposit = ' . $this->sqlRemoteBeginningDeposit() . ','; } elseif( true == array_key_exists( 'RemoteBeginningDeposit', $this->getChangedColumns() ) ) { $strSql .= ' remote_beginning_deposit = ' . $this->sqlRemoteBeginningDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_guest_stay = ' . $this->sqlMaxGuestStay() . ','; } elseif( true == array_key_exists( 'MaxGuestStay', $this->getChangedColumns() ) ) { $strSql .= ' max_guest_stay = ' . $this->sqlMaxGuestStay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_unit_keys = ' . $this->sqlNumberOfUnitKeys() . ','; } elseif( true == array_key_exists( 'NumberOfUnitKeys', $this->getChangedColumns() ) ) { $strSql .= ' number_of_unit_keys = ' . $this->sqlNumberOfUnitKeys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_mail_keys = ' . $this->sqlNumberOfMailKeys() . ','; } elseif( true == array_key_exists( 'NumberOfMailKeys', $this->getChangedColumns() ) ) { $strSql .= ' number_of_mail_keys = ' . $this->sqlNumberOfMailKeys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_other_keys = ' . $this->sqlNumberOfOtherKeys() . ','; } elseif( true == array_key_exists( 'NumberOfOtherKeys', $this->getChangedColumns() ) ) { $strSql .= ' number_of_other_keys = ' . $this->sqlNumberOfOtherKeys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alarm_key = ' . $this->sqlAlarmKey() . ','; } elseif( true == array_key_exists( 'AlarmKey', $this->getChangedColumns() ) ) { $strSql .= ' alarm_key = ' . $this->sqlAlarmKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_key_type = ' . $this->sqlOtherKeyType() . ','; } elseif( true == array_key_exists( 'OtherKeyType', $this->getChangedColumns() ) ) { $strSql .= ' other_key_type = ' . $this->sqlOtherKeyType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_charge_late_fees = ' . $this->sqlDontChargeLateFees() . ','; } elseif( true == array_key_exists( 'DontChargeLateFees', $this->getChangedColumns() ) ) { $strSql .= ' dont_charge_late_fees = ' . $this->sqlDontChargeLateFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_payments_count = ' . $this->sqlReturnedPaymentsCount() . ','; } elseif( true == array_key_exists( 'ReturnedPaymentsCount', $this->getChangedColumns() ) ) { $strSql .= ' returned_payments_count = ' . $this->sqlReturnedPaymentsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_payment_count = ' . $this->sqlLatePaymentCount() . ','; } elseif( true == array_key_exists( 'LatePaymentCount', $this->getChangedColumns() ) ) { $strSql .= ' late_payment_count = ' . $this->sqlLatePaymentCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_posted_by = ' . $this->sqlLateFeePostedBy() . ','; } elseif( true == array_key_exists( 'LateFeePostedBy', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_posted_by = ' . $this->sqlLateFeePostedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_posted_on = ' . $this->sqlLateFeePostedOn() . ','; } elseif( true == array_key_exists( 'LateFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_posted_on = ' . $this->sqlLateFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_queued = ' . $this->sqlIsQueued() . ','; } elseif( true == array_key_exists( 'IsQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_queued = ' . $this->sqlIsQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queued_on = ' . $this->sqlQueuedOn() . ','; } elseif( true == array_key_exists( 'QueuedOn', $this->getChangedColumns() ) ) { $strSql .= ' queued_on = ' . $this->sqlQueuedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_prompt_muted_on = ' . $this->sqlRenewalPromptMutedOn() . ','; } elseif( true == array_key_exists( 'RenewalPromptMutedOn', $this->getChangedColumns() ) ) { $strSql .= ' renewal_prompt_muted_on = ' . $this->sqlRenewalPromptMutedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accel_rent_adjusted_on = ' . $this->sqlAccelRentAdjustedOn() . ','; } elseif( true == array_key_exists( 'AccelRentAdjustedOn', $this->getChangedColumns() ) ) { $strSql .= ' accel_rent_adjusted_on = ' . $this->sqlAccelRentAdjustedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_prompt_muted_until = ' . $this->sqlRenewalPromptMutedUntil() . ','; } elseif( true == array_key_exists( 'RenewalPromptMutedUntil', $this->getChangedColumns() ) ) { $strSql .= ' renewal_prompt_muted_until = ' . $this->sqlRenewalPromptMutedUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; } elseif( true == array_key_exists( 'LockSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_data_rebuild = ' . $this->sqlRequiresDataRebuild() . ','; } elseif( true == array_key_exists( 'RequiresDataRebuild', $this->getChangedColumns() ) ) { $strSql .= ' requires_data_rebuild = ' . $this->sqlRequiresDataRebuild() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_late_notice_sent = ' . $this->sqlIsLateNoticeSent() . ','; } elseif( true == array_key_exists( 'IsLateNoticeSent', $this->getChangedColumns() ) ) { $strSql .= ' is_late_notice_sent = ' . $this->sqlIsLateNoticeSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_soft_locked = ' . $this->sqlIsSoftLocked() . ','; } elseif( true == array_key_exists( 'IsSoftLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_soft_locked = ' . $this->sqlIsSoftLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renewal_blacklist = ' . $this->sqlIsRenewalBlacklist() . ','; } elseif( true == array_key_exists( 'IsRenewalBlacklist', $this->getChangedColumns() ) ) { $strSql .= ' is_renewal_blacklist = ' . $this->sqlIsRenewalBlacklist() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime() . ','; } elseif( true == array_key_exists( 'ScheduledMoveInStartTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime() . ','; } elseif( true == array_key_exists( 'ScheduledMoveInEndTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'lease_type_id' => $this->getLeaseTypeId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'split_billing_type_id' => $this->getSplitBillingTypeId(),
			'accel_rent_adjusting_lease_id' => $this->getAccelRentAdjustingLeaseId(),
			'delinquency_property_interest_formula_id' => $this->getDelinquencyPropertyInterestFormulaId(),
			'delinquency_policy_id' => $this->getDelinquencyPolicyId(),
			'collections_delinquency_policy_id' => $this->getCollectionsDelinquencyPolicyId(),
			'split_billing_fee' => $this->getSplitBillingFee(),
			'split_billing_commission' => $this->getSplitBillingCommission(),
			'split_billing_day1' => $this->getSplitBillingDay1(),
			'split_billing_day2' => $this->getSplitBillingDay2(),
			'last_payroll_deposit' => $this->getLastPayrollDeposit(),
			'remote_beginning_balance' => $this->getRemoteBeginningBalance(),
			'remote_beginning_deposit' => $this->getRemoteBeginningDeposit(),
			'max_guest_stay' => $this->getMaxGuestStay(),
			'number_of_unit_keys' => $this->getNumberOfUnitKeys(),
			'number_of_mail_keys' => $this->getNumberOfMailKeys(),
			'number_of_other_keys' => $this->getNumberOfOtherKeys(),
			'alarm_key' => $this->getAlarmKey(),
			'other_key_type' => $this->getOtherKeyType(),
			'notes' => $this->getNotes(),
			'dont_charge_late_fees' => $this->getDontChargeLateFees(),
			'returned_payments_count' => $this->getReturnedPaymentsCount(),
			'late_payment_count' => $this->getLatePaymentCount(),
			'late_fee_posted_by' => $this->getLateFeePostedBy(),
			'late_fee_posted_on' => $this->getLateFeePostedOn(),
			'is_queued' => $this->getIsQueued(),
			'queued_on' => $this->getQueuedOn(),
			'renewal_prompt_muted_on' => $this->getRenewalPromptMutedOn(),
			'accel_rent_adjusted_on' => $this->getAccelRentAdjustedOn(),
			'renewal_prompt_muted_until' => $this->getRenewalPromptMutedUntil(),
			'lock_sequence_number' => $this->getLockSequenceNumber(),
			'requires_data_rebuild' => $this->getRequiresDataRebuild(),
			'is_late_notice_sent' => $this->getIsLateNoticeSent(),
			'is_soft_locked' => $this->getIsSoftLocked(),
			'is_renewal_blacklist' => $this->getIsRenewalBlacklist(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'scheduled_move_in_start_time' => $this->getScheduledMoveInStartTime(),
			'scheduled_move_in_end_time' => $this->getScheduledMoveInEndTime(),
			'details' => $this->getDetails()
		);
	}

}
?>