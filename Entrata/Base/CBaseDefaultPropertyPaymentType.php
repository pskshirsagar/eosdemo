<?php

class CBaseDefaultPropertyPaymentType extends CEosSingularBase {

	const TABLE_NAME = 'public.default_property_payment_types';

	protected $m_intId;
	protected $m_intPaymentTypeId;
	protected $m_boolAllowInEntrata;
	protected $m_boolAllowInResidentPortal;
	protected $m_boolAllowInProspectPortal;
	protected $m_boolIsCertifiedFunds;
	protected $m_boolIsAutoAdded;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowInEntrata = true;
		$this->m_boolAllowInResidentPortal = true;
		$this->m_boolAllowInProspectPortal = true;
		$this->m_boolIsCertifiedFunds = false;
		$this->m_boolIsAutoAdded = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['allow_in_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInEntrata', trim( stripcslashes( $arrValues['allow_in_entrata'] ) ) ); elseif( isset( $arrValues['allow_in_entrata'] ) ) $this->setAllowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_entrata'] ) : $arrValues['allow_in_entrata'] );
		if( isset( $arrValues['allow_in_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInResidentPortal', trim( stripcslashes( $arrValues['allow_in_resident_portal'] ) ) ); elseif( isset( $arrValues['allow_in_resident_portal'] ) ) $this->setAllowInResidentPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_resident_portal'] ) : $arrValues['allow_in_resident_portal'] );
		if( isset( $arrValues['allow_in_prospect_portal'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInProspectPortal', trim( stripcslashes( $arrValues['allow_in_prospect_portal'] ) ) ); elseif( isset( $arrValues['allow_in_prospect_portal'] ) ) $this->setAllowInProspectPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_prospect_portal'] ) : $arrValues['allow_in_prospect_portal'] );
		if( isset( $arrValues['is_certified_funds'] ) && $boolDirectSet ) $this->set( 'm_boolIsCertifiedFunds', trim( stripcslashes( $arrValues['is_certified_funds'] ) ) ); elseif( isset( $arrValues['is_certified_funds'] ) ) $this->setIsCertifiedFunds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_certified_funds'] ) : $arrValues['is_certified_funds'] );
		if( isset( $arrValues['is_auto_added'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoAdded', trim( stripcslashes( $arrValues['is_auto_added'] ) ) ); elseif( isset( $arrValues['is_auto_added'] ) ) $this->setIsAutoAdded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_added'] ) : $arrValues['is_auto_added'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setAllowInEntrata( $boolAllowInEntrata ) {
		$this->set( 'm_boolAllowInEntrata', CStrings::strToBool( $boolAllowInEntrata ) );
	}

	public function getAllowInEntrata() {
		return $this->m_boolAllowInEntrata;
	}

	public function sqlAllowInEntrata() {
		return ( true == isset( $this->m_boolAllowInEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowInResidentPortal( $boolAllowInResidentPortal ) {
		$this->set( 'm_boolAllowInResidentPortal', CStrings::strToBool( $boolAllowInResidentPortal ) );
	}

	public function getAllowInResidentPortal() {
		return $this->m_boolAllowInResidentPortal;
	}

	public function sqlAllowInResidentPortal() {
		return ( true == isset( $this->m_boolAllowInResidentPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInResidentPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowInProspectPortal( $boolAllowInProspectPortal ) {
		$this->set( 'm_boolAllowInProspectPortal', CStrings::strToBool( $boolAllowInProspectPortal ) );
	}

	public function getAllowInProspectPortal() {
		return $this->m_boolAllowInProspectPortal;
	}

	public function sqlAllowInProspectPortal() {
		return ( true == isset( $this->m_boolAllowInProspectPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInProspectPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCertifiedFunds( $boolIsCertifiedFunds ) {
		$this->set( 'm_boolIsCertifiedFunds', CStrings::strToBool( $boolIsCertifiedFunds ) );
	}

	public function getIsCertifiedFunds() {
		return $this->m_boolIsCertifiedFunds;
	}

	public function sqlIsCertifiedFunds() {
		return ( true == isset( $this->m_boolIsCertifiedFunds ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCertifiedFunds ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoAdded( $boolIsAutoAdded ) {
		$this->set( 'm_boolIsAutoAdded', CStrings::strToBool( $boolIsAutoAdded ) );
	}

	public function getIsAutoAdded() {
		return $this->m_boolIsAutoAdded;
	}

	public function sqlIsAutoAdded() {
		return ( true == isset( $this->m_boolIsAutoAdded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoAdded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'allow_in_entrata' => $this->getAllowInEntrata(),
			'allow_in_resident_portal' => $this->getAllowInResidentPortal(),
			'allow_in_prospect_portal' => $this->getAllowInProspectPortal(),
			'is_certified_funds' => $this->getIsCertifiedFunds(),
			'is_auto_added' => $this->getIsAutoAdded()
		);
	}

}
?>