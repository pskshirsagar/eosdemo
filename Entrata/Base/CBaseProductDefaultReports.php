<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductDefaultReports
 * Do not add any new functions to this class.
 */

class CBaseProductDefaultReports extends CEosPluralBase {

	/**
	 * @return CProductDefaultReport[]
	 */
	public static function fetchProductDefaultReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CProductDefaultReport', $objDatabase );
	}

	/**
	 * @return CProductDefaultReport
	 */
	public static function fetchProductDefaultReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProductDefaultReport', $objDatabase );
	}

	public static function fetchProductDefaultReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'product_default_reports', $objDatabase );
	}

	public static function fetchProductDefaultReportById( $intId, $objDatabase ) {
		return self::fetchProductDefaultReport( sprintf( 'SELECT * FROM product_default_reports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProductDefaultReportsByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		return self::fetchProductDefaultReports( sprintf( 'SELECT * FROM product_default_reports WHERE default_report_id = %d', ( int ) $intDefaultReportId ), $objDatabase );
	}

	public static function fetchProductDefaultReportsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchProductDefaultReports( sprintf( 'SELECT * FROM product_default_reports WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>