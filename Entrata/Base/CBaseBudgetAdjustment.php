<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetAdjustment extends CEosSingularBase {

	const TABLE_NAME = 'public.budget_adjustments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobId;
	protected $m_intFromApHeaderId;
	protected $m_intToApHeaderId;
	protected $m_strDescription;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['from_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intFromApHeaderId', trim( $arrValues['from_ap_header_id'] ) ); elseif( isset( $arrValues['from_ap_header_id'] ) ) $this->setFromApHeaderId( $arrValues['from_ap_header_id'] );
		if( isset( $arrValues['to_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intToApHeaderId', trim( $arrValues['to_ap_header_id'] ) ); elseif( isset( $arrValues['to_ap_header_id'] ) ) $this->setToApHeaderId( $arrValues['to_ap_header_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setFromApHeaderId( $intFromApHeaderId ) {
		$this->set( 'm_intFromApHeaderId', CStrings::strToIntDef( $intFromApHeaderId, NULL, false ) );
	}

	public function getFromApHeaderId() {
		return $this->m_intFromApHeaderId;
	}

	public function sqlFromApHeaderId() {
		return ( true == isset( $this->m_intFromApHeaderId ) ) ? ( string ) $this->m_intFromApHeaderId : 'NULL';
	}

	public function setToApHeaderId( $intToApHeaderId ) {
		$this->set( 'm_intToApHeaderId', CStrings::strToIntDef( $intToApHeaderId, NULL, false ) );
	}

	public function getToApHeaderId() {
		return $this->m_intToApHeaderId;
	}

	public function sqlToApHeaderId() {
		return ( true == isset( $this->m_intToApHeaderId ) ) ? ( string ) $this->m_intToApHeaderId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_id, from_ap_header_id, to_ap_header_id, description, approved_by, approved_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlJobId() . ', ' .
 						$this->sqlFromApHeaderId() . ', ' .
 						$this->sqlToApHeaderId() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_ap_header_id = ' . $this->sqlFromApHeaderId() . ','; } elseif( true == array_key_exists( 'FromApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' from_ap_header_id = ' . $this->sqlFromApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_ap_header_id = ' . $this->sqlToApHeaderId() . ','; } elseif( true == array_key_exists( 'ToApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' to_ap_header_id = ' . $this->sqlToApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_id' => $this->getJobId(),
			'from_ap_header_id' => $this->getFromApHeaderId(),
			'to_ap_header_id' => $this->getToApHeaderId(),
			'description' => $this->getDescription(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>