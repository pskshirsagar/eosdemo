<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentPreferences extends CEosPluralBase {

	/**
	 * @return CDocumentPreference[]
	 */
	public static function fetchDocumentPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDocumentPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocumentPreference
	 */
	public static function fetchDocumentPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentPreference', $objDatabase );
	}

	public static function fetchDocumentPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_preferences', $objDatabase );
	}

	public static function fetchDocumentPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocumentPreference( sprintf( 'SELECT * FROM document_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentPreferences( sprintf( 'SELECT * FROM document_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentPreferencesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentPreferences( sprintf( 'SELECT * FROM document_preferences WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

}
?>