<?php

class CBaseDefaultCompetitorStrategy extends CEosSingularBase {

	const TABLE_NAME = 'public.default_competitor_strategies';

	protected $m_intId;
	protected $m_intRevenueCalcTypeId;
	protected $m_fltAdjustmentAmount;
	protected $m_intIsFixedAmount;

	public function __construct() {
		parent::__construct();

		$this->m_intRevenueCalcTypeId = '1';
		$this->m_fltAdjustmentAmount = '0';
		$this->m_intIsFixedAmount = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['revenue_calc_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueCalcTypeId', trim( $arrValues['revenue_calc_type_id'] ) ); elseif( isset( $arrValues['revenue_calc_type_id'] ) ) $this->setRevenueCalcTypeId( $arrValues['revenue_calc_type_id'] );
		if( isset( $arrValues['adjustment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentAmount', trim( $arrValues['adjustment_amount'] ) ); elseif( isset( $arrValues['adjustment_amount'] ) ) $this->setAdjustmentAmount( $arrValues['adjustment_amount'] );
		if( isset( $arrValues['is_fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_intIsFixedAmount', trim( $arrValues['is_fixed_amount'] ) ); elseif( isset( $arrValues['is_fixed_amount'] ) ) $this->setIsFixedAmount( $arrValues['is_fixed_amount'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRevenueCalcTypeId( $intRevenueCalcTypeId ) {
		$this->set( 'm_intRevenueCalcTypeId', CStrings::strToIntDef( $intRevenueCalcTypeId, NULL, false ) );
	}

	public function getRevenueCalcTypeId() {
		return $this->m_intRevenueCalcTypeId;
	}

	public function sqlRevenueCalcTypeId() {
		return ( true == isset( $this->m_intRevenueCalcTypeId ) ) ? ( string ) $this->m_intRevenueCalcTypeId : '1';
	}

	public function setAdjustmentAmount( $fltAdjustmentAmount ) {
		$this->set( 'm_fltAdjustmentAmount', CStrings::strToFloatDef( $fltAdjustmentAmount, NULL, false, 6 ) );
	}

	public function getAdjustmentAmount() {
		return $this->m_fltAdjustmentAmount;
	}

	public function sqlAdjustmentAmount() {
		return ( true == isset( $this->m_fltAdjustmentAmount ) ) ? ( string ) $this->m_fltAdjustmentAmount : '0';
	}

	public function setIsFixedAmount( $intIsFixedAmount ) {
		$this->set( 'm_intIsFixedAmount', CStrings::strToIntDef( $intIsFixedAmount, NULL, false ) );
	}

	public function getIsFixedAmount() {
		return $this->m_intIsFixedAmount;
	}

	public function sqlIsFixedAmount() {
		return ( true == isset( $this->m_intIsFixedAmount ) ) ? ( string ) $this->m_intIsFixedAmount : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'revenue_calc_type_id' => $this->getRevenueCalcTypeId(),
			'adjustment_amount' => $this->getAdjustmentAmount(),
			'is_fixed_amount' => $this->getIsFixedAmount()
		);
	}

}
?>