<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataCleanupRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataCleanupRequests extends CEosPluralBase {

	/**
	 * @return CDataCleanupRequest[]
	 */
	public static function fetchDataCleanupRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDataCleanupRequest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDataCleanupRequest
	 */
	public static function fetchDataCleanupRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDataCleanupRequest::class, $objDatabase );
	}

	public static function fetchDataCleanupRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_cleanup_requests', $objDatabase );
	}

	public static function fetchDataCleanupRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDataCleanupRequest( sprintf( 'SELECT * FROM data_cleanup_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataCleanupRequestsByCid( $intCid, $objDatabase, $boolIncludeAllDataCleanupType = true ) {

		$strSubSql = ( false == $boolIncludeAllDataCleanupType ) ? ' AND data_cleanup_type_id <> ' . CDataCleanupType::DATA_CLEAN_UP_TYPE_GL_HISTORY : '';
		$strSql = 'SELECT * FROM data_cleanup_requests WHERE cid = ' . ( int ) $intCid . '' . $strSubSql . ' ORDER BY id DESC';

		return self::fetchDataCleanupRequests( $strSql, $objDatabase );
	}

	public static function fetchDataCleanupRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDataCleanupRequests( sprintf( 'SELECT * FROM data_cleanup_requests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDataCleanupRequestsByDataCleanupTypeIdByCid( $intDataCleanupTypeId, $intCid, $objDatabase ) {
		return self::fetchDataCleanupRequests( sprintf( 'SELECT * FROM data_cleanup_requests WHERE data_cleanup_type_id = %d AND cid = %d', ( int ) $intDataCleanupTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>