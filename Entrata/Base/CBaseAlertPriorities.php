<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAlertPriorities
 * Do not add any new functions to this class.
 */

class CBaseAlertPriorities extends CEosPluralBase {

	/**
	 * @return CAlertPriority[]
	 */
	public static function fetchAlertPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAlertPriority::class, $objDatabase );
	}

	/**
	 * @return CAlertPriority
	 */
	public static function fetchAlertPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAlertPriority::class, $objDatabase );
	}

	public static function fetchAlertPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'alert_priorities', $objDatabase );
	}

	public static function fetchAlertPriorityById( $intId, $objDatabase ) {
		return self::fetchAlertPriority( sprintf( 'SELECT * FROM alert_priorities WHERE id = %d', $intId ), $objDatabase );
	}

}
?>