<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCompanyDepartments
 * Do not add any new functions to this class.
 */

class CBaseDefaultCompanyDepartments extends CEosPluralBase {

	/**
	 * @return CDefaultCompanyDepartment[]
	 */
	public static function fetchDefaultCompanyDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCompanyDepartment::class, $objDatabase );
	}

	/**
	 * @return CDefaultCompanyDepartment
	 */
	public static function fetchDefaultCompanyDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCompanyDepartment::class, $objDatabase );
	}

	public static function fetchDefaultCompanyDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_company_departments', $objDatabase );
	}

	public static function fetchDefaultCompanyDepartmentById( $intId, $objDatabase ) {
		return self::fetchDefaultCompanyDepartment( sprintf( 'SELECT * FROM default_company_departments WHERE id = %d', $intId ), $objDatabase );
	}

}
?>