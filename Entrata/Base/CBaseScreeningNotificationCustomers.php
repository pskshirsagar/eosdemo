<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotificationCustomers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningNotificationCustomers extends CEosPluralBase {

	/**
	 * @return CScreeningNotificationCustomer[]
	 */
	public static function fetchScreeningNotificationCustomers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningNotificationCustomer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningNotificationCustomer
	 */
	public static function fetchScreeningNotificationCustomer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningNotificationCustomer', $objDatabase );
	}

	public static function fetchScreeningNotificationCustomerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_notification_customers', $objDatabase );
	}

	public static function fetchScreeningNotificationCustomerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotificationCustomer( sprintf( 'SELECT * FROM screening_notification_customers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationCustomersByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningNotificationCustomers( sprintf( 'SELECT * FROM screening_notification_customers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationCustomersByScreeningNotificationIdByCid( $intScreeningNotificationId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotificationCustomers( sprintf( 'SELECT * FROM screening_notification_customers WHERE screening_notification_id = %d AND cid = %d', ( int ) $intScreeningNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningNotificationCustomersByCustomerTypeIdByCid( $intCustomerTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningNotificationCustomers( sprintf( 'SELECT * FROM screening_notification_customers WHERE customer_type_id = %d AND cid = %d', ( int ) $intCustomerTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>