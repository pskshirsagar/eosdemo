<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChore extends CEosSingularBase {

	const TABLE_NAME = 'public.chores';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intChoreCategoryId;
	protected $m_intChoreTypeId;
	protected $m_intChoreStatusTypeId;
	protected $m_intChorePriorityId;
	protected $m_intTaskId;
	protected $m_strChoreDatetime;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDueDate;
	protected $m_intItemCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['chore_category_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreCategoryId', trim( $arrValues['chore_category_id'] ) ); elseif( isset( $arrValues['chore_category_id'] ) ) $this->setChoreCategoryId( $arrValues['chore_category_id'] );
		if( isset( $arrValues['chore_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreTypeId', trim( $arrValues['chore_type_id'] ) ); elseif( isset( $arrValues['chore_type_id'] ) ) $this->setChoreTypeId( $arrValues['chore_type_id'] );
		if( isset( $arrValues['chore_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreStatusTypeId', trim( $arrValues['chore_status_type_id'] ) ); elseif( isset( $arrValues['chore_status_type_id'] ) ) $this->setChoreStatusTypeId( $arrValues['chore_status_type_id'] );
		if( isset( $arrValues['chore_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intChorePriorityId', trim( $arrValues['chore_priority_id'] ) ); elseif( isset( $arrValues['chore_priority_id'] ) ) $this->setChorePriorityId( $arrValues['chore_priority_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['chore_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChoreDatetime', trim( $arrValues['chore_datetime'] ) ); elseif( isset( $arrValues['chore_datetime'] ) ) $this->setChoreDatetime( $arrValues['chore_datetime'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['item_count'] ) && $boolDirectSet ) $this->set( 'm_intItemCount', trim( $arrValues['item_count'] ) ); elseif( isset( $arrValues['item_count'] ) ) $this->setItemCount( $arrValues['item_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setChoreCategoryId( $intChoreCategoryId ) {
		$this->set( 'm_intChoreCategoryId', CStrings::strToIntDef( $intChoreCategoryId, NULL, false ) );
	}

	public function getChoreCategoryId() {
		return $this->m_intChoreCategoryId;
	}

	public function sqlChoreCategoryId() {
		return ( true == isset( $this->m_intChoreCategoryId ) ) ? ( string ) $this->m_intChoreCategoryId : 'NULL';
	}

	public function setChoreTypeId( $intChoreTypeId ) {
		$this->set( 'm_intChoreTypeId', CStrings::strToIntDef( $intChoreTypeId, NULL, false ) );
	}

	public function getChoreTypeId() {
		return $this->m_intChoreTypeId;
	}

	public function sqlChoreTypeId() {
		return ( true == isset( $this->m_intChoreTypeId ) ) ? ( string ) $this->m_intChoreTypeId : 'NULL';
	}

	public function setChoreStatusTypeId( $intChoreStatusTypeId ) {
		$this->set( 'm_intChoreStatusTypeId', CStrings::strToIntDef( $intChoreStatusTypeId, NULL, false ) );
	}

	public function getChoreStatusTypeId() {
		return $this->m_intChoreStatusTypeId;
	}

	public function sqlChoreStatusTypeId() {
		return ( true == isset( $this->m_intChoreStatusTypeId ) ) ? ( string ) $this->m_intChoreStatusTypeId : 'NULL';
	}

	public function setChorePriorityId( $intChorePriorityId ) {
		$this->set( 'm_intChorePriorityId', CStrings::strToIntDef( $intChorePriorityId, NULL, false ) );
	}

	public function getChorePriorityId() {
		return $this->m_intChorePriorityId;
	}

	public function sqlChorePriorityId() {
		return ( true == isset( $this->m_intChorePriorityId ) ) ? ( string ) $this->m_intChorePriorityId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setChoreDatetime( $strChoreDatetime ) {
		$this->set( 'm_strChoreDatetime', CStrings::strTrimDef( $strChoreDatetime, -1, NULL, true ) );
	}

	public function getChoreDatetime() {
		return $this->m_strChoreDatetime;
	}

	public function sqlChoreDatetime() {
		return ( true == isset( $this->m_strChoreDatetime ) ) ? '\'' . $this->m_strChoreDatetime . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setItemCount( $intItemCount ) {
		$this->set( 'm_intItemCount', CStrings::strToIntDef( $intItemCount, NULL, false ) );
	}

	public function getItemCount() {
		return $this->m_intItemCount;
	}

	public function sqlItemCount() {
		return ( true == isset( $this->m_intItemCount ) ) ? ( string ) $this->m_intItemCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, chore_category_id, chore_type_id, chore_status_type_id, chore_priority_id, task_id, chore_datetime, name, description, due_date, item_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlChoreCategoryId() . ', ' .
 						$this->sqlChoreTypeId() . ', ' .
 						$this->sqlChoreStatusTypeId() . ', ' .
 						$this->sqlChorePriorityId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlChoreDatetime() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDueDate() . ', ' .
 						$this->sqlItemCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_category_id = ' . $this->sqlChoreCategoryId() . ','; } elseif( true == array_key_exists( 'ChoreCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' chore_category_id = ' . $this->sqlChoreCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_type_id = ' . $this->sqlChoreTypeId() . ','; } elseif( true == array_key_exists( 'ChoreTypeId', $this->getChangedColumns() ) ) { $strSql .= ' chore_type_id = ' . $this->sqlChoreTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_status_type_id = ' . $this->sqlChoreStatusTypeId() . ','; } elseif( true == array_key_exists( 'ChoreStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' chore_status_type_id = ' . $this->sqlChoreStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_priority_id = ' . $this->sqlChorePriorityId() . ','; } elseif( true == array_key_exists( 'ChorePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' chore_priority_id = ' . $this->sqlChorePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_datetime = ' . $this->sqlChoreDatetime() . ','; } elseif( true == array_key_exists( 'ChoreDatetime', $this->getChangedColumns() ) ) { $strSql .= ' chore_datetime = ' . $this->sqlChoreDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_count = ' . $this->sqlItemCount() . ','; } elseif( true == array_key_exists( 'ItemCount', $this->getChangedColumns() ) ) { $strSql .= ' item_count = ' . $this->sqlItemCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'chore_category_id' => $this->getChoreCategoryId(),
			'chore_type_id' => $this->getChoreTypeId(),
			'chore_status_type_id' => $this->getChoreStatusTypeId(),
			'chore_priority_id' => $this->getChorePriorityId(),
			'task_id' => $this->getTaskId(),
			'chore_datetime' => $this->getChoreDatetime(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'due_date' => $this->getDueDate(),
			'item_count' => $this->getItemCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>