<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransmissions extends CEosPluralBase {

	/**
	 * @return CTransmission[]
	 */
	public static function fetchTransmissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTransmission::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTransmission
	 */
	public static function fetchTransmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransmission::class, $objDatabase );
	}

	public static function fetchTransmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transmissions', $objDatabase );
	}

	public static function fetchTransmissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTransmission( sprintf( 'SELECT * FROM transmissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByCid( $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByTransmissionTypeIdByCid( $intTransmissionTypeId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE transmission_type_id = %d AND cid = %d', ( int ) $intTransmissionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE transmission_vendor_id = %d AND cid = %d', ( int ) $intTransmissionVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE company_transmission_vendor_id = %d AND cid = %d', ( int ) $intCompanyTransmissionVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByTransmissionResponseTypeIdByCid( $intTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE transmission_response_type_id = %d AND cid = %d', ( int ) $intTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionsByScreeningConfigurationIdByCid( $intScreeningConfigurationId, $intCid, $objDatabase ) {
		return self::fetchTransmissions( sprintf( 'SELECT * FROM transmissions WHERE screening_configuration_id = %d AND cid = %d', ( int ) $intScreeningConfigurationId, ( int ) $intCid ), $objDatabase );
	}

}
?>