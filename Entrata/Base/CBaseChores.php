<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChores
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChores extends CEosPluralBase {

	/**
	 * @return CChore[]
	 */
	public static function fetchChores( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CChore', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChore
	 */
	public static function fetchChore( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChore', $objDatabase );
	}

	public static function fetchChoreCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chores', $objDatabase );
	}

	public static function fetchChoreByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChore( sprintf( 'SELECT * FROM chores WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByCid( $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByChoreCategoryIdByCid( $intChoreCategoryId, $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE chore_category_id = %d AND cid = %d', ( int ) $intChoreCategoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByChoreTypeIdByCid( $intChoreTypeId, $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE chore_type_id = %d AND cid = %d', ( int ) $intChoreTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByChoreStatusTypeIdByCid( $intChoreStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE chore_status_type_id = %d AND cid = %d', ( int ) $intChoreStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByChorePriorityIdByCid( $intChorePriorityId, $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE chore_priority_id = %d AND cid = %d', ( int ) $intChorePriorityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoresByTaskIdByCid( $intTaskId, $intCid, $objDatabase ) {
		return self::fetchChores( sprintf( 'SELECT * FROM chores WHERE task_id = %d AND cid = %d', ( int ) $intTaskId, ( int ) $intCid ), $objDatabase );
	}

}
?>