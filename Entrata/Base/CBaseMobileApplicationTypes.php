<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMobileApplicationTypes
 * Do not add any new functions to this class.
 */

class CBaseMobileApplicationTypes extends CEosPluralBase {

	/**
	 * @return CMobileApplicationType[]
	 */
	public static function fetchMobileApplicationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMobileApplicationType::class, $objDatabase );
	}

	/**
	 * @return CMobileApplicationType
	 */
	public static function fetchMobileApplicationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMobileApplicationType::class, $objDatabase );
	}

	public static function fetchMobileApplicationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mobile_application_types', $objDatabase );
	}

	public static function fetchMobileApplicationTypeById( $intId, $objDatabase ) {
		return self::fetchMobileApplicationType( sprintf( 'SELECT * FROM mobile_application_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>