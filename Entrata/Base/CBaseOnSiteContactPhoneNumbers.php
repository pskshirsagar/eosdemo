<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COnSiteContactPhoneNumbers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOnSiteContactPhoneNumbers extends CEosPluralBase {

	/**
	 * @return COnSiteContactPhoneNumber[]
	 */
	public static function fetchOnSiteContactPhoneNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COnSiteContactPhoneNumber', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COnSiteContactPhoneNumber
	 */
	public static function fetchOnSiteContactPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COnSiteContactPhoneNumber', $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'on_site_contact_phone_numbers', $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumberByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactPhoneNumber( sprintf( 'SELECT * FROM on_site_contact_phone_numbers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchOnSiteContactPhoneNumbers( sprintf( 'SELECT * FROM on_site_contact_phone_numbers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactPhoneNumbers( sprintf( 'SELECT * FROM on_site_contact_phone_numbers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumbersByPropertyOnSiteContactIdByCid( $intPropertyOnSiteContactId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactPhoneNumbers( sprintf( 'SELECT * FROM on_site_contact_phone_numbers WHERE property_on_site_contact_id = %d AND cid = %d', ( int ) $intPropertyOnSiteContactId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactPhoneNumbersByPhoneNumberTypeIdByCid( $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactPhoneNumbers( sprintf( 'SELECT * FROM on_site_contact_phone_numbers WHERE phone_number_type_id = %d AND cid = %d', ( int ) $intPhoneNumberTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>