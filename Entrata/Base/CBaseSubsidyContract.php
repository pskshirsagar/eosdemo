<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyContract extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_contracts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyContractTypeId;
	protected $m_intSubsidyContractSubTypeId;
	protected $m_intSubsidyUnitAssignmentTypeId;
	protected $m_intSubsidyActionPlanTypeId;
	protected $m_intUnitCount;
	protected $m_fltRentLimitsPercent;
	protected $m_strHapTitle;
	protected $m_strHapPersonName;
	protected $m_strHapPersonPhoneNumber;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strContractNumber;
	protected $m_boolAppliesToAllUnits;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSubsidyUnitAssignmentTypeId = '2';
		$this->m_intUnitCount = '0';
		$this->m_strStartDate = '01/01/1970';
		$this->m_strEndDate = '12/31/2099';
		$this->m_boolAppliesToAllUnits = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractTypeId', trim( $arrValues['subsidy_contract_type_id'] ) ); elseif( isset( $arrValues['subsidy_contract_type_id'] ) ) $this->setSubsidyContractTypeId( $arrValues['subsidy_contract_type_id'] );
		if( isset( $arrValues['subsidy_contract_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractSubTypeId', trim( $arrValues['subsidy_contract_sub_type_id'] ) ); elseif( isset( $arrValues['subsidy_contract_sub_type_id'] ) ) $this->setSubsidyContractSubTypeId( $arrValues['subsidy_contract_sub_type_id'] );
		if( isset( $arrValues['subsidy_unit_assignment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyUnitAssignmentTypeId', trim( $arrValues['subsidy_unit_assignment_type_id'] ) ); elseif( isset( $arrValues['subsidy_unit_assignment_type_id'] ) ) $this->setSubsidyUnitAssignmentTypeId( $arrValues['subsidy_unit_assignment_type_id'] );
		if( isset( $arrValues['subsidy_action_plan_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyActionPlanTypeId', trim( $arrValues['subsidy_action_plan_type_id'] ) ); elseif( isset( $arrValues['subsidy_action_plan_type_id'] ) ) $this->setSubsidyActionPlanTypeId( $arrValues['subsidy_action_plan_type_id'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['rent_limits_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRentLimitsPercent', trim( $arrValues['rent_limits_percent'] ) ); elseif( isset( $arrValues['rent_limits_percent'] ) ) $this->setRentLimitsPercent( $arrValues['rent_limits_percent'] );
		if( isset( $arrValues['hap_title'] ) && $boolDirectSet ) $this->set( 'm_strHapTitle', trim( stripcslashes( $arrValues['hap_title'] ) ) ); elseif( isset( $arrValues['hap_title'] ) ) $this->setHapTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hap_title'] ) : $arrValues['hap_title'] );
		if( isset( $arrValues['hap_person_name'] ) && $boolDirectSet ) $this->set( 'm_strHapPersonName', trim( stripcslashes( $arrValues['hap_person_name'] ) ) ); elseif( isset( $arrValues['hap_person_name'] ) ) $this->setHapPersonName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hap_person_name'] ) : $arrValues['hap_person_name'] );
		if( isset( $arrValues['hap_person_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strHapPersonPhoneNumber', trim( stripcslashes( $arrValues['hap_person_phone_number'] ) ) ); elseif( isset( $arrValues['hap_person_phone_number'] ) ) $this->setHapPersonPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hap_person_phone_number'] ) : $arrValues['hap_person_phone_number'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['contract_number'] ) && $boolDirectSet ) $this->set( 'm_strContractNumber', trim( stripcslashes( $arrValues['contract_number'] ) ) ); elseif( isset( $arrValues['contract_number'] ) ) $this->setContractNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contract_number'] ) : $arrValues['contract_number'] );
		if( isset( $arrValues['applies_to_all_units'] ) && $boolDirectSet ) $this->set( 'm_boolAppliesToAllUnits', trim( stripcslashes( $arrValues['applies_to_all_units'] ) ) ); elseif( isset( $arrValues['applies_to_all_units'] ) ) $this->setAppliesToAllUnits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applies_to_all_units'] ) : $arrValues['applies_to_all_units'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyContractTypeId( $intSubsidyContractTypeId ) {
		$this->set( 'm_intSubsidyContractTypeId', CStrings::strToIntDef( $intSubsidyContractTypeId, NULL, false ) );
	}

	public function getSubsidyContractTypeId() {
		return $this->m_intSubsidyContractTypeId;
	}

	public function sqlSubsidyContractTypeId() {
		return ( true == isset( $this->m_intSubsidyContractTypeId ) ) ? ( string ) $this->m_intSubsidyContractTypeId : 'NULL';
	}

	public function setSubsidyContractSubTypeId( $intSubsidyContractSubTypeId ) {
		$this->set( 'm_intSubsidyContractSubTypeId', CStrings::strToIntDef( $intSubsidyContractSubTypeId, NULL, false ) );
	}

	public function getSubsidyContractSubTypeId() {
		return $this->m_intSubsidyContractSubTypeId;
	}

	public function sqlSubsidyContractSubTypeId() {
		return ( true == isset( $this->m_intSubsidyContractSubTypeId ) ) ? ( string ) $this->m_intSubsidyContractSubTypeId : 'NULL';
	}

	public function setSubsidyUnitAssignmentTypeId( $intSubsidyUnitAssignmentTypeId ) {
		$this->set( 'm_intSubsidyUnitAssignmentTypeId', CStrings::strToIntDef( $intSubsidyUnitAssignmentTypeId, NULL, false ) );
	}

	public function getSubsidyUnitAssignmentTypeId() {
		return $this->m_intSubsidyUnitAssignmentTypeId;
	}

	public function sqlSubsidyUnitAssignmentTypeId() {
		return ( true == isset( $this->m_intSubsidyUnitAssignmentTypeId ) ) ? ( string ) $this->m_intSubsidyUnitAssignmentTypeId : '2';
	}

	public function setSubsidyActionPlanTypeId( $intSubsidyActionPlanTypeId ) {
		$this->set( 'm_intSubsidyActionPlanTypeId', CStrings::strToIntDef( $intSubsidyActionPlanTypeId, NULL, false ) );
	}

	public function getSubsidyActionPlanTypeId() {
		return $this->m_intSubsidyActionPlanTypeId;
	}

	public function sqlSubsidyActionPlanTypeId() {
		return ( true == isset( $this->m_intSubsidyActionPlanTypeId ) ) ? ( string ) $this->m_intSubsidyActionPlanTypeId : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : '0';
	}

	public function setRentLimitsPercent( $fltRentLimitsPercent ) {
		$this->set( 'm_fltRentLimitsPercent', CStrings::strToFloatDef( $fltRentLimitsPercent, NULL, false, 2 ) );
	}

	public function getRentLimitsPercent() {
		return $this->m_fltRentLimitsPercent;
	}

	public function sqlRentLimitsPercent() {
		return ( true == isset( $this->m_fltRentLimitsPercent ) ) ? ( string ) $this->m_fltRentLimitsPercent : 'NULL';
	}

	public function setHapTitle( $strHapTitle ) {
		$this->set( 'm_strHapTitle', CStrings::strTrimDef( $strHapTitle, 50, NULL, true ) );
	}

	public function getHapTitle() {
		return $this->m_strHapTitle;
	}

	public function sqlHapTitle() {
		return ( true == isset( $this->m_strHapTitle ) ) ? '\'' . addslashes( $this->m_strHapTitle ) . '\'' : 'NULL';
	}

	public function setHapPersonName( $strHapPersonName ) {
		$this->set( 'm_strHapPersonName', CStrings::strTrimDef( $strHapPersonName, 50, NULL, true ) );
	}

	public function getHapPersonName() {
		return $this->m_strHapPersonName;
	}

	public function sqlHapPersonName() {
		return ( true == isset( $this->m_strHapPersonName ) ) ? '\'' . addslashes( $this->m_strHapPersonName ) . '\'' : 'NULL';
	}

	public function setHapPersonPhoneNumber( $strHapPersonPhoneNumber ) {
		$this->set( 'm_strHapPersonPhoneNumber', CStrings::strTrimDef( $strHapPersonPhoneNumber, 30, NULL, true ) );
	}

	public function getHapPersonPhoneNumber() {
		return $this->m_strHapPersonPhoneNumber;
	}

	public function sqlHapPersonPhoneNumber() {
		return ( true == isset( $this->m_strHapPersonPhoneNumber ) ) ? '\'' . addslashes( $this->m_strHapPersonPhoneNumber ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function setContractNumber( $strContractNumber ) {
		$this->set( 'm_strContractNumber', CStrings::strTrimDef( $strContractNumber, 11, NULL, true ) );
	}

	public function getContractNumber() {
		return $this->m_strContractNumber;
	}

	public function sqlContractNumber() {
		return ( true == isset( $this->m_strContractNumber ) ) ? '\'' . addslashes( $this->m_strContractNumber ) . '\'' : 'NULL';
	}

	public function setAppliesToAllUnits( $boolAppliesToAllUnits ) {
		$this->set( 'm_boolAppliesToAllUnits', CStrings::strToBool( $boolAppliesToAllUnits ) );
	}

	public function getAppliesToAllUnits() {
		return $this->m_boolAppliesToAllUnits;
	}

	public function sqlAppliesToAllUnits() {
		return ( true == isset( $this->m_boolAppliesToAllUnits ) ) ? '\'' . ( true == ( bool ) $this->m_boolAppliesToAllUnits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_contract_type_id, subsidy_contract_sub_type_id, subsidy_unit_assignment_type_id, subsidy_action_plan_type_id, unit_count, rent_limits_percent, hap_title, hap_person_name, hap_person_phone_number, start_date, end_date, contract_number, applies_to_all_units, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidyContractTypeId() . ', ' .
 						$this->sqlSubsidyContractSubTypeId() . ', ' .
 						$this->sqlSubsidyUnitAssignmentTypeId() . ', ' .
 						$this->sqlSubsidyActionPlanTypeId() . ', ' .
 						$this->sqlUnitCount() . ', ' .
 						$this->sqlRentLimitsPercent() . ', ' .
 						$this->sqlHapTitle() . ', ' .
 						$this->sqlHapPersonName() . ', ' .
 						$this->sqlHapPersonPhoneNumber() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlContractNumber() . ', ' .
 						$this->sqlAppliesToAllUnits() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_sub_type_id = ' . $this->sqlSubsidyContractSubTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyContractSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_sub_type_id = ' . $this->sqlSubsidyContractSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_unit_assignment_type_id = ' . $this->sqlSubsidyUnitAssignmentTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyUnitAssignmentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_unit_assignment_type_id = ' . $this->sqlSubsidyUnitAssignmentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_action_plan_type_id = ' . $this->sqlSubsidyActionPlanTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyActionPlanTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_action_plan_type_id = ' . $this->sqlSubsidyActionPlanTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_limits_percent = ' . $this->sqlRentLimitsPercent() . ','; } elseif( true == array_key_exists( 'RentLimitsPercent', $this->getChangedColumns() ) ) { $strSql .= ' rent_limits_percent = ' . $this->sqlRentLimitsPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hap_title = ' . $this->sqlHapTitle() . ','; } elseif( true == array_key_exists( 'HapTitle', $this->getChangedColumns() ) ) { $strSql .= ' hap_title = ' . $this->sqlHapTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hap_person_name = ' . $this->sqlHapPersonName() . ','; } elseif( true == array_key_exists( 'HapPersonName', $this->getChangedColumns() ) ) { $strSql .= ' hap_person_name = ' . $this->sqlHapPersonName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hap_person_phone_number = ' . $this->sqlHapPersonPhoneNumber() . ','; } elseif( true == array_key_exists( 'HapPersonPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' hap_person_phone_number = ' . $this->sqlHapPersonPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_number = ' . $this->sqlContractNumber() . ','; } elseif( true == array_key_exists( 'ContractNumber', $this->getChangedColumns() ) ) { $strSql .= ' contract_number = ' . $this->sqlContractNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applies_to_all_units = ' . $this->sqlAppliesToAllUnits() . ','; } elseif( true == array_key_exists( 'AppliesToAllUnits', $this->getChangedColumns() ) ) { $strSql .= ' applies_to_all_units = ' . $this->sqlAppliesToAllUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_contract_type_id' => $this->getSubsidyContractTypeId(),
			'subsidy_contract_sub_type_id' => $this->getSubsidyContractSubTypeId(),
			'subsidy_unit_assignment_type_id' => $this->getSubsidyUnitAssignmentTypeId(),
			'subsidy_action_plan_type_id' => $this->getSubsidyActionPlanTypeId(),
			'unit_count' => $this->getUnitCount(),
			'rent_limits_percent' => $this->getRentLimitsPercent(),
			'hap_title' => $this->getHapTitle(),
			'hap_person_name' => $this->getHapPersonName(),
			'hap_person_phone_number' => $this->getHapPersonPhoneNumber(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'contract_number' => $this->getContractNumber(),
			'applies_to_all_units' => $this->getAppliesToAllUnits(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>