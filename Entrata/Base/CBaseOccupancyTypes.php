<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COccupancyTypes
 * Do not add any new functions to this class.
 */

class CBaseOccupancyTypes extends CEosPluralBase {

	/**
	 * @return COccupancyType[]
	 */
	public static function fetchOccupancyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COccupancyType::class, $objDatabase );
	}

	/**
	 * @return COccupancyType
	 */
	public static function fetchOccupancyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COccupancyType::class, $objDatabase );
	}

	public static function fetchOccupancyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'occupancy_types', $objDatabase );
	}

	public static function fetchOccupancyTypeById( $intId, $objDatabase ) {
		return self::fetchOccupancyType( sprintf( 'SELECT * FROM occupancy_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchOccupancyTypesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchOccupancyTypes( sprintf( 'SELECT * FROM occupancy_types WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

}
?>