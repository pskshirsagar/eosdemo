<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportRequestReports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportRequestReports extends CEosPluralBase {

	/**
	 * @return CImportRequestReport[]
	 */
	public static function fetchImportRequestReports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CImportRequestReport', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CImportRequestReport
	 */
	public static function fetchImportRequestReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CImportRequestReport', $objDatabase );
	}

	public static function fetchImportRequestReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_request_reports', $objDatabase );
	}

	public static function fetchImportRequestReportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchImportRequestReport( sprintf( 'SELECT * FROM import_request_reports WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportRequestReportsByCid( $intCid, $objDatabase ) {
		return self::fetchImportRequestReports( sprintf( 'SELECT * FROM import_request_reports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportRequestReportsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchImportRequestReports( sprintf( 'SELECT * FROM import_request_reports WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportRequestReportsByImportEntityReportIdByCid( $intImportEntityReportId, $intCid, $objDatabase ) {
		return self::fetchImportRequestReports( sprintf( 'SELECT * FROM import_request_reports WHERE import_entity_report_id = %d AND cid = %d', ( int ) $intImportEntityReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportRequestReportsByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchImportRequestReports( sprintf( 'SELECT * FROM import_request_reports WHERE report_id = %d AND cid = %d', ( int ) $intReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportRequestReportsByImportRequestIdByCid( $intImportRequestId, $intCid, $objDatabase ) {
		return self::fetchImportRequestReports( sprintf( 'SELECT * FROM import_request_reports WHERE import_request_id = %d AND cid = %d', ( int ) $intImportRequestId, ( int ) $intCid ), $objDatabase );
	}

}
?>