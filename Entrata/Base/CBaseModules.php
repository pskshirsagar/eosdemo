<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CModules
 * Do not add any new functions to this class.
 */

class CBaseModules extends CEosPluralBase {

	/**
	 * @return CModule[]
	 */
	public static function fetchModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CModule::class, $objDatabase );
	}

	/**
	 * @return CModule
	 */
	public static function fetchModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CModule::class, $objDatabase );
	}

	public static function fetchModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'modules', $objDatabase );
	}

	public static function fetchModuleById( $intId, $objDatabase ) {
		return self::fetchModule( sprintf( 'SELECT * FROM modules WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchModulesByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchModules( sprintf( 'SELECT * FROM modules WHERE default_cid = %d', $intDefaultCid ), $objDatabase );
	}

	public static function fetchModulesByParentModuleId( $intParentModuleId, $objDatabase ) {
		return self::fetchModules( sprintf( 'SELECT * FROM modules WHERE parent_module_id = %d', $intParentModuleId ), $objDatabase );
	}

}
?>