<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxTransmitterDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTaxTransmitterDetails extends CEosPluralBase {

	/**
	 * @return CTaxTransmitterDetail[]
	 */
	public static function fetchTaxTransmitterDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTaxTransmitterDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTaxTransmitterDetail
	 */
	public static function fetchTaxTransmitterDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxTransmitterDetail::class, $objDatabase );
	}

	public static function fetchTaxTransmitterDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tax_transmitter_details', $objDatabase );
	}

	public static function fetchTaxTransmitterDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTaxTransmitterDetail( sprintf( 'SELECT * FROM tax_transmitter_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTaxTransmitterDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchTaxTransmitterDetails( sprintf( 'SELECT * FROM tax_transmitter_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>