<?php

class CBaseRenewalOfferSpecial extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intRenewalOfferId;
    protected $m_intCompanySpecialId;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if ( isset( $arrValues['id'] )) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if ( isset( $arrValues['cid'] )) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['renewal_offer_id'] ) && $boolDirectSet ) $this->m_intRenewalOfferId = trim( $arrValues['renewal_offer_id'] ); else if ( isset( $arrValues['renewal_offer_id'] )) $this->setRenewalOfferId( $arrValues['renewal_offer_id'] );
        if( isset( $arrValues['company_special_id'] ) && $boolDirectSet ) $this->m_intCompanySpecialId = trim( $arrValues['company_special_id'] ); else if ( isset( $arrValues['company_special_id'] )) $this->setCompanySpecialId( $arrValues['company_special_id'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if ( isset( $arrValues['created_by'] )) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if ( isset( $arrValues['created_on'] )) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return  ( true == isset( $this->m_intId )) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return  ( true == isset( $this->m_intCid )) ? (string) $this->m_intCid : 'NULL';
    }

    public function setRenewalOfferId( $intRenewalOfferId ) {
        $this->m_intRenewalOfferId = CStrings::strToIntDef( $intRenewalOfferId, NULL, false );
    }

    public function getRenewalOfferId() {
        return $this->m_intRenewalOfferId;
    }

    public function sqlRenewalOfferId() {
        return  ( true == isset( $this->m_intRenewalOfferId )) ? (string) $this->m_intRenewalOfferId : 'NULL';
    }

    public function setCompanySpecialId( $intCompanySpecialId ) {
        $this->m_intCompanySpecialId = CStrings::strToIntDef( $intCompanySpecialId, NULL, false );
    }

    public function getCompanySpecialId() {
        return $this->m_intCompanySpecialId;
    }

    public function sqlCompanySpecialId() {
        return  ( true == isset( $this->m_intCompanySpecialId )) ? (string) $this->m_intCompanySpecialId : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return  ( true == isset( $this->m_intCreatedBy )) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return  ( true == isset( $this->m_strCreatedOn )) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() )) ?  'nextval( \'public.renewal_offer_specials_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.renewal_offer_specials
					VALUES ( ' .
	                    $strId . ', ' .
	                    $this->sqlCid() . ', ' .
	                    $this->sqlRenewalOfferId() . ', ' .
	                    $this->sqlCompanySpecialId() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate()) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.renewal_offer_specials
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid()) != $this->getOriginalValueByFieldName ( 'cid' )) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_offer_id = ' . $this->sqlRenewalOfferId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRenewalOfferId()) != $this->getOriginalValueByFieldName ( 'renewal_offer_id' )) { $arrstrOriginalValueChanges['renewal_offer_id'] = $this->sqlRenewalOfferId(); $strSql .= ' renewal_offer_id = ' . $this->sqlRenewalOfferId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_special_id = ' . $this->sqlCompanySpecialId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanySpecialId()) != $this->getOriginalValueByFieldName ( 'company_special_id' )) { $arrstrOriginalValueChanges['company_special_id'] = $this->sqlCompanySpecialId(); $strSql .= ' company_special_id = ' . $this->sqlCompanySpecialId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= '    WHERE
					        id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ){
			    if( true == $this->executeSql( $strSql, $this, $objDatabase )){
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.renewal_offer_specials WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase ) {
        return parent::fetchNextId( 'public.renewal_offer_specials_id_seq', $objDatabase );
    }
}
?>