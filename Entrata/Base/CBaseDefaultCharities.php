<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCharities
 * Do not add any new functions to this class.
 */

class CBaseDefaultCharities extends CEosPluralBase {

	/**
	 * @return CDefaultCharity[]
	 */
	public static function fetchDefaultCharities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCharity::class, $objDatabase );
	}

	/**
	 * @return CDefaultCharity
	 */
	public static function fetchDefaultCharity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCharity::class, $objDatabase );
	}

	public static function fetchDefaultCharityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_charities', $objDatabase );
	}

	public static function fetchDefaultCharityById( $intId, $objDatabase ) {
		return self::fetchDefaultCharity( sprintf( 'SELECT * FROM default_charities WHERE id = %d', $intId ), $objDatabase );
	}

}
?>