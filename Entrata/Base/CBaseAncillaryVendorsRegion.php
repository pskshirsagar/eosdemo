<?php

class CBaseAncillaryVendorsRegion extends CEosSingularBase {

	const TABLE_NAME = 'public.ancillary_vendors_regions';

	protected $m_intId;
	protected $m_intAncillaryVendorId;
	protected $m_strStateCode;

	public function __construct() {
		parent::__construct();

		$this->m_intAncillaryVendorId = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ancillary_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intAncillaryVendorId', trim( $arrValues['ancillary_vendor_id'] ) ); elseif( isset( $arrValues['ancillary_vendor_id'] ) ) $this->setAncillaryVendorId( $arrValues['ancillary_vendor_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAncillaryVendorId( $intAncillaryVendorId ) {
		$this->set( 'm_intAncillaryVendorId', CStrings::strToIntDef( $intAncillaryVendorId, NULL, false ) );
	}

	public function getAncillaryVendorId() {
		return $this->m_intAncillaryVendorId;
	}

	public function sqlAncillaryVendorId() {
		return ( true == isset( $this->m_intAncillaryVendorId ) ) ? ( string ) $this->m_intAncillaryVendorId : '0';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ancillary_vendor_id' => $this->getAncillaryVendorId(),
			'state_code' => $this->getStateCode()
		);
	}

}
?>