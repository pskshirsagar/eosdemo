<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceAvailabilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceAvailabilities extends CEosPluralBase {

	/**
	 * @return CMaintenanceAvailability[]
	 */
	public static function fetchMaintenanceAvailabilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceAvailability', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceAvailability
	 */
	public static function fetchMaintenanceAvailability( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceAvailability', $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_availabilities', $objDatabase );
	}

	public static function fetchMaintenanceAvailabilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailability( sprintf( 'SELECT * FROM maintenance_availabilities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceAvailabilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilities( sprintf( 'SELECT * FROM maintenance_availabilities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceAvailabilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceAvailabilities( sprintf( 'SELECT * FROM maintenance_availabilities WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>