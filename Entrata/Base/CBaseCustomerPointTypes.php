<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPointTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerPointTypes extends CEosPluralBase {

	/**
	 * @return CCustomerPointType[]
	 */
	public static function fetchCustomerPointTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerPointType::class, $objDatabase );
	}

	/**
	 * @return CCustomerPointType
	 */
	public static function fetchCustomerPointType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerPointType::class, $objDatabase );
	}

	public static function fetchCustomerPointTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_point_types', $objDatabase );
	}

	public static function fetchCustomerPointTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerPointType( sprintf( 'SELECT * FROM customer_point_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>