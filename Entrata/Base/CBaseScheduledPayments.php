<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPayments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledPayments extends CEosPluralBase {

	/**
	 * @return CScheduledPayment[]
	 */
	public static function fetchScheduledPayments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScheduledPayment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledPayment
	 */
	public static function fetchScheduledPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledPayment::class, $objDatabase );
	}

	public static function fetchScheduledPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'view_scheduled_payments', $objDatabase );
	}

	public static function fetchScheduledPaymentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayment( sprintf( 'SELECT * FROM view_scheduled_payments WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE payment_type_id = %d AND cid = %d', $intPaymentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByScheduledPaymentTypeIdByCid( $intScheduledPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE scheduled_payment_type_id = %d AND cid = %d', $intScheduledPaymentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByScheduledPaymentFrequencyIdByCid( $intScheduledPaymentFrequencyId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE scheduled_payment_frequency_id = %d AND cid = %d', $intScheduledPaymentFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByPsProductOptionIdByCid( $intPsProductOptionId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE ps_product_option_id = %d AND cid = %d', $intPsProductOptionId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByCompanyCharityIdByCid( $intCompanyCharityId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE company_charity_id = %d AND cid = %d', $intCompanyCharityId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByAccountVerificationLogIdByCid( $intAccountVerificationLogId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE account_verification_log_id = %d AND cid = %d', $intAccountVerificationLogId, $intCid ), $objDatabase );
	}

	public static function fetchScheduledPaymentsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( sprintf( 'SELECT * FROM view_scheduled_payments WHERE check_account_type_id = %d AND cid = %d', $intCheckAccountTypeId, $intCid ), $objDatabase );
	}

}
?>