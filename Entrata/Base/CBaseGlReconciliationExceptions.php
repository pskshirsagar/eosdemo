<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationExceptions extends CEosPluralBase {

	/**
	 * @return CGlReconciliationException[]
	 */
	public static function fetchGlReconciliationExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlReconciliationException', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlReconciliationException
	 */
	public static function fetchGlReconciliationException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlReconciliationException', $objDatabase );
	}

	public static function fetchGlReconciliationExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_reconciliation_exceptions', $objDatabase );
	}

	public static function fetchGlReconciliationExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationException( sprintf( 'SELECT * FROM gl_reconciliation_exceptions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchGlReconciliationExceptions( sprintf( 'SELECT * FROM gl_reconciliation_exceptions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationExceptionsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationExceptions( sprintf( 'SELECT * FROM gl_reconciliation_exceptions WHERE gl_reconciliation_id = %d AND cid = %d', ( int ) $intGlReconciliationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationExceptionsByGlDetailIdByCid( $intGlDetailId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationExceptions( sprintf( 'SELECT * FROM gl_reconciliation_exceptions WHERE gl_detail_id = %d AND cid = %d', ( int ) $intGlDetailId, ( int ) $intCid ), $objDatabase );
	}

}
?>