<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplicationCustomerDataTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApplicationCustomerDataTypes extends CEosPluralBase {

	/**
	 * @return CPropertyApplicationCustomerDataType[]
	 */
	public static function fetchPropertyApplicationCustomerDataTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyApplicationCustomerDataType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApplicationCustomerDataType
	 */
	public static function fetchPropertyApplicationCustomerDataType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyApplicationCustomerDataType', $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_application_customer_data_types', $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationCustomerDataType( sprintf( 'SELECT * FROM property_application_customer_data_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationCustomerDataTypes( sprintf( 'SELECT * FROM property_application_customer_data_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationCustomerDataTypes( sprintf( 'SELECT * FROM property_application_customer_data_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypesByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationCustomerDataTypes( sprintf( 'SELECT * FROM property_application_customer_data_types WHERE company_application_id = %d AND cid = %d', ( int ) $intCompanyApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypesByCustomerDataTypeIdByCid( $intCustomerDataTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationCustomerDataTypes( sprintf( 'SELECT * FROM property_application_customer_data_types WHERE customer_data_type_id = %d AND cid = %d', ( int ) $intCustomerDataTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>