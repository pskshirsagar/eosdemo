<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportConfigurations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApExportConfigurations extends CEosPluralBase {

	/**
	 * @return CApExportConfiguration[]
	 */
	public static function fetchApExportConfigurations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApExportConfiguration', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApExportConfiguration
	 */
	public static function fetchApExportConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApExportConfiguration', $objDatabase );
	}

	public static function fetchApExportConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_export_configurations', $objDatabase );
	}

	public static function fetchApExportConfigurationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApExportConfiguration( sprintf( 'SELECT * FROM ap_export_configurations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApExportConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchApExportConfigurations( sprintf( 'SELECT * FROM ap_export_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApExportConfigurationsByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApExportConfigurations( sprintf( 'SELECT * FROM ap_export_configurations WHERE ap_export_batch_id = %d AND cid = %d', ( int ) $intApExportBatchId, ( int ) $intCid ), $objDatabase );
	}

}
?>