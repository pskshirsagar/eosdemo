<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultApPayeeTerms
 * Do not add any new functions to this class.
 */

class CBaseDefaultApPayeeTerms extends CEosPluralBase {

	/**
	 * @return CDefaultApPayeeTerm[]
	 */
	public static function fetchDefaultApPayeeTerms( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultApPayeeTerm::class, $objDatabase );
	}

	/**
	 * @return CDefaultApPayeeTerm
	 */
	public static function fetchDefaultApPayeeTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultApPayeeTerm::class, $objDatabase );
	}

	public static function fetchDefaultApPayeeTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_ap_payee_terms', $objDatabase );
	}

	public static function fetchDefaultApPayeeTermById( $intId, $objDatabase ) {
		return self::fetchDefaultApPayeeTerm( sprintf( 'SELECT * FROM default_ap_payee_terms WHERE id = %d', $intId ), $objDatabase );
	}

}
?>