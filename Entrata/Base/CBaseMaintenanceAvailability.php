<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceAvailability extends CEosSingularBase {

	const TABLE_NAME = 'public.maintenance_availabilities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMondayAm;
	protected $m_intMondayPm;
	protected $m_intTuesdayAm;
	protected $m_intTuesdayPm;
	protected $m_intWednesdayAm;
	protected $m_intWednesdayPm;
	protected $m_intThursdayAm;
	protected $m_intThursdayPm;
	protected $m_intFridayAm;
	protected $m_intFridayPm;
	protected $m_intSaturdayAm;
	protected $m_intSaturdayPm;
	protected $m_intSundayAm;
	protected $m_intSundayPm;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMondayAm = '1';
		$this->m_intMondayPm = '1';
		$this->m_intTuesdayAm = '1';
		$this->m_intTuesdayPm = '1';
		$this->m_intWednesdayAm = '1';
		$this->m_intWednesdayPm = '1';
		$this->m_intThursdayAm = '1';
		$this->m_intThursdayPm = '1';
		$this->m_intFridayAm = '1';
		$this->m_intFridayPm = '1';
		$this->m_intSaturdayAm = '0';
		$this->m_intSaturdayPm = '0';
		$this->m_intSundayAm = '0';
		$this->m_intSundayPm = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['monday_am'] ) && $boolDirectSet ) $this->set( 'm_intMondayAm', trim( $arrValues['monday_am'] ) ); elseif( isset( $arrValues['monday_am'] ) ) $this->setMondayAm( $arrValues['monday_am'] );
		if( isset( $arrValues['monday_pm'] ) && $boolDirectSet ) $this->set( 'm_intMondayPm', trim( $arrValues['monday_pm'] ) ); elseif( isset( $arrValues['monday_pm'] ) ) $this->setMondayPm( $arrValues['monday_pm'] );
		if( isset( $arrValues['tuesday_am'] ) && $boolDirectSet ) $this->set( 'm_intTuesdayAm', trim( $arrValues['tuesday_am'] ) ); elseif( isset( $arrValues['tuesday_am'] ) ) $this->setTuesdayAm( $arrValues['tuesday_am'] );
		if( isset( $arrValues['tuesday_pm'] ) && $boolDirectSet ) $this->set( 'm_intTuesdayPm', trim( $arrValues['tuesday_pm'] ) ); elseif( isset( $arrValues['tuesday_pm'] ) ) $this->setTuesdayPm( $arrValues['tuesday_pm'] );
		if( isset( $arrValues['wednesday_am'] ) && $boolDirectSet ) $this->set( 'm_intWednesdayAm', trim( $arrValues['wednesday_am'] ) ); elseif( isset( $arrValues['wednesday_am'] ) ) $this->setWednesdayAm( $arrValues['wednesday_am'] );
		if( isset( $arrValues['wednesday_pm'] ) && $boolDirectSet ) $this->set( 'm_intWednesdayPm', trim( $arrValues['wednesday_pm'] ) ); elseif( isset( $arrValues['wednesday_pm'] ) ) $this->setWednesdayPm( $arrValues['wednesday_pm'] );
		if( isset( $arrValues['thursday_am'] ) && $boolDirectSet ) $this->set( 'm_intThursdayAm', trim( $arrValues['thursday_am'] ) ); elseif( isset( $arrValues['thursday_am'] ) ) $this->setThursdayAm( $arrValues['thursday_am'] );
		if( isset( $arrValues['thursday_pm'] ) && $boolDirectSet ) $this->set( 'm_intThursdayPm', trim( $arrValues['thursday_pm'] ) ); elseif( isset( $arrValues['thursday_pm'] ) ) $this->setThursdayPm( $arrValues['thursday_pm'] );
		if( isset( $arrValues['friday_am'] ) && $boolDirectSet ) $this->set( 'm_intFridayAm', trim( $arrValues['friday_am'] ) ); elseif( isset( $arrValues['friday_am'] ) ) $this->setFridayAm( $arrValues['friday_am'] );
		if( isset( $arrValues['friday_pm'] ) && $boolDirectSet ) $this->set( 'm_intFridayPm', trim( $arrValues['friday_pm'] ) ); elseif( isset( $arrValues['friday_pm'] ) ) $this->setFridayPm( $arrValues['friday_pm'] );
		if( isset( $arrValues['saturday_am'] ) && $boolDirectSet ) $this->set( 'm_intSaturdayAm', trim( $arrValues['saturday_am'] ) ); elseif( isset( $arrValues['saturday_am'] ) ) $this->setSaturdayAm( $arrValues['saturday_am'] );
		if( isset( $arrValues['saturday_pm'] ) && $boolDirectSet ) $this->set( 'm_intSaturdayPm', trim( $arrValues['saturday_pm'] ) ); elseif( isset( $arrValues['saturday_pm'] ) ) $this->setSaturdayPm( $arrValues['saturday_pm'] );
		if( isset( $arrValues['sunday_am'] ) && $boolDirectSet ) $this->set( 'm_intSundayAm', trim( $arrValues['sunday_am'] ) ); elseif( isset( $arrValues['sunday_am'] ) ) $this->setSundayAm( $arrValues['sunday_am'] );
		if( isset( $arrValues['sunday_pm'] ) && $boolDirectSet ) $this->set( 'm_intSundayPm', trim( $arrValues['sunday_pm'] ) ); elseif( isset( $arrValues['sunday_pm'] ) ) $this->setSundayPm( $arrValues['sunday_pm'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMondayAm( $intMondayAm ) {
		$this->set( 'm_intMondayAm', CStrings::strToIntDef( $intMondayAm, NULL, false ) );
	}

	public function getMondayAm() {
		return $this->m_intMondayAm;
	}

	public function sqlMondayAm() {
		return ( true == isset( $this->m_intMondayAm ) ) ? ( string ) $this->m_intMondayAm : '1';
	}

	public function setMondayPm( $intMondayPm ) {
		$this->set( 'm_intMondayPm', CStrings::strToIntDef( $intMondayPm, NULL, false ) );
	}

	public function getMondayPm() {
		return $this->m_intMondayPm;
	}

	public function sqlMondayPm() {
		return ( true == isset( $this->m_intMondayPm ) ) ? ( string ) $this->m_intMondayPm : '1';
	}

	public function setTuesdayAm( $intTuesdayAm ) {
		$this->set( 'm_intTuesdayAm', CStrings::strToIntDef( $intTuesdayAm, NULL, false ) );
	}

	public function getTuesdayAm() {
		return $this->m_intTuesdayAm;
	}

	public function sqlTuesdayAm() {
		return ( true == isset( $this->m_intTuesdayAm ) ) ? ( string ) $this->m_intTuesdayAm : '1';
	}

	public function setTuesdayPm( $intTuesdayPm ) {
		$this->set( 'm_intTuesdayPm', CStrings::strToIntDef( $intTuesdayPm, NULL, false ) );
	}

	public function getTuesdayPm() {
		return $this->m_intTuesdayPm;
	}

	public function sqlTuesdayPm() {
		return ( true == isset( $this->m_intTuesdayPm ) ) ? ( string ) $this->m_intTuesdayPm : '1';
	}

	public function setWednesdayAm( $intWednesdayAm ) {
		$this->set( 'm_intWednesdayAm', CStrings::strToIntDef( $intWednesdayAm, NULL, false ) );
	}

	public function getWednesdayAm() {
		return $this->m_intWednesdayAm;
	}

	public function sqlWednesdayAm() {
		return ( true == isset( $this->m_intWednesdayAm ) ) ? ( string ) $this->m_intWednesdayAm : '1';
	}

	public function setWednesdayPm( $intWednesdayPm ) {
		$this->set( 'm_intWednesdayPm', CStrings::strToIntDef( $intWednesdayPm, NULL, false ) );
	}

	public function getWednesdayPm() {
		return $this->m_intWednesdayPm;
	}

	public function sqlWednesdayPm() {
		return ( true == isset( $this->m_intWednesdayPm ) ) ? ( string ) $this->m_intWednesdayPm : '1';
	}

	public function setThursdayAm( $intThursdayAm ) {
		$this->set( 'm_intThursdayAm', CStrings::strToIntDef( $intThursdayAm, NULL, false ) );
	}

	public function getThursdayAm() {
		return $this->m_intThursdayAm;
	}

	public function sqlThursdayAm() {
		return ( true == isset( $this->m_intThursdayAm ) ) ? ( string ) $this->m_intThursdayAm : '1';
	}

	public function setThursdayPm( $intThursdayPm ) {
		$this->set( 'm_intThursdayPm', CStrings::strToIntDef( $intThursdayPm, NULL, false ) );
	}

	public function getThursdayPm() {
		return $this->m_intThursdayPm;
	}

	public function sqlThursdayPm() {
		return ( true == isset( $this->m_intThursdayPm ) ) ? ( string ) $this->m_intThursdayPm : '1';
	}

	public function setFridayAm( $intFridayAm ) {
		$this->set( 'm_intFridayAm', CStrings::strToIntDef( $intFridayAm, NULL, false ) );
	}

	public function getFridayAm() {
		return $this->m_intFridayAm;
	}

	public function sqlFridayAm() {
		return ( true == isset( $this->m_intFridayAm ) ) ? ( string ) $this->m_intFridayAm : '1';
	}

	public function setFridayPm( $intFridayPm ) {
		$this->set( 'm_intFridayPm', CStrings::strToIntDef( $intFridayPm, NULL, false ) );
	}

	public function getFridayPm() {
		return $this->m_intFridayPm;
	}

	public function sqlFridayPm() {
		return ( true == isset( $this->m_intFridayPm ) ) ? ( string ) $this->m_intFridayPm : '1';
	}

	public function setSaturdayAm( $intSaturdayAm ) {
		$this->set( 'm_intSaturdayAm', CStrings::strToIntDef( $intSaturdayAm, NULL, false ) );
	}

	public function getSaturdayAm() {
		return $this->m_intSaturdayAm;
	}

	public function sqlSaturdayAm() {
		return ( true == isset( $this->m_intSaturdayAm ) ) ? ( string ) $this->m_intSaturdayAm : '0';
	}

	public function setSaturdayPm( $intSaturdayPm ) {
		$this->set( 'm_intSaturdayPm', CStrings::strToIntDef( $intSaturdayPm, NULL, false ) );
	}

	public function getSaturdayPm() {
		return $this->m_intSaturdayPm;
	}

	public function sqlSaturdayPm() {
		return ( true == isset( $this->m_intSaturdayPm ) ) ? ( string ) $this->m_intSaturdayPm : '0';
	}

	public function setSundayAm( $intSundayAm ) {
		$this->set( 'm_intSundayAm', CStrings::strToIntDef( $intSundayAm, NULL, false ) );
	}

	public function getSundayAm() {
		return $this->m_intSundayAm;
	}

	public function sqlSundayAm() {
		return ( true == isset( $this->m_intSundayAm ) ) ? ( string ) $this->m_intSundayAm : '0';
	}

	public function setSundayPm( $intSundayPm ) {
		$this->set( 'm_intSundayPm', CStrings::strToIntDef( $intSundayPm, NULL, false ) );
	}

	public function getSundayPm() {
		return $this->m_intSundayPm;
	}

	public function sqlSundayPm() {
		return ( true == isset( $this->m_intSundayPm ) ) ? ( string ) $this->m_intSundayPm : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, monday_am, monday_pm, tuesday_am, tuesday_pm, wednesday_am, wednesday_pm, thursday_am, thursday_pm, friday_am, friday_pm, saturday_am, saturday_pm, sunday_am, sunday_pm, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlMondayAm() . ', ' .
 						$this->sqlMondayPm() . ', ' .
 						$this->sqlTuesdayAm() . ', ' .
 						$this->sqlTuesdayPm() . ', ' .
 						$this->sqlWednesdayAm() . ', ' .
 						$this->sqlWednesdayPm() . ', ' .
 						$this->sqlThursdayAm() . ', ' .
 						$this->sqlThursdayPm() . ', ' .
 						$this->sqlFridayAm() . ', ' .
 						$this->sqlFridayPm() . ', ' .
 						$this->sqlSaturdayAm() . ', ' .
 						$this->sqlSaturdayPm() . ', ' .
 						$this->sqlSundayAm() . ', ' .
 						$this->sqlSundayPm() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monday_am = ' . $this->sqlMondayAm() . ','; } elseif( true == array_key_exists( 'MondayAm', $this->getChangedColumns() ) ) { $strSql .= ' monday_am = ' . $this->sqlMondayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monday_pm = ' . $this->sqlMondayPm() . ','; } elseif( true == array_key_exists( 'MondayPm', $this->getChangedColumns() ) ) { $strSql .= ' monday_pm = ' . $this->sqlMondayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tuesday_am = ' . $this->sqlTuesdayAm() . ','; } elseif( true == array_key_exists( 'TuesdayAm', $this->getChangedColumns() ) ) { $strSql .= ' tuesday_am = ' . $this->sqlTuesdayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tuesday_pm = ' . $this->sqlTuesdayPm() . ','; } elseif( true == array_key_exists( 'TuesdayPm', $this->getChangedColumns() ) ) { $strSql .= ' tuesday_pm = ' . $this->sqlTuesdayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wednesday_am = ' . $this->sqlWednesdayAm() . ','; } elseif( true == array_key_exists( 'WednesdayAm', $this->getChangedColumns() ) ) { $strSql .= ' wednesday_am = ' . $this->sqlWednesdayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wednesday_pm = ' . $this->sqlWednesdayPm() . ','; } elseif( true == array_key_exists( 'WednesdayPm', $this->getChangedColumns() ) ) { $strSql .= ' wednesday_pm = ' . $this->sqlWednesdayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thursday_am = ' . $this->sqlThursdayAm() . ','; } elseif( true == array_key_exists( 'ThursdayAm', $this->getChangedColumns() ) ) { $strSql .= ' thursday_am = ' . $this->sqlThursdayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thursday_pm = ' . $this->sqlThursdayPm() . ','; } elseif( true == array_key_exists( 'ThursdayPm', $this->getChangedColumns() ) ) { $strSql .= ' thursday_pm = ' . $this->sqlThursdayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' friday_am = ' . $this->sqlFridayAm() . ','; } elseif( true == array_key_exists( 'FridayAm', $this->getChangedColumns() ) ) { $strSql .= ' friday_am = ' . $this->sqlFridayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' friday_pm = ' . $this->sqlFridayPm() . ','; } elseif( true == array_key_exists( 'FridayPm', $this->getChangedColumns() ) ) { $strSql .= ' friday_pm = ' . $this->sqlFridayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' saturday_am = ' . $this->sqlSaturdayAm() . ','; } elseif( true == array_key_exists( 'SaturdayAm', $this->getChangedColumns() ) ) { $strSql .= ' saturday_am = ' . $this->sqlSaturdayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' saturday_pm = ' . $this->sqlSaturdayPm() . ','; } elseif( true == array_key_exists( 'SaturdayPm', $this->getChangedColumns() ) ) { $strSql .= ' saturday_pm = ' . $this->sqlSaturdayPm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sunday_am = ' . $this->sqlSundayAm() . ','; } elseif( true == array_key_exists( 'SundayAm', $this->getChangedColumns() ) ) { $strSql .= ' sunday_am = ' . $this->sqlSundayAm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sunday_pm = ' . $this->sqlSundayPm() . ','; } elseif( true == array_key_exists( 'SundayPm', $this->getChangedColumns() ) ) { $strSql .= ' sunday_pm = ' . $this->sqlSundayPm() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'monday_am' => $this->getMondayAm(),
			'monday_pm' => $this->getMondayPm(),
			'tuesday_am' => $this->getTuesdayAm(),
			'tuesday_pm' => $this->getTuesdayPm(),
			'wednesday_am' => $this->getWednesdayAm(),
			'wednesday_pm' => $this->getWednesdayPm(),
			'thursday_am' => $this->getThursdayAm(),
			'thursday_pm' => $this->getThursdayPm(),
			'friday_am' => $this->getFridayAm(),
			'friday_pm' => $this->getFridayPm(),
			'saturday_am' => $this->getSaturdayAm(),
			'saturday_pm' => $this->getSaturdayPm(),
			'sunday_am' => $this->getSundayAm(),
			'sunday_pm' => $this->getSundayPm(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>