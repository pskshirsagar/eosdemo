<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentMergeField extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.document_merge_fields';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyMergeFieldId;
	protected $m_intDefaultMergeFieldId;
	protected $m_intBlockDocumentMergeFieldId;
	protected $m_intDocumentId;
	protected $m_intDocumentAddendaId;
	protected $m_intPropertyId;
	protected $m_intMergeFieldGroupId;
	protected $m_intExternalMergeFieldGroupId;
	protected $m_strField;
	protected $m_strExternalFieldName;
	protected $m_strValidationType;
	protected $m_intXPos;
	protected $m_intYPos;
	protected $m_intHeight;
	protected $m_intWidth;
	protected $m_strUniqueLayoutKey;
	protected $m_intMaxCharLimit;
	protected $m_intPageNumber;
	protected $m_intFontSize;
	protected $m_strFontStyle;
	protected $m_intStartOrdinal;
	protected $m_intEndOrdinal;
	protected $m_strRepeatDirection;
	protected $m_strHorizontalAlign;
	protected $m_intRowNumber;
	protected $m_strDataType;
	protected $m_strDescription;
	protected $m_strDefaultValue;
	protected $m_intAllowUserUpdate;
	protected $m_intAllowAdminUpdate;
	protected $m_intForceOriginUpdate;
	protected $m_intShowInMergeDisplay;
	protected $m_intIsBlockField;
	protected $m_intIsRequired;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMergeFieldSubGroupId;

	public function __construct() {
		parent::__construct();

		$this->m_strDataType = 'text';
		$this->m_intAllowUserUpdate = '0';
		$this->m_intAllowAdminUpdate = '0';
		$this->m_intForceOriginUpdate = '0';
		$this->m_intShowInMergeDisplay = '0';
		$this->m_intIsBlockField = '0';
		$this->m_intIsRequired = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMergeFieldId', trim( $arrValues['company_merge_field_id'] ) ); elseif( isset( $arrValues['company_merge_field_id'] ) ) $this->setCompanyMergeFieldId( $arrValues['company_merge_field_id'] );
		if( isset( $arrValues['default_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMergeFieldId', trim( $arrValues['default_merge_field_id'] ) ); elseif( isset( $arrValues['default_merge_field_id'] ) ) $this->setDefaultMergeFieldId( $arrValues['default_merge_field_id'] );
		if( isset( $arrValues['block_document_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockDocumentMergeFieldId', trim( $arrValues['block_document_merge_field_id'] ) ); elseif( isset( $arrValues['block_document_merge_field_id'] ) ) $this->setBlockDocumentMergeFieldId( $arrValues['block_document_merge_field_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['document_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentAddendaId', trim( $arrValues['document_addenda_id'] ) ); elseif( isset( $arrValues['document_addenda_id'] ) ) $this->setDocumentAddendaId( $arrValues['document_addenda_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldGroupId', trim( $arrValues['merge_field_group_id'] ) ); elseif( isset( $arrValues['merge_field_group_id'] ) ) $this->setMergeFieldGroupId( $arrValues['merge_field_group_id'] );
		if( isset( $arrValues['external_merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalMergeFieldGroupId', trim( $arrValues['external_merge_field_group_id'] ) ); elseif( isset( $arrValues['external_merge_field_group_id'] ) ) $this->setExternalMergeFieldGroupId( $arrValues['external_merge_field_group_id'] );
		if( isset( $arrValues['field'] ) && $boolDirectSet ) $this->set( 'm_strField', trim( $arrValues['field'] ) ); elseif( isset( $arrValues['field'] ) ) $this->setField( $arrValues['field'] );
		if( isset( $arrValues['external_field_name'] ) && $boolDirectSet ) $this->set( 'm_strExternalFieldName', trim( $arrValues['external_field_name'] ) ); elseif( isset( $arrValues['external_field_name'] ) ) $this->setExternalFieldName( $arrValues['external_field_name'] );
		if( isset( $arrValues['validation_type'] ) && $boolDirectSet ) $this->set( 'm_strValidationType', trim( $arrValues['validation_type'] ) ); elseif( isset( $arrValues['validation_type'] ) ) $this->setValidationType( $arrValues['validation_type'] );
		if( isset( $arrValues['x_pos'] ) && $boolDirectSet ) $this->set( 'm_intXPos', trim( $arrValues['x_pos'] ) ); elseif( isset( $arrValues['x_pos'] ) ) $this->setXPos( $arrValues['x_pos'] );
		if( isset( $arrValues['y_pos'] ) && $boolDirectSet ) $this->set( 'm_intYPos', trim( $arrValues['y_pos'] ) ); elseif( isset( $arrValues['y_pos'] ) ) $this->setYPos( $arrValues['y_pos'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		if( isset( $arrValues['width'] ) && $boolDirectSet ) $this->set( 'm_intWidth', trim( $arrValues['width'] ) ); elseif( isset( $arrValues['width'] ) ) $this->setWidth( $arrValues['width'] );
		if( isset( $arrValues['unique_layout_key'] ) && $boolDirectSet ) $this->set( 'm_strUniqueLayoutKey', trim( $arrValues['unique_layout_key'] ) ); elseif( isset( $arrValues['unique_layout_key'] ) ) $this->setUniqueLayoutKey( $arrValues['unique_layout_key'] );
		if( isset( $arrValues['max_char_limit'] ) && $boolDirectSet ) $this->set( 'm_intMaxCharLimit', trim( $arrValues['max_char_limit'] ) ); elseif( isset( $arrValues['max_char_limit'] ) ) $this->setMaxCharLimit( $arrValues['max_char_limit'] );
		if( isset( $arrValues['page_number'] ) && $boolDirectSet ) $this->set( 'm_intPageNumber', trim( $arrValues['page_number'] ) ); elseif( isset( $arrValues['page_number'] ) ) $this->setPageNumber( $arrValues['page_number'] );
		if( isset( $arrValues['font_size'] ) && $boolDirectSet ) $this->set( 'm_intFontSize', trim( $arrValues['font_size'] ) ); elseif( isset( $arrValues['font_size'] ) ) $this->setFontSize( $arrValues['font_size'] );
		if( isset( $arrValues['font_style'] ) && $boolDirectSet ) $this->set( 'm_strFontStyle', trim( $arrValues['font_style'] ) ); elseif( isset( $arrValues['font_style'] ) ) $this->setFontStyle( $arrValues['font_style'] );
		if( isset( $arrValues['start_ordinal'] ) && $boolDirectSet ) $this->set( 'm_intStartOrdinal', trim( $arrValues['start_ordinal'] ) ); elseif( isset( $arrValues['start_ordinal'] ) ) $this->setStartOrdinal( $arrValues['start_ordinal'] );
		if( isset( $arrValues['end_ordinal'] ) && $boolDirectSet ) $this->set( 'm_intEndOrdinal', trim( $arrValues['end_ordinal'] ) ); elseif( isset( $arrValues['end_ordinal'] ) ) $this->setEndOrdinal( $arrValues['end_ordinal'] );
		if( isset( $arrValues['repeat_direction'] ) && $boolDirectSet ) $this->set( 'm_strRepeatDirection', trim( $arrValues['repeat_direction'] ) ); elseif( isset( $arrValues['repeat_direction'] ) ) $this->setRepeatDirection( $arrValues['repeat_direction'] );
		if( isset( $arrValues['horizontal_align'] ) && $boolDirectSet ) $this->set( 'm_strHorizontalAlign', trim( $arrValues['horizontal_align'] ) ); elseif( isset( $arrValues['horizontal_align'] ) ) $this->setHorizontalAlign( $arrValues['horizontal_align'] );
		if( isset( $arrValues['row_number'] ) && $boolDirectSet ) $this->set( 'm_intRowNumber', trim( $arrValues['row_number'] ) ); elseif( isset( $arrValues['row_number'] ) ) $this->setRowNumber( $arrValues['row_number'] );
		if( isset( $arrValues['data_type'] ) && $boolDirectSet ) $this->set( 'm_strDataType', trim( $arrValues['data_type'] ) ); elseif( isset( $arrValues['data_type'] ) ) $this->setDataType( $arrValues['data_type'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['default_value'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDefaultValue', trim( $arrValues['default_value'] ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( $arrValues['default_value'] );
		if( isset( $arrValues['allow_user_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowUserUpdate', trim( $arrValues['allow_user_update'] ) ); elseif( isset( $arrValues['allow_user_update'] ) ) $this->setAllowUserUpdate( $arrValues['allow_user_update'] );
		if( isset( $arrValues['allow_admin_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowAdminUpdate', trim( $arrValues['allow_admin_update'] ) ); elseif( isset( $arrValues['allow_admin_update'] ) ) $this->setAllowAdminUpdate( $arrValues['allow_admin_update'] );
		if( isset( $arrValues['force_origin_update'] ) && $boolDirectSet ) $this->set( 'm_intForceOriginUpdate', trim( $arrValues['force_origin_update'] ) ); elseif( isset( $arrValues['force_origin_update'] ) ) $this->setForceOriginUpdate( $arrValues['force_origin_update'] );
		if( isset( $arrValues['show_in_merge_display'] ) && $boolDirectSet ) $this->set( 'm_intShowInMergeDisplay', trim( $arrValues['show_in_merge_display'] ) ); elseif( isset( $arrValues['show_in_merge_display'] ) ) $this->setShowInMergeDisplay( $arrValues['show_in_merge_display'] );
		if( isset( $arrValues['is_block_field'] ) && $boolDirectSet ) $this->set( 'm_intIsBlockField', trim( $arrValues['is_block_field'] ) ); elseif( isset( $arrValues['is_block_field'] ) ) $this->setIsBlockField( $arrValues['is_block_field'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['merge_field_sub_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldSubGroupId', trim( $arrValues['merge_field_sub_group_id'] ) ); elseif( isset( $arrValues['merge_field_sub_group_id'] ) ) $this->setMergeFieldSubGroupId( $arrValues['merge_field_sub_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyMergeFieldId( $intCompanyMergeFieldId ) {
		$this->set( 'm_intCompanyMergeFieldId', CStrings::strToIntDef( $intCompanyMergeFieldId, NULL, false ) );
	}

	public function getCompanyMergeFieldId() {
		return $this->m_intCompanyMergeFieldId;
	}

	public function sqlCompanyMergeFieldId() {
		return ( true == isset( $this->m_intCompanyMergeFieldId ) ) ? ( string ) $this->m_intCompanyMergeFieldId : 'NULL';
	}

	public function setDefaultMergeFieldId( $intDefaultMergeFieldId ) {
		$this->set( 'm_intDefaultMergeFieldId', CStrings::strToIntDef( $intDefaultMergeFieldId, NULL, false ) );
	}

	public function getDefaultMergeFieldId() {
		return $this->m_intDefaultMergeFieldId;
	}

	public function sqlDefaultMergeFieldId() {
		return ( true == isset( $this->m_intDefaultMergeFieldId ) ) ? ( string ) $this->m_intDefaultMergeFieldId : 'NULL';
	}

	public function setBlockDocumentMergeFieldId( $intBlockDocumentMergeFieldId ) {
		$this->set( 'm_intBlockDocumentMergeFieldId', CStrings::strToIntDef( $intBlockDocumentMergeFieldId, NULL, false ) );
	}

	public function getBlockDocumentMergeFieldId() {
		return $this->m_intBlockDocumentMergeFieldId;
	}

	public function sqlBlockDocumentMergeFieldId() {
		return ( true == isset( $this->m_intBlockDocumentMergeFieldId ) ) ? ( string ) $this->m_intBlockDocumentMergeFieldId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setDocumentAddendaId( $intDocumentAddendaId ) {
		$this->set( 'm_intDocumentAddendaId', CStrings::strToIntDef( $intDocumentAddendaId, NULL, false ) );
	}

	public function getDocumentAddendaId() {
		return $this->m_intDocumentAddendaId;
	}

	public function sqlDocumentAddendaId() {
		return ( true == isset( $this->m_intDocumentAddendaId ) ) ? ( string ) $this->m_intDocumentAddendaId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMergeFieldGroupId( $intMergeFieldGroupId ) {
		$this->set( 'm_intMergeFieldGroupId', CStrings::strToIntDef( $intMergeFieldGroupId, NULL, false ) );
	}

	public function getMergeFieldGroupId() {
		return $this->m_intMergeFieldGroupId;
	}

	public function sqlMergeFieldGroupId() {
		return ( true == isset( $this->m_intMergeFieldGroupId ) ) ? ( string ) $this->m_intMergeFieldGroupId : 'NULL';
	}

	public function setExternalMergeFieldGroupId( $intExternalMergeFieldGroupId ) {
		$this->set( 'm_intExternalMergeFieldGroupId', CStrings::strToIntDef( $intExternalMergeFieldGroupId, NULL, false ) );
	}

	public function getExternalMergeFieldGroupId() {
		return $this->m_intExternalMergeFieldGroupId;
	}

	public function sqlExternalMergeFieldGroupId() {
		return ( true == isset( $this->m_intExternalMergeFieldGroupId ) ) ? ( string ) $this->m_intExternalMergeFieldGroupId : 'NULL';
	}

	public function setField( $strField ) {
		$this->set( 'm_strField', CStrings::strTrimDef( $strField, 64, NULL, true ) );
	}

	public function getField() {
		return $this->m_strField;
	}

	public function sqlField() {
		return ( true == isset( $this->m_strField ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strField ) : '\'' . addslashes( $this->m_strField ) . '\'' ) : 'NULL';
	}

	public function setExternalFieldName( $strExternalFieldName ) {
		$this->set( 'm_strExternalFieldName', CStrings::strTrimDef( $strExternalFieldName, 500, NULL, true ) );
	}

	public function getExternalFieldName() {
		return $this->m_strExternalFieldName;
	}

	public function sqlExternalFieldName() {
		return ( true == isset( $this->m_strExternalFieldName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalFieldName ) : '\'' . addslashes( $this->m_strExternalFieldName ) . '\'' ) : 'NULL';
	}

	public function setValidationType( $strValidationType ) {
		$this->set( 'm_strValidationType', CStrings::strTrimDef( $strValidationType, 50, NULL, true ) );
	}

	public function getValidationType() {
		return $this->m_strValidationType;
	}

	public function sqlValidationType() {
		return ( true == isset( $this->m_strValidationType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValidationType ) : '\'' . addslashes( $this->m_strValidationType ) . '\'' ) : 'NULL';
	}

	public function setXPos( $intXPos ) {
		$this->set( 'm_intXPos', CStrings::strToIntDef( $intXPos, NULL, false ) );
	}

	public function getXPos() {
		return $this->m_intXPos;
	}

	public function sqlXPos() {
		return ( true == isset( $this->m_intXPos ) ) ? ( string ) $this->m_intXPos : 'NULL';
	}

	public function setYPos( $intYPos ) {
		$this->set( 'm_intYPos', CStrings::strToIntDef( $intYPos, NULL, false ) );
	}

	public function getYPos() {
		return $this->m_intYPos;
	}

	public function sqlYPos() {
		return ( true == isset( $this->m_intYPos ) ) ? ( string ) $this->m_intYPos : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function setWidth( $intWidth ) {
		$this->set( 'm_intWidth', CStrings::strToIntDef( $intWidth, NULL, false ) );
	}

	public function getWidth() {
		return $this->m_intWidth;
	}

	public function sqlWidth() {
		return ( true == isset( $this->m_intWidth ) ) ? ( string ) $this->m_intWidth : 'NULL';
	}

	public function setUniqueLayoutKey( $strUniqueLayoutKey ) {
		$this->set( 'm_strUniqueLayoutKey', CStrings::strTrimDef( $strUniqueLayoutKey, 255, NULL, true ) );
	}

	public function getUniqueLayoutKey() {
		return $this->m_strUniqueLayoutKey;
	}

	public function sqlUniqueLayoutKey() {
		return ( true == isset( $this->m_strUniqueLayoutKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUniqueLayoutKey ) : '\'' . addslashes( $this->m_strUniqueLayoutKey ) . '\'' ) : 'NULL';
	}

	public function setMaxCharLimit( $intMaxCharLimit ) {
		$this->set( 'm_intMaxCharLimit', CStrings::strToIntDef( $intMaxCharLimit, NULL, false ) );
	}

	public function getMaxCharLimit() {
		return $this->m_intMaxCharLimit;
	}

	public function sqlMaxCharLimit() {
		return ( true == isset( $this->m_intMaxCharLimit ) ) ? ( string ) $this->m_intMaxCharLimit : 'NULL';
	}

	public function setPageNumber( $intPageNumber ) {
		$this->set( 'm_intPageNumber', CStrings::strToIntDef( $intPageNumber, NULL, false ) );
	}

	public function getPageNumber() {
		return $this->m_intPageNumber;
	}

	public function sqlPageNumber() {
		return ( true == isset( $this->m_intPageNumber ) ) ? ( string ) $this->m_intPageNumber : 'NULL';
	}

	public function setFontSize( $intFontSize ) {
		$this->set( 'm_intFontSize', CStrings::strToIntDef( $intFontSize, NULL, false ) );
	}

	public function getFontSize() {
		return $this->m_intFontSize;
	}

	public function sqlFontSize() {
		return ( true == isset( $this->m_intFontSize ) ) ? ( string ) $this->m_intFontSize : 'NULL';
	}

	public function setFontStyle( $strFontStyle ) {
		$this->set( 'm_strFontStyle', CStrings::strTrimDef( $strFontStyle, 64, NULL, true ) );
	}

	public function getFontStyle() {
		return $this->m_strFontStyle;
	}

	public function sqlFontStyle() {
		return ( true == isset( $this->m_strFontStyle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFontStyle ) : '\'' . addslashes( $this->m_strFontStyle ) . '\'' ) : 'NULL';
	}

	public function setStartOrdinal( $intStartOrdinal ) {
		$this->set( 'm_intStartOrdinal', CStrings::strToIntDef( $intStartOrdinal, NULL, false ) );
	}

	public function getStartOrdinal() {
		return $this->m_intStartOrdinal;
	}

	public function sqlStartOrdinal() {
		return ( true == isset( $this->m_intStartOrdinal ) ) ? ( string ) $this->m_intStartOrdinal : 'NULL';
	}

	public function setEndOrdinal( $intEndOrdinal ) {
		$this->set( 'm_intEndOrdinal', CStrings::strToIntDef( $intEndOrdinal, NULL, false ) );
	}

	public function getEndOrdinal() {
		return $this->m_intEndOrdinal;
	}

	public function sqlEndOrdinal() {
		return ( true == isset( $this->m_intEndOrdinal ) ) ? ( string ) $this->m_intEndOrdinal : 'NULL';
	}

	public function setRepeatDirection( $strRepeatDirection ) {
		$this->set( 'm_strRepeatDirection', CStrings::strTrimDef( $strRepeatDirection, 50, NULL, true ) );
	}

	public function getRepeatDirection() {
		return $this->m_strRepeatDirection;
	}

	public function sqlRepeatDirection() {
		return ( true == isset( $this->m_strRepeatDirection ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRepeatDirection ) : '\'' . addslashes( $this->m_strRepeatDirection ) . '\'' ) : 'NULL';
	}

	public function setHorizontalAlign( $strHorizontalAlign ) {
		$this->set( 'm_strHorizontalAlign', CStrings::strTrimDef( $strHorizontalAlign, 50, NULL, true ) );
	}

	public function getHorizontalAlign() {
		return $this->m_strHorizontalAlign;
	}

	public function sqlHorizontalAlign() {
		return ( true == isset( $this->m_strHorizontalAlign ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHorizontalAlign ) : '\'' . addslashes( $this->m_strHorizontalAlign ) . '\'' ) : 'NULL';
	}

	public function setRowNumber( $intRowNumber ) {
		$this->set( 'm_intRowNumber', CStrings::strToIntDef( $intRowNumber, NULL, false ) );
	}

	public function getRowNumber() {
		return $this->m_intRowNumber;
	}

	public function sqlRowNumber() {
		return ( true == isset( $this->m_intRowNumber ) ) ? ( string ) $this->m_intRowNumber : 'NULL';
	}

	public function setDataType( $strDataType ) {
		$this->set( 'm_strDataType', CStrings::strTrimDef( $strDataType, 50, NULL, true ) );
	}

	public function getDataType() {
		return $this->m_strDataType;
	}

	public function sqlDataType() {
		return ( true == isset( $this->m_strDataType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataType ) : '\'' . addslashes( $this->m_strDataType ) . '\'' ) : '\'text\'';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ), $strLocaleCode );
	}

	public function getDefaultValue( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDefaultValue', $strLocaleCode );
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDefaultValue ) : '\'' . addslashes( $this->m_strDefaultValue ) . '\'' ) : 'NULL';
	}

	public function setAllowUserUpdate( $intAllowUserUpdate ) {
		$this->set( 'm_intAllowUserUpdate', CStrings::strToIntDef( $intAllowUserUpdate, NULL, false ) );
	}

	public function getAllowUserUpdate() {
		return $this->m_intAllowUserUpdate;
	}

	public function sqlAllowUserUpdate() {
		return ( true == isset( $this->m_intAllowUserUpdate ) ) ? ( string ) $this->m_intAllowUserUpdate : '0';
	}

	public function setAllowAdminUpdate( $intAllowAdminUpdate ) {
		$this->set( 'm_intAllowAdminUpdate', CStrings::strToIntDef( $intAllowAdminUpdate, NULL, false ) );
	}

	public function getAllowAdminUpdate() {
		return $this->m_intAllowAdminUpdate;
	}

	public function sqlAllowAdminUpdate() {
		return ( true == isset( $this->m_intAllowAdminUpdate ) ) ? ( string ) $this->m_intAllowAdminUpdate : '0';
	}

	public function setForceOriginUpdate( $intForceOriginUpdate ) {
		$this->set( 'm_intForceOriginUpdate', CStrings::strToIntDef( $intForceOriginUpdate, NULL, false ) );
	}

	public function getForceOriginUpdate() {
		return $this->m_intForceOriginUpdate;
	}

	public function sqlForceOriginUpdate() {
		return ( true == isset( $this->m_intForceOriginUpdate ) ) ? ( string ) $this->m_intForceOriginUpdate : '0';
	}

	public function setShowInMergeDisplay( $intShowInMergeDisplay ) {
		$this->set( 'm_intShowInMergeDisplay', CStrings::strToIntDef( $intShowInMergeDisplay, NULL, false ) );
	}

	public function getShowInMergeDisplay() {
		return $this->m_intShowInMergeDisplay;
	}

	public function sqlShowInMergeDisplay() {
		return ( true == isset( $this->m_intShowInMergeDisplay ) ) ? ( string ) $this->m_intShowInMergeDisplay : '0';
	}

	public function setIsBlockField( $intIsBlockField ) {
		$this->set( 'm_intIsBlockField', CStrings::strToIntDef( $intIsBlockField, NULL, false ) );
	}

	public function getIsBlockField() {
		return $this->m_intIsBlockField;
	}

	public function sqlIsBlockField() {
		return ( true == isset( $this->m_intIsBlockField ) ) ? ( string ) $this->m_intIsBlockField : '0';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setMergeFieldSubGroupId( $intMergeFieldSubGroupId ) {
		$this->set( 'm_intMergeFieldSubGroupId', CStrings::strToIntDef( $intMergeFieldSubGroupId, NULL, false ) );
	}

	public function getMergeFieldSubGroupId() {
		return $this->m_intMergeFieldSubGroupId;
	}

	public function sqlMergeFieldSubGroupId() {
		return ( true == isset( $this->m_intMergeFieldSubGroupId ) ) ? ( string ) $this->m_intMergeFieldSubGroupId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_merge_field_id, default_merge_field_id, block_document_merge_field_id, document_id, document_addenda_id, property_id, merge_field_group_id, external_merge_field_group_id, field, external_field_name, validation_type, x_pos, y_pos, height, width, unique_layout_key, max_char_limit, page_number, font_size, font_style, start_ordinal, end_ordinal, repeat_direction, horizontal_align, row_number, data_type, description, default_value, allow_user_update, allow_admin_update, force_origin_update, show_in_merge_display, is_block_field, is_required, order_num, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, details, merge_field_sub_group_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyMergeFieldId() . ', ' .
						$this->sqlDefaultMergeFieldId() . ', ' .
						$this->sqlBlockDocumentMergeFieldId() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlDocumentAddendaId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMergeFieldGroupId() . ', ' .
						$this->sqlExternalMergeFieldGroupId() . ', ' .
						$this->sqlField() . ', ' .
						$this->sqlExternalFieldName() . ', ' .
						$this->sqlValidationType() . ', ' .
						$this->sqlXPos() . ', ' .
						$this->sqlYPos() . ', ' .
						$this->sqlHeight() . ', ' .
						$this->sqlWidth() . ', ' .
						$this->sqlUniqueLayoutKey() . ', ' .
						$this->sqlMaxCharLimit() . ', ' .
						$this->sqlPageNumber() . ', ' .
						$this->sqlFontSize() . ', ' .
						$this->sqlFontStyle() . ', ' .
						$this->sqlStartOrdinal() . ', ' .
						$this->sqlEndOrdinal() . ', ' .
						$this->sqlRepeatDirection() . ', ' .
						$this->sqlHorizontalAlign() . ', ' .
						$this->sqlRowNumber() . ', ' .
						$this->sqlDataType() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDefaultValue() . ', ' .
						$this->sqlAllowUserUpdate() . ', ' .
						$this->sqlAllowAdminUpdate() . ', ' .
						$this->sqlForceOriginUpdate() . ', ' .
						$this->sqlShowInMergeDisplay() . ', ' .
						$this->sqlIsBlockField() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMergeFieldSubGroupId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merge_field_id = ' . $this->sqlCompanyMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'CompanyMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' company_merge_field_id = ' . $this->sqlCompanyMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_merge_field_id = ' . $this->sqlDefaultMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'DefaultMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' default_merge_field_id = ' . $this->sqlDefaultMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_document_merge_field_id = ' . $this->sqlBlockDocumentMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'BlockDocumentMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' block_document_merge_field_id = ' . $this->sqlBlockDocumentMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId(). ',' ; } elseif( true == array_key_exists( 'DocumentAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' document_addenda_id = ' . $this->sqlDocumentAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_merge_field_group_id = ' . $this->sqlExternalMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'ExternalMergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' external_merge_field_group_id = ' . $this->sqlExternalMergeFieldGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field = ' . $this->sqlField(). ',' ; } elseif( true == array_key_exists( 'Field', $this->getChangedColumns() ) ) { $strSql .= ' field = ' . $this->sqlField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_field_name = ' . $this->sqlExternalFieldName(). ',' ; } elseif( true == array_key_exists( 'ExternalFieldName', $this->getChangedColumns() ) ) { $strSql .= ' external_field_name = ' . $this->sqlExternalFieldName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_type = ' . $this->sqlValidationType(). ',' ; } elseif( true == array_key_exists( 'ValidationType', $this->getChangedColumns() ) ) { $strSql .= ' validation_type = ' . $this->sqlValidationType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x_pos = ' . $this->sqlXPos(). ',' ; } elseif( true == array_key_exists( 'XPos', $this->getChangedColumns() ) ) { $strSql .= ' x_pos = ' . $this->sqlXPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' y_pos = ' . $this->sqlYPos(). ',' ; } elseif( true == array_key_exists( 'YPos', $this->getChangedColumns() ) ) { $strSql .= ' y_pos = ' . $this->sqlYPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' height = ' . $this->sqlHeight(). ',' ; } elseif( true == array_key_exists( 'Height', $this->getChangedColumns() ) ) { $strSql .= ' height = ' . $this->sqlHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' width = ' . $this->sqlWidth(). ',' ; } elseif( true == array_key_exists( 'Width', $this->getChangedColumns() ) ) { $strSql .= ' width = ' . $this->sqlWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_layout_key = ' . $this->sqlUniqueLayoutKey(). ',' ; } elseif( true == array_key_exists( 'UniqueLayoutKey', $this->getChangedColumns() ) ) { $strSql .= ' unique_layout_key = ' . $this->sqlUniqueLayoutKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_char_limit = ' . $this->sqlMaxCharLimit(). ',' ; } elseif( true == array_key_exists( 'MaxCharLimit', $this->getChangedColumns() ) ) { $strSql .= ' max_char_limit = ' . $this->sqlMaxCharLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_number = ' . $this->sqlPageNumber(). ',' ; } elseif( true == array_key_exists( 'PageNumber', $this->getChangedColumns() ) ) { $strSql .= ' page_number = ' . $this->sqlPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' font_size = ' . $this->sqlFontSize(). ',' ; } elseif( true == array_key_exists( 'FontSize', $this->getChangedColumns() ) ) { $strSql .= ' font_size = ' . $this->sqlFontSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' font_style = ' . $this->sqlFontStyle(). ',' ; } elseif( true == array_key_exists( 'FontStyle', $this->getChangedColumns() ) ) { $strSql .= ' font_style = ' . $this->sqlFontStyle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_ordinal = ' . $this->sqlStartOrdinal(). ',' ; } elseif( true == array_key_exists( 'StartOrdinal', $this->getChangedColumns() ) ) { $strSql .= ' start_ordinal = ' . $this->sqlStartOrdinal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_ordinal = ' . $this->sqlEndOrdinal(). ',' ; } elseif( true == array_key_exists( 'EndOrdinal', $this->getChangedColumns() ) ) { $strSql .= ' end_ordinal = ' . $this->sqlEndOrdinal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repeat_direction = ' . $this->sqlRepeatDirection(). ',' ; } elseif( true == array_key_exists( 'RepeatDirection', $this->getChangedColumns() ) ) { $strSql .= ' repeat_direction = ' . $this->sqlRepeatDirection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' horizontal_align = ' . $this->sqlHorizontalAlign(). ',' ; } elseif( true == array_key_exists( 'HorizontalAlign', $this->getChangedColumns() ) ) { $strSql .= ' horizontal_align = ' . $this->sqlHorizontalAlign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' row_number = ' . $this->sqlRowNumber(). ',' ; } elseif( true == array_key_exists( 'RowNumber', $this->getChangedColumns() ) ) { $strSql .= ' row_number = ' . $this->sqlRowNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_type = ' . $this->sqlDataType(). ',' ; } elseif( true == array_key_exists( 'DataType', $this->getChangedColumns() ) ) { $strSql .= ' data_type = ' . $this->sqlDataType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue(). ',' ; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowUserUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_admin_update = ' . $this->sqlAllowAdminUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowAdminUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_admin_update = ' . $this->sqlAllowAdminUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' force_origin_update = ' . $this->sqlForceOriginUpdate(). ',' ; } elseif( true == array_key_exists( 'ForceOriginUpdate', $this->getChangedColumns() ) ) { $strSql .= ' force_origin_update = ' . $this->sqlForceOriginUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay(). ',' ; } elseif( true == array_key_exists( 'ShowInMergeDisplay', $this->getChangedColumns() ) ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField(). ',' ; } elseif( true == array_key_exists( 'IsBlockField', $this->getChangedColumns() ) ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldSubGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_merge_field_id' => $this->getCompanyMergeFieldId(),
			'default_merge_field_id' => $this->getDefaultMergeFieldId(),
			'block_document_merge_field_id' => $this->getBlockDocumentMergeFieldId(),
			'document_id' => $this->getDocumentId(),
			'document_addenda_id' => $this->getDocumentAddendaId(),
			'property_id' => $this->getPropertyId(),
			'merge_field_group_id' => $this->getMergeFieldGroupId(),
			'external_merge_field_group_id' => $this->getExternalMergeFieldGroupId(),
			'field' => $this->getField(),
			'external_field_name' => $this->getExternalFieldName(),
			'validation_type' => $this->getValidationType(),
			'x_pos' => $this->getXPos(),
			'y_pos' => $this->getYPos(),
			'height' => $this->getHeight(),
			'width' => $this->getWidth(),
			'unique_layout_key' => $this->getUniqueLayoutKey(),
			'max_char_limit' => $this->getMaxCharLimit(),
			'page_number' => $this->getPageNumber(),
			'font_size' => $this->getFontSize(),
			'font_style' => $this->getFontStyle(),
			'start_ordinal' => $this->getStartOrdinal(),
			'end_ordinal' => $this->getEndOrdinal(),
			'repeat_direction' => $this->getRepeatDirection(),
			'horizontal_align' => $this->getHorizontalAlign(),
			'row_number' => $this->getRowNumber(),
			'data_type' => $this->getDataType(),
			'description' => $this->getDescription(),
			'default_value' => $this->getDefaultValue(),
			'allow_user_update' => $this->getAllowUserUpdate(),
			'allow_admin_update' => $this->getAllowAdminUpdate(),
			'force_origin_update' => $this->getForceOriginUpdate(),
			'show_in_merge_display' => $this->getShowInMergeDisplay(),
			'is_block_field' => $this->getIsBlockField(),
			'is_required' => $this->getIsRequired(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails(),
			'merge_field_sub_group_id' => $this->getMergeFieldSubGroupId()
		);
	}

}
?>