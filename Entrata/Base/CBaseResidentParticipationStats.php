<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentParticipationStats
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseResidentParticipationStats extends CEosPluralBase {

	/**
	 * @return CResidentParticipationStat[]
	 */
	public static function fetchResidentParticipationStats( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CResidentParticipationStat', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CResidentParticipationStat
	 */
	public static function fetchResidentParticipationStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CResidentParticipationStat', $objDatabase );
	}

	public static function fetchResidentParticipationStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resident_participation_stats', $objDatabase );
	}

	public static function fetchResidentParticipationStatByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchResidentParticipationStat( sprintf( 'SELECT * FROM resident_participation_stats WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentParticipationStatsByCid( $intCid, $objDatabase ) {
		return self::fetchResidentParticipationStats( sprintf( 'SELECT * FROM resident_participation_stats WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentParticipationStatsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchResidentParticipationStats( sprintf( 'SELECT * FROM resident_participation_stats WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>