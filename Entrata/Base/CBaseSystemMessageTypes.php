<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemMessageTypes
 * Do not add any new functions to this class.
 */

class CBaseSystemMessageTypes extends CEosPluralBase {

	/**
	 * @return CSystemMessageType[]
	 */
	public static function fetchSystemMessageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemMessageType::class, $objDatabase );
	}

	/**
	 * @return CSystemMessageType
	 */
	public static function fetchSystemMessageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemMessageType::class, $objDatabase );
	}

	public static function fetchSystemMessageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_message_types', $objDatabase );
	}

	public static function fetchSystemMessageTypeById( $intId, $objDatabase ) {
		return self::fetchSystemMessageType( sprintf( 'SELECT * FROM system_message_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>