<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArTiers extends CEosPluralBase {

    const TABLE_AR_TIERS = 'public.ar_tiers';

    public static function fetchArTiers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CArTier', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchArTier( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArTier', $objDatabase );
    }

    public static function fetchArTierCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_tiers', $objDatabase );
    }

    public static function fetchArTierByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchArTier( sprintf( 'SELECT * FROM ar_tiers WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchArTiersByCid( $intCid, $objDatabase ) {
        return self::fetchArTiers( sprintf( 'SELECT * FROM ar_tiers WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchArTiersByArMultiplierIdByCid( $intArMultiplierId, $intCid, $objDatabase ) {
        return self::fetchArTiers( sprintf( 'SELECT * FROM ar_tiers WHERE ar_multiplier_id = %d AND cid = %d', (int) $intArMultiplierId, (int) $intCid ), $objDatabase );
    }

}
?>