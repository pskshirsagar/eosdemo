<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestNote extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_request_notes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestId;
	protected $m_intCompanyUserId;
	protected $m_strNote;
	protected $m_strNoteDatetime;
	protected $m_intIsResidentVisible;
	protected $m_boolIsEntryNote;
	protected $m_boolIsClosingNote;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsResidentVisible = '0';
		$this->m_boolIsEntryNote = false;
		$this->m_boolIsClosingNote = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['note'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['note_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNoteDatetime', trim( $arrValues['note_datetime'] ) ); elseif( isset( $arrValues['note_datetime'] ) ) $this->setNoteDatetime( $arrValues['note_datetime'] );
		if( isset( $arrValues['is_resident_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsResidentVisible', trim( $arrValues['is_resident_visible'] ) ); elseif( isset( $arrValues['is_resident_visible'] ) ) $this->setIsResidentVisible( $arrValues['is_resident_visible'] );
		if( isset( $arrValues['is_entry_note'] ) && $boolDirectSet ) $this->set( 'm_boolIsEntryNote', trim( stripcslashes( $arrValues['is_entry_note'] ) ) ); elseif( isset( $arrValues['is_entry_note'] ) ) $this->setIsEntryNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_entry_note'] ) : $arrValues['is_entry_note'] );
		if( isset( $arrValues['is_closing_note'] ) && $boolDirectSet ) $this->set( 'm_boolIsClosingNote', trim( stripcslashes( $arrValues['is_closing_note'] ) ) ); elseif( isset( $arrValues['is_closing_note'] ) ) $this->setIsClosingNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_closing_note'] ) : $arrValues['is_closing_note'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setNote( $strNote, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ), $strLocaleCode );
	}

	public function getNote( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNote', $strLocaleCode );
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setNoteDatetime( $strNoteDatetime ) {
		$this->set( 'm_strNoteDatetime', CStrings::strTrimDef( $strNoteDatetime, -1, NULL, true ) );
	}

	public function getNoteDatetime() {
		return $this->m_strNoteDatetime;
	}

	public function sqlNoteDatetime() {
		return ( true == isset( $this->m_strNoteDatetime ) ) ? '\'' . $this->m_strNoteDatetime . '\'' : 'NOW()';
	}

	public function setIsResidentVisible( $intIsResidentVisible ) {
		$this->set( 'm_intIsResidentVisible', CStrings::strToIntDef( $intIsResidentVisible, NULL, false ) );
	}

	public function getIsResidentVisible() {
		return $this->m_intIsResidentVisible;
	}

	public function sqlIsResidentVisible() {
		return ( true == isset( $this->m_intIsResidentVisible ) ) ? ( string ) $this->m_intIsResidentVisible : '0';
	}

	public function setIsEntryNote( $boolIsEntryNote ) {
		$this->set( 'm_boolIsEntryNote', CStrings::strToBool( $boolIsEntryNote ) );
	}

	public function getIsEntryNote() {
		return $this->m_boolIsEntryNote;
	}

	public function sqlIsEntryNote() {
		return ( true == isset( $this->m_boolIsEntryNote ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEntryNote ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsClosingNote( $boolIsClosingNote ) {
		$this->set( 'm_boolIsClosingNote', CStrings::strToBool( $boolIsClosingNote ) );
	}

	public function getIsClosingNote() {
		return $this->m_boolIsClosingNote;
	}

	public function sqlIsClosingNote() {
		return ( true == isset( $this->m_boolIsClosingNote ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClosingNote ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_id, company_user_id, note, note_datetime, is_resident_visible, is_entry_note, is_closing_note, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlNoteDatetime() . ', ' .
						$this->sqlIsResidentVisible() . ', ' .
						$this->sqlIsEntryNote() . ', ' .
						$this->sqlIsClosingNote() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime(). ',' ; } elseif( true == array_key_exists( 'NoteDatetime', $this->getChangedColumns() ) ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_visible = ' . $this->sqlIsResidentVisible(). ',' ; } elseif( true == array_key_exists( 'IsResidentVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_visible = ' . $this->sqlIsResidentVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_entry_note = ' . $this->sqlIsEntryNote(). ',' ; } elseif( true == array_key_exists( 'IsEntryNote', $this->getChangedColumns() ) ) { $strSql .= ' is_entry_note = ' . $this->sqlIsEntryNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_closing_note = ' . $this->sqlIsClosingNote(). ',' ; } elseif( true == array_key_exists( 'IsClosingNote', $this->getChangedColumns() ) ) { $strSql .= ' is_closing_note = ' . $this->sqlIsClosingNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'company_user_id' => $this->getCompanyUserId(),
			'note' => $this->getNote(),
			'note_datetime' => $this->getNoteDatetime(),
			'is_resident_visible' => $this->getIsResidentVisible(),
			'is_entry_note' => $this->getIsEntryNote(),
			'is_closing_note' => $this->getIsClosingNote(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>