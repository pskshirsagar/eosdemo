<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseGlGroupTypes extends CEosPluralBase {

	/**
	 * @return CGlGroupType[]
	 */
	public static function fetchGlGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlGroupType::class, $objDatabase );
	}

	/**
	 * @return CGlGroupType
	 */
	public static function fetchGlGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlGroupType::class, $objDatabase );
	}

	public static function fetchGlGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_group_types', $objDatabase );
	}

	public static function fetchGlGroupTypeById( $intId, $objDatabase ) {
		return self::fetchGlGroupType( sprintf( 'SELECT * FROM gl_group_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>