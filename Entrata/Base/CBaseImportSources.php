<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportSources
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportSources extends CEosPluralBase {

	/**
	 * @return CImportSource[]
	 */
	public static function fetchImportSources( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CImportSource', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CImportSource
	 */
	public static function fetchImportSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CImportSource', $objDatabase );
	}

	public static function fetchImportSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_sources', $objDatabase );
	}

	public static function fetchImportSourceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchImportSource( sprintf( 'SELECT * FROM import_sources WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportSourcesByCid( $intCid, $objDatabase ) {
		return self::fetchImportSources( sprintf( 'SELECT * FROM import_sources WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportSourcesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchImportSources( sprintf( 'SELECT * FROM import_sources WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>