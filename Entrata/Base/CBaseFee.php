<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFee extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.fees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFeeTypeId;
	protected $m_intFeeTemplateId;
	protected $m_intPeriodId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPostAr;
	protected $m_intArCodeId;
	protected $m_intFeeCalculationTypeId;
	protected $m_intPropertyId;
	protected $m_strPostMonth;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsLatest;
	protected $m_strStatus;
	protected $m_strErrors;
	protected $m_strQueuedDatetime;
	protected $m_strStartedDatetime;
	protected $m_strCompletedDatetime;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPostAr = false;
		$this->m_boolIsLatest = true;
		$this->m_strStatus = 'Completed';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTypeId', trim( $arrValues['fee_type_id'] ) ); elseif( isset( $arrValues['fee_type_id'] ) ) $this->setFeeTypeId( $arrValues['fee_type_id'] );
		if( isset( $arrValues['fee_template_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTemplateId', trim( $arrValues['fee_template_id'] ) ); elseif( isset( $arrValues['fee_template_id'] ) ) $this->setFeeTemplateId( $arrValues['fee_template_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_post_ar'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostAr', trim( stripcslashes( $arrValues['is_post_ar'] ) ) ); elseif( isset( $arrValues['is_post_ar'] ) ) $this->setIsPostAr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_ar'] ) : $arrValues['is_post_ar'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['fee_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeCalculationTypeId', trim( $arrValues['fee_calculation_type_id'] ) ); elseif( isset( $arrValues['fee_calculation_type_id'] ) ) $this->setFeeCalculationTypeId( $arrValues['fee_calculation_type_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_latest'] ) && $boolDirectSet ) $this->set( 'm_boolIsLatest', trim( stripcslashes( $arrValues['is_latest'] ) ) ); elseif( isset( $arrValues['is_latest'] ) ) $this->setIsLatest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_latest'] ) : $arrValues['is_latest'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( $arrValues['status'] ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( $arrValues['status'] );
		if( isset( $arrValues['errors'] ) && $boolDirectSet ) $this->set( 'm_strErrors', trim( $arrValues['errors'] ) ); elseif( isset( $arrValues['errors'] ) ) $this->setErrors( $arrValues['errors'] );
		if( isset( $arrValues['queued_datetime'] ) && $boolDirectSet ) $this->set( 'm_strQueuedDatetime', trim( $arrValues['queued_datetime'] ) ); elseif( isset( $arrValues['queued_datetime'] ) ) $this->setQueuedDatetime( $arrValues['queued_datetime'] );
		if( isset( $arrValues['started_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartedDatetime', trim( $arrValues['started_datetime'] ) ); elseif( isset( $arrValues['started_datetime'] ) ) $this->setStartedDatetime( $arrValues['started_datetime'] );
		if( isset( $arrValues['completed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCompletedDatetime', trim( $arrValues['completed_datetime'] ) ); elseif( isset( $arrValues['completed_datetime'] ) ) $this->setCompletedDatetime( $arrValues['completed_datetime'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFeeTypeId( $intFeeTypeId ) {
		$this->set( 'm_intFeeTypeId', CStrings::strToIntDef( $intFeeTypeId, NULL, false ) );
	}

	public function getFeeTypeId() {
		return $this->m_intFeeTypeId;
	}

	public function sqlFeeTypeId() {
		return ( true == isset( $this->m_intFeeTypeId ) ) ? ( string ) $this->m_intFeeTypeId : 'NULL';
	}

	public function setFeeTemplateId( $intFeeTemplateId ) {
		$this->set( 'm_intFeeTemplateId', CStrings::strToIntDef( $intFeeTemplateId, NULL, false ) );
	}

	public function getFeeTemplateId() {
		return $this->m_intFeeTemplateId;
	}

	public function sqlFeeTemplateId() {
		return ( true == isset( $this->m_intFeeTemplateId ) ) ? ( string ) $this->m_intFeeTemplateId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPostAr( $boolIsPostAr ) {
		$this->set( 'm_boolIsPostAr', CStrings::strToBool( $boolIsPostAr ) );
	}

	public function getIsPostAr() {
		return $this->m_boolIsPostAr;
	}

	public function sqlIsPostAr() {
		return ( true == isset( $this->m_boolIsPostAr ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostAr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setFeeCalculationTypeId( $intFeeCalculationTypeId ) {
		$this->set( 'm_intFeeCalculationTypeId', CStrings::strToIntDef( $intFeeCalculationTypeId, NULL, false ) );
	}

	public function getFeeCalculationTypeId() {
		return $this->m_intFeeCalculationTypeId;
	}

	public function sqlFeeCalculationTypeId() {
		return ( true == isset( $this->m_intFeeCalculationTypeId ) ) ? ( string ) $this->m_intFeeCalculationTypeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setIsLatest( $boolIsLatest ) {
		$this->set( 'm_boolIsLatest', CStrings::strToBool( $boolIsLatest ) );
	}

	public function getIsLatest() {
		return $this->m_boolIsLatest;
	}

	public function sqlIsLatest() {
		return ( true == isset( $this->m_boolIsLatest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLatest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 20, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStatus ) : '\'' . addslashes( $this->m_strStatus ) . '\'' ) : '\'Completed\'';
	}

	public function setErrors( $strErrors ) {
		$this->set( 'm_strErrors', CStrings::strTrimDef( $strErrors, -1, NULL, true ) );
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	public function sqlErrors() {
		return ( true == isset( $this->m_strErrors ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrors ) : '\'' . addslashes( $this->m_strErrors ) . '\'' ) : 'NULL';
	}

	public function setQueuedDatetime( $strQueuedDatetime ) {
		$this->set( 'm_strQueuedDatetime', CStrings::strTrimDef( $strQueuedDatetime, -1, NULL, true ) );
	}

	public function getQueuedDatetime() {
		return $this->m_strQueuedDatetime;
	}

	public function sqlQueuedDatetime() {
		return ( true == isset( $this->m_strQueuedDatetime ) ) ? '\'' . $this->m_strQueuedDatetime . '\'' : 'NULL';
	}

	public function setStartedDatetime( $strStartedDatetime ) {
		$this->set( 'm_strStartedDatetime', CStrings::strTrimDef( $strStartedDatetime, -1, NULL, true ) );
	}

	public function getStartedDatetime() {
		return $this->m_strStartedDatetime;
	}

	public function sqlStartedDatetime() {
		return ( true == isset( $this->m_strStartedDatetime ) ) ? '\'' . $this->m_strStartedDatetime . '\'' : 'NULL';
	}

	public function setCompletedDatetime( $strCompletedDatetime ) {
		$this->set( 'm_strCompletedDatetime', CStrings::strTrimDef( $strCompletedDatetime, -1, NULL, true ) );
	}

	public function getCompletedDatetime() {
		return $this->m_strCompletedDatetime;
	}

	public function sqlCompletedDatetime() {
		return ( true == isset( $this->m_strCompletedDatetime ) ) ? '\'' . $this->m_strCompletedDatetime . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, fee_type_id, fee_template_id, period_id, deleted_by, deleted_on, created_by, created_on, is_post_ar, ar_code_id, fee_calculation_type_id, property_id, post_month, details, is_latest, status, errors, queued_datetime, started_datetime, completed_datetime )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlFeeTypeId() . ', ' .
						$this->sqlFeeTemplateId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsPostAr() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlFeeCalculationTypeId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsLatest() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlErrors() . ', ' .
						$this->sqlQueuedDatetime() . ', ' .
						$this->sqlStartedDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_type_id = ' . $this->sqlFeeTypeId(). ',' ; } elseif( true == array_key_exists( 'FeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_type_id = ' . $this->sqlFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_template_id = ' . $this->sqlFeeTemplateId(). ',' ; } elseif( true == array_key_exists( 'FeeTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' fee_template_id = ' . $this->sqlFeeTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_ar = ' . $this->sqlIsPostAr(). ',' ; } elseif( true == array_key_exists( 'IsPostAr', $this->getChangedColumns() ) ) { $strSql .= ' is_post_ar = ' . $this->sqlIsPostAr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_calculation_type_id = ' . $this->sqlFeeCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'FeeCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_calculation_type_id = ' . $this->sqlFeeCalculationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_latest = ' . $this->sqlIsLatest(). ',' ; } elseif( true == array_key_exists( 'IsLatest', $this->getChangedColumns() ) ) { $strSql .= ' is_latest = ' . $this->sqlIsLatest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' errors = ' . $this->sqlErrors(). ',' ; } elseif( true == array_key_exists( 'Errors', $this->getChangedColumns() ) ) { $strSql .= ' errors = ' . $this->sqlErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime(). ',' ; } elseif( true == array_key_exists( 'QueuedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime(). ',' ; } elseif( true == array_key_exists( 'StartedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime() ; } elseif( true == array_key_exists( 'CompletedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'fee_type_id' => $this->getFeeTypeId(),
			'fee_template_id' => $this->getFeeTemplateId(),
			'period_id' => $this->getPeriodId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_post_ar' => $this->getIsPostAr(),
			'ar_code_id' => $this->getArCodeId(),
			'fee_calculation_type_id' => $this->getFeeCalculationTypeId(),
			'property_id' => $this->getPropertyId(),
			'post_month' => $this->getPostMonth(),
			'details' => $this->getDetails(),
			'is_latest' => $this->getIsLatest(),
			'status' => $this->getStatus(),
			'errors' => $this->getErrors(),
			'queued_datetime' => $this->getQueuedDatetime(),
			'started_datetime' => $this->getStartedDatetime(),
			'completed_datetime' => $this->getCompletedDatetime()
		);
	}

}
?>