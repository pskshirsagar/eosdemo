<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAmenityAvailability extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_amenity_availabilities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intAmenityId;
	protected $m_intRateAssociationId;
	protected $m_intReservationFeeOccurrenceTypeId;
	protected $m_intAvailability;
	protected $m_strRpDescription;
	protected $m_intChargeResidentAutomatically;
	protected $m_intRequirePaymentImmediately;
	protected $m_intRequirePropertyApproval;
	protected $m_intMaxReservationTimeLimit;
	protected $m_intIsReservedByDay;
	protected $m_intIsReservedByHoursByDay;
	protected $m_strAmenityAvailability;
	protected $m_strBlackoutHolidays;
	protected $m_boolHideAmenity;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsFutureAllowed;
	protected $m_intReservableWithinDays;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intQuantity;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intAvailability = '0';
		$this->m_boolHideAmenity = false;
		$this->m_boolIsFutureAllowed = false;
		$this->m_intReservableWithinDays = '30';
		$this->m_intQuantity = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityId', trim( $arrValues['amenity_id'] ) ); elseif( isset( $arrValues['amenity_id'] ) ) $this->setAmenityId( $arrValues['amenity_id'] );
		if( isset( $arrValues['rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intRateAssociationId', trim( $arrValues['rate_association_id'] ) ); elseif( isset( $arrValues['rate_association_id'] ) ) $this->setRateAssociationId( $arrValues['rate_association_id'] );
		if( isset( $arrValues['reservation_fee_occurrence_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReservationFeeOccurrenceTypeId', trim( $arrValues['reservation_fee_occurrence_type_id'] ) ); elseif( isset( $arrValues['reservation_fee_occurrence_type_id'] ) ) $this->setReservationFeeOccurrenceTypeId( $arrValues['reservation_fee_occurrence_type_id'] );
		if( isset( $arrValues['availability'] ) && $boolDirectSet ) $this->set( 'm_intAvailability', trim( $arrValues['availability'] ) ); elseif( isset( $arrValues['availability'] ) ) $this->setAvailability( $arrValues['availability'] );
		if( isset( $arrValues['rp_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRpDescription', trim( $arrValues['rp_description'] ) ); elseif( isset( $arrValues['rp_description'] ) ) $this->setRpDescription( $arrValues['rp_description'] );
		if( isset( $arrValues['charge_resident_automatically'] ) && $boolDirectSet ) $this->set( 'm_intChargeResidentAutomatically', trim( $arrValues['charge_resident_automatically'] ) ); elseif( isset( $arrValues['charge_resident_automatically'] ) ) $this->setChargeResidentAutomatically( $arrValues['charge_resident_automatically'] );
		if( isset( $arrValues['require_payment_immediately'] ) && $boolDirectSet ) $this->set( 'm_intRequirePaymentImmediately', trim( $arrValues['require_payment_immediately'] ) ); elseif( isset( $arrValues['require_payment_immediately'] ) ) $this->setRequirePaymentImmediately( $arrValues['require_payment_immediately'] );
		if( isset( $arrValues['require_property_approval'] ) && $boolDirectSet ) $this->set( 'm_intRequirePropertyApproval', trim( $arrValues['require_property_approval'] ) ); elseif( isset( $arrValues['require_property_approval'] ) ) $this->setRequirePropertyApproval( $arrValues['require_property_approval'] );
		if( isset( $arrValues['max_reservation_time_limit'] ) && $boolDirectSet ) $this->set( 'm_intMaxReservationTimeLimit', trim( $arrValues['max_reservation_time_limit'] ) ); elseif( isset( $arrValues['max_reservation_time_limit'] ) ) $this->setMaxReservationTimeLimit( $arrValues['max_reservation_time_limit'] );
		if( isset( $arrValues['is_reserved_by_day'] ) && $boolDirectSet ) $this->set( 'm_intIsReservedByDay', trim( $arrValues['is_reserved_by_day'] ) ); elseif( isset( $arrValues['is_reserved_by_day'] ) ) $this->setIsReservedByDay( $arrValues['is_reserved_by_day'] );
		if( isset( $arrValues['is_reserved_by_hours_by_day'] ) && $boolDirectSet ) $this->set( 'm_intIsReservedByHoursByDay', trim( $arrValues['is_reserved_by_hours_by_day'] ) ); elseif( isset( $arrValues['is_reserved_by_hours_by_day'] ) ) $this->setIsReservedByHoursByDay( $arrValues['is_reserved_by_hours_by_day'] );
		if( isset( $arrValues['amenity_availability'] ) && $boolDirectSet ) $this->set( 'm_strAmenityAvailability', trim( stripcslashes( $arrValues['amenity_availability'] ) ) ); elseif( isset( $arrValues['amenity_availability'] ) ) $this->setAmenityAvailability( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amenity_availability'] ) : $arrValues['amenity_availability'] );
		if( isset( $arrValues['blackout_holidays'] ) && $boolDirectSet ) $this->set( 'm_strBlackoutHolidays', trim( $arrValues['blackout_holidays'] ) ); elseif( isset( $arrValues['blackout_holidays'] ) ) $this->setBlackoutHolidays( $arrValues['blackout_holidays'] );
		if( isset( $arrValues['hide_amenity'] ) && $boolDirectSet ) $this->set( 'm_boolHideAmenity', trim( stripcslashes( $arrValues['hide_amenity'] ) ) ); elseif( isset( $arrValues['hide_amenity'] ) ) $this->setHideAmenity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_amenity'] ) : $arrValues['hide_amenity'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_future_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFutureAllowed', trim( stripcslashes( $arrValues['is_future_allowed'] ) ) ); elseif( isset( $arrValues['is_future_allowed'] ) ) $this->setIsFutureAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_future_allowed'] ) : $arrValues['is_future_allowed'] );
		if( isset( $arrValues['reservable_within_days'] ) && $boolDirectSet ) $this->set( 'm_intReservableWithinDays', trim( $arrValues['reservable_within_days'] ) ); elseif( isset( $arrValues['reservable_within_days'] ) ) $this->setReservableWithinDays( $arrValues['reservable_within_days'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['quantity'] ) && $boolDirectSet ) $this->set( 'm_intQuantity', trim( $arrValues['quantity'] ) ); elseif( isset( $arrValues['quantity'] ) ) $this->setQuantity( $arrValues['quantity'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setAmenityId( $intAmenityId ) {
		$this->set( 'm_intAmenityId', CStrings::strToIntDef( $intAmenityId, NULL, false ) );
	}

	public function getAmenityId() {
		return $this->m_intAmenityId;
	}

	public function sqlAmenityId() {
		return ( true == isset( $this->m_intAmenityId ) ) ? ( string ) $this->m_intAmenityId : 'NULL';
	}

	public function setRateAssociationId( $intRateAssociationId ) {
		$this->set( 'm_intRateAssociationId', CStrings::strToIntDef( $intRateAssociationId, NULL, false ) );
	}

	public function getRateAssociationId() {
		return $this->m_intRateAssociationId;
	}

	public function sqlRateAssociationId() {
		return ( true == isset( $this->m_intRateAssociationId ) ) ? ( string ) $this->m_intRateAssociationId : 'NULL';
	}

	public function setReservationFeeOccurrenceTypeId( $intReservationFeeOccurrenceTypeId ) {
		$this->set( 'm_intReservationFeeOccurrenceTypeId', CStrings::strToIntDef( $intReservationFeeOccurrenceTypeId, NULL, false ) );
	}

	public function getReservationFeeOccurrenceTypeId() {
		return $this->m_intReservationFeeOccurrenceTypeId;
	}

	public function sqlReservationFeeOccurrenceTypeId() {
		return ( true == isset( $this->m_intReservationFeeOccurrenceTypeId ) ) ? ( string ) $this->m_intReservationFeeOccurrenceTypeId : 'NULL';
	}

	public function setAvailability( $intAvailability ) {
		$this->set( 'm_intAvailability', CStrings::strToIntDef( $intAvailability, NULL, false ) );
	}

	public function getAvailability() {
		return $this->m_intAvailability;
	}

	public function sqlAvailability() {
		return ( true == isset( $this->m_intAvailability ) ) ? ( string ) $this->m_intAvailability : '0';
	}

	public function setRpDescription( $strRpDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRpDescription', CStrings::strTrimDef( $strRpDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getRpDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRpDescription', $strLocaleCode );
	}

	public function sqlRpDescription() {
		return ( true == isset( $this->m_strRpDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRpDescription ) : '\'' . addslashes( $this->m_strRpDescription ) . '\'' ) : 'NULL';
	}

	public function setChargeResidentAutomatically( $intChargeResidentAutomatically ) {
		$this->set( 'm_intChargeResidentAutomatically', CStrings::strToIntDef( $intChargeResidentAutomatically, NULL, false ) );
	}

	public function getChargeResidentAutomatically() {
		return $this->m_intChargeResidentAutomatically;
	}

	public function sqlChargeResidentAutomatically() {
		return ( true == isset( $this->m_intChargeResidentAutomatically ) ) ? ( string ) $this->m_intChargeResidentAutomatically : 'NULL';
	}

	public function setRequirePaymentImmediately( $intRequirePaymentImmediately ) {
		$this->set( 'm_intRequirePaymentImmediately', CStrings::strToIntDef( $intRequirePaymentImmediately, NULL, false ) );
	}

	public function getRequirePaymentImmediately() {
		return $this->m_intRequirePaymentImmediately;
	}

	public function sqlRequirePaymentImmediately() {
		return ( true == isset( $this->m_intRequirePaymentImmediately ) ) ? ( string ) $this->m_intRequirePaymentImmediately : 'NULL';
	}

	public function setRequirePropertyApproval( $intRequirePropertyApproval ) {
		$this->set( 'm_intRequirePropertyApproval', CStrings::strToIntDef( $intRequirePropertyApproval, NULL, false ) );
	}

	public function getRequirePropertyApproval() {
		return $this->m_intRequirePropertyApproval;
	}

	public function sqlRequirePropertyApproval() {
		return ( true == isset( $this->m_intRequirePropertyApproval ) ) ? ( string ) $this->m_intRequirePropertyApproval : 'NULL';
	}

	public function setMaxReservationTimeLimit( $intMaxReservationTimeLimit ) {
		$this->set( 'm_intMaxReservationTimeLimit', CStrings::strToIntDef( $intMaxReservationTimeLimit, NULL, false ) );
	}

	public function getMaxReservationTimeLimit() {
		return $this->m_intMaxReservationTimeLimit;
	}

	public function sqlMaxReservationTimeLimit() {
		return ( true == isset( $this->m_intMaxReservationTimeLimit ) ) ? ( string ) $this->m_intMaxReservationTimeLimit : 'NULL';
	}

	public function setIsReservedByDay( $intIsReservedByDay ) {
		$this->set( 'm_intIsReservedByDay', CStrings::strToIntDef( $intIsReservedByDay, NULL, false ) );
	}

	public function getIsReservedByDay() {
		return $this->m_intIsReservedByDay;
	}

	public function sqlIsReservedByDay() {
		return ( true == isset( $this->m_intIsReservedByDay ) ) ? ( string ) $this->m_intIsReservedByDay : 'NULL';
	}

	public function setIsReservedByHoursByDay( $intIsReservedByHoursByDay ) {
		$this->set( 'm_intIsReservedByHoursByDay', CStrings::strToIntDef( $intIsReservedByHoursByDay, NULL, false ) );
	}

	public function getIsReservedByHoursByDay() {
		return $this->m_intIsReservedByHoursByDay;
	}

	public function sqlIsReservedByHoursByDay() {
		return ( true == isset( $this->m_intIsReservedByHoursByDay ) ) ? ( string ) $this->m_intIsReservedByHoursByDay : 'NULL';
	}

	public function setAmenityAvailability( $strAmenityAvailability ) {
		$this->set( 'm_strAmenityAvailability', CStrings::strTrimDef( $strAmenityAvailability, -1, NULL, true ) );
	}

	public function getAmenityAvailability() {
		return $this->m_strAmenityAvailability;
	}

	public function sqlAmenityAvailability() {
		return ( true == isset( $this->m_strAmenityAvailability ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAmenityAvailability ) : '\'' . addslashes( $this->m_strAmenityAvailability ) . '\'' ) : 'NULL';
	}

	public function setBlackoutHolidays( $strBlackoutHolidays ) {
		$this->set( 'm_strBlackoutHolidays', CStrings::strTrimDef( $strBlackoutHolidays, 130, NULL, true ) );
	}

	public function getBlackoutHolidays() {
		return $this->m_strBlackoutHolidays;
	}

	public function sqlBlackoutHolidays() {
		return ( true == isset( $this->m_strBlackoutHolidays ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBlackoutHolidays ) : '\'' . addslashes( $this->m_strBlackoutHolidays ) . '\'' ) : 'NULL';
	}

	public function setHideAmenity( $boolHideAmenity ) {
		$this->set( 'm_boolHideAmenity', CStrings::strToBool( $boolHideAmenity ) );
	}

	public function getHideAmenity() {
		return $this->m_boolHideAmenity;
	}

	public function sqlHideAmenity() {
		return ( true == isset( $this->m_boolHideAmenity ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideAmenity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsFutureAllowed( $boolIsFutureAllowed ) {
		$this->set( 'm_boolIsFutureAllowed', CStrings::strToBool( $boolIsFutureAllowed ) );
	}

	public function getIsFutureAllowed() {
		return $this->m_boolIsFutureAllowed;
	}

	public function sqlIsFutureAllowed() {
		return ( true == isset( $this->m_boolIsFutureAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFutureAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReservableWithinDays( $intReservableWithinDays ) {
		$this->set( 'm_intReservableWithinDays', CStrings::strToIntDef( $intReservableWithinDays, NULL, false ) );
	}

	public function getReservableWithinDays() {
		return $this->m_intReservableWithinDays;
	}

	public function sqlReservableWithinDays() {
		return ( true == isset( $this->m_intReservableWithinDays ) ) ? ( string ) $this->m_intReservableWithinDays : '30';
	}

	public function setQuantity( $intQuantity ) {
		$this->set( 'm_intQuantity', CStrings::strToIntDef( $intQuantity, NULL, false ) );
	}

	public function getQuantity() {
		return $this->m_intQuantity;
	}

	public function sqlQuantity() {
		return ( true == isset( $this->m_intQuantity ) ) ? ( string ) $this->m_intQuantity : '1';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, amenity_id, rate_association_id, reservation_fee_occurrence_type_id, availability, rp_description, charge_resident_automatically, require_payment_immediately, require_property_approval, max_reservation_time_limit, is_reserved_by_day, is_reserved_by_hours_by_day, amenity_availability, blackout_holidays, hide_amenity, updated_by, updated_on, created_by, created_on, is_future_allowed, reservable_within_days, details, quantity )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlAmenityId() . ', ' .
		          $this->sqlRateAssociationId() . ', ' .
		          $this->sqlReservationFeeOccurrenceTypeId() . ', ' .
		          $this->sqlAvailability() . ', ' .
		          $this->sqlRpDescription() . ', ' .
		          $this->sqlChargeResidentAutomatically() . ', ' .
		          $this->sqlRequirePaymentImmediately() . ', ' .
		          $this->sqlRequirePropertyApproval() . ', ' .
		          $this->sqlMaxReservationTimeLimit() . ', ' .
		          $this->sqlIsReservedByDay() . ', ' .
		          $this->sqlIsReservedByHoursByDay() . ', ' .
		          $this->sqlAmenityAvailability() . ', ' .
		          $this->sqlBlackoutHolidays() . ', ' .
		          $this->sqlHideAmenity() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsFutureAllowed() . ', ' .
		          $this->sqlReservableWithinDays() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlQuantity() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId(). ',' ; } elseif( true == array_key_exists( 'AmenityId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_id = ' . $this->sqlAmenityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_association_id = ' . $this->sqlRateAssociationId(). ',' ; } elseif( true == array_key_exists( 'RateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' rate_association_id = ' . $this->sqlRateAssociationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_fee_occurrence_type_id = ' . $this->sqlReservationFeeOccurrenceTypeId(). ',' ; } elseif( true == array_key_exists( 'ReservationFeeOccurrenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reservation_fee_occurrence_type_id = ' . $this->sqlReservationFeeOccurrenceTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' availability = ' . $this->sqlAvailability(). ',' ; } elseif( true == array_key_exists( 'Availability', $this->getChangedColumns() ) ) { $strSql .= ' availability = ' . $this->sqlAvailability() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rp_description = ' . $this->sqlRpDescription(). ',' ; } elseif( true == array_key_exists( 'RpDescription', $this->getChangedColumns() ) ) { $strSql .= ' rp_description = ' . $this->sqlRpDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_resident_automatically = ' . $this->sqlChargeResidentAutomatically(). ',' ; } elseif( true == array_key_exists( 'ChargeResidentAutomatically', $this->getChangedColumns() ) ) { $strSql .= ' charge_resident_automatically = ' . $this->sqlChargeResidentAutomatically() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_payment_immediately = ' . $this->sqlRequirePaymentImmediately(). ',' ; } elseif( true == array_key_exists( 'RequirePaymentImmediately', $this->getChangedColumns() ) ) { $strSql .= ' require_payment_immediately = ' . $this->sqlRequirePaymentImmediately() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_property_approval = ' . $this->sqlRequirePropertyApproval(). ',' ; } elseif( true == array_key_exists( 'RequirePropertyApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_property_approval = ' . $this->sqlRequirePropertyApproval() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_reservation_time_limit = ' . $this->sqlMaxReservationTimeLimit(). ',' ; } elseif( true == array_key_exists( 'MaxReservationTimeLimit', $this->getChangedColumns() ) ) { $strSql .= ' max_reservation_time_limit = ' . $this->sqlMaxReservationTimeLimit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved_by_day = ' . $this->sqlIsReservedByDay(). ',' ; } elseif( true == array_key_exists( 'IsReservedByDay', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved_by_day = ' . $this->sqlIsReservedByDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved_by_hours_by_day = ' . $this->sqlIsReservedByHoursByDay(). ',' ; } elseif( true == array_key_exists( 'IsReservedByHoursByDay', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved_by_hours_by_day = ' . $this->sqlIsReservedByHoursByDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_availability = ' . $this->sqlAmenityAvailability(). ',' ; } elseif( true == array_key_exists( 'AmenityAvailability', $this->getChangedColumns() ) ) { $strSql .= ' amenity_availability = ' . $this->sqlAmenityAvailability() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blackout_holidays = ' . $this->sqlBlackoutHolidays(). ',' ; } elseif( true == array_key_exists( 'BlackoutHolidays', $this->getChangedColumns() ) ) { $strSql .= ' blackout_holidays = ' . $this->sqlBlackoutHolidays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_amenity = ' . $this->sqlHideAmenity(). ',' ; } elseif( true == array_key_exists( 'HideAmenity', $this->getChangedColumns() ) ) { $strSql .= ' hide_amenity = ' . $this->sqlHideAmenity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_future_allowed = ' . $this->sqlIsFutureAllowed(). ',' ; } elseif( true == array_key_exists( 'IsFutureAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_future_allowed = ' . $this->sqlIsFutureAllowed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservable_within_days = ' . $this->sqlReservableWithinDays(). ',' ; } elseif( true == array_key_exists( 'ReservableWithinDays', $this->getChangedColumns() ) ) { $strSql .= ' reservable_within_days = ' . $this->sqlReservableWithinDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity = ' . $this->sqlQuantity(). ',' ; } elseif( true == array_key_exists( 'Quantity', $this->getChangedColumns() ) ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'amenity_id' => $this->getAmenityId(),
			'rate_association_id' => $this->getRateAssociationId(),
			'reservation_fee_occurrence_type_id' => $this->getReservationFeeOccurrenceTypeId(),
			'availability' => $this->getAvailability(),
			'rp_description' => $this->getRpDescription(),
			'charge_resident_automatically' => $this->getChargeResidentAutomatically(),
			'require_payment_immediately' => $this->getRequirePaymentImmediately(),
			'require_property_approval' => $this->getRequirePropertyApproval(),
			'max_reservation_time_limit' => $this->getMaxReservationTimeLimit(),
			'is_reserved_by_day' => $this->getIsReservedByDay(),
			'is_reserved_by_hours_by_day' => $this->getIsReservedByHoursByDay(),
			'amenity_availability' => $this->getAmenityAvailability(),
			'blackout_holidays' => $this->getBlackoutHolidays(),
			'hide_amenity' => $this->getHideAmenity(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_future_allowed' => $this->getIsFutureAllowed(),
			'reservable_within_days' => $this->getReservableWithinDays(),
			'details' => $this->getDetails(),
			'quantity' => $this->getQuantity()
		);
	}

}
?>