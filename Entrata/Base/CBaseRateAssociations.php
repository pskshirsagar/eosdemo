<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRateAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRateAssociations extends CEosPluralBase {

	/**
	 * @return CRateAssociation[]
	 */
	public static function fetchRateAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRateAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRateAssociation
	 */
	public static function fetchRateAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRateAssociation', $objDatabase );
	}

	public static function fetchRateAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rate_associations', $objDatabase );
	}

	public static function fetchRateAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRateAssociation( sprintf( 'SELECT * FROM rate_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeIdByCid( $intArCascadeId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE ar_cascade_id = %d AND cid = %d', ( int ) $intArCascadeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeReferenceIdByCid( $intArCascadeReferenceId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE ar_cascade_reference_id = %d AND cid = %d', ( int ) $intArCascadeReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE ar_origin_id = %d AND cid = %d', ( int ) $intArOriginId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE ar_origin_reference_id = %d AND cid = %d', ( int ) $intArOriginReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByMappingIdByCid( $intMappingId, $intCid, $objDatabase ) {
		return self::fetchRateAssociations( sprintf( 'SELECT * FROM rate_associations WHERE mapping_id = %d AND cid = %d', ( int ) $intMappingId, ( int ) $intCid ), $objDatabase );
	}

}
?>