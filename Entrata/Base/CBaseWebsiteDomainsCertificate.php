<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteDomainsCertificate extends CEosSingularBase {

	const TABLE_NAME = 'public.website_domains_certificates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCertId;
	protected $m_intWebsiteId;
	protected $m_intDomainId;
	protected $m_strDomainName;
	protected $m_boolIsActive;
	protected $m_strF5LastSyncOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['cert_id'] ) && $boolDirectSet ) $this->set( 'm_intCertId', trim( $arrValues['cert_id'] ) ); elseif( isset( $arrValues['cert_id'] ) ) $this->setCertId( $arrValues['cert_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['domain_id'] ) && $boolDirectSet ) $this->set( 'm_intDomainId', trim( $arrValues['domain_id'] ) ); elseif( isset( $arrValues['domain_id'] ) ) $this->setDomainId( $arrValues['domain_id'] );
		if( isset( $arrValues['domain_name'] ) && $boolDirectSet ) $this->set( 'm_strDomainName', trim( stripcslashes( $arrValues['domain_name'] ) ) ); elseif( isset( $arrValues['domain_name'] ) ) $this->setDomainName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain_name'] ) : $arrValues['domain_name'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['f5_last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strF5LastSyncOn', trim( $arrValues['f5_last_sync_on'] ) ); elseif( isset( $arrValues['f5_last_sync_on'] ) ) $this->setF5LastSyncOn( $arrValues['f5_last_sync_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCertId( $intCertId ) {
		$this->set( 'm_intCertId', CStrings::strToIntDef( $intCertId, NULL, false ) );
	}

	public function getCertId() {
		return $this->m_intCertId;
	}

	public function sqlCertId() {
		return ( true == isset( $this->m_intCertId ) ) ? ( string ) $this->m_intCertId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setDomainId( $intDomainId ) {
		$this->set( 'm_intDomainId', CStrings::strToIntDef( $intDomainId, NULL, false ) );
	}

	public function getDomainId() {
		return $this->m_intDomainId;
	}

	public function sqlDomainId() {
		return ( true == isset( $this->m_intDomainId ) ) ? ( string ) $this->m_intDomainId : 'NULL';
	}

	public function setDomainName( $strDomainName ) {
		$this->set( 'm_strDomainName', CStrings::strTrimDef( $strDomainName, 255, NULL, true ) );
	}

	public function getDomainName() {
		return $this->m_strDomainName;
	}

	public function sqlDomainName() {
		return ( true == isset( $this->m_strDomainName ) ) ? '\'' . addslashes( $this->m_strDomainName ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setF5LastSyncOn( $strF5LastSyncOn ) {
		$this->set( 'm_strF5LastSyncOn', CStrings::strTrimDef( $strF5LastSyncOn, -1, NULL, true ) );
	}

	public function getF5LastSyncOn() {
		return $this->m_strF5LastSyncOn;
	}

	public function sqlF5LastSyncOn() {
		return ( true == isset( $this->m_strF5LastSyncOn ) ) ? '\'' . $this->m_strF5LastSyncOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, cert_id, website_id, domain_id, domain_name, is_active, f5_last_sync_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCertId() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlDomainId() . ', ' .
 						$this->sqlDomainName() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlF5LastSyncOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cert_id = ' . $this->sqlCertId() . ','; } elseif( true == array_key_exists( 'CertId', $this->getChangedColumns() ) ) { $strSql .= ' cert_id = ' . $this->sqlCertId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain_id = ' . $this->sqlDomainId() . ','; } elseif( true == array_key_exists( 'DomainId', $this->getChangedColumns() ) ) { $strSql .= ' domain_id = ' . $this->sqlDomainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain_name = ' . $this->sqlDomainName() . ','; } elseif( true == array_key_exists( 'DomainName', $this->getChangedColumns() ) ) { $strSql .= ' domain_name = ' . $this->sqlDomainName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' f5_last_sync_on = ' . $this->sqlF5LastSyncOn() . ','; } elseif( true == array_key_exists( 'F5LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' f5_last_sync_on = ' . $this->sqlF5LastSyncOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'cert_id' => $this->getCertId(),
			'website_id' => $this->getWebsiteId(),
			'domain_id' => $this->getDomainId(),
			'domain_name' => $this->getDomainName(),
			'is_active' => $this->getIsActive(),
			'f5_last_sync_on' => $this->getF5LastSyncOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>