<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationTransmissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationTransmissions extends CEosPluralBase {

	/**
	 * @return CApplicantApplicationTransmission[]
	 */
	public static function fetchApplicantApplicationTransmissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplicantApplicationTransmission::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantApplicationTransmission
	 */
	public static function fetchApplicantApplicationTransmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicantApplicationTransmission::class, $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_application_transmissions', $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmission( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByTransmissionIdByCid( $intTransmissionId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE transmission_id = %d AND cid = %d', ( int ) $intTransmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE transmission_vendor_id = %d AND cid = %d', ( int ) $intTransmissionVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE company_transmission_vendor_id = %d AND cid = %d', ( int ) $intCompanyTransmissionVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByCreditTransmissionResponseTypeIdByCid( $intCreditTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE credit_transmission_response_type_id = %d AND cid = %d', ( int ) $intCreditTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByCriminalTransmissionResponseTypeIdByCid( $intCriminalTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE criminal_transmission_response_type_id = %d AND cid = %d', ( int ) $intCriminalTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByIdentifyTransmissionResponseTypeIdByCid( $intIdentifyTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE identify_transmission_response_type_id = %d AND cid = %d', ( int ) $intIdentifyTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByIndividualTransmissionResponseTypeIdByCid( $intIndividualTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE individual_transmission_response_type_id = %d AND cid = %d', ( int ) $intIndividualTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByTransmissionResponseTypeIdByCid( $intTransmissionResponseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE transmission_response_type_id = %d AND cid = %d', ( int ) $intTransmissionResponseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationTransmissionsByScreeningApplicantIdByCid( $intScreeningApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationTransmissions( sprintf( 'SELECT * FROM applicant_application_transmissions WHERE screening_applicant_id = %d AND cid = %d', ( int ) $intScreeningApplicantId, ( int ) $intCid ), $objDatabase );
	}

}
?>