<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheetTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetWorksheetTypes extends CEosPluralBase {

	/**
	 * @return CBudgetWorksheetType[]
	 */
	public static function fetchBudgetWorksheetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetWorksheetType::class, $objDatabase );
	}

	/**
	 * @return CBudgetWorksheetType
	 */
	public static function fetchBudgetWorksheetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorksheetType::class, $objDatabase );
	}

	public static function fetchBudgetWorksheetTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_worksheet_types', $objDatabase );
	}

	public static function fetchBudgetWorksheetTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetWorksheetType( sprintf( 'SELECT * FROM budget_worksheet_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>