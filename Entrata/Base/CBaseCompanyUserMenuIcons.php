<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserMenuIcons
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserMenuIcons extends CEosPluralBase {

	/**
	 * @return CCompanyUserMenuIcon[]
	 */
	public static function fetchCompanyUserMenuIcons( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserMenuIcon', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserMenuIcon
	 */
	public static function fetchCompanyUserMenuIcon( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserMenuIcon', $objDatabase );
	}

	public static function fetchCompanyUserMenuIconCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_menu_icons', $objDatabase );
	}

	public static function fetchCompanyUserMenuIconByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserMenuIcon( sprintf( 'SELECT * FROM company_user_menu_icons WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserMenuIconsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserMenuIcons( sprintf( 'SELECT * FROM company_user_menu_icons WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserMenuIconsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserMenuIcons( sprintf( 'SELECT * FROM company_user_menu_icons WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserMenuIconsByMenuIconIdByCid( $intMenuIconId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserMenuIcons( sprintf( 'SELECT * FROM company_user_menu_icons WHERE menu_icon_id = %d AND cid = %d', ( int ) $intMenuIconId, ( int ) $intCid ), $objDatabase );
	}

}
?>