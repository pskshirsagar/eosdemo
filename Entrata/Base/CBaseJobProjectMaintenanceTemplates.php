<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectMaintenanceTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjectMaintenanceTemplates extends CEosPluralBase {

	/**
	 * @return CJobProjectMaintenanceTemplate[]
	 */
	public static function fetchJobProjectMaintenanceTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobProjectMaintenanceTemplate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobProjectMaintenanceTemplate
	 */
	public static function fetchJobProjectMaintenanceTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobProjectMaintenanceTemplate', $objDatabase );
	}

	public static function fetchJobProjectMaintenanceTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_project_maintenance_templates', $objDatabase );
	}

	public static function fetchJobProjectMaintenanceTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobProjectMaintenanceTemplate( sprintf( 'SELECT * FROM job_project_maintenance_templates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectMaintenanceTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchJobProjectMaintenanceTemplates( sprintf( 'SELECT * FROM job_project_maintenance_templates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectMaintenanceTemplatesByJobProjectIdByCid( $intJobProjectId, $intCid, $objDatabase ) {
		return self::fetchJobProjectMaintenanceTemplates( sprintf( 'SELECT * FROM job_project_maintenance_templates WHERE job_project_id = %d AND cid = %d', ( int ) $intJobProjectId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectMaintenanceTemplatesByMaintenanceTemplateIdByCid( $intMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchJobProjectMaintenanceTemplates( sprintf( 'SELECT * FROM job_project_maintenance_templates WHERE maintenance_template_id = %d AND cid = %d', ( int ) $intMaintenanceTemplateId, ( int ) $intCid ), $objDatabase );
	}

}
?>