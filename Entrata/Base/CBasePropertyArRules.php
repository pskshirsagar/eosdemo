<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyArRules extends CEosPluralBase {

    const TABLE_PROPERTY_AR_RULES = 'public.property_ar_rules';

    public static function fetchPropertyArRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CPropertyArRule', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchPropertyArRule( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPropertyArRule', $objDatabase );
    }

    public static function fetchPropertyArRuleCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'property_ar_rules', $objDatabase );
    }

    public static function fetchPropertyArRuleByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRule( sprintf( 'SELECT * FROM property_ar_rules WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByCid( $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE ar_origin_id = %d AND cid = %d', (int) $intArOriginId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByArCodeTypeIdByCid( $intArCodeTypeId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE ar_code_type_id = %d AND cid = %d', (int) $intArCodeTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE ar_trigger_id = %d AND cid = %d', (int) $intArTriggerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyArRulesByArActionIdByCid( $intArActionId, $intCid, $objDatabase ) {
        return self::fetchPropertyArRules( sprintf( 'SELECT * FROM property_ar_rules WHERE ar_action_id = %d AND cid = %d', (int) $intArActionId, (int) $intCid ), $objDatabase );
    }

}
?>