<?php

class CBaseInterCompanyTypes extends CEosPluralBase {

	public static function fetchInterCompanyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInterCompanyType', $objDatabase );
	}

	public static function fetchInterCompanyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInterCompanyType', $objDatabase );
	}

	public static function fetchInterCompanyTypeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inter_company_types', $objDatabase );
	}

	public static function fetchInterCompanyTypeById( $intId, $objDatabase ) {
		return self::fetchInterCompanyType( sprintf( 'SELECT * FROM inter_company_types WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>