<?php

class CBaseMarketingIntegrationLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.marketing_integration_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintPropertyIds;
	protected $m_intInternetListingServiceId;
	protected $m_intMarketingIntegrationLogTypeId;
	protected $m_strRequestType;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strEndTime;
	protected $m_intResponseStatus;
	protected $m_intServiceId;
	protected $m_strCategoryType;
	protected $m_strEndPointUrl;
	protected $m_strStartTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['marketing_integration_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingIntegrationLogTypeId', trim( $arrValues['marketing_integration_log_type_id'] ) ); elseif( isset( $arrValues['marketing_integration_log_type_id'] ) ) $this->setMarketingIntegrationLogTypeId( $arrValues['marketing_integration_log_type_id'] );
		if( isset( $arrValues['request_type'] ) && $boolDirectSet ) $this->set( 'm_strRequestType', trim( $arrValues['request_type'] ) ); elseif( isset( $arrValues['request_type'] ) ) $this->setRequestType( $arrValues['request_type'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['response_status'] ) && $boolDirectSet ) $this->set( 'm_intResponseStatus', trim( $arrValues['response_status'] ) ); elseif( isset( $arrValues['response_status'] ) ) $this->setResponseStatus( $arrValues['response_status'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['category_type'] ) && $boolDirectSet ) $this->set( 'm_strCategoryType', trim( $arrValues['category_type'] ) ); elseif( isset( $arrValues['category_type'] ) ) $this->setCategoryType( $arrValues['category_type'] );
		if( isset( $arrValues['end_point_url'] ) && $boolDirectSet ) $this->set( 'm_strEndPointUrl', trim( $arrValues['end_point_url'] ) ); elseif( isset( $arrValues['end_point_url'] ) ) $this->setEndPointUrl( $arrValues['end_point_url'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) && true == valArr( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setMarketingIntegrationLogTypeId( $intMarketingIntegrationLogTypeId ) {
		$this->set( 'm_intMarketingIntegrationLogTypeId', CStrings::strToIntDef( $intMarketingIntegrationLogTypeId, NULL, false ) );
	}

	public function getMarketingIntegrationLogTypeId() {
		return $this->m_intMarketingIntegrationLogTypeId;
	}

	public function sqlMarketingIntegrationLogTypeId() {
		return ( true == isset( $this->m_intMarketingIntegrationLogTypeId ) ) ? ( string ) $this->m_intMarketingIntegrationLogTypeId : 'NULL';
	}

	public function setRequestType( $strRequestType ) {
		$this->set( 'm_strRequestType', CStrings::strTrimDef( $strRequestType, 240, NULL, true ) );
	}

	public function getRequestType() {
		return $this->m_strRequestType;
	}

	public function sqlRequestType() {
		return ( true == isset( $this->m_strRequestType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestType ) : '\'' . addslashes( $this->m_strRequestType ) . '\'' ) : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, -1, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setResponseStatus( $intResponseStatus ) {
		$this->set( 'm_intResponseStatus', CStrings::strToIntDef( $intResponseStatus, NULL, false ) );
	}

	public function getResponseStatus() {
		return $this->m_intResponseStatus;
	}

	public function sqlResponseStatus() {
		return ( true == isset( $this->m_intResponseStatus ) ) ? ( string ) $this->m_intResponseStatus : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setCategoryType( $strCategoryType ) {
		$this->set( 'm_strCategoryType', CStrings::strTrimDef( $strCategoryType, 15, NULL, true ) );
	}

	public function getCategoryType() {
		return $this->m_strCategoryType;
	}

	public function sqlCategoryType() {
		return ( true == isset( $this->m_strCategoryType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCategoryType ) : '\'' . addslashes( $this->m_strCategoryType ) . '\'' ) : 'NULL';
	}

	public function setEndPointUrl( $strEndPointUrl ) {
		$this->set( 'm_strEndPointUrl', CStrings::strTrimDef( $strEndPointUrl, 255, NULL, true ) );
	}

	public function getEndPointUrl() {
		return $this->m_strEndPointUrl;
	}

	public function sqlEndPointUrl() {
		return ( true == isset( $this->m_strEndPointUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEndPointUrl ) : '\'' . addslashes( $this->m_strEndPointUrl ) . '\'' ) : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_ids, internet_listing_service_id, marketing_integration_log_type_id, request_type, details, end_time, response_status, service_id, category_type, end_point_url, start_time, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyIds() . ', ' .
						$this->sqlInternetListingServiceId() . ', ' .
						$this->sqlMarketingIntegrationLogTypeId() . ', ' .
						$this->sqlRequestType() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlEndTime() . ', ' .
						$this->sqlResponseStatus() . ', ' .
						$this->sqlServiceId() . ', ' .
						$this->sqlCategoryType() . ', ' .
						$this->sqlEndPointUrl() . ', ' .
						$this->sqlStartTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds(). ',' ; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_integration_log_type_id = ' . $this->sqlMarketingIntegrationLogTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketingIntegrationLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_integration_log_type_id = ' . $this->sqlMarketingIntegrationLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_type = ' . $this->sqlRequestType(). ',' ; } elseif( true == array_key_exists( 'RequestType', $this->getChangedColumns() ) ) { $strSql .= ' request_type = ' . $this->sqlRequestType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime(). ',' ; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_status = ' . $this->sqlResponseStatus(). ',' ; } elseif( true == array_key_exists( 'ResponseStatus', $this->getChangedColumns() ) ) { $strSql .= ' response_status = ' . $this->sqlResponseStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId(). ',' ; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' category_type = ' . $this->sqlCategoryType(). ',' ; } elseif( true == array_key_exists( 'CategoryType', $this->getChangedColumns() ) ) { $strSql .= ' category_type = ' . $this->sqlCategoryType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_point_url = ' . $this->sqlEndPointUrl(). ',' ; } elseif( true == array_key_exists( 'EndPointUrl', $this->getChangedColumns() ) ) { $strSql .= ' end_point_url = ' . $this->sqlEndPointUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime(). ',' ; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_ids' => $this->getPropertyIds(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'marketing_integration_log_type_id' => $this->getMarketingIntegrationLogTypeId(),
			'request_type' => $this->getRequestType(),
			'details' => $this->getDetails(),
			'end_time' => $this->getEndTime(),
			'response_status' => $this->getResponseStatus(),
			'service_id' => $this->getServiceId(),
			'category_type' => $this->getCategoryType(),
			'end_point_url' => $this->getEndPointUrl(),
			'start_time' => $this->getStartTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>