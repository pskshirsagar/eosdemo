<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentLeasingCaps
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseStudentLeasingCaps extends CEosPluralBase {

	/**
	 * @return CStudentLeasingCap[]
	 */
	public static function fetchStudentLeasingCaps( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CStudentLeasingCap::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CStudentLeasingCap
	 */
	public static function fetchStudentLeasingCap( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStudentLeasingCap::class, $objDatabase );
	}

	public static function fetchStudentLeasingCapCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'student_leasing_caps', $objDatabase );
	}

	public static function fetchStudentLeasingCapByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCap( sprintf( 'SELECT * FROM student_leasing_caps WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsByCid( $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchStudentLeasingCapsBySpaceOptionIdByCid( $intSpaceOptionId, $intCid, $objDatabase ) {
		return self::fetchStudentLeasingCaps( sprintf( 'SELECT * FROM student_leasing_caps WHERE space_option_id = %d AND cid = %d', ( int ) $intSpaceOptionId, ( int ) $intCid ), $objDatabase );
	}

}
?>