<?php

class CBaseModule extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.modules';

	protected $m_intId;
	protected $m_intDefaultCid;
	protected $m_intParentModuleId;
	protected $m_strName;
	protected $m_arrstrActions;
	protected $m_strTitle;
	protected $m_strUrl;
	protected $m_strDescription;
	protected $m_boolHasWriteOperation;
	protected $m_intIsAction;
	protected $m_intIsWebMethod;
	protected $m_intIsPublic;
	protected $m_intIsPublished;
	protected $m_intAllowOnlyAdmin;
	protected $m_intAllowOnlyPsiAdmin;
	protected $m_intIsHidden;
	protected $m_strOptions;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intDefaultCid = '1';
		$this->m_boolHasWriteOperation = false;
		$this->m_intIsAction = '0';
		$this->m_intIsWebMethod = '0';
		$this->m_intIsPublic = '0';
		$this->m_intIsPublished = '1';
		$this->m_intAllowOnlyAdmin = '0';
		$this->m_intAllowOnlyPsiAdmin = '0';
		$this->m_intIsHidden = '0';
		$this->m_intOrderNum = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['parent_module_id'] ) && $boolDirectSet ) $this->set( 'm_intParentModuleId', trim( $arrValues['parent_module_id'] ) ); elseif( isset( $arrValues['parent_module_id'] ) ) $this->setParentModuleId( $arrValues['parent_module_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['actions'] ) && $boolDirectSet ) $this->set( 'm_arrstrActions', trim( $arrValues['actions'] ) ); elseif( isset( $arrValues['actions'] ) ) $this->setActions( $arrValues['actions'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['has_write_operation'] ) && $boolDirectSet ) $this->set( 'm_boolHasWriteOperation', trim( stripcslashes( $arrValues['has_write_operation'] ) ) ); elseif( isset( $arrValues['has_write_operation'] ) ) $this->setHasWriteOperation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_write_operation'] ) : $arrValues['has_write_operation'] );
		if( isset( $arrValues['is_action'] ) && $boolDirectSet ) $this->set( 'm_intIsAction', trim( $arrValues['is_action'] ) ); elseif( isset( $arrValues['is_action'] ) ) $this->setIsAction( $arrValues['is_action'] );
		if( isset( $arrValues['is_web_method'] ) && $boolDirectSet ) $this->set( 'm_intIsWebMethod', trim( $arrValues['is_web_method'] ) ); elseif( isset( $arrValues['is_web_method'] ) ) $this->setIsWebMethod( $arrValues['is_web_method'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['allow_only_admin'] ) && $boolDirectSet ) $this->set( 'm_intAllowOnlyAdmin', trim( $arrValues['allow_only_admin'] ) ); elseif( isset( $arrValues['allow_only_admin'] ) ) $this->setAllowOnlyAdmin( $arrValues['allow_only_admin'] );
		if( isset( $arrValues['allow_only_psi_admin'] ) && $boolDirectSet ) $this->set( 'm_intAllowOnlyPsiAdmin', trim( $arrValues['allow_only_psi_admin'] ) ); elseif( isset( $arrValues['allow_only_psi_admin'] ) ) $this->setAllowOnlyPsiAdmin( $arrValues['allow_only_psi_admin'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_intIsHidden', trim( $arrValues['is_hidden'] ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( $arrValues['is_hidden'] );
		if( isset( $arrValues['options'] ) && $boolDirectSet ) $this->set( 'm_strOptions', trim( stripcslashes( $arrValues['options'] ) ) ); elseif( isset( $arrValues['options'] ) ) $this->setOptions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['options'] ) : $arrValues['options'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : '1';
	}

	public function setParentModuleId( $intParentModuleId ) {
		$this->set( 'm_intParentModuleId', CStrings::strToIntDef( $intParentModuleId, NULL, false ) );
	}

	public function getParentModuleId() {
		return $this->m_intParentModuleId;
	}

	public function sqlParentModuleId() {
		return ( true == isset( $this->m_intParentModuleId ) ) ? ( string ) $this->m_intParentModuleId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 150, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setActions( $arrstrActions ) {
		$this->set( 'm_arrstrActions', CStrings::strToArrIntDef( $arrstrActions, NULL ) );
	}

	public function getActions() {
		return $this->m_arrstrActions;
	}

	public function sqlActions() {
		return ( true == isset( $this->m_arrstrActions ) && true == valArr( $this->m_arrstrActions ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrActions, NULL ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 1024, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setHasWriteOperation( $boolHasWriteOperation ) {
		$this->set( 'm_boolHasWriteOperation', CStrings::strToBool( $boolHasWriteOperation ) );
	}

	public function getHasWriteOperation() {
		return $this->m_boolHasWriteOperation;
	}

	public function sqlHasWriteOperation() {
		return ( true == isset( $this->m_boolHasWriteOperation ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasWriteOperation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAction( $intIsAction ) {
		$this->set( 'm_intIsAction', CStrings::strToIntDef( $intIsAction, NULL, false ) );
	}

	public function getIsAction() {
		return $this->m_intIsAction;
	}

	public function sqlIsAction() {
		return ( true == isset( $this->m_intIsAction ) ) ? ( string ) $this->m_intIsAction : '0';
	}

	public function setIsWebMethod( $intIsWebMethod ) {
		$this->set( 'm_intIsWebMethod', CStrings::strToIntDef( $intIsWebMethod, NULL, false ) );
	}

	public function getIsWebMethod() {
		return $this->m_intIsWebMethod;
	}

	public function sqlIsWebMethod() {
		return ( true == isset( $this->m_intIsWebMethod ) ) ? ( string ) $this->m_intIsWebMethod : '0';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setAllowOnlyAdmin( $intAllowOnlyAdmin ) {
		$this->set( 'm_intAllowOnlyAdmin', CStrings::strToIntDef( $intAllowOnlyAdmin, NULL, false ) );
	}

	public function getAllowOnlyAdmin() {
		return $this->m_intAllowOnlyAdmin;
	}

	public function sqlAllowOnlyAdmin() {
		return ( true == isset( $this->m_intAllowOnlyAdmin ) ) ? ( string ) $this->m_intAllowOnlyAdmin : '0';
	}

	public function setAllowOnlyPsiAdmin( $intAllowOnlyPsiAdmin ) {
		$this->set( 'm_intAllowOnlyPsiAdmin', CStrings::strToIntDef( $intAllowOnlyPsiAdmin, NULL, false ) );
	}

	public function getAllowOnlyPsiAdmin() {
		return $this->m_intAllowOnlyPsiAdmin;
	}

	public function sqlAllowOnlyPsiAdmin() {
		return ( true == isset( $this->m_intAllowOnlyPsiAdmin ) ) ? ( string ) $this->m_intAllowOnlyPsiAdmin : '0';
	}

	public function setIsHidden( $intIsHidden ) {
		$this->set( 'm_intIsHidden', CStrings::strToIntDef( $intIsHidden, NULL, false ) );
	}

	public function getIsHidden() {
		return $this->m_intIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_intIsHidden ) ) ? ( string ) $this->m_intIsHidden : '0';
	}

	public function setOptions( $strOptions ) {
		$this->set( 'm_strOptions', CStrings::strTrimDef( $strOptions, -1, NULL, true ) );
	}

	public function getOptions() {
		return $this->m_strOptions;
	}

	public function sqlOptions() {
		return ( true == isset( $this->m_strOptions ) ) ? '\'' . addslashes( $this->m_strOptions ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, default_cid, parent_module_id, name, actions, title, url, description, has_write_operation, is_action, is_web_method, is_public, is_published, allow_only_admin, allow_only_psi_admin, is_hidden, options, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlParentModuleId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlActions() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlHasWriteOperation() . ', ' .
						$this->sqlIsAction() . ', ' .
						$this->sqlIsWebMethod() . ', ' .
						$this->sqlIsPublic() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlAllowOnlyAdmin() . ', ' .
						$this->sqlAllowOnlyPsiAdmin() . ', ' .
						$this->sqlIsHidden() . ', ' .
						$this->sqlOptions() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId(). ',' ; } elseif( true == array_key_exists( 'ParentModuleId', $this->getChangedColumns() ) ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actions = ' . $this->sqlActions(). ',' ; } elseif( true == array_key_exists( 'Actions', $this->getChangedColumns() ) ) { $strSql .= ' actions = ' . $this->sqlActions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_write_operation = ' . $this->sqlHasWriteOperation(). ',' ; } elseif( true == array_key_exists( 'HasWriteOperation', $this->getChangedColumns() ) ) { $strSql .= ' has_write_operation = ' . $this->sqlHasWriteOperation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_action = ' . $this->sqlIsAction(). ',' ; } elseif( true == array_key_exists( 'IsAction', $this->getChangedColumns() ) ) { $strSql .= ' is_action = ' . $this->sqlIsAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_web_method = ' . $this->sqlIsWebMethod(). ',' ; } elseif( true == array_key_exists( 'IsWebMethod', $this->getChangedColumns() ) ) { $strSql .= ' is_web_method = ' . $this->sqlIsWebMethod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_public = ' . $this->sqlIsPublic(). ',' ; } elseif( true == array_key_exists( 'IsPublic', $this->getChangedColumns() ) ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_only_admin = ' . $this->sqlAllowOnlyAdmin(). ',' ; } elseif( true == array_key_exists( 'AllowOnlyAdmin', $this->getChangedColumns() ) ) { $strSql .= ' allow_only_admin = ' . $this->sqlAllowOnlyAdmin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_only_psi_admin = ' . $this->sqlAllowOnlyPsiAdmin(). ',' ; } elseif( true == array_key_exists( 'AllowOnlyPsiAdmin', $this->getChangedColumns() ) ) { $strSql .= ' allow_only_psi_admin = ' . $this->sqlAllowOnlyPsiAdmin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden(). ',' ; } elseif( true == array_key_exists( 'IsHidden', $this->getChangedColumns() ) ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' options = ' . $this->sqlOptions(). ',' ; } elseif( true == array_key_exists( 'Options', $this->getChangedColumns() ) ) { $strSql .= ' options = ' . $this->sqlOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_cid' => $this->getDefaultCid(),
			'parent_module_id' => $this->getParentModuleId(),
			'name' => $this->getName(),
			'actions' => $this->getActions(),
			'title' => $this->getTitle(),
			'url' => $this->getUrl(),
			'description' => $this->getDescription(),
			'has_write_operation' => $this->getHasWriteOperation(),
			'is_action' => $this->getIsAction(),
			'is_web_method' => $this->getIsWebMethod(),
			'is_public' => $this->getIsPublic(),
			'is_published' => $this->getIsPublished(),
			'allow_only_admin' => $this->getAllowOnlyAdmin(),
			'allow_only_psi_admin' => $this->getAllowOnlyPsiAdmin(),
			'is_hidden' => $this->getIsHidden(),
			'options' => $this->getOptions(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>