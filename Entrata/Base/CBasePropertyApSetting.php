<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_ap_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intIsCashBasisFee;
	protected $m_strLastFeeDate;
	protected $m_intFeeGreaterFlatOrPercent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCashBasisFee = '1';
		$this->m_intFeeGreaterFlatOrPercent = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['is_cash_basis_fee'] ) && $boolDirectSet ) $this->set( 'm_intIsCashBasisFee', trim( $arrValues['is_cash_basis_fee'] ) ); elseif( isset( $arrValues['is_cash_basis_fee'] ) ) $this->setIsCashBasisFee( $arrValues['is_cash_basis_fee'] );
		if( isset( $arrValues['last_fee_date'] ) && $boolDirectSet ) $this->set( 'm_strLastFeeDate', trim( $arrValues['last_fee_date'] ) ); elseif( isset( $arrValues['last_fee_date'] ) ) $this->setLastFeeDate( $arrValues['last_fee_date'] );
		if( isset( $arrValues['fee_greater_flat_or_percent'] ) && $boolDirectSet ) $this->set( 'm_intFeeGreaterFlatOrPercent', trim( $arrValues['fee_greater_flat_or_percent'] ) ); elseif( isset( $arrValues['fee_greater_flat_or_percent'] ) ) $this->setFeeGreaterFlatOrPercent( $arrValues['fee_greater_flat_or_percent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setIsCashBasisFee( $intIsCashBasisFee ) {
		$this->set( 'm_intIsCashBasisFee', CStrings::strToIntDef( $intIsCashBasisFee, NULL, false ) );
	}

	public function getIsCashBasisFee() {
		return $this->m_intIsCashBasisFee;
	}

	public function sqlIsCashBasisFee() {
		return ( true == isset( $this->m_intIsCashBasisFee ) ) ? ( string ) $this->m_intIsCashBasisFee : '1';
	}

	public function setLastFeeDate( $strLastFeeDate ) {
		$this->set( 'm_strLastFeeDate', CStrings::strTrimDef( $strLastFeeDate, -1, NULL, true ) );
	}

	public function getLastFeeDate() {
		return $this->m_strLastFeeDate;
	}

	public function sqlLastFeeDate() {
		return ( true == isset( $this->m_strLastFeeDate ) ) ? '\'' . $this->m_strLastFeeDate . '\'' : 'NULL';
	}

	public function setFeeGreaterFlatOrPercent( $intFeeGreaterFlatOrPercent ) {
		$this->set( 'm_intFeeGreaterFlatOrPercent', CStrings::strToIntDef( $intFeeGreaterFlatOrPercent, NULL, false ) );
	}

	public function getFeeGreaterFlatOrPercent() {
		return $this->m_intFeeGreaterFlatOrPercent;
	}

	public function sqlFeeGreaterFlatOrPercent() {
		return ( true == isset( $this->m_intFeeGreaterFlatOrPercent ) ) ? ( string ) $this->m_intFeeGreaterFlatOrPercent : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, is_cash_basis_fee, last_fee_date, fee_greater_flat_or_percent, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlIsCashBasisFee() . ', ' .
 						$this->sqlLastFeeDate() . ', ' .
 						$this->sqlFeeGreaterFlatOrPercent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cash_basis_fee = ' . $this->sqlIsCashBasisFee() . ','; } elseif( true == array_key_exists( 'IsCashBasisFee', $this->getChangedColumns() ) ) { $strSql .= ' is_cash_basis_fee = ' . $this->sqlIsCashBasisFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_fee_date = ' . $this->sqlLastFeeDate() . ','; } elseif( true == array_key_exists( 'LastFeeDate', $this->getChangedColumns() ) ) { $strSql .= ' last_fee_date = ' . $this->sqlLastFeeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_greater_flat_or_percent = ' . $this->sqlFeeGreaterFlatOrPercent() . ','; } elseif( true == array_key_exists( 'FeeGreaterFlatOrPercent', $this->getChangedColumns() ) ) { $strSql .= ' fee_greater_flat_or_percent = ' . $this->sqlFeeGreaterFlatOrPercent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'is_cash_basis_fee' => $this->getIsCashBasisFee(),
			'last_fee_date' => $this->getLastFeeDate(),
			'fee_greater_flat_or_percent' => $this->getFeeGreaterFlatOrPercent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>