<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOffers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningConditionOffers extends CEosPluralBase {

	/**
	 * @return CScreeningConditionOffer[]
	 */
	public static function fetchScreeningConditionOffers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningConditionOffer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningConditionOffer
	 */
	public static function fetchScreeningConditionOffer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningConditionOffer', $objDatabase );
	}

	public static function fetchScreeningConditionOfferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_offers', $objDatabase );
	}

	public static function fetchScreeningConditionOfferByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditionOffer( sprintf( 'SELECT * FROM screening_condition_offers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionOffersByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningConditionOffers( sprintf( 'SELECT * FROM screening_condition_offers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionOffersByScreeningApplicationRequestIdByCid( $intScreeningApplicationRequestId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditionOffers( sprintf( 'SELECT * FROM screening_condition_offers WHERE screening_application_request_id = %d AND cid = %d', ( int ) $intScreeningApplicationRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningConditionOffersByScreeningConditionOfferStatusIdByCid( $intScreeningConditionOfferStatusId, $intCid, $objDatabase ) {
		return self::fetchScreeningConditionOffers( sprintf( 'SELECT * FROM screening_condition_offers WHERE screening_condition_offer_status_id = %d AND cid = %d', ( int ) $intScreeningConditionOfferStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>