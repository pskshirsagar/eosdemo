<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskExecution extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_task_executions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledTaskId;
	protected $m_intEventId;
	protected $m_strScheduledTaskAction;
	protected $m_intExecutedBy;
	protected $m_strExecutedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['scheduled_task_action'] ) && $boolDirectSet ) $this->set( 'm_strScheduledTaskAction', trim( stripcslashes( $arrValues['scheduled_task_action'] ) ) ); elseif( isset( $arrValues['scheduled_task_action'] ) ) $this->setScheduledTaskAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_task_action'] ) : $arrValues['scheduled_task_action'] );
		if( isset( $arrValues['executed_by'] ) && $boolDirectSet ) $this->set( 'm_intExecutedBy', trim( $arrValues['executed_by'] ) ); elseif( isset( $arrValues['executed_by'] ) ) $this->setExecutedBy( $arrValues['executed_by'] );
		if( isset( $arrValues['executed_on'] ) && $boolDirectSet ) $this->set( 'm_strExecutedOn', trim( $arrValues['executed_on'] ) ); elseif( isset( $arrValues['executed_on'] ) ) $this->setExecutedOn( $arrValues['executed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function sqlScheduledTaskId() {
		return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setScheduledTaskAction( $strScheduledTaskAction ) {
		$this->set( 'm_strScheduledTaskAction', CStrings::strTrimDef( $strScheduledTaskAction, -1, NULL, true ) );
	}

	public function getScheduledTaskAction() {
		return $this->m_strScheduledTaskAction;
	}

	public function sqlScheduledTaskAction() {
		return ( true == isset( $this->m_strScheduledTaskAction ) ) ? '\'' . addslashes( $this->m_strScheduledTaskAction ) . '\'' : 'NULL';
	}

	public function setExecutedBy( $intExecutedBy ) {
		$this->set( 'm_intExecutedBy', CStrings::strToIntDef( $intExecutedBy, NULL, false ) );
	}

	public function getExecutedBy() {
		return $this->m_intExecutedBy;
	}

	public function sqlExecutedBy() {
		return ( true == isset( $this->m_intExecutedBy ) ) ? ( string ) $this->m_intExecutedBy : 'NULL';
	}

	public function setExecutedOn( $strExecutedOn ) {
		$this->set( 'm_strExecutedOn', CStrings::strTrimDef( $strExecutedOn, -1, NULL, true ) );
	}

	public function getExecutedOn() {
		return $this->m_strExecutedOn;
	}

	public function sqlExecutedOn() {
		return ( true == isset( $this->m_strExecutedOn ) ) ? '\'' . $this->m_strExecutedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_task_id, event_id, scheduled_task_action, executed_by, executed_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlScheduledTaskId() . ', ' .
		          $this->sqlEventId() . ', ' .
		          $this->sqlScheduledTaskAction() . ', ' .
		          $this->sqlExecutedBy() . ', ' .
		          $this->sqlExecutedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_action = ' . $this->sqlScheduledTaskAction() . ','; } elseif( true == array_key_exists( 'ScheduledTaskAction', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_action = ' . $this->sqlScheduledTaskAction() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_by = ' . $this->sqlExecutedBy() . ','; } elseif( true == array_key_exists( 'ExecutedBy', $this->getChangedColumns() ) ) { $strSql .= ' executed_by = ' . $this->sqlExecutedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_on = ' . $this->sqlExecutedOn() . ','; } elseif( true == array_key_exists( 'ExecutedOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_on = ' . $this->sqlExecutedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_task_id' => $this->getScheduledTaskId(),
			'event_id' => $this->getEventId(),
			'scheduled_task_action' => $this->getScheduledTaskAction(),
			'executed_by' => $this->getExecutedBy(),
			'executed_on' => $this->getExecutedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>