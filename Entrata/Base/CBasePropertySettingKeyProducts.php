<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySettingKeyProducts
 * Do not add any new functions to this class.
 */

class CBasePropertySettingKeyProducts extends CEosPluralBase {

	/**
	 * @return CPropertySettingKeyProduct[]
	 */
	public static function fetchPropertySettingKeyProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertySettingKeyProduct', $objDatabase );
	}

	/**
	 * @return CPropertySettingKeyProduct
	 */
	public static function fetchPropertySettingKeyProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertySettingKeyProduct', $objDatabase );
	}

	public static function fetchPropertySettingKeyProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_setting_key_products', $objDatabase );
	}

	public static function fetchPropertySettingKeyProductById( $intId, $objDatabase ) {
		return self::fetchPropertySettingKeyProduct( sprintf( 'SELECT * FROM property_setting_key_products WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertySettingKeyProductsByPropertySettingKeyId( $intPropertySettingKeyId, $objDatabase ) {
		return self::fetchPropertySettingKeyProducts( sprintf( 'SELECT * FROM property_setting_key_products WHERE property_setting_key_id = %d', ( int ) $intPropertySettingKeyId ), $objDatabase );
	}

	public static function fetchPropertySettingKeyProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPropertySettingKeyProducts( sprintf( 'SELECT * FROM property_setting_key_products WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>