<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPaymentTransfer extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_payment_transfers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_intOldPropertyId;
	protected $m_intNewPropertyId;
	protected $m_intOldPaymentArTransactionId;
	protected $m_intNewPaymentArTransactionId;
	protected $m_intApPaymentId;
	protected $m_intInvoiceApHeaderId;
	protected $m_intPaymentApHeaderId;
	protected $m_intChargeArTransactionId;
	protected $m_intPaymentArTransactionId;
	protected $m_intOldBankAccountId;
	protected $m_intNewBankAccountId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['old_property_id'] ) && $boolDirectSet ) $this->set( 'm_intOldPropertyId', trim( $arrValues['old_property_id'] ) ); elseif( isset( $arrValues['old_property_id'] ) ) $this->setOldPropertyId( $arrValues['old_property_id'] );
		if( isset( $arrValues['new_property_id'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyId', trim( $arrValues['new_property_id'] ) ); elseif( isset( $arrValues['new_property_id'] ) ) $this->setNewPropertyId( $arrValues['new_property_id'] );
		if( isset( $arrValues['old_payment_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intOldPaymentArTransactionId', trim( $arrValues['old_payment_ar_transaction_id'] ) ); elseif( isset( $arrValues['old_payment_ar_transaction_id'] ) ) $this->setOldPaymentArTransactionId( $arrValues['old_payment_ar_transaction_id'] );
		if( isset( $arrValues['new_payment_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intNewPaymentArTransactionId', trim( $arrValues['new_payment_ar_transaction_id'] ) ); elseif( isset( $arrValues['new_payment_ar_transaction_id'] ) ) $this->setNewPaymentArTransactionId( $arrValues['new_payment_ar_transaction_id'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['invoice_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceApHeaderId', trim( $arrValues['invoice_ap_header_id'] ) ); elseif( isset( $arrValues['invoice_ap_header_id'] ) ) $this->setInvoiceApHeaderId( $arrValues['invoice_ap_header_id'] );
		if( isset( $arrValues['payment_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentApHeaderId', trim( $arrValues['payment_ap_header_id'] ) ); elseif( isset( $arrValues['payment_ap_header_id'] ) ) $this->setPaymentApHeaderId( $arrValues['payment_ap_header_id'] );
		if( isset( $arrValues['charge_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeArTransactionId', trim( $arrValues['charge_ar_transaction_id'] ) ); elseif( isset( $arrValues['charge_ar_transaction_id'] ) ) $this->setChargeArTransactionId( $arrValues['charge_ar_transaction_id'] );
		if( isset( $arrValues['payment_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentArTransactionId', trim( $arrValues['payment_ar_transaction_id'] ) ); elseif( isset( $arrValues['payment_ar_transaction_id'] ) ) $this->setPaymentArTransactionId( $arrValues['payment_ar_transaction_id'] );
		if( isset( $arrValues['old_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intOldBankAccountId', trim( $arrValues['old_bank_account_id'] ) ); elseif( isset( $arrValues['old_bank_account_id'] ) ) $this->setOldBankAccountId( $arrValues['old_bank_account_id'] );
		if( isset( $arrValues['new_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intNewBankAccountId', trim( $arrValues['new_bank_account_id'] ) ); elseif( isset( $arrValues['new_bank_account_id'] ) ) $this->setNewBankAccountId( $arrValues['new_bank_account_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setOldPropertyId( $intOldPropertyId ) {
		$this->set( 'm_intOldPropertyId', CStrings::strToIntDef( $intOldPropertyId, NULL, false ) );
	}

	public function getOldPropertyId() {
		return $this->m_intOldPropertyId;
	}

	public function sqlOldPropertyId() {
		return ( true == isset( $this->m_intOldPropertyId ) ) ? ( string ) $this->m_intOldPropertyId : 'NULL';
	}

	public function setNewPropertyId( $intNewPropertyId ) {
		$this->set( 'm_intNewPropertyId', CStrings::strToIntDef( $intNewPropertyId, NULL, false ) );
	}

	public function getNewPropertyId() {
		return $this->m_intNewPropertyId;
	}

	public function sqlNewPropertyId() {
		return ( true == isset( $this->m_intNewPropertyId ) ) ? ( string ) $this->m_intNewPropertyId : 'NULL';
	}

	public function setOldPaymentArTransactionId( $intOldPaymentArTransactionId ) {
		$this->set( 'm_intOldPaymentArTransactionId', CStrings::strToIntDef( $intOldPaymentArTransactionId, NULL, false ) );
	}

	public function getOldPaymentArTransactionId() {
		return $this->m_intOldPaymentArTransactionId;
	}

	public function sqlOldPaymentArTransactionId() {
		return ( true == isset( $this->m_intOldPaymentArTransactionId ) ) ? ( string ) $this->m_intOldPaymentArTransactionId : 'NULL';
	}

	public function setNewPaymentArTransactionId( $intNewPaymentArTransactionId ) {
		$this->set( 'm_intNewPaymentArTransactionId', CStrings::strToIntDef( $intNewPaymentArTransactionId, NULL, false ) );
	}

	public function getNewPaymentArTransactionId() {
		return $this->m_intNewPaymentArTransactionId;
	}

	public function sqlNewPaymentArTransactionId() {
		return ( true == isset( $this->m_intNewPaymentArTransactionId ) ) ? ( string ) $this->m_intNewPaymentArTransactionId : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setInvoiceApHeaderId( $intInvoiceApHeaderId ) {
		$this->set( 'm_intInvoiceApHeaderId', CStrings::strToIntDef( $intInvoiceApHeaderId, NULL, false ) );
	}

	public function getInvoiceApHeaderId() {
		return $this->m_intInvoiceApHeaderId;
	}

	public function sqlInvoiceApHeaderId() {
		return ( true == isset( $this->m_intInvoiceApHeaderId ) ) ? ( string ) $this->m_intInvoiceApHeaderId : 'NULL';
	}

	public function setPaymentApHeaderId( $intPaymentApHeaderId ) {
		$this->set( 'm_intPaymentApHeaderId', CStrings::strToIntDef( $intPaymentApHeaderId, NULL, false ) );
	}

	public function getPaymentApHeaderId() {
		return $this->m_intPaymentApHeaderId;
	}

	public function sqlPaymentApHeaderId() {
		return ( true == isset( $this->m_intPaymentApHeaderId ) ) ? ( string ) $this->m_intPaymentApHeaderId : 'NULL';
	}

	public function setChargeArTransactionId( $intChargeArTransactionId ) {
		$this->set( 'm_intChargeArTransactionId', CStrings::strToIntDef( $intChargeArTransactionId, NULL, false ) );
	}

	public function getChargeArTransactionId() {
		return $this->m_intChargeArTransactionId;
	}

	public function sqlChargeArTransactionId() {
		return ( true == isset( $this->m_intChargeArTransactionId ) ) ? ( string ) $this->m_intChargeArTransactionId : 'NULL';
	}

	public function setPaymentArTransactionId( $intPaymentArTransactionId ) {
		$this->set( 'm_intPaymentArTransactionId', CStrings::strToIntDef( $intPaymentArTransactionId, NULL, false ) );
	}

	public function getPaymentArTransactionId() {
		return $this->m_intPaymentArTransactionId;
	}

	public function sqlPaymentArTransactionId() {
		return ( true == isset( $this->m_intPaymentArTransactionId ) ) ? ( string ) $this->m_intPaymentArTransactionId : 'NULL';
	}

	public function setOldBankAccountId( $intOldBankAccountId ) {
		$this->set( 'm_intOldBankAccountId', CStrings::strToIntDef( $intOldBankAccountId, NULL, false ) );
	}

	public function getOldBankAccountId() {
		return $this->m_intOldBankAccountId;
	}

	public function sqlOldBankAccountId() {
		return ( true == isset( $this->m_intOldBankAccountId ) ) ? ( string ) $this->m_intOldBankAccountId : 'NULL';
	}

	public function setNewBankAccountId( $intNewBankAccountId ) {
		$this->set( 'm_intNewBankAccountId', CStrings::strToIntDef( $intNewBankAccountId, NULL, false ) );
	}

	public function getNewBankAccountId() {
		return $this->m_intNewBankAccountId;
	}

	public function sqlNewBankAccountId() {
		return ( true == isset( $this->m_intNewBankAccountId ) ) ? ( string ) $this->m_intNewBankAccountId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ar_payment_id, old_property_id, new_property_id, old_payment_ar_transaction_id, new_payment_ar_transaction_id, ap_payment_id, invoice_ap_header_id, payment_ap_header_id, charge_ar_transaction_id, payment_ar_transaction_id, old_bank_account_id, new_bank_account_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlOldPropertyId() . ', ' .
 						$this->sqlNewPropertyId() . ', ' .
 						$this->sqlOldPaymentArTransactionId() . ', ' .
 						$this->sqlNewPaymentArTransactionId() . ', ' .
 						$this->sqlApPaymentId() . ', ' .
 						$this->sqlInvoiceApHeaderId() . ', ' .
 						$this->sqlPaymentApHeaderId() . ', ' .
 						$this->sqlChargeArTransactionId() . ', ' .
 						$this->sqlPaymentArTransactionId() . ', ' .
 						$this->sqlOldBankAccountId() . ', ' .
 						$this->sqlNewBankAccountId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_property_id = ' . $this->sqlOldPropertyId() . ','; } elseif( true == array_key_exists( 'OldPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' old_property_id = ' . $this->sqlOldPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_id = ' . $this->sqlNewPropertyId() . ','; } elseif( true == array_key_exists( 'NewPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' new_property_id = ' . $this->sqlNewPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_payment_ar_transaction_id = ' . $this->sqlOldPaymentArTransactionId() . ','; } elseif( true == array_key_exists( 'OldPaymentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' old_payment_ar_transaction_id = ' . $this->sqlOldPaymentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_payment_ar_transaction_id = ' . $this->sqlNewPaymentArTransactionId() . ','; } elseif( true == array_key_exists( 'NewPaymentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' new_payment_ar_transaction_id = ' . $this->sqlNewPaymentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_ap_header_id = ' . $this->sqlInvoiceApHeaderId() . ','; } elseif( true == array_key_exists( 'InvoiceApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_ap_header_id = ' . $this->sqlInvoiceApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId() . ','; } elseif( true == array_key_exists( 'PaymentApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; } elseif( true == array_key_exists( 'ChargeArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ar_transaction_id = ' . $this->sqlPaymentArTransactionId() . ','; } elseif( true == array_key_exists( 'PaymentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' payment_ar_transaction_id = ' . $this->sqlPaymentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_bank_account_id = ' . $this->sqlOldBankAccountId() . ','; } elseif( true == array_key_exists( 'OldBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' old_bank_account_id = ' . $this->sqlOldBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_bank_account_id = ' . $this->sqlNewBankAccountId() . ','; } elseif( true == array_key_exists( 'NewBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' new_bank_account_id = ' . $this->sqlNewBankAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'old_property_id' => $this->getOldPropertyId(),
			'new_property_id' => $this->getNewPropertyId(),
			'old_payment_ar_transaction_id' => $this->getOldPaymentArTransactionId(),
			'new_payment_ar_transaction_id' => $this->getNewPaymentArTransactionId(),
			'ap_payment_id' => $this->getApPaymentId(),
			'invoice_ap_header_id' => $this->getInvoiceApHeaderId(),
			'payment_ap_header_id' => $this->getPaymentApHeaderId(),
			'charge_ar_transaction_id' => $this->getChargeArTransactionId(),
			'payment_ar_transaction_id' => $this->getPaymentArTransactionId(),
			'old_bank_account_id' => $this->getOldBankAccountId(),
			'new_bank_account_id' => $this->getNewBankAccountId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>