<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequests extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequest[]
	 */
	public static function fetchMaintenanceRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequest
	 */
	public static function fetchMaintenanceRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequest::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'view_maintenance_requests', $objDatabase );
	}

	public static function fetchMaintenanceRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequest( sprintf( 'SELECT * FROM view_maintenance_requests WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByParentMaintenanceRequestIdByCid( $intParentMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE parent_maintenance_request_id = %d AND cid = %d', $intParentMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByJobProjectIdByCid( $intJobProjectId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE job_project_id = %d AND cid = %d', $intJobProjectId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenancePriorityIdByCid( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_priority_id = %d AND cid = %d', $intMaintenancePriorityId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceStatusIdByCid( $intMaintenanceStatusId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_status_id = %d AND cid = %d', $intMaintenanceStatusId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByPropertyUnitMaintenanceLocationIdByCid( $intPropertyUnitMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE property_unit_maintenance_location_id = %d AND cid = %d', $intPropertyUnitMaintenanceLocationId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_problem_id = %d AND cid = %d', $intMaintenanceProblemId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsBySubMaintenanceProblemIdByCid( $intSubMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE sub_maintenance_problem_id = %d AND cid = %d', $intSubMaintenanceProblemId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByRecurringMaintenanceRequestIdByCid( $intRecurringMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE recurring_maintenance_request_id = %d AND cid = %d', $intRecurringMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceTemplateIdByCid( $intMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_template_id = %d AND cid = %d', $intMaintenanceTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceRequestTypeIdByCid( $intMaintenanceRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_request_type_id = %d AND cid = %d', $intMaintenanceRequestTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMainPhoneNumberTypeIdByCid( $intMainPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE main_phone_number_type_id = %d AND cid = %d', $intMainPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByAltPhoneNumberTypeIdByCid( $intAltPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE alt_phone_number_type_id = %d AND cid = %d', $intAltPhoneNumberTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMainMessageOperatorIdByCid( $intMainMessageOperatorId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE main_message_operator_id = %d AND cid = %d', $intMainMessageOperatorId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByAltMessageOperatorIdByCid( $intAltMessageOperatorId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE alt_message_operator_id = %d AND cid = %d', $intAltMessageOperatorId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE asset_id = %d AND cid = %d', $intAssetId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByAddOnIdByCid( $intAddOnId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE add_on_id = %d AND cid = %d', $intAddOnId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceGroupIdByCid( $intMaintenanceGroupId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_group_id = %d AND cid = %d', $intMaintenanceGroupId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceExceptionIdByCid( $intMaintenanceExceptionId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_exception_id = %d AND cid = %d', $intMaintenanceExceptionId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestsByMaintenanceRequestMediumIdByCid( $intMaintenanceRequestMediumId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequests( sprintf( 'SELECT * FROM view_maintenance_requests WHERE maintenance_request_medium_id = %d AND cid = %d', $intMaintenanceRequestMediumId, $intCid ), $objDatabase );
	}

}
?>