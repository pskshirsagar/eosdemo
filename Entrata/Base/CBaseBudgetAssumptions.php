<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetAssumptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetAssumptions extends CEosPluralBase {

	/**
	 * @return CBudgetAssumption[]
	 */
	public static function fetchBudgetAssumptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetAssumption::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetAssumption
	 */
	public static function fetchBudgetAssumption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetAssumption::class, $objDatabase );
	}

	public static function fetchBudgetAssumptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_assumptions', $objDatabase );
	}

	public static function fetchBudgetAssumptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumption( sprintf( 'SELECT * FROM budget_assumptions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptions( sprintf( 'SELECT * FROM budget_assumptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptions( sprintf( 'SELECT * FROM budget_assumptions WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionsByBudgetWorkbookAssumptionIdByCid( $intBudgetWorkbookAssumptionId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptions( sprintf( 'SELECT * FROM budget_assumptions WHERE budget_workbook_assumption_id = %d AND cid = %d', $intBudgetWorkbookAssumptionId, $intCid ), $objDatabase );
	}

}
?>