<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialInsurancePolicies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialInsurancePolicies extends CEosPluralBase {

	/**
	 * @return CCommercialInsurancePolicy[]
	 */
	public static function fetchCommercialInsurancePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCommercialInsurancePolicy::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialInsurancePolicy
	 */
	public static function fetchCommercialInsurancePolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCommercialInsurancePolicy::class, $objDatabase );
	}

	public static function fetchCommercialInsurancePolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_insurance_policies', $objDatabase );
	}

	public static function fetchCommercialInsurancePolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicy( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByCommercialSuiteIdByCid( $intCommercialSuiteId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE commercial_suite_id = %d AND cid = %d', $intCommercialSuiteId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByInsurancePolicyTypeIdByCid( $intInsurancePolicyTypeId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE insurance_policy_type_id = %d AND cid = %d', $intInsurancePolicyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialInsurancePoliciesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchCommercialInsurancePolicies( sprintf( 'SELECT * FROM commercial_insurance_policies WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

}
?>