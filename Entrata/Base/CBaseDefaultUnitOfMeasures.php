<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultUnitOfMeasures
 * Do not add any new functions to this class.
 */

class CBaseDefaultUnitOfMeasures extends CEosPluralBase {

	/**
	 * @return CDefaultUnitOfMeasure[]
	 */
	public static function fetchDefaultUnitOfMeasures( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultUnitOfMeasure::class, $objDatabase );
	}

	/**
	 * @return CDefaultUnitOfMeasure
	 */
	public static function fetchDefaultUnitOfMeasure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultUnitOfMeasure::class, $objDatabase );
	}

	public static function fetchDefaultUnitOfMeasureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_unit_of_measures', $objDatabase );
	}

	public static function fetchDefaultUnitOfMeasureById( $intId, $objDatabase ) {
		return self::fetchDefaultUnitOfMeasure( sprintf( 'SELECT * FROM default_unit_of_measures WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultUnitOfMeasuresByUnitOfMeasureTypeId( $intUnitOfMeasureTypeId, $objDatabase ) {
		return self::fetchDefaultUnitOfMeasures( sprintf( 'SELECT * FROM default_unit_of_measures WHERE unit_of_measure_type_id = %d', $intUnitOfMeasureTypeId ), $objDatabase );
	}

}
?>