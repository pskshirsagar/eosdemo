<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedRate extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_rates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intUnitKindId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intSpaceConfigurationId;
	protected $m_intLeaseTermMonths;
	protected $m_fltBaseRent;
	protected $m_fltAmenityRent;
	protected $m_fltSpecialRent;
	protected $m_fltAddOnRent;
	protected $m_fltMarketRent;
	protected $m_fltBaseDeposit;
	protected $m_fltAmenityDeposit;
	protected $m_fltSpecialDeposit;
	protected $m_fltAddOnDeposit;
	protected $m_fltTotalDeposit;
	protected $m_strCachedDatetime;
	protected $m_strEffectiveDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolShowOnWebsite;
	protected $m_intOccupancyTypeId;
	protected $m_intArTriggerId;
	protected $m_fltBaseRentMonthly;
	protected $m_fltAmenityRentMonthly;
	protected $m_fltSpecialRentMonthly;
	protected $m_fltAddOnRentMonthly;
	protected $m_fltMarketRentMonthly;
	protected $m_boolIncludeTaxInAdvertisedRent;
	protected $m_boolIncludeOtherInAdvertisedRent;
	protected $m_fltBaseAmount;
	protected $m_fltBaseTax;
	protected $m_fltAmenityAmount;
	protected $m_fltAmenityTax;
	protected $m_fltSpecialAmount;
	protected $m_fltSpecialTax;
	protected $m_fltAddOnAmount;
	protected $m_fltAddOnTax;
	protected $m_fltBaseAmountMonthly;
	protected $m_fltBaseTaxMonthly;
	protected $m_fltAmenityAmountMonthly;
	protected $m_fltAmenityTaxMonthly;
	protected $m_fltSpecialAmountMonthly;
	protected $m_fltSpecialTaxMonthly;
	protected $m_fltAddOnAmountMonthly;
	protected $m_fltAddOnTaxMonthly;
	protected $m_fltMarketAmountMonthly;
	protected $m_fltEntrataMarketAmountMonthly;
	protected $m_strEffectiveRange;

	public function __construct() {
		parent::__construct();

		$this->m_intLeaseTermId = '0';
		$this->m_intLeaseStartWindowId = '0';
		$this->m_intSpaceConfigurationId = '0';
		$this->m_intLeaseTermMonths = '0';
		$this->m_fltBaseRent = '0';
		$this->m_fltAmenityRent = '0';
		$this->m_fltSpecialRent = '0';
		$this->m_fltAddOnRent = '0';
		$this->m_fltMarketRent = '0';
		$this->m_fltBaseDeposit = '0';
		$this->m_fltAmenityDeposit = '0';
		$this->m_fltSpecialDeposit = '0';
		$this->m_fltAddOnDeposit = '0';
		$this->m_fltTotalDeposit = '0';
		$this->m_strCachedDatetime = 'now()';
		$this->m_boolShowOnWebsite = true;
		$this->m_intOccupancyTypeId = '1';
		$this->m_boolIncludeTaxInAdvertisedRent = false;
		$this->m_boolIncludeOtherInAdvertisedRent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermMonths', trim( $arrValues['lease_term_months'] ) ); elseif( isset( $arrValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrValues['lease_term_months'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnRent', trim( $arrValues['add_on_rent'] ) ); elseif( isset( $arrValues['add_on_rent'] ) ) $this->setAddOnRent( $arrValues['add_on_rent'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltBaseDeposit', trim( $arrValues['base_deposit'] ) ); elseif( isset( $arrValues['base_deposit'] ) ) $this->setBaseDeposit( $arrValues['base_deposit'] );
		if( isset( $arrValues['amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityDeposit', trim( $arrValues['amenity_deposit'] ) ); elseif( isset( $arrValues['amenity_deposit'] ) ) $this->setAmenityDeposit( $arrValues['amenity_deposit'] );
		if( isset( $arrValues['special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialDeposit', trim( $arrValues['special_deposit'] ) ); elseif( isset( $arrValues['special_deposit'] ) ) $this->setSpecialDeposit( $arrValues['special_deposit'] );
		if( isset( $arrValues['add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnDeposit', trim( $arrValues['add_on_deposit'] ) ); elseif( isset( $arrValues['add_on_deposit'] ) ) $this->setAddOnDeposit( $arrValues['add_on_deposit'] );
		if( isset( $arrValues['total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltTotalDeposit', trim( $arrValues['total_deposit'] ) ); elseif( isset( $arrValues['total_deposit'] ) ) $this->setTotalDeposit( $arrValues['total_deposit'] );
		if( isset( $arrValues['cached_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCachedDatetime', trim( $arrValues['cached_datetime'] ) ); elseif( isset( $arrValues['cached_datetime'] ) ) $this->setCachedDatetime( $arrValues['cached_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRentMonthly', trim( $arrValues['base_rent_monthly'] ) ); elseif( isset( $arrValues['base_rent_monthly'] ) ) $this->setBaseRentMonthly( $arrValues['base_rent_monthly'] );
		if( isset( $arrValues['amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityRentMonthly', trim( $arrValues['amenity_rent_monthly'] ) ); elseif( isset( $arrValues['amenity_rent_monthly'] ) ) $this->setAmenityRentMonthly( $arrValues['amenity_rent_monthly'] );
		if( isset( $arrValues['special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialRentMonthly', trim( $arrValues['special_rent_monthly'] ) ); elseif( isset( $arrValues['special_rent_monthly'] ) ) $this->setSpecialRentMonthly( $arrValues['special_rent_monthly'] );
		if( isset( $arrValues['add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnRentMonthly', trim( $arrValues['add_on_rent_monthly'] ) ); elseif( isset( $arrValues['add_on_rent_monthly'] ) ) $this->setAddOnRentMonthly( $arrValues['add_on_rent_monthly'] );
		if( isset( $arrValues['market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRentMonthly', trim( $arrValues['market_rent_monthly'] ) ); elseif( isset( $arrValues['market_rent_monthly'] ) ) $this->setMarketRentMonthly( $arrValues['market_rent_monthly'] );
		if( isset( $arrValues['include_tax_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeTaxInAdvertisedRent', trim( stripcslashes( $arrValues['include_tax_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_tax_in_advertised_rent'] ) ) $this->setIncludeTaxInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_tax_in_advertised_rent'] ) : $arrValues['include_tax_in_advertised_rent'] );
		if( isset( $arrValues['include_other_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeOtherInAdvertisedRent', trim( stripcslashes( $arrValues['include_other_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_other_in_advertised_rent'] ) ) $this->setIncludeOtherInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_other_in_advertised_rent'] ) : $arrValues['include_other_in_advertised_rent'] );
		if( isset( $arrValues['base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBaseAmount', trim( $arrValues['base_amount'] ) ); elseif( isset( $arrValues['base_amount'] ) ) $this->setBaseAmount( $arrValues['base_amount'] );
		if( isset( $arrValues['base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltBaseTax', trim( $arrValues['base_tax'] ) ); elseif( isset( $arrValues['base_tax'] ) ) $this->setBaseTax( $arrValues['base_tax'] );
		if( isset( $arrValues['amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityAmount', trim( $arrValues['amenity_amount'] ) ); elseif( isset( $arrValues['amenity_amount'] ) ) $this->setAmenityAmount( $arrValues['amenity_amount'] );
		if( isset( $arrValues['amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityTax', trim( $arrValues['amenity_tax'] ) ); elseif( isset( $arrValues['amenity_tax'] ) ) $this->setAmenityTax( $arrValues['amenity_tax'] );
		if( isset( $arrValues['special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialAmount', trim( $arrValues['special_amount'] ) ); elseif( isset( $arrValues['special_amount'] ) ) $this->setSpecialAmount( $arrValues['special_amount'] );
		if( isset( $arrValues['special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialTax', trim( $arrValues['special_tax'] ) ); elseif( isset( $arrValues['special_tax'] ) ) $this->setSpecialTax( $arrValues['special_tax'] );
		if( isset( $arrValues['add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnAmount', trim( $arrValues['add_on_amount'] ) ); elseif( isset( $arrValues['add_on_amount'] ) ) $this->setAddOnAmount( $arrValues['add_on_amount'] );
		if( isset( $arrValues['add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnTax', trim( $arrValues['add_on_tax'] ) ); elseif( isset( $arrValues['add_on_tax'] ) ) $this->setAddOnTax( $arrValues['add_on_tax'] );
		if( isset( $arrValues['base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseAmountMonthly', trim( $arrValues['base_amount_monthly'] ) ); elseif( isset( $arrValues['base_amount_monthly'] ) ) $this->setBaseAmountMonthly( $arrValues['base_amount_monthly'] );
		if( isset( $arrValues['base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseTaxMonthly', trim( $arrValues['base_tax_monthly'] ) ); elseif( isset( $arrValues['base_tax_monthly'] ) ) $this->setBaseTaxMonthly( $arrValues['base_tax_monthly'] );
		if( isset( $arrValues['amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityAmountMonthly', trim( $arrValues['amenity_amount_monthly'] ) ); elseif( isset( $arrValues['amenity_amount_monthly'] ) ) $this->setAmenityAmountMonthly( $arrValues['amenity_amount_monthly'] );
		if( isset( $arrValues['amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityTaxMonthly', trim( $arrValues['amenity_tax_monthly'] ) ); elseif( isset( $arrValues['amenity_tax_monthly'] ) ) $this->setAmenityTaxMonthly( $arrValues['amenity_tax_monthly'] );
		if( isset( $arrValues['special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialAmountMonthly', trim( $arrValues['special_amount_monthly'] ) ); elseif( isset( $arrValues['special_amount_monthly'] ) ) $this->setSpecialAmountMonthly( $arrValues['special_amount_monthly'] );
		if( isset( $arrValues['special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialTaxMonthly', trim( $arrValues['special_tax_monthly'] ) ); elseif( isset( $arrValues['special_tax_monthly'] ) ) $this->setSpecialTaxMonthly( $arrValues['special_tax_monthly'] );
		if( isset( $arrValues['add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnAmountMonthly', trim( $arrValues['add_on_amount_monthly'] ) ); elseif( isset( $arrValues['add_on_amount_monthly'] ) ) $this->setAddOnAmountMonthly( $arrValues['add_on_amount_monthly'] );
		if( isset( $arrValues['add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnTaxMonthly', trim( $arrValues['add_on_tax_monthly'] ) ); elseif( isset( $arrValues['add_on_tax_monthly'] ) ) $this->setAddOnTaxMonthly( $arrValues['add_on_tax_monthly'] );
		if( isset( $arrValues['market_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMarketAmountMonthly', trim( $arrValues['market_amount_monthly'] ) ); elseif( isset( $arrValues['market_amount_monthly'] ) ) $this->setMarketAmountMonthly( $arrValues['market_amount_monthly'] );
		if( isset( $arrValues['entrata_market_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMarketAmountMonthly', trim( $arrValues['entrata_market_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_market_amount_monthly'] ) ) $this->setEntrataMarketAmountMonthly( $arrValues['entrata_market_amount_monthly'] );
		if( isset( $arrValues['effective_range'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveRange', trim( $arrValues['effective_range'] ) ); elseif( isset( $arrValues['effective_range'] ) ) $this->setEffectiveRange( $arrValues['effective_range'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : '0';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : '0';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : '0';
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->set( 'm_intLeaseTermMonths', CStrings::strToIntDef( $intLeaseTermMonths, NULL, false ) );
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	public function sqlLeaseTermMonths() {
		return ( true == isset( $this->m_intLeaseTermMonths ) ) ? ( string ) $this->m_intLeaseTermMonths : '0';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : '0';
	}

	public function setAmenityRent( $fltAmenityRent ) {
		$this->set( 'm_fltAmenityRent', CStrings::strToFloatDef( $fltAmenityRent, NULL, false, 2 ) );
	}

	public function getAmenityRent() {
		return $this->m_fltAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_fltAmenityRent ) ) ? ( string ) $this->m_fltAmenityRent : '0';
	}

	public function setSpecialRent( $fltSpecialRent ) {
		$this->set( 'm_fltSpecialRent', CStrings::strToFloatDef( $fltSpecialRent, NULL, false, 2 ) );
	}

	public function getSpecialRent() {
		return $this->m_fltSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_fltSpecialRent ) ) ? ( string ) $this->m_fltSpecialRent : '0';
	}

	public function setAddOnRent( $fltAddOnRent ) {
		$this->set( 'm_fltAddOnRent', CStrings::strToFloatDef( $fltAddOnRent, NULL, false, 2 ) );
	}

	public function getAddOnRent() {
		return $this->m_fltAddOnRent;
	}

	public function sqlAddOnRent() {
		return ( true == isset( $this->m_fltAddOnRent ) ) ? ( string ) $this->m_fltAddOnRent : '0';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : '0';
	}

	public function setBaseDeposit( $fltBaseDeposit ) {
		$this->set( 'm_fltBaseDeposit', CStrings::strToFloatDef( $fltBaseDeposit, NULL, false, 2 ) );
	}

	public function getBaseDeposit() {
		return $this->m_fltBaseDeposit;
	}

	public function sqlBaseDeposit() {
		return ( true == isset( $this->m_fltBaseDeposit ) ) ? ( string ) $this->m_fltBaseDeposit : '0';
	}

	public function setAmenityDeposit( $fltAmenityDeposit ) {
		$this->set( 'm_fltAmenityDeposit', CStrings::strToFloatDef( $fltAmenityDeposit, NULL, false, 2 ) );
	}

	public function getAmenityDeposit() {
		return $this->m_fltAmenityDeposit;
	}

	public function sqlAmenityDeposit() {
		return ( true == isset( $this->m_fltAmenityDeposit ) ) ? ( string ) $this->m_fltAmenityDeposit : '0';
	}

	public function setSpecialDeposit( $fltSpecialDeposit ) {
		$this->set( 'm_fltSpecialDeposit', CStrings::strToFloatDef( $fltSpecialDeposit, NULL, false, 2 ) );
	}

	public function getSpecialDeposit() {
		return $this->m_fltSpecialDeposit;
	}

	public function sqlSpecialDeposit() {
		return ( true == isset( $this->m_fltSpecialDeposit ) ) ? ( string ) $this->m_fltSpecialDeposit : '0';
	}

	public function setAddOnDeposit( $fltAddOnDeposit ) {
		$this->set( 'm_fltAddOnDeposit', CStrings::strToFloatDef( $fltAddOnDeposit, NULL, false, 2 ) );
	}

	public function getAddOnDeposit() {
		return $this->m_fltAddOnDeposit;
	}

	public function sqlAddOnDeposit() {
		return ( true == isset( $this->m_fltAddOnDeposit ) ) ? ( string ) $this->m_fltAddOnDeposit : '0';
	}

	public function setTotalDeposit( $fltTotalDeposit ) {
		$this->set( 'm_fltTotalDeposit', CStrings::strToFloatDef( $fltTotalDeposit, NULL, false, 2 ) );
	}

	public function getTotalDeposit() {
		return $this->m_fltTotalDeposit;
	}

	public function sqlTotalDeposit() {
		return ( true == isset( $this->m_fltTotalDeposit ) ) ? ( string ) $this->m_fltTotalDeposit : '0';
	}

	public function setCachedDatetime( $strCachedDatetime ) {
		$this->set( 'm_strCachedDatetime', CStrings::strTrimDef( $strCachedDatetime, -1, NULL, true ) );
	}

	public function getCachedDatetime() {
		return $this->m_strCachedDatetime;
	}

	public function sqlCachedDatetime() {
		return ( true == isset( $this->m_strCachedDatetime ) ) ? '\'' . $this->m_strCachedDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setBaseRentMonthly( $fltBaseRentMonthly ) {
		$this->set( 'm_fltBaseRentMonthly', CStrings::strToFloatDef( $fltBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getBaseRentMonthly() {
		return $this->m_fltBaseRentMonthly;
	}

	public function sqlBaseRentMonthly() {
		return ( true == isset( $this->m_fltBaseRentMonthly ) ) ? ( string ) $this->m_fltBaseRentMonthly : 'NULL';
	}

	public function setAmenityRentMonthly( $fltAmenityRentMonthly ) {
		$this->set( 'm_fltAmenityRentMonthly', CStrings::strToFloatDef( $fltAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getAmenityRentMonthly() {
		return $this->m_fltAmenityRentMonthly;
	}

	public function sqlAmenityRentMonthly() {
		return ( true == isset( $this->m_fltAmenityRentMonthly ) ) ? ( string ) $this->m_fltAmenityRentMonthly : 'NULL';
	}

	public function setSpecialRentMonthly( $fltSpecialRentMonthly ) {
		$this->set( 'm_fltSpecialRentMonthly', CStrings::strToFloatDef( $fltSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getSpecialRentMonthly() {
		return $this->m_fltSpecialRentMonthly;
	}

	public function sqlSpecialRentMonthly() {
		return ( true == isset( $this->m_fltSpecialRentMonthly ) ) ? ( string ) $this->m_fltSpecialRentMonthly : 'NULL';
	}

	public function setAddOnRentMonthly( $fltAddOnRentMonthly ) {
		$this->set( 'm_fltAddOnRentMonthly', CStrings::strToFloatDef( $fltAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getAddOnRentMonthly() {
		return $this->m_fltAddOnRentMonthly;
	}

	public function sqlAddOnRentMonthly() {
		return ( true == isset( $this->m_fltAddOnRentMonthly ) ) ? ( string ) $this->m_fltAddOnRentMonthly : 'NULL';
	}

	public function setMarketRentMonthly( $fltMarketRentMonthly ) {
		$this->set( 'm_fltMarketRentMonthly', CStrings::strToFloatDef( $fltMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getMarketRentMonthly() {
		return $this->m_fltMarketRentMonthly;
	}

	public function sqlMarketRentMonthly() {
		return ( true == isset( $this->m_fltMarketRentMonthly ) ) ? ( string ) $this->m_fltMarketRentMonthly : 'NULL';
	}

	public function setIncludeTaxInAdvertisedRent( $boolIncludeTaxInAdvertisedRent ) {
		$this->set( 'm_boolIncludeTaxInAdvertisedRent', CStrings::strToBool( $boolIncludeTaxInAdvertisedRent ) );
	}

	public function getIncludeTaxInAdvertisedRent() {
		return $this->m_boolIncludeTaxInAdvertisedRent;
	}

	public function sqlIncludeTaxInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeTaxInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeTaxInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeOtherInAdvertisedRent( $boolIncludeOtherInAdvertisedRent ) {
		$this->set( 'm_boolIncludeOtherInAdvertisedRent', CStrings::strToBool( $boolIncludeOtherInAdvertisedRent ) );
	}

	public function getIncludeOtherInAdvertisedRent() {
		return $this->m_boolIncludeOtherInAdvertisedRent;
	}

	public function sqlIncludeOtherInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeOtherInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeOtherInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBaseAmount( $fltBaseAmount ) {
		$this->set( 'm_fltBaseAmount', CStrings::strToFloatDef( $fltBaseAmount, NULL, false, 2 ) );
	}

	public function getBaseAmount() {
		return $this->m_fltBaseAmount;
	}

	public function sqlBaseAmount() {
		return ( true == isset( $this->m_fltBaseAmount ) ) ? ( string ) $this->m_fltBaseAmount : 'NULL';
	}

	public function setBaseTax( $fltBaseTax ) {
		$this->set( 'm_fltBaseTax', CStrings::strToFloatDef( $fltBaseTax, NULL, false, 2 ) );
	}

	public function getBaseTax() {
		return $this->m_fltBaseTax;
	}

	public function sqlBaseTax() {
		return ( true == isset( $this->m_fltBaseTax ) ) ? ( string ) $this->m_fltBaseTax : 'NULL';
	}

	public function setAmenityAmount( $fltAmenityAmount ) {
		$this->set( 'm_fltAmenityAmount', CStrings::strToFloatDef( $fltAmenityAmount, NULL, false, 2 ) );
	}

	public function getAmenityAmount() {
		return $this->m_fltAmenityAmount;
	}

	public function sqlAmenityAmount() {
		return ( true == isset( $this->m_fltAmenityAmount ) ) ? ( string ) $this->m_fltAmenityAmount : 'NULL';
	}

	public function setAmenityTax( $fltAmenityTax ) {
		$this->set( 'm_fltAmenityTax', CStrings::strToFloatDef( $fltAmenityTax, NULL, false, 2 ) );
	}

	public function getAmenityTax() {
		return $this->m_fltAmenityTax;
	}

	public function sqlAmenityTax() {
		return ( true == isset( $this->m_fltAmenityTax ) ) ? ( string ) $this->m_fltAmenityTax : 'NULL';
	}

	public function setSpecialAmount( $fltSpecialAmount ) {
		$this->set( 'm_fltSpecialAmount', CStrings::strToFloatDef( $fltSpecialAmount, NULL, false, 2 ) );
	}

	public function getSpecialAmount() {
		return $this->m_fltSpecialAmount;
	}

	public function sqlSpecialAmount() {
		return ( true == isset( $this->m_fltSpecialAmount ) ) ? ( string ) $this->m_fltSpecialAmount : 'NULL';
	}

	public function setSpecialTax( $fltSpecialTax ) {
		$this->set( 'm_fltSpecialTax', CStrings::strToFloatDef( $fltSpecialTax, NULL, false, 2 ) );
	}

	public function getSpecialTax() {
		return $this->m_fltSpecialTax;
	}

	public function sqlSpecialTax() {
		return ( true == isset( $this->m_fltSpecialTax ) ) ? ( string ) $this->m_fltSpecialTax : 'NULL';
	}

	public function setAddOnAmount( $fltAddOnAmount ) {
		$this->set( 'm_fltAddOnAmount', CStrings::strToFloatDef( $fltAddOnAmount, NULL, false, 2 ) );
	}

	public function getAddOnAmount() {
		return $this->m_fltAddOnAmount;
	}

	public function sqlAddOnAmount() {
		return ( true == isset( $this->m_fltAddOnAmount ) ) ? ( string ) $this->m_fltAddOnAmount : 'NULL';
	}

	public function setAddOnTax( $fltAddOnTax ) {
		$this->set( 'm_fltAddOnTax', CStrings::strToFloatDef( $fltAddOnTax, NULL, false, 2 ) );
	}

	public function getAddOnTax() {
		return $this->m_fltAddOnTax;
	}

	public function sqlAddOnTax() {
		return ( true == isset( $this->m_fltAddOnTax ) ) ? ( string ) $this->m_fltAddOnTax : 'NULL';
	}

	public function setBaseAmountMonthly( $fltBaseAmountMonthly ) {
		$this->set( 'm_fltBaseAmountMonthly', CStrings::strToFloatDef( $fltBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getBaseAmountMonthly() {
		return $this->m_fltBaseAmountMonthly;
	}

	public function sqlBaseAmountMonthly() {
		return ( true == isset( $this->m_fltBaseAmountMonthly ) ) ? ( string ) $this->m_fltBaseAmountMonthly : 'NULL';
	}

	public function setBaseTaxMonthly( $fltBaseTaxMonthly ) {
		$this->set( 'm_fltBaseTaxMonthly', CStrings::strToFloatDef( $fltBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getBaseTaxMonthly() {
		return $this->m_fltBaseTaxMonthly;
	}

	public function sqlBaseTaxMonthly() {
		return ( true == isset( $this->m_fltBaseTaxMonthly ) ) ? ( string ) $this->m_fltBaseTaxMonthly : 'NULL';
	}

	public function setAmenityAmountMonthly( $fltAmenityAmountMonthly ) {
		$this->set( 'm_fltAmenityAmountMonthly', CStrings::strToFloatDef( $fltAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getAmenityAmountMonthly() {
		return $this->m_fltAmenityAmountMonthly;
	}

	public function sqlAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltAmenityAmountMonthly ) ) ? ( string ) $this->m_fltAmenityAmountMonthly : 'NULL';
	}

	public function setAmenityTaxMonthly( $fltAmenityTaxMonthly ) {
		$this->set( 'm_fltAmenityTaxMonthly', CStrings::strToFloatDef( $fltAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getAmenityTaxMonthly() {
		return $this->m_fltAmenityTaxMonthly;
	}

	public function sqlAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltAmenityTaxMonthly ) ) ? ( string ) $this->m_fltAmenityTaxMonthly : 'NULL';
	}

	public function setSpecialAmountMonthly( $fltSpecialAmountMonthly ) {
		$this->set( 'm_fltSpecialAmountMonthly', CStrings::strToFloatDef( $fltSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getSpecialAmountMonthly() {
		return $this->m_fltSpecialAmountMonthly;
	}

	public function sqlSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltSpecialAmountMonthly ) ) ? ( string ) $this->m_fltSpecialAmountMonthly : 'NULL';
	}

	public function setSpecialTaxMonthly( $fltSpecialTaxMonthly ) {
		$this->set( 'm_fltSpecialTaxMonthly', CStrings::strToFloatDef( $fltSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getSpecialTaxMonthly() {
		return $this->m_fltSpecialTaxMonthly;
	}

	public function sqlSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltSpecialTaxMonthly ) ) ? ( string ) $this->m_fltSpecialTaxMonthly : 'NULL';
	}

	public function setAddOnAmountMonthly( $fltAddOnAmountMonthly ) {
		$this->set( 'm_fltAddOnAmountMonthly', CStrings::strToFloatDef( $fltAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getAddOnAmountMonthly() {
		return $this->m_fltAddOnAmountMonthly;
	}

	public function sqlAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltAddOnAmountMonthly ) ) ? ( string ) $this->m_fltAddOnAmountMonthly : 'NULL';
	}

	public function setAddOnTaxMonthly( $fltAddOnTaxMonthly ) {
		$this->set( 'm_fltAddOnTaxMonthly', CStrings::strToFloatDef( $fltAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getAddOnTaxMonthly() {
		return $this->m_fltAddOnTaxMonthly;
	}

	public function sqlAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltAddOnTaxMonthly ) ) ? ( string ) $this->m_fltAddOnTaxMonthly : 'NULL';
	}

	public function setMarketAmountMonthly( $fltMarketAmountMonthly ) {
		$this->set( 'm_fltMarketAmountMonthly', CStrings::strToFloatDef( $fltMarketAmountMonthly, NULL, false, 2 ) );
	}

	public function getMarketAmountMonthly() {
		return $this->m_fltMarketAmountMonthly;
	}

	public function sqlMarketAmountMonthly() {
		return ( true == isset( $this->m_fltMarketAmountMonthly ) ) ? ( string ) $this->m_fltMarketAmountMonthly : 'NULL';
	}

	public function setEntrataMarketAmountMonthly( $fltEntrataMarketAmountMonthly ) {
		$this->set( 'm_fltEntrataMarketAmountMonthly', CStrings::strToFloatDef( $fltEntrataMarketAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMarketAmountMonthly() {
		return $this->m_fltEntrataMarketAmountMonthly;
	}

	public function sqlEntrataMarketAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMarketAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMarketAmountMonthly : 'NULL';
	}

	public function setEffectiveRange( $strEffectiveRange ) {
		$this->set( 'm_strEffectiveRange', CStrings::strTrimDef( $strEffectiveRange, NULL, NULL, true ) );
	}

	public function getEffectiveRange() {
		return $this->m_strEffectiveRange;
	}

	public function sqlEffectiveRange() {
		return ( true == isset( $this->m_strEffectiveRange ) ) ? '\'' . addslashes( $this->m_strEffectiveRange ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, unit_kind_id, property_building_id, property_unit_id, unit_space_id, lease_term_id, lease_start_window_id, space_configuration_id, lease_term_months, base_rent, amenity_rent, special_rent, add_on_rent, market_rent, base_deposit, amenity_deposit, special_deposit, add_on_deposit, total_deposit, cached_datetime, effective_date, created_by, created_on, show_on_website, occupancy_type_id, ar_trigger_id, base_rent_monthly, amenity_rent_monthly, special_rent_monthly, add_on_rent_monthly, market_rent_monthly, include_tax_in_advertised_rent, include_other_in_advertised_rent, base_amount, base_tax, amenity_amount, amenity_tax, special_amount, special_tax, add_on_amount, add_on_tax, base_amount_monthly, base_tax_monthly, amenity_amount_monthly, amenity_tax_monthly, special_amount_monthly, special_tax_monthly, add_on_amount_monthly, add_on_tax_monthly, market_amount_monthly, entrata_market_amount_monthly, effective_range )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlUnitKindId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlSpaceConfigurationId() . ', ' .
						$this->sqlLeaseTermMonths() . ', ' .
						$this->sqlBaseRent() . ', ' .
						$this->sqlAmenityRent() . ', ' .
						$this->sqlSpecialRent() . ', ' .
						$this->sqlAddOnRent() . ', ' .
						$this->sqlMarketRent() . ', ' .
						$this->sqlBaseDeposit() . ', ' .
						$this->sqlAmenityDeposit() . ', ' .
						$this->sqlSpecialDeposit() . ', ' .
						$this->sqlAddOnDeposit() . ', ' .
						$this->sqlTotalDeposit() . ', ' .
						$this->sqlCachedDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlBaseRentMonthly() . ', ' .
						$this->sqlAmenityRentMonthly() . ', ' .
						$this->sqlSpecialRentMonthly() . ', ' .
						$this->sqlAddOnRentMonthly() . ', ' .
						$this->sqlMarketRentMonthly() . ', ' .
						$this->sqlIncludeTaxInAdvertisedRent() . ', ' .
						$this->sqlIncludeOtherInAdvertisedRent() . ', ' .
						$this->sqlBaseAmount() . ', ' .
						$this->sqlBaseTax() . ', ' .
						$this->sqlAmenityAmount() . ', ' .
						$this->sqlAmenityTax() . ', ' .
						$this->sqlSpecialAmount() . ', ' .
						$this->sqlSpecialTax() . ', ' .
						$this->sqlAddOnAmount() . ', ' .
						$this->sqlAddOnTax() . ', ' .
						$this->sqlBaseAmountMonthly() . ', ' .
						$this->sqlBaseTaxMonthly() . ', ' .
						$this->sqlAmenityAmountMonthly() . ', ' .
						$this->sqlAmenityTaxMonthly() . ', ' .
						$this->sqlSpecialAmountMonthly() . ', ' .
						$this->sqlSpecialTaxMonthly() . ', ' .
						$this->sqlAddOnAmountMonthly() . ', ' .
						$this->sqlAddOnTaxMonthly() . ', ' .
						$this->sqlMarketAmountMonthly() . ', ' .
						$this->sqlEntrataMarketAmountMonthly() . ', ' .
						$this->sqlEffectiveRange() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths(). ',' ; } elseif( true == array_key_exists( 'LeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent(). ',' ; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent(). ',' ; } elseif( true == array_key_exists( 'AddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent(). ',' ; } elseif( true == array_key_exists( 'MarketRent', $this->getChangedColumns() ) ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'BaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'AmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'SpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'AddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'TotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_datetime = ' . $this->sqlCachedDatetime(). ',' ; } elseif( true == array_key_exists( 'CachedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' cached_datetime = ' . $this->sqlCachedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent_monthly = ' . $this->sqlBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_rent_monthly = ' . $this->sqlBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent_monthly = ' . $this->sqlAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent_monthly = ' . $this->sqlAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent_monthly = ' . $this->sqlSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_rent_monthly = ' . $this->sqlSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_rent_monthly = ' . $this->sqlAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_rent_monthly = ' . $this->sqlAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_monthly = ' . $this->sqlMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_monthly = ' . $this->sqlMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeTaxInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeOtherInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_amount = ' . $this->sqlBaseAmount(). ',' ; } elseif( true == array_key_exists( 'BaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' base_amount = ' . $this->sqlBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax(). ',' ; } elseif( true == array_key_exists( 'BaseTax', $this->getChangedColumns() ) ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'AmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_tax = ' . $this->sqlAmenityTax(). ',' ; } elseif( true == array_key_exists( 'AmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' amenity_tax = ' . $this->sqlAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_amount = ' . $this->sqlSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'SpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' special_amount = ' . $this->sqlSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_tax = ' . $this->sqlSpecialTax(). ',' ; } elseif( true == array_key_exists( 'SpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' special_tax = ' . $this->sqlSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_amount = ' . $this->sqlAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'AddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' add_on_amount = ' . $this->sqlAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_tax = ' . $this->sqlAddOnTax(). ',' ; } elseif( true == array_key_exists( 'AddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' add_on_tax = ' . $this->sqlAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_amount_monthly = ' . $this->sqlBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_amount_monthly = ' . $this->sqlBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax_monthly = ' . $this->sqlBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_tax_monthly = ' . $this->sqlBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_amount_monthly = ' . $this->sqlAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_amount_monthly = ' . $this->sqlAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_tax_monthly = ' . $this->sqlAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_tax_monthly = ' . $this->sqlAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_amount_monthly = ' . $this->sqlSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_amount_monthly = ' . $this->sqlSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_tax_monthly = ' . $this->sqlSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_tax_monthly = ' . $this->sqlSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_amount_monthly = ' . $this->sqlAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_amount_monthly = ' . $this->sqlAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_tax_monthly = ' . $this->sqlAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_tax_monthly = ' . $this->sqlAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_amount_monthly = ' . $this->sqlMarketAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MarketAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' market_amount_monthly = ' . $this->sqlMarketAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_market_amount_monthly = ' . $this->sqlEntrataMarketAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMarketAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_market_amount_monthly = ' . $this->sqlEntrataMarketAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange() ; } elseif( true == array_key_exists( 'EffectiveRange', $this->getChangedColumns() ) ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'lease_term_months' => $this->getLeaseTermMonths(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'add_on_rent' => $this->getAddOnRent(),
			'market_rent' => $this->getMarketRent(),
			'base_deposit' => $this->getBaseDeposit(),
			'amenity_deposit' => $this->getAmenityDeposit(),
			'special_deposit' => $this->getSpecialDeposit(),
			'add_on_deposit' => $this->getAddOnDeposit(),
			'total_deposit' => $this->getTotalDeposit(),
			'cached_datetime' => $this->getCachedDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'show_on_website' => $this->getShowOnWebsite(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'base_rent_monthly' => $this->getBaseRentMonthly(),
			'amenity_rent_monthly' => $this->getAmenityRentMonthly(),
			'special_rent_monthly' => $this->getSpecialRentMonthly(),
			'add_on_rent_monthly' => $this->getAddOnRentMonthly(),
			'market_rent_monthly' => $this->getMarketRentMonthly(),
			'include_tax_in_advertised_rent' => $this->getIncludeTaxInAdvertisedRent(),
			'include_other_in_advertised_rent' => $this->getIncludeOtherInAdvertisedRent(),
			'base_amount' => $this->getBaseAmount(),
			'base_tax' => $this->getBaseTax(),
			'amenity_amount' => $this->getAmenityAmount(),
			'amenity_tax' => $this->getAmenityTax(),
			'special_amount' => $this->getSpecialAmount(),
			'special_tax' => $this->getSpecialTax(),
			'add_on_amount' => $this->getAddOnAmount(),
			'add_on_tax' => $this->getAddOnTax(),
			'base_amount_monthly' => $this->getBaseAmountMonthly(),
			'base_tax_monthly' => $this->getBaseTaxMonthly(),
			'amenity_amount_monthly' => $this->getAmenityAmountMonthly(),
			'amenity_tax_monthly' => $this->getAmenityTaxMonthly(),
			'special_amount_monthly' => $this->getSpecialAmountMonthly(),
			'special_tax_monthly' => $this->getSpecialTaxMonthly(),
			'add_on_amount_monthly' => $this->getAddOnAmountMonthly(),
			'add_on_tax_monthly' => $this->getAddOnTaxMonthly(),
			'market_amount_monthly' => $this->getMarketAmountMonthly(),
			'entrata_market_amount_monthly' => $this->getEntrataMarketAmountMonthly(),
			'effective_range' => $this->getEffectiveRange()
		);
	}

}
?>