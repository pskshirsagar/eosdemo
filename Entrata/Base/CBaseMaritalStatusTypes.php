<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaritalStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseMaritalStatusTypes extends CEosPluralBase {

	/**
	 * @return CMaritalStatusType[]
	 */
	public static function fetchMaritalStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaritalStatusType::class, $objDatabase );
	}

	/**
	 * @return CMaritalStatusType
	 */
	public static function fetchMaritalStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaritalStatusType::class, $objDatabase );
	}

	public static function fetchMaritalStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'marital_status_types', $objDatabase );
	}

	public static function fetchMaritalStatusTypeById( $intId, $objDatabase ) {
		return self::fetchMaritalStatusType( sprintf( 'SELECT * FROM marital_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>