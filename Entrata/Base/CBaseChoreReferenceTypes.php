<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseChoreReferenceTypes extends CEosPluralBase {

	/**
	 * @return CChoreReferenceType[]
	 */
	public static function fetchChoreReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChoreReferenceType::class, $objDatabase );
	}

	/**
	 * @return CChoreReferenceType
	 */
	public static function fetchChoreReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChoreReferenceType::class, $objDatabase );
	}

	public static function fetchChoreReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_reference_types', $objDatabase );
	}

	public static function fetchChoreReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchChoreReferenceType( sprintf( 'SELECT * FROM chore_reference_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>