<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPetLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPetLogs extends CEosPluralBase {

	/**
	 * @return CCustomerPetLog[]
	 */
	public static function fetchCustomerPetLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerPetLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerPetLog
	 */
	public static function fetchCustomerPetLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerPetLog', $objDatabase );
	}

	public static function fetchCustomerPetLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_pet_logs', $objDatabase );
	}

	public static function fetchCustomerPetLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLog( sprintf( 'SELECT * FROM customer_pet_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByCustomerPetIdByCid( $intCustomerPetId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE customer_pet_id = %d AND cid = %d', ( int ) $intCustomerPetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByPriorCustomerPetLogIdByCid( $intPriorCustomerPetLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE prior_customer_pet_log_id = %d AND cid = %d', ( int ) $intPriorCustomerPetLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByPetTypeIdByCid( $intPetTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE pet_type_id = %d AND cid = %d', ( int ) $intPetTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByApplicantApplicationPetIdByCid( $intApplicantApplicationPetId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE applicant_application_pet_id = %d AND cid = %d', ( int ) $intApplicantApplicationPetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerPetLogsByCustomerAnimalIdByCid( $intCustomerAnimalId, $intCid, $objDatabase ) {
		return self::fetchCustomerPetLogs( sprintf( 'SELECT * FROM customer_pet_logs WHERE customer_animal_id = %d AND cid = %d', ( int ) $intCustomerAnimalId, ( int ) $intCid ), $objDatabase );
	}

}
?>