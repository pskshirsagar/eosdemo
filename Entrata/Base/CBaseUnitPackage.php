<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitPackage extends CEosSingularBase {

	const TABLE_NAME = 'public.unit_packages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intPackageTypeId;
	protected $m_intMessageId;
	protected $m_intAnnouncementId;
	protected $m_intSystemEmailId;
	protected $m_intCustomerId;
	protected $m_strPackageDatetime;
	protected $m_strBarcode;
	protected $m_strCalledOn;
	protected $m_intSentEmailCount;
	protected $m_intIsLobbyDisplayVisible;
	protected $m_intReceivedBy;
	protected $m_strReceivedOn;
	protected $m_intDeliveredBy;
	protected $m_strDeliveredOn;
	protected $m_intRetrievedBy;
	protected $m_strRetrievedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDescription;
	protected $m_intFileId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsLobbyDisplayVisible = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['package_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageTypeId', trim( $arrValues['package_type_id'] ) ); elseif( isset( $arrValues['package_type_id'] ) ) $this->setPackageTypeId( $arrValues['package_type_id'] );
		if( isset( $arrValues['message_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageId', trim( $arrValues['message_id'] ) ); elseif( isset( $arrValues['message_id'] ) ) $this->setMessageId( $arrValues['message_id'] );
		if( isset( $arrValues['announcement_id'] ) && $boolDirectSet ) $this->set( 'm_intAnnouncementId', trim( $arrValues['announcement_id'] ) ); elseif( isset( $arrValues['announcement_id'] ) ) $this->setAnnouncementId( $arrValues['announcement_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['package_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPackageDatetime', trim( $arrValues['package_datetime'] ) ); elseif( isset( $arrValues['package_datetime'] ) ) $this->setPackageDatetime( $arrValues['package_datetime'] );
		if( isset( $arrValues['barcode'] ) && $boolDirectSet ) $this->set( 'm_strBarcode', trim( stripcslashes( $arrValues['barcode'] ) ) ); elseif( isset( $arrValues['barcode'] ) ) $this->setBarcode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['barcode'] ) : $arrValues['barcode'] );
		if( isset( $arrValues['called_on'] ) && $boolDirectSet ) $this->set( 'm_strCalledOn', trim( $arrValues['called_on'] ) ); elseif( isset( $arrValues['called_on'] ) ) $this->setCalledOn( $arrValues['called_on'] );
		if( isset( $arrValues['sent_email_count'] ) && $boolDirectSet ) $this->set( 'm_intSentEmailCount', trim( $arrValues['sent_email_count'] ) ); elseif( isset( $arrValues['sent_email_count'] ) ) $this->setSentEmailCount( $arrValues['sent_email_count'] );
		if( isset( $arrValues['is_lobby_display_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsLobbyDisplayVisible', trim( $arrValues['is_lobby_display_visible'] ) ); elseif( isset( $arrValues['is_lobby_display_visible'] ) ) $this->setIsLobbyDisplayVisible( $arrValues['is_lobby_display_visible'] );
		if( isset( $arrValues['received_by'] ) && $boolDirectSet ) $this->set( 'm_intReceivedBy', trim( $arrValues['received_by'] ) ); elseif( isset( $arrValues['received_by'] ) ) $this->setReceivedBy( $arrValues['received_by'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['delivered_by'] ) && $boolDirectSet ) $this->set( 'm_intDeliveredBy', trim( $arrValues['delivered_by'] ) ); elseif( isset( $arrValues['delivered_by'] ) ) $this->setDeliveredBy( $arrValues['delivered_by'] );
		if( isset( $arrValues['delivered_on'] ) && $boolDirectSet ) $this->set( 'm_strDeliveredOn', trim( $arrValues['delivered_on'] ) ); elseif( isset( $arrValues['delivered_on'] ) ) $this->setDeliveredOn( $arrValues['delivered_on'] );
		if( isset( $arrValues['retrieved_by'] ) && $boolDirectSet ) $this->set( 'm_intRetrievedBy', trim( $arrValues['retrieved_by'] ) ); elseif( isset( $arrValues['retrieved_by'] ) ) $this->setRetrievedBy( $arrValues['retrieved_by'] );
		if( isset( $arrValues['retrieved_on'] ) && $boolDirectSet ) $this->set( 'm_strRetrievedOn', trim( $arrValues['retrieved_on'] ) ); elseif( isset( $arrValues['retrieved_on'] ) ) $this->setRetrievedOn( $arrValues['retrieved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPackageTypeId( $intPackageTypeId ) {
		$this->set( 'm_intPackageTypeId', CStrings::strToIntDef( $intPackageTypeId, NULL, false ) );
	}

	public function getPackageTypeId() {
		return $this->m_intPackageTypeId;
	}

	public function sqlPackageTypeId() {
		return ( true == isset( $this->m_intPackageTypeId ) ) ? ( string ) $this->m_intPackageTypeId : 'NULL';
	}

	public function setMessageId( $intMessageId ) {
		$this->set( 'm_intMessageId', CStrings::strToIntDef( $intMessageId, NULL, false ) );
	}

	public function getMessageId() {
		return $this->m_intMessageId;
	}

	public function sqlMessageId() {
		return ( true == isset( $this->m_intMessageId ) ) ? ( string ) $this->m_intMessageId : 'NULL';
	}

	public function setAnnouncementId( $intAnnouncementId ) {
		$this->set( 'm_intAnnouncementId', CStrings::strToIntDef( $intAnnouncementId, NULL, false ) );
	}

	public function getAnnouncementId() {
		return $this->m_intAnnouncementId;
	}

	public function sqlAnnouncementId() {
		return ( true == isset( $this->m_intAnnouncementId ) ) ? ( string ) $this->m_intAnnouncementId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPackageDatetime( $strPackageDatetime ) {
		$this->set( 'm_strPackageDatetime', CStrings::strTrimDef( $strPackageDatetime, -1, NULL, true ) );
	}

	public function getPackageDatetime() {
		return $this->m_strPackageDatetime;
	}

	public function sqlPackageDatetime() {
		return ( true == isset( $this->m_strPackageDatetime ) ) ? '\'' . $this->m_strPackageDatetime . '\'' : 'NOW()';
	}

	public function setBarcode( $strBarcode ) {
		$this->set( 'm_strBarcode', CStrings::strTrimDef( $strBarcode, 64, NULL, true ) );
	}

	public function getBarcode() {
		return $this->m_strBarcode;
	}

	public function sqlBarcode() {
		return ( true == isset( $this->m_strBarcode ) ) ? '\'' . addslashes( $this->m_strBarcode ) . '\'' : 'NULL';
	}

	public function setCalledOn( $strCalledOn ) {
		$this->set( 'm_strCalledOn', CStrings::strTrimDef( $strCalledOn, -1, NULL, true ) );
	}

	public function getCalledOn() {
		return $this->m_strCalledOn;
	}

	public function sqlCalledOn() {
		return ( true == isset( $this->m_strCalledOn ) ) ? '\'' . $this->m_strCalledOn . '\'' : 'NULL';
	}

	public function setSentEmailCount( $intSentEmailCount ) {
		$this->set( 'm_intSentEmailCount', CStrings::strToIntDef( $intSentEmailCount, NULL, false ) );
	}

	public function getSentEmailCount() {
		return $this->m_intSentEmailCount;
	}

	public function sqlSentEmailCount() {
		return ( true == isset( $this->m_intSentEmailCount ) ) ? ( string ) $this->m_intSentEmailCount : 'NULL';
	}

	public function setIsLobbyDisplayVisible( $intIsLobbyDisplayVisible ) {
		$this->set( 'm_intIsLobbyDisplayVisible', CStrings::strToIntDef( $intIsLobbyDisplayVisible, NULL, false ) );
	}

	public function getIsLobbyDisplayVisible() {
		return $this->m_intIsLobbyDisplayVisible;
	}

	public function sqlIsLobbyDisplayVisible() {
		return ( true == isset( $this->m_intIsLobbyDisplayVisible ) ) ? ( string ) $this->m_intIsLobbyDisplayVisible : '0';
	}

	public function setReceivedBy( $intReceivedBy ) {
		$this->set( 'm_intReceivedBy', CStrings::strToIntDef( $intReceivedBy, NULL, false ) );
	}

	public function getReceivedBy() {
		return $this->m_intReceivedBy;
	}

	public function sqlReceivedBy() {
		return ( true == isset( $this->m_intReceivedBy ) ) ? ( string ) $this->m_intReceivedBy : 'NULL';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setDeliveredBy( $intDeliveredBy ) {
		$this->set( 'm_intDeliveredBy', CStrings::strToIntDef( $intDeliveredBy, NULL, false ) );
	}

	public function getDeliveredBy() {
		return $this->m_intDeliveredBy;
	}

	public function sqlDeliveredBy() {
		return ( true == isset( $this->m_intDeliveredBy ) ) ? ( string ) $this->m_intDeliveredBy : 'NULL';
	}

	public function setDeliveredOn( $strDeliveredOn ) {
		$this->set( 'm_strDeliveredOn', CStrings::strTrimDef( $strDeliveredOn, -1, NULL, true ) );
	}

	public function getDeliveredOn() {
		return $this->m_strDeliveredOn;
	}

	public function sqlDeliveredOn() {
		return ( true == isset( $this->m_strDeliveredOn ) ) ? '\'' . $this->m_strDeliveredOn . '\'' : 'NULL';
	}

	public function setRetrievedBy( $intRetrievedBy ) {
		$this->set( 'm_intRetrievedBy', CStrings::strToIntDef( $intRetrievedBy, NULL, false ) );
	}

	public function getRetrievedBy() {
		return $this->m_intRetrievedBy;
	}

	public function sqlRetrievedBy() {
		return ( true == isset( $this->m_intRetrievedBy ) ) ? ( string ) $this->m_intRetrievedBy : 'NULL';
	}

	public function setRetrievedOn( $strRetrievedOn ) {
		$this->set( 'm_strRetrievedOn', CStrings::strTrimDef( $strRetrievedOn, -1, NULL, true ) );
	}

	public function getRetrievedOn() {
		return $this->m_strRetrievedOn;
	}

	public function sqlRetrievedOn() {
		return ( true == isset( $this->m_strRetrievedOn ) ) ? '\'' . $this->m_strRetrievedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 100, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_building_id, property_unit_id, unit_space_id, package_type_id, message_id, announcement_id, system_email_id, customer_id, package_datetime, barcode, called_on, sent_email_count, is_lobby_display_visible, received_by, received_on, delivered_by, delivered_on, retrieved_by, retrieved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, description, file_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlPackageTypeId() . ', ' .
 						$this->sqlMessageId() . ', ' .
 						$this->sqlAnnouncementId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlPackageDatetime() . ', ' .
 						$this->sqlBarcode() . ', ' .
 						$this->sqlCalledOn() . ', ' .
 						$this->sqlSentEmailCount() . ', ' .
 						$this->sqlIsLobbyDisplayVisible() . ', ' .
 						$this->sqlReceivedBy() . ', ' .
 						$this->sqlReceivedOn() . ', ' .
 						$this->sqlDeliveredBy() . ', ' .
 						$this->sqlDeliveredOn() . ', ' .
 						$this->sqlRetrievedBy() . ', ' .
 						$this->sqlRetrievedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlFileId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_type_id = ' . $this->sqlPackageTypeId() . ','; } elseif( true == array_key_exists( 'PackageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' package_type_id = ' . $this->sqlPackageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_id = ' . $this->sqlMessageId() . ','; } elseif( true == array_key_exists( 'MessageId', $this->getChangedColumns() ) ) { $strSql .= ' message_id = ' . $this->sqlMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' announcement_id = ' . $this->sqlAnnouncementId() . ','; } elseif( true == array_key_exists( 'AnnouncementId', $this->getChangedColumns() ) ) { $strSql .= ' announcement_id = ' . $this->sqlAnnouncementId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_datetime = ' . $this->sqlPackageDatetime() . ','; } elseif( true == array_key_exists( 'PackageDatetime', $this->getChangedColumns() ) ) { $strSql .= ' package_datetime = ' . $this->sqlPackageDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' barcode = ' . $this->sqlBarcode() . ','; } elseif( true == array_key_exists( 'Barcode', $this->getChangedColumns() ) ) { $strSql .= ' barcode = ' . $this->sqlBarcode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' called_on = ' . $this->sqlCalledOn() . ','; } elseif( true == array_key_exists( 'CalledOn', $this->getChangedColumns() ) ) { $strSql .= ' called_on = ' . $this->sqlCalledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_email_count = ' . $this->sqlSentEmailCount() . ','; } elseif( true == array_key_exists( 'SentEmailCount', $this->getChangedColumns() ) ) { $strSql .= ' sent_email_count = ' . $this->sqlSentEmailCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lobby_display_visible = ' . $this->sqlIsLobbyDisplayVisible() . ','; } elseif( true == array_key_exists( 'IsLobbyDisplayVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_lobby_display_visible = ' . $this->sqlIsLobbyDisplayVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy() . ','; } elseif( true == array_key_exists( 'ReceivedBy', $this->getChangedColumns() ) ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivered_by = ' . $this->sqlDeliveredBy() . ','; } elseif( true == array_key_exists( 'DeliveredBy', $this->getChangedColumns() ) ) { $strSql .= ' delivered_by = ' . $this->sqlDeliveredBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn() . ','; } elseif( true == array_key_exists( 'DeliveredOn', $this->getChangedColumns() ) ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retrieved_by = ' . $this->sqlRetrievedBy() . ','; } elseif( true == array_key_exists( 'RetrievedBy', $this->getChangedColumns() ) ) { $strSql .= ' retrieved_by = ' . $this->sqlRetrievedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retrieved_on = ' . $this->sqlRetrievedOn() . ','; } elseif( true == array_key_exists( 'RetrievedOn', $this->getChangedColumns() ) ) { $strSql .= ' retrieved_on = ' . $this->sqlRetrievedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'package_type_id' => $this->getPackageTypeId(),
			'message_id' => $this->getMessageId(),
			'announcement_id' => $this->getAnnouncementId(),
			'system_email_id' => $this->getSystemEmailId(),
			'customer_id' => $this->getCustomerId(),
			'package_datetime' => $this->getPackageDatetime(),
			'barcode' => $this->getBarcode(),
			'called_on' => $this->getCalledOn(),
			'sent_email_count' => $this->getSentEmailCount(),
			'is_lobby_display_visible' => $this->getIsLobbyDisplayVisible(),
			'received_by' => $this->getReceivedBy(),
			'received_on' => $this->getReceivedOn(),
			'delivered_by' => $this->getDeliveredBy(),
			'delivered_on' => $this->getDeliveredOn(),
			'retrieved_by' => $this->getRetrievedBy(),
			'retrieved_on' => $this->getRetrievedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'description' => $this->getDescription(),
			'file_id' => $this->getFileId()
		);
	}

}
?>