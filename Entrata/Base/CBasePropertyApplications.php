<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApplications extends CEosPluralBase {

	/**
	 * @return CPropertyApplication[]
	 */
	public static function fetchPropertyApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyApplication', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApplication
	 */
	public static function fetchPropertyApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyApplication', $objDatabase );
	}

	public static function fetchPropertyApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_applications', $objDatabase );
	}

	public static function fetchPropertyApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplication( sprintf( 'SELECT * FROM property_applications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApplications( sprintf( 'SELECT * FROM property_applications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplications( sprintf( 'SELECT * FROM property_applications WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplications( sprintf( 'SELECT * FROM property_applications WHERE company_application_id = %d AND cid = %d', ( int ) $intCompanyApplicationId, ( int ) $intCid ), $objDatabase );
	}

}
?>