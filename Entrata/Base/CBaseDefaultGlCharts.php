<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlCharts
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlCharts extends CEosPluralBase {

	/**
	 * @return CDefaultGlChart[]
	 */
	public static function fetchDefaultGlCharts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlChart::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlChart
	 */
	public static function fetchDefaultGlChart( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlChart::class, $objDatabase );
	}

	public static function fetchDefaultGlChartCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_charts', $objDatabase );
	}

	public static function fetchDefaultGlChartById( $intId, $objDatabase ) {
		return self::fetchDefaultGlChart( sprintf( 'SELECT * FROM default_gl_charts WHERE id = %d', $intId ), $objDatabase );
	}

}
?>