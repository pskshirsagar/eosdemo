<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPeriodId;
	protected $m_intPropertyId;
	protected $m_intGlHeaderId;
	protected $m_intGlTransactionTypeId;
	protected $m_intAccrualGlAccountId;
	protected $m_intCashGlAccountId;
	protected $m_intTemplateAmountTypeId;
	protected $m_intTemplateBalanceFromId;
	protected $m_intLeaseId;
	protected $m_intPropertyUnitId;
	protected $m_intGlReconciliationId;
	protected $m_intApDetailId;
	protected $m_intOffsettingGlDetailId;
	protected $m_intCompanyDepartmentId;
	protected $m_intGlDimensionId;
	protected $m_intApPayeeLocationId;
	protected $m_intApCodeId;
	protected $m_intArCodeId;
	protected $m_intReferenceId;
	protected $m_intJobPhaseId;
	protected $m_intApContractId;
	protected $m_intAssetId;
	protected $m_intPropertyBuildingId;
	protected $m_intReclassGlDetailId;
	protected $m_strPostMonth;
	protected $m_fltAmount;
	protected $m_strMemo;
	protected $m_boolIsConfidential;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMaintenanceLocationId;
	protected $m_intUnitTypeId;
	protected $m_intTemplateGlBookId;
	protected $m_boolTemplateIsAccrual;
	protected $m_intCompetitorId;
	protected $m_intPsProductId;
	protected $m_intApPayeeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsConfidential = false;
		$this->m_boolTemplateIsAccrual = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderId', trim( $arrValues['gl_header_id'] ) ); elseif( isset( $arrValues['gl_header_id'] ) ) $this->setGlHeaderId( $arrValues['gl_header_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['accrual_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualGlAccountId', trim( $arrValues['accrual_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_gl_account_id'] ) ) $this->setAccrualGlAccountId( $arrValues['accrual_gl_account_id'] );
		if( isset( $arrValues['cash_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashGlAccountId', trim( $arrValues['cash_gl_account_id'] ) ); elseif( isset( $arrValues['cash_gl_account_id'] ) ) $this->setCashGlAccountId( $arrValues['cash_gl_account_id'] );
		if( isset( $arrValues['template_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateAmountTypeId', trim( $arrValues['template_amount_type_id'] ) ); elseif( isset( $arrValues['template_amount_type_id'] ) ) $this->setTemplateAmountTypeId( $arrValues['template_amount_type_id'] );
		if( isset( $arrValues['template_balance_from_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateBalanceFromId', trim( $arrValues['template_balance_from_id'] ) ); elseif( isset( $arrValues['template_balance_from_id'] ) ) $this->setTemplateBalanceFromId( $arrValues['template_balance_from_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationId', trim( $arrValues['gl_reconciliation_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrValues['gl_reconciliation_id'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['offsetting_gl_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intOffsettingGlDetailId', trim( $arrValues['offsetting_gl_detail_id'] ) ); elseif( isset( $arrValues['offsetting_gl_detail_id'] ) ) $this->setOffsettingGlDetailId( $arrValues['offsetting_gl_detail_id'] );
		if( isset( $arrValues['company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyDepartmentId', trim( $arrValues['company_department_id'] ) ); elseif( isset( $arrValues['company_department_id'] ) ) $this->setCompanyDepartmentId( $arrValues['company_department_id'] );
		if( isset( $arrValues['gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDimensionId', trim( $arrValues['gl_dimension_id'] ) ); elseif( isset( $arrValues['gl_dimension_id'] ) ) $this->setGlDimensionId( $arrValues['gl_dimension_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPhaseId', trim( $arrValues['job_phase_id'] ) ); elseif( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
		if( isset( $arrValues['ap_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intApContractId', trim( $arrValues['ap_contract_id'] ) ); elseif( isset( $arrValues['ap_contract_id'] ) ) $this->setApContractId( $arrValues['ap_contract_id'] );
		if( isset( $arrValues['asset_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetId', trim( $arrValues['asset_id'] ) ); elseif( isset( $arrValues['asset_id'] ) ) $this->setAssetId( $arrValues['asset_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['reclass_gl_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReclassGlDetailId', trim( $arrValues['reclass_gl_detail_id'] ) ); elseif( isset( $arrValues['reclass_gl_detail_id'] ) ) $this->setReclassGlDetailId( $arrValues['reclass_gl_detail_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( $arrValues['memo'] ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( $arrValues['memo'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationId', trim( $arrValues['maintenance_location_id'] ) ); elseif( isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['template_gl_book_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateGlBookId', trim( $arrValues['template_gl_book_id'] ) ); elseif( isset( $arrValues['template_gl_book_id'] ) ) $this->setTemplateGlBookId( $arrValues['template_gl_book_id'] );
		if( isset( $arrValues['template_is_accrual'] ) && $boolDirectSet ) $this->set( 'm_boolTemplateIsAccrual', trim( stripcslashes( $arrValues['template_is_accrual'] ) ) ); elseif( isset( $arrValues['template_is_accrual'] ) ) $this->setTemplateIsAccrual( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['template_is_accrual'] ) : $arrValues['template_is_accrual'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setGlHeaderId( $intGlHeaderId ) {
		$this->set( 'm_intGlHeaderId', CStrings::strToIntDef( $intGlHeaderId, NULL, false ) );
	}

	public function getGlHeaderId() {
		return $this->m_intGlHeaderId;
	}

	public function sqlGlHeaderId() {
		return ( true == isset( $this->m_intGlHeaderId ) ) ? ( string ) $this->m_intGlHeaderId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setAccrualGlAccountId( $intAccrualGlAccountId ) {
		$this->set( 'm_intAccrualGlAccountId', CStrings::strToIntDef( $intAccrualGlAccountId, NULL, false ) );
	}

	public function getAccrualGlAccountId() {
		return $this->m_intAccrualGlAccountId;
	}

	public function sqlAccrualGlAccountId() {
		return ( true == isset( $this->m_intAccrualGlAccountId ) ) ? ( string ) $this->m_intAccrualGlAccountId : 'NULL';
	}

	public function setCashGlAccountId( $intCashGlAccountId ) {
		$this->set( 'm_intCashGlAccountId', CStrings::strToIntDef( $intCashGlAccountId, NULL, false ) );
	}

	public function getCashGlAccountId() {
		return $this->m_intCashGlAccountId;
	}

	public function sqlCashGlAccountId() {
		return ( true == isset( $this->m_intCashGlAccountId ) ) ? ( string ) $this->m_intCashGlAccountId : 'NULL';
	}

	public function setTemplateAmountTypeId( $intTemplateAmountTypeId ) {
		$this->set( 'm_intTemplateAmountTypeId', CStrings::strToIntDef( $intTemplateAmountTypeId, NULL, false ) );
	}

	public function getTemplateAmountTypeId() {
		return $this->m_intTemplateAmountTypeId;
	}

	public function sqlTemplateAmountTypeId() {
		return ( true == isset( $this->m_intTemplateAmountTypeId ) ) ? ( string ) $this->m_intTemplateAmountTypeId : 'NULL';
	}

	public function setTemplateBalanceFromId( $intTemplateBalanceFromId ) {
		$this->set( 'm_intTemplateBalanceFromId', CStrings::strToIntDef( $intTemplateBalanceFromId, NULL, false ) );
	}

	public function getTemplateBalanceFromId() {
		return $this->m_intTemplateBalanceFromId;
	}

	public function sqlTemplateBalanceFromId() {
		return ( true == isset( $this->m_intTemplateBalanceFromId ) ) ? ( string ) $this->m_intTemplateBalanceFromId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->set( 'm_intGlReconciliationId', CStrings::strToIntDef( $intGlReconciliationId, NULL, false ) );
	}

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function sqlGlReconciliationId() {
		return ( true == isset( $this->m_intGlReconciliationId ) ) ? ( string ) $this->m_intGlReconciliationId : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setOffsettingGlDetailId( $intOffsettingGlDetailId ) {
		$this->set( 'm_intOffsettingGlDetailId', CStrings::strToIntDef( $intOffsettingGlDetailId, NULL, false ) );
	}

	public function getOffsettingGlDetailId() {
		return $this->m_intOffsettingGlDetailId;
	}

	public function sqlOffsettingGlDetailId() {
		return ( true == isset( $this->m_intOffsettingGlDetailId ) ) ? ( string ) $this->m_intOffsettingGlDetailId : 'NULL';
	}

	public function setCompanyDepartmentId( $intCompanyDepartmentId ) {
		$this->set( 'm_intCompanyDepartmentId', CStrings::strToIntDef( $intCompanyDepartmentId, NULL, false ) );
	}

	public function getCompanyDepartmentId() {
		return $this->m_intCompanyDepartmentId;
	}

	public function sqlCompanyDepartmentId() {
		return ( true == isset( $this->m_intCompanyDepartmentId ) ) ? ( string ) $this->m_intCompanyDepartmentId : 'NULL';
	}

	public function setGlDimensionId( $intGlDimensionId ) {
		$this->set( 'm_intGlDimensionId', CStrings::strToIntDef( $intGlDimensionId, NULL, false ) );
	}

	public function getGlDimensionId() {
		return $this->m_intGlDimensionId;
	}

	public function sqlGlDimensionId() {
		return ( true == isset( $this->m_intGlDimensionId ) ) ? ( string ) $this->m_intGlDimensionId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setJobPhaseId( $intJobPhaseId ) {
		$this->set( 'm_intJobPhaseId', CStrings::strToIntDef( $intJobPhaseId, NULL, false ) );
	}

	public function getJobPhaseId() {
		return $this->m_intJobPhaseId;
	}

	public function sqlJobPhaseId() {
		return ( true == isset( $this->m_intJobPhaseId ) ) ? ( string ) $this->m_intJobPhaseId : 'NULL';
	}

	public function setApContractId( $intApContractId ) {
		$this->set( 'm_intApContractId', CStrings::strToIntDef( $intApContractId, NULL, false ) );
	}

	public function getApContractId() {
		return $this->m_intApContractId;
	}

	public function sqlApContractId() {
		return ( true == isset( $this->m_intApContractId ) ) ? ( string ) $this->m_intApContractId : 'NULL';
	}

	public function setAssetId( $intAssetId ) {
		$this->set( 'm_intAssetId', CStrings::strToIntDef( $intAssetId, NULL, false ) );
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function sqlAssetId() {
		return ( true == isset( $this->m_intAssetId ) ) ? ( string ) $this->m_intAssetId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setReclassGlDetailId( $intReclassGlDetailId ) {
		$this->set( 'm_intReclassGlDetailId', CStrings::strToIntDef( $intReclassGlDetailId, NULL, false ) );
	}

	public function getReclassGlDetailId() {
		return $this->m_intReclassGlDetailId;
	}

	public function sqlReclassGlDetailId() {
		return ( true == isset( $this->m_intReclassGlDetailId ) ) ? ( string ) $this->m_intReclassGlDetailId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMemo ) : '\'' . addslashes( $this->m_strMemo ) . '\'' ) : 'NULL';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->set( 'm_intMaintenanceLocationId', CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false ) );
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function sqlMaintenanceLocationId() {
		return ( true == isset( $this->m_intMaintenanceLocationId ) ) ? ( string ) $this->m_intMaintenanceLocationId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setTemplateGlBookId( $intTemplateGlBookId ) {
		$this->set( 'm_intTemplateGlBookId', CStrings::strToIntDef( $intTemplateGlBookId, NULL, false ) );
	}

	public function getTemplateGlBookId() {
		return $this->m_intTemplateGlBookId;
	}

	public function sqlTemplateGlBookId() {
		return ( true == isset( $this->m_intTemplateGlBookId ) ) ? ( string ) $this->m_intTemplateGlBookId : 'NULL';
	}

	public function setTemplateIsAccrual( $boolTemplateIsAccrual ) {
		$this->set( 'm_boolTemplateIsAccrual', CStrings::strToBool( $boolTemplateIsAccrual ) );
	}

	public function getTemplateIsAccrual() {
		return $this->m_boolTemplateIsAccrual;
	}

	public function sqlTemplateIsAccrual() {
		return ( true == isset( $this->m_boolTemplateIsAccrual ) ) ? '\'' . ( true == ( bool ) $this->m_boolTemplateIsAccrual ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, period_id, property_id, gl_header_id, gl_transaction_type_id, accrual_gl_account_id, cash_gl_account_id, template_amount_type_id, template_balance_from_id, lease_id, property_unit_id, gl_reconciliation_id, ap_detail_id, offsetting_gl_detail_id, company_department_id, gl_dimension_id, ap_payee_location_id, ap_code_id, ar_code_id, reference_id, job_phase_id, ap_contract_id, asset_id, property_building_id, reclass_gl_detail_id, post_month, amount, memo, is_confidential, updated_by, updated_on, created_by, created_on, maintenance_location_id, unit_type_id, template_gl_book_id, template_is_accrual, competitor_id, ps_product_id, ap_payee_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlGlHeaderId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlAccrualGlAccountId() . ', ' .
						$this->sqlCashGlAccountId() . ', ' .
						$this->sqlTemplateAmountTypeId() . ', ' .
						$this->sqlTemplateBalanceFromId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlGlReconciliationId() . ', ' .
						$this->sqlApDetailId() . ', ' .
						$this->sqlOffsettingGlDetailId() . ', ' .
						$this->sqlCompanyDepartmentId() . ', ' .
						$this->sqlGlDimensionId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlJobPhaseId() . ', ' .
						$this->sqlApContractId() . ', ' .
						$this->sqlAssetId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlReclassGlDetailId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlIsConfidential() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMaintenanceLocationId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlTemplateGlBookId() . ', ' .
						$this->sqlTemplateIsAccrual() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlApPayeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_gl_account_id = ' . $this->sqlAccrualGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_gl_account_id = ' . $this->sqlAccrualGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_gl_account_id = ' . $this->sqlCashGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_gl_account_id = ' . $this->sqlCashGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_amount_type_id = ' . $this->sqlTemplateAmountTypeId(). ',' ; } elseif( true == array_key_exists( 'TemplateAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' template_amount_type_id = ' . $this->sqlTemplateAmountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_balance_from_id = ' . $this->sqlTemplateBalanceFromId(). ',' ; } elseif( true == array_key_exists( 'TemplateBalanceFromId', $this->getChangedColumns() ) ) { $strSql .= ' template_balance_from_id = ' . $this->sqlTemplateBalanceFromId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId(). ',' ; } elseif( true == array_key_exists( 'GlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offsetting_gl_detail_id = ' . $this->sqlOffsettingGlDetailId(). ',' ; } elseif( true == array_key_exists( 'OffsettingGlDetailId', $this->getChangedColumns() ) ) { $strSql .= ' offsetting_gl_detail_id = ' . $this->sqlOffsettingGlDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId(). ',' ; } elseif( true == array_key_exists( 'CompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId(). ',' ; } elseif( true == array_key_exists( 'GlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'JobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId(). ',' ; } elseif( true == array_key_exists( 'ApContractId', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_id = ' . $this->sqlAssetId(). ',' ; } elseif( true == array_key_exists( 'AssetId', $this->getChangedColumns() ) ) { $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reclass_gl_detail_id = ' . $this->sqlReclassGlDetailId(). ',' ; } elseif( true == array_key_exists( 'ReclassGlDetailId', $this->getChangedColumns() ) ) { $strSql .= ' reclass_gl_detail_id = ' . $this->sqlReclassGlDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_gl_book_id = ' . $this->sqlTemplateGlBookId(). ',' ; } elseif( true == array_key_exists( 'TemplateGlBookId', $this->getChangedColumns() ) ) { $strSql .= ' template_gl_book_id = ' . $this->sqlTemplateGlBookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_is_accrual = ' . $this->sqlTemplateIsAccrual(). ',' ; } elseif( true == array_key_exists( 'TemplateIsAccrual', $this->getChangedColumns() ) ) { $strSql .= ' template_is_accrual = ' . $this->sqlTemplateIsAccrual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'period_id' => $this->getPeriodId(),
			'property_id' => $this->getPropertyId(),
			'gl_header_id' => $this->getGlHeaderId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'accrual_gl_account_id' => $this->getAccrualGlAccountId(),
			'cash_gl_account_id' => $this->getCashGlAccountId(),
			'template_amount_type_id' => $this->getTemplateAmountTypeId(),
			'template_balance_from_id' => $this->getTemplateBalanceFromId(),
			'lease_id' => $this->getLeaseId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'gl_reconciliation_id' => $this->getGlReconciliationId(),
			'ap_detail_id' => $this->getApDetailId(),
			'offsetting_gl_detail_id' => $this->getOffsettingGlDetailId(),
			'company_department_id' => $this->getCompanyDepartmentId(),
			'gl_dimension_id' => $this->getGlDimensionId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_code_id' => $this->getApCodeId(),
			'ar_code_id' => $this->getArCodeId(),
			'reference_id' => $this->getReferenceId(),
			'job_phase_id' => $this->getJobPhaseId(),
			'ap_contract_id' => $this->getApContractId(),
			'asset_id' => $this->getAssetId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'reclass_gl_detail_id' => $this->getReclassGlDetailId(),
			'post_month' => $this->getPostMonth(),
			'amount' => $this->getAmount(),
			'memo' => $this->getMemo(),
			'is_confidential' => $this->getIsConfidential(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'maintenance_location_id' => $this->getMaintenanceLocationId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'template_gl_book_id' => $this->getTemplateGlBookId(),
			'template_is_accrual' => $this->getTemplateIsAccrual(),
			'competitor_id' => $this->getCompetitorId(),
			'ps_product_id' => $this->getPsProductId(),
			'ap_payee_id' => $this->getApPayeeId()
		);
	}

}
?>