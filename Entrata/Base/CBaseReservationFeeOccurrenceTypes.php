<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReservationFeeOccurrenceTypes
 * Do not add any new functions to this class.
 */

class CBaseReservationFeeOccurrenceTypes extends CEosPluralBase {

	/**
	 * @return CReservationFeeOccurrenceType[]
	 */
	public static function fetchReservationFeeOccurrenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReservationFeeOccurrenceType::class, $objDatabase );
	}

	/**
	 * @return CReservationFeeOccurrenceType
	 */
	public static function fetchReservationFeeOccurrenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReservationFeeOccurrenceType::class, $objDatabase );
	}

	public static function fetchReservationFeeOccurrenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reservation_fee_occurrence_types', $objDatabase );
	}

	public static function fetchReservationFeeOccurrenceTypeById( $intId, $objDatabase ) {
		return self::fetchReservationFeeOccurrenceType( sprintf( 'SELECT * FROM reservation_fee_occurrence_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>