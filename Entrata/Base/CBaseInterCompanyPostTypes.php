<?php

class CBaseInterCompanyPostTypes extends CEosPluralBase {

    public static function fetchInterCompanyPostTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CInterCompanyPostType', $objDatabase );
    }

    public static function fetchInterCompanyPostType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CInterCompanyPostType', $objDatabase );
    }

    public static function fetchInterCompanyPostTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'inter_company_post_types', $objDatabase );
    }

    public static function fetchInterCompanyPostTypeById( $intId, $objDatabase ) {
        return self::fetchInterCompanyPostType( sprintf( 'SELECT * FROM inter_company_post_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>