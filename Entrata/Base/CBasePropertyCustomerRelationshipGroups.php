<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCustomerRelationshipGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCustomerRelationshipGroups extends CEosPluralBase {

	public static function fetchPropertyCustomerRelationshipGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCustomerRelationshipGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertyCustomerRelationshipGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCustomerRelationshipGroup', $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_customer_relationship_groups', $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomerRelationshipGroup( sprintf( 'SELECT * FROM property_customer_relationship_groups WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCustomerRelationshipGroups( sprintf( 'SELECT * FROM property_customer_relationship_groups WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomerRelationshipGroups( sprintf( 'SELECT * FROM property_customer_relationship_groups WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomerRelationshipGroups( sprintf( 'SELECT * FROM property_customer_relationship_groups WHERE occupancy_type_id = %d AND cid = %d', (int) $intOccupancyTypeId, (int) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCustomerRelationshipGroupsByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {
		return self::fetchPropertyCustomerRelationshipGroups( sprintf( 'SELECT * FROM property_customer_relationship_groups WHERE customer_relationship_group_id = %d AND cid = %d', (int) $intCustomerRelationshipGroupId, (int) $intCid ), $objDatabase );
	}

}
?>