<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountMonths
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetGlAccountMonths extends CEosPluralBase {

	/**
	 * @return CBudgetGlAccountMonth[]
	 */
	public static function fetchBudgetGlAccountMonths( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetGlAccountMonth::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetGlAccountMonth
	 */
	public static function fetchBudgetGlAccountMonth( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetGlAccountMonth::class, $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_gl_account_months', $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonth( sprintf( 'SELECT * FROM budget_gl_account_months WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonths( sprintf( 'SELECT * FROM budget_gl_account_months WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonths( sprintf( 'SELECT * FROM budget_gl_account_months WHERE budget_id = %d AND cid = %d', ( int ) $intBudgetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonths( sprintf( 'SELECT * FROM budget_gl_account_months WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonths( sprintf( 'SELECT * FROM budget_gl_account_months WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByGlDimensionIdByCid( $intGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchBudgetGlAccountMonths( sprintf( 'SELECT * FROM budget_gl_account_months WHERE gl_dimension_id = %d AND cid = %d', ( int ) $intGlDimensionId, ( int ) $intCid ), $objDatabase );
	}

}
?>