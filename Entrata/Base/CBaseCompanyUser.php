<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUser extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.company_users';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserTypeId;
	protected $m_intDefaultCompanyUserId;
	protected $m_intCompanyEmployeeId;
	protected $m_intMainEulaId;
	protected $m_intCheck21EulaId;
	protected $m_intCompanyUserScoreContactTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strLastLogin;
	protected $m_strMainEulaAcceptedOn;
	protected $m_strCheck21EulaAcceptedOn;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_intAllowWebServices;
	protected $m_intBlockAdminLogin;
	protected $m_intEmployeePortalOnly;
	protected $m_intIsAdministrator;
	protected $m_intIsDisabled;
	protected $m_intIsActiveDirectoryUser;
	protected $m_intIsGeneralTaskStakeholder;
	protected $m_strImportedOn;
	protected $m_strLastScoredOn;
	protected $m_strScoreInviteEmailedOn;
	protected $m_strScoreInvitePoppedOn;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intReferenceId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intCompanyUserTypeId = '2';
		$this->m_intAllowWebServices = '0';
		$this->m_intBlockAdminLogin = '0';
		$this->m_intEmployeePortalOnly = '0';
		$this->m_intIsAdministrator = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsActiveDirectoryUser = '0';
		$this->m_intIsGeneralTaskStakeholder = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserTypeId', trim( $arrValues['company_user_type_id'] ) ); elseif( isset( $arrValues['company_user_type_id'] ) ) $this->setCompanyUserTypeId( $arrValues['company_user_type_id'] );
		if( isset( $arrValues['default_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCompanyUserId', trim( $arrValues['default_company_user_id'] ) ); elseif( isset( $arrValues['default_company_user_id'] ) ) $this->setDefaultCompanyUserId( $arrValues['default_company_user_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['main_eula_id'] ) && $boolDirectSet ) $this->set( 'm_intMainEulaId', trim( $arrValues['main_eula_id'] ) ); elseif( isset( $arrValues['main_eula_id'] ) ) $this->setMainEulaId( $arrValues['main_eula_id'] );
		if( isset( $arrValues['check21_eula_id'] ) && $boolDirectSet ) $this->set( 'm_intCheck21EulaId', trim( $arrValues['check21_eula_id'] ) ); elseif( isset( $arrValues['check21_eula_id'] ) ) $this->setCheck21EulaId( $arrValues['check21_eula_id'] );
		if( isset( $arrValues['company_user_score_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserScoreContactTypeId', trim( $arrValues['company_user_score_contact_type_id'] ) ); elseif( isset( $arrValues['company_user_score_contact_type_id'] ) ) $this->setCompanyUserScoreContactTypeId( $arrValues['company_user_score_contact_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['main_eula_accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strMainEulaAcceptedOn', trim( $arrValues['main_eula_accepted_on'] ) ); elseif( isset( $arrValues['main_eula_accepted_on'] ) ) $this->setMainEulaAcceptedOn( $arrValues['main_eula_accepted_on'] );
		if( isset( $arrValues['check21_eula_accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strCheck21EulaAcceptedOn', trim( $arrValues['check21_eula_accepted_on'] ) ); elseif( isset( $arrValues['check21_eula_accepted_on'] ) ) $this->setCheck21EulaAcceptedOn( $arrValues['check21_eula_accepted_on'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['allow_web_services'] ) && $boolDirectSet ) $this->set( 'm_intAllowWebServices', trim( $arrValues['allow_web_services'] ) ); elseif( isset( $arrValues['allow_web_services'] ) ) $this->setAllowWebServices( $arrValues['allow_web_services'] );
		if( isset( $arrValues['block_admin_login'] ) && $boolDirectSet ) $this->set( 'm_intBlockAdminLogin', trim( $arrValues['block_admin_login'] ) ); elseif( isset( $arrValues['block_admin_login'] ) ) $this->setBlockAdminLogin( $arrValues['block_admin_login'] );
		if( isset( $arrValues['employee_portal_only'] ) && $boolDirectSet ) $this->set( 'm_intEmployeePortalOnly', trim( $arrValues['employee_portal_only'] ) ); elseif( isset( $arrValues['employee_portal_only'] ) ) $this->setEmployeePortalOnly( $arrValues['employee_portal_only'] );
		if( isset( $arrValues['is_administrator'] ) && $boolDirectSet ) $this->set( 'm_intIsAdministrator', trim( $arrValues['is_administrator'] ) ); elseif( isset( $arrValues['is_administrator'] ) ) $this->setIsAdministrator( $arrValues['is_administrator'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_active_directory_user'] ) && $boolDirectSet ) $this->set( 'm_intIsActiveDirectoryUser', trim( $arrValues['is_active_directory_user'] ) ); elseif( isset( $arrValues['is_active_directory_user'] ) ) $this->setIsActiveDirectoryUser( $arrValues['is_active_directory_user'] );
		if( isset( $arrValues['is_general_task_stakeholder'] ) && $boolDirectSet ) $this->set( 'm_intIsGeneralTaskStakeholder', trim( $arrValues['is_general_task_stakeholder'] ) ); elseif( isset( $arrValues['is_general_task_stakeholder'] ) ) $this->setIsGeneralTaskStakeholder( $arrValues['is_general_task_stakeholder'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['last_scored_on'] ) && $boolDirectSet ) $this->set( 'm_strLastScoredOn', trim( $arrValues['last_scored_on'] ) ); elseif( isset( $arrValues['last_scored_on'] ) ) $this->setLastScoredOn( $arrValues['last_scored_on'] );
		if( isset( $arrValues['score_invite_emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strScoreInviteEmailedOn', trim( $arrValues['score_invite_emailed_on'] ) ); elseif( isset( $arrValues['score_invite_emailed_on'] ) ) $this->setScoreInviteEmailedOn( $arrValues['score_invite_emailed_on'] );
		if( isset( $arrValues['score_invite_popped_on'] ) && $boolDirectSet ) $this->set( 'm_strScoreInvitePoppedOn', trim( $arrValues['score_invite_popped_on'] ) ); elseif( isset( $arrValues['score_invite_popped_on'] ) ) $this->setScoreInvitePoppedOn( $arrValues['score_invite_popped_on'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setCompanyUserTypeId( $intCompanyUserTypeId ) {
		$this->set( 'm_intCompanyUserTypeId', CStrings::strToIntDef( $intCompanyUserTypeId, NULL, false ) );
	}

	public function getCompanyUserTypeId() {
		return $this->m_intCompanyUserTypeId;
	}

	public function sqlCompanyUserTypeId() {
		return ( true == isset( $this->m_intCompanyUserTypeId ) ) ? ( string ) $this->m_intCompanyUserTypeId : '2';
	}

	public function setDefaultCompanyUserId( $intDefaultCompanyUserId ) {
		$this->set( 'm_intDefaultCompanyUserId', CStrings::strToIntDef( $intDefaultCompanyUserId, NULL, false ) );
	}

	public function getDefaultCompanyUserId() {
		return $this->m_intDefaultCompanyUserId;
	}

	public function sqlDefaultCompanyUserId() {
		return ( true == isset( $this->m_intDefaultCompanyUserId ) ) ? ( string ) $this->m_intDefaultCompanyUserId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setMainEulaId( $intMainEulaId ) {
		$this->set( 'm_intMainEulaId', CStrings::strToIntDef( $intMainEulaId, NULL, false ) );
	}

	public function getMainEulaId() {
		return $this->m_intMainEulaId;
	}

	public function sqlMainEulaId() {
		return ( true == isset( $this->m_intMainEulaId ) ) ? ( string ) $this->m_intMainEulaId : 'NULL';
	}

	public function setCheck21EulaId( $intCheck21EulaId ) {
		$this->set( 'm_intCheck21EulaId', CStrings::strToIntDef( $intCheck21EulaId, NULL, false ) );
	}

	public function getCheck21EulaId() {
		return $this->m_intCheck21EulaId;
	}

	public function sqlCheck21EulaId() {
		return ( true == isset( $this->m_intCheck21EulaId ) ) ? ( string ) $this->m_intCheck21EulaId : 'NULL';
	}

	public function setCompanyUserScoreContactTypeId( $intCompanyUserScoreContactTypeId ) {
		$this->set( 'm_intCompanyUserScoreContactTypeId', CStrings::strToIntDef( $intCompanyUserScoreContactTypeId, NULL, false ) );
	}

	public function getCompanyUserScoreContactTypeId() {
		return $this->m_intCompanyUserScoreContactTypeId;
	}

	public function sqlCompanyUserScoreContactTypeId() {
		return ( true == isset( $this->m_intCompanyUserScoreContactTypeId ) ) ? ( string ) $this->m_intCompanyUserScoreContactTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setMainEulaAcceptedOn( $strMainEulaAcceptedOn ) {
		$this->set( 'm_strMainEulaAcceptedOn', CStrings::strTrimDef( $strMainEulaAcceptedOn, -1, NULL, true ) );
	}

	public function getMainEulaAcceptedOn() {
		return $this->m_strMainEulaAcceptedOn;
	}

	public function sqlMainEulaAcceptedOn() {
		return ( true == isset( $this->m_strMainEulaAcceptedOn ) ) ? '\'' . $this->m_strMainEulaAcceptedOn . '\'' : 'NULL';
	}

	public function setCheck21EulaAcceptedOn( $strCheck21EulaAcceptedOn ) {
		$this->set( 'm_strCheck21EulaAcceptedOn', CStrings::strTrimDef( $strCheck21EulaAcceptedOn, -1, NULL, true ) );
	}

	public function getCheck21EulaAcceptedOn() {
		return $this->m_strCheck21EulaAcceptedOn;
	}

	public function sqlCheck21EulaAcceptedOn() {
		return ( true == isset( $this->m_strCheck21EulaAcceptedOn ) ) ? '\'' . $this->m_strCheck21EulaAcceptedOn . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setAllowWebServices( $intAllowWebServices ) {
		$this->set( 'm_intAllowWebServices', CStrings::strToIntDef( $intAllowWebServices, NULL, false ) );
	}

	public function getAllowWebServices() {
		return $this->m_intAllowWebServices;
	}

	public function sqlAllowWebServices() {
		return ( true == isset( $this->m_intAllowWebServices ) ) ? ( string ) $this->m_intAllowWebServices : '0';
	}

	public function setBlockAdminLogin( $intBlockAdminLogin ) {
		$this->set( 'm_intBlockAdminLogin', CStrings::strToIntDef( $intBlockAdminLogin, NULL, false ) );
	}

	public function getBlockAdminLogin() {
		return $this->m_intBlockAdminLogin;
	}

	public function sqlBlockAdminLogin() {
		return ( true == isset( $this->m_intBlockAdminLogin ) ) ? ( string ) $this->m_intBlockAdminLogin : '0';
	}

	public function setEmployeePortalOnly( $intEmployeePortalOnly ) {
		$this->set( 'm_intEmployeePortalOnly', CStrings::strToIntDef( $intEmployeePortalOnly, NULL, false ) );
	}

	public function getEmployeePortalOnly() {
		return $this->m_intEmployeePortalOnly;
	}

	public function sqlEmployeePortalOnly() {
		return ( true == isset( $this->m_intEmployeePortalOnly ) ) ? ( string ) $this->m_intEmployeePortalOnly : '0';
	}

	public function setIsAdministrator( $intIsAdministrator ) {
		$this->set( 'm_intIsAdministrator', CStrings::strToIntDef( $intIsAdministrator, NULL, false ) );
	}

	public function getIsAdministrator() {
		return $this->m_intIsAdministrator;
	}

	public function sqlIsAdministrator() {
		return ( true == isset( $this->m_intIsAdministrator ) ) ? ( string ) $this->m_intIsAdministrator : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsActiveDirectoryUser( $intIsActiveDirectoryUser ) {
		$this->set( 'm_intIsActiveDirectoryUser', CStrings::strToIntDef( $intIsActiveDirectoryUser, NULL, false ) );
	}

	public function getIsActiveDirectoryUser() {
		return $this->m_intIsActiveDirectoryUser;
	}

	public function sqlIsActiveDirectoryUser() {
		return ( true == isset( $this->m_intIsActiveDirectoryUser ) ) ? ( string ) $this->m_intIsActiveDirectoryUser : '0';
	}

	public function setIsGeneralTaskStakeholder( $intIsGeneralTaskStakeholder ) {
		$this->set( 'm_intIsGeneralTaskStakeholder', CStrings::strToIntDef( $intIsGeneralTaskStakeholder, NULL, false ) );
	}

	public function getIsGeneralTaskStakeholder() {
		return $this->m_intIsGeneralTaskStakeholder;
	}

	public function sqlIsGeneralTaskStakeholder() {
		return ( true == isset( $this->m_intIsGeneralTaskStakeholder ) ) ? ( string ) $this->m_intIsGeneralTaskStakeholder : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setLastScoredOn( $strLastScoredOn ) {
		$this->set( 'm_strLastScoredOn', CStrings::strTrimDef( $strLastScoredOn, -1, NULL, true ) );
	}

	public function getLastScoredOn() {
		return $this->m_strLastScoredOn;
	}

	public function sqlLastScoredOn() {
		return ( true == isset( $this->m_strLastScoredOn ) ) ? '\'' . $this->m_strLastScoredOn . '\'' : 'NULL';
	}

	public function setScoreInviteEmailedOn( $strScoreInviteEmailedOn ) {
		$this->set( 'm_strScoreInviteEmailedOn', CStrings::strTrimDef( $strScoreInviteEmailedOn, -1, NULL, true ) );
	}

	public function getScoreInviteEmailedOn() {
		return $this->m_strScoreInviteEmailedOn;
	}

	public function sqlScoreInviteEmailedOn() {
		return ( true == isset( $this->m_strScoreInviteEmailedOn ) ) ? '\'' . $this->m_strScoreInviteEmailedOn . '\'' : 'NULL';
	}

	public function setScoreInvitePoppedOn( $strScoreInvitePoppedOn ) {
		$this->set( 'm_strScoreInvitePoppedOn', CStrings::strTrimDef( $strScoreInvitePoppedOn, -1, NULL, true ) );
	}

	public function getScoreInvitePoppedOn() {
		return $this->m_strScoreInvitePoppedOn;
	}

	public function sqlScoreInvitePoppedOn() {
		return ( true == isset( $this->m_strScoreInvitePoppedOn ) ) ? '\'' . $this->m_strScoreInvitePoppedOn . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_type_id, default_company_user_id, company_employee_id, main_eula_id, check21_eula_id, company_user_score_contact_type_id, remote_primary_key, username, password_encrypted, last_login, main_eula_accepted_on, check21_eula_accepted_on, login_attempt_count, last_login_attempt_on, allow_web_services, block_admin_login, employee_portal_only, is_administrator, is_disabled, is_active_directory_user, is_general_task_stakeholder, imported_on, last_scored_on, score_invite_emailed_on, score_invite_popped_on, deleted_on, updated_by, updated_on, created_by, created_on, reference_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserTypeId() . ', ' .
						$this->sqlDefaultCompanyUserId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlMainEulaId() . ', ' .
						$this->sqlCheck21EulaId() . ', ' .
						$this->sqlCompanyUserScoreContactTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlLastLogin() . ', ' .
						$this->sqlMainEulaAcceptedOn() . ', ' .
						$this->sqlCheck21EulaAcceptedOn() . ', ' .
						$this->sqlLoginAttemptCount() . ', ' .
						$this->sqlLastLoginAttemptOn() . ', ' .
						$this->sqlAllowWebServices() . ', ' .
						$this->sqlBlockAdminLogin() . ', ' .
						$this->sqlEmployeePortalOnly() . ', ' .
						$this->sqlIsAdministrator() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsActiveDirectoryUser() . ', ' .
						$this->sqlIsGeneralTaskStakeholder() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlLastScoredOn() . ', ' .
						$this->sqlScoreInviteEmailedOn() . ', ' .
						$this->sqlScoreInvitePoppedOn() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_type_id = ' . $this->sqlCompanyUserTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_type_id = ' . $this->sqlCompanyUserTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_company_user_id = ' . $this->sqlDefaultCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'DefaultCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' default_company_user_id = ' . $this->sqlDefaultCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_eula_id = ' . $this->sqlMainEulaId(). ',' ; } elseif( true == array_key_exists( 'MainEulaId', $this->getChangedColumns() ) ) { $strSql .= ' main_eula_id = ' . $this->sqlMainEulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check21_eula_id = ' . $this->sqlCheck21EulaId(). ',' ; } elseif( true == array_key_exists( 'Check21EulaId', $this->getChangedColumns() ) ) { $strSql .= ' check21_eula_id = ' . $this->sqlCheck21EulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_score_contact_type_id = ' . $this->sqlCompanyUserScoreContactTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserScoreContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_score_contact_type_id = ' . $this->sqlCompanyUserScoreContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin(). ',' ; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' main_eula_accepted_on = ' . $this->sqlMainEulaAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'MainEulaAcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' main_eula_accepted_on = ' . $this->sqlMainEulaAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check21_eula_accepted_on = ' . $this->sqlCheck21EulaAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'Check21EulaAcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' check21_eula_accepted_on = ' . $this->sqlCheck21EulaAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount(). ',' ; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn(). ',' ; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_web_services = ' . $this->sqlAllowWebServices(). ',' ; } elseif( true == array_key_exists( 'AllowWebServices', $this->getChangedColumns() ) ) { $strSql .= ' allow_web_services = ' . $this->sqlAllowWebServices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_admin_login = ' . $this->sqlBlockAdminLogin(). ',' ; } elseif( true == array_key_exists( 'BlockAdminLogin', $this->getChangedColumns() ) ) { $strSql .= ' block_admin_login = ' . $this->sqlBlockAdminLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_portal_only = ' . $this->sqlEmployeePortalOnly(). ',' ; } elseif( true == array_key_exists( 'EmployeePortalOnly', $this->getChangedColumns() ) ) { $strSql .= ' employee_portal_only = ' . $this->sqlEmployeePortalOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator(). ',' ; } elseif( true == array_key_exists( 'IsAdministrator', $this->getChangedColumns() ) ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active_directory_user = ' . $this->sqlIsActiveDirectoryUser(). ',' ; } elseif( true == array_key_exists( 'IsActiveDirectoryUser', $this->getChangedColumns() ) ) { $strSql .= ' is_active_directory_user = ' . $this->sqlIsActiveDirectoryUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_general_task_stakeholder = ' . $this->sqlIsGeneralTaskStakeholder(). ',' ; } elseif( true == array_key_exists( 'IsGeneralTaskStakeholder', $this->getChangedColumns() ) ) { $strSql .= ' is_general_task_stakeholder = ' . $this->sqlIsGeneralTaskStakeholder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_scored_on = ' . $this->sqlLastScoredOn(). ',' ; } elseif( true == array_key_exists( 'LastScoredOn', $this->getChangedColumns() ) ) { $strSql .= ' last_scored_on = ' . $this->sqlLastScoredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score_invite_emailed_on = ' . $this->sqlScoreInviteEmailedOn(). ',' ; } elseif( true == array_key_exists( 'ScoreInviteEmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' score_invite_emailed_on = ' . $this->sqlScoreInviteEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score_invite_popped_on = ' . $this->sqlScoreInvitePoppedOn(). ',' ; } elseif( true == array_key_exists( 'ScoreInvitePoppedOn', $this->getChangedColumns() ) ) { $strSql .= ' score_invite_popped_on = ' . $this->sqlScoreInvitePoppedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_type_id' => $this->getCompanyUserTypeId(),
			'default_company_user_id' => $this->getDefaultCompanyUserId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'main_eula_id' => $this->getMainEulaId(),
			'check21_eula_id' => $this->getCheck21EulaId(),
			'company_user_score_contact_type_id' => $this->getCompanyUserScoreContactTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'last_login' => $this->getLastLogin(),
			'main_eula_accepted_on' => $this->getMainEulaAcceptedOn(),
			'check21_eula_accepted_on' => $this->getCheck21EulaAcceptedOn(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'allow_web_services' => $this->getAllowWebServices(),
			'block_admin_login' => $this->getBlockAdminLogin(),
			'employee_portal_only' => $this->getEmployeePortalOnly(),
			'is_administrator' => $this->getIsAdministrator(),
			'is_disabled' => $this->getIsDisabled(),
			'is_active_directory_user' => $this->getIsActiveDirectoryUser(),
			'is_general_task_stakeholder' => $this->getIsGeneralTaskStakeholder(),
			'imported_on' => $this->getImportedOn(),
			'last_scored_on' => $this->getLastScoredOn(),
			'score_invite_emailed_on' => $this->getScoreInviteEmailedOn(),
			'score_invite_popped_on' => $this->getScoreInvitePoppedOn(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'reference_id' => $this->getReferenceId(),
			'details' => $this->getDetails()
		);
	}

}
?>