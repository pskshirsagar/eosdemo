<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorkbookAssumptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorkbookAssumptions extends CEosPluralBase {

	/**
	 * @return CBudgetWorkbookAssumption[]
	 */
	public static function fetchBudgetWorkbookAssumptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {

		return parent::fetchObjects( $strSql, CBudgetWorkbookAssumption::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorkbookAssumption
	 */
	public static function fetchBudgetWorkbookAssumption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorkbookAssumption::class, $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_workbook_assumptions', $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookAssumption( sprintf( 'SELECT * FROM budget_workbook_assumptions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookAssumptions( sprintf( 'SELECT * FROM budget_workbook_assumptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookAssumptions( sprintf( 'SELECT * FROM budget_workbook_assumptions WHERE budget_workbook_id = %d AND cid = %d AND deleted_on IS NULL', $intBudgetWorkbookId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionsByBudgetAssumptionTypeIdByCid( $intBudgetAssumptionTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookAssumptions( sprintf( 'SELECT * FROM budget_workbook_assumptions WHERE budget_assumption_type_id = %d AND cid = %d', $intBudgetAssumptionTypeId, $intCid ), $objDatabase );
	}

}
?>