<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRentLimitVersions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyRentLimitVersions extends CEosPluralBase {

	/**
	 * @return CSubsidyRentLimitVersion[]
	 */
	public static function fetchSubsidyRentLimitVersions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyRentLimitVersion::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyRentLimitVersion
	 */
	public static function fetchSubsidyRentLimitVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyRentLimitVersion::class, $objDatabase );
	}

	public static function fetchSubsidyRentLimitVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_rent_limit_versions', $objDatabase );
	}

	public static function fetchSubsidyRentLimitVersionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimitVersion( sprintf( 'SELECT * FROM subsidy_rent_limit_versions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitVersionsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimitVersions( sprintf( 'SELECT * FROM subsidy_rent_limit_versions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitVersionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimitVersions( sprintf( 'SELECT * FROM subsidy_rent_limit_versions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>