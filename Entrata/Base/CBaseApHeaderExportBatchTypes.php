<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseApHeaderExportBatchTypes extends CEosPluralBase {

	/**
	 * @return CApHeaderExportBatchType[]
	 */
	public static function fetchApHeaderExportBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApHeaderExportBatchType::class, $objDatabase );
	}

	/**
	 * @return CApHeaderExportBatchType
	 */
	public static function fetchApHeaderExportBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApHeaderExportBatchType::class, $objDatabase );
	}

	public static function fetchApHeaderExportBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_header_export_batch_types', $objDatabase );
	}

	public static function fetchApHeaderExportBatchTypeById( $intId, $objDatabase ) {
		return self::fetchApHeaderExportBatchType( sprintf( 'SELECT * FROM ap_header_export_batch_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>