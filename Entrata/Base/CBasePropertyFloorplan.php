<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyFloorplan extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_floorplans';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitKindId;
	protected $m_strRemotePrimaryKey;
	protected $m_strVaultwareNumber;
	protected $m_strFloorplanName;
	protected $m_strDescription;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltMinDeposit;
	protected $m_fltMaxDeposit;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intNumberOfUnits;
	protected $m_intNumberOfAvailableUnits;
	protected $m_intNumberOfFloors;
	protected $m_intNumberOfRooms;
	protected $m_intNumberOfBedrooms;
	protected $m_strBedroomDescription;
	protected $m_fltNumberOfBathrooms;
	protected $m_strBathroomDescription;
	protected $m_strNotes;
	protected $m_boolBlockPricingIntegration;
	protected $m_boolIsDisabledVirtualMoveIn;
	protected $m_boolHasSpaceConfigurationTerms;
	protected $m_boolIsManualRentRange;
	protected $m_boolIsManualDepositRange;
	protected $m_boolIsPublished;
	protected $m_boolIsFeatured;
	protected $m_boolAllowsCats;
	protected $m_boolAllowsDogs;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strSeoTitle;
	protected $m_strSeoDescription;
	protected $m_strSeoKeywords;
	protected $m_boolIsCallForPricing;

	public function __construct() {
		parent::__construct();

		$this->m_boolBlockPricingIntegration = false;
		$this->m_boolIsDisabledVirtualMoveIn = false;
		$this->m_boolHasSpaceConfigurationTerms = false;
		$this->m_boolIsManualRentRange = false;
		$this->m_boolIsManualDepositRange = false;
		$this->m_boolIsPublished = true;
		$this->m_boolIsFeatured = false;
		$this->m_boolAllowsCats = false;
		$this->m_boolAllowsDogs = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsCallForPricing = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['vaultware_number'] ) && $boolDirectSet ) $this->set( 'm_strVaultwareNumber', trim( $arrValues['vaultware_number'] ) ); elseif( isset( $arrValues['vaultware_number'] ) ) $this->setVaultwareNumber( $arrValues['vaultware_number'] );
		if( isset( $arrValues['floorplan_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFloorplanName', trim( $arrValues['floorplan_name'] ) ); elseif( isset( $arrValues['floorplan_name'] ) ) $this->setFloorplanName( $arrValues['floorplan_name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['min_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinRent', trim( $arrValues['min_rent'] ) ); elseif( isset( $arrValues['min_rent'] ) ) $this->setMinRent( $arrValues['min_rent'] );
		if( isset( $arrValues['max_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxRent', trim( $arrValues['max_rent'] ) ); elseif( isset( $arrValues['max_rent'] ) ) $this->setMaxRent( $arrValues['max_rent'] );
		if( isset( $arrValues['min_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinDeposit', trim( $arrValues['min_deposit'] ) ); elseif( isset( $arrValues['min_deposit'] ) ) $this->setMinDeposit( $arrValues['min_deposit'] );
		if( isset( $arrValues['max_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxDeposit', trim( $arrValues['max_deposit'] ) ); elseif( isset( $arrValues['max_deposit'] ) ) $this->setMaxDeposit( $arrValues['max_deposit'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['number_of_available_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfAvailableUnits', trim( $arrValues['number_of_available_units'] ) ); elseif( isset( $arrValues['number_of_available_units'] ) ) $this->setNumberOfAvailableUnits( $arrValues['number_of_available_units'] );
		if( isset( $arrValues['number_of_floors'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfFloors', trim( $arrValues['number_of_floors'] ) ); elseif( isset( $arrValues['number_of_floors'] ) ) $this->setNumberOfFloors( $arrValues['number_of_floors'] );
		if( isset( $arrValues['number_of_rooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfRooms', trim( $arrValues['number_of_rooms'] ) ); elseif( isset( $arrValues['number_of_rooms'] ) ) $this->setNumberOfRooms( $arrValues['number_of_rooms'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['bedroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBedroomDescription', trim( $arrValues['bedroom_description'] ) ); elseif( isset( $arrValues['bedroom_description'] ) ) $this->setBedroomDescription( $arrValues['bedroom_description'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['bathroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBathroomDescription', trim( $arrValues['bathroom_description'] ) ); elseif( isset( $arrValues['bathroom_description'] ) ) $this->setBathroomDescription( $arrValues['bathroom_description'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['block_pricing_integration'] ) && $boolDirectSet ) $this->set( 'm_boolBlockPricingIntegration', trim( stripcslashes( $arrValues['block_pricing_integration'] ) ) ); elseif( isset( $arrValues['block_pricing_integration'] ) ) $this->setBlockPricingIntegration( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['block_pricing_integration'] ) : $arrValues['block_pricing_integration'] );
		if( isset( $arrValues['is_disabled_virtual_move_in'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabledVirtualMoveIn', trim( stripcslashes( $arrValues['is_disabled_virtual_move_in'] ) ) ); elseif( isset( $arrValues['is_disabled_virtual_move_in'] ) ) $this->setIsDisabledVirtualMoveIn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled_virtual_move_in'] ) : $arrValues['is_disabled_virtual_move_in'] );
		if( isset( $arrValues['has_space_configuration_terms'] ) && $boolDirectSet ) $this->set( 'm_boolHasSpaceConfigurationTerms', trim( stripcslashes( $arrValues['has_space_configuration_terms'] ) ) ); elseif( isset( $arrValues['has_space_configuration_terms'] ) ) $this->setHasSpaceConfigurationTerms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_space_configuration_terms'] ) : $arrValues['has_space_configuration_terms'] );
		if( isset( $arrValues['is_manual_rent_range'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualRentRange', trim( stripcslashes( $arrValues['is_manual_rent_range'] ) ) ); elseif( isset( $arrValues['is_manual_rent_range'] ) ) $this->setIsManualRentRange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_rent_range'] ) : $arrValues['is_manual_rent_range'] );
		if( isset( $arrValues['is_manual_deposit_range'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualDepositRange', trim( stripcslashes( $arrValues['is_manual_deposit_range'] ) ) ); elseif( isset( $arrValues['is_manual_deposit_range'] ) ) $this->setIsManualDepositRange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_deposit_range'] ) : $arrValues['is_manual_deposit_range'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['allows_cats'] ) && $boolDirectSet ) $this->set( 'm_boolAllowsCats', trim( stripcslashes( $arrValues['allows_cats'] ) ) ); elseif( isset( $arrValues['allows_cats'] ) ) $this->setAllowsCats( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allows_cats'] ) : $arrValues['allows_cats'] );
		if( isset( $arrValues['allows_dogs'] ) && $boolDirectSet ) $this->set( 'm_boolAllowsDogs', trim( stripcslashes( $arrValues['allows_dogs'] ) ) ); elseif( isset( $arrValues['allows_dogs'] ) ) $this->setAllowsDogs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allows_dogs'] ) : $arrValues['allows_dogs'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['seo_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSeoTitle', trim( $arrValues['seo_title'] ) ); elseif( isset( $arrValues['seo_title'] ) ) $this->setSeoTitle( $arrValues['seo_title'] );
		if( isset( $arrValues['seo_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSeoDescription', trim( $arrValues['seo_description'] ) ); elseif( isset( $arrValues['seo_description'] ) ) $this->setSeoDescription( $arrValues['seo_description'] );
		if( isset( $arrValues['seo_keywords'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSeoKeywords', trim( $arrValues['seo_keywords'] ) ); elseif( isset( $arrValues['seo_keywords'] ) ) $this->setSeoKeywords( $arrValues['seo_keywords'] );
		if( isset( $arrValues['is_call_for_pricing'] ) && $boolDirectSet ) $this->set( 'm_boolIsCallForPricing', trim( stripcslashes( $arrValues['is_call_for_pricing'] ) ) ); elseif( isset( $arrValues['is_call_for_pricing'] ) ) $this->setIsCallForPricing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_call_for_pricing'] ) : $arrValues['is_call_for_pricing'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setVaultwareNumber( $strVaultwareNumber ) {
		$this->set( 'm_strVaultwareNumber', CStrings::strTrimDef( $strVaultwareNumber, 64, NULL, true ) );
	}

	public function getVaultwareNumber() {
		return $this->m_strVaultwareNumber;
	}

	public function sqlVaultwareNumber() {
		return ( true == isset( $this->m_strVaultwareNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVaultwareNumber ) : '\'' . addslashes( $this->m_strVaultwareNumber ) . '\'' ) : 'NULL';
	}

	public function setFloorplanName( $strFloorplanName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFloorplanName', CStrings::strTrimDef( $strFloorplanName, 50, NULL, true ), $strLocaleCode );
	}

	public function getFloorplanName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFloorplanName', $strLocaleCode );
	}

	public function sqlFloorplanName() {
		return ( true == isset( $this->m_strFloorplanName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFloorplanName ) : '\'' . addslashes( $this->m_strFloorplanName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setMinRent( $fltMinRent ) {
		$this->set( 'm_fltMinRent', CStrings::strToFloatDef( $fltMinRent, NULL, false, 4 ) );
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function sqlMinRent() {
		return ( true == isset( $this->m_fltMinRent ) ) ? ( string ) $this->m_fltMinRent : 'NULL';
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->set( 'm_fltMaxRent', CStrings::strToFloatDef( $fltMaxRent, NULL, false, 4 ) );
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function sqlMaxRent() {
		return ( true == isset( $this->m_fltMaxRent ) ) ? ( string ) $this->m_fltMaxRent : 'NULL';
	}

	public function setMinDeposit( $fltMinDeposit ) {
		$this->set( 'm_fltMinDeposit', CStrings::strToFloatDef( $fltMinDeposit, NULL, false, 4 ) );
	}

	public function getMinDeposit() {
		return $this->m_fltMinDeposit;
	}

	public function sqlMinDeposit() {
		return ( true == isset( $this->m_fltMinDeposit ) ) ? ( string ) $this->m_fltMinDeposit : 'NULL';
	}

	public function setMaxDeposit( $fltMaxDeposit ) {
		$this->set( 'm_fltMaxDeposit', CStrings::strToFloatDef( $fltMaxDeposit, NULL, false, 4 ) );
	}

	public function getMaxDeposit() {
		return $this->m_fltMaxDeposit;
	}

	public function sqlMaxDeposit() {
		return ( true == isset( $this->m_fltMaxDeposit ) ) ? ( string ) $this->m_fltMaxDeposit : 'NULL';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setNumberOfAvailableUnits( $intNumberOfAvailableUnits ) {
		$this->set( 'm_intNumberOfAvailableUnits', CStrings::strToIntDef( $intNumberOfAvailableUnits, NULL, false ) );
	}

	public function getNumberOfAvailableUnits() {
		return $this->m_intNumberOfAvailableUnits;
	}

	public function sqlNumberOfAvailableUnits() {
		return ( true == isset( $this->m_intNumberOfAvailableUnits ) ) ? ( string ) $this->m_intNumberOfAvailableUnits : 'NULL';
	}

	public function setNumberOfFloors( $intNumberOfFloors ) {
		$this->set( 'm_intNumberOfFloors', CStrings::strToIntDef( $intNumberOfFloors, NULL, false ) );
	}

	public function getNumberOfFloors() {
		return $this->m_intNumberOfFloors;
	}

	public function sqlNumberOfFloors() {
		return ( true == isset( $this->m_intNumberOfFloors ) ) ? ( string ) $this->m_intNumberOfFloors : 'NULL';
	}

	public function setNumberOfRooms( $intNumberOfRooms ) {
		$this->set( 'm_intNumberOfRooms', CStrings::strToIntDef( $intNumberOfRooms, NULL, false ) );
	}

	public function getNumberOfRooms() {
		return $this->m_intNumberOfRooms;
	}

	public function sqlNumberOfRooms() {
		return ( true == isset( $this->m_intNumberOfRooms ) ) ? ( string ) $this->m_intNumberOfRooms : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setBedroomDescription( $strBedroomDescription ) {
		$this->set( 'm_strBedroomDescription', CStrings::strTrimDef( $strBedroomDescription, 240, NULL, true ) );
	}

	public function getBedroomDescription() {
		return $this->m_strBedroomDescription;
	}

	public function sqlBedroomDescription() {
		return ( true == isset( $this->m_strBedroomDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBedroomDescription ) : '\'' . addslashes( $this->m_strBedroomDescription ) . '\'' ) : 'NULL';
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->set( 'm_fltNumberOfBathrooms', CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_fltNumberOfBathrooms ) ) ? ( string ) $this->m_fltNumberOfBathrooms : 'NULL';
	}

	public function setBathroomDescription( $strBathroomDescription ) {
		$this->set( 'm_strBathroomDescription', CStrings::strTrimDef( $strBathroomDescription, 240, NULL, true ) );
	}

	public function getBathroomDescription() {
		return $this->m_strBathroomDescription;
	}

	public function sqlBathroomDescription() {
		return ( true == isset( $this->m_strBathroomDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBathroomDescription ) : '\'' . addslashes( $this->m_strBathroomDescription ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setBlockPricingIntegration( $boolBlockPricingIntegration ) {
		$this->set( 'm_boolBlockPricingIntegration', CStrings::strToBool( $boolBlockPricingIntegration ) );
	}

	public function getBlockPricingIntegration() {
		return $this->m_boolBlockPricingIntegration;
	}

	public function sqlBlockPricingIntegration() {
		return ( true == isset( $this->m_boolBlockPricingIntegration ) ) ? '\'' . ( true == ( bool ) $this->m_boolBlockPricingIntegration ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabledVirtualMoveIn( $boolIsDisabledVirtualMoveIn ) {
		$this->set( 'm_boolIsDisabledVirtualMoveIn', CStrings::strToBool( $boolIsDisabledVirtualMoveIn ) );
	}

	public function getIsDisabledVirtualMoveIn() {
		return $this->m_boolIsDisabledVirtualMoveIn;
	}

	public function sqlIsDisabledVirtualMoveIn() {
		return ( true == isset( $this->m_boolIsDisabledVirtualMoveIn ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabledVirtualMoveIn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasSpaceConfigurationTerms( $boolHasSpaceConfigurationTerms ) {
		$this->set( 'm_boolHasSpaceConfigurationTerms', CStrings::strToBool( $boolHasSpaceConfigurationTerms ) );
	}

	public function getHasSpaceConfigurationTerms() {
		return $this->m_boolHasSpaceConfigurationTerms;
	}

	public function sqlHasSpaceConfigurationTerms() {
		return ( true == isset( $this->m_boolHasSpaceConfigurationTerms ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasSpaceConfigurationTerms ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManualRentRange( $boolIsManualRentRange ) {
		$this->set( 'm_boolIsManualRentRange', CStrings::strToBool( $boolIsManualRentRange ) );
	}

	public function getIsManualRentRange() {
		return $this->m_boolIsManualRentRange;
	}

	public function sqlIsManualRentRange() {
		return ( true == isset( $this->m_boolIsManualRentRange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualRentRange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManualDepositRange( $boolIsManualDepositRange ) {
		$this->set( 'm_boolIsManualDepositRange', CStrings::strToBool( $boolIsManualDepositRange ) );
	}

	public function getIsManualDepositRange() {
		return $this->m_boolIsManualDepositRange;
	}

	public function sqlIsManualDepositRange() {
		return ( true == isset( $this->m_boolIsManualDepositRange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualDepositRange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowsCats( $boolAllowsCats ) {
		$this->set( 'm_boolAllowsCats', CStrings::strToBool( $boolAllowsCats ) );
	}

	public function getAllowsCats() {
		return $this->m_boolAllowsCats;
	}

	public function sqlAllowsCats() {
		return ( true == isset( $this->m_boolAllowsCats ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowsCats ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowsDogs( $boolAllowsDogs ) {
		$this->set( 'm_boolAllowsDogs', CStrings::strToBool( $boolAllowsDogs ) );
	}

	public function getAllowsDogs() {
		return $this->m_boolAllowsDogs;
	}

	public function sqlAllowsDogs() {
		return ( true == isset( $this->m_boolAllowsDogs ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowsDogs ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSeoTitle( $strSeoTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSeoTitle', CStrings::strTrimDef( $strSeoTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getSeoTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSeoTitle', $strLocaleCode );
	}

	public function sqlSeoTitle() {
		return ( true == isset( $this->m_strSeoTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSeoTitle ) : '\'' . addslashes( $this->m_strSeoTitle ) . '\'' ) : 'NULL';
	}

	public function setSeoDescription( $strSeoDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSeoDescription', CStrings::strTrimDef( $strSeoDescription, 500, NULL, true ), $strLocaleCode );
	}

	public function getSeoDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSeoDescription', $strLocaleCode );
	}

	public function sqlSeoDescription() {
		return ( true == isset( $this->m_strSeoDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSeoDescription ) : '\'' . addslashes( $this->m_strSeoDescription ) . '\'' ) : 'NULL';
	}

	public function setSeoKeywords( $strSeoKeywords, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSeoKeywords', CStrings::strTrimDef( $strSeoKeywords, 500, NULL, true ), $strLocaleCode );
	}

	public function getSeoKeywords( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSeoKeywords', $strLocaleCode );
	}

	public function sqlSeoKeywords() {
		return ( true == isset( $this->m_strSeoKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSeoKeywords ) : '\'' . addslashes( $this->m_strSeoKeywords ) . '\'' ) : 'NULL';
	}

	public function setIsCallForPricing( $boolIsCallForPricing ) {
		$this->set( 'm_boolIsCallForPricing', CStrings::strToBool( $boolIsCallForPricing ) );
	}

	public function getIsCallForPricing() {
		return $this->m_boolIsCallForPricing;
	}

	public function sqlIsCallForPricing() {
		return ( true == isset( $this->m_boolIsCallForPricing ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCallForPricing ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_kind_id, remote_primary_key, vaultware_number, floorplan_name, description, min_rent, max_rent, min_deposit, max_deposit, min_square_feet, max_square_feet, number_of_units, number_of_available_units, number_of_floors, number_of_rooms, number_of_bedrooms, bedroom_description, number_of_bathrooms, bathroom_description, notes, block_pricing_integration, is_disabled_virtual_move_in, has_space_configuration_terms, is_manual_rent_range, is_manual_deposit_range, is_published, is_featured, allows_cats, allows_dogs, order_num, imported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, seo_title, seo_description, seo_keywords, is_call_for_pricing )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlUnitKindId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlVaultwareNumber() . ', ' .
		          $this->sqlFloorplanName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlMinRent() . ', ' .
		          $this->sqlMaxRent() . ', ' .
		          $this->sqlMinDeposit() . ', ' .
		          $this->sqlMaxDeposit() . ', ' .
		          $this->sqlMinSquareFeet() . ', ' .
		          $this->sqlMaxSquareFeet() . ', ' .
		          $this->sqlNumberOfUnits() . ', ' .
		          $this->sqlNumberOfAvailableUnits() . ', ' .
		          $this->sqlNumberOfFloors() . ', ' .
		          $this->sqlNumberOfRooms() . ', ' .
		          $this->sqlNumberOfBedrooms() . ', ' .
		          $this->sqlBedroomDescription() . ', ' .
		          $this->sqlNumberOfBathrooms() . ', ' .
		          $this->sqlBathroomDescription() . ', ' .
		          $this->sqlNotes() . ', ' .
		          $this->sqlBlockPricingIntegration() . ', ' .
		          $this->sqlIsDisabledVirtualMoveIn() . ', ' .
		          $this->sqlHasSpaceConfigurationTerms() . ', ' .
		          $this->sqlIsManualRentRange() . ', ' .
		          $this->sqlIsManualDepositRange() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlIsFeatured() . ', ' .
		          $this->sqlAllowsCats() . ', ' .
		          $this->sqlAllowsDogs() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlImportedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlSeoTitle() . ', ' .
		          $this->sqlSeoDescription() . ', ' .
		          $this->sqlSeoKeywords() . ', ' .
		          $this->sqlIsCallForPricing() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vaultware_number = ' . $this->sqlVaultwareNumber(). ',' ; } elseif( true == array_key_exists( 'VaultwareNumber', $this->getChangedColumns() ) ) { $strSql .= ' vaultware_number = ' . $this->sqlVaultwareNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floorplan_name = ' . $this->sqlFloorplanName(). ',' ; } elseif( true == array_key_exists( 'FloorplanName', $this->getChangedColumns() ) ) { $strSql .= ' floorplan_name = ' . $this->sqlFloorplanName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_rent = ' . $this->sqlMinRent(). ',' ; } elseif( true == array_key_exists( 'MinRent', $this->getChangedColumns() ) ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent(). ',' ; } elseif( true == array_key_exists( 'MaxRent', $this->getChangedColumns() ) ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit(). ',' ; } elseif( true == array_key_exists( 'MinDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_available_units = ' . $this->sqlNumberOfAvailableUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfAvailableUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_available_units = ' . $this->sqlNumberOfAvailableUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors(). ',' ; } elseif( true == array_key_exists( 'NumberOfFloors', $this->getChangedColumns() ) ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfRooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription(). ',' ; } elseif( true == array_key_exists( 'BedroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription(). ',' ; } elseif( true == array_key_exists( 'BathroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration(). ',' ; } elseif( true == array_key_exists( 'BlockPricingIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled_virtual_move_in = ' . $this->sqlIsDisabledVirtualMoveIn(). ',' ; } elseif( true == array_key_exists( 'IsDisabledVirtualMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled_virtual_move_in = ' . $this->sqlIsDisabledVirtualMoveIn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_space_configuration_terms = ' . $this->sqlHasSpaceConfigurationTerms(). ',' ; } elseif( true == array_key_exists( 'HasSpaceConfigurationTerms', $this->getChangedColumns() ) ) { $strSql .= ' has_space_configuration_terms = ' . $this->sqlHasSpaceConfigurationTerms() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_rent_range = ' . $this->sqlIsManualRentRange(). ',' ; } elseif( true == array_key_exists( 'IsManualRentRange', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_rent_range = ' . $this->sqlIsManualRentRange() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_deposit_range = ' . $this->sqlIsManualDepositRange(). ',' ; } elseif( true == array_key_exists( 'IsManualDepositRange', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_deposit_range = ' . $this->sqlIsManualDepositRange() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured(). ',' ; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats(). ',' ; } elseif( true == array_key_exists( 'AllowsCats', $this->getChangedColumns() ) ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs(). ',' ; } elseif( true == array_key_exists( 'AllowsDogs', $this->getChangedColumns() ) ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_title = ' . $this->sqlSeoTitle(). ',' ; } elseif( true == array_key_exists( 'SeoTitle', $this->getChangedColumns() ) ) { $strSql .= ' seo_title = ' . $this->sqlSeoTitle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_description = ' . $this->sqlSeoDescription(). ',' ; } elseif( true == array_key_exists( 'SeoDescription', $this->getChangedColumns() ) ) { $strSql .= ' seo_description = ' . $this->sqlSeoDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_keywords = ' . $this->sqlSeoKeywords(). ',' ; } elseif( true == array_key_exists( 'SeoKeywords', $this->getChangedColumns() ) ) { $strSql .= ' seo_keywords = ' . $this->sqlSeoKeywords() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_call_for_pricing = ' . $this->sqlIsCallForPricing(). ',' ; } elseif( true == array_key_exists( 'IsCallForPricing', $this->getChangedColumns() ) ) { $strSql .= ' is_call_for_pricing = ' . $this->sqlIsCallForPricing() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'vaultware_number' => $this->getVaultwareNumber(),
			'floorplan_name' => $this->getFloorplanName(),
			'description' => $this->getDescription(),
			'min_rent' => $this->getMinRent(),
			'max_rent' => $this->getMaxRent(),
			'min_deposit' => $this->getMinDeposit(),
			'max_deposit' => $this->getMaxDeposit(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'number_of_units' => $this->getNumberOfUnits(),
			'number_of_available_units' => $this->getNumberOfAvailableUnits(),
			'number_of_floors' => $this->getNumberOfFloors(),
			'number_of_rooms' => $this->getNumberOfRooms(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'bedroom_description' => $this->getBedroomDescription(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'bathroom_description' => $this->getBathroomDescription(),
			'notes' => $this->getNotes(),
			'block_pricing_integration' => $this->getBlockPricingIntegration(),
			'is_disabled_virtual_move_in' => $this->getIsDisabledVirtualMoveIn(),
			'has_space_configuration_terms' => $this->getHasSpaceConfigurationTerms(),
			'is_manual_rent_range' => $this->getIsManualRentRange(),
			'is_manual_deposit_range' => $this->getIsManualDepositRange(),
			'is_published' => $this->getIsPublished(),
			'is_featured' => $this->getIsFeatured(),
			'allows_cats' => $this->getAllowsCats(),
			'allows_dogs' => $this->getAllowsDogs(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'seo_title' => $this->getSeoTitle(),
			'seo_description' => $this->getSeoDescription(),
			'seo_keywords' => $this->getSeoKeywords(),
			'is_call_for_pricing' => $this->getIsCallForPricing()
		);
	}

}
?>