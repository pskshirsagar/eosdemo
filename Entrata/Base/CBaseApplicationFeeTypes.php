<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationFeeTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationFeeTypes extends CEosPluralBase {

	/**
	 * @return CApplicationFeeType[]
	 */
	public static function fetchApplicationFeeTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationFeeType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationFeeType
	 */
	public static function fetchApplicationFeeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationFeeType', $objDatabase );
	}

	public static function fetchApplicationFeeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_fee_types', $objDatabase );
	}

	public static function fetchApplicationFeeTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationFeeType( sprintf( 'SELECT * FROM application_fee_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationFeeTypesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationFeeTypes( sprintf( 'SELECT * FROM application_fee_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>