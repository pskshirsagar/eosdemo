<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerIncomeLog extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_income_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerIncomeId;
	protected $m_intCustomerId;
	protected $m_intIncomeTypeId;
	protected $m_intFrequencyId;
	protected $m_intPriorCustomerIncomeLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strInstitutionName;
	protected $m_strInstitutionPhoneNumber;
	protected $m_strInstitutionStreetLine1;
	protected $m_strInstitutionStreetLine2;
	protected $m_strInstitutionStreetLine3;
	protected $m_strInstitutionCity;
	protected $m_strInstitutionStateCode;
	protected $m_strInstitutionProvince;
	protected $m_strInstitutionPostalCode;
	protected $m_strInstitutionCountryCode;
	protected $m_strContactName;
	protected $m_strContactPhoneNumber;
	protected $m_strContactPhoneExtension;
	protected $m_strContactFaxNumber;
	protected $m_strContactEmail;
	protected $m_strPositionOrTitle;
	protected $m_fltAmount;
	protected $m_strSocialSecurityClaimNumberEncrypted;
	protected $m_strIncomeEffectiveDate;
	protected $m_strDateStarted;
	protected $m_strDateEnded;
	protected $m_boolIsInstitutionAddressVerified;
	protected $m_strLogDatetime;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApplicantId;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intFrequencyId = '4';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_boolIsInstitutionAddressVerified = false;
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strInstitutionStreetLine1' => 'addressLine1',
				'm_strInstitutionStreetLine2' => 'addressLine2',
				'm_strInstitutionStreetLine3' => 'addressLine3',
				'm_strInstitutionCity' => 'locality',
				'm_strInstitutionStateCode' => 'administrativeArea',
				'm_strInstitutionPostalCode' => 'postalCode',
				'm_strInstitutionCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_income_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerIncomeId', trim( $arrValues['customer_income_id'] ) ); elseif( isset( $arrValues['customer_income_id'] ) ) $this->setCustomerIncomeId( $arrValues['customer_income_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['income_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIncomeTypeId', trim( $arrValues['income_type_id'] ) ); elseif( isset( $arrValues['income_type_id'] ) ) $this->setIncomeTypeId( $arrValues['income_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['prior_customer_income_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerIncomeLogId', trim( $arrValues['prior_customer_income_log_id'] ) ); elseif( isset( $arrValues['prior_customer_income_log_id'] ) ) $this->setPriorCustomerIncomeLogId( $arrValues['prior_customer_income_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['institution_name'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionName', trim( stripcslashes( $arrValues['institution_name'] ) ) ); elseif( isset( $arrValues['institution_name'] ) ) $this->setInstitutionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_name'] ) : $arrValues['institution_name'] );
		if( isset( $arrValues['institution_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionPhoneNumber', trim( stripcslashes( $arrValues['institution_phone_number'] ) ) ); elseif( isset( $arrValues['institution_phone_number'] ) ) $this->setInstitutionPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_phone_number'] ) : $arrValues['institution_phone_number'] );
		if( isset( $arrValues['institution_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine1', trim( stripcslashes( $arrValues['institution_street_line1'] ) ) ); elseif( isset( $arrValues['institution_street_line1'] ) ) $this->setInstitutionStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line1'] ) : $arrValues['institution_street_line1'] );
		if( isset( $arrValues['institution_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine2', trim( stripcslashes( $arrValues['institution_street_line2'] ) ) ); elseif( isset( $arrValues['institution_street_line2'] ) ) $this->setInstitutionStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line2'] ) : $arrValues['institution_street_line2'] );
		if( isset( $arrValues['institution_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine3', trim( stripcslashes( $arrValues['institution_street_line3'] ) ) ); elseif( isset( $arrValues['institution_street_line3'] ) ) $this->setInstitutionStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line3'] ) : $arrValues['institution_street_line3'] );
		if( isset( $arrValues['institution_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCity', trim( stripcslashes( $arrValues['institution_city'] ) ) ); elseif( isset( $arrValues['institution_city'] ) ) $this->setInstitutionCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_city'] ) : $arrValues['institution_city'] );
		if( isset( $arrValues['institution_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStateCode', trim( stripcslashes( $arrValues['institution_state_code'] ) ) ); elseif( isset( $arrValues['institution_state_code'] ) ) $this->setInstitutionStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_state_code'] ) : $arrValues['institution_state_code'] );
		if( isset( $arrValues['institution_province'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionProvince', trim( stripcslashes( $arrValues['institution_province'] ) ) ); elseif( isset( $arrValues['institution_province'] ) ) $this->setInstitutionProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_province'] ) : $arrValues['institution_province'] );
		if( isset( $arrValues['institution_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionPostalCode', trim( stripcslashes( $arrValues['institution_postal_code'] ) ) ); elseif( isset( $arrValues['institution_postal_code'] ) ) $this->setInstitutionPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_postal_code'] ) : $arrValues['institution_postal_code'] );
		if( isset( $arrValues['institution_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCountryCode', trim( stripcslashes( $arrValues['institution_country_code'] ) ) ); elseif( isset( $arrValues['institution_country_code'] ) ) $this->setInstitutionCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_country_code'] ) : $arrValues['institution_country_code'] );
		if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->set( 'm_strContactName', trim( stripcslashes( $arrValues['contact_name'] ) ) ); elseif( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
		if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneNumber', trim( stripcslashes( $arrValues['contact_phone_number'] ) ) ); elseif( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_number'] ) : $arrValues['contact_phone_number'] );
		if( isset( $arrValues['contact_phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneExtension', trim( stripcslashes( $arrValues['contact_phone_extension'] ) ) ); elseif( isset( $arrValues['contact_phone_extension'] ) ) $this->setContactPhoneExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_extension'] ) : $arrValues['contact_phone_extension'] );
		if( isset( $arrValues['contact_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strContactFaxNumber', trim( stripcslashes( $arrValues['contact_fax_number'] ) ) ); elseif( isset( $arrValues['contact_fax_number'] ) ) $this->setContactFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_fax_number'] ) : $arrValues['contact_fax_number'] );
		if( isset( $arrValues['contact_email'] ) && $boolDirectSet ) $this->set( 'm_strContactEmail', trim( stripcslashes( $arrValues['contact_email'] ) ) ); elseif( isset( $arrValues['contact_email'] ) ) $this->setContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_email'] ) : $arrValues['contact_email'] );
		if( isset( $arrValues['position_or_title'] ) && $boolDirectSet ) $this->set( 'm_strPositionOrTitle', trim( stripcslashes( $arrValues['position_or_title'] ) ) ); elseif( isset( $arrValues['position_or_title'] ) ) $this->setPositionOrTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['position_or_title'] ) : $arrValues['position_or_title'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['social_security_claim_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strSocialSecurityClaimNumberEncrypted', trim( stripcslashes( $arrValues['social_security_claim_number_encrypted'] ) ) ); elseif( isset( $arrValues['social_security_claim_number_encrypted'] ) ) $this->setSocialSecurityClaimNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['social_security_claim_number_encrypted'] ) : $arrValues['social_security_claim_number_encrypted'] );
		if( isset( $arrValues['income_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strIncomeEffectiveDate', trim( $arrValues['income_effective_date'] ) ); elseif( isset( $arrValues['income_effective_date'] ) ) $this->setIncomeEffectiveDate( $arrValues['income_effective_date'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_ended'] ) && $boolDirectSet ) $this->set( 'm_strDateEnded', trim( $arrValues['date_ended'] ) ); elseif( isset( $arrValues['date_ended'] ) ) $this->setDateEnded( $arrValues['date_ended'] );
		if( isset( $arrValues['is_institution_address_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsInstitutionAddressVerified', trim( stripcslashes( $arrValues['is_institution_address_verified'] ) ) ); elseif( isset( $arrValues['is_institution_address_verified'] ) ) $this->setIsInstitutionAddressVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_institution_address_verified'] ) : $arrValues['is_institution_address_verified'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerIncomeId( $intCustomerIncomeId ) {
		$this->set( 'm_intCustomerIncomeId', CStrings::strToIntDef( $intCustomerIncomeId, NULL, false ) );
	}

	public function getCustomerIncomeId() {
		return $this->m_intCustomerIncomeId;
	}

	public function sqlCustomerIncomeId() {
		return ( true == isset( $this->m_intCustomerIncomeId ) ) ? ( string ) $this->m_intCustomerIncomeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setIncomeTypeId( $intIncomeTypeId ) {
		$this->set( 'm_intIncomeTypeId', CStrings::strToIntDef( $intIncomeTypeId, NULL, false ) );
	}

	public function getIncomeTypeId() {
		return $this->m_intIncomeTypeId;
	}

	public function sqlIncomeTypeId() {
		return ( true == isset( $this->m_intIncomeTypeId ) ) ? ( string ) $this->m_intIncomeTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : '4';
	}

	public function setPriorCustomerIncomeLogId( $intPriorCustomerIncomeLogId ) {
		$this->set( 'm_intPriorCustomerIncomeLogId', CStrings::strToIntDef( $intPriorCustomerIncomeLogId, NULL, false ) );
	}

	public function getPriorCustomerIncomeLogId() {
		return $this->m_intPriorCustomerIncomeLogId;
	}

	public function sqlPriorCustomerIncomeLogId() {
		return ( true == isset( $this->m_intPriorCustomerIncomeLogId ) ) ? ( string ) $this->m_intPriorCustomerIncomeLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstitutionName( $strInstitutionName ) {
		$this->set( 'm_strInstitutionName', CStrings::strTrimDef( $strInstitutionName, 50, NULL, true ) );
	}

	public function getInstitutionName() {
		return $this->m_strInstitutionName;
	}

	public function sqlInstitutionName() {
		return ( true == isset( $this->m_strInstitutionName ) ) ? '\'' . addslashes( $this->m_strInstitutionName ) . '\'' : 'NULL';
	}

	public function setInstitutionPhoneNumber( $strInstitutionPhoneNumber ) {
		$this->set( 'm_strInstitutionPhoneNumber', CStrings::strTrimDef( $strInstitutionPhoneNumber, 30, NULL, true ) );
	}

	public function getInstitutionPhoneNumber() {
		return $this->m_strInstitutionPhoneNumber;
	}

	public function sqlInstitutionPhoneNumber() {
		return ( true == isset( $this->m_strInstitutionPhoneNumber ) ) ? '\'' . addslashes( $this->m_strInstitutionPhoneNumber ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine1( $strInstitutionStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strInstitutionStreetLine1, $strAddressKey = 'default', 'm_strInstitutionStreetLine1' );
	}

	public function getInstitutionStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strInstitutionStreetLine1' );
	}

	public function sqlInstitutionStreetLine1() {
		return ( true == isset( $this->m_strInstitutionStreetLine1 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine1 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine2( $strInstitutionStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strInstitutionStreetLine2, $strAddressKey = 'default', 'm_strInstitutionStreetLine2' );
	}

	public function getInstitutionStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strInstitutionStreetLine2' );
	}

	public function sqlInstitutionStreetLine2() {
		return ( true == isset( $this->m_strInstitutionStreetLine2 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine2 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine3( $strInstitutionStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strInstitutionStreetLine3, $strAddressKey = 'default', 'm_strInstitutionStreetLine3' );
	}

	public function getInstitutionStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strInstitutionStreetLine3' );
	}

	public function sqlInstitutionStreetLine3() {
		return ( true == isset( $this->m_strInstitutionStreetLine3 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine3 ) . '\'' : 'NULL';
	}

	public function setInstitutionCity( $strInstitutionCity ) {
		$this->setPostalAddressField( 'locality', $strInstitutionCity, $strAddressKey = 'default', 'm_strInstitutionCity' );
	}

	public function getInstitutionCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strInstitutionCity' );
	}

	public function sqlInstitutionCity() {
		return ( true == isset( $this->m_strInstitutionCity ) ) ? '\'' . addslashes( $this->m_strInstitutionCity ) . '\'' : 'NULL';
	}

	public function setInstitutionStateCode( $strInstitutionStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strInstitutionStateCode, $strAddressKey = 'default', 'm_strInstitutionStateCode' );
	}

	public function getInstitutionStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strInstitutionStateCode' );
	}

	public function sqlInstitutionStateCode() {
		return ( true == isset( $this->m_strInstitutionStateCode ) ) ? '\'' . addslashes( $this->m_strInstitutionStateCode ) . '\'' : 'NULL';
	}

	public function setInstitutionProvince( $strInstitutionProvince ) {
		$this->set( 'm_strInstitutionProvince', CStrings::strTrimDef( $strInstitutionProvince, 50, NULL, true ) );
	}

	public function getInstitutionProvince() {
		return $this->m_strInstitutionProvince;
	}

	public function sqlInstitutionProvince() {
		return ( true == isset( $this->m_strInstitutionProvince ) ) ? '\'' . addslashes( $this->m_strInstitutionProvince ) . '\'' : 'NULL';
	}

	public function setInstitutionPostalCode( $strInstitutionPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strInstitutionPostalCode, $strAddressKey = 'default', 'm_strInstitutionPostalCode' );
	}

	public function getInstitutionPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strInstitutionPostalCode' );
	}

	public function sqlInstitutionPostalCode() {
		return ( true == isset( $this->m_strInstitutionPostalCode ) ) ? '\'' . addslashes( $this->m_strInstitutionPostalCode ) . '\'' : 'NULL';
	}

	public function setInstitutionCountryCode( $strInstitutionCountryCode ) {
		$this->setPostalAddressField( 'country', $strInstitutionCountryCode, $strAddressKey = 'default', 'm_strInstitutionCountryCode' );
	}

	public function getInstitutionCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strInstitutionCountryCode' );
	}

	public function sqlInstitutionCountryCode() {
		return ( true == isset( $this->m_strInstitutionCountryCode ) ) ? '\'' . addslashes( $this->m_strInstitutionCountryCode ) . '\'' : 'NULL';
	}

	public function setContactName( $strContactName ) {
		$this->set( 'm_strContactName', CStrings::strTrimDef( $strContactName, 100, NULL, true ) );
	}

	public function getContactName() {
		return $this->m_strContactName;
	}

	public function sqlContactName() {
		return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
	}

	public function setContactPhoneNumber( $strContactPhoneNumber ) {
		$this->set( 'm_strContactPhoneNumber', CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true ) );
	}

	public function getContactPhoneNumber() {
		return $this->m_strContactPhoneNumber;
	}

	public function sqlContactPhoneNumber() {
		return ( true == isset( $this->m_strContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' : 'NULL';
	}

	public function setContactPhoneExtension( $strContactPhoneExtension ) {
		$this->set( 'm_strContactPhoneExtension', CStrings::strTrimDef( $strContactPhoneExtension, 6, NULL, true ) );
	}

	public function getContactPhoneExtension() {
		return $this->m_strContactPhoneExtension;
	}

	public function sqlContactPhoneExtension() {
		return ( true == isset( $this->m_strContactPhoneExtension ) ) ? '\'' . addslashes( $this->m_strContactPhoneExtension ) . '\'' : 'NULL';
	}

	public function setContactFaxNumber( $strContactFaxNumber ) {
		$this->set( 'm_strContactFaxNumber', CStrings::strTrimDef( $strContactFaxNumber, 30, NULL, true ) );
	}

	public function getContactFaxNumber() {
		return $this->m_strContactFaxNumber;
	}

	public function sqlContactFaxNumber() {
		return ( true == isset( $this->m_strContactFaxNumber ) ) ? '\'' . addslashes( $this->m_strContactFaxNumber ) . '\'' : 'NULL';
	}

	public function setContactEmail( $strContactEmail ) {
		$this->set( 'm_strContactEmail', CStrings::strTrimDef( $strContactEmail, 240, NULL, true ) );
	}

	public function getContactEmail() {
		return $this->m_strContactEmail;
	}

	public function sqlContactEmail() {
		return ( true == isset( $this->m_strContactEmail ) ) ? '\'' . addslashes( $this->m_strContactEmail ) . '\'' : 'NULL';
	}

	public function setPositionOrTitle( $strPositionOrTitle ) {
		$this->set( 'm_strPositionOrTitle', CStrings::strTrimDef( $strPositionOrTitle, 50, NULL, true ) );
	}

	public function getPositionOrTitle() {
		return $this->m_strPositionOrTitle;
	}

	public function sqlPositionOrTitle() {
		return ( true == isset( $this->m_strPositionOrTitle ) ) ? '\'' . addslashes( $this->m_strPositionOrTitle ) . '\'' : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 4 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setSocialSecurityClaimNumberEncrypted( $strSocialSecurityClaimNumberEncrypted ) {
		$this->set( 'm_strSocialSecurityClaimNumberEncrypted', CStrings::strTrimDef( $strSocialSecurityClaimNumberEncrypted, 12, NULL, true ) );
	}

	public function getSocialSecurityClaimNumberEncrypted() {
		return $this->m_strSocialSecurityClaimNumberEncrypted;
	}

	public function sqlSocialSecurityClaimNumberEncrypted() {
		return ( true == isset( $this->m_strSocialSecurityClaimNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strSocialSecurityClaimNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setIncomeEffectiveDate( $strIncomeEffectiveDate ) {
		$this->set( 'm_strIncomeEffectiveDate', CStrings::strTrimDef( $strIncomeEffectiveDate, -1, NULL, true ) );
	}

	public function getIncomeEffectiveDate() {
		return $this->m_strIncomeEffectiveDate;
	}

	public function sqlIncomeEffectiveDate() {
		return ( true == isset( $this->m_strIncomeEffectiveDate ) ) ? '\'' . $this->m_strIncomeEffectiveDate . '\'' : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateEnded( $strDateEnded ) {
		$this->set( 'm_strDateEnded', CStrings::strTrimDef( $strDateEnded, -1, NULL, true ) );
	}

	public function getDateEnded() {
		return $this->m_strDateEnded;
	}

	public function sqlDateEnded() {
		return ( true == isset( $this->m_strDateEnded ) ) ? '\'' . $this->m_strDateEnded . '\'' : 'NULL';
	}

	public function setIsInstitutionAddressVerified( $boolIsInstitutionAddressVerified ) {
		$this->set( 'm_boolIsInstitutionAddressVerified', CStrings::strToBool( $boolIsInstitutionAddressVerified ) );
	}

	public function getIsInstitutionAddressVerified() {
		return $this->m_boolIsInstitutionAddressVerified;
	}

	public function sqlIsInstitutionAddressVerified() {
		return ( true == isset( $this->m_boolIsInstitutionAddressVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInstitutionAddressVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_income_id, customer_id, income_type_id, frequency_id, prior_customer_income_log_id, post_date, apply_through_post_date, description, details, institution_name, institution_phone_number, institution_street_line1, institution_street_line2, institution_street_line3, institution_city, institution_state_code, institution_province, institution_postal_code, institution_country_code, contact_name, contact_phone_number, contact_phone_extension, contact_fax_number, contact_email, position_or_title, amount, social_security_claim_number_encrypted, income_effective_date, date_started, date_ended, is_institution_address_verified, log_datetime, is_post_date_ignored, is_opening_log, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, applicant_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerIncomeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlIncomeTypeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlPriorCustomerIncomeLogId() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlInstitutionName() . ', ' .
						$this->sqlInstitutionPhoneNumber() . ', ' .
						$this->sqlInstitutionStreetLine1() . ', ' .
						$this->sqlInstitutionStreetLine2() . ', ' .
						$this->sqlInstitutionStreetLine3() . ', ' .
						$this->sqlInstitutionCity() . ', ' .
						$this->sqlInstitutionStateCode() . ', ' .
						$this->sqlInstitutionProvince() . ', ' .
						$this->sqlInstitutionPostalCode() . ', ' .
						$this->sqlInstitutionCountryCode() . ', ' .
						$this->sqlContactName() . ', ' .
						$this->sqlContactPhoneNumber() . ', ' .
						$this->sqlContactPhoneExtension() . ', ' .
						$this->sqlContactFaxNumber() . ', ' .
						$this->sqlContactEmail() . ', ' .
						$this->sqlPositionOrTitle() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlSocialSecurityClaimNumberEncrypted() . ', ' .
						$this->sqlIncomeEffectiveDate() . ', ' .
						$this->sqlDateStarted() . ', ' .
						$this->sqlDateEnded() . ', ' .
						$this->sqlIsInstitutionAddressVerified() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApplicantId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_income_id = ' . $this->sqlCustomerIncomeId(). ',' ; } elseif( true == array_key_exists( 'CustomerIncomeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_income_id = ' . $this->sqlCustomerIncomeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId(). ',' ; } elseif( true == array_key_exists( 'IncomeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_income_log_id = ' . $this->sqlPriorCustomerIncomeLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCustomerIncomeLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_income_log_id = ' . $this->sqlPriorCustomerIncomeLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName(). ',' ; } elseif( true == array_key_exists( 'InstitutionName', $this->getChangedColumns() ) ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_phone_number = ' . $this->sqlInstitutionPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'InstitutionPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' institution_phone_number = ' . $this->sqlInstitutionPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity(). ',' ; } elseif( true == array_key_exists( 'InstitutionCity', $this->getChangedColumns() ) ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionStateCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince(). ',' ; } elseif( true == array_key_exists( 'InstitutionProvince', $this->getChangedColumns() ) ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName(). ',' ; } elseif( true == array_key_exists( 'ContactName', $this->getChangedColumns() ) ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber(). ',' ; } elseif( true == array_key_exists( 'ContactFaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail(). ',' ; } elseif( true == array_key_exists( 'ContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' position_or_title = ' . $this->sqlPositionOrTitle(). ',' ; } elseif( true == array_key_exists( 'PositionOrTitle', $this->getChangedColumns() ) ) { $strSql .= ' position_or_title = ' . $this->sqlPositionOrTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_security_claim_number_encrypted = ' . $this->sqlSocialSecurityClaimNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'SocialSecurityClaimNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' social_security_claim_number_encrypted = ' . $this->sqlSocialSecurityClaimNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income_effective_date = ' . $this->sqlIncomeEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'IncomeEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' income_effective_date = ' . $this->sqlIncomeEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted(). ',' ; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded(). ',' ; } elseif( true == array_key_exists( 'DateEnded', $this->getChangedColumns() ) ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_institution_address_verified = ' . $this->sqlIsInstitutionAddressVerified(). ',' ; } elseif( true == array_key_exists( 'IsInstitutionAddressVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_institution_address_verified = ' . $this->sqlIsInstitutionAddressVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_income_id' => $this->getCustomerIncomeId(),
			'customer_id' => $this->getCustomerId(),
			'income_type_id' => $this->getIncomeTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'prior_customer_income_log_id' => $this->getPriorCustomerIncomeLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'institution_name' => $this->getInstitutionName(),
			'institution_phone_number' => $this->getInstitutionPhoneNumber(),
			'institution_street_line1' => $this->getInstitutionStreetLine1(),
			'institution_street_line2' => $this->getInstitutionStreetLine2(),
			'institution_street_line3' => $this->getInstitutionStreetLine3(),
			'institution_city' => $this->getInstitutionCity(),
			'institution_state_code' => $this->getInstitutionStateCode(),
			'institution_province' => $this->getInstitutionProvince(),
			'institution_postal_code' => $this->getInstitutionPostalCode(),
			'institution_country_code' => $this->getInstitutionCountryCode(),
			'contact_name' => $this->getContactName(),
			'contact_phone_number' => $this->getContactPhoneNumber(),
			'contact_phone_extension' => $this->getContactPhoneExtension(),
			'contact_fax_number' => $this->getContactFaxNumber(),
			'contact_email' => $this->getContactEmail(),
			'position_or_title' => $this->getPositionOrTitle(),
			'amount' => $this->getAmount(),
			'social_security_claim_number_encrypted' => $this->getSocialSecurityClaimNumberEncrypted(),
			'income_effective_date' => $this->getIncomeEffectiveDate(),
			'date_started' => $this->getDateStarted(),
			'date_ended' => $this->getDateEnded(),
			'is_institution_address_verified' => $this->getIsInstitutionAddressVerified(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'applicant_id' => $this->getApplicantId()
		);
	}

}
?>