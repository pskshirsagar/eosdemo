<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyOccupancyTypeSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_occupancy_type_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intOccupancyTypeId;
	protected $m_intArTriggerId;
	protected $m_boolDisplayAdvertisedFrequency;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intInvoiceDocumentId;
	protected $m_boolEnableInvoicing;
	protected $m_strInvoicingStartDate;
	protected $m_strInvoicePrefix;
	protected $m_strCorrectionInvoicePrefix;
	protected $m_boolAutoGenerateInvoices;
	protected $m_intAutoInvoiceDay;
	protected $m_boolAutoAllocatePayments;
	protected $m_intCorrectiveInvoiceDocumentId;
	protected $m_intDownPaymentInvoiceDocumentId;
	protected $m_strDownPaymentInvoicePrefix;
	protected $m_intCreditNoteInvoiceDocumentId;
	protected $m_strCreditNoteInvoicePrefix;
	protected $m_boolEmailCustomerInvoices;
	protected $m_intInvoiceDefaultDueDays;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupancyTypeId = '1';
		$this->m_intArTriggerId = '307';
		$this->m_boolDisplayAdvertisedFrequency = true;
		$this->m_boolEnableInvoicing = false;
		$this->m_boolAutoGenerateInvoices = false;
		$this->m_boolAutoAllocatePayments = true;
		$this->m_boolEmailCustomerInvoices = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['display_advertised_frequency'] ) && $boolDirectSet ) $this->set( 'm_boolDisplayAdvertisedFrequency', trim( stripcslashes( $arrValues['display_advertised_frequency'] ) ) ); elseif( isset( $arrValues['display_advertised_frequency'] ) ) $this->setDisplayAdvertisedFrequency( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_advertised_frequency'] ) : $arrValues['display_advertised_frequency'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['invoice_document_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDocumentId', trim( $arrValues['invoice_document_id'] ) ); elseif( isset( $arrValues['invoice_document_id'] ) ) $this->setInvoiceDocumentId( $arrValues['invoice_document_id'] );
		if( isset( $arrValues['enable_invoicing'] ) && $boolDirectSet ) $this->set( 'm_boolEnableInvoicing', trim( stripcslashes( $arrValues['enable_invoicing'] ) ) ); elseif( isset( $arrValues['enable_invoicing'] ) ) $this->setEnableInvoicing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['enable_invoicing'] ) : $arrValues['enable_invoicing'] );
		if( isset( $arrValues['invoicing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoicingStartDate', trim( $arrValues['invoicing_start_date'] ) ); elseif( isset( $arrValues['invoicing_start_date'] ) ) $this->setInvoicingStartDate( $arrValues['invoicing_start_date'] );
		if( isset( $arrValues['invoice_prefix'] ) && $boolDirectSet ) $this->set( 'm_strInvoicePrefix', trim( $arrValues['invoice_prefix'] ) ); elseif( isset( $arrValues['invoice_prefix'] ) ) $this->setInvoicePrefix( $arrValues['invoice_prefix'] );
		if( isset( $arrValues['correction_invoice_prefix'] ) && $boolDirectSet ) $this->set( 'm_strCorrectionInvoicePrefix', trim( $arrValues['correction_invoice_prefix'] ) ); elseif( isset( $arrValues['correction_invoice_prefix'] ) ) $this->setCorrectionInvoicePrefix( $arrValues['correction_invoice_prefix'] );
		if( isset( $arrValues['auto_generate_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAutoGenerateInvoices', trim( stripcslashes( $arrValues['auto_generate_invoices'] ) ) ); elseif( isset( $arrValues['auto_generate_invoices'] ) ) $this->setAutoGenerateInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_generate_invoices'] ) : $arrValues['auto_generate_invoices'] );
		if( isset( $arrValues['auto_invoice_day'] ) && $boolDirectSet ) $this->set( 'm_intAutoInvoiceDay', trim( $arrValues['auto_invoice_day'] ) ); elseif( isset( $arrValues['auto_invoice_day'] ) ) $this->setAutoInvoiceDay( $arrValues['auto_invoice_day'] );
		if( isset( $arrValues['auto_allocate_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAutoAllocatePayments', trim( stripcslashes( $arrValues['auto_allocate_payments'] ) ) ); elseif( isset( $arrValues['auto_allocate_payments'] ) ) $this->setAutoAllocatePayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_allocate_payments'] ) : $arrValues['auto_allocate_payments'] );
		if( isset( $arrValues['corrective_invoice_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCorrectiveInvoiceDocumentId', trim( $arrValues['corrective_invoice_document_id'] ) ); elseif( isset( $arrValues['corrective_invoice_document_id'] ) ) $this->setCorrectiveInvoiceDocumentId( $arrValues['corrective_invoice_document_id'] );
		if( isset( $arrValues['down_payment_invoice_document_id'] ) && $boolDirectSet ) $this->set( 'm_intDownPaymentInvoiceDocumentId', trim( $arrValues['down_payment_invoice_document_id'] ) ); elseif( isset( $arrValues['down_payment_invoice_document_id'] ) ) $this->setDownPaymentInvoiceDocumentId( $arrValues['down_payment_invoice_document_id'] );
		if( isset( $arrValues['down_payment_invoice_prefix'] ) && $boolDirectSet ) $this->set( 'm_strDownPaymentInvoicePrefix', trim( $arrValues['down_payment_invoice_prefix'] ) ); elseif( isset( $arrValues['down_payment_invoice_prefix'] ) ) $this->setDownPaymentInvoicePrefix( $arrValues['down_payment_invoice_prefix'] );
		if( isset( $arrValues['credit_note_invoice_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditNoteInvoiceDocumentId', trim( $arrValues['credit_note_invoice_document_id'] ) ); elseif( isset( $arrValues['credit_note_invoice_document_id'] ) ) $this->setCreditNoteInvoiceDocumentId( $arrValues['credit_note_invoice_document_id'] );
		if( isset( $arrValues['credit_note_invoice_prefix'] ) && $boolDirectSet ) $this->set( 'm_strCreditNoteInvoicePrefix', trim( $arrValues['credit_note_invoice_prefix'] ) ); elseif( isset( $arrValues['credit_note_invoice_prefix'] ) ) $this->setCreditNoteInvoicePrefix( $arrValues['credit_note_invoice_prefix'] );
		if( isset( $arrValues['email_customer_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolEmailCustomerInvoices', trim( stripcslashes( $arrValues['email_customer_invoices'] ) ) ); elseif( isset( $arrValues['email_customer_invoices'] ) ) $this->setEmailCustomerInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_customer_invoices'] ) : $arrValues['email_customer_invoices'] );
		if( isset( $arrValues['invoice_default_due_days'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDefaultDueDays', trim( $arrValues['invoice_default_due_days'] ) ); elseif( isset( $arrValues['invoice_default_due_days'] ) ) $this->setInvoiceDefaultDueDays( $arrValues['invoice_default_due_days'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : '307';
	}

	public function setDisplayAdvertisedFrequency( $boolDisplayAdvertisedFrequency ) {
		$this->set( 'm_boolDisplayAdvertisedFrequency', CStrings::strToBool( $boolDisplayAdvertisedFrequency ) );
	}

	public function getDisplayAdvertisedFrequency() {
		return $this->m_boolDisplayAdvertisedFrequency;
	}

	public function sqlDisplayAdvertisedFrequency() {
		return ( true == isset( $this->m_boolDisplayAdvertisedFrequency ) ) ? '\'' . ( true == ( bool ) $this->m_boolDisplayAdvertisedFrequency ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInvoiceDocumentId( $intInvoiceDocumentId ) {
		$this->set( 'm_intInvoiceDocumentId', CStrings::strToIntDef( $intInvoiceDocumentId, NULL, false ) );
	}

	public function getInvoiceDocumentId() {
		return $this->m_intInvoiceDocumentId;
	}

	public function sqlInvoiceDocumentId() {
		return ( true == isset( $this->m_intInvoiceDocumentId ) ) ? ( string ) $this->m_intInvoiceDocumentId : 'NULL';
	}

	public function setEnableInvoicing( $boolEnableInvoicing ) {
		$this->set( 'm_boolEnableInvoicing', CStrings::strToBool( $boolEnableInvoicing ) );
	}

	public function getEnableInvoicing() {
		return $this->m_boolEnableInvoicing;
	}

	public function sqlEnableInvoicing() {
		return ( true == isset( $this->m_boolEnableInvoicing ) ) ? '\'' . ( true == ( bool ) $this->m_boolEnableInvoicing ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInvoicingStartDate( $strInvoicingStartDate ) {
		$this->set( 'm_strInvoicingStartDate', CStrings::strTrimDef( $strInvoicingStartDate, -1, NULL, true ) );
	}

	public function getInvoicingStartDate() {
		return $this->m_strInvoicingStartDate;
	}

	public function sqlInvoicingStartDate() {
		return ( true == isset( $this->m_strInvoicingStartDate ) ) ? '\'' . $this->m_strInvoicingStartDate . '\'' : 'NULL';
	}

	public function setInvoicePrefix( $strInvoicePrefix ) {
		$this->set( 'm_strInvoicePrefix', CStrings::strTrimDef( $strInvoicePrefix, 100, NULL, true ) );
	}

	public function getInvoicePrefix() {
		return $this->m_strInvoicePrefix;
	}

	public function sqlInvoicePrefix() {
		return ( true == isset( $this->m_strInvoicePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInvoicePrefix ) : '\'' . addslashes( $this->m_strInvoicePrefix ) . '\'' ) : 'NULL';
	}

	public function setCorrectionInvoicePrefix( $strCorrectionInvoicePrefix ) {
		$this->set( 'm_strCorrectionInvoicePrefix', CStrings::strTrimDef( $strCorrectionInvoicePrefix, 100, NULL, true ) );
	}

	public function getCorrectionInvoicePrefix() {
		return $this->m_strCorrectionInvoicePrefix;
	}

	public function sqlCorrectionInvoicePrefix() {
		return ( true == isset( $this->m_strCorrectionInvoicePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectionInvoicePrefix ) : '\'' . addslashes( $this->m_strCorrectionInvoicePrefix ) . '\'' ) : 'NULL';
	}

	public function setAutoGenerateInvoices( $boolAutoGenerateInvoices ) {
		$this->set( 'm_boolAutoGenerateInvoices', CStrings::strToBool( $boolAutoGenerateInvoices ) );
	}

	public function getAutoGenerateInvoices() {
		return $this->m_boolAutoGenerateInvoices;
	}

	public function sqlAutoGenerateInvoices() {
		return ( true == isset( $this->m_boolAutoGenerateInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoGenerateInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoInvoiceDay( $intAutoInvoiceDay ) {
		$this->set( 'm_intAutoInvoiceDay', CStrings::strToIntDef( $intAutoInvoiceDay, NULL, false ) );
	}

	public function getAutoInvoiceDay() {
		return $this->m_intAutoInvoiceDay;
	}

	public function sqlAutoInvoiceDay() {
		return ( true == isset( $this->m_intAutoInvoiceDay ) ) ? ( string ) $this->m_intAutoInvoiceDay : 'NULL';
	}

	public function setAutoAllocatePayments( $boolAutoAllocatePayments ) {
		$this->set( 'm_boolAutoAllocatePayments', CStrings::strToBool( $boolAutoAllocatePayments ) );
	}

	public function getAutoAllocatePayments() {
		return $this->m_boolAutoAllocatePayments;
	}

	public function sqlAutoAllocatePayments() {
		return ( true == isset( $this->m_boolAutoAllocatePayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoAllocatePayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCorrectiveInvoiceDocumentId( $intCorrectiveInvoiceDocumentId ) {
		$this->set( 'm_intCorrectiveInvoiceDocumentId', CStrings::strToIntDef( $intCorrectiveInvoiceDocumentId, NULL, false ) );
	}

	public function getCorrectiveInvoiceDocumentId() {
		return $this->m_intCorrectiveInvoiceDocumentId;
	}

	public function sqlCorrectiveInvoiceDocumentId() {
		return ( true == isset( $this->m_intCorrectiveInvoiceDocumentId ) ) ? ( string ) $this->m_intCorrectiveInvoiceDocumentId : 'NULL';
	}

	public function setDownPaymentInvoiceDocumentId( $intDownPaymentInvoiceDocumentId ) {
		$this->set( 'm_intDownPaymentInvoiceDocumentId', CStrings::strToIntDef( $intDownPaymentInvoiceDocumentId, NULL, false ) );
	}

	public function getDownPaymentInvoiceDocumentId() {
		return $this->m_intDownPaymentInvoiceDocumentId;
	}

	public function sqlDownPaymentInvoiceDocumentId() {
		return ( true == isset( $this->m_intDownPaymentInvoiceDocumentId ) ) ? ( string ) $this->m_intDownPaymentInvoiceDocumentId : 'NULL';
	}

	public function setDownPaymentInvoicePrefix( $strDownPaymentInvoicePrefix ) {
		$this->set( 'm_strDownPaymentInvoicePrefix', CStrings::strTrimDef( $strDownPaymentInvoicePrefix, 100, NULL, true ) );
	}

	public function getDownPaymentInvoicePrefix() {
		return $this->m_strDownPaymentInvoicePrefix;
	}

	public function sqlDownPaymentInvoicePrefix() {
		return ( true == isset( $this->m_strDownPaymentInvoicePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDownPaymentInvoicePrefix ) : '\'' . addslashes( $this->m_strDownPaymentInvoicePrefix ) . '\'' ) : 'NULL';
	}

	public function setCreditNoteInvoiceDocumentId( $intCreditNoteInvoiceDocumentId ) {
		$this->set( 'm_intCreditNoteInvoiceDocumentId', CStrings::strToIntDef( $intCreditNoteInvoiceDocumentId, NULL, false ) );
	}

	public function getCreditNoteInvoiceDocumentId() {
		return $this->m_intCreditNoteInvoiceDocumentId;
	}

	public function sqlCreditNoteInvoiceDocumentId() {
		return ( true == isset( $this->m_intCreditNoteInvoiceDocumentId ) ) ? ( string ) $this->m_intCreditNoteInvoiceDocumentId : 'NULL';
	}

	public function setCreditNoteInvoicePrefix( $strCreditNoteInvoicePrefix ) {
		$this->set( 'm_strCreditNoteInvoicePrefix', CStrings::strTrimDef( $strCreditNoteInvoicePrefix, -1, NULL, true ) );
	}

	public function getCreditNoteInvoicePrefix() {
		return $this->m_strCreditNoteInvoicePrefix;
	}

	public function sqlCreditNoteInvoicePrefix() {
		return ( true == isset( $this->m_strCreditNoteInvoicePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCreditNoteInvoicePrefix ) : '\'' . addslashes( $this->m_strCreditNoteInvoicePrefix ) . '\'' ) : 'NULL';
	}

	public function setEmailCustomerInvoices( $boolEmailCustomerInvoices ) {
		$this->set( 'm_boolEmailCustomerInvoices', CStrings::strToBool( $boolEmailCustomerInvoices ) );
	}

	public function getEmailCustomerInvoices() {
		return $this->m_boolEmailCustomerInvoices;
	}

	public function sqlEmailCustomerInvoices() {
		return ( true == isset( $this->m_boolEmailCustomerInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolEmailCustomerInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInvoiceDefaultDueDays( $intInvoiceDefaultDueDays ) {
		$this->set( 'm_intInvoiceDefaultDueDays', CStrings::strToIntDef( $intInvoiceDefaultDueDays, NULL, false ) );
	}

	public function getInvoiceDefaultDueDays() {
		return $this->m_intInvoiceDefaultDueDays;
	}

	public function sqlInvoiceDefaultDueDays() {
		return ( true == isset( $this->m_intInvoiceDefaultDueDays ) ) ? ( string ) $this->m_intInvoiceDefaultDueDays : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, occupancy_type_id, ar_trigger_id, display_advertised_frequency, updated_by, updated_on, created_by, created_on, invoice_document_id, enable_invoicing, invoicing_start_date, invoice_prefix, correction_invoice_prefix, auto_generate_invoices, auto_invoice_day, auto_allocate_payments, corrective_invoice_document_id, down_payment_invoice_document_id, down_payment_invoice_prefix, credit_note_invoice_document_id, credit_note_invoice_prefix, email_customer_invoices, invoice_default_due_days )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlDisplayAdvertisedFrequency() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlInvoiceDocumentId() . ', ' .
						$this->sqlEnableInvoicing() . ', ' .
						$this->sqlInvoicingStartDate() . ', ' .
						$this->sqlInvoicePrefix() . ', ' .
						$this->sqlCorrectionInvoicePrefix() . ', ' .
						$this->sqlAutoGenerateInvoices() . ', ' .
						$this->sqlAutoInvoiceDay() . ', ' .
						$this->sqlAutoAllocatePayments() . ', ' .
						$this->sqlCorrectiveInvoiceDocumentId() . ', ' .
						$this->sqlDownPaymentInvoiceDocumentId() . ', ' .
						$this->sqlDownPaymentInvoicePrefix() . ', ' .
						$this->sqlCreditNoteInvoiceDocumentId() . ', ' .
						$this->sqlCreditNoteInvoicePrefix() . ', ' .
						$this->sqlEmailCustomerInvoices() . ', ' .
						$this->sqlInvoiceDefaultDueDays() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_advertised_frequency = ' . $this->sqlDisplayAdvertisedFrequency(). ',' ; } elseif( true == array_key_exists( 'DisplayAdvertisedFrequency', $this->getChangedColumns() ) ) { $strSql .= ' display_advertised_frequency = ' . $this->sqlDisplayAdvertisedFrequency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_document_id = ' . $this->sqlInvoiceDocumentId(). ',' ; } elseif( true == array_key_exists( 'InvoiceDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_document_id = ' . $this->sqlInvoiceDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_invoicing = ' . $this->sqlEnableInvoicing(). ',' ; } elseif( true == array_key_exists( 'EnableInvoicing', $this->getChangedColumns() ) ) { $strSql .= ' enable_invoicing = ' . $this->sqlEnableInvoicing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoicing_start_date = ' . $this->sqlInvoicingStartDate(). ',' ; } elseif( true == array_key_exists( 'InvoicingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' invoicing_start_date = ' . $this->sqlInvoicingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_prefix = ' . $this->sqlInvoicePrefix(). ',' ; } elseif( true == array_key_exists( 'InvoicePrefix', $this->getChangedColumns() ) ) { $strSql .= ' invoice_prefix = ' . $this->sqlInvoicePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correction_invoice_prefix = ' . $this->sqlCorrectionInvoicePrefix(). ',' ; } elseif( true == array_key_exists( 'CorrectionInvoicePrefix', $this->getChangedColumns() ) ) { $strSql .= ' correction_invoice_prefix = ' . $this->sqlCorrectionInvoicePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_generate_invoices = ' . $this->sqlAutoGenerateInvoices(). ',' ; } elseif( true == array_key_exists( 'AutoGenerateInvoices', $this->getChangedColumns() ) ) { $strSql .= ' auto_generate_invoices = ' . $this->sqlAutoGenerateInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_invoice_day = ' . $this->sqlAutoInvoiceDay(). ',' ; } elseif( true == array_key_exists( 'AutoInvoiceDay', $this->getChangedColumns() ) ) { $strSql .= ' auto_invoice_day = ' . $this->sqlAutoInvoiceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_allocate_payments = ' . $this->sqlAutoAllocatePayments(). ',' ; } elseif( true == array_key_exists( 'AutoAllocatePayments', $this->getChangedColumns() ) ) { $strSql .= ' auto_allocate_payments = ' . $this->sqlAutoAllocatePayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrective_invoice_document_id = ' . $this->sqlCorrectiveInvoiceDocumentId(). ',' ; } elseif( true == array_key_exists( 'CorrectiveInvoiceDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' corrective_invoice_document_id = ' . $this->sqlCorrectiveInvoiceDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' down_payment_invoice_document_id = ' . $this->sqlDownPaymentInvoiceDocumentId(). ',' ; } elseif( true == array_key_exists( 'DownPaymentInvoiceDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' down_payment_invoice_document_id = ' . $this->sqlDownPaymentInvoiceDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' down_payment_invoice_prefix = ' . $this->sqlDownPaymentInvoicePrefix(). ',' ; } elseif( true == array_key_exists( 'DownPaymentInvoicePrefix', $this->getChangedColumns() ) ) { $strSql .= ' down_payment_invoice_prefix = ' . $this->sqlDownPaymentInvoicePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_note_invoice_document_id = ' . $this->sqlCreditNoteInvoiceDocumentId(). ',' ; } elseif( true == array_key_exists( 'CreditNoteInvoiceDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' credit_note_invoice_document_id = ' . $this->sqlCreditNoteInvoiceDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_note_invoice_prefix = ' . $this->sqlCreditNoteInvoicePrefix(). ',' ; } elseif( true == array_key_exists( 'CreditNoteInvoicePrefix', $this->getChangedColumns() ) ) { $strSql .= ' credit_note_invoice_prefix = ' . $this->sqlCreditNoteInvoicePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_customer_invoices = ' . $this->sqlEmailCustomerInvoices(). ',' ; } elseif( true == array_key_exists( 'EmailCustomerInvoices', $this->getChangedColumns() ) ) { $strSql .= ' email_customer_invoices = ' . $this->sqlEmailCustomerInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_default_due_days = ' . $this->sqlInvoiceDefaultDueDays(). ',' ; } elseif( true == array_key_exists( 'InvoiceDefaultDueDays', $this->getChangedColumns() ) ) { $strSql .= ' invoice_default_due_days = ' . $this->sqlInvoiceDefaultDueDays() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'display_advertised_frequency' => $this->getDisplayAdvertisedFrequency(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'invoice_document_id' => $this->getInvoiceDocumentId(),
			'enable_invoicing' => $this->getEnableInvoicing(),
			'invoicing_start_date' => $this->getInvoicingStartDate(),
			'invoice_prefix' => $this->getInvoicePrefix(),
			'correction_invoice_prefix' => $this->getCorrectionInvoicePrefix(),
			'auto_generate_invoices' => $this->getAutoGenerateInvoices(),
			'auto_invoice_day' => $this->getAutoInvoiceDay(),
			'auto_allocate_payments' => $this->getAutoAllocatePayments(),
			'corrective_invoice_document_id' => $this->getCorrectiveInvoiceDocumentId(),
			'down_payment_invoice_document_id' => $this->getDownPaymentInvoiceDocumentId(),
			'down_payment_invoice_prefix' => $this->getDownPaymentInvoicePrefix(),
			'credit_note_invoice_document_id' => $this->getCreditNoteInvoiceDocumentId(),
			'credit_note_invoice_prefix' => $this->getCreditNoteInvoicePrefix(),
			'email_customer_invoices' => $this->getEmailCustomerInvoices(),
			'invoice_default_due_days' => $this->getInvoiceDefaultDueDays()
		);
	}

}
?>