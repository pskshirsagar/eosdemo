<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationConditionSet extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_application_condition_sets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intScreeningApplicationRequestId;
	protected $m_intConditionSetId;
	protected $m_strConditionSetName;
	protected $m_intConditionSubsetOneId;
	protected $m_intConditionSubsetTwoId;
	protected $m_intRequiredItemsSubsetOne;
	protected $m_intRequiredItemsSubsetTwo;
	protected $m_intIsActive;
	protected $m_intSatisfiedBy;
	protected $m_strSatisfiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScreeningConditionOperandTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['screening_application_request_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicationRequestId', trim( $arrValues['screening_application_request_id'] ) ); elseif( isset( $arrValues['screening_application_request_id'] ) ) $this->setScreeningApplicationRequestId( $arrValues['screening_application_request_id'] );
		if( isset( $arrValues['condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionSetId', trim( $arrValues['condition_set_id'] ) ); elseif( isset( $arrValues['condition_set_id'] ) ) $this->setConditionSetId( $arrValues['condition_set_id'] );
		if( isset( $arrValues['condition_set_name'] ) && $boolDirectSet ) $this->set( 'm_strConditionSetName', trim( stripcslashes( $arrValues['condition_set_name'] ) ) ); elseif( isset( $arrValues['condition_set_name'] ) ) $this->setConditionSetName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_set_name'] ) : $arrValues['condition_set_name'] );
		if( isset( $arrValues['condition_subset_one_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionSubsetOneId', trim( $arrValues['condition_subset_one_id'] ) ); elseif( isset( $arrValues['condition_subset_one_id'] ) ) $this->setConditionSubsetOneId( $arrValues['condition_subset_one_id'] );
		if( isset( $arrValues['condition_subset_two_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionSubsetTwoId', trim( $arrValues['condition_subset_two_id'] ) ); elseif( isset( $arrValues['condition_subset_two_id'] ) ) $this->setConditionSubsetTwoId( $arrValues['condition_subset_two_id'] );
		if( isset( $arrValues['required_items_subset_one'] ) && $boolDirectSet ) $this->set( 'm_intRequiredItemsSubsetOne', trim( $arrValues['required_items_subset_one'] ) ); elseif( isset( $arrValues['required_items_subset_one'] ) ) $this->setRequiredItemsSubsetOne( $arrValues['required_items_subset_one'] );
		if( isset( $arrValues['required_items_subset_two'] ) && $boolDirectSet ) $this->set( 'm_intRequiredItemsSubsetTwo', trim( $arrValues['required_items_subset_two'] ) ); elseif( isset( $arrValues['required_items_subset_two'] ) ) $this->setRequiredItemsSubsetTwo( $arrValues['required_items_subset_two'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['satisfied_by'] ) && $boolDirectSet ) $this->set( 'm_intSatisfiedBy', trim( $arrValues['satisfied_by'] ) ); elseif( isset( $arrValues['satisfied_by'] ) ) $this->setSatisfiedBy( $arrValues['satisfied_by'] );
		if( isset( $arrValues['satisfied_on'] ) && $boolDirectSet ) $this->set( 'm_strSatisfiedOn', trim( $arrValues['satisfied_on'] ) ); elseif( isset( $arrValues['satisfied_on'] ) ) $this->setSatisfiedOn( $arrValues['satisfied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_condition_operand_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConditionOperandTypeId', trim( $arrValues['screening_condition_operand_type_id'] ) ); elseif( isset( $arrValues['screening_condition_operand_type_id'] ) ) $this->setScreeningConditionOperandTypeId( $arrValues['screening_condition_operand_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setScreeningApplicationRequestId( $intScreeningApplicationRequestId ) {
		$this->set( 'm_intScreeningApplicationRequestId', CStrings::strToIntDef( $intScreeningApplicationRequestId, NULL, false ) );
	}

	public function getScreeningApplicationRequestId() {
		return $this->m_intScreeningApplicationRequestId;
	}

	public function sqlScreeningApplicationRequestId() {
		return ( true == isset( $this->m_intScreeningApplicationRequestId ) ) ? ( string ) $this->m_intScreeningApplicationRequestId : 'NULL';
	}

	public function setConditionSetId( $intConditionSetId ) {
		$this->set( 'm_intConditionSetId', CStrings::strToIntDef( $intConditionSetId, NULL, false ) );
	}

	public function getConditionSetId() {
		return $this->m_intConditionSetId;
	}

	public function sqlConditionSetId() {
		return ( true == isset( $this->m_intConditionSetId ) ) ? ( string ) $this->m_intConditionSetId : 'NULL';
	}

	public function setConditionSetName( $strConditionSetName ) {
		$this->set( 'm_strConditionSetName', CStrings::strTrimDef( $strConditionSetName, -1, NULL, true ) );
	}

	public function getConditionSetName() {
		return $this->m_strConditionSetName;
	}

	public function sqlConditionSetName() {
		return ( true == isset( $this->m_strConditionSetName ) ) ? '\'' . addslashes( $this->m_strConditionSetName ) . '\'' : 'NULL';
	}

	public function setConditionSubsetOneId( $intConditionSubsetOneId ) {
		$this->set( 'm_intConditionSubsetOneId', CStrings::strToIntDef( $intConditionSubsetOneId, NULL, false ) );
	}

	public function getConditionSubsetOneId() {
		return $this->m_intConditionSubsetOneId;
	}

	public function sqlConditionSubsetOneId() {
		return ( true == isset( $this->m_intConditionSubsetOneId ) ) ? ( string ) $this->m_intConditionSubsetOneId : 'NULL';
	}

	public function setConditionSubsetTwoId( $intConditionSubsetTwoId ) {
		$this->set( 'm_intConditionSubsetTwoId', CStrings::strToIntDef( $intConditionSubsetTwoId, NULL, false ) );
	}

	public function getConditionSubsetTwoId() {
		return $this->m_intConditionSubsetTwoId;
	}

	public function sqlConditionSubsetTwoId() {
		return ( true == isset( $this->m_intConditionSubsetTwoId ) ) ? ( string ) $this->m_intConditionSubsetTwoId : 'NULL';
	}

	public function setRequiredItemsSubsetOne( $intRequiredItemsSubsetOne ) {
		$this->set( 'm_intRequiredItemsSubsetOne', CStrings::strToIntDef( $intRequiredItemsSubsetOne, NULL, false ) );
	}

	public function getRequiredItemsSubsetOne() {
		return $this->m_intRequiredItemsSubsetOne;
	}

	public function sqlRequiredItemsSubsetOne() {
		return ( true == isset( $this->m_intRequiredItemsSubsetOne ) ) ? ( string ) $this->m_intRequiredItemsSubsetOne : 'NULL';
	}

	public function setRequiredItemsSubsetTwo( $intRequiredItemsSubsetTwo ) {
		$this->set( 'm_intRequiredItemsSubsetTwo', CStrings::strToIntDef( $intRequiredItemsSubsetTwo, NULL, false ) );
	}

	public function getRequiredItemsSubsetTwo() {
		return $this->m_intRequiredItemsSubsetTwo;
	}

	public function sqlRequiredItemsSubsetTwo() {
		return ( true == isset( $this->m_intRequiredItemsSubsetTwo ) ) ? ( string ) $this->m_intRequiredItemsSubsetTwo : 'NULL';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : 'NULL';
	}

	public function setSatisfiedBy( $intSatisfiedBy ) {
		$this->set( 'm_intSatisfiedBy', CStrings::strToIntDef( $intSatisfiedBy, NULL, false ) );
	}

	public function getSatisfiedBy() {
		return $this->m_intSatisfiedBy;
	}

	public function sqlSatisfiedBy() {
		return ( true == isset( $this->m_intSatisfiedBy ) ) ? ( string ) $this->m_intSatisfiedBy : 'NULL';
	}

	public function setSatisfiedOn( $strSatisfiedOn ) {
		$this->set( 'm_strSatisfiedOn', CStrings::strTrimDef( $strSatisfiedOn, -1, NULL, true ) );
	}

	public function getSatisfiedOn() {
		return $this->m_strSatisfiedOn;
	}

	public function sqlSatisfiedOn() {
		return ( true == isset( $this->m_strSatisfiedOn ) ) ? '\'' . $this->m_strSatisfiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningConditionOperandTypeId( $intScreeningConditionOperandTypeId ) {
		$this->set( 'm_intScreeningConditionOperandTypeId', CStrings::strToIntDef( $intScreeningConditionOperandTypeId, NULL, false ) );
	}

	public function getScreeningConditionOperandTypeId() {
		return $this->m_intScreeningConditionOperandTypeId;
	}

	public function sqlScreeningConditionOperandTypeId() {
		return ( true == isset( $this->m_intScreeningConditionOperandTypeId ) ) ? ( string ) $this->m_intScreeningConditionOperandTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, screening_application_request_id, condition_set_id, condition_set_name, condition_subset_one_id, condition_subset_two_id, required_items_subset_one, required_items_subset_two, is_active, satisfied_by, satisfied_on, updated_by, updated_on, created_by, created_on, screening_condition_operand_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlScreeningApplicationRequestId() . ', ' .
						$this->sqlConditionSetId() . ', ' .
						$this->sqlConditionSetName() . ', ' .
						$this->sqlConditionSubsetOneId() . ', ' .
						$this->sqlConditionSubsetTwoId() . ', ' .
						$this->sqlRequiredItemsSubsetOne() . ', ' .
						$this->sqlRequiredItemsSubsetTwo() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlSatisfiedBy() . ', ' .
						$this->sqlSatisfiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningConditionOperandTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_set_id = ' . $this->sqlConditionSetId(). ',' ; } elseif( true == array_key_exists( 'ConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' condition_set_id = ' . $this->sqlConditionSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_set_name = ' . $this->sqlConditionSetName(). ',' ; } elseif( true == array_key_exists( 'ConditionSetName', $this->getChangedColumns() ) ) { $strSql .= ' condition_set_name = ' . $this->sqlConditionSetName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_subset_one_id = ' . $this->sqlConditionSubsetOneId(). ',' ; } elseif( true == array_key_exists( 'ConditionSubsetOneId', $this->getChangedColumns() ) ) { $strSql .= ' condition_subset_one_id = ' . $this->sqlConditionSubsetOneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_subset_two_id = ' . $this->sqlConditionSubsetTwoId(). ',' ; } elseif( true == array_key_exists( 'ConditionSubsetTwoId', $this->getChangedColumns() ) ) { $strSql .= ' condition_subset_two_id = ' . $this->sqlConditionSubsetTwoId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_items_subset_one = ' . $this->sqlRequiredItemsSubsetOne(). ',' ; } elseif( true == array_key_exists( 'RequiredItemsSubsetOne', $this->getChangedColumns() ) ) { $strSql .= ' required_items_subset_one = ' . $this->sqlRequiredItemsSubsetOne() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_items_subset_two = ' . $this->sqlRequiredItemsSubsetTwo(). ',' ; } elseif( true == array_key_exists( 'RequiredItemsSubsetTwo', $this->getChangedColumns() ) ) { $strSql .= ' required_items_subset_two = ' . $this->sqlRequiredItemsSubsetTwo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy(). ',' ; } elseif( true == array_key_exists( 'SatisfiedBy', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn(). ',' ; } elseif( true == array_key_exists( 'SatisfiedOn', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_condition_operand_type_id = ' . $this->sqlScreeningConditionOperandTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningConditionOperandTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_condition_operand_type_id = ' . $this->sqlScreeningConditionOperandTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'screening_application_request_id' => $this->getScreeningApplicationRequestId(),
			'condition_set_id' => $this->getConditionSetId(),
			'condition_set_name' => $this->getConditionSetName(),
			'condition_subset_one_id' => $this->getConditionSubsetOneId(),
			'condition_subset_two_id' => $this->getConditionSubsetTwoId(),
			'required_items_subset_one' => $this->getRequiredItemsSubsetOne(),
			'required_items_subset_two' => $this->getRequiredItemsSubsetTwo(),
			'is_active' => $this->getIsActive(),
			'satisfied_by' => $this->getSatisfiedBy(),
			'satisfied_on' => $this->getSatisfiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_condition_operand_type_id' => $this->getScreeningConditionOperandTypeId()
		);
	}

}
?>