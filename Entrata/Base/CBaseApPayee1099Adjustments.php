<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayee1099Adjustments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayee1099Adjustments extends CEosPluralBase {

	/**
	 * @return CApPayee1099Adjustment[]
	 */
	public static function fetchApPayee1099Adjustments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayee1099Adjustment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayee1099Adjustment
	 */
	public static function fetchApPayee1099Adjustment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayee1099Adjustment', $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_1099_adjustments', $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayee1099Adjustment( sprintf( 'SELECT * FROM ap_payee_1099_adjustments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayee1099Adjustments( sprintf( 'SELECT * FROM ap_payee_1099_adjustments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayee1099Adjustments( sprintf( 'SELECT * FROM ap_payee_1099_adjustments WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApPayee1099Adjustments( sprintf( 'SELECT * FROM ap_payee_1099_adjustments WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayee1099AdjustmentsByApLegalEntityIdByCid( $intApLegalEntityId, $intCid, $objDatabase ) {
		return self::fetchApPayee1099Adjustments( sprintf( 'SELECT * FROM ap_payee_1099_adjustments WHERE ap_legal_entity_id = %d AND cid = %d', ( int ) $intApLegalEntityId, ( int ) $intCid ), $objDatabase );
	}

}
?>