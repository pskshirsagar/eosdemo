<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRankGroups
 * Do not add any new functions to this class.
 */

class CBaseRankGroups extends CEosPluralBase {

	/**
	 * @return CRankGroup[]
	 */
	public static function fetchRankGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRankGroup::class, $objDatabase );
	}

	/**
	 * @return CRankGroup
	 */
	public static function fetchRankGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRankGroup::class, $objDatabase );
	}

	public static function fetchRankGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rank_groups', $objDatabase );
	}

	public static function fetchRankGroupById( $intId, $objDatabase ) {
		return self::fetchRankGroup( sprintf( 'SELECT * FROM rank_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>