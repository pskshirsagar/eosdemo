<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantAsset extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intApplicantId;
    protected $m_intApplicantAssetTypeId;
    protected $m_strInstitutionName;
    protected $m_fltAssetValue;
    protected $m_strAccountNumberEncrypted;
    protected $m_strRoutingNumber;
    protected $m_strNameOnAccount;
    protected $m_strAddress;
    protected $m_strCity;
    protected $m_strStateCode;
    protected $m_strContactName;
    protected $m_strContactPhoneNumber;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->m_intApplicantId = trim( $arrValues['applicant_id'] ); else if( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
        if( isset( $arrValues['applicant_asset_type_id'] ) && $boolDirectSet ) $this->m_intApplicantAssetTypeId = trim( $arrValues['applicant_asset_type_id'] ); else if( isset( $arrValues['applicant_asset_type_id'] ) ) $this->setApplicantAssetTypeId( $arrValues['applicant_asset_type_id'] );
        if( isset( $arrValues['institution_name'] ) && $boolDirectSet ) $this->m_strInstitutionName = trim( stripcslashes( $arrValues['institution_name'] ) ); else if( isset( $arrValues['institution_name'] ) ) $this->setInstitutionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_name'] ) : $arrValues['institution_name'] );
        if( isset( $arrValues['asset_value'] ) && $boolDirectSet ) $this->m_fltAssetValue = trim( $arrValues['asset_value'] ); else if( isset( $arrValues['asset_value'] ) ) $this->setAssetValue( $arrValues['asset_value'] );
        if( isset( $arrValues['account_number_encrypted'] ) && $boolDirectSet ) $this->m_strAccountNumberEncrypted = trim( stripcslashes( $arrValues['account_number_encrypted'] ) ); else if( isset( $arrValues['account_number_encrypted'] ) ) $this->setAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number_encrypted'] ) : $arrValues['account_number_encrypted'] );
        if( isset( $arrValues['routing_number'] ) && $boolDirectSet ) $this->m_strRoutingNumber = trim( stripcslashes( $arrValues['routing_number'] ) ); else if( isset( $arrValues['routing_number'] ) ) $this->setRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['routing_number'] ) : $arrValues['routing_number'] );
        if( isset( $arrValues['name_on_account'] ) && $boolDirectSet ) $this->m_strNameOnAccount = trim( stripcslashes( $arrValues['name_on_account'] ) ); else if( isset( $arrValues['name_on_account'] ) ) $this->setNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_on_account'] ) : $arrValues['name_on_account'] );
        if( isset( $arrValues['address'] ) && $boolDirectSet ) $this->m_strAddress = trim( stripcslashes( $arrValues['address'] ) ); else if( isset( $arrValues['address'] ) ) $this->setAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address'] ) : $arrValues['address'] );
        if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->m_strCity = trim( stripcslashes( $arrValues['city'] ) ); else if( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
        if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->m_strStateCode = trim( stripcslashes( $arrValues['state_code'] ) ); else if( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
        if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->m_strContactName = trim( stripcslashes( $arrValues['contact_name'] ) ); else if( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
        if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->m_strContactPhoneNumber = trim( stripcslashes( $arrValues['contact_phone_number'] ) ); else if( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_number'] ) : $arrValues['contact_phone_number'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setApplicantId( $intApplicantId ) {
        $this->m_intApplicantId = CStrings::strToIntDef( $intApplicantId, NULL, false );
    }

    public function getApplicantId() {
        return $this->m_intApplicantId;
    }

    public function sqlApplicantId() {
        return ( true == isset( $this->m_intApplicantId ) ) ? (string) $this->m_intApplicantId : 'NULL';
    }

    public function setApplicantAssetTypeId( $intApplicantAssetTypeId ) {
        $this->m_intApplicantAssetTypeId = CStrings::strToIntDef( $intApplicantAssetTypeId, NULL, false );
    }

    public function getApplicantAssetTypeId() {
        return $this->m_intApplicantAssetTypeId;
    }

    public function sqlApplicantAssetTypeId() {
        return ( true == isset( $this->m_intApplicantAssetTypeId ) ) ? (string) $this->m_intApplicantAssetTypeId : 'NULL';
    }

    public function setInstitutionName( $strInstitutionName ) {
        $this->m_strInstitutionName = CStrings::strTrimDef( $strInstitutionName, 100, NULL, true );
    }

    public function getInstitutionName() {
        return $this->m_strInstitutionName;
    }

    public function sqlInstitutionName() {
        return ( true == isset( $this->m_strInstitutionName ) ) ? '\'' . addslashes( $this->m_strInstitutionName ) . '\'' : 'NULL';
    }

    public function setAssetValue( $fltAssetValue ) {
        $this->m_fltAssetValue = CStrings::strToFloatDef( $fltAssetValue, NULL, false, 4 );
    }

    public function getAssetValue() {
        return $this->m_fltAssetValue;
    }

    public function sqlAssetValue() {
        return ( true == isset( $this->m_fltAssetValue ) ) ? (string) $this->m_fltAssetValue : 'NULL';
    }

    public function setAccountNumberEncrypted( $strAccountNumberEncrypted ) {
        $this->m_strAccountNumberEncrypted = CStrings::strTrimDef( $strAccountNumberEncrypted, 240, NULL, true );
    }

    public function getAccountNumberEncrypted() {
        return $this->m_strAccountNumberEncrypted;
    }

    public function sqlAccountNumberEncrypted() {
        return ( true == isset( $this->m_strAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strAccountNumberEncrypted ) . '\'' : 'NULL';
    }

    public function setRoutingNumber( $strRoutingNumber ) {
        $this->m_strRoutingNumber = CStrings::strTrimDef( $strRoutingNumber, 240, NULL, true );
    }

    public function getRoutingNumber() {
        return $this->m_strRoutingNumber;
    }

    public function sqlRoutingNumber() {
        return ( true == isset( $this->m_strRoutingNumber ) ) ? '\'' . addslashes( $this->m_strRoutingNumber ) . '\'' : 'NULL';
    }

    public function setNameOnAccount( $strNameOnAccount ) {
        $this->m_strNameOnAccount = CStrings::strTrimDef( $strNameOnAccount, 50, NULL, true );
    }

    public function getNameOnAccount() {
        return $this->m_strNameOnAccount;
    }

    public function sqlNameOnAccount() {
        return ( true == isset( $this->m_strNameOnAccount ) ) ? '\'' . addslashes( $this->m_strNameOnAccount ) . '\'' : 'NULL';
    }

    public function setAddress( $strAddress ) {
        $this->m_strAddress = CStrings::strTrimDef( $strAddress, 100, NULL, true );
    }

    public function getAddress() {
        return $this->m_strAddress;
    }

    public function sqlAddress() {
        return ( true == isset( $this->m_strAddress ) ) ? '\'' . addslashes( $this->m_strAddress ) . '\'' : 'NULL';
    }

    public function setCity( $strCity ) {
        $this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
    }

    public function getCity() {
        return $this->m_strCity;
    }

    public function sqlCity() {
        return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
    }

    public function setStateCode( $strStateCode ) {
        $this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
    }

    public function getStateCode() {
        return $this->m_strStateCode;
    }

    public function sqlStateCode() {
        return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
    }

    public function setContactName( $strContactName ) {
        $this->m_strContactName = CStrings::strTrimDef( $strContactName, 50, NULL, true );
    }

    public function getContactName() {
        return $this->m_strContactName;
    }

    public function sqlContactName() {
        return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
    }

    public function setContactPhoneNumber( $strContactPhoneNumber ) {
        $this->m_strContactPhoneNumber = CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true );
    }

    public function getContactPhoneNumber() {
        return $this->m_strContactPhoneNumber;
    }

    public function sqlContactPhoneNumber() {
        return ( true == isset( $this->m_strContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.applicant_assets_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.applicant_assets					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlApplicantId() . ', ' . 		                $this->sqlApplicantAssetTypeId() . ', ' . 		                $this->sqlInstitutionName() . ', ' . 		                $this->sqlAssetValue() . ', ' . 		                $this->sqlAccountNumberEncrypted() . ', ' . 		                $this->sqlRoutingNumber() . ', ' . 		                $this->sqlNameOnAccount() . ', ' . 		                $this->sqlAddress() . ', ' . 		                $this->sqlCity() . ', ' . 		                $this->sqlStateCode() . ', ' . 		                $this->sqlContactName() . ', ' . 		                $this->sqlContactPhoneNumber() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.applicant_assets
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicantId() ) != $this->getOriginalValueByFieldName ( 'applicant_id' ) ) { $arrstrOriginalValueChanges['applicant_id'] = $this->sqlApplicantId(); $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_asset_type_id = ' . $this->sqlApplicantAssetTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicantAssetTypeId() ) != $this->getOriginalValueByFieldName ( 'applicant_asset_type_id' ) ) { $arrstrOriginalValueChanges['applicant_asset_type_id'] = $this->sqlApplicantAssetTypeId(); $strSql .= ' applicant_asset_type_id = ' . $this->sqlApplicantAssetTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInstitutionName() ) != $this->getOriginalValueByFieldName ( 'institution_name' ) ) { $arrstrOriginalValueChanges['institution_name'] = $this->sqlInstitutionName(); $strSql .= ' institution_name = ' . $this->sqlInstitutionName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_value = ' . $this->sqlAssetValue() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlAssetValue() ), $this->getOriginalValueByFieldName ( 'asset_value' ), 4 ) ) ) { $arrstrOriginalValueChanges['asset_value'] = $this->sqlAssetValue(); $strSql .= ' asset_value = ' . $this->sqlAssetValue() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAccountNumberEncrypted() ) != $this->getOriginalValueByFieldName ( 'account_number_encrypted' ) ) { $arrstrOriginalValueChanges['account_number_encrypted'] = $this->sqlAccountNumberEncrypted(); $strSql .= ' account_number_encrypted = ' . $this->sqlAccountNumberEncrypted() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRoutingNumber() ) != $this->getOriginalValueByFieldName ( 'routing_number' ) ) { $arrstrOriginalValueChanges['routing_number'] = $this->sqlRoutingNumber(); $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNameOnAccount() ) != $this->getOriginalValueByFieldName ( 'name_on_account' ) ) { $arrstrOriginalValueChanges['name_on_account'] = $this->sqlNameOnAccount(); $strSql .= ' name_on_account = ' . $this->sqlNameOnAccount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAddress() ) != $this->getOriginalValueByFieldName ( 'address' ) ) { $arrstrOriginalValueChanges['address'] = $this->sqlAddress(); $strSql .= ' address = ' . $this->sqlAddress() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCity() ) != $this->getOriginalValueByFieldName ( 'city' ) ) { $arrstrOriginalValueChanges['city'] = $this->sqlCity(); $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStateCode() ) != $this->getOriginalValueByFieldName ( 'state_code' ) ) { $arrstrOriginalValueChanges['state_code'] = $this->sqlStateCode(); $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactName() ) != $this->getOriginalValueByFieldName ( 'contact_name' ) ) { $arrstrOriginalValueChanges['contact_name'] = $this->sqlContactName(); $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactPhoneNumber() ) != $this->getOriginalValueByFieldName ( 'contact_phone_number' ) ) { $arrstrOriginalValueChanges['contact_phone_number'] = $this->sqlContactPhoneNumber(); $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.applicant_assets WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.applicant_assets_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'applicant_id' => $this->getApplicantId(),
        	'applicant_asset_type_id' => $this->getApplicantAssetTypeId(),
        	'institution_name' => $this->getInstitutionName(),
        	'asset_value' => $this->getAssetValue(),
        	'account_number_encrypted' => $this->getAccountNumberEncrypted(),
        	'routing_number' => $this->getRoutingNumber(),
        	'name_on_account' => $this->getNameOnAccount(),
        	'address' => $this->getAddress(),
        	'city' => $this->getCity(),
        	'state_code' => $this->getStateCode(),
        	'contact_name' => $this->getContactName(),
        	'contact_phone_number' => $this->getContactPhoneNumber(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>