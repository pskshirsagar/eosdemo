<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CArPaymentTransmissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPaymentTransmissions extends CEosPluralBase {

	/**
	 * @return CArPaymentTransmission[]
	 */
	public static function fetchArPaymentTransmissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CArPaymentTransmission', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPaymentTransmission
	 */
	public static function fetchArPaymentTransmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CArPaymentTransmission', $objDatabase );
	}

	public static function fetchArPaymentTransmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_transmissions', $objDatabase );
	}

	public static function fetchArPaymentTransmissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransmission( sprintf( 'SELECT * FROM ar_payment_transmissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransmissionsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentTransmissions( sprintf( 'SELECT * FROM ar_payment_transmissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransmissionsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransmissions( sprintf( 'SELECT * FROM ar_payment_transmissions WHERE company_merchant_account_id = %d AND cid = %d', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentTransmissionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchArPaymentTransmissions( sprintf( 'SELECT * FROM ar_payment_transmissions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>