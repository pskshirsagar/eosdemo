<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySpecialClaimStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySpecialClaimStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySpecialClaimStatusType[]
	 */
	public static function fetchSubsidySpecialClaimStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySpecialClaimStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySpecialClaimStatusType
	 */
	public static function fetchSubsidySpecialClaimStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySpecialClaimStatusType::class, $objDatabase );
	}

	public static function fetchSubsidySpecialClaimStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_special_claim_status_types', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySpecialClaimStatusType( sprintf( 'SELECT * FROM subsidy_special_claim_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>