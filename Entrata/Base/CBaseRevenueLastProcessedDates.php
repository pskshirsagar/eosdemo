<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueLastProcessedDates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueLastProcessedDates extends CEosPluralBase {

	/**
	 * @return CRevenueLastProcessedDate[]
	 */
	public static function fetchRevenueLastProcessedDates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueLastProcessedDate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueLastProcessedDate
	 */
	public static function fetchRevenueLastProcessedDate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueLastProcessedDate', $objDatabase );
	}

	public static function fetchRevenueLastProcessedDateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_last_processed_dates', $objDatabase );
	}

	public static function fetchRevenueLastProcessedDateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueLastProcessedDate( sprintf( 'SELECT * FROM revenue_last_processed_dates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueLastProcessedDatesByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueLastProcessedDates( sprintf( 'SELECT * FROM revenue_last_processed_dates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>