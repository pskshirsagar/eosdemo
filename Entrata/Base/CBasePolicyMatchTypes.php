<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPolicyMatchTypes
 * Do not add any new functions to this class.
 */

class CBasePolicyMatchTypes extends CEosPluralBase {

	/**
	 * @return CPolicyMatchType[]
	 */
	public static function fetchPolicyMatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPolicyMatchType::class, $objDatabase );
	}

	/**
	 * @return CPolicyMatchType
	 */
	public static function fetchPolicyMatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPolicyMatchType::class, $objDatabase );
	}

	public static function fetchPolicyMatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'policy_match_types', $objDatabase );
	}

	public static function fetchPolicyMatchTypeById( $intId, $objDatabase ) {
		return self::fetchPolicyMatchType( sprintf( 'SELECT * FROM policy_match_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>