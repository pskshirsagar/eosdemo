<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceBoards
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceBoards extends CEosPluralBase {

	/**
	 * @return CMaintenanceBoard[]
	 */
	public static function fetchMaintenanceBoards( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceBoard::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceBoard
	 */
	public static function fetchMaintenanceBoard( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceBoard::class, $objDatabase );
	}

	public static function fetchMaintenanceBoardCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_boards', $objDatabase );
	}

	public static function fetchMaintenanceBoardByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoard( sprintf( 'SELECT * FROM maintenance_boards WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceBoardsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoards( sprintf( 'SELECT * FROM maintenance_boards WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>