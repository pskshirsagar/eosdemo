<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyStudentExceptionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyStudentExceptionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyStudentExceptionType[]
	 */
	public static function fetchSubsidyStudentExceptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyStudentExceptionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyStudentExceptionType
	 */
	public static function fetchSubsidyStudentExceptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyStudentExceptionType::class, $objDatabase );
	}

	public static function fetchSubsidyStudentExceptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_student_exception_types', $objDatabase );
	}

	public static function fetchSubsidyStudentExceptionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyStudentExceptionType( sprintf( 'SELECT * FROM subsidy_student_exception_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>