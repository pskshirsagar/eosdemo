<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerConciergeServices
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerConciergeServices extends CEosPluralBase {

	/**
	 * @return CCustomerConciergeService[]
	 */
	public static function fetchCustomerConciergeServices( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerConciergeService', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerConciergeService
	 */
	public static function fetchCustomerConciergeService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerConciergeService', $objDatabase );
	}

	public static function fetchCustomerConciergeServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_concierge_services', $objDatabase );
	}

	public static function fetchCustomerConciergeServiceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeService( sprintf( 'SELECT * FROM customer_concierge_services WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCustomerConciergeServices( sprintf( 'SELECT * FROM customer_concierge_services WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>