<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApCode extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ap_codes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultApCodeId;
	protected $m_intApCodeTypeId;
	protected $m_intApGlAccountId;
	protected $m_intItemGlAccountId;
	protected $m_intWipGlAccountId;
	protected $m_intConsumptionGlAccountId;
	protected $m_intArCodeId;
	protected $m_intJobApCodeId;
	protected $m_intAssetTypeId;
	protected $m_intApCodeCategoryId;
	protected $m_intUnitOfMeasureId;
	protected $m_intDepreciationCategoryId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intReferenceId;
	protected $m_strRemotePrimaryKey;
	protected $m_strName;
	protected $m_strItemNumber;
	protected $m_strDescription;
	protected $m_fltDefaultAmount;
	protected $m_intOrderNum;
	protected $m_boolPostToWip;
	protected $m_boolIsReserved;
	protected $m_boolIsSystem;
	protected $m_boolIsDisabled;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCompetitorId;
	protected $m_intPsProductId;
	protected $m_intServiceLifeMonths;

	public function __construct() {
		parent::__construct();

		$this->m_intApCodeTypeId = '1';
		$this->m_fltDefaultAmount = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolPostToWip = true;
		$this->m_boolIsReserved = false;
		$this->m_boolIsSystem = false;
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultApCodeId', trim( $arrValues['default_ap_code_id'] ) ); elseif( isset( $arrValues['default_ap_code_id'] ) ) $this->setDefaultApCodeId( $arrValues['default_ap_code_id'] );
		if( isset( $arrValues['ap_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeTypeId', trim( $arrValues['ap_code_type_id'] ) ); elseif( isset( $arrValues['ap_code_type_id'] ) ) $this->setApCodeTypeId( $arrValues['ap_code_type_id'] );
		if( isset( $arrValues['ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApGlAccountId', trim( $arrValues['ap_gl_account_id'] ) ); elseif( isset( $arrValues['ap_gl_account_id'] ) ) $this->setApGlAccountId( $arrValues['ap_gl_account_id'] );
		if( isset( $arrValues['item_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intItemGlAccountId', trim( $arrValues['item_gl_account_id'] ) ); elseif( isset( $arrValues['item_gl_account_id'] ) ) $this->setItemGlAccountId( $arrValues['item_gl_account_id'] );
		if( isset( $arrValues['wip_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intWipGlAccountId', trim( $arrValues['wip_gl_account_id'] ) ); elseif( isset( $arrValues['wip_gl_account_id'] ) ) $this->setWipGlAccountId( $arrValues['wip_gl_account_id'] );
		if( isset( $arrValues['consumption_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumptionGlAccountId', trim( $arrValues['consumption_gl_account_id'] ) ); elseif( isset( $arrValues['consumption_gl_account_id'] ) ) $this->setConsumptionGlAccountId( $arrValues['consumption_gl_account_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['job_ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intJobApCodeId', trim( $arrValues['job_ap_code_id'] ) ); elseif( isset( $arrValues['job_ap_code_id'] ) ) $this->setJobApCodeId( $arrValues['job_ap_code_id'] );
		if( isset( $arrValues['asset_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetTypeId', trim( $arrValues['asset_type_id'] ) ); elseif( isset( $arrValues['asset_type_id'] ) ) $this->setAssetTypeId( $arrValues['asset_type_id'] );
		if( isset( $arrValues['ap_code_category_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeCategoryId', trim( $arrValues['ap_code_category_id'] ) ); elseif( isset( $arrValues['ap_code_category_id'] ) ) $this->setApCodeCategoryId( $arrValues['ap_code_category_id'] );
		if( isset( $arrValues['unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitOfMeasureId', trim( $arrValues['unit_of_measure_id'] ) ); elseif( isset( $arrValues['unit_of_measure_id'] ) ) $this->setUnitOfMeasureId( $arrValues['unit_of_measure_id'] );
		if( isset( $arrValues['depreciation_category_id'] ) && $boolDirectSet ) $this->set( 'm_intDepreciationCategoryId', trim( $arrValues['depreciation_category_id'] ) ); elseif( isset( $arrValues['depreciation_category_id'] ) ) $this->setDepreciationCategoryId( $arrValues['depreciation_category_id'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['item_number'] ) && $boolDirectSet ) $this->set( 'm_strItemNumber', trim( $arrValues['item_number'] ) ); elseif( isset( $arrValues['item_number'] ) ) $this->setItemNumber( $arrValues['item_number'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['post_to_wip'] ) && $boolDirectSet ) $this->set( 'm_boolPostToWip', trim( stripcslashes( $arrValues['post_to_wip'] ) ) ); elseif( isset( $arrValues['post_to_wip'] ) ) $this->setPostToWip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_to_wip'] ) : $arrValues['post_to_wip'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_boolIsReserved', trim( stripcslashes( $arrValues['is_reserved'] ) ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reserved'] ) : $arrValues['is_reserved'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['service_life_months'] ) && $boolDirectSet ) $this->set( 'm_intServiceLifeMonths', trim( $arrValues['service_life_months'] ) ); elseif( isset( $arrValues['service_life_months'] ) ) $this->setServiceLifeMonths( $arrValues['service_life_months'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultApCodeId( $intDefaultApCodeId ) {
		$this->set( 'm_intDefaultApCodeId', CStrings::strToIntDef( $intDefaultApCodeId, NULL, false ) );
	}

	public function getDefaultApCodeId() {
		return $this->m_intDefaultApCodeId;
	}

	public function sqlDefaultApCodeId() {
		return ( true == isset( $this->m_intDefaultApCodeId ) ) ? ( string ) $this->m_intDefaultApCodeId : 'NULL';
	}

	public function setApCodeTypeId( $intApCodeTypeId ) {
		$this->set( 'm_intApCodeTypeId', CStrings::strToIntDef( $intApCodeTypeId, NULL, false ) );
	}

	public function getApCodeTypeId() {
		return $this->m_intApCodeTypeId;
	}

	public function sqlApCodeTypeId() {
		return ( true == isset( $this->m_intApCodeTypeId ) ) ? ( string ) $this->m_intApCodeTypeId : '1';
	}

	public function setApGlAccountId( $intApGlAccountId ) {
		$this->set( 'm_intApGlAccountId', CStrings::strToIntDef( $intApGlAccountId, NULL, false ) );
	}

	public function getApGlAccountId() {
		return $this->m_intApGlAccountId;
	}

	public function sqlApGlAccountId() {
		return ( true == isset( $this->m_intApGlAccountId ) ) ? ( string ) $this->m_intApGlAccountId : 'NULL';
	}

	public function setItemGlAccountId( $intItemGlAccountId ) {
		$this->set( 'm_intItemGlAccountId', CStrings::strToIntDef( $intItemGlAccountId, NULL, false ) );
	}

	public function getItemGlAccountId() {
		return $this->m_intItemGlAccountId;
	}

	public function sqlItemGlAccountId() {
		return ( true == isset( $this->m_intItemGlAccountId ) ) ? ( string ) $this->m_intItemGlAccountId : 'NULL';
	}

	public function setWipGlAccountId( $intWipGlAccountId ) {
		$this->set( 'm_intWipGlAccountId', CStrings::strToIntDef( $intWipGlAccountId, NULL, false ) );
	}

	public function getWipGlAccountId() {
		return $this->m_intWipGlAccountId;
	}

	public function sqlWipGlAccountId() {
		return ( true == isset( $this->m_intWipGlAccountId ) ) ? ( string ) $this->m_intWipGlAccountId : 'NULL';
	}

	public function setConsumptionGlAccountId( $intConsumptionGlAccountId ) {
		$this->set( 'm_intConsumptionGlAccountId', CStrings::strToIntDef( $intConsumptionGlAccountId, NULL, false ) );
	}

	public function getConsumptionGlAccountId() {
		return $this->m_intConsumptionGlAccountId;
	}

	public function sqlConsumptionGlAccountId() {
		return ( true == isset( $this->m_intConsumptionGlAccountId ) ) ? ( string ) $this->m_intConsumptionGlAccountId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setJobApCodeId( $intJobApCodeId ) {
		$this->set( 'm_intJobApCodeId', CStrings::strToIntDef( $intJobApCodeId, NULL, false ) );
	}

	public function getJobApCodeId() {
		return $this->m_intJobApCodeId;
	}

	public function sqlJobApCodeId() {
		return ( true == isset( $this->m_intJobApCodeId ) ) ? ( string ) $this->m_intJobApCodeId : 'NULL';
	}

	public function setAssetTypeId( $intAssetTypeId ) {
		$this->set( 'm_intAssetTypeId', CStrings::strToIntDef( $intAssetTypeId, NULL, false ) );
	}

	public function getAssetTypeId() {
		return $this->m_intAssetTypeId;
	}

	public function sqlAssetTypeId() {
		return ( true == isset( $this->m_intAssetTypeId ) ) ? ( string ) $this->m_intAssetTypeId : 'NULL';
	}

	public function setApCodeCategoryId( $intApCodeCategoryId ) {
		$this->set( 'm_intApCodeCategoryId', CStrings::strToIntDef( $intApCodeCategoryId, NULL, false ) );
	}

	public function getApCodeCategoryId() {
		return $this->m_intApCodeCategoryId;
	}

	public function sqlApCodeCategoryId() {
		return ( true == isset( $this->m_intApCodeCategoryId ) ) ? ( string ) $this->m_intApCodeCategoryId : 'NULL';
	}

	public function setUnitOfMeasureId( $intUnitOfMeasureId ) {
		$this->set( 'm_intUnitOfMeasureId', CStrings::strToIntDef( $intUnitOfMeasureId, NULL, false ) );
	}

	public function getUnitOfMeasureId() {
		return $this->m_intUnitOfMeasureId;
	}

	public function sqlUnitOfMeasureId() {
		return ( true == isset( $this->m_intUnitOfMeasureId ) ) ? ( string ) $this->m_intUnitOfMeasureId : 'NULL';
	}

	public function setDepreciationCategoryId( $intDepreciationCategoryId ) {
		$this->set( 'm_intDepreciationCategoryId', CStrings::strToIntDef( $intDepreciationCategoryId, NULL, false ) );
	}

	public function getDepreciationCategoryId() {
		return $this->m_intDepreciationCategoryId;
	}

	public function sqlDepreciationCategoryId() {
		return ( true == isset( $this->m_intDepreciationCategoryId ) ) ? ( string ) $this->m_intDepreciationCategoryId : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 240, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setItemNumber( $strItemNumber ) {
		$this->set( 'm_strItemNumber', CStrings::strTrimDef( $strItemNumber, 50, NULL, true ) );
	}

	public function getItemNumber() {
		return $this->m_strItemNumber;
	}

	public function sqlItemNumber() {
		return ( true == isset( $this->m_strItemNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strItemNumber ) : '\'' . addslashes( $this->m_strItemNumber ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 4 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPostToWip( $boolPostToWip ) {
		$this->set( 'm_boolPostToWip', CStrings::strToBool( $boolPostToWip ) );
	}

	public function getPostToWip() {
		return $this->m_boolPostToWip;
	}

	public function sqlPostToWip() {
		return ( true == isset( $this->m_boolPostToWip ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostToWip ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->set( 'm_boolIsReserved', CStrings::strToBool( $boolIsReserved ) );
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_boolIsReserved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReserved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setServiceLifeMonths( $intServiceLifeMonths ) {
		$this->set( 'm_intServiceLifeMonths', CStrings::strToIntDef( $intServiceLifeMonths, NULL, false ) );
	}

	public function getServiceLifeMonths() {
		return $this->m_intServiceLifeMonths;
	}

	public function sqlServiceLifeMonths() {
		return ( true == isset( $this->m_intServiceLifeMonths ) ) ? ( string ) $this->m_intServiceLifeMonths : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_ap_code_id, ap_code_type_id, ap_gl_account_id, item_gl_account_id, wip_gl_account_id, consumption_gl_account_id, ar_code_id, job_ap_code_id, asset_type_id, ap_code_category_id, unit_of_measure_id, depreciation_category_id, integration_database_id, reference_id, remote_primary_key, name, item_number, description, default_amount, order_num, post_to_wip, is_reserved, is_system, is_disabled, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, competitor_id, ps_product_id, service_life_months )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultApCodeId() . ', ' .
						$this->sqlApCodeTypeId() . ', ' .
						$this->sqlApGlAccountId() . ', ' .
						$this->sqlItemGlAccountId() . ', ' .
						$this->sqlWipGlAccountId() . ', ' .
						$this->sqlConsumptionGlAccountId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlJobApCodeId() . ', ' .
						$this->sqlAssetTypeId() . ', ' .
						$this->sqlApCodeCategoryId() . ', ' .
						$this->sqlUnitOfMeasureId() . ', ' .
						$this->sqlDepreciationCategoryId() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlItemNumber() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDefaultAmount() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlPostToWip() . ', ' .
						$this->sqlIsReserved() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlServiceLifeMonths() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_ap_code_id = ' . $this->sqlDefaultApCodeId(). ',' ; } elseif( true == array_key_exists( 'DefaultApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' default_ap_code_id = ' . $this->sqlDefaultApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_gl_account_id = ' . $this->sqlItemGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ItemGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' item_gl_account_id = ' . $this->sqlItemGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wip_gl_account_id = ' . $this->sqlWipGlAccountId(). ',' ; } elseif( true == array_key_exists( 'WipGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' wip_gl_account_id = ' . $this->sqlWipGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_gl_account_id = ' . $this->sqlConsumptionGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ConsumptionGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' consumption_gl_account_id = ' . $this->sqlConsumptionGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_ap_code_id = ' . $this->sqlJobApCodeId(). ',' ; } elseif( true == array_key_exists( 'JobApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' job_ap_code_id = ' . $this->sqlJobApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_type_id = ' . $this->sqlAssetTypeId(). ',' ; } elseif( true == array_key_exists( 'AssetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' asset_type_id = ' . $this->sqlAssetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_category_id = ' . $this->sqlApCodeCategoryId(). ',' ; } elseif( true == array_key_exists( 'ApCodeCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_category_id = ' . $this->sqlApCodeCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId(). ',' ; } elseif( true == array_key_exists( 'UnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' depreciation_category_id = ' . $this->sqlDepreciationCategoryId(). ',' ; } elseif( true == array_key_exists( 'DepreciationCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' depreciation_category_id = ' . $this->sqlDepreciationCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_number = ' . $this->sqlItemNumber(). ',' ; } elseif( true == array_key_exists( 'ItemNumber', $this->getChangedColumns() ) ) { $strSql .= ' item_number = ' . $this->sqlItemNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount(). ',' ; } elseif( true == array_key_exists( 'DefaultAmount', $this->getChangedColumns() ) ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_to_wip = ' . $this->sqlPostToWip(). ',' ; } elseif( true == array_key_exists( 'PostToWip', $this->getChangedColumns() ) ) { $strSql .= ' post_to_wip = ' . $this->sqlPostToWip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved(). ',' ; } elseif( true == array_key_exists( 'IsReserved', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_life_months = ' . $this->sqlServiceLifeMonths(). ',' ; } elseif( true == array_key_exists( 'ServiceLifeMonths', $this->getChangedColumns() ) ) { $strSql .= ' service_life_months = ' . $this->sqlServiceLifeMonths() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_ap_code_id' => $this->getDefaultApCodeId(),
			'ap_code_type_id' => $this->getApCodeTypeId(),
			'ap_gl_account_id' => $this->getApGlAccountId(),
			'item_gl_account_id' => $this->getItemGlAccountId(),
			'wip_gl_account_id' => $this->getWipGlAccountId(),
			'consumption_gl_account_id' => $this->getConsumptionGlAccountId(),
			'ar_code_id' => $this->getArCodeId(),
			'job_ap_code_id' => $this->getJobApCodeId(),
			'asset_type_id' => $this->getAssetTypeId(),
			'ap_code_category_id' => $this->getApCodeCategoryId(),
			'unit_of_measure_id' => $this->getUnitOfMeasureId(),
			'depreciation_category_id' => $this->getDepreciationCategoryId(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'reference_id' => $this->getReferenceId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name' => $this->getName(),
			'item_number' => $this->getItemNumber(),
			'description' => $this->getDescription(),
			'default_amount' => $this->getDefaultAmount(),
			'order_num' => $this->getOrderNum(),
			'post_to_wip' => $this->getPostToWip(),
			'is_reserved' => $this->getIsReserved(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'competitor_id' => $this->getCompetitorId(),
			'ps_product_id' => $this->getPsProductId(),
			'service_life_months' => $this->getServiceLifeMonths()
		);
	}

}
?>