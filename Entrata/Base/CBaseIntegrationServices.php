<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationServices
 * Do not add any new functions to this class.
 */

class CBaseIntegrationServices extends CEosPluralBase {

	/**
	 * @return CIntegrationService[]
	 */
	public static function fetchIntegrationServices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationService::class, $objDatabase );
	}

	/**
	 * @return CIntegrationService
	 */
	public static function fetchIntegrationService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationService::class, $objDatabase );
	}

	public static function fetchIntegrationServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_services', $objDatabase );
	}

	public static function fetchIntegrationServiceById( $intId, $objDatabase ) {
		return self::fetchIntegrationService( sprintf( 'SELECT * FROM integration_services WHERE id = %d', $intId ), $objDatabase );
	}

}
?>