<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeRuleTypes
 * Do not add any new functions to this class.
 */

class CBaseFeeRuleTypes extends CEosPluralBase {

	/**
	 * @return CFeeRuleType[]
	 */
	public static function fetchFeeRuleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFeeRuleType::class, $objDatabase );
	}

	/**
	 * @return CFeeRuleType
	 */
	public static function fetchFeeRuleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeRuleType::class, $objDatabase );
	}

	public static function fetchFeeRuleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_rule_types', $objDatabase );
	}

	public static function fetchFeeRuleTypeById( $intId, $objDatabase ) {
		return self::fetchFeeRuleType( sprintf( 'SELECT * FROM fee_rule_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>