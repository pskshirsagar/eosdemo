<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReviewResponseTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReviewResponseTemplates extends CEosPluralBase {

	/**
	 * @return CReviewResponseTemplate[]
	 */
	public static function fetchReviewResponseTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReviewResponseTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReviewResponseTemplate
	 */
	public static function fetchReviewResponseTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReviewResponseTemplate::class, $objDatabase );
	}

	public static function fetchReviewResponseTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'review_response_templates', $objDatabase );
	}

	public static function fetchReviewResponseTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReviewResponseTemplate( sprintf( 'SELECT * FROM review_response_templates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReviewResponseTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchReviewResponseTemplates( sprintf( 'SELECT * FROM review_response_templates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>