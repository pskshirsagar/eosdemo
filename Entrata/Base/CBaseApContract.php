<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApContract extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_contracts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApContractStatusId;
	protected $m_intProposalId;
	protected $m_intJobId;
	protected $m_intBudgetSummaryApHeaderId;
	protected $m_boolIsAddedToJobBudget;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strOriginalStartDate;
	protected $m_strOriginalEndDate;
	protected $m_strActualStartDate;
	protected $m_strActualEndDate;
	protected $m_strBidExpirationDate;
	protected $m_fltRetentionPercent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAddedToJobBudget = false;
		$this->m_fltRetentionPercent = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_contract_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApContractStatusId', trim( $arrValues['ap_contract_status_id'] ) ); elseif( isset( $arrValues['ap_contract_status_id'] ) ) $this->setApContractStatusId( $arrValues['ap_contract_status_id'] );
		if( isset( $arrValues['proposal_id'] ) && $boolDirectSet ) $this->set( 'm_intProposalId', trim( $arrValues['proposal_id'] ) ); elseif( isset( $arrValues['proposal_id'] ) ) $this->setProposalId( $arrValues['proposal_id'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['budget_summary_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetSummaryApHeaderId', trim( $arrValues['budget_summary_ap_header_id'] ) ); elseif( isset( $arrValues['budget_summary_ap_header_id'] ) ) $this->setBudgetSummaryApHeaderId( $arrValues['budget_summary_ap_header_id'] );
		if( isset( $arrValues['is_added_to_job_budget'] ) && $boolDirectSet ) $this->set( 'm_boolIsAddedToJobBudget', trim( stripcslashes( $arrValues['is_added_to_job_budget'] ) ) ); elseif( isset( $arrValues['is_added_to_job_budget'] ) ) $this->setIsAddedToJobBudget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_added_to_job_budget'] ) : $arrValues['is_added_to_job_budget'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['original_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalStartDate', trim( $arrValues['original_start_date'] ) ); elseif( isset( $arrValues['original_start_date'] ) ) $this->setOriginalStartDate( $arrValues['original_start_date'] );
		if( isset( $arrValues['original_end_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalEndDate', trim( $arrValues['original_end_date'] ) ); elseif( isset( $arrValues['original_end_date'] ) ) $this->setOriginalEndDate( $arrValues['original_end_date'] );
		if( isset( $arrValues['actual_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDate', trim( $arrValues['actual_start_date'] ) ); elseif( isset( $arrValues['actual_start_date'] ) ) $this->setActualStartDate( $arrValues['actual_start_date'] );
		if( isset( $arrValues['actual_end_date'] ) && $boolDirectSet ) $this->set( 'm_strActualEndDate', trim( $arrValues['actual_end_date'] ) ); elseif( isset( $arrValues['actual_end_date'] ) ) $this->setActualEndDate( $arrValues['actual_end_date'] );
		if( isset( $arrValues['bid_expiration_date'] ) && $boolDirectSet ) $this->set( 'm_strBidExpirationDate', trim( $arrValues['bid_expiration_date'] ) ); elseif( isset( $arrValues['bid_expiration_date'] ) ) $this->setBidExpirationDate( $arrValues['bid_expiration_date'] );
		if( isset( $arrValues['retention_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRetentionPercent', trim( $arrValues['retention_percent'] ) ); elseif( isset( $arrValues['retention_percent'] ) ) $this->setRetentionPercent( $arrValues['retention_percent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApContractStatusId( $intApContractStatusId ) {
		$this->set( 'm_intApContractStatusId', CStrings::strToIntDef( $intApContractStatusId, NULL, false ) );
	}

	public function getApContractStatusId() {
		return $this->m_intApContractStatusId;
	}

	public function sqlApContractStatusId() {
		return ( true == isset( $this->m_intApContractStatusId ) ) ? ( string ) $this->m_intApContractStatusId : 'NULL';
	}

	public function setProposalId( $intProposalId ) {
		$this->set( 'm_intProposalId', CStrings::strToIntDef( $intProposalId, NULL, false ) );
	}

	public function getProposalId() {
		return $this->m_intProposalId;
	}

	public function sqlProposalId() {
		return ( true == isset( $this->m_intProposalId ) ) ? ( string ) $this->m_intProposalId : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setBudgetSummaryApHeaderId( $intBudgetSummaryApHeaderId ) {
		$this->set( 'm_intBudgetSummaryApHeaderId', CStrings::strToIntDef( $intBudgetSummaryApHeaderId, NULL, false ) );
	}

	public function getBudgetSummaryApHeaderId() {
		return $this->m_intBudgetSummaryApHeaderId;
	}

	public function sqlBudgetSummaryApHeaderId() {
		return ( true == isset( $this->m_intBudgetSummaryApHeaderId ) ) ? ( string ) $this->m_intBudgetSummaryApHeaderId : 'NULL';
	}

	public function setIsAddedToJobBudget( $boolIsAddedToJobBudget ) {
		$this->set( 'm_boolIsAddedToJobBudget', CStrings::strToBool( $boolIsAddedToJobBudget ) );
	}

	public function getIsAddedToJobBudget() {
		return $this->m_boolIsAddedToJobBudget;
	}

	public function sqlIsAddedToJobBudget() {
		return ( true == isset( $this->m_boolIsAddedToJobBudget ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAddedToJobBudget ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setOriginalStartDate( $strOriginalStartDate ) {
		$this->set( 'm_strOriginalStartDate', CStrings::strTrimDef( $strOriginalStartDate, -1, NULL, true ) );
	}

	public function getOriginalStartDate() {
		return $this->m_strOriginalStartDate;
	}

	public function sqlOriginalStartDate() {
		return ( true == isset( $this->m_strOriginalStartDate ) ) ? '\'' . $this->m_strOriginalStartDate . '\'' : 'NOW()';
	}

	public function setOriginalEndDate( $strOriginalEndDate ) {
		$this->set( 'm_strOriginalEndDate', CStrings::strTrimDef( $strOriginalEndDate, -1, NULL, true ) );
	}

	public function getOriginalEndDate() {
		return $this->m_strOriginalEndDate;
	}

	public function sqlOriginalEndDate() {
		return ( true == isset( $this->m_strOriginalEndDate ) ) ? '\'' . $this->m_strOriginalEndDate . '\'' : 'NOW()';
	}

	public function setActualStartDate( $strActualStartDate ) {
		$this->set( 'm_strActualStartDate', CStrings::strTrimDef( $strActualStartDate, -1, NULL, true ) );
	}

	public function getActualStartDate() {
		return $this->m_strActualStartDate;
	}

	public function sqlActualStartDate() {
		return ( true == isset( $this->m_strActualStartDate ) ) ? '\'' . $this->m_strActualStartDate . '\'' : 'NULL';
	}

	public function setActualEndDate( $strActualEndDate ) {
		$this->set( 'm_strActualEndDate', CStrings::strTrimDef( $strActualEndDate, -1, NULL, true ) );
	}

	public function getActualEndDate() {
		return $this->m_strActualEndDate;
	}

	public function sqlActualEndDate() {
		return ( true == isset( $this->m_strActualEndDate ) ) ? '\'' . $this->m_strActualEndDate . '\'' : 'NULL';
	}

	public function setBidExpirationDate( $strBidExpirationDate ) {
		$this->set( 'm_strBidExpirationDate', CStrings::strTrimDef( $strBidExpirationDate, -1, NULL, true ) );
	}

	public function getBidExpirationDate() {
		return $this->m_strBidExpirationDate;
	}

	public function sqlBidExpirationDate() {
		return ( true == isset( $this->m_strBidExpirationDate ) ) ? '\'' . $this->m_strBidExpirationDate . '\'' : 'NULL';
	}

	public function setRetentionPercent( $fltRetentionPercent ) {
		$this->set( 'm_fltRetentionPercent', CStrings::strToFloatDef( $fltRetentionPercent, NULL, false, 4 ) );
	}

	public function getRetentionPercent() {
		return $this->m_fltRetentionPercent;
	}

	public function sqlRetentionPercent() {
		return ( true == isset( $this->m_fltRetentionPercent ) ) ? ( string ) $this->m_fltRetentionPercent : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_contract_status_id, proposal_id, job_id, budget_summary_ap_header_id, is_added_to_job_budget, name, description, original_start_date, original_end_date, actual_start_date, actual_end_date, bid_expiration_date, retention_percent, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApContractStatusId() . ', ' .
 						$this->sqlProposalId() . ', ' .
 						$this->sqlJobId() . ', ' .
 						$this->sqlBudgetSummaryApHeaderId() . ', ' .
 						$this->sqlIsAddedToJobBudget() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlOriginalStartDate() . ', ' .
 						$this->sqlOriginalEndDate() . ', ' .
 						$this->sqlActualStartDate() . ', ' .
 						$this->sqlActualEndDate() . ', ' .
 						$this->sqlBidExpirationDate() . ', ' .
 						$this->sqlRetentionPercent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_status_id = ' . $this->sqlApContractStatusId() . ','; } elseif( true == array_key_exists( 'ApContractStatusId', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_status_id = ' . $this->sqlApContractStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_id = ' . $this->sqlProposalId() . ','; } elseif( true == array_key_exists( 'ProposalId', $this->getChangedColumns() ) ) { $strSql .= ' proposal_id = ' . $this->sqlProposalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_summary_ap_header_id = ' . $this->sqlBudgetSummaryApHeaderId() . ','; } elseif( true == array_key_exists( 'BudgetSummaryApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' budget_summary_ap_header_id = ' . $this->sqlBudgetSummaryApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_added_to_job_budget = ' . $this->sqlIsAddedToJobBudget() . ','; } elseif( true == array_key_exists( 'IsAddedToJobBudget', $this->getChangedColumns() ) ) { $strSql .= ' is_added_to_job_budget = ' . $this->sqlIsAddedToJobBudget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate() . ','; } elseif( true == array_key_exists( 'OriginalStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate() . ','; } elseif( true == array_key_exists( 'OriginalEndDate', $this->getChangedColumns() ) ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; } elseif( true == array_key_exists( 'ActualStartDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate() . ','; } elseif( true == array_key_exists( 'ActualEndDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bid_expiration_date = ' . $this->sqlBidExpirationDate() . ','; } elseif( true == array_key_exists( 'BidExpirationDate', $this->getChangedColumns() ) ) { $strSql .= ' bid_expiration_date = ' . $this->sqlBidExpirationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retention_percent = ' . $this->sqlRetentionPercent() . ','; } elseif( true == array_key_exists( 'RetentionPercent', $this->getChangedColumns() ) ) { $strSql .= ' retention_percent = ' . $this->sqlRetentionPercent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_contract_status_id' => $this->getApContractStatusId(),
			'proposal_id' => $this->getProposalId(),
			'job_id' => $this->getJobId(),
			'budget_summary_ap_header_id' => $this->getBudgetSummaryApHeaderId(),
			'is_added_to_job_budget' => $this->getIsAddedToJobBudget(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'original_start_date' => $this->getOriginalStartDate(),
			'original_end_date' => $this->getOriginalEndDate(),
			'actual_start_date' => $this->getActualStartDate(),
			'actual_end_date' => $this->getActualEndDate(),
			'bid_expiration_date' => $this->getBidExpirationDate(),
			'retention_percent' => $this->getRetentionPercent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>