<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyProject extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.subsidy_projects';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyTypeId;
	protected $m_intSubsidyProgramTypeId;
	protected $m_intSubsidyProgramSubTypeId;
	protected $m_strProjectName;
	protected $m_strProjectNumber;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_boolIsFederal;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strStartDate = '01/01/1970';
		$this->m_strEndDate = '12/31/2099';
		$this->m_boolIsFederal = true;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTypeId', trim( $arrValues['subsidy_type_id'] ) ); elseif( isset( $arrValues['subsidy_type_id'] ) ) $this->setSubsidyTypeId( $arrValues['subsidy_type_id'] );
		if( isset( $arrValues['subsidy_program_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyProgramTypeId', trim( $arrValues['subsidy_program_type_id'] ) ); elseif( isset( $arrValues['subsidy_program_type_id'] ) ) $this->setSubsidyProgramTypeId( $arrValues['subsidy_program_type_id'] );
		if( isset( $arrValues['subsidy_program_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyProgramSubTypeId', trim( $arrValues['subsidy_program_sub_type_id'] ) ); elseif( isset( $arrValues['subsidy_program_sub_type_id'] ) ) $this->setSubsidyProgramSubTypeId( $arrValues['subsidy_program_sub_type_id'] );
		if( isset( $arrValues['project_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strProjectName', trim( $arrValues['project_name'] ) ); elseif( isset( $arrValues['project_name'] ) ) $this->setProjectName( $arrValues['project_name'] );
		if( isset( $arrValues['project_number'] ) && $boolDirectSet ) $this->set( 'm_strProjectNumber', trim( $arrValues['project_number'] ) ); elseif( isset( $arrValues['project_number'] ) ) $this->setProjectNumber( $arrValues['project_number'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_federal'] ) && $boolDirectSet ) $this->set( 'm_boolIsFederal', trim( stripcslashes( $arrValues['is_federal'] ) ) ); elseif( isset( $arrValues['is_federal'] ) ) $this->setIsFederal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_federal'] ) : $arrValues['is_federal'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyTypeId( $intSubsidyTypeId ) {
		$this->set( 'm_intSubsidyTypeId', CStrings::strToIntDef( $intSubsidyTypeId, NULL, false ) );
	}

	public function getSubsidyTypeId() {
		return $this->m_intSubsidyTypeId;
	}

	public function sqlSubsidyTypeId() {
		return ( true == isset( $this->m_intSubsidyTypeId ) ) ? ( string ) $this->m_intSubsidyTypeId : 'NULL';
	}

	public function setSubsidyProgramTypeId( $intSubsidyProgramTypeId ) {
		$this->set( 'm_intSubsidyProgramTypeId', CStrings::strToIntDef( $intSubsidyProgramTypeId, NULL, false ) );
	}

	public function getSubsidyProgramTypeId() {
		return $this->m_intSubsidyProgramTypeId;
	}

	public function sqlSubsidyProgramTypeId() {
		return ( true == isset( $this->m_intSubsidyProgramTypeId ) ) ? ( string ) $this->m_intSubsidyProgramTypeId : 'NULL';
	}

	public function setSubsidyProgramSubTypeId( $intSubsidyProgramSubTypeId ) {
		$this->set( 'm_intSubsidyProgramSubTypeId', CStrings::strToIntDef( $intSubsidyProgramSubTypeId, NULL, false ) );
	}

	public function getSubsidyProgramSubTypeId() {
		return $this->m_intSubsidyProgramSubTypeId;
	}

	public function sqlSubsidyProgramSubTypeId() {
		return ( true == isset( $this->m_intSubsidyProgramSubTypeId ) ) ? ( string ) $this->m_intSubsidyProgramSubTypeId : 'NULL';
	}

	public function setProjectName( $strProjectName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strProjectName', CStrings::strTrimDef( $strProjectName, 100, NULL, true ), $strLocaleCode );
	}

	public function getProjectName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strProjectName', $strLocaleCode );
	}

	public function sqlProjectName() {
		return ( true == isset( $this->m_strProjectName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProjectName ) : '\'' . addslashes( $this->m_strProjectName ) . '\'' ) : 'NULL';
	}

	public function setProjectNumber( $strProjectNumber ) {
		$this->set( 'm_strProjectNumber', CStrings::strTrimDef( $strProjectNumber, 50, NULL, true ) );
	}

	public function getProjectNumber() {
		return $this->m_strProjectNumber;
	}

	public function sqlProjectNumber() {
		return ( true == isset( $this->m_strProjectNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProjectNumber ) : '\'' . addslashes( $this->m_strProjectNumber ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function setIsFederal( $boolIsFederal ) {
		$this->set( 'm_boolIsFederal', CStrings::strToBool( $boolIsFederal ) );
	}

	public function getIsFederal() {
		return $this->m_boolIsFederal;
	}

	public function sqlIsFederal() {
		return ( true == isset( $this->m_boolIsFederal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFederal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDetails( $jsonDetails ) {
		if( true == valObj( $jsonDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDetails', $jsonDetails );
		} elseif( true == valJsonString( $jsonDetails ) ) {
			$this->set( 'm_jsonDetails', CStrings::strToJson( $jsonDetails ) );
		} else {
			$this->set( 'm_jsonDetails', NULL ); 
		}
		unset( $this->m_strDetails );
	}

	public function getDetails() {
		if( true == isset( $this->m_strDetails ) ) {
			$this->m_jsonDetails = CStrings::strToJson( $this->m_strDetails );
			unset( $this->m_strDetails );
		}
		$this->loadUserDefinedFields();
		return $this->m_jsonDetails;
	}

	public function sqlDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_type_id, subsidy_program_type_id, subsidy_program_sub_type_id, project_name, project_number, start_date, end_date, is_federal, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSubsidyTypeId() . ', ' .
						$this->sqlSubsidyProgramTypeId() . ', ' .
						$this->sqlSubsidyProgramSubTypeId() . ', ' .
						$this->sqlProjectName() . ', ' .
						$this->sqlProjectNumber() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlIsFederal() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_program_type_id = ' . $this->sqlSubsidyProgramTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyProgramTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_program_type_id = ' . $this->sqlSubsidyProgramTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_program_sub_type_id = ' . $this->sqlSubsidyProgramSubTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyProgramSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_program_sub_type_id = ' . $this->sqlSubsidyProgramSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_name = ' . $this->sqlProjectName(). ',' ; } elseif( true == array_key_exists( 'ProjectName', $this->getChangedColumns() ) ) { $strSql .= ' project_name = ' . $this->sqlProjectName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_number = ' . $this->sqlProjectNumber(). ',' ; } elseif( true == array_key_exists( 'ProjectNumber', $this->getChangedColumns() ) ) { $strSql .= ' project_number = ' . $this->sqlProjectNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_federal = ' . $this->sqlIsFederal(). ',' ; } elseif( true == array_key_exists( 'IsFederal', $this->getChangedColumns() ) ) { $strSql .= ' is_federal = ' . $this->sqlIsFederal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_type_id' => $this->getSubsidyTypeId(),
			'subsidy_program_type_id' => $this->getSubsidyProgramTypeId(),
			'subsidy_program_sub_type_id' => $this->getSubsidyProgramSubTypeId(),
			'project_name' => $this->getProjectName(),
			'project_number' => $this->getProjectNumber(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_federal' => $this->getIsFederal(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>