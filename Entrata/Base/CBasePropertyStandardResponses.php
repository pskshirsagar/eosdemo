<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyStandardResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyStandardResponses extends CEosPluralBase {

	/**
	 * @return CPropertyStandardResponse[]
	 */
	public static function fetchPropertyStandardResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyStandardResponse', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyStandardResponse
	 */
	public static function fetchPropertyStandardResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyStandardResponse', $objDatabase );
	}

	public static function fetchPropertyStandardResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_standard_responses', $objDatabase );
	}

	public static function fetchPropertyStandardResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyStandardResponse( sprintf( 'SELECT * FROM property_standard_responses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyStandardResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyStandardResponses( sprintf( 'SELECT * FROM property_standard_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyStandardResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyStandardResponses( sprintf( 'SELECT * FROM property_standard_responses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyStandardResponsesBySettingsTemplateIdByCid( $intSettingsTemplateId, $intCid, $objDatabase ) {
		return self::fetchPropertyStandardResponses( sprintf( 'SELECT * FROM property_standard_responses WHERE settings_template_id = %d AND cid = %d', ( int ) $intSettingsTemplateId, ( int ) $intCid ), $objDatabase );
	}

}
?>