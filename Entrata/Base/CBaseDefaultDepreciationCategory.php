<?php

class CBaseDefaultDepreciationCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.default_depreciation_categories';

	protected $m_intId;
	protected $m_intTaxRecoveryPeriodId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intUsefulLifeMonths;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_boolIsDepreciable;

	public function __construct() {
		parent::__construct();

		$this->m_intUsefulLifeMonths = '1';
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_boolIsDepreciable = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['tax_recovery_period_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRecoveryPeriodId', trim( $arrValues['tax_recovery_period_id'] ) ); elseif( isset( $arrValues['tax_recovery_period_id'] ) ) $this->setTaxRecoveryPeriodId( $arrValues['tax_recovery_period_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['useful_life_months'] ) && $boolDirectSet ) $this->set( 'm_intUsefulLifeMonths', trim( $arrValues['useful_life_months'] ) ); elseif( isset( $arrValues['useful_life_months'] ) ) $this->setUsefulLifeMonths( $arrValues['useful_life_months'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_depreciable'] ) && $boolDirectSet ) $this->set( 'm_boolIsDepreciable', trim( stripcslashes( $arrValues['is_depreciable'] ) ) ); elseif( isset( $arrValues['is_depreciable'] ) ) $this->setIsDepreciable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_depreciable'] ) : $arrValues['is_depreciable'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaxRecoveryPeriodId( $intTaxRecoveryPeriodId ) {
		$this->set( 'm_intTaxRecoveryPeriodId', CStrings::strToIntDef( $intTaxRecoveryPeriodId, NULL, false ) );
	}

	public function getTaxRecoveryPeriodId() {
		return $this->m_intTaxRecoveryPeriodId;
	}

	public function sqlTaxRecoveryPeriodId() {
		return ( true == isset( $this->m_intTaxRecoveryPeriodId ) ) ? ( string ) $this->m_intTaxRecoveryPeriodId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUsefulLifeMonths( $intUsefulLifeMonths ) {
		$this->set( 'm_intUsefulLifeMonths', CStrings::strToIntDef( $intUsefulLifeMonths, NULL, false ) );
	}

	public function getUsefulLifeMonths() {
		return $this->m_intUsefulLifeMonths;
	}

	public function sqlUsefulLifeMonths() {
		return ( true == isset( $this->m_intUsefulLifeMonths ) ) ? ( string ) $this->m_intUsefulLifeMonths : '1';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsDepreciable( $boolIsDepreciable ) {
		$this->set( 'm_boolIsDepreciable', CStrings::strToBool( $boolIsDepreciable ) );
	}

	public function getIsDepreciable() {
		return $this->m_boolIsDepreciable;
	}

	public function sqlIsDepreciable() {
		return ( true == isset( $this->m_boolIsDepreciable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDepreciable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'tax_recovery_period_id' => $this->getTaxRecoveryPeriodId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'useful_life_months' => $this->getUsefulLifeMonths(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'is_depreciable' => $this->getIsDepreciable()
		);
	}

}
?>