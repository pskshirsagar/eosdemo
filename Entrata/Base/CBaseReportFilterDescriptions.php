<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilterDescriptions
 * Do not add any new functions to this class.
 */

class CBaseReportFilterDescriptions extends CEosPluralBase {

	/**
	 * @return CReportFilterDescription[]
	 */
	public static function fetchReportFilterDescriptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportFilterDescription::class, $objDatabase );
	}

	/**
	 * @return CReportFilterDescription
	 */
	public static function fetchReportFilterDescription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportFilterDescription::class, $objDatabase );
	}

	public static function fetchReportFilterDescriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_filter_descriptions', $objDatabase );
	}

	public static function fetchReportFilterDescriptionById( $intId, $objDatabase ) {
		return self::fetchReportFilterDescription( sprintf( 'SELECT * FROM report_filter_descriptions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportFilterDescriptionsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchReportFilterDescriptions( sprintf( 'SELECT * FROM report_filter_descriptions WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

	public static function fetchReportFilterDescriptionsByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		return self::fetchReportFilterDescriptions( sprintf( 'SELECT * FROM report_filter_descriptions WHERE default_report_version_id = %d', ( int ) $intDefaultReportVersionId ), $objDatabase );
	}

}
?>