<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDocumentExportRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDocumentExportRequestStatusTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDocumentExportRequestStatusType[]
	 */
	public static function fetchScreeningDocumentExportRequestStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningDocumentExportRequestStatusType::class, $objDatabase );
	}

	/**
	 * @return CScreeningDocumentExportRequestStatusType
	 */
	public static function fetchScreeningDocumentExportRequestStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningDocumentExportRequestStatusType::class, $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_document_export_request_status_types', $objDatabase );
	}

	public static function fetchScreeningDocumentExportRequestStatusTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDocumentExportRequestStatusType( sprintf( 'SELECT * FROM screening_document_export_request_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>