<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialSaleBreakpoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialSaleBreakpoints extends CEosPluralBase {

	/**
	 * @return CCommercialSaleBreakpoint[]
	 */
	public static function fetchCommercialSaleBreakpoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCommercialSaleBreakpoint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialSaleBreakpoint
	 */
	public static function fetchCommercialSaleBreakpoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommercialSaleBreakpoint', $objDatabase );
	}

	public static function fetchCommercialSaleBreakpointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_sale_breakpoints', $objDatabase );
	}

	public static function fetchCommercialSaleBreakpointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialSaleBreakpoint( sprintf( 'SELECT * FROM commercial_sale_breakpoints WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSaleBreakpointsByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialSaleBreakpoints( sprintf( 'SELECT * FROM commercial_sale_breakpoints WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSaleBreakpointsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCommercialSaleBreakpoints( sprintf( 'SELECT * FROM commercial_sale_breakpoints WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSaleBreakpointsByThresholdArTriggerIdByCid( $intThresholdArTriggerId, $intCid, $objDatabase ) {
		return self::fetchCommercialSaleBreakpoints( sprintf( 'SELECT * FROM commercial_sale_breakpoints WHERE threshold_ar_trigger_id = %d AND cid = %d', ( int ) $intThresholdArTriggerId, ( int ) $intCid ), $objDatabase );
	}

}
?>