<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationContactTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationContactTypes extends CEosPluralBase {

	/**
	 * @return CIntegrationContactType[]
	 */
	public static function fetchIntegrationContactTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationContactType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationContactType
	 */
	public static function fetchIntegrationContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationContactType', $objDatabase );
	}

	public static function fetchIntegrationContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_contact_types', $objDatabase );
	}

	public static function fetchIntegrationContactTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationContactType( sprintf( 'SELECT * FROM integration_contact_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationContactTypesByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationContactTypes( sprintf( 'SELECT * FROM integration_contact_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationContactTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchIntegrationContactTypes( sprintf( 'SELECT * FROM integration_contact_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>