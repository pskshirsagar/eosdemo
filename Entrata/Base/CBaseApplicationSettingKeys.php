<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSettingKeys
 * Do not add any new functions to this class.
 */

class CBaseApplicationSettingKeys extends CEosPluralBase {

	/**
	 * @return CApplicationSettingKey[]
	 */
	public static function fetchApplicationSettingKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CApplicationSettingKey', $objDatabase );
	}

	/**
	 * @return CApplicationSettingKey
	 */
	public static function fetchApplicationSettingKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationSettingKey', $objDatabase );
	}

	public static function fetchApplicationSettingKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_setting_keys', $objDatabase );
	}

	public static function fetchApplicationSettingKeyById( $intId, $objDatabase ) {
		return self::fetchApplicationSettingKey( sprintf( 'SELECT * FROM application_setting_keys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchApplicationSettingKeysByApplicationSettingGroupId( $intApplicationSettingGroupId, $objDatabase ) {
		return self::fetchApplicationSettingKeys( sprintf( 'SELECT * FROM application_setting_keys WHERE application_setting_group_id = %d', ( int ) $intApplicationSettingGroupId ), $objDatabase );
	}

	public static function fetchApplicationSettingKeysByOccupancyTypeId( $intOccupancyTypeId, $objDatabase ) {
		return self::fetchApplicationSettingKeys( sprintf( 'SELECT * FROM application_setting_keys WHERE occupancy_type_id = %d', ( int ) $intOccupancyTypeId ), $objDatabase );
	}

	public static function fetchApplicationSettingKeysByToolTipId( $intToolTipId, $objDatabase ) {
		return self::fetchApplicationSettingKeys( sprintf( 'SELECT * FROM application_setting_keys WHERE tool_tip_id = %d', ( int ) $intToolTipId ), $objDatabase );
	}

}
?>