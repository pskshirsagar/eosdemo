<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobs extends CEosPluralBase {

	/**
	 * @return CJob[]
	 */
	public static function fetchJobs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJob::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJob
	 */
	public static function fetchJob( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJob::class, $objDatabase );
	}

	public static function fetchJobCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'jobs', $objDatabase );
	}

	public static function fetchJobByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJob( sprintf( 'SELECT * FROM jobs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchJobsByCid( $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchJobsByJobTypeIdByCid( $intJobTypeId, $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE job_type_id = %d AND cid = %d', $intJobTypeId, $intCid ), $objDatabase );
	}

	public static function fetchJobsByJobStatusIdByCid( $intJobStatusId, $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE job_status_id = %d AND cid = %d', $intJobStatusId, $intCid ), $objDatabase );
	}

	public static function fetchJobsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchJobsByBudgetSummaryApHeaderIdByCid( $intBudgetSummaryApHeaderId, $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE budget_summary_ap_header_id = %d AND cid = %d', $intBudgetSummaryApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchJobsByJobCategoryIdByCid( $intJobCategoryId, $intCid, $objDatabase ) {
		return self::fetchJobs( sprintf( 'SELECT * FROM jobs WHERE job_category_id = %d AND cid = %d', $intJobCategoryId, $intCid ), $objDatabase );
	}

}
?>