<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultBadges
 * Do not add any new functions to this class.
 */

class CBaseDefaultBadges extends CEosPluralBase {

	/**
	 * @return CDefaultBadge[]
	 */
	public static function fetchDefaultBadges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultBadge::class, $objDatabase );
	}

	/**
	 * @return CDefaultBadge
	 */
	public static function fetchDefaultBadge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultBadge::class, $objDatabase );
	}

	public static function fetchDefaultBadgeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_badges', $objDatabase );
	}

	public static function fetchDefaultBadgeById( $intId, $objDatabase ) {
		return self::fetchDefaultBadge( sprintf( 'SELECT * FROM default_badges WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultBadgesByDefaultActivityPointId( $intDefaultActivityPointId, $objDatabase ) {
		return self::fetchDefaultBadges( sprintf( 'SELECT * FROM default_badges WHERE default_activity_point_id = %d', $intDefaultActivityPointId ), $objDatabase );
	}

}
?>