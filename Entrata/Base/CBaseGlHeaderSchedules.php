<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderSchedules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeaderSchedules extends CEosPluralBase {

	/**
	 * @return CGlHeaderSchedule[]
	 */
	public static function fetchGlHeaderSchedules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlHeaderSchedule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlHeaderSchedule
	 */
	public static function fetchGlHeaderSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlHeaderSchedule', $objDatabase );
	}

	public static function fetchGlHeaderScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_schedules', $objDatabase );
	}

	public static function fetchGlHeaderScheduleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderSchedule( sprintf( 'SELECT * FROM gl_header_schedules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderSchedulesByCid( $intCid, $objDatabase ) {
		return self::fetchGlHeaderSchedules( sprintf( 'SELECT * FROM gl_header_schedules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderSchedulesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderSchedules( sprintf( 'SELECT * FROM gl_header_schedules WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>