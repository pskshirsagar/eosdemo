<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyNumber extends CEosSingularBase {

	const TABLE_NAME = 'public.company_numbers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerNumber;
	protected $m_intLeaseNumber;
	protected $m_intPropertyNumber;
	protected $m_intArDepositNumber;
	protected $m_intArBatchNumber;
	protected $m_intApBatchNumber;
	protected $m_intGlHeaderNumber;
	protected $m_intGlHeaderScheduleNumber;
	protected $m_intScheduledApTransactionHeaderNumber;
	protected $m_intPoNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_number'] ) && $boolDirectSet ) $this->set( 'm_intCustomerNumber', trim( $arrValues['customer_number'] ) ); elseif( isset( $arrValues['customer_number'] ) ) $this->setCustomerNumber( $arrValues['customer_number'] );
		if( isset( $arrValues['lease_number'] ) && $boolDirectSet ) $this->set( 'm_intLeaseNumber', trim( $arrValues['lease_number'] ) ); elseif( isset( $arrValues['lease_number'] ) ) $this->setLeaseNumber( $arrValues['lease_number'] );
		if( isset( $arrValues['property_number'] ) && $boolDirectSet ) $this->set( 'm_intPropertyNumber', trim( $arrValues['property_number'] ) ); elseif( isset( $arrValues['property_number'] ) ) $this->setPropertyNumber( $arrValues['property_number'] );
		if( isset( $arrValues['ar_deposit_number'] ) && $boolDirectSet ) $this->set( 'm_intArDepositNumber', trim( $arrValues['ar_deposit_number'] ) ); elseif( isset( $arrValues['ar_deposit_number'] ) ) $this->setArDepositNumber( $arrValues['ar_deposit_number'] );
		if( isset( $arrValues['ar_batch_number'] ) && $boolDirectSet ) $this->set( 'm_intArBatchNumber', trim( $arrValues['ar_batch_number'] ) ); elseif( isset( $arrValues['ar_batch_number'] ) ) $this->setArBatchNumber( $arrValues['ar_batch_number'] );
		if( isset( $arrValues['ap_batch_number'] ) && $boolDirectSet ) $this->set( 'm_intApBatchNumber', trim( $arrValues['ap_batch_number'] ) ); elseif( isset( $arrValues['ap_batch_number'] ) ) $this->setApBatchNumber( $arrValues['ap_batch_number'] );
		if( isset( $arrValues['gl_header_number'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderNumber', trim( $arrValues['gl_header_number'] ) ); elseif( isset( $arrValues['gl_header_number'] ) ) $this->setGlHeaderNumber( $arrValues['gl_header_number'] );
		if( isset( $arrValues['gl_header_schedule_number'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderScheduleNumber', trim( $arrValues['gl_header_schedule_number'] ) ); elseif( isset( $arrValues['gl_header_schedule_number'] ) ) $this->setGlHeaderScheduleNumber( $arrValues['gl_header_schedule_number'] );
		if( isset( $arrValues['scheduled_ap_transaction_header_number'] ) && $boolDirectSet ) $this->set( 'm_intScheduledApTransactionHeaderNumber', trim( $arrValues['scheduled_ap_transaction_header_number'] ) ); elseif( isset( $arrValues['scheduled_ap_transaction_header_number'] ) ) $this->setScheduledApTransactionHeaderNumber( $arrValues['scheduled_ap_transaction_header_number'] );
		if( isset( $arrValues['po_number'] ) && $boolDirectSet ) $this->set( 'm_intPoNumber', trim( $arrValues['po_number'] ) ); elseif( isset( $arrValues['po_number'] ) ) $this->setPoNumber( $arrValues['po_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerNumber( $intCustomerNumber ) {
		$this->set( 'm_intCustomerNumber', CStrings::strToIntDef( $intCustomerNumber, NULL, false ) );
	}

	public function getCustomerNumber() {
		return $this->m_intCustomerNumber;
	}

	public function sqlCustomerNumber() {
		return ( true == isset( $this->m_intCustomerNumber ) ) ? ( string ) $this->m_intCustomerNumber : 'NULL';
	}

	public function setLeaseNumber( $intLeaseNumber ) {
		$this->set( 'm_intLeaseNumber', CStrings::strToIntDef( $intLeaseNumber, NULL, false ) );
	}

	public function getLeaseNumber() {
		return $this->m_intLeaseNumber;
	}

	public function sqlLeaseNumber() {
		return ( true == isset( $this->m_intLeaseNumber ) ) ? ( string ) $this->m_intLeaseNumber : 'NULL';
	}

	public function setPropertyNumber( $intPropertyNumber ) {
		$this->set( 'm_intPropertyNumber', CStrings::strToIntDef( $intPropertyNumber, NULL, false ) );
	}

	public function getPropertyNumber() {
		return $this->m_intPropertyNumber;
	}

	public function sqlPropertyNumber() {
		return ( true == isset( $this->m_intPropertyNumber ) ) ? ( string ) $this->m_intPropertyNumber : 'NULL';
	}

	public function setArDepositNumber( $intArDepositNumber ) {
		$this->set( 'm_intArDepositNumber', CStrings::strToIntDef( $intArDepositNumber, NULL, false ) );
	}

	public function getArDepositNumber() {
		return $this->m_intArDepositNumber;
	}

	public function sqlArDepositNumber() {
		return ( true == isset( $this->m_intArDepositNumber ) ) ? ( string ) $this->m_intArDepositNumber : 'NULL';
	}

	public function setArBatchNumber( $intArBatchNumber ) {
		$this->set( 'm_intArBatchNumber', CStrings::strToIntDef( $intArBatchNumber, NULL, false ) );
	}

	public function getArBatchNumber() {
		return $this->m_intArBatchNumber;
	}

	public function sqlArBatchNumber() {
		return ( true == isset( $this->m_intArBatchNumber ) ) ? ( string ) $this->m_intArBatchNumber : 'NULL';
	}

	public function setApBatchNumber( $intApBatchNumber ) {
		$this->set( 'm_intApBatchNumber', CStrings::strToIntDef( $intApBatchNumber, NULL, false ) );
	}

	public function getApBatchNumber() {
		return $this->m_intApBatchNumber;
	}

	public function sqlApBatchNumber() {
		return ( true == isset( $this->m_intApBatchNumber ) ) ? ( string ) $this->m_intApBatchNumber : 'NULL';
	}

	public function setGlHeaderNumber( $intGlHeaderNumber ) {
		$this->set( 'm_intGlHeaderNumber', CStrings::strToIntDef( $intGlHeaderNumber, NULL, false ) );
	}

	public function getGlHeaderNumber() {
		return $this->m_intGlHeaderNumber;
	}

	public function sqlGlHeaderNumber() {
		return ( true == isset( $this->m_intGlHeaderNumber ) ) ? ( string ) $this->m_intGlHeaderNumber : 'NULL';
	}

	public function setGlHeaderScheduleNumber( $intGlHeaderScheduleNumber ) {
		$this->set( 'm_intGlHeaderScheduleNumber', CStrings::strToIntDef( $intGlHeaderScheduleNumber, NULL, false ) );
	}

	public function getGlHeaderScheduleNumber() {
		return $this->m_intGlHeaderScheduleNumber;
	}

	public function sqlGlHeaderScheduleNumber() {
		return ( true == isset( $this->m_intGlHeaderScheduleNumber ) ) ? ( string ) $this->m_intGlHeaderScheduleNumber : 'NULL';
	}

	public function setScheduledApTransactionHeaderNumber( $intScheduledApTransactionHeaderNumber ) {
		$this->set( 'm_intScheduledApTransactionHeaderNumber', CStrings::strToIntDef( $intScheduledApTransactionHeaderNumber, NULL, false ) );
	}

	public function getScheduledApTransactionHeaderNumber() {
		return $this->m_intScheduledApTransactionHeaderNumber;
	}

	public function sqlScheduledApTransactionHeaderNumber() {
		return ( true == isset( $this->m_intScheduledApTransactionHeaderNumber ) ) ? ( string ) $this->m_intScheduledApTransactionHeaderNumber : 'NULL';
	}

	public function setPoNumber( $intPoNumber ) {
		$this->set( 'm_intPoNumber', CStrings::strToIntDef( $intPoNumber, NULL, false ) );
	}

	public function getPoNumber() {
		return $this->m_intPoNumber;
	}

	public function sqlPoNumber() {
		return ( true == isset( $this->m_intPoNumber ) ) ? ( string ) $this->m_intPoNumber : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_number, lease_number, property_number, ar_deposit_number, ar_batch_number, ap_batch_number, gl_header_number, gl_header_schedule_number, scheduled_ap_transaction_header_number, po_number, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerNumber() . ', ' .
 						$this->sqlLeaseNumber() . ', ' .
 						$this->sqlPropertyNumber() . ', ' .
 						$this->sqlArDepositNumber() . ', ' .
 						$this->sqlArBatchNumber() . ', ' .
 						$this->sqlApBatchNumber() . ', ' .
 						$this->sqlGlHeaderNumber() . ', ' .
 						$this->sqlGlHeaderScheduleNumber() . ', ' .
 						$this->sqlScheduledApTransactionHeaderNumber() . ', ' .
 						$this->sqlPoNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_number = ' . $this->sqlCustomerNumber() . ','; } elseif( true == array_key_exists( 'CustomerNumber', $this->getChangedColumns() ) ) { $strSql .= ' customer_number = ' . $this->sqlCustomerNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_number = ' . $this->sqlLeaseNumber() . ','; } elseif( true == array_key_exists( 'LeaseNumber', $this->getChangedColumns() ) ) { $strSql .= ' lease_number = ' . $this->sqlLeaseNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_number = ' . $this->sqlPropertyNumber() . ','; } elseif( true == array_key_exists( 'PropertyNumber', $this->getChangedColumns() ) ) { $strSql .= ' property_number = ' . $this->sqlPropertyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_deposit_number = ' . $this->sqlArDepositNumber() . ','; } elseif( true == array_key_exists( 'ArDepositNumber', $this->getChangedColumns() ) ) { $strSql .= ' ar_deposit_number = ' . $this->sqlArDepositNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_batch_number = ' . $this->sqlArBatchNumber() . ','; } elseif( true == array_key_exists( 'ArBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' ar_batch_number = ' . $this->sqlArBatchNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_batch_number = ' . $this->sqlApBatchNumber() . ','; } elseif( true == array_key_exists( 'ApBatchNumber', $this->getChangedColumns() ) ) { $strSql .= ' ap_batch_number = ' . $this->sqlApBatchNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_number = ' . $this->sqlGlHeaderNumber() . ','; } elseif( true == array_key_exists( 'GlHeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_number = ' . $this->sqlGlHeaderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_schedule_number = ' . $this->sqlGlHeaderScheduleNumber() . ','; } elseif( true == array_key_exists( 'GlHeaderScheduleNumber', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_schedule_number = ' . $this->sqlGlHeaderScheduleNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_ap_transaction_header_number = ' . $this->sqlScheduledApTransactionHeaderNumber() . ','; } elseif( true == array_key_exists( 'ScheduledApTransactionHeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_ap_transaction_header_number = ' . $this->sqlScheduledApTransactionHeaderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_number = ' . $this->sqlPoNumber() . ','; } elseif( true == array_key_exists( 'PoNumber', $this->getChangedColumns() ) ) { $strSql .= ' po_number = ' . $this->sqlPoNumber() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_number' => $this->getCustomerNumber(),
			'lease_number' => $this->getLeaseNumber(),
			'property_number' => $this->getPropertyNumber(),
			'ar_deposit_number' => $this->getArDepositNumber(),
			'ar_batch_number' => $this->getArBatchNumber(),
			'ap_batch_number' => $this->getApBatchNumber(),
			'gl_header_number' => $this->getGlHeaderNumber(),
			'gl_header_schedule_number' => $this->getGlHeaderScheduleNumber(),
			'scheduled_ap_transaction_header_number' => $this->getScheduledApTransactionHeaderNumber(),
			'po_number' => $this->getPoNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>