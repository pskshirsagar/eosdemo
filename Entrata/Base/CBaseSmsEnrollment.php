<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSmsEnrollment extends CEosSingularBase {

	const TABLE_NAME = 'public.sms_enrollments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intMessageTypeId;
	protected $m_strEnrollmentDatetime;
	protected $m_strLastSentOn;
	protected $m_strNotifiedOn;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageTypeId', trim( $arrValues['message_type_id'] ) ); elseif( isset( $arrValues['message_type_id'] ) ) $this->setMessageTypeId( $arrValues['message_type_id'] );
		if( isset( $arrValues['enrollment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEnrollmentDatetime', trim( $arrValues['enrollment_datetime'] ) ); elseif( isset( $arrValues['enrollment_datetime'] ) ) $this->setEnrollmentDatetime( $arrValues['enrollment_datetime'] );
		if( isset( $arrValues['last_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSentOn', trim( $arrValues['last_sent_on'] ) ); elseif( isset( $arrValues['last_sent_on'] ) ) $this->setLastSentOn( $arrValues['last_sent_on'] );
		if( isset( $arrValues['notified_on'] ) && $boolDirectSet ) $this->set( 'm_strNotifiedOn', trim( $arrValues['notified_on'] ) ); elseif( isset( $arrValues['notified_on'] ) ) $this->setNotifiedOn( $arrValues['notified_on'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setMessageTypeId( $intMessageTypeId ) {
		$this->set( 'm_intMessageTypeId', CStrings::strToIntDef( $intMessageTypeId, NULL, false ) );
	}

	public function getMessageTypeId() {
		return $this->m_intMessageTypeId;
	}

	public function sqlMessageTypeId() {
		return ( true == isset( $this->m_intMessageTypeId ) ) ? ( string ) $this->m_intMessageTypeId : 'NULL';
	}

	public function setEnrollmentDatetime( $strEnrollmentDatetime ) {
		$this->set( 'm_strEnrollmentDatetime', CStrings::strTrimDef( $strEnrollmentDatetime, -1, NULL, true ) );
	}

	public function getEnrollmentDatetime() {
		return $this->m_strEnrollmentDatetime;
	}

	public function sqlEnrollmentDatetime() {
		return ( true == isset( $this->m_strEnrollmentDatetime ) ) ? '\'' . $this->m_strEnrollmentDatetime . '\'' : 'NOW()';
	}

	public function setLastSentOn( $strLastSentOn ) {
		$this->set( 'm_strLastSentOn', CStrings::strTrimDef( $strLastSentOn, -1, NULL, true ) );
	}

	public function getLastSentOn() {
		return $this->m_strLastSentOn;
	}

	public function sqlLastSentOn() {
		return ( true == isset( $this->m_strLastSentOn ) ) ? '\'' . $this->m_strLastSentOn . '\'' : 'NULL';
	}

	public function setNotifiedOn( $strNotifiedOn ) {
		$this->set( 'm_strNotifiedOn', CStrings::strTrimDef( $strNotifiedOn, -1, NULL, true ) );
	}

	public function getNotifiedOn() {
		return $this->m_strNotifiedOn;
	}

	public function sqlNotifiedOn() {
		return ( true == isset( $this->m_strNotifiedOn ) ) ? '\'' . $this->m_strNotifiedOn . '\'' : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, message_type_id, enrollment_datetime, last_sent_on, notified_on, disabled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlMessageTypeId() . ', ' .
 						$this->sqlEnrollmentDatetime() . ', ' .
 						$this->sqlLastSentOn() . ', ' .
 						$this->sqlNotifiedOn() . ', ' .
 						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId() . ','; } elseif( true == array_key_exists( 'MessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enrollment_datetime = ' . $this->sqlEnrollmentDatetime() . ','; } elseif( true == array_key_exists( 'EnrollmentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' enrollment_datetime = ' . $this->sqlEnrollmentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; } elseif( true == array_key_exists( 'LastSentOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; } elseif( true == array_key_exists( 'NotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'message_type_id' => $this->getMessageTypeId(),
			'enrollment_datetime' => $this->getEnrollmentDatetime(),
			'last_sent_on' => $this->getLastSentOn(),
			'notified_on' => $this->getNotifiedOn(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>