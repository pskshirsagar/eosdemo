<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyActivityPoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyActivityPoints extends CEosPluralBase {

	/**
	 * @return CCompanyActivityPoint[]
	 */
	public static function fetchCompanyActivityPoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyActivityPoint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyActivityPoint
	 */
	public static function fetchCompanyActivityPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyActivityPoint', $objDatabase );
	}

	public static function fetchCompanyActivityPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_activity_points', $objDatabase );
	}

	public static function fetchCompanyActivityPointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyActivityPoint( sprintf( 'SELECT * FROM company_activity_points WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyActivityPointsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyActivityPoints( sprintf( 'SELECT * FROM company_activity_points WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyActivityPointsByDefaultActivityPointIdByCid( $intDefaultActivityPointId, $intCid, $objDatabase ) {
		return self::fetchCompanyActivityPoints( sprintf( 'SELECT * FROM company_activity_points WHERE default_activity_point_id = %d AND cid = %d', ( int ) $intDefaultActivityPointId, ( int ) $intCid ), $objDatabase );
	}

}
?>