<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledPayment extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intPropertyId;
	protected $m_intPaymentTypeId;
	protected $m_intScheduledPaymentTypeId;
	protected $m_intScheduledPaymentFrequencyId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intCompanyCharityId;
	protected $m_fltPaymentAmount;
	protected $m_fltPaymentCeilingAmount;
	protected $m_fltDonationAmount;
	protected $m_fltPaymentAmountRate;
	protected $m_strCachedBalance;
	protected $m_strPaymentStartDate;
	protected $m_strPaymentEndDate;
	protected $m_strBilltoUnitNumber;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameMiddle;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoIpAddress;
	protected $m_strBilltoAccountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoEmailAddress;
	protected $m_intSplitEmailsCount;
	protected $m_strSplitReminderOn;
	protected $m_strCcCardNumberEncrypted;
	protected $m_intCcBinNumber;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCheckBankName;
	protected $m_strCheckNameOnAccount;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_intSecureReferenceNumber;
	protected $m_strPaymentMemo;
	protected $m_intFailureCount;
	protected $m_intFeeWaived;
	protected $m_intIsVariableAmount;
	protected $m_boolIsPayByText;
	protected $m_intForceReprocess;
	protected $m_boolUseLeaseEnd;
	protected $m_strTermsAcceptedOn;
	protected $m_strLastWarnedOn;
	protected $m_strLastEmailedOn;
	protected $m_strApprovedOn;
	protected $m_intLastPostedBy;
	protected $m_strLastPostedOn;
	protected $m_intPausedBy;
	protected $m_strPausedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intProcessDay;
	protected $m_intCardTypeId;
	protected $m_intCustomerPaymentAccountId;

	public function __construct() {
		parent::__construct();

		$this->m_fltDonationAmount = ( 0 );
		$this->m_intFailureCount = '0';
		$this->m_intFeeWaived = '0';
		$this->m_intIsVariableAmount = '0';
		$this->m_boolIsPayByText = false;
		$this->m_intForceReprocess = '0';
		$this->m_boolUseLeaseEnd = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->m_intLeaseId = trim( $arrValues['lease_id'] ); else if( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->m_intCustomerId = trim( $arrValues['customer_id'] ); else if( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->m_intPaymentTypeId = trim( $arrValues['payment_type_id'] ); else if( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['scheduled_payment_type_id'] ) && $boolDirectSet ) $this->m_intScheduledPaymentTypeId = trim( $arrValues['scheduled_payment_type_id'] ); elseif( isset( $arrValues['scheduled_payment_type_id'] ) ) $this->setScheduledPaymentTypeId( $arrValues['scheduled_payment_type_id'] );
		if( isset( $arrValues['scheduled_payment_frequency_id'] ) && $boolDirectSet ) $this->m_intScheduledPaymentFrequencyId = trim( $arrValues['scheduled_payment_frequency_id'] ); else if( isset( $arrValues['scheduled_payment_frequency_id'] ) ) $this->setScheduledPaymentFrequencyId( $arrValues['scheduled_payment_frequency_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->m_intPsProductId = trim( $arrValues['ps_product_id'] ); else if( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->m_intPsProductOptionId = trim( $arrValues['ps_product_option_id'] ); else if( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['company_charity_id'] ) && $boolDirectSet ) $this->m_intCompanyCharityId = trim( $arrValues['company_charity_id'] ); else if( isset( $arrValues['company_charity_id'] ) ) $this->setCompanyCharityId( $arrValues['company_charity_id'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->m_fltPaymentAmount = trim( $arrValues['payment_amount'] ); else if( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['payment_ceiling_amount'] ) && $boolDirectSet ) $this->m_fltPaymentCeilingAmount = trim( $arrValues['payment_ceiling_amount'] ); else if( isset( $arrValues['payment_ceiling_amount'] ) ) $this->setPaymentCeilingAmount( $arrValues['payment_ceiling_amount'] );
		if( isset( $arrValues['donation_amount'] ) && $boolDirectSet ) $this->m_fltDonationAmount = trim( $arrValues['donation_amount'] ); else if( isset( $arrValues['donation_amount'] ) ) $this->setDonationAmount( $arrValues['donation_amount'] );
		if( isset( $arrValues['payment_amount_rate'] ) && $boolDirectSet ) $this->m_fltPaymentAmountRate = trim( $arrValues['payment_amount_rate'] ); else if( isset( $arrValues['payment_amount_rate'] ) ) $this->setPaymentAmountRate( $arrValues['payment_amount_rate'] );
		if( isset( $arrValues['cached_balance'] ) && $boolDirectSet ) $this->m_strCachedBalance = trim( $arrValues['cached_balance'] ); elseif( isset( $arrValues['cached_balance'] ) ) $this->setCachedBalance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cached_balance'] ) : $arrValues['cached_balance'] );
		if( isset( $arrValues['payment_start_date'] ) && $boolDirectSet ) $this->m_strPaymentStartDate = trim( $arrValues['payment_start_date'] ); else if( isset( $arrValues['payment_start_date'] ) ) $this->setPaymentStartDate( $arrValues['payment_start_date'] );
		if( isset( $arrValues['payment_end_date'] ) && $boolDirectSet ) $this->m_strPaymentEndDate = trim( $arrValues['payment_end_date'] ); else if( isset( $arrValues['payment_end_date'] ) ) $this->setPaymentEndDate( $arrValues['payment_end_date'] );
		if( isset( $arrValues['billto_unit_number'] ) && $boolDirectSet ) $this->m_strBilltoUnitNumber = trim( stripcslashes( $arrValues['billto_unit_number'] ) ); else if( isset( $arrValues['billto_unit_number'] ) ) $this->setBilltoUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_unit_number'] ) : $arrValues['billto_unit_number'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->m_strBilltoNameFirst = trim( stripcslashes( $arrValues['billto_name_first'] ) ); else if( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first'] ) : $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_middle'] ) && $boolDirectSet ) $this->m_strBilltoNameMiddle = trim( stripcslashes( $arrValues['billto_name_middle'] ) ); else if( isset( $arrValues['billto_name_middle'] ) ) $this->setBilltoNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle'] ) : $arrValues['billto_name_middle'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->m_strBilltoNameLast = trim( stripcslashes( $arrValues['billto_name_last'] ) ); else if( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last'] ) : $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_ip_address'] ) && $boolDirectSet ) $this->m_strBilltoIpAddress = trim( stripcslashes( $arrValues['billto_ip_address'] ) ); else if( isset( $arrValues['billto_ip_address'] ) ) $this->setBilltoIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_ip_address'] ) : $arrValues['billto_ip_address'] );
		if( isset( $arrValues['billto_account_number'] ) && $boolDirectSet ) $this->m_strBilltoAccountNumber = trim( stripcslashes( $arrValues['billto_account_number'] ) ); else if( isset( $arrValues['billto_account_number'] ) ) $this->setBilltoAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_account_number'] ) : $arrValues['billto_account_number'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->m_strBilltoCompanyName = trim( stripcslashes( $arrValues['billto_company_name'] ) ); else if( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->m_strBilltoStreetLine1 = trim( stripcslashes( $arrValues['billto_street_line1'] ) ); else if( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && $boolDirectSet ) $this->m_strBilltoStreetLine2 = trim( stripcslashes( $arrValues['billto_street_line2'] ) ); else if( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line2'] ) : $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && $boolDirectSet ) $this->m_strBilltoStreetLine3 = trim( stripcslashes( $arrValues['billto_street_line3'] ) ); else if( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line3'] ) : $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->m_strBilltoCity = trim( stripcslashes( $arrValues['billto_city'] ) ); else if( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->m_strBilltoStateCode = trim( stripcslashes( $arrValues['billto_state_code'] ) ); else if( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->m_strBilltoProvince = trim( stripcslashes( $arrValues['billto_province'] ) ); else if( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_province'] ) : $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->m_strBilltoPostalCode = trim( stripcslashes( $arrValues['billto_postal_code'] ) ); else if( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->m_strBilltoCountryCode = trim( stripcslashes( $arrValues['billto_country_code'] ) ); else if( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->m_strBilltoPhoneNumber = trim( stripcslashes( $arrValues['billto_phone_number'] ) ); else if( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number'] ) : $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->m_strBilltoEmailAddress = trim( stripcslashes( $arrValues['billto_email_address'] ) ); else if( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address'] ) : $arrValues['billto_email_address'] );
		if( isset( $arrValues['split_emails_count'] ) && $boolDirectSet ) $this->m_intSplitEmailsCount = trim( $arrValues['split_emails_count'] ); else if( isset( $arrValues['split_emails_count'] ) ) $this->setSplitEmailsCount( $arrValues['split_emails_count'] );
		if( isset( $arrValues['split_reminder_on'] ) && $boolDirectSet ) $this->m_strSplitReminderOn = trim( $arrValues['split_reminder_on'] ); else if( isset( $arrValues['split_reminder_on'] ) ) $this->setSplitReminderOn( $arrValues['split_reminder_on'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->m_strCcCardNumberEncrypted = trim( stripcslashes( $arrValues['cc_card_number_encrypted'] ) ); else if( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_card_number_encrypted'] ) : $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_bin_number'] ) && $boolDirectSet ) $this->m_intCcBinNumber = trim( $arrValues['cc_bin_number'] ); else if( isset( $arrValues['cc_bin_number'] ) ) $this->setCcBinNumber( $arrValues['cc_bin_number'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->m_strCcExpDateMonth = trim( stripcslashes( $arrValues['cc_exp_date_month'] ) ); else if( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_month'] ) : $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->m_strCcExpDateYear = trim( stripcslashes( $arrValues['cc_exp_date_year'] ) ); else if( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_year'] ) : $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->m_strCcNameOnCard = trim( stripcslashes( $arrValues['cc_name_on_card'] ) ); else if( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_name_on_card'] ) : $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->m_strCheckBankName = trim( stripcslashes( $arrValues['check_bank_name'] ) ); else if( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_bank_name'] ) : $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->m_strCheckNameOnAccount = trim( stripcslashes( $arrValues['check_name_on_account'] ) ); else if( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_name_on_account'] ) : $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->m_intCheckAccountTypeId = trim( $arrValues['check_account_type_id'] ); else if( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->m_strCheckRoutingNumber = trim( stripcslashes( $arrValues['check_routing_number'] ) ); else if( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->m_strCheckAccountNumberEncrypted = trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ); else if( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->m_intSecureReferenceNumber = trim( $arrValues['secure_reference_number'] ); else if( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->m_strPaymentMemo = trim( stripcslashes( $arrValues['payment_memo'] ) ); else if( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_memo'] ) : $arrValues['payment_memo'] );
		if( isset( $arrValues['failure_count'] ) && $boolDirectSet ) $this->m_intFailureCount = trim( $arrValues['failure_count'] ); else if( isset( $arrValues['failure_count'] ) ) $this->setFailureCount( $arrValues['failure_count'] );
		if( isset( $arrValues['fee_waived'] ) && $boolDirectSet ) $this->m_intFeeWaived = trim( $arrValues['fee_waived'] ); else if( isset( $arrValues['fee_waived'] ) ) $this->setFeeWaived( $arrValues['fee_waived'] );
		if( isset( $arrValues['is_variable_amount'] ) && $boolDirectSet ) $this->m_intIsVariableAmount = trim( $arrValues['is_variable_amount'] ); else if( isset( $arrValues['is_variable_amount'] ) ) $this->setIsVariableAmount( $arrValues['is_variable_amount'] );
		if( isset( $arrValues['is_pay_by_text'] ) && $boolDirectSet ) $this->m_boolIsPayByText = trim( stripcslashes( $arrValues['is_pay_by_text'] ) ); else if( isset( $arrValues['is_pay_by_text'] ) ) $this->setIsPayByText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pay_by_text'] ) : $arrValues['is_pay_by_text'] );
		if( isset( $arrValues['force_reprocess'] ) && $boolDirectSet ) $this->m_intForceReprocess = trim( $arrValues['force_reprocess'] ); else if( isset( $arrValues['force_reprocess'] ) ) $this->setForceReprocess( $arrValues['force_reprocess'] );
		if( isset( $arrValues['use_lease_end'] ) && $boolDirectSet ) $this->m_boolUseLeaseEnd = trim( stripcslashes( $arrValues['use_lease_end'] ) ); else if( isset( $arrValues['use_lease_end'] ) ) $this->setUseLeaseEnd( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_end'] ) : $arrValues['use_lease_end'] );
		if( isset( $arrValues['terms_accepted_on'] ) && $boolDirectSet ) $this->m_strTermsAcceptedOn = trim( $arrValues['terms_accepted_on'] ); else if( isset( $arrValues['terms_accepted_on'] ) ) $this->setTermsAcceptedOn( $arrValues['terms_accepted_on'] );
		if( isset( $arrValues['last_warned_on'] ) && $boolDirectSet ) $this->m_strLastWarnedOn = trim( $arrValues['last_warned_on'] ); else if( isset( $arrValues['last_warned_on'] ) ) $this->setLastWarnedOn( $arrValues['last_warned_on'] );
		if( isset( $arrValues['last_emailed_on'] ) && $boolDirectSet ) $this->m_strLastEmailedOn = trim( $arrValues['last_emailed_on'] ); else if( isset( $arrValues['last_emailed_on'] ) ) $this->setLastEmailedOn( $arrValues['last_emailed_on'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->m_strApprovedOn = trim( $arrValues['approved_on'] ); else if( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['last_posted_by'] ) && $boolDirectSet ) $this->m_intLastPostedBy = trim( $arrValues['last_posted_by'] ); else if( isset( $arrValues['last_posted_by'] ) ) $this->setLastPostedBy( $arrValues['last_posted_by'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->m_strLastPostedOn = trim( $arrValues['last_posted_on'] ); else if( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['paused_by'] ) && $boolDirectSet ) $this->m_intPausedBy = trim( $arrValues['paused_by'] ); else if( isset( $arrValues['paused_by'] ) ) $this->setPausedBy( $arrValues['paused_by'] );
		if( isset( $arrValues['paused_on'] ) && $boolDirectSet ) $this->m_strPausedOn = trim( $arrValues['paused_on'] ); else if( isset( $arrValues['paused_on'] ) ) $this->setPausedOn( $arrValues['paused_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['process_day'] ) && $boolDirectSet ) $this->set( 'm_intProcessDay', trim( $arrValues['process_day'] ) ); elseif( isset( $arrValues['process_day'] ) ) $this->setProcessDay( $arrValues['process_day'] );
		if( isset( $arrValues['card_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCardTypeId', trim( $arrValues['card_type_id'] ) ); elseif( isset( $arrValues['card_type_id'] ) ) $this->setCardTypeId( $arrValues['card_type_id'] );
		if( isset( $arrValues['customer_payment_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerPaymentAccountId', trim( $arrValues['customer_payment_account_id'] ) ); elseif( isset( $arrValues['customer_payment_account_id'] ) ) $this->setCustomerPaymentAccountId( $arrValues['customer_payment_account_id'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->m_intPaymentTypeId = CStrings::strToIntDef( $intPaymentTypeId, NULL, false );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setScheduledPaymentTypeId( $intScheduledPaymentTypeId ) {
		$this->m_intScheduledPaymentTypeId = CStrings::strToIntDef( $intScheduledPaymentTypeId, NULL, false );
	}

	public function getScheduledPaymentTypeId() {
		return $this->m_intScheduledPaymentTypeId;
	}

	public function sqlScheduledPaymentTypeId() {
		return ( true == isset( $this->m_intScheduledPaymentTypeId ) ) ? ( string ) $this->m_intScheduledPaymentTypeId : 'NULL';
	}

	public function setScheduledPaymentFrequencyId( $intScheduledPaymentFrequencyId ) {
		$this->m_intScheduledPaymentFrequencyId = CStrings::strToIntDef( $intScheduledPaymentFrequencyId, NULL, false );
	}

	public function getScheduledPaymentFrequencyId() {
		return $this->m_intScheduledPaymentFrequencyId;
	}

	public function sqlScheduledPaymentFrequencyId() {
		return ( true == isset( $this->m_intScheduledPaymentFrequencyId ) ) ? ( string ) $this->m_intScheduledPaymentFrequencyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = CStrings::strToIntDef( $intPsProductId, NULL, false );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->m_intPsProductOptionId = CStrings::strToIntDef( $intPsProductOptionId, NULL, false );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setCompanyCharityId( $intCompanyCharityId ) {
		$this->m_intCompanyCharityId = CStrings::strToIntDef( $intCompanyCharityId, NULL, false );
	}

	public function getCompanyCharityId() {
		return $this->m_intCompanyCharityId;
	}

	public function sqlCompanyCharityId() {
		return ( true == isset( $this->m_intCompanyCharityId ) ) ? ( string ) $this->m_intCompanyCharityId : 'NULL';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->m_fltPaymentAmount = CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setPaymentCeilingAmount( $fltPaymentCeilingAmount ) {
		$this->m_fltPaymentCeilingAmount = CStrings::strToFloatDef( $fltPaymentCeilingAmount, NULL, false, 2 );
	}

	public function getPaymentCeilingAmount() {
		return $this->m_fltPaymentCeilingAmount;
	}

	public function sqlPaymentCeilingAmount() {
		return ( true == isset( $this->m_fltPaymentCeilingAmount ) ) ? ( string ) $this->m_fltPaymentCeilingAmount : 'NULL';
	}

	public function setDonationAmount( $fltDonationAmount ) {
		$this->m_fltDonationAmount = CStrings::strToFloatDef( $fltDonationAmount, NULL, false, 2 );
	}

	public function getDonationAmount() {
		return $this->m_fltDonationAmount;
	}

	public function sqlDonationAmount() {
		return ( true == isset( $this->m_fltDonationAmount ) ) ? ( string ) $this->m_fltDonationAmount : '( 0 )::numeric';
	}

	public function setPaymentAmountRate( $fltPaymentAmountRate ) {
		$this->m_fltPaymentAmountRate = CStrings::strToFloatDef( $fltPaymentAmountRate, NULL, false, 2 );
	}

	public function getPaymentAmountRate() {
		return $this->m_fltPaymentAmountRate;
	}

	public function sqlPaymentAmountRate() {
		return ( true == isset( $this->m_fltPaymentAmountRate ) ) ? ( string ) $this->m_fltPaymentAmountRate : 'NULL';
	}

	public function setCachedBalance( $strCachedBalance ) {
		$this->m_strCachedBalance = CStrings::strTrimDef( $strCachedBalance, -1, NULL, true );
	}

	public function getCachedBalance() {
		if( true == isset( $this->m_strCachedBalance ) ) {
			return $this->m_strCachedBalance ;
		}
	}

	public function sqlCachedBalance() {
		return ( true == isset( $this->m_strCachedBalance ) ) ? '\'' . addslashes( $this->m_strCachedBalance ) . '\'' : 'NULL';
	}

	public function setPaymentStartDate( $strPaymentStartDate ) {
		$this->m_strPaymentStartDate = CStrings::strTrimDef( $strPaymentStartDate, -1, NULL, true );
	}

	public function getPaymentStartDate() {
		return $this->m_strPaymentStartDate;
	}

	public function sqlPaymentStartDate() {
		return ( true == isset( $this->m_strPaymentStartDate ) ) ? '\'' . $this->m_strPaymentStartDate . '\'' : 'NOW()';
	}

	public function setPaymentEndDate( $strPaymentEndDate ) {
		$this->m_strPaymentEndDate = CStrings::strTrimDef( $strPaymentEndDate, -1, NULL, true );
	}

	public function getPaymentEndDate() {
		return $this->m_strPaymentEndDate;
	}

	public function sqlPaymentEndDate() {
		return ( true == isset( $this->m_strPaymentEndDate ) ) ? '\'' . $this->m_strPaymentEndDate . '\'' : 'NULL';
	}

	public function setBilltoUnitNumber( $strBilltoUnitNumber ) {
		$this->m_strBilltoUnitNumber = CStrings::strTrimDef( $strBilltoUnitNumber, 50, NULL, true );
	}

	public function getBilltoUnitNumber() {
		return $this->m_strBilltoUnitNumber;
	}

	public function sqlBilltoUnitNumber() {
		return ( true == isset( $this->m_strBilltoUnitNumber ) ) ? '\'' . addslashes( $this->m_strBilltoUnitNumber ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->m_strBilltoNameFirst = CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->m_strBilltoNameMiddle = CStrings::strTrimDef( $strBilltoNameMiddle, 50, NULL, true );
	}

	public function getBilltoNameMiddle() {
		return $this->m_strBilltoNameMiddle;
	}

	public function sqlBilltoNameMiddle() {
		return ( true == isset( $this->m_strBilltoNameMiddle ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddle ) . '\'' : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->m_strBilltoNameLast = CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' : 'NULL';
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->m_strBilltoIpAddress = CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' : 'NULL';
	}

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->m_strBilltoAccountNumber = CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true );
	}

	public function getBilltoAccountNumber() {
		return $this->m_strBilltoAccountNumber;
	}

	public function sqlBilltoAccountNumber() {
		return ( true == isset( $this->m_strBilltoAccountNumber ) ) ? '\'' . addslashes( $this->m_strBilltoAccountNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->m_strBilltoCompanyName = CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->m_strBilltoStreetLine1 = CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->m_strBilltoStreetLine2 = CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->m_strBilltoStreetLine3 = CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->m_strBilltoCity = CStrings::strTrimDef( $strBilltoCity, 50, NULL, true );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->m_strBilltoStateCode = CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->m_strBilltoProvince = CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->m_strBilltoPostalCode = CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->m_strBilltoCountryCode = CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->m_strBilltoEmailAddress = CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setSplitEmailsCount( $intSplitEmailsCount ) {
		$this->m_intSplitEmailsCount = CStrings::strToIntDef( $intSplitEmailsCount, NULL, false );
	}

	public function getSplitEmailsCount() {
		return $this->m_intSplitEmailsCount;
	}

	public function sqlSplitEmailsCount() {
		return ( true == isset( $this->m_intSplitEmailsCount ) ) ? ( string ) $this->m_intSplitEmailsCount : '0';
	}

	public function setSplitReminderOn( $strSplitReminderOn ) {
		$this->m_strSplitReminderOn = CStrings::strTrimDef( $strSplitReminderOn, -1, NULL, true );
	}

	public function getSplitReminderOn() {
		return $this->m_strSplitReminderOn;
	}

	public function sqlSplitReminderOn() {
		return ( true == isset( $this->m_strSplitReminderOn ) ) ? '\'' . $this->m_strSplitReminderOn . '\'' : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->m_strCcCardNumberEncrypted = CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCcBinNumber( $intCcBinNumber ) {
		$this->m_intCcBinNumber = CStrings::strToIntDef( $intCcBinNumber, NULL, false );
	}

	public function getCcBinNumber() {
		return $this->m_intCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_intCcBinNumber ) ) ? ( string ) $this->m_intCcBinNumber : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->m_strCcExpDateMonth = CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->m_strCcExpDateYear = CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->m_strCcNameOnCard = CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->m_strCheckBankName = CStrings::strTrimDef( $strCheckBankName, 100, NULL, true );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->m_strCheckNameOnAccount = CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->m_intCheckAccountTypeId = CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->m_strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->m_intSecureReferenceNumber = CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->m_strPaymentMemo = CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' : 'NULL';
	}

	public function setFailureCount( $intFailureCount ) {
		$this->m_intFailureCount = CStrings::strToIntDef( $intFailureCount, NULL, false );
	}

	public function getFailureCount() {
		return $this->m_intFailureCount;
	}

	public function sqlFailureCount() {
		return ( true == isset( $this->m_intFailureCount ) ) ? ( string ) $this->m_intFailureCount : '0';
	}

	public function setFeeWaived( $intFeeWaived ) {
		$this->m_intFeeWaived = CStrings::strToIntDef( $intFeeWaived, NULL, false );
	}

	public function getFeeWaived() {
		return $this->m_intFeeWaived;
	}

	public function sqlFeeWaived() {
		return ( true == isset( $this->m_intFeeWaived ) ) ? ( string ) $this->m_intFeeWaived : '0';
	}

	public function setIsVariableAmount( $intIsVariableAmount ) {
		$this->m_intIsVariableAmount = CStrings::strToIntDef( $intIsVariableAmount, NULL, false );
	}

	public function getIsVariableAmount() {
		return $this->m_intIsVariableAmount;
	}

	public function sqlIsVariableAmount() {
		return ( true == isset( $this->m_intIsVariableAmount ) ) ? ( string ) $this->m_intIsVariableAmount : '0';
	}

	public function setIsPayByText( $boolIsPayByText ) {
		$this->m_boolIsPayByText = CStrings::strToBool( $boolIsPayByText );
	}

	public function getIsPayByText() {
		return $this->m_boolIsPayByText;
	}

	public function sqlIsPayByText() {
		return ( true == isset( $this->m_boolIsPayByText ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPayByText ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setForceReprocess( $intForceReprocess ) {
		$this->m_intForceReprocess = CStrings::strToIntDef( $intForceReprocess, NULL, false );
	}

	public function getForceReprocess() {
		return $this->m_intForceReprocess;
	}

	public function sqlForceReprocess() {
		return ( true == isset( $this->m_intForceReprocess ) ) ? ( string ) $this->m_intForceReprocess : '0';
	}

	public function setUseLeaseEnd( $boolUseLeaseEnd ) {
		$this->m_boolUseLeaseEnd = CStrings::strToBool( $boolUseLeaseEnd );
	}

	public function getUseLeaseEnd() {
		return $this->m_boolUseLeaseEnd;
	}

	public function sqlUseLeaseEnd() {
		return ( true == isset( $this->m_boolUseLeaseEnd ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseEnd ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTermsAcceptedOn( $strTermsAcceptedOn ) {
		$this->m_strTermsAcceptedOn = CStrings::strTrimDef( $strTermsAcceptedOn, -1, NULL, true );
	}

	public function getTermsAcceptedOn() {
		return $this->m_strTermsAcceptedOn;
	}

	public function sqlTermsAcceptedOn() {
		return ( true == isset( $this->m_strTermsAcceptedOn ) ) ? '\'' . $this->m_strTermsAcceptedOn . '\'' : 'NULL';
	}

	public function setLastWarnedOn( $strLastWarnedOn ) {
		$this->m_strLastWarnedOn = CStrings::strTrimDef( $strLastWarnedOn, -1, NULL, true );
	}

	public function getLastWarnedOn() {
		return $this->m_strLastWarnedOn;
	}

	public function sqlLastWarnedOn() {
		return ( true == isset( $this->m_strLastWarnedOn ) ) ? '\'' . $this->m_strLastWarnedOn . '\'' : 'NULL';
	}

	public function setLastEmailedOn( $strLastEmailedOn ) {
		$this->m_strLastEmailedOn = CStrings::strTrimDef( $strLastEmailedOn, -1, NULL, true );
	}

	public function getLastEmailedOn() {
		return $this->m_strLastEmailedOn;
	}

	public function sqlLastEmailedOn() {
		return ( true == isset( $this->m_strLastEmailedOn ) ) ? '\'' . $this->m_strLastEmailedOn . '\'' : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->m_strApprovedOn = CStrings::strTrimDef( $strApprovedOn, -1, NULL, true );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setLastPostedBy( $intLastPostedBy ) {
		$this->m_intLastPostedBy = CStrings::strToIntDef( $intLastPostedBy, NULL, false );
	}

	public function getLastPostedBy() {
		return $this->m_intLastPostedBy;
	}

	public function sqlLastPostedBy() {
		return ( true == isset( $this->m_intLastPostedBy ) ) ? ( string ) $this->m_intLastPostedBy : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->m_strLastPostedOn = CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setPausedBy( $intPausedBy ) {
		$this->m_intPausedBy = CStrings::strToIntDef( $intPausedBy, NULL, false );
	}

	public function getPausedBy() {
		return $this->m_intPausedBy;
	}

	public function sqlPausedBy() {
		return ( true == isset( $this->m_intPausedBy ) ) ? ( string ) $this->m_intPausedBy : 'NULL';
	}

	public function setPausedOn( $strPausedOn ) {
		$this->m_strPausedOn = CStrings::strTrimDef( $strPausedOn, -1, NULL, true );
	}

	public function getPausedOn() {
		return $this->m_strPausedOn;
	}

	public function sqlPausedOn() {
		return ( true == isset( $this->m_strPausedOn ) ) ? '\'' . $this->m_strPausedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setProcessDay( $intProcessDay ) {
		$this->set( 'm_intProcessDay', CStrings::strToIntDef( $intProcessDay, NULL, false ) );
	}

	public function getProcessDay() {
		return $this->m_intProcessDay;
	}

	public function sqlProcessDay() {
		return ( true == isset( $this->m_intProcessDay ) ) ? ( string ) $this->m_intProcessDay : 'NULL';
	}

	public function setCardTypeId( $intCardTypeId ) {
		$this->set( 'm_intCardTypeId', CStrings::strToIntDef( $intCardTypeId, NULL, false ) );
	}

	public function getCardTypeId() {
		return $this->m_intCardTypeId;
	}

	public function sqlCardTypeId() {
		return ( true == isset( $this->m_intCardTypeId ) ) ? ( string ) $this->m_intCardTypeId : 'NULL';
	}

	public function setCustomerPaymentAccountId( $intCustomerPaymentAccountId ) {
		$this->set( 'm_intCustomerPaymentAccountId', CStrings::strToIntDef( $intCustomerPaymentAccountId, NULL, false ) );
	}

	public function getCustomerPaymentAccountId() {
		return $this->m_intCustomerPaymentAccountId;
	}

	public function sqlCustomerPaymentAccountId() {
		return ( true == isset( $this->m_intCustomerPaymentAccountId ) ) ? ( string ) $this->m_intCustomerPaymentAccountId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
		          'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentFrequencyId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlCompanyCharityId() . ', ' .
		          $this->sqlPaymentAmount() . ', ' .
		          $this->sqlPaymentCeilingAmount() . ', ' .
		          $this->sqlDonationAmount() . ', ' .
		          $this->sqlPaymentAmountRate() . ', ' .
		          $this->sqlCachedBalance() . ', ' .
		          $this->sqlPaymentStartDate() . ', ' .
		          $this->sqlPaymentEndDate() . ', ' .
		          $this->sqlBilltoUnitNumber() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameMiddle() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlBilltoIpAddress() . ', ' .
		          $this->sqlBilltoAccountNumber() . ', ' .
		          $this->sqlBilltoCompanyName() . ', ' .
		          $this->sqlBilltoStreetLine1() . ', ' .
		          $this->sqlBilltoStreetLine2() . ', ' .
		          $this->sqlBilltoStreetLine3() . ', ' .
		          $this->sqlBilltoCity() . ', ' .
		          $this->sqlBilltoStateCode() . ', ' .
		          $this->sqlBilltoProvince() . ', ' .
		          $this->sqlBilltoPostalCode() . ', ' .
		          $this->sqlBilltoCountryCode() . ', ' .
		          $this->sqlBilltoPhoneNumber() . ', ' .
		          $this->sqlBilltoEmailAddress() . ', ' .
		          $this->sqlSplitEmailsCount() . ', ' .
		          $this->sqlSplitReminderOn() . ', ' .
		          $this->sqlCcCardNumberEncrypted() . ', ' .
		          $this->sqlCcBinNumber() . ', ' .
		          $this->sqlCcExpDateMonth() . ', ' .
		          $this->sqlCcExpDateYear() . ', ' .
		          $this->sqlCcNameOnCard() . ', ' .
		          $this->sqlCheckBankName() . ', ' .
		          $this->sqlCheckNameOnAccount() . ', ' .
		          $this->sqlCheckAccountTypeId() . ', ' .
		          $this->sqlCheckRoutingNumber() . ', ' .
		          $this->sqlCheckAccountNumberEncrypted() . ', ' .
		          $this->sqlSecureReferenceNumber() . ', ' .
		          $this->sqlPaymentMemo() . ', ' .
		          $this->sqlFailureCount() . ', ' .
		          $this->sqlFeeWaived() . ', ' .
		          $this->sqlIsVariableAmount() . ', ' .
		          $this->sqlIsPayByText() . ', ' .
		          $this->sqlForceReprocess() . ', ' .
		          $this->sqlUseLeaseEnd() . ', ' .
		          $this->sqlTermsAcceptedOn() . ', ' .
		          $this->sqlLastWarnedOn() . ', ' .
		          $this->sqlLastEmailedOn() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlLastPostedBy() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlPausedBy() . ', ' .
		          $this->sqlPausedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlProcessDay() . ', ' .
		          $this->sqlCardTypeId() . ', ' .
		          $this->sqlCustomerPaymentAccountId() . ', ' .
		          ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
		          'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentFrequencyId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlCompanyCharityId() . ', ' .
		          $this->sqlPaymentAmount() . ', ' .
		          $this->sqlPaymentCeilingAmount() . ', ' .
		          $this->sqlDonationAmount() . ', ' .
		          $this->sqlPaymentAmountRate() . ', ' .
		          $this->sqlCachedBalance() . ', ' .
		          $this->sqlPaymentStartDate() . ', ' .
		          $this->sqlPaymentEndDate() . ', ' .
		          $this->sqlBilltoUnitNumber() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameMiddle() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlBilltoIpAddress() . ', ' .
		          $this->sqlBilltoAccountNumber() . ', ' .
		          $this->sqlBilltoCompanyName() . ', ' .
		          $this->sqlBilltoStreetLine1() . ', ' .
		          $this->sqlBilltoStreetLine2() . ', ' .
		          $this->sqlBilltoStreetLine3() . ', ' .
		          $this->sqlBilltoCity() . ', ' .
		          $this->sqlBilltoStateCode() . ', ' .
		          $this->sqlBilltoProvince() . ', ' .
		          $this->sqlBilltoPostalCode() . ', ' .
		          $this->sqlBilltoCountryCode() . ', ' .
		          $this->sqlBilltoPhoneNumber() . ', ' .
		          $this->sqlBilltoEmailAddress() . ', ' .
		          $this->sqlSplitEmailsCount() . ', ' .
		          $this->sqlSplitReminderOn() . ', ' .
		          $this->sqlCcCardNumberEncrypted() . ', ' .
		          $this->sqlCcBinNumber() . ', ' .
		          $this->sqlCcExpDateMonth() . ', ' .
		          $this->sqlCcExpDateYear() . ', ' .
		          $this->sqlCcNameOnCard() . ', ' .
		          $this->sqlCheckBankName() . ', ' .
		          $this->sqlCheckNameOnAccount() . ', ' .
		          $this->sqlCheckAccountTypeId() . ', ' .
		          $this->sqlCheckRoutingNumber() . ', ' .
		          $this->sqlCheckAccountNumberEncrypted() . ', ' .
		          $this->sqlSecureReferenceNumber() . ', ' .
		          $this->sqlPaymentMemo() . ', ' .
		          $this->sqlFailureCount() . ', ' .
		          $this->sqlFeeWaived() . ', ' .
		          $this->sqlIsVariableAmount() . ', ' .
		          $this->sqlIsPayByText() . ', ' .
		          $this->sqlForceReprocess() . ', ' .
		          $this->sqlUseLeaseEnd() . ', ' .
		          $this->sqlTermsAcceptedOn() . ', ' .
		          $this->sqlLastWarnedOn() . ', ' .
		          $this->sqlLastEmailedOn() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlLastPostedBy() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlPausedBy() . ', ' .
		          $this->sqlPausedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlProcessDay() . ', ' .
		          $this->sqlCardTypeId() . ', ' .
		          $this->sqlCustomerPaymentAccountId() . ', ' .
		          ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
		          'FROM ' . static::TABLE_NAME . '_delete( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete scheduled payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete scheduled payment record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'property_id' => $this->getPropertyId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'scheduled_payment_type_id' => $this->getScheduledPaymentTypeId(),
			'scheduled_payment_frequency_id' => $this->getScheduledPaymentFrequencyId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'company_charity_id' => $this->getCompanyCharityId(),
			'payment_amount' => $this->getPaymentAmount(),
			'payment_ceiling_amount' => $this->getPaymentCeilingAmount(),
			'donation_amount' => $this->getDonationAmount(),
			'payment_amount_rate' => $this->getPaymentAmountRate(),
			'cached_balance' => $this->getCachedBalance(),
			'payment_start_date' => $this->getPaymentStartDate(),
			'payment_end_date' => $this->getPaymentEndDate(),
			'billto_unit_number' => $this->getBilltoUnitNumber(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_middle' => $this->getBilltoNameMiddle(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_ip_address' => $this->getBilltoIpAddress(),
			'billto_account_number' => $this->getBilltoAccountNumber(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'split_emails_count' => $this->getSplitEmailsCount(),
			'split_reminder_on' => $this->getSplitReminderOn(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_bin_number' => $this->getCcBinNumber(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'payment_memo' => $this->getPaymentMemo(),
			'failure_count' => $this->getFailureCount(),
			'fee_waived' => $this->getFeeWaived(),
			'is_variable_amount' => $this->getIsVariableAmount(),
			'is_pay_by_text' => $this->getIsPayByText(),
			'force_reprocess' => $this->getForceReprocess(),
			'use_lease_end' => $this->getUseLeaseEnd(),
			'terms_accepted_on' => $this->getTermsAcceptedOn(),
			'last_warned_on' => $this->getLastWarnedOn(),
			'last_emailed_on' => $this->getLastEmailedOn(),
			'approved_on' => $this->getApprovedOn(),
			'last_posted_by' => $this->getLastPostedBy(),
			'last_posted_on' => $this->getLastPostedOn(),
			'paused_by' => $this->getPausedBy(),
			'paused_on' => $this->getPausedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'process_day' => $this->getProcessDay(),
			'card_type_id' => $this->getCardTypeId(),
			'customer_payment_account_id' => $this->getCustomerPaymentAccountId()
		);
	}

}
?>