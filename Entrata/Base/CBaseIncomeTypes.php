<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIncomeTypes
 * Do not add any new functions to this class.
 */

class CBaseIncomeTypes extends CEosPluralBase {

	/**
	 * @return CIncomeType[]
	 */
	public static function fetchIncomeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIncomeType::class, $objDatabase );
	}

	/**
	 * @return CIncomeType
	 */
	public static function fetchIncomeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIncomeType::class, $objDatabase );
	}

	public static function fetchIncomeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'income_types', $objDatabase );
	}

	public static function fetchIncomeTypeById( $intId, $objDatabase ) {
		return self::fetchIncomeType( sprintf( 'SELECT * FROM income_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchIncomeTypesByIncomeTypeGroupId( $intIncomeTypeGroupId, $objDatabase ) {
		return self::fetchIncomeTypes( sprintf( 'SELECT * FROM income_types WHERE income_type_group_id = %d', $intIncomeTypeGroupId ), $objDatabase );
	}

}
?>