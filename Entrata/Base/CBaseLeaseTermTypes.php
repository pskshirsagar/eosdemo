<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTermTypes
 * Do not add any new functions to this class.
 */

class CBaseLeaseTermTypes extends CEosPluralBase {

	/**
	 * @return CLeaseTermType[]
	 */
	public static function fetchLeaseTermTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLeaseTermType::class, $objDatabase );
	}

	/**
	 * @return CLeaseTermType
	 */
	public static function fetchLeaseTermType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseTermType::class, $objDatabase );
	}

	public static function fetchLeaseTermTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_term_types', $objDatabase );
	}

	public static function fetchLeaseTermTypeById( $intId, $objDatabase ) {
		return self::fetchLeaseTermType( sprintf( 'SELECT * FROM lease_term_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>