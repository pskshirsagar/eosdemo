<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpacePriorities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpacePriorities extends CEosPluralBase {

	/**
	 * @return CUnitSpacePriority[]
	 */
	public static function fetchUnitSpacePriorities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitSpacePriority', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpacePriority
	 */
	public static function fetchUnitSpacePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitSpacePriority', $objDatabase );
	}

	public static function fetchUnitSpacePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_priorities', $objDatabase );
	}

	public static function fetchUnitSpacePriorityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpacePriority( sprintf( 'SELECT * FROM unit_space_priorities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpacePrioritiesByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpacePriorities( sprintf( 'SELECT * FROM unit_space_priorities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpacePrioritiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpacePriorities( sprintf( 'SELECT * FROM unit_space_priorities WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpacePrioritiesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpacePriorities( sprintf( 'SELECT * FROM unit_space_priorities WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>