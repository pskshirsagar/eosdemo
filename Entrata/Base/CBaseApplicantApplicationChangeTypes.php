<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationChangeTypes
 * Do not add any new functions to this class.
 */

class CBaseApplicantApplicationChangeTypes extends CEosPluralBase {

	/**
	 * @return CApplicantApplicationChangeType[]
	 */
	public static function fetchApplicantApplicationChangeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicantApplicationChangeType::class, $objDatabase );
	}

	/**
	 * @return CApplicantApplicationChangeType
	 */
	public static function fetchApplicantApplicationChangeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicantApplicationChangeType::class, $objDatabase );
	}

	public static function fetchApplicantApplicationChangeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_application_change_types', $objDatabase );
	}

	public static function fetchApplicantApplicationChangeTypeById( $intId, $objDatabase ) {
		return self::fetchApplicantApplicationChangeType( sprintf( 'SELECT * FROM applicant_application_change_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>