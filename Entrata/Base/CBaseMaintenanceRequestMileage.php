<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestMileage extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_request_mileages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestId;
	protected $m_intCompanyEmployeeId;
	protected $m_intVehicleId;
	protected $m_strDescription;
	protected $m_fltStartingMileage;
	protected $m_fltEndingMileage;
	protected $m_fltTotalMileage;
	protected $m_strMileageDatetime;
	protected $m_intInvoiceDetailId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strMileageDatetime = 'now()';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['vehicle_id'] ) && $boolDirectSet ) $this->set( 'm_intVehicleId', trim( $arrValues['vehicle_id'] ) ); elseif( isset( $arrValues['vehicle_id'] ) ) $this->setVehicleId( $arrValues['vehicle_id'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['starting_mileage'] ) && $boolDirectSet ) $this->set( 'm_fltStartingMileage', trim( $arrValues['starting_mileage'] ) ); elseif( isset( $arrValues['starting_mileage'] ) ) $this->setStartingMileage( $arrValues['starting_mileage'] );
		if( isset( $arrValues['ending_mileage'] ) && $boolDirectSet ) $this->set( 'm_fltEndingMileage', trim( $arrValues['ending_mileage'] ) ); elseif( isset( $arrValues['ending_mileage'] ) ) $this->setEndingMileage( $arrValues['ending_mileage'] );
		if( isset( $arrValues['total_mileage'] ) && $boolDirectSet ) $this->set( 'm_fltTotalMileage', trim( $arrValues['total_mileage'] ) ); elseif( isset( $arrValues['total_mileage'] ) ) $this->setTotalMileage( $arrValues['total_mileage'] );
		if( isset( $arrValues['mileage_datetime'] ) && $boolDirectSet ) $this->set( 'm_strMileageDatetime', trim( $arrValues['mileage_datetime'] ) ); elseif( isset( $arrValues['mileage_datetime'] ) ) $this->setMileageDatetime( $arrValues['mileage_datetime'] );
		if( isset( $arrValues['invoice_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDetailId', trim( $arrValues['invoice_detail_id'] ) ); elseif( isset( $arrValues['invoice_detail_id'] ) ) $this->setInvoiceDetailId( $arrValues['invoice_detail_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setVehicleId( $intVehicleId ) {
		$this->set( 'm_intVehicleId', CStrings::strToIntDef( $intVehicleId, NULL, false ) );
	}

	public function getVehicleId() {
		return $this->m_intVehicleId;
	}

	public function sqlVehicleId() {
		return ( true == isset( $this->m_intVehicleId ) ) ? ( string ) $this->m_intVehicleId : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setStartingMileage( $fltStartingMileage ) {
		$this->set( 'm_fltStartingMileage', CStrings::strToFloatDef( $fltStartingMileage, NULL, false, 2 ) );
	}

	public function getStartingMileage() {
		return $this->m_fltStartingMileage;
	}

	public function sqlStartingMileage() {
		return ( true == isset( $this->m_fltStartingMileage ) ) ? ( string ) $this->m_fltStartingMileage : 'NULL';
	}

	public function setEndingMileage( $fltEndingMileage ) {
		$this->set( 'm_fltEndingMileage', CStrings::strToFloatDef( $fltEndingMileage, NULL, false, 2 ) );
	}

	public function getEndingMileage() {
		return $this->m_fltEndingMileage;
	}

	public function sqlEndingMileage() {
		return ( true == isset( $this->m_fltEndingMileage ) ) ? ( string ) $this->m_fltEndingMileage : 'NULL';
	}

	public function setTotalMileage( $fltTotalMileage ) {
		$this->set( 'm_fltTotalMileage', CStrings::strToFloatDef( $fltTotalMileage, NULL, false, 2 ) );
	}

	public function getTotalMileage() {
		return $this->m_fltTotalMileage;
	}

	public function sqlTotalMileage() {
		return ( true == isset( $this->m_fltTotalMileage ) ) ? ( string ) $this->m_fltTotalMileage : 'NULL';
	}

	public function setMileageDatetime( $strMileageDatetime ) {
		$this->set( 'm_strMileageDatetime', CStrings::strTrimDef( $strMileageDatetime, -1, NULL, true ) );
	}

	public function getMileageDatetime() {
		return $this->m_strMileageDatetime;
	}

	public function sqlMileageDatetime() {
		return ( true == isset( $this->m_strMileageDatetime ) ) ? '\'' . $this->m_strMileageDatetime . '\'' : 'NOW()';
	}

	public function setInvoiceDetailId( $intInvoiceDetailId ) {
		$this->set( 'm_intInvoiceDetailId', CStrings::strToIntDef( $intInvoiceDetailId, NULL, false ) );
	}

	public function getInvoiceDetailId() {
		return $this->m_intInvoiceDetailId;
	}

	public function sqlInvoiceDetailId() {
		return ( true == isset( $this->m_intInvoiceDetailId ) ) ? ( string ) $this->m_intInvoiceDetailId : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_id, company_employee_id, vehicle_id, description, starting_mileage, ending_mileage, total_mileage, mileage_datetime, invoice_detail_id, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlVehicleId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlStartingMileage() . ', ' .
						$this->sqlEndingMileage() . ', ' .
						$this->sqlTotalMileage() . ', ' .
						$this->sqlMileageDatetime() . ', ' .
						$this->sqlInvoiceDetailId() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vehicle_id = ' . $this->sqlVehicleId(). ',' ; } elseif( true == array_key_exists( 'VehicleId', $this->getChangedColumns() ) ) { $strSql .= ' vehicle_id = ' . $this->sqlVehicleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' starting_mileage = ' . $this->sqlStartingMileage(). ',' ; } elseif( true == array_key_exists( 'StartingMileage', $this->getChangedColumns() ) ) { $strSql .= ' starting_mileage = ' . $this->sqlStartingMileage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ending_mileage = ' . $this->sqlEndingMileage(). ',' ; } elseif( true == array_key_exists( 'EndingMileage', $this->getChangedColumns() ) ) { $strSql .= ' ending_mileage = ' . $this->sqlEndingMileage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_mileage = ' . $this->sqlTotalMileage(). ',' ; } elseif( true == array_key_exists( 'TotalMileage', $this->getChangedColumns() ) ) { $strSql .= ' total_mileage = ' . $this->sqlTotalMileage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mileage_datetime = ' . $this->sqlMileageDatetime(). ',' ; } elseif( true == array_key_exists( 'MileageDatetime', $this->getChangedColumns() ) ) { $strSql .= ' mileage_datetime = ' . $this->sqlMileageDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_detail_id = ' . $this->sqlInvoiceDetailId(). ',' ; } elseif( true == array_key_exists( 'InvoiceDetailId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_detail_id = ' . $this->sqlInvoiceDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'vehicle_id' => $this->getVehicleId(),
			'description' => $this->getDescription(),
			'starting_mileage' => $this->getStartingMileage(),
			'ending_mileage' => $this->getEndingMileage(),
			'total_mileage' => $this->getTotalMileage(),
			'mileage_datetime' => $this->getMileageDatetime(),
			'invoice_detail_id' => $this->getInvoiceDetailId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>