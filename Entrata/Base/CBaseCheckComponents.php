<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCheckComponents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCheckComponents extends CEosPluralBase {

	/**
	 * @return CCheckComponent[]
	 */
	public static function fetchCheckComponents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCheckComponent', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCheckComponent
	 */
	public static function fetchCheckComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCheckComponent', $objDatabase );
	}

	public static function fetchCheckComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'check_components', $objDatabase );
	}

	public static function fetchCheckComponentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCheckComponent( sprintf( 'SELECT * FROM check_components WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCheckComponentsByCid( $intCid, $objDatabase ) {
		return self::fetchCheckComponents( sprintf( 'SELECT * FROM check_components WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCheckComponentsByCheckTemplateIdByCid( $intCheckTemplateId, $intCid, $objDatabase ) {
		return self::fetchCheckComponents( sprintf( 'SELECT * FROM check_components WHERE check_template_id = %d AND cid = %d', ( int ) $intCheckTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCheckComponentsByCheckComponentTypeIdByCid( $intCheckComponentTypeId, $intCid, $objDatabase ) {
		return self::fetchCheckComponents( sprintf( 'SELECT * FROM check_components WHERE check_component_type_id = %d AND cid = %d', ( int ) $intCheckComponentTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>