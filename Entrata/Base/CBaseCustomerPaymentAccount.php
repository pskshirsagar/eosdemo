<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPaymentAccount extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_payment_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intApplicantId;
	protected $m_intPaymentTypeId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intAccountVerificationLogId;
	protected $m_strBilltoAccountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameMiddle;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoUnitNumber;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoEmailAddress;
	protected $m_intSecureReferenceNumber;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCcBinNumber;
	protected $m_boolIsDebitCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsCreditCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCommercialCard;
	protected $m_strCheckBankName;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckNameOnAccount;
	protected $m_strAccountNickname;
	protected $m_intIsInternational;
	protected $m_strLastUsedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strPaymentBankAccountId;
	protected $m_intCardTypeId;
	protected $m_intCustomerPlaidItemId;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intIsInternational = '0';
		$this->m_arrstrPostalAddressFields = [
			 'billto' => [
				'm_strBilltoStreetLine1' => 'addressLine1',
				'm_strBilltoStreetLine2' => 'addressLine2',
				'm_strBilltoStreetLine3' => 'addressLine3',
				'm_strBilltoCity' => 'locality',
				'm_strBilltoStateCode' => 'administrativeArea',
				'm_strBilltoPostalCode' => 'postalCode',
				'm_strBilltoCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['account_verification_log_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountVerificationLogId', trim( $arrValues['account_verification_log_id'] ) ); elseif( isset( $arrValues['account_verification_log_id'] ) ) $this->setAccountVerificationLogId( $arrValues['account_verification_log_id'] );
		if( isset( $arrValues['billto_account_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoAccountNumber', trim( stripcslashes( $arrValues['billto_account_number'] ) ) ); elseif( isset( $arrValues['billto_account_number'] ) ) $this->setBilltoAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_account_number'] ) : $arrValues['billto_account_number'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( stripcslashes( $arrValues['billto_company_name'] ) ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirst', trim( stripcslashes( $arrValues['billto_name_first'] ) ) ); elseif( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first'] ) : $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddle', trim( stripcslashes( $arrValues['billto_name_middle'] ) ) ); elseif( isset( $arrValues['billto_name_middle'] ) ) $this->setBilltoNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle'] ) : $arrValues['billto_name_middle'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLast', trim( stripcslashes( $arrValues['billto_name_last'] ) ) ); elseif( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last'] ) : $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine1', trim( stripcslashes( $arrValues['billto_street_line1'] ) ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine2', trim( stripcslashes( $arrValues['billto_street_line2'] ) ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line2'] ) : $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine3', trim( stripcslashes( $arrValues['billto_street_line3'] ) ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line3'] ) : $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_unit_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoUnitNumber', trim( stripcslashes( $arrValues['billto_unit_number'] ) ) ); elseif( isset( $arrValues['billto_unit_number'] ) ) $this->setBilltoUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_unit_number'] ) : $arrValues['billto_unit_number'] );
		if( isset( $arrValues['billto_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCity', trim( stripcslashes( $arrValues['billto_city'] ) ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStateCode', trim( stripcslashes( $arrValues['billto_state_code'] ) ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( stripcslashes( $arrValues['billto_province'] ) ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_province'] ) : $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoPostalCode', trim( stripcslashes( $arrValues['billto_postal_code'] ) ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCountryCode', trim( stripcslashes( $arrValues['billto_country_code'] ) ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumber', trim( stripcslashes( $arrValues['billto_phone_number'] ) ) ); elseif( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number'] ) : $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( stripcslashes( $arrValues['billto_email_address'] ) ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address'] ) : $arrValues['billto_email_address'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( stripcslashes( $arrValues['cc_card_number_encrypted'] ) ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_card_number_encrypted'] ) : $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( stripcslashes( $arrValues['cc_exp_date_month'] ) ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_month'] ) : $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( stripcslashes( $arrValues['cc_exp_date_year'] ) ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_year'] ) : $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->set( 'm_strCcNameOnCard', trim( stripcslashes( $arrValues['cc_name_on_card'] ) ) ); elseif( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_name_on_card'] ) : $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['cc_bin_number'] ) && $boolDirectSet ) $this->set( 'm_strCcBinNumber', trim( stripcslashes( $arrValues['cc_bin_number'] ) ) ); elseif( isset( $arrValues['cc_bin_number'] ) ) $this->setCcBinNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_bin_number'] ) : $arrValues['cc_bin_number'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsDebitCard', trim( stripcslashes( $arrValues['is_debit_card'] ) ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_debit_card'] ) : $arrValues['is_debit_card'] );
		if( isset( $arrValues['is_check_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCheckCard', trim( stripcslashes( $arrValues['is_check_card'] ) ) ); elseif( isset( $arrValues['is_check_card'] ) ) $this->setIsCheckCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_check_card'] ) : $arrValues['is_check_card'] );
		if( isset( $arrValues['is_gift_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsGiftCard', trim( stripcslashes( $arrValues['is_gift_card'] ) ) ); elseif( isset( $arrValues['is_gift_card'] ) ) $this->setIsGiftCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gift_card'] ) : $arrValues['is_gift_card'] );
		if( isset( $arrValues['is_prepaid_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrepaidCard', trim( stripcslashes( $arrValues['is_prepaid_card'] ) ) ); elseif( isset( $arrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prepaid_card'] ) : $arrValues['is_prepaid_card'] );
		if( isset( $arrValues['is_credit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreditCard', trim( stripcslashes( $arrValues['is_credit_card'] ) ) ); elseif( isset( $arrValues['is_credit_card'] ) ) $this->setIsCreditCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit_card'] ) : $arrValues['is_credit_card'] );
		if( isset( $arrValues['is_international_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternationalCard', trim( stripcslashes( $arrValues['is_international_card'] ) ) ); elseif( isset( $arrValues['is_international_card'] ) ) $this->setIsInternationalCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_international_card'] ) : $arrValues['is_international_card'] );
		if( isset( $arrValues['is_commercial_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommercialCard', trim( stripcslashes( $arrValues['is_commercial_card'] ) ) ); elseif( isset( $arrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_commercial_card'] ) : $arrValues['is_commercial_card'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( stripcslashes( $arrValues['check_bank_name'] ) ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_bank_name'] ) : $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( stripcslashes( $arrValues['check_name_on_account'] ) ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_name_on_account'] ) : $arrValues['check_name_on_account'] );
		if( isset( $arrValues['account_nickname'] ) && $boolDirectSet ) $this->set( 'm_strAccountNickname', trim( stripcslashes( $arrValues['account_nickname'] ) ) ); elseif( isset( $arrValues['account_nickname'] ) ) $this->setAccountNickname( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_nickname'] ) : $arrValues['account_nickname'] );
		if( isset( $arrValues['is_international'] ) && $boolDirectSet ) $this->set( 'm_intIsInternational', trim( $arrValues['is_international'] ) ); elseif( isset( $arrValues['is_international'] ) ) $this->setIsInternational( $arrValues['is_international'] );
		if( isset( $arrValues['last_used_on'] ) && $boolDirectSet ) $this->set( 'm_strLastUsedOn', trim( $arrValues['last_used_on'] ) ); elseif( isset( $arrValues['last_used_on'] ) ) $this->setLastUsedOn( $arrValues['last_used_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['payment_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_strPaymentBankAccountId', trim( $arrValues['payment_bank_account_id'] ) ); elseif( isset( $arrValues['payment_bank_account_id'] ) ) $this->setPaymentBankAccountId( $arrValues['payment_bank_account_id'] );
		if( isset( $arrValues['card_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCardTypeId', trim( $arrValues['card_type_id'] ) ); elseif( isset( $arrValues['card_type_id'] ) ) $this->setCardTypeId( $arrValues['card_type_id'] );
		if( isset( $arrValues['customer_plaid_item_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerPlaidItemId', trim( $arrValues['customer_plaid_item_id'] ) ); elseif( isset( $arrValues['customer_plaid_item_id'] ) ) $this->setCustomerPlaidItemId( $arrValues['customer_plaid_item_id'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setAccountVerificationLogId( $intAccountVerificationLogId ) {
		$this->set( 'm_intAccountVerificationLogId', CStrings::strToIntDef( $intAccountVerificationLogId, NULL, false ) );
	}

	public function getAccountVerificationLogId() {
		return $this->m_intAccountVerificationLogId;
	}

	public function sqlAccountVerificationLogId() {
		return ( true == isset( $this->m_intAccountVerificationLogId ) ) ? ( string ) $this->m_intAccountVerificationLogId : 'NULL';
	}

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->set( 'm_strBilltoAccountNumber', CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true ) );
	}

	public function getBilltoAccountNumber() {
		return $this->m_strBilltoAccountNumber;
	}

	public function sqlBilltoAccountNumber() {
		return ( true == isset( $this->m_strBilltoAccountNumber ) ) ? '\'' . addslashes( $this->m_strBilltoAccountNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->set( 'm_strBilltoNameFirst', CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true ) );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->set( 'm_strBilltoNameMiddle', CStrings::strTrimDef( $strBilltoNameMiddle, 50, NULL, true ) );
	}

	public function getBilltoNameMiddle() {
		return $this->m_strBilltoNameMiddle;
	}

	public function sqlBilltoNameMiddle() {
		return ( true == isset( $this->m_strBilltoNameMiddle ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddle ) . '\'' : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->set( 'm_strBilltoNameLast', CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true ) );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strBilltoStreetLine1, $strAddressKey = 'billto', 'm_strBilltoStreetLine1' );
	}

	public function getBilltoStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'billto', 'm_strBilltoStreetLine1' );
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strBilltoStreetLine2, $strAddressKey = 'billto', 'm_strBilltoStreetLine2' );
	}

	public function getBilltoStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'billto', 'm_strBilltoStreetLine2' );
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strBilltoStreetLine3, $strAddressKey = 'billto', 'm_strBilltoStreetLine3' );
	}

	public function getBilltoStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'billto', 'm_strBilltoStreetLine3' );
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoUnitNumber( $strBilltoUnitNumber ) {
		$this->set( 'm_strBilltoUnitNumber', CStrings::strTrimDef( $strBilltoUnitNumber, 50, NULL, true ) );
	}

	public function getBilltoUnitNumber() {
		return $this->m_strBilltoUnitNumber;
	}

	public function sqlBilltoUnitNumber() {
		return ( true == isset( $this->m_strBilltoUnitNumber ) ) ? '\'' . addslashes( $this->m_strBilltoUnitNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->setPostalAddressField( 'locality', $strBilltoCity, $strAddressKey = 'billto', 'm_strBilltoCity' );
	}

	public function getBilltoCity() {
		return $this->getPostalAddressField( 'locality', 'billto', 'm_strBilltoCity' );
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strBilltoStateCode, $strAddressKey = 'billto', 'm_strBilltoStateCode' );
	}

	public function getBilltoStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'billto', 'm_strBilltoStateCode' );
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strBilltoPostalCode, $strAddressKey = 'billto', 'm_strBilltoPostalCode' );
	}

	public function getBilltoPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'billto', 'm_strBilltoPostalCode' );
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->setPostalAddressField( 'country', $strBilltoCountryCode, $strAddressKey = 'billto', 'm_strBilltoCountryCode' );
	}

	public function getBilltoCountryCode() {
		return $this->getPostalAddressField( 'country', 'billto', 'm_strBilltoCountryCode' );
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->set( 'm_strBilltoPhoneNumber', CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true ) );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->set( 'm_strCcNameOnCard', CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true ) );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' : 'NULL';
	}

	public function setCcBinNumber( $strCcBinNumber ) {
		$this->set( 'm_strCcBinNumber', CStrings::strTrimDef( $strCcBinNumber, 6, NULL, true ) );
	}

	public function getCcBinNumber() {
		return $this->m_strCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_strCcBinNumber ) ) ? '\'' . addslashes( $this->m_strCcBinNumber ) . '\'' : 'NULL';
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->set( 'm_boolIsDebitCard', CStrings::strToBool( $boolIsDebitCard ) );
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_boolIsDebitCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDebitCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->set( 'm_boolIsCheckCard', CStrings::strToBool( $boolIsCheckCard ) );
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function sqlIsCheckCard() {
		return ( true == isset( $this->m_boolIsCheckCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCheckCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->set( 'm_boolIsGiftCard', CStrings::strToBool( $boolIsGiftCard ) );
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function sqlIsGiftCard() {
		return ( true == isset( $this->m_boolIsGiftCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGiftCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->set( 'm_boolIsPrepaidCard', CStrings::strToBool( $boolIsPrepaidCard ) );
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function sqlIsPrepaidCard() {
		return ( true == isset( $this->m_boolIsPrepaidCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrepaidCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->set( 'm_boolIsCreditCard', CStrings::strToBool( $boolIsCreditCard ) );
	}

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function sqlIsCreditCard() {
		return ( true == isset( $this->m_boolIsCreditCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreditCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->set( 'm_boolIsInternationalCard', CStrings::strToBool( $boolIsInternationalCard ) );
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function sqlIsInternationalCard() {
		return ( true == isset( $this->m_boolIsInternationalCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternationalCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->set( 'm_boolIsCommercialCard', CStrings::strToBool( $boolIsCommercialCard ) );
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function sqlIsCommercialCard() {
		return ( true == isset( $this->m_boolIsCommercialCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommercialCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setAccountNickname( $strAccountNickname ) {
		$this->set( 'm_strAccountNickname', CStrings::strTrimDef( $strAccountNickname, 30, NULL, true ) );
	}

	public function getAccountNickname() {
		return $this->m_strAccountNickname;
	}

	public function sqlAccountNickname() {
		return ( true == isset( $this->m_strAccountNickname ) ) ? '\'' . addslashes( $this->m_strAccountNickname ) . '\'' : 'NULL';
	}

	public function setIsInternational( $intIsInternational ) {
		$this->set( 'm_intIsInternational', CStrings::strToIntDef( $intIsInternational, NULL, false ) );
	}

	public function getIsInternational() {
		return $this->m_intIsInternational;
	}

	public function sqlIsInternational() {
		return ( true == isset( $this->m_intIsInternational ) ) ? ( string ) $this->m_intIsInternational : '0';
	}

	public function setLastUsedOn( $strLastUsedOn ) {
		$this->set( 'm_strLastUsedOn', CStrings::strTrimDef( $strLastUsedOn, -1, NULL, true ) );
	}

	public function getLastUsedOn() {
		return $this->m_strLastUsedOn;
	}

	public function sqlLastUsedOn() {
		return ( true == isset( $this->m_strLastUsedOn ) ) ? '\'' . $this->m_strLastUsedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPaymentBankAccountId( $strPaymentBankAccountId ) {
		$this->set( 'm_strPaymentBankAccountId', CStrings::strTrimDef( $strPaymentBankAccountId, NULL, NULL, true ) );
	}

	public function getPaymentBankAccountId() {
		return $this->m_strPaymentBankAccountId;
	}

	public function sqlPaymentBankAccountId() {
		return ( true == isset( $this->m_strPaymentBankAccountId ) ) ? '\'' . addslashes( $this->m_strPaymentBankAccountId ) . '\'' : 'NULL';
	}


	public function setCardTypeId( $intCardTypeId ) {
		$this->set( 'm_intCardTypeId', CStrings::strToIntDef( $intCardTypeId, NULL, false ) );
	}

	public function getCardTypeId() {
		return $this->m_intCardTypeId;
	}

	public function sqlCardTypeId() {
		return ( true == isset( $this->m_intCardTypeId ) ) ? ( string ) $this->m_intCardTypeId : 'NULL';
	}

	public function setCustomerPlaidItemId( $intCustomerPlaidItemId ) {
		$this->set( 'm_intCustomerPlaidItemId', CStrings::strToIntDef( $intCustomerPlaidItemId, NULL, false ) );
	}

	public function getCustomerPlaidItemId() {
		return $this->m_intCustomerPlaidItemId;
	}

	public function sqlCustomerPlaidItemId() {
		return ( true == isset( $this->m_intCustomerPlaidItemId ) ) ? ( string ) $this->m_intCustomerPlaidItemId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, applicant_id, payment_type_id, ps_product_id, ps_product_option_id, account_verification_log_id, billto_account_number, billto_company_name, billto_name_first, billto_name_middle, billto_name_last, billto_street_line1, billto_street_line2, billto_street_line3, billto_unit_number, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, billto_phone_number, billto_email_address, secure_reference_number, cc_card_number_encrypted, cc_exp_date_month, cc_exp_date_year, cc_name_on_card, cc_bin_number, is_debit_card, is_check_card, is_gift_card, is_prepaid_card, is_credit_card, is_international_card, is_commercial_card, check_bank_name, check_account_type_id, check_account_number_encrypted, check_routing_number, check_name_on_account, account_nickname, is_international, last_used_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, payment_bank_account_id, card_type_id, customer_plaid_item_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlPaymentTypeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlPsProductOptionId() . ', ' .
						$this->sqlAccountVerificationLogId() . ', ' .
						$this->sqlBilltoAccountNumber() . ', ' .
						$this->sqlBilltoCompanyName() . ', ' .
						$this->sqlBilltoNameFirst() . ', ' .
						$this->sqlBilltoNameMiddle() . ', ' .
						$this->sqlBilltoNameLast() . ', ' .
						$this->sqlBilltoStreetLine1() . ', ' .
						$this->sqlBilltoStreetLine2() . ', ' .
						$this->sqlBilltoStreetLine3() . ', ' .
						$this->sqlBilltoUnitNumber() . ', ' .
						$this->sqlBilltoCity() . ', ' .
						$this->sqlBilltoStateCode() . ', ' .
						$this->sqlBilltoProvince() . ', ' .
						$this->sqlBilltoPostalCode() . ', ' .
						$this->sqlBilltoCountryCode() . ', ' .
						$this->sqlBilltoPhoneNumber() . ', ' .
						$this->sqlBilltoEmailAddress() . ', ' .
						$this->sqlSecureReferenceNumber() . ', ' .
						$this->sqlCcCardNumberEncrypted() . ', ' .
						$this->sqlCcExpDateMonth() . ', ' .
						$this->sqlCcExpDateYear() . ', ' .
						$this->sqlCcNameOnCard() . ', ' .
						$this->sqlCcBinNumber() . ', ' .
						$this->sqlIsDebitCard() . ', ' .
						$this->sqlIsCheckCard() . ', ' .
						$this->sqlIsGiftCard() . ', ' .
						$this->sqlIsPrepaidCard() . ', ' .
						$this->sqlIsCreditCard() . ', ' .
						$this->sqlIsInternationalCard() . ', ' .
						$this->sqlIsCommercialCard() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckNameOnAccount() . ', ' .
						$this->sqlAccountNickname() . ', ' .
						$this->sqlIsInternational() . ', ' .
						$this->sqlLastUsedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlPaymentBankAccountId() . ', ' .
						$this->sqlCardTypeId() . ',' .
		                $this->sqlCustomerPlaidItemId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_verification_log_id = ' . $this->sqlAccountVerificationLogId(). ',' ; } elseif( true == array_key_exists( 'AccountVerificationLogId', $this->getChangedColumns() ) ) { $strSql .= ' account_verification_log_id = ' . $this->sqlAccountVerificationLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_account_number = ' . $this->sqlBilltoAccountNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_account_number = ' . $this->sqlBilltoAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName(). ',' ; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst(). ',' ; } elseif( true == array_key_exists( 'BilltoNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle(). ',' ; } elseif( true == array_key_exists( 'BilltoNameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_middle = ' . $this->sqlBilltoNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast(). ',' ; } elseif( true == array_key_exists( 'BilltoNameLast', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_unit_number = ' . $this->sqlBilltoUnitNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoUnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_unit_number = ' . $this->sqlBilltoUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity(). ',' ; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode(). ',' ; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince(). ',' ; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode(). ',' ; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode(). ',' ; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CcCardNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth(). ',' ; } elseif( true == array_key_exists( 'CcExpDateMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear(). ',' ; } elseif( true == array_key_exists( 'CcExpDateYear', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard(). ',' ; } elseif( true == array_key_exists( 'CcNameOnCard', $this->getChangedColumns() ) ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber(). ',' ; } elseif( true == array_key_exists( 'CcBinNumber', $this->getChangedColumns() ) ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard(). ',' ; } elseif( true == array_key_exists( 'IsDebitCard', $this->getChangedColumns() ) ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard(). ',' ; } elseif( true == array_key_exists( 'IsCheckCard', $this->getChangedColumns() ) ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard(). ',' ; } elseif( true == array_key_exists( 'IsGiftCard', $this->getChangedColumns() ) ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard(). ',' ; } elseif( true == array_key_exists( 'IsPrepaidCard', $this->getChangedColumns() ) ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard(). ',' ; } elseif( true == array_key_exists( 'IsCreditCard', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_international_card = ' . $this->sqlIsInternationalCard(). ',' ; } elseif( true == array_key_exists( 'IsInternationalCard', $this->getChangedColumns() ) ) { $strSql .= ' is_international_card = ' . $this->sqlIsInternationalCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard(). ',' ; } elseif( true == array_key_exists( 'IsCommercialCard', $this->getChangedColumns() ) ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_nickname = ' . $this->sqlAccountNickname(). ',' ; } elseif( true == array_key_exists( 'AccountNickname', $this->getChangedColumns() ) ) { $strSql .= ' account_nickname = ' . $this->sqlAccountNickname() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_international = ' . $this->sqlIsInternational(). ',' ; } elseif( true == array_key_exists( 'IsInternational', $this->getChangedColumns() ) ) { $strSql .= ' is_international = ' . $this->sqlIsInternational() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_used_on = ' . $this->sqlLastUsedOn(). ',' ; } elseif( true == array_key_exists( 'LastUsedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_used_on = ' . $this->sqlLastUsedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_bank_account_id = ' . $this->sqlPaymentBankAccountId(). ',' ; } elseif( true == array_key_exists( 'PaymentBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' payment_bank_account_id = ' . $this->sqlPaymentBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_type_id = ' . $this->sqlCardTypeId(). ',' ; } elseif( true == array_key_exists( 'CardTypeId', $this->getChangedColumns() ) ) { $strSql .= ' card_type_id = ' . $this->sqlCardTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'applicant_id' => $this->getApplicantId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'account_verification_log_id' => $this->getAccountVerificationLogId(),
			'billto_account_number' => $this->getBilltoAccountNumber(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_middle' => $this->getBilltoNameMiddle(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_unit_number' => $this->getBilltoUnitNumber(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'cc_bin_number' => $this->getCcBinNumber(),
			'is_debit_card' => $this->getIsDebitCard(),
			'is_check_card' => $this->getIsCheckCard(),
			'is_gift_card' => $this->getIsGiftCard(),
			'is_prepaid_card' => $this->getIsPrepaidCard(),
			'is_credit_card' => $this->getIsCreditCard(),
			'is_international_card' => $this->getIsInternationalCard(),
			'is_commercial_card' => $this->getIsCommercialCard(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'account_nickname' => $this->getAccountNickname(),
			'is_international' => $this->getIsInternational(),
			'last_used_on' => $this->getLastUsedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'payment_bank_account_id' => $this->getPaymentBankAccountId(),
			'card_type_id' => $this->getCardTypeId()
		);
	}

}
?>