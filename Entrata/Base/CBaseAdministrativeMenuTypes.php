<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAdministrativeMenuTypes
 * Do not add any new functions to this class.
 */

class CBaseAdministrativeMenuTypes extends CEosPluralBase {

	/**
	 * @return CAdministrativeMenuType[]
	 */
	public static function fetchAdministrativeMenuTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAdministrativeMenuType::class, $objDatabase );
	}

	/**
	 * @return CAdministrativeMenuType
	 */
	public static function fetchAdministrativeMenuType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAdministrativeMenuType::class, $objDatabase );
	}

	public static function fetchAdministrativeMenuTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'administrative_menu_types', $objDatabase );
	}

	public static function fetchAdministrativeMenuTypeById( $intId, $objDatabase ) {
		return self::fetchAdministrativeMenuType( sprintf( 'SELECT * FROM administrative_menu_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>