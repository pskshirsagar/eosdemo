<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseStartWindow extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_start_windows';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseStartStructureId;
	protected $m_intDefaultLeaseStartWindowId;
	protected $m_intPropertyId;
	protected $m_intLeaseTermId;
	protected $m_intOffsetStartDays;
	protected $m_intOffsetEndDays;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strBillingEndDate;
	protected $m_strRenewalStartDate;
	protected $m_strRenewalBillingStartDate;
	protected $m_boolIsActive;
	protected $m_boolIsDefault;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOrganizationContractId;
	protected $m_strLeaseExtensionDate;
	protected $m_intOccupancyTypeId;
	protected $m_intMinDays;
	protected $m_intMaxDays;
	protected $m_intLeaseTermTypeId;
	protected $m_boolShowOnWebsite;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;
		$this->m_boolIsDefault = false;
		$this->m_intMinDays = '0';
		$this->m_intMaxDays = '0';
		$this->m_boolShowOnWebsite = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_start_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartStructureId', trim( $arrValues['lease_start_structure_id'] ) ); elseif( isset( $arrValues['lease_start_structure_id'] ) ) $this->setLeaseStartStructureId( $arrValues['lease_start_structure_id'] );
		if( isset( $arrValues['default_lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseStartWindowId', trim( $arrValues['default_lease_start_window_id'] ) ); elseif( isset( $arrValues['default_lease_start_window_id'] ) ) $this->setDefaultLeaseStartWindowId( $arrValues['default_lease_start_window_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['offset_start_days'] ) && $boolDirectSet ) $this->set( 'm_intOffsetStartDays', trim( $arrValues['offset_start_days'] ) ); elseif( isset( $arrValues['offset_start_days'] ) ) $this->setOffsetStartDays( $arrValues['offset_start_days'] );
		if( isset( $arrValues['offset_end_days'] ) && $boolDirectSet ) $this->set( 'm_intOffsetEndDays', trim( $arrValues['offset_end_days'] ) ); elseif( isset( $arrValues['offset_end_days'] ) ) $this->setOffsetEndDays( $arrValues['offset_end_days'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['billing_end_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingEndDate', trim( $arrValues['billing_end_date'] ) ); elseif( isset( $arrValues['billing_end_date'] ) ) $this->setBillingEndDate( $arrValues['billing_end_date'] );
		if( isset( $arrValues['renewal_start_date'] ) && $boolDirectSet ) $this->set( 'm_strRenewalStartDate', trim( $arrValues['renewal_start_date'] ) ); elseif( isset( $arrValues['renewal_start_date'] ) ) $this->setRenewalStartDate( $arrValues['renewal_start_date'] );
		if( isset( $arrValues['renewal_billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strRenewalBillingStartDate', trim( $arrValues['renewal_billing_start_date'] ) ); elseif( isset( $arrValues['renewal_billing_start_date'] ) ) $this->setRenewalBillingStartDate( $arrValues['renewal_billing_start_date'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
		if( isset( $arrValues['lease_extension_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseExtensionDate', trim( $arrValues['lease_extension_date'] ) ); elseif( isset( $arrValues['lease_extension_date'] ) ) $this->setLeaseExtensionDate( $arrValues['lease_extension_date'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['min_days'] ) && $boolDirectSet ) $this->set( 'm_intMinDays', trim( $arrValues['min_days'] ) ); elseif( isset( $arrValues['min_days'] ) ) $this->setMinDays( $arrValues['min_days'] );
		if( isset( $arrValues['max_days'] ) && $boolDirectSet ) $this->set( 'm_intMaxDays', trim( $arrValues['max_days'] ) ); elseif( isset( $arrValues['max_days'] ) ) $this->setMaxDays( $arrValues['max_days'] );
		if( isset( $arrValues['lease_term_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermTypeId', trim( $arrValues['lease_term_type_id'] ) ); elseif( isset( $arrValues['lease_term_type_id'] ) ) $this->setLeaseTermTypeId( $arrValues['lease_term_type_id'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseStartStructureId( $intLeaseStartStructureId ) {
		$this->set( 'm_intLeaseStartStructureId', CStrings::strToIntDef( $intLeaseStartStructureId, NULL, false ) );
	}

	public function getLeaseStartStructureId() {
		return $this->m_intLeaseStartStructureId;
	}

	public function sqlLeaseStartStructureId() {
		return ( true == isset( $this->m_intLeaseStartStructureId ) ) ? ( string ) $this->m_intLeaseStartStructureId : 'NULL';
	}

	public function setDefaultLeaseStartWindowId( $intDefaultLeaseStartWindowId ) {
		$this->set( 'm_intDefaultLeaseStartWindowId', CStrings::strToIntDef( $intDefaultLeaseStartWindowId, NULL, false ) );
	}

	public function getDefaultLeaseStartWindowId() {
		return $this->m_intDefaultLeaseStartWindowId;
	}

	public function sqlDefaultLeaseStartWindowId() {
		return ( true == isset( $this->m_intDefaultLeaseStartWindowId ) ) ? ( string ) $this->m_intDefaultLeaseStartWindowId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setOffsetStartDays( $intOffsetStartDays ) {
		$this->set( 'm_intOffsetStartDays', CStrings::strToIntDef( $intOffsetStartDays, NULL, false ) );
	}

	public function getOffsetStartDays() {
		return $this->m_intOffsetStartDays;
	}

	public function sqlOffsetStartDays() {
		return ( true == isset( $this->m_intOffsetStartDays ) ) ? ( string ) $this->m_intOffsetStartDays : 'NULL';
	}

	public function setOffsetEndDays( $intOffsetEndDays ) {
		$this->set( 'm_intOffsetEndDays', CStrings::strToIntDef( $intOffsetEndDays, NULL, false ) );
	}

	public function getOffsetEndDays() {
		return $this->m_intOffsetEndDays;
	}

	public function sqlOffsetEndDays() {
		return ( true == isset( $this->m_intOffsetEndDays ) ) ? ( string ) $this->m_intOffsetEndDays : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setBillingEndDate( $strBillingEndDate ) {
		$this->set( 'm_strBillingEndDate', CStrings::strTrimDef( $strBillingEndDate, -1, NULL, true ) );
	}

	public function getBillingEndDate() {
		return $this->m_strBillingEndDate;
	}

	public function sqlBillingEndDate() {
		return ( true == isset( $this->m_strBillingEndDate ) ) ? '\'' . $this->m_strBillingEndDate . '\'' : 'NULL';
	}

	public function setRenewalStartDate( $strRenewalStartDate ) {
		$this->set( 'm_strRenewalStartDate', CStrings::strTrimDef( $strRenewalStartDate, -1, NULL, true ) );
	}

	public function getRenewalStartDate() {
		return $this->m_strRenewalStartDate;
	}

	public function sqlRenewalStartDate() {
		return ( true == isset( $this->m_strRenewalStartDate ) ) ? '\'' . $this->m_strRenewalStartDate . '\'' : 'NULL';
	}

	public function setRenewalBillingStartDate( $strRenewalBillingStartDate ) {
		$this->set( 'm_strRenewalBillingStartDate', CStrings::strTrimDef( $strRenewalBillingStartDate, -1, NULL, true ) );
	}

	public function getRenewalBillingStartDate() {
		return $this->m_strRenewalBillingStartDate;
	}

	public function sqlRenewalBillingStartDate() {
		return ( true == isset( $this->m_strRenewalBillingStartDate ) ) ? '\'' . $this->m_strRenewalBillingStartDate . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function sqlOrganizationContractId() {
		return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
	}

	public function setLeaseExtensionDate( $strLeaseExtensionDate ) {
		$this->set( 'm_strLeaseExtensionDate', CStrings::strTrimDef( $strLeaseExtensionDate, -1, NULL, true ) );
	}

	public function getLeaseExtensionDate() {
		return $this->m_strLeaseExtensionDate;
	}

	public function sqlLeaseExtensionDate() {
		return ( true == isset( $this->m_strLeaseExtensionDate ) ) ? '\'' . $this->m_strLeaseExtensionDate . '\'' : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setMinDays( $intMinDays ) {
		$this->set( 'm_intMinDays', CStrings::strToIntDef( $intMinDays, NULL, false ) );
	}

	public function getMinDays() {
		return $this->m_intMinDays;
	}

	public function sqlMinDays() {
		return ( true == isset( $this->m_intMinDays ) ) ? ( string ) $this->m_intMinDays : '0';
	}

	public function setMaxDays( $intMaxDays ) {
		$this->set( 'm_intMaxDays', CStrings::strToIntDef( $intMaxDays, NULL, false ) );
	}

	public function getMaxDays() {
		return $this->m_intMaxDays;
	}

	public function sqlMaxDays() {
		return ( true == isset( $this->m_intMaxDays ) ) ? ( string ) $this->m_intMaxDays : '0';
	}

	public function setLeaseTermTypeId( $intLeaseTermTypeId ) {
		$this->set( 'm_intLeaseTermTypeId', CStrings::strToIntDef( $intLeaseTermTypeId, NULL, false ) );
	}

	public function getLeaseTermTypeId() {
		return $this->m_intLeaseTermTypeId;
	}

	public function sqlLeaseTermTypeId() {
		return ( true == isset( $this->m_intLeaseTermTypeId ) ) ? ( string ) $this->m_intLeaseTermTypeId : 'NULL';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_start_structure_id, default_lease_start_window_id, property_id, lease_term_id, offset_start_days, offset_end_days, start_date, end_date, billing_end_date, renewal_start_date, renewal_billing_start_date, is_active, is_default, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, organization_contract_id, lease_extension_date, occupancy_type_id, min_days, max_days, lease_term_type_id, show_on_website, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlLeaseStartStructureId() . ', ' .
						$this->sqlDefaultLeaseStartWindowId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlOffsetStartDays() . ', ' .
						$this->sqlOffsetEndDays() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlBillingEndDate() . ', ' .
						$this->sqlRenewalStartDate() . ', ' .
						$this->sqlRenewalBillingStartDate() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlLeaseExtensionDate() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlMinDays() . ', ' .
						$this->sqlMaxDays() . ', ' .
						$this->sqlLeaseTermTypeId() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_structure_id = ' . $this->sqlLeaseStartStructureId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartStructureId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_structure_id = ' . $this->sqlLeaseStartStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_lease_start_window_id = ' . $this->sqlDefaultLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'DefaultLeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' default_lease_start_window_id = ' . $this->sqlDefaultLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offset_start_days = ' . $this->sqlOffsetStartDays(). ',' ; } elseif( true == array_key_exists( 'OffsetStartDays', $this->getChangedColumns() ) ) { $strSql .= ' offset_start_days = ' . $this->sqlOffsetStartDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offset_end_days = ' . $this->sqlOffsetEndDays(). ',' ; } elseif( true == array_key_exists( 'OffsetEndDays', $this->getChangedColumns() ) ) { $strSql .= ' offset_end_days = ' . $this->sqlOffsetEndDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate(). ',' ; } elseif( true == array_key_exists( 'BillingEndDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_start_date = ' . $this->sqlRenewalStartDate(). ',' ; } elseif( true == array_key_exists( 'RenewalStartDate', $this->getChangedColumns() ) ) { $strSql .= ' renewal_start_date = ' . $this->sqlRenewalStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_billing_start_date = ' . $this->sqlRenewalBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'RenewalBillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' renewal_billing_start_date = ' . $this->sqlRenewalBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_extension_date = ' . $this->sqlLeaseExtensionDate(). ',' ; } elseif( true == array_key_exists( 'LeaseExtensionDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_extension_date = ' . $this->sqlLeaseExtensionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_days = ' . $this->sqlMinDays(). ',' ; } elseif( true == array_key_exists( 'MinDays', $this->getChangedColumns() ) ) { $strSql .= ' min_days = ' . $this->sqlMinDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_days = ' . $this->sqlMaxDays(). ',' ; } elseif( true == array_key_exists( 'MaxDays', $this->getChangedColumns() ) ) { $strSql .= ' max_days = ' . $this->sqlMaxDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_type_id = ' . $this->sqlLeaseTermTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_type_id = ' . $this->sqlLeaseTermTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_start_structure_id' => $this->getLeaseStartStructureId(),
			'default_lease_start_window_id' => $this->getDefaultLeaseStartWindowId(),
			'property_id' => $this->getPropertyId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'offset_start_days' => $this->getOffsetStartDays(),
			'offset_end_days' => $this->getOffsetEndDays(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'billing_end_date' => $this->getBillingEndDate(),
			'renewal_start_date' => $this->getRenewalStartDate(),
			'renewal_billing_start_date' => $this->getRenewalBillingStartDate(),
			'is_active' => $this->getIsActive(),
			'is_default' => $this->getIsDefault(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'organization_contract_id' => $this->getOrganizationContractId(),
			'lease_extension_date' => $this->getLeaseExtensionDate(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'min_days' => $this->getMinDays(),
			'max_days' => $this->getMaxDays(),
			'lease_term_type_id' => $this->getLeaseTermTypeId(),
			'show_on_website' => $this->getShowOnWebsite(),
			'details' => $this->getDetails()
		);
	}

}
?>