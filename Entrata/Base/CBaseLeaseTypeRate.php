<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseTypeRate extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intLeaseTypeId;
    protected $m_intRateId;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->m_intLeaseTypeId = trim( $arrValues['lease_type_id'] ); else if( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
        if( isset( $arrValues['rate_id'] ) && $boolDirectSet ) $this->m_intRateId = trim( $arrValues['rate_id'] ); else if( isset( $arrValues['rate_id'] ) ) $this->setRateId( $arrValues['rate_id'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setLeaseTypeId( $intLeaseTypeId ) {
        $this->m_intLeaseTypeId = CStrings::strToIntDef( $intLeaseTypeId, NULL, false );
    }

    public function getLeaseTypeId() {
        return $this->m_intLeaseTypeId;
    }

    public function sqlLeaseTypeId() {
        return ( true == isset( $this->m_intLeaseTypeId ) ) ? (string) $this->m_intLeaseTypeId : 'NULL';
    }

    public function setRateId( $intRateId ) {
        $this->m_intRateId = CStrings::strToDoubleDef( $intRateId, NULL, false );
    }

    public function getRateId() {
        return $this->m_intRateId;
    }

    public function sqlRateId() {
        return ( true == isset( $this->m_intRateId ) ) ? ( double ) $this->m_intRateId : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.lease_type_rates_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.lease_type_rates
					VALUES ( ' .
	                    $strId . ', ' .
 		                $this->sqlCid() . ', ' .
 		                $this->sqlLeaseTypeId() . ', ' .
 		                $this->sqlRateId() . ', ' .
                    	(int) $intCurrentUserId . ', ' .
 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.lease_type_rates
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseTypeId() ) != $this->getOriginalValueByFieldName ( 'lease_type_id' ) ) { $arrstrOriginalValueChanges['lease_type_id'] = $this->sqlLeaseTypeId(); $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_id = ' . $this->sqlRateId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRateId() ) != $this->getOriginalValueByFieldName ( 'rate_id' ) ) { $arrstrOriginalValueChanges['rate_id'] = $this->sqlRateId(); $strSql .= ' rate_id = ' . $this->sqlRateId() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE
						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ) {
			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.lease_type_rates WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.lease_type_rates_id_seq', $objDatabase );
    }

}
?>