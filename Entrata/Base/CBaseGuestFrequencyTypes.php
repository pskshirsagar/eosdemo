<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestFrequencyTypes
 * Do not add any new functions to this class.
 */

class CBaseGuestFrequencyTypes extends CEosPluralBase {

	/**
	 * @return CGuestFrequencyType[]
	 */
	public static function fetchGuestFrequencyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGuestFrequencyType::class, $objDatabase );
	}

	/**
	 * @return CGuestFrequencyType
	 */
	public static function fetchGuestFrequencyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGuestFrequencyType::class, $objDatabase );
	}

	public static function fetchGuestFrequencyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'guest_frequency_types', $objDatabase );
	}

	public static function fetchGuestFrequencyTypeById( $intId, $objDatabase ) {
		return self::fetchGuestFrequencyType( sprintf( 'SELECT * FROM guest_frequency_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>