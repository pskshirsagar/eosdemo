<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePackageBatches extends CEosPluralBase {

	/**
	 * @return CPackageBatch[]
	 */
	public static function fetchPackageBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPackageBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPackageBatch
	 */
	public static function fetchPackageBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPackageBatch', $objDatabase );
	}

	public static function fetchPackageBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'package_batches', $objDatabase );
	}

	public static function fetchPackageBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPackageBatch( sprintf( 'SELECT * FROM package_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPackageBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchPackageBatches( sprintf( 'SELECT * FROM package_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPackageBatchesByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchPackageBatches( sprintf( 'SELECT * FROM package_batches WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>