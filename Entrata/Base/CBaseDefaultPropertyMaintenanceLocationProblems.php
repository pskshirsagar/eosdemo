<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultPropertyMaintenanceLocationProblems
 * Do not add any new functions to this class.
 */

class CBaseDefaultPropertyMaintenanceLocationProblems extends CEosPluralBase {

	/**
	 * @return CDefaultPropertyMaintenanceLocationProblem[]
	 */
	public static function fetchDefaultPropertyMaintenanceLocationProblems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultPropertyMaintenanceLocationProblem', $objDatabase );
	}

	/**
	 * @return CDefaultPropertyMaintenanceLocationProblem
	 */
	public static function fetchDefaultPropertyMaintenanceLocationProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultPropertyMaintenanceLocationProblem', $objDatabase );
	}

	public static function fetchDefaultPropertyMaintenanceLocationProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_property_maintenance_location_problems', $objDatabase );
	}

	public static function fetchDefaultPropertyMaintenanceLocationProblemById( $intId, $objDatabase ) {
		return self::fetchDefaultPropertyMaintenanceLocationProblem( sprintf( 'SELECT * FROM default_property_maintenance_location_problems WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultPropertyMaintenanceLocationProblemsByDefaultMaintenanceLocationId( $intDefaultMaintenanceLocationId, $objDatabase ) {
		return self::fetchDefaultPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM default_property_maintenance_location_problems WHERE default_maintenance_location_id = %d', ( int ) $intDefaultMaintenanceLocationId ), $objDatabase );
	}

	public static function fetchDefaultPropertyMaintenanceLocationProblemsByDefaultMaintenanceProblemId( $intDefaultMaintenanceProblemId, $objDatabase ) {
		return self::fetchDefaultPropertyMaintenanceLocationProblems( sprintf( 'SELECT * FROM default_property_maintenance_location_problems WHERE default_maintenance_problem_id = %d', ( int ) $intDefaultMaintenanceProblemId ), $objDatabase );
	}

}
?>