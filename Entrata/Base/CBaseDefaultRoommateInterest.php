<?php

class CBaseDefaultRoommateInterest extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_roommate_interests';

	protected $m_intId;
	protected $m_intInputTypeId;
	protected $m_intRoommateInterestCategoryId;
	protected $m_strSystemCode;
	protected $m_strQuestion;
	protected $m_intMatchingWeight;
	protected $m_intIsRequired;
	protected $m_intIsPublished;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intMatchingWeight = '1';
		$this->m_intIsRequired = '1';
		$this->m_intIsPublished = '1';
		$this->m_intIsSystem = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['input_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInputTypeId', trim( $arrValues['input_type_id'] ) ); elseif( isset( $arrValues['input_type_id'] ) ) $this->setInputTypeId( $arrValues['input_type_id'] );
		if( isset( $arrValues['roommate_interest_category_id'] ) && $boolDirectSet ) $this->set( 'm_intRoommateInterestCategoryId', trim( $arrValues['roommate_interest_category_id'] ) ); elseif( isset( $arrValues['roommate_interest_category_id'] ) ) $this->setRoommateInterestCategoryId( $arrValues['roommate_interest_category_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['question'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['matching_weight'] ) && $boolDirectSet ) $this->set( 'm_intMatchingWeight', trim( $arrValues['matching_weight'] ) ); elseif( isset( $arrValues['matching_weight'] ) ) $this->setMatchingWeight( $arrValues['matching_weight'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInputTypeId( $intInputTypeId ) {
		$this->set( 'm_intInputTypeId', CStrings::strToIntDef( $intInputTypeId, NULL, false ) );
	}

	public function getInputTypeId() {
		return $this->m_intInputTypeId;
	}

	public function sqlInputTypeId() {
		return ( true == isset( $this->m_intInputTypeId ) ) ? ( string ) $this->m_intInputTypeId : 'NULL';
	}

	public function setRoommateInterestCategoryId( $intRoommateInterestCategoryId ) {
		$this->set( 'm_intRoommateInterestCategoryId', CStrings::strToIntDef( $intRoommateInterestCategoryId, NULL, false ) );
	}

	public function getRoommateInterestCategoryId() {
		return $this->m_intRoommateInterestCategoryId;
	}

	public function sqlRoommateInterestCategoryId() {
		return ( true == isset( $this->m_intRoommateInterestCategoryId ) ) ? ( string ) $this->m_intRoommateInterestCategoryId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setQuestion( $strQuestion, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, 240, NULL, true ), $strLocaleCode );
	}

	public function getQuestion( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strQuestion', $strLocaleCode );
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setMatchingWeight( $intMatchingWeight ) {
		$this->set( 'm_intMatchingWeight', CStrings::strToIntDef( $intMatchingWeight, NULL, false ) );
	}

	public function getMatchingWeight() {
		return $this->m_intMatchingWeight;
	}

	public function sqlMatchingWeight() {
		return ( true == isset( $this->m_intMatchingWeight ) ) ? ( string ) $this->m_intMatchingWeight : '1';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'input_type_id' => $this->getInputTypeId(),
			'roommate_interest_category_id' => $this->getRoommateInterestCategoryId(),
			'system_code' => $this->getSystemCode(),
			'question' => $this->getQuestion(),
			'matching_weight' => $this->getMatchingWeight(),
			'is_required' => $this->getIsRequired(),
			'is_published' => $this->getIsPublished(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>