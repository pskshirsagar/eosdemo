<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestLaborTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceRequestLaborTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestLaborType[]
	 */
	public static function fetchMaintenanceRequestLaborTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestLaborType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceRequestLaborType
	 */
	public static function fetchMaintenanceRequestLaborType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestLaborType::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_labor_types', $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceRequestLaborType( sprintf( 'SELECT * FROM maintenance_request_labor_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>