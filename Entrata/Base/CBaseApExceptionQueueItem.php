<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApExceptionQueueItem extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ap_exception_queue_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityBillId;
	protected $m_intBillEscalationId;
	protected $m_intBillEscalateTypeId;
	protected $m_intOrderHeaderId;
	protected $m_intApHeaderId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strMissingItems;
	protected $m_jsonMissingItems;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_intCancelledBy;
	protected $m_strCancelledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['bill_escalation_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalationId', trim( $arrValues['bill_escalation_id'] ) ); elseif( isset( $arrValues['bill_escalation_id'] ) ) $this->setBillEscalationId( $arrValues['bill_escalation_id'] );
		if( isset( $arrValues['bill_escalate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalateTypeId', trim( $arrValues['bill_escalate_type_id'] ) ); elseif( isset( $arrValues['bill_escalate_type_id'] ) ) $this->setBillEscalateTypeId( $arrValues['bill_escalate_type_id'] );
		if( isset( $arrValues['order_header_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderHeaderId', trim( $arrValues['order_header_id'] ) ); elseif( isset( $arrValues['order_header_id'] ) ) $this->setOrderHeaderId( $arrValues['order_header_id'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['missing_items'] ) ) $this->set( 'm_strMissingItems', trim( $arrValues['missing_items'] ) );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['cancelled_by'] ) && $boolDirectSet ) $this->set( 'm_intCancelledBy', trim( $arrValues['cancelled_by'] ) ); elseif( isset( $arrValues['cancelled_by'] ) ) $this->setCancelledBy( $arrValues['cancelled_by'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setBillEscalationId( $intBillEscalationId ) {
		$this->set( 'm_intBillEscalationId', CStrings::strToIntDef( $intBillEscalationId, NULL, false ) );
	}

	public function getBillEscalationId() {
		return $this->m_intBillEscalationId;
	}

	public function sqlBillEscalationId() {
		return ( true == isset( $this->m_intBillEscalationId ) ) ? ( string ) $this->m_intBillEscalationId : 'NULL';
	}

	public function setBillEscalateTypeId( $intBillEscalateTypeId ) {
		$this->set( 'm_intBillEscalateTypeId', CStrings::strToIntDef( $intBillEscalateTypeId, NULL, false ) );
	}

	public function getBillEscalateTypeId() {
		return $this->m_intBillEscalateTypeId;
	}

	public function sqlBillEscalateTypeId() {
		return ( true == isset( $this->m_intBillEscalateTypeId ) ) ? ( string ) $this->m_intBillEscalateTypeId : 'NULL';
	}

	public function setOrderHeaderId( $intOrderHeaderId ) {
		$this->set( 'm_intOrderHeaderId', CStrings::strToIntDef( $intOrderHeaderId, NULL, false ) );
	}

	public function getOrderHeaderId() {
		return $this->m_intOrderHeaderId;
	}

	public function sqlOrderHeaderId() {
		return ( true == isset( $this->m_intOrderHeaderId ) ) ? ( string ) $this->m_intOrderHeaderId : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setMissingItems( $jsonMissingItems ) {
		if( true == valObj( $jsonMissingItems, 'stdClass' ) ) {
			$this->set( 'm_jsonMissingItems', $jsonMissingItems );
		} elseif( true == valJsonString( $jsonMissingItems ) ) {
			$this->set( 'm_jsonMissingItems', CStrings::strToJson( $jsonMissingItems ) );
		} else {
			$this->set( 'm_jsonMissingItems', NULL ); 
		}
		unset( $this->m_strMissingItems );
	}

	public function getMissingItems() {
		if( true == isset( $this->m_strMissingItems ) ) {
			$this->m_jsonMissingItems = CStrings::strToJson( $this->m_strMissingItems );
			unset( $this->m_strMissingItems );
		}
		return $this->m_jsonMissingItems;
	}

	public function sqlMissingItems() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getMissingItems() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getMissingItems() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setCancelledBy( $intCancelledBy ) {
		$this->set( 'm_intCancelledBy', CStrings::strToIntDef( $intCancelledBy, NULL, false ) );
	}

	public function getCancelledBy() {
		return $this->m_intCancelledBy;
	}

	public function sqlCancelledBy() {
		return ( true == isset( $this->m_intCancelledBy ) ) ? ( string ) $this->m_intCancelledBy : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_bill_id, bill_escalation_id, bill_escalate_type_id, order_header_id, ap_header_id, details, missing_items, resolved_by, resolved_on, processed_by, processed_on, cancelled_by, cancelled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlBillEscalationId() . ', ' .
						$this->sqlBillEscalateTypeId() . ', ' .
						$this->sqlOrderHeaderId() . ', ' .
						$this->sqlApHeaderId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMissingItems() . ', ' .
						$this->sqlResolvedBy() . ', ' .
						$this->sqlResolvedOn() . ', ' .
						$this->sqlProcessedBy() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlCancelledBy() . ', ' .
						$this->sqlCancelledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalation_id = ' . $this->sqlBillEscalationId(). ',' ; } elseif( true == array_key_exists( 'BillEscalationId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalation_id = ' . $this->sqlBillEscalationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId(). ',' ; } elseif( true == array_key_exists( 'BillEscalateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId(). ',' ; } elseif( true == array_key_exists( 'OrderHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_items = ' . $this->sqlMissingItems(). ',' ; } elseif( true == array_key_exists( 'MissingItems', $this->getChangedColumns() ) ) { $strSql .= ' missing_items = ' . $this->sqlMissingItems() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy(). ',' ; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn(). ',' ; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy(). ',' ; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy(). ',' ; } elseif( true == array_key_exists( 'CancelledBy', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'bill_escalation_id' => $this->getBillEscalationId(),
			'bill_escalate_type_id' => $this->getBillEscalateTypeId(),
			'order_header_id' => $this->getOrderHeaderId(),
			'ap_header_id' => $this->getApHeaderId(),
			'details' => $this->getDetails(),
			'missing_items' => $this->getMissingItems(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'cancelled_by' => $this->getCancelledBy(),
			'cancelled_on' => $this->getCancelledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>