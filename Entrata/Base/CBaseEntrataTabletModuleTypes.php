<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntrataTabletModuleTypes
 * Do not add any new functions to this class.
 */

class CBaseEntrataTabletModuleTypes extends CEosPluralBase {

	/**
	 * @return CEntrataTabletModuleType[]
	 */
	public static function fetchEntrataTabletModuleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEntrataTabletModuleType::class, $objDatabase );
	}

	/**
	 * @return CEntrataTabletModuleType
	 */
	public static function fetchEntrataTabletModuleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEntrataTabletModuleType::class, $objDatabase );
	}

	public static function fetchEntrataTabletModuleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entrata_tablet_module_types', $objDatabase );
	}

	public static function fetchEntrataTabletModuleTypeById( $intId, $objDatabase ) {
		return self::fetchEntrataTabletModuleType( sprintf( 'SELECT * FROM entrata_tablet_module_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>