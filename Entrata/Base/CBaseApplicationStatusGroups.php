<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStatusGroups
 * Do not add any new functions to this class.
 */

class CBaseApplicationStatusGroups extends CEosPluralBase {

	/**
	 * @return CApplicationStatusGroup[]
	 */
	public static function fetchApplicationStatusGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationStatusGroup::class, $objDatabase );
	}

	/**
	 * @return CApplicationStatusGroup
	 */
	public static function fetchApplicationStatusGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationStatusGroup::class, $objDatabase );
	}

	public static function fetchApplicationStatusGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_status_groups', $objDatabase );
	}

	public static function fetchApplicationStatusGroupById( $intId, $objDatabase ) {
		return self::fetchApplicationStatusGroup( sprintf( 'SELECT * FROM application_status_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>