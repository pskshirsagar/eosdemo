<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentTypes extends CEosPluralBase {

	/**
	 * @return CDocumentType[]
	 */
	public static function fetchDocumentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDocumentType::class, $objDatabase );
	}

	/**
	 * @return CDocumentType
	 */
	public static function fetchDocumentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentType::class, $objDatabase );
	}

	public static function fetchDocumentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_types', $objDatabase );
	}

	public static function fetchDocumentTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentType( sprintf( 'SELECT * FROM document_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>