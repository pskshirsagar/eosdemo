<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySignatureExceptionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySignatureExceptionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySignatureExceptionType[]
	 */
	public static function fetchSubsidySignatureExceptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySignatureExceptionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySignatureExceptionType
	 */
	public static function fetchSubsidySignatureExceptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySignatureExceptionType::class, $objDatabase );
	}

	public static function fetchSubsidySignatureExceptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_signature_exception_types', $objDatabase );
	}

	public static function fetchSubsidySignatureExceptionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySignatureExceptionType( sprintf( 'SELECT * FROM subsidy_signature_exception_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>