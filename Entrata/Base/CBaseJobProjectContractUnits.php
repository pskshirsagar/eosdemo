<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectContractUnits
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobProjectContractUnits extends CEosPluralBase {

	/**
	 * @return CJobProjectContractUnit[]
	 */
	public static function fetchJobProjectContractUnits( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobProjectContractUnit', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobProjectContractUnit
	 */
	public static function fetchJobProjectContractUnit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobProjectContractUnit', $objDatabase );
	}

	public static function fetchJobProjectContractUnitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_project_contract_units', $objDatabase );
	}

	public static function fetchJobProjectContractUnitByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnit( sprintf( 'SELECT * FROM job_project_contract_units WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractUnitsByCid( $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnits( sprintf( 'SELECT * FROM job_project_contract_units WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractUnitsByJobProjectIdByCid( $intJobProjectId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnits( sprintf( 'SELECT * FROM job_project_contract_units WHERE job_project_id = %d AND cid = %d', ( int ) $intJobProjectId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractUnitsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnits( sprintf( 'SELECT * FROM job_project_contract_units WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractUnitsByContractIdByCid( $intContractId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnits( sprintf( 'SELECT * FROM job_project_contract_units WHERE contract_id = %d AND cid = %d', ( int ) $intContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobProjectContractUnitsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchJobProjectContractUnits( sprintf( 'SELECT * FROM job_project_contract_units WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>