<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplications extends CEosPluralBase {

	/**
	 * @return CApplication[]
	 */
	public static function fetchApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplication::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplication
	 */
	public static function fetchApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplication::class, $objDatabase );
	}

	public static function fetchApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applications', $objDatabase );
	}

	public static function fetchApplicationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplication( sprintf( 'SELECT * FROM applications WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCid( $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE lease_interval_id = %d AND cid = %d', $intLeaseIntervalId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE lease_type_id = %d AND cid = %d', $intLeaseTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByApplicationStageIdByCid( $intApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE application_stage_id = %d AND cid = %d', $intApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByApplicationStatusIdByCid( $intApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE application_status_id = %d AND cid = %d', $intApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByApplicationStepIdByCid( $intApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE application_step_id = %d AND cid = %d', $intApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByMaxApplicationStageIdByCid( $intMaxApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE max_application_stage_id = %d AND cid = %d', $intMaxApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByMaxApplicationStatusIdByCid( $intMaxApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE max_application_status_id = %d AND cid = %d', $intMaxApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByMaxApplicationStepIdByCid( $intMaxApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE max_application_step_id = %d AND cid = %d', $intMaxApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCancellationListTypeIdByCid( $intCancellationListTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE cancellation_list_type_id = %d AND cid = %d', $intCancellationListTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCancellationListItemIdByCid( $intCancellationListItemId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE cancellation_list_item_id = %d AND cid = %d', $intCancellationListItemId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPrimaryApplicantIdByCid( $intPrimaryApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE primary_applicant_id = %d AND cid = %d', $intPrimaryApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByUnitKindIdByCid( $intUnitKindId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE unit_kind_id = %d AND cid = %d', $intUnitKindId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE space_configuration_id = %d AND cid = %d', $intSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByDesiredSpaceConfigurationIdByCid( $intDesiredSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE desired_space_configuration_id = %d AND cid = %d', $intDesiredSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE screening_id = %d AND cid = %d', $intScreeningId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByApplicationPsProductIdByCid( $intApplicationPsProductId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE application_ps_product_id = %d AND cid = %d', $intApplicationPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeasePsProductIdByCid( $intLeasePsProductId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE lease_ps_product_id = %d AND cid = %d', $intLeasePsProductId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeasingAgentIdByCid( $intLeasingAgentId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE leasing_agent_id = %d AND cid = %d', $intLeasingAgentId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByInternetListingServiceIdByCid( $intInternetListingServiceId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE internet_listing_service_id = %d AND cid = %d', $intInternetListingServiceId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByOriginatingLeadSourceIdByCid( $intOriginatingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE originating_lead_source_id = %d AND cid = %d', $intOriginatingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByConvertingLeadSourceIdByCid( $intConvertingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE converting_lead_source_id = %d AND cid = %d', $intConvertingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE company_application_id = %d AND cid = %d', $intCompanyApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCombinedApplicationIdByCid( $intCombinedApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE combined_application_id = %d AND cid = %d', $intCombinedApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE quote_id = %d AND cid = %d', $intQuoteId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsBySemAdGroupIdByCid( $intSemAdGroupId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE sem_ad_group_id = %d AND cid = %d', $intSemAdGroupId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsBySemKeywordIdByCid( $intSemKeywordId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE sem_keyword_id = %d AND cid = %d', $intSemKeywordId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsBySemSourceIdByCid( $intSemSourceId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE sem_source_id = %d AND cid = %d', $intSemSourceId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE call_id = %d AND cid = %d', $intCallId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE website_id = %d AND cid = %d', $intWebsiteId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByCraigslistPostIdByCid( $intCraigslistPostId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE craigslist_post_id = %d AND cid = %d', $intCraigslistPostId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByClAdIdByCid( $intClAdId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE cl_ad_id = %d AND cid = %d', $intClAdId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByTrafficCookieIdByCid( $intTrafficCookieId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE traffic_cookie_id = %d AND cid = %d', $intTrafficCookieId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByFirstEventIdByCid( $intFirstEventId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE first_event_id = %d AND cid = %d', $intFirstEventId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByFirstEventMediumIdByCid( $intFirstEventMediumId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE first_event_medium_id = %d AND cid = %d', $intFirstEventMediumId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByFirstLeasingCenterEventIdByCid( $intFirstLeasingCenterEventId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE first_leasing_center_event_id = %d AND cid = %d', $intFirstLeasingCenterEventId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByResponseEventIdByCid( $intResponseEventId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE response_event_id = %d AND cid = %d', $intResponseEventId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLastEventIdByCid( $intLastEventId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE last_event_id = %d AND cid = %d', $intLastEventId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByReferringLeaseCustomerIdByCid( $intReferringLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE referring_lease_customer_id = %d AND cid = %d', $intReferringLeaseCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByLeaseSignatureTypeIdByCid( $intLeaseSignatureTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE lease_signature_type_id = %d AND cid = %d', $intLeaseSignatureTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByInsurancePolicyIdByCid( $intInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE insurance_policy_id = %d AND cid = %d', $intInsurancePolicyId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByInsurancePolicyStatusTypeIdByCid( $intInsurancePolicyStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE insurance_policy_status_type_id = %d AND cid = %d', $intInsurancePolicyStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApplicationsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchApplications( sprintf( 'SELECT * FROM applications WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

}
?>