<?php

class CBaseArOrigin extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ar_origins';

	protected $m_intId;
	protected $m_arrintArTriggerIds;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCategoryDescription;
	protected $m_strGroupDescription;
	protected $m_boolAllowGlobalConfig;
	protected $m_boolAllowPropertyConfig;
	protected $m_boolAllowUnitTypeConfig;
	protected $m_boolAllowUnitSpaceConfig;
	protected $m_boolRequireArCodeTriggerMapping;
	protected $m_boolRequireReferenceAssociation;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowGlobalConfig = false;
		$this->m_boolAllowPropertyConfig = false;
		$this->m_boolAllowUnitTypeConfig = false;
		$this->m_boolAllowUnitSpaceConfig = false;
		$this->m_boolRequireArCodeTriggerMapping = false;
		$this->m_boolRequireReferenceAssociation = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ar_trigger_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintArTriggerIds', trim( $arrValues['ar_trigger_ids'] ) ); elseif( isset( $arrValues['ar_trigger_ids'] ) ) $this->setArTriggerIds( $arrValues['ar_trigger_ids'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['category_description'] ) && $boolDirectSet ) $this->set( 'm_strCategoryDescription', trim( stripcslashes( $arrValues['category_description'] ) ) ); elseif( isset( $arrValues['category_description'] ) ) $this->setCategoryDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['category_description'] ) : $arrValues['category_description'] );
		if( isset( $arrValues['group_description'] ) && $boolDirectSet ) $this->set( 'm_strGroupDescription', trim( stripcslashes( $arrValues['group_description'] ) ) ); elseif( isset( $arrValues['group_description'] ) ) $this->setGroupDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['group_description'] ) : $arrValues['group_description'] );
		if( isset( $arrValues['allow_global_config'] ) && $boolDirectSet ) $this->set( 'm_boolAllowGlobalConfig', trim( stripcslashes( $arrValues['allow_global_config'] ) ) ); elseif( isset( $arrValues['allow_global_config'] ) ) $this->setAllowGlobalConfig( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_global_config'] ) : $arrValues['allow_global_config'] );
		if( isset( $arrValues['allow_property_config'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPropertyConfig', trim( stripcslashes( $arrValues['allow_property_config'] ) ) ); elseif( isset( $arrValues['allow_property_config'] ) ) $this->setAllowPropertyConfig( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_property_config'] ) : $arrValues['allow_property_config'] );
		if( isset( $arrValues['allow_unit_type_config'] ) && $boolDirectSet ) $this->set( 'm_boolAllowUnitTypeConfig', trim( stripcslashes( $arrValues['allow_unit_type_config'] ) ) ); elseif( isset( $arrValues['allow_unit_type_config'] ) ) $this->setAllowUnitTypeConfig( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_unit_type_config'] ) : $arrValues['allow_unit_type_config'] );
		if( isset( $arrValues['allow_unit_space_config'] ) && $boolDirectSet ) $this->set( 'm_boolAllowUnitSpaceConfig', trim( stripcslashes( $arrValues['allow_unit_space_config'] ) ) ); elseif( isset( $arrValues['allow_unit_space_config'] ) ) $this->setAllowUnitSpaceConfig( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_unit_space_config'] ) : $arrValues['allow_unit_space_config'] );
		if( isset( $arrValues['require_ar_code_trigger_mapping'] ) && $boolDirectSet ) $this->set( 'm_boolRequireArCodeTriggerMapping', trim( stripcslashes( $arrValues['require_ar_code_trigger_mapping'] ) ) ); elseif( isset( $arrValues['require_ar_code_trigger_mapping'] ) ) $this->setRequireArCodeTriggerMapping( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_ar_code_trigger_mapping'] ) : $arrValues['require_ar_code_trigger_mapping'] );
		if( isset( $arrValues['require_reference_association'] ) && $boolDirectSet ) $this->set( 'm_boolRequireReferenceAssociation', trim( stripcslashes( $arrValues['require_reference_association'] ) ) ); elseif( isset( $arrValues['require_reference_association'] ) ) $this->setRequireReferenceAssociation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_reference_association'] ) : $arrValues['require_reference_association'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setArTriggerIds( $arrintArTriggerIds ) {
		$this->set( 'm_arrintArTriggerIds', CStrings::strToArrIntDef( $arrintArTriggerIds, NULL ) );
	}

	public function getArTriggerIds() {
		return $this->m_arrintArTriggerIds;
	}

	public function sqlArTriggerIds() {
		return ( true == isset( $this->m_arrintArTriggerIds ) && true == valArr( $this->m_arrintArTriggerIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintArTriggerIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCategoryDescription( $strCategoryDescription ) {
		$this->set( 'm_strCategoryDescription', CStrings::strTrimDef( $strCategoryDescription, 240, NULL, true ) );
	}

	public function getCategoryDescription() {
		return $this->m_strCategoryDescription;
	}

	public function sqlCategoryDescription() {
		return ( true == isset( $this->m_strCategoryDescription ) ) ? '\'' . addslashes( $this->m_strCategoryDescription ) . '\'' : 'NULL';
	}

	public function setGroupDescription( $strGroupDescription ) {
		$this->set( 'm_strGroupDescription', CStrings::strTrimDef( $strGroupDescription, 240, NULL, true ) );
	}

	public function getGroupDescription() {
		return $this->m_strGroupDescription;
	}

	public function sqlGroupDescription() {
		return ( true == isset( $this->m_strGroupDescription ) ) ? '\'' . addslashes( $this->m_strGroupDescription ) . '\'' : 'NULL';
	}

	public function setAllowGlobalConfig( $boolAllowGlobalConfig ) {
		$this->set( 'm_boolAllowGlobalConfig', CStrings::strToBool( $boolAllowGlobalConfig ) );
	}

	public function getAllowGlobalConfig() {
		return $this->m_boolAllowGlobalConfig;
	}

	public function sqlAllowGlobalConfig() {
		return ( true == isset( $this->m_boolAllowGlobalConfig ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowGlobalConfig ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPropertyConfig( $boolAllowPropertyConfig ) {
		$this->set( 'm_boolAllowPropertyConfig', CStrings::strToBool( $boolAllowPropertyConfig ) );
	}

	public function getAllowPropertyConfig() {
		return $this->m_boolAllowPropertyConfig;
	}

	public function sqlAllowPropertyConfig() {
		return ( true == isset( $this->m_boolAllowPropertyConfig ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPropertyConfig ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowUnitTypeConfig( $boolAllowUnitTypeConfig ) {
		$this->set( 'm_boolAllowUnitTypeConfig', CStrings::strToBool( $boolAllowUnitTypeConfig ) );
	}

	public function getAllowUnitTypeConfig() {
		return $this->m_boolAllowUnitTypeConfig;
	}

	public function sqlAllowUnitTypeConfig() {
		return ( true == isset( $this->m_boolAllowUnitTypeConfig ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowUnitTypeConfig ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowUnitSpaceConfig( $boolAllowUnitSpaceConfig ) {
		$this->set( 'm_boolAllowUnitSpaceConfig', CStrings::strToBool( $boolAllowUnitSpaceConfig ) );
	}

	public function getAllowUnitSpaceConfig() {
		return $this->m_boolAllowUnitSpaceConfig;
	}

	public function sqlAllowUnitSpaceConfig() {
		return ( true == isset( $this->m_boolAllowUnitSpaceConfig ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowUnitSpaceConfig ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireArCodeTriggerMapping( $boolRequireArCodeTriggerMapping ) {
		$this->set( 'm_boolRequireArCodeTriggerMapping', CStrings::strToBool( $boolRequireArCodeTriggerMapping ) );
	}

	public function getRequireArCodeTriggerMapping() {
		return $this->m_boolRequireArCodeTriggerMapping;
	}

	public function sqlRequireArCodeTriggerMapping() {
		return ( true == isset( $this->m_boolRequireArCodeTriggerMapping ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireArCodeTriggerMapping ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireReferenceAssociation( $boolRequireReferenceAssociation ) {
		$this->set( 'm_boolRequireReferenceAssociation', CStrings::strToBool( $boolRequireReferenceAssociation ) );
	}

	public function getRequireReferenceAssociation() {
		return $this->m_boolRequireReferenceAssociation;
	}

	public function sqlRequireReferenceAssociation() {
		return ( true == isset( $this->m_boolRequireReferenceAssociation ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireReferenceAssociation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ar_trigger_ids' => $this->getArTriggerIds(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'category_description' => $this->getCategoryDescription(),
			'group_description' => $this->getGroupDescription(),
			'allow_global_config' => $this->getAllowGlobalConfig(),
			'allow_property_config' => $this->getAllowPropertyConfig(),
			'allow_unit_type_config' => $this->getAllowUnitTypeConfig(),
			'allow_unit_space_config' => $this->getAllowUnitSpaceConfig(),
			'require_ar_code_trigger_mapping' => $this->getRequireArCodeTriggerMapping(),
			'require_reference_association' => $this->getRequireReferenceAssociation(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>