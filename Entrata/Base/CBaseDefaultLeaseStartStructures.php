<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseStartStructures
 * Do not add any new functions to this class.
 */

class CBaseDefaultLeaseStartStructures extends CEosPluralBase {

	/**
	 * @return CDefaultLeaseStartStructure[]
	 */
	public static function fetchDefaultLeaseStartStructures( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultLeaseStartStructure::class, $objDatabase );
	}

	/**
	 * @return CDefaultLeaseStartStructure
	 */
	public static function fetchDefaultLeaseStartStructure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultLeaseStartStructure::class, $objDatabase );
	}

	public static function fetchDefaultLeaseStartStructureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_lease_start_structures', $objDatabase );
	}

	public static function fetchDefaultLeaseStartStructureById( $intId, $objDatabase ) {
		return self::fetchDefaultLeaseStartStructure( sprintf( 'SELECT * FROM default_lease_start_structures WHERE id = %d', $intId ), $objDatabase );
	}

}
?>