<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyWaiverTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyWaiverTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyWaiverType[]
	 */
	public static function fetchSubsidyWaiverTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyWaiverType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyWaiverType
	 */
	public static function fetchSubsidyWaiverType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyWaiverType::class, $objDatabase );
	}

	public static function fetchSubsidyWaiverTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_waiver_types', $objDatabase );
	}

	public static function fetchSubsidyWaiverTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyWaiverType( sprintf( 'SELECT * FROM subsidy_waiver_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>