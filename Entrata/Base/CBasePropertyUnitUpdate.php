<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUnitUpdate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_unit_updates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitIdCurrent;
	protected $m_intPropertyUnitIdNew;
	protected $m_boolUnitChanged;
	protected $m_strUnitNumberOld;
	protected $m_strUnitNumberNew;
	protected $m_intUnitSpaceId;
	protected $m_strSpaceNumber;
	protected $m_intUnitNumberPatternId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolUnitChanged = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id_current'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitIdCurrent', trim( $arrValues['property_unit_id_current'] ) ); elseif( isset( $arrValues['property_unit_id_current'] ) ) $this->setPropertyUnitIdCurrent( $arrValues['property_unit_id_current'] );
		if( isset( $arrValues['property_unit_id_new'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitIdNew', trim( $arrValues['property_unit_id_new'] ) ); elseif( isset( $arrValues['property_unit_id_new'] ) ) $this->setPropertyUnitIdNew( $arrValues['property_unit_id_new'] );
		if( isset( $arrValues['unit_changed'] ) && $boolDirectSet ) $this->set( 'm_boolUnitChanged', trim( stripcslashes( $arrValues['unit_changed'] ) ) ); elseif( isset( $arrValues['unit_changed'] ) ) $this->setUnitChanged( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_changed'] ) : $arrValues['unit_changed'] );
		if( isset( $arrValues['unit_number_old'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitNumberOld', trim( stripcslashes( $arrValues['unit_number_old'] ) ) ); elseif( isset( $arrValues['unit_number_old'] ) ) $this->setUnitNumberOld( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_old'] ) : $arrValues['unit_number_old'] );
		if( isset( $arrValues['unit_number_new'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitNumberNew', trim( stripcslashes( $arrValues['unit_number_new'] ) ) ); elseif( isset( $arrValues['unit_number_new'] ) ) $this->setUnitNumberNew( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_new'] ) : $arrValues['unit_number_new'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['space_number'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSpaceNumber', trim( stripcslashes( $arrValues['space_number'] ) ) ); elseif( isset( $arrValues['space_number'] ) ) $this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['space_number'] ) : $arrValues['space_number'] );
		if( isset( $arrValues['unit_number_pattern_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitNumberPatternId', trim( $arrValues['unit_number_pattern_id'] ) ); elseif( isset( $arrValues['unit_number_pattern_id'] ) ) $this->setUnitNumberPatternId( $arrValues['unit_number_pattern_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitIdCurrent( $intPropertyUnitIdCurrent ) {
		$this->set( 'm_intPropertyUnitIdCurrent', CStrings::strToIntDef( $intPropertyUnitIdCurrent, NULL, false ) );
	}

	public function getPropertyUnitIdCurrent() {
		return $this->m_intPropertyUnitIdCurrent;
	}

	public function sqlPropertyUnitIdCurrent() {
		return ( true == isset( $this->m_intPropertyUnitIdCurrent ) ) ? ( string ) $this->m_intPropertyUnitIdCurrent : 'NULL';
	}

	public function setPropertyUnitIdNew( $intPropertyUnitIdNew ) {
		$this->set( 'm_intPropertyUnitIdNew', CStrings::strToIntDef( $intPropertyUnitIdNew, NULL, false ) );
	}

	public function getPropertyUnitIdNew() {
		return $this->m_intPropertyUnitIdNew;
	}

	public function sqlPropertyUnitIdNew() {
		return ( true == isset( $this->m_intPropertyUnitIdNew ) ) ? ( string ) $this->m_intPropertyUnitIdNew : 'NULL';
	}

	public function setUnitChanged( $boolUnitChanged ) {
		$this->set( 'm_boolUnitChanged', CStrings::strToBool( $boolUnitChanged ) );
	}

	public function getUnitChanged() {
		return $this->m_boolUnitChanged;
	}

	public function sqlUnitChanged() {
		return ( true == isset( $this->m_boolUnitChanged ) ) ? '\'' . ( true == ( bool ) $this->m_boolUnitChanged ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUnitNumberOld( $strUnitNumberOld, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitNumberOld', CStrings::strTrimDef( $strUnitNumberOld, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitNumberOld( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitNumberOld', $strLocaleCode );
	}

	public function sqlUnitNumberOld() {
		return ( true == isset( $this->m_strUnitNumberOld ) ) ? '\'' . addslashes( $this->m_strUnitNumberOld ) . '\'' : 'NULL';
	}

	public function setUnitNumberNew( $strUnitNumberNew, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitNumberNew', CStrings::strTrimDef( $strUnitNumberNew, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitNumberNew( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitNumberNew', $strLocaleCode );
	}

	public function sqlUnitNumberNew() {
		return ( true == isset( $this->m_strUnitNumberNew ) ) ? '\'' . addslashes( $this->m_strUnitNumberNew ) . '\'' : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setSpaceNumber( $strSpaceNumber, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSpaceNumber', CStrings::strTrimDef( $strSpaceNumber, 50, NULL, true ), $strLocaleCode );
	}

	public function getSpaceNumber( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSpaceNumber', $strLocaleCode );
	}

	public function sqlSpaceNumber() {
		return ( true == isset( $this->m_strSpaceNumber ) ) ? '\'' . addslashes( $this->m_strSpaceNumber ) . '\'' : 'NULL';
	}

	public function setUnitNumberPatternId( $intUnitNumberPatternId ) {
		$this->set( 'm_intUnitNumberPatternId', CStrings::strToIntDef( $intUnitNumberPatternId, NULL, false ) );
	}

	public function getUnitNumberPatternId() {
		return $this->m_intUnitNumberPatternId;
	}

	public function sqlUnitNumberPatternId() {
		return ( true == isset( $this->m_intUnitNumberPatternId ) ) ? ( string ) $this->m_intUnitNumberPatternId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id_current, property_unit_id_new, unit_changed, unit_number_old, unit_number_new, unit_space_id, space_number, unit_number_pattern_id, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitIdCurrent() . ', ' .
						$this->sqlPropertyUnitIdNew() . ', ' .
						$this->sqlUnitChanged() . ', ' .
						$this->sqlUnitNumberOld() . ', ' .
						$this->sqlUnitNumberNew() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlSpaceNumber() . ', ' .
						$this->sqlUnitNumberPatternId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id_current = ' . $this->sqlPropertyUnitIdCurrent(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitIdCurrent', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id_current = ' . $this->sqlPropertyUnitIdCurrent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id_new = ' . $this->sqlPropertyUnitIdNew(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitIdNew', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id_new = ' . $this->sqlPropertyUnitIdNew() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_changed = ' . $this->sqlUnitChanged(). ',' ; } elseif( true == array_key_exists( 'UnitChanged', $this->getChangedColumns() ) ) { $strSql .= ' unit_changed = ' . $this->sqlUnitChanged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_old = ' . $this->sqlUnitNumberOld(). ',' ; } elseif( true == array_key_exists( 'UnitNumberOld', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_old = ' . $this->sqlUnitNumberOld() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_new = ' . $this->sqlUnitNumberNew(). ',' ; } elseif( true == array_key_exists( 'UnitNumberNew', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_new = ' . $this->sqlUnitNumberNew() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_number = ' . $this->sqlSpaceNumber(). ',' ; } elseif( true == array_key_exists( 'SpaceNumber', $this->getChangedColumns() ) ) { $strSql .= ' space_number = ' . $this->sqlSpaceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_pattern_id = ' . $this->sqlUnitNumberPatternId(). ',' ; } elseif( true == array_key_exists( 'UnitNumberPatternId', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_pattern_id = ' . $this->sqlUnitNumberPatternId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id_current' => $this->getPropertyUnitIdCurrent(),
			'property_unit_id_new' => $this->getPropertyUnitIdNew(),
			'unit_changed' => $this->getUnitChanged(),
			'unit_number_old' => $this->getUnitNumberOld(),
			'unit_number_new' => $this->getUnitNumberNew(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'space_number' => $this->getSpaceNumber(),
			'unit_number_pattern_id' => $this->getUnitNumberPatternId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>