<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlBooks
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlBooks extends CEosPluralBase {

	/**
	 * @return CDefaultGlBook[]
	 */
	public static function fetchDefaultGlBooks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlBook::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlBook
	 */
	public static function fetchDefaultGlBook( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlBook::class, $objDatabase );
	}

	public static function fetchDefaultGlBookCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_books', $objDatabase );
	}

	public static function fetchDefaultGlBookById( $intId, $objDatabase ) {
		return self::fetchDefaultGlBook( sprintf( 'SELECT * FROM default_gl_books WHERE id = %d', $intId ), $objDatabase );
	}

}
?>