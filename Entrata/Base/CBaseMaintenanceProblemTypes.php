<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceProblemTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceProblemTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceProblemType[]
	 */
	public static function fetchMaintenanceProblemTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceProblemType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceProblemType
	 */
	public static function fetchMaintenanceProblemType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceProblemType::class, $objDatabase );
	}

	public static function fetchMaintenanceProblemTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_problem_types', $objDatabase );
	}

	public static function fetchMaintenanceProblemTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceProblemType( sprintf( 'SELECT * FROM maintenance_problem_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>