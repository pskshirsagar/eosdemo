<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceProblems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceProblems extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceProblem[]
	 */
	public static function fetchPropertyMaintenanceProblems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMaintenanceProblem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceProblem
	 */
	public static function fetchPropertyMaintenanceProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMaintenanceProblem', $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_problems', $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblem( sprintf( 'SELECT * FROM property_maintenance_problems WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByPropertyMaintenanceProblemIdByCid( $intPropertyMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE property_maintenance_problem_id = %d AND cid = %d', ( int ) $intPropertyMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByForcedMaintenancePriorityIdByCid( $intForcedMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE forced_maintenance_priority_id = %d AND cid = %d', ( int ) $intForcedMaintenancePriorityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceProblemsByEmployeeIdByCid( $intEmployeeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceProblems( sprintf( 'SELECT * FROM property_maintenance_problems WHERE employee_id = %d AND cid = %d', ( int ) $intEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>