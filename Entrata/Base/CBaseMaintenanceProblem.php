<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceProblem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_problems';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intMaintenanceProblemId;
	protected $m_intScheduledApTransactionHeaderId;
	protected $m_intMaintenanceProblemTypeId;
	protected $m_intMaintenancePriorityId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsSystem;
	protected $m_intLaborStandardHours;
	protected $m_intIsPublished;
	protected $m_intIsAppliedToProperty;
	protected $m_intOrderNum;
	protected $m_intShowOnResidentPortal;
	protected $m_strImportedOn;
	protected $m_strExportedOn;
	protected $m_boolIncludeInMaintenanceRequests;
	protected $m_boolIncludeInInspections;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intMaintenanceProblemTypeId = '1';
		$this->m_intIsSystem = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsAppliedToProperty = '0';
		$this->m_intOrderNum = '0';
		$this->m_intShowOnResidentPortal = '1';
		$this->m_boolIncludeInMaintenanceRequests = true;
		$this->m_boolIncludeInInspections = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemId', trim( $arrValues['maintenance_problem_id'] ) ); elseif( isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( isset( $arrValues['scheduled_ap_transaction_header_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledApTransactionHeaderId', trim( $arrValues['scheduled_ap_transaction_header_id'] ) ); elseif( isset( $arrValues['scheduled_ap_transaction_header_id'] ) ) $this->setScheduledApTransactionHeaderId( $arrValues['scheduled_ap_transaction_header_id'] );
		if( isset( $arrValues['maintenance_problem_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemTypeId', trim( $arrValues['maintenance_problem_type_id'] ) ); elseif( isset( $arrValues['maintenance_problem_type_id'] ) ) $this->setMaintenanceProblemTypeId( $arrValues['maintenance_problem_type_id'] );
		if( isset( $arrValues['maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenancePriorityId', trim( $arrValues['maintenance_priority_id'] ) ); elseif( isset( $arrValues['maintenance_priority_id'] ) ) $this->setMaintenancePriorityId( $arrValues['maintenance_priority_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['labor_standard_hours'] ) && $boolDirectSet ) $this->set( 'm_intLaborStandardHours', trim( $arrValues['labor_standard_hours'] ) ); elseif( isset( $arrValues['labor_standard_hours'] ) ) $this->setLaborStandardHours( $arrValues['labor_standard_hours'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_applied_to_property'] ) && $boolDirectSet ) $this->set( 'm_intIsAppliedToProperty', trim( $arrValues['is_applied_to_property'] ) ); elseif( isset( $arrValues['is_applied_to_property'] ) ) $this->setIsAppliedToProperty( $arrValues['is_applied_to_property'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['show_on_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowOnResidentPortal', trim( $arrValues['show_on_resident_portal'] ) ); elseif( isset( $arrValues['show_on_resident_portal'] ) ) $this->setShowOnResidentPortal( $arrValues['show_on_resident_portal'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['include_in_maintenance_requests'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeInMaintenanceRequests', trim( stripcslashes( $arrValues['include_in_maintenance_requests'] ) ) ); elseif( isset( $arrValues['include_in_maintenance_requests'] ) ) $this->setIncludeInMaintenanceRequests( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_in_maintenance_requests'] ) : $arrValues['include_in_maintenance_requests'] );
		if( isset( $arrValues['include_in_inspections'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeInInspections', trim( stripcslashes( $arrValues['include_in_inspections'] ) ) ); elseif( isset( $arrValues['include_in_inspections'] ) ) $this->setIncludeInInspections( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_in_inspections'] ) : $arrValues['include_in_inspections'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->set( 'm_intMaintenanceProblemId', CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false ) );
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function sqlMaintenanceProblemId() {
		return ( true == isset( $this->m_intMaintenanceProblemId ) ) ? ( string ) $this->m_intMaintenanceProblemId : 'NULL';
	}

	public function setScheduledApTransactionHeaderId( $intScheduledApTransactionHeaderId ) {
		$this->set( 'm_intScheduledApTransactionHeaderId', CStrings::strToIntDef( $intScheduledApTransactionHeaderId, NULL, false ) );
	}

	public function getScheduledApTransactionHeaderId() {
		return $this->m_intScheduledApTransactionHeaderId;
	}

	public function sqlScheduledApTransactionHeaderId() {
		return ( true == isset( $this->m_intScheduledApTransactionHeaderId ) ) ? ( string ) $this->m_intScheduledApTransactionHeaderId : 'NULL';
	}

	public function setMaintenanceProblemTypeId( $intMaintenanceProblemTypeId ) {
		$this->set( 'm_intMaintenanceProblemTypeId', CStrings::strToIntDef( $intMaintenanceProblemTypeId, NULL, false ) );
	}

	public function getMaintenanceProblemTypeId() {
		return $this->m_intMaintenanceProblemTypeId;
	}

	public function sqlMaintenanceProblemTypeId() {
		return ( true == isset( $this->m_intMaintenanceProblemTypeId ) ) ? ( string ) $this->m_intMaintenanceProblemTypeId : '1';
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->set( 'm_intMaintenancePriorityId', CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false ) );
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function sqlMaintenancePriorityId() {
		return ( true == isset( $this->m_intMaintenancePriorityId ) ) ? ( string ) $this->m_intMaintenancePriorityId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setLaborStandardHours( $intLaborStandardHours ) {
		$this->set( 'm_intLaborStandardHours', CStrings::strToIntDef( $intLaborStandardHours, NULL, false ) );
	}

	public function getLaborStandardHours() {
		return $this->m_intLaborStandardHours;
	}

	public function sqlLaborStandardHours() {
		return ( true == isset( $this->m_intLaborStandardHours ) ) ? ( string ) $this->m_intLaborStandardHours : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsAppliedToProperty( $intIsAppliedToProperty ) {
		$this->set( 'm_intIsAppliedToProperty', CStrings::strToIntDef( $intIsAppliedToProperty, NULL, false ) );
	}

	public function getIsAppliedToProperty() {
		return $this->m_intIsAppliedToProperty;
	}

	public function sqlIsAppliedToProperty() {
		return ( true == isset( $this->m_intIsAppliedToProperty ) ) ? ( string ) $this->m_intIsAppliedToProperty : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setShowOnResidentPortal( $intShowOnResidentPortal ) {
		$this->set( 'm_intShowOnResidentPortal', CStrings::strToIntDef( $intShowOnResidentPortal, NULL, false ) );
	}

	public function getShowOnResidentPortal() {
		return $this->m_intShowOnResidentPortal;
	}

	public function sqlShowOnResidentPortal() {
		return ( true == isset( $this->m_intShowOnResidentPortal ) ) ? ( string ) $this->m_intShowOnResidentPortal : '1';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setIncludeInMaintenanceRequests( $boolIncludeInMaintenanceRequests ) {
		$this->set( 'm_boolIncludeInMaintenanceRequests', CStrings::strToBool( $boolIncludeInMaintenanceRequests ) );
	}

	public function getIncludeInMaintenanceRequests() {
		return $this->m_boolIncludeInMaintenanceRequests;
	}

	public function sqlIncludeInMaintenanceRequests() {
		return ( true == isset( $this->m_boolIncludeInMaintenanceRequests ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeInMaintenanceRequests ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeInInspections( $boolIncludeInInspections ) {
		$this->set( 'm_boolIncludeInInspections', CStrings::strToBool( $boolIncludeInInspections ) );
	}

	public function getIncludeInInspections() {
		return $this->m_boolIncludeInInspections;
	}

	public function sqlIncludeInInspections() {
		return ( true == isset( $this->m_boolIncludeInInspections ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeInInspections ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_database_id, maintenance_problem_id, scheduled_ap_transaction_header_id, maintenance_problem_type_id, maintenance_priority_id, remote_primary_key, system_code, name, description, is_system, labor_standard_hours, is_published, is_applied_to_property, order_num, show_on_resident_portal, imported_on, exported_on, include_in_maintenance_requests, include_in_inspections, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlMaintenanceProblemId() . ', ' .
						$this->sqlScheduledApTransactionHeaderId() . ', ' .
						$this->sqlMaintenanceProblemTypeId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlLaborStandardHours() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsAppliedToProperty() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlShowOnResidentPortal() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlIncludeInMaintenanceRequests() . ', ' .
						$this->sqlIncludeInInspections() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceProblemId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_ap_transaction_header_id = ' . $this->sqlScheduledApTransactionHeaderId(). ',' ; } elseif( true == array_key_exists( 'ScheduledApTransactionHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_ap_transaction_header_id = ' . $this->sqlScheduledApTransactionHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_type_id = ' . $this->sqlMaintenanceProblemTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceProblemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_type_id = ' . $this->sqlMaintenanceProblemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId(). ',' ; } elseif( true == array_key_exists( 'MaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' labor_standard_hours = ' . $this->sqlLaborStandardHours(). ',' ; } elseif( true == array_key_exists( 'LaborStandardHours', $this->getChangedColumns() ) ) { $strSql .= ' labor_standard_hours = ' . $this->sqlLaborStandardHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_applied_to_property = ' . $this->sqlIsAppliedToProperty(). ',' ; } elseif( true == array_key_exists( 'IsAppliedToProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_applied_to_property = ' . $this->sqlIsAppliedToProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_resident_portal = ' . $this->sqlShowOnResidentPortal(). ',' ; } elseif( true == array_key_exists( 'ShowOnResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_on_resident_portal = ' . $this->sqlShowOnResidentPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_in_maintenance_requests = ' . $this->sqlIncludeInMaintenanceRequests(). ',' ; } elseif( true == array_key_exists( 'IncludeInMaintenanceRequests', $this->getChangedColumns() ) ) { $strSql .= ' include_in_maintenance_requests = ' . $this->sqlIncludeInMaintenanceRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_in_inspections = ' . $this->sqlIncludeInInspections(). ',' ; } elseif( true == array_key_exists( 'IncludeInInspections', $this->getChangedColumns() ) ) { $strSql .= ' include_in_inspections = ' . $this->sqlIncludeInInspections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'maintenance_problem_id' => $this->getMaintenanceProblemId(),
			'scheduled_ap_transaction_header_id' => $this->getScheduledApTransactionHeaderId(),
			'maintenance_problem_type_id' => $this->getMaintenanceProblemTypeId(),
			'maintenance_priority_id' => $this->getMaintenancePriorityId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_system' => $this->getIsSystem(),
			'labor_standard_hours' => $this->getLaborStandardHours(),
			'is_published' => $this->getIsPublished(),
			'is_applied_to_property' => $this->getIsAppliedToProperty(),
			'order_num' => $this->getOrderNum(),
			'show_on_resident_portal' => $this->getShowOnResidentPortal(),
			'imported_on' => $this->getImportedOn(),
			'exported_on' => $this->getExportedOn(),
			'include_in_maintenance_requests' => $this->getIncludeInMaintenanceRequests(),
			'include_in_inspections' => $this->getIncludeInInspections(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>