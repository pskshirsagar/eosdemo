<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateEducation extends CEosSingularBase {

	const TABLE_NAME = 'public.candidate_educations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCandidateId;
	protected $m_intCandidateEducationTypeId;
	protected $m_strName;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strAverageGpa;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strDegreeDescription;
	protected $m_strSubjectOfSpecialization;
	protected $m_intDegreeCompleted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['candidate_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateId', trim( $arrValues['candidate_id'] ) ); elseif( isset( $arrValues['candidate_id'] ) ) $this->setCandidateId( $arrValues['candidate_id'] );
		if( isset( $arrValues['candidate_education_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateEducationTypeId', trim( $arrValues['candidate_education_type_id'] ) ); elseif( isset( $arrValues['candidate_education_type_id'] ) ) $this->setCandidateEducationTypeId( $arrValues['candidate_education_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['average_gpa'] ) && $boolDirectSet ) $this->set( 'm_strAverageGpa', trim( stripcslashes( $arrValues['average_gpa'] ) ) ); elseif( isset( $arrValues['average_gpa'] ) ) $this->setAverageGpa( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['average_gpa'] ) : $arrValues['average_gpa'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['degree_description'] ) && $boolDirectSet ) $this->set( 'm_strDegreeDescription', trim( stripcslashes( $arrValues['degree_description'] ) ) ); elseif( isset( $arrValues['degree_description'] ) ) $this->setDegreeDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['degree_description'] ) : $arrValues['degree_description'] );
		if( isset( $arrValues['subject_of_specialization'] ) && $boolDirectSet ) $this->set( 'm_strSubjectOfSpecialization', trim( stripcslashes( $arrValues['subject_of_specialization'] ) ) ); elseif( isset( $arrValues['subject_of_specialization'] ) ) $this->setSubjectOfSpecialization( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject_of_specialization'] ) : $arrValues['subject_of_specialization'] );
		if( isset( $arrValues['degree_completed'] ) && $boolDirectSet ) $this->set( 'm_intDegreeCompleted', trim( $arrValues['degree_completed'] ) ); elseif( isset( $arrValues['degree_completed'] ) ) $this->setDegreeCompleted( $arrValues['degree_completed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCandidateId( $intCandidateId ) {
		$this->set( 'm_intCandidateId', CStrings::strToIntDef( $intCandidateId, NULL, false ) );
	}

	public function getCandidateId() {
		return $this->m_intCandidateId;
	}

	public function sqlCandidateId() {
		return ( true == isset( $this->m_intCandidateId ) ) ? ( string ) $this->m_intCandidateId : 'NULL';
	}

	public function setCandidateEducationTypeId( $intCandidateEducationTypeId ) {
		$this->set( 'm_intCandidateEducationTypeId', CStrings::strToIntDef( $intCandidateEducationTypeId, NULL, false ) );
	}

	public function getCandidateEducationTypeId() {
		return $this->m_intCandidateEducationTypeId;
	}

	public function sqlCandidateEducationTypeId() {
		return ( true == isset( $this->m_intCandidateEducationTypeId ) ) ? ( string ) $this->m_intCandidateEducationTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setAverageGpa( $strAverageGpa ) {
		$this->set( 'm_strAverageGpa', CStrings::strTrimDef( $strAverageGpa, 240, NULL, true ) );
	}

	public function getAverageGpa() {
		return $this->m_strAverageGpa;
	}

	public function sqlAverageGpa() {
		return ( true == isset( $this->m_strAverageGpa ) ) ? '\'' . addslashes( $this->m_strAverageGpa ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setDegreeDescription( $strDegreeDescription ) {
		$this->set( 'm_strDegreeDescription', CStrings::strTrimDef( $strDegreeDescription, 2000, NULL, true ) );
	}

	public function getDegreeDescription() {
		return $this->m_strDegreeDescription;
	}

	public function sqlDegreeDescription() {
		return ( true == isset( $this->m_strDegreeDescription ) ) ? '\'' . addslashes( $this->m_strDegreeDescription ) . '\'' : 'NULL';
	}

	public function setSubjectOfSpecialization( $strSubjectOfSpecialization ) {
		$this->set( 'm_strSubjectOfSpecialization', CStrings::strTrimDef( $strSubjectOfSpecialization, 2000, NULL, true ) );
	}

	public function getSubjectOfSpecialization() {
		return $this->m_strSubjectOfSpecialization;
	}

	public function sqlSubjectOfSpecialization() {
		return ( true == isset( $this->m_strSubjectOfSpecialization ) ) ? '\'' . addslashes( $this->m_strSubjectOfSpecialization ) . '\'' : 'NULL';
	}

	public function setDegreeCompleted( $intDegreeCompleted ) {
		$this->set( 'm_intDegreeCompleted', CStrings::strToIntDef( $intDegreeCompleted, NULL, false ) );
	}

	public function getDegreeCompleted() {
		return $this->m_intDegreeCompleted;
	}

	public function sqlDegreeCompleted() {
		return ( true == isset( $this->m_intDegreeCompleted ) ) ? ( string ) $this->m_intDegreeCompleted : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, candidate_id, candidate_education_type_id, name, city, state_code, average_gpa, start_date, end_date, degree_description, subject_of_specialization, degree_completed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCandidateId() . ', ' .
 						$this->sqlCandidateEducationTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlAverageGpa() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlDegreeDescription() . ', ' .
 						$this->sqlSubjectOfSpecialization() . ', ' .
 						$this->sqlDegreeCompleted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; } elseif( true == array_key_exists( 'CandidateId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_education_type_id = ' . $this->sqlCandidateEducationTypeId() . ','; } elseif( true == array_key_exists( 'CandidateEducationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_education_type_id = ' . $this->sqlCandidateEducationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_gpa = ' . $this->sqlAverageGpa() . ','; } elseif( true == array_key_exists( 'AverageGpa', $this->getChangedColumns() ) ) { $strSql .= ' average_gpa = ' . $this->sqlAverageGpa() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' degree_description = ' . $this->sqlDegreeDescription() . ','; } elseif( true == array_key_exists( 'DegreeDescription', $this->getChangedColumns() ) ) { $strSql .= ' degree_description = ' . $this->sqlDegreeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject_of_specialization = ' . $this->sqlSubjectOfSpecialization() . ','; } elseif( true == array_key_exists( 'SubjectOfSpecialization', $this->getChangedColumns() ) ) { $strSql .= ' subject_of_specialization = ' . $this->sqlSubjectOfSpecialization() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' degree_completed = ' . $this->sqlDegreeCompleted() . ','; } elseif( true == array_key_exists( 'DegreeCompleted', $this->getChangedColumns() ) ) { $strSql .= ' degree_completed = ' . $this->sqlDegreeCompleted() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'candidate_id' => $this->getCandidateId(),
			'candidate_education_type_id' => $this->getCandidateEducationTypeId(),
			'name' => $this->getName(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'average_gpa' => $this->getAverageGpa(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'degree_description' => $this->getDegreeDescription(),
			'subject_of_specialization' => $this->getSubjectOfSpecialization(),
			'degree_completed' => $this->getDegreeCompleted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>