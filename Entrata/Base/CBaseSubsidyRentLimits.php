<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRentLimits
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyRentLimits extends CEosPluralBase {

	/**
	 * @return CSubsidyRentLimit[]
	 */
	public static function fetchSubsidyRentLimits( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyRentLimit::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyRentLimit
	 */
	public static function fetchSubsidyRentLimit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyRentLimit::class, $objDatabase );
	}

	public static function fetchSubsidyRentLimitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_rent_limits', $objDatabase );
	}

	public static function fetchSubsidyRentLimitByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimit( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimits( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimits( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitsBySubsidyRentLimitVersionIdByCid( $intSubsidyRentLimitVersionId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimits( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE subsidy_rent_limit_version_id = %d AND cid = %d', ( int ) $intSubsidyRentLimitVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitsBySubsidyAreaIdByCid( $intSubsidyAreaId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimits( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE subsidy_area_id = %d AND cid = %d', ( int ) $intSubsidyAreaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyRentLimitsBySubsidyContractTypeIdByCid( $intSubsidyContractTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyRentLimits( sprintf( 'SELECT * FROM subsidy_rent_limits WHERE subsidy_contract_type_id = %d AND cid = %d', ( int ) $intSubsidyContractTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>