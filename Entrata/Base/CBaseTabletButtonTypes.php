<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabletButtonTypes
 * Do not add any new functions to this class.
 */

class CBaseTabletButtonTypes extends CEosPluralBase {

	/**
	 * @return CTabletButtonType[]
	 */
	public static function fetchTabletButtonTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTabletButtonType::class, $objDatabase );
	}

	/**
	 * @return CTabletButtonType
	 */
	public static function fetchTabletButtonType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTabletButtonType::class, $objDatabase );
	}

	public static function fetchTabletButtonTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tablet_button_types', $objDatabase );
	}

	public static function fetchTabletButtonTypeById( $intId, $objDatabase ) {
		return self::fetchTabletButtonType( sprintf( 'SELECT * FROM tablet_button_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>