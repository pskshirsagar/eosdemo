<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTypes
 * Do not add any new functions to this class.
 */

class CBaseAssetTypes extends CEosPluralBase {

	/**
	 * @return CAssetType[]
	 */
	public static function fetchAssetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAssetType::class, $objDatabase );
	}

	/**
	 * @return CAssetType
	 */
	public static function fetchAssetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAssetType::class, $objDatabase );
	}

	public static function fetchAssetTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_types', $objDatabase );
	}

	public static function fetchAssetTypeById( $intId, $objDatabase ) {
		return self::fetchAssetType( sprintf( 'SELECT * FROM asset_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>