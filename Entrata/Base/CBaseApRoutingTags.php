<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApRoutingTags
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApRoutingTags extends CEosPluralBase {

	/**
	 * @return CApRoutingTag[]
	 */
	public static function fetchApRoutingTags( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApRoutingTag::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApRoutingTag
	 */
	public static function fetchApRoutingTag( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApRoutingTag::class, $objDatabase );
	}

	public static function fetchApRoutingTagCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_routing_tags', $objDatabase );
	}

	public static function fetchApRoutingTagByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApRoutingTag( sprintf( 'SELECT * FROM ap_routing_tags WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApRoutingTagsByCid( $intCid, $objDatabase ) {
		return self::fetchApRoutingTags( sprintf( 'SELECT * FROM ap_routing_tags WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>