<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssets extends CEosPluralBase {

	/**
	 * @return CAsset[]
	 */
	public static function fetchAssets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAsset', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAsset
	 */
	public static function fetchAsset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAsset', $objDatabase );
	}

	public static function fetchAssetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'assets', $objDatabase );
	}

	public static function fetchAssetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAsset( sprintf( 'SELECT * FROM assets WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetsByCid( $intCid, $objDatabase ) {
		return self::fetchAssets( sprintf( 'SELECT * FROM assets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAssets( sprintf( 'SELECT * FROM assets WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchAssets( sprintf( 'SELECT * FROM assets WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetsByAssetLocationIdByCid( $intAssetLocationId, $intCid, $objDatabase ) {
		return self::fetchAssets( sprintf( 'SELECT * FROM assets WHERE asset_location_id = %d AND cid = %d', ( int ) $intAssetLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchAssets( sprintf( 'SELECT * FROM assets WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>