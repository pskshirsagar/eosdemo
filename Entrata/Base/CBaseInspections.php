<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspections
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspections extends CEosPluralBase {

	/**
	 * @return CInspection[]
	 */
	public static function fetchInspections( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInspection::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspection
	 */
	public static function fetchInspection( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspection::class, $objDatabase );
	}

	public static function fetchInspectionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspections', $objDatabase );
	}

	public static function fetchInspectionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspection( sprintf( 'SELECT * FROM inspections WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByCid( $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE inspection_form_id = %d AND cid = %d', $intInspectionFormId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsBySignatureFileIdByCid( $intSignatureFileId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE signature_file_id = %d AND cid = %d', $intSignatureFileId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByInspectionFileIdByCid( $intInspectionFileId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE inspection_file_id = %d AND cid = %d', $intInspectionFileId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE scheduled_task_id = %d AND cid = %d', $intScheduledTaskId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByMaintenanceGroupIdByCid( $intMaintenanceGroupId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE maintenance_group_id = %d AND cid = %d', $intMaintenanceGroupId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionsByInspectionTypeIdByCid( $intInspectionTypeId, $intCid, $objDatabase ) {
		return self::fetchInspections( sprintf( 'SELECT * FROM inspections WHERE inspection_type_id = %d AND cid = %d', $intInspectionTypeId, $intCid ), $objDatabase );
	}

}
?>