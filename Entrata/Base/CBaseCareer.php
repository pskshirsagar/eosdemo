<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCareer extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.careers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCareerTypeId;
	protected $m_intCareerStatusTypeId;
	protected $m_intCareerApplicationId;
	protected $m_intCompanyDepartmentId;
	protected $m_intManagerEmployeeId;
	protected $m_intRecruiterEmployeeId;
	protected $m_strHiringManagerName;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strLegalTerms;
	protected $m_fltMinCompensation;
	protected $m_fltMaxCompensation;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strFaxNumber;
	protected $m_strSpecialRequirements;
	protected $m_strEssentialFunctions;
	protected $m_strQualifications;
	protected $m_strCareerShiftType;
	protected $m_strNotes;
	protected $m_strApplicationFilePath;
	protected $m_strApplicationFileName;
	protected $m_strCareerAvailableOn;
	protected $m_strSelectionScheduledOn;
	protected $m_strListDatetime;
	protected $m_strHideDatetime;
	protected $m_intKeepLocationAnonymous;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intKeepLocationAnonymous = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['career_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerTypeId', trim( $arrValues['career_type_id'] ) ); elseif( isset( $arrValues['career_type_id'] ) ) $this->setCareerTypeId( $arrValues['career_type_id'] );
		if( isset( $arrValues['career_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerStatusTypeId', trim( $arrValues['career_status_type_id'] ) ); elseif( isset( $arrValues['career_status_type_id'] ) ) $this->setCareerStatusTypeId( $arrValues['career_status_type_id'] );
		if( isset( $arrValues['career_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerApplicationId', trim( $arrValues['career_application_id'] ) ); elseif( isset( $arrValues['career_application_id'] ) ) $this->setCareerApplicationId( $arrValues['career_application_id'] );
		if( isset( $arrValues['company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyDepartmentId', trim( $arrValues['company_department_id'] ) ); elseif( isset( $arrValues['company_department_id'] ) ) $this->setCompanyDepartmentId( $arrValues['company_department_id'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['recruiter_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRecruiterEmployeeId', trim( $arrValues['recruiter_employee_id'] ) ); elseif( isset( $arrValues['recruiter_employee_id'] ) ) $this->setRecruiterEmployeeId( $arrValues['recruiter_employee_id'] );
		if( isset( $arrValues['hiring_manager_name'] ) && $boolDirectSet ) $this->set( 'm_strHiringManagerName', trim( stripcslashes( $arrValues['hiring_manager_name'] ) ) ); elseif( isset( $arrValues['hiring_manager_name'] ) ) $this->setHiringManagerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hiring_manager_name'] ) : $arrValues['hiring_manager_name'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['legal_terms'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLegalTerms', trim( stripcslashes( $arrValues['legal_terms'] ) ) ); elseif( isset( $arrValues['legal_terms'] ) ) $this->setLegalTerms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['legal_terms'] ) : $arrValues['legal_terms'] );
		if( isset( $arrValues['min_compensation'] ) && $boolDirectSet ) $this->set( 'm_fltMinCompensation', trim( $arrValues['min_compensation'] ) ); elseif( isset( $arrValues['min_compensation'] ) ) $this->setMinCompensation( $arrValues['min_compensation'] );
		if( isset( $arrValues['max_compensation'] ) && $boolDirectSet ) $this->set( 'm_fltMaxCompensation', trim( $arrValues['max_compensation'] ) ); elseif( isset( $arrValues['max_compensation'] ) ) $this->setMaxCompensation( $arrValues['max_compensation'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( stripcslashes( $arrValues['fax_number'] ) ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fax_number'] ) : $arrValues['fax_number'] );
		if( isset( $arrValues['special_requirements'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSpecialRequirements', trim( stripcslashes( $arrValues['special_requirements'] ) ) ); elseif( isset( $arrValues['special_requirements'] ) ) $this->setSpecialRequirements( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['special_requirements'] ) : $arrValues['special_requirements'] );
		if( isset( $arrValues['essential_functions'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strEssentialFunctions', trim( stripcslashes( $arrValues['essential_functions'] ) ) ); elseif( isset( $arrValues['essential_functions'] ) ) $this->setEssentialFunctions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['essential_functions'] ) : $arrValues['essential_functions'] );
		if( isset( $arrValues['qualifications'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strQualifications', trim( stripcslashes( $arrValues['qualifications'] ) ) ); elseif( isset( $arrValues['qualifications'] ) ) $this->setQualifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qualifications'] ) : $arrValues['qualifications'] );
		if( isset( $arrValues['career_shift_type'] ) && $boolDirectSet ) $this->set( 'm_strCareerShiftType', trim( stripcslashes( $arrValues['career_shift_type'] ) ) ); elseif( isset( $arrValues['career_shift_type'] ) ) $this->setCareerShiftType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['career_shift_type'] ) : $arrValues['career_shift_type'] );
		if( isset( $arrValues['notes'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['application_file_path'] ) && $boolDirectSet ) $this->set( 'm_strApplicationFilePath', trim( stripcslashes( $arrValues['application_file_path'] ) ) ); elseif( isset( $arrValues['application_file_path'] ) ) $this->setApplicationFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_file_path'] ) : $arrValues['application_file_path'] );
		if( isset( $arrValues['application_file_name'] ) && $boolDirectSet ) $this->set( 'm_strApplicationFileName', trim( stripcslashes( $arrValues['application_file_name'] ) ) ); elseif( isset( $arrValues['application_file_name'] ) ) $this->setApplicationFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_file_name'] ) : $arrValues['application_file_name'] );
		if( isset( $arrValues['career_available_on'] ) && $boolDirectSet ) $this->set( 'm_strCareerAvailableOn', trim( $arrValues['career_available_on'] ) ); elseif( isset( $arrValues['career_available_on'] ) ) $this->setCareerAvailableOn( $arrValues['career_available_on'] );
		if( isset( $arrValues['selection_scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strSelectionScheduledOn', trim( $arrValues['selection_scheduled_on'] ) ); elseif( isset( $arrValues['selection_scheduled_on'] ) ) $this->setSelectionScheduledOn( $arrValues['selection_scheduled_on'] );
		if( isset( $arrValues['list_datetime'] ) && $boolDirectSet ) $this->set( 'm_strListDatetime', trim( $arrValues['list_datetime'] ) ); elseif( isset( $arrValues['list_datetime'] ) ) $this->setListDatetime( $arrValues['list_datetime'] );
		if( isset( $arrValues['hide_datetime'] ) && $boolDirectSet ) $this->set( 'm_strHideDatetime', trim( $arrValues['hide_datetime'] ) ); elseif( isset( $arrValues['hide_datetime'] ) ) $this->setHideDatetime( $arrValues['hide_datetime'] );
		if( isset( $arrValues['keep_location_anonymous'] ) && $boolDirectSet ) $this->set( 'm_intKeepLocationAnonymous', trim( $arrValues['keep_location_anonymous'] ) ); elseif( isset( $arrValues['keep_location_anonymous'] ) ) $this->setKeepLocationAnonymous( $arrValues['keep_location_anonymous'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCareerTypeId( $intCareerTypeId ) {
		$this->set( 'm_intCareerTypeId', CStrings::strToIntDef( $intCareerTypeId, NULL, false ) );
	}

	public function getCareerTypeId() {
		return $this->m_intCareerTypeId;
	}

	public function sqlCareerTypeId() {
		return ( true == isset( $this->m_intCareerTypeId ) ) ? ( string ) $this->m_intCareerTypeId : 'NULL';
	}

	public function setCareerStatusTypeId( $intCareerStatusTypeId ) {
		$this->set( 'm_intCareerStatusTypeId', CStrings::strToIntDef( $intCareerStatusTypeId, NULL, false ) );
	}

	public function getCareerStatusTypeId() {
		return $this->m_intCareerStatusTypeId;
	}

	public function sqlCareerStatusTypeId() {
		return ( true == isset( $this->m_intCareerStatusTypeId ) ) ? ( string ) $this->m_intCareerStatusTypeId : 'NULL';
	}

	public function setCareerApplicationId( $intCareerApplicationId ) {
		$this->set( 'm_intCareerApplicationId', CStrings::strToIntDef( $intCareerApplicationId, NULL, false ) );
	}

	public function getCareerApplicationId() {
		return $this->m_intCareerApplicationId;
	}

	public function sqlCareerApplicationId() {
		return ( true == isset( $this->m_intCareerApplicationId ) ) ? ( string ) $this->m_intCareerApplicationId : 'NULL';
	}

	public function setCompanyDepartmentId( $intCompanyDepartmentId ) {
		$this->set( 'm_intCompanyDepartmentId', CStrings::strToIntDef( $intCompanyDepartmentId, NULL, false ) );
	}

	public function getCompanyDepartmentId() {
		return $this->m_intCompanyDepartmentId;
	}

	public function sqlCompanyDepartmentId() {
		return ( true == isset( $this->m_intCompanyDepartmentId ) ) ? ( string ) $this->m_intCompanyDepartmentId : 'NULL';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : 'NULL';
	}

	public function setRecruiterEmployeeId( $intRecruiterEmployeeId ) {
		$this->set( 'm_intRecruiterEmployeeId', CStrings::strToIntDef( $intRecruiterEmployeeId, NULL, false ) );
	}

	public function getRecruiterEmployeeId() {
		return $this->m_intRecruiterEmployeeId;
	}

	public function sqlRecruiterEmployeeId() {
		return ( true == isset( $this->m_intRecruiterEmployeeId ) ) ? ( string ) $this->m_intRecruiterEmployeeId : 'NULL';
	}

	public function setHiringManagerName( $strHiringManagerName ) {
		$this->set( 'm_strHiringManagerName', CStrings::strTrimDef( $strHiringManagerName, 50, NULL, true ) );
	}

	public function getHiringManagerName() {
		return $this->m_strHiringManagerName;
	}

	public function sqlHiringManagerName() {
		return ( true == isset( $this->m_strHiringManagerName ) ) ? '\'' . addslashes( $this->m_strHiringManagerName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setLegalTerms( $strLegalTerms, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLegalTerms', CStrings::strTrimDef( $strLegalTerms, -1, NULL, true ), $strLocaleCode );
	}

	public function getLegalTerms( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLegalTerms', $strLocaleCode );
	}

	public function sqlLegalTerms() {
		return ( true == isset( $this->m_strLegalTerms ) ) ? '\'' . addslashes( $this->m_strLegalTerms ) . '\'' : 'NULL';
	}

	public function setMinCompensation( $fltMinCompensation ) {
		$this->set( 'm_fltMinCompensation', CStrings::strToFloatDef( $fltMinCompensation, NULL, false, 2 ) );
	}

	public function getMinCompensation() {
		return $this->m_fltMinCompensation;
	}

	public function sqlMinCompensation() {
		return ( true == isset( $this->m_fltMinCompensation ) ) ? ( string ) $this->m_fltMinCompensation : 'NULL';
	}

	public function setMaxCompensation( $fltMaxCompensation ) {
		$this->set( 'm_fltMaxCompensation', CStrings::strToFloatDef( $fltMaxCompensation, NULL, false, 2 ) );
	}

	public function getMaxCompensation() {
		return $this->m_fltMaxCompensation;
	}

	public function sqlMaxCompensation() {
		return ( true == isset( $this->m_fltMaxCompensation ) ) ? ( string ) $this->m_fltMaxCompensation : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? '\'' . addslashes( $this->m_strFaxNumber ) . '\'' : 'NULL';
	}

	public function setSpecialRequirements( $strSpecialRequirements, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSpecialRequirements', CStrings::strTrimDef( $strSpecialRequirements, -1, NULL, true ), $strLocaleCode );
	}

	public function getSpecialRequirements( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSpecialRequirements', $strLocaleCode );
	}

	public function sqlSpecialRequirements() {
		return ( true == isset( $this->m_strSpecialRequirements ) ) ? '\'' . addslashes( $this->m_strSpecialRequirements ) . '\'' : 'NULL';
	}

	public function setEssentialFunctions( $strEssentialFunctions, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strEssentialFunctions', CStrings::strTrimDef( $strEssentialFunctions, -1, NULL, true ), $strLocaleCode );
	}

	public function getEssentialFunctions( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strEssentialFunctions', $strLocaleCode );
	}

	public function sqlEssentialFunctions() {
		return ( true == isset( $this->m_strEssentialFunctions ) ) ? '\'' . addslashes( $this->m_strEssentialFunctions ) . '\'' : 'NULL';
	}

	public function setQualifications( $strQualifications, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strQualifications', CStrings::strTrimDef( $strQualifications, -1, NULL, true ), $strLocaleCode );
	}

	public function getQualifications( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strQualifications', $strLocaleCode );
	}

	public function sqlQualifications() {
		return ( true == isset( $this->m_strQualifications ) ) ? '\'' . addslashes( $this->m_strQualifications ) . '\'' : 'NULL';
	}

	public function setCareerShiftType( $strCareerShiftType ) {
		$this->set( 'm_strCareerShiftType', CStrings::strTrimDef( $strCareerShiftType, 2000, NULL, true ) );
	}

	public function getCareerShiftType() {
		return $this->m_strCareerShiftType;
	}

	public function sqlCareerShiftType() {
		return ( true == isset( $this->m_strCareerShiftType ) ) ? '\'' . addslashes( $this->m_strCareerShiftType ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ), $strLocaleCode );
	}

	public function getNotes( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNotes', $strLocaleCode );
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setApplicationFilePath( $strApplicationFilePath ) {
		$this->set( 'm_strApplicationFilePath', CStrings::strTrimDef( $strApplicationFilePath, 4096, NULL, true ) );
	}

	public function getApplicationFilePath() {
		return $this->m_strApplicationFilePath;
	}

	public function sqlApplicationFilePath() {
		return ( true == isset( $this->m_strApplicationFilePath ) ) ? '\'' . addslashes( $this->m_strApplicationFilePath ) . '\'' : 'NULL';
	}

	public function setApplicationFileName( $strApplicationFileName ) {
		$this->set( 'm_strApplicationFileName', CStrings::strTrimDef( $strApplicationFileName, 240, NULL, true ) );
	}

	public function getApplicationFileName() {
		return $this->m_strApplicationFileName;
	}

	public function sqlApplicationFileName() {
		return ( true == isset( $this->m_strApplicationFileName ) ) ? '\'' . addslashes( $this->m_strApplicationFileName ) . '\'' : 'NULL';
	}

	public function setCareerAvailableOn( $strCareerAvailableOn ) {
		$this->set( 'm_strCareerAvailableOn', CStrings::strTrimDef( $strCareerAvailableOn, -1, NULL, true ) );
	}

	public function getCareerAvailableOn() {
		return $this->m_strCareerAvailableOn;
	}

	public function sqlCareerAvailableOn() {
		return ( true == isset( $this->m_strCareerAvailableOn ) ) ? '\'' . $this->m_strCareerAvailableOn . '\'' : 'NULL';
	}

	public function setSelectionScheduledOn( $strSelectionScheduledOn ) {
		$this->set( 'm_strSelectionScheduledOn', CStrings::strTrimDef( $strSelectionScheduledOn, -1, NULL, true ) );
	}

	public function getSelectionScheduledOn() {
		return $this->m_strSelectionScheduledOn;
	}

	public function sqlSelectionScheduledOn() {
		return ( true == isset( $this->m_strSelectionScheduledOn ) ) ? '\'' . $this->m_strSelectionScheduledOn . '\'' : 'NULL';
	}

	public function setListDatetime( $strListDatetime ) {
		$this->set( 'm_strListDatetime', CStrings::strTrimDef( $strListDatetime, -1, NULL, true ) );
	}

	public function getListDatetime() {
		return $this->m_strListDatetime;
	}

	public function sqlListDatetime() {
		return ( true == isset( $this->m_strListDatetime ) ) ? '\'' . $this->m_strListDatetime . '\'' : 'NULL';
	}

	public function setHideDatetime( $strHideDatetime ) {
		$this->set( 'm_strHideDatetime', CStrings::strTrimDef( $strHideDatetime, -1, NULL, true ) );
	}

	public function getHideDatetime() {
		return $this->m_strHideDatetime;
	}

	public function sqlHideDatetime() {
		return ( true == isset( $this->m_strHideDatetime ) ) ? '\'' . $this->m_strHideDatetime . '\'' : 'NULL';
	}

	public function setKeepLocationAnonymous( $intKeepLocationAnonymous ) {
		$this->set( 'm_intKeepLocationAnonymous', CStrings::strToIntDef( $intKeepLocationAnonymous, NULL, false ) );
	}

	public function getKeepLocationAnonymous() {
		return $this->m_intKeepLocationAnonymous;
	}

	public function sqlKeepLocationAnonymous() {
		return ( true == isset( $this->m_intKeepLocationAnonymous ) ) ? ( string ) $this->m_intKeepLocationAnonymous : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, career_type_id, career_status_type_id, career_application_id, company_department_id, manager_employee_id, recruiter_employee_id, hiring_manager_name, title, description, legal_terms, min_compensation, max_compensation, email_address, phone_number, fax_number, special_requirements, essential_functions, qualifications, career_shift_type, notes, application_file_path, application_file_name, career_available_on, selection_scheduled_on, list_datetime, hide_datetime, keep_location_anonymous, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCareerTypeId() . ', ' .
						$this->sqlCareerStatusTypeId() . ', ' .
						$this->sqlCareerApplicationId() . ', ' .
						$this->sqlCompanyDepartmentId() . ', ' .
						$this->sqlManagerEmployeeId() . ', ' .
						$this->sqlRecruiterEmployeeId() . ', ' .
						$this->sqlHiringManagerName() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlLegalTerms() . ', ' .
						$this->sqlMinCompensation() . ', ' .
						$this->sqlMaxCompensation() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlSpecialRequirements() . ', ' .
						$this->sqlEssentialFunctions() . ', ' .
						$this->sqlQualifications() . ', ' .
						$this->sqlCareerShiftType() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlApplicationFilePath() . ', ' .
						$this->sqlApplicationFileName() . ', ' .
						$this->sqlCareerAvailableOn() . ', ' .
						$this->sqlSelectionScheduledOn() . ', ' .
						$this->sqlListDatetime() . ', ' .
						$this->sqlHideDatetime() . ', ' .
						$this->sqlKeepLocationAnonymous() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_type_id = ' . $this->sqlCareerTypeId(). ',' ; } elseif( true == array_key_exists( 'CareerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' career_type_id = ' . $this->sqlCareerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_status_type_id = ' . $this->sqlCareerStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CareerStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' career_status_type_id = ' . $this->sqlCareerStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_application_id = ' . $this->sqlCareerApplicationId(). ',' ; } elseif( true == array_key_exists( 'CareerApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' career_application_id = ' . $this->sqlCareerApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId(). ',' ; } elseif( true == array_key_exists( 'CompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId(). ',' ; } elseif( true == array_key_exists( 'RecruiterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hiring_manager_name = ' . $this->sqlHiringManagerName(). ',' ; } elseif( true == array_key_exists( 'HiringManagerName', $this->getChangedColumns() ) ) { $strSql .= ' hiring_manager_name = ' . $this->sqlHiringManagerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_terms = ' . $this->sqlLegalTerms(). ',' ; } elseif( true == array_key_exists( 'LegalTerms', $this->getChangedColumns() ) ) { $strSql .= ' legal_terms = ' . $this->sqlLegalTerms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_compensation = ' . $this->sqlMinCompensation(). ',' ; } elseif( true == array_key_exists( 'MinCompensation', $this->getChangedColumns() ) ) { $strSql .= ' min_compensation = ' . $this->sqlMinCompensation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_compensation = ' . $this->sqlMaxCompensation(). ',' ; } elseif( true == array_key_exists( 'MaxCompensation', $this->getChangedColumns() ) ) { $strSql .= ' max_compensation = ' . $this->sqlMaxCompensation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_requirements = ' . $this->sqlSpecialRequirements(). ',' ; } elseif( true == array_key_exists( 'SpecialRequirements', $this->getChangedColumns() ) ) { $strSql .= ' special_requirements = ' . $this->sqlSpecialRequirements() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' essential_functions = ' . $this->sqlEssentialFunctions(). ',' ; } elseif( true == array_key_exists( 'EssentialFunctions', $this->getChangedColumns() ) ) { $strSql .= ' essential_functions = ' . $this->sqlEssentialFunctions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qualifications = ' . $this->sqlQualifications(). ',' ; } elseif( true == array_key_exists( 'Qualifications', $this->getChangedColumns() ) ) { $strSql .= ' qualifications = ' . $this->sqlQualifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_shift_type = ' . $this->sqlCareerShiftType(). ',' ; } elseif( true == array_key_exists( 'CareerShiftType', $this->getChangedColumns() ) ) { $strSql .= ' career_shift_type = ' . $this->sqlCareerShiftType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_file_path = ' . $this->sqlApplicationFilePath(). ',' ; } elseif( true == array_key_exists( 'ApplicationFilePath', $this->getChangedColumns() ) ) { $strSql .= ' application_file_path = ' . $this->sqlApplicationFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_file_name = ' . $this->sqlApplicationFileName(). ',' ; } elseif( true == array_key_exists( 'ApplicationFileName', $this->getChangedColumns() ) ) { $strSql .= ' application_file_name = ' . $this->sqlApplicationFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_available_on = ' . $this->sqlCareerAvailableOn(). ',' ; } elseif( true == array_key_exists( 'CareerAvailableOn', $this->getChangedColumns() ) ) { $strSql .= ' career_available_on = ' . $this->sqlCareerAvailableOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selection_scheduled_on = ' . $this->sqlSelectionScheduledOn(). ',' ; } elseif( true == array_key_exists( 'SelectionScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' selection_scheduled_on = ' . $this->sqlSelectionScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_datetime = ' . $this->sqlListDatetime(). ',' ; } elseif( true == array_key_exists( 'ListDatetime', $this->getChangedColumns() ) ) { $strSql .= ' list_datetime = ' . $this->sqlListDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_datetime = ' . $this->sqlHideDatetime(). ',' ; } elseif( true == array_key_exists( 'HideDatetime', $this->getChangedColumns() ) ) { $strSql .= ' hide_datetime = ' . $this->sqlHideDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keep_location_anonymous = ' . $this->sqlKeepLocationAnonymous(). ',' ; } elseif( true == array_key_exists( 'KeepLocationAnonymous', $this->getChangedColumns() ) ) { $strSql .= ' keep_location_anonymous = ' . $this->sqlKeepLocationAnonymous() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'career_type_id' => $this->getCareerTypeId(),
			'career_status_type_id' => $this->getCareerStatusTypeId(),
			'career_application_id' => $this->getCareerApplicationId(),
			'company_department_id' => $this->getCompanyDepartmentId(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'recruiter_employee_id' => $this->getRecruiterEmployeeId(),
			'hiring_manager_name' => $this->getHiringManagerName(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'legal_terms' => $this->getLegalTerms(),
			'min_compensation' => $this->getMinCompensation(),
			'max_compensation' => $this->getMaxCompensation(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'fax_number' => $this->getFaxNumber(),
			'special_requirements' => $this->getSpecialRequirements(),
			'essential_functions' => $this->getEssentialFunctions(),
			'qualifications' => $this->getQualifications(),
			'career_shift_type' => $this->getCareerShiftType(),
			'notes' => $this->getNotes(),
			'application_file_path' => $this->getApplicationFilePath(),
			'application_file_name' => $this->getApplicationFileName(),
			'career_available_on' => $this->getCareerAvailableOn(),
			'selection_scheduled_on' => $this->getSelectionScheduledOn(),
			'list_datetime' => $this->getListDatetime(),
			'hide_datetime' => $this->getHideDatetime(),
			'keep_location_anonymous' => $this->getKeepLocationAnonymous(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>