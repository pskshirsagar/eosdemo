<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationPreferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseApplicationPreferenceTypes extends CEosPluralBase {

	/**
	 * @return CApplicationPreferenceType[]
	 */
	public static function fetchApplicationPreferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationPreferenceType::class, $objDatabase );
	}

	/**
	 * @return CApplicationPreferenceType
	 */
	public static function fetchApplicationPreferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationPreferenceType::class, $objDatabase );
	}

	public static function fetchApplicationPreferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_preference_types', $objDatabase );
	}

	public static function fetchApplicationPreferenceTypeById( $intId, $objDatabase ) {
		return self::fetchApplicationPreferenceType( sprintf( 'SELECT * FROM application_preference_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>