<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePaymentPlanInstallment extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPaymentPlanId;
    protected $m_strName;
    protected $m_strChargeStartDate;
    protected $m_strChargeEndDate;
    protected $m_intEarlyChargeMonths;
    protected $m_intUseBillingMonthAsPostMonth;
    protected $m_intPostAllAtOnce;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intEarlyChargeMonths = '0';
        $this->m_intUseBillingMonthAsPostMonth = '0';
        $this->m_intPostAllAtOnce = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['payment_plan_id'] ) && $boolDirectSet ) $this->m_intPaymentPlanId = trim( $arrValues['payment_plan_id'] ); else if( isset( $arrValues['payment_plan_id'] ) ) $this->setPaymentPlanId( $arrValues['payment_plan_id'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['charge_start_date'] ) && $boolDirectSet ) $this->m_strChargeStartDate = trim( $arrValues['charge_start_date'] ); else if( isset( $arrValues['charge_start_date'] ) ) $this->setChargeStartDate( $arrValues['charge_start_date'] );
        if( isset( $arrValues['charge_end_date'] ) && $boolDirectSet ) $this->m_strChargeEndDate = trim( $arrValues['charge_end_date'] ); else if( isset( $arrValues['charge_end_date'] ) ) $this->setChargeEndDate( $arrValues['charge_end_date'] );
        if( isset( $arrValues['early_charge_months'] ) && $boolDirectSet ) $this->m_intEarlyChargeMonths = trim( $arrValues['early_charge_months'] ); else if( isset( $arrValues['early_charge_months'] ) ) $this->setEarlyChargeMonths( $arrValues['early_charge_months'] );
        if( isset( $arrValues['use_billing_month_as_post_month'] ) && $boolDirectSet ) $this->m_intUseBillingMonthAsPostMonth = trim( $arrValues['use_billing_month_as_post_month'] ); else if( isset( $arrValues['use_billing_month_as_post_month'] ) ) $this->setUseBillingMonthAsPostMonth( $arrValues['use_billing_month_as_post_month'] );
        if( isset( $arrValues['post_all_at_once'] ) && $boolDirectSet ) $this->m_intPostAllAtOnce = trim( $arrValues['post_all_at_once'] ); else if( isset( $arrValues['post_all_at_once'] ) ) $this->setPostAllAtOnce( $arrValues['post_all_at_once'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPaymentPlanId( $intPaymentPlanId ) {
        $this->m_intPaymentPlanId = CStrings::strToIntDef( $intPaymentPlanId, NULL, false );
    }

    public function getPaymentPlanId() {
        return $this->m_intPaymentPlanId;
    }

    public function sqlPaymentPlanId() {
        return ( true == isset( $this->m_intPaymentPlanId ) ) ? (string) $this->m_intPaymentPlanId : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setChargeStartDate( $strChargeStartDate ) {
        $this->m_strChargeStartDate = CStrings::strTrimDef( $strChargeStartDate, -1, NULL, true );
    }

    public function getChargeStartDate() {
        return $this->m_strChargeStartDate;
    }

    public function sqlChargeStartDate() {
        return ( true == isset( $this->m_strChargeStartDate ) ) ? '\'' . $this->m_strChargeStartDate . '\'' : 'NOW()';
    }

    public function setChargeEndDate( $strChargeEndDate ) {
        $this->m_strChargeEndDate = CStrings::strTrimDef( $strChargeEndDate, -1, NULL, true );
    }

    public function getChargeEndDate() {
        return $this->m_strChargeEndDate;
    }

    public function sqlChargeEndDate() {
        return ( true == isset( $this->m_strChargeEndDate ) ) ? '\'' . $this->m_strChargeEndDate . '\'' : 'NOW()';
    }

    public function setEarlyChargeMonths( $intEarlyChargeMonths ) {
        $this->m_intEarlyChargeMonths = CStrings::strToIntDef( $intEarlyChargeMonths, NULL, false );
    }

    public function getEarlyChargeMonths() {
        return $this->m_intEarlyChargeMonths;
    }

    public function sqlEarlyChargeMonths() {
        return ( true == isset( $this->m_intEarlyChargeMonths ) ) ? (string) $this->m_intEarlyChargeMonths : '0';
    }

    public function setUseBillingMonthAsPostMonth( $intUseBillingMonthAsPostMonth ) {
        $this->m_intUseBillingMonthAsPostMonth = CStrings::strToIntDef( $intUseBillingMonthAsPostMonth, NULL, false );
    }

    public function getUseBillingMonthAsPostMonth() {
        return $this->m_intUseBillingMonthAsPostMonth;
    }

    public function sqlUseBillingMonthAsPostMonth() {
        return ( true == isset( $this->m_intUseBillingMonthAsPostMonth ) ) ? (string) $this->m_intUseBillingMonthAsPostMonth : '0';
    }

    public function setPostAllAtOnce( $intPostAllAtOnce ) {
        $this->m_intPostAllAtOnce = CStrings::strToIntDef( $intPostAllAtOnce, NULL, false );
    }

    public function getPostAllAtOnce() {
        return $this->m_intPostAllAtOnce;
    }

    public function sqlPostAllAtOnce() {
        return ( true == isset( $this->m_intPostAllAtOnce ) ) ? (string) $this->m_intPostAllAtOnce : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.payment_plan_installments_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.payment_plan_installments( id, cid, payment_plan_id, name, charge_start_date, charge_end_date, early_charge_months, use_billing_month_as_post_month, post_all_at_once, updated_by, updated_on, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPaymentPlanId() . ', ' . 		                $this->sqlName() . ', ' . 		                $this->sqlChargeStartDate() . ', ' . 		                $this->sqlChargeEndDate() . ', ' . 		                $this->sqlEarlyChargeMonths() . ', ' . 		                $this->sqlUseBillingMonthAsPostMonth() . ', ' . 		                $this->sqlPostAllAtOnce() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.payment_plan_installments
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_plan_id = ' . $this->sqlPaymentPlanId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPaymentPlanId() ) != $this->getOriginalValueByFieldName ( 'payment_plan_id' ) ) { $arrstrOriginalValueChanges['payment_plan_id'] = $this->sqlPaymentPlanId(); $strSql .= ' payment_plan_id = ' . $this->sqlPaymentPlanId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlChargeStartDate() ) != $this->getOriginalValueByFieldName ( 'charge_start_date' ) ) { $arrstrOriginalValueChanges['charge_start_date'] = $this->sqlChargeStartDate(); $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlChargeEndDate() ) != $this->getOriginalValueByFieldName ( 'charge_end_date' ) ) { $arrstrOriginalValueChanges['charge_end_date'] = $this->sqlChargeEndDate(); $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' early_charge_months = ' . $this->sqlEarlyChargeMonths() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEarlyChargeMonths() ) != $this->getOriginalValueByFieldName ( 'early_charge_months' ) ) { $arrstrOriginalValueChanges['early_charge_months'] = $this->sqlEarlyChargeMonths(); $strSql .= ' early_charge_months = ' . $this->sqlEarlyChargeMonths() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_billing_month_as_post_month = ' . $this->sqlUseBillingMonthAsPostMonth() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUseBillingMonthAsPostMonth() ) != $this->getOriginalValueByFieldName ( 'use_billing_month_as_post_month' ) ) { $arrstrOriginalValueChanges['use_billing_month_as_post_month'] = $this->sqlUseBillingMonthAsPostMonth(); $strSql .= ' use_billing_month_as_post_month = ' . $this->sqlUseBillingMonthAsPostMonth() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_all_at_once = ' . $this->sqlPostAllAtOnce() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostAllAtOnce() ) != $this->getOriginalValueByFieldName ( 'post_all_at_once' ) ) { $arrstrOriginalValueChanges['post_all_at_once'] = $this->sqlPostAllAtOnce(); $strSql .= ' post_all_at_once = ' . $this->sqlPostAllAtOnce() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.payment_plan_installments WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.payment_plan_installments_id_seq', $objDatabase );
    }

}
?>