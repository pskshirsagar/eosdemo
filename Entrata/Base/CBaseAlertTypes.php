<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAlertTypes
 * Do not add any new functions to this class.
 */

class CBaseAlertTypes extends CEosPluralBase {

	/**
	 * @return CAlertType[]
	 */
	public static function fetchAlertTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAlertType::class, $objDatabase );
	}

	/**
	 * @return CAlertType
	 */
	public static function fetchAlertType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAlertType::class, $objDatabase );
	}

	public static function fetchAlertTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'alert_types', $objDatabase );
	}

	public static function fetchAlertTypeById( $intId, $objDatabase ) {
		return self::fetchAlertType( sprintf( 'SELECT * FROM alert_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>