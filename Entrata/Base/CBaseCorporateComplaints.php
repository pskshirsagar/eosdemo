<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateComplaints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateComplaints extends CEosPluralBase {

	/**
	 * @return CCorporateComplaint[]
	 */
	public static function fetchCorporateComplaints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCorporateComplaint::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporateComplaint
	 */
	public static function fetchCorporateComplaint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCorporateComplaint::class, $objDatabase );
	}

	public static function fetchCorporateComplaintCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_complaints', $objDatabase );
	}

	public static function fetchCorporateComplaintByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaint( sprintf( 'SELECT * FROM corporate_complaints WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByCid( $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByContactSubmissionIdByCid( $intContactSubmissionId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE contact_submission_id = %d AND cid = %d', ( int ) $intContactSubmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByCorporateComplaintIdByCid( $intCorporateComplaintId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE corporate_complaint_id = %d AND cid = %d', ( int ) $intCorporateComplaintId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateComplaintsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchCorporateComplaints( sprintf( 'SELECT * FROM corporate_complaints WHERE call_id = %d AND cid = %d', ( int ) $intCallId, ( int ) $intCid ), $objDatabase );
	}

}
?>