<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetChangeOrders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetChangeOrders extends CEosPluralBase {

	/**
	 * @return CBudgetChangeOrder[]
	 */
	public static function fetchBudgetChangeOrders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CBudgetChangeOrder', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetChangeOrder
	 */
	public static function fetchBudgetChangeOrder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBudgetChangeOrder', $objDatabase );
	}

	public static function fetchBudgetChangeOrderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_change_orders', $objDatabase );
	}

	public static function fetchBudgetChangeOrderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrder( sprintf( 'SELECT * FROM budget_change_orders WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrdersByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrders( sprintf( 'SELECT * FROM budget_change_orders WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrdersByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrders( sprintf( 'SELECT * FROM budget_change_orders WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrdersByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrders( sprintf( 'SELECT * FROM budget_change_orders WHERE ap_contract_id = %d AND cid = %d', ( int ) $intApContractId, ( int ) $intCid ), $objDatabase );
	}

}
?>