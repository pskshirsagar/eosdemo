<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMerchantAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMerchantAccounts extends CEosPluralBase {

	/**
	 * @return CMerchantAccount[]
	 */
	public static function fetchMerchantAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMerchantAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMerchantAccount
	 */
	public static function fetchMerchantAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantAccount::class, $objDatabase );
	}

	public static function fetchMerchantAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_accounts', $objDatabase );
	}

	public static function fetchMerchantAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccount( sprintf( 'SELECT * FROM merchant_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByProcessingBankIdByCid( $intProcessingBankId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE processing_bank_id = %d AND cid = %d', ( int ) $intProcessingBankId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByFundMovementTypeIdByCid( $intFundMovementTypeId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE fund_movement_type_id = %d AND cid = %d', ( int ) $intFundMovementTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByCommercialMerchantAccountIdByCid( $intCommercialMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE commercial_merchant_account_id = %d AND cid = %d', ( int ) $intCommercialMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByDistributionAccountIdByCid( $intDistributionAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE distribution_account_id = %d AND cid = %d', ( int ) $intDistributionAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByIntermediaryAccountIdByCid( $intIntermediaryAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE intermediary_account_id = %d AND cid = %d', ( int ) $intIntermediaryAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByBillingAccountIdByCid( $intBillingAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE billing_account_id = %d AND cid = %d', ( int ) $intBillingAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByReturnsAccountIdByCid( $intReturnsAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE returns_account_id = %d AND cid = %d', ( int ) $intReturnsAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByClearingAccountIdByCid( $intClearingAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE clearing_account_id = %d AND cid = %d', ( int ) $intClearingAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByDonationAccountIdByCid( $intDonationAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE donation_account_id = %d AND cid = %d', ( int ) $intDonationAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByRebateAccountIdByCid( $intRebateAccountId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE rebate_account_id = %d AND cid = %d', ( int ) $intRebateAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByMerchantProcessingTypeIdByCid( $intMerchantProcessingTypeId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE merchant_processing_type_id = %d AND cid = %d', ( int ) $intMerchantProcessingTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByDistributionGroupTypeIdByCid( $intDistributionGroupTypeId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE distribution_group_type_id = %d AND cid = %d', ( int ) $intDistributionGroupTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByAllocationTypeIdByCid( $intAllocationTypeId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE allocation_type_id = %d AND cid = %d', ( int ) $intAllocationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMerchantAccountsByExternalLegalEntityIdByCid( $intExternalLegalEntityId, $intCid, $objDatabase ) {
		return self::fetchMerchantAccounts( sprintf( 'SELECT * FROM merchant_accounts WHERE external_legal_entity_id = %d AND cid = %d', ( int ) $intExternalLegalEntityId, ( int ) $intCid ), $objDatabase );
	}

}
?>