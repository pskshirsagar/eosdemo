<?php

class CBaseDefaultReportGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_report_groups';

	protected $m_intId;
	protected $m_intReportGroupTypeId;
	protected $m_intParentModuleId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intPositionNumber;
	protected $m_boolIsInternal;
	protected $m_intIsPublished;
	protected $m_intIsDisabled;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsInternal = false;
		$this->m_intIsPublished = '0';
		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['report_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportGroupTypeId', trim( $arrValues['report_group_type_id'] ) ); elseif( isset( $arrValues['report_group_type_id'] ) ) $this->setReportGroupTypeId( $arrValues['report_group_type_id'] );
		if( isset( $arrValues['parent_module_id'] ) && $boolDirectSet ) $this->set( 'm_intParentModuleId', trim( $arrValues['parent_module_id'] ) ); elseif( isset( $arrValues['parent_module_id'] ) ) $this->setParentModuleId( $arrValues['parent_module_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['position_number'] ) && $boolDirectSet ) $this->set( 'm_intPositionNumber', trim( $arrValues['position_number'] ) ); elseif( isset( $arrValues['position_number'] ) ) $this->setPositionNumber( $arrValues['position_number'] );
		if( isset( $arrValues['is_internal'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternal', trim( stripcslashes( $arrValues['is_internal'] ) ) ); elseif( isset( $arrValues['is_internal'] ) ) $this->setIsInternal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_internal'] ) : $arrValues['is_internal'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReportGroupTypeId( $intReportGroupTypeId ) {
		$this->set( 'm_intReportGroupTypeId', CStrings::strToIntDef( $intReportGroupTypeId, NULL, false ) );
	}

	public function getReportGroupTypeId() {
		return $this->m_intReportGroupTypeId;
	}

	public function sqlReportGroupTypeId() {
		return ( true == isset( $this->m_intReportGroupTypeId ) ) ? ( string ) $this->m_intReportGroupTypeId : 'NULL';
	}

	public function setParentModuleId( $intParentModuleId ) {
		$this->set( 'm_intParentModuleId', CStrings::strToIntDef( $intParentModuleId, NULL, false ) );
	}

	public function getParentModuleId() {
		return $this->m_intParentModuleId;
	}

	public function sqlParentModuleId() {
		return ( true == isset( $this->m_intParentModuleId ) ) ? ( string ) $this->m_intParentModuleId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setPositionNumber( $intPositionNumber ) {
		$this->set( 'm_intPositionNumber', CStrings::strToIntDef( $intPositionNumber, NULL, false ) );
	}

	public function getPositionNumber() {
		return $this->m_intPositionNumber;
	}

	public function sqlPositionNumber() {
		return ( true == isset( $this->m_intPositionNumber ) ) ? ( string ) $this->m_intPositionNumber : 'NULL';
	}

	public function setIsInternal( $boolIsInternal ) {
		$this->set( 'm_boolIsInternal', CStrings::strToBool( $boolIsInternal ) );
	}

	public function getIsInternal() {
		return $this->m_boolIsInternal;
	}

	public function sqlIsInternal() {
		return ( true == isset( $this->m_boolIsInternal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'report_group_type_id' => $this->getReportGroupTypeId(),
			'parent_module_id' => $this->getParentModuleId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'position_number' => $this->getPositionNumber(),
			'is_internal' => $this->getIsInternal(),
			'is_published' => $this->getIsPublished(),
			'is_disabled' => $this->getIsDisabled(),
			'details' => $this->getDetails()
		);
	}

}
?>