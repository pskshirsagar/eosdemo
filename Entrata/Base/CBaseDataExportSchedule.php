<?php

	/**
	 * Warning : This is a composite key based class. If you are regenerating the base
	 * class again, please do so by checking the composite key checkbox.
	 */
	class CBaseDataExportSchedule extends CEosSingularBase {

		const TABLE_NAME = 'public.data_export_schedules';

		protected $m_intId;
		protected $m_intCid;
		protected $m_intDataExportFrequencyId;
		protected $m_intDataExportTypeId;
		protected $m_strExportName;
		protected $m_strNextRunDate;
		protected $m_strLastRunDate;
		protected $m_boolUseAwsProcess;
		protected $m_intDeletedBy;
		protected $m_strDeletedOn;
		protected $m_intUpdatedBy;
		protected $m_strUpdatedOn;
		protected $m_intCreatedBy;
		protected $m_strCreatedOn;
		protected $m_strEmailAddress;
		protected $m_strNotificationOptions;
		protected $m_strLastFailedOn;
		protected $m_strLastSuccessOn;
		protected $m_intCompanyTransmissionVendorId;
		protected $m_intExpectedBackupTime;

		public function __construct() {
			parent::__construct();

			$this->m_boolUseAwsProcess = true;

			return;
		}

		public function setDefaults() {
			return;
		}

		/**
		 * @SuppressWarnings( BooleanArgumentFlag )
		 */
		public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
			if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
			if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
			if( isset( $arrValues['data_export_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportFrequencyId', trim( $arrValues['data_export_frequency_id'] ) ); elseif( isset( $arrValues['data_export_frequency_id'] ) ) $this->setDataExportFrequencyId( $arrValues['data_export_frequency_id'] );
			if( isset( $arrValues['data_export_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportTypeId', trim( $arrValues['data_export_type_id'] ) ); elseif( isset( $arrValues['data_export_type_id'] ) ) $this->setDataExportTypeId( $arrValues['data_export_type_id'] );
			if( isset( $arrValues['export_name'] ) && $boolDirectSet ) $this->set( 'm_strExportName', trim( $arrValues['export_name'] ) ); elseif( isset( $arrValues['export_name'] ) ) $this->setExportName( $arrValues['export_name'] );
			if( isset( $arrValues['next_run_date'] ) && $boolDirectSet ) $this->set( 'm_strNextRunDate', trim( $arrValues['next_run_date'] ) ); elseif( isset( $arrValues['next_run_date'] ) ) $this->setNextRunDate( $arrValues['next_run_date'] );
			if( isset( $arrValues['last_run_date'] ) && $boolDirectSet ) $this->set( 'm_strLastRunDate', trim( $arrValues['last_run_date'] ) ); elseif( isset( $arrValues['last_run_date'] ) ) $this->setLastRunDate( $arrValues['last_run_date'] );
			if( isset( $arrValues['use_aws_process'] ) && $boolDirectSet ) $this->set( 'm_boolUseAwsProcess', trim( stripcslashes( $arrValues['use_aws_process'] ) ) ); elseif( isset( $arrValues['use_aws_process'] ) ) $this->setUseAwsProcess( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_aws_process'] ) : $arrValues['use_aws_process'] );
			if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
			if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
			if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
			if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
			if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
			if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
			if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
			if( isset( $arrValues['notification_options'] ) && $boolDirectSet ) $this->set( 'm_strNotificationOptions', trim( stripcslashes( $arrValues['notification_options'] ) ) ); elseif( isset( $arrValues['notification_options'] ) ) $this->setNotificationOptions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notification_options'] ) : $arrValues['notification_options'] );
			if( isset( $arrValues['last_failed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastFailedOn', trim( $arrValues['last_failed_on'] ) ); elseif( isset( $arrValues['last_failed_on'] ) ) $this->setLastFailedOn( $arrValues['last_failed_on'] );
			if( isset( $arrValues['last_success_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSuccessOn', trim( $arrValues['last_success_on'] ) ); elseif( isset( $arrValues['last_success_on'] ) ) $this->setLastSuccessOn( $arrValues['last_success_on'] );
			if( isset( $arrValues['company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransmissionVendorId', trim( $arrValues['company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['company_transmission_vendor_id'] ) ) $this->setCompanyTransmissionVendorId( $arrValues['company_transmission_vendor_id'] );
			if( isset( $arrValues['expected_backup_time'] ) && $boolDirectSet ) $this->set( 'm_intExpectedBackupTime', trim( $arrValues['expected_backup_time'] ) ); elseif( isset( $arrValues['expected_backup_time'] ) ) $this->setExpectedBackupTime( $arrValues['expected_backup_time'] );
			$this->m_boolInitialized = true;
		}

		public function setId( $intId ) {
			$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
		}

		public function getId() {
			return $this->m_intId;
		}

		public function sqlId() {
			return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
		}

		public function setCid( $intCid ) {
			$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
		}

		public function getCid() {
			return $this->m_intCid;
		}

		public function sqlCid() {
			return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
		}

		public function setDataExportFrequencyId( $intDataExportFrequencyId ) {
			$this->set( 'm_intDataExportFrequencyId', CStrings::strToIntDef( $intDataExportFrequencyId, NULL, false ) );
		}

		public function getDataExportFrequencyId() {
			return $this->m_intDataExportFrequencyId;
		}

		public function sqlDataExportFrequencyId() {
			return ( true == isset( $this->m_intDataExportFrequencyId ) ) ? ( string ) $this->m_intDataExportFrequencyId : 'NULL';
		}

		public function setDataExportTypeId( $intDataExportTypeId ) {
			$this->set( 'm_intDataExportTypeId', CStrings::strToIntDef( $intDataExportTypeId, NULL, false ) );
		}

		public function getDataExportTypeId() {
			return $this->m_intDataExportTypeId;
		}

		public function sqlDataExportTypeId() {
			return ( true == isset( $this->m_intDataExportTypeId ) ) ? ( string ) $this->m_intDataExportTypeId : 'NULL';
		}

		public function setExportName( $strExportName ) {
			$this->set( 'm_strExportName', CStrings::strTrimDef( $strExportName, 240, NULL, true ) );
		}

		public function getExportName() {
			return $this->m_strExportName;
		}

		public function sqlExportName() {
			return ( true == isset( $this->m_strExportName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExportName ) : '\'' . addslashes( $this->m_strExportName ) . '\'' ) : 'NULL';
		}

		public function setNextRunDate( $strNextRunDate ) {
			$this->set( 'm_strNextRunDate', CStrings::strTrimDef( $strNextRunDate, -1, NULL, true ) );
		}

		public function getNextRunDate() {
			return $this->m_strNextRunDate;
		}

		public function sqlNextRunDate() {
			return ( true == isset( $this->m_strNextRunDate ) ) ? '\'' . $this->m_strNextRunDate . '\'' : 'NULL';
		}

		public function setLastRunDate( $strLastRunDate ) {
			$this->set( 'm_strLastRunDate', CStrings::strTrimDef( $strLastRunDate, -1, NULL, true ) );
		}

		public function getLastRunDate() {
			return $this->m_strLastRunDate;
		}

		public function sqlLastRunDate() {
			return ( true == isset( $this->m_strLastRunDate ) ) ? '\'' . $this->m_strLastRunDate . '\'' : 'NULL';
		}

		public function setUseAwsProcess( $boolUseAwsProcess ) {
			$this->set( 'm_boolUseAwsProcess', CStrings::strToBool( $boolUseAwsProcess ) );
		}

		public function getUseAwsProcess() {
			return $this->m_boolUseAwsProcess;
		}

		public function sqlUseAwsProcess() {
			return ( true == isset( $this->m_boolUseAwsProcess ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseAwsProcess ? 'true' : 'false' ) . '\'' : 'NULL';
		}

		public function setDeletedBy( $intDeletedBy ) {
			$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
		}

		public function getDeletedBy() {
			return $this->m_intDeletedBy;
		}

		public function sqlDeletedBy() {
			return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
		}

		public function setDeletedOn( $strDeletedOn ) {
			$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
		}

		public function getDeletedOn() {
			return $this->m_strDeletedOn;
		}

		public function sqlDeletedOn() {
			return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
		}

		public function setUpdatedBy( $intUpdatedBy ) {
			$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
		}

		public function getUpdatedBy() {
			return $this->m_intUpdatedBy;
		}

		public function sqlUpdatedBy() {
			return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
		}

		public function setUpdatedOn( $strUpdatedOn ) {
			$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
		}

		public function getUpdatedOn() {
			return $this->m_strUpdatedOn;
		}

		public function sqlUpdatedOn() {
			return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
		}

		public function setCreatedBy( $intCreatedBy ) {
			$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
		}

		public function getCreatedBy() {
			return $this->m_intCreatedBy;
		}

		public function sqlCreatedBy() {
			return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
		}

		public function setCreatedOn( $strCreatedOn ) {
			$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
		}

		public function getCreatedOn() {
			return $this->m_strCreatedOn;
		}

		public function sqlCreatedOn() {
			return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
		}

		public function setEmailAddress( $strEmailAddress ) {
			$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, -1, NULL, true ) );
		}

		public function getEmailAddress() {
			return $this->m_strEmailAddress;
		}

		public function sqlEmailAddress() {
			return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
		}

		public function setNotificationOptions( $strNotificationOptions ) {
			$this->set( 'm_strNotificationOptions', CStrings::strTrimDef( $strNotificationOptions, -1, NULL, true ) );
		}

		public function getNotificationOptions() {
			return $this->m_strNotificationOptions;
		}

		public function sqlNotificationOptions() {
			return ( true == isset( $this->m_strNotificationOptions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotificationOptions ) : '\'' . addslashes( $this->m_strNotificationOptions ) . '\'' ) : 'NULL';
		}

		public function setLastFailedOn( $strLastFailedOn ) {
			$this->set( 'm_strLastFailedOn', CStrings::strTrimDef( $strLastFailedOn, -1, NULL, true ) );
		}

		public function getLastFailedOn() {
			return $this->m_strLastFailedOn;
		}

		public function sqlLastFailedOn() {
			return ( true == isset( $this->m_strLastFailedOn ) ) ? '\'' . $this->m_strLastFailedOn . '\'' : 'NULL';
		}

		public function setLastSuccessOn( $strLastSuccessOn ) {
			$this->set( 'm_strLastSuccessOn', CStrings::strTrimDef( $strLastSuccessOn, -1, NULL, true ) );
		}

		public function getLastSuccessOn() {
			return $this->m_strLastSuccessOn;
		}

		public function sqlLastSuccessOn() {
			return ( true == isset( $this->m_strLastSuccessOn ) ) ? '\'' . $this->m_strLastSuccessOn . '\'' : 'NULL';
		}

		public function setCompanyTransmissionVendorId( $intCompanyTransmissionVendorId ) {
			$this->set( 'm_intCompanyTransmissionVendorId', CStrings::strToIntDef( $intCompanyTransmissionVendorId, NULL, false ) );
		}

		public function getCompanyTransmissionVendorId() {
			return $this->m_intCompanyTransmissionVendorId;
		}

		public function sqlCompanyTransmissionVendorId() {
			return ( true == isset( $this->m_intCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intCompanyTransmissionVendorId : 'NULL';
		}

		public function setExpectedBackupTime( $intExpectedBackupTime ) {
			$this->set( 'm_intExpectedBackupTime', CStrings::strToIntDef( $intExpectedBackupTime, NULL, false ) );
		}

		public function getExpectedBackupTime() {
			return $this->m_intExpectedBackupTime;
		}

		public function sqlExpectedBackupTime() {
			return ( true == isset( $this->m_intExpectedBackupTime ) ) ? ( string ) $this->m_intExpectedBackupTime : 'NULL';
		}

		public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, data_export_frequency_id, data_export_type_id, export_name, next_run_date, last_run_date, use_aws_process, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, email_address, notification_options, last_failed_on, last_success_on, company_transmission_vendor_id, expected_backup_time )
					VALUES ( ' .
					  $strId . ', ' .
					  $this->sqlCid() . ', ' .
					  $this->sqlDataExportFrequencyId() . ', ' .
					  $this->sqlDataExportTypeId() . ', ' .
					  $this->sqlExportName() . ', ' .
					  $this->sqlNextRunDate() . ', ' .
					  $this->sqlLastRunDate() . ', ' .
					  $this->sqlUseAwsProcess() . ', ' .
					  $this->sqlDeletedBy() . ', ' .
					  $this->sqlDeletedOn() . ', ' .
					  ( int ) $intCurrentUserId . ', ' .
					  $this->sqlUpdatedOn() . ', ' .
					  ( int ) $intCurrentUserId . ', ' .
					  $this->sqlCreatedOn() . ', ' .
					  $this->sqlEmailAddress() . ', ' .
					  $this->sqlNotificationOptions() . ', ' .
					  $this->sqlLastFailedOn() . ', ' .
					  $this->sqlLastSuccessOn() . ', ' .
					  $this->sqlCompanyTransmissionVendorId() . ', ' .
					  $this->sqlExpectedBackupTime() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}

		public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

			$this->setDatabase( $objDatabase );

			if( false == $this->getAllowDifferentialUpdate() ) {
				$boolUpdate = true;
			} else {
				$boolUpdate = false;
			}

			$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_export_frequency_id = ' . $this->sqlDataExportFrequencyId(). ',' ; } elseif( true == array_key_exists( 'DataExportFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' data_export_frequency_id = ' . $this->sqlDataExportFrequencyId() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_export_type_id = ' . $this->sqlDataExportTypeId(). ',' ; } elseif( true == array_key_exists( 'DataExportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_export_type_id = ' . $this->sqlDataExportTypeId() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_name = ' . $this->sqlExportName(). ',' ; } elseif( true == array_key_exists( 'ExportName', $this->getChangedColumns() ) ) { $strSql .= ' export_name = ' . $this->sqlExportName() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_run_date = ' . $this->sqlNextRunDate(). ',' ; } elseif( true == array_key_exists( 'NextRunDate', $this->getChangedColumns() ) ) { $strSql .= ' next_run_date = ' . $this->sqlNextRunDate() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_run_date = ' . $this->sqlLastRunDate(). ',' ; } elseif( true == array_key_exists( 'LastRunDate', $this->getChangedColumns() ) ) { $strSql .= ' last_run_date = ' . $this->sqlLastRunDate() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_aws_process = ' . $this->sqlUseAwsProcess(). ',' ; } elseif( true == array_key_exists( 'UseAwsProcess', $this->getChangedColumns() ) ) { $strSql .= ' use_aws_process = ' . $this->sqlUseAwsProcess() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_options = ' . $this->sqlNotificationOptions(). ',' ; } elseif( true == array_key_exists( 'NotificationOptions', $this->getChangedColumns() ) ) { $strSql .= ' notification_options = ' . $this->sqlNotificationOptions() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn(). ',' ; } elseif( true == array_key_exists( 'LastFailedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_success_on = ' . $this->sqlLastSuccessOn(). ',' ; } elseif( true == array_key_exists( 'LastSuccessOn', $this->getChangedColumns() ) ) { $strSql .= ' last_success_on = ' . $this->sqlLastSuccessOn() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'CompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
			if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_backup_time = ' . $this->sqlExpectedBackupTime(). ',' ; } elseif( true == array_key_exists( 'ExpectedBackupTime', $this->getChangedColumns() ) ) { $strSql .= ' expected_backup_time = ' . $this->sqlExpectedBackupTime() . ','; $boolUpdate = true; }
			$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
			$strSql .= ' updated_on = \'NOW()\' ';

			$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

			if( true == $boolReturnSqlOnly ) {
				return ( true == $boolUpdate ) ? $strSql : false;
			} else {
				if( true == $boolUpdate ) {
					if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
						if( true == $this->getAllowDifferentialUpdate() ) {
							$this->resetChangedColumns();
							return true;
						}
					} else {
						return false;
					}
				}
				return true;
			}
		}

		public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

			$this->setDatabase( $objDatabase );

			$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}

		public function toArray() {
			return array(
				'id' => $this->getId(),
				'cid' => $this->getCid(),
				'data_export_frequency_id' => $this->getDataExportFrequencyId(),
				'data_export_type_id' => $this->getDataExportTypeId(),
				'export_name' => $this->getExportName(),
				'next_run_date' => $this->getNextRunDate(),
				'last_run_date' => $this->getLastRunDate(),
				'use_aws_process' => $this->getUseAwsProcess(),
				'deleted_by' => $this->getDeletedBy(),
				'deleted_on' => $this->getDeletedOn(),
				'updated_by' => $this->getUpdatedBy(),
				'updated_on' => $this->getUpdatedOn(),
				'created_by' => $this->getCreatedBy(),
				'created_on' => $this->getCreatedOn(),
				'email_address' => $this->getEmailAddress(),
				'notification_options' => $this->getNotificationOptions(),
				'last_failed_on' => $this->getLastFailedOn(),
				'last_success_on' => $this->getLastSuccessOn(),
				'company_transmission_vendor_id' => $this->getCompanyTransmissionVendorId(),
				'expected_backup_time' => $this->getExpectedBackupTime()
			);
		}

	}
?>