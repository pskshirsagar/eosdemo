<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRewardPoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerRewardPoints extends CEosPluralBase {

	/**
	 * @return CCustomerRewardPoint[]
	 */
	public static function fetchCustomerRewardPoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerRewardPoint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerRewardPoint
	 */
	public static function fetchCustomerRewardPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerRewardPoint', $objDatabase );
	}

	public static function fetchCustomerRewardPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_reward_points', $objDatabase );
	}

	public static function fetchCustomerRewardPointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerRewardPoint( sprintf( 'SELECT * FROM customer_reward_points WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRewardPointsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerRewardPoints( sprintf( 'SELECT * FROM customer_reward_points WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRewardPointsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerRewardPoints( sprintf( 'SELECT * FROM customer_reward_points WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRewardPointsByCustomerPointTypeIdByCid( $intCustomerPointTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerRewardPoints( sprintf( 'SELECT * FROM customer_reward_points WHERE customer_point_type_id = %d AND cid = %d', ( int ) $intCustomerPointTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>