<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAuthenticationTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerAuthenticationTypes extends CEosPluralBase {

	/**
	 * @return CCustomerAuthenticationType[]
	 */
	public static function fetchCustomerAuthenticationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerAuthenticationType::class, $objDatabase );
	}

	/**
	 * @return CCustomerAuthenticationType
	 */
	public static function fetchCustomerAuthenticationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAuthenticationType::class, $objDatabase );
	}

	public static function fetchCustomerAuthenticationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_authentication_types', $objDatabase );
	}

	public static function fetchCustomerAuthenticationTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerAuthenticationType( sprintf( 'SELECT * FROM customer_authentication_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>