<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIlsEmail extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_emails';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeadSourceId;
	protected $m_intInternetListingServiceId;
	protected $m_intApplicationId;
	protected $m_strToEmailAddress;
	protected $m_strFromEmailAddress;
	protected $m_strSubject;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strParsedOn;
	protected $m_strForwardedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strToEmailAddress', trim( stripcslashes( $arrValues['to_email_address'] ) ) ); elseif( isset( $arrValues['to_email_address'] ) ) $this->setToEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['to_email_address'] ) : $arrValues['to_email_address'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( stripcslashes( $arrValues['from_email_address'] ) ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_email_address'] ) : $arrValues['from_email_address'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['parsed_on'] ) && $boolDirectSet ) $this->set( 'm_strParsedOn', trim( $arrValues['parsed_on'] ) ); elseif( isset( $arrValues['parsed_on'] ) ) $this->setParsedOn( $arrValues['parsed_on'] );
		if( isset( $arrValues['forwarded_on'] ) && $boolDirectSet ) $this->set( 'm_strForwardedOn', trim( $arrValues['forwarded_on'] ) ); elseif( isset( $arrValues['forwarded_on'] ) ) $this->setForwardedOn( $arrValues['forwarded_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->set( 'm_strToEmailAddress', CStrings::strTrimDef( $strToEmailAddress, 240, NULL, true ) );
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function sqlToEmailAddress() {
		return ( true == isset( $this->m_strToEmailAddress ) ) ? '\'' . addslashes( $this->m_strToEmailAddress ) . '\'' : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 240, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 2000, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 50, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setParsedOn( $strParsedOn ) {
		$this->set( 'm_strParsedOn', CStrings::strTrimDef( $strParsedOn, -1, NULL, true ) );
	}

	public function getParsedOn() {
		return $this->m_strParsedOn;
	}

	public function sqlParsedOn() {
		return ( true == isset( $this->m_strParsedOn ) ) ? '\'' . $this->m_strParsedOn . '\'' : 'NULL';
	}

	public function setForwardedOn( $strForwardedOn ) {
		$this->set( 'm_strForwardedOn', CStrings::strTrimDef( $strForwardedOn, -1, NULL, true ) );
	}

	public function getForwardedOn() {
		return $this->m_strForwardedOn;
	}

	public function sqlForwardedOn() {
		return ( true == isset( $this->m_strForwardedOn ) ) ? '\'' . $this->m_strForwardedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lead_source_id, internet_listing_service_id, application_id, to_email_address, from_email_address, subject, file_name, file_path, parsed_on, forwarded_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeadSourceId() . ', ' .
 						$this->sqlInternetListingServiceId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlToEmailAddress() . ', ' .
 						$this->sqlFromEmailAddress() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlParsedOn() . ', ' .
 						$this->sqlForwardedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; } elseif( true == array_key_exists( 'ToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parsed_on = ' . $this->sqlParsedOn() . ','; } elseif( true == array_key_exists( 'ParsedOn', $this->getChangedColumns() ) ) { $strSql .= ' parsed_on = ' . $this->sqlParsedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' forwarded_on = ' . $this->sqlForwardedOn() . ','; } elseif( true == array_key_exists( 'ForwardedOn', $this->getChangedColumns() ) ) { $strSql .= ' forwarded_on = ' . $this->sqlForwardedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'application_id' => $this->getApplicationId(),
			'to_email_address' => $this->getToEmailAddress(),
			'from_email_address' => $this->getFromEmailAddress(),
			'subject' => $this->getSubject(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'parsed_on' => $this->getParsedOn(),
			'forwarded_on' => $this->getForwardedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>