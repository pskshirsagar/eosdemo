<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyCertification extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.subsidy_certifications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intApplicationId;
	protected $m_intSubsidyContractId;
	protected $m_intSetAsideId;
	protected $m_intPrimaryCustomerId;
	protected $m_intSubsidyCertificationTypeId;
	protected $m_intSubsidySubCertificationTypeId;
	protected $m_intSubsidyCertificationStatusTypeId;
	protected $m_intSubsidyCertificationReasonTypeId;
	protected $m_intSubsidyIncomeLimitVersionId;
	protected $m_intSubsidyCertificationCorrectionTypeId;
	protected $m_intSubsidySignatureExceptionTypeId;
	protected $m_intCorrectedSubsidyCertificationId;
	protected $m_intResultOfSubsidyCertificationId;
	protected $m_intSubsidyGrossRentChangeId;
	protected $m_intRepaymentId;
	protected $m_intSubsidyTracsVersionId;
	protected $m_strHmfaCode;
	protected $m_strMatData;
	protected $m_fltRentOverrideAmount;
	protected $m_fltSecurityDepositOverrideAmount;
	protected $m_fltHudOverpaidAmount;
	protected $m_strEffectiveDate;
	protected $m_strEffectiveThroughDate;
	protected $m_strProcessedDatetime;
	protected $m_strDatePrimaryApplicantSigned;
	protected $m_strDateAgentSigned;
	protected $m_strAnticipatedVoucherDate;
	protected $m_strFirstVoucherDate;
	protected $m_strTenantResponseDate;
	protected $m_boolIsTrial;
	protected $m_boolIsHistorical;
	protected $m_boolIsResultOfEiv;
	protected $m_boolEligibilityCheckNotRequired;
	protected $m_boolHasBeenCorrected;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intSubsidyRecertificationTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intSubsidyCertificationDisableReasonId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsTrial = false;
		$this->m_boolIsHistorical = false;
		$this->m_boolIsResultOfEiv = false;
		$this->m_boolEligibilityCheckNotRequired = false;
		$this->m_boolHasBeenCorrected = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['subsidy_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractId', trim( $arrValues['subsidy_contract_id'] ) ); elseif( isset( $arrValues['subsidy_contract_id'] ) ) $this->setSubsidyContractId( $arrValues['subsidy_contract_id'] );
		if( isset( $arrValues['set_aside_id'] ) && $boolDirectSet ) $this->set( 'm_intSetAsideId', trim( $arrValues['set_aside_id'] ) ); elseif( isset( $arrValues['set_aside_id'] ) ) $this->setSetAsideId( $arrValues['set_aside_id'] );
		if( isset( $arrValues['primary_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerId', trim( $arrValues['primary_customer_id'] ) ); elseif( isset( $arrValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrValues['primary_customer_id'] );
		if( isset( $arrValues['subsidy_certification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationTypeId', trim( $arrValues['subsidy_certification_type_id'] ) ); elseif( isset( $arrValues['subsidy_certification_type_id'] ) ) $this->setSubsidyCertificationTypeId( $arrValues['subsidy_certification_type_id'] );
		if( isset( $arrValues['subsidy_sub_certification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySubCertificationTypeId', trim( $arrValues['subsidy_sub_certification_type_id'] ) ); elseif( isset( $arrValues['subsidy_sub_certification_type_id'] ) ) $this->setSubsidySubCertificationTypeId( $arrValues['subsidy_sub_certification_type_id'] );
		if( isset( $arrValues['subsidy_certification_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationStatusTypeId', trim( $arrValues['subsidy_certification_status_type_id'] ) ); elseif( isset( $arrValues['subsidy_certification_status_type_id'] ) ) $this->setSubsidyCertificationStatusTypeId( $arrValues['subsidy_certification_status_type_id'] );
		if( isset( $arrValues['subsidy_certification_reason_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationReasonTypeId', trim( $arrValues['subsidy_certification_reason_type_id'] ) ); elseif( isset( $arrValues['subsidy_certification_reason_type_id'] ) ) $this->setSubsidyCertificationReasonTypeId( $arrValues['subsidy_certification_reason_type_id'] );
		if( isset( $arrValues['subsidy_income_limit_version_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLimitVersionId', trim( $arrValues['subsidy_income_limit_version_id'] ) ); elseif( isset( $arrValues['subsidy_income_limit_version_id'] ) ) $this->setSubsidyIncomeLimitVersionId( $arrValues['subsidy_income_limit_version_id'] );
		if( isset( $arrValues['subsidy_certification_correction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationCorrectionTypeId', trim( $arrValues['subsidy_certification_correction_type_id'] ) ); elseif( isset( $arrValues['subsidy_certification_correction_type_id'] ) ) $this->setSubsidyCertificationCorrectionTypeId( $arrValues['subsidy_certification_correction_type_id'] );
		if( isset( $arrValues['subsidy_signature_exception_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySignatureExceptionTypeId', trim( $arrValues['subsidy_signature_exception_type_id'] ) ); elseif( isset( $arrValues['subsidy_signature_exception_type_id'] ) ) $this->setSubsidySignatureExceptionTypeId( $arrValues['subsidy_signature_exception_type_id'] );
		if( isset( $arrValues['corrected_subsidy_certification_id'] ) && $boolDirectSet ) $this->set( 'm_intCorrectedSubsidyCertificationId', trim( $arrValues['corrected_subsidy_certification_id'] ) ); elseif( isset( $arrValues['corrected_subsidy_certification_id'] ) ) $this->setCorrectedSubsidyCertificationId( $arrValues['corrected_subsidy_certification_id'] );
		if( isset( $arrValues['result_of_subsidy_certification_id'] ) && $boolDirectSet ) $this->set( 'm_intResultOfSubsidyCertificationId', trim( $arrValues['result_of_subsidy_certification_id'] ) ); elseif( isset( $arrValues['result_of_subsidy_certification_id'] ) ) $this->setResultOfSubsidyCertificationId( $arrValues['result_of_subsidy_certification_id'] );
		if( isset( $arrValues['subsidy_gross_rent_change_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyGrossRentChangeId', trim( $arrValues['subsidy_gross_rent_change_id'] ) ); elseif( isset( $arrValues['subsidy_gross_rent_change_id'] ) ) $this->setSubsidyGrossRentChangeId( $arrValues['subsidy_gross_rent_change_id'] );
		if( isset( $arrValues['repayment_id'] ) && $boolDirectSet ) $this->set( 'm_intRepaymentId', trim( $arrValues['repayment_id'] ) ); elseif( isset( $arrValues['repayment_id'] ) ) $this->setRepaymentId( $arrValues['repayment_id'] );
		if( isset( $arrValues['subsidy_tracs_version_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTracsVersionId', trim( $arrValues['subsidy_tracs_version_id'] ) ); elseif( isset( $arrValues['subsidy_tracs_version_id'] ) ) $this->setSubsidyTracsVersionId( $arrValues['subsidy_tracs_version_id'] );
		if( isset( $arrValues['hmfa_code'] ) && $boolDirectSet ) $this->set( 'm_strHmfaCode', trim( stripcslashes( $arrValues['hmfa_code'] ) ) ); elseif( isset( $arrValues['hmfa_code'] ) ) $this->setHmfaCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hmfa_code'] ) : $arrValues['hmfa_code'] );
		if( isset( $arrValues['mat_data'] ) && $boolDirectSet ) $this->set( 'm_strMatData', trim( stripcslashes( $arrValues['mat_data'] ) ) ); elseif( isset( $arrValues['mat_data'] ) ) $this->setMatData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mat_data'] ) : $arrValues['mat_data'] );
		if( isset( $arrValues['rent_override_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRentOverrideAmount', trim( $arrValues['rent_override_amount'] ) ); elseif( isset( $arrValues['rent_override_amount'] ) ) $this->setRentOverrideAmount( $arrValues['rent_override_amount'] );
		if( isset( $arrValues['security_deposit_override_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSecurityDepositOverrideAmount', trim( $arrValues['security_deposit_override_amount'] ) ); elseif( isset( $arrValues['security_deposit_override_amount'] ) ) $this->setSecurityDepositOverrideAmount( $arrValues['security_deposit_override_amount'] );
		if( isset( $arrValues['hud_overpaid_amount'] ) && $boolDirectSet ) $this->set( 'm_fltHudOverpaidAmount', trim( $arrValues['hud_overpaid_amount'] ) ); elseif( isset( $arrValues['hud_overpaid_amount'] ) ) $this->setHudOverpaidAmount( $arrValues['hud_overpaid_amount'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['effective_through_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveThroughDate', trim( $arrValues['effective_through_date'] ) ); elseif( isset( $arrValues['effective_through_date'] ) ) $this->setEffectiveThroughDate( $arrValues['effective_through_date'] );
		if( isset( $arrValues['processed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessedDatetime', trim( $arrValues['processed_datetime'] ) ); elseif( isset( $arrValues['processed_datetime'] ) ) $this->setProcessedDatetime( $arrValues['processed_datetime'] );
		if( isset( $arrValues['date_primary_applicant_signed'] ) && $boolDirectSet ) $this->set( 'm_strDatePrimaryApplicantSigned', trim( $arrValues['date_primary_applicant_signed'] ) ); elseif( isset( $arrValues['date_primary_applicant_signed'] ) ) $this->setDatePrimaryApplicantSigned( $arrValues['date_primary_applicant_signed'] );
		if( isset( $arrValues['date_agent_signed'] ) && $boolDirectSet ) $this->set( 'm_strDateAgentSigned', trim( $arrValues['date_agent_signed'] ) ); elseif( isset( $arrValues['date_agent_signed'] ) ) $this->setDateAgentSigned( $arrValues['date_agent_signed'] );
		if( isset( $arrValues['anticipated_voucher_date'] ) && $boolDirectSet ) $this->set( 'm_strAnticipatedVoucherDate', trim( $arrValues['anticipated_voucher_date'] ) ); elseif( isset( $arrValues['anticipated_voucher_date'] ) ) $this->setAnticipatedVoucherDate( $arrValues['anticipated_voucher_date'] );
		if( isset( $arrValues['first_voucher_date'] ) && $boolDirectSet ) $this->set( 'm_strFirstVoucherDate', trim( $arrValues['first_voucher_date'] ) ); elseif( isset( $arrValues['first_voucher_date'] ) ) $this->setFirstVoucherDate( $arrValues['first_voucher_date'] );
		if( isset( $arrValues['tenant_response_date'] ) && $boolDirectSet ) $this->set( 'm_strTenantResponseDate', trim( $arrValues['tenant_response_date'] ) ); elseif( isset( $arrValues['tenant_response_date'] ) ) $this->setTenantResponseDate( $arrValues['tenant_response_date'] );
		if( isset( $arrValues['is_trial'] ) && $boolDirectSet ) $this->set( 'm_boolIsTrial', trim( stripcslashes( $arrValues['is_trial'] ) ) ); elseif( isset( $arrValues['is_trial'] ) ) $this->setIsTrial( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_trial'] ) : $arrValues['is_trial'] );
		if( isset( $arrValues['is_historical'] ) && $boolDirectSet ) $this->set( 'm_boolIsHistorical', trim( stripcslashes( $arrValues['is_historical'] ) ) ); elseif( isset( $arrValues['is_historical'] ) ) $this->setIsHistorical( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_historical'] ) : $arrValues['is_historical'] );
		if( isset( $arrValues['is_result_of_eiv'] ) && $boolDirectSet ) $this->set( 'm_boolIsResultOfEiv', trim( stripcslashes( $arrValues['is_result_of_eiv'] ) ) ); elseif( isset( $arrValues['is_result_of_eiv'] ) ) $this->setIsResultOfEiv( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_result_of_eiv'] ) : $arrValues['is_result_of_eiv'] );
		if( isset( $arrValues['eligibility_check_not_required'] ) && $boolDirectSet ) $this->set( 'm_boolEligibilityCheckNotRequired', trim( stripcslashes( $arrValues['eligibility_check_not_required'] ) ) ); elseif( isset( $arrValues['eligibility_check_not_required'] ) ) $this->setEligibilityCheckNotRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['eligibility_check_not_required'] ) : $arrValues['eligibility_check_not_required'] );
		if( isset( $arrValues['has_been_corrected'] ) && $boolDirectSet ) $this->set( 'm_boolHasBeenCorrected', trim( stripcslashes( $arrValues['has_been_corrected'] ) ) ); elseif( isset( $arrValues['has_been_corrected'] ) ) $this->setHasBeenCorrected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_been_corrected'] ) : $arrValues['has_been_corrected'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['subsidy_recertification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyRecertificationTypeId', trim( $arrValues['subsidy_recertification_type_id'] ) ); elseif( isset( $arrValues['subsidy_recertification_type_id'] ) ) $this->setSubsidyRecertificationTypeId( $arrValues['subsidy_recertification_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['subsidy_certification_disable_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationDisableReasonId', trim( $arrValues['subsidy_certification_disable_reason_id'] ) ); elseif( isset( $arrValues['subsidy_certification_disable_reason_id'] ) ) $this->setSubsidyCertificationDisableReasonId( $arrValues['subsidy_certification_disable_reason_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setSubsidyContractId( $intSubsidyContractId ) {
		$this->set( 'm_intSubsidyContractId', CStrings::strToIntDef( $intSubsidyContractId, NULL, false ) );
	}

	public function getSubsidyContractId() {
		return $this->m_intSubsidyContractId;
	}

	public function sqlSubsidyContractId() {
		return ( true == isset( $this->m_intSubsidyContractId ) ) ? ( string ) $this->m_intSubsidyContractId : 'NULL';
	}

	public function setSetAsideId( $intSetAsideId ) {
		$this->set( 'm_intSetAsideId', CStrings::strToIntDef( $intSetAsideId, NULL, false ) );
	}

	public function getSetAsideId() {
		return $this->m_intSetAsideId;
	}

	public function sqlSetAsideId() {
		return ( true == isset( $this->m_intSetAsideId ) ) ? ( string ) $this->m_intSetAsideId : 'NULL';
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->set( 'm_intPrimaryCustomerId', CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false ) );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function sqlPrimaryCustomerId() {
		return ( true == isset( $this->m_intPrimaryCustomerId ) ) ? ( string ) $this->m_intPrimaryCustomerId : 'NULL';
	}

	public function setSubsidyCertificationTypeId( $intSubsidyCertificationTypeId ) {
		$this->set( 'm_intSubsidyCertificationTypeId', CStrings::strToIntDef( $intSubsidyCertificationTypeId, NULL, false ) );
	}

	public function getSubsidyCertificationTypeId() {
		return $this->m_intSubsidyCertificationTypeId;
	}

	public function sqlSubsidyCertificationTypeId() {
		return ( true == isset( $this->m_intSubsidyCertificationTypeId ) ) ? ( string ) $this->m_intSubsidyCertificationTypeId : 'NULL';
	}

	public function setSubsidySubCertificationTypeId( $intSubsidySubCertificationTypeId ) {
		$this->set( 'm_intSubsidySubCertificationTypeId', CStrings::strToIntDef( $intSubsidySubCertificationTypeId, NULL, false ) );
	}

	public function getSubsidySubCertificationTypeId() {
		return $this->m_intSubsidySubCertificationTypeId;
	}

	public function sqlSubsidySubCertificationTypeId() {
		return ( true == isset( $this->m_intSubsidySubCertificationTypeId ) ) ? ( string ) $this->m_intSubsidySubCertificationTypeId : 'NULL';
	}

	public function setSubsidyCertificationStatusTypeId( $intSubsidyCertificationStatusTypeId ) {
		$this->set( 'm_intSubsidyCertificationStatusTypeId', CStrings::strToIntDef( $intSubsidyCertificationStatusTypeId, NULL, false ) );
	}

	public function getSubsidyCertificationStatusTypeId() {
		return $this->m_intSubsidyCertificationStatusTypeId;
	}

	public function sqlSubsidyCertificationStatusTypeId() {
		return ( true == isset( $this->m_intSubsidyCertificationStatusTypeId ) ) ? ( string ) $this->m_intSubsidyCertificationStatusTypeId : 'NULL';
	}

	public function setSubsidyCertificationReasonTypeId( $intSubsidyCertificationReasonTypeId ) {
		$this->set( 'm_intSubsidyCertificationReasonTypeId', CStrings::strToIntDef( $intSubsidyCertificationReasonTypeId, NULL, false ) );
	}

	public function getSubsidyCertificationReasonTypeId() {
		return $this->m_intSubsidyCertificationReasonTypeId;
	}

	public function sqlSubsidyCertificationReasonTypeId() {
		return ( true == isset( $this->m_intSubsidyCertificationReasonTypeId ) ) ? ( string ) $this->m_intSubsidyCertificationReasonTypeId : 'NULL';
	}

	public function setSubsidyIncomeLimitVersionId( $intSubsidyIncomeLimitVersionId ) {
		$this->set( 'm_intSubsidyIncomeLimitVersionId', CStrings::strToIntDef( $intSubsidyIncomeLimitVersionId, NULL, false ) );
	}

	public function getSubsidyIncomeLimitVersionId() {
		return $this->m_intSubsidyIncomeLimitVersionId;
	}

	public function sqlSubsidyIncomeLimitVersionId() {
		return ( true == isset( $this->m_intSubsidyIncomeLimitVersionId ) ) ? ( string ) $this->m_intSubsidyIncomeLimitVersionId : 'NULL';
	}

	public function setSubsidyCertificationCorrectionTypeId( $intSubsidyCertificationCorrectionTypeId ) {
		$this->set( 'm_intSubsidyCertificationCorrectionTypeId', CStrings::strToIntDef( $intSubsidyCertificationCorrectionTypeId, NULL, false ) );
	}

	public function getSubsidyCertificationCorrectionTypeId() {
		return $this->m_intSubsidyCertificationCorrectionTypeId;
	}

	public function sqlSubsidyCertificationCorrectionTypeId() {
		return ( true == isset( $this->m_intSubsidyCertificationCorrectionTypeId ) ) ? ( string ) $this->m_intSubsidyCertificationCorrectionTypeId : 'NULL';
	}

	public function setSubsidySignatureExceptionTypeId( $intSubsidySignatureExceptionTypeId ) {
		$this->set( 'm_intSubsidySignatureExceptionTypeId', CStrings::strToIntDef( $intSubsidySignatureExceptionTypeId, NULL, false ) );
	}

	public function getSubsidySignatureExceptionTypeId() {
		return $this->m_intSubsidySignatureExceptionTypeId;
	}

	public function sqlSubsidySignatureExceptionTypeId() {
		return ( true == isset( $this->m_intSubsidySignatureExceptionTypeId ) ) ? ( string ) $this->m_intSubsidySignatureExceptionTypeId : 'NULL';
	}

	public function setCorrectedSubsidyCertificationId( $intCorrectedSubsidyCertificationId ) {
		$this->set( 'm_intCorrectedSubsidyCertificationId', CStrings::strToIntDef( $intCorrectedSubsidyCertificationId, NULL, false ) );
	}

	public function getCorrectedSubsidyCertificationId() {
		return $this->m_intCorrectedSubsidyCertificationId;
	}

	public function sqlCorrectedSubsidyCertificationId() {
		return ( true == isset( $this->m_intCorrectedSubsidyCertificationId ) ) ? ( string ) $this->m_intCorrectedSubsidyCertificationId : 'NULL';
	}

	public function setResultOfSubsidyCertificationId( $intResultOfSubsidyCertificationId ) {
		$this->set( 'm_intResultOfSubsidyCertificationId', CStrings::strToIntDef( $intResultOfSubsidyCertificationId, NULL, false ) );
	}

	public function getResultOfSubsidyCertificationId() {
		return $this->m_intResultOfSubsidyCertificationId;
	}

	public function sqlResultOfSubsidyCertificationId() {
		return ( true == isset( $this->m_intResultOfSubsidyCertificationId ) ) ? ( string ) $this->m_intResultOfSubsidyCertificationId : 'NULL';
	}

	public function setSubsidyGrossRentChangeId( $intSubsidyGrossRentChangeId ) {
		$this->set( 'm_intSubsidyGrossRentChangeId', CStrings::strToIntDef( $intSubsidyGrossRentChangeId, NULL, false ) );
	}

	public function getSubsidyGrossRentChangeId() {
		return $this->m_intSubsidyGrossRentChangeId;
	}

	public function sqlSubsidyGrossRentChangeId() {
		return ( true == isset( $this->m_intSubsidyGrossRentChangeId ) ) ? ( string ) $this->m_intSubsidyGrossRentChangeId : 'NULL';
	}

	public function setRepaymentId( $intRepaymentId ) {
		$this->set( 'm_intRepaymentId', CStrings::strToIntDef( $intRepaymentId, NULL, false ) );
	}

	public function getRepaymentId() {
		return $this->m_intRepaymentId;
	}

	public function sqlRepaymentId() {
		return ( true == isset( $this->m_intRepaymentId ) ) ? ( string ) $this->m_intRepaymentId : 'NULL';
	}

	public function setSubsidyTracsVersionId( $intSubsidyTracsVersionId ) {
		$this->set( 'm_intSubsidyTracsVersionId', CStrings::strToIntDef( $intSubsidyTracsVersionId, NULL, false ) );
	}

	public function getSubsidyTracsVersionId() {
		return $this->m_intSubsidyTracsVersionId;
	}

	public function sqlSubsidyTracsVersionId() {
		return ( true == isset( $this->m_intSubsidyTracsVersionId ) ) ? ( string ) $this->m_intSubsidyTracsVersionId : 'NULL';
	}

	public function setHmfaCode( $strHmfaCode ) {
		$this->set( 'm_strHmfaCode', CStrings::strTrimDef( $strHmfaCode, 16, NULL, true ) );
	}

	public function getHmfaCode() {
		return $this->m_strHmfaCode;
	}

	public function sqlHmfaCode() {
		return ( true == isset( $this->m_strHmfaCode ) ) ? '\'' . addslashes( $this->m_strHmfaCode ) . '\'' : 'NULL';
	}

	public function setMatData( $strMatData ) {
		$this->set( 'm_strMatData', CStrings::strTrimDef( $strMatData, -1, NULL, true ) );
	}

	public function getMatData() {
		return $this->m_strMatData;
	}

	public function sqlMatData() {
		return ( true == isset( $this->m_strMatData ) ) ? '\'' . addslashes( $this->m_strMatData ) . '\'' : 'NULL';
	}

	public function setRentOverrideAmount( $fltRentOverrideAmount ) {
		$this->set( 'm_fltRentOverrideAmount', CStrings::strToFloatDef( $fltRentOverrideAmount, NULL, false, 2 ) );
	}

	public function getRentOverrideAmount() {
		return $this->m_fltRentOverrideAmount;
	}

	public function sqlRentOverrideAmount() {
		return ( true == isset( $this->m_fltRentOverrideAmount ) ) ? ( string ) $this->m_fltRentOverrideAmount : 'NULL';
	}

	public function setSecurityDepositOverrideAmount( $fltSecurityDepositOverrideAmount ) {
		$this->set( 'm_fltSecurityDepositOverrideAmount', CStrings::strToFloatDef( $fltSecurityDepositOverrideAmount, NULL, false, 2 ) );
	}

	public function getSecurityDepositOverrideAmount() {
		return $this->m_fltSecurityDepositOverrideAmount;
	}

	public function sqlSecurityDepositOverrideAmount() {
		return ( true == isset( $this->m_fltSecurityDepositOverrideAmount ) ) ? ( string ) $this->m_fltSecurityDepositOverrideAmount : 'NULL';
	}

	public function setHudOverpaidAmount( $fltHudOverpaidAmount ) {
		$this->set( 'm_fltHudOverpaidAmount', CStrings::strToFloatDef( $fltHudOverpaidAmount, NULL, false, 2 ) );
	}

	public function getHudOverpaidAmount() {
		return $this->m_fltHudOverpaidAmount;
	}

	public function sqlHudOverpaidAmount() {
		return ( true == isset( $this->m_fltHudOverpaidAmount ) ) ? ( string ) $this->m_fltHudOverpaidAmount : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setEffectiveThroughDate( $strEffectiveThroughDate ) {
		$this->set( 'm_strEffectiveThroughDate', CStrings::strTrimDef( $strEffectiveThroughDate, -1, NULL, true ) );
	}

	public function getEffectiveThroughDate() {
		return $this->m_strEffectiveThroughDate;
	}

	public function sqlEffectiveThroughDate() {
		return ( true == isset( $this->m_strEffectiveThroughDate ) ) ? '\'' . $this->m_strEffectiveThroughDate . '\'' : 'NULL';
	}

	public function setProcessedDatetime( $strProcessedDatetime ) {
		$this->set( 'm_strProcessedDatetime', CStrings::strTrimDef( $strProcessedDatetime, -1, NULL, true ) );
	}

	public function getProcessedDatetime() {
		return $this->m_strProcessedDatetime;
	}

	public function sqlProcessedDatetime() {
		return ( true == isset( $this->m_strProcessedDatetime ) ) ? '\'' . $this->m_strProcessedDatetime . '\'' : 'NULL';
	}

	public function setDatePrimaryApplicantSigned( $strDatePrimaryApplicantSigned ) {
		$this->set( 'm_strDatePrimaryApplicantSigned', CStrings::strTrimDef( $strDatePrimaryApplicantSigned, -1, NULL, true ) );
	}

	public function getDatePrimaryApplicantSigned() {
		return $this->m_strDatePrimaryApplicantSigned;
	}

	public function sqlDatePrimaryApplicantSigned() {
		return ( true == isset( $this->m_strDatePrimaryApplicantSigned ) ) ? '\'' . $this->m_strDatePrimaryApplicantSigned . '\'' : 'NULL';
	}

	public function setDateAgentSigned( $strDateAgentSigned ) {
		$this->set( 'm_strDateAgentSigned', CStrings::strTrimDef( $strDateAgentSigned, -1, NULL, true ) );
	}

	public function getDateAgentSigned() {
		return $this->m_strDateAgentSigned;
	}

	public function sqlDateAgentSigned() {
		return ( true == isset( $this->m_strDateAgentSigned ) ) ? '\'' . $this->m_strDateAgentSigned . '\'' : 'NULL';
	}

	public function setAnticipatedVoucherDate( $strAnticipatedVoucherDate ) {
		$this->set( 'm_strAnticipatedVoucherDate', CStrings::strTrimDef( $strAnticipatedVoucherDate, -1, NULL, true ) );
	}

	public function getAnticipatedVoucherDate() {
		return $this->m_strAnticipatedVoucherDate;
	}

	public function sqlAnticipatedVoucherDate() {
		return ( true == isset( $this->m_strAnticipatedVoucherDate ) ) ? '\'' . $this->m_strAnticipatedVoucherDate . '\'' : 'NULL';
	}

	public function setFirstVoucherDate( $strFirstVoucherDate ) {
		$this->set( 'm_strFirstVoucherDate', CStrings::strTrimDef( $strFirstVoucherDate, -1, NULL, true ) );
	}

	public function getFirstVoucherDate() {
		return $this->m_strFirstVoucherDate;
	}

	public function sqlFirstVoucherDate() {
		return ( true == isset( $this->m_strFirstVoucherDate ) ) ? '\'' . $this->m_strFirstVoucherDate . '\'' : 'NULL';
	}

	public function setTenantResponseDate( $strTenantResponseDate ) {
		$this->set( 'm_strTenantResponseDate', CStrings::strTrimDef( $strTenantResponseDate, -1, NULL, true ) );
	}

	public function getTenantResponseDate() {
		return $this->m_strTenantResponseDate;
	}

	public function sqlTenantResponseDate() {
		return ( true == isset( $this->m_strTenantResponseDate ) ) ? '\'' . $this->m_strTenantResponseDate . '\'' : 'NULL';
	}

	public function setIsTrial( $boolIsTrial ) {
		$this->set( 'm_boolIsTrial', CStrings::strToBool( $boolIsTrial ) );
	}

	public function getIsTrial() {
		return $this->m_boolIsTrial;
	}

	public function sqlIsTrial() {
		return ( true == isset( $this->m_boolIsTrial ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTrial ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHistorical( $boolIsHistorical ) {
		$this->set( 'm_boolIsHistorical', CStrings::strToBool( $boolIsHistorical ) );
	}

	public function getIsHistorical() {
		return $this->m_boolIsHistorical;
	}

	public function sqlIsHistorical() {
		return ( true == isset( $this->m_boolIsHistorical ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHistorical ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResultOfEiv( $boolIsResultOfEiv ) {
		$this->set( 'm_boolIsResultOfEiv', CStrings::strToBool( $boolIsResultOfEiv ) );
	}

	public function getIsResultOfEiv() {
		return $this->m_boolIsResultOfEiv;
	}

	public function sqlIsResultOfEiv() {
		return ( true == isset( $this->m_boolIsResultOfEiv ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResultOfEiv ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEligibilityCheckNotRequired( $boolEligibilityCheckNotRequired ) {
		$this->set( 'm_boolEligibilityCheckNotRequired', CStrings::strToBool( $boolEligibilityCheckNotRequired ) );
	}

	public function getEligibilityCheckNotRequired() {
		return $this->m_boolEligibilityCheckNotRequired;
	}

	public function sqlEligibilityCheckNotRequired() {
		return ( true == isset( $this->m_boolEligibilityCheckNotRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolEligibilityCheckNotRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasBeenCorrected( $boolHasBeenCorrected ) {
		$this->set( 'm_boolHasBeenCorrected', CStrings::strToBool( $boolHasBeenCorrected ) );
	}

	public function getHasBeenCorrected() {
		return $this->m_boolHasBeenCorrected;
	}

	public function sqlHasBeenCorrected() {
		return ( true == isset( $this->m_boolHasBeenCorrected ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasBeenCorrected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSubsidyRecertificationTypeId( $intSubsidyRecertificationTypeId ) {
		$this->set( 'm_intSubsidyRecertificationTypeId', CStrings::strToIntDef( $intSubsidyRecertificationTypeId, NULL, false ) );
	}

	public function getSubsidyRecertificationTypeId() {
		return $this->m_intSubsidyRecertificationTypeId;
	}

	public function sqlSubsidyRecertificationTypeId() {
		return ( true == isset( $this->m_intSubsidyRecertificationTypeId ) ) ? ( string ) $this->m_intSubsidyRecertificationTypeId : 'NULL';
	}

	public function setSubsidyCertificationDisableReasonId( $intSubsidyCertificationDisableReasonId ) {
		$this->set( 'm_intSubsidyCertificationDisableReasonId', CStrings::strToIntDef( $intSubsidyCertificationDisableReasonId, NULL, false ) );
	}

	public function getSubsidyCertificationDisableReasonId() {
		return $this->m_intSubsidyCertificationDisableReasonId;
	}

	public function sqlSubsidyCertificationDisableReasonId() {
		return ( true == isset( $this->m_intSubsidyCertificationDisableReasonId ) ) ? ( string ) $this->m_intSubsidyCertificationDisableReasonId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, application_id, subsidy_contract_id, set_aside_id, primary_customer_id, subsidy_certification_type_id, subsidy_sub_certification_type_id, subsidy_certification_status_type_id, subsidy_certification_reason_type_id, subsidy_income_limit_version_id, subsidy_certification_correction_type_id, subsidy_signature_exception_type_id, corrected_subsidy_certification_id, result_of_subsidy_certification_id, subsidy_gross_rent_change_id, repayment_id, subsidy_tracs_version_id, hmfa_code, mat_data, rent_override_amount, security_deposit_override_amount, hud_overpaid_amount, effective_date, effective_through_date, processed_datetime, date_primary_applicant_signed, date_agent_signed, anticipated_voucher_date, first_voucher_date, tenant_response_date, is_trial, is_historical, is_result_of_eiv, eligibility_check_not_required, has_been_corrected, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, subsidy_recertification_type_id, details, subsidy_certification_disable_reason_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlSubsidyContractId() . ', ' .
						$this->sqlSetAsideId() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlSubsidyCertificationTypeId() . ', ' .
						$this->sqlSubsidySubCertificationTypeId() . ', ' .
						$this->sqlSubsidyCertificationStatusTypeId() . ', ' .
						$this->sqlSubsidyCertificationReasonTypeId() . ', ' .
						$this->sqlSubsidyIncomeLimitVersionId() . ', ' .
						$this->sqlSubsidyCertificationCorrectionTypeId() . ', ' .
						$this->sqlSubsidySignatureExceptionTypeId() . ', ' .
						$this->sqlCorrectedSubsidyCertificationId() . ', ' .
						$this->sqlResultOfSubsidyCertificationId() . ', ' .
						$this->sqlSubsidyGrossRentChangeId() . ', ' .
						$this->sqlRepaymentId() . ', ' .
						$this->sqlSubsidyTracsVersionId() . ', ' .
						$this->sqlHmfaCode() . ', ' .
						$this->sqlMatData() . ', ' .
						$this->sqlRentOverrideAmount() . ', ' .
						$this->sqlSecurityDepositOverrideAmount() . ', ' .
						$this->sqlHudOverpaidAmount() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlEffectiveThroughDate() . ', ' .
						$this->sqlProcessedDatetime() . ', ' .
						$this->sqlDatePrimaryApplicantSigned() . ', ' .
						$this->sqlDateAgentSigned() . ', ' .
						$this->sqlAnticipatedVoucherDate() . ', ' .
						$this->sqlFirstVoucherDate() . ', ' .
						$this->sqlTenantResponseDate() . ', ' .
						$this->sqlIsTrial() . ', ' .
						$this->sqlIsHistorical() . ', ' .
						$this->sqlIsResultOfEiv() . ', ' .
						$this->sqlEligibilityCheckNotRequired() . ', ' .
						$this->sqlHasBeenCorrected() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSubsidyRecertificationTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlSubsidyCertificationDisableReasonId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId(). ',' ; } elseif( true == array_key_exists( 'SetAsideId', $this->getChangedColumns() ) ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId(). ',' ; } elseif( true == array_key_exists( 'PrimaryCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_type_id = ' . $this->sqlSubsidyCertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_type_id = ' . $this->sqlSubsidyCertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_sub_certification_type_id = ' . $this->sqlSubsidySubCertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidySubCertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_sub_certification_type_id = ' . $this->sqlSubsidySubCertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_status_type_id = ' . $this->sqlSubsidyCertificationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_status_type_id = ' . $this->sqlSubsidyCertificationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_reason_type_id = ' . $this->sqlSubsidyCertificationReasonTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationReasonTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_reason_type_id = ' . $this->sqlSubsidyCertificationReasonTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId(). ',' ; } elseif( true == array_key_exists( 'SubsidyIncomeLimitVersionId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_correction_type_id = ' . $this->sqlSubsidyCertificationCorrectionTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationCorrectionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_correction_type_id = ' . $this->sqlSubsidyCertificationCorrectionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_signature_exception_type_id = ' . $this->sqlSubsidySignatureExceptionTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidySignatureExceptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_signature_exception_type_id = ' . $this->sqlSubsidySignatureExceptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corrected_subsidy_certification_id = ' . $this->sqlCorrectedSubsidyCertificationId(). ',' ; } elseif( true == array_key_exists( 'CorrectedSubsidyCertificationId', $this->getChangedColumns() ) ) { $strSql .= ' corrected_subsidy_certification_id = ' . $this->sqlCorrectedSubsidyCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_of_subsidy_certification_id = ' . $this->sqlResultOfSubsidyCertificationId(). ',' ; } elseif( true == array_key_exists( 'ResultOfSubsidyCertificationId', $this->getChangedColumns() ) ) { $strSql .= ' result_of_subsidy_certification_id = ' . $this->sqlResultOfSubsidyCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_gross_rent_change_id = ' . $this->sqlSubsidyGrossRentChangeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyGrossRentChangeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_gross_rent_change_id = ' . $this->sqlSubsidyGrossRentChangeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_id = ' . $this->sqlRepaymentId(). ',' ; } elseif( true == array_key_exists( 'RepaymentId', $this->getChangedColumns() ) ) { $strSql .= ' repayment_id = ' . $this->sqlRepaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_tracs_version_id = ' . $this->sqlSubsidyTracsVersionId(). ',' ; } elseif( true == array_key_exists( 'SubsidyTracsVersionId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_tracs_version_id = ' . $this->sqlSubsidyTracsVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode(). ',' ; } elseif( true == array_key_exists( 'HmfaCode', $this->getChangedColumns() ) ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mat_data = ' . $this->sqlMatData(). ',' ; } elseif( true == array_key_exists( 'MatData', $this->getChangedColumns() ) ) { $strSql .= ' mat_data = ' . $this->sqlMatData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_override_amount = ' . $this->sqlRentOverrideAmount(). ',' ; } elseif( true == array_key_exists( 'RentOverrideAmount', $this->getChangedColumns() ) ) { $strSql .= ' rent_override_amount = ' . $this->sqlRentOverrideAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_deposit_override_amount = ' . $this->sqlSecurityDepositOverrideAmount(). ',' ; } elseif( true == array_key_exists( 'SecurityDepositOverrideAmount', $this->getChangedColumns() ) ) { $strSql .= ' security_deposit_override_amount = ' . $this->sqlSecurityDepositOverrideAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_overpaid_amount = ' . $this->sqlHudOverpaidAmount(). ',' ; } elseif( true == array_key_exists( 'HudOverpaidAmount', $this->getChangedColumns() ) ) { $strSql .= ' hud_overpaid_amount = ' . $this->sqlHudOverpaidAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveThroughDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_datetime = ' . $this->sqlProcessedDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' processed_datetime = ' . $this->sqlProcessedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_primary_applicant_signed = ' . $this->sqlDatePrimaryApplicantSigned(). ',' ; } elseif( true == array_key_exists( 'DatePrimaryApplicantSigned', $this->getChangedColumns() ) ) { $strSql .= ' date_primary_applicant_signed = ' . $this->sqlDatePrimaryApplicantSigned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_agent_signed = ' . $this->sqlDateAgentSigned(). ',' ; } elseif( true == array_key_exists( 'DateAgentSigned', $this->getChangedColumns() ) ) { $strSql .= ' date_agent_signed = ' . $this->sqlDateAgentSigned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_voucher_date = ' . $this->sqlAnticipatedVoucherDate(). ',' ; } elseif( true == array_key_exists( 'AnticipatedVoucherDate', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_voucher_date = ' . $this->sqlAnticipatedVoucherDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_voucher_date = ' . $this->sqlFirstVoucherDate(). ',' ; } elseif( true == array_key_exists( 'FirstVoucherDate', $this->getChangedColumns() ) ) { $strSql .= ' first_voucher_date = ' . $this->sqlFirstVoucherDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tenant_response_date = ' . $this->sqlTenantResponseDate(). ',' ; } elseif( true == array_key_exists( 'TenantResponseDate', $this->getChangedColumns() ) ) { $strSql .= ' tenant_response_date = ' . $this->sqlTenantResponseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_trial = ' . $this->sqlIsTrial(). ',' ; } elseif( true == array_key_exists( 'IsTrial', $this->getChangedColumns() ) ) { $strSql .= ' is_trial = ' . $this->sqlIsTrial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_historical = ' . $this->sqlIsHistorical(). ',' ; } elseif( true == array_key_exists( 'IsHistorical', $this->getChangedColumns() ) ) { $strSql .= ' is_historical = ' . $this->sqlIsHistorical() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_result_of_eiv = ' . $this->sqlIsResultOfEiv(). ',' ; } elseif( true == array_key_exists( 'IsResultOfEiv', $this->getChangedColumns() ) ) { $strSql .= ' is_result_of_eiv = ' . $this->sqlIsResultOfEiv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligibility_check_not_required = ' . $this->sqlEligibilityCheckNotRequired(). ',' ; } elseif( true == array_key_exists( 'EligibilityCheckNotRequired', $this->getChangedColumns() ) ) { $strSql .= ' eligibility_check_not_required = ' . $this->sqlEligibilityCheckNotRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_been_corrected = ' . $this->sqlHasBeenCorrected(). ',' ; } elseif( true == array_key_exists( 'HasBeenCorrected', $this->getChangedColumns() ) ) { $strSql .= ' has_been_corrected = ' . $this->sqlHasBeenCorrected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_recertification_type_id = ' . $this->sqlSubsidyRecertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyRecertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_recertification_type_id = ' . $this->sqlSubsidyRecertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_disable_reason_id = ' . $this->sqlSubsidyCertificationDisableReasonId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationDisableReasonId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_disable_reason_id = ' . $this->sqlSubsidyCertificationDisableReasonId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'application_id' => $this->getApplicationId(),
			'subsidy_contract_id' => $this->getSubsidyContractId(),
			'set_aside_id' => $this->getSetAsideId(),
			'primary_customer_id' => $this->getPrimaryCustomerId(),
			'subsidy_certification_type_id' => $this->getSubsidyCertificationTypeId(),
			'subsidy_sub_certification_type_id' => $this->getSubsidySubCertificationTypeId(),
			'subsidy_certification_status_type_id' => $this->getSubsidyCertificationStatusTypeId(),
			'subsidy_certification_reason_type_id' => $this->getSubsidyCertificationReasonTypeId(),
			'subsidy_income_limit_version_id' => $this->getSubsidyIncomeLimitVersionId(),
			'subsidy_certification_correction_type_id' => $this->getSubsidyCertificationCorrectionTypeId(),
			'subsidy_signature_exception_type_id' => $this->getSubsidySignatureExceptionTypeId(),
			'corrected_subsidy_certification_id' => $this->getCorrectedSubsidyCertificationId(),
			'result_of_subsidy_certification_id' => $this->getResultOfSubsidyCertificationId(),
			'subsidy_gross_rent_change_id' => $this->getSubsidyGrossRentChangeId(),
			'repayment_id' => $this->getRepaymentId(),
			'subsidy_tracs_version_id' => $this->getSubsidyTracsVersionId(),
			'hmfa_code' => $this->getHmfaCode(),
			'mat_data' => $this->getMatData(),
			'rent_override_amount' => $this->getRentOverrideAmount(),
			'security_deposit_override_amount' => $this->getSecurityDepositOverrideAmount(),
			'hud_overpaid_amount' => $this->getHudOverpaidAmount(),
			'effective_date' => $this->getEffectiveDate(),
			'effective_through_date' => $this->getEffectiveThroughDate(),
			'processed_datetime' => $this->getProcessedDatetime(),
			'date_primary_applicant_signed' => $this->getDatePrimaryApplicantSigned(),
			'date_agent_signed' => $this->getDateAgentSigned(),
			'anticipated_voucher_date' => $this->getAnticipatedVoucherDate(),
			'first_voucher_date' => $this->getFirstVoucherDate(),
			'tenant_response_date' => $this->getTenantResponseDate(),
			'is_trial' => $this->getIsTrial(),
			'is_historical' => $this->getIsHistorical(),
			'is_result_of_eiv' => $this->getIsResultOfEiv(),
			'eligibility_check_not_required' => $this->getEligibilityCheckNotRequired(),
			'has_been_corrected' => $this->getHasBeenCorrected(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'subsidy_recertification_type_id' => $this->getSubsidyRecertificationTypeId(),
			'details' => $this->getDetails(),
			'subsidy_certification_disable_reason_id' => $this->getSubsidyCertificationDisableReasonId()
		);
	}

}
?>