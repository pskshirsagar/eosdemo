<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueArCodeTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueArCodeTypes extends CEosPluralBase {

	/**
	 * @return CRevenueArCodeType[]
	 */
	public static function fetchRevenueArCodeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueArCodeType::class, $objDatabase );
	}

	/**
	 * @return CRevenueArCodeType
	 */
	public static function fetchRevenueArCodeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueArCodeType::class, $objDatabase );
	}

	public static function fetchRevenueArCodeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_ar_code_types', $objDatabase );
	}

	public static function fetchRevenueArCodeTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueArCodeType( sprintf( 'SELECT * FROM revenue_ar_code_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>