<?php

class CBaseDefaultCheckComponent extends CEosSingularBase {

	const TABLE_NAME = 'public.default_check_components';

	protected $m_intId;
	protected $m_intDefaultCheckTemplateId;
	protected $m_intCheckComponentTypeId;
	protected $m_fltXPosition;
	protected $m_fltYPosition;
	protected $m_intWidth;
	protected $m_intHeight;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_check_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCheckTemplateId', trim( $arrValues['default_check_template_id'] ) ); elseif( isset( $arrValues['default_check_template_id'] ) ) $this->setDefaultCheckTemplateId( $arrValues['default_check_template_id'] );
		if( isset( $arrValues['check_component_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckComponentTypeId', trim( $arrValues['check_component_type_id'] ) ); elseif( isset( $arrValues['check_component_type_id'] ) ) $this->setCheckComponentTypeId( $arrValues['check_component_type_id'] );
		if( isset( $arrValues['x_position'] ) && $boolDirectSet ) $this->set( 'm_fltXPosition', trim( $arrValues['x_position'] ) ); elseif( isset( $arrValues['x_position'] ) ) $this->setXPosition( $arrValues['x_position'] );
		if( isset( $arrValues['y_position'] ) && $boolDirectSet ) $this->set( 'm_fltYPosition', trim( $arrValues['y_position'] ) ); elseif( isset( $arrValues['y_position'] ) ) $this->setYPosition( $arrValues['y_position'] );
		if( isset( $arrValues['width'] ) && $boolDirectSet ) $this->set( 'm_intWidth', trim( $arrValues['width'] ) ); elseif( isset( $arrValues['width'] ) ) $this->setWidth( $arrValues['width'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultCheckTemplateId( $intDefaultCheckTemplateId ) {
		$this->set( 'm_intDefaultCheckTemplateId', CStrings::strToIntDef( $intDefaultCheckTemplateId, NULL, false ) );
	}

	public function getDefaultCheckTemplateId() {
		return $this->m_intDefaultCheckTemplateId;
	}

	public function sqlDefaultCheckTemplateId() {
		return ( true == isset( $this->m_intDefaultCheckTemplateId ) ) ? ( string ) $this->m_intDefaultCheckTemplateId : 'NULL';
	}

	public function setCheckComponentTypeId( $intCheckComponentTypeId ) {
		$this->set( 'm_intCheckComponentTypeId', CStrings::strToIntDef( $intCheckComponentTypeId, NULL, false ) );
	}

	public function getCheckComponentTypeId() {
		return $this->m_intCheckComponentTypeId;
	}

	public function sqlCheckComponentTypeId() {
		return ( true == isset( $this->m_intCheckComponentTypeId ) ) ? ( string ) $this->m_intCheckComponentTypeId : 'NULL';
	}

	public function setXPosition( $fltXPosition ) {
		$this->set( 'm_fltXPosition', CStrings::strToFloatDef( $fltXPosition, NULL, false, 4 ) );
	}

	public function getXPosition() {
		return $this->m_fltXPosition;
	}

	public function sqlXPosition() {
		return ( true == isset( $this->m_fltXPosition ) ) ? ( string ) $this->m_fltXPosition : 'NULL';
	}

	public function setYPosition( $fltYPosition ) {
		$this->set( 'm_fltYPosition', CStrings::strToFloatDef( $fltYPosition, NULL, false, 4 ) );
	}

	public function getYPosition() {
		return $this->m_fltYPosition;
	}

	public function sqlYPosition() {
		return ( true == isset( $this->m_fltYPosition ) ) ? ( string ) $this->m_fltYPosition : 'NULL';
	}

	public function setWidth( $intWidth ) {
		$this->set( 'm_intWidth', CStrings::strToIntDef( $intWidth, NULL, false ) );
	}

	public function getWidth() {
		return $this->m_intWidth;
	}

	public function sqlWidth() {
		return ( true == isset( $this->m_intWidth ) ) ? ( string ) $this->m_intWidth : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_check_template_id' => $this->getDefaultCheckTemplateId(),
			'check_component_type_id' => $this->getCheckComponentTypeId(),
			'x_position' => $this->getXPosition(),
			'y_position' => $this->getYPosition(),
			'width' => $this->getWidth(),
			'height' => $this->getHeight()
		);
	}

}
?>