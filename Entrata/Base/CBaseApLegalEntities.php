<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApLegalEntities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApLegalEntities extends CEosPluralBase {

	/**
	 * @return CApLegalEntity[]
	 */
	public static function fetchApLegalEntities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApLegalEntity::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApLegalEntity
	 */
	public static function fetchApLegalEntity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApLegalEntity::class, $objDatabase );
	}

	public static function fetchApLegalEntityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_legal_entities', $objDatabase );
	}

	public static function fetchApLegalEntityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntity( sprintf( 'SELECT * FROM ap_legal_entities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByCid( $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByForm1099TypeIdByCid( $intForm1099TypeId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE form_1099_type_id = %d AND cid = %d', $intForm1099TypeId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByForm1099BoxTypeIdByCid( $intForm1099BoxTypeId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE form_1099_box_type_id = %d AND cid = %d', $intForm1099BoxTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByOwnerTypeIdByCid( $intOwnerTypeId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE owner_type_id = %d AND cid = %d', $intOwnerTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByVendorEntityIdByCid( $intVendorEntityId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE vendor_entity_id = %d AND cid = %d', $intVendorEntityId, $intCid ), $objDatabase );
	}

	public static function fetchApLegalEntitiesByApRemittanceIdByCid( $intApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApLegalEntities( sprintf( 'SELECT * FROM ap_legal_entities WHERE ap_remittance_id = %d AND cid = %d', $intApRemittanceId, $intCid ), $objDatabase );
	}

}
?>