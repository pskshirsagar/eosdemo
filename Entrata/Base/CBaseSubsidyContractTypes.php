<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyContractTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyContractTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyContractType[]
	 */
	public static function fetchSubsidyContractTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyContractType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyContractType
	 */
	public static function fetchSubsidyContractType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyContractType::class, $objDatabase );
	}

	public static function fetchSubsidyContractTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_contract_types', $objDatabase );
	}

	public static function fetchSubsidyContractTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyContractType( sprintf( 'SELECT * FROM subsidy_contract_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyContractTypesBySubsidyTypeId( $intSubsidyTypeId, $objDatabase ) {
		return self::fetchSubsidyContractTypes( sprintf( 'SELECT * FROM subsidy_contract_types WHERE subsidy_type_id = %d', $intSubsidyTypeId ), $objDatabase );
	}

}
?>