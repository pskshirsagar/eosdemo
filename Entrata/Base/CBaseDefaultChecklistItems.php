<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChecklistItems
 * Do not add any new functions to this class.
 */

class CBaseDefaultChecklistItems extends CEosPluralBase {

	/**
	 * @return CDefaultChecklistItem[]
	 */
	public static function fetchDefaultChecklistItems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultChecklistItem::class, $objDatabase );
	}

	/**
	 * @return CDefaultChecklistItem
	 */
	public static function fetchDefaultChecklistItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultChecklistItem::class, $objDatabase );
	}

	public static function fetchDefaultChecklistItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_checklist_items', $objDatabase );
	}

	public static function fetchDefaultChecklistItemById( $intId, $objDatabase ) {
		return self::fetchDefaultChecklistItem( sprintf( 'SELECT * FROM default_checklist_items WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultChecklistItemsByDefaultChecklistId( $intDefaultChecklistId, $objDatabase ) {
		return self::fetchDefaultChecklistItems( sprintf( 'SELECT * FROM default_checklist_items WHERE default_checklist_id = %d', $intDefaultChecklistId ), $objDatabase );
	}

	public static function fetchDefaultChecklistItemsByChecklistItemTypeId( $intChecklistItemTypeId, $objDatabase ) {
		return self::fetchDefaultChecklistItems( sprintf( 'SELECT * FROM default_checklist_items WHERE checklist_item_type_id = %d', $intChecklistItemTypeId ), $objDatabase );
	}

	public static function fetchDefaultChecklistItemsByChecklistItemTypeOptionId( $intChecklistItemTypeOptionId, $objDatabase ) {
		return self::fetchDefaultChecklistItems( sprintf( 'SELECT * FROM default_checklist_items WHERE checklist_item_type_option_id = %d', $intChecklistItemTypeOptionId ), $objDatabase );
	}

}
?>