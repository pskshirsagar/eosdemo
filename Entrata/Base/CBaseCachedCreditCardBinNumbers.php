<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedCreditCardBinNumbers
 * Do not add any new functions to this class.
 */

class CBaseCachedCreditCardBinNumbers extends CEosPluralBase {

	/**
	 * @return CCachedCreditCardBinNumber[]
	 */
	public static function fetchCachedCreditCardBinNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCachedCreditCardBinNumber', $objDatabase );
	}

	/**
	 * @return CCachedCreditCardBinNumber
	 */
	public static function fetchCachedCreditCardBinNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCachedCreditCardBinNumber', $objDatabase );
	}

	public static function fetchCachedCreditCardBinNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_credit_card_bin_numbers', $objDatabase );
	}

	public static function fetchCachedCreditCardBinNumberById( $intId, $objDatabase ) {
		return self::fetchCachedCreditCardBinNumber( sprintf( 'SELECT * FROM cached_credit_card_bin_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCachedCreditCardBinNumbersByTableId( $strTableId, $objDatabase ) {
		return self::fetchCachedCreditCardBinNumbers( sprintf( 'SELECT * FROM cached_credit_card_bin_numbers WHERE table_id = \'%s\'', $strTableId ), $objDatabase );
	}

}
?>