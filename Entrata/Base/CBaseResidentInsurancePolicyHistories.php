<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentInsurancePolicyHistories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseResidentInsurancePolicyHistories extends CEosPluralBase {

	/**
	 * @return CResidentInsurancePolicyHistory[]
	 */
	public static function fetchResidentInsurancePolicyHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CResidentInsurancePolicyHistory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CResidentInsurancePolicyHistory
	 */
	public static function fetchResidentInsurancePolicyHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CResidentInsurancePolicyHistory', $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resident_insurance_policy_histories', $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistory( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoriesByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistories( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE resident_insurance_policy_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoriesByCid( $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistories( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistories( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoriesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistories( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyHistoriesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicyHistories( sprintf( 'SELECT * FROM resident_insurance_policy_histories WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>