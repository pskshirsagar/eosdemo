<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReviewDetailEvent extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.review_detail_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intReviewDetailId;
	protected $m_intReviewEventTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intParentReviewDetailEventId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['review_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewDetailId', trim( $arrValues['review_detail_id'] ) ); elseif( isset( $arrValues['review_detail_id'] ) ) $this->setReviewDetailId( $arrValues['review_detail_id'] );
		if( isset( $arrValues['review_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewEventTypeId', trim( $arrValues['review_event_type_id'] ) ); elseif( isset( $arrValues['review_event_type_id'] ) ) $this->setReviewEventTypeId( $arrValues['review_event_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['parent_review_detail_event_id'] ) && $boolDirectSet ) $this->set( 'm_intParentReviewDetailEventId', trim( $arrValues['parent_review_detail_event_id'] ) ); elseif( isset( $arrValues['parent_review_detail_event_id'] ) ) $this->setParentReviewDetailEventId( $arrValues['parent_review_detail_event_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setReviewDetailId( $intReviewDetailId ) {
		$this->set( 'm_intReviewDetailId', CStrings::strToIntDef( $intReviewDetailId, NULL, false ) );
	}

	public function getReviewDetailId() {
		return $this->m_intReviewDetailId;
	}

	public function sqlReviewDetailId() {
		return ( true == isset( $this->m_intReviewDetailId ) ) ? ( string ) $this->m_intReviewDetailId : 'NULL';
	}

	public function setReviewEventTypeId( $intReviewEventTypeId ) {
		$this->set( 'm_intReviewEventTypeId', CStrings::strToIntDef( $intReviewEventTypeId, NULL, false ) );
	}

	public function getReviewEventTypeId() {
		return $this->m_intReviewEventTypeId;
	}

	public function sqlReviewEventTypeId() {
		return ( true == isset( $this->m_intReviewEventTypeId ) ) ? ( string ) $this->m_intReviewEventTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setParentReviewDetailEventId( $intParentReviewDetailEventId ) {
		$this->set( 'm_intParentReviewDetailEventId', CStrings::strToIntDef( $intParentReviewDetailEventId, NULL, false ) );
	}

	public function getParentReviewDetailEventId() {
		return $this->m_intParentReviewDetailEventId;
	}

	public function sqlParentReviewDetailEventId() {
		return ( true == isset( $this->m_intParentReviewDetailEventId ) ) ? ( string ) $this->m_intParentReviewDetailEventId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, review_detail_id, review_event_type_id, details, created_by, created_on, parent_review_detail_event_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReviewDetailId() . ', ' .
						$this->sqlReviewEventTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlParentReviewDetailEventId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId(). ',' ; } elseif( true == array_key_exists( 'ReviewDetailId', $this->getChangedColumns() ) ) { $strSql .= ' review_detail_id = ' . $this->sqlReviewDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_event_type_id = ' . $this->sqlReviewEventTypeId(). ',' ; } elseif( true == array_key_exists( 'ReviewEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' review_event_type_id = ' . $this->sqlReviewEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_review_detail_event_id = ' . $this->sqlParentReviewDetailEventId() ; } elseif( true == array_key_exists( 'ParentReviewDetailEventId', $this->getChangedColumns() ) ) { $strSql .= ' parent_review_detail_event_id = ' . $this->sqlParentReviewDetailEventId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'review_detail_id' => $this->getReviewDetailId(),
			'review_event_type_id' => $this->getReviewEventTypeId(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'parent_review_detail_event_id' => $this->getParentReviewDetailEventId()
		);
	}

}
?>