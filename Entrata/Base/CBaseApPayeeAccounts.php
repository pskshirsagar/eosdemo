<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeAccounts extends CEosPluralBase {

	/**
	 * @return CApPayeeAccount[]
	 */
	public static function fetchApPayeeAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayeeAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeAccount
	 */
	public static function fetchApPayeeAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeAccount::class, $objDatabase );
	}

	public static function fetchApPayeeAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_accounts', $objDatabase );
	}

	public static function fetchApPayeeAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccount( sprintf( 'SELECT * FROM ap_payee_accounts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByApRemittanceIdByCid( $intApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE default_ap_remittance_id = %d AND cid = %d', $intApRemittanceId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByDefaultGlAccountIdByCid( $intDefaultGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE default_gl_account_id = %d AND cid = %d', $intDefaultGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByDefaultPropertyIdByCid( $intDefaultPropertyId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE default_property_id = %d AND cid = %d', $intDefaultPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByUtilityBillAccountIdByCid( $intUtilityBillAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE utility_bill_account_id = %d AND cid = %d', $intUtilityBillAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByReplacementApPayeeAccountIdByCid( $intReplacementApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE replacement_ap_payee_account_id = %d AND cid = %d', $intReplacementApPayeeAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByAuditFrequencyIdByCid( $intAuditFrequencyId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE audit_frequency_id = %d AND cid = %d', $intAuditFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByBuyerAccountIdByCid( $intBuyerAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE buyer_account_id = %d AND cid = %d', $intBuyerAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByUtilityBillReceiptTypeIdByCid( $intUtilityBillReceiptTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE utility_bill_receipt_type_id = %d AND cid = %d', $intUtilityBillReceiptTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeAccountsByCatalogRequestStatusTypeIdByCid( $intCatalogRequestStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeAccounts( sprintf( 'SELECT * FROM ap_payee_accounts WHERE catalog_request_status_type_id = %d AND cid = %d', $intCatalogRequestStatusTypeId, $intCid ), $objDatabase );
	}

}
?>