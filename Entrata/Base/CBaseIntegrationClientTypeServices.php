<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientTypeServices
 * Do not add any new functions to this class.
 */

class CBaseIntegrationClientTypeServices extends CEosPluralBase {

	/**
	 * @return CIntegrationClientTypeService[]
	 */
	public static function fetchIntegrationClientTypeServices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIntegrationClientTypeService', $objDatabase );
	}

	/**
	 * @return CIntegrationClientTypeService
	 */
	public static function fetchIntegrationClientTypeService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationClientTypeService', $objDatabase );
	}

	public static function fetchIntegrationClientTypeServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_client_type_services', $objDatabase );
	}

	public static function fetchIntegrationClientTypeServiceById( $intId, $objDatabase ) {
		return self::fetchIntegrationClientTypeService( sprintf( 'SELECT * FROM integration_client_type_services WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIntegrationClientTypeServicesByIntegrationClientTypeId( $intIntegrationClientTypeId, $objDatabase ) {
		return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_client_type_id = %d', ( int ) $intIntegrationClientTypeId ), $objDatabase );
	}

	public static function fetchIntegrationClientTypeServicesByIntegrationServiceId( $intIntegrationServiceId, $objDatabase ) {
		return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_service_id = %d', ( int ) $intIntegrationServiceId ), $objDatabase );
	}

	public static function fetchIntegrationClientTypeServicesByIntegrationSyncTypeId( $intIntegrationSyncTypeId, $objDatabase ) {
		return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_sync_type_id = %d', ( int ) $intIntegrationSyncTypeId ), $objDatabase );
	}

	public static function fetchIntegrationClientTypeServicesByIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId, $objDatabase ) {
		return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_client_status_type_id = %d', ( int ) $intIntegrationClientStatusTypeId ), $objDatabase );
	}

	public static function fetchIntegrationClientTypeServicesByIntegrationPeriodId( $intIntegrationPeriodId, $objDatabase ) {
		return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_period_id = %d', ( int ) $intIntegrationPeriodId ), $objDatabase );
	}

}
?>