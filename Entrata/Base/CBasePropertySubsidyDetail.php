<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySubsidyDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.property_subsidy_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intHudSubsidyRecertificationTypeId;
	protected $m_intTaxCreditSubsidyRecertificationTypeId;
	protected $m_intHomeSubsidyRecertificationTypeId;
	protected $m_intSubsidyTracsVersionId;
	protected $m_intFileTypeId;
	protected $m_strHmfaCode;
	protected $m_strHudProjectName;
	protected $m_strHudProjectNumber;
	protected $m_strHudProjectImaxIdEncrypted;
	protected $m_strHudProjectImaxPasswordEncrypted;
	protected $m_strHudRecipientImaxIdEncrypted;
	protected $m_strTaxCreditProjectName;
	protected $m_strTaxCreditProjectNumber;
	protected $m_boolTaxCreditIsHoldHarmless;
	protected $m_boolTaxCreditUseHeraLimits;
	protected $m_boolIsTaxCreditMultipleBuildingProject;
	protected $m_strHomeStartDate;
	protected $m_strHomeEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strTaxCreditSameBuildingTransfer;
	protected $m_strTaxCreditBuildingToBuildingTransfer;
	protected $m_strTaxCreditProgramToProgramTransfer;
	protected $m_strTaxCreditAnnualRecertificationSchedule;
	protected $m_boolTaxCreditHouseholdCompositionChangeAllowed;
	protected $m_intTaxCreditHhCompChangeSubsidyRecertificationTypeId;
	protected $m_boolIsHudRentCalculationEditable;
	protected $m_boolIsHudHapRequestEditable;

	public function __construct() {
		parent::__construct();

		$this->m_intHudSubsidyRecertificationTypeId = '1';
		$this->m_intTaxCreditSubsidyRecertificationTypeId = '5';
		$this->m_intHomeSubsidyRecertificationTypeId = '1';
		$this->m_boolIsTaxCreditMultipleBuildingProject = true;
		$this->m_strHomeStartDate = '01/01/1970';
		$this->m_strHomeEndDate = '12/31/2099';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strTaxCreditSameBuildingTransfer = 'Partial';
		$this->m_strTaxCreditBuildingToBuildingTransfer = 'Full';
		$this->m_strTaxCreditProgramToProgramTransfer = 'Full';
		$this->m_strTaxCreditAnnualRecertificationSchedule = 'Anniversary Date';
		$this->m_boolTaxCreditHouseholdCompositionChangeAllowed = false;
		$this->m_intTaxCreditHhCompChangeSubsidyRecertificationTypeId = '9';
		$this->m_boolIsHudRentCalculationEditable = false;
		$this->m_boolIsHudHapRequestEditable = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['hud_subsidy_recertification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHudSubsidyRecertificationTypeId', trim( $arrValues['hud_subsidy_recertification_type_id'] ) ); elseif( isset( $arrValues['hud_subsidy_recertification_type_id'] ) ) $this->setHudSubsidyRecertificationTypeId( $arrValues['hud_subsidy_recertification_type_id'] );
		if( isset( $arrValues['tax_credit_subsidy_recertification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxCreditSubsidyRecertificationTypeId', trim( $arrValues['tax_credit_subsidy_recertification_type_id'] ) ); elseif( isset( $arrValues['tax_credit_subsidy_recertification_type_id'] ) ) $this->setTaxCreditSubsidyRecertificationTypeId( $arrValues['tax_credit_subsidy_recertification_type_id'] );
		if( isset( $arrValues['home_subsidy_recertification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHomeSubsidyRecertificationTypeId', trim( $arrValues['home_subsidy_recertification_type_id'] ) ); elseif( isset( $arrValues['home_subsidy_recertification_type_id'] ) ) $this->setHomeSubsidyRecertificationTypeId( $arrValues['home_subsidy_recertification_type_id'] );
		if( isset( $arrValues['subsidy_tracs_version_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTracsVersionId', trim( $arrValues['subsidy_tracs_version_id'] ) ); elseif( isset( $arrValues['subsidy_tracs_version_id'] ) ) $this->setSubsidyTracsVersionId( $arrValues['subsidy_tracs_version_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['hmfa_code'] ) && $boolDirectSet ) $this->set( 'm_strHmfaCode', trim( stripcslashes( $arrValues['hmfa_code'] ) ) ); elseif( isset( $arrValues['hmfa_code'] ) ) $this->setHmfaCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hmfa_code'] ) : $arrValues['hmfa_code'] );
		if( isset( $arrValues['hud_project_name'] ) && $boolDirectSet ) $this->set( 'm_strHudProjectName', trim( stripcslashes( $arrValues['hud_project_name'] ) ) ); elseif( isset( $arrValues['hud_project_name'] ) ) $this->setHudProjectName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_project_name'] ) : $arrValues['hud_project_name'] );
		if( isset( $arrValues['hud_project_number'] ) && $boolDirectSet ) $this->set( 'm_strHudProjectNumber', trim( stripcslashes( $arrValues['hud_project_number'] ) ) ); elseif( isset( $arrValues['hud_project_number'] ) ) $this->setHudProjectNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_project_number'] ) : $arrValues['hud_project_number'] );
		if( isset( $arrValues['hud_project_imax_id_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHudProjectImaxIdEncrypted', trim( stripcslashes( $arrValues['hud_project_imax_id_encrypted'] ) ) ); elseif( isset( $arrValues['hud_project_imax_id_encrypted'] ) ) $this->setHudProjectImaxIdEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_project_imax_id_encrypted'] ) : $arrValues['hud_project_imax_id_encrypted'] );
		if( isset( $arrValues['hud_project_imax_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHudProjectImaxPasswordEncrypted', trim( stripcslashes( $arrValues['hud_project_imax_password_encrypted'] ) ) ); elseif( isset( $arrValues['hud_project_imax_password_encrypted'] ) ) $this->setHudProjectImaxPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_project_imax_password_encrypted'] ) : $arrValues['hud_project_imax_password_encrypted'] );
		if( isset( $arrValues['hud_recipient_imax_id_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHudRecipientImaxIdEncrypted', trim( stripcslashes( $arrValues['hud_recipient_imax_id_encrypted'] ) ) ); elseif( isset( $arrValues['hud_recipient_imax_id_encrypted'] ) ) $this->setHudRecipientImaxIdEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_recipient_imax_id_encrypted'] ) : $arrValues['hud_recipient_imax_id_encrypted'] );
		if( isset( $arrValues['tax_credit_project_name'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditProjectName', trim( stripcslashes( $arrValues['tax_credit_project_name'] ) ) ); elseif( isset( $arrValues['tax_credit_project_name'] ) ) $this->setTaxCreditProjectName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_project_name'] ) : $arrValues['tax_credit_project_name'] );
		if( isset( $arrValues['tax_credit_project_number'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditProjectNumber', trim( stripcslashes( $arrValues['tax_credit_project_number'] ) ) ); elseif( isset( $arrValues['tax_credit_project_number'] ) ) $this->setTaxCreditProjectNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_project_number'] ) : $arrValues['tax_credit_project_number'] );
		if( isset( $arrValues['tax_credit_is_hold_harmless'] ) && $boolDirectSet ) $this->set( 'm_boolTaxCreditIsHoldHarmless', trim( stripcslashes( $arrValues['tax_credit_is_hold_harmless'] ) ) ); elseif( isset( $arrValues['tax_credit_is_hold_harmless'] ) ) $this->setTaxCreditIsHoldHarmless( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_is_hold_harmless'] ) : $arrValues['tax_credit_is_hold_harmless'] );
		if( isset( $arrValues['tax_credit_use_hera_limits'] ) && $boolDirectSet ) $this->set( 'm_boolTaxCreditUseHeraLimits', trim( stripcslashes( $arrValues['tax_credit_use_hera_limits'] ) ) ); elseif( isset( $arrValues['tax_credit_use_hera_limits'] ) ) $this->setTaxCreditUseHeraLimits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_use_hera_limits'] ) : $arrValues['tax_credit_use_hera_limits'] );
		if( isset( $arrValues['is_tax_credit_multiple_building_project'] ) && $boolDirectSet ) $this->set( 'm_boolIsTaxCreditMultipleBuildingProject', trim( stripcslashes( $arrValues['is_tax_credit_multiple_building_project'] ) ) ); elseif( isset( $arrValues['is_tax_credit_multiple_building_project'] ) ) $this->setIsTaxCreditMultipleBuildingProject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_tax_credit_multiple_building_project'] ) : $arrValues['is_tax_credit_multiple_building_project'] );
		if( isset( $arrValues['home_start_date'] ) && $boolDirectSet ) $this->set( 'm_strHomeStartDate', trim( $arrValues['home_start_date'] ) ); elseif( isset( $arrValues['home_start_date'] ) ) $this->setHomeStartDate( $arrValues['home_start_date'] );
		if( isset( $arrValues['home_end_date'] ) && $boolDirectSet ) $this->set( 'm_strHomeEndDate', trim( $arrValues['home_end_date'] ) ); elseif( isset( $arrValues['home_end_date'] ) ) $this->setHomeEndDate( $arrValues['home_end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['tax_credit_same_building_transfer'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditSameBuildingTransfer', trim( stripcslashes( $arrValues['tax_credit_same_building_transfer'] ) ) ); elseif( isset( $arrValues['tax_credit_same_building_transfer'] ) ) $this->setTaxCreditSameBuildingTransfer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_same_building_transfer'] ) : $arrValues['tax_credit_same_building_transfer'] );
		if( isset( $arrValues['tax_credit_building_to_building_transfer'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditBuildingToBuildingTransfer', trim( stripcslashes( $arrValues['tax_credit_building_to_building_transfer'] ) ) ); elseif( isset( $arrValues['tax_credit_building_to_building_transfer'] ) ) $this->setTaxCreditBuildingToBuildingTransfer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_building_to_building_transfer'] ) : $arrValues['tax_credit_building_to_building_transfer'] );
		if( isset( $arrValues['tax_credit_program_to_program_transfer'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditProgramToProgramTransfer', trim( stripcslashes( $arrValues['tax_credit_program_to_program_transfer'] ) ) ); elseif( isset( $arrValues['tax_credit_program_to_program_transfer'] ) ) $this->setTaxCreditProgramToProgramTransfer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_program_to_program_transfer'] ) : $arrValues['tax_credit_program_to_program_transfer'] );
		if( isset( $arrValues['tax_credit_annual_recertification_schedule'] ) && $boolDirectSet ) $this->set( 'm_strTaxCreditAnnualRecertificationSchedule', trim( stripcslashes( $arrValues['tax_credit_annual_recertification_schedule'] ) ) ); elseif( isset( $arrValues['tax_credit_annual_recertification_schedule'] ) ) $this->setTaxCreditAnnualRecertificationSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_annual_recertification_schedule'] ) : $arrValues['tax_credit_annual_recertification_schedule'] );
		if( isset( $arrValues['tax_credit_household_composition_change_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolTaxCreditHouseholdCompositionChangeAllowed', trim( stripcslashes( $arrValues['tax_credit_household_composition_change_allowed'] ) ) ); elseif( isset( $arrValues['tax_credit_household_composition_change_allowed'] ) ) $this->setTaxCreditHouseholdCompositionChangeAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_credit_household_composition_change_allowed'] ) : $arrValues['tax_credit_household_composition_change_allowed'] );
		if( isset( $arrValues['tax_credit_hh_comp_change_subsidy_recertification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxCreditHhCompChangeSubsidyRecertificationTypeId', trim( $arrValues['tax_credit_hh_comp_change_subsidy_recertification_type_id'] ) ); elseif( isset( $arrValues['tax_credit_hh_comp_change_subsidy_recertification_type_id'] ) ) $this->setTaxCreditHhCompChangeSubsidyRecertificationTypeId( $arrValues['tax_credit_hh_comp_change_subsidy_recertification_type_id'] );
		if( isset( $arrValues['is_hud_rent_calculation_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsHudRentCalculationEditable', trim( stripcslashes( $arrValues['is_hud_rent_calculation_editable'] ) ) ); elseif( isset( $arrValues['is_hud_rent_calculation_editable'] ) ) $this->setIsHudRentCalculationEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hud_rent_calculation_editable'] ) : $arrValues['is_hud_rent_calculation_editable'] );
		if( isset( $arrValues['is_hud_hap_request_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsHudHapRequestEditable', trim( stripcslashes( $arrValues['is_hud_hap_request_editable'] ) ) ); elseif( isset( $arrValues['is_hud_hap_request_editable'] ) ) $this->setIsHudHapRequestEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hud_hap_request_editable'] ) : $arrValues['is_hud_hap_request_editable'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setHudSubsidyRecertificationTypeId( $intHudSubsidyRecertificationTypeId ) {
		$this->set( 'm_intHudSubsidyRecertificationTypeId', CStrings::strToIntDef( $intHudSubsidyRecertificationTypeId, NULL, false ) );
	}

	public function getHudSubsidyRecertificationTypeId() {
		return $this->m_intHudSubsidyRecertificationTypeId;
	}

	public function sqlHudSubsidyRecertificationTypeId() {
		return ( true == isset( $this->m_intHudSubsidyRecertificationTypeId ) ) ? ( string ) $this->m_intHudSubsidyRecertificationTypeId : '1';
	}

	public function setTaxCreditSubsidyRecertificationTypeId( $intTaxCreditSubsidyRecertificationTypeId ) {
		$this->set( 'm_intTaxCreditSubsidyRecertificationTypeId', CStrings::strToIntDef( $intTaxCreditSubsidyRecertificationTypeId, NULL, false ) );
	}

	public function getTaxCreditSubsidyRecertificationTypeId() {
		return $this->m_intTaxCreditSubsidyRecertificationTypeId;
	}

	public function sqlTaxCreditSubsidyRecertificationTypeId() {
		return ( true == isset( $this->m_intTaxCreditSubsidyRecertificationTypeId ) ) ? ( string ) $this->m_intTaxCreditSubsidyRecertificationTypeId : '5';
	}

	public function setHomeSubsidyRecertificationTypeId( $intHomeSubsidyRecertificationTypeId ) {
		$this->set( 'm_intHomeSubsidyRecertificationTypeId', CStrings::strToIntDef( $intHomeSubsidyRecertificationTypeId, NULL, false ) );
	}

	public function getHomeSubsidyRecertificationTypeId() {
		return $this->m_intHomeSubsidyRecertificationTypeId;
	}

	public function sqlHomeSubsidyRecertificationTypeId() {
		return ( true == isset( $this->m_intHomeSubsidyRecertificationTypeId ) ) ? ( string ) $this->m_intHomeSubsidyRecertificationTypeId : '1';
	}

	public function setSubsidyTracsVersionId( $intSubsidyTracsVersionId ) {
		$this->set( 'm_intSubsidyTracsVersionId', CStrings::strToIntDef( $intSubsidyTracsVersionId, NULL, false ) );
	}

	public function getSubsidyTracsVersionId() {
		return $this->m_intSubsidyTracsVersionId;
	}

	public function sqlSubsidyTracsVersionId() {
		return ( true == isset( $this->m_intSubsidyTracsVersionId ) ) ? ( string ) $this->m_intSubsidyTracsVersionId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setHmfaCode( $strHmfaCode ) {
		$this->set( 'm_strHmfaCode', CStrings::strTrimDef( $strHmfaCode, 16, NULL, true ) );
	}

	public function getHmfaCode() {
		return $this->m_strHmfaCode;
	}

	public function sqlHmfaCode() {
		return ( true == isset( $this->m_strHmfaCode ) ) ? '\'' . addslashes( $this->m_strHmfaCode ) . '\'' : 'NULL';
	}

	public function setHudProjectName( $strHudProjectName ) {
		$this->set( 'm_strHudProjectName', CStrings::strTrimDef( $strHudProjectName, 35, NULL, true ) );
	}

	public function getHudProjectName() {
		return $this->m_strHudProjectName;
	}

	public function sqlHudProjectName() {
		return ( true == isset( $this->m_strHudProjectName ) ) ? '\'' . addslashes( $this->m_strHudProjectName ) . '\'' : 'NULL';
	}

	public function setHudProjectNumber( $strHudProjectNumber ) {
		$this->set( 'm_strHudProjectNumber', CStrings::strTrimDef( $strHudProjectNumber, 8, NULL, true ) );
	}

	public function getHudProjectNumber() {
		return $this->m_strHudProjectNumber;
	}

	public function sqlHudProjectNumber() {
		return ( true == isset( $this->m_strHudProjectNumber ) ) ? '\'' . addslashes( $this->m_strHudProjectNumber ) . '\'' : 'NULL';
	}

	public function setHudProjectImaxIdEncrypted( $strHudProjectImaxIdEncrypted ) {
		$this->set( 'm_strHudProjectImaxIdEncrypted', CStrings::strTrimDef( $strHudProjectImaxIdEncrypted, 240, NULL, true ) );
	}

	public function getHudProjectImaxIdEncrypted() {
		return $this->m_strHudProjectImaxIdEncrypted;
	}

	public function sqlHudProjectImaxIdEncrypted() {
		return ( true == isset( $this->m_strHudProjectImaxIdEncrypted ) ) ? '\'' . addslashes( $this->m_strHudProjectImaxIdEncrypted ) . '\'' : 'NULL';
	}

	public function setHudProjectImaxPasswordEncrypted( $strHudProjectImaxPasswordEncrypted ) {
		$this->set( 'm_strHudProjectImaxPasswordEncrypted', CStrings::strTrimDef( $strHudProjectImaxPasswordEncrypted, 240, NULL, true ) );
	}

	public function getHudProjectImaxPasswordEncrypted() {
		return $this->m_strHudProjectImaxPasswordEncrypted;
	}

	public function sqlHudProjectImaxPasswordEncrypted() {
		return ( true == isset( $this->m_strHudProjectImaxPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strHudProjectImaxPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setHudRecipientImaxIdEncrypted( $strHudRecipientImaxIdEncrypted ) {
		$this->set( 'm_strHudRecipientImaxIdEncrypted', CStrings::strTrimDef( $strHudRecipientImaxIdEncrypted, 240, NULL, true ) );
	}

	public function getHudRecipientImaxIdEncrypted() {
		return $this->m_strHudRecipientImaxIdEncrypted;
	}

	public function sqlHudRecipientImaxIdEncrypted() {
		return ( true == isset( $this->m_strHudRecipientImaxIdEncrypted ) ) ? '\'' . addslashes( $this->m_strHudRecipientImaxIdEncrypted ) . '\'' : 'NULL';
	}

	public function setTaxCreditProjectName( $strTaxCreditProjectName ) {
		$this->set( 'm_strTaxCreditProjectName', CStrings::strTrimDef( $strTaxCreditProjectName, 35, NULL, true ) );
	}

	public function getTaxCreditProjectName() {
		return $this->m_strTaxCreditProjectName;
	}

	public function sqlTaxCreditProjectName() {
		return ( true == isset( $this->m_strTaxCreditProjectName ) ) ? '\'' . addslashes( $this->m_strTaxCreditProjectName ) . '\'' : 'NULL';
	}

	public function setTaxCreditProjectNumber( $strTaxCreditProjectNumber ) {
		$this->set( 'm_strTaxCreditProjectNumber', CStrings::strTrimDef( $strTaxCreditProjectNumber, 50, NULL, true ) );
	}

	public function getTaxCreditProjectNumber() {
		return $this->m_strTaxCreditProjectNumber;
	}

	public function sqlTaxCreditProjectNumber() {
		return ( true == isset( $this->m_strTaxCreditProjectNumber ) ) ? '\'' . addslashes( $this->m_strTaxCreditProjectNumber ) . '\'' : 'NULL';
	}

	public function setTaxCreditIsHoldHarmless( $boolTaxCreditIsHoldHarmless ) {
		$this->set( 'm_boolTaxCreditIsHoldHarmless', CStrings::strToBool( $boolTaxCreditIsHoldHarmless ) );
	}

	public function getTaxCreditIsHoldHarmless() {
		return $this->m_boolTaxCreditIsHoldHarmless;
	}

	public function sqlTaxCreditIsHoldHarmless() {
		return ( true == isset( $this->m_boolTaxCreditIsHoldHarmless ) ) ? '\'' . ( true == ( bool ) $this->m_boolTaxCreditIsHoldHarmless ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTaxCreditUseHeraLimits( $boolTaxCreditUseHeraLimits ) {
		$this->set( 'm_boolTaxCreditUseHeraLimits', CStrings::strToBool( $boolTaxCreditUseHeraLimits ) );
	}

	public function getTaxCreditUseHeraLimits() {
		return $this->m_boolTaxCreditUseHeraLimits;
	}

	public function sqlTaxCreditUseHeraLimits() {
		return ( true == isset( $this->m_boolTaxCreditUseHeraLimits ) ) ? '\'' . ( true == ( bool ) $this->m_boolTaxCreditUseHeraLimits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTaxCreditMultipleBuildingProject( $boolIsTaxCreditMultipleBuildingProject ) {
		$this->set( 'm_boolIsTaxCreditMultipleBuildingProject', CStrings::strToBool( $boolIsTaxCreditMultipleBuildingProject ) );
	}

	public function getIsTaxCreditMultipleBuildingProject() {
		return $this->m_boolIsTaxCreditMultipleBuildingProject;
	}

	public function sqlIsTaxCreditMultipleBuildingProject() {
		return ( true == isset( $this->m_boolIsTaxCreditMultipleBuildingProject ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTaxCreditMultipleBuildingProject ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHomeStartDate( $strHomeStartDate ) {
		$this->set( 'm_strHomeStartDate', CStrings::strTrimDef( $strHomeStartDate, -1, NULL, true ) );
	}

	public function getHomeStartDate() {
		return $this->m_strHomeStartDate;
	}

	public function sqlHomeStartDate() {
		return ( true == isset( $this->m_strHomeStartDate ) ) ? '\'' . $this->m_strHomeStartDate . '\'' : 'NOW()';
	}

	public function setHomeEndDate( $strHomeEndDate ) {
		$this->set( 'm_strHomeEndDate', CStrings::strTrimDef( $strHomeEndDate, -1, NULL, true ) );
	}

	public function getHomeEndDate() {
		return $this->m_strHomeEndDate;
	}

	public function sqlHomeEndDate() {
		return ( true == isset( $this->m_strHomeEndDate ) ) ? '\'' . $this->m_strHomeEndDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTaxCreditSameBuildingTransfer( $strTaxCreditSameBuildingTransfer ) {
		$this->set( 'm_strTaxCreditSameBuildingTransfer', CStrings::strTrimDef( $strTaxCreditSameBuildingTransfer, -1, NULL, true ) );
	}

	public function getTaxCreditSameBuildingTransfer() {
		return $this->m_strTaxCreditSameBuildingTransfer;
	}

	public function sqlTaxCreditSameBuildingTransfer() {
		return ( true == isset( $this->m_strTaxCreditSameBuildingTransfer ) ) ? '\'' . addslashes( $this->m_strTaxCreditSameBuildingTransfer ) . '\'' : '\'Partial\'';
	}

	public function setTaxCreditBuildingToBuildingTransfer( $strTaxCreditBuildingToBuildingTransfer ) {
		$this->set( 'm_strTaxCreditBuildingToBuildingTransfer', CStrings::strTrimDef( $strTaxCreditBuildingToBuildingTransfer, -1, NULL, true ) );
	}

	public function getTaxCreditBuildingToBuildingTransfer() {
		return $this->m_strTaxCreditBuildingToBuildingTransfer;
	}

	public function sqlTaxCreditBuildingToBuildingTransfer() {
		return ( true == isset( $this->m_strTaxCreditBuildingToBuildingTransfer ) ) ? '\'' . addslashes( $this->m_strTaxCreditBuildingToBuildingTransfer ) . '\'' : '\'Full\'';
	}

	public function setTaxCreditProgramToProgramTransfer( $strTaxCreditProgramToProgramTransfer ) {
		$this->set( 'm_strTaxCreditProgramToProgramTransfer', CStrings::strTrimDef( $strTaxCreditProgramToProgramTransfer, -1, NULL, true ) );
	}

	public function getTaxCreditProgramToProgramTransfer() {
		return $this->m_strTaxCreditProgramToProgramTransfer;
	}

	public function sqlTaxCreditProgramToProgramTransfer() {
		return ( true == isset( $this->m_strTaxCreditProgramToProgramTransfer ) ) ? '\'' . addslashes( $this->m_strTaxCreditProgramToProgramTransfer ) . '\'' : '\'Full\'';
	}

	public function setTaxCreditAnnualRecertificationSchedule( $strTaxCreditAnnualRecertificationSchedule ) {
		$this->set( 'm_strTaxCreditAnnualRecertificationSchedule', CStrings::strTrimDef( $strTaxCreditAnnualRecertificationSchedule, -1, NULL, true ) );
	}

	public function getTaxCreditAnnualRecertificationSchedule() {
		return $this->m_strTaxCreditAnnualRecertificationSchedule;
	}

	public function sqlTaxCreditAnnualRecertificationSchedule() {
		return ( true == isset( $this->m_strTaxCreditAnnualRecertificationSchedule ) ) ? '\'' . addslashes( $this->m_strTaxCreditAnnualRecertificationSchedule ) . '\'' : '\'Anniversary Date\'';
	}

	public function setTaxCreditHouseholdCompositionChangeAllowed( $boolTaxCreditHouseholdCompositionChangeAllowed ) {
		$this->set( 'm_boolTaxCreditHouseholdCompositionChangeAllowed', CStrings::strToBool( $boolTaxCreditHouseholdCompositionChangeAllowed ) );
	}

	public function getTaxCreditHouseholdCompositionChangeAllowed() {
		return $this->m_boolTaxCreditHouseholdCompositionChangeAllowed;
	}

	public function sqlTaxCreditHouseholdCompositionChangeAllowed() {
		return ( true == isset( $this->m_boolTaxCreditHouseholdCompositionChangeAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolTaxCreditHouseholdCompositionChangeAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTaxCreditHhCompChangeSubsidyRecertificationTypeId( $intTaxCreditHhCompChangeSubsidyRecertificationTypeId ) {
		$this->set( 'm_intTaxCreditHhCompChangeSubsidyRecertificationTypeId', CStrings::strToIntDef( $intTaxCreditHhCompChangeSubsidyRecertificationTypeId, NULL, false ) );
	}

	public function getTaxCreditHhCompChangeSubsidyRecertificationTypeId() {
		return $this->m_intTaxCreditHhCompChangeSubsidyRecertificationTypeId;
	}

	public function sqlTaxCreditHhCompChangeSubsidyRecertificationTypeId() {
		return ( true == isset( $this->m_intTaxCreditHhCompChangeSubsidyRecertificationTypeId ) ) ? ( string ) $this->m_intTaxCreditHhCompChangeSubsidyRecertificationTypeId : '9';
	}

	public function setIsHudRentCalculationEditable( $boolIsHudRentCalculationEditable ) {
		$this->set( 'm_boolIsHudRentCalculationEditable', CStrings::strToBool( $boolIsHudRentCalculationEditable ) );
	}

	public function getIsHudRentCalculationEditable() {
		return $this->m_boolIsHudRentCalculationEditable;
	}

	public function sqlIsHudRentCalculationEditable() {
		return ( true == isset( $this->m_boolIsHudRentCalculationEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHudRentCalculationEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHudHapRequestEditable( $boolIsHudHapRequestEditable ) {
		$this->set( 'm_boolIsHudHapRequestEditable', CStrings::strToBool( $boolIsHudHapRequestEditable ) );
	}

	public function getIsHudHapRequestEditable() {
		return $this->m_boolIsHudHapRequestEditable;
	}

	public function sqlIsHudHapRequestEditable() {
		return ( true == isset( $this->m_boolIsHudHapRequestEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHudHapRequestEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, hud_subsidy_recertification_type_id, tax_credit_subsidy_recertification_type_id, home_subsidy_recertification_type_id, subsidy_tracs_version_id, file_type_id, hmfa_code, hud_project_name, hud_project_number, hud_project_imax_id_encrypted, hud_project_imax_password_encrypted, hud_recipient_imax_id_encrypted, tax_credit_project_name, tax_credit_project_number, tax_credit_is_hold_harmless, tax_credit_use_hera_limits, is_tax_credit_multiple_building_project, home_start_date, home_end_date, updated_by, updated_on, created_by, created_on, tax_credit_same_building_transfer, tax_credit_building_to_building_transfer, tax_credit_program_to_program_transfer, tax_credit_annual_recertification_schedule, tax_credit_household_composition_change_allowed, tax_credit_hh_comp_change_subsidy_recertification_type_id, is_hud_rent_calculation_editable, is_hud_hap_request_editable )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlHudSubsidyRecertificationTypeId() . ', ' .
						$this->sqlTaxCreditSubsidyRecertificationTypeId() . ', ' .
						$this->sqlHomeSubsidyRecertificationTypeId() . ', ' .
						$this->sqlSubsidyTracsVersionId() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlHmfaCode() . ', ' .
						$this->sqlHudProjectName() . ', ' .
						$this->sqlHudProjectNumber() . ', ' .
						$this->sqlHudProjectImaxIdEncrypted() . ', ' .
						$this->sqlHudProjectImaxPasswordEncrypted() . ', ' .
						$this->sqlHudRecipientImaxIdEncrypted() . ', ' .
						$this->sqlTaxCreditProjectName() . ', ' .
						$this->sqlTaxCreditProjectNumber() . ', ' .
						$this->sqlTaxCreditIsHoldHarmless() . ', ' .
						$this->sqlTaxCreditUseHeraLimits() . ', ' .
						$this->sqlIsTaxCreditMultipleBuildingProject() . ', ' .
						$this->sqlHomeStartDate() . ', ' .
						$this->sqlHomeEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTaxCreditSameBuildingTransfer() . ', ' .
						$this->sqlTaxCreditBuildingToBuildingTransfer() . ', ' .
						$this->sqlTaxCreditProgramToProgramTransfer() . ', ' .
						$this->sqlTaxCreditAnnualRecertificationSchedule() . ', ' .
						$this->sqlTaxCreditHouseholdCompositionChangeAllowed() . ', ' .
						$this->sqlTaxCreditHhCompChangeSubsidyRecertificationTypeId() . ', ' .
						$this->sqlIsHudRentCalculationEditable() . ', ' .
						$this->sqlIsHudHapRequestEditable() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_subsidy_recertification_type_id = ' . $this->sqlHudSubsidyRecertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'HudSubsidyRecertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' hud_subsidy_recertification_type_id = ' . $this->sqlHudSubsidyRecertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_subsidy_recertification_type_id = ' . $this->sqlTaxCreditSubsidyRecertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'TaxCreditSubsidyRecertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_subsidy_recertification_type_id = ' . $this->sqlTaxCreditSubsidyRecertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_subsidy_recertification_type_id = ' . $this->sqlHomeSubsidyRecertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'HomeSubsidyRecertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' home_subsidy_recertification_type_id = ' . $this->sqlHomeSubsidyRecertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_tracs_version_id = ' . $this->sqlSubsidyTracsVersionId(). ',' ; } elseif( true == array_key_exists( 'SubsidyTracsVersionId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_tracs_version_id = ' . $this->sqlSubsidyTracsVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId(). ',' ; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode(). ',' ; } elseif( true == array_key_exists( 'HmfaCode', $this->getChangedColumns() ) ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_project_name = ' . $this->sqlHudProjectName(). ',' ; } elseif( true == array_key_exists( 'HudProjectName', $this->getChangedColumns() ) ) { $strSql .= ' hud_project_name = ' . $this->sqlHudProjectName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_project_number = ' . $this->sqlHudProjectNumber(). ',' ; } elseif( true == array_key_exists( 'HudProjectNumber', $this->getChangedColumns() ) ) { $strSql .= ' hud_project_number = ' . $this->sqlHudProjectNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_project_imax_id_encrypted = ' . $this->sqlHudProjectImaxIdEncrypted(). ',' ; } elseif( true == array_key_exists( 'HudProjectImaxIdEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hud_project_imax_id_encrypted = ' . $this->sqlHudProjectImaxIdEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_project_imax_password_encrypted = ' . $this->sqlHudProjectImaxPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'HudProjectImaxPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hud_project_imax_password_encrypted = ' . $this->sqlHudProjectImaxPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hud_recipient_imax_id_encrypted = ' . $this->sqlHudRecipientImaxIdEncrypted(). ',' ; } elseif( true == array_key_exists( 'HudRecipientImaxIdEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hud_recipient_imax_id_encrypted = ' . $this->sqlHudRecipientImaxIdEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_project_name = ' . $this->sqlTaxCreditProjectName(). ',' ; } elseif( true == array_key_exists( 'TaxCreditProjectName', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_project_name = ' . $this->sqlTaxCreditProjectName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_project_number = ' . $this->sqlTaxCreditProjectNumber(). ',' ; } elseif( true == array_key_exists( 'TaxCreditProjectNumber', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_project_number = ' . $this->sqlTaxCreditProjectNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_is_hold_harmless = ' . $this->sqlTaxCreditIsHoldHarmless(). ',' ; } elseif( true == array_key_exists( 'TaxCreditIsHoldHarmless', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_is_hold_harmless = ' . $this->sqlTaxCreditIsHoldHarmless() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_use_hera_limits = ' . $this->sqlTaxCreditUseHeraLimits(). ',' ; } elseif( true == array_key_exists( 'TaxCreditUseHeraLimits', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_use_hera_limits = ' . $this->sqlTaxCreditUseHeraLimits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_tax_credit_multiple_building_project = ' . $this->sqlIsTaxCreditMultipleBuildingProject(). ',' ; } elseif( true == array_key_exists( 'IsTaxCreditMultipleBuildingProject', $this->getChangedColumns() ) ) { $strSql .= ' is_tax_credit_multiple_building_project = ' . $this->sqlIsTaxCreditMultipleBuildingProject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_start_date = ' . $this->sqlHomeStartDate(). ',' ; } elseif( true == array_key_exists( 'HomeStartDate', $this->getChangedColumns() ) ) { $strSql .= ' home_start_date = ' . $this->sqlHomeStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_end_date = ' . $this->sqlHomeEndDate(). ',' ; } elseif( true == array_key_exists( 'HomeEndDate', $this->getChangedColumns() ) ) { $strSql .= ' home_end_date = ' . $this->sqlHomeEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_same_building_transfer = ' . $this->sqlTaxCreditSameBuildingTransfer(). ',' ; } elseif( true == array_key_exists( 'TaxCreditSameBuildingTransfer', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_same_building_transfer = ' . $this->sqlTaxCreditSameBuildingTransfer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_building_to_building_transfer = ' . $this->sqlTaxCreditBuildingToBuildingTransfer(). ',' ; } elseif( true == array_key_exists( 'TaxCreditBuildingToBuildingTransfer', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_building_to_building_transfer = ' . $this->sqlTaxCreditBuildingToBuildingTransfer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_program_to_program_transfer = ' . $this->sqlTaxCreditProgramToProgramTransfer(). ',' ; } elseif( true == array_key_exists( 'TaxCreditProgramToProgramTransfer', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_program_to_program_transfer = ' . $this->sqlTaxCreditProgramToProgramTransfer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_annual_recertification_schedule = ' . $this->sqlTaxCreditAnnualRecertificationSchedule(). ',' ; } elseif( true == array_key_exists( 'TaxCreditAnnualRecertificationSchedule', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_annual_recertification_schedule = ' . $this->sqlTaxCreditAnnualRecertificationSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_household_composition_change_allowed = ' . $this->sqlTaxCreditHouseholdCompositionChangeAllowed(). ',' ; } elseif( true == array_key_exists( 'TaxCreditHouseholdCompositionChangeAllowed', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_household_composition_change_allowed = ' . $this->sqlTaxCreditHouseholdCompositionChangeAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_credit_hh_comp_change_subsidy_recertification_type_id = ' . $this->sqlTaxCreditHhCompChangeSubsidyRecertificationTypeId(). ',' ; } elseif( true == array_key_exists( 'TaxCreditHhCompChangeSubsidyRecertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_credit_hh_comp_change_subsidy_recertification_type_id = ' . $this->sqlTaxCreditHhCompChangeSubsidyRecertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hud_rent_calculation_editable = ' . $this->sqlIsHudRentCalculationEditable(). ',' ; } elseif( true == array_key_exists( 'IsHudRentCalculationEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_hud_rent_calculation_editable = ' . $this->sqlIsHudRentCalculationEditable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hud_hap_request_editable = ' . $this->sqlIsHudHapRequestEditable(). ',' ; } elseif( true == array_key_exists( 'IsHudHapRequestEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_hud_hap_request_editable = ' . $this->sqlIsHudHapRequestEditable() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'hud_subsidy_recertification_type_id' => $this->getHudSubsidyRecertificationTypeId(),
			'tax_credit_subsidy_recertification_type_id' => $this->getTaxCreditSubsidyRecertificationTypeId(),
			'home_subsidy_recertification_type_id' => $this->getHomeSubsidyRecertificationTypeId(),
			'subsidy_tracs_version_id' => $this->getSubsidyTracsVersionId(),
			'file_type_id' => $this->getFileTypeId(),
			'hmfa_code' => $this->getHmfaCode(),
			'hud_project_name' => $this->getHudProjectName(),
			'hud_project_number' => $this->getHudProjectNumber(),
			'hud_project_imax_id_encrypted' => $this->getHudProjectImaxIdEncrypted(),
			'hud_project_imax_password_encrypted' => $this->getHudProjectImaxPasswordEncrypted(),
			'hud_recipient_imax_id_encrypted' => $this->getHudRecipientImaxIdEncrypted(),
			'tax_credit_project_name' => $this->getTaxCreditProjectName(),
			'tax_credit_project_number' => $this->getTaxCreditProjectNumber(),
			'tax_credit_is_hold_harmless' => $this->getTaxCreditIsHoldHarmless(),
			'tax_credit_use_hera_limits' => $this->getTaxCreditUseHeraLimits(),
			'is_tax_credit_multiple_building_project' => $this->getIsTaxCreditMultipleBuildingProject(),
			'home_start_date' => $this->getHomeStartDate(),
			'home_end_date' => $this->getHomeEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'tax_credit_same_building_transfer' => $this->getTaxCreditSameBuildingTransfer(),
			'tax_credit_building_to_building_transfer' => $this->getTaxCreditBuildingToBuildingTransfer(),
			'tax_credit_program_to_program_transfer' => $this->getTaxCreditProgramToProgramTransfer(),
			'tax_credit_annual_recertification_schedule' => $this->getTaxCreditAnnualRecertificationSchedule(),
			'tax_credit_household_composition_change_allowed' => $this->getTaxCreditHouseholdCompositionChangeAllowed(),
			'tax_credit_hh_comp_change_subsidy_recertification_type_id' => $this->getTaxCreditHhCompChangeSubsidyRecertificationTypeId(),
			'is_hud_rent_calculation_editable' => $this->getIsHudRentCalculationEditable(),
			'is_hud_hap_request_editable' => $this->getIsHudHapRequestEditable()
		);
	}

}
?>
