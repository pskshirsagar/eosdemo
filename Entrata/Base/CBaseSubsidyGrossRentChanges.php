<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyGrossRentChanges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyGrossRentChanges extends CEosPluralBase {

	/**
	 * @return CSubsidyGrossRentChange[]
	 */
	public static function fetchSubsidyGrossRentChanges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyGrossRentChange', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyGrossRentChange
	 */
	public static function fetchSubsidyGrossRentChange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyGrossRentChange', $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_gross_rent_changes', $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyGrossRentChange( sprintf( 'SELECT * FROM subsidy_gross_rent_changes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangesByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyGrossRentChanges( sprintf( 'SELECT * FROM subsidy_gross_rent_changes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyGrossRentChanges( sprintf( 'SELECT * FROM subsidy_gross_rent_changes WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangesBySubsidyContractIdByCid( $intSubsidyContractId, $intCid, $objDatabase ) {
		return self::fetchSubsidyGrossRentChanges( sprintf( 'SELECT * FROM subsidy_gross_rent_changes WHERE subsidy_contract_id = %d AND cid = %d', ( int ) $intSubsidyContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyGrossRentChangesByCorrectedSubsidyGrossRentChangeIdByCid( $intCorrectedSubsidyGrossRentChangeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyGrossRentChanges( sprintf( 'SELECT * FROM subsidy_gross_rent_changes WHERE corrected_subsidy_gross_rent_change_id = %d AND cid = %d', ( int ) $intCorrectedSubsidyGrossRentChangeId, ( int ) $intCid ), $objDatabase );
	}

}
?>