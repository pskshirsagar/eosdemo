<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerContacts extends CEosPluralBase {

	/**
	 * @return CCustomerContact[]
	 */
	public static function fetchCustomerContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerContact', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerContact
	 */
	public static function fetchCustomerContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerContact', $objDatabase );
	}

	public static function fetchCustomerContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_contacts', $objDatabase );
	}

	public static function fetchCustomerContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerContact( sprintf( 'SELECT * FROM customer_contacts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerContactsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerContacts( sprintf( 'SELECT * FROM customer_contacts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerContacts( sprintf( 'SELECT * FROM customer_contacts WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerContactsByCustomerContactTypeIdByCid( $intCustomerContactTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerContacts( sprintf( 'SELECT * FROM customer_contacts WHERE customer_contact_type_id = %d AND cid = %d', ( int ) $intCustomerContactTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>