<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserDashboardItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserDashboardItems extends CEosPluralBase {

	/**
	 * @return CCompanyUserDashboardItem[]
	 */
	public static function fetchCompanyUserDashboardItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserDashboardItem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserDashboardItem
	 */
	public static function fetchCompanyUserDashboardItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserDashboardItem', $objDatabase );
	}

	public static function fetchCompanyUserDashboardItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_dashboard_items', $objDatabase );
	}

	public static function fetchCompanyUserDashboardItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDashboardItem( sprintf( 'SELECT * FROM company_user_dashboard_items WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDashboardItemsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserDashboardItems( sprintf( 'SELECT * FROM company_user_dashboard_items WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDashboardItemsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDashboardItems( sprintf( 'SELECT * FROM company_user_dashboard_items WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserDashboardItemsByDashboardItemIdByCid( $intDashboardItemId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserDashboardItems( sprintf( 'SELECT * FROM company_user_dashboard_items WHERE dashboard_item_id = %d AND cid = %d', ( int ) $intDashboardItemId, ( int ) $intCid ), $objDatabase );
	}

}
?>