<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCharge extends CEosSingularBase {

	use TEosDetails;

    const TABLE_NAME = 'public.scheduled_charges';

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intLeaseId;
    protected $m_intLeaseIntervalId;
    protected $m_intCustomerId;
    protected $m_intArCodeTypeId;
    protected $m_intArCodeId;
    protected $m_intRateId;
    protected $m_intRateLogId;
    protected $m_intArTriggerId;
    protected $m_intArCascadeId;
    protected $m_intArCascadeReferenceId;
    protected $m_intArOriginId;
    protected $m_intArOriginReferenceId;
    protected $m_intArOriginObjectId;
    protected $m_intArFormulaId;
    protected $m_intArFormulaReferenceId;
    protected $m_intQuoteId;
    protected $m_intLeaseTermId;
    protected $m_intLeaseStartWindowId;
    protected $m_intScheduledChargeTypeId;
    protected $m_intScheduledChargeLogTypeId;
    protected $m_intLeaseAssociationId;
    protected $m_intParentScheduledChargeId;
    protected $m_intInstallmentPlanId;
    protected $m_intInstallmentId;
    protected $m_strRemotePrimaryKey;
    protected $m_intLeaseTermMonths;
    protected $m_fltChargeAmount;
    protected $m_fltMonthToMonthMultiplier;
    protected $m_fltChargeMultiplierAmount;
    protected $m_fltChargePercent;
    protected $m_fltNormalizedAmount;
    protected $m_fltNormalizedPercent;
    protected $m_fltMonthToMonthAmount;
    protected $m_fltMonthToMonthFee;
    protected $m_strChargeStartDate;
    protected $m_strChargeEndDate;
    protected $m_strPostedThroughDate;
    protected $m_strLastPostedOn;
    protected $m_strMemo;
    protected $m_strInternalMemo;
    protected $m_intRateIntervalStart;
    protected $m_intRateIntervalOccurances;
    protected $m_intRateIntervalOffset;
    protected $m_boolStartsWithLease;
    protected $m_boolEndsWithMoveOut;
    protected $m_boolIsUnselectedQuote;
    protected $m_boolIsCachedToLease;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;
    protected $m_strDetails;
    protected $m_jsonDetails;
    protected $m_fltTotalValue;
    protected $m_intScheduledChargeGroupId;

    public function __construct() {
        parent::__construct();

        $this->m_intScheduledChargeTypeId = '1';
        $this->m_intScheduledChargeLogTypeId = '1';
        $this->m_intLeaseTermMonths = '0';
        $this->m_fltMonthToMonthMultiplier = '1';
        $this->m_fltChargeMultiplierAmount = '0';
        $this->m_fltChargePercent = '0';
        $this->m_fltNormalizedAmount = '0';
        $this->m_fltNormalizedPercent = '0';
        $this->m_intRateIntervalStart = '1';
        $this->m_intRateIntervalOccurances = '1';
        $this->m_intRateIntervalOffset = '0';
        $this->m_boolStartsWithLease = true;
        $this->m_boolEndsWithMoveOut = true;
        $this->m_boolIsUnselectedQuote = false;
        $this->m_boolIsCachedToLease = true;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
        if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
        if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
        if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
        if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
        if( isset( $arrValues['rate_id'] ) && $boolDirectSet ) $this->set( 'm_intRateId', trim( $arrValues['rate_id'] ) ); elseif( isset( $arrValues['rate_id'] ) ) $this->setRateId( $arrValues['rate_id'] );
        if( isset( $arrValues['rate_log_id'] ) && $boolDirectSet ) $this->set( 'm_intRateLogId', trim( $arrValues['rate_log_id'] ) ); elseif( isset( $arrValues['rate_log_id'] ) ) $this->setRateLogId( $arrValues['rate_log_id'] );
        if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
        if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeId', trim( $arrValues['ar_cascade_id'] ) ); elseif( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
        if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeReferenceId', trim( $arrValues['ar_cascade_reference_id'] ) ); elseif( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
        if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
        if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
        if( isset( $arrValues['ar_origin_object_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginObjectId', trim( $arrValues['ar_origin_object_id'] ) ); elseif( isset( $arrValues['ar_origin_object_id'] ) ) $this->setArOriginObjectId( $arrValues['ar_origin_object_id'] );
        if( isset( $arrValues['ar_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intArFormulaId', trim( $arrValues['ar_formula_id'] ) ); elseif( isset( $arrValues['ar_formula_id'] ) ) $this->setArFormulaId( $arrValues['ar_formula_id'] );
        if( isset( $arrValues['ar_formula_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArFormulaReferenceId', trim( $arrValues['ar_formula_reference_id'] ) ); elseif( isset( $arrValues['ar_formula_reference_id'] ) ) $this->setArFormulaReferenceId( $arrValues['ar_formula_reference_id'] );
        if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
        if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
        if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
        if( isset( $arrValues['scheduled_charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeTypeId', trim( $arrValues['scheduled_charge_type_id'] ) ); elseif( isset( $arrValues['scheduled_charge_type_id'] ) ) $this->setScheduledChargeTypeId( $arrValues['scheduled_charge_type_id'] );
        if( isset( $arrValues['scheduled_charge_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeLogTypeId', trim( $arrValues['scheduled_charge_log_type_id'] ) ); elseif( isset( $arrValues['scheduled_charge_log_type_id'] ) ) $this->setScheduledChargeLogTypeId( $arrValues['scheduled_charge_log_type_id'] );
        if( isset( $arrValues['lease_association_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseAssociationId', trim( $arrValues['lease_association_id'] ) ); elseif( isset( $arrValues['lease_association_id'] ) ) $this->setLeaseAssociationId( $arrValues['lease_association_id'] );
        if( isset( $arrValues['parent_scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intParentScheduledChargeId', trim( $arrValues['parent_scheduled_charge_id'] ) ); elseif( isset( $arrValues['parent_scheduled_charge_id'] ) ) $this->setParentScheduledChargeId( $arrValues['parent_scheduled_charge_id'] );
        if( isset( $arrValues['installment_plan_id'] ) && $boolDirectSet ) $this->set( 'm_intInstallmentPlanId', trim( $arrValues['installment_plan_id'] ) ); elseif( isset( $arrValues['installment_plan_id'] ) ) $this->setInstallmentPlanId( $arrValues['installment_plan_id'] );
        if( isset( $arrValues['installment_id'] ) && $boolDirectSet ) $this->set( 'm_intInstallmentId', trim( $arrValues['installment_id'] ) ); elseif( isset( $arrValues['installment_id'] ) ) $this->setInstallmentId( $arrValues['installment_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
        if( isset( $arrValues['lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermMonths', trim( $arrValues['lease_term_months'] ) ); elseif( isset( $arrValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrValues['lease_term_months'] );
        if( isset( $arrValues['charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeAmount', trim( $arrValues['charge_amount'] ) ); elseif( isset( $arrValues['charge_amount'] ) ) $this->setChargeAmount( $arrValues['charge_amount'] );
        if( isset( $arrValues['month_to_month_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltMonthToMonthMultiplier', trim( $arrValues['month_to_month_multiplier'] ) ); elseif( isset( $arrValues['month_to_month_multiplier'] ) ) $this->setMonthToMonthMultiplier( $arrValues['month_to_month_multiplier'] );
        if( isset( $arrValues['charge_multiplier_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeMultiplierAmount', trim( $arrValues['charge_multiplier_amount'] ) ); elseif( isset( $arrValues['charge_multiplier_amount'] ) ) $this->setChargeMultiplierAmount( $arrValues['charge_multiplier_amount'] );
        if( isset( $arrValues['charge_percent'] ) && $boolDirectSet ) $this->set( 'm_fltChargePercent', trim( $arrValues['charge_percent'] ) ); elseif( isset( $arrValues['charge_percent'] ) ) $this->setChargePercent( $arrValues['charge_percent'] );
        if( isset( $arrValues['normalized_amount'] ) && $boolDirectSet ) $this->set( 'm_fltNormalizedAmount', trim( $arrValues['normalized_amount'] ) ); elseif( isset( $arrValues['normalized_amount'] ) ) $this->setNormalizedAmount( $arrValues['normalized_amount'] );
        if( isset( $arrValues['normalized_percent'] ) && $boolDirectSet ) $this->set( 'm_fltNormalizedPercent', trim( $arrValues['normalized_percent'] ) ); elseif( isset( $arrValues['normalized_percent'] ) ) $this->setNormalizedPercent( $arrValues['normalized_percent'] );
        if( isset( $arrValues['month_to_month_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthToMonthAmount', trim( $arrValues['month_to_month_amount'] ) ); elseif( isset( $arrValues['month_to_month_amount'] ) ) $this->setMonthToMonthAmount( $arrValues['month_to_month_amount'] );
        if( isset( $arrValues['month_to_month_fee'] ) && $boolDirectSet ) $this->set( 'm_fltMonthToMonthFee', trim( $arrValues['month_to_month_fee'] ) ); elseif( isset( $arrValues['month_to_month_fee'] ) ) $this->setMonthToMonthFee( $arrValues['month_to_month_fee'] );
        if( isset( $arrValues['charge_start_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeStartDate', trim( $arrValues['charge_start_date'] ) ); elseif( isset( $arrValues['charge_start_date'] ) ) $this->setChargeStartDate( $arrValues['charge_start_date'] );
        if( isset( $arrValues['charge_end_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeEndDate', trim( $arrValues['charge_end_date'] ) ); elseif( isset( $arrValues['charge_end_date'] ) ) $this->setChargeEndDate( $arrValues['charge_end_date'] );
        if( isset( $arrValues['posted_through_date'] ) && $boolDirectSet ) $this->set( 'm_strPostedThroughDate', trim( $arrValues['posted_through_date'] ) ); elseif( isset( $arrValues['posted_through_date'] ) ) $this->setPostedThroughDate( $arrValues['posted_through_date'] );
        if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
        if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( $arrValues['memo'] ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( $arrValues['memo'] );
        if( isset( $arrValues['internal_memo'] ) && $boolDirectSet ) $this->set( 'm_strInternalMemo', trim( $arrValues['internal_memo'] ) ); elseif( isset( $arrValues['internal_memo'] ) ) $this->setInternalMemo( $arrValues['internal_memo'] );
        if( isset( $arrValues['rate_interval_start'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalStart', trim( $arrValues['rate_interval_start'] ) ); elseif( isset( $arrValues['rate_interval_start'] ) ) $this->setRateIntervalStart( $arrValues['rate_interval_start'] );
        if( isset( $arrValues['rate_interval_occurances'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalOccurances', trim( $arrValues['rate_interval_occurances'] ) ); elseif( isset( $arrValues['rate_interval_occurances'] ) ) $this->setRateIntervalOccurances( $arrValues['rate_interval_occurances'] );
        if( isset( $arrValues['rate_interval_offset'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalOffset', trim( $arrValues['rate_interval_offset'] ) ); elseif( isset( $arrValues['rate_interval_offset'] ) ) $this->setRateIntervalOffset( $arrValues['rate_interval_offset'] );
        if( isset( $arrValues['starts_with_lease'] ) && $boolDirectSet ) $this->set( 'm_boolStartsWithLease', trim( stripcslashes( $arrValues['starts_with_lease'] ) ) ); elseif( isset( $arrValues['starts_with_lease'] ) ) $this->setStartsWithLease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['starts_with_lease'] ) : $arrValues['starts_with_lease'] );
        if( isset( $arrValues['ends_with_move_out'] ) && $boolDirectSet ) $this->set( 'm_boolEndsWithMoveOut', trim( stripcslashes( $arrValues['ends_with_move_out'] ) ) ); elseif( isset( $arrValues['ends_with_move_out'] ) ) $this->setEndsWithMoveOut( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ends_with_move_out'] ) : $arrValues['ends_with_move_out'] );
        if( isset( $arrValues['is_unselected_quote'] ) && $boolDirectSet ) $this->set( 'm_boolIsUnselectedQuote', trim( stripcslashes( $arrValues['is_unselected_quote'] ) ) ); elseif( isset( $arrValues['is_unselected_quote'] ) ) $this->setIsUnselectedQuote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_unselected_quote'] ) : $arrValues['is_unselected_quote'] );
        if( isset( $arrValues['is_cached_to_lease'] ) && $boolDirectSet ) $this->set( 'm_boolIsCachedToLease', trim( stripcslashes( $arrValues['is_cached_to_lease'] ) ) ); elseif( isset( $arrValues['is_cached_to_lease'] ) ) $this->setIsCachedToLease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cached_to_lease'] ) : $arrValues['is_cached_to_lease'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
        if( isset( $arrValues['total_value'] ) && $boolDirectSet ) $this->set( 'm_fltTotalValue', trim( $arrValues['total_value'] ) ); elseif( isset( $arrValues['total_value'] ) ) $this->setTotalValue( $arrValues['total_value'] );
        if( isset( $arrValues['scheduled_charge_group_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeGroupId', trim( $arrValues['scheduled_charge_group_id'] ) ); elseif( isset( $arrValues['scheduled_charge_group_id'] ) ) $this->setScheduledChargeGroupId( $arrValues['scheduled_charge_group_id'] );
        $this->m_boolInitialized = true;
    }

    public function setId( $intId ) {
        $this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
    }

    public function setLeaseId( $intLeaseId ) {
        $this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
    }

    public function getLeaseId() {
        return $this->m_intLeaseId;
    }

    public function sqlLeaseId() {
        return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
    }

    public function setLeaseIntervalId( $intLeaseIntervalId ) {
        $this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
    }

    public function getLeaseIntervalId() {
        return $this->m_intLeaseIntervalId;
    }

    public function sqlLeaseIntervalId() {
        return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
    }

    public function setCustomerId( $intCustomerId ) {
        $this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
    }

    public function getCustomerId() {
        return $this->m_intCustomerId;
    }

    public function sqlCustomerId() {
        return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
    }

    public function setArCodeTypeId( $intArCodeTypeId ) {
        $this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
    }

    public function getArCodeTypeId() {
        return $this->m_intArCodeTypeId;
    }

    public function sqlArCodeTypeId() {
        return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
    }

    public function setArCodeId( $intArCodeId ) {
        $this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
    }

    public function getArCodeId() {
        return $this->m_intArCodeId;
    }

    public function sqlArCodeId() {
        return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
    }

    public function setRateId( $intRateId ) {
        $this->set( 'm_intRateId', CStrings::strToIntDef( $intRateId, NULL, false ) );
    }

    public function getRateId() {
        return $this->m_intRateId;
    }

    public function sqlRateId() {
        return ( true == isset( $this->m_intRateId ) ) ? ( string ) $this->m_intRateId : 'NULL';
    }

    public function setRateLogId( $intRateLogId ) {
        $this->set( 'm_intRateLogId', CStrings::strToIntDef( $intRateLogId, NULL, false ) );
    }

    public function getRateLogId() {
        return $this->m_intRateLogId;
    }

    public function sqlRateLogId() {
        return ( true == isset( $this->m_intRateLogId ) ) ? ( string ) $this->m_intRateLogId : 'NULL';
    }

    public function setArTriggerId( $intArTriggerId ) {
        $this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
    }

    public function getArTriggerId() {
        return $this->m_intArTriggerId;
    }

    public function sqlArTriggerId() {
        return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
    }

    public function setArCascadeId( $intArCascadeId ) {
        $this->set( 'm_intArCascadeId', CStrings::strToIntDef( $intArCascadeId, NULL, false ) );
    }

    public function getArCascadeId() {
        return $this->m_intArCascadeId;
    }

    public function sqlArCascadeId() {
        return ( true == isset( $this->m_intArCascadeId ) ) ? ( string ) $this->m_intArCascadeId : 'NULL';
    }

    public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
        $this->set( 'm_intArCascadeReferenceId', CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false ) );
    }

    public function getArCascadeReferenceId() {
        return $this->m_intArCascadeReferenceId;
    }

    public function sqlArCascadeReferenceId() {
        return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? ( string ) $this->m_intArCascadeReferenceId : 'NULL';
    }

    public function setArOriginId( $intArOriginId ) {
        $this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
    }

    public function getArOriginId() {
        return $this->m_intArOriginId;
    }

    public function sqlArOriginId() {
        return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
    }

    public function setArOriginReferenceId( $intArOriginReferenceId ) {
        $this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
    }

    public function getArOriginReferenceId() {
        return $this->m_intArOriginReferenceId;
    }

    public function sqlArOriginReferenceId() {
        return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
    }

    public function setArOriginObjectId( $intArOriginObjectId ) {
        $this->set( 'm_intArOriginObjectId', CStrings::strToIntDef( $intArOriginObjectId, NULL, false ) );
    }

    public function getArOriginObjectId() {
        return $this->m_intArOriginObjectId;
    }

    public function sqlArOriginObjectId() {
        return ( true == isset( $this->m_intArOriginObjectId ) ) ? ( string ) $this->m_intArOriginObjectId : 'NULL';
    }

    public function setArFormulaId( $intArFormulaId ) {
        $this->set( 'm_intArFormulaId', CStrings::strToIntDef( $intArFormulaId, NULL, false ) );
    }

    public function getArFormulaId() {
        return $this->m_intArFormulaId;
    }

    public function sqlArFormulaId() {
        return ( true == isset( $this->m_intArFormulaId ) ) ? ( string ) $this->m_intArFormulaId : 'NULL';
    }

    public function setArFormulaReferenceId( $intArFormulaReferenceId ) {
        $this->set( 'm_intArFormulaReferenceId', CStrings::strToIntDef( $intArFormulaReferenceId, NULL, false ) );
    }

    public function getArFormulaReferenceId() {
        return $this->m_intArFormulaReferenceId;
    }

    public function sqlArFormulaReferenceId() {
        return ( true == isset( $this->m_intArFormulaReferenceId ) ) ? ( string ) $this->m_intArFormulaReferenceId : 'NULL';
    }

    public function setQuoteId( $intQuoteId ) {
        $this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
    }

    public function getQuoteId() {
        return $this->m_intQuoteId;
    }

    public function sqlQuoteId() {
        return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
    }

    public function setLeaseTermId( $intLeaseTermId ) {
        $this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
    }

    public function getLeaseTermId() {
        return $this->m_intLeaseTermId;
    }

    public function sqlLeaseTermId() {
        return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
    }

    public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
        $this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
    }

    public function getLeaseStartWindowId() {
        return $this->m_intLeaseStartWindowId;
    }

    public function sqlLeaseStartWindowId() {
        return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
    }

    public function setScheduledChargeTypeId( $intScheduledChargeTypeId ) {
        $this->set( 'm_intScheduledChargeTypeId', CStrings::strToIntDef( $intScheduledChargeTypeId, NULL, false ) );
    }

    public function getScheduledChargeTypeId() {
        return $this->m_intScheduledChargeTypeId;
    }

    public function sqlScheduledChargeTypeId() {
        return ( true == isset( $this->m_intScheduledChargeTypeId ) ) ? ( string ) $this->m_intScheduledChargeTypeId : '1';
    }

    public function setScheduledChargeLogTypeId( $intScheduledChargeLogTypeId ) {
        $this->set( 'm_intScheduledChargeLogTypeId', CStrings::strToIntDef( $intScheduledChargeLogTypeId, NULL, false ) );
    }

    public function getScheduledChargeLogTypeId() {
        return $this->m_intScheduledChargeLogTypeId;
    }

    public function sqlScheduledChargeLogTypeId() {
        return ( true == isset( $this->m_intScheduledChargeLogTypeId ) ) ? ( string ) $this->m_intScheduledChargeLogTypeId : '1';
    }

    public function setLeaseAssociationId( $intLeaseAssociationId ) {
        $this->set( 'm_intLeaseAssociationId', CStrings::strToIntDef( $intLeaseAssociationId, NULL, false ) );
    }

    public function getLeaseAssociationId() {
        return $this->m_intLeaseAssociationId;
    }

    public function sqlLeaseAssociationId() {
        return ( true == isset( $this->m_intLeaseAssociationId ) ) ? ( string ) $this->m_intLeaseAssociationId : 'NULL';
    }

    public function setParentScheduledChargeId( $intParentScheduledChargeId ) {
        $this->set( 'm_intParentScheduledChargeId', CStrings::strToIntDef( $intParentScheduledChargeId, NULL, false ) );
    }

    public function getParentScheduledChargeId() {
        return $this->m_intParentScheduledChargeId;
    }

    public function sqlParentScheduledChargeId() {
        return ( true == isset( $this->m_intParentScheduledChargeId ) ) ? ( string ) $this->m_intParentScheduledChargeId : 'NULL';
    }

    public function setInstallmentPlanId( $intInstallmentPlanId ) {
        $this->set( 'm_intInstallmentPlanId', CStrings::strToIntDef( $intInstallmentPlanId, NULL, false ) );
    }

    public function getInstallmentPlanId() {
        return $this->m_intInstallmentPlanId;
    }

    public function sqlInstallmentPlanId() {
        return ( true == isset( $this->m_intInstallmentPlanId ) ) ? ( string ) $this->m_intInstallmentPlanId : 'NULL';
    }

    public function setInstallmentId( $intInstallmentId ) {
        $this->set( 'm_intInstallmentId', CStrings::strToIntDef( $intInstallmentId, NULL, false ) );
    }

    public function getInstallmentId() {
        return $this->m_intInstallmentId;
    }

    public function sqlInstallmentId() {
        return ( true == isset( $this->m_intInstallmentId ) ) ? ( string ) $this->m_intInstallmentId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 128, NULL, true ) );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
    }

    public function setLeaseTermMonths( $intLeaseTermMonths ) {
        $this->set( 'm_intLeaseTermMonths', CStrings::strToIntDef( $intLeaseTermMonths, NULL, false ) );
    }

    public function getLeaseTermMonths() {
        return $this->m_intLeaseTermMonths;
    }

    public function sqlLeaseTermMonths() {
        return ( true == isset( $this->m_intLeaseTermMonths ) ) ? ( string ) $this->m_intLeaseTermMonths : '0';
    }

    public function setChargeAmount( $fltChargeAmount ) {
        $this->set( 'm_fltChargeAmount', CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 ) );
    }

    public function getChargeAmount() {
        return $this->m_fltChargeAmount;
    }

    public function sqlChargeAmount() {
        return ( true == isset( $this->m_fltChargeAmount ) ) ? ( string ) $this->m_fltChargeAmount : 'NULL';
    }

    public function setMonthToMonthMultiplier( $fltMonthToMonthMultiplier ) {
        $this->set( 'm_fltMonthToMonthMultiplier', CStrings::strToFloatDef( $fltMonthToMonthMultiplier, NULL, false, 4 ) );
    }

    public function getMonthToMonthMultiplier() {
        return $this->m_fltMonthToMonthMultiplier;
    }

    public function sqlMonthToMonthMultiplier() {
        return ( true == isset( $this->m_fltMonthToMonthMultiplier ) ) ? ( string ) $this->m_fltMonthToMonthMultiplier : '1';
    }

    public function setChargeMultiplierAmount( $fltChargeMultiplierAmount ) {
        $this->set( 'm_fltChargeMultiplierAmount', CStrings::strToFloatDef( $fltChargeMultiplierAmount, NULL, false, 2 ) );
    }

    public function getChargeMultiplierAmount() {
        return $this->m_fltChargeMultiplierAmount;
    }

    public function sqlChargeMultiplierAmount() {
        return ( true == isset( $this->m_fltChargeMultiplierAmount ) ) ? ( string ) $this->m_fltChargeMultiplierAmount : '0';
    }

    public function setChargePercent( $fltChargePercent ) {
        $this->set( 'm_fltChargePercent', CStrings::strToFloatDef( $fltChargePercent, NULL, false, 6 ) );
    }

    public function getChargePercent() {
        return $this->m_fltChargePercent;
    }

    public function sqlChargePercent() {
        return ( true == isset( $this->m_fltChargePercent ) ) ? ( string ) $this->m_fltChargePercent : '0';
    }

    public function setNormalizedAmount( $fltNormalizedAmount ) {
        $this->set( 'm_fltNormalizedAmount', CStrings::strToFloatDef( $fltNormalizedAmount, NULL, false, 2 ) );
    }

    public function getNormalizedAmount() {
        return $this->m_fltNormalizedAmount;
    }

    public function sqlNormalizedAmount() {
        return ( true == isset( $this->m_fltNormalizedAmount ) ) ? ( string ) $this->m_fltNormalizedAmount : '0';
    }

    public function setNormalizedPercent( $fltNormalizedPercent ) {
        $this->set( 'm_fltNormalizedPercent', CStrings::strToFloatDef( $fltNormalizedPercent, NULL, false, 2 ) );
    }

    public function getNormalizedPercent() {
        return $this->m_fltNormalizedPercent;
    }

    public function sqlNormalizedPercent() {
        return ( true == isset( $this->m_fltNormalizedPercent ) ) ? ( string ) $this->m_fltNormalizedPercent : '0';
    }

    public function setMonthToMonthAmount( $fltMonthToMonthAmount ) {
        $this->set( 'm_fltMonthToMonthAmount', CStrings::strToFloatDef( $fltMonthToMonthAmount, NULL, false, 2 ) );
    }

    public function getMonthToMonthAmount() {
        return $this->m_fltMonthToMonthAmount;
    }

    public function sqlMonthToMonthAmount() {
        return ( true == isset( $this->m_fltMonthToMonthAmount ) ) ? ( string ) $this->m_fltMonthToMonthAmount : 'NULL';
    }

    public function setMonthToMonthFee( $fltMonthToMonthFee ) {
        $this->set( 'm_fltMonthToMonthFee', CStrings::strToFloatDef( $fltMonthToMonthFee, NULL, false, 2 ) );
    }

    public function getMonthToMonthFee() {
        return $this->m_fltMonthToMonthFee;
    }

    public function sqlMonthToMonthFee() {
        return ( true == isset( $this->m_fltMonthToMonthFee ) ) ? ( string ) $this->m_fltMonthToMonthFee : 'NULL';
    }

    public function setChargeStartDate( $strChargeStartDate ) {
        $this->set( 'm_strChargeStartDate', CStrings::strTrimDef( $strChargeStartDate, -1, NULL, true ) );
    }

    public function getChargeStartDate() {
        return $this->m_strChargeStartDate;
    }

    public function sqlChargeStartDate() {
        return ( true == isset( $this->m_strChargeStartDate ) ) ? '\'' . $this->m_strChargeStartDate . '\'' : 'NOW()';
    }

    public function setChargeEndDate( $strChargeEndDate ) {
        $this->set( 'm_strChargeEndDate', CStrings::strTrimDef( $strChargeEndDate, -1, NULL, true ) );
    }

    public function getChargeEndDate() {
        return $this->m_strChargeEndDate;
    }

    public function sqlChargeEndDate() {
        return ( true == isset( $this->m_strChargeEndDate ) ) ? '\'' . $this->m_strChargeEndDate . '\'' : 'NULL';
    }

    public function setPostedThroughDate( $strPostedThroughDate ) {
        $this->set( 'm_strPostedThroughDate', CStrings::strTrimDef( $strPostedThroughDate, -1, NULL, true ) );
    }

    public function getPostedThroughDate() {
        return $this->m_strPostedThroughDate;
    }

    public function sqlPostedThroughDate() {
        return ( true == isset( $this->m_strPostedThroughDate ) ) ? '\'' . $this->m_strPostedThroughDate . '\'' : 'NULL';
    }

    public function setLastPostedOn( $strLastPostedOn ) {
        $this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
    }

    public function getLastPostedOn() {
        return $this->m_strLastPostedOn;
    }

    public function sqlLastPostedOn() {
        return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
    }

    public function setMemo( $strMemo ) {
        $this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 2000, NULL, true ) );
    }

    public function getMemo() {
        return $this->m_strMemo;
    }

    public function sqlMemo() {
        return ( true == isset( $this->m_strMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMemo ) : '\'' . addslashes( $this->m_strMemo ) . '\'' ) : 'NULL';
    }

    public function setInternalMemo( $strInternalMemo ) {
        $this->set( 'm_strInternalMemo', CStrings::strTrimDef( $strInternalMemo, 2000, NULL, true ) );
    }

    public function getInternalMemo() {
        return $this->m_strInternalMemo;
    }

    public function sqlInternalMemo() {
        return ( true == isset( $this->m_strInternalMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInternalMemo ) : '\'' . addslashes( $this->m_strInternalMemo ) . '\'' ) : 'NULL';
    }

    public function setRateIntervalStart( $intRateIntervalStart ) {
        $this->set( 'm_intRateIntervalStart', CStrings::strToIntDef( $intRateIntervalStart, NULL, false ) );
    }

    public function getRateIntervalStart() {
        return $this->m_intRateIntervalStart;
    }

    public function sqlRateIntervalStart() {
        return ( true == isset( $this->m_intRateIntervalStart ) ) ? ( string ) $this->m_intRateIntervalStart : '1';
    }

    public function setRateIntervalOccurances( $intRateIntervalOccurances ) {
        $this->set( 'm_intRateIntervalOccurances', CStrings::strToIntDef( $intRateIntervalOccurances, NULL, false ) );
    }

    public function getRateIntervalOccurances() {
        return $this->m_intRateIntervalOccurances;
    }

    public function sqlRateIntervalOccurances() {
        return ( true == isset( $this->m_intRateIntervalOccurances ) ) ? ( string ) $this->m_intRateIntervalOccurances : '1';
    }

    public function setRateIntervalOffset( $intRateIntervalOffset ) {
        $this->set( 'm_intRateIntervalOffset', CStrings::strToIntDef( $intRateIntervalOffset, NULL, false ) );
    }

    public function getRateIntervalOffset() {
        return $this->m_intRateIntervalOffset;
    }

    public function sqlRateIntervalOffset() {
        return ( true == isset( $this->m_intRateIntervalOffset ) ) ? ( string ) $this->m_intRateIntervalOffset : '0';
    }

    public function setStartsWithLease( $boolStartsWithLease ) {
        $this->set( 'm_boolStartsWithLease', CStrings::strToBool( $boolStartsWithLease ) );
    }

    public function getStartsWithLease() {
        return $this->m_boolStartsWithLease;
    }

    public function sqlStartsWithLease() {
        return ( true == isset( $this->m_boolStartsWithLease ) ) ? '\'' . ( true == ( bool ) $this->m_boolStartsWithLease ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setEndsWithMoveOut( $boolEndsWithMoveOut ) {
        $this->set( 'm_boolEndsWithMoveOut', CStrings::strToBool( $boolEndsWithMoveOut ) );
    }

    public function getEndsWithMoveOut() {
        return $this->m_boolEndsWithMoveOut;
    }

    public function sqlEndsWithMoveOut() {
        return ( true == isset( $this->m_boolEndsWithMoveOut ) ) ? '\'' . ( true == ( bool ) $this->m_boolEndsWithMoveOut ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsUnselectedQuote( $boolIsUnselectedQuote ) {
        $this->set( 'm_boolIsUnselectedQuote', CStrings::strToBool( $boolIsUnselectedQuote ) );
    }

    public function getIsUnselectedQuote() {
        return $this->m_boolIsUnselectedQuote;
    }

    public function sqlIsUnselectedQuote() {
        return ( true == isset( $this->m_boolIsUnselectedQuote ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUnselectedQuote ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsCachedToLease( $boolIsCachedToLease ) {
        $this->set( 'm_boolIsCachedToLease', CStrings::strToBool( $boolIsCachedToLease ) );
    }

    public function getIsCachedToLease() {
        return $this->m_boolIsCachedToLease;
    }

    public function sqlIsCachedToLease() {
        return ( true == isset( $this->m_boolIsCachedToLease ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCachedToLease ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function setTotalValue( $fltTotalValue ) {
        $this->set( 'm_fltTotalValue', CStrings::strToFloatDef( $fltTotalValue, NULL, false, 2 ) );
    }

    public function getTotalValue() {
        return $this->m_fltTotalValue;
    }

    public function sqlTotalValue() {
        return ( true == isset( $this->m_fltTotalValue ) ) ? ( string ) $this->m_fltTotalValue : 'NULL';
    }

    public function setScheduledChargeGroupId( $intScheduledChargeGroupId ) {
        $this->set( 'm_intScheduledChargeGroupId', CStrings::strToIntDef( $intScheduledChargeGroupId, NULL, false ) );
    }

    public function getScheduledChargeGroupId() {
        return $this->m_intScheduledChargeGroupId;
    }

    public function sqlScheduledChargeGroupId() {
        return ( true == isset( $this->m_intScheduledChargeGroupId ) ) ? ( string ) $this->m_intScheduledChargeGroupId : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

        $strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, lease_interval_id, customer_id, ar_code_type_id, ar_code_id, rate_id, rate_log_id, ar_trigger_id, ar_cascade_id, ar_cascade_reference_id, ar_origin_id, ar_origin_reference_id, ar_origin_object_id, ar_formula_id, ar_formula_reference_id, quote_id, lease_term_id, lease_start_window_id, scheduled_charge_type_id, scheduled_charge_log_type_id, lease_association_id, parent_scheduled_charge_id, installment_plan_id, installment_id, remote_primary_key, lease_term_months, charge_amount, month_to_month_multiplier, charge_multiplier_amount, charge_percent, normalized_amount, normalized_percent, month_to_month_amount, month_to_month_fee, charge_start_date, charge_end_date, posted_through_date, last_posted_on, memo, internal_memo, rate_interval_start, rate_interval_occurances, rate_interval_offset, starts_with_lease, ends_with_move_out, is_unselected_quote, is_cached_to_lease, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, total_value, scheduled_charge_group_id )
					VALUES ( ' .
            $strId . ', ' .
            $this->sqlCid() . ', ' .
            $this->sqlPropertyId() . ', ' .
            $this->sqlLeaseId() . ', ' .
            $this->sqlLeaseIntervalId() . ', ' .
            $this->sqlCustomerId() . ', ' .
            $this->sqlArCodeTypeId() . ', ' .
            $this->sqlArCodeId() . ', ' .
            $this->sqlRateId() . ', ' .
            $this->sqlRateLogId() . ', ' .
            $this->sqlArTriggerId() . ', ' .
            $this->sqlArCascadeId() . ', ' .
            $this->sqlArCascadeReferenceId() . ', ' .
            $this->sqlArOriginId() . ', ' .
            $this->sqlArOriginReferenceId() . ', ' .
            $this->sqlArOriginObjectId() . ', ' .
            $this->sqlArFormulaId() . ', ' .
            $this->sqlArFormulaReferenceId() . ', ' .
            $this->sqlQuoteId() . ', ' .
            $this->sqlLeaseTermId() . ', ' .
            $this->sqlLeaseStartWindowId() . ', ' .
            $this->sqlScheduledChargeTypeId() . ', ' .
            $this->sqlScheduledChargeLogTypeId() . ', ' .
            $this->sqlLeaseAssociationId() . ', ' .
            $this->sqlParentScheduledChargeId() . ', ' .
            $this->sqlInstallmentPlanId() . ', ' .
            $this->sqlInstallmentId() . ', ' .
            $this->sqlRemotePrimaryKey() . ', ' .
            $this->sqlLeaseTermMonths() . ', ' .
            $this->sqlChargeAmount() . ', ' .
            $this->sqlMonthToMonthMultiplier() . ', ' .
            $this->sqlChargeMultiplierAmount() . ', ' .
            $this->sqlChargePercent() . ', ' .
            $this->sqlNormalizedAmount() . ', ' .
            $this->sqlNormalizedPercent() . ', ' .
            $this->sqlMonthToMonthAmount() . ', ' .
            $this->sqlMonthToMonthFee() . ', ' .
            $this->sqlChargeStartDate() . ', ' .
            $this->sqlChargeEndDate() . ', ' .
            $this->sqlPostedThroughDate() . ', ' .
            $this->sqlLastPostedOn() . ', ' .
            $this->sqlMemo() . ', ' .
            $this->sqlInternalMemo() . ', ' .
            $this->sqlRateIntervalStart() . ', ' .
            $this->sqlRateIntervalOccurances() . ', ' .
            $this->sqlRateIntervalOffset() . ', ' .
            $this->sqlStartsWithLease() . ', ' .
            $this->sqlEndsWithMoveOut() . ', ' .
            $this->sqlIsUnselectedQuote() . ', ' .
            $this->sqlIsCachedToLease() . ', ' .
            $this->sqlDeletedBy() . ', ' .
            $this->sqlDeletedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlUpdatedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlCreatedOn() . ', ' .
            $this->sqlDetails() . ', ' .
            $this->sqlTotalValue() . ', ' .
            $this->sqlScheduledChargeGroupId() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        if( false == $this->getAllowDifferentialUpdate() ) {
            $boolUpdate = true;
        } else {
            $boolUpdate = false;
        }

        $strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_type_id = ' . $this->sqlArCodeTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_id = ' . $this->sqlRateId(). ',' ; } elseif( true == array_key_exists( 'RateId', $this->getChangedColumns() ) ) { $strSql .= ' rate_id = ' . $this->sqlRateId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_log_id = ' . $this->sqlRateLogId(). ',' ; } elseif( true == array_key_exists( 'RateLogId', $this->getChangedColumns() ) ) { $strSql .= ' rate_log_id = ' . $this->sqlRateLogId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId(). ',' ; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId(). ',' ; } elseif( true == array_key_exists( 'ArOriginObjectId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_formula_id = ' . $this->sqlArFormulaId(). ',' ; } elseif( true == array_key_exists( 'ArFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' ar_formula_id = ' . $this->sqlArFormulaId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_formula_reference_id = ' . $this->sqlArFormulaReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArFormulaReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_formula_reference_id = ' . $this->sqlArFormulaReferenceId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId(). ',' ; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_type_id = ' . $this->sqlScheduledChargeTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_type_id = ' . $this->sqlScheduledChargeTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_log_type_id = ' . $this->sqlScheduledChargeLogTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_log_type_id = ' . $this->sqlScheduledChargeLogTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_association_id = ' . $this->sqlLeaseAssociationId(). ',' ; } elseif( true == array_key_exists( 'LeaseAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' lease_association_id = ' . $this->sqlLeaseAssociationId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_scheduled_charge_id = ' . $this->sqlParentScheduledChargeId(). ',' ; } elseif( true == array_key_exists( 'ParentScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' parent_scheduled_charge_id = ' . $this->sqlParentScheduledChargeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installment_plan_id = ' . $this->sqlInstallmentPlanId(). ',' ; } elseif( true == array_key_exists( 'InstallmentPlanId', $this->getChangedColumns() ) ) { $strSql .= ' installment_plan_id = ' . $this->sqlInstallmentPlanId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installment_id = ' . $this->sqlInstallmentId(). ',' ; } elseif( true == array_key_exists( 'InstallmentId', $this->getChangedColumns() ) ) { $strSql .= ' installment_id = ' . $this->sqlInstallmentId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths(). ',' ; } elseif( true == array_key_exists( 'LeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount(). ',' ; } elseif( true == array_key_exists( 'ChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_multiplier = ' . $this->sqlMonthToMonthMultiplier(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_multiplier = ' . $this->sqlMonthToMonthMultiplier() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_multiplier_amount = ' . $this->sqlChargeMultiplierAmount(). ',' ; } elseif( true == array_key_exists( 'ChargeMultiplierAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_multiplier_amount = ' . $this->sqlChargeMultiplierAmount() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_percent = ' . $this->sqlChargePercent(). ',' ; } elseif( true == array_key_exists( 'ChargePercent', $this->getChangedColumns() ) ) { $strSql .= ' charge_percent = ' . $this->sqlChargePercent() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' normalized_amount = ' . $this->sqlNormalizedAmount(). ',' ; } elseif( true == array_key_exists( 'NormalizedAmount', $this->getChangedColumns() ) ) { $strSql .= ' normalized_amount = ' . $this->sqlNormalizedAmount() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' normalized_percent = ' . $this->sqlNormalizedPercent(). ',' ; } elseif( true == array_key_exists( 'NormalizedPercent', $this->getChangedColumns() ) ) { $strSql .= ' normalized_percent = ' . $this->sqlNormalizedPercent() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_amount = ' . $this->sqlMonthToMonthAmount(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthAmount', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_amount = ' . $this->sqlMonthToMonthAmount() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_to_month_fee = ' . $this->sqlMonthToMonthFee(). ',' ; } elseif( true == array_key_exists( 'MonthToMonthFee', $this->getChangedColumns() ) ) { $strSql .= ' month_to_month_fee = ' . $this->sqlMonthToMonthFee() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate(). ',' ; } elseif( true == array_key_exists( 'ChargeStartDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate(). ',' ; } elseif( true == array_key_exists( 'ChargeEndDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_through_date = ' . $this->sqlPostedThroughDate(). ',' ; } elseif( true == array_key_exists( 'PostedThroughDate', $this->getChangedColumns() ) ) { $strSql .= ' posted_through_date = ' . $this->sqlPostedThroughDate() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_memo = ' . $this->sqlInternalMemo(). ',' ; } elseif( true == array_key_exists( 'InternalMemo', $this->getChangedColumns() ) ) { $strSql .= ' internal_memo = ' . $this->sqlInternalMemo() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_start = ' . $this->sqlRateIntervalStart(). ',' ; } elseif( true == array_key_exists( 'RateIntervalStart', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_start = ' . $this->sqlRateIntervalStart() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_occurances = ' . $this->sqlRateIntervalOccurances(). ',' ; } elseif( true == array_key_exists( 'RateIntervalOccurances', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_occurances = ' . $this->sqlRateIntervalOccurances() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset(). ',' ; } elseif( true == array_key_exists( 'RateIntervalOffset', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' starts_with_lease = ' . $this->sqlStartsWithLease(). ',' ; } elseif( true == array_key_exists( 'StartsWithLease', $this->getChangedColumns() ) ) { $strSql .= ' starts_with_lease = ' . $this->sqlStartsWithLease() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ends_with_move_out = ' . $this->sqlEndsWithMoveOut(). ',' ; } elseif( true == array_key_exists( 'EndsWithMoveOut', $this->getChangedColumns() ) ) { $strSql .= ' ends_with_move_out = ' . $this->sqlEndsWithMoveOut() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unselected_quote = ' . $this->sqlIsUnselectedQuote(). ',' ; } elseif( true == array_key_exists( 'IsUnselectedQuote', $this->getChangedColumns() ) ) { $strSql .= ' is_unselected_quote = ' . $this->sqlIsUnselectedQuote() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cached_to_lease = ' . $this->sqlIsCachedToLease(). ',' ; } elseif( true == array_key_exists( 'IsCachedToLease', $this->getChangedColumns() ) ) { $strSql .= ' is_cached_to_lease = ' . $this->sqlIsCachedToLease() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_value = ' . $this->sqlTotalValue(). ',' ; } elseif( true == array_key_exists( 'TotalValue', $this->getChangedColumns() ) ) { $strSql .= ' total_value = ' . $this->sqlTotalValue() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_group_id = ' . $this->sqlScheduledChargeGroupId(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeGroupId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_group_id = ' . $this->sqlScheduledChargeGroupId() . ','; $boolUpdate = true; }
        $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
        $strSql .= ' updated_on = \'NOW()\' ';

        $strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
            return ( true == $boolUpdate ) ? $strSql : false;
        } else {
            if( true == $boolUpdate ) {
                if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
                    if( true == $this->getAllowDifferentialUpdate() ) {
                        $this->resetChangedColumns();
                        return true;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'cid' => $this->getCid(),
            'property_id' => $this->getPropertyId(),
            'lease_id' => $this->getLeaseId(),
            'lease_interval_id' => $this->getLeaseIntervalId(),
            'customer_id' => $this->getCustomerId(),
            'ar_code_type_id' => $this->getArCodeTypeId(),
            'ar_code_id' => $this->getArCodeId(),
            'rate_id' => $this->getRateId(),
            'rate_log_id' => $this->getRateLogId(),
            'ar_trigger_id' => $this->getArTriggerId(),
            'ar_cascade_id' => $this->getArCascadeId(),
            'ar_cascade_reference_id' => $this->getArCascadeReferenceId(),
            'ar_origin_id' => $this->getArOriginId(),
            'ar_origin_reference_id' => $this->getArOriginReferenceId(),
            'ar_origin_object_id' => $this->getArOriginObjectId(),
            'ar_formula_id' => $this->getArFormulaId(),
            'ar_formula_reference_id' => $this->getArFormulaReferenceId(),
            'quote_id' => $this->getQuoteId(),
            'lease_term_id' => $this->getLeaseTermId(),
            'lease_start_window_id' => $this->getLeaseStartWindowId(),
            'scheduled_charge_type_id' => $this->getScheduledChargeTypeId(),
            'scheduled_charge_log_type_id' => $this->getScheduledChargeLogTypeId(),
            'lease_association_id' => $this->getLeaseAssociationId(),
            'parent_scheduled_charge_id' => $this->getParentScheduledChargeId(),
            'installment_plan_id' => $this->getInstallmentPlanId(),
            'installment_id' => $this->getInstallmentId(),
            'remote_primary_key' => $this->getRemotePrimaryKey(),
            'lease_term_months' => $this->getLeaseTermMonths(),
            'charge_amount' => $this->getChargeAmount(),
            'month_to_month_multiplier' => $this->getMonthToMonthMultiplier(),
            'charge_multiplier_amount' => $this->getChargeMultiplierAmount(),
            'charge_percent' => $this->getChargePercent(),
            'normalized_amount' => $this->getNormalizedAmount(),
            'normalized_percent' => $this->getNormalizedPercent(),
            'month_to_month_amount' => $this->getMonthToMonthAmount(),
            'month_to_month_fee' => $this->getMonthToMonthFee(),
            'charge_start_date' => $this->getChargeStartDate(),
            'charge_end_date' => $this->getChargeEndDate(),
            'posted_through_date' => $this->getPostedThroughDate(),
            'last_posted_on' => $this->getLastPostedOn(),
            'memo' => $this->getMemo(),
            'internal_memo' => $this->getInternalMemo(),
            'rate_interval_start' => $this->getRateIntervalStart(),
            'rate_interval_occurances' => $this->getRateIntervalOccurances(),
            'rate_interval_offset' => $this->getRateIntervalOffset(),
            'starts_with_lease' => $this->getStartsWithLease(),
            'ends_with_move_out' => $this->getEndsWithMoveOut(),
            'is_unselected_quote' => $this->getIsUnselectedQuote(),
            'is_cached_to_lease' => $this->getIsCachedToLease(),
            'deleted_by' => $this->getDeletedBy(),
            'deleted_on' => $this->getDeletedOn(),
            'updated_by' => $this->getUpdatedBy(),
            'updated_on' => $this->getUpdatedOn(),
            'created_by' => $this->getCreatedBy(),
            'created_on' => $this->getCreatedOn(),
            'details' => $this->getDetails(),
            'total_value' => $this->getTotalValue(),
            'scheduled_charge_group_id' => $this->getScheduledChargeGroupId()
        );
    }

}
?>