<?php

class CBaseImportTypeEntity extends CEosSingularBase {

	const TABLE_NAME = 'public.import_type_entities';

	protected $m_intId;
	protected $m_intImportTypeId;
	protected $m_intImportEntityId;
	protected $m_intIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['import_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTypeId', trim( $arrValues['import_type_id'] ) ); elseif( isset( $arrValues['import_type_id'] ) ) $this->setImportTypeId( $arrValues['import_type_id'] );
		if( isset( $arrValues['import_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intImportEntityId', trim( $arrValues['import_entity_id'] ) ); elseif( isset( $arrValues['import_entity_id'] ) ) $this->setImportEntityId( $arrValues['import_entity_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setImportTypeId( $intImportTypeId ) {
		$this->set( 'm_intImportTypeId', CStrings::strToIntDef( $intImportTypeId, NULL, false ) );
	}

	public function getImportTypeId() {
		return $this->m_intImportTypeId;
	}

	public function sqlImportTypeId() {
		return ( true == isset( $this->m_intImportTypeId ) ) ? ( string ) $this->m_intImportTypeId : 'NULL';
	}

	public function setImportEntityId( $intImportEntityId ) {
		$this->set( 'm_intImportEntityId', CStrings::strToIntDef( $intImportEntityId, NULL, false ) );
	}

	public function getImportEntityId() {
		return $this->m_intImportEntityId;
	}

	public function sqlImportEntityId() {
		return ( true == isset( $this->m_intImportEntityId ) ) ? ( string ) $this->m_intImportEntityId : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'import_type_id' => $this->getImportTypeId(),
			'import_entity_id' => $this->getImportEntityId(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>