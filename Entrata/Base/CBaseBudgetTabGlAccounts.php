<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccounts extends CEosPluralBase {

	/**
	 * @return CBudgetTabGlAccount[]
	 */
	public static function fetchBudgetTabGlAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetTabGlAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetTabGlAccount
	 */
	public static function fetchBudgetTabGlAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetTabGlAccount::class, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_tab_gl_accounts', $objDatabase );
	}

	public static function fetchBudgetTabGlAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccount( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByBudgetTabIdByCid( $intBudgetTabId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE budget_tab_id = %d AND cid = %d', $intBudgetTabId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountsByBudgetDataSourceSubTypeIdByCid( $intBudgetDataSourceSubTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccounts( sprintf( 'SELECT * FROM budget_tab_gl_accounts WHERE budget_data_source_sub_type_id = %d AND cid = %d', $intBudgetDataSourceSubTypeId, $intCid ), $objDatabase );
	}

}
?>