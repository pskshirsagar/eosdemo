<?php

class CBaseOccupancyType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.occupancy_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intPsProductId;
	protected $m_intAllowUnitAssociation;
	protected $m_intAllowLeaseAssociation;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intAllowUnitAssociation = '1';
		$this->m_intAllowLeaseAssociation = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['allow_unit_association'] ) && $boolDirectSet ) $this->set( 'm_intAllowUnitAssociation', trim( $arrValues['allow_unit_association'] ) ); elseif( isset( $arrValues['allow_unit_association'] ) ) $this->setAllowUnitAssociation( $arrValues['allow_unit_association'] );
		if( isset( $arrValues['allow_lease_association'] ) && $boolDirectSet ) $this->set( 'm_intAllowLeaseAssociation', trim( $arrValues['allow_lease_association'] ) ); elseif( isset( $arrValues['allow_lease_association'] ) ) $this->setAllowLeaseAssociation( $arrValues['allow_lease_association'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setAllowUnitAssociation( $intAllowUnitAssociation ) {
		$this->set( 'm_intAllowUnitAssociation', CStrings::strToIntDef( $intAllowUnitAssociation, NULL, false ) );
	}

	public function getAllowUnitAssociation() {
		return $this->m_intAllowUnitAssociation;
	}

	public function sqlAllowUnitAssociation() {
		return ( true == isset( $this->m_intAllowUnitAssociation ) ) ? ( string ) $this->m_intAllowUnitAssociation : '1';
	}

	public function setAllowLeaseAssociation( $intAllowLeaseAssociation ) {
		$this->set( 'm_intAllowLeaseAssociation', CStrings::strToIntDef( $intAllowLeaseAssociation, NULL, false ) );
	}

	public function getAllowLeaseAssociation() {
		return $this->m_intAllowLeaseAssociation;
	}

	public function sqlAllowLeaseAssociation() {
		return ( true == isset( $this->m_intAllowLeaseAssociation ) ) ? ( string ) $this->m_intAllowLeaseAssociation : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'ps_product_id' => $this->getPsProductId(),
			'allow_unit_association' => $this->getAllowUnitAssociation(),
			'allow_lease_association' => $this->getAllowLeaseAssociation(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>