<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUsers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUsers extends CEosPluralBase {

	/**
	 * @return CCompanyUser[]
	 */
	public static function fetchCompanyUsers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyUser::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUser
	 */
	public static function fetchCompanyUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyUser::class, $objDatabase );
	}

	public static function fetchCompanyUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_users', $objDatabase );
	}

	public static function fetchCompanyUserByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUser( sprintf( 'SELECT * FROM company_users WHERE id = %d AND cid = %d AND default_company_user_id IS NULL', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE cid = %d AND default_company_user_id IS NULL', $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserTypeIdByCid( $intCompanyUserTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE company_user_type_id = %d AND cid = %d AND default_company_user_id IS NULL', $intCompanyUserTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByDefaultCompanyUserIdByCid( $intDefaultCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE default_company_user_id = %d AND cid = %d', $intDefaultCompanyUserId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE company_employee_id = %d AND cid = %d AND default_company_user_id IS NULL', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByMainEulaIdByCid( $intMainEulaId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE main_eula_id = %d AND cid = %d AND default_company_user_id IS NULL', $intMainEulaId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByCheck21EulaIdByCid( $intCheck21EulaId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE check21_eula_id = %d AND cid = %d AND default_company_user_id IS NULL', $intCheck21EulaId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserScoreContactTypeIdByCid( $intCompanyUserScoreContactTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE company_user_score_contact_type_id = %d AND cid = %d AND default_company_user_id IS NULL', $intCompanyUserScoreContactTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyUsersByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchCompanyUsers( sprintf( 'SELECT * FROM company_users WHERE reference_id = %d AND cid = %d AND default_company_user_id IS NULL', $intReferenceId, $intCid ), $objDatabase );
	}

}
?>