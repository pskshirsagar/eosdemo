<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRuleCondition extends CEosSingularBase {

	const TABLE_NAME = 'public.rule_conditions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRouteId;
	protected $m_intRouteRuleId;
	protected $m_intRouteRuleTypeId;
	protected $m_fltMinimumAmount;
	protected $m_fltMaximumAmount;
	protected $m_strConditionDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['route_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteId', trim( $arrValues['route_id'] ) ); elseif( isset( $arrValues['route_id'] ) ) $this->setRouteId( $arrValues['route_id'] );
		if( isset( $arrValues['route_rule_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteRuleId', trim( $arrValues['route_rule_id'] ) ); elseif( isset( $arrValues['route_rule_id'] ) ) $this->setRouteRuleId( $arrValues['route_rule_id'] );
		if( isset( $arrValues['route_rule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteRuleTypeId', trim( $arrValues['route_rule_type_id'] ) ); elseif( isset( $arrValues['route_rule_type_id'] ) ) $this->setRouteRuleTypeId( $arrValues['route_rule_type_id'] );
		if( isset( $arrValues['minimum_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumAmount', trim( $arrValues['minimum_amount'] ) ); elseif( isset( $arrValues['minimum_amount'] ) ) $this->setMinimumAmount( $arrValues['minimum_amount'] );
		if( isset( $arrValues['maximum_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaximumAmount', trim( $arrValues['maximum_amount'] ) ); elseif( isset( $arrValues['maximum_amount'] ) ) $this->setMaximumAmount( $arrValues['maximum_amount'] );
		if( isset( $arrValues['condition_details'] ) && $boolDirectSet ) $this->set( 'm_strConditionDetails', trim( stripcslashes( $arrValues['condition_details'] ) ) ); elseif( isset( $arrValues['condition_details'] ) ) $this->setConditionDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_details'] ) : $arrValues['condition_details'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRouteId( $intRouteId ) {
		$this->set( 'm_intRouteId', CStrings::strToIntDef( $intRouteId, NULL, false ) );
	}

	public function getRouteId() {
		return $this->m_intRouteId;
	}

	public function sqlRouteId() {
		return ( true == isset( $this->m_intRouteId ) ) ? ( string ) $this->m_intRouteId : 'NULL';
	}

	public function setRouteRuleId( $intRouteRuleId ) {
		$this->set( 'm_intRouteRuleId', CStrings::strToIntDef( $intRouteRuleId, NULL, false ) );
	}

	public function getRouteRuleId() {
		return $this->m_intRouteRuleId;
	}

	public function sqlRouteRuleId() {
		return ( true == isset( $this->m_intRouteRuleId ) ) ? ( string ) $this->m_intRouteRuleId : 'NULL';
	}

	public function setRouteRuleTypeId( $intRouteRuleTypeId ) {
		$this->set( 'm_intRouteRuleTypeId', CStrings::strToIntDef( $intRouteRuleTypeId, NULL, false ) );
	}

	public function getRouteRuleTypeId() {
		return $this->m_intRouteRuleTypeId;
	}

	public function sqlRouteRuleTypeId() {
		return ( true == isset( $this->m_intRouteRuleTypeId ) ) ? ( string ) $this->m_intRouteRuleTypeId : 'NULL';
	}

	public function setMinimumAmount( $fltMinimumAmount ) {
		$this->set( 'm_fltMinimumAmount', CStrings::strToFloatDef( $fltMinimumAmount, NULL, false, 3 ) );
	}

	public function getMinimumAmount() {
		return $this->m_fltMinimumAmount;
	}

	public function sqlMinimumAmount() {
		return ( true == isset( $this->m_fltMinimumAmount ) ) ? ( string ) $this->m_fltMinimumAmount : 'NULL';
	}

	public function setMaximumAmount( $fltMaximumAmount ) {
		$this->set( 'm_fltMaximumAmount', CStrings::strToFloatDef( $fltMaximumAmount, NULL, false, 3 ) );
	}

	public function getMaximumAmount() {
		return $this->m_fltMaximumAmount;
	}

	public function sqlMaximumAmount() {
		return ( true == isset( $this->m_fltMaximumAmount ) ) ? ( string ) $this->m_fltMaximumAmount : 'NULL';
	}

	public function setConditionDetails( $strConditionDetails ) {
		$this->set( 'm_strConditionDetails', CStrings::strTrimDef( $strConditionDetails, -1, NULL, true ) );
	}

	public function getConditionDetails() {
		return $this->m_strConditionDetails;
	}

	public function sqlConditionDetails() {
		return ( true == isset( $this->m_strConditionDetails ) ) ? '\'' . addslashes( $this->m_strConditionDetails ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, route_id, route_rule_id, route_rule_type_id, minimum_amount, maximum_amount, condition_details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRouteId() . ', ' .
 						$this->sqlRouteRuleId() . ', ' .
 						$this->sqlRouteRuleTypeId() . ', ' .
 						$this->sqlMinimumAmount() . ', ' .
 						$this->sqlMaximumAmount() . ', ' .
 						$this->sqlConditionDetails() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_id = ' . $this->sqlRouteId() . ','; } elseif( true == array_key_exists( 'RouteId', $this->getChangedColumns() ) ) { $strSql .= ' route_id = ' . $this->sqlRouteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_rule_id = ' . $this->sqlRouteRuleId() . ','; } elseif( true == array_key_exists( 'RouteRuleId', $this->getChangedColumns() ) ) { $strSql .= ' route_rule_id = ' . $this->sqlRouteRuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_rule_type_id = ' . $this->sqlRouteRuleTypeId() . ','; } elseif( true == array_key_exists( 'RouteRuleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' route_rule_type_id = ' . $this->sqlRouteRuleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_amount = ' . $this->sqlMinimumAmount() . ','; } elseif( true == array_key_exists( 'MinimumAmount', $this->getChangedColumns() ) ) { $strSql .= ' minimum_amount = ' . $this->sqlMinimumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_amount = ' . $this->sqlMaximumAmount() . ','; } elseif( true == array_key_exists( 'MaximumAmount', $this->getChangedColumns() ) ) { $strSql .= ' maximum_amount = ' . $this->sqlMaximumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_details = ' . $this->sqlConditionDetails() . ','; } elseif( true == array_key_exists( 'ConditionDetails', $this->getChangedColumns() ) ) { $strSql .= ' condition_details = ' . $this->sqlConditionDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'route_id' => $this->getRouteId(),
			'route_rule_id' => $this->getRouteRuleId(),
			'route_rule_type_id' => $this->getRouteRuleTypeId(),
			'minimum_amount' => $this->getMinimumAmount(),
			'maximum_amount' => $this->getMaximumAmount(),
			'condition_details' => $this->getConditionDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>