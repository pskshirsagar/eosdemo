<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExpenses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerExpenses extends CEosPluralBase {

	/**
	 * @return CCustomerExpense[]
	 */
	public static function fetchCustomerExpenses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerExpense::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerExpense
	 */
	public static function fetchCustomerExpense( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerExpense::class, $objDatabase );
	}

	public static function fetchCustomerExpenseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_expenses', $objDatabase );
	}

	public static function fetchCustomerExpenseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpense( sprintf( 'SELECT * FROM customer_expenses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpensesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerExpenses( sprintf( 'SELECT * FROM customer_expenses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpensesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenses( sprintf( 'SELECT * FROM customer_expenses WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpensesByExpenseTypeIdByCid( $intExpenseTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenses( sprintf( 'SELECT * FROM customer_expenses WHERE expense_type_id = %d AND cid = %d', $intExpenseTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpensesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenses( sprintf( 'SELECT * FROM customer_expenses WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

}
?>