<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CHelpResourceRatings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResourceRatings extends CEosPluralBase {

	/**
	 * @return CHelpResourceRating[]
	 */
	public static function fetchHelpResourceRatings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CHelpResourceRating', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CHelpResourceRating
	 */
	public static function fetchHelpResourceRating( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHelpResourceRating', $objDatabase );
	}

	public static function fetchHelpResourceRatingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_resource_ratings', $objDatabase );
	}

	public static function fetchHelpResourceRatingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchHelpResourceRating( sprintf( 'SELECT * FROM help_resource_ratings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceRatingsByCid( $intCid, $objDatabase ) {
		return self::fetchHelpResourceRatings( sprintf( 'SELECT * FROM help_resource_ratings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceRatingsByDefaultCidByCid( $intDefaultCid, $intCid, $objDatabase ) {
		return self::fetchHelpResourceRatings( sprintf( 'SELECT * FROM help_resource_ratings WHERE default_cid = %d AND cid = %d', ( int ) $intDefaultCid, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceRatingsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchHelpResourceRatings( sprintf( 'SELECT * FROM help_resource_ratings WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpResourceRatingsByHelpResourceIdByCid( $intHelpResourceId, $intCid, $objDatabase ) {
		return self::fetchHelpResourceRatings( sprintf( 'SELECT * FROM help_resource_ratings WHERE help_resource_id = %d AND cid = %d', ( int ) $intHelpResourceId, ( int ) $intCid ), $objDatabase );
	}

}
?>