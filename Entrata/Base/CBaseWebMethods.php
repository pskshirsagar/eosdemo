<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWebMethods
 * Do not add any new functions to this class.
 */

class CBaseWebMethods extends CEosPluralBase {

	/**
	 * @return CWebMethod[]
	 */
	public static function fetchWebMethods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWebMethod::class, $objDatabase );
	}

	/**
	 * @return CWebMethod
	 */
	public static function fetchWebMethod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWebMethod::class, $objDatabase );
	}

	public static function fetchWebMethodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'web_methods', $objDatabase );
	}

	public static function fetchWebMethodById( $intId, $objDatabase ) {
		return self::fetchWebMethod( sprintf( 'SELECT * FROM web_methods WHERE id = %d', $intId ), $objDatabase );
	}

}
?>