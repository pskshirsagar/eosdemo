<?php

class CBaseDefaultRevenueRateConstraint extends CEosSingularBase {

	const TABLE_NAME = 'public.default_revenue_rate_constraints';

	protected $m_intId;
	protected $m_intRevenueRateConstraintTypeId;
	protected $m_fltRuleChangeAmount;
	protected $m_intIsFixed;
	protected $m_intIsDisabled;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_fltRuleChangeAmount = '0';
		$this->m_intIsFixed = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['revenue_rate_constraint_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueRateConstraintTypeId', trim( $arrValues['revenue_rate_constraint_type_id'] ) ); elseif( isset( $arrValues['revenue_rate_constraint_type_id'] ) ) $this->setRevenueRateConstraintTypeId( $arrValues['revenue_rate_constraint_type_id'] );
		if( isset( $arrValues['rule_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRuleChangeAmount', trim( $arrValues['rule_change_amount'] ) ); elseif( isset( $arrValues['rule_change_amount'] ) ) $this->setRuleChangeAmount( $arrValues['rule_change_amount'] );
		if( isset( $arrValues['is_fixed'] ) && $boolDirectSet ) $this->set( 'm_intIsFixed', trim( $arrValues['is_fixed'] ) ); elseif( isset( $arrValues['is_fixed'] ) ) $this->setIsFixed( $arrValues['is_fixed'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRevenueRateConstraintTypeId( $intRevenueRateConstraintTypeId ) {
		$this->set( 'm_intRevenueRateConstraintTypeId', CStrings::strToIntDef( $intRevenueRateConstraintTypeId, NULL, false ) );
	}

	public function getRevenueRateConstraintTypeId() {
		return $this->m_intRevenueRateConstraintTypeId;
	}

	public function sqlRevenueRateConstraintTypeId() {
		return ( true == isset( $this->m_intRevenueRateConstraintTypeId ) ) ? ( string ) $this->m_intRevenueRateConstraintTypeId : 'NULL';
	}

	public function setRuleChangeAmount( $fltRuleChangeAmount ) {
		$this->set( 'm_fltRuleChangeAmount', CStrings::strToFloatDef( $fltRuleChangeAmount, NULL, false, 2 ) );
	}

	public function getRuleChangeAmount() {
		return $this->m_fltRuleChangeAmount;
	}

	public function sqlRuleChangeAmount() {
		return ( true == isset( $this->m_fltRuleChangeAmount ) ) ? ( string ) $this->m_fltRuleChangeAmount : '0';
	}

	public function setIsFixed( $intIsFixed ) {
		$this->set( 'm_intIsFixed', CStrings::strToIntDef( $intIsFixed, NULL, false ) );
	}

	public function getIsFixed() {
		return $this->m_intIsFixed;
	}

	public function sqlIsFixed() {
		return ( true == isset( $this->m_intIsFixed ) ) ? ( string ) $this->m_intIsFixed : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'revenue_rate_constraint_type_id' => $this->getRevenueRateConstraintTypeId(),
			'rule_change_amount' => $this->getRuleChangeAmount(),
			'is_fixed' => $this->getIsFixed(),
			'is_disabled' => $this->getIsDisabled(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>