<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliations extends CEosPluralBase {

	/**
	 * @return CGlReconciliation[]
	 */
	public static function fetchGlReconciliations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlReconciliation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlReconciliation
	 */
	public static function fetchGlReconciliation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlReconciliation', $objDatabase );
	}

	public static function fetchGlReconciliationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_reconciliations', $objDatabase );
	}

	public static function fetchGlReconciliationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliation( sprintf( 'SELECT * FROM gl_reconciliations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationsByCid( $intCid, $objDatabase ) {
		return self::fetchGlReconciliations( sprintf( 'SELECT * FROM gl_reconciliations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationsByGlReconciliationStatusTypeIdByCid( $intGlReconciliationStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliations( sprintf( 'SELECT * FROM gl_reconciliations WHERE gl_reconciliation_status_type_id = %d AND cid = %d', ( int ) $intGlReconciliationStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliations( sprintf( 'SELECT * FROM gl_reconciliations WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>