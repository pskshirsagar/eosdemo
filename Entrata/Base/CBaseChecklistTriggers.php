<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistTriggers
 * Do not add any new functions to this class.
 */

class CBaseChecklistTriggers extends CEosPluralBase {

	/**
	 * @return CChecklistTrigger[]
	 */
	public static function fetchChecklistTriggers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChecklistTrigger::class, $objDatabase );
	}

	/**
	 * @return CChecklistTrigger
	 */
	public static function fetchChecklistTrigger( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklistTrigger::class, $objDatabase );
	}

	public static function fetchChecklistTriggerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklist_triggers', $objDatabase );
	}

	public static function fetchChecklistTriggerById( $intId, $objDatabase ) {
		return self::fetchChecklistTrigger( sprintf( 'SELECT * FROM checklist_triggers WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchChecklistTriggersByChecklistTypeId( $intChecklistTypeId, $objDatabase ) {
		return self::fetchChecklistTriggers( sprintf( 'SELECT * FROM checklist_triggers WHERE checklist_type_id = %d', $intChecklistTypeId ), $objDatabase );
	}

	public static function fetchChecklistTriggersByApplicationStageId( $intApplicationStageId, $objDatabase ) {
		return self::fetchChecklistTriggers( sprintf( 'SELECT * FROM checklist_triggers WHERE application_stage_id = %d', $intApplicationStageId ), $objDatabase );
	}

	public static function fetchChecklistTriggersByApplicationStatusId( $intApplicationStatusId, $objDatabase ) {
		return self::fetchChecklistTriggers( sprintf( 'SELECT * FROM checklist_triggers WHERE application_status_id = %d', $intApplicationStatusId ), $objDatabase );
	}

	public static function fetchChecklistTriggersByEventTypeId( $intEventTypeId, $objDatabase ) {
		return self::fetchChecklistTriggers( sprintf( 'SELECT * FROM checklist_triggers WHERE event_type_id = %d', $intEventTypeId ), $objDatabase );
	}

}
?>