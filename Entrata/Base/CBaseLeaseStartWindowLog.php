<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseStartWindowLog extends CEosSingularBase {

	const TABLE_NAME = 'logs.lease_start_window_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseStartWindowId;
	protected $m_strAction;
	protected $m_strRowData;
	protected $m_jsonRowData;
	protected $m_strChangedRowData;
	protected $m_jsonChangedRowData;
	protected $m_strSessionApplicationName;
	protected $m_strSessionUserName;
	protected $m_strLogDateTime;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( $arrValues['action'] ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( $arrValues['action'] );
		if( isset( $arrValues['row_data'] ) ) $this->set( 'm_strRowData', trim( $arrValues['row_data'] ) );
		if( isset( $arrValues['changed_row_data'] ) ) $this->set( 'm_strChangedRowData', trim( $arrValues['changed_row_data'] ) );
		if( isset( $arrValues['session_application_name'] ) && $boolDirectSet ) $this->set( 'm_strSessionApplicationName', trim( $arrValues['session_application_name'] ) ); elseif( isset( $arrValues['session_application_name'] ) ) $this->setSessionApplicationName( $arrValues['session_application_name'] );
		if( isset( $arrValues['session_user_name'] ) && $boolDirectSet ) $this->set( 'm_strSessionUserName', trim( $arrValues['session_user_name'] ) ); elseif( isset( $arrValues['session_user_name'] ) ) $this->setSessionUserName( $arrValues['session_user_name'] );
		if( isset( $arrValues['log_date_time'] ) && $boolDirectSet ) $this->set( 'm_strLogDateTime', trim( $arrValues['log_date_time'] ) ); elseif( isset( $arrValues['log_date_time'] ) ) $this->setLogDateTime( $arrValues['log_date_time'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, -1, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAction ) : '\'' . addslashes( $this->m_strAction ) . '\'' ) : 'NULL';
	}

	public function setRowData( $jsonRowData ) {
		if( true == valObj( $jsonRowData, 'stdClass' ) ) {
			$this->set( 'm_jsonRowData', $jsonRowData );
		} elseif( true == valJsonString( $jsonRowData ) ) {
			$this->set( 'm_jsonRowData', CStrings::strToJson( $jsonRowData ) );
		} else {
			$this->set( 'm_jsonRowData', NULL ); 
		}
		unset( $this->m_strRowData );
	}

	public function getRowData() {
		if( true == isset( $this->m_strRowData ) ) {
			$this->m_jsonRowData = CStrings::strToJson( $this->m_strRowData );
			unset( $this->m_strRowData );
		}
		return $this->m_jsonRowData;
	}

	public function sqlRowData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getRowData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getRowData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getRowData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setChangedRowData( $jsonChangedRowData ) {
		if( true == valObj( $jsonChangedRowData, 'stdClass' ) ) {
			$this->set( 'm_jsonChangedRowData', $jsonChangedRowData );
		} elseif( true == valJsonString( $jsonChangedRowData ) ) {
			$this->set( 'm_jsonChangedRowData', CStrings::strToJson( $jsonChangedRowData ) );
		} else {
			$this->set( 'm_jsonChangedRowData', NULL ); 
		}
		unset( $this->m_strChangedRowData );
	}

	public function getChangedRowData() {
		if( true == isset( $this->m_strChangedRowData ) ) {
			$this->m_jsonChangedRowData = CStrings::strToJson( $this->m_strChangedRowData );
			unset( $this->m_strChangedRowData );
		}
		return $this->m_jsonChangedRowData;
	}

	public function sqlChangedRowData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getChangedRowData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getChangedRowData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getChangedRowData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setSessionApplicationName( $strSessionApplicationName ) {
		$this->set( 'm_strSessionApplicationName', CStrings::strTrimDef( $strSessionApplicationName, -1, NULL, true ) );
	}

	public function getSessionApplicationName() {
		return $this->m_strSessionApplicationName;
	}

	public function sqlSessionApplicationName() {
		return ( true == isset( $this->m_strSessionApplicationName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSessionApplicationName ) : '\'' . addslashes( $this->m_strSessionApplicationName ) . '\'' ) : 'NULL';
	}

	public function setSessionUserName( $strSessionUserName ) {
		$this->set( 'm_strSessionUserName', CStrings::strTrimDef( $strSessionUserName, -1, NULL, true ) );
	}

	public function getSessionUserName() {
		return $this->m_strSessionUserName;
	}

	public function sqlSessionUserName() {
		return ( true == isset( $this->m_strSessionUserName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSessionUserName ) : '\'' . addslashes( $this->m_strSessionUserName ) . '\'' ) : 'NULL';
	}

	public function setLogDateTime( $strLogDateTime ) {
		$this->set( 'm_strLogDateTime', CStrings::strTrimDef( $strLogDateTime, -1, NULL, true ) );
	}

	public function getLogDateTime() {
		return $this->m_strLogDateTime;
	}

	public function sqlLogDateTime() {
		return ( true == isset( $this->m_strLogDateTime ) ) ? '\'' . $this->m_strLogDateTime . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_start_window_id, action, row_data, changed_row_data, session_application_name, session_user_name, log_date_time, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlAction() . ', ' .
						$this->sqlRowData() . ', ' .
						$this->sqlChangedRowData() . ', ' .
						$this->sqlSessionApplicationName() . ', ' .
						$this->sqlSessionUserName() . ', ' .
						$this->sqlLogDateTime() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction(). ',' ; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' row_data = ' . $this->sqlRowData(). ',' ; } elseif( true == array_key_exists( 'RowData', $this->getChangedColumns() ) ) { $strSql .= ' row_data = ' . $this->sqlRowData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' changed_row_data = ' . $this->sqlChangedRowData(). ',' ; } elseif( true == array_key_exists( 'ChangedRowData', $this->getChangedColumns() ) ) { $strSql .= ' changed_row_data = ' . $this->sqlChangedRowData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_application_name = ' . $this->sqlSessionApplicationName(). ',' ; } elseif( true == array_key_exists( 'SessionApplicationName', $this->getChangedColumns() ) ) { $strSql .= ' session_application_name = ' . $this->sqlSessionApplicationName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_user_name = ' . $this->sqlSessionUserName(). ',' ; } elseif( true == array_key_exists( 'SessionUserName', $this->getChangedColumns() ) ) { $strSql .= ' session_user_name = ' . $this->sqlSessionUserName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date_time = ' . $this->sqlLogDateTime() ; } elseif( true == array_key_exists( 'LogDateTime', $this->getChangedColumns() ) ) { $strSql .= ' log_date_time = ' . $this->sqlLogDateTime() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'action' => $this->getAction(),
			'row_data' => $this->getRowData(),
			'changed_row_data' => $this->getChangedRowData(),
			'session_application_name' => $this->getSessionApplicationName(),
			'session_user_name' => $this->getSessionUserName(),
			'log_date_time' => $this->getLogDateTime(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>