<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItemReservation extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intRentableItemOptionId;
    protected $m_intRentableItemId;
    protected $m_intLeaseId;
    protected $m_intCustomerId;
    protected $m_intApplicationId;
    protected $m_intApplicantId;
    protected $m_intLeaseStatusTypeId;
    protected $m_intLeaseIntervalId;
    protected $m_intRentArCodeId;
    protected $m_intCompanyEmployeeId;
    protected $m_intDepositArCodeId;
    protected $m_intOtherArCodeId;
    protected $m_intQuoteId;
    protected $m_strRemotePrimaryKey;
    protected $m_strNotes;
    protected $m_fltRentAmount;
    protected $m_fltDepositAmount;
    protected $m_fltOtherAmount;
    protected $m_intIsMonthToMonth;
    protected $m_intIsDisabled;
    protected $m_strReservationDatetime;
    protected $m_strStartDate;
    protected $m_strEndDate;
    protected $m_strRevertEndDate;
    protected $m_strOnNoticeDate;
    protected $m_strReturnedOn;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_fltRentAmount = '0';
        $this->m_fltDepositAmount = '0';
        $this->m_fltOtherAmount = '0';
        $this->m_intIsMonthToMonth = '0';
        $this->m_intIsDisabled = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['rentable_item_option_id'] ) && $boolDirectSet ) $this->m_intRentableItemOptionId = trim( $arrValues['rentable_item_option_id'] ); else if( isset( $arrValues['rentable_item_option_id'] ) ) $this->setRentableItemOptionId( $arrValues['rentable_item_option_id'] );
        if( isset( $arrValues['rentable_item_id'] ) && $boolDirectSet ) $this->m_intRentableItemId = trim( $arrValues['rentable_item_id'] ); else if( isset( $arrValues['rentable_item_id'] ) ) $this->setRentableItemId( $arrValues['rentable_item_id'] );
        if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->m_intLeaseId = trim( $arrValues['lease_id'] ); else if( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
        if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->m_intCustomerId = trim( $arrValues['customer_id'] ); else if( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
        if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->m_intApplicationId = trim( $arrValues['application_id'] ); else if( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
        if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->m_intApplicantId = trim( $arrValues['applicant_id'] ); else if( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
        if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->m_intLeaseStatusTypeId = trim( $arrValues['lease_status_type_id'] ); else if( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
        if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->m_intLeaseIntervalId = trim( $arrValues['lease_interval_id'] ); else if( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
        if( isset( $arrValues['rent_ar_code_id'] ) && $boolDirectSet ) $this->m_intRentArCodeId = trim( $arrValues['rent_ar_code_id'] ); else if( isset( $arrValues['rent_ar_code_id'] ) ) $this->setRentArCodeId( $arrValues['rent_ar_code_id'] );
        if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->m_intCompanyEmployeeId = trim( $arrValues['company_employee_id'] ); else if( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
        if( isset( $arrValues['deposit_ar_code_id'] ) && $boolDirectSet ) $this->m_intDepositArCodeId = trim( $arrValues['deposit_ar_code_id'] ); else if( isset( $arrValues['deposit_ar_code_id'] ) ) $this->setDepositArCodeId( $arrValues['deposit_ar_code_id'] );
        if( isset( $arrValues['other_ar_code_id'] ) && $boolDirectSet ) $this->m_intOtherArCodeId = trim( $arrValues['other_ar_code_id'] ); else if( isset( $arrValues['other_ar_code_id'] ) ) $this->setOtherArCodeId( $arrValues['other_ar_code_id'] );
        if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->m_intQuoteId = trim( $arrValues['quote_id'] ); else if( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->m_strRemotePrimaryKey = trim( stripcslashes( $arrValues['remote_primary_key'] ) ); else if( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
        if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->m_strNotes = trim( stripcslashes( $arrValues['notes'] ) ); else if( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
        if( isset( $arrValues['rent_amount'] ) && $boolDirectSet ) $this->m_fltRentAmount = trim( $arrValues['rent_amount'] ); else if( isset( $arrValues['rent_amount'] ) ) $this->setRentAmount( $arrValues['rent_amount'] );
        if( isset( $arrValues['deposit_amount'] ) && $boolDirectSet ) $this->m_fltDepositAmount = trim( $arrValues['deposit_amount'] ); else if( isset( $arrValues['deposit_amount'] ) ) $this->setDepositAmount( $arrValues['deposit_amount'] );
        if( isset( $arrValues['other_amount'] ) && $boolDirectSet ) $this->m_fltOtherAmount = trim( $arrValues['other_amount'] ); else if( isset( $arrValues['other_amount'] ) ) $this->setOtherAmount( $arrValues['other_amount'] );
        if( isset( $arrValues['is_month_to_month'] ) && $boolDirectSet ) $this->m_intIsMonthToMonth = trim( $arrValues['is_month_to_month'] ); else if( isset( $arrValues['is_month_to_month'] ) ) $this->setIsMonthToMonth( $arrValues['is_month_to_month'] );
        if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->m_intIsDisabled = trim( $arrValues['is_disabled'] ); else if( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
        if( isset( $arrValues['reservation_datetime'] ) && $boolDirectSet ) $this->m_strReservationDatetime = trim( $arrValues['reservation_datetime'] ); else if( isset( $arrValues['reservation_datetime'] ) ) $this->setReservationDatetime( $arrValues['reservation_datetime'] );
        if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->m_strStartDate = trim( $arrValues['start_date'] ); else if( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
        if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->m_strEndDate = trim( $arrValues['end_date'] ); else if( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
        if( isset( $arrValues['revert_end_date'] ) && $boolDirectSet ) $this->m_strRevertEndDate = trim( $arrValues['revert_end_date'] ); else if( isset( $arrValues['revert_end_date'] ) ) $this->setRevertEndDate( $arrValues['revert_end_date'] );
        if( isset( $arrValues['on_notice_date'] ) && $boolDirectSet ) $this->m_strOnNoticeDate = trim( $arrValues['on_notice_date'] ); else if( isset( $arrValues['on_notice_date'] ) ) $this->setOnNoticeDate( $arrValues['on_notice_date'] );
        if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->m_strReturnedOn = trim( $arrValues['returned_on'] ); else if( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setRentableItemOptionId( $intRentableItemOptionId ) {
        $this->m_intRentableItemOptionId = CStrings::strToIntDef( $intRentableItemOptionId, NULL, false );
    }

    public function getRentableItemOptionId() {
        return $this->m_intRentableItemOptionId;
    }

    public function sqlRentableItemOptionId() {
        return ( true == isset( $this->m_intRentableItemOptionId ) ) ? (string) $this->m_intRentableItemOptionId : 'NULL';
    }

    public function setRentableItemId( $intRentableItemId ) {
        $this->m_intRentableItemId = CStrings::strToIntDef( $intRentableItemId, NULL, false );
    }

    public function getRentableItemId() {
        return $this->m_intRentableItemId;
    }

    public function sqlRentableItemId() {
        return ( true == isset( $this->m_intRentableItemId ) ) ? (string) $this->m_intRentableItemId : 'NULL';
    }

    public function setLeaseId( $intLeaseId ) {
        $this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
    }

    public function getLeaseId() {
        return $this->m_intLeaseId;
    }

    public function sqlLeaseId() {
        return ( true == isset( $this->m_intLeaseId ) ) ? (string) $this->m_intLeaseId : 'NULL';
    }

    public function setCustomerId( $intCustomerId ) {
        $this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
    }

    public function getCustomerId() {
        return $this->m_intCustomerId;
    }

    public function sqlCustomerId() {
        return ( true == isset( $this->m_intCustomerId ) ) ? (string) $this->m_intCustomerId : 'NULL';
    }

    public function setApplicationId( $intApplicationId ) {
        $this->m_intApplicationId = CStrings::strToIntDef( $intApplicationId, NULL, false );
    }

    public function getApplicationId() {
        return $this->m_intApplicationId;
    }

    public function sqlApplicationId() {
        return ( true == isset( $this->m_intApplicationId ) ) ? (string) $this->m_intApplicationId : 'NULL';
    }

    public function setApplicantId( $intApplicantId ) {
        $this->m_intApplicantId = CStrings::strToIntDef( $intApplicantId, NULL, false );
    }

    public function getApplicantId() {
        return $this->m_intApplicantId;
    }

    public function sqlApplicantId() {
        return ( true == isset( $this->m_intApplicantId ) ) ? (string) $this->m_intApplicantId : 'NULL';
    }

    public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
        $this->m_intLeaseStatusTypeId = CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false );
    }

    public function getLeaseStatusTypeId() {
        return $this->m_intLeaseStatusTypeId;
    }

    public function sqlLeaseStatusTypeId() {
        return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? (string) $this->m_intLeaseStatusTypeId : 'NULL';
    }

    public function setLeaseIntervalId( $intLeaseIntervalId ) {
        $this->m_intLeaseIntervalId = CStrings::strToIntDef( $intLeaseIntervalId, NULL, false );
    }

    public function getLeaseIntervalId() {
        return $this->m_intLeaseIntervalId;
    }

    public function sqlLeaseIntervalId() {
        return ( true == isset( $this->m_intLeaseIntervalId ) ) ? (string) $this->m_intLeaseIntervalId : 'NULL';
    }

    public function setRentArCodeId( $intRentArCodeId ) {
        $this->m_intRentArCodeId = CStrings::strToIntDef( $intRentArCodeId, NULL, false );
    }

    public function getRentArCodeId() {
        return $this->m_intRentArCodeId;
    }

    public function sqlRentArCodeId() {
        return ( true == isset( $this->m_intRentArCodeId ) ) ? (string) $this->m_intRentArCodeId : 'NULL';
    }

    public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
        $this->m_intCompanyEmployeeId = CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false );
    }

    public function getCompanyEmployeeId() {
        return $this->m_intCompanyEmployeeId;
    }

    public function sqlCompanyEmployeeId() {
        return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? (string) $this->m_intCompanyEmployeeId : 'NULL';
    }

    public function setDepositArCodeId( $intDepositArCodeId ) {
        $this->m_intDepositArCodeId = CStrings::strToIntDef( $intDepositArCodeId, NULL, false );
    }

    public function getDepositArCodeId() {
        return $this->m_intDepositArCodeId;
    }

    public function sqlDepositArCodeId() {
        return ( true == isset( $this->m_intDepositArCodeId ) ) ? (string) $this->m_intDepositArCodeId : 'NULL';
    }

    public function setOtherArCodeId( $intOtherArCodeId ) {
        $this->m_intOtherArCodeId = CStrings::strToIntDef( $intOtherArCodeId, NULL, false );
    }

    public function getOtherArCodeId() {
        return $this->m_intOtherArCodeId;
    }

    public function sqlOtherArCodeId() {
        return ( true == isset( $this->m_intOtherArCodeId ) ) ? (string) $this->m_intOtherArCodeId : 'NULL';
    }

    public function setQuoteId( $intQuoteId ) {
        $this->m_intQuoteId = CStrings::strToIntDef( $intQuoteId, NULL, false );
    }

    public function getQuoteId() {
        return $this->m_intQuoteId;
    }

    public function sqlQuoteId() {
        return ( true == isset( $this->m_intQuoteId ) ) ? (string) $this->m_intQuoteId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->m_strRemotePrimaryKey = CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
    }

    public function setNotes( $strNotes ) {
        $this->m_strNotes = CStrings::strTrimDef( $strNotes, -1, NULL, true );
    }

    public function getNotes() {
        return $this->m_strNotes;
    }

    public function sqlNotes() {
        return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
    }

    public function setRentAmount( $fltRentAmount ) {
        $this->m_fltRentAmount = CStrings::strToFloatDef( $fltRentAmount, NULL, false, 2 );
    }

    public function getRentAmount() {
        return $this->m_fltRentAmount;
    }

    public function sqlRentAmount() {
        return ( true == isset( $this->m_fltRentAmount ) ) ? (string) $this->m_fltRentAmount : '0';
    }

    public function setDepositAmount( $fltDepositAmount ) {
        $this->m_fltDepositAmount = CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 2 );
    }

    public function getDepositAmount() {
        return $this->m_fltDepositAmount;
    }

    public function sqlDepositAmount() {
        return ( true == isset( $this->m_fltDepositAmount ) ) ? (string) $this->m_fltDepositAmount : '0';
    }

    public function setOtherAmount( $fltOtherAmount ) {
        $this->m_fltOtherAmount = CStrings::strToFloatDef( $fltOtherAmount, NULL, false, 2 );
    }

    public function getOtherAmount() {
        return $this->m_fltOtherAmount;
    }

    public function sqlOtherAmount() {
        return ( true == isset( $this->m_fltOtherAmount ) ) ? (string) $this->m_fltOtherAmount : '0';
    }

    public function setIsMonthToMonth( $intIsMonthToMonth ) {
        $this->m_intIsMonthToMonth = CStrings::strToIntDef( $intIsMonthToMonth, NULL, false );
    }

    public function getIsMonthToMonth() {
        return $this->m_intIsMonthToMonth;
    }

    public function sqlIsMonthToMonth() {
        return ( true == isset( $this->m_intIsMonthToMonth ) ) ? (string) $this->m_intIsMonthToMonth : '0';
    }

    public function setIsDisabled( $intIsDisabled ) {
        $this->m_intIsDisabled = CStrings::strToIntDef( $intIsDisabled, NULL, false );
    }

    public function getIsDisabled() {
        return $this->m_intIsDisabled;
    }

    public function sqlIsDisabled() {
        return ( true == isset( $this->m_intIsDisabled ) ) ? (string) $this->m_intIsDisabled : '0';
    }

    public function setReservationDatetime( $strReservationDatetime ) {
        $this->m_strReservationDatetime = CStrings::strTrimDef( $strReservationDatetime, -1, NULL, true );
    }

    public function getReservationDatetime() {
        return $this->m_strReservationDatetime;
    }

    public function sqlReservationDatetime() {
        return ( true == isset( $this->m_strReservationDatetime ) ) ? '\'' . $this->m_strReservationDatetime . '\'' : 'NOW()';
    }

    public function setStartDate( $strStartDate ) {
        $this->m_strStartDate = CStrings::strTrimDef( $strStartDate, -1, NULL, true );
    }

    public function getStartDate() {
        return $this->m_strStartDate;
    }

    public function sqlStartDate() {
        return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
    }

    public function setEndDate( $strEndDate ) {
        $this->m_strEndDate = CStrings::strTrimDef( $strEndDate, -1, NULL, true );
    }

    public function getEndDate() {
        return $this->m_strEndDate;
    }

    public function sqlEndDate() {
        return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
    }

    public function setRevertEndDate( $strRevertEndDate ) {
        $this->m_strRevertEndDate = CStrings::strTrimDef( $strRevertEndDate, -1, NULL, true );
    }

    public function getRevertEndDate() {
        return $this->m_strRevertEndDate;
    }

    public function sqlRevertEndDate() {
        return ( true == isset( $this->m_strRevertEndDate ) ) ? '\'' . $this->m_strRevertEndDate . '\'' : 'NULL';
    }

    public function setOnNoticeDate( $strOnNoticeDate ) {
        $this->m_strOnNoticeDate = CStrings::strTrimDef( $strOnNoticeDate, -1, NULL, true );
    }

    public function getOnNoticeDate() {
        return $this->m_strOnNoticeDate;
    }

    public function sqlOnNoticeDate() {
        return ( true == isset( $this->m_strOnNoticeDate ) ) ? '\'' . $this->m_strOnNoticeDate . '\'' : 'NULL';
    }

    public function setReturnedOn( $strReturnedOn ) {
        $this->m_strReturnedOn = CStrings::strTrimDef( $strReturnedOn, -1, NULL, true );
    }

    public function getReturnedOn() {
        return $this->m_strReturnedOn;
    }

    public function sqlReturnedOn() {
        return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return ( true == isset( $this->m_intDeletedBy ) ) ? (string) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.rentable_item_reservations_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.rentable_item_reservations					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlRentableItemOptionId() . ', ' . 		                $this->sqlRentableItemId() . ', ' . 		                $this->sqlLeaseId() . ', ' . 		                $this->sqlCustomerId() . ', ' . 		                $this->sqlApplicationId() . ', ' . 		                $this->sqlApplicantId() . ', ' . 		                $this->sqlLeaseStatusTypeId() . ', ' . 		                $this->sqlLeaseIntervalId() . ', ' . 		                $this->sqlRentArCodeId() . ', ' . 		                $this->sqlCompanyEmployeeId() . ', ' . 		                $this->sqlDepositArCodeId() . ', ' . 		                $this->sqlOtherArCodeId() . ', ' . 		                $this->sqlQuoteId() . ', ' . 		                $this->sqlRemotePrimaryKey() . ', ' . 		                $this->sqlNotes() . ', ' . 		                $this->sqlRentAmount() . ', ' . 		                $this->sqlDepositAmount() . ', ' . 		                $this->sqlOtherAmount() . ', ' . 		                $this->sqlIsMonthToMonth() . ', ' . 		                $this->sqlIsDisabled() . ', ' . 		                $this->sqlReservationDatetime() . ', ' . 		                $this->sqlStartDate() . ', ' . 		                $this->sqlEndDate() . ', ' . 		                $this->sqlRevertEndDate() . ', ' . 		                $this->sqlOnNoticeDate() . ', ' . 		                $this->sqlReturnedOn() . ', ' . 		                $this->sqlDeletedBy() . ', ' . 		                $this->sqlDeletedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.rentable_item_reservations
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_item_option_id = ' . $this->sqlRentableItemOptionId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRentableItemOptionId() ) != $this->getOriginalValueByFieldName ( 'rentable_item_option_id' ) ) { $arrstrOriginalValueChanges['rentable_item_option_id'] = $this->sqlRentableItemOptionId(); $strSql .= ' rentable_item_option_id = ' . $this->sqlRentableItemOptionId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_item_id = ' . $this->sqlRentableItemId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRentableItemId() ) != $this->getOriginalValueByFieldName ( 'rentable_item_id' ) ) { $arrstrOriginalValueChanges['rentable_item_id'] = $this->sqlRentableItemId(); $strSql .= ' rentable_item_id = ' . $this->sqlRentableItemId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseId() ) != $this->getOriginalValueByFieldName ( 'lease_id' ) ) { $arrstrOriginalValueChanges['lease_id'] = $this->sqlLeaseId(); $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCustomerId() ) != $this->getOriginalValueByFieldName ( 'customer_id' ) ) { $arrstrOriginalValueChanges['customer_id'] = $this->sqlCustomerId(); $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicationId() ) != $this->getOriginalValueByFieldName ( 'application_id' ) ) { $arrstrOriginalValueChanges['application_id'] = $this->sqlApplicationId(); $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicantId() ) != $this->getOriginalValueByFieldName ( 'applicant_id' ) ) { $arrstrOriginalValueChanges['applicant_id'] = $this->sqlApplicantId(); $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseStatusTypeId() ) != $this->getOriginalValueByFieldName ( 'lease_status_type_id' ) ) { $arrstrOriginalValueChanges['lease_status_type_id'] = $this->sqlLeaseStatusTypeId(); $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseIntervalId() ) != $this->getOriginalValueByFieldName ( 'lease_interval_id' ) ) { $arrstrOriginalValueChanges['lease_interval_id'] = $this->sqlLeaseIntervalId(); $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRentArCodeId() ) != $this->getOriginalValueByFieldName ( 'rent_ar_code_id' ) ) { $arrstrOriginalValueChanges['rent_ar_code_id'] = $this->sqlRentArCodeId(); $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyEmployeeId() ) != $this->getOriginalValueByFieldName ( 'company_employee_id' ) ) { $arrstrOriginalValueChanges['company_employee_id'] = $this->sqlCompanyEmployeeId(); $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDepositArCodeId() ) != $this->getOriginalValueByFieldName ( 'deposit_ar_code_id' ) ) { $arrstrOriginalValueChanges['deposit_ar_code_id'] = $this->sqlDepositArCodeId(); $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_ar_code_id = ' . $this->sqlOtherArCodeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOtherArCodeId() ) != $this->getOriginalValueByFieldName ( 'other_ar_code_id' ) ) { $arrstrOriginalValueChanges['other_ar_code_id'] = $this->sqlOtherArCodeId(); $strSql .= ' other_ar_code_id = ' . $this->sqlOtherArCodeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlQuoteId() ) != $this->getOriginalValueByFieldName ( 'quote_id' ) ) { $arrstrOriginalValueChanges['quote_id'] = $this->sqlQuoteId(); $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRemotePrimaryKey() ) != $this->getOriginalValueByFieldName ( 'remote_primary_key' ) ) { $arrstrOriginalValueChanges['remote_primary_key'] = $this->sqlRemotePrimaryKey(); $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNotes() ) != $this->getOriginalValueByFieldName ( 'notes' ) ) { $arrstrOriginalValueChanges['notes'] = $this->sqlNotes(); $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_amount = ' . $this->sqlRentAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlRentAmount() ), $this->getOriginalValueByFieldName ( 'rent_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['rent_amount'] = $this->sqlRentAmount(); $strSql .= ' rent_amount = ' . $this->sqlRentAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlDepositAmount() ), $this->getOriginalValueByFieldName ( 'deposit_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['deposit_amount'] = $this->sqlDepositAmount(); $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_amount = ' . $this->sqlOtherAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlOtherAmount() ), $this->getOriginalValueByFieldName ( 'other_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['other_amount'] = $this->sqlOtherAmount(); $strSql .= ' other_amount = ' . $this->sqlOtherAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_month_to_month = ' . $this->sqlIsMonthToMonth() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsMonthToMonth() ) != $this->getOriginalValueByFieldName ( 'is_month_to_month' ) ) { $arrstrOriginalValueChanges['is_month_to_month'] = $this->sqlIsMonthToMonth(); $strSql .= ' is_month_to_month = ' . $this->sqlIsMonthToMonth() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsDisabled() ) != $this->getOriginalValueByFieldName ( 'is_disabled' ) ) { $arrstrOriginalValueChanges['is_disabled'] = $this->sqlIsDisabled(); $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_datetime = ' . $this->sqlReservationDatetime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlReservationDatetime() ) != $this->getOriginalValueByFieldName ( 'reservation_datetime' ) ) { $arrstrOriginalValueChanges['reservation_datetime'] = $this->sqlReservationDatetime(); $strSql .= ' reservation_datetime = ' . $this->sqlReservationDatetime() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStartDate() ) != $this->getOriginalValueByFieldName ( 'start_date' ) ) { $arrstrOriginalValueChanges['start_date'] = $this->sqlStartDate(); $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEndDate() ) != $this->getOriginalValueByFieldName ( 'end_date' ) ) { $arrstrOriginalValueChanges['end_date'] = $this->sqlEndDate(); $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revert_end_date = ' . $this->sqlRevertEndDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRevertEndDate() ) != $this->getOriginalValueByFieldName ( 'revert_end_date' ) ) { $arrstrOriginalValueChanges['revert_end_date'] = $this->sqlRevertEndDate(); $strSql .= ' revert_end_date = ' . $this->sqlRevertEndDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_notice_date = ' . $this->sqlOnNoticeDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOnNoticeDate() ) != $this->getOriginalValueByFieldName ( 'on_notice_date' ) ) { $arrstrOriginalValueChanges['on_notice_date'] = $this->sqlOnNoticeDate(); $strSql .= ' on_notice_date = ' . $this->sqlOnNoticeDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlReturnedOn() ) != $this->getOriginalValueByFieldName ( 'returned_on' ) ) { $arrstrOriginalValueChanges['returned_on'] = $this->sqlReturnedOn(); $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName ( 'deleted_by' ) ) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName ( 'deleted_on' ) ) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.rentable_item_reservations WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.rentable_item_reservations_id_seq', $objDatabase );
    }

}
?>