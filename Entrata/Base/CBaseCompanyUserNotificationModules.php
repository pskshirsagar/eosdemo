<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserNotificationModules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserNotificationModules extends CEosPluralBase {

	/**
	 * @return CCompanyUserNotificationModule[]
	 */
	public static function fetchCompanyUserNotificationModules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserNotificationModule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserNotificationModule
	 */
	public static function fetchCompanyUserNotificationModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserNotificationModule', $objDatabase );
	}

	public static function fetchCompanyUserNotificationModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_notification_modules', $objDatabase );
	}

	public static function fetchCompanyUserNotificationModuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotificationModule( sprintf( 'SELECT * FROM company_user_notification_modules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserNotificationModulesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotificationModules( sprintf( 'SELECT * FROM company_user_notification_modules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserNotificationModulesByCompanyUserNotificationIdByCid( $intCompanyUserNotificationId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotificationModules( sprintf( 'SELECT * FROM company_user_notification_modules WHERE company_user_notification_id = %d AND cid = %d', ( int ) $intCompanyUserNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserNotificationModulesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotificationModules( sprintf( 'SELECT * FROM company_user_notification_modules WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>