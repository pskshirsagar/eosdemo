<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceNamePattern extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.unit_space_name_patterns';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strUnitNumberPattern;
	protected $m_strUnitSpacePattern;
	protected $m_strUnitSpaceNameExamples;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['unit_number_pattern'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitNumberPattern', trim( stripcslashes( $arrValues['unit_number_pattern'] ) ) ); elseif( isset( $arrValues['unit_number_pattern'] ) ) $this->setUnitNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_pattern'] ) : $arrValues['unit_number_pattern'] );
		if( isset( $arrValues['unit_space_pattern'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitSpacePattern', trim( stripcslashes( $arrValues['unit_space_pattern'] ) ) ); elseif( isset( $arrValues['unit_space_pattern'] ) ) $this->setUnitSpacePattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_pattern'] ) : $arrValues['unit_space_pattern'] );
		if( isset( $arrValues['unit_space_name_examples'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitSpaceNameExamples', trim( stripcslashes( $arrValues['unit_space_name_examples'] ) ) ); elseif( isset( $arrValues['unit_space_name_examples'] ) ) $this->setUnitSpaceNameExamples( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_name_examples'] ) : $arrValues['unit_space_name_examples'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUnitNumberPattern( $strUnitNumberPattern, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitNumberPattern', CStrings::strTrimDef( $strUnitNumberPattern, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitNumberPattern( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitNumberPattern', $strLocaleCode );
	}

	public function sqlUnitNumberPattern() {
		return ( true == isset( $this->m_strUnitNumberPattern ) ) ? '\'' . addslashes( $this->m_strUnitNumberPattern ) . '\'' : 'NULL';
	}

	public function setUnitSpacePattern( $strUnitSpacePattern, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitSpacePattern', CStrings::strTrimDef( $strUnitSpacePattern, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitSpacePattern( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitSpacePattern', $strLocaleCode );
	}

	public function sqlUnitSpacePattern() {
		return ( true == isset( $this->m_strUnitSpacePattern ) ) ? '\'' . addslashes( $this->m_strUnitSpacePattern ) . '\'' : 'NULL';
	}

	public function setUnitSpaceNameExamples( $strUnitSpaceNameExamples, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitSpaceNameExamples', CStrings::strTrimDef( $strUnitSpaceNameExamples, -1, NULL, true ), $strLocaleCode );
	}

	public function getUnitSpaceNameExamples( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitSpaceNameExamples', $strLocaleCode );
	}

	public function sqlUnitSpaceNameExamples() {
		return ( true == isset( $this->m_strUnitSpaceNameExamples ) ) ? '\'' . addslashes( $this->m_strUnitSpaceNameExamples ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, unit_number_pattern, unit_space_pattern, unit_space_name_examples, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUnitNumberPattern() . ', ' .
						$this->sqlUnitSpacePattern() . ', ' .
						$this->sqlUnitSpaceNameExamples() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_pattern = ' . $this->sqlUnitNumberPattern(). ',' ; } elseif( true == array_key_exists( 'UnitNumberPattern', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_pattern = ' . $this->sqlUnitNumberPattern() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_pattern = ' . $this->sqlUnitSpacePattern(). ',' ; } elseif( true == array_key_exists( 'UnitSpacePattern', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_pattern = ' . $this->sqlUnitSpacePattern() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_name_examples = ' . $this->sqlUnitSpaceNameExamples(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceNameExamples', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_name_examples = ' . $this->sqlUnitSpaceNameExamples() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'unit_number_pattern' => $this->getUnitNumberPattern(),
			'unit_space_pattern' => $this->getUnitSpacePattern(),
			'unit_space_name_examples' => $this->getUnitSpaceNameExamples(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>