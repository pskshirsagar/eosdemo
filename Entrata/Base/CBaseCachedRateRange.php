<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedRateRange extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.cached_rate_ranges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intOccupancyTypeId;
	protected $m_intArTriggerId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intSpaceConfigurationId;
	protected $m_intLeaseTermMonths;
	protected $m_strEffectiveRange;
	protected $m_fltBaseAmount;
	protected $m_fltBaseTax;
	protected $m_fltBaseRent;
	protected $m_fltAmenityAmount;
	protected $m_fltAmenityTax;
	protected $m_fltAmenityRent;
	protected $m_fltSpecialAmount;
	protected $m_fltSpecialTax;
	protected $m_fltSpecialRent;
	protected $m_fltAddOnAmount;
	protected $m_fltAddOnTax;
	protected $m_fltAddOnRent;
	protected $m_fltMarketRent;
	protected $m_fltBaseDepositAmount;
	protected $m_fltBaseDepositTax;
	protected $m_fltBaseDeposit;
	protected $m_fltAmenityDepositAmount;
	protected $m_fltAmenityDepositTax;
	protected $m_fltAmenityDeposit;
	protected $m_fltSpecialDepositAmount;
	protected $m_fltSpecialDepositTax;
	protected $m_fltSpecialDeposit;
	protected $m_fltAddOnDepositAmount;
	protected $m_fltAddOnDepositTax;
	protected $m_fltAddOnDeposit;
	protected $m_fltTotalDeposit;
	protected $m_fltEntrataBaseAmount;
	protected $m_fltEntrataBaseTaxAmount;
	protected $m_fltEntrataBaseRent;
	protected $m_fltEntrataAmenityAmount;
	protected $m_fltEntrataAmenityTaxAmount;
	protected $m_fltEntrataAmenityRent;
	protected $m_fltEntrataSpecialAmount;
	protected $m_fltEntrataSpecialTaxAmount;
	protected $m_fltEntrataSpecialRent;
	protected $m_fltEntrataAddOnAmount;
	protected $m_fltEntrataAddOnTaxAmount;
	protected $m_fltEntrataAddOnRent;
	protected $m_fltEntrataMarketRent;
	protected $m_fltEntrataBaseDepositAmount;
	protected $m_fltEntrataBaseDepositTaxAmount;
	protected $m_fltEntrataBaseDeposit;
	protected $m_fltEntrataAmenityDepositAmount;
	protected $m_fltEntrataAmenityDepositTaxAmount;
	protected $m_fltEntrataAmenityDeposit;
	protected $m_fltEntrataSpecialDepositAmount;
	protected $m_fltEntrataSpecialDepositTaxAmount;
	protected $m_fltEntrataSpecialDeposit;
	protected $m_fltEntrataAddOnDepositAmount;
	protected $m_fltEntrataAddOnDepositTaxAmount;
	protected $m_fltEntrataAddOnDeposit;
	protected $m_fltEntrataTotalDeposit;
	protected $m_fltBaseAmountMonthly;
	protected $m_fltBaseTaxMonthly;
	protected $m_fltBaseRentMonthly;
	protected $m_fltAmenityAmountMonthly;
	protected $m_fltAmenityTaxMonthly;
	protected $m_fltAmenityRentMonthly;
	protected $m_fltSpecialAmountMonthly;
	protected $m_fltSpecialTaxMonthly;
	protected $m_fltSpecialRentMonthly;
	protected $m_fltAddOnAmountMonthly;
	protected $m_fltAddOnTaxMonthly;
	protected $m_fltAddOnRentMonthly;
	protected $m_fltMarketRentMonthly;
	protected $m_fltMarketAmountMonthly;
	protected $m_fltEntrataMarketAmountMonthly;
	protected $m_strCachedDatetime;
	protected $m_boolShowOnWebsite;
	protected $m_boolIncludeTaxInAdvertisedRent;
	protected $m_boolIncludeOtherInAdvertisedRent;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_fltBaseOther;
	protected $m_fltAmenityOther;
	protected $m_fltSpecialOther;
	protected $m_fltAddOnOther;
	protected $m_fltEntrataBaseOther;
	protected $m_fltEntrataAmenityOther;
	protected $m_fltEntrataSpecialOther;
	protected $m_fltEntrataAddOnOther;
	protected $m_fltBaseOtherMonthly;
	protected $m_fltAmenityOtherMonthly;
	protected $m_fltSpecialOtherMonthly;
	protected $m_fltAddOnOtherMonthly;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupancyTypeId = '1';
		$this->m_intLeaseStartWindowId = '0';
		$this->m_intSpaceConfigurationId = '0';
		$this->m_intLeaseTermMonths = '0';
		$this->m_fltBaseRent = '0';
		$this->m_fltAmenityRent = '0';
		$this->m_fltSpecialRent = '0';
		$this->m_fltAddOnRent = '0';
		$this->m_fltMarketRent = '0';
		$this->m_fltBaseDeposit = '0';
		$this->m_fltAmenityDeposit = '0';
		$this->m_fltSpecialDeposit = '0';
		$this->m_fltAddOnDeposit = '0';
		$this->m_fltTotalDeposit = '0';
		$this->m_strCachedDatetime = 'now()';
		$this->m_boolShowOnWebsite = true;
		$this->m_boolIncludeTaxInAdvertisedRent = false;
		$this->m_boolIncludeOtherInAdvertisedRent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermMonths', trim( $arrValues['lease_term_months'] ) ); elseif( isset( $arrValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrValues['lease_term_months'] );
		if( isset( $arrValues['effective_range'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveRange', trim( $arrValues['effective_range'] ) ); elseif( isset( $arrValues['effective_range'] ) ) $this->setEffectiveRange( $arrValues['effective_range'] );
		if( isset( $arrValues['base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBaseAmount', trim( $arrValues['base_amount'] ) ); elseif( isset( $arrValues['base_amount'] ) ) $this->setBaseAmount( $arrValues['base_amount'] );
		if( isset( $arrValues['base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltBaseTax', trim( $arrValues['base_tax'] ) ); elseif( isset( $arrValues['base_tax'] ) ) $this->setBaseTax( $arrValues['base_tax'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityAmount', trim( $arrValues['amenity_amount'] ) ); elseif( isset( $arrValues['amenity_amount'] ) ) $this->setAmenityAmount( $arrValues['amenity_amount'] );
		if( isset( $arrValues['amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityTax', trim( $arrValues['amenity_tax'] ) ); elseif( isset( $arrValues['amenity_tax'] ) ) $this->setAmenityTax( $arrValues['amenity_tax'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialAmount', trim( $arrValues['special_amount'] ) ); elseif( isset( $arrValues['special_amount'] ) ) $this->setSpecialAmount( $arrValues['special_amount'] );
		if( isset( $arrValues['special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialTax', trim( $arrValues['special_tax'] ) ); elseif( isset( $arrValues['special_tax'] ) ) $this->setSpecialTax( $arrValues['special_tax'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnAmount', trim( $arrValues['add_on_amount'] ) ); elseif( isset( $arrValues['add_on_amount'] ) ) $this->setAddOnAmount( $arrValues['add_on_amount'] );
		if( isset( $arrValues['add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnTax', trim( $arrValues['add_on_tax'] ) ); elseif( isset( $arrValues['add_on_tax'] ) ) $this->setAddOnTax( $arrValues['add_on_tax'] );
		if( isset( $arrValues['add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnRent', trim( $arrValues['add_on_rent'] ) ); elseif( isset( $arrValues['add_on_rent'] ) ) $this->setAddOnRent( $arrValues['add_on_rent'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['base_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBaseDepositAmount', trim( $arrValues['base_deposit_amount'] ) ); elseif( isset( $arrValues['base_deposit_amount'] ) ) $this->setBaseDepositAmount( $arrValues['base_deposit_amount'] );
		if( isset( $arrValues['base_deposit_tax'] ) && $boolDirectSet ) $this->set( 'm_fltBaseDepositTax', trim( $arrValues['base_deposit_tax'] ) ); elseif( isset( $arrValues['base_deposit_tax'] ) ) $this->setBaseDepositTax( $arrValues['base_deposit_tax'] );
		if( isset( $arrValues['base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltBaseDeposit', trim( $arrValues['base_deposit'] ) ); elseif( isset( $arrValues['base_deposit'] ) ) $this->setBaseDeposit( $arrValues['base_deposit'] );
		if( isset( $arrValues['amenity_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityDepositAmount', trim( $arrValues['amenity_deposit_amount'] ) ); elseif( isset( $arrValues['amenity_deposit_amount'] ) ) $this->setAmenityDepositAmount( $arrValues['amenity_deposit_amount'] );
		if( isset( $arrValues['amenity_deposit_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityDepositTax', trim( $arrValues['amenity_deposit_tax'] ) ); elseif( isset( $arrValues['amenity_deposit_tax'] ) ) $this->setAmenityDepositTax( $arrValues['amenity_deposit_tax'] );
		if( isset( $arrValues['amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityDeposit', trim( $arrValues['amenity_deposit'] ) ); elseif( isset( $arrValues['amenity_deposit'] ) ) $this->setAmenityDeposit( $arrValues['amenity_deposit'] );
		if( isset( $arrValues['special_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialDepositAmount', trim( $arrValues['special_deposit_amount'] ) ); elseif( isset( $arrValues['special_deposit_amount'] ) ) $this->setSpecialDepositAmount( $arrValues['special_deposit_amount'] );
		if( isset( $arrValues['special_deposit_tax'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialDepositTax', trim( $arrValues['special_deposit_tax'] ) ); elseif( isset( $arrValues['special_deposit_tax'] ) ) $this->setSpecialDepositTax( $arrValues['special_deposit_tax'] );
		if( isset( $arrValues['special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialDeposit', trim( $arrValues['special_deposit'] ) ); elseif( isset( $arrValues['special_deposit'] ) ) $this->setSpecialDeposit( $arrValues['special_deposit'] );
		if( isset( $arrValues['add_on_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnDepositAmount', trim( $arrValues['add_on_deposit_amount'] ) ); elseif( isset( $arrValues['add_on_deposit_amount'] ) ) $this->setAddOnDepositAmount( $arrValues['add_on_deposit_amount'] );
		if( isset( $arrValues['add_on_deposit_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnDepositTax', trim( $arrValues['add_on_deposit_tax'] ) ); elseif( isset( $arrValues['add_on_deposit_tax'] ) ) $this->setAddOnDepositTax( $arrValues['add_on_deposit_tax'] );
		if( isset( $arrValues['add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnDeposit', trim( $arrValues['add_on_deposit'] ) ); elseif( isset( $arrValues['add_on_deposit'] ) ) $this->setAddOnDeposit( $arrValues['add_on_deposit'] );
		if( isset( $arrValues['total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltTotalDeposit', trim( $arrValues['total_deposit'] ) ); elseif( isset( $arrValues['total_deposit'] ) ) $this->setTotalDeposit( $arrValues['total_deposit'] );
		if( isset( $arrValues['entrata_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseAmount', trim( $arrValues['entrata_base_amount'] ) ); elseif( isset( $arrValues['entrata_base_amount'] ) ) $this->setEntrataBaseAmount( $arrValues['entrata_base_amount'] );
		if( isset( $arrValues['entrata_base_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseTaxAmount', trim( $arrValues['entrata_base_tax_amount'] ) ); elseif( isset( $arrValues['entrata_base_tax_amount'] ) ) $this->setEntrataBaseTaxAmount( $arrValues['entrata_base_tax_amount'] );
		if( isset( $arrValues['entrata_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseRent', trim( $arrValues['entrata_base_rent'] ) ); elseif( isset( $arrValues['entrata_base_rent'] ) ) $this->setEntrataBaseRent( $arrValues['entrata_base_rent'] );
		if( isset( $arrValues['entrata_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityAmount', trim( $arrValues['entrata_amenity_amount'] ) ); elseif( isset( $arrValues['entrata_amenity_amount'] ) ) $this->setEntrataAmenityAmount( $arrValues['entrata_amenity_amount'] );
		if( isset( $arrValues['entrata_amenity_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityTaxAmount', trim( $arrValues['entrata_amenity_tax_amount'] ) ); elseif( isset( $arrValues['entrata_amenity_tax_amount'] ) ) $this->setEntrataAmenityTaxAmount( $arrValues['entrata_amenity_tax_amount'] );
		if( isset( $arrValues['entrata_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityRent', trim( $arrValues['entrata_amenity_rent'] ) ); elseif( isset( $arrValues['entrata_amenity_rent'] ) ) $this->setEntrataAmenityRent( $arrValues['entrata_amenity_rent'] );
		if( isset( $arrValues['entrata_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialAmount', trim( $arrValues['entrata_special_amount'] ) ); elseif( isset( $arrValues['entrata_special_amount'] ) ) $this->setEntrataSpecialAmount( $arrValues['entrata_special_amount'] );
		if( isset( $arrValues['entrata_special_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialTaxAmount', trim( $arrValues['entrata_special_tax_amount'] ) ); elseif( isset( $arrValues['entrata_special_tax_amount'] ) ) $this->setEntrataSpecialTaxAmount( $arrValues['entrata_special_tax_amount'] );
		if( isset( $arrValues['entrata_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialRent', trim( $arrValues['entrata_special_rent'] ) ); elseif( isset( $arrValues['entrata_special_rent'] ) ) $this->setEntrataSpecialRent( $arrValues['entrata_special_rent'] );
		if( isset( $arrValues['entrata_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnAmount', trim( $arrValues['entrata_add_on_amount'] ) ); elseif( isset( $arrValues['entrata_add_on_amount'] ) ) $this->setEntrataAddOnAmount( $arrValues['entrata_add_on_amount'] );
		if( isset( $arrValues['entrata_add_on_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnTaxAmount', trim( $arrValues['entrata_add_on_tax_amount'] ) ); elseif( isset( $arrValues['entrata_add_on_tax_amount'] ) ) $this->setEntrataAddOnTaxAmount( $arrValues['entrata_add_on_tax_amount'] );
		if( isset( $arrValues['entrata_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnRent', trim( $arrValues['entrata_add_on_rent'] ) ); elseif( isset( $arrValues['entrata_add_on_rent'] ) ) $this->setEntrataAddOnRent( $arrValues['entrata_add_on_rent'] );
		if( isset( $arrValues['entrata_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMarketRent', trim( $arrValues['entrata_market_rent'] ) ); elseif( isset( $arrValues['entrata_market_rent'] ) ) $this->setEntrataMarketRent( $arrValues['entrata_market_rent'] );
		if( isset( $arrValues['entrata_base_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseDepositAmount', trim( $arrValues['entrata_base_deposit_amount'] ) ); elseif( isset( $arrValues['entrata_base_deposit_amount'] ) ) $this->setEntrataBaseDepositAmount( $arrValues['entrata_base_deposit_amount'] );
		if( isset( $arrValues['entrata_base_deposit_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseDepositTaxAmount', trim( $arrValues['entrata_base_deposit_tax_amount'] ) ); elseif( isset( $arrValues['entrata_base_deposit_tax_amount'] ) ) $this->setEntrataBaseDepositTaxAmount( $arrValues['entrata_base_deposit_tax_amount'] );
		if( isset( $arrValues['entrata_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseDeposit', trim( $arrValues['entrata_base_deposit'] ) ); elseif( isset( $arrValues['entrata_base_deposit'] ) ) $this->setEntrataBaseDeposit( $arrValues['entrata_base_deposit'] );
		if( isset( $arrValues['entrata_amenity_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityDepositAmount', trim( $arrValues['entrata_amenity_deposit_amount'] ) ); elseif( isset( $arrValues['entrata_amenity_deposit_amount'] ) ) $this->setEntrataAmenityDepositAmount( $arrValues['entrata_amenity_deposit_amount'] );
		if( isset( $arrValues['entrata_amenity_deposit_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityDepositTaxAmount', trim( $arrValues['entrata_amenity_deposit_tax_amount'] ) ); elseif( isset( $arrValues['entrata_amenity_deposit_tax_amount'] ) ) $this->setEntrataAmenityDepositTaxAmount( $arrValues['entrata_amenity_deposit_tax_amount'] );
		if( isset( $arrValues['entrata_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityDeposit', trim( $arrValues['entrata_amenity_deposit'] ) ); elseif( isset( $arrValues['entrata_amenity_deposit'] ) ) $this->setEntrataAmenityDeposit( $arrValues['entrata_amenity_deposit'] );
		if( isset( $arrValues['entrata_special_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialDepositAmount', trim( $arrValues['entrata_special_deposit_amount'] ) ); elseif( isset( $arrValues['entrata_special_deposit_amount'] ) ) $this->setEntrataSpecialDepositAmount( $arrValues['entrata_special_deposit_amount'] );
		if( isset( $arrValues['entrata_special_deposit_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialDepositTaxAmount', trim( $arrValues['entrata_special_deposit_tax_amount'] ) ); elseif( isset( $arrValues['entrata_special_deposit_tax_amount'] ) ) $this->setEntrataSpecialDepositTaxAmount( $arrValues['entrata_special_deposit_tax_amount'] );
		if( isset( $arrValues['entrata_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialDeposit', trim( $arrValues['entrata_special_deposit'] ) ); elseif( isset( $arrValues['entrata_special_deposit'] ) ) $this->setEntrataSpecialDeposit( $arrValues['entrata_special_deposit'] );
		if( isset( $arrValues['entrata_add_on_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnDepositAmount', trim( $arrValues['entrata_add_on_deposit_amount'] ) ); elseif( isset( $arrValues['entrata_add_on_deposit_amount'] ) ) $this->setEntrataAddOnDepositAmount( $arrValues['entrata_add_on_deposit_amount'] );
		if( isset( $arrValues['entrata_add_on_deposit_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnDepositTaxAmount', trim( $arrValues['entrata_add_on_deposit_tax_amount'] ) ); elseif( isset( $arrValues['entrata_add_on_deposit_tax_amount'] ) ) $this->setEntrataAddOnDepositTaxAmount( $arrValues['entrata_add_on_deposit_tax_amount'] );
		if( isset( $arrValues['entrata_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnDeposit', trim( $arrValues['entrata_add_on_deposit'] ) ); elseif( isset( $arrValues['entrata_add_on_deposit'] ) ) $this->setEntrataAddOnDeposit( $arrValues['entrata_add_on_deposit'] );
		if( isset( $arrValues['entrata_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataTotalDeposit', trim( $arrValues['entrata_total_deposit'] ) ); elseif( isset( $arrValues['entrata_total_deposit'] ) ) $this->setEntrataTotalDeposit( $arrValues['entrata_total_deposit'] );
		if( isset( $arrValues['base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseAmountMonthly', trim( $arrValues['base_amount_monthly'] ) ); elseif( isset( $arrValues['base_amount_monthly'] ) ) $this->setBaseAmountMonthly( $arrValues['base_amount_monthly'] );
		if( isset( $arrValues['base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseTaxMonthly', trim( $arrValues['base_tax_monthly'] ) ); elseif( isset( $arrValues['base_tax_monthly'] ) ) $this->setBaseTaxMonthly( $arrValues['base_tax_monthly'] );
		if( isset( $arrValues['base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRentMonthly', trim( $arrValues['base_rent_monthly'] ) ); elseif( isset( $arrValues['base_rent_monthly'] ) ) $this->setBaseRentMonthly( $arrValues['base_rent_monthly'] );
		if( isset( $arrValues['amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityAmountMonthly', trim( $arrValues['amenity_amount_monthly'] ) ); elseif( isset( $arrValues['amenity_amount_monthly'] ) ) $this->setAmenityAmountMonthly( $arrValues['amenity_amount_monthly'] );
		if( isset( $arrValues['amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityTaxMonthly', trim( $arrValues['amenity_tax_monthly'] ) ); elseif( isset( $arrValues['amenity_tax_monthly'] ) ) $this->setAmenityTaxMonthly( $arrValues['amenity_tax_monthly'] );
		if( isset( $arrValues['amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityRentMonthly', trim( $arrValues['amenity_rent_monthly'] ) ); elseif( isset( $arrValues['amenity_rent_monthly'] ) ) $this->setAmenityRentMonthly( $arrValues['amenity_rent_monthly'] );
		if( isset( $arrValues['special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialAmountMonthly', trim( $arrValues['special_amount_monthly'] ) ); elseif( isset( $arrValues['special_amount_monthly'] ) ) $this->setSpecialAmountMonthly( $arrValues['special_amount_monthly'] );
		if( isset( $arrValues['special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialTaxMonthly', trim( $arrValues['special_tax_monthly'] ) ); elseif( isset( $arrValues['special_tax_monthly'] ) ) $this->setSpecialTaxMonthly( $arrValues['special_tax_monthly'] );
		if( isset( $arrValues['special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialRentMonthly', trim( $arrValues['special_rent_monthly'] ) ); elseif( isset( $arrValues['special_rent_monthly'] ) ) $this->setSpecialRentMonthly( $arrValues['special_rent_monthly'] );
		if( isset( $arrValues['add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnAmountMonthly', trim( $arrValues['add_on_amount_monthly'] ) ); elseif( isset( $arrValues['add_on_amount_monthly'] ) ) $this->setAddOnAmountMonthly( $arrValues['add_on_amount_monthly'] );
		if( isset( $arrValues['add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnTaxMonthly', trim( $arrValues['add_on_tax_monthly'] ) ); elseif( isset( $arrValues['add_on_tax_monthly'] ) ) $this->setAddOnTaxMonthly( $arrValues['add_on_tax_monthly'] );
		if( isset( $arrValues['add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnRentMonthly', trim( $arrValues['add_on_rent_monthly'] ) ); elseif( isset( $arrValues['add_on_rent_monthly'] ) ) $this->setAddOnRentMonthly( $arrValues['add_on_rent_monthly'] );
		if( isset( $arrValues['market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRentMonthly', trim( $arrValues['market_rent_monthly'] ) ); elseif( isset( $arrValues['market_rent_monthly'] ) ) $this->setMarketRentMonthly( $arrValues['market_rent_monthly'] );
		if( isset( $arrValues['market_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMarketAmountMonthly', trim( $arrValues['market_amount_monthly'] ) ); elseif( isset( $arrValues['market_amount_monthly'] ) ) $this->setMarketAmountMonthly( $arrValues['market_amount_monthly'] );
		if( isset( $arrValues['entrata_market_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMarketAmountMonthly', trim( $arrValues['entrata_market_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_market_amount_monthly'] ) ) $this->setEntrataMarketAmountMonthly( $arrValues['entrata_market_amount_monthly'] );
		if( isset( $arrValues['cached_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCachedDatetime', trim( $arrValues['cached_datetime'] ) ); elseif( isset( $arrValues['cached_datetime'] ) ) $this->setCachedDatetime( $arrValues['cached_datetime'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['include_tax_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeTaxInAdvertisedRent', trim( stripcslashes( $arrValues['include_tax_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_tax_in_advertised_rent'] ) ) $this->setIncludeTaxInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_tax_in_advertised_rent'] ) : $arrValues['include_tax_in_advertised_rent'] );
		if( isset( $arrValues['include_other_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeOtherInAdvertisedRent', trim( stripcslashes( $arrValues['include_other_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_other_in_advertised_rent'] ) ) $this->setIncludeOtherInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_other_in_advertised_rent'] ) : $arrValues['include_other_in_advertised_rent'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['base_other'] ) && $boolDirectSet ) $this->set( 'm_fltBaseOther', trim( $arrValues['base_other'] ) ); elseif( isset( $arrValues['base_other'] ) ) $this->setBaseOther( $arrValues['base_other'] );
		if( isset( $arrValues['amenity_other'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityOther', trim( $arrValues['amenity_other'] ) ); elseif( isset( $arrValues['amenity_other'] ) ) $this->setAmenityOther( $arrValues['amenity_other'] );
		if( isset( $arrValues['special_other'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialOther', trim( $arrValues['special_other'] ) ); elseif( isset( $arrValues['special_other'] ) ) $this->setSpecialOther( $arrValues['special_other'] );
		if( isset( $arrValues['add_on_other'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnOther', trim( $arrValues['add_on_other'] ) ); elseif( isset( $arrValues['add_on_other'] ) ) $this->setAddOnOther( $arrValues['add_on_other'] );
		if( isset( $arrValues['entrata_base_other'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataBaseOther', trim( $arrValues['entrata_base_other'] ) ); elseif( isset( $arrValues['entrata_base_other'] ) ) $this->setEntrataBaseOther( $arrValues['entrata_base_other'] );
		if( isset( $arrValues['entrata_amenity_other'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAmenityOther', trim( $arrValues['entrata_amenity_other'] ) ); elseif( isset( $arrValues['entrata_amenity_other'] ) ) $this->setEntrataAmenityOther( $arrValues['entrata_amenity_other'] );
		if( isset( $arrValues['entrata_special_other'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataSpecialOther', trim( $arrValues['entrata_special_other'] ) ); elseif( isset( $arrValues['entrata_special_other'] ) ) $this->setEntrataSpecialOther( $arrValues['entrata_special_other'] );
		if( isset( $arrValues['entrata_add_on_other'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAddOnOther', trim( $arrValues['entrata_add_on_other'] ) ); elseif( isset( $arrValues['entrata_add_on_other'] ) ) $this->setEntrataAddOnOther( $arrValues['entrata_add_on_other'] );
		if( isset( $arrValues['base_other_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltBaseOtherMonthly', trim( $arrValues['base_other_monthly'] ) ); elseif( isset( $arrValues['base_other_monthly'] ) ) $this->setBaseOtherMonthly( $arrValues['base_other_monthly'] );
		if( isset( $arrValues['amenity_other_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityOtherMonthly', trim( $arrValues['amenity_other_monthly'] ) ); elseif( isset( $arrValues['amenity_other_monthly'] ) ) $this->setAmenityOtherMonthly( $arrValues['amenity_other_monthly'] );
		if( isset( $arrValues['special_other_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialOtherMonthly', trim( $arrValues['special_other_monthly'] ) ); elseif( isset( $arrValues['special_other_monthly'] ) ) $this->setSpecialOtherMonthly( $arrValues['special_other_monthly'] );
		if( isset( $arrValues['add_on_other_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnOtherMonthly', trim( $arrValues['add_on_other_monthly'] ) ); elseif( isset( $arrValues['add_on_other_monthly'] ) ) $this->setAddOnOtherMonthly( $arrValues['add_on_other_monthly'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : '0';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : '0';
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->set( 'm_intLeaseTermMonths', CStrings::strToIntDef( $intLeaseTermMonths, NULL, false ) );
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	public function sqlLeaseTermMonths() {
		return ( true == isset( $this->m_intLeaseTermMonths ) ) ? ( string ) $this->m_intLeaseTermMonths : '0';
	}

	public function setEffectiveRange( $strEffectiveRange ) {
		$this->set( 'm_strEffectiveRange', CStrings::strTrimDef( $strEffectiveRange, NULL, NULL, true ) );
	}

	public function getEffectiveRange() {
		return $this->m_strEffectiveRange;
	}

	public function sqlEffectiveRange() {
		return ( true == isset( $this->m_strEffectiveRange ) ) ? '\'' . addslashes( $this->m_strEffectiveRange ) . '\'' : 'NULL';
	}

	public function setBaseAmount( $fltBaseAmount ) {
		$this->set( 'm_fltBaseAmount', CStrings::strToFloatDef( $fltBaseAmount, NULL, false, 2 ) );
	}

	public function getBaseAmount() {
		return $this->m_fltBaseAmount;
	}

	public function sqlBaseAmount() {
		return ( true == isset( $this->m_fltBaseAmount ) ) ? ( string ) $this->m_fltBaseAmount : 'NULL';
	}

	public function setBaseTax( $fltBaseTax ) {
		$this->set( 'm_fltBaseTax', CStrings::strToFloatDef( $fltBaseTax, NULL, false, 2 ) );
	}

	public function getBaseTax() {
		return $this->m_fltBaseTax;
	}

	public function sqlBaseTax() {
		return ( true == isset( $this->m_fltBaseTax ) ) ? ( string ) $this->m_fltBaseTax : 'NULL';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : '0';
	}

	public function setAmenityAmount( $fltAmenityAmount ) {
		$this->set( 'm_fltAmenityAmount', CStrings::strToFloatDef( $fltAmenityAmount, NULL, false, 2 ) );
	}

	public function getAmenityAmount() {
		return $this->m_fltAmenityAmount;
	}

	public function sqlAmenityAmount() {
		return ( true == isset( $this->m_fltAmenityAmount ) ) ? ( string ) $this->m_fltAmenityAmount : 'NULL';
	}

	public function setAmenityTax( $fltAmenityTax ) {
		$this->set( 'm_fltAmenityTax', CStrings::strToFloatDef( $fltAmenityTax, NULL, false, 2 ) );
	}

	public function getAmenityTax() {
		return $this->m_fltAmenityTax;
	}

	public function sqlAmenityTax() {
		return ( true == isset( $this->m_fltAmenityTax ) ) ? ( string ) $this->m_fltAmenityTax : 'NULL';
	}

	public function setAmenityRent( $fltAmenityRent ) {
		$this->set( 'm_fltAmenityRent', CStrings::strToFloatDef( $fltAmenityRent, NULL, false, 2 ) );
	}

	public function getAmenityRent() {
		return $this->m_fltAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_fltAmenityRent ) ) ? ( string ) $this->m_fltAmenityRent : '0';
	}

	public function setSpecialAmount( $fltSpecialAmount ) {
		$this->set( 'm_fltSpecialAmount', CStrings::strToFloatDef( $fltSpecialAmount, NULL, false, 2 ) );
	}

	public function getSpecialAmount() {
		return $this->m_fltSpecialAmount;
	}

	public function sqlSpecialAmount() {
		return ( true == isset( $this->m_fltSpecialAmount ) ) ? ( string ) $this->m_fltSpecialAmount : 'NULL';
	}

	public function setSpecialTax( $fltSpecialTax ) {
		$this->set( 'm_fltSpecialTax', CStrings::strToFloatDef( $fltSpecialTax, NULL, false, 2 ) );
	}

	public function getSpecialTax() {
		return $this->m_fltSpecialTax;
	}

	public function sqlSpecialTax() {
		return ( true == isset( $this->m_fltSpecialTax ) ) ? ( string ) $this->m_fltSpecialTax : 'NULL';
	}

	public function setSpecialRent( $fltSpecialRent ) {
		$this->set( 'm_fltSpecialRent', CStrings::strToFloatDef( $fltSpecialRent, NULL, false, 2 ) );
	}

	public function getSpecialRent() {
		return $this->m_fltSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_fltSpecialRent ) ) ? ( string ) $this->m_fltSpecialRent : '0';
	}

	public function setAddOnAmount( $fltAddOnAmount ) {
		$this->set( 'm_fltAddOnAmount', CStrings::strToFloatDef( $fltAddOnAmount, NULL, false, 2 ) );
	}

	public function getAddOnAmount() {
		return $this->m_fltAddOnAmount;
	}

	public function sqlAddOnAmount() {
		return ( true == isset( $this->m_fltAddOnAmount ) ) ? ( string ) $this->m_fltAddOnAmount : 'NULL';
	}

	public function setAddOnTax( $fltAddOnTax ) {
		$this->set( 'm_fltAddOnTax', CStrings::strToFloatDef( $fltAddOnTax, NULL, false, 2 ) );
	}

	public function getAddOnTax() {
		return $this->m_fltAddOnTax;
	}

	public function sqlAddOnTax() {
		return ( true == isset( $this->m_fltAddOnTax ) ) ? ( string ) $this->m_fltAddOnTax : 'NULL';
	}

	public function setAddOnRent( $fltAddOnRent ) {
		$this->set( 'm_fltAddOnRent', CStrings::strToFloatDef( $fltAddOnRent, NULL, false, 2 ) );
	}

	public function getAddOnRent() {
		return $this->m_fltAddOnRent;
	}

	public function sqlAddOnRent() {
		return ( true == isset( $this->m_fltAddOnRent ) ) ? ( string ) $this->m_fltAddOnRent : '0';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : '0';
	}

	public function setBaseDepositAmount( $fltBaseDepositAmount ) {
		$this->set( 'm_fltBaseDepositAmount', CStrings::strToFloatDef( $fltBaseDepositAmount, NULL, false, 2 ) );
	}

	public function getBaseDepositAmount() {
		return $this->m_fltBaseDepositAmount;
	}

	public function sqlBaseDepositAmount() {
		return ( true == isset( $this->m_fltBaseDepositAmount ) ) ? ( string ) $this->m_fltBaseDepositAmount : 'NULL';
	}

	public function setBaseDepositTax( $fltBaseDepositTax ) {
		$this->set( 'm_fltBaseDepositTax', CStrings::strToFloatDef( $fltBaseDepositTax, NULL, false, 2 ) );
	}

	public function getBaseDepositTax() {
		return $this->m_fltBaseDepositTax;
	}

	public function sqlBaseDepositTax() {
		return ( true == isset( $this->m_fltBaseDepositTax ) ) ? ( string ) $this->m_fltBaseDepositTax : 'NULL';
	}

	public function setBaseDeposit( $fltBaseDeposit ) {
		$this->set( 'm_fltBaseDeposit', CStrings::strToFloatDef( $fltBaseDeposit, NULL, false, 2 ) );
	}

	public function getBaseDeposit() {
		return $this->m_fltBaseDeposit;
	}

	public function sqlBaseDeposit() {
		return ( true == isset( $this->m_fltBaseDeposit ) ) ? ( string ) $this->m_fltBaseDeposit : '0';
	}

	public function setAmenityDepositAmount( $fltAmenityDepositAmount ) {
		$this->set( 'm_fltAmenityDepositAmount', CStrings::strToFloatDef( $fltAmenityDepositAmount, NULL, false, 2 ) );
	}

	public function getAmenityDepositAmount() {
		return $this->m_fltAmenityDepositAmount;
	}

	public function sqlAmenityDepositAmount() {
		return ( true == isset( $this->m_fltAmenityDepositAmount ) ) ? ( string ) $this->m_fltAmenityDepositAmount : 'NULL';
	}

	public function setAmenityDepositTax( $fltAmenityDepositTax ) {
		$this->set( 'm_fltAmenityDepositTax', CStrings::strToFloatDef( $fltAmenityDepositTax, NULL, false, 2 ) );
	}

	public function getAmenityDepositTax() {
		return $this->m_fltAmenityDepositTax;
	}

	public function sqlAmenityDepositTax() {
		return ( true == isset( $this->m_fltAmenityDepositTax ) ) ? ( string ) $this->m_fltAmenityDepositTax : 'NULL';
	}

	public function setAmenityDeposit( $fltAmenityDeposit ) {
		$this->set( 'm_fltAmenityDeposit', CStrings::strToFloatDef( $fltAmenityDeposit, NULL, false, 2 ) );
	}

	public function getAmenityDeposit() {
		return $this->m_fltAmenityDeposit;
	}

	public function sqlAmenityDeposit() {
		return ( true == isset( $this->m_fltAmenityDeposit ) ) ? ( string ) $this->m_fltAmenityDeposit : '0';
	}

	public function setSpecialDepositAmount( $fltSpecialDepositAmount ) {
		$this->set( 'm_fltSpecialDepositAmount', CStrings::strToFloatDef( $fltSpecialDepositAmount, NULL, false, 2 ) );
	}

	public function getSpecialDepositAmount() {
		return $this->m_fltSpecialDepositAmount;
	}

	public function sqlSpecialDepositAmount() {
		return ( true == isset( $this->m_fltSpecialDepositAmount ) ) ? ( string ) $this->m_fltSpecialDepositAmount : 'NULL';
	}

	public function setSpecialDepositTax( $fltSpecialDepositTax ) {
		$this->set( 'm_fltSpecialDepositTax', CStrings::strToFloatDef( $fltSpecialDepositTax, NULL, false, 2 ) );
	}

	public function getSpecialDepositTax() {
		return $this->m_fltSpecialDepositTax;
	}

	public function sqlSpecialDepositTax() {
		return ( true == isset( $this->m_fltSpecialDepositTax ) ) ? ( string ) $this->m_fltSpecialDepositTax : 'NULL';
	}

	public function setSpecialDeposit( $fltSpecialDeposit ) {
		$this->set( 'm_fltSpecialDeposit', CStrings::strToFloatDef( $fltSpecialDeposit, NULL, false, 2 ) );
	}

	public function getSpecialDeposit() {
		return $this->m_fltSpecialDeposit;
	}

	public function sqlSpecialDeposit() {
		return ( true == isset( $this->m_fltSpecialDeposit ) ) ? ( string ) $this->m_fltSpecialDeposit : '0';
	}

	public function setAddOnDepositAmount( $fltAddOnDepositAmount ) {
		$this->set( 'm_fltAddOnDepositAmount', CStrings::strToFloatDef( $fltAddOnDepositAmount, NULL, false, 2 ) );
	}

	public function getAddOnDepositAmount() {
		return $this->m_fltAddOnDepositAmount;
	}

	public function sqlAddOnDepositAmount() {
		return ( true == isset( $this->m_fltAddOnDepositAmount ) ) ? ( string ) $this->m_fltAddOnDepositAmount : 'NULL';
	}

	public function setAddOnDepositTax( $fltAddOnDepositTax ) {
		$this->set( 'm_fltAddOnDepositTax', CStrings::strToFloatDef( $fltAddOnDepositTax, NULL, false, 2 ) );
	}

	public function getAddOnDepositTax() {
		return $this->m_fltAddOnDepositTax;
	}

	public function sqlAddOnDepositTax() {
		return ( true == isset( $this->m_fltAddOnDepositTax ) ) ? ( string ) $this->m_fltAddOnDepositTax : 'NULL';
	}

	public function setAddOnDeposit( $fltAddOnDeposit ) {
		$this->set( 'm_fltAddOnDeposit', CStrings::strToFloatDef( $fltAddOnDeposit, NULL, false, 2 ) );
	}

	public function getAddOnDeposit() {
		return $this->m_fltAddOnDeposit;
	}

	public function sqlAddOnDeposit() {
		return ( true == isset( $this->m_fltAddOnDeposit ) ) ? ( string ) $this->m_fltAddOnDeposit : '0';
	}

	public function setTotalDeposit( $fltTotalDeposit ) {
		$this->set( 'm_fltTotalDeposit', CStrings::strToFloatDef( $fltTotalDeposit, NULL, false, 2 ) );
	}

	public function getTotalDeposit() {
		return $this->m_fltTotalDeposit;
	}

	public function sqlTotalDeposit() {
		return ( true == isset( $this->m_fltTotalDeposit ) ) ? ( string ) $this->m_fltTotalDeposit : '0';
	}

	public function setEntrataBaseAmount( $fltEntrataBaseAmount ) {
		$this->set( 'm_fltEntrataBaseAmount', CStrings::strToFloatDef( $fltEntrataBaseAmount, NULL, false, 2 ) );
	}

	public function getEntrataBaseAmount() {
		return $this->m_fltEntrataBaseAmount;
	}

	public function sqlEntrataBaseAmount() {
		return ( true == isset( $this->m_fltEntrataBaseAmount ) ) ? ( string ) $this->m_fltEntrataBaseAmount : 'NULL';
	}

	public function setEntrataBaseTaxAmount( $fltEntrataBaseTaxAmount ) {
		$this->set( 'm_fltEntrataBaseTaxAmount', CStrings::strToFloatDef( $fltEntrataBaseTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataBaseTaxAmount() {
		return $this->m_fltEntrataBaseTaxAmount;
	}

	public function sqlEntrataBaseTaxAmount() {
		return ( true == isset( $this->m_fltEntrataBaseTaxAmount ) ) ? ( string ) $this->m_fltEntrataBaseTaxAmount : 'NULL';
	}

	public function setEntrataBaseRent( $fltEntrataBaseRent ) {
		$this->set( 'm_fltEntrataBaseRent', CStrings::strToFloatDef( $fltEntrataBaseRent, NULL, false, 2 ) );
	}

	public function getEntrataBaseRent() {
		return $this->m_fltEntrataBaseRent;
	}

	public function sqlEntrataBaseRent() {
		return ( true == isset( $this->m_fltEntrataBaseRent ) ) ? ( string ) $this->m_fltEntrataBaseRent : 'NULL';
	}

	public function setEntrataAmenityAmount( $fltEntrataAmenityAmount ) {
		$this->set( 'm_fltEntrataAmenityAmount', CStrings::strToFloatDef( $fltEntrataAmenityAmount, NULL, false, 2 ) );
	}

	public function getEntrataAmenityAmount() {
		return $this->m_fltEntrataAmenityAmount;
	}

	public function sqlEntrataAmenityAmount() {
		return ( true == isset( $this->m_fltEntrataAmenityAmount ) ) ? ( string ) $this->m_fltEntrataAmenityAmount : 'NULL';
	}

	public function setEntrataAmenityTaxAmount( $fltEntrataAmenityTaxAmount ) {
		$this->set( 'm_fltEntrataAmenityTaxAmount', CStrings::strToFloatDef( $fltEntrataAmenityTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataAmenityTaxAmount() {
		return $this->m_fltEntrataAmenityTaxAmount;
	}

	public function sqlEntrataAmenityTaxAmount() {
		return ( true == isset( $this->m_fltEntrataAmenityTaxAmount ) ) ? ( string ) $this->m_fltEntrataAmenityTaxAmount : 'NULL';
	}

	public function setEntrataAmenityRent( $fltEntrataAmenityRent ) {
		$this->set( 'm_fltEntrataAmenityRent', CStrings::strToFloatDef( $fltEntrataAmenityRent, NULL, false, 2 ) );
	}

	public function getEntrataAmenityRent() {
		return $this->m_fltEntrataAmenityRent;
	}

	public function sqlEntrataAmenityRent() {
		return ( true == isset( $this->m_fltEntrataAmenityRent ) ) ? ( string ) $this->m_fltEntrataAmenityRent : 'NULL';
	}

	public function setEntrataSpecialAmount( $fltEntrataSpecialAmount ) {
		$this->set( 'm_fltEntrataSpecialAmount', CStrings::strToFloatDef( $fltEntrataSpecialAmount, NULL, false, 2 ) );
	}

	public function getEntrataSpecialAmount() {
		return $this->m_fltEntrataSpecialAmount;
	}

	public function sqlEntrataSpecialAmount() {
		return ( true == isset( $this->m_fltEntrataSpecialAmount ) ) ? ( string ) $this->m_fltEntrataSpecialAmount : 'NULL';
	}

	public function setEntrataSpecialTaxAmount( $fltEntrataSpecialTaxAmount ) {
		$this->set( 'm_fltEntrataSpecialTaxAmount', CStrings::strToFloatDef( $fltEntrataSpecialTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataSpecialTaxAmount() {
		return $this->m_fltEntrataSpecialTaxAmount;
	}

	public function sqlEntrataSpecialTaxAmount() {
		return ( true == isset( $this->m_fltEntrataSpecialTaxAmount ) ) ? ( string ) $this->m_fltEntrataSpecialTaxAmount : 'NULL';
	}

	public function setEntrataSpecialRent( $fltEntrataSpecialRent ) {
		$this->set( 'm_fltEntrataSpecialRent', CStrings::strToFloatDef( $fltEntrataSpecialRent, NULL, false, 2 ) );
	}

	public function getEntrataSpecialRent() {
		return $this->m_fltEntrataSpecialRent;
	}

	public function sqlEntrataSpecialRent() {
		return ( true == isset( $this->m_fltEntrataSpecialRent ) ) ? ( string ) $this->m_fltEntrataSpecialRent : 'NULL';
	}

	public function setEntrataAddOnAmount( $fltEntrataAddOnAmount ) {
		$this->set( 'm_fltEntrataAddOnAmount', CStrings::strToFloatDef( $fltEntrataAddOnAmount, NULL, false, 2 ) );
	}

	public function getEntrataAddOnAmount() {
		return $this->m_fltEntrataAddOnAmount;
	}

	public function sqlEntrataAddOnAmount() {
		return ( true == isset( $this->m_fltEntrataAddOnAmount ) ) ? ( string ) $this->m_fltEntrataAddOnAmount : 'NULL';
	}

	public function setEntrataAddOnTaxAmount( $fltEntrataAddOnTaxAmount ) {
		$this->set( 'm_fltEntrataAddOnTaxAmount', CStrings::strToFloatDef( $fltEntrataAddOnTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataAddOnTaxAmount() {
		return $this->m_fltEntrataAddOnTaxAmount;
	}

	public function sqlEntrataAddOnTaxAmount() {
		return ( true == isset( $this->m_fltEntrataAddOnTaxAmount ) ) ? ( string ) $this->m_fltEntrataAddOnTaxAmount : 'NULL';
	}

	public function setEntrataAddOnRent( $fltEntrataAddOnRent ) {
		$this->set( 'm_fltEntrataAddOnRent', CStrings::strToFloatDef( $fltEntrataAddOnRent, NULL, false, 2 ) );
	}

	public function getEntrataAddOnRent() {
		return $this->m_fltEntrataAddOnRent;
	}

	public function sqlEntrataAddOnRent() {
		return ( true == isset( $this->m_fltEntrataAddOnRent ) ) ? ( string ) $this->m_fltEntrataAddOnRent : 'NULL';
	}

	public function setEntrataMarketRent( $fltEntrataMarketRent ) {
		$this->set( 'm_fltEntrataMarketRent', CStrings::strToFloatDef( $fltEntrataMarketRent, NULL, false, 2 ) );
	}

	public function getEntrataMarketRent() {
		return $this->m_fltEntrataMarketRent;
	}

	public function sqlEntrataMarketRent() {
		return ( true == isset( $this->m_fltEntrataMarketRent ) ) ? ( string ) $this->m_fltEntrataMarketRent : 'NULL';
	}

	public function setEntrataBaseDepositAmount( $fltEntrataBaseDepositAmount ) {
		$this->set( 'm_fltEntrataBaseDepositAmount', CStrings::strToFloatDef( $fltEntrataBaseDepositAmount, NULL, false, 2 ) );
	}

	public function getEntrataBaseDepositAmount() {
		return $this->m_fltEntrataBaseDepositAmount;
	}

	public function sqlEntrataBaseDepositAmount() {
		return ( true == isset( $this->m_fltEntrataBaseDepositAmount ) ) ? ( string ) $this->m_fltEntrataBaseDepositAmount : 'NULL';
	}

	public function setEntrataBaseDepositTaxAmount( $fltEntrataBaseDepositTaxAmount ) {
		$this->set( 'm_fltEntrataBaseDepositTaxAmount', CStrings::strToFloatDef( $fltEntrataBaseDepositTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataBaseDepositTaxAmount() {
		return $this->m_fltEntrataBaseDepositTaxAmount;
	}

	public function sqlEntrataBaseDepositTaxAmount() {
		return ( true == isset( $this->m_fltEntrataBaseDepositTaxAmount ) ) ? ( string ) $this->m_fltEntrataBaseDepositTaxAmount : 'NULL';
	}

	public function setEntrataBaseDeposit( $fltEntrataBaseDeposit ) {
		$this->set( 'm_fltEntrataBaseDeposit', CStrings::strToFloatDef( $fltEntrataBaseDeposit, NULL, false, 2 ) );
	}

	public function getEntrataBaseDeposit() {
		return $this->m_fltEntrataBaseDeposit;
	}

	public function sqlEntrataBaseDeposit() {
		return ( true == isset( $this->m_fltEntrataBaseDeposit ) ) ? ( string ) $this->m_fltEntrataBaseDeposit : 'NULL';
	}

	public function setEntrataAmenityDepositAmount( $fltEntrataAmenityDepositAmount ) {
		$this->set( 'm_fltEntrataAmenityDepositAmount', CStrings::strToFloatDef( $fltEntrataAmenityDepositAmount, NULL, false, 2 ) );
	}

	public function getEntrataAmenityDepositAmount() {
		return $this->m_fltEntrataAmenityDepositAmount;
	}

	public function sqlEntrataAmenityDepositAmount() {
		return ( true == isset( $this->m_fltEntrataAmenityDepositAmount ) ) ? ( string ) $this->m_fltEntrataAmenityDepositAmount : 'NULL';
	}

	public function setEntrataAmenityDepositTaxAmount( $fltEntrataAmenityDepositTaxAmount ) {
		$this->set( 'm_fltEntrataAmenityDepositTaxAmount', CStrings::strToFloatDef( $fltEntrataAmenityDepositTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataAmenityDepositTaxAmount() {
		return $this->m_fltEntrataAmenityDepositTaxAmount;
	}

	public function sqlEntrataAmenityDepositTaxAmount() {
		return ( true == isset( $this->m_fltEntrataAmenityDepositTaxAmount ) ) ? ( string ) $this->m_fltEntrataAmenityDepositTaxAmount : 'NULL';
	}

	public function setEntrataAmenityDeposit( $fltEntrataAmenityDeposit ) {
		$this->set( 'm_fltEntrataAmenityDeposit', CStrings::strToFloatDef( $fltEntrataAmenityDeposit, NULL, false, 2 ) );
	}

	public function getEntrataAmenityDeposit() {
		return $this->m_fltEntrataAmenityDeposit;
	}

	public function sqlEntrataAmenityDeposit() {
		return ( true == isset( $this->m_fltEntrataAmenityDeposit ) ) ? ( string ) $this->m_fltEntrataAmenityDeposit : 'NULL';
	}

	public function setEntrataSpecialDepositAmount( $fltEntrataSpecialDepositAmount ) {
		$this->set( 'm_fltEntrataSpecialDepositAmount', CStrings::strToFloatDef( $fltEntrataSpecialDepositAmount, NULL, false, 2 ) );
	}

	public function getEntrataSpecialDepositAmount() {
		return $this->m_fltEntrataSpecialDepositAmount;
	}

	public function sqlEntrataSpecialDepositAmount() {
		return ( true == isset( $this->m_fltEntrataSpecialDepositAmount ) ) ? ( string ) $this->m_fltEntrataSpecialDepositAmount : 'NULL';
	}

	public function setEntrataSpecialDepositTaxAmount( $fltEntrataSpecialDepositTaxAmount ) {
		$this->set( 'm_fltEntrataSpecialDepositTaxAmount', CStrings::strToFloatDef( $fltEntrataSpecialDepositTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataSpecialDepositTaxAmount() {
		return $this->m_fltEntrataSpecialDepositTaxAmount;
	}

	public function sqlEntrataSpecialDepositTaxAmount() {
		return ( true == isset( $this->m_fltEntrataSpecialDepositTaxAmount ) ) ? ( string ) $this->m_fltEntrataSpecialDepositTaxAmount : 'NULL';
	}

	public function setEntrataSpecialDeposit( $fltEntrataSpecialDeposit ) {
		$this->set( 'm_fltEntrataSpecialDeposit', CStrings::strToFloatDef( $fltEntrataSpecialDeposit, NULL, false, 2 ) );
	}

	public function getEntrataSpecialDeposit() {
		return $this->m_fltEntrataSpecialDeposit;
	}

	public function sqlEntrataSpecialDeposit() {
		return ( true == isset( $this->m_fltEntrataSpecialDeposit ) ) ? ( string ) $this->m_fltEntrataSpecialDeposit : 'NULL';
	}

	public function setEntrataAddOnDepositAmount( $fltEntrataAddOnDepositAmount ) {
		$this->set( 'm_fltEntrataAddOnDepositAmount', CStrings::strToFloatDef( $fltEntrataAddOnDepositAmount, NULL, false, 2 ) );
	}

	public function getEntrataAddOnDepositAmount() {
		return $this->m_fltEntrataAddOnDepositAmount;
	}

	public function sqlEntrataAddOnDepositAmount() {
		return ( true == isset( $this->m_fltEntrataAddOnDepositAmount ) ) ? ( string ) $this->m_fltEntrataAddOnDepositAmount : 'NULL';
	}

	public function setEntrataAddOnDepositTaxAmount( $fltEntrataAddOnDepositTaxAmount ) {
		$this->set( 'm_fltEntrataAddOnDepositTaxAmount', CStrings::strToFloatDef( $fltEntrataAddOnDepositTaxAmount, NULL, false, 2 ) );
	}

	public function getEntrataAddOnDepositTaxAmount() {
		return $this->m_fltEntrataAddOnDepositTaxAmount;
	}

	public function sqlEntrataAddOnDepositTaxAmount() {
		return ( true == isset( $this->m_fltEntrataAddOnDepositTaxAmount ) ) ? ( string ) $this->m_fltEntrataAddOnDepositTaxAmount : 'NULL';
	}

	public function setEntrataAddOnDeposit( $fltEntrataAddOnDeposit ) {
		$this->set( 'm_fltEntrataAddOnDeposit', CStrings::strToFloatDef( $fltEntrataAddOnDeposit, NULL, false, 2 ) );
	}

	public function getEntrataAddOnDeposit() {
		return $this->m_fltEntrataAddOnDeposit;
	}

	public function sqlEntrataAddOnDeposit() {
		return ( true == isset( $this->m_fltEntrataAddOnDeposit ) ) ? ( string ) $this->m_fltEntrataAddOnDeposit : 'NULL';
	}

	public function setEntrataTotalDeposit( $fltEntrataTotalDeposit ) {
		$this->set( 'm_fltEntrataTotalDeposit', CStrings::strToFloatDef( $fltEntrataTotalDeposit, NULL, false, 2 ) );
	}

	public function getEntrataTotalDeposit() {
		return $this->m_fltEntrataTotalDeposit;
	}

	public function sqlEntrataTotalDeposit() {
		return ( true == isset( $this->m_fltEntrataTotalDeposit ) ) ? ( string ) $this->m_fltEntrataTotalDeposit : 'NULL';
	}

	public function setBaseAmountMonthly( $fltBaseAmountMonthly ) {
		$this->set( 'm_fltBaseAmountMonthly', CStrings::strToFloatDef( $fltBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getBaseAmountMonthly() {
		return $this->m_fltBaseAmountMonthly;
	}

	public function sqlBaseAmountMonthly() {
		return ( true == isset( $this->m_fltBaseAmountMonthly ) ) ? ( string ) $this->m_fltBaseAmountMonthly : 'NULL';
	}

	public function setBaseTaxMonthly( $fltBaseTaxMonthly ) {
		$this->set( 'm_fltBaseTaxMonthly', CStrings::strToFloatDef( $fltBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getBaseTaxMonthly() {
		return $this->m_fltBaseTaxMonthly;
	}

	public function sqlBaseTaxMonthly() {
		return ( true == isset( $this->m_fltBaseTaxMonthly ) ) ? ( string ) $this->m_fltBaseTaxMonthly : 'NULL';
	}

	public function setBaseRentMonthly( $fltBaseRentMonthly ) {
		$this->set( 'm_fltBaseRentMonthly', CStrings::strToFloatDef( $fltBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getBaseRentMonthly() {
		return $this->m_fltBaseRentMonthly;
	}

	public function sqlBaseRentMonthly() {
		return ( true == isset( $this->m_fltBaseRentMonthly ) ) ? ( string ) $this->m_fltBaseRentMonthly : 'NULL';
	}

	public function setAmenityAmountMonthly( $fltAmenityAmountMonthly ) {
		$this->set( 'm_fltAmenityAmountMonthly', CStrings::strToFloatDef( $fltAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getAmenityAmountMonthly() {
		return $this->m_fltAmenityAmountMonthly;
	}

	public function sqlAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltAmenityAmountMonthly ) ) ? ( string ) $this->m_fltAmenityAmountMonthly : 'NULL';
	}

	public function setAmenityTaxMonthly( $fltAmenityTaxMonthly ) {
		$this->set( 'm_fltAmenityTaxMonthly', CStrings::strToFloatDef( $fltAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getAmenityTaxMonthly() {
		return $this->m_fltAmenityTaxMonthly;
	}

	public function sqlAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltAmenityTaxMonthly ) ) ? ( string ) $this->m_fltAmenityTaxMonthly : 'NULL';
	}

	public function setAmenityRentMonthly( $fltAmenityRentMonthly ) {
		$this->set( 'm_fltAmenityRentMonthly', CStrings::strToFloatDef( $fltAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getAmenityRentMonthly() {
		return $this->m_fltAmenityRentMonthly;
	}

	public function sqlAmenityRentMonthly() {
		return ( true == isset( $this->m_fltAmenityRentMonthly ) ) ? ( string ) $this->m_fltAmenityRentMonthly : 'NULL';
	}

	public function setSpecialAmountMonthly( $fltSpecialAmountMonthly ) {
		$this->set( 'm_fltSpecialAmountMonthly', CStrings::strToFloatDef( $fltSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getSpecialAmountMonthly() {
		return $this->m_fltSpecialAmountMonthly;
	}

	public function sqlSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltSpecialAmountMonthly ) ) ? ( string ) $this->m_fltSpecialAmountMonthly : 'NULL';
	}

	public function setSpecialTaxMonthly( $fltSpecialTaxMonthly ) {
		$this->set( 'm_fltSpecialTaxMonthly', CStrings::strToFloatDef( $fltSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getSpecialTaxMonthly() {
		return $this->m_fltSpecialTaxMonthly;
	}

	public function sqlSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltSpecialTaxMonthly ) ) ? ( string ) $this->m_fltSpecialTaxMonthly : 'NULL';
	}

	public function setSpecialRentMonthly( $fltSpecialRentMonthly ) {
		$this->set( 'm_fltSpecialRentMonthly', CStrings::strToFloatDef( $fltSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getSpecialRentMonthly() {
		return $this->m_fltSpecialRentMonthly;
	}

	public function sqlSpecialRentMonthly() {
		return ( true == isset( $this->m_fltSpecialRentMonthly ) ) ? ( string ) $this->m_fltSpecialRentMonthly : 'NULL';
	}

	public function setAddOnAmountMonthly( $fltAddOnAmountMonthly ) {
		$this->set( 'm_fltAddOnAmountMonthly', CStrings::strToFloatDef( $fltAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getAddOnAmountMonthly() {
		return $this->m_fltAddOnAmountMonthly;
	}

	public function sqlAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltAddOnAmountMonthly ) ) ? ( string ) $this->m_fltAddOnAmountMonthly : 'NULL';
	}

	public function setAddOnTaxMonthly( $fltAddOnTaxMonthly ) {
		$this->set( 'm_fltAddOnTaxMonthly', CStrings::strToFloatDef( $fltAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getAddOnTaxMonthly() {
		return $this->m_fltAddOnTaxMonthly;
	}

	public function sqlAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltAddOnTaxMonthly ) ) ? ( string ) $this->m_fltAddOnTaxMonthly : 'NULL';
	}

	public function setAddOnRentMonthly( $fltAddOnRentMonthly ) {
		$this->set( 'm_fltAddOnRentMonthly', CStrings::strToFloatDef( $fltAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getAddOnRentMonthly() {
		return $this->m_fltAddOnRentMonthly;
	}

	public function sqlAddOnRentMonthly() {
		return ( true == isset( $this->m_fltAddOnRentMonthly ) ) ? ( string ) $this->m_fltAddOnRentMonthly : 'NULL';
	}

	public function setMarketRentMonthly( $fltMarketRentMonthly ) {
		$this->set( 'm_fltMarketRentMonthly', CStrings::strToFloatDef( $fltMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getMarketRentMonthly() {
		return $this->m_fltMarketRentMonthly;
	}

	public function sqlMarketRentMonthly() {
		return ( true == isset( $this->m_fltMarketRentMonthly ) ) ? ( string ) $this->m_fltMarketRentMonthly : 'NULL';
	}

	public function setMarketAmountMonthly( $fltMarketAmountMonthly ) {
		$this->set( 'm_fltMarketAmountMonthly', CStrings::strToFloatDef( $fltMarketAmountMonthly, NULL, false, 2 ) );
	}

	public function getMarketAmountMonthly() {
		return $this->m_fltMarketAmountMonthly;
	}

	public function sqlMarketAmountMonthly() {
		return ( true == isset( $this->m_fltMarketAmountMonthly ) ) ? ( string ) $this->m_fltMarketAmountMonthly : 'NULL';
	}

	public function setEntrataMarketAmountMonthly( $fltEntrataMarketAmountMonthly ) {
		$this->set( 'm_fltEntrataMarketAmountMonthly', CStrings::strToFloatDef( $fltEntrataMarketAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMarketAmountMonthly() {
		return $this->m_fltEntrataMarketAmountMonthly;
	}

	public function sqlEntrataMarketAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMarketAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMarketAmountMonthly : 'NULL';
	}

	public function setCachedDatetime( $strCachedDatetime ) {
		$this->set( 'm_strCachedDatetime', CStrings::strTrimDef( $strCachedDatetime, -1, NULL, true ) );
	}

	public function getCachedDatetime() {
		return $this->m_strCachedDatetime;
	}

	public function sqlCachedDatetime() {
		return ( true == isset( $this->m_strCachedDatetime ) ) ? '\'' . $this->m_strCachedDatetime . '\'' : 'NOW()';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeTaxInAdvertisedRent( $boolIncludeTaxInAdvertisedRent ) {
		$this->set( 'm_boolIncludeTaxInAdvertisedRent', CStrings::strToBool( $boolIncludeTaxInAdvertisedRent ) );
	}

	public function getIncludeTaxInAdvertisedRent() {
		return $this->m_boolIncludeTaxInAdvertisedRent;
	}

	public function sqlIncludeTaxInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeTaxInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeTaxInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeOtherInAdvertisedRent( $boolIncludeOtherInAdvertisedRent ) {
		$this->set( 'm_boolIncludeOtherInAdvertisedRent', CStrings::strToBool( $boolIncludeOtherInAdvertisedRent ) );
	}

	public function getIncludeOtherInAdvertisedRent() {
		return $this->m_boolIncludeOtherInAdvertisedRent;
	}

	public function sqlIncludeOtherInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeOtherInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeOtherInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setBaseOther( $fltBaseOther ) {
		$this->set( 'm_fltBaseOther', CStrings::strToFloatDef( $fltBaseOther, NULL, false, 2 ) );
	}

	public function getBaseOther() {
		return $this->m_fltBaseOther;
	}

	public function sqlBaseOther() {
		return ( true == isset( $this->m_fltBaseOther ) ) ? ( string ) $this->m_fltBaseOther : 'NULL';
	}

	public function setAmenityOther( $fltAmenityOther ) {
		$this->set( 'm_fltAmenityOther', CStrings::strToFloatDef( $fltAmenityOther, NULL, false, 2 ) );
	}

	public function getAmenityOther() {
		return $this->m_fltAmenityOther;
	}

	public function sqlAmenityOther() {
		return ( true == isset( $this->m_fltAmenityOther ) ) ? ( string ) $this->m_fltAmenityOther : 'NULL';
	}

	public function setSpecialOther( $fltSpecialOther ) {
		$this->set( 'm_fltSpecialOther', CStrings::strToFloatDef( $fltSpecialOther, NULL, false, 2 ) );
	}

	public function getSpecialOther() {
		return $this->m_fltSpecialOther;
	}

	public function sqlSpecialOther() {
		return ( true == isset( $this->m_fltSpecialOther ) ) ? ( string ) $this->m_fltSpecialOther : 'NULL';
	}

	public function setAddOnOther( $fltAddOnOther ) {
		$this->set( 'm_fltAddOnOther', CStrings::strToFloatDef( $fltAddOnOther, NULL, false, 2 ) );
	}

	public function getAddOnOther() {
		return $this->m_fltAddOnOther;
	}

	public function sqlAddOnOther() {
		return ( true == isset( $this->m_fltAddOnOther ) ) ? ( string ) $this->m_fltAddOnOther : 'NULL';
	}

	public function setEntrataBaseOther( $fltEntrataBaseOther ) {
		$this->set( 'm_fltEntrataBaseOther', CStrings::strToFloatDef( $fltEntrataBaseOther, NULL, false, 2 ) );
	}

	public function getEntrataBaseOther() {
		return $this->m_fltEntrataBaseOther;
	}

	public function sqlEntrataBaseOther() {
		return ( true == isset( $this->m_fltEntrataBaseOther ) ) ? ( string ) $this->m_fltEntrataBaseOther : 'NULL';
	}

	public function setEntrataAmenityOther( $fltEntrataAmenityOther ) {
		$this->set( 'm_fltEntrataAmenityOther', CStrings::strToFloatDef( $fltEntrataAmenityOther, NULL, false, 2 ) );
	}

	public function getEntrataAmenityOther() {
		return $this->m_fltEntrataAmenityOther;
	}

	public function sqlEntrataAmenityOther() {
		return ( true == isset( $this->m_fltEntrataAmenityOther ) ) ? ( string ) $this->m_fltEntrataAmenityOther : 'NULL';
	}

	public function setEntrataSpecialOther( $fltEntrataSpecialOther ) {
		$this->set( 'm_fltEntrataSpecialOther', CStrings::strToFloatDef( $fltEntrataSpecialOther, NULL, false, 2 ) );
	}

	public function getEntrataSpecialOther() {
		return $this->m_fltEntrataSpecialOther;
	}

	public function sqlEntrataSpecialOther() {
		return ( true == isset( $this->m_fltEntrataSpecialOther ) ) ? ( string ) $this->m_fltEntrataSpecialOther : 'NULL';
	}

	public function setEntrataAddOnOther( $fltEntrataAddOnOther ) {
		$this->set( 'm_fltEntrataAddOnOther', CStrings::strToFloatDef( $fltEntrataAddOnOther, NULL, false, 2 ) );
	}

	public function getEntrataAddOnOther() {
		return $this->m_fltEntrataAddOnOther;
	}

	public function sqlEntrataAddOnOther() {
		return ( true == isset( $this->m_fltEntrataAddOnOther ) ) ? ( string ) $this->m_fltEntrataAddOnOther : 'NULL';
	}

	public function setBaseOtherMonthly( $fltBaseOtherMonthly ) {
		$this->set( 'm_fltBaseOtherMonthly', CStrings::strToFloatDef( $fltBaseOtherMonthly, NULL, false, 2 ) );
	}

	public function getBaseOtherMonthly() {
		return $this->m_fltBaseOtherMonthly;
	}

	public function sqlBaseOtherMonthly() {
		return ( true == isset( $this->m_fltBaseOtherMonthly ) ) ? ( string ) $this->m_fltBaseOtherMonthly : 'NULL';
	}

	public function setAmenityOtherMonthly( $fltAmenityOtherMonthly ) {
		$this->set( 'm_fltAmenityOtherMonthly', CStrings::strToFloatDef( $fltAmenityOtherMonthly, NULL, false, 2 ) );
	}

	public function getAmenityOtherMonthly() {
		return $this->m_fltAmenityOtherMonthly;
	}

	public function sqlAmenityOtherMonthly() {
		return ( true == isset( $this->m_fltAmenityOtherMonthly ) ) ? ( string ) $this->m_fltAmenityOtherMonthly : 'NULL';
	}

	public function setSpecialOtherMonthly( $fltSpecialOtherMonthly ) {
		$this->set( 'm_fltSpecialOtherMonthly', CStrings::strToFloatDef( $fltSpecialOtherMonthly, NULL, false, 2 ) );
	}

	public function getSpecialOtherMonthly() {
		return $this->m_fltSpecialOtherMonthly;
	}

	public function sqlSpecialOtherMonthly() {
		return ( true == isset( $this->m_fltSpecialOtherMonthly ) ) ? ( string ) $this->m_fltSpecialOtherMonthly : 'NULL';
	}

	public function setAddOnOtherMonthly( $fltAddOnOtherMonthly ) {
		$this->set( 'm_fltAddOnOtherMonthly', CStrings::strToFloatDef( $fltAddOnOtherMonthly, NULL, false, 2 ) );
	}

	public function getAddOnOtherMonthly() {
		return $this->m_fltAddOnOtherMonthly;
	}

	public function sqlAddOnOtherMonthly() {
		return ( true == isset( $this->m_fltAddOnOtherMonthly ) ) ? ( string ) $this->m_fltAddOnOtherMonthly : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
					' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, property_unit_id, unit_space_id, occupancy_type_id, ar_trigger_id, lease_term_id, lease_start_window_id, space_configuration_id, lease_term_months, effective_range, base_amount, base_tax, base_rent, amenity_amount, amenity_tax, amenity_rent, special_amount, special_tax, special_rent, add_on_amount, add_on_tax, add_on_rent, market_rent, base_deposit_amount, base_deposit_tax, base_deposit, amenity_deposit_amount, amenity_deposit_tax, amenity_deposit, special_deposit_amount, special_deposit_tax, special_deposit, add_on_deposit_amount, add_on_deposit_tax, add_on_deposit, total_deposit, entrata_base_amount, entrata_base_tax_amount, entrata_base_rent, entrata_amenity_amount, entrata_amenity_tax_amount, entrata_amenity_rent, entrata_special_amount, entrata_special_tax_amount, entrata_special_rent, entrata_add_on_amount, entrata_add_on_tax_amount, entrata_add_on_rent, entrata_market_rent, entrata_base_deposit_amount, entrata_base_deposit_tax_amount, entrata_base_deposit, entrata_amenity_deposit_amount, entrata_amenity_deposit_tax_amount, entrata_amenity_deposit, entrata_special_deposit_amount, entrata_special_deposit_tax_amount, entrata_special_deposit, entrata_add_on_deposit_amount, entrata_add_on_deposit_tax_amount, entrata_add_on_deposit, entrata_total_deposit, base_amount_monthly, base_tax_monthly, base_rent_monthly, amenity_amount_monthly, amenity_tax_monthly, amenity_rent_monthly, special_amount_monthly, special_tax_monthly, special_rent_monthly, add_on_amount_monthly, add_on_tax_monthly, add_on_rent_monthly, market_rent_monthly, market_amount_monthly, entrata_market_amount_monthly, cached_datetime, show_on_website, include_tax_in_advertised_rent, include_other_in_advertised_rent, created_by, created_on, details, base_other, amenity_other, special_other, add_on_other, entrata_base_other, entrata_amenity_other, entrata_special_other, entrata_add_on_other, base_other_monthly, amenity_other_monthly, special_other_monthly, add_on_other_monthly )
				VALUES ( ' .
				  $strId . ', ' .
				  $this->sqlCid() . ', ' .
				  $this->sqlPropertyId() . ', ' .
				  $this->sqlPropertyFloorplanId() . ', ' .
				  $this->sqlUnitTypeId() . ', ' .
				  $this->sqlPropertyUnitId() . ', ' .
				  $this->sqlUnitSpaceId() . ', ' .
				  $this->sqlOccupancyTypeId() . ', ' .
				  $this->sqlArTriggerId() . ', ' .
				  $this->sqlLeaseTermId() . ', ' .
				  $this->sqlLeaseStartWindowId() . ', ' .
				  $this->sqlSpaceConfigurationId() . ', ' .
				  $this->sqlLeaseTermMonths() . ', ' .
				  $this->sqlEffectiveRange() . ', ' .
				  $this->sqlBaseAmount() . ', ' .
				  $this->sqlBaseTax() . ', ' .
				  $this->sqlBaseRent() . ', ' .
				  $this->sqlAmenityAmount() . ', ' .
				  $this->sqlAmenityTax() . ', ' .
				  $this->sqlAmenityRent() . ', ' .
				  $this->sqlSpecialAmount() . ', ' .
				  $this->sqlSpecialTax() . ', ' .
				  $this->sqlSpecialRent() . ', ' .
				  $this->sqlAddOnAmount() . ', ' .
				  $this->sqlAddOnTax() . ', ' .
				  $this->sqlAddOnRent() . ', ' .
				  $this->sqlMarketRent() . ', ' .
				  $this->sqlBaseDepositAmount() . ', ' .
				  $this->sqlBaseDepositTax() . ', ' .
				  $this->sqlBaseDeposit() . ', ' .
				  $this->sqlAmenityDepositAmount() . ', ' .
				  $this->sqlAmenityDepositTax() . ', ' .
				  $this->sqlAmenityDeposit() . ', ' .
				  $this->sqlSpecialDepositAmount() . ', ' .
				  $this->sqlSpecialDepositTax() . ', ' .
				  $this->sqlSpecialDeposit() . ', ' .
				  $this->sqlAddOnDepositAmount() . ', ' .
				  $this->sqlAddOnDepositTax() . ', ' .
				  $this->sqlAddOnDeposit() . ', ' .
				  $this->sqlTotalDeposit() . ', ' .
				  $this->sqlEntrataBaseAmount() . ', ' .
				  $this->sqlEntrataBaseTaxAmount() . ', ' .
				  $this->sqlEntrataBaseRent() . ', ' .
				  $this->sqlEntrataAmenityAmount() . ', ' .
				  $this->sqlEntrataAmenityTaxAmount() . ', ' .
				  $this->sqlEntrataAmenityRent() . ', ' .
				  $this->sqlEntrataSpecialAmount() . ', ' .
				  $this->sqlEntrataSpecialTaxAmount() . ', ' .
				  $this->sqlEntrataSpecialRent() . ', ' .
				  $this->sqlEntrataAddOnAmount() . ', ' .
				  $this->sqlEntrataAddOnTaxAmount() . ', ' .
				  $this->sqlEntrataAddOnRent() . ', ' .
				  $this->sqlEntrataMarketRent() . ', ' .
				  $this->sqlEntrataBaseDepositAmount() . ', ' .
				  $this->sqlEntrataBaseDepositTaxAmount() . ', ' .
				  $this->sqlEntrataBaseDeposit() . ', ' .
				  $this->sqlEntrataAmenityDepositAmount() . ', ' .
				  $this->sqlEntrataAmenityDepositTaxAmount() . ', ' .
				  $this->sqlEntrataAmenityDeposit() . ', ' .
				  $this->sqlEntrataSpecialDepositAmount() . ', ' .
				  $this->sqlEntrataSpecialDepositTaxAmount() . ', ' .
				  $this->sqlEntrataSpecialDeposit() . ', ' .
				  $this->sqlEntrataAddOnDepositAmount() . ', ' .
				  $this->sqlEntrataAddOnDepositTaxAmount() . ', ' .
				  $this->sqlEntrataAddOnDeposit() . ', ' .
				  $this->sqlEntrataTotalDeposit() . ', ' .
				  $this->sqlBaseAmountMonthly() . ', ' .
				  $this->sqlBaseTaxMonthly() . ', ' .
				  $this->sqlBaseRentMonthly() . ', ' .
				  $this->sqlAmenityAmountMonthly() . ', ' .
				  $this->sqlAmenityTaxMonthly() . ', ' .
				  $this->sqlAmenityRentMonthly() . ', ' .
				  $this->sqlSpecialAmountMonthly() . ', ' .
				  $this->sqlSpecialTaxMonthly() . ', ' .
				  $this->sqlSpecialRentMonthly() . ', ' .
				  $this->sqlAddOnAmountMonthly() . ', ' .
				  $this->sqlAddOnTaxMonthly() . ', ' .
				  $this->sqlAddOnRentMonthly() . ', ' .
				  $this->sqlMarketRentMonthly() . ', ' .
				  $this->sqlMarketAmountMonthly() . ', ' .
				  $this->sqlEntrataMarketAmountMonthly() . ', ' .
				  $this->sqlCachedDatetime() . ', ' .
				  $this->sqlShowOnWebsite() . ', ' .
				  $this->sqlIncludeTaxInAdvertisedRent() . ', ' .
				  $this->sqlIncludeOtherInAdvertisedRent() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlCreatedOn() . ', ' .
				  $this->sqlDetails() . ', ' .
				  $this->sqlBaseOther() . ', ' .
				  $this->sqlAmenityOther() . ', ' .
				  $this->sqlSpecialOther() . ', ' .
				  $this->sqlAddOnOther() . ', ' .
				  $this->sqlEntrataBaseOther() . ', ' .
				  $this->sqlEntrataAmenityOther() . ', ' .
				  $this->sqlEntrataSpecialOther() . ', ' .
				  $this->sqlEntrataAddOnOther() . ', ' .
				  $this->sqlBaseOtherMonthly() . ', ' .
				  $this->sqlAmenityOtherMonthly() . ', ' .
				  $this->sqlSpecialOtherMonthly() . ', ' .
				  $this->sqlAddOnOtherMonthly() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
					' . static::TABLE_NAME . '
				SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths(). ',' ; } elseif( true == array_key_exists( 'LeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange(). ',' ; } elseif( true == array_key_exists( 'EffectiveRange', $this->getChangedColumns() ) ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_amount = ' . $this->sqlBaseAmount(). ',' ; } elseif( true == array_key_exists( 'BaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' base_amount = ' . $this->sqlBaseAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax(). ',' ; } elseif( true == array_key_exists( 'BaseTax', $this->getChangedColumns() ) ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'AmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_tax = ' . $this->sqlAmenityTax(). ',' ; } elseif( true == array_key_exists( 'AmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' amenity_tax = ' . $this->sqlAmenityTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_amount = ' . $this->sqlSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'SpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' special_amount = ' . $this->sqlSpecialAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_tax = ' . $this->sqlSpecialTax(). ',' ; } elseif( true == array_key_exists( 'SpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' special_tax = ' . $this->sqlSpecialTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent(). ',' ; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_amount = ' . $this->sqlAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'AddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' add_on_amount = ' . $this->sqlAddOnAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_tax = ' . $this->sqlAddOnTax(). ',' ; } elseif( true == array_key_exists( 'AddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' add_on_tax = ' . $this->sqlAddOnTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent(). ',' ; } elseif( true == array_key_exists( 'AddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent(). ',' ; } elseif( true == array_key_exists( 'MarketRent', $this->getChangedColumns() ) ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_deposit_amount = ' . $this->sqlBaseDepositAmount(). ',' ; } elseif( true == array_key_exists( 'BaseDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' base_deposit_amount = ' . $this->sqlBaseDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_deposit_tax = ' . $this->sqlBaseDepositTax(). ',' ; } elseif( true == array_key_exists( 'BaseDepositTax', $this->getChangedColumns() ) ) { $strSql .= ' base_deposit_tax = ' . $this->sqlBaseDepositTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'BaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_deposit_amount = ' . $this->sqlAmenityDepositAmount(). ',' ; } elseif( true == array_key_exists( 'AmenityDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' amenity_deposit_amount = ' . $this->sqlAmenityDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_deposit_tax = ' . $this->sqlAmenityDepositTax(). ',' ; } elseif( true == array_key_exists( 'AmenityDepositTax', $this->getChangedColumns() ) ) { $strSql .= ' amenity_deposit_tax = ' . $this->sqlAmenityDepositTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'AmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_deposit_amount = ' . $this->sqlSpecialDepositAmount(). ',' ; } elseif( true == array_key_exists( 'SpecialDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' special_deposit_amount = ' . $this->sqlSpecialDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_deposit_tax = ' . $this->sqlSpecialDepositTax(). ',' ; } elseif( true == array_key_exists( 'SpecialDepositTax', $this->getChangedColumns() ) ) { $strSql .= ' special_deposit_tax = ' . $this->sqlSpecialDepositTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'SpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_deposit_amount = ' . $this->sqlAddOnDepositAmount(). ',' ; } elseif( true == array_key_exists( 'AddOnDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' add_on_deposit_amount = ' . $this->sqlAddOnDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_deposit_tax = ' . $this->sqlAddOnDepositTax(). ',' ; } elseif( true == array_key_exists( 'AddOnDepositTax', $this->getChangedColumns() ) ) { $strSql .= ' add_on_deposit_tax = ' . $this->sqlAddOnDepositTax() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'AddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'TotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_amount = ' . $this->sqlEntrataBaseAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_amount = ' . $this->sqlEntrataBaseAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_tax_amount = ' . $this->sqlEntrataBaseTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_tax_amount = ' . $this->sqlEntrataBaseTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_rent = ' . $this->sqlEntrataBaseRent(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_rent = ' . $this->sqlEntrataBaseRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_amount = ' . $this->sqlEntrataAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_amount = ' . $this->sqlEntrataAmenityAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_tax_amount = ' . $this->sqlEntrataAmenityTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_tax_amount = ' . $this->sqlEntrataAmenityTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_rent = ' . $this->sqlEntrataAmenityRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_rent = ' . $this->sqlEntrataAmenityRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_amount = ' . $this->sqlEntrataSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_amount = ' . $this->sqlEntrataSpecialAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_tax_amount = ' . $this->sqlEntrataSpecialTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_tax_amount = ' . $this->sqlEntrataSpecialTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_rent = ' . $this->sqlEntrataSpecialRent(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_rent = ' . $this->sqlEntrataSpecialRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_amount = ' . $this->sqlEntrataAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_amount = ' . $this->sqlEntrataAddOnAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_tax_amount = ' . $this->sqlEntrataAddOnTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_tax_amount = ' . $this->sqlEntrataAddOnTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_rent = ' . $this->sqlEntrataAddOnRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_rent = ' . $this->sqlEntrataAddOnRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_market_rent = ' . $this->sqlEntrataMarketRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_market_rent = ' . $this->sqlEntrataMarketRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_deposit_amount = ' . $this->sqlEntrataBaseDepositAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_deposit_amount = ' . $this->sqlEntrataBaseDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_deposit_tax_amount = ' . $this->sqlEntrataBaseDepositTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseDepositTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_deposit_tax_amount = ' . $this->sqlEntrataBaseDepositTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_deposit = ' . $this->sqlEntrataBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_deposit = ' . $this->sqlEntrataBaseDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_deposit_amount = ' . $this->sqlEntrataAmenityDepositAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_deposit_amount = ' . $this->sqlEntrataAmenityDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_deposit_tax_amount = ' . $this->sqlEntrataAmenityDepositTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityDepositTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_deposit_tax_amount = ' . $this->sqlEntrataAmenityDepositTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_deposit = ' . $this->sqlEntrataAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_deposit = ' . $this->sqlEntrataAmenityDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_deposit_amount = ' . $this->sqlEntrataSpecialDepositAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_deposit_amount = ' . $this->sqlEntrataSpecialDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_deposit_tax_amount = ' . $this->sqlEntrataSpecialDepositTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialDepositTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_deposit_tax_amount = ' . $this->sqlEntrataSpecialDepositTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_deposit = ' . $this->sqlEntrataSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_deposit = ' . $this->sqlEntrataSpecialDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_deposit_amount = ' . $this->sqlEntrataAddOnDepositAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_deposit_amount = ' . $this->sqlEntrataAddOnDepositAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_deposit_tax_amount = ' . $this->sqlEntrataAddOnDepositTaxAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnDepositTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_deposit_tax_amount = ' . $this->sqlEntrataAddOnDepositTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_deposit = ' . $this->sqlEntrataAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_deposit = ' . $this->sqlEntrataAddOnDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_total_deposit = ' . $this->sqlEntrataTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_total_deposit = ' . $this->sqlEntrataTotalDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_amount_monthly = ' . $this->sqlBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_amount_monthly = ' . $this->sqlBaseAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax_monthly = ' . $this->sqlBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_tax_monthly = ' . $this->sqlBaseTaxMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent_monthly = ' . $this->sqlBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_rent_monthly = ' . $this->sqlBaseRentMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_amount_monthly = ' . $this->sqlAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_amount_monthly = ' . $this->sqlAmenityAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_tax_monthly = ' . $this->sqlAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_tax_monthly = ' . $this->sqlAmenityTaxMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent_monthly = ' . $this->sqlAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent_monthly = ' . $this->sqlAmenityRentMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_amount_monthly = ' . $this->sqlSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_amount_monthly = ' . $this->sqlSpecialAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_tax_monthly = ' . $this->sqlSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_tax_monthly = ' . $this->sqlSpecialTaxMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent_monthly = ' . $this->sqlSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_rent_monthly = ' . $this->sqlSpecialRentMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_amount_monthly = ' . $this->sqlAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_amount_monthly = ' . $this->sqlAddOnAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_tax_monthly = ' . $this->sqlAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_tax_monthly = ' . $this->sqlAddOnTaxMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_rent_monthly = ' . $this->sqlAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_rent_monthly = ' . $this->sqlAddOnRentMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_monthly = ' . $this->sqlMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_monthly = ' . $this->sqlMarketRentMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_amount_monthly = ' . $this->sqlMarketAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MarketAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' market_amount_monthly = ' . $this->sqlMarketAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_market_amount_monthly = ' . $this->sqlEntrataMarketAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMarketAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_market_amount_monthly = ' . $this->sqlEntrataMarketAmountMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_datetime = ' . $this->sqlCachedDatetime(). ',' ; } elseif( true == array_key_exists( 'CachedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' cached_datetime = ' . $this->sqlCachedDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeTaxInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeOtherInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_other = ' . $this->sqlBaseOther(). ',' ; } elseif( true == array_key_exists( 'BaseOther', $this->getChangedColumns() ) ) { $strSql .= ' base_other = ' . $this->sqlBaseOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_other = ' . $this->sqlAmenityOther(). ',' ; } elseif( true == array_key_exists( 'AmenityOther', $this->getChangedColumns() ) ) { $strSql .= ' amenity_other = ' . $this->sqlAmenityOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_other = ' . $this->sqlSpecialOther(). ',' ; } elseif( true == array_key_exists( 'SpecialOther', $this->getChangedColumns() ) ) { $strSql .= ' special_other = ' . $this->sqlSpecialOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_other = ' . $this->sqlAddOnOther(). ',' ; } elseif( true == array_key_exists( 'AddOnOther', $this->getChangedColumns() ) ) { $strSql .= ' add_on_other = ' . $this->sqlAddOnOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_base_other = ' . $this->sqlEntrataBaseOther(). ',' ; } elseif( true == array_key_exists( 'EntrataBaseOther', $this->getChangedColumns() ) ) { $strSql .= ' entrata_base_other = ' . $this->sqlEntrataBaseOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_amenity_other = ' . $this->sqlEntrataAmenityOther(). ',' ; } elseif( true == array_key_exists( 'EntrataAmenityOther', $this->getChangedColumns() ) ) { $strSql .= ' entrata_amenity_other = ' . $this->sqlEntrataAmenityOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_special_other = ' . $this->sqlEntrataSpecialOther(). ',' ; } elseif( true == array_key_exists( 'EntrataSpecialOther', $this->getChangedColumns() ) ) { $strSql .= ' entrata_special_other = ' . $this->sqlEntrataSpecialOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_add_on_other = ' . $this->sqlEntrataAddOnOther(). ',' ; } elseif( true == array_key_exists( 'EntrataAddOnOther', $this->getChangedColumns() ) ) { $strSql .= ' entrata_add_on_other = ' . $this->sqlEntrataAddOnOther() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_other_monthly = ' . $this->sqlBaseOtherMonthly(). ',' ; } elseif( true == array_key_exists( 'BaseOtherMonthly', $this->getChangedColumns() ) ) { $strSql .= ' base_other_monthly = ' . $this->sqlBaseOtherMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_other_monthly = ' . $this->sqlAmenityOtherMonthly(). ',' ; } elseif( true == array_key_exists( 'AmenityOtherMonthly', $this->getChangedColumns() ) ) { $strSql .= ' amenity_other_monthly = ' . $this->sqlAmenityOtherMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_other_monthly = ' . $this->sqlSpecialOtherMonthly(). ',' ; } elseif( true == array_key_exists( 'SpecialOtherMonthly', $this->getChangedColumns() ) ) { $strSql .= ' special_other_monthly = ' . $this->sqlSpecialOtherMonthly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_other_monthly = ' . $this->sqlAddOnOtherMonthly() ; } elseif( true == array_key_exists( 'AddOnOtherMonthly', $this->getChangedColumns() ) ) { $strSql .= ' add_on_other_monthly = ' . $this->sqlAddOnOtherMonthly() ; $boolUpdate = true; }
		$strSql .= ' WHERE
					id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'lease_term_months' => $this->getLeaseTermMonths(),
			'effective_range' => $this->getEffectiveRange(),
			'base_amount' => $this->getBaseAmount(),
			'base_tax' => $this->getBaseTax(),
			'base_rent' => $this->getBaseRent(),
			'amenity_amount' => $this->getAmenityAmount(),
			'amenity_tax' => $this->getAmenityTax(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_amount' => $this->getSpecialAmount(),
			'special_tax' => $this->getSpecialTax(),
			'special_rent' => $this->getSpecialRent(),
			'add_on_amount' => $this->getAddOnAmount(),
			'add_on_tax' => $this->getAddOnTax(),
			'add_on_rent' => $this->getAddOnRent(),
			'market_rent' => $this->getMarketRent(),
			'base_deposit_amount' => $this->getBaseDepositAmount(),
			'base_deposit_tax' => $this->getBaseDepositTax(),
			'base_deposit' => $this->getBaseDeposit(),
			'amenity_deposit_amount' => $this->getAmenityDepositAmount(),
			'amenity_deposit_tax' => $this->getAmenityDepositTax(),
			'amenity_deposit' => $this->getAmenityDeposit(),
			'special_deposit_amount' => $this->getSpecialDepositAmount(),
			'special_deposit_tax' => $this->getSpecialDepositTax(),
			'special_deposit' => $this->getSpecialDeposit(),
			'add_on_deposit_amount' => $this->getAddOnDepositAmount(),
			'add_on_deposit_tax' => $this->getAddOnDepositTax(),
			'add_on_deposit' => $this->getAddOnDeposit(),
			'total_deposit' => $this->getTotalDeposit(),
			'entrata_base_amount' => $this->getEntrataBaseAmount(),
			'entrata_base_tax_amount' => $this->getEntrataBaseTaxAmount(),
			'entrata_base_rent' => $this->getEntrataBaseRent(),
			'entrata_amenity_amount' => $this->getEntrataAmenityAmount(),
			'entrata_amenity_tax_amount' => $this->getEntrataAmenityTaxAmount(),
			'entrata_amenity_rent' => $this->getEntrataAmenityRent(),
			'entrata_special_amount' => $this->getEntrataSpecialAmount(),
			'entrata_special_tax_amount' => $this->getEntrataSpecialTaxAmount(),
			'entrata_special_rent' => $this->getEntrataSpecialRent(),
			'entrata_add_on_amount' => $this->getEntrataAddOnAmount(),
			'entrata_add_on_tax_amount' => $this->getEntrataAddOnTaxAmount(),
			'entrata_add_on_rent' => $this->getEntrataAddOnRent(),
			'entrata_market_rent' => $this->getEntrataMarketRent(),
			'entrata_base_deposit_amount' => $this->getEntrataBaseDepositAmount(),
			'entrata_base_deposit_tax_amount' => $this->getEntrataBaseDepositTaxAmount(),
			'entrata_base_deposit' => $this->getEntrataBaseDeposit(),
			'entrata_amenity_deposit_amount' => $this->getEntrataAmenityDepositAmount(),
			'entrata_amenity_deposit_tax_amount' => $this->getEntrataAmenityDepositTaxAmount(),
			'entrata_amenity_deposit' => $this->getEntrataAmenityDeposit(),
			'entrata_special_deposit_amount' => $this->getEntrataSpecialDepositAmount(),
			'entrata_special_deposit_tax_amount' => $this->getEntrataSpecialDepositTaxAmount(),
			'entrata_special_deposit' => $this->getEntrataSpecialDeposit(),
			'entrata_add_on_deposit_amount' => $this->getEntrataAddOnDepositAmount(),
			'entrata_add_on_deposit_tax_amount' => $this->getEntrataAddOnDepositTaxAmount(),
			'entrata_add_on_deposit' => $this->getEntrataAddOnDeposit(),
			'entrata_total_deposit' => $this->getEntrataTotalDeposit(),
			'base_amount_monthly' => $this->getBaseAmountMonthly(),
			'base_tax_monthly' => $this->getBaseTaxMonthly(),
			'base_rent_monthly' => $this->getBaseRentMonthly(),
			'amenity_amount_monthly' => $this->getAmenityAmountMonthly(),
			'amenity_tax_monthly' => $this->getAmenityTaxMonthly(),
			'amenity_rent_monthly' => $this->getAmenityRentMonthly(),
			'special_amount_monthly' => $this->getSpecialAmountMonthly(),
			'special_tax_monthly' => $this->getSpecialTaxMonthly(),
			'special_rent_monthly' => $this->getSpecialRentMonthly(),
			'add_on_amount_monthly' => $this->getAddOnAmountMonthly(),
			'add_on_tax_monthly' => $this->getAddOnTaxMonthly(),
			'add_on_rent_monthly' => $this->getAddOnRentMonthly(),
			'market_rent_monthly' => $this->getMarketRentMonthly(),
			'market_amount_monthly' => $this->getMarketAmountMonthly(),
			'entrata_market_amount_monthly' => $this->getEntrataMarketAmountMonthly(),
			'cached_datetime' => $this->getCachedDatetime(),
			'show_on_website' => $this->getShowOnWebsite(),
			'include_tax_in_advertised_rent' => $this->getIncludeTaxInAdvertisedRent(),
			'include_other_in_advertised_rent' => $this->getIncludeOtherInAdvertisedRent(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'base_other' => $this->getBaseOther(),
			'amenity_other' => $this->getAmenityOther(),
			'special_other' => $this->getSpecialOther(),
			'add_on_other' => $this->getAddOnOther(),
			'entrata_base_other' => $this->getEntrataBaseOther(),
			'entrata_amenity_other' => $this->getEntrataAmenityOther(),
			'entrata_special_other' => $this->getEntrataSpecialOther(),
			'entrata_add_on_other' => $this->getEntrataAddOnOther(),
			'base_other_monthly' => $this->getBaseOtherMonthly(),
			'amenity_other_monthly' => $this->getAmenityOtherMonthly(),
			'special_other_monthly' => $this->getSpecialOtherMonthly(),
			'add_on_other_monthly' => $this->getAddOnOtherMonthly()
		);
	}

}
?>