<?php

class CBaseMoneygramLocation extends CEosSingularBase {

	const TABLE_NAME = 'public.moneygram_locations';

	protected $m_intId;
	protected $m_strName;
	protected $m_strAddress;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strCountryCode;
	protected $m_strPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strWeekdayOpenTime;
	protected $m_strWeekdayCloseTime;
	protected $m_strSatOpenTime;
	protected $m_strSatCloseTime;
	protected $m_strSunOpenTime;
	protected $m_strSunCloseTime;
	protected $m_intIsBlacklisted;
	protected $m_strLongitude;
	protected $m_strLatitude;
	protected $m_intFailureCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsBlacklisted = '0';
		$this->m_intFailureCount = '0';
		$this->m_intUpdatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['address'] ) && $boolDirectSet ) $this->set( 'm_strAddress', trim( stripcslashes( $arrValues['address'] ) ) ); elseif( isset( $arrValues['address'] ) ) $this->setAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address'] ) : $arrValues['address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['weekday_open_time'] ) && $boolDirectSet ) $this->set( 'm_strWeekdayOpenTime', trim( stripcslashes( $arrValues['weekday_open_time'] ) ) ); elseif( isset( $arrValues['weekday_open_time'] ) ) $this->setWeekdayOpenTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['weekday_open_time'] ) : $arrValues['weekday_open_time'] );
		if( isset( $arrValues['weekday_close_time'] ) && $boolDirectSet ) $this->set( 'm_strWeekdayCloseTime', trim( stripcslashes( $arrValues['weekday_close_time'] ) ) ); elseif( isset( $arrValues['weekday_close_time'] ) ) $this->setWeekdayCloseTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['weekday_close_time'] ) : $arrValues['weekday_close_time'] );
		if( isset( $arrValues['sat_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSatOpenTime', trim( stripcslashes( $arrValues['sat_open_time'] ) ) ); elseif( isset( $arrValues['sat_open_time'] ) ) $this->setSatOpenTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sat_open_time'] ) : $arrValues['sat_open_time'] );
		if( isset( $arrValues['sat_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSatCloseTime', trim( stripcslashes( $arrValues['sat_close_time'] ) ) ); elseif( isset( $arrValues['sat_close_time'] ) ) $this->setSatCloseTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sat_close_time'] ) : $arrValues['sat_close_time'] );
		if( isset( $arrValues['sun_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSunOpenTime', trim( stripcslashes( $arrValues['sun_open_time'] ) ) ); elseif( isset( $arrValues['sun_open_time'] ) ) $this->setSunOpenTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sun_open_time'] ) : $arrValues['sun_open_time'] );
		if( isset( $arrValues['sun_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSunCloseTime', trim( stripcslashes( $arrValues['sun_close_time'] ) ) ); elseif( isset( $arrValues['sun_close_time'] ) ) $this->setSunCloseTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sun_close_time'] ) : $arrValues['sun_close_time'] );
		if( isset( $arrValues['is_blacklisted'] ) && $boolDirectSet ) $this->set( 'm_intIsBlacklisted', trim( $arrValues['is_blacklisted'] ) ); elseif( isset( $arrValues['is_blacklisted'] ) ) $this->setIsBlacklisted( $arrValues['is_blacklisted'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( stripcslashes( $arrValues['longitude'] ) ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['longitude'] ) : $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( stripcslashes( $arrValues['latitude'] ) ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latitude'] ) : $arrValues['latitude'] );
		if( isset( $arrValues['failure_count'] ) && $boolDirectSet ) $this->set( 'm_intFailureCount', trim( $arrValues['failure_count'] ) ); elseif( isset( $arrValues['failure_count'] ) ) $this->setFailureCount( $arrValues['failure_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAddress( $strAddress ) {
		$this->set( 'm_strAddress', CStrings::strTrimDef( $strAddress, 100, NULL, true ) );
	}

	public function getAddress() {
		return $this->m_strAddress;
	}

	public function sqlAddress() {
		return ( true == isset( $this->m_strAddress ) ) ? '\'' . addslashes( $this->m_strAddress ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 50, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 3, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 10, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setWeekdayOpenTime( $strWeekdayOpenTime ) {
		$this->set( 'm_strWeekdayOpenTime', CStrings::strTrimDef( $strWeekdayOpenTime, 100, NULL, true ) );
	}

	public function getWeekdayOpenTime() {
		return $this->m_strWeekdayOpenTime;
	}

	public function sqlWeekdayOpenTime() {
		return ( true == isset( $this->m_strWeekdayOpenTime ) ) ? '\'' . addslashes( $this->m_strWeekdayOpenTime ) . '\'' : 'NULL';
	}

	public function setWeekdayCloseTime( $strWeekdayCloseTime ) {
		$this->set( 'm_strWeekdayCloseTime', CStrings::strTrimDef( $strWeekdayCloseTime, 100, NULL, true ) );
	}

	public function getWeekdayCloseTime() {
		return $this->m_strWeekdayCloseTime;
	}

	public function sqlWeekdayCloseTime() {
		return ( true == isset( $this->m_strWeekdayCloseTime ) ) ? '\'' . addslashes( $this->m_strWeekdayCloseTime ) . '\'' : 'NULL';
	}

	public function setSatOpenTime( $strSatOpenTime ) {
		$this->set( 'm_strSatOpenTime', CStrings::strTrimDef( $strSatOpenTime, 100, NULL, true ) );
	}

	public function getSatOpenTime() {
		return $this->m_strSatOpenTime;
	}

	public function sqlSatOpenTime() {
		return ( true == isset( $this->m_strSatOpenTime ) ) ? '\'' . addslashes( $this->m_strSatOpenTime ) . '\'' : 'NULL';
	}

	public function setSatCloseTime( $strSatCloseTime ) {
		$this->set( 'm_strSatCloseTime', CStrings::strTrimDef( $strSatCloseTime, 100, NULL, true ) );
	}

	public function getSatCloseTime() {
		return $this->m_strSatCloseTime;
	}

	public function sqlSatCloseTime() {
		return ( true == isset( $this->m_strSatCloseTime ) ) ? '\'' . addslashes( $this->m_strSatCloseTime ) . '\'' : 'NULL';
	}

	public function setSunOpenTime( $strSunOpenTime ) {
		$this->set( 'm_strSunOpenTime', CStrings::strTrimDef( $strSunOpenTime, 100, NULL, true ) );
	}

	public function getSunOpenTime() {
		return $this->m_strSunOpenTime;
	}

	public function sqlSunOpenTime() {
		return ( true == isset( $this->m_strSunOpenTime ) ) ? '\'' . addslashes( $this->m_strSunOpenTime ) . '\'' : 'NULL';
	}

	public function setSunCloseTime( $strSunCloseTime ) {
		$this->set( 'm_strSunCloseTime', CStrings::strTrimDef( $strSunCloseTime, 100, NULL, true ) );
	}

	public function getSunCloseTime() {
		return $this->m_strSunCloseTime;
	}

	public function sqlSunCloseTime() {
		return ( true == isset( $this->m_strSunCloseTime ) ) ? '\'' . addslashes( $this->m_strSunCloseTime ) . '\'' : 'NULL';
	}

	public function setIsBlacklisted( $intIsBlacklisted ) {
		$this->set( 'm_intIsBlacklisted', CStrings::strToIntDef( $intIsBlacklisted, NULL, false ) );
	}

	public function getIsBlacklisted() {
		return $this->m_intIsBlacklisted;
	}

	public function sqlIsBlacklisted() {
		return ( true == isset( $this->m_intIsBlacklisted ) ) ? ( string ) $this->m_intIsBlacklisted : '0';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 100, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? '\'' . addslashes( $this->m_strLongitude ) . '\'' : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 100, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? '\'' . addslashes( $this->m_strLatitude ) . '\'' : 'NULL';
	}

	public function setFailureCount( $intFailureCount ) {
		$this->set( 'm_intFailureCount', CStrings::strToIntDef( $intFailureCount, NULL, false ) );
	}

	public function getFailureCount() {
		return $this->m_intFailureCount;
	}

	public function sqlFailureCount() {
		return ( true == isset( $this->m_intFailureCount ) ) ? ( string ) $this->m_intFailureCount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '0';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, address, city, state_code, country_code, postal_code, phone_number, weekday_open_time, weekday_close_time, sat_open_time, sat_close_time, sun_open_time, sun_close_time, is_blacklisted, longitude, latitude, failure_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlAddress() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlWeekdayOpenTime() . ', ' .
 						$this->sqlWeekdayCloseTime() . ', ' .
 						$this->sqlSatOpenTime() . ', ' .
 						$this->sqlSatCloseTime() . ', ' .
 						$this->sqlSunOpenTime() . ', ' .
 						$this->sqlSunCloseTime() . ', ' .
 						$this->sqlIsBlacklisted() . ', ' .
 						$this->sqlLongitude() . ', ' .
 						$this->sqlLatitude() . ', ' .
 						$this->sqlFailureCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; } elseif( true == array_key_exists( 'Address', $this->getChangedColumns() ) ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weekday_open_time = ' . $this->sqlWeekdayOpenTime() . ','; } elseif( true == array_key_exists( 'WeekdayOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' weekday_open_time = ' . $this->sqlWeekdayOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weekday_close_time = ' . $this->sqlWeekdayCloseTime() . ','; } elseif( true == array_key_exists( 'WeekdayCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' weekday_close_time = ' . $this->sqlWeekdayCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; } elseif( true == array_key_exists( 'SatOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; } elseif( true == array_key_exists( 'SatCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; } elseif( true == array_key_exists( 'SunOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; } elseif( true == array_key_exists( 'SunCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_blacklisted = ' . $this->sqlIsBlacklisted() . ','; } elseif( true == array_key_exists( 'IsBlacklisted', $this->getChangedColumns() ) ) { $strSql .= ' is_blacklisted = ' . $this->sqlIsBlacklisted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failure_count = ' . $this->sqlFailureCount() . ','; } elseif( true == array_key_exists( 'FailureCount', $this->getChangedColumns() ) ) { $strSql .= ' failure_count = ' . $this->sqlFailureCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'address' => $this->getAddress(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'country_code' => $this->getCountryCode(),
			'postal_code' => $this->getPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'weekday_open_time' => $this->getWeekdayOpenTime(),
			'weekday_close_time' => $this->getWeekdayCloseTime(),
			'sat_open_time' => $this->getSatOpenTime(),
			'sat_close_time' => $this->getSatCloseTime(),
			'sun_open_time' => $this->getSunOpenTime(),
			'sun_close_time' => $this->getSunCloseTime(),
			'is_blacklisted' => $this->getIsBlacklisted(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'failure_count' => $this->getFailureCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>