<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIconColors
 * Do not add any new functions to this class.
 */

class CBaseIconColors extends CEosPluralBase {

	/**
	 * @return CIconColor[]
	 */
	public static function fetchIconColors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIconColor', $objDatabase );
	}

	/**
	 * @return CIconColor
	 */
	public static function fetchIconColor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIconColor', $objDatabase );
	}

	public static function fetchIconColorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'icon_colors', $objDatabase );
	}

	public static function fetchIconColorById( $intId, $objDatabase ) {
		return self::fetchIconColor( sprintf( 'SELECT * FROM icon_colors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>