<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestCalls
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestCalls extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestCall[]
	 */
	public static function fetchMaintenanceRequestCalls( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceRequestCall', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestCall
	 */
	public static function fetchMaintenanceRequestCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceRequestCall', $objDatabase );
	}

	public static function fetchMaintenanceRequestCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_calls', $objDatabase );
	}

	public static function fetchMaintenanceRequestCallByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestCall( sprintf( 'SELECT * FROM maintenance_request_calls WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestCallsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestCalls( sprintf( 'SELECT * FROM maintenance_request_calls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestCallsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestCalls( sprintf( 'SELECT * FROM maintenance_request_calls WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestCallsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestCalls( sprintf( 'SELECT * FROM maintenance_request_calls WHERE call_id = %d AND cid = %d', ( int ) $intCallId, ( int ) $intCid ), $objDatabase );
	}

}
?>