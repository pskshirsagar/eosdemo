<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLeadSurveyQuestions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyLeadSurveyQuestions extends CEosPluralBase {

	/**
	 * @return CPropertyLeadSurveyQuestion[]
	 */
	public static function fetchPropertyLeadSurveyQuestions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyLeadSurveyQuestion::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyLeadSurveyQuestion
	 */
	public static function fetchPropertyLeadSurveyQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyLeadSurveyQuestion::class, $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_lead_survey_questions', $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeadSurveyQuestion( sprintf( 'SELECT * FROM property_lead_survey_questions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyLeadSurveyQuestions( sprintf( 'SELECT * FROM property_lead_survey_questions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeadSurveyQuestionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeadSurveyQuestions( sprintf( 'SELECT * FROM property_lead_survey_questions WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>