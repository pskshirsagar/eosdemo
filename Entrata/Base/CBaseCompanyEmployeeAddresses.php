<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployeeAddresses extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeAddress[]
	 */
	public static function fetchCompanyEmployeeAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyEmployeeAddress', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployeeAddress
	 */
	public static function fetchCompanyEmployeeAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyEmployeeAddress', $objDatabase );
	}

	public static function fetchCompanyEmployeeAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_addresses', $objDatabase );
	}

	public static function fetchCompanyEmployeeAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeAddress( sprintf( 'SELECT * FROM company_employee_addresses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeAddresses( sprintf( 'SELECT * FROM company_employee_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeAddressesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeAddresses( sprintf( 'SELECT * FROM company_employee_addresses WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeAddressesByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeAddresses( sprintf( 'SELECT * FROM company_employee_addresses WHERE address_type_id = %d AND cid = %d', ( int ) $intAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>