<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyContracts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyContracts extends CEosPluralBase {

	/**
	 * @return CSubsidyContract[]
	 */
	public static function fetchSubsidyContracts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyContract::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyContract
	 */
	public static function fetchSubsidyContract( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyContract::class, $objDatabase );
	}

	public static function fetchSubsidyContractCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_contracts', $objDatabase );
	}

	public static function fetchSubsidyContractByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContract( sprintf( 'SELECT * FROM subsidy_contracts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsBySubsidyContractTypeIdByCid( $intSubsidyContractTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE subsidy_contract_type_id = %d AND cid = %d', ( int ) $intSubsidyContractTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsBySubsidyContractSubTypeIdByCid( $intSubsidyContractSubTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE subsidy_contract_sub_type_id = %d AND cid = %d', ( int ) $intSubsidyContractSubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsBySubsidyUnitAssignmentTypeIdByCid( $intSubsidyUnitAssignmentTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE subsidy_unit_assignment_type_id = %d AND cid = %d', ( int ) $intSubsidyUnitAssignmentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractsBySubsidyActionPlanTypeIdByCid( $intSubsidyActionPlanTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContracts( sprintf( 'SELECT * FROM subsidy_contracts WHERE subsidy_action_plan_type_id = %d AND cid = %d', ( int ) $intSubsidyActionPlanTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>