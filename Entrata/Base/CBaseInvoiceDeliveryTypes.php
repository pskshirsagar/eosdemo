<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInvoiceDeliveryTypes
 * Do not add any new functions to this class.
 */

class CBaseInvoiceDeliveryTypes extends CEosPluralBase {

	/**
	 * @return CInvoiceDeliveryType[]
	 */
	public static function fetchInvoiceDeliveryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInvoiceDeliveryType::class, $objDatabase );
	}

	/**
	 * @return CInvoiceDeliveryType
	 */
	public static function fetchInvoiceDeliveryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInvoiceDeliveryType::class, $objDatabase );
	}

	public static function fetchInvoiceDeliveryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'invoice_delivery_types', $objDatabase );
	}

	public static function fetchInvoiceDeliveryTypeById( $intId, $objDatabase ) {
		return self::fetchInvoiceDeliveryType( sprintf( 'SELECT * FROM invoice_delivery_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>