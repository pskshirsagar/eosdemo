<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenancePriorityTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenancePriorityTypes extends CEosPluralBase {

	/**
	 * @return CMaintenancePriorityType[]
	 */
	public static function fetchMaintenancePriorityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenancePriorityType::class, $objDatabase );
	}

	/**
	 * @return CMaintenancePriorityType
	 */
	public static function fetchMaintenancePriorityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenancePriorityType::class, $objDatabase );
	}

	public static function fetchMaintenancePriorityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_priority_types', $objDatabase );
	}

	public static function fetchMaintenancePriorityTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenancePriorityType( sprintf( 'SELECT * FROM maintenance_priority_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>