<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCamCalculationTypes
 * Do not add any new functions to this class.
 */

class CBaseCamCalculationTypes extends CEosPluralBase {

	/**
	 * @return CCamCalculationType[]
	 */
	public static function fetchCamCalculationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCamCalculationType::class, $objDatabase );
	}

	/**
	 * @return CCamCalculationType
	 */
	public static function fetchCamCalculationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCamCalculationType::class, $objDatabase );
	}

	public static function fetchCamCalculationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cam_calculation_types', $objDatabase );
	}

	public static function fetchCamCalculationTypeById( $intId, $objDatabase ) {
		return self::fetchCamCalculationType( sprintf( 'SELECT * FROM cam_calculation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>