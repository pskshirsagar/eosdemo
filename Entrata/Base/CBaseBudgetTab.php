<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTab extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.budget_tabs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetId;
	protected $m_intBudgetWorksheetId;
	protected $m_intBudgetDataSourceTypeId;
	protected $m_strName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsConfidential;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intBudgetWorksheetTypeId;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsConfidential = false;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_intBudgetWorksheetTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetId', trim( $arrValues['budget_id'] ) ); elseif( isset( $arrValues['budget_id'] ) ) $this->setBudgetId( $arrValues['budget_id'] );
		if( isset( $arrValues['budget_worksheet_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorksheetId', trim( $arrValues['budget_worksheet_id'] ) ); elseif( isset( $arrValues['budget_worksheet_id'] ) ) $this->setBudgetWorksheetId( $arrValues['budget_worksheet_id'] );
		if( isset( $arrValues['budget_data_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceTypeId', trim( $arrValues['budget_data_source_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_type_id'] ) ) $this->setBudgetDataSourceTypeId( $arrValues['budget_data_source_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['budget_worksheet_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorksheetTypeId', trim( $arrValues['budget_worksheet_type_id'] ) ); elseif( isset( $arrValues['budget_worksheet_type_id'] ) ) $this->setBudgetWorksheetTypeId( $arrValues['budget_worksheet_type_id'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetId( $intBudgetId ) {
		$this->set( 'm_intBudgetId', CStrings::strToIntDef( $intBudgetId, NULL, false ) );
	}

	public function getBudgetId() {
		return $this->m_intBudgetId;
	}

	public function sqlBudgetId() {
		return ( true == isset( $this->m_intBudgetId ) ) ? ( string ) $this->m_intBudgetId : 'NULL';
	}

	public function setBudgetWorksheetId( $intBudgetWorksheetId ) {
		$this->set( 'm_intBudgetWorksheetId', CStrings::strToIntDef( $intBudgetWorksheetId, NULL, false ) );
	}

	public function getBudgetWorksheetId() {
		return $this->m_intBudgetWorksheetId;
	}

	public function sqlBudgetWorksheetId() {
		return ( true == isset( $this->m_intBudgetWorksheetId ) ) ? ( string ) $this->m_intBudgetWorksheetId : 'NULL';
	}

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->set( 'm_intBudgetDataSourceTypeId', CStrings::strToIntDef( $intBudgetDataSourceTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function sqlBudgetDataSourceTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setBudgetWorksheetTypeId( $intBudgetWorksheetTypeId ) {
		$this->set( 'm_intBudgetWorksheetTypeId', CStrings::strToIntDef( $intBudgetWorksheetTypeId, NULL, false ) );
	}

	public function getBudgetWorksheetTypeId() {
		return $this->m_intBudgetWorksheetTypeId;
	}

	public function sqlBudgetWorksheetTypeId() {
		return ( true == isset( $this->m_intBudgetWorksheetTypeId ) ) ? ( string ) $this->m_intBudgetWorksheetTypeId : '1';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_id, budget_worksheet_id, budget_data_source_type_id, name, details, is_confidential, order_num, updated_by, updated_on, created_by, created_on, budget_worksheet_type_id, approved_by, approved_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlBudgetId() . ', ' .
		          $this->sqlBudgetWorksheetId() . ', ' .
		          $this->sqlBudgetDataSourceTypeId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlIsConfidential() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlBudgetWorksheetTypeId() . ', ' .
		          $this->sqlApprovedBy() . ', ' .
		          $this->sqlApprovedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId(). ',' ; } elseif( true == array_key_exists( 'BudgetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_id = ' . $this->sqlBudgetId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_worksheet_id = ' . $this->sqlBudgetWorksheetId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorksheetId', $this->getChangedColumns() ) ) { $strSql .= ' budget_worksheet_id = ' . $this->sqlBudgetWorksheetId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_worksheet_type_id = ' . $this->sqlBudgetWorksheetTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorksheetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_worksheet_type_id = ' . $this->sqlBudgetWorksheetTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_id' => $this->getBudgetId(),
			'budget_worksheet_id' => $this->getBudgetWorksheetId(),
			'budget_data_source_type_id' => $this->getBudgetDataSourceTypeId(),
			'name' => $this->getName(),
			'details' => $this->getDetails(),
			'is_confidential' => $this->getIsConfidential(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'budget_worksheet_type_id' => $this->getBudgetWorksheetTypeId(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn()
		);
	}

}
?>