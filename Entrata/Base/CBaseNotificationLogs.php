<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotificationLogs extends CEosPluralBase {

	/**
	 * @return CNotificationLog[]
	 */
	public static function fetchNotificationLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CNotificationLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CNotificationLog
	 */
	public static function fetchNotificationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNotificationLog', $objDatabase );
	}

	public static function fetchNotificationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notification_logs', $objDatabase );
	}

	public static function fetchNotificationLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchNotificationLog( sprintf( 'SELECT * FROM notification_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationLogsByCid( $intCid, $objDatabase ) {
		return self::fetchNotificationLogs( sprintf( 'SELECT * FROM notification_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationLogsByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {
		return self::fetchNotificationLogs( sprintf( 'SELECT * FROM notification_logs WHERE notification_id = %d AND cid = %d', ( int ) $intNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationLogsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchNotificationLogs( sprintf( 'SELECT * FROM notification_logs WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>