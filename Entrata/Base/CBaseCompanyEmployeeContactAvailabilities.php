<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactAvailabilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployeeContactAvailabilities extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeContactAvailability[]
	 */
	public static function fetchCompanyEmployeeContactAvailabilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyEmployeeContactAvailability::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployeeContactAvailability
	 */
	public static function fetchCompanyEmployeeContactAvailability( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEmployeeContactAvailability::class, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_contact_availabilities', $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailability( sprintf( 'SELECT * FROM company_employee_contact_availabilities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailabilities( sprintf( 'SELECT * FROM company_employee_contact_availabilities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailabilities( sprintf( 'SELECT * FROM company_employee_contact_availabilities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByCompanyEmployeeContactIdByCid( $intCompanyEmployeeContactId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailabilities( sprintf( 'SELECT * FROM company_employee_contact_availabilities WHERE company_employee_contact_id = %d AND cid = %d', $intCompanyEmployeeContactId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByCompanyEmployeeContactAvailabilityTypeIdByCid( $intCompanyEmployeeContactAvailabilityTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailabilities( sprintf( 'SELECT * FROM company_employee_contact_availabilities WHERE company_employee_contact_availability_type_id = %d AND cid = %d', $intCompanyEmployeeContactAvailabilityTypeId, $intCid ), $objDatabase );
	}

}
?>