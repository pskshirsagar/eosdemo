<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyDependentTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyDependentTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyDependentType[]
	 */
	public static function fetchSubsidyDependentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyDependentType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyDependentType
	 */
	public static function fetchSubsidyDependentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyDependentType::class, $objDatabase );
	}

	public static function fetchSubsidyDependentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_dependent_types', $objDatabase );
	}

	public static function fetchSubsidyDependentTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyDependentType( sprintf( 'SELECT * FROM subsidy_dependent_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>