<?php

class CBaseDefaultMergeField extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_merge_fields';

	protected $m_intId;
	protected $m_intMergeFieldTypeId;
	protected $m_intBlockDefaultMergeFieldId;
	protected $m_intMergeFieldGroupId;
	protected $m_intMergeFieldSubGroupId;
	protected $m_intExternalMergeFieldGroupId;
	protected $m_strField;
	protected $m_arrstrAliasNames;
	protected $m_strTitle;
	protected $m_strDefaultValue;
	protected $m_strViewExpression;
	protected $m_jsonViewExpression;
	protected $m_strRequiredInputTable;
	protected $m_strEditExpression;
	protected $m_jsonEditExpression;
	protected $m_strDataType;
	protected $m_intAllowUserUpdate;
	protected $m_intAllowAdminUpdate;
	protected $m_intAllowOriginUpdate;
	protected $m_intShowInMergeDisplay;
	protected $m_intAllowDefaultText;
	protected $m_intIsLocked;
	protected $m_intIsBlockField;
	protected $m_intIsRequired;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intMergeFieldTypeId = '3';
		$this->m_arrstrAliasNames = array(	);
		$this->m_strDataType = 'text';
		$this->m_intAllowUserUpdate = '0';
		$this->m_intAllowAdminUpdate = '0';
		$this->m_intAllowOriginUpdate = '0';
		$this->m_intShowInMergeDisplay = '1';
		$this->m_intAllowDefaultText = '1';
		$this->m_intIsLocked = '0';
		$this->m_intIsBlockField = '0';
		$this->m_intIsRequired = '0';
		$this->m_intOrderNum = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['merge_field_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldTypeId', trim( $arrValues['merge_field_type_id'] ) ); elseif( isset( $arrValues['merge_field_type_id'] ) ) $this->setMergeFieldTypeId( $arrValues['merge_field_type_id'] );
		if( isset( $arrValues['block_default_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockDefaultMergeFieldId', trim( $arrValues['block_default_merge_field_id'] ) ); elseif( isset( $arrValues['block_default_merge_field_id'] ) ) $this->setBlockDefaultMergeFieldId( $arrValues['block_default_merge_field_id'] );
		if( isset( $arrValues['merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldGroupId', trim( $arrValues['merge_field_group_id'] ) ); elseif( isset( $arrValues['merge_field_group_id'] ) ) $this->setMergeFieldGroupId( $arrValues['merge_field_group_id'] );
		if( isset( $arrValues['merge_field_sub_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldSubGroupId', trim( $arrValues['merge_field_sub_group_id'] ) ); elseif( isset( $arrValues['merge_field_sub_group_id'] ) ) $this->setMergeFieldSubGroupId( $arrValues['merge_field_sub_group_id'] );
		if( isset( $arrValues['external_merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalMergeFieldGroupId', trim( $arrValues['external_merge_field_group_id'] ) ); elseif( isset( $arrValues['external_merge_field_group_id'] ) ) $this->setExternalMergeFieldGroupId( $arrValues['external_merge_field_group_id'] );
		if( isset( $arrValues['field'] ) && $boolDirectSet ) $this->set( 'm_strField', trim( stripcslashes( $arrValues['field'] ) ) ); elseif( isset( $arrValues['field'] ) ) $this->setField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['field'] ) : $arrValues['field'] );
		if( isset( $arrValues['alias_names'] ) && $boolDirectSet ) $this->set( 'm_arrstrAliasNames', trim( $arrValues['alias_names'] ) ); elseif( isset( $arrValues['alias_names'] ) ) $this->setAliasNames( $arrValues['alias_names'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['default_value'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDefaultValue', trim( stripcslashes( $arrValues['default_value'] ) ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_value'] ) : $arrValues['default_value'] );
		if( isset( $arrValues['view_expression'] ) ) $this->set( 'm_strViewExpression', trim( $arrValues['view_expression'] ) );
		if( isset( $arrValues['required_input_table'] ) && $boolDirectSet ) $this->set( 'm_strRequiredInputTable', trim( stripcslashes( $arrValues['required_input_table'] ) ) ); elseif( isset( $arrValues['required_input_table'] ) ) $this->setRequiredInputTable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['required_input_table'] ) : $arrValues['required_input_table'] );
		if( isset( $arrValues['edit_expression'] ) ) $this->set( 'm_strEditExpression', trim( $arrValues['edit_expression'] ) );
		if( isset( $arrValues['data_type'] ) && $boolDirectSet ) $this->set( 'm_strDataType', trim( stripcslashes( $arrValues['data_type'] ) ) ); elseif( isset( $arrValues['data_type'] ) ) $this->setDataType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_type'] ) : $arrValues['data_type'] );
		if( isset( $arrValues['allow_user_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowUserUpdate', trim( $arrValues['allow_user_update'] ) ); elseif( isset( $arrValues['allow_user_update'] ) ) $this->setAllowUserUpdate( $arrValues['allow_user_update'] );
		if( isset( $arrValues['allow_admin_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowAdminUpdate', trim( $arrValues['allow_admin_update'] ) ); elseif( isset( $arrValues['allow_admin_update'] ) ) $this->setAllowAdminUpdate( $arrValues['allow_admin_update'] );
		if( isset( $arrValues['allow_origin_update'] ) && $boolDirectSet ) $this->set( 'm_intAllowOriginUpdate', trim( $arrValues['allow_origin_update'] ) ); elseif( isset( $arrValues['allow_origin_update'] ) ) $this->setAllowOriginUpdate( $arrValues['allow_origin_update'] );
		if( isset( $arrValues['show_in_merge_display'] ) && $boolDirectSet ) $this->set( 'm_intShowInMergeDisplay', trim( $arrValues['show_in_merge_display'] ) ); elseif( isset( $arrValues['show_in_merge_display'] ) ) $this->setShowInMergeDisplay( $arrValues['show_in_merge_display'] );
		if( isset( $arrValues['allow_default_text'] ) && $boolDirectSet ) $this->set( 'm_intAllowDefaultText', trim( $arrValues['allow_default_text'] ) ); elseif( isset( $arrValues['allow_default_text'] ) ) $this->setAllowDefaultText( $arrValues['allow_default_text'] );
		if( isset( $arrValues['is_locked'] ) && $boolDirectSet ) $this->set( 'm_intIsLocked', trim( $arrValues['is_locked'] ) ); elseif( isset( $arrValues['is_locked'] ) ) $this->setIsLocked( $arrValues['is_locked'] );
		if( isset( $arrValues['is_block_field'] ) && $boolDirectSet ) $this->set( 'm_intIsBlockField', trim( $arrValues['is_block_field'] ) ); elseif( isset( $arrValues['is_block_field'] ) ) $this->setIsBlockField( $arrValues['is_block_field'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMergeFieldTypeId( $intMergeFieldTypeId ) {
		$this->set( 'm_intMergeFieldTypeId', CStrings::strToIntDef( $intMergeFieldTypeId, NULL, false ) );
	}

	public function getMergeFieldTypeId() {
		return $this->m_intMergeFieldTypeId;
	}

	public function sqlMergeFieldTypeId() {
		return ( true == isset( $this->m_intMergeFieldTypeId ) ) ? ( string ) $this->m_intMergeFieldTypeId : '3';
	}

	public function setBlockDefaultMergeFieldId( $intBlockDefaultMergeFieldId ) {
		$this->set( 'm_intBlockDefaultMergeFieldId', CStrings::strToIntDef( $intBlockDefaultMergeFieldId, NULL, false ) );
	}

	public function getBlockDefaultMergeFieldId() {
		return $this->m_intBlockDefaultMergeFieldId;
	}

	public function sqlBlockDefaultMergeFieldId() {
		return ( true == isset( $this->m_intBlockDefaultMergeFieldId ) ) ? ( string ) $this->m_intBlockDefaultMergeFieldId : 'NULL';
	}

	public function setMergeFieldGroupId( $intMergeFieldGroupId ) {
		$this->set( 'm_intMergeFieldGroupId', CStrings::strToIntDef( $intMergeFieldGroupId, NULL, false ) );
	}

	public function getMergeFieldGroupId() {
		return $this->m_intMergeFieldGroupId;
	}

	public function sqlMergeFieldGroupId() {
		return ( true == isset( $this->m_intMergeFieldGroupId ) ) ? ( string ) $this->m_intMergeFieldGroupId : 'NULL';
	}

	public function setMergeFieldSubGroupId( $intMergeFieldSubGroupId ) {
		$this->set( 'm_intMergeFieldSubGroupId', CStrings::strToIntDef( $intMergeFieldSubGroupId, NULL, false ) );
	}

	public function getMergeFieldSubGroupId() {
		return $this->m_intMergeFieldSubGroupId;
	}

	public function sqlMergeFieldSubGroupId() {
		return ( true == isset( $this->m_intMergeFieldSubGroupId ) ) ? ( string ) $this->m_intMergeFieldSubGroupId : 'NULL';
	}

	public function setExternalMergeFieldGroupId( $intExternalMergeFieldGroupId ) {
		$this->set( 'm_intExternalMergeFieldGroupId', CStrings::strToIntDef( $intExternalMergeFieldGroupId, NULL, false ) );
	}

	public function getExternalMergeFieldGroupId() {
		return $this->m_intExternalMergeFieldGroupId;
	}

	public function sqlExternalMergeFieldGroupId() {
		return ( true == isset( $this->m_intExternalMergeFieldGroupId ) ) ? ( string ) $this->m_intExternalMergeFieldGroupId : 'NULL';
	}

	public function setField( $strField ) {
		$this->set( 'm_strField', CStrings::strTrimDef( $strField, 64, NULL, true ) );
	}

	public function getField() {
		return $this->m_strField;
	}

	public function sqlField() {
		return ( true == isset( $this->m_strField ) ) ? '\'' . addslashes( $this->m_strField ) . '\'' : 'NULL';
	}

	public function setAliasNames( $arrstrAliasNames ) {
		$this->set( 'm_arrstrAliasNames', CStrings::strToArrIntDef( $arrstrAliasNames, NULL ) );
	}

	public function getAliasNames() {
		return $this->m_arrstrAliasNames;
	}

	public function sqlAliasNames() {
		return ( true == isset( $this->m_arrstrAliasNames ) && true == valArr( $this->m_arrstrAliasNames ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrAliasNames, NULL ) . '\'' : '\'{}\'';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ), $strLocaleCode );
	}

	public function getDefaultValue( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDefaultValue', $strLocaleCode );
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? '\'' . addslashes( $this->m_strDefaultValue ) . '\'' : 'NULL';
	}

	public function setViewExpression( $jsonViewExpression ) {
		if( true == valObj( $jsonViewExpression, 'stdClass' ) ) {
			$this->set( 'm_jsonViewExpression', $jsonViewExpression );
		} elseif( true == valJsonString( $jsonViewExpression ) ) {
			$this->set( 'm_jsonViewExpression', CStrings::strToJson( $jsonViewExpression ) );
		} else {
			$this->set( 'm_jsonViewExpression', NULL ); 
		}
		unset( $this->m_strViewExpression );
	}

	public function getViewExpression() {
		if( true == isset( $this->m_strViewExpression ) ) {
			$this->m_jsonViewExpression = CStrings::strToJson( $this->m_strViewExpression );
			unset( $this->m_strViewExpression );
		}
		return $this->m_jsonViewExpression;
	}

	public function sqlViewExpression() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getViewExpression() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getViewExpression() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setRequiredInputTable( $strRequiredInputTable ) {
		$this->set( 'm_strRequiredInputTable', CStrings::strTrimDef( $strRequiredInputTable, -1, NULL, true ) );
	}

	public function getRequiredInputTable() {
		return $this->m_strRequiredInputTable;
	}

	public function sqlRequiredInputTable() {
		return ( true == isset( $this->m_strRequiredInputTable ) ) ? '\'' . addslashes( $this->m_strRequiredInputTable ) . '\'' : 'NULL';
	}

	public function setEditExpression( $jsonEditExpression ) {
		if( true == valObj( $jsonEditExpression, 'stdClass' ) ) {
			$this->set( 'm_jsonEditExpression', $jsonEditExpression );
		} elseif( true == valJsonString( $jsonEditExpression ) ) {
			$this->set( 'm_jsonEditExpression', CStrings::strToJson( $jsonEditExpression ) );
		} else {
			$this->set( 'm_jsonEditExpression', NULL ); 
		}
		unset( $this->m_strEditExpression );
	}

	public function getEditExpression() {
		if( true == isset( $this->m_strEditExpression ) ) {
			$this->m_jsonEditExpression = CStrings::strToJson( $this->m_strEditExpression );
			unset( $this->m_strEditExpression );
		}
		return $this->m_jsonEditExpression;
	}

	public function sqlEditExpression() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getEditExpression() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getEditExpression() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setDataType( $strDataType ) {
		$this->set( 'm_strDataType', CStrings::strTrimDef( $strDataType, 50, NULL, true ) );
	}

	public function getDataType() {
		return $this->m_strDataType;
	}

	public function sqlDataType() {
		return ( true == isset( $this->m_strDataType ) ) ? '\'' . addslashes( $this->m_strDataType ) . '\'' : '\'text\'';
	}

	public function setAllowUserUpdate( $intAllowUserUpdate ) {
		$this->set( 'm_intAllowUserUpdate', CStrings::strToIntDef( $intAllowUserUpdate, NULL, false ) );
	}

	public function getAllowUserUpdate() {
		return $this->m_intAllowUserUpdate;
	}

	public function sqlAllowUserUpdate() {
		return ( true == isset( $this->m_intAllowUserUpdate ) ) ? ( string ) $this->m_intAllowUserUpdate : '0';
	}

	public function setAllowAdminUpdate( $intAllowAdminUpdate ) {
		$this->set( 'm_intAllowAdminUpdate', CStrings::strToIntDef( $intAllowAdminUpdate, NULL, false ) );
	}

	public function getAllowAdminUpdate() {
		return $this->m_intAllowAdminUpdate;
	}

	public function sqlAllowAdminUpdate() {
		return ( true == isset( $this->m_intAllowAdminUpdate ) ) ? ( string ) $this->m_intAllowAdminUpdate : '0';
	}

	public function setAllowOriginUpdate( $intAllowOriginUpdate ) {
		$this->set( 'm_intAllowOriginUpdate', CStrings::strToIntDef( $intAllowOriginUpdate, NULL, false ) );
	}

	public function getAllowOriginUpdate() {
		return $this->m_intAllowOriginUpdate;
	}

	public function sqlAllowOriginUpdate() {
		return ( true == isset( $this->m_intAllowOriginUpdate ) ) ? ( string ) $this->m_intAllowOriginUpdate : '0';
	}

	public function setShowInMergeDisplay( $intShowInMergeDisplay ) {
		$this->set( 'm_intShowInMergeDisplay', CStrings::strToIntDef( $intShowInMergeDisplay, NULL, false ) );
	}

	public function getShowInMergeDisplay() {
		return $this->m_intShowInMergeDisplay;
	}

	public function sqlShowInMergeDisplay() {
		return ( true == isset( $this->m_intShowInMergeDisplay ) ) ? ( string ) $this->m_intShowInMergeDisplay : '1';
	}

	public function setAllowDefaultText( $intAllowDefaultText ) {
		$this->set( 'm_intAllowDefaultText', CStrings::strToIntDef( $intAllowDefaultText, NULL, false ) );
	}

	public function getAllowDefaultText() {
		return $this->m_intAllowDefaultText;
	}

	public function sqlAllowDefaultText() {
		return ( true == isset( $this->m_intAllowDefaultText ) ) ? ( string ) $this->m_intAllowDefaultText : '1';
	}

	public function setIsLocked( $intIsLocked ) {
		$this->set( 'm_intIsLocked', CStrings::strToIntDef( $intIsLocked, NULL, false ) );
	}

	public function getIsLocked() {
		return $this->m_intIsLocked;
	}

	public function sqlIsLocked() {
		return ( true == isset( $this->m_intIsLocked ) ) ? ( string ) $this->m_intIsLocked : '0';
	}

	public function setIsBlockField( $intIsBlockField ) {
		$this->set( 'm_intIsBlockField', CStrings::strToIntDef( $intIsBlockField, NULL, false ) );
	}

	public function getIsBlockField() {
		return $this->m_intIsBlockField;
	}

	public function sqlIsBlockField() {
		return ( true == isset( $this->m_intIsBlockField ) ) ? ( string ) $this->m_intIsBlockField : '0';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, merge_field_type_id, block_default_merge_field_id, merge_field_group_id, merge_field_sub_group_id, external_merge_field_group_id, field, alias_names, title, default_value, view_expression, required_input_table, edit_expression, data_type, allow_user_update, allow_admin_update, allow_origin_update, show_in_merge_display, allow_default_text, is_locked, is_block_field, is_required, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlMergeFieldTypeId() . ', ' .
						$this->sqlBlockDefaultMergeFieldId() . ', ' .
						$this->sqlMergeFieldGroupId() . ', ' .
						$this->sqlMergeFieldSubGroupId() . ', ' .
						$this->sqlExternalMergeFieldGroupId() . ', ' .
						$this->sqlField() . ', ' .
						$this->sqlAliasNames() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDefaultValue() . ', ' .
						$this->sqlViewExpression() . ', ' .
						$this->sqlRequiredInputTable() . ', ' .
						$this->sqlEditExpression() . ', ' .
						$this->sqlDataType() . ', ' .
						$this->sqlAllowUserUpdate() . ', ' .
						$this->sqlAllowAdminUpdate() . ', ' .
						$this->sqlAllowOriginUpdate() . ', ' .
						$this->sqlShowInMergeDisplay() . ', ' .
						$this->sqlAllowDefaultText() . ', ' .
						$this->sqlIsLocked() . ', ' .
						$this->sqlIsBlockField() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_type_id = ' . $this->sqlMergeFieldTypeId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldTypeId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_type_id = ' . $this->sqlMergeFieldTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_default_merge_field_id = ' . $this->sqlBlockDefaultMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'BlockDefaultMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' block_default_merge_field_id = ' . $this->sqlBlockDefaultMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldSubGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_merge_field_group_id = ' . $this->sqlExternalMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'ExternalMergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' external_merge_field_group_id = ' . $this->sqlExternalMergeFieldGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' field = ' . $this->sqlField(). ',' ; } elseif( true == array_key_exists( 'Field', $this->getChangedColumns() ) ) { $strSql .= ' field = ' . $this->sqlField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alias_names = ' . $this->sqlAliasNames(). ',' ; } elseif( true == array_key_exists( 'AliasNames', $this->getChangedColumns() ) ) { $strSql .= ' alias_names = ' . $this->sqlAliasNames() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue(). ',' ; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression(). ',' ; } elseif( true == array_key_exists( 'ViewExpression', $this->getChangedColumns() ) ) { $strSql .= ' view_expression = ' . $this->sqlViewExpression() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_input_table = ' . $this->sqlRequiredInputTable(). ',' ; } elseif( true == array_key_exists( 'RequiredInputTable', $this->getChangedColumns() ) ) { $strSql .= ' required_input_table = ' . $this->sqlRequiredInputTable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' edit_expression = ' . $this->sqlEditExpression(). ',' ; } elseif( true == array_key_exists( 'EditExpression', $this->getChangedColumns() ) ) { $strSql .= ' edit_expression = ' . $this->sqlEditExpression() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_type = ' . $this->sqlDataType(). ',' ; } elseif( true == array_key_exists( 'DataType', $this->getChangedColumns() ) ) { $strSql .= ' data_type = ' . $this->sqlDataType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowUserUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_user_update = ' . $this->sqlAllowUserUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_admin_update = ' . $this->sqlAllowAdminUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowAdminUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_admin_update = ' . $this->sqlAllowAdminUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_origin_update = ' . $this->sqlAllowOriginUpdate(). ',' ; } elseif( true == array_key_exists( 'AllowOriginUpdate', $this->getChangedColumns() ) ) { $strSql .= ' allow_origin_update = ' . $this->sqlAllowOriginUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay(). ',' ; } elseif( true == array_key_exists( 'ShowInMergeDisplay', $this->getChangedColumns() ) ) { $strSql .= ' show_in_merge_display = ' . $this->sqlShowInMergeDisplay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_default_text = ' . $this->sqlAllowDefaultText(). ',' ; } elseif( true == array_key_exists( 'AllowDefaultText', $this->getChangedColumns() ) ) { $strSql .= ' allow_default_text = ' . $this->sqlAllowDefaultText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked(). ',' ; } elseif( true == array_key_exists( 'IsLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField(). ',' ; } elseif( true == array_key_exists( 'IsBlockField', $this->getChangedColumns() ) ) { $strSql .= ' is_block_field = ' . $this->sqlIsBlockField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'merge_field_type_id' => $this->getMergeFieldTypeId(),
			'block_default_merge_field_id' => $this->getBlockDefaultMergeFieldId(),
			'merge_field_group_id' => $this->getMergeFieldGroupId(),
			'merge_field_sub_group_id' => $this->getMergeFieldSubGroupId(),
			'external_merge_field_group_id' => $this->getExternalMergeFieldGroupId(),
			'field' => $this->getField(),
			'alias_names' => $this->getAliasNames(),
			'title' => $this->getTitle(),
			'default_value' => $this->getDefaultValue(),
			'view_expression' => $this->getViewExpression(),
			'required_input_table' => $this->getRequiredInputTable(),
			'edit_expression' => $this->getEditExpression(),
			'data_type' => $this->getDataType(),
			'allow_user_update' => $this->getAllowUserUpdate(),
			'allow_admin_update' => $this->getAllowAdminUpdate(),
			'allow_origin_update' => $this->getAllowOriginUpdate(),
			'show_in_merge_display' => $this->getShowInMergeDisplay(),
			'allow_default_text' => $this->getAllowDefaultText(),
			'is_locked' => $this->getIsLocked(),
			'is_block_field' => $this->getIsBlockField(),
			'is_required' => $this->getIsRequired(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>