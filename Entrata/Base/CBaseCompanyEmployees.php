<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEmployees extends CEosPluralBase {

	/**
	 * @return CCompanyEmployee[]
	 */
	public static function fetchCompanyEmployees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyEmployee::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEmployee
	 */
	public static function fetchCompanyEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEmployee::class, $objDatabase );
	}

	public static function fetchCompanyEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employees', $objDatabase );
	}

	public static function fetchCompanyEmployeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployee( sprintf( 'SELECT * FROM company_employees WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByEmployeeStatusTypeIdByCid( $intEmployeeStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE employee_status_type_id = %d AND cid = %d', $intEmployeeStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByCompanyManagerIdByCid( $intCompanyManagerId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE company_manager_id = %d AND cid = %d', $intCompanyManagerId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByRoleReferenceIdByCid( $intRoleReferenceId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE role_reference_id = %d AND cid = %d', $intRoleReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByPhoneNumber1TypeIdByCid( $intPhoneNumber1TypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE phone_number1_type_id = %d AND cid = %d', $intPhoneNumber1TypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEmployeesByPhoneNumber2TypeIdByCid( $intPhoneNumber2TypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEmployees( sprintf( 'SELECT * FROM company_employees WHERE phone_number2_type_id = %d AND cid = %d', $intPhoneNumber2TypeId, $intCid ), $objDatabase );
	}

}
?>