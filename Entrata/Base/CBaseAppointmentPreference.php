<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAppointmentPreference extends CEosSingularBase {

	const TABLE_NAME = 'public.appointment_preferences';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyUserId;
	protected $m_intDayOfWeek;
	protected $m_strAvailableStart;
	protected $m_strAvailableEnd;
	protected $m_intMaxSimultaneousAppointments;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAppointmentPreferenceType;

	public function __construct() {
		parent::__construct();

		$this->m_strAppointmentPreferenceType = 'Staff';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['day_of_week'] ) && $boolDirectSet ) $this->set( 'm_intDayOfWeek', trim( $arrValues['day_of_week'] ) ); elseif( isset( $arrValues['day_of_week'] ) ) $this->setDayOfWeek( $arrValues['day_of_week'] );
		if( isset( $arrValues['available_start'] ) && $boolDirectSet ) $this->set( 'm_strAvailableStart', trim( $arrValues['available_start'] ) ); elseif( isset( $arrValues['available_start'] ) ) $this->setAvailableStart( $arrValues['available_start'] );
		if( isset( $arrValues['available_end'] ) && $boolDirectSet ) $this->set( 'm_strAvailableEnd', trim( $arrValues['available_end'] ) ); elseif( isset( $arrValues['available_end'] ) ) $this->setAvailableEnd( $arrValues['available_end'] );
		if( isset( $arrValues['max_simultaneous_appointments'] ) && $boolDirectSet ) $this->set( 'm_intMaxSimultaneousAppointments', trim( $arrValues['max_simultaneous_appointments'] ) ); elseif( isset( $arrValues['max_simultaneous_appointments'] ) ) $this->setMaxSimultaneousAppointments( $arrValues['max_simultaneous_appointments'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['appointment_preference_type'] ) && $boolDirectSet ) $this->set( 'm_strAppointmentPreferenceType', trim( stripcslashes( $arrValues['appointment_preference_type'] ) ) ); elseif( isset( $arrValues['appointment_preference_type'] ) ) $this->setAppointmentPreferenceType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['appointment_preference_type'] ) : $arrValues['appointment_preference_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setDayOfWeek( $intDayOfWeek ) {
		$this->set( 'm_intDayOfWeek', CStrings::strToIntDef( $intDayOfWeek, NULL, false ) );
	}

	public function getDayOfWeek() {
		return $this->m_intDayOfWeek;
	}

	public function sqlDayOfWeek() {
		return ( true == isset( $this->m_intDayOfWeek ) ) ? ( string ) $this->m_intDayOfWeek : 'NULL';
	}

	public function setAvailableStart( $strAvailableStart ) {
		$this->set( 'm_strAvailableStart', CStrings::strTrimDef( $strAvailableStart, NULL, NULL, true ) );
	}

	public function getAvailableStart() {
		return $this->m_strAvailableStart;
	}

	public function sqlAvailableStart() {
		return ( true == isset( $this->m_strAvailableStart ) ) ? '\'' . $this->m_strAvailableStart . '\'' : 'NULL';
	}

	public function setAvailableEnd( $strAvailableEnd ) {
		$this->set( 'm_strAvailableEnd', CStrings::strTrimDef( $strAvailableEnd, NULL, NULL, true ) );
	}

	public function getAvailableEnd() {
		return $this->m_strAvailableEnd;
	}

	public function sqlAvailableEnd() {
		return ( true == isset( $this->m_strAvailableEnd ) ) ? '\'' . $this->m_strAvailableEnd . '\'' : 'NULL';
	}

	public function setMaxSimultaneousAppointments( $intMaxSimultaneousAppointments ) {
		$this->set( 'm_intMaxSimultaneousAppointments', CStrings::strToIntDef( $intMaxSimultaneousAppointments, NULL, false ) );
	}

	public function getMaxSimultaneousAppointments() {
		return $this->m_intMaxSimultaneousAppointments;
	}

	public function sqlMaxSimultaneousAppointments() {
		return ( true == isset( $this->m_intMaxSimultaneousAppointments ) ) ? ( string ) $this->m_intMaxSimultaneousAppointments : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAppointmentPreferenceType( $strAppointmentPreferenceType ) {
		$this->set( 'm_strAppointmentPreferenceType', CStrings::strTrimDef( $strAppointmentPreferenceType, -1, NULL, true ) );
	}

	public function getAppointmentPreferenceType() {
		return $this->m_strAppointmentPreferenceType;
	}

	public function sqlAppointmentPreferenceType() {
		return ( true == isset( $this->m_strAppointmentPreferenceType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAppointmentPreferenceType ) : '\'' . addslashes( $this->m_strAppointmentPreferenceType ) . '\'' ) : '\'Staff\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, company_user_id, day_of_week, available_start, available_end, max_simultaneous_appointments, updated_by, updated_on, created_by, created_on, appointment_preference_type )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlDayOfWeek() . ', ' .
						$this->sqlAvailableStart() . ', ' .
						$this->sqlAvailableEnd() . ', ' .
						$this->sqlMaxSimultaneousAppointments() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAppointmentPreferenceType() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek(). ',' ; } elseif( true == array_key_exists( 'DayOfWeek', $this->getChangedColumns() ) ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_start = ' . $this->sqlAvailableStart(). ',' ; } elseif( true == array_key_exists( 'AvailableStart', $this->getChangedColumns() ) ) { $strSql .= ' available_start = ' . $this->sqlAvailableStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_end = ' . $this->sqlAvailableEnd(). ',' ; } elseif( true == array_key_exists( 'AvailableEnd', $this->getChangedColumns() ) ) { $strSql .= ' available_end = ' . $this->sqlAvailableEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_simultaneous_appointments = ' . $this->sqlMaxSimultaneousAppointments(). ',' ; } elseif( true == array_key_exists( 'MaxSimultaneousAppointments', $this->getChangedColumns() ) ) { $strSql .= ' max_simultaneous_appointments = ' . $this->sqlMaxSimultaneousAppointments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' appointment_preference_type = ' . $this->sqlAppointmentPreferenceType(). ',' ; } elseif( true == array_key_exists( 'AppointmentPreferenceType', $this->getChangedColumns() ) ) { $strSql .= ' appointment_preference_type = ' . $this->sqlAppointmentPreferenceType() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_user_id' => $this->getCompanyUserId(),
			'day_of_week' => $this->getDayOfWeek(),
			'available_start' => $this->getAvailableStart(),
			'available_end' => $this->getAvailableEnd(),
			'max_simultaneous_appointments' => $this->getMaxSimultaneousAppointments(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'appointment_preference_type' => $this->getAppointmentPreferenceType()
		);
	}

}
?>