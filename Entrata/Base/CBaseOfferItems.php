<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferItems extends CEosPluralBase {

	/**
	 * @return COfferItem[]
	 */
	public static function fetchOfferItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COfferItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COfferItem
	 */
	public static function fetchOfferItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfferItem::class, $objDatabase );
	}

	public static function fetchOfferItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offer_items', $objDatabase );
	}

	public static function fetchOfferItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOfferItem( sprintf( 'SELECT * FROM offer_items WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemsByCid( $intCid, $objDatabase ) {
		return self::fetchOfferItems( sprintf( 'SELECT * FROM offer_items WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemsByOfferIdByCid( $intOfferId, $intCid, $objDatabase ) {
		return self::fetchOfferItems( sprintf( 'SELECT * FROM offer_items WHERE offer_id = %d AND cid = %d', ( int ) $intOfferId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
		return self::fetchOfferItems( sprintf( 'SELECT * FROM offer_items WHERE ar_origin_id = %d AND cid = %d', ( int ) $intArOriginId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {
		return self::fetchOfferItems( sprintf( 'SELECT * FROM offer_items WHERE ar_origin_reference_id = %d AND cid = %d', ( int ) $intArOriginReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemsByImportOfferItemIdByCid( $intImportOfferItemId, $intCid, $objDatabase ) {
		return self::fetchOfferItems( sprintf( 'SELECT * FROM offer_items WHERE import_offer_item_id = %d AND cid = %d', ( int ) $intImportOfferItemId, ( int ) $intCid ), $objDatabase );
	}

}
?>