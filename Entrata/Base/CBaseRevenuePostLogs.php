<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenuePostLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenuePostLogs extends CEosPluralBase {

	/**
	 * @return CRevenuePostLog[]
	 */
	public static function fetchRevenuePostLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenuePostLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenuePostLog
	 */
	public static function fetchRevenuePostLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenuePostLog', $objDatabase );
	}

	public static function fetchRevenuePostLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_post_logs', $objDatabase );
	}

	public static function fetchRevenuePostLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostLog( sprintf( 'SELECT * FROM revenue_post_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostLogsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenuePostLogs( sprintf( 'SELECT * FROM revenue_post_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostLogs( sprintf( 'SELECT * FROM revenue_post_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostLogs( sprintf( 'SELECT * FROM revenue_post_logs WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>