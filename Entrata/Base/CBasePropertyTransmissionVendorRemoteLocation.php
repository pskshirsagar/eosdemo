<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransmissionVendorRemoteLocation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.property_transmission_vendor_remote_locations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intTransmissionVendorId;
	protected $m_intPropertyTransmissionVendorRemoteLocationLogId;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strPropertyInfoEncrypted;
	protected $m_strRemoteLocationKey;
	protected $m_strSubscriptionRemoteKey;
	protected $m_strActivatedOn;
	protected $m_strRejectedOn;
	protected $m_strCancelledOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['property_transmission_vendor_remote_location_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTransmissionVendorRemoteLocationLogId', trim( $arrValues['property_transmission_vendor_remote_location_log_id'] ) ); elseif( isset( $arrValues['property_transmission_vendor_remote_location_log_id'] ) ) $this->setPropertyTransmissionVendorRemoteLocationLogId( $arrValues['property_transmission_vendor_remote_location_log_id'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( stripcslashes( $arrValues['username_encrypted'] ) ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username_encrypted'] ) : $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['property_info_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPropertyInfoEncrypted', trim( stripcslashes( $arrValues['property_info_encrypted'] ) ) ); elseif( isset( $arrValues['property_info_encrypted'] ) ) $this->setPropertyInfoEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_info_encrypted'] ) : $arrValues['property_info_encrypted'] );
		if( isset( $arrValues['remote_location_key'] ) && $boolDirectSet ) $this->set( 'm_strRemoteLocationKey', trim( stripcslashes( $arrValues['remote_location_key'] ) ) ); elseif( isset( $arrValues['remote_location_key'] ) ) $this->setRemoteLocationKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_location_key'] ) : $arrValues['remote_location_key'] );
		if( isset( $arrValues['subscription_remote_key'] ) && $boolDirectSet ) $this->set( 'm_strSubscriptionRemoteKey', trim( stripcslashes( $arrValues['subscription_remote_key'] ) ) ); elseif( isset( $arrValues['subscription_remote_key'] ) ) $this->setSubscriptionRemoteKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subscription_remote_key'] ) : $arrValues['subscription_remote_key'] );
		if( isset( $arrValues['activated_on'] ) && $boolDirectSet ) $this->set( 'm_strActivatedOn', trim( $arrValues['activated_on'] ) ); elseif( isset( $arrValues['activated_on'] ) ) $this->setActivatedOn( $arrValues['activated_on'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setPropertyTransmissionVendorRemoteLocationLogId( $intPropertyTransmissionVendorRemoteLocationLogId ) {
		$this->set( 'm_intPropertyTransmissionVendorRemoteLocationLogId', CStrings::strToIntDef( $intPropertyTransmissionVendorRemoteLocationLogId, NULL, false ) );
	}

	public function getPropertyTransmissionVendorRemoteLocationLogId() {
		return $this->m_intPropertyTransmissionVendorRemoteLocationLogId;
	}

	public function sqlPropertyTransmissionVendorRemoteLocationLogId() {
		return ( true == isset( $this->m_intPropertyTransmissionVendorRemoteLocationLogId ) ) ? ( string ) $this->m_intPropertyTransmissionVendorRemoteLocationLogId : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, 250, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 250, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setPropertyInfoEncrypted( $strPropertyInfoEncrypted ) {
		$this->set( 'm_strPropertyInfoEncrypted', CStrings::strTrimDef( $strPropertyInfoEncrypted, 1000, NULL, true ) );
	}

	public function getPropertyInfoEncrypted() {
		return $this->m_strPropertyInfoEncrypted;
	}

	public function sqlPropertyInfoEncrypted() {
		return ( true == isset( $this->m_strPropertyInfoEncrypted ) ) ? '\'' . addslashes( $this->m_strPropertyInfoEncrypted ) . '\'' : 'NULL';
	}

	public function setRemoteLocationKey( $strRemoteLocationKey ) {
		$this->set( 'm_strRemoteLocationKey', CStrings::strTrimDef( $strRemoteLocationKey, 250, NULL, true ) );
	}

	public function getRemoteLocationKey() {
		return $this->m_strRemoteLocationKey;
	}

	public function sqlRemoteLocationKey() {
		return ( true == isset( $this->m_strRemoteLocationKey ) ) ? '\'' . addslashes( $this->m_strRemoteLocationKey ) . '\'' : 'NULL';
	}

	public function setSubscriptionRemoteKey( $strSubscriptionRemoteKey ) {
		$this->set( 'm_strSubscriptionRemoteKey', CStrings::strTrimDef( $strSubscriptionRemoteKey, 250, NULL, true ) );
	}

	public function getSubscriptionRemoteKey() {
		return $this->m_strSubscriptionRemoteKey;
	}

	public function sqlSubscriptionRemoteKey() {
		return ( true == isset( $this->m_strSubscriptionRemoteKey ) ) ? '\'' . addslashes( $this->m_strSubscriptionRemoteKey ) . '\'' : 'NULL';
	}

	public function setActivatedOn( $strActivatedOn ) {
		$this->set( 'm_strActivatedOn', CStrings::strTrimDef( $strActivatedOn, -1, NULL, true ) );
	}

	public function getActivatedOn() {
		return $this->m_strActivatedOn;
	}

	public function sqlActivatedOn() {
		return ( true == isset( $this->m_strActivatedOn ) ) ? '\'' . $this->m_strActivatedOn . '\'' : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, transmission_vendor_id, property_transmission_vendor_remote_location_log_id, username_encrypted, password_encrypted, property_info_encrypted, remote_location_key, subscription_remote_key, activated_on, rejected_on, cancelled_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlPropertyTransmissionVendorRemoteLocationLogId() . ', ' .
						$this->sqlUsernameEncrypted() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlPropertyInfoEncrypted() . ', ' .
						$this->sqlRemoteLocationKey() . ', ' .
						$this->sqlSubscriptionRemoteKey() . ', ' .
						$this->sqlActivatedOn() . ', ' .
						$this->sqlRejectedOn() . ', ' .
						$this->sqlCancelledOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_transmission_vendor_remote_location_log_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationLogId(). ',' ; } elseif( true == array_key_exists( 'PropertyTransmissionVendorRemoteLocationLogId', $this->getChangedColumns() ) ) { $strSql .= ' property_transmission_vendor_remote_location_log_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_info_encrypted = ' . $this->sqlPropertyInfoEncrypted(). ',' ; } elseif( true == array_key_exists( 'PropertyInfoEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' property_info_encrypted = ' . $this->sqlPropertyInfoEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_location_key = ' . $this->sqlRemoteLocationKey(). ',' ; } elseif( true == array_key_exists( 'RemoteLocationKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_location_key = ' . $this->sqlRemoteLocationKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_remote_key = ' . $this->sqlSubscriptionRemoteKey(). ',' ; } elseif( true == array_key_exists( 'SubscriptionRemoteKey', $this->getChangedColumns() ) ) { $strSql .= ' subscription_remote_key = ' . $this->sqlSubscriptionRemoteKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn(). ',' ; } elseif( true == array_key_exists( 'ActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'property_transmission_vendor_remote_location_log_id' => $this->getPropertyTransmissionVendorRemoteLocationLogId(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'property_info_encrypted' => $this->getPropertyInfoEncrypted(),
			'remote_location_key' => $this->getRemoteLocationKey(),
			'subscription_remote_key' => $this->getSubscriptionRemoteKey(),
			'activated_on' => $this->getActivatedOn(),
			'rejected_on' => $this->getRejectedOn(),
			'cancelled_on' => $this->getCancelledOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>