<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyInsuranceCarriers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyInsuranceCarriers extends CEosPluralBase {

	/**
	 * @return CPropertyInsuranceCarrier[]
	 */
	public static function fetchPropertyInsuranceCarriers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyInsuranceCarrier', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyInsuranceCarrier
	 */
	public static function fetchPropertyInsuranceCarrier( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyInsuranceCarrier', $objDatabase );
	}

	public static function fetchPropertyInsuranceCarrierCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_insurance_carriers', $objDatabase );
	}

	public static function fetchPropertyInsuranceCarrierByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyInsuranceCarrier( sprintf( 'SELECT * FROM property_insurance_carriers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInsuranceCarriersByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyInsuranceCarriers( sprintf( 'SELECT * FROM property_insurance_carriers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInsuranceCarriersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyInsuranceCarriers( sprintf( 'SELECT * FROM property_insurance_carriers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>