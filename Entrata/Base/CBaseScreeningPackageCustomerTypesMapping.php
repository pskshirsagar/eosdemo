<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageCustomerTypesMapping extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_package_customer_types_mappings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intCustomerRelationshipId;
	protected $m_intDefaultScreeningPackageId;
	protected $m_intScreeningApplicantRequirementTypeId;
	protected $m_intIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningApplicantRequirementTypeId = '1';
		$this->m_intIsActive = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTypeId', trim( $arrValues['lease_type_id'] ) ); elseif( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipId', trim( $arrValues['customer_relationship_id'] ) ); elseif( isset( $arrValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrValues['customer_relationship_id'] );
		if( isset( $arrValues['default_screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultScreeningPackageId', trim( $arrValues['default_screening_package_id'] ) ); elseif( isset( $arrValues['default_screening_package_id'] ) ) $this->setDefaultScreeningPackageId( $arrValues['default_screening_package_id'] );
		if( isset( $arrValues['screening_applicant_requirement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantRequirementTypeId', trim( $arrValues['screening_applicant_requirement_type_id'] ) ); elseif( isset( $arrValues['screening_applicant_requirement_type_id'] ) ) $this->setScreeningApplicantRequirementTypeId( $arrValues['screening_applicant_requirement_type_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseTypeId( $intLeaseTypeId ) {
		$this->set( 'm_intLeaseTypeId', CStrings::strToIntDef( $intLeaseTypeId, NULL, false ) );
	}

	public function getLeaseTypeId() {
		return $this->m_intLeaseTypeId;
	}

	public function sqlLeaseTypeId() {
		return ( true == isset( $this->m_intLeaseTypeId ) ) ? ( string ) $this->m_intLeaseTypeId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->set( 'm_intCustomerRelationshipId', CStrings::strToIntDef( $intCustomerRelationshipId, NULL, false ) );
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function sqlCustomerRelationshipId() {
		return ( true == isset( $this->m_intCustomerRelationshipId ) ) ? ( string ) $this->m_intCustomerRelationshipId : 'NULL';
	}

	public function setDefaultScreeningPackageId( $intDefaultScreeningPackageId ) {
		$this->set( 'm_intDefaultScreeningPackageId', CStrings::strToIntDef( $intDefaultScreeningPackageId, NULL, false ) );
	}

	public function getDefaultScreeningPackageId() {
		return $this->m_intDefaultScreeningPackageId;
	}

	public function sqlDefaultScreeningPackageId() {
		return ( true == isset( $this->m_intDefaultScreeningPackageId ) ) ? ( string ) $this->m_intDefaultScreeningPackageId : 'NULL';
	}

	public function setScreeningApplicantRequirementTypeId( $intScreeningApplicantRequirementTypeId ) {
		$this->set( 'm_intScreeningApplicantRequirementTypeId', CStrings::strToIntDef( $intScreeningApplicantRequirementTypeId, NULL, false ) );
	}

	public function getScreeningApplicantRequirementTypeId() {
		return $this->m_intScreeningApplicantRequirementTypeId;
	}

	public function sqlScreeningApplicantRequirementTypeId() {
		return ( true == isset( $this->m_intScreeningApplicantRequirementTypeId ) ) ? ( string ) $this->m_intScreeningApplicantRequirementTypeId : '1';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_type_id, customer_type_id, customer_relationship_id, default_screening_package_id, screening_applicant_requirement_type_id, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeaseTypeId() . ', ' .
 						$this->sqlCustomerTypeId() . ', ' .
 						$this->sqlCustomerRelationshipId() . ', ' .
 						$this->sqlDefaultScreeningPackageId() . ', ' .
 						$this->sqlScreeningApplicantRequirementTypeId() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; } elseif( true == array_key_exists( 'LeaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; } elseif( true == array_key_exists( 'CustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_screening_package_id = ' . $this->sqlDefaultScreeningPackageId() . ','; } elseif( true == array_key_exists( 'DefaultScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' default_screening_package_id = ' . $this->sqlDefaultScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_requirement_type_id = ' . $this->sqlScreeningApplicantRequirementTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicantRequirementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_requirement_type_id = ' . $this->sqlScreeningApplicantRequirementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_type_id' => $this->getLeaseTypeId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'customer_relationship_id' => $this->getCustomerRelationshipId(),
			'default_screening_package_id' => $this->getDefaultScreeningPackageId(),
			'screening_applicant_requirement_type_id' => $this->getScreeningApplicantRequirementTypeId(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>