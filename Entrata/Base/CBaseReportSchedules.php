<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportSchedules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportSchedules extends CEosPluralBase {

	/**
	 * @return CReportSchedule[]
	 */
	public static function fetchReportSchedules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportSchedule::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportSchedule
	 */
	public static function fetchReportSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportSchedule::class, $objDatabase );
	}

	public static function fetchReportScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_schedules', $objDatabase );
	}

	public static function fetchReportScheduleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportSchedule( sprintf( 'SELECT * FROM report_schedules WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByCid( $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportScheduleTypeIdByCid( $intReportScheduleTypeId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_schedule_type_id = %d AND cid = %d', $intReportScheduleTypeId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_group_id = %d AND cid = %d', $intReportGroupId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportNewGroupIdByCid( $intReportNewGroupId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_new_group_id = %d AND cid = %d', $intReportNewGroupId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportNewInstanceIdByCid( $intReportNewInstanceId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_new_instance_id = %d AND cid = %d', $intReportNewInstanceId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_filter_id = %d AND cid = %d', $intReportFilterId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByReportVersionIdByCid( $intReportVersionId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE report_version_id = %d AND cid = %d', $intReportVersionId, $intCid ), $objDatabase );
	}

	public static function fetchReportSchedulesByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchReportSchedules( sprintf( 'SELECT * FROM report_schedules WHERE scheduled_task_id = %d AND cid = %d', $intScheduledTaskId, $intCid ), $objDatabase );
	}

}
?>