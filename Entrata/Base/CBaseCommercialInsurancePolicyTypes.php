<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialInsurancePolicyTypes
 * Do not add any new functions to this class.
 */

class CBaseCommercialInsurancePolicyTypes extends CEosPluralBase {

	/**
	 * @return CCommercialInsurancePolicyType[]
	 */
	public static function fetchCommercialInsurancePolicyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCommercialInsurancePolicyType::class, $objDatabase );
	}

	/**
	 * @return CCommercialInsurancePolicyType
	 */
	public static function fetchCommercialInsurancePolicyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCommercialInsurancePolicyType::class, $objDatabase );
	}

	public static function fetchCommercialInsurancePolicyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_insurance_policy_types', $objDatabase );
	}

	public static function fetchCommercialInsurancePolicyTypeById( $intId, $objDatabase ) {
		return self::fetchCommercialInsurancePolicyType( sprintf( 'SELECT * FROM commercial_insurance_policy_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>