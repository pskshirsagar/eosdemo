<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceProblems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceProblems extends CEosPluralBase {

	/**
	 * @return CMaintenanceProblem[]
	 */
	public static function fetchMaintenanceProblems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceProblem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceProblem
	 */
	public static function fetchMaintenanceProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceProblem::class, $objDatabase );
	}

	public static function fetchMaintenanceProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_problems', $objDatabase );
	}

	public static function fetchMaintenanceProblemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblem( sprintf( 'SELECT * FROM maintenance_problems WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE integration_database_id = %d AND cid = %d', $intIntegrationDatabaseId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE maintenance_problem_id = %d AND cid = %d', $intMaintenanceProblemId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByScheduledApTransactionHeaderIdByCid( $intScheduledApTransactionHeaderId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE scheduled_ap_transaction_header_id = %d AND cid = %d', $intScheduledApTransactionHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByMaintenanceProblemTypeIdByCid( $intMaintenanceProblemTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE maintenance_problem_type_id = %d AND cid = %d', $intMaintenanceProblemTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemsByMaintenancePriorityIdByCid( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblems( sprintf( 'SELECT * FROM maintenance_problems WHERE maintenance_priority_id = %d AND cid = %d', $intMaintenancePriorityId, $intCid ), $objDatabase );
	}

}
?>