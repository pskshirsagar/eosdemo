<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItemTypes
 * Do not add any new functions to this class.
 */

class CBaseChecklistItemTypes extends CEosPluralBase {

	/**
	 * @return CChecklistItemType[]
	 */
	public static function fetchChecklistItemTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChecklistItemType::class, $objDatabase );
	}

	/**
	 * @return CChecklistItemType
	 */
	public static function fetchChecklistItemType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklistItemType::class, $objDatabase );
	}

	public static function fetchChecklistItemTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklist_item_types', $objDatabase );
	}

	public static function fetchChecklistItemTypeById( $intId, $objDatabase ) {
		return self::fetchChecklistItemType( sprintf( 'SELECT * FROM checklist_item_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>