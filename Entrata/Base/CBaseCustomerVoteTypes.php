<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVoteTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerVoteTypes extends CEosPluralBase {

	/**
	 * @return CCustomerVoteType[]
	 */
	public static function fetchCustomerVoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerVoteType::class, $objDatabase );
	}

	/**
	 * @return CCustomerVoteType
	 */
	public static function fetchCustomerVoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerVoteType::class, $objDatabase );
	}

	public static function fetchCustomerVoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_vote_types', $objDatabase );
	}

	public static function fetchCustomerVoteTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerVoteType( sprintf( 'SELECT * FROM customer_vote_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>