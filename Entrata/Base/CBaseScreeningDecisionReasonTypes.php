<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningDecisionReasonTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDecisionReasonTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDecisionReasonType[]
	 */
	public static function fetchScreeningDecisionReasonTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningDecisionReasonType::class, $objDatabase );
	}

	/**
	 * @return CScreeningDecisionReasonType
	 */
	public static function fetchScreeningDecisionReasonType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningDecisionReasonType::class, $objDatabase );
	}

	public static function fetchScreeningDecisionReasonTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_decision_reason_types', $objDatabase );
	}

	public static function fetchScreeningDecisionReasonTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDecisionReasonType( sprintf( 'SELECT * FROM screening_decision_reason_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningDecisionReasonTypesByScreenTypeId( $intScreenTypeId, $objDatabase ) {
		return self::fetchScreeningDecisionReasonTypes( sprintf( 'SELECT * FROM screening_decision_reason_types WHERE screen_type_id = %d', $intScreenTypeId ), $objDatabase );
	}

}
?>