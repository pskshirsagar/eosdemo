<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueSupplyDemands
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueSupplyDemands extends CEosPluralBase {

	/**
	 * @return CRevenueSupplyDemand[]
	 */
	public static function fetchRevenueSupplyDemands( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueSupplyDemand', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueSupplyDemand
	 */
	public static function fetchRevenueSupplyDemand( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueSupplyDemand', $objDatabase );
	}

	public static function fetchRevenueSupplyDemandCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_supply_demands', $objDatabase );
	}

	public static function fetchRevenueSupplyDemandByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueSupplyDemand( sprintf( 'SELECT * FROM revenue_supply_demands WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueSupplyDemandsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueSupplyDemands( sprintf( 'SELECT * FROM revenue_supply_demands WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueSupplyDemandsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueSupplyDemands( sprintf( 'SELECT * FROM revenue_supply_demands WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueSupplyDemandsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueSupplyDemands( sprintf( 'SELECT * FROM revenue_supply_demands WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>