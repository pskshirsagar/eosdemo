<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyHolidays
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyHolidays extends CEosPluralBase {

	/**
	 * @return CPropertyHoliday[]
	 */
	public static function fetchPropertyHolidays( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyHoliday', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyHoliday
	 */
	public static function fetchPropertyHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyHoliday', $objDatabase );
	}

	public static function fetchPropertyHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_holidays', $objDatabase );
	}

	public static function fetchPropertyHolidayByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyHoliday( sprintf( 'SELECT * FROM property_holidays WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyHolidaysByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyHolidays( sprintf( 'SELECT * FROM property_holidays WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyHolidaysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyHolidays( sprintf( 'SELECT * FROM property_holidays WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyHolidaysByCompanyHolidayIdByCid( $intCompanyHolidayId, $intCid, $objDatabase ) {
		return self::fetchPropertyHolidays( sprintf( 'SELECT * FROM property_holidays WHERE company_holiday_id = %d AND cid = %d', ( int ) $intCompanyHolidayId, ( int ) $intCid ), $objDatabase );
	}

}
?>