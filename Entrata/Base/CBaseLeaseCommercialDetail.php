<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCommercialDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_commercial_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intMasterLeaseId;
	protected $m_intCustomerId;
	protected $m_intNoticePriorCustomerAddressId;
	protected $m_intNoticeAfterCustomerAddressId;
	protected $m_strLookupCode;
	protected $m_strCobrokerName;
	protected $m_strCobrokerPhone;
	protected $m_strCobrokerAddress;
	protected $m_strBaseTaxYear;
	protected $m_strBaseExpensesYear;
	protected $m_strTenantDescription;
	protected $m_strOperatingHours;
	protected $m_strCamExpenses;
	protected $m_boolIsResponsibleUtilities;
	protected $m_boolIsResponsibleJanitorial;
	protected $m_boolIsResponsibleSecurity;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['master_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intMasterLeaseId', trim( $arrValues['master_lease_id'] ) ); elseif( isset( $arrValues['master_lease_id'] ) ) $this->setMasterLeaseId( $arrValues['master_lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['notice_prior_customer_address_id'] ) && $boolDirectSet ) $this->set( 'm_intNoticePriorCustomerAddressId', trim( $arrValues['notice_prior_customer_address_id'] ) ); elseif( isset( $arrValues['notice_prior_customer_address_id'] ) ) $this->setNoticePriorCustomerAddressId( $arrValues['notice_prior_customer_address_id'] );
		if( isset( $arrValues['notice_after_customer_address_id'] ) && $boolDirectSet ) $this->set( 'm_intNoticeAfterCustomerAddressId', trim( $arrValues['notice_after_customer_address_id'] ) ); elseif( isset( $arrValues['notice_after_customer_address_id'] ) ) $this->setNoticeAfterCustomerAddressId( $arrValues['notice_after_customer_address_id'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( $arrValues['lookup_code'] ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( $arrValues['lookup_code'] );
		if( isset( $arrValues['cobroker_name'] ) && $boolDirectSet ) $this->set( 'm_strCobrokerName', trim( $arrValues['cobroker_name'] ) ); elseif( isset( $arrValues['cobroker_name'] ) ) $this->setCobrokerName( $arrValues['cobroker_name'] );
		if( isset( $arrValues['cobroker_phone'] ) && $boolDirectSet ) $this->set( 'm_strCobrokerPhone', trim( $arrValues['cobroker_phone'] ) ); elseif( isset( $arrValues['cobroker_phone'] ) ) $this->setCobrokerPhone( $arrValues['cobroker_phone'] );
		if( isset( $arrValues['cobroker_address'] ) && $boolDirectSet ) $this->set( 'm_strCobrokerAddress', trim( $arrValues['cobroker_address'] ) ); elseif( isset( $arrValues['cobroker_address'] ) ) $this->setCobrokerAddress( $arrValues['cobroker_address'] );
		if( isset( $arrValues['base_tax_year'] ) && $boolDirectSet ) $this->set( 'm_strBaseTaxYear', trim( $arrValues['base_tax_year'] ) ); elseif( isset( $arrValues['base_tax_year'] ) ) $this->setBaseTaxYear( $arrValues['base_tax_year'] );
		if( isset( $arrValues['base_expenses_year'] ) && $boolDirectSet ) $this->set( 'm_strBaseExpensesYear', trim( $arrValues['base_expenses_year'] ) ); elseif( isset( $arrValues['base_expenses_year'] ) ) $this->setBaseExpensesYear( $arrValues['base_expenses_year'] );
		if( isset( $arrValues['tenant_description'] ) && $boolDirectSet ) $this->set( 'm_strTenantDescription', trim( $arrValues['tenant_description'] ) ); elseif( isset( $arrValues['tenant_description'] ) ) $this->setTenantDescription( $arrValues['tenant_description'] );
		if( isset( $arrValues['operating_hours'] ) && $boolDirectSet ) $this->set( 'm_strOperatingHours', trim( $arrValues['operating_hours'] ) ); elseif( isset( $arrValues['operating_hours'] ) ) $this->setOperatingHours( $arrValues['operating_hours'] );
		if( isset( $arrValues['cam_expenses'] ) && $boolDirectSet ) $this->set( 'm_strCamExpenses', trim( $arrValues['cam_expenses'] ) ); elseif( isset( $arrValues['cam_expenses'] ) ) $this->setCamExpenses( $arrValues['cam_expenses'] );
		if( isset( $arrValues['is_responsible_utilities'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponsibleUtilities', trim( stripcslashes( $arrValues['is_responsible_utilities'] ) ) ); elseif( isset( $arrValues['is_responsible_utilities'] ) ) $this->setIsResponsibleUtilities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_utilities'] ) : $arrValues['is_responsible_utilities'] );
		if( isset( $arrValues['is_responsible_janitorial'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponsibleJanitorial', trim( stripcslashes( $arrValues['is_responsible_janitorial'] ) ) ); elseif( isset( $arrValues['is_responsible_janitorial'] ) ) $this->setIsResponsibleJanitorial( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_janitorial'] ) : $arrValues['is_responsible_janitorial'] );
		if( isset( $arrValues['is_responsible_security'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponsibleSecurity', trim( stripcslashes( $arrValues['is_responsible_security'] ) ) ); elseif( isset( $arrValues['is_responsible_security'] ) ) $this->setIsResponsibleSecurity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_security'] ) : $arrValues['is_responsible_security'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setMasterLeaseId( $intMasterLeaseId ) {
		$this->set( 'm_intMasterLeaseId', CStrings::strToIntDef( $intMasterLeaseId, NULL, false ) );
	}

	public function getMasterLeaseId() {
		return $this->m_intMasterLeaseId;
	}

	public function sqlMasterLeaseId() {
		return ( true == isset( $this->m_intMasterLeaseId ) ) ? ( string ) $this->m_intMasterLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setNoticePriorCustomerAddressId( $intNoticePriorCustomerAddressId ) {
		$this->set( 'm_intNoticePriorCustomerAddressId', CStrings::strToIntDef( $intNoticePriorCustomerAddressId, NULL, false ) );
	}

	public function getNoticePriorCustomerAddressId() {
		return $this->m_intNoticePriorCustomerAddressId;
	}

	public function sqlNoticePriorCustomerAddressId() {
		return ( true == isset( $this->m_intNoticePriorCustomerAddressId ) ) ? ( string ) $this->m_intNoticePriorCustomerAddressId : 'NULL';
	}

	public function setNoticeAfterCustomerAddressId( $intNoticeAfterCustomerAddressId ) {
		$this->set( 'm_intNoticeAfterCustomerAddressId', CStrings::strToIntDef( $intNoticeAfterCustomerAddressId, NULL, false ) );
	}

	public function getNoticeAfterCustomerAddressId() {
		return $this->m_intNoticeAfterCustomerAddressId;
	}

	public function sqlNoticeAfterCustomerAddressId() {
		return ( true == isset( $this->m_intNoticeAfterCustomerAddressId ) ) ? ( string ) $this->m_intNoticeAfterCustomerAddressId : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 64, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLookupCode ) : '\'' . addslashes( $this->m_strLookupCode ) . '\'' ) : 'NULL';
	}

	public function setCobrokerName( $strCobrokerName ) {
		$this->set( 'm_strCobrokerName', CStrings::strTrimDef( $strCobrokerName, 250, NULL, true ) );
	}

	public function getCobrokerName() {
		return $this->m_strCobrokerName;
	}

	public function sqlCobrokerName() {
		return ( true == isset( $this->m_strCobrokerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCobrokerName ) : '\'' . addslashes( $this->m_strCobrokerName ) . '\'' ) : 'NULL';
	}

	public function setCobrokerPhone( $strCobrokerPhone ) {
		$this->set( 'm_strCobrokerPhone', CStrings::strTrimDef( $strCobrokerPhone, 30, NULL, true ) );
	}

	public function getCobrokerPhone() {
		return $this->m_strCobrokerPhone;
	}

	public function sqlCobrokerPhone() {
		return ( true == isset( $this->m_strCobrokerPhone ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCobrokerPhone ) : '\'' . addslashes( $this->m_strCobrokerPhone ) . '\'' ) : 'NULL';
	}

	public function setCobrokerAddress( $strCobrokerAddress ) {
		$this->set( 'm_strCobrokerAddress', CStrings::strTrimDef( $strCobrokerAddress, 250, NULL, true ) );
	}

	public function getCobrokerAddress() {
		return $this->m_strCobrokerAddress;
	}

	public function sqlCobrokerAddress() {
		return ( true == isset( $this->m_strCobrokerAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCobrokerAddress ) : '\'' . addslashes( $this->m_strCobrokerAddress ) . '\'' ) : 'NULL';
	}

	public function setBaseTaxYear( $strBaseTaxYear ) {
		$this->set( 'm_strBaseTaxYear', CStrings::strTrimDef( $strBaseTaxYear, -1, NULL, true ) );
	}

	public function getBaseTaxYear() {
		return $this->m_strBaseTaxYear;
	}

	public function sqlBaseTaxYear() {
		return ( true == isset( $this->m_strBaseTaxYear ) ) ? '\'' . $this->m_strBaseTaxYear . '\'' : 'NULL';
	}

	public function setBaseExpensesYear( $strBaseExpensesYear ) {
		$this->set( 'm_strBaseExpensesYear', CStrings::strTrimDef( $strBaseExpensesYear, -1, NULL, true ) );
	}

	public function getBaseExpensesYear() {
		return $this->m_strBaseExpensesYear;
	}

	public function sqlBaseExpensesYear() {
		return ( true == isset( $this->m_strBaseExpensesYear ) ) ? '\'' . $this->m_strBaseExpensesYear . '\'' : 'NULL';
	}

	public function setTenantDescription( $strTenantDescription ) {
		$this->set( 'm_strTenantDescription', CStrings::strTrimDef( $strTenantDescription, -1, NULL, true ) );
	}

	public function getTenantDescription() {
		return $this->m_strTenantDescription;
	}

	public function sqlTenantDescription() {
		return ( true == isset( $this->m_strTenantDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTenantDescription ) : '\'' . addslashes( $this->m_strTenantDescription ) . '\'' ) : 'NULL';
	}

	public function setOperatingHours( $strOperatingHours ) {
		$this->set( 'm_strOperatingHours', CStrings::strTrimDef( $strOperatingHours, -1, NULL, true ) );
	}

	public function getOperatingHours() {
		return $this->m_strOperatingHours;
	}

	public function sqlOperatingHours() {
		return ( true == isset( $this->m_strOperatingHours ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOperatingHours ) : '\'' . addslashes( $this->m_strOperatingHours ) . '\'' ) : 'NULL';
	}

	public function setCamExpenses( $strCamExpenses ) {
		$this->set( 'm_strCamExpenses', CStrings::strTrimDef( $strCamExpenses, -1, NULL, true ) );
	}

	public function getCamExpenses() {
		return $this->m_strCamExpenses;
	}

	public function sqlCamExpenses() {
		return ( true == isset( $this->m_strCamExpenses ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCamExpenses ) : '\'' . addslashes( $this->m_strCamExpenses ) . '\'' ) : 'NULL';
	}

	public function setIsResponsibleUtilities( $boolIsResponsibleUtilities ) {
		$this->set( 'm_boolIsResponsibleUtilities', CStrings::strToBool( $boolIsResponsibleUtilities ) );
	}

	public function getIsResponsibleUtilities() {
		return $this->m_boolIsResponsibleUtilities;
	}

	public function sqlIsResponsibleUtilities() {
		return ( true == isset( $this->m_boolIsResponsibleUtilities ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleUtilities ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponsibleJanitorial( $boolIsResponsibleJanitorial ) {
		$this->set( 'm_boolIsResponsibleJanitorial', CStrings::strToBool( $boolIsResponsibleJanitorial ) );
	}

	public function getIsResponsibleJanitorial() {
		return $this->m_boolIsResponsibleJanitorial;
	}

	public function sqlIsResponsibleJanitorial() {
		return ( true == isset( $this->m_boolIsResponsibleJanitorial ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleJanitorial ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponsibleSecurity( $boolIsResponsibleSecurity ) {
		$this->set( 'm_boolIsResponsibleSecurity', CStrings::strToBool( $boolIsResponsibleSecurity ) );
	}

	public function getIsResponsibleSecurity() {
		return $this->m_boolIsResponsibleSecurity;
	}

	public function sqlIsResponsibleSecurity() {
		return ( true == isset( $this->m_boolIsResponsibleSecurity ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleSecurity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, master_lease_id, customer_id, notice_prior_customer_address_id, notice_after_customer_address_id, lookup_code, cobroker_name, cobroker_phone, cobroker_address, base_tax_year, base_expenses_year, tenant_description, operating_hours, cam_expenses, is_responsible_utilities, is_responsible_janitorial, is_responsible_security, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlMasterLeaseId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlNoticePriorCustomerAddressId() . ', ' .
						$this->sqlNoticeAfterCustomerAddressId() . ', ' .
						$this->sqlLookupCode() . ', ' .
						$this->sqlCobrokerName() . ', ' .
						$this->sqlCobrokerPhone() . ', ' .
						$this->sqlCobrokerAddress() . ', ' .
						$this->sqlBaseTaxYear() . ', ' .
						$this->sqlBaseExpensesYear() . ', ' .
						$this->sqlTenantDescription() . ', ' .
						$this->sqlOperatingHours() . ', ' .
						$this->sqlCamExpenses() . ', ' .
						$this->sqlIsResponsibleUtilities() . ', ' .
						$this->sqlIsResponsibleJanitorial() . ', ' .
						$this->sqlIsResponsibleSecurity() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' master_lease_id = ' . $this->sqlMasterLeaseId(). ',' ; } elseif( true == array_key_exists( 'MasterLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' master_lease_id = ' . $this->sqlMasterLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_prior_customer_address_id = ' . $this->sqlNoticePriorCustomerAddressId(). ',' ; } elseif( true == array_key_exists( 'NoticePriorCustomerAddressId', $this->getChangedColumns() ) ) { $strSql .= ' notice_prior_customer_address_id = ' . $this->sqlNoticePriorCustomerAddressId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_after_customer_address_id = ' . $this->sqlNoticeAfterCustomerAddressId(). ',' ; } elseif( true == array_key_exists( 'NoticeAfterCustomerAddressId', $this->getChangedColumns() ) ) { $strSql .= ' notice_after_customer_address_id = ' . $this->sqlNoticeAfterCustomerAddressId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode(). ',' ; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cobroker_name = ' . $this->sqlCobrokerName(). ',' ; } elseif( true == array_key_exists( 'CobrokerName', $this->getChangedColumns() ) ) { $strSql .= ' cobroker_name = ' . $this->sqlCobrokerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cobroker_phone = ' . $this->sqlCobrokerPhone(). ',' ; } elseif( true == array_key_exists( 'CobrokerPhone', $this->getChangedColumns() ) ) { $strSql .= ' cobroker_phone = ' . $this->sqlCobrokerPhone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cobroker_address = ' . $this->sqlCobrokerAddress(). ',' ; } elseif( true == array_key_exists( 'CobrokerAddress', $this->getChangedColumns() ) ) { $strSql .= ' cobroker_address = ' . $this->sqlCobrokerAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax_year = ' . $this->sqlBaseTaxYear(). ',' ; } elseif( true == array_key_exists( 'BaseTaxYear', $this->getChangedColumns() ) ) { $strSql .= ' base_tax_year = ' . $this->sqlBaseTaxYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_expenses_year = ' . $this->sqlBaseExpensesYear(). ',' ; } elseif( true == array_key_exists( 'BaseExpensesYear', $this->getChangedColumns() ) ) { $strSql .= ' base_expenses_year = ' . $this->sqlBaseExpensesYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tenant_description = ' . $this->sqlTenantDescription(). ',' ; } elseif( true == array_key_exists( 'TenantDescription', $this->getChangedColumns() ) ) { $strSql .= ' tenant_description = ' . $this->sqlTenantDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' operating_hours = ' . $this->sqlOperatingHours(). ',' ; } elseif( true == array_key_exists( 'OperatingHours', $this->getChangedColumns() ) ) { $strSql .= ' operating_hours = ' . $this->sqlOperatingHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cam_expenses = ' . $this->sqlCamExpenses(). ',' ; } elseif( true == array_key_exists( 'CamExpenses', $this->getChangedColumns() ) ) { $strSql .= ' cam_expenses = ' . $this->sqlCamExpenses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_utilities = ' . $this->sqlIsResponsibleUtilities(). ',' ; } elseif( true == array_key_exists( 'IsResponsibleUtilities', $this->getChangedColumns() ) ) { $strSql .= ' is_responsible_utilities = ' . $this->sqlIsResponsibleUtilities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_janitorial = ' . $this->sqlIsResponsibleJanitorial(). ',' ; } elseif( true == array_key_exists( 'IsResponsibleJanitorial', $this->getChangedColumns() ) ) { $strSql .= ' is_responsible_janitorial = ' . $this->sqlIsResponsibleJanitorial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_security = ' . $this->sqlIsResponsibleSecurity(). ',' ; } elseif( true == array_key_exists( 'IsResponsibleSecurity', $this->getChangedColumns() ) ) { $strSql .= ' is_responsible_security = ' . $this->sqlIsResponsibleSecurity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'master_lease_id' => $this->getMasterLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'notice_prior_customer_address_id' => $this->getNoticePriorCustomerAddressId(),
			'notice_after_customer_address_id' => $this->getNoticeAfterCustomerAddressId(),
			'lookup_code' => $this->getLookupCode(),
			'cobroker_name' => $this->getCobrokerName(),
			'cobroker_phone' => $this->getCobrokerPhone(),
			'cobroker_address' => $this->getCobrokerAddress(),
			'base_tax_year' => $this->getBaseTaxYear(),
			'base_expenses_year' => $this->getBaseExpensesYear(),
			'tenant_description' => $this->getTenantDescription(),
			'operating_hours' => $this->getOperatingHours(),
			'cam_expenses' => $this->getCamExpenses(),
			'is_responsible_utilities' => $this->getIsResponsibleUtilities(),
			'is_responsible_janitorial' => $this->getIsResponsibleJanitorial(),
			'is_responsible_security' => $this->getIsResponsibleSecurity(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>