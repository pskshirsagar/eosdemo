<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.asset_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intAssetTransactionTypeId;
	protected $m_intApCodeId;
	protected $m_intUnitOfMeasureId;
	protected $m_intDisplayUnitOfMeasureId;
	protected $m_intPoApDetailId;
	protected $m_intInvoiceApDetailId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intMaintenanceRequestId;
	protected $m_intAssetId;
	protected $m_intAssetLocationId;
	protected $m_intAssetTransactionId;
	protected $m_intUnitSpaceId;
	protected $m_intMaintenanceLocationId;
	protected $m_intPeriodId;
	protected $m_intMaintenanceRequestMaterialId;
	protected $m_fltUnitCost;
	protected $m_fltQuantity;
	protected $m_fltDisplayQuantity;
	protected $m_fltCachedRemaining;
	protected $m_strMemo;
	protected $m_fltTransactionAmount;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_boolIsConsumed;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltQuantity = '1';
		$this->m_fltCachedRemaining = ( 0 );
		$this->m_strPostDate = 'now()';
		$this->m_boolIsConsumed = false;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['asset_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetTransactionTypeId', trim( $arrValues['asset_transaction_type_id'] ) ); elseif( isset( $arrValues['asset_transaction_type_id'] ) ) $this->setAssetTransactionTypeId( $arrValues['asset_transaction_type_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitOfMeasureId', trim( $arrValues['unit_of_measure_id'] ) ); elseif( isset( $arrValues['unit_of_measure_id'] ) ) $this->setUnitOfMeasureId( $arrValues['unit_of_measure_id'] );
		if( isset( $arrValues['display_unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intDisplayUnitOfMeasureId', trim( $arrValues['display_unit_of_measure_id'] ) ); elseif( isset( $arrValues['display_unit_of_measure_id'] ) ) $this->setDisplayUnitOfMeasureId( $arrValues['display_unit_of_measure_id'] );
		if( isset( $arrValues['po_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intPoApDetailId', trim( $arrValues['po_ap_detail_id'] ) ); elseif( isset( $arrValues['po_ap_detail_id'] ) ) $this->setPoApDetailId( $arrValues['po_ap_detail_id'] );
		if( isset( $arrValues['invoice_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceApDetailId', trim( $arrValues['invoice_ap_detail_id'] ) ); elseif( isset( $arrValues['invoice_ap_detail_id'] ) ) $this->setInvoiceApDetailId( $arrValues['invoice_ap_detail_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['asset_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetId', trim( $arrValues['asset_id'] ) ); elseif( isset( $arrValues['asset_id'] ) ) $this->setAssetId( $arrValues['asset_id'] );
		if( isset( $arrValues['asset_location_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetLocationId', trim( $arrValues['asset_location_id'] ) ); elseif( isset( $arrValues['asset_location_id'] ) ) $this->setAssetLocationId( $arrValues['asset_location_id'] );
		if( isset( $arrValues['asset_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetTransactionId', trim( $arrValues['asset_transaction_id'] ) ); elseif( isset( $arrValues['asset_transaction_id'] ) ) $this->setAssetTransactionId( $arrValues['asset_transaction_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationId', trim( $arrValues['maintenance_location_id'] ) ); elseif( isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['maintenance_request_material_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestMaterialId', trim( $arrValues['maintenance_request_material_id'] ) ); elseif( isset( $arrValues['maintenance_request_material_id'] ) ) $this->setMaintenanceRequestMaterialId( $arrValues['maintenance_request_material_id'] );
		if( isset( $arrValues['unit_cost'] ) && $boolDirectSet ) $this->set( 'm_fltUnitCost', trim( $arrValues['unit_cost'] ) ); elseif( isset( $arrValues['unit_cost'] ) ) $this->setUnitCost( $arrValues['unit_cost'] );
		if( isset( $arrValues['quantity'] ) && $boolDirectSet ) $this->set( 'm_fltQuantity', trim( $arrValues['quantity'] ) ); elseif( isset( $arrValues['quantity'] ) ) $this->setQuantity( $arrValues['quantity'] );
		if( isset( $arrValues['display_quantity'] ) && $boolDirectSet ) $this->set( 'm_fltDisplayQuantity', trim( $arrValues['display_quantity'] ) ); elseif( isset( $arrValues['display_quantity'] ) ) $this->setDisplayQuantity( $arrValues['display_quantity'] );
		if( isset( $arrValues['cached_remaining'] ) && $boolDirectSet ) $this->set( 'm_fltCachedRemaining', trim( $arrValues['cached_remaining'] ) ); elseif( isset( $arrValues['cached_remaining'] ) ) $this->setCachedRemaining( $arrValues['cached_remaining'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['is_consumed'] ) && $boolDirectSet ) $this->set( 'm_boolIsConsumed', trim( stripcslashes( $arrValues['is_consumed'] ) ) ); elseif( isset( $arrValues['is_consumed'] ) ) $this->setIsConsumed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_consumed'] ) : $arrValues['is_consumed'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setAssetTransactionTypeId( $intAssetTransactionTypeId ) {
		$this->set( 'm_intAssetTransactionTypeId', CStrings::strToIntDef( $intAssetTransactionTypeId, NULL, false ) );
	}

	public function getAssetTransactionTypeId() {
		return $this->m_intAssetTransactionTypeId;
	}

	public function sqlAssetTransactionTypeId() {
		return ( true == isset( $this->m_intAssetTransactionTypeId ) ) ? ( string ) $this->m_intAssetTransactionTypeId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setUnitOfMeasureId( $intUnitOfMeasureId ) {
		$this->set( 'm_intUnitOfMeasureId', CStrings::strToIntDef( $intUnitOfMeasureId, NULL, false ) );
	}

	public function getUnitOfMeasureId() {
		return $this->m_intUnitOfMeasureId;
	}

	public function sqlUnitOfMeasureId() {
		return ( true == isset( $this->m_intUnitOfMeasureId ) ) ? ( string ) $this->m_intUnitOfMeasureId : 'NULL';
	}

	public function setDisplayUnitOfMeasureId( $intDisplayUnitOfMeasureId ) {
		$this->set( 'm_intDisplayUnitOfMeasureId', CStrings::strToIntDef( $intDisplayUnitOfMeasureId, NULL, false ) );
	}

	public function getDisplayUnitOfMeasureId() {
		return $this->m_intDisplayUnitOfMeasureId;
	}

	public function sqlDisplayUnitOfMeasureId() {
		return ( true == isset( $this->m_intDisplayUnitOfMeasureId ) ) ? ( string ) $this->m_intDisplayUnitOfMeasureId : 'NULL';
	}

	public function setPoApDetailId( $intPoApDetailId ) {
		$this->set( 'm_intPoApDetailId', CStrings::strToIntDef( $intPoApDetailId, NULL, false ) );
	}

	public function getPoApDetailId() {
		return $this->m_intPoApDetailId;
	}

	public function sqlPoApDetailId() {
		return ( true == isset( $this->m_intPoApDetailId ) ) ? ( string ) $this->m_intPoApDetailId : 'NULL';
	}

	public function setInvoiceApDetailId( $intInvoiceApDetailId ) {
		$this->set( 'm_intInvoiceApDetailId', CStrings::strToIntDef( $intInvoiceApDetailId, NULL, false ) );
	}

	public function getInvoiceApDetailId() {
		return $this->m_intInvoiceApDetailId;
	}

	public function sqlInvoiceApDetailId() {
		return ( true == isset( $this->m_intInvoiceApDetailId ) ) ? ( string ) $this->m_intInvoiceApDetailId : 'NULL';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setAssetId( $intAssetId ) {
		$this->set( 'm_intAssetId', CStrings::strToIntDef( $intAssetId, NULL, false ) );
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function sqlAssetId() {
		return ( true == isset( $this->m_intAssetId ) ) ? ( string ) $this->m_intAssetId : 'NULL';
	}

	public function setAssetLocationId( $intAssetLocationId ) {
		$this->set( 'm_intAssetLocationId', CStrings::strToIntDef( $intAssetLocationId, NULL, false ) );
	}

	public function getAssetLocationId() {
		return $this->m_intAssetLocationId;
	}

	public function sqlAssetLocationId() {
		return ( true == isset( $this->m_intAssetLocationId ) ) ? ( string ) $this->m_intAssetLocationId : 'NULL';
	}

	public function setAssetTransactionId( $intAssetTransactionId ) {
		$this->set( 'm_intAssetTransactionId', CStrings::strToIntDef( $intAssetTransactionId, NULL, false ) );
	}

	public function getAssetTransactionId() {
		return $this->m_intAssetTransactionId;
	}

	public function sqlAssetTransactionId() {
		return ( true == isset( $this->m_intAssetTransactionId ) ) ? ( string ) $this->m_intAssetTransactionId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->set( 'm_intMaintenanceLocationId', CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false ) );
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function sqlMaintenanceLocationId() {
		return ( true == isset( $this->m_intMaintenanceLocationId ) ) ? ( string ) $this->m_intMaintenanceLocationId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setMaintenanceRequestMaterialId( $intMaintenanceRequestMaterialId ) {
		$this->set( 'm_intMaintenanceRequestMaterialId', CStrings::strToIntDef( $intMaintenanceRequestMaterialId, NULL, false ) );
	}

	public function getMaintenanceRequestMaterialId() {
		return $this->m_intMaintenanceRequestMaterialId;
	}

	public function sqlMaintenanceRequestMaterialId() {
		return ( true == isset( $this->m_intMaintenanceRequestMaterialId ) ) ? ( string ) $this->m_intMaintenanceRequestMaterialId : 'NULL';
	}

	public function setUnitCost( $fltUnitCost ) {
		$this->set( 'm_fltUnitCost', CStrings::strToFloatDef( $fltUnitCost, NULL, false, 2 ) );
	}

	public function getUnitCost() {
		return $this->m_fltUnitCost;
	}

	public function sqlUnitCost() {
		return ( true == isset( $this->m_fltUnitCost ) ) ? ( string ) $this->m_fltUnitCost : 'NULL';
	}

	public function setQuantity( $fltQuantity ) {
		$this->set( 'm_fltQuantity', CStrings::strToFloatDef( $fltQuantity, NULL, false, 3 ) );
	}

	public function getQuantity() {
		return $this->m_fltQuantity;
	}

	public function sqlQuantity() {
		return ( true == isset( $this->m_fltQuantity ) ) ? ( string ) $this->m_fltQuantity : '1';
	}

	public function setDisplayQuantity( $fltDisplayQuantity ) {
		$this->set( 'm_fltDisplayQuantity', CStrings::strToFloatDef( $fltDisplayQuantity, NULL, false, 3 ) );
	}

	public function getDisplayQuantity() {
		return $this->m_fltDisplayQuantity;
	}

	public function sqlDisplayQuantity() {
		return ( true == isset( $this->m_fltDisplayQuantity ) ) ? ( string ) $this->m_fltDisplayQuantity : 'NULL';
	}

	public function setCachedRemaining( $fltCachedRemaining ) {
		$this->set( 'm_fltCachedRemaining', CStrings::strToFloatDef( $fltCachedRemaining, NULL, false, 3 ) );
	}

	public function getCachedRemaining() {
		return $this->m_fltCachedRemaining;
	}

	public function sqlCachedRemaining() {
		return ( true == isset( $this->m_fltCachedRemaining ) ) ? ( string ) $this->m_fltCachedRemaining : '( 0 )::numeric';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 255, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setIsConsumed( $boolIsConsumed ) {
		$this->set( 'm_boolIsConsumed', CStrings::strToBool( $boolIsConsumed ) );
	}

	public function getIsConsumed() {
		return $this->m_boolIsConsumed;
	}

	public function sqlIsConsumed() {
		return ( true == isset( $this->m_boolIsConsumed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConsumed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, asset_transaction_type_id, ap_code_id, unit_of_measure_id, display_unit_of_measure_id, po_ap_detail_id, invoice_ap_detail_id, debit_gl_account_id, credit_gl_account_id, maintenance_request_id, asset_id, asset_location_id, asset_transaction_id, unit_space_id, maintenance_location_id, period_id, maintenance_request_material_id, unit_cost, quantity, display_quantity, cached_remaining, memo, transaction_amount, post_date, post_month, is_consumed, is_posted, is_initial_import, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlAssetTransactionTypeId() . ', ' .
 						$this->sqlApCodeId() . ', ' .
 						$this->sqlUnitOfMeasureId() . ', ' .
 						$this->sqlDisplayUnitOfMeasureId() . ', ' .
 						$this->sqlPoApDetailId() . ', ' .
 						$this->sqlInvoiceApDetailId() . ', ' .
 						$this->sqlDebitGlAccountId() . ', ' .
 						$this->sqlCreditGlAccountId() . ', ' .
 						$this->sqlMaintenanceRequestId() . ', ' .
 						$this->sqlAssetId() . ', ' .
 						$this->sqlAssetLocationId() . ', ' .
 						$this->sqlAssetTransactionId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlMaintenanceLocationId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlMaintenanceRequestMaterialId() . ', ' .
 						$this->sqlUnitCost() . ', ' .
 						$this->sqlQuantity() . ', ' .
 						$this->sqlDisplayQuantity() . ', ' .
 						$this->sqlCachedRemaining() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlTransactionAmount() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlIsConsumed() . ', ' .
 						$this->sqlIsPosted() . ', ' .
 						$this->sqlIsInitialImport() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_transaction_type_id = ' . $this->sqlAssetTransactionTypeId() . ','; } elseif( true == array_key_exists( 'AssetTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' asset_transaction_type_id = ' . $this->sqlAssetTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId() . ','; } elseif( true == array_key_exists( 'UnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_unit_of_measure_id = ' . $this->sqlDisplayUnitOfMeasureId() . ','; } elseif( true == array_key_exists( 'DisplayUnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' display_unit_of_measure_id = ' . $this->sqlDisplayUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId() . ','; } elseif( true == array_key_exists( 'PoApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_ap_detail_id = ' . $this->sqlInvoiceApDetailId() . ','; } elseif( true == array_key_exists( 'InvoiceApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_ap_detail_id = ' . $this->sqlInvoiceApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; } elseif( true == array_key_exists( 'DebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; } elseif( true == array_key_exists( 'CreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; } elseif( true == array_key_exists( 'AssetId', $this->getChangedColumns() ) ) { $strSql .= ' asset_id = ' . $this->sqlAssetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_location_id = ' . $this->sqlAssetLocationId() . ','; } elseif( true == array_key_exists( 'AssetLocationId', $this->getChangedColumns() ) ) { $strSql .= ' asset_location_id = ' . $this->sqlAssetLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_transaction_id = ' . $this->sqlAssetTransactionId() . ','; } elseif( true == array_key_exists( 'AssetTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' asset_transaction_id = ' . $this->sqlAssetTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId() . ','; } elseif( true == array_key_exists( 'MaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_material_id = ' . $this->sqlMaintenanceRequestMaterialId() . ','; } elseif( true == array_key_exists( 'MaintenanceRequestMaterialId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_material_id = ' . $this->sqlMaintenanceRequestMaterialId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_cost = ' . $this->sqlUnitCost() . ','; } elseif( true == array_key_exists( 'UnitCost', $this->getChangedColumns() ) ) { $strSql .= ' unit_cost = ' . $this->sqlUnitCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; } elseif( true == array_key_exists( 'Quantity', $this->getChangedColumns() ) ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_quantity = ' . $this->sqlDisplayQuantity() . ','; } elseif( true == array_key_exists( 'DisplayQuantity', $this->getChangedColumns() ) ) { $strSql .= ' display_quantity = ' . $this->sqlDisplayQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_remaining = ' . $this->sqlCachedRemaining() . ','; } elseif( true == array_key_exists( 'CachedRemaining', $this->getChangedColumns() ) ) { $strSql .= ' cached_remaining = ' . $this->sqlCachedRemaining() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_consumed = ' . $this->sqlIsConsumed() . ','; } elseif( true == array_key_exists( 'IsConsumed', $this->getChangedColumns() ) ) { $strSql .= ' is_consumed = ' . $this->sqlIsConsumed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'asset_transaction_type_id' => $this->getAssetTransactionTypeId(),
			'ap_code_id' => $this->getApCodeId(),
			'unit_of_measure_id' => $this->getUnitOfMeasureId(),
			'display_unit_of_measure_id' => $this->getDisplayUnitOfMeasureId(),
			'po_ap_detail_id' => $this->getPoApDetailId(),
			'invoice_ap_detail_id' => $this->getInvoiceApDetailId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'asset_id' => $this->getAssetId(),
			'asset_location_id' => $this->getAssetLocationId(),
			'asset_transaction_id' => $this->getAssetTransactionId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'maintenance_location_id' => $this->getMaintenanceLocationId(),
			'period_id' => $this->getPeriodId(),
			'maintenance_request_material_id' => $this->getMaintenanceRequestMaterialId(),
			'unit_cost' => $this->getUnitCost(),
			'quantity' => $this->getQuantity(),
			'display_quantity' => $this->getDisplayQuantity(),
			'cached_remaining' => $this->getCachedRemaining(),
			'memo' => $this->getMemo(),
			'transaction_amount' => $this->getTransactionAmount(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'is_consumed' => $this->getIsConsumed(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>