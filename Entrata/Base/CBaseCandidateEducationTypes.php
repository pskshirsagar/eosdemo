<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateEducationTypes
 * Do not add any new functions to this class.
 */

class CBaseCandidateEducationTypes extends CEosPluralBase {

	/**
	 * @return CCandidateEducationType[]
	 */
	public static function fetchCandidateEducationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCandidateEducationType::class, $objDatabase );
	}

	/**
	 * @return CCandidateEducationType
	 */
	public static function fetchCandidateEducationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCandidateEducationType::class, $objDatabase );
	}

	public static function fetchCandidateEducationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_education_types', $objDatabase );
	}

	public static function fetchCandidateEducationTypeById( $intId, $objDatabase ) {
		return self::fetchCandidateEducationType( sprintf( 'SELECT * FROM candidate_education_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>