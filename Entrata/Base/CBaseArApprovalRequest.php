<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArApprovalRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_approval_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intRouteTypeId;
	protected $m_intLeaseId;
	protected $m_intArTransactionId;
	protected $m_intScheduledChargeId;
	protected $m_strRequestDetails;
	protected $m_boolIsArchived;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApprovalStatusTypeId;
	protected $m_intLeaseAccommodationId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsArchived = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['route_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteTypeId', trim( $arrValues['route_type_id'] ) ); elseif( isset( $arrValues['route_type_id'] ) ) $this->setRouteTypeId( $arrValues['route_type_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeId', trim( $arrValues['scheduled_charge_id'] ) ); elseif( isset( $arrValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrValues['scheduled_charge_id'] );
		if( isset( $arrValues['request_details'] ) && $boolDirectSet ) $this->set( 'm_strRequestDetails', trim( stripcslashes( $arrValues['request_details'] ) ) ); elseif( isset( $arrValues['request_details'] ) ) $this->setRequestDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_details'] ) : $arrValues['request_details'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchived', trim( stripcslashes( $arrValues['is_archived'] ) ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived'] ) : $arrValues['is_archived'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['approval_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApprovalStatusTypeId', trim( $arrValues['approval_status_type_id'] ) ); elseif( isset( $arrValues['approval_status_type_id'] ) ) $this->setApprovalStatusTypeId( $arrValues['approval_status_type_id'] );
		if( isset( $arrValues['lease_accommodation_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseAccommodationId', trim( $arrValues['lease_accommodation_id'] ) ); elseif( isset( $arrValues['lease_accommodation_id'] ) ) $this->setLeaseAccommodationId( $arrValues['lease_accommodation_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setRouteTypeId( $intRouteTypeId ) {
		$this->set( 'm_intRouteTypeId', CStrings::strToIntDef( $intRouteTypeId, NULL, false ) );
	}

	public function getRouteTypeId() {
		return $this->m_intRouteTypeId;
	}

	public function sqlRouteTypeId() {
		return ( true == isset( $this->m_intRouteTypeId ) ) ? ( string ) $this->m_intRouteTypeId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->set( 'm_intScheduledChargeId', CStrings::strToIntDef( $intScheduledChargeId, NULL, false ) );
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function sqlScheduledChargeId() {
		return ( true == isset( $this->m_intScheduledChargeId ) ) ? ( string ) $this->m_intScheduledChargeId : 'NULL';
	}

	public function setRequestDetails( $strRequestDetails ) {
		$this->set( 'm_strRequestDetails', CStrings::strTrimDef( $strRequestDetails, -1, NULL, true ) );
	}

	public function getRequestDetails() {
		return $this->m_strRequestDetails;
	}

	public function sqlRequestDetails() {
		return ( true == isset( $this->m_strRequestDetails ) ) ? '\'' . addslashes( $this->m_strRequestDetails ) . '\'' : 'NULL';
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_boolIsArchived ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchived ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApprovalStatusTypeId( $intApprovalStatusTypeId ) {
		$this->set( 'm_intApprovalStatusTypeId', CStrings::strToIntDef( $intApprovalStatusTypeId, NULL, false ) );
	}

	public function getApprovalStatusTypeId() {
		return $this->m_intApprovalStatusTypeId;
	}

	public function sqlApprovalStatusTypeId() {
		return ( true == isset( $this->m_intApprovalStatusTypeId ) ) ? ( string ) $this->m_intApprovalStatusTypeId : 'NULL';
	}

	public function setLeaseAccommodationId( $intLeaseAccommodationId ) {
		$this->set( 'm_intLeaseAccommodationId', CStrings::strToIntDef( $intLeaseAccommodationId, NULL, false ) );
	}

	public function getLeaseAccommodationId() {
		return $this->m_intLeaseAccommodationId;
	}

	public function sqlLeaseAccommodationId() {
		return ( true == isset( $this->m_intLeaseAccommodationId ) ) ? ( string ) $this->m_intLeaseAccommodationId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, route_type_id, lease_id, ar_transaction_id, scheduled_charge_id, request_details, is_archived, updated_by, updated_on, created_by, created_on, approval_status_type_id, lease_accommodation_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlRouteTypeId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlArTransactionId() . ', ' .
 						$this->sqlScheduledChargeId() . ', ' .
 						$this->sqlRequestDetails() . ', ' .
 						$this->sqlIsArchived() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApprovalStatusTypeId() . ', ' .
 						$this->sqlLeaseAccommodationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; } elseif( true == array_key_exists( 'RouteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; } elseif( true == array_key_exists( 'ScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_details = ' . $this->sqlRequestDetails() . ','; } elseif( true == array_key_exists( 'RequestDetails', $this->getChangedColumns() ) ) { $strSql .= ' request_details = ' . $this->sqlRequestDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_status_type_id = ' . $this->sqlApprovalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApprovalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' approval_status_type_id = ' . $this->sqlApprovalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_accommodation_id = ' . $this->sqlLeaseAccommodationId(). ',' ; } elseif( true == array_key_exists( 'LeaseAccommodationId', $this->getChangedColumns() ) ) { $strSql .= ' lease_accommodation_id = ' . $this->sqlLeaseAccommodationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'route_type_id' => $this->getRouteTypeId(),
			'lease_id' => $this->getLeaseId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'scheduled_charge_id' => $this->getScheduledChargeId(),
			'request_details' => $this->getRequestDetails(),
			'is_archived' => $this->getIsArchived(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'approval_status_type_id' => $this->getApprovalStatusTypeId(),
			'lease_accommodation_id' => $this->getLeaseAccommodationId()
		);
	}

}
?>