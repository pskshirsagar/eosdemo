<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitListPoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWaitListPoints extends CEosPluralBase {

	/**
	 * @return CWaitListPoint[]
	 */
	public static function fetchWaitListPoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CWaitListPoint::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CWaitListPoint
	 */
	public static function fetchWaitListPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWaitListPoint::class, $objDatabase );
	}

	public static function fetchWaitListPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'wait_list_points', $objDatabase );
	}

	public static function fetchWaitListPointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchWaitListPoint( sprintf( 'SELECT * FROM wait_list_points WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchWaitListPointsByCid( $intCid, $objDatabase ) {
		return self::fetchWaitListPoints( sprintf( 'SELECT * FROM wait_list_points WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchWaitListPointsByWaitListIdByCid( $intWaitListId, $intCid, $objDatabase ) {
		return self::fetchWaitListPoints( sprintf( 'SELECT * FROM wait_list_points WHERE wait_list_id = %d AND cid = %d', $intWaitListId, $intCid ), $objDatabase );
	}

	public static function fetchWaitListPointsByApplicationSettingKeyIdByCid( $intApplicationSettingKeyId, $intCid, $objDatabase ) {
		return self::fetchWaitListPoints( sprintf( 'SELECT * FROM wait_list_points WHERE application_setting_key_id = %d AND cid = %d', $intApplicationSettingKeyId, $intCid ), $objDatabase );
	}

	public static function fetchWaitListPointsByMergeFieldIdByCid( $intMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchWaitListPoints( sprintf( 'SELECT * FROM wait_list_points WHERE merge_field_id = %d AND cid = %d', $intMergeFieldId, $intCid ), $objDatabase );
	}

}
?>