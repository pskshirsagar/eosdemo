<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobUnitTypePhase extends CEosSingularBase {

	const TABLE_NAME = 'public.job_unit_type_phases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobId;
	protected $m_intUnitTypeId;
	protected $m_intJobPhaseId;
	protected $m_intUnitCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpgradedUnitTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPhaseId', trim( $arrValues['job_phase_id'] ) ); elseif( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['upgraded_unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUpgradedUnitTypeId', trim( $arrValues['upgraded_unit_type_id'] ) ); elseif( isset( $arrValues['upgraded_unit_type_id'] ) ) $this->setUpgradedUnitTypeId( $arrValues['upgraded_unit_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setJobPhaseId( $intJobPhaseId ) {
		$this->set( 'm_intJobPhaseId', CStrings::strToIntDef( $intJobPhaseId, NULL, false ) );
	}

	public function getJobPhaseId() {
		return $this->m_intJobPhaseId;
	}

	public function sqlJobPhaseId() {
		return ( true == isset( $this->m_intJobPhaseId ) ) ? ( string ) $this->m_intJobPhaseId : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpgradedUnitTypeId( $intUpgradedUnitTypeId ) {
		$this->set( 'm_intUpgradedUnitTypeId', CStrings::strToIntDef( $intUpgradedUnitTypeId, NULL, false ) );
	}

	public function getUpgradedUnitTypeId() {
		return $this->m_intUpgradedUnitTypeId;
	}

	public function sqlUpgradedUnitTypeId() {
		return ( true == isset( $this->m_intUpgradedUnitTypeId ) ) ? ( string ) $this->m_intUpgradedUnitTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_id, unit_type_id, job_phase_id, unit_count, updated_by, updated_on, created_by, created_on, upgraded_unit_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlJobId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlJobPhaseId() . ', ' .
						$this->sqlUnitCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUpgradedUnitTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId(). ',' ; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'JobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' upgraded_unit_type_id = ' . $this->sqlUpgradedUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UpgradedUnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' upgraded_unit_type_id = ' . $this->sqlUpgradedUnitTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_id' => $this->getJobId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'job_phase_id' => $this->getJobPhaseId(),
			'unit_count' => $this->getUnitCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'upgraded_unit_type_id' => $this->getUpgradedUnitTypeId()
		);
	}

}
?>