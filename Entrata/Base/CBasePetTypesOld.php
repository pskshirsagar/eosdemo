<?php

class CBasePetTypesOld extends CEosPluralBase {

    const TABLE_PET_TYPES = 'public.pet_types_old';

    public static function fetchPetTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CPetTypeOld', $objDatabase );
    }

    public static function fetchPetType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPetTypeOld', $objDatabase );
    }

    public static function fetchPetTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'pet_types_old', $objDatabase );
    }

    public static function fetchPetTypeById( $intId, $objDatabase ) {
        return self::fetchPetType( sprintf( 'SELECT * FROM pet_types_old WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>