<?php

class CBaseApCamTypes extends CEosPluralBase {

    public static function fetchApCamTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CApCamType', $objDatabase );
    }

    public static function fetchApCamType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApCamType', $objDatabase );
    }

    public static function fetchApCamTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ap_cam_types', $objDatabase );
    }

    public static function fetchApCamTypeById( $intId, $objDatabase ) {
        return self::fetchApCamType( sprintf( 'SELECT * FROM ap_cam_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>