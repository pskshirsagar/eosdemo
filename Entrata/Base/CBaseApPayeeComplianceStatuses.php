<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeComplianceStatuses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeComplianceStatuses extends CEosPluralBase {

	/**
	 * @return CApPayeeComplianceStatus[]
	 */
	public static function fetchApPayeeComplianceStatuses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayeeComplianceStatus::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeComplianceStatus
	 */
	public static function fetchApPayeeComplianceStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeComplianceStatus::class, $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_compliance_statuses', $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatus( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatuses( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatuses( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByApLegalEntityIdByCid( $intApLegalEntityId, $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatuses( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE ap_legal_entity_id = %d AND cid = %d', ( int ) $intApLegalEntityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatuses( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeeComplianceStatusesByComplianceStatusIdByCid( $intComplianceStatusId, $intCid, $objDatabase ) {
		return self::fetchApPayeeComplianceStatuses( sprintf( 'SELECT * FROM ap_payee_compliance_statuses WHERE compliance_status_id = %d AND cid = %d', ( int ) $intComplianceStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>