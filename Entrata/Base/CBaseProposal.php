<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProposal extends CEosSingularBase {

	const TABLE_NAME = 'public.proposals';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProposalStatusTypeId;
	protected $m_intPropertyId;
	protected $m_intJobId;
	protected $m_strRequestName;
	protected $m_strProposalNumber;
	protected $m_fltEstimateAmount;
	protected $m_strBidCloseDate;
	protected $m_strProjectDescription;
	protected $m_strSelectionExplanation;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intProposalStatusTypeId = '1';
		$this->m_fltEstimateAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['proposal_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intProposalStatusTypeId', trim( $arrValues['proposal_status_type_id'] ) ); elseif( isset( $arrValues['proposal_status_type_id'] ) ) $this->setProposalStatusTypeId( $arrValues['proposal_status_type_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['request_name'] ) && $boolDirectSet ) $this->set( 'm_strRequestName', trim( stripcslashes( $arrValues['request_name'] ) ) ); elseif( isset( $arrValues['request_name'] ) ) $this->setRequestName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_name'] ) : $arrValues['request_name'] );
		if( isset( $arrValues['proposal_number'] ) && $boolDirectSet ) $this->set( 'm_strProposalNumber', trim( stripcslashes( $arrValues['proposal_number'] ) ) ); elseif( isset( $arrValues['proposal_number'] ) ) $this->setProposalNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['proposal_number'] ) : $arrValues['proposal_number'] );
		if( isset( $arrValues['estimate_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEstimateAmount', trim( $arrValues['estimate_amount'] ) ); elseif( isset( $arrValues['estimate_amount'] ) ) $this->setEstimateAmount( $arrValues['estimate_amount'] );
		if( isset( $arrValues['bid_close_date'] ) && $boolDirectSet ) $this->set( 'm_strBidCloseDate', trim( $arrValues['bid_close_date'] ) ); elseif( isset( $arrValues['bid_close_date'] ) ) $this->setBidCloseDate( $arrValues['bid_close_date'] );
		if( isset( $arrValues['project_description'] ) && $boolDirectSet ) $this->set( 'm_strProjectDescription', trim( stripcslashes( $arrValues['project_description'] ) ) ); elseif( isset( $arrValues['project_description'] ) ) $this->setProjectDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['project_description'] ) : $arrValues['project_description'] );
		if( isset( $arrValues['selection_explanation'] ) && $boolDirectSet ) $this->set( 'm_strSelectionExplanation', trim( stripcslashes( $arrValues['selection_explanation'] ) ) ); elseif( isset( $arrValues['selection_explanation'] ) ) $this->setSelectionExplanation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['selection_explanation'] ) : $arrValues['selection_explanation'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProposalStatusTypeId( $intProposalStatusTypeId ) {
		$this->set( 'm_intProposalStatusTypeId', CStrings::strToIntDef( $intProposalStatusTypeId, NULL, false ) );
	}

	public function getProposalStatusTypeId() {
		return $this->m_intProposalStatusTypeId;
	}

	public function sqlProposalStatusTypeId() {
		return ( true == isset( $this->m_intProposalStatusTypeId ) ) ? ( string ) $this->m_intProposalStatusTypeId : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setRequestName( $strRequestName ) {
		$this->set( 'm_strRequestName', CStrings::strTrimDef( $strRequestName, 2000, NULL, true ) );
	}

	public function getRequestName() {
		return $this->m_strRequestName;
	}

	public function sqlRequestName() {
		return ( true == isset( $this->m_strRequestName ) ) ? '\'' . addslashes( $this->m_strRequestName ) . '\'' : 'NULL';
	}

	public function setProposalNumber( $strProposalNumber ) {
		$this->set( 'm_strProposalNumber', CStrings::strTrimDef( $strProposalNumber, 2000, NULL, true ) );
	}

	public function getProposalNumber() {
		return $this->m_strProposalNumber;
	}

	public function sqlProposalNumber() {
		return ( true == isset( $this->m_strProposalNumber ) ) ? '\'' . addslashes( $this->m_strProposalNumber ) . '\'' : 'NULL';
	}

	public function setEstimateAmount( $fltEstimateAmount ) {
		$this->set( 'm_fltEstimateAmount', CStrings::strToFloatDef( $fltEstimateAmount, NULL, false, 3 ) );
	}

	public function getEstimateAmount() {
		return $this->m_fltEstimateAmount;
	}

	public function sqlEstimateAmount() {
		return ( true == isset( $this->m_fltEstimateAmount ) ) ? ( string ) $this->m_fltEstimateAmount : '0';
	}

	public function setBidCloseDate( $strBidCloseDate ) {
		$this->set( 'm_strBidCloseDate', CStrings::strTrimDef( $strBidCloseDate, -1, NULL, true ) );
	}

	public function getBidCloseDate() {
		return $this->m_strBidCloseDate;
	}

	public function sqlBidCloseDate() {
		return ( true == isset( $this->m_strBidCloseDate ) ) ? '\'' . $this->m_strBidCloseDate . '\'' : 'NULL';
	}

	public function setProjectDescription( $strProjectDescription ) {
		$this->set( 'm_strProjectDescription', CStrings::strTrimDef( $strProjectDescription, -1, NULL, true ) );
	}

	public function getProjectDescription() {
		return $this->m_strProjectDescription;
	}

	public function sqlProjectDescription() {
		return ( true == isset( $this->m_strProjectDescription ) ) ? '\'' . addslashes( $this->m_strProjectDescription ) . '\'' : 'NULL';
	}

	public function setSelectionExplanation( $strSelectionExplanation ) {
		$this->set( 'm_strSelectionExplanation', CStrings::strTrimDef( $strSelectionExplanation, -1, NULL, true ) );
	}

	public function getSelectionExplanation() {
		return $this->m_strSelectionExplanation;
	}

	public function sqlSelectionExplanation() {
		return ( true == isset( $this->m_strSelectionExplanation ) ) ? '\'' . addslashes( $this->m_strSelectionExplanation ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, proposal_status_type_id, property_id, job_id, request_name, proposal_number, estimate_amount, bid_close_date, project_description, selection_explanation, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlProposalStatusTypeId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlJobId() . ', ' .
 						$this->sqlRequestName() . ', ' .
 						$this->sqlProposalNumber() . ', ' .
 						$this->sqlEstimateAmount() . ', ' .
 						$this->sqlBidCloseDate() . ', ' .
 						$this->sqlProjectDescription() . ', ' .
 						$this->sqlSelectionExplanation() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_status_type_id = ' . $this->sqlProposalStatusTypeId() . ','; } elseif( true == array_key_exists( 'ProposalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' proposal_status_type_id = ' . $this->sqlProposalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_name = ' . $this->sqlRequestName() . ','; } elseif( true == array_key_exists( 'RequestName', $this->getChangedColumns() ) ) { $strSql .= ' request_name = ' . $this->sqlRequestName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_number = ' . $this->sqlProposalNumber() . ','; } elseif( true == array_key_exists( 'ProposalNumber', $this->getChangedColumns() ) ) { $strSql .= ' proposal_number = ' . $this->sqlProposalNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimate_amount = ' . $this->sqlEstimateAmount() . ','; } elseif( true == array_key_exists( 'EstimateAmount', $this->getChangedColumns() ) ) { $strSql .= ' estimate_amount = ' . $this->sqlEstimateAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bid_close_date = ' . $this->sqlBidCloseDate() . ','; } elseif( true == array_key_exists( 'BidCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' bid_close_date = ' . $this->sqlBidCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_description = ' . $this->sqlProjectDescription() . ','; } elseif( true == array_key_exists( 'ProjectDescription', $this->getChangedColumns() ) ) { $strSql .= ' project_description = ' . $this->sqlProjectDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selection_explanation = ' . $this->sqlSelectionExplanation() . ','; } elseif( true == array_key_exists( 'SelectionExplanation', $this->getChangedColumns() ) ) { $strSql .= ' selection_explanation = ' . $this->sqlSelectionExplanation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'proposal_status_type_id' => $this->getProposalStatusTypeId(),
			'property_id' => $this->getPropertyId(),
			'job_id' => $this->getJobId(),
			'request_name' => $this->getRequestName(),
			'proposal_number' => $this->getProposalNumber(),
			'estimate_amount' => $this->getEstimateAmount(),
			'bid_close_date' => $this->getBidCloseDate(),
			'project_description' => $this->getProjectDescription(),
			'selection_explanation' => $this->getSelectionExplanation(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>