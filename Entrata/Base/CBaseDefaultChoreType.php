<?php

class CBaseDefaultChoreType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_chore_types';

	protected $m_intId;
	protected $m_intChoreCategoryId;
	protected $m_strName;
	protected $m_intSendEmail;
	protected $m_intIsRequiredToLockMonth;
	protected $m_intIsRequiredToAdvanceMonth;
	protected $m_intIsPublished;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intSendEmail = '1';
		$this->m_intIsRequiredToLockMonth = '0';
		$this->m_intIsRequiredToAdvanceMonth = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['chore_category_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreCategoryId', trim( $arrValues['chore_category_id'] ) ); elseif( isset( $arrValues['chore_category_id'] ) ) $this->setChoreCategoryId( $arrValues['chore_category_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['send_email'] ) && $boolDirectSet ) $this->set( 'm_intSendEmail', trim( $arrValues['send_email'] ) ); elseif( isset( $arrValues['send_email'] ) ) $this->setSendEmail( $arrValues['send_email'] );
		if( isset( $arrValues['is_required_to_lock_month'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiredToLockMonth', trim( $arrValues['is_required_to_lock_month'] ) ); elseif( isset( $arrValues['is_required_to_lock_month'] ) ) $this->setIsRequiredToLockMonth( $arrValues['is_required_to_lock_month'] );
		if( isset( $arrValues['is_required_to_advance_month'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiredToAdvanceMonth', trim( $arrValues['is_required_to_advance_month'] ) ); elseif( isset( $arrValues['is_required_to_advance_month'] ) ) $this->setIsRequiredToAdvanceMonth( $arrValues['is_required_to_advance_month'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChoreCategoryId( $intChoreCategoryId ) {
		$this->set( 'm_intChoreCategoryId', CStrings::strToIntDef( $intChoreCategoryId, NULL, false ) );
	}

	public function getChoreCategoryId() {
		return $this->m_intChoreCategoryId;
	}

	public function sqlChoreCategoryId() {
		return ( true == isset( $this->m_intChoreCategoryId ) ) ? ( string ) $this->m_intChoreCategoryId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSendEmail( $intSendEmail ) {
		$this->set( 'm_intSendEmail', CStrings::strToIntDef( $intSendEmail, NULL, false ) );
	}

	public function getSendEmail() {
		return $this->m_intSendEmail;
	}

	public function sqlSendEmail() {
		return ( true == isset( $this->m_intSendEmail ) ) ? ( string ) $this->m_intSendEmail : '1';
	}

	public function setIsRequiredToLockMonth( $intIsRequiredToLockMonth ) {
		$this->set( 'm_intIsRequiredToLockMonth', CStrings::strToIntDef( $intIsRequiredToLockMonth, NULL, false ) );
	}

	public function getIsRequiredToLockMonth() {
		return $this->m_intIsRequiredToLockMonth;
	}

	public function sqlIsRequiredToLockMonth() {
		return ( true == isset( $this->m_intIsRequiredToLockMonth ) ) ? ( string ) $this->m_intIsRequiredToLockMonth : '0';
	}

	public function setIsRequiredToAdvanceMonth( $intIsRequiredToAdvanceMonth ) {
		$this->set( 'm_intIsRequiredToAdvanceMonth', CStrings::strToIntDef( $intIsRequiredToAdvanceMonth, NULL, false ) );
	}

	public function getIsRequiredToAdvanceMonth() {
		return $this->m_intIsRequiredToAdvanceMonth;
	}

	public function sqlIsRequiredToAdvanceMonth() {
		return ( true == isset( $this->m_intIsRequiredToAdvanceMonth ) ) ? ( string ) $this->m_intIsRequiredToAdvanceMonth : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'chore_category_id' => $this->getChoreCategoryId(),
			'name' => $this->getName(),
			'send_email' => $this->getSendEmail(),
			'is_required_to_lock_month' => $this->getIsRequiredToLockMonth(),
			'is_required_to_advance_month' => $this->getIsRequiredToAdvanceMonth(),
			'is_published' => $this->getIsPublished(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>