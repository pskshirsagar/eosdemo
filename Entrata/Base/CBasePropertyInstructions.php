<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyInstructions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyInstructions extends CEosPluralBase {

	/**
	 * @return CPropertyInstruction[]
	 */
	public static function fetchPropertyInstructions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyInstruction', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyInstruction
	 */
	public static function fetchPropertyInstruction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyInstruction', $objDatabase );
	}

	public static function fetchPropertyInstructionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_instructions', $objDatabase );
	}

	public static function fetchPropertyInstructionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyInstruction( sprintf( 'SELECT * FROM property_instructions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInstructionsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyInstructions( sprintf( 'SELECT * FROM property_instructions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInstructionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyInstructions( sprintf( 'SELECT * FROM property_instructions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInstructionsByPropertyInstructionIdByCid( $intPropertyInstructionId, $intCid, $objDatabase ) {
		return self::fetchPropertyInstructions( sprintf( 'SELECT * FROM property_instructions WHERE property_instruction_id = %d AND cid = %d', ( int ) $intPropertyInstructionId, ( int ) $intCid ), $objDatabase );
	}

}
?>