<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewGroupInstances
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportNewGroupInstances extends CEosPluralBase {

	/**
	 * @return CReportNewGroupInstance[]
	 */
	public static function fetchReportNewGroupInstances( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportNewGroupInstance::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportNewGroupInstance
	 */
	public static function fetchReportNewGroupInstance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportNewGroupInstance::class, $objDatabase );
	}

	public static function fetchReportNewGroupInstanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_new_group_instances', $objDatabase );
	}

	public static function fetchReportNewGroupInstanceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroupInstance( sprintf( 'SELECT * FROM report_new_group_instances WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupInstancesByCid( $intCid, $objDatabase ) {
		return self::fetchReportNewGroupInstances( sprintf( 'SELECT * FROM report_new_group_instances WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupInstancesByReportNewGroupIdByCid( $intReportNewGroupId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroupInstances( sprintf( 'SELECT * FROM report_new_group_instances WHERE report_new_group_id = %d AND cid = %d', ( int ) $intReportNewGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupInstancesByReportNewInstanceIdByCid( $intReportNewInstanceId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroupInstances( sprintf( 'SELECT * FROM report_new_group_instances WHERE report_new_instance_id = %d AND cid = %d', ( int ) $intReportNewInstanceId, ( int ) $intCid ), $objDatabase );
	}

}
?>