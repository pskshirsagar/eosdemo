<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInsurancePolicyCustomers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInsurancePolicyCustomers extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyCustomer[]
	 */
	public static function fetchInsurancePolicyCustomers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyCustomer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInsurancePolicyCustomer
	 */
	public static function fetchInsurancePolicyCustomer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyCustomer', $objDatabase );
	}

	public static function fetchInsurancePolicyCustomerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_customers', $objDatabase );
	}

	public static function fetchInsurancePolicyCustomerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomer( sprintf( 'SELECT * FROM insurance_policy_customers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE resident_insurance_policy_id = %d AND cid = %d', ( int ) $intResidentInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByCid( $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyCustomersByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyCustomers( sprintf( 'SELECT * FROM insurance_policy_customers WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

}
?>