<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApRemittances
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApRemittances extends CEosPluralBase {

	/**
	 * @return CApRemittance[]
	 */
	public static function fetchApRemittances( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApRemittance::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApRemittance
	 */
	public static function fetchApRemittance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApRemittance::class, $objDatabase );
	}

	public static function fetchApRemittanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_remittances', $objDatabase );
	}

	public static function fetchApRemittanceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApRemittance( sprintf( 'SELECT * FROM ap_remittances WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByCid( $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByApPaymentTypeIdByCid( $intApPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE ap_payment_type_id = %d AND cid = %d', $intApPaymentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE bank_account_id = %d AND cid = %d', $intBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByVendorRemittanceIdByCid( $intVendorRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE vendor_remittance_id = %d AND cid = %d', $intVendorRemittanceId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE check_account_type_id = %d AND cid = %d', $intCheckAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApRemittancesByDebitCardPaymentTypeIdByCid( $intDebitCardPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchApRemittances( sprintf( 'SELECT * FROM ap_remittances WHERE debit_card_payment_type_id = %d AND cid = %d', $intDebitCardPaymentTypeId, $intCid ), $objDatabase );
	}

}
?>