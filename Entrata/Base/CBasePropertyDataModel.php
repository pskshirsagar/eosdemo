<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyDataModel extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.property_data_models';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strLeadScoringModel;
	protected $m_strLeadScoringModelCreatedOn;
	protected $m_intLeadScoringModelTrainingSetRowCount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lead_scoring_model'] ) && $boolDirectSet ) $this->set( 'm_strLeadScoringModel', trim( stripcslashes( $arrValues['lead_scoring_model'] ) ) ); elseif( isset( $arrValues['lead_scoring_model'] ) ) $this->setLeadScoringModel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_scoring_model'] ) : $arrValues['lead_scoring_model'] );
		if( isset( $arrValues['lead_scoring_model_created_on'] ) && $boolDirectSet ) $this->set( 'm_strLeadScoringModelCreatedOn', trim( $arrValues['lead_scoring_model_created_on'] ) ); elseif( isset( $arrValues['lead_scoring_model_created_on'] ) ) $this->setLeadScoringModelCreatedOn( $arrValues['lead_scoring_model_created_on'] );
		if( isset( $arrValues['lead_scoring_model_training_set_row_count'] ) && $boolDirectSet ) $this->set( 'm_intLeadScoringModelTrainingSetRowCount', trim( $arrValues['lead_scoring_model_training_set_row_count'] ) ); elseif( isset( $arrValues['lead_scoring_model_training_set_row_count'] ) ) $this->setLeadScoringModelTrainingSetRowCount( $arrValues['lead_scoring_model_training_set_row_count'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeadScoringModel( $strLeadScoringModel ) {
		$this->set( 'm_strLeadScoringModel', CStrings::strTrimDef( $strLeadScoringModel, -1, NULL, true ) );
	}

	public function getLeadScoringModel() {
		return $this->m_strLeadScoringModel;
	}

	public function sqlLeadScoringModel() {
		return ( true == isset( $this->m_strLeadScoringModel ) ) ? '\'' . addslashes( $this->m_strLeadScoringModel ) . '\'' : 'NULL';
	}

	public function setLeadScoringModelCreatedOn( $strLeadScoringModelCreatedOn ) {
		$this->set( 'm_strLeadScoringModelCreatedOn', CStrings::strTrimDef( $strLeadScoringModelCreatedOn, -1, NULL, true ) );
	}

	public function getLeadScoringModelCreatedOn() {
		return $this->m_strLeadScoringModelCreatedOn;
	}

	public function sqlLeadScoringModelCreatedOn() {
		return ( true == isset( $this->m_strLeadScoringModelCreatedOn ) ) ? '\'' . $this->m_strLeadScoringModelCreatedOn . '\'' : 'NULL';
	}

	public function setLeadScoringModelTrainingSetRowCount( $intLeadScoringModelTrainingSetRowCount ) {
		$this->set( 'm_intLeadScoringModelTrainingSetRowCount', CStrings::strToIntDef( $intLeadScoringModelTrainingSetRowCount, NULL, false ) );
	}

	public function getLeadScoringModelTrainingSetRowCount() {
		return $this->m_intLeadScoringModelTrainingSetRowCount;
	}

	public function sqlLeadScoringModelTrainingSetRowCount() {
		return ( true == isset( $this->m_intLeadScoringModelTrainingSetRowCount ) ) ? ( string ) $this->m_intLeadScoringModelTrainingSetRowCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lead_scoring_model, lead_scoring_model_created_on, lead_scoring_model_training_set_row_count, details, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeadScoringModel() . ', ' .
						$this->sqlLeadScoringModelCreatedOn() . ', ' .
						$this->sqlLeadScoringModelTrainingSetRowCount() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_scoring_model = ' . $this->sqlLeadScoringModel(). ',' ; } elseif( true == array_key_exists( 'LeadScoringModel', $this->getChangedColumns() ) ) { $strSql .= ' lead_scoring_model = ' . $this->sqlLeadScoringModel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_scoring_model_created_on = ' . $this->sqlLeadScoringModelCreatedOn(). ',' ; } elseif( true == array_key_exists( 'LeadScoringModelCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' lead_scoring_model_created_on = ' . $this->sqlLeadScoringModelCreatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_scoring_model_training_set_row_count = ' . $this->sqlLeadScoringModelTrainingSetRowCount(). ',' ; } elseif( true == array_key_exists( 'LeadScoringModelTrainingSetRowCount', $this->getChangedColumns() ) ) { $strSql .= ' lead_scoring_model_training_set_row_count = ' . $this->sqlLeadScoringModelTrainingSetRowCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lead_scoring_model' => $this->getLeadScoringModel(),
			'lead_scoring_model_created_on' => $this->getLeadScoringModelCreatedOn(),
			'lead_scoring_model_training_set_row_count' => $this->getLeadScoringModelTrainingSetRowCount(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>