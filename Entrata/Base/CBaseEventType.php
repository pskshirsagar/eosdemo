<?php

class CBaseEventType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.event_types';

	protected $m_intId;
	protected $m_intEventMediumId;
	protected $m_intEventResultTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolAllowResults;
	protected $m_boolIsFirstEventType;
	protected $m_boolIsLastEventType;
	protected $m_boolDoNotExport;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
    protected $m_strTag;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowResults = false;
		$this->m_boolIsFirstEventType = false;
		$this->m_boolIsLastEventType = false;
		$this->m_boolDoNotExport = true;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['event_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intEventMediumId', trim( $arrValues['event_medium_id'] ) ); elseif( isset( $arrValues['event_medium_id'] ) ) $this->setEventMediumId( $arrValues['event_medium_id'] );
		if( isset( $arrValues['event_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventResultTypeId', trim( $arrValues['event_result_type_id'] ) ); elseif( isset( $arrValues['event_result_type_id'] ) ) $this->setEventResultTypeId( $arrValues['event_result_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['allow_results'] ) && $boolDirectSet ) $this->set( 'm_boolAllowResults', trim( stripcslashes( $arrValues['allow_results'] ) ) ); elseif( isset( $arrValues['allow_results'] ) ) $this->setAllowResults( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_results'] ) : $arrValues['allow_results'] );
		if( isset( $arrValues['is_first_event_type'] ) && $boolDirectSet ) $this->set( 'm_boolIsFirstEventType', trim( stripcslashes( $arrValues['is_first_event_type'] ) ) ); elseif( isset( $arrValues['is_first_event_type'] ) ) $this->setIsFirstEventType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_first_event_type'] ) : $arrValues['is_first_event_type'] );
		if( isset( $arrValues['is_last_event_type'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastEventType', trim( stripcslashes( $arrValues['is_last_event_type'] ) ) ); elseif( isset( $arrValues['is_last_event_type'] ) ) $this->setIsLastEventType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_event_type'] ) : $arrValues['is_last_event_type'] );
		if( isset( $arrValues['do_not_export'] ) && $boolDirectSet ) $this->set( 'm_boolDoNotExport', trim( stripcslashes( $arrValues['do_not_export'] ) ) ); elseif( isset( $arrValues['do_not_export'] ) ) $this->setDoNotExport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['do_not_export'] ) : $arrValues['do_not_export'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
        if( isset( $arrValues['tag'] ) && $boolDirectSet ) $this->set( 'm_strTag', trim( $arrValues['tag'] ) ); elseif( isset( $arrValues['tag'] ) ) $this->setTag( $arrValues['tag'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEventMediumId( $intEventMediumId ) {
		$this->set( 'm_intEventMediumId', CStrings::strToIntDef( $intEventMediumId, NULL, false ) );
	}

	public function getEventMediumId() {
		return $this->m_intEventMediumId;
	}

	public function sqlEventMediumId() {
		return ( true == isset( $this->m_intEventMediumId ) ) ? ( string ) $this->m_intEventMediumId : 'NULL';
	}

	public function setEventResultTypeId( $intEventResultTypeId ) {
		$this->set( 'm_intEventResultTypeId', CStrings::strToIntDef( $intEventResultTypeId, NULL, false ) );
	}

	public function getEventResultTypeId() {
		return $this->m_intEventResultTypeId;
	}

	public function sqlEventResultTypeId() {
		return ( true == isset( $this->m_intEventResultTypeId ) ) ? ( string ) $this->m_intEventResultTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setAllowResults( $boolAllowResults ) {
		$this->set( 'm_boolAllowResults', CStrings::strToBool( $boolAllowResults ) );
	}

	public function getAllowResults() {
		return $this->m_boolAllowResults;
	}

	public function sqlAllowResults() {
		return ( true == isset( $this->m_boolAllowResults ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowResults ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFirstEventType( $boolIsFirstEventType ) {
		$this->set( 'm_boolIsFirstEventType', CStrings::strToBool( $boolIsFirstEventType ) );
	}

	public function getIsFirstEventType() {
		return $this->m_boolIsFirstEventType;
	}

	public function sqlIsFirstEventType() {
		return ( true == isset( $this->m_boolIsFirstEventType ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFirstEventType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastEventType( $boolIsLastEventType ) {
		$this->set( 'm_boolIsLastEventType', CStrings::strToBool( $boolIsLastEventType ) );
	}

	public function getIsLastEventType() {
		return $this->m_boolIsLastEventType;
	}

	public function sqlIsLastEventType() {
		return ( true == isset( $this->m_boolIsLastEventType ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastEventType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDoNotExport( $boolDoNotExport ) {
		$this->set( 'm_boolDoNotExport', CStrings::strToBool( $boolDoNotExport ) );
	}

	public function getDoNotExport() {
		return $this->m_boolDoNotExport;
	}

	public function sqlDoNotExport() {
		return ( true == isset( $this->m_boolDoNotExport ) ) ? '\'' . ( true == ( bool ) $this->m_boolDoNotExport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

    public function setTag( $strTag ) {
        $this->set( 'm_strTag', CStrings::strTrimDef( $strTag, -1, NULL, true ) );
    }

    public function getTag() {
        return $this->m_strTag;
    }

    public function sqlTag() {
        return ( true == isset( $this->m_strTag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTag ) : '\'' . addslashes( $this->m_strTag ) . '\'' ) : 'NULL';
    }

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'event_medium_id' => $this->getEventMediumId(),
			'event_result_type_id' => $this->getEventResultTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'allow_results' => $this->getAllowResults(),
			'is_first_event_type' => $this->getIsFirstEventType(),
			'is_last_event_type' => $this->getIsLastEventType(),
			'do_not_export' => $this->getDoNotExport(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
            'tag' => $this->getTag()
		);
	}

}
?>