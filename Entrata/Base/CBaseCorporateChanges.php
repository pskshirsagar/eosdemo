<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateChanges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateChanges extends CEosPluralBase {

	/**
	 * @return CCorporateChange[]
	 */
	public static function fetchCorporateChanges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCorporateChange', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporateChange
	 */
	public static function fetchCorporateChange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCorporateChange', $objDatabase );
	}

	public static function fetchCorporateChangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_changes', $objDatabase );
	}

	public static function fetchCorporateChangeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporateChange( sprintf( 'SELECT * FROM corporate_changes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateChangesByCid( $intCid, $objDatabase ) {
		return self::fetchCorporateChanges( sprintf( 'SELECT * FROM corporate_changes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateChangesByCorporateEventIdByCid( $intCorporateEventId, $intCid, $objDatabase ) {
		return self::fetchCorporateChanges( sprintf( 'SELECT * FROM corporate_changes WHERE corporate_event_id = %d AND cid = %d', ( int ) $intCorporateEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateChangesByCorporateChangeTypeIdByCid( $intCorporateChangeTypeId, $intCid, $objDatabase ) {
		return self::fetchCorporateChanges( sprintf( 'SELECT * FROM corporate_changes WHERE corporate_change_type_id = %d AND cid = %d', ( int ) $intCorporateChangeTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>