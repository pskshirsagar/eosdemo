<?php

class CBaseRevenueStudentDemand extends CEosSingularBase {

	const TABLE_NAME = 'public.revenue_student_demands';

	protected $m_intId;
	protected $m_strModelProvider;
	protected $m_strDistance;
	protected $m_fltBaseRent;
	protected $m_fltDemand;
	protected $m_strDemandDate;
	protected $m_strModelDate;
	protected $m_strCreatedOn;
	protected $m_fltCreatedBy;
	protected $m_strForecastModel;
	protected $m_intMarketId;
	protected $m_intBedCount;
	protected $m_strMarketName;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['model_provider'] ) && $boolDirectSet ) $this->set( 'm_strModelProvider', trim( $arrValues['model_provider'] ) ); elseif( isset( $arrValues['model_provider'] ) ) $this->setModelProvider( $arrValues['model_provider'] );
		if( isset( $arrValues['distance'] ) && $boolDirectSet ) $this->set( 'm_strDistance', trim( $arrValues['distance'] ) ); elseif( isset( $arrValues['distance'] ) ) $this->setDistance( $arrValues['distance'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['demand'] ) && $boolDirectSet ) $this->set( 'm_fltDemand', trim( $arrValues['demand'] ) ); elseif( isset( $arrValues['demand'] ) ) $this->setDemand( $arrValues['demand'] );
		if( isset( $arrValues['demand_date'] ) && $boolDirectSet ) $this->set( 'm_strDemandDate', trim( $arrValues['demand_date'] ) ); elseif( isset( $arrValues['demand_date'] ) ) $this->setDemandDate( $arrValues['demand_date'] );
		if( isset( $arrValues['model_date'] ) && $boolDirectSet ) $this->set( 'm_strModelDate', trim( $arrValues['model_date'] ) ); elseif( isset( $arrValues['model_date'] ) ) $this->setModelDate( $arrValues['model_date'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_fltCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['forecast_model'] ) && $boolDirectSet ) $this->set( 'm_strForecastModel', trim( $arrValues['forecast_model'] ) ); elseif( isset( $arrValues['forecast_model'] ) ) $this->setForecastModel( $arrValues['forecast_model'] );
		if( isset( $arrValues['market_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketId', trim( $arrValues['market_id'] ) ); elseif( isset( $arrValues['market_id'] ) ) $this->setMarketId( $arrValues['market_id'] );
		if( isset( $arrValues['bed_count'] ) && $boolDirectSet ) $this->set( 'm_intBedCount', trim( $arrValues['bed_count'] ) ); elseif( isset( $arrValues['bed_count'] ) ) $this->setBedCount( $arrValues['bed_count'] );
		if( isset( $arrValues['market_name'] ) && $boolDirectSet ) $this->set( 'm_strMarketName', trim( $arrValues['market_name'] ) ); elseif( isset( $arrValues['market_name'] ) ) $this->setMarketName( $arrValues['market_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setModelProvider( $strModelProvider ) {
		$this->set( 'm_strModelProvider', CStrings::strTrimDef( $strModelProvider, 100, NULL, true ) );
	}

	public function getModelProvider() {
		return $this->m_strModelProvider;
	}

	public function sqlModelProvider() {
		return ( true == isset( $this->m_strModelProvider ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strModelProvider ) : '\'' . addslashes( $this->m_strModelProvider ) . '\'' ) : 'NULL';
	}

	public function setDistance( $strDistance ) {
		$this->set( 'm_strDistance', CStrings::strTrimDef( $strDistance, NULL, NULL, true ) );
	}

	public function getDistance() {
		return $this->m_strDistance;
	}

	public function sqlDistance() {
		return ( true == isset( $this->m_strDistance ) ) ? '\'' . addslashes( $this->m_strDistance ) . '\'' : 'NULL';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 0 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : 'NULL';
	}

	public function setDemand( $fltDemand ) {
		$this->set( 'm_fltDemand', CStrings::strToFloatDef( $fltDemand, NULL, false, 0 ) );
	}

	public function getDemand() {
		return $this->m_fltDemand;
	}

	public function sqlDemand() {
		return ( true == isset( $this->m_fltDemand ) ) ? ( string ) $this->m_fltDemand : 'NULL';
	}

	public function setDemandDate( $strDemandDate ) {
		$this->set( 'm_strDemandDate', CStrings::strTrimDef( $strDemandDate, -1, NULL, true ) );
	}

	public function getDemandDate() {
		return $this->m_strDemandDate;
	}

	public function sqlDemandDate() {
		return ( true == isset( $this->m_strDemandDate ) ) ? '\'' . $this->m_strDemandDate . '\'' : 'NULL';
	}

	public function setModelDate( $strModelDate ) {
		$this->set( 'm_strModelDate', CStrings::strTrimDef( $strModelDate, -1, NULL, true ) );
	}

	public function getModelDate() {
		return $this->m_strModelDate;
	}

	public function sqlModelDate() {
		return ( true == isset( $this->m_strModelDate ) ) ? '\'' . $this->m_strModelDate . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $fltCreatedBy ) {
		$this->set( 'm_fltCreatedBy', CStrings::strToFloatDef( $fltCreatedBy, NULL, false, 0 ) );
	}

	public function getCreatedBy() {
		return $this->m_fltCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_fltCreatedBy ) ) ? ( string ) $this->m_fltCreatedBy : 'NULL';
	}

	public function setForecastModel( $strForecastModel ) {
		$this->set( 'm_strForecastModel', CStrings::strTrimDef( $strForecastModel, 255, NULL, true ) );
	}

	public function getForecastModel() {
		return $this->m_strForecastModel;
	}

	public function sqlForecastModel() {
		return ( true == isset( $this->m_strForecastModel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strForecastModel ) : '\'' . addslashes( $this->m_strForecastModel ) . '\'' ) : 'NULL';
	}

	public function setMarketId( $intMarketId ) {
		$this->set( 'm_intMarketId', CStrings::strToIntDef( $intMarketId, NULL, false ) );
	}

	public function getMarketId() {
		return $this->m_intMarketId;
	}

	public function sqlMarketId() {
		return ( true == isset( $this->m_intMarketId ) ) ? ( string ) $this->m_intMarketId : 'NULL';
	}

	public function setBedCount( $intBedCount ) {
		$this->set( 'm_intBedCount', CStrings::strToIntDef( $intBedCount, NULL, false ) );
	}

	public function getBedCount() {
		return $this->m_intBedCount;
	}

	public function sqlBedCount() {
		return ( true == isset( $this->m_intBedCount ) ) ? ( string ) $this->m_intBedCount : 'NULL';
	}

	public function setMarketName( $strMarketName ) {
		$this->set( 'm_strMarketName', CStrings::strTrimDef( $strMarketName, 255, NULL, true ) );
	}

	public function getMarketName() {
		return $this->m_strMarketName;
	}

	public function sqlMarketName() {
		return ( true == isset( $this->m_strMarketName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketName ) : '\'' . addslashes( $this->m_strMarketName ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, model_provider, distance, base_rent, demand, demand_date, model_date, created_on, created_by, forecast_model, market_id, bed_count, market_name )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlModelProvider() . ', ' .
		          $this->sqlDistance() . ', ' .
		          $this->sqlBaseRent() . ', ' .
		          $this->sqlDemand() . ', ' .
		          $this->sqlDemandDate() . ', ' .
		          $this->sqlModelDate() . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlForecastModel() . ', ' .
		          $this->sqlMarketId() . ', ' .
		          $this->sqlBedCount() . ', ' .
		          $this->sqlMarketName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' model_provider = ' . $this->sqlModelProvider(). ',' ; } elseif( true == array_key_exists( 'ModelProvider', $this->getChangedColumns() ) ) { $strSql .= ' model_provider = ' . $this->sqlModelProvider() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distance = ' . $this->sqlDistance(). ',' ; } elseif( true == array_key_exists( 'Distance', $this->getChangedColumns() ) ) { $strSql .= ' distance = ' . $this->sqlDistance() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand = ' . $this->sqlDemand(). ',' ; } elseif( true == array_key_exists( 'Demand', $this->getChangedColumns() ) ) { $strSql .= ' demand = ' . $this->sqlDemand() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_date = ' . $this->sqlDemandDate(). ',' ; } elseif( true == array_key_exists( 'DemandDate', $this->getChangedColumns() ) ) { $strSql .= ' demand_date = ' . $this->sqlDemandDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' model_date = ' . $this->sqlModelDate(). ',' ; } elseif( true == array_key_exists( 'ModelDate', $this->getChangedColumns() ) ) { $strSql .= ' model_date = ' . $this->sqlModelDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' forecast_model = ' . $this->sqlForecastModel(). ',' ; } elseif( true == array_key_exists( 'ForecastModel', $this->getChangedColumns() ) ) { $strSql .= ' forecast_model = ' . $this->sqlForecastModel() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_id = ' . $this->sqlMarketId(). ',' ; } elseif( true == array_key_exists( 'MarketId', $this->getChangedColumns() ) ) { $strSql .= ' market_id = ' . $this->sqlMarketId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bed_count = ' . $this->sqlBedCount(). ',' ; } elseif( true == array_key_exists( 'BedCount', $this->getChangedColumns() ) ) { $strSql .= ' bed_count = ' . $this->sqlBedCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_name = ' . $this->sqlMarketName() ; } elseif( true == array_key_exists( 'MarketName', $this->getChangedColumns() ) ) { $strSql .= ' market_name = ' . $this->sqlMarketName() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'model_provider' => $this->getModelProvider(),
			'distance' => $this->getDistance(),
			'base_rent' => $this->getBaseRent(),
			'demand' => $this->getDemand(),
			'demand_date' => $this->getDemandDate(),
			'model_date' => $this->getModelDate(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'forecast_model' => $this->getForecastModel(),
			'market_id' => $this->getMarketId(),
			'bed_count' => $this->getBedCount(),
			'market_name' => $this->getMarketName()
		);
	}

}
?>