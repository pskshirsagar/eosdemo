<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRelationshipGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerRelationshipGroups extends CEosPluralBase {

	/**
	 * @return CCustomerRelationshipGroup[]
	 */
	public static function fetchCustomerRelationshipGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerRelationshipGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerRelationshipGroup
	 */
	public static function fetchCustomerRelationshipGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerRelationshipGroup::class, $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_relationship_groups', $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationshipGroup( sprintf( 'SELECT * FROM customer_relationship_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerRelationshipGroups( sprintf( 'SELECT * FROM customer_relationship_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupsByDefaultCustomerRelationshipGroupIdByCid( $intDefaultCustomerRelationshipGroupId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationshipGroups( sprintf( 'SELECT * FROM customer_relationship_groups WHERE default_customer_relationship_group_id = %d AND cid = %d', $intDefaultCustomerRelationshipGroupId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationshipGroups( sprintf( 'SELECT * FROM customer_relationship_groups WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerRelationshipGroupsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerRelationshipGroups( sprintf( 'SELECT * FROM customer_relationship_groups WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>