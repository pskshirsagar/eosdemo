<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobGroupLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobGroupLocations extends CEosPluralBase {

	/**
	 * @return CJobGroupLocation[]
	 */
	public static function fetchJobGroupLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobGroupLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobGroupLocation
	 */
	public static function fetchJobGroupLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobGroupLocation::class, $objDatabase );
	}

	public static function fetchJobGroupLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_group_locations', $objDatabase );
	}

	public static function fetchJobGroupLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobGroupLocation( sprintf( 'SELECT * FROM job_group_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchJobGroupLocations( sprintf( 'SELECT * FROM job_group_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupLocationsByJobGroupIdByCid( $intJobGroupId, $intCid, $objDatabase ) {
		return self::fetchJobGroupLocations( sprintf( 'SELECT * FROM job_group_locations WHERE job_group_id = %d AND cid = %d', ( int ) $intJobGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupLocationsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchJobGroupLocations( sprintf( 'SELECT * FROM job_group_locations WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupLocationsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchJobGroupLocations( sprintf( 'SELECT * FROM job_group_locations WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

}
?>