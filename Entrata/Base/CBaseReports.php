<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReports extends CEosPluralBase {

	/**
	 * @return CReport[]
	 */
	public static function fetchReports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReport::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReport
	 */
	public static function fetchReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReport::class, $objDatabase );
	}

	public static function fetchReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reports', $objDatabase );
	}

	public static function fetchReportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReport( sprintf( 'SELECT * FROM reports WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportsByCid( $intCid, $objDatabase ) {
		return self::fetchReports( sprintf( 'SELECT * FROM reports WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportsByReportTypeIdByCid( $intReportTypeId, $intCid, $objDatabase ) {
		return self::fetchReports( sprintf( 'SELECT * FROM reports WHERE report_type_id = %d AND cid = %d', $intReportTypeId, $intCid ), $objDatabase );
	}

	public static function fetchReportsByDefaultReportIdByCid( $intDefaultReportId, $intCid, $objDatabase ) {
		return self::fetchReports( sprintf( 'SELECT * FROM reports WHERE default_report_id = %d AND cid = %d', $intDefaultReportId, $intCid ), $objDatabase );
	}

}
?>