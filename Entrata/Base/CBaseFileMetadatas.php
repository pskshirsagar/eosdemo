<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMetadatas
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileMetadatas extends CEosPluralBase {

	/**
	 * @return CFileMetadata[]
	 */
	public static function fetchFileMetadatas( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileMetadata::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileMetadata
	 */
	public static function fetchFileMetadata( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileMetadata::class, $objDatabase );
	}

	public static function fetchFileMetadataCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_metadatas', $objDatabase );
	}

	public static function fetchFileMetadataByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileMetadata( sprintf( 'SELECT * FROM file_metadatas WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasByCid( $intCid, $objDatabase ) {
		return self::fetchFileMetadatas( sprintf( 'SELECT * FROM file_metadatas WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileMetadatas( sprintf( 'SELECT * FROM file_metadatas WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasByEntityTypeIdByCid( $intEntityTypeId, $intCid, $objDatabase ) {
		return self::fetchFileMetadatas( sprintf( 'SELECT * FROM file_metadatas WHERE entity_type_id = %d AND cid = %d', $intEntityTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasByEntityIdByCid( $intEntityId, $intCid, $objDatabase ) {
		return self::fetchFileMetadatas( sprintf( 'SELECT * FROM file_metadatas WHERE entity_id = %d AND cid = %d', $intEntityId, $intCid ), $objDatabase );
	}

	public static function fetchFileMetadatasByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFileMetadatas( sprintf( 'SELECT * FROM file_metadatas WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>