<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDepreciationCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDepreciationCategories extends CEosPluralBase {

	/**
	 * @return CDepreciationCategory[]
	 */
	public static function fetchDepreciationCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDepreciationCategory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDepreciationCategory
	 */
	public static function fetchDepreciationCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDepreciationCategory::class, $objDatabase );
	}

	public static function fetchDepreciationCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'depreciation_categories', $objDatabase );
	}

	public static function fetchDepreciationCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDepreciationCategory( sprintf( 'SELECT * FROM depreciation_categories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDepreciationCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchDepreciationCategories( sprintf( 'SELECT * FROM depreciation_categories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDepreciationCategoriesByDefaultDepreciationCategoryIdByCid( $intDefaultDepreciationCategoryId, $intCid, $objDatabase ) {
		return self::fetchDepreciationCategories( sprintf( 'SELECT * FROM depreciation_categories WHERE default_depreciation_category_id = %d AND cid = %d', ( int ) $intDefaultDepreciationCategoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDepreciationCategoriesByDepreciationGlAccountIdByCid( $intDepreciationGlAccountId, $intCid, $objDatabase ) {
		return self::fetchDepreciationCategories( sprintf( 'SELECT * FROM depreciation_categories WHERE depreciation_gl_account_id = %d AND cid = %d', ( int ) $intDepreciationGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDepreciationCategoriesByAccumDepreciationGlAccountIdByCid( $intAccumDepreciationGlAccountId, $intCid, $objDatabase ) {
		return self::fetchDepreciationCategories( sprintf( 'SELECT * FROM depreciation_categories WHERE accum_depreciation_gl_account_id = %d AND cid = %d', ( int ) $intAccumDepreciationGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDepreciationCategoriesByTaxRecoveryPeriodIdByCid( $intTaxRecoveryPeriodId, $intCid, $objDatabase ) {
		return self::fetchDepreciationCategories( sprintf( 'SELECT * FROM depreciation_categories WHERE tax_recovery_period_id = %d AND cid = %d', ( int ) $intTaxRecoveryPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>