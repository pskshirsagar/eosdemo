<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyDisplacementStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyDisplacementStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyDisplacementStatusType[]
	 */
	public static function fetchSubsidyDisplacementStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyDisplacementStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyDisplacementStatusType
	 */
	public static function fetchSubsidyDisplacementStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyDisplacementStatusType::class, $objDatabase );
	}

	public static function fetchSubsidyDisplacementStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_displacement_status_types', $objDatabase );
	}

	public static function fetchSubsidyDisplacementStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyDisplacementStatusType( sprintf( 'SELECT * FROM subsidy_displacement_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>