<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwnerContributionRequest extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intJobId;
    protected $m_intJobPhaseId;
    protected $m_intOwnerId;
    protected $m_intArTransactionId;
    protected $m_fltContributionAmount;
    protected $m_strScheduledPostDate;
    protected $m_strPostMonth;
    protected $m_strNotes;
    protected $m_strEmailMessage;
    protected $m_boolSendEmailReminder;
    protected $m_strEmailedOn;
    protected $m_intPostedBy;
    protected $m_strPostedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_fltContributionAmount = '0';
        $this->m_boolSendEmailReminder = false;
        $this->m_strEmailedOn = 'now()';
        $this->m_strPostedOn = 'now()';
        $this->m_intUpdatedBy = '1';
        $this->m_strUpdatedOn = 'now()';
        $this->m_intCreatedBy = '1';
        $this->m_strCreatedOn = 'now()';

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->m_intJobId = trim( $arrValues['job_id'] ); else if( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
        if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->m_intJobPhaseId = trim( $arrValues['job_phase_id'] ); else if( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
        if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->m_intOwnerId = trim( $arrValues['owner_id'] ); else if( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
        if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->m_intArTransactionId = trim( $arrValues['ar_transaction_id'] ); else if( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
        if( isset( $arrValues['contribution_amount'] ) && $boolDirectSet ) $this->m_fltContributionAmount = trim( $arrValues['contribution_amount'] ); else if( isset( $arrValues['contribution_amount'] ) ) $this->setContributionAmount( $arrValues['contribution_amount'] );
        if( isset( $arrValues['scheduled_post_date'] ) && $boolDirectSet ) $this->m_strScheduledPostDate = trim( $arrValues['scheduled_post_date'] ); else if( isset( $arrValues['scheduled_post_date'] ) ) $this->setScheduledPostDate( $arrValues['scheduled_post_date'] );
        if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->m_strPostMonth = trim( $arrValues['post_month'] ); else if( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
        if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->m_strNotes = trim( stripcslashes( $arrValues['notes'] ) ); else if( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
        if( isset( $arrValues['email_message'] ) && $boolDirectSet ) $this->m_strEmailMessage = trim( stripcslashes( $arrValues['email_message'] ) ); else if( isset( $arrValues['email_message'] ) ) $this->setEmailMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_message'] ) : $arrValues['email_message'] );
        if( isset( $arrValues['send_email_reminder'] ) && $boolDirectSet ) $this->m_boolSendEmailReminder = trim( stripcslashes( $arrValues['send_email_reminder'] ) ); else if( isset( $arrValues['send_email_reminder'] ) ) $this->setSendEmailReminder( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_email_reminder'] ) : $arrValues['send_email_reminder'] );
        if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->m_strEmailedOn = trim( $arrValues['emailed_on'] ); else if( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
        if( isset( $arrValues['posted_by'] ) && $boolDirectSet ) $this->m_intPostedBy = trim( $arrValues['posted_by'] ); else if( isset( $arrValues['posted_by'] ) ) $this->setPostedBy( $arrValues['posted_by'] );
        if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->m_strPostedOn = trim( $arrValues['posted_on'] ); else if( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setJobId( $intJobId ) {
        $this->m_intJobId = CStrings::strToIntDef( $intJobId, NULL, false );
    }

    public function getJobId() {
        return $this->m_intJobId;
    }

    public function sqlJobId() {
        return ( true == isset( $this->m_intJobId ) ) ? (string) $this->m_intJobId : 'NULL';
    }

    public function setJobPhaseId( $intJobPhaseId ) {
        $this->m_intJobPhaseId = CStrings::strToIntDef( $intJobPhaseId, NULL, false );
    }

    public function getJobPhaseId() {
        return $this->m_intJobPhaseId;
    }

    public function sqlJobPhaseId() {
        return ( true == isset( $this->m_intJobPhaseId ) ) ? (string) $this->m_intJobPhaseId : 'NULL';
    }

    public function setOwnerId( $intOwnerId ) {
        $this->m_intOwnerId = CStrings::strToIntDef( $intOwnerId, NULL, false );
    }

    public function getOwnerId() {
        return $this->m_intOwnerId;
    }

    public function sqlOwnerId() {
        return ( true == isset( $this->m_intOwnerId ) ) ? (string) $this->m_intOwnerId : 'NULL';
    }

    public function setArTransactionId( $intArTransactionId ) {
        $this->m_intArTransactionId = CStrings::strToIntDef( $intArTransactionId, NULL, false );
    }

    public function getArTransactionId() {
        return $this->m_intArTransactionId;
    }

    public function sqlArTransactionId() {
        return ( true == isset( $this->m_intArTransactionId ) ) ? (string) $this->m_intArTransactionId : 'NULL';
    }

    public function setContributionAmount( $fltContributionAmount ) {
        $this->m_fltContributionAmount = CStrings::strToFloatDef( $fltContributionAmount, NULL, false, 2 );
    }

    public function getContributionAmount() {
        return $this->m_fltContributionAmount;
    }

    public function sqlContributionAmount() {
        return ( true == isset( $this->m_fltContributionAmount ) ) ? (string) $this->m_fltContributionAmount : '0';
    }

    public function setScheduledPostDate( $strScheduledPostDate ) {
        $this->m_strScheduledPostDate = CStrings::strTrimDef( $strScheduledPostDate, -1, NULL, true );
    }

    public function getScheduledPostDate() {
        return $this->m_strScheduledPostDate;
    }

    public function sqlScheduledPostDate() {
        return ( true == isset( $this->m_strScheduledPostDate ) ) ? '\'' . $this->m_strScheduledPostDate . '\'' : 'NOW()';
    }

    public function setPostMonth( $strPostMonth ) {
        $this->m_strPostMonth = CStrings::strTrimDef( $strPostMonth, -1, NULL, true );
    }

    public function getPostMonth() {
        return $this->m_strPostMonth;
    }

    public function sqlPostMonth() {
        return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
    }

    public function setNotes( $strNotes ) {
        $this->m_strNotes = CStrings::strTrimDef( $strNotes, 2000, NULL, true );
    }

    public function getNotes() {
        return $this->m_strNotes;
    }

    public function sqlNotes() {
        return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
    }

    public function setEmailMessage( $strEmailMessage ) {
        $this->m_strEmailMessage = CStrings::strTrimDef( $strEmailMessage, -1, NULL, true );
    }

    public function getEmailMessage() {
        return $this->m_strEmailMessage;
    }

    public function sqlEmailMessage() {
        return ( true == isset( $this->m_strEmailMessage ) ) ? '\'' . addslashes( $this->m_strEmailMessage ) . '\'' : 'NULL';
    }

    public function setSendEmailReminder( $boolSendEmailReminder ) {
        $this->m_boolSendEmailReminder = CStrings::strToBool( $boolSendEmailReminder );
    }

    public function getSendEmailReminder() {
        return $this->m_boolSendEmailReminder;
    }

    public function sqlSendEmailReminder() {
        return ( true == isset( $this->m_boolSendEmailReminder ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendEmailReminder ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setEmailedOn( $strEmailedOn ) {
        $this->m_strEmailedOn = CStrings::strTrimDef( $strEmailedOn, -1, NULL, true );
    }

    public function getEmailedOn() {
        return $this->m_strEmailedOn;
    }

    public function sqlEmailedOn() {
        return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
    }

    public function setPostedBy( $intPostedBy ) {
        $this->m_intPostedBy = CStrings::strToIntDef( $intPostedBy, NULL, false );
    }

    public function getPostedBy() {
        return $this->m_intPostedBy;
    }

    public function sqlPostedBy() {
        return ( true == isset( $this->m_intPostedBy ) ) ? (string) $this->m_intPostedBy : 'NULL';
    }

    public function setPostedOn( $strPostedOn ) {
        $this->m_strPostedOn = CStrings::strTrimDef( $strPostedOn, -1, NULL, true );
    }

    public function getPostedOn() {
        return $this->m_strPostedOn;
    }

    public function sqlPostedOn() {
        return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : '1';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : '1';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.owner_contribution_requests_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.owner_contribution_requests					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlJobId() . ', ' . 		                $this->sqlJobPhaseId() . ', ' . 		                $this->sqlOwnerId() . ', ' . 		                $this->sqlArTransactionId() . ', ' . 		                $this->sqlContributionAmount() . ', ' . 		                $this->sqlScheduledPostDate() . ', ' . 		                $this->sqlPostMonth() . ', ' . 		                $this->sqlNotes() . ', ' . 		                $this->sqlEmailMessage() . ', ' . 		                $this->sqlSendEmailReminder() . ', ' . 		                $this->sqlEmailedOn() . ', ' . 		                $this->sqlPostedBy() . ', ' . 		                $this->sqlPostedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.owner_contribution_requests
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlJobId() ) != $this->getOriginalValueByFieldName ( 'job_id' ) ) { $arrstrOriginalValueChanges['job_id'] = $this->sqlJobId(); $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlJobPhaseId() ) != $this->getOriginalValueByFieldName ( 'job_phase_id' ) ) { $arrstrOriginalValueChanges['job_phase_id'] = $this->sqlJobPhaseId(); $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOwnerId() ) != $this->getOriginalValueByFieldName ( 'owner_id' ) ) { $arrstrOriginalValueChanges['owner_id'] = $this->sqlOwnerId(); $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArTransactionId() ) != $this->getOriginalValueByFieldName ( 'ar_transaction_id' ) ) { $arrstrOriginalValueChanges['ar_transaction_id'] = $this->sqlArTransactionId(); $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contribution_amount = ' . $this->sqlContributionAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlContributionAmount() ), $this->getOriginalValueByFieldName ( 'contribution_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['contribution_amount'] = $this->sqlContributionAmount(); $strSql .= ' contribution_amount = ' . $this->sqlContributionAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_post_date = ' . $this->sqlScheduledPostDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlScheduledPostDate() ) != $this->getOriginalValueByFieldName ( 'scheduled_post_date' ) ) { $arrstrOriginalValueChanges['scheduled_post_date'] = $this->sqlScheduledPostDate(); $strSql .= ' scheduled_post_date = ' . $this->sqlScheduledPostDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostMonth() ) != $this->getOriginalValueByFieldName ( 'post_month' ) ) { $arrstrOriginalValueChanges['post_month'] = $this->sqlPostMonth(); $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNotes() ) != $this->getOriginalValueByFieldName ( 'notes' ) ) { $arrstrOriginalValueChanges['notes'] = $this->sqlNotes(); $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_message = ' . $this->sqlEmailMessage() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmailMessage() ) != $this->getOriginalValueByFieldName ( 'email_message' ) ) { $arrstrOriginalValueChanges['email_message'] = $this->sqlEmailMessage(); $strSql .= ' email_message = ' . $this->sqlEmailMessage() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email_reminder = ' . $this->sqlSendEmailReminder() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSendEmailReminder() ) != $this->getOriginalValueByFieldName ( 'send_email_reminder' ) ) { $arrstrOriginalValueChanges['send_email_reminder'] = $this->sqlSendEmailReminder(); $strSql .= ' send_email_reminder = ' . $this->sqlSendEmailReminder() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmailedOn() ) != $this->getOriginalValueByFieldName ( 'emailed_on' ) ) { $arrstrOriginalValueChanges['emailed_on'] = $this->sqlEmailedOn(); $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostedBy() ) != $this->getOriginalValueByFieldName ( 'posted_by' ) ) { $arrstrOriginalValueChanges['posted_by'] = $this->sqlPostedBy(); $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostedOn() ) != $this->getOriginalValueByFieldName ( 'posted_on' ) ) { $arrstrOriginalValueChanges['posted_on'] = $this->sqlPostedOn(); $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.owner_contribution_requests WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.owner_contribution_requests_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'job_id' => $this->getJobId(),
        	'job_phase_id' => $this->getJobPhaseId(),
        	'owner_id' => $this->getOwnerId(),
        	'ar_transaction_id' => $this->getArTransactionId(),
        	'contribution_amount' => $this->getContributionAmount(),
        	'scheduled_post_date' => $this->getScheduledPostDate(),
        	'post_month' => $this->getPostMonth(),
        	'notes' => $this->getNotes(),
        	'email_message' => $this->getEmailMessage(),
        	'send_email_reminder' => $this->getSendEmailReminder(),
        	'emailed_on' => $this->getEmailedOn(),
        	'posted_by' => $this->getPostedBy(),
        	'posted_on' => $this->getPostedOn(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>