<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultShippingVendors
 * Do not add any new functions to this class.
 */

class CBaseDefaultShippingVendors extends CEosPluralBase {

	/**
	 * @return CDefaultShippingVendor[]
	 */
	public static function fetchDefaultShippingVendors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultShippingVendor::class, $objDatabase );
	}

	/**
	 * @return CDefaultShippingVendor
	 */
	public static function fetchDefaultShippingVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultShippingVendor::class, $objDatabase );
	}

	public static function fetchDefaultShippingVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_shipping_vendors', $objDatabase );
	}

	public static function fetchDefaultShippingVendorById( $intId, $objDatabase ) {
		return self::fetchDefaultShippingVendor( sprintf( 'SELECT * FROM default_shipping_vendors WHERE id = %d', $intId ), $objDatabase );
	}

}
?>