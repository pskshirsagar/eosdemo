<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReferrerTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultReferrerTypes extends CEosPluralBase {

	/**
	 * @return CDefaultReferrerType[]
	 */
	public static function fetchDefaultReferrerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReferrerType::class, $objDatabase );
	}

	/**
	 * @return CDefaultReferrerType
	 */
	public static function fetchDefaultReferrerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReferrerType::class, $objDatabase );
	}

	public static function fetchDefaultReferrerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_referrer_types', $objDatabase );
	}

	public static function fetchDefaultReferrerTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultReferrerType( sprintf( 'SELECT * FROM default_referrer_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>