<?php

class CBaseMilitaryOfficerStructure extends CEosSingularBase {

	const TABLE_NAME = 'public.military_officer_structures';

	protected $m_intId;
	protected $m_strName;
	protected $m_strMilitaryOfficerStructureAbbreviation;
	protected $m_boolIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['military_officer_structure_abbreviation'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryOfficerStructureAbbreviation', trim( stripcslashes( $arrValues['military_officer_structure_abbreviation'] ) ) ); elseif( isset( $arrValues['military_officer_structure_abbreviation'] ) ) $this->setMilitaryOfficerStructureAbbreviation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_officer_structure_abbreviation'] ) : $arrValues['military_officer_structure_abbreviation'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 1024, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setMilitaryOfficerStructureAbbreviation( $strMilitaryOfficerStructureAbbreviation ) {
		$this->set( 'm_strMilitaryOfficerStructureAbbreviation', CStrings::strTrimDef( $strMilitaryOfficerStructureAbbreviation, 252, NULL, true ) );
	}

	public function getMilitaryOfficerStructureAbbreviation() {
		return $this->m_strMilitaryOfficerStructureAbbreviation;
	}

	public function sqlMilitaryOfficerStructureAbbreviation() {
		return ( true == isset( $this->m_strMilitaryOfficerStructureAbbreviation ) ) ? '\'' . addslashes( $this->m_strMilitaryOfficerStructureAbbreviation ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'military_officer_structure_abbreviation' => $this->getMilitaryOfficerStructureAbbreviation(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>