<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProposalApPayee extends CEosSingularBase {

	const TABLE_NAME = 'public.proposal_ap_payees';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intProposalId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeContactId;
	protected $m_strProposalDescription;
	protected $m_strResultDescription;
	protected $m_strAdditionalEmailAddresses;
	protected $m_boolIsFinalist;
	protected $m_boolAgreesToTerms;
	protected $m_strIpAddress;
	protected $m_strSignature;
	protected $m_intSignatureBy;
	protected $m_strSignatureOn;
	protected $m_intInvitedBy;
	protected $m_strInvitedOn;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFinalist = false;
		$this->m_boolAgreesToTerms = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['proposal_id'] ) && $boolDirectSet ) $this->set( 'm_intProposalId', trim( $arrValues['proposal_id'] ) ); elseif( isset( $arrValues['proposal_id'] ) ) $this->setProposalId( $arrValues['proposal_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_contact_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeContactId', trim( $arrValues['ap_payee_contact_id'] ) ); elseif( isset( $arrValues['ap_payee_contact_id'] ) ) $this->setApPayeeContactId( $arrValues['ap_payee_contact_id'] );
		if( isset( $arrValues['proposal_description'] ) && $boolDirectSet ) $this->set( 'm_strProposalDescription', trim( stripcslashes( $arrValues['proposal_description'] ) ) ); elseif( isset( $arrValues['proposal_description'] ) ) $this->setProposalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['proposal_description'] ) : $arrValues['proposal_description'] );
		if( isset( $arrValues['result_description'] ) && $boolDirectSet ) $this->set( 'm_strResultDescription', trim( stripcslashes( $arrValues['result_description'] ) ) ); elseif( isset( $arrValues['result_description'] ) ) $this->setResultDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['result_description'] ) : $arrValues['result_description'] );
		if( isset( $arrValues['additional_email_addresses'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalEmailAddresses', trim( stripcslashes( $arrValues['additional_email_addresses'] ) ) ); elseif( isset( $arrValues['additional_email_addresses'] ) ) $this->setAdditionalEmailAddresses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_email_addresses'] ) : $arrValues['additional_email_addresses'] );
		if( isset( $arrValues['is_finalist'] ) && $boolDirectSet ) $this->set( 'm_boolIsFinalist', trim( stripcslashes( $arrValues['is_finalist'] ) ) ); elseif( isset( $arrValues['is_finalist'] ) ) $this->setIsFinalist( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_finalist'] ) : $arrValues['is_finalist'] );
		if( isset( $arrValues['agrees_to_terms'] ) && $boolDirectSet ) $this->set( 'm_boolAgreesToTerms', trim( stripcslashes( $arrValues['agrees_to_terms'] ) ) ); elseif( isset( $arrValues['agrees_to_terms'] ) ) $this->setAgreesToTerms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agrees_to_terms'] ) : $arrValues['agrees_to_terms'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( stripcslashes( $arrValues['signature'] ) ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature'] ) : $arrValues['signature'] );
		if( isset( $arrValues['signature_by'] ) && $boolDirectSet ) $this->set( 'm_intSignatureBy', trim( $arrValues['signature_by'] ) ); elseif( isset( $arrValues['signature_by'] ) ) $this->setSignatureBy( $arrValues['signature_by'] );
		if( isset( $arrValues['signature_on'] ) && $boolDirectSet ) $this->set( 'm_strSignatureOn', trim( $arrValues['signature_on'] ) ); elseif( isset( $arrValues['signature_on'] ) ) $this->setSignatureOn( $arrValues['signature_on'] );
		if( isset( $arrValues['invited_by'] ) && $boolDirectSet ) $this->set( 'm_intInvitedBy', trim( $arrValues['invited_by'] ) ); elseif( isset( $arrValues['invited_by'] ) ) $this->setInvitedBy( $arrValues['invited_by'] );
		if( isset( $arrValues['invited_on'] ) && $boolDirectSet ) $this->set( 'm_strInvitedOn', trim( $arrValues['invited_on'] ) ); elseif( isset( $arrValues['invited_on'] ) ) $this->setInvitedOn( $arrValues['invited_on'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setProposalId( $intProposalId ) {
		$this->set( 'm_intProposalId', CStrings::strToIntDef( $intProposalId, NULL, false ) );
	}

	public function getProposalId() {
		return $this->m_intProposalId;
	}

	public function sqlProposalId() {
		return ( true == isset( $this->m_intProposalId ) ) ? ( string ) $this->m_intProposalId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeContactId( $intApPayeeContactId ) {
		$this->set( 'm_intApPayeeContactId', CStrings::strToIntDef( $intApPayeeContactId, NULL, false ) );
	}

	public function getApPayeeContactId() {
		return $this->m_intApPayeeContactId;
	}

	public function sqlApPayeeContactId() {
		return ( true == isset( $this->m_intApPayeeContactId ) ) ? ( string ) $this->m_intApPayeeContactId : 'NULL';
	}

	public function setProposalDescription( $strProposalDescription ) {
		$this->set( 'm_strProposalDescription', CStrings::strTrimDef( $strProposalDescription, -1, NULL, true ) );
	}

	public function getProposalDescription() {
		return $this->m_strProposalDescription;
	}

	public function sqlProposalDescription() {
		return ( true == isset( $this->m_strProposalDescription ) ) ? '\'' . addslashes( $this->m_strProposalDescription ) . '\'' : 'NULL';
	}

	public function setResultDescription( $strResultDescription ) {
		$this->set( 'm_strResultDescription', CStrings::strTrimDef( $strResultDescription, -1, NULL, true ) );
	}

	public function getResultDescription() {
		return $this->m_strResultDescription;
	}

	public function sqlResultDescription() {
		return ( true == isset( $this->m_strResultDescription ) ) ? '\'' . addslashes( $this->m_strResultDescription ) . '\'' : 'NULL';
	}

	public function setAdditionalEmailAddresses( $strAdditionalEmailAddresses ) {
		$this->set( 'm_strAdditionalEmailAddresses', CStrings::strTrimDef( $strAdditionalEmailAddresses, 2000, NULL, true ) );
	}

	public function getAdditionalEmailAddresses() {
		return $this->m_strAdditionalEmailAddresses;
	}

	public function sqlAdditionalEmailAddresses() {
		return ( true == isset( $this->m_strAdditionalEmailAddresses ) ) ? '\'' . addslashes( $this->m_strAdditionalEmailAddresses ) . '\'' : 'NULL';
	}

	public function setIsFinalist( $boolIsFinalist ) {
		$this->set( 'm_boolIsFinalist', CStrings::strToBool( $boolIsFinalist ) );
	}

	public function getIsFinalist() {
		return $this->m_boolIsFinalist;
	}

	public function sqlIsFinalist() {
		return ( true == isset( $this->m_boolIsFinalist ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFinalist ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		$this->set( 'm_boolAgreesToTerms', CStrings::strToBool( $boolAgreesToTerms ) );
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	public function sqlAgreesToTerms() {
		return ( true == isset( $this->m_boolAgreesToTerms ) ) ? '\'' . ( true == ( bool ) $this->m_boolAgreesToTerms ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 2000, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? '\'' . addslashes( $this->m_strSignature ) . '\'' : 'NULL';
	}

	public function setSignatureBy( $intSignatureBy ) {
		$this->set( 'm_intSignatureBy', CStrings::strToIntDef( $intSignatureBy, NULL, false ) );
	}

	public function getSignatureBy() {
		return $this->m_intSignatureBy;
	}

	public function sqlSignatureBy() {
		return ( true == isset( $this->m_intSignatureBy ) ) ? ( string ) $this->m_intSignatureBy : 'NULL';
	}

	public function setSignatureOn( $strSignatureOn ) {
		$this->set( 'm_strSignatureOn', CStrings::strTrimDef( $strSignatureOn, -1, NULL, true ) );
	}

	public function getSignatureOn() {
		return $this->m_strSignatureOn;
	}

	public function sqlSignatureOn() {
		return ( true == isset( $this->m_strSignatureOn ) ) ? '\'' . $this->m_strSignatureOn . '\'' : 'NULL';
	}

	public function setInvitedBy( $intInvitedBy ) {
		$this->set( 'm_intInvitedBy', CStrings::strToIntDef( $intInvitedBy, NULL, false ) );
	}

	public function getInvitedBy() {
		return $this->m_intInvitedBy;
	}

	public function sqlInvitedBy() {
		return ( true == isset( $this->m_intInvitedBy ) ) ? ( string ) $this->m_intInvitedBy : 'NULL';
	}

	public function setInvitedOn( $strInvitedOn ) {
		$this->set( 'm_strInvitedOn', CStrings::strTrimDef( $strInvitedOn, -1, NULL, true ) );
	}

	public function getInvitedOn() {
		return $this->m_strInvitedOn;
	}

	public function sqlInvitedOn() {
		return ( true == isset( $this->m_strInvitedOn ) ) ? '\'' . $this->m_strInvitedOn . '\'' : 'NULL';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, proposal_id, ap_payee_id, ap_payee_contact_id, proposal_description, result_description, additional_email_addresses, is_finalist, agrees_to_terms, ip_address, signature, signature_by, signature_on, invited_by, invited_on, accepted_by, accepted_on, rejected_by, rejected_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlProposalId() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApPayeeContactId() . ', ' .
 						$this->sqlProposalDescription() . ', ' .
 						$this->sqlResultDescription() . ', ' .
 						$this->sqlAdditionalEmailAddresses() . ', ' .
 						$this->sqlIsFinalist() . ', ' .
 						$this->sqlAgreesToTerms() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlSignature() . ', ' .
 						$this->sqlSignatureBy() . ', ' .
 						$this->sqlSignatureOn() . ', ' .
 						$this->sqlInvitedBy() . ', ' .
 						$this->sqlInvitedOn() . ', ' .
 						$this->sqlAcceptedBy() . ', ' .
 						$this->sqlAcceptedOn() . ', ' .
 						$this->sqlRejectedBy() . ', ' .
 						$this->sqlRejectedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_id = ' . $this->sqlProposalId() . ','; } elseif( true == array_key_exists( 'ProposalId', $this->getChangedColumns() ) ) { $strSql .= ' proposal_id = ' . $this->sqlProposalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_contact_id = ' . $this->sqlApPayeeContactId() . ','; } elseif( true == array_key_exists( 'ApPayeeContactId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_contact_id = ' . $this->sqlApPayeeContactId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_description = ' . $this->sqlProposalDescription() . ','; } elseif( true == array_key_exists( 'ProposalDescription', $this->getChangedColumns() ) ) { $strSql .= ' proposal_description = ' . $this->sqlProposalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_description = ' . $this->sqlResultDescription() . ','; } elseif( true == array_key_exists( 'ResultDescription', $this->getChangedColumns() ) ) { $strSql .= ' result_description = ' . $this->sqlResultDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_email_addresses = ' . $this->sqlAdditionalEmailAddresses() . ','; } elseif( true == array_key_exists( 'AdditionalEmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' additional_email_addresses = ' . $this->sqlAdditionalEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_finalist = ' . $this->sqlIsFinalist() . ','; } elseif( true == array_key_exists( 'IsFinalist', $this->getChangedColumns() ) ) { $strSql .= ' is_finalist = ' . $this->sqlIsFinalist() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agrees_to_terms = ' . $this->sqlAgreesToTerms() . ','; } elseif( true == array_key_exists( 'AgreesToTerms', $this->getChangedColumns() ) ) { $strSql .= ' agrees_to_terms = ' . $this->sqlAgreesToTerms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_by = ' . $this->sqlSignatureBy() . ','; } elseif( true == array_key_exists( 'SignatureBy', $this->getChangedColumns() ) ) { $strSql .= ' signature_by = ' . $this->sqlSignatureBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_on = ' . $this->sqlSignatureOn() . ','; } elseif( true == array_key_exists( 'SignatureOn', $this->getChangedColumns() ) ) { $strSql .= ' signature_on = ' . $this->sqlSignatureOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invited_by = ' . $this->sqlInvitedBy() . ','; } elseif( true == array_key_exists( 'InvitedBy', $this->getChangedColumns() ) ) { $strSql .= ' invited_by = ' . $this->sqlInvitedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invited_on = ' . $this->sqlInvitedOn() . ','; } elseif( true == array_key_exists( 'InvitedOn', $this->getChangedColumns() ) ) { $strSql .= ' invited_on = ' . $this->sqlInvitedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'proposal_id' => $this->getProposalId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_contact_id' => $this->getApPayeeContactId(),
			'proposal_description' => $this->getProposalDescription(),
			'result_description' => $this->getResultDescription(),
			'additional_email_addresses' => $this->getAdditionalEmailAddresses(),
			'is_finalist' => $this->getIsFinalist(),
			'agrees_to_terms' => $this->getAgreesToTerms(),
			'ip_address' => $this->getIpAddress(),
			'signature' => $this->getSignature(),
			'signature_by' => $this->getSignatureBy(),
			'signature_on' => $this->getSignatureOn(),
			'invited_by' => $this->getInvitedBy(),
			'invited_on' => $this->getInvitedOn(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>