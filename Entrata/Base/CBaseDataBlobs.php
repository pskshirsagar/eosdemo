<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataBlobs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataBlobs extends CEosPluralBase {

	/**
	 * @return CDataBlob[]
	 */
	public static function fetchDataBlobs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDataBlob::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDataBlob
	 */
	public static function fetchDataBlob( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDataBlob::class, $objDatabase );
	}

	public static function fetchDataBlobCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_blobs', $objDatabase );
	}

	public static function fetchDataBlobByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDataBlob( sprintf( 'SELECT * FROM data_blobs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByCid( $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByDataBlobTypeIdByCid( $intDataBlobTypeId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE data_blob_type_id = %d AND cid = %d', $intDataBlobTypeId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByLeaseCustomerIdByCid( $intLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE lease_customer_id = %d AND cid = %d', $intLeaseCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByEmployeeIdByCid( $intEmployeeId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE employee_id = %d AND cid = %d', $intEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE applicant_application_id = %d AND cid = %d', $intApplicantApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE application_id = %d AND cid = %d', $intApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchDataBlobsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchDataBlobs( sprintf( 'SELECT * FROM data_blobs WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

}
?>