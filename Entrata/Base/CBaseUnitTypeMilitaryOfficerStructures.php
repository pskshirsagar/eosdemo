<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitTypeMilitaryOfficerStructures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitTypeMilitaryOfficerStructures extends CEosPluralBase {

	/**
	 * @return CUnitTypeMilitaryOfficerStructure[]
	 */
	public static function fetchUnitTypeMilitaryOfficerStructures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitTypeMilitaryOfficerStructure::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitTypeMilitaryOfficerStructure
	 */
	public static function fetchUnitTypeMilitaryOfficerStructure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitTypeMilitaryOfficerStructure::class, $objDatabase );
	}

	public static function fetchUnitTypeMilitaryOfficerStructureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_type_military_officer_structures', $objDatabase );
	}

	public static function fetchUnitTypeMilitaryOfficerStructureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryOfficerStructure( sprintf( 'SELECT * FROM unit_type_military_officer_structures WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryOfficerStructuresByCid( $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryOfficerStructures( sprintf( 'SELECT * FROM unit_type_military_officer_structures WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryOfficerStructuresByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryOfficerStructure( sprintf( 'SELECT * FROM unit_type_military_officer_structures WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryOfficerStructuresByMilitaryOfficerStructureIdByCid( $intMilitaryOfficerStructureId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryOfficerStructures( sprintf( 'SELECT * FROM unit_type_military_officer_structures WHERE military_officer_structure_id = %d AND cid = %d', $intMilitaryOfficerStructureId, $intCid ), $objDatabase );
	}

}
?>