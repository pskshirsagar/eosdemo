<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseResidentParticipationStat extends CEosSingularBase {

	const TABLE_NAME = 'public.resident_participation_stats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeases;
	protected $m_intEmailAddresses;
	protected $m_intResidentAccounts;
	protected $m_intArPayments;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intLeases = '0';
		$this->m_intEmailAddresses = '0';
		$this->m_intResidentAccounts = '0';
		$this->m_intArPayments = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['leases'] ) && $boolDirectSet ) $this->set( 'm_intLeases', trim( $arrValues['leases'] ) ); elseif( isset( $arrValues['leases'] ) ) $this->setLeases( $arrValues['leases'] );
		if( isset( $arrValues['email_addresses'] ) && $boolDirectSet ) $this->set( 'm_intEmailAddresses', trim( $arrValues['email_addresses'] ) ); elseif( isset( $arrValues['email_addresses'] ) ) $this->setEmailAddresses( $arrValues['email_addresses'] );
		if( isset( $arrValues['resident_accounts'] ) && $boolDirectSet ) $this->set( 'm_intResidentAccounts', trim( $arrValues['resident_accounts'] ) ); elseif( isset( $arrValues['resident_accounts'] ) ) $this->setResidentAccounts( $arrValues['resident_accounts'] );
		if( isset( $arrValues['ar_payments'] ) && $boolDirectSet ) $this->set( 'm_intArPayments', trim( $arrValues['ar_payments'] ) ); elseif( isset( $arrValues['ar_payments'] ) ) $this->setArPayments( $arrValues['ar_payments'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeases( $intLeases ) {
		$this->set( 'm_intLeases', CStrings::strToIntDef( $intLeases, NULL, false ) );
	}

	public function getLeases() {
		return $this->m_intLeases;
	}

	public function sqlLeases() {
		return ( true == isset( $this->m_intLeases ) ) ? ( string ) $this->m_intLeases : '0';
	}

	public function setEmailAddresses( $intEmailAddresses ) {
		$this->set( 'm_intEmailAddresses', CStrings::strToIntDef( $intEmailAddresses, NULL, false ) );
	}

	public function getEmailAddresses() {
		return $this->m_intEmailAddresses;
	}

	public function sqlEmailAddresses() {
		return ( true == isset( $this->m_intEmailAddresses ) ) ? ( string ) $this->m_intEmailAddresses : '0';
	}

	public function setResidentAccounts( $intResidentAccounts ) {
		$this->set( 'm_intResidentAccounts', CStrings::strToIntDef( $intResidentAccounts, NULL, false ) );
	}

	public function getResidentAccounts() {
		return $this->m_intResidentAccounts;
	}

	public function sqlResidentAccounts() {
		return ( true == isset( $this->m_intResidentAccounts ) ) ? ( string ) $this->m_intResidentAccounts : '0';
	}

	public function setArPayments( $intArPayments ) {
		$this->set( 'm_intArPayments', CStrings::strToIntDef( $intArPayments, NULL, false ) );
	}

	public function getArPayments() {
		return $this->m_intArPayments;
	}

	public function sqlArPayments() {
		return ( true == isset( $this->m_intArPayments ) ) ? ( string ) $this->m_intArPayments : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, leases, email_addresses, resident_accounts, ar_payments, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeases() . ', ' .
 						$this->sqlEmailAddresses() . ', ' .
 						$this->sqlResidentAccounts() . ', ' .
 						$this->sqlArPayments() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leases = ' . $this->sqlLeases() . ','; } elseif( true == array_key_exists( 'Leases', $this->getChangedColumns() ) ) { $strSql .= ' leases = ' . $this->sqlLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; } elseif( true == array_key_exists( 'EmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_accounts = ' . $this->sqlResidentAccounts() . ','; } elseif( true == array_key_exists( 'ResidentAccounts', $this->getChangedColumns() ) ) { $strSql .= ' resident_accounts = ' . $this->sqlResidentAccounts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payments = ' . $this->sqlArPayments() . ','; } elseif( true == array_key_exists( 'ArPayments', $this->getChangedColumns() ) ) { $strSql .= ' ar_payments = ' . $this->sqlArPayments() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'leases' => $this->getLeases(),
			'email_addresses' => $this->getEmailAddresses(),
			'resident_accounts' => $this->getResidentAccounts(),
			'ar_payments' => $this->getArPayments(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>