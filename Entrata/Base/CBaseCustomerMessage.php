<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerMessage extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_messages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intSenderId;
	protected $m_intSenderTypeId;
	protected $m_intSystemEmailId;
	protected $m_intCustomerMessageTypeId;
	protected $m_intReferenceId;
	protected $m_strSubject;
	protected $m_strMessage;
	protected $m_intIsUrgent;
	protected $m_intIsRead;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsUrgent = '0';
		$this->m_intIsRead = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['sender_id'] ) && $boolDirectSet ) $this->set( 'm_intSenderId', trim( $arrValues['sender_id'] ) ); elseif( isset( $arrValues['sender_id'] ) ) $this->setSenderId( $arrValues['sender_id'] );
		if( isset( $arrValues['sender_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSenderTypeId', trim( $arrValues['sender_type_id'] ) ); elseif( isset( $arrValues['sender_type_id'] ) ) $this->setSenderTypeId( $arrValues['sender_type_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['customer_message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerMessageTypeId', trim( $arrValues['customer_message_type_id'] ) ); elseif( isset( $arrValues['customer_message_type_id'] ) ) $this->setCustomerMessageTypeId( $arrValues['customer_message_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['is_urgent'] ) && $boolDirectSet ) $this->set( 'm_intIsUrgent', trim( $arrValues['is_urgent'] ) ); elseif( isset( $arrValues['is_urgent'] ) ) $this->setIsUrgent( $arrValues['is_urgent'] );
		if( isset( $arrValues['is_read'] ) && $boolDirectSet ) $this->set( 'm_intIsRead', trim( $arrValues['is_read'] ) ); elseif( isset( $arrValues['is_read'] ) ) $this->setIsRead( $arrValues['is_read'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setSenderId( $intSenderId ) {
		$this->set( 'm_intSenderId', CStrings::strToIntDef( $intSenderId, NULL, false ) );
	}

	public function getSenderId() {
		return $this->m_intSenderId;
	}

	public function sqlSenderId() {
		return ( true == isset( $this->m_intSenderId ) ) ? ( string ) $this->m_intSenderId : 'NULL';
	}

	public function setSenderTypeId( $intSenderTypeId ) {
		$this->set( 'm_intSenderTypeId', CStrings::strToIntDef( $intSenderTypeId, NULL, false ) );
	}

	public function getSenderTypeId() {
		return $this->m_intSenderTypeId;
	}

	public function sqlSenderTypeId() {
		return ( true == isset( $this->m_intSenderTypeId ) ) ? ( string ) $this->m_intSenderTypeId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setCustomerMessageTypeId( $intCustomerMessageTypeId ) {
		$this->set( 'm_intCustomerMessageTypeId', CStrings::strToIntDef( $intCustomerMessageTypeId, NULL, false ) );
	}

	public function getCustomerMessageTypeId() {
		return $this->m_intCustomerMessageTypeId;
	}

	public function sqlCustomerMessageTypeId() {
		return ( true == isset( $this->m_intCustomerMessageTypeId ) ) ? ( string ) $this->m_intCustomerMessageTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setIsUrgent( $intIsUrgent ) {
		$this->set( 'm_intIsUrgent', CStrings::strToIntDef( $intIsUrgent, NULL, false ) );
	}

	public function getIsUrgent() {
		return $this->m_intIsUrgent;
	}

	public function sqlIsUrgent() {
		return ( true == isset( $this->m_intIsUrgent ) ) ? ( string ) $this->m_intIsUrgent : '0';
	}

	public function setIsRead( $intIsRead ) {
		$this->set( 'm_intIsRead', CStrings::strToIntDef( $intIsRead, NULL, false ) );
	}

	public function getIsRead() {
		return $this->m_intIsRead;
	}

	public function sqlIsRead() {
		return ( true == isset( $this->m_intIsRead ) ) ? ( string ) $this->m_intIsRead : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, sender_id, sender_type_id, system_email_id, customer_message_type_id, reference_id, subject, message, is_urgent, is_read, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlSenderId() . ', ' .
 						$this->sqlSenderTypeId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlCustomerMessageTypeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlIsUrgent() . ', ' .
 						$this->sqlIsRead() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sender_id = ' . $this->sqlSenderId() . ','; } elseif( true == array_key_exists( 'SenderId', $this->getChangedColumns() ) ) { $strSql .= ' sender_id = ' . $this->sqlSenderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sender_type_id = ' . $this->sqlSenderTypeId() . ','; } elseif( true == array_key_exists( 'SenderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sender_type_id = ' . $this->sqlSenderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_message_type_id = ' . $this->sqlCustomerMessageTypeId() . ','; } elseif( true == array_key_exists( 'CustomerMessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_message_type_id = ' . $this->sqlCustomerMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_urgent = ' . $this->sqlIsUrgent() . ','; } elseif( true == array_key_exists( 'IsUrgent', $this->getChangedColumns() ) ) { $strSql .= ' is_urgent = ' . $this->sqlIsUrgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_read = ' . $this->sqlIsRead() . ','; } elseif( true == array_key_exists( 'IsRead', $this->getChangedColumns() ) ) { $strSql .= ' is_read = ' . $this->sqlIsRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'sender_id' => $this->getSenderId(),
			'sender_type_id' => $this->getSenderTypeId(),
			'system_email_id' => $this->getSystemEmailId(),
			'customer_message_type_id' => $this->getCustomerMessageTypeId(),
			'reference_id' => $this->getReferenceId(),
			'subject' => $this->getSubject(),
			'message' => $this->getMessage(),
			'is_urgent' => $this->getIsUrgent(),
			'is_read' => $this->getIsRead(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>