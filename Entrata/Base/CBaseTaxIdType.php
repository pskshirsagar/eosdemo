<?php

class CBaseTaxIdType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.tax_id_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strAbbreviation;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCountryCode;
	protected $m_strMaskFormat;
	protected $m_strAssignType;
	protected $m_boolIsOrganzation;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_boolIsProperty;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strMaskFormat = NULL;
		$this->m_boolIsOrganzation = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_boolIsProperty = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['abbreviation'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAbbreviation', trim( stripcslashes( $arrValues['abbreviation'] ) ) ); elseif( isset( $arrValues['abbreviation'] ) ) $this->setAbbreviation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['abbreviation'] ) : $arrValues['abbreviation'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['mask_format'] ) && $boolDirectSet ) $this->set( 'm_strMaskFormat', trim( stripcslashes( $arrValues['mask_format'] ) ) ); elseif( isset( $arrValues['mask_format'] ) ) $this->setMaskFormat( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mask_format'] ) : $arrValues['mask_format'] );
		if( isset( $arrValues['assign_type'] ) && $boolDirectSet ) $this->set( 'm_strAssignType', trim( stripcslashes( $arrValues['assign_type'] ) ) ); elseif( isset( $arrValues['assign_type'] ) ) $this->setAssignType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['assign_type'] ) : $arrValues['assign_type'] );
		if( isset( $arrValues['is_organzation'] ) && $boolDirectSet ) $this->set( 'm_boolIsOrganzation', trim( stripcslashes( $arrValues['is_organzation'] ) ) ); elseif( isset( $arrValues['is_organzation'] ) ) $this->setIsOrganzation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_organzation'] ) : $arrValues['is_organzation'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsProperty', trim( stripcslashes( $arrValues['is_property'] ) ) ); elseif( isset( $arrValues['is_property'] ) ) $this->setIsProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property'] ) : $arrValues['is_property'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAbbreviation( $strAbbreviation, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAbbreviation', CStrings::strTrimDef( $strAbbreviation, 4, NULL, true ), $strLocaleCode );
	}

	public function getAbbreviation( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAbbreviation', $strLocaleCode );
	}

	public function sqlAbbreviation() {
		return ( true == isset( $this->m_strAbbreviation ) ) ? '\'' . addslashes( $this->m_strAbbreviation ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setMaskFormat( $strMaskFormat ) {
		$this->set( 'm_strMaskFormat', CStrings::strTrimDef( $strMaskFormat, 50, NULL, true ) );
	}

	public function getMaskFormat() {
		return $this->m_strMaskFormat;
	}

	public function sqlMaskFormat() {
		return ( true == isset( $this->m_strMaskFormat ) ) ? '\'' . addslashes( $this->m_strMaskFormat ) . '\'' : '\'NULL\'';
	}

	public function setAssignType( $strAssignType ) {
		$this->set( 'm_strAssignType', CStrings::strTrimDef( $strAssignType, -1, NULL, true ) );
	}

	public function getAssignType() {
		return $this->m_strAssignType;
	}

	public function sqlAssignType() {
		return ( true == isset( $this->m_strAssignType ) ) ? '\'' . addslashes( $this->m_strAssignType ) . '\'' : 'NULL';
	}

	public function setIsOrganzation( $boolIsOrganzation ) {
		$this->set( 'm_boolIsOrganzation', CStrings::strToBool( $boolIsOrganzation ) );
	}

	public function getIsOrganzation() {
		return $this->m_boolIsOrganzation;
	}

	public function sqlIsOrganzation() {
		return ( true == isset( $this->m_boolIsOrganzation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOrganzation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsProperty( $boolIsProperty ) {
		$this->set( 'm_boolIsProperty', CStrings::strToBool( $boolIsProperty ) );
	}

	public function getIsProperty() {
		return $this->m_boolIsProperty;
	}

	public function sqlIsProperty() {
		return ( true == isset( $this->m_boolIsProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'abbreviation' => $this->getAbbreviation(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'country_code' => $this->getCountryCode(),
			'mask_format' => $this->getMaskFormat(),
			'assign_type' => $this->getAssignType(),
			'is_organzation' => $this->getIsOrganzation(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'is_property' => $this->getIsProperty()
		);
	}

}
?>