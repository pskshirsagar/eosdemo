<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyNotifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyNotifications extends CEosPluralBase {

	/**
	 * @return CCompanyNotification[]
	 */
	public static function fetchCompanyNotifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyNotification::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyNotification
	 */
	public static function fetchCompanyNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyNotification::class, $objDatabase );
	}

	public static function fetchCompanyNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_notifications', $objDatabase );
	}

	public static function fetchCompanyNotificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyNotification( sprintf( 'SELECT * FROM company_notifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyNotificationsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyNotifications( sprintf( 'SELECT * FROM company_notifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyNotificationsByPropertyNotificationGroupIdByCid( $intPropertyNotificationGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyNotifications( sprintf( 'SELECT * FROM company_notifications WHERE property_notification_group_id = %d AND cid = %d', ( int ) $intPropertyNotificationGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>