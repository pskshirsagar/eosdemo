<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpecial extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.specials';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSpecialRecipientId;
	protected $m_intSpecialTypeId;
	protected $m_intSpecialGroupId;
	protected $m_intCompanyMediaFileId;
	protected $m_intApplicationStageStatusId;
	protected $m_strRemotePrimaryKey;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strNotes;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intMinDaysToLeaseStart;
	protected $m_intMaxDaysToLeaseStart;
	protected $m_fltGiftValue;
	protected $m_intRecurringPaymentDayRestriction;
	protected $m_strCouponCode;
	protected $m_boolCouponCodeRequired;
	protected $m_intQuantityBudgeted;
	protected $m_intQuantityRemaining;
	protected $m_boolLimitQuantity;
	protected $m_boolHideEndDate;
	protected $m_boolIsSystem;
	protected $m_boolIsActive;
	protected $m_boolIsDefault;
	protected $m_boolShowOnWebsite;
	protected $m_boolIsAutoFulfilled;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImportRenewalTemplateTierId;
	protected $m_strQualifyingCriteria;

	public function __construct() {
		parent::__construct();

		$this->m_intMinDaysToLeaseStart = '0';
		$this->m_intMaxDaysToLeaseStart = '0';
		$this->m_fltGiftValue = '0';
		$this->m_intRecurringPaymentDayRestriction = '0';
		$this->m_boolCouponCodeRequired = false;
		$this->m_boolLimitQuantity = false;
		$this->m_boolHideEndDate = false;
		$this->m_boolIsSystem = true;
		$this->m_boolIsActive = true;
		$this->m_boolIsDefault = false;
		$this->m_boolShowOnWebsite = true;
		$this->m_boolIsAutoFulfilled = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['special_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intSpecialRecipientId', trim( $arrValues['special_recipient_id'] ) ); elseif( isset( $arrValues['special_recipient_id'] ) ) $this->setSpecialRecipientId( $arrValues['special_recipient_id'] );
		if( isset( $arrValues['special_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSpecialTypeId', trim( $arrValues['special_type_id'] ) ); elseif( isset( $arrValues['special_type_id'] ) ) $this->setSpecialTypeId( $arrValues['special_type_id'] );
		if( isset( $arrValues['special_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSpecialGroupId', trim( $arrValues['special_group_id'] ) ); elseif( isset( $arrValues['special_group_id'] ) ) $this->setSpecialGroupId( $arrValues['special_group_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['application_stage_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageStatusId', trim( $arrValues['application_stage_status_id'] ) ); elseif( isset( $arrValues['application_stage_status_id'] ) ) $this->setApplicationStageStatusId( $arrValues['application_stage_status_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['notes'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['min_days_to_lease_start'] ) && $boolDirectSet ) $this->set( 'm_intMinDaysToLeaseStart', trim( $arrValues['min_days_to_lease_start'] ) ); elseif( isset( $arrValues['min_days_to_lease_start'] ) ) $this->setMinDaysToLeaseStart( $arrValues['min_days_to_lease_start'] );
		if( isset( $arrValues['max_days_to_lease_start'] ) && $boolDirectSet ) $this->set( 'm_intMaxDaysToLeaseStart', trim( $arrValues['max_days_to_lease_start'] ) ); elseif( isset( $arrValues['max_days_to_lease_start'] ) ) $this->setMaxDaysToLeaseStart( $arrValues['max_days_to_lease_start'] );
		if( isset( $arrValues['gift_value'] ) && $boolDirectSet ) $this->set( 'm_fltGiftValue', trim( $arrValues['gift_value'] ) ); elseif( isset( $arrValues['gift_value'] ) ) $this->setGiftValue( $arrValues['gift_value'] );
		if( isset( $arrValues['recurring_payment_day_restriction'] ) && $boolDirectSet ) $this->set( 'm_intRecurringPaymentDayRestriction', trim( $arrValues['recurring_payment_day_restriction'] ) ); elseif( isset( $arrValues['recurring_payment_day_restriction'] ) ) $this->setRecurringPaymentDayRestriction( $arrValues['recurring_payment_day_restriction'] );
		if( isset( $arrValues['coupon_code'] ) && $boolDirectSet ) $this->set( 'm_strCouponCode', trim( $arrValues['coupon_code'] ) ); elseif( isset( $arrValues['coupon_code'] ) ) $this->setCouponCode( $arrValues['coupon_code'] );
		if( isset( $arrValues['coupon_code_required'] ) && $boolDirectSet ) $this->set( 'm_boolCouponCodeRequired', trim( stripcslashes( $arrValues['coupon_code_required'] ) ) ); elseif( isset( $arrValues['coupon_code_required'] ) ) $this->setCouponCodeRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['coupon_code_required'] ) : $arrValues['coupon_code_required'] );
		if( isset( $arrValues['quantity_budgeted'] ) && $boolDirectSet ) $this->set( 'm_intQuantityBudgeted', trim( $arrValues['quantity_budgeted'] ) ); elseif( isset( $arrValues['quantity_budgeted'] ) ) $this->setQuantityBudgeted( $arrValues['quantity_budgeted'] );
		if( isset( $arrValues['quantity_remaining'] ) && $boolDirectSet ) $this->set( 'm_intQuantityRemaining', trim( $arrValues['quantity_remaining'] ) ); elseif( isset( $arrValues['quantity_remaining'] ) ) $this->setQuantityRemaining( $arrValues['quantity_remaining'] );
		if( isset( $arrValues['limit_quantity'] ) && $boolDirectSet ) $this->set( 'm_boolLimitQuantity', trim( stripcslashes( $arrValues['limit_quantity'] ) ) ); elseif( isset( $arrValues['limit_quantity'] ) ) $this->setLimitQuantity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['limit_quantity'] ) : $arrValues['limit_quantity'] );
		if( isset( $arrValues['hide_end_date'] ) && $boolDirectSet ) $this->set( 'm_boolHideEndDate', trim( stripcslashes( $arrValues['hide_end_date'] ) ) ); elseif( isset( $arrValues['hide_end_date'] ) ) $this->setHideEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_end_date'] ) : $arrValues['hide_end_date'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['is_auto_fulfilled'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoFulfilled', trim( stripcslashes( $arrValues['is_auto_fulfilled'] ) ) ); elseif( isset( $arrValues['is_auto_fulfilled'] ) ) $this->setIsAutoFulfilled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_fulfilled'] ) : $arrValues['is_auto_fulfilled'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['import_renewal_template_tier_id'] ) && $boolDirectSet ) $this->set( 'm_intImportRenewalTemplateTierId', trim( $arrValues['import_renewal_template_tier_id'] ) ); elseif( isset( $arrValues['import_renewal_template_tier_id'] ) ) $this->setImportRenewalTemplateTierId( $arrValues['import_renewal_template_tier_id'] );
		if( isset( $arrValues['qualifying_criteria'] ) && $boolDirectSet ) $this->set( 'm_strQualifyingCriteria', trim( stripcslashes( $arrValues['qualifying_criteria'] ) ) ); elseif( isset( $arrValues['qualifying_criteria'] ) ) $this->setQualifyingCriteria( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qualifying_criteria'] ) : $arrValues['qualifying_criteria'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSpecialRecipientId( $intSpecialRecipientId ) {
		$this->set( 'm_intSpecialRecipientId', CStrings::strToIntDef( $intSpecialRecipientId, NULL, false ) );
	}

	public function getSpecialRecipientId() {
		return $this->m_intSpecialRecipientId;
	}

	public function sqlSpecialRecipientId() {
		return ( true == isset( $this->m_intSpecialRecipientId ) ) ? ( string ) $this->m_intSpecialRecipientId : 'NULL';
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		$this->set( 'm_intSpecialTypeId', CStrings::strToIntDef( $intSpecialTypeId, NULL, false ) );
	}

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	public function sqlSpecialTypeId() {
		return ( true == isset( $this->m_intSpecialTypeId ) ) ? ( string ) $this->m_intSpecialTypeId : 'NULL';
	}

	public function setSpecialGroupId( $intSpecialGroupId ) {
		$this->set( 'm_intSpecialGroupId', CStrings::strToIntDef( $intSpecialGroupId, NULL, false ) );
	}

	public function getSpecialGroupId() {
		return $this->m_intSpecialGroupId;
	}

	public function sqlSpecialGroupId() {
		return ( true == isset( $this->m_intSpecialGroupId ) ) ? ( string ) $this->m_intSpecialGroupId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->set( 'm_intApplicationStageStatusId', CStrings::strToIntDef( $intApplicationStageStatusId, NULL, false ) );
	}

	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	public function sqlApplicationStageStatusId() {
		return ( true == isset( $this->m_intApplicationStageStatusId ) ) ? ( string ) $this->m_intApplicationStageStatusId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 60, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ), $strLocaleCode );
	}

	public function getNotes( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNotes', $strLocaleCode );
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setMinDaysToLeaseStart( $intMinDaysToLeaseStart ) {
		$this->set( 'm_intMinDaysToLeaseStart', CStrings::strToIntDef( $intMinDaysToLeaseStart, NULL, false ) );
	}

	public function getMinDaysToLeaseStart() {
		return $this->m_intMinDaysToLeaseStart;
	}

	public function sqlMinDaysToLeaseStart() {
		return ( true == isset( $this->m_intMinDaysToLeaseStart ) ) ? ( string ) $this->m_intMinDaysToLeaseStart : '0';
	}

	public function setMaxDaysToLeaseStart( $intMaxDaysToLeaseStart ) {
		$this->set( 'm_intMaxDaysToLeaseStart', CStrings::strToIntDef( $intMaxDaysToLeaseStart, NULL, false ) );
	}

	public function getMaxDaysToLeaseStart() {
		return $this->m_intMaxDaysToLeaseStart;
	}

	public function sqlMaxDaysToLeaseStart() {
		return ( true == isset( $this->m_intMaxDaysToLeaseStart ) ) ? ( string ) $this->m_intMaxDaysToLeaseStart : '0';
	}

	public function setGiftValue( $fltGiftValue ) {
		$this->set( 'm_fltGiftValue', CStrings::strToFloatDef( $fltGiftValue, NULL, false, 2 ) );
	}

	public function getGiftValue() {
		return $this->m_fltGiftValue;
	}

	public function sqlGiftValue() {
		return ( true == isset( $this->m_fltGiftValue ) ) ? ( string ) $this->m_fltGiftValue : '0';
	}

	public function setRecurringPaymentDayRestriction( $intRecurringPaymentDayRestriction ) {
		$this->set( 'm_intRecurringPaymentDayRestriction', CStrings::strToIntDef( $intRecurringPaymentDayRestriction, NULL, false ) );
	}

	public function getRecurringPaymentDayRestriction() {
		return $this->m_intRecurringPaymentDayRestriction;
	}

	public function sqlRecurringPaymentDayRestriction() {
		return ( true == isset( $this->m_intRecurringPaymentDayRestriction ) ) ? ( string ) $this->m_intRecurringPaymentDayRestriction : '0';
	}

	public function setCouponCode( $strCouponCode ) {
		$this->set( 'm_strCouponCode', CStrings::strTrimDef( $strCouponCode, 64, NULL, true ) );
	}

	public function getCouponCode() {
		return $this->m_strCouponCode;
	}

	public function sqlCouponCode() {
		return ( true == isset( $this->m_strCouponCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCouponCode ) : '\'' . addslashes( $this->m_strCouponCode ) . '\'' ) : 'NULL';
	}

	public function setCouponCodeRequired( $boolCouponCodeRequired ) {
		$this->set( 'm_boolCouponCodeRequired', CStrings::strToBool( $boolCouponCodeRequired ) );
	}

	public function getCouponCodeRequired() {
		return $this->m_boolCouponCodeRequired;
	}

	public function sqlCouponCodeRequired() {
		return ( true == isset( $this->m_boolCouponCodeRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolCouponCodeRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setQuantityBudgeted( $intQuantityBudgeted ) {
		$this->set( 'm_intQuantityBudgeted', CStrings::strToIntDef( $intQuantityBudgeted, NULL, false ) );
	}

	public function getQuantityBudgeted() {
		return $this->m_intQuantityBudgeted;
	}

	public function sqlQuantityBudgeted() {
		return ( true == isset( $this->m_intQuantityBudgeted ) ) ? ( string ) $this->m_intQuantityBudgeted : 'NULL';
	}

	public function setQuantityRemaining( $intQuantityRemaining ) {
		$this->set( 'm_intQuantityRemaining', CStrings::strToIntDef( $intQuantityRemaining, NULL, false ) );
	}

	public function getQuantityRemaining() {
		return $this->m_intQuantityRemaining;
	}

	public function sqlQuantityRemaining() {
		return ( true == isset( $this->m_intQuantityRemaining ) ) ? ( string ) $this->m_intQuantityRemaining : 'NULL';
	}

	public function setLimitQuantity( $boolLimitQuantity ) {
		$this->set( 'm_boolLimitQuantity', CStrings::strToBool( $boolLimitQuantity ) );
	}

	public function getLimitQuantity() {
		return $this->m_boolLimitQuantity;
	}

	public function sqlLimitQuantity() {
		return ( true == isset( $this->m_boolLimitQuantity ) ) ? '\'' . ( true == ( bool ) $this->m_boolLimitQuantity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideEndDate( $boolHideEndDate ) {
		$this->set( 'm_boolHideEndDate', CStrings::strToBool( $boolHideEndDate ) );
	}

	public function getHideEndDate() {
		return $this->m_boolHideEndDate;
	}

	public function sqlHideEndDate() {
		return ( true == isset( $this->m_boolHideEndDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideEndDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoFulfilled( $boolIsAutoFulfilled ) {
		$this->set( 'm_boolIsAutoFulfilled', CStrings::strToBool( $boolIsAutoFulfilled ) );
	}

	public function getIsAutoFulfilled() {
		return $this->m_boolIsAutoFulfilled;
	}

	public function sqlIsAutoFulfilled() {
		return ( true == isset( $this->m_boolIsAutoFulfilled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoFulfilled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImportRenewalTemplateTierId( $intImportRenewalTemplateTierId ) {
		$this->set( 'm_intImportRenewalTemplateTierId', CStrings::strToIntDef( $intImportRenewalTemplateTierId, NULL, false ) );
	}

	public function getImportRenewalTemplateTierId() {
		return $this->m_intImportRenewalTemplateTierId;
	}

	public function sqlImportRenewalTemplateTierId() {
		return ( true == isset( $this->m_intImportRenewalTemplateTierId ) ) ? ( string ) $this->m_intImportRenewalTemplateTierId : 'NULL';
	}

	public function setQualifyingCriteria( $strQualifyingCriteria ) {
		$this->set( 'm_strQualifyingCriteria', CStrings::strTrimDef( $strQualifyingCriteria, -1, NULL, true ) );
	}

	public function getQualifyingCriteria() {
		return $this->m_strQualifyingCriteria;
	}

	public function sqlQualifyingCriteria() {
		return ( true == isset( $this->m_strQualifyingCriteria ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQualifyingCriteria ) : '\'' . addslashes( $this->m_strQualifyingCriteria ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, special_recipient_id, special_type_id, special_group_id, company_media_file_id, application_stage_status_id, remote_primary_key, name, description, notes, details, start_date, end_date, min_days_to_lease_start, max_days_to_lease_start, gift_value, recurring_payment_day_restriction, coupon_code, coupon_code_required, quantity_budgeted, quantity_remaining, limit_quantity, hide_end_date, is_system, is_active, is_default, show_on_website, is_auto_fulfilled, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, import_renewal_template_tier_id, qualifying_criteria )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlSpecialRecipientId() . ', ' .
		          $this->sqlSpecialTypeId() . ', ' .
		          $this->sqlSpecialGroupId() . ', ' .
		          $this->sqlCompanyMediaFileId() . ', ' .
		          $this->sqlApplicationStageStatusId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlNotes() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlStartDate() . ', ' .
		          $this->sqlEndDate() . ', ' .
		          $this->sqlMinDaysToLeaseStart() . ', ' .
		          $this->sqlMaxDaysToLeaseStart() . ', ' .
		          $this->sqlGiftValue() . ', ' .
		          $this->sqlRecurringPaymentDayRestriction() . ', ' .
		          $this->sqlCouponCode() . ', ' .
		          $this->sqlCouponCodeRequired() . ', ' .
		          $this->sqlQuantityBudgeted() . ', ' .
		          $this->sqlQuantityRemaining() . ', ' .
		          $this->sqlLimitQuantity() . ', ' .
		          $this->sqlHideEndDate() . ', ' .
		          $this->sqlIsSystem() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          $this->sqlIsDefault() . ', ' .
		          $this->sqlShowOnWebsite() . ', ' .
		          $this->sqlIsAutoFulfilled() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlImportRenewalTemplateTierId() . ', ' .
		          $this->sqlQualifyingCriteria() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_recipient_id = ' . $this->sqlSpecialRecipientId(). ',' ; } elseif( true == array_key_exists( 'SpecialRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' special_recipient_id = ' . $this->sqlSpecialRecipientId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_type_id = ' . $this->sqlSpecialTypeId(). ',' ; } elseif( true == array_key_exists( 'SpecialTypeId', $this->getChangedColumns() ) ) { $strSql .= ' special_type_id = ' . $this->sqlSpecialTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_group_id = ' . $this->sqlSpecialGroupId(). ',' ; } elseif( true == array_key_exists( 'SpecialGroupId', $this->getChangedColumns() ) ) { $strSql .= ' special_group_id = ' . $this->sqlSpecialGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStageStatusId', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_days_to_lease_start = ' . $this->sqlMinDaysToLeaseStart(). ',' ; } elseif( true == array_key_exists( 'MinDaysToLeaseStart', $this->getChangedColumns() ) ) { $strSql .= ' min_days_to_lease_start = ' . $this->sqlMinDaysToLeaseStart() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_days_to_lease_start = ' . $this->sqlMaxDaysToLeaseStart(). ',' ; } elseif( true == array_key_exists( 'MaxDaysToLeaseStart', $this->getChangedColumns() ) ) { $strSql .= ' max_days_to_lease_start = ' . $this->sqlMaxDaysToLeaseStart() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gift_value = ' . $this->sqlGiftValue(). ',' ; } elseif( true == array_key_exists( 'GiftValue', $this->getChangedColumns() ) ) { $strSql .= ' gift_value = ' . $this->sqlGiftValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_payment_day_restriction = ' . $this->sqlRecurringPaymentDayRestriction(). ',' ; } elseif( true == array_key_exists( 'RecurringPaymentDayRestriction', $this->getChangedColumns() ) ) { $strSql .= ' recurring_payment_day_restriction = ' . $this->sqlRecurringPaymentDayRestriction() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coupon_code = ' . $this->sqlCouponCode(). ',' ; } elseif( true == array_key_exists( 'CouponCode', $this->getChangedColumns() ) ) { $strSql .= ' coupon_code = ' . $this->sqlCouponCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coupon_code_required = ' . $this->sqlCouponCodeRequired(). ',' ; } elseif( true == array_key_exists( 'CouponCodeRequired', $this->getChangedColumns() ) ) { $strSql .= ' coupon_code_required = ' . $this->sqlCouponCodeRequired() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity_budgeted = ' . $this->sqlQuantityBudgeted(). ',' ; } elseif( true == array_key_exists( 'QuantityBudgeted', $this->getChangedColumns() ) ) { $strSql .= ' quantity_budgeted = ' . $this->sqlQuantityBudgeted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity_remaining = ' . $this->sqlQuantityRemaining(). ',' ; } elseif( true == array_key_exists( 'QuantityRemaining', $this->getChangedColumns() ) ) { $strSql .= ' quantity_remaining = ' . $this->sqlQuantityRemaining() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' limit_quantity = ' . $this->sqlLimitQuantity(). ',' ; } elseif( true == array_key_exists( 'LimitQuantity', $this->getChangedColumns() ) ) { $strSql .= ' limit_quantity = ' . $this->sqlLimitQuantity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_end_date = ' . $this->sqlHideEndDate(). ',' ; } elseif( true == array_key_exists( 'HideEndDate', $this->getChangedColumns() ) ) { $strSql .= ' hide_end_date = ' . $this->sqlHideEndDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_fulfilled = ' . $this->sqlIsAutoFulfilled(). ',' ; } elseif( true == array_key_exists( 'IsAutoFulfilled', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_fulfilled = ' . $this->sqlIsAutoFulfilled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_renewal_template_tier_id = ' . $this->sqlImportRenewalTemplateTierId(). ',' ; } elseif( true == array_key_exists( 'ImportRenewalTemplateTierId', $this->getChangedColumns() ) ) { $strSql .= ' import_renewal_template_tier_id = ' . $this->sqlImportRenewalTemplateTierId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qualifying_criteria = ' . $this->sqlQualifyingCriteria(). ',' ; } elseif( true == array_key_exists( 'QualifyingCriteria', $this->getChangedColumns() ) ) { $strSql .= ' qualifying_criteria = ' . $this->sqlQualifyingCriteria() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'special_recipient_id' => $this->getSpecialRecipientId(),
			'special_type_id' => $this->getSpecialTypeId(),
			'special_group_id' => $this->getSpecialGroupId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'application_stage_status_id' => $this->getApplicationStageStatusId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'notes' => $this->getNotes(),
			'details' => $this->getDetails(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'min_days_to_lease_start' => $this->getMinDaysToLeaseStart(),
			'max_days_to_lease_start' => $this->getMaxDaysToLeaseStart(),
			'gift_value' => $this->getGiftValue(),
			'recurring_payment_day_restriction' => $this->getRecurringPaymentDayRestriction(),
			'coupon_code' => $this->getCouponCode(),
			'coupon_code_required' => $this->getCouponCodeRequired(),
			'quantity_budgeted' => $this->getQuantityBudgeted(),
			'quantity_remaining' => $this->getQuantityRemaining(),
			'limit_quantity' => $this->getLimitQuantity(),
			'hide_end_date' => $this->getHideEndDate(),
			'is_system' => $this->getIsSystem(),
			'is_active' => $this->getIsActive(),
			'is_default' => $this->getIsDefault(),
			'show_on_website' => $this->getShowOnWebsite(),
			'is_auto_fulfilled' => $this->getIsAutoFulfilled(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'import_renewal_template_tier_id' => $this->getImportRenewalTemplateTierId(),
			'qualifying_criteria' => $this->getQualifyingCriteria()
		);
	}

}
?>