<?php

class CBaseDefaultLeaseStartWindow extends CEosSingularBase {

	const TABLE_NAME = 'public.default_lease_start_windows';

	protected $m_intId;
	protected $m_intDefaultLeaseStartStructureId;
	protected $m_intOffsetStartDays;
	protected $m_intOffsetEndDays;
	protected $m_boolIsDefault;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDefault = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_lease_start_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseStartStructureId', trim( $arrValues['default_lease_start_structure_id'] ) ); elseif( isset( $arrValues['default_lease_start_structure_id'] ) ) $this->setDefaultLeaseStartStructureId( $arrValues['default_lease_start_structure_id'] );
		if( isset( $arrValues['offset_start_days'] ) && $boolDirectSet ) $this->set( 'm_intOffsetStartDays', trim( $arrValues['offset_start_days'] ) ); elseif( isset( $arrValues['offset_start_days'] ) ) $this->setOffsetStartDays( $arrValues['offset_start_days'] );
		if( isset( $arrValues['offset_end_days'] ) && $boolDirectSet ) $this->set( 'm_intOffsetEndDays', trim( $arrValues['offset_end_days'] ) ); elseif( isset( $arrValues['offset_end_days'] ) ) $this->setOffsetEndDays( $arrValues['offset_end_days'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultLeaseStartStructureId( $intDefaultLeaseStartStructureId ) {
		$this->set( 'm_intDefaultLeaseStartStructureId', CStrings::strToIntDef( $intDefaultLeaseStartStructureId, NULL, false ) );
	}

	public function getDefaultLeaseStartStructureId() {
		return $this->m_intDefaultLeaseStartStructureId;
	}

	public function sqlDefaultLeaseStartStructureId() {
		return ( true == isset( $this->m_intDefaultLeaseStartStructureId ) ) ? ( string ) $this->m_intDefaultLeaseStartStructureId : 'NULL';
	}

	public function setOffsetStartDays( $intOffsetStartDays ) {
		$this->set( 'm_intOffsetStartDays', CStrings::strToIntDef( $intOffsetStartDays, NULL, false ) );
	}

	public function getOffsetStartDays() {
		return $this->m_intOffsetStartDays;
	}

	public function sqlOffsetStartDays() {
		return ( true == isset( $this->m_intOffsetStartDays ) ) ? ( string ) $this->m_intOffsetStartDays : 'NULL';
	}

	public function setOffsetEndDays( $intOffsetEndDays ) {
		$this->set( 'm_intOffsetEndDays', CStrings::strToIntDef( $intOffsetEndDays, NULL, false ) );
	}

	public function getOffsetEndDays() {
		return $this->m_intOffsetEndDays;
	}

	public function sqlOffsetEndDays() {
		return ( true == isset( $this->m_intOffsetEndDays ) ) ? ( string ) $this->m_intOffsetEndDays : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_lease_start_structure_id' => $this->getDefaultLeaseStartStructureId(),
			'offset_start_days' => $this->getOffsetStartDays(),
			'offset_end_days' => $this->getOffsetEndDays(),
			'is_default' => $this->getIsDefault()
		);
	}

}
?>