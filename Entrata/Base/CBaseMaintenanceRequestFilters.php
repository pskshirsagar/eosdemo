<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestFilters extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestFilter[]
	 */
	public static function fetchMaintenanceRequestFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestFilter::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestFilter
	 */
	public static function fetchMaintenanceRequestFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestFilter::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_filters', $objDatabase );
	}

	public static function fetchMaintenanceRequestFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestFilter( sprintf( 'SELECT * FROM maintenance_request_filters WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestFilters( sprintf( 'SELECT * FROM maintenance_request_filters WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestFiltersByMaintenanceRequestTypeIdByCid( $intMaintenanceRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestFilters( sprintf( 'SELECT * FROM maintenance_request_filters WHERE maintenance_request_type_id = %d AND cid = %d', $intMaintenanceRequestTypeId, $intCid ), $objDatabase );
	}

}
?>