<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitPackages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitPackages extends CEosPluralBase {

	/**
	 * @return CUnitPackage[]
	 */
	public static function fetchUnitPackages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitPackage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitPackage
	 */
	public static function fetchUnitPackage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitPackage', $objDatabase );
	}

	public static function fetchUnitPackageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_packages', $objDatabase );
	}

	public static function fetchUnitPackageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitPackage( sprintf( 'SELECT * FROM unit_packages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByCid( $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByPackageTypeIdByCid( $intPackageTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE package_type_id = %d AND cid = %d', ( int ) $intPackageTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByMessageIdByCid( $intMessageId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE message_id = %d AND cid = %d', ( int ) $intMessageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByAnnouncementIdByCid( $intAnnouncementId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE announcement_id = %d AND cid = %d', ( int ) $intAnnouncementId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE system_email_id = %d AND cid = %d', ( int ) $intSystemEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitPackagesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchUnitPackages( sprintf( 'SELECT * FROM unit_packages WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>