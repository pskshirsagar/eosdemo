<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenancePriorities
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenancePriorities extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenancePriority[]
	 */
	public static function fetchDefaultMaintenancePriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenancePriority::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenancePriority
	 */
	public static function fetchDefaultMaintenancePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenancePriority::class, $objDatabase );
	}

	public static function fetchDefaultMaintenancePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_priorities', $objDatabase );
	}

	public static function fetchDefaultMaintenancePriorityById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenancePriority( sprintf( 'SELECT * FROM default_maintenance_priorities WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMaintenancePrioritiesByMaintenancePriorityTypeId( $intMaintenancePriorityTypeId, $objDatabase ) {
		return self::fetchDefaultMaintenancePriorities( sprintf( 'SELECT * FROM default_maintenance_priorities WHERE maintenance_priority_type_id = %d', $intMaintenancePriorityTypeId ), $objDatabase );
	}

}
?>