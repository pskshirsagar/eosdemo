<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomer extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationDatabaseId;
	protected $m_intLeadSourceId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_intMaritalStatusTypeId;
	protected $m_intLeasingAgentId;
	protected $m_intMessageOperatorId;
	protected $m_strStudentIdNumber;
	protected $m_strParentNameFirst;
	protected $m_strParentNameLast;
	protected $m_strParentEmailAddress;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intSecondaryPhoneNumberTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSecondaryNumber;
	protected $m_strCompanyName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameFull;
	protected $m_strNameSuffix;
	protected $m_strNameMaiden;
	protected $m_strNameSpouse;
	protected $m_strPrimaryStreetLine1;
	protected $m_strPrimaryStreetLine2;
	protected $m_strPrimaryStreetLine3;
	protected $m_strPrimaryCity;
	protected $m_strPrimaryStateCode;
	protected $m_strPrimaryPostalCode;
	protected $m_strPrimaryCountryCode;
	protected $m_boolPrimaryIsVerified;
	protected $m_strPreviousStreetLine1;
	protected $m_strPreviousStreetLine2;
	protected $m_strPreviousStreetLine3;
	protected $m_strPreviousCity;
	protected $m_strPreviousStateCode;
	protected $m_strPreviousPostalCode;
	protected $m_strPreviousCountryCode;
	protected $m_strPreviousProvince;
	protected $m_boolPreviousIsVerified;
	protected $m_strPermanentStreetLine1;
	protected $m_strPermanentStreetLine2;
	protected $m_strPermanentStreetLine3;
	protected $m_strPermanentCity;
	protected $m_strPermanentStateCode;
	protected $m_strPermanentPostalCode;
	protected $m_strPermanentCountryCode;
	protected $m_strPermanentProvince;
	protected $m_boolPermanentIsVerified;
	protected $m_strPrimaryVehicleMake;
	protected $m_strPrimaryVehicleModel;
	protected $m_intPrimaryVehicleYear;
	protected $m_strPrimaryVehicleColor;
	protected $m_strPrimaryVehicleLicensePlateNumber;
	protected $m_strPrimaryVehicleStateCode;
	protected $m_strPrimaryVehicleProvince;
	protected $m_strSecondaryVehicleMake;
	protected $m_strSecondaryVehicleModel;
	protected $m_intSecondaryVehicleYear;
	protected $m_strSecondaryVehicleColor;
	protected $m_strSecondaryVehicleLicensePlateNumber;
	protected $m_strSecondaryVehicleStateCode;
	protected $m_strSecondaryVehicleProvince;
	protected $m_strPhoneNumber;
	protected $m_strMobileNumber;
	protected $m_strWorkNumber;
	protected $m_strFaxNumber;
	protected $m_strEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTaxNumberMasked;
	protected $m_intReturnedPaymentsCount;
	protected $m_strBirthDate;
	protected $m_strGender;
	protected $m_strDlNumberEncrypted;
	protected $m_strDlStateCode;
	protected $m_strDlProvince;
	protected $m_intCompanyIdentificationTypeId;
	protected $m_strIdentificationValue;
	protected $m_strIdentificationExpiration;
	protected $m_strNotes;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strPasswordQuestion;
	protected $m_strPasswordAnswerEncrypted;
	protected $m_strPasswordExpiration;
	protected $m_strFacebookUser;
	protected $m_intDontAllowLogin;
	protected $m_intDontAcceptPayments;
	protected $m_intBlockPosting;
	protected $m_intBlockNetworking;
	protected $m_intBlockOnlinePayments;
	protected $m_intIsNetworkingEnrolled;
	protected $m_strApprovalIpAddress;
	protected $m_strTermsApprovalIp;
	protected $m_strTermsApprovedOn;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_intProfilePublishedBy;
	protected $m_strProfilePublishedOn;
	protected $m_strMobileNumberAuthorizedOn;
	protected $m_strImportedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsOrganization;
	protected $m_strAltNameFirst;
	protected $m_strAltNameMiddle;
	protected $m_strAltNameLast;
	protected $m_strPreferredName;
	protected $m_strPreferredLocaleCode;
	protected $m_strNameLastMatronymic;
	protected $m_intHasNameLastMatronymic;
	protected $m_strCitizenshipCountryCode;
	protected $m_intTaxIdTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strTaxNumberBlindIndex;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intReturnedPaymentsCount = '0';
		$this->m_intDontAllowLogin = '0';
		$this->m_boolIsOrganization = false;
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strPrimaryStreetLine1' => 'addressLine1',
				'm_strPrimaryStreetLine2' => 'addressLine2',
				'm_strPrimaryStreetLine3' => 'addressLine3',
				'm_strPrimaryCity' => 'locality',
				'm_strPrimaryStateCode' => 'administrativeArea',
				'm_strPrimaryPostalCode' => 'postalCode',
				'm_strPrimaryCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['payment_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAllowanceTypeId', trim( $arrValues['payment_allowance_type_id'] ) ); elseif( isset( $arrValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrValues['payment_allowance_type_id'] );
		if( isset( $arrValues['marital_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaritalStatusTypeId', trim( $arrValues['marital_status_type_id'] ) ); elseif( isset( $arrValues['marital_status_type_id'] ) ) $this->setMaritalStatusTypeId( $arrValues['marital_status_type_id'] );
		if( isset( $arrValues['leasing_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingAgentId', trim( $arrValues['leasing_agent_id'] ) ); elseif( isset( $arrValues['leasing_agent_id'] ) ) $this->setLeasingAgentId( $arrValues['leasing_agent_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['student_id_number'] ) && $boolDirectSet ) $this->set( 'm_strStudentIdNumber', trim( $arrValues['student_id_number'] ) ); elseif( isset( $arrValues['student_id_number'] ) ) $this->setStudentIdNumber( $arrValues['student_id_number'] );
		if( isset( $arrValues['parent_name_first'] ) && $boolDirectSet ) $this->set( 'm_strParentNameFirst', trim( $arrValues['parent_name_first'] ) ); elseif( isset( $arrValues['parent_name_first'] ) ) $this->setParentNameFirst( $arrValues['parent_name_first'] );
		if( isset( $arrValues['parent_name_last'] ) && $boolDirectSet ) $this->set( 'm_strParentNameLast', trim( $arrValues['parent_name_last'] ) ); elseif( isset( $arrValues['parent_name_last'] ) ) $this->setParentNameLast( $arrValues['parent_name_last'] );
		if( isset( $arrValues['parent_email_address'] ) && $boolDirectSet ) $this->set( 'm_strParentEmailAddress', trim( $arrValues['parent_email_address'] ) ); elseif( isset( $arrValues['parent_email_address'] ) ) $this->setParentEmailAddress( $arrValues['parent_email_address'] );
		if( isset( $arrValues['primary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryPhoneNumberTypeId', trim( $arrValues['primary_phone_number_type_id'] ) ); elseif( isset( $arrValues['primary_phone_number_type_id'] ) ) $this->setPrimaryPhoneNumberTypeId( $arrValues['primary_phone_number_type_id'] );
		if( isset( $arrValues['secondary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryPhoneNumberTypeId', trim( $arrValues['secondary_phone_number_type_id'] ) ); elseif( isset( $arrValues['secondary_phone_number_type_id'] ) ) $this->setSecondaryPhoneNumberTypeId( $arrValues['secondary_phone_number_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( $arrValues['secondary_number'] ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrValues['secondary_number'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_full'] ) && $boolDirectSet ) $this->set( 'm_strNameFull', trim( $arrValues['name_full'] ) ); elseif( isset( $arrValues['name_full'] ) ) $this->setNameFull( $arrValues['name_full'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['name_spouse'] ) && $boolDirectSet ) $this->set( 'm_strNameSpouse', trim( $arrValues['name_spouse'] ) ); elseif( isset( $arrValues['name_spouse'] ) ) $this->setNameSpouse( $arrValues['name_spouse'] );
		if( isset( $arrValues['primary_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryStreetLine1', trim( $arrValues['primary_street_line1'] ) ); elseif( isset( $arrValues['primary_street_line1'] ) ) $this->setPrimaryStreetLine1( $arrValues['primary_street_line1'] );
		if( isset( $arrValues['primary_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryStreetLine2', trim( $arrValues['primary_street_line2'] ) ); elseif( isset( $arrValues['primary_street_line2'] ) ) $this->setPrimaryStreetLine2( $arrValues['primary_street_line2'] );
		if( isset( $arrValues['primary_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryStreetLine3', trim( $arrValues['primary_street_line3'] ) ); elseif( isset( $arrValues['primary_street_line3'] ) ) $this->setPrimaryStreetLine3( $arrValues['primary_street_line3'] );
		if( isset( $arrValues['primary_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryCity', trim( $arrValues['primary_city'] ) ); elseif( isset( $arrValues['primary_city'] ) ) $this->setPrimaryCity( $arrValues['primary_city'] );
		if( isset( $arrValues['primary_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryStateCode', trim( $arrValues['primary_state_code'] ) ); elseif( isset( $arrValues['primary_state_code'] ) ) $this->setPrimaryStateCode( $arrValues['primary_state_code'] );
		if( isset( $arrValues['primary_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryPostalCode', trim( $arrValues['primary_postal_code'] ) ); elseif( isset( $arrValues['primary_postal_code'] ) ) $this->setPrimaryPostalCode( $arrValues['primary_postal_code'] );
		if( isset( $arrValues['primary_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPrimaryCountryCode', trim( $arrValues['primary_country_code'] ) ); elseif( isset( $arrValues['primary_country_code'] ) ) $this->setPrimaryCountryCode( $arrValues['primary_country_code'] );
		if( isset( $arrValues['primary_is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolPrimaryIsVerified', trim( stripcslashes( $arrValues['primary_is_verified'] ) ) ); elseif( isset( $arrValues['primary_is_verified'] ) ) $this->setPrimaryIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_is_verified'] ) : $arrValues['primary_is_verified'] );
		if( isset( $arrValues['previous_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strPreviousStreetLine1', trim( $arrValues['previous_street_line1'] ) ); elseif( isset( $arrValues['previous_street_line1'] ) ) $this->setPreviousStreetLine1( $arrValues['previous_street_line1'] );
		if( isset( $arrValues['previous_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strPreviousStreetLine2', trim( $arrValues['previous_street_line2'] ) ); elseif( isset( $arrValues['previous_street_line2'] ) ) $this->setPreviousStreetLine2( $arrValues['previous_street_line2'] );
		if( isset( $arrValues['previous_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strPreviousStreetLine3', trim( $arrValues['previous_street_line3'] ) ); elseif( isset( $arrValues['previous_street_line3'] ) ) $this->setPreviousStreetLine3( $arrValues['previous_street_line3'] );
		if( isset( $arrValues['previous_city'] ) && $boolDirectSet ) $this->set( 'm_strPreviousCity', trim( $arrValues['previous_city'] ) ); elseif( isset( $arrValues['previous_city'] ) ) $this->setPreviousCity( $arrValues['previous_city'] );
		if( isset( $arrValues['previous_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPreviousStateCode', trim( $arrValues['previous_state_code'] ) ); elseif( isset( $arrValues['previous_state_code'] ) ) $this->setPreviousStateCode( $arrValues['previous_state_code'] );
		if( isset( $arrValues['previous_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPreviousPostalCode', trim( $arrValues['previous_postal_code'] ) ); elseif( isset( $arrValues['previous_postal_code'] ) ) $this->setPreviousPostalCode( $arrValues['previous_postal_code'] );
		if( isset( $arrValues['previous_country_code'] ) && $boolDirectSet ) $this->set( 'm_strPreviousCountryCode', trim( $arrValues['previous_country_code'] ) ); elseif( isset( $arrValues['previous_country_code'] ) ) $this->setPreviousCountryCode( $arrValues['previous_country_code'] );
		if( isset( $arrValues['previous_province'] ) && $boolDirectSet ) $this->set( 'm_strPreviousProvince', trim( $arrValues['previous_province'] ) ); elseif( isset( $arrValues['previous_province'] ) ) $this->setPreviousProvince( $arrValues['previous_province'] );
		if( isset( $arrValues['previous_is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolPreviousIsVerified', trim( stripcslashes( $arrValues['previous_is_verified'] ) ) ); elseif( isset( $arrValues['previous_is_verified'] ) ) $this->setPreviousIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['previous_is_verified'] ) : $arrValues['previous_is_verified'] );
		if( isset( $arrValues['permanent_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strPermanentStreetLine1', trim( $arrValues['permanent_street_line1'] ) ); elseif( isset( $arrValues['permanent_street_line1'] ) ) $this->setPermanentStreetLine1( $arrValues['permanent_street_line1'] );
		if( isset( $arrValues['permanent_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strPermanentStreetLine2', trim( $arrValues['permanent_street_line2'] ) ); elseif( isset( $arrValues['permanent_street_line2'] ) ) $this->setPermanentStreetLine2( $arrValues['permanent_street_line2'] );
		if( isset( $arrValues['permanent_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strPermanentStreetLine3', trim( $arrValues['permanent_street_line3'] ) ); elseif( isset( $arrValues['permanent_street_line3'] ) ) $this->setPermanentStreetLine3( $arrValues['permanent_street_line3'] );
		if( isset( $arrValues['permanent_city'] ) && $boolDirectSet ) $this->set( 'm_strPermanentCity', trim( $arrValues['permanent_city'] ) ); elseif( isset( $arrValues['permanent_city'] ) ) $this->setPermanentCity( $arrValues['permanent_city'] );
		if( isset( $arrValues['permanent_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPermanentStateCode', trim( $arrValues['permanent_state_code'] ) ); elseif( isset( $arrValues['permanent_state_code'] ) ) $this->setPermanentStateCode( $arrValues['permanent_state_code'] );
		if( isset( $arrValues['permanent_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPermanentPostalCode', trim( $arrValues['permanent_postal_code'] ) ); elseif( isset( $arrValues['permanent_postal_code'] ) ) $this->setPermanentPostalCode( $arrValues['permanent_postal_code'] );
		if( isset( $arrValues['permanent_country_code'] ) && $boolDirectSet ) $this->set( 'm_strPermanentCountryCode', trim( $arrValues['permanent_country_code'] ) ); elseif( isset( $arrValues['permanent_country_code'] ) ) $this->setPermanentCountryCode( $arrValues['permanent_country_code'] );
		if( isset( $arrValues['permanent_province'] ) && $boolDirectSet ) $this->set( 'm_strPermanentProvince', trim( $arrValues['permanent_province'] ) ); elseif( isset( $arrValues['permanent_province'] ) ) $this->setPermanentProvince( $arrValues['permanent_province'] );
		if( isset( $arrValues['permanent_is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolPermanentIsVerified', trim( stripcslashes( $arrValues['permanent_is_verified'] ) ) ); elseif( isset( $arrValues['permanent_is_verified'] ) ) $this->setPermanentIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['permanent_is_verified'] ) : $arrValues['permanent_is_verified'] );
		if( isset( $arrValues['primary_vehicle_make'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleMake', trim( $arrValues['primary_vehicle_make'] ) ); elseif( isset( $arrValues['primary_vehicle_make'] ) ) $this->setPrimaryVehicleMake( $arrValues['primary_vehicle_make'] );
		if( isset( $arrValues['primary_vehicle_model'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleModel', trim( $arrValues['primary_vehicle_model'] ) ); elseif( isset( $arrValues['primary_vehicle_model'] ) ) $this->setPrimaryVehicleModel( $arrValues['primary_vehicle_model'] );
		if( isset( $arrValues['primary_vehicle_year'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryVehicleYear', trim( $arrValues['primary_vehicle_year'] ) ); elseif( isset( $arrValues['primary_vehicle_year'] ) ) $this->setPrimaryVehicleYear( $arrValues['primary_vehicle_year'] );
		if( isset( $arrValues['primary_vehicle_color'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleColor', trim( $arrValues['primary_vehicle_color'] ) ); elseif( isset( $arrValues['primary_vehicle_color'] ) ) $this->setPrimaryVehicleColor( $arrValues['primary_vehicle_color'] );
		if( isset( $arrValues['primary_vehicle_license_plate_number'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleLicensePlateNumber', trim( $arrValues['primary_vehicle_license_plate_number'] ) ); elseif( isset( $arrValues['primary_vehicle_license_plate_number'] ) ) $this->setPrimaryVehicleLicensePlateNumber( $arrValues['primary_vehicle_license_plate_number'] );
		if( isset( $arrValues['primary_vehicle_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleStateCode', trim( $arrValues['primary_vehicle_state_code'] ) ); elseif( isset( $arrValues['primary_vehicle_state_code'] ) ) $this->setPrimaryVehicleStateCode( $arrValues['primary_vehicle_state_code'] );
		if( isset( $arrValues['primary_vehicle_province'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryVehicleProvince', trim( $arrValues['primary_vehicle_province'] ) ); elseif( isset( $arrValues['primary_vehicle_province'] ) ) $this->setPrimaryVehicleProvince( $arrValues['primary_vehicle_province'] );
		if( isset( $arrValues['secondary_vehicle_make'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleMake', trim( $arrValues['secondary_vehicle_make'] ) ); elseif( isset( $arrValues['secondary_vehicle_make'] ) ) $this->setSecondaryVehicleMake( $arrValues['secondary_vehicle_make'] );
		if( isset( $arrValues['secondary_vehicle_model'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleModel', trim( $arrValues['secondary_vehicle_model'] ) ); elseif( isset( $arrValues['secondary_vehicle_model'] ) ) $this->setSecondaryVehicleModel( $arrValues['secondary_vehicle_model'] );
		if( isset( $arrValues['secondary_vehicle_year'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryVehicleYear', trim( $arrValues['secondary_vehicle_year'] ) ); elseif( isset( $arrValues['secondary_vehicle_year'] ) ) $this->setSecondaryVehicleYear( $arrValues['secondary_vehicle_year'] );
		if( isset( $arrValues['secondary_vehicle_color'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleColor', trim( $arrValues['secondary_vehicle_color'] ) ); elseif( isset( $arrValues['secondary_vehicle_color'] ) ) $this->setSecondaryVehicleColor( $arrValues['secondary_vehicle_color'] );
		if( isset( $arrValues['secondary_vehicle_license_plate_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleLicensePlateNumber', trim( $arrValues['secondary_vehicle_license_plate_number'] ) ); elseif( isset( $arrValues['secondary_vehicle_license_plate_number'] ) ) $this->setSecondaryVehicleLicensePlateNumber( $arrValues['secondary_vehicle_license_plate_number'] );
		if( isset( $arrValues['secondary_vehicle_state_code'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleStateCode', trim( $arrValues['secondary_vehicle_state_code'] ) ); elseif( isset( $arrValues['secondary_vehicle_state_code'] ) ) $this->setSecondaryVehicleStateCode( $arrValues['secondary_vehicle_state_code'] );
		if( isset( $arrValues['secondary_vehicle_province'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryVehicleProvince', trim( $arrValues['secondary_vehicle_province'] ) ); elseif( isset( $arrValues['secondary_vehicle_province'] ) ) $this->setSecondaryVehicleProvince( $arrValues['secondary_vehicle_province'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( $arrValues['mobile_number'] ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( $arrValues['mobile_number'] );
		if( isset( $arrValues['work_number'] ) && $boolDirectSet ) $this->set( 'm_strWorkNumber', trim( $arrValues['work_number'] ) ); elseif( isset( $arrValues['work_number'] ) ) $this->setWorkNumber( $arrValues['work_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberMasked', trim( $arrValues['tax_number_masked'] ) ); elseif( isset( $arrValues['tax_number_masked'] ) ) $this->setTaxNumberMasked( $arrValues['tax_number_masked'] );
		if( isset( $arrValues['returned_payments_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedPaymentsCount', trim( $arrValues['returned_payments_count'] ) ); elseif( isset( $arrValues['returned_payments_count'] ) ) $this->setReturnedPaymentsCount( $arrValues['returned_payments_count'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['dl_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDlNumberEncrypted', trim( $arrValues['dl_number_encrypted'] ) ); elseif( isset( $arrValues['dl_number_encrypted'] ) ) $this->setDlNumberEncrypted( $arrValues['dl_number_encrypted'] );
		if( isset( $arrValues['dl_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDlStateCode', trim( $arrValues['dl_state_code'] ) ); elseif( isset( $arrValues['dl_state_code'] ) ) $this->setDlStateCode( $arrValues['dl_state_code'] );
		if( isset( $arrValues['dl_province'] ) && $boolDirectSet ) $this->set( 'm_strDlProvince', trim( $arrValues['dl_province'] ) ); elseif( isset( $arrValues['dl_province'] ) ) $this->setDlProvince( $arrValues['dl_province'] );
		if( isset( $arrValues['company_identification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyIdentificationTypeId', trim( $arrValues['company_identification_type_id'] ) ); elseif( isset( $arrValues['company_identification_type_id'] ) ) $this->setCompanyIdentificationTypeId( $arrValues['company_identification_type_id'] );
		if( isset( $arrValues['identification_value'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationValue', trim( $arrValues['identification_value'] ) ); elseif( isset( $arrValues['identification_value'] ) ) $this->setIdentificationValue( $arrValues['identification_value'] );
		if( isset( $arrValues['identification_expiration'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationExpiration', trim( $arrValues['identification_expiration'] ) ); elseif( isset( $arrValues['identification_expiration'] ) ) $this->setIdentificationExpiration( $arrValues['identification_expiration'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['password_question'] ) && $boolDirectSet ) $this->set( 'm_strPasswordQuestion', trim( $arrValues['password_question'] ) ); elseif( isset( $arrValues['password_question'] ) ) $this->setPasswordQuestion( $arrValues['password_question'] );
		if( isset( $arrValues['password_answer_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordAnswerEncrypted', trim( $arrValues['password_answer_encrypted'] ) ); elseif( isset( $arrValues['password_answer_encrypted'] ) ) $this->setPasswordAnswerEncrypted( $arrValues['password_answer_encrypted'] );
		if( isset( $arrValues['password_expiration'] ) && $boolDirectSet ) $this->set( 'm_strPasswordExpiration', trim( $arrValues['password_expiration'] ) ); elseif( isset( $arrValues['password_expiration'] ) ) $this->setPasswordExpiration( $arrValues['password_expiration'] );
		if( isset( $arrValues['facebook_user'] ) && $boolDirectSet ) $this->set( 'm_strFacebookUser', trim( $arrValues['facebook_user'] ) ); elseif( isset( $arrValues['facebook_user'] ) ) $this->setFacebookUser( $arrValues['facebook_user'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['dont_accept_payments'] ) && $boolDirectSet ) $this->set( 'm_intDontAcceptPayments', trim( $arrValues['dont_accept_payments'] ) ); elseif( isset( $arrValues['dont_accept_payments'] ) ) $this->setDontAcceptPayments( $arrValues['dont_accept_payments'] );
		if( isset( $arrValues['block_posting'] ) && $boolDirectSet ) $this->set( 'm_intBlockPosting', trim( $arrValues['block_posting'] ) ); elseif( isset( $arrValues['block_posting'] ) ) $this->setBlockPosting( $arrValues['block_posting'] );
		if( isset( $arrValues['block_networking'] ) && $boolDirectSet ) $this->set( 'm_intBlockNetworking', trim( $arrValues['block_networking'] ) ); elseif( isset( $arrValues['block_networking'] ) ) $this->setBlockNetworking( $arrValues['block_networking'] );
		if( isset( $arrValues['block_online_payments'] ) && $boolDirectSet ) $this->set( 'm_intBlockOnlinePayments', trim( $arrValues['block_online_payments'] ) ); elseif( isset( $arrValues['block_online_payments'] ) ) $this->setBlockOnlinePayments( $arrValues['block_online_payments'] );
		if( isset( $arrValues['is_networking_enrolled'] ) && $boolDirectSet ) $this->set( 'm_intIsNetworkingEnrolled', trim( $arrValues['is_networking_enrolled'] ) ); elseif( isset( $arrValues['is_networking_enrolled'] ) ) $this->setIsNetworkingEnrolled( $arrValues['is_networking_enrolled'] );
		if( isset( $arrValues['approval_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strApprovalIpAddress', trim( $arrValues['approval_ip_address'] ) ); elseif( isset( $arrValues['approval_ip_address'] ) ) $this->setApprovalIpAddress( $arrValues['approval_ip_address'] );
		if( isset( $arrValues['terms_approval_ip'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovalIp', trim( $arrValues['terms_approval_ip'] ) ); elseif( isset( $arrValues['terms_approval_ip'] ) ) $this->setTermsApprovalIp( $arrValues['terms_approval_ip'] );
		if( isset( $arrValues['terms_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovedOn', trim( $arrValues['terms_approved_on'] ) ); elseif( isset( $arrValues['terms_approved_on'] ) ) $this->setTermsApprovedOn( $arrValues['terms_approved_on'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['profile_published_by'] ) && $boolDirectSet ) $this->set( 'm_intProfilePublishedBy', trim( $arrValues['profile_published_by'] ) ); elseif( isset( $arrValues['profile_published_by'] ) ) $this->setProfilePublishedBy( $arrValues['profile_published_by'] );
		if( isset( $arrValues['profile_published_on'] ) && $boolDirectSet ) $this->set( 'm_strProfilePublishedOn', trim( $arrValues['profile_published_on'] ) ); elseif( isset( $arrValues['profile_published_on'] ) ) $this->setProfilePublishedOn( $arrValues['profile_published_on'] );
		if( isset( $arrValues['mobile_number_authorized_on'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumberAuthorizedOn', trim( $arrValues['mobile_number_authorized_on'] ) ); elseif( isset( $arrValues['mobile_number_authorized_on'] ) ) $this->setMobileNumberAuthorizedOn( $arrValues['mobile_number_authorized_on'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_organization'] ) && $boolDirectSet ) $this->set( 'm_boolIsOrganization', trim( stripcslashes( $arrValues['is_organization'] ) ) ); elseif( isset( $arrValues['is_organization'] ) ) $this->setIsOrganization( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_organization'] ) : $arrValues['is_organization'] );
		if( isset( $arrValues['alt_name_first'] ) && $boolDirectSet ) $this->set( 'm_strAltNameFirst', trim( $arrValues['alt_name_first'] ) ); elseif( isset( $arrValues['alt_name_first'] ) ) $this->setAltNameFirst( $arrValues['alt_name_first'] );
		if( isset( $arrValues['alt_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strAltNameMiddle', trim( $arrValues['alt_name_middle'] ) ); elseif( isset( $arrValues['alt_name_middle'] ) ) $this->setAltNameMiddle( $arrValues['alt_name_middle'] );
		if( isset( $arrValues['alt_name_last'] ) && $boolDirectSet ) $this->set( 'm_strAltNameLast', trim( $arrValues['alt_name_last'] ) ); elseif( isset( $arrValues['alt_name_last'] ) ) $this->setAltNameLast( $arrValues['alt_name_last'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['preferred_locale_code'] ) && $boolDirectSet ) $this->set( 'm_strPreferredLocaleCode', trim( $arrValues['preferred_locale_code'] ) ); elseif( isset( $arrValues['preferred_locale_code'] ) ) $this->setPreferredLocaleCode( $arrValues['preferred_locale_code'] );
		if( isset( $arrValues['name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_strNameLastMatronymic', trim( $arrValues['name_last_matronymic'] ) ); elseif( isset( $arrValues['name_last_matronymic'] ) ) $this->setNameLastMatronymic( $arrValues['name_last_matronymic'] );
		if( isset( $arrValues['has_name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_intHasNameLastMatronymic', trim( $arrValues['has_name_last_matronymic'] ) ); elseif( isset( $arrValues['has_name_last_matronymic'] ) ) $this->setHasNameLastMatronymic( $arrValues['has_name_last_matronymic'] );
		if( isset( $arrValues['citizenship_country_code'] ) && $boolDirectSet ) $this->set( 'm_strCitizenshipCountryCode', trim( $arrValues['citizenship_country_code'] ) ); elseif( isset( $arrValues['citizenship_country_code'] ) ) $this->setCitizenshipCountryCode( $arrValues['citizenship_country_code'] );
		if( isset( $arrValues['tax_id_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxIdTypeId', trim( $arrValues['tax_id_type_id'] ) ); elseif( isset( $arrValues['tax_id_type_id'] ) ) $this->setTaxIdTypeId( $arrValues['tax_id_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['tax_number_blind_index'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberBlindIndex', trim( $arrValues['tax_number_blind_index'] ) ); elseif( isset( $arrValues['tax_number_blind_index'] ) ) $this->setTaxNumberBlindIndex( $arrValues['tax_number_blind_index'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
		$this->set( 'm_intPaymentAllowanceTypeId', CStrings::strToIntDef( $intPaymentAllowanceTypeId, NULL, false ) );
	}

	public function getPaymentAllowanceTypeId() {
		return $this->m_intPaymentAllowanceTypeId;
	}

	public function sqlPaymentAllowanceTypeId() {
		return ( true == isset( $this->m_intPaymentAllowanceTypeId ) ) ? ( string ) $this->m_intPaymentAllowanceTypeId : 'NULL';
	}

	public function setMaritalStatusTypeId( $intMaritalStatusTypeId ) {
		$this->set( 'm_intMaritalStatusTypeId', CStrings::strToIntDef( $intMaritalStatusTypeId, NULL, false ) );
	}

	public function getMaritalStatusTypeId() {
		return $this->m_intMaritalStatusTypeId;
	}

	public function sqlMaritalStatusTypeId() {
		return ( true == isset( $this->m_intMaritalStatusTypeId ) ) ? ( string ) $this->m_intMaritalStatusTypeId : 'NULL';
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->set( 'm_intLeasingAgentId', CStrings::strToIntDef( $intLeasingAgentId, NULL, false ) );
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function sqlLeasingAgentId() {
		return ( true == isset( $this->m_intLeasingAgentId ) ) ? ( string ) $this->m_intLeasingAgentId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setStudentIdNumber( $strStudentIdNumber ) {
		$this->set( 'm_strStudentIdNumber', CStrings::strTrimDef( $strStudentIdNumber, 25, NULL, true ) );
	}

	public function getStudentIdNumber() {
		return $this->m_strStudentIdNumber;
	}

	public function sqlStudentIdNumber() {
		return ( true == isset( $this->m_strStudentIdNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStudentIdNumber ) : '\'' . addslashes( $this->m_strStudentIdNumber ) . '\'' ) : 'NULL';
	}

	public function setParentNameFirst( $strParentNameFirst ) {
		$this->set( 'm_strParentNameFirst', CStrings::strTrimDef( $strParentNameFirst, 50, NULL, true ) );
	}

	public function getParentNameFirst() {
		return $this->m_strParentNameFirst;
	}

	public function sqlParentNameFirst() {
		return ( true == isset( $this->m_strParentNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentNameFirst ) : '\'' . addslashes( $this->m_strParentNameFirst ) . '\'' ) : 'NULL';
	}

	public function setParentNameLast( $strParentNameLast ) {
		$this->set( 'm_strParentNameLast', CStrings::strTrimDef( $strParentNameLast, 50, NULL, true ) );
	}

	public function getParentNameLast() {
		return $this->m_strParentNameLast;
	}

	public function sqlParentNameLast() {
		return ( true == isset( $this->m_strParentNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentNameLast ) : '\'' . addslashes( $this->m_strParentNameLast ) . '\'' ) : 'NULL';
	}

	public function setParentEmailAddress( $strParentEmailAddress ) {
		$this->set( 'm_strParentEmailAddress', CStrings::strTrimDef( $strParentEmailAddress, 240, NULL, true ) );
	}

	public function getParentEmailAddress() {
		return $this->m_strParentEmailAddress;
	}

	public function sqlParentEmailAddress() {
		return ( true == isset( $this->m_strParentEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentEmailAddress ) : '\'' . addslashes( $this->m_strParentEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->set( 'm_intPrimaryPhoneNumberTypeId', CStrings::strToIntDef( $intPrimaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getPrimaryPhoneNumberTypeId() {
		return $this->m_intPrimaryPhoneNumberTypeId;
	}

	public function sqlPrimaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPrimaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intPrimaryPhoneNumberTypeId : 'NULL';
	}

	public function setSecondaryPhoneNumberTypeId( $intSecondaryPhoneNumberTypeId ) {
		$this->set( 'm_intSecondaryPhoneNumberTypeId', CStrings::strToIntDef( $intSecondaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getSecondaryPhoneNumberTypeId() {
		return $this->m_intSecondaryPhoneNumberTypeId;
	}

	public function sqlSecondaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intSecondaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intSecondaryPhoneNumberTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 50, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryNumber ) : '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameFull( $strNameFull ) {
		$this->set( 'm_strNameFull', CStrings::strTrimDef( $strNameFull, 3000, NULL, true ) );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getNameFullKey() {
		return str_replace( ' ', '_', strtolower( $this->getNameFirst() . ' ' . $this->getNameLast() ) );
	}

	public function sqlNameFull() {
		return ( true == isset( $this->m_strNameFull ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFull ) : '\'' . addslashes( $this->m_strNameFull ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 1000, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setNameSpouse( $strNameSpouse ) {
		$this->set( 'm_strNameSpouse', CStrings::strTrimDef( $strNameSpouse, 1000, NULL, true ) );
	}

	public function getNameSpouse() {
		return $this->m_strNameSpouse;
	}

	public function sqlNameSpouse() {
		return ( true == isset( $this->m_strNameSpouse ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSpouse ) : '\'' . addslashes( $this->m_strNameSpouse ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine1( $strPrimaryStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strPrimaryStreetLine1, $strAddressKey = 'default', 'm_strPrimaryStreetLine1' );
	}

	public function getPrimaryStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strPrimaryStreetLine1' );
	}

	public function sqlPrimaryStreetLine1() {
		return ( true == isset( $this->m_strPrimaryStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine1 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine2( $strPrimaryStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strPrimaryStreetLine2, $strAddressKey = 'default', 'm_strPrimaryStreetLine2' );
	}

	public function getPrimaryStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strPrimaryStreetLine2' );
	}

	public function sqlPrimaryStreetLine2() {
		return ( true == isset( $this->m_strPrimaryStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine2 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine3( $strPrimaryStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strPrimaryStreetLine3, $strAddressKey = 'default', 'm_strPrimaryStreetLine3' );
	}

	public function getPrimaryStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strPrimaryStreetLine3' );
	}

	public function sqlPrimaryStreetLine3() {
		return ( true == isset( $this->m_strPrimaryStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine3 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryCity( $strPrimaryCity ) {
		$this->setPostalAddressField( 'locality', $strPrimaryCity, $strAddressKey = 'default', 'm_strPrimaryCity' );
	}

	public function getPrimaryCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strPrimaryCity' );
	}

	public function sqlPrimaryCity() {
		return ( true == isset( $this->m_strPrimaryCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryCity ) : '\'' . addslashes( $this->m_strPrimaryCity ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStateCode( $strPrimaryStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strPrimaryStateCode, $strAddressKey = 'default', 'm_strPrimaryStateCode' );
	}

	public function getPrimaryStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strPrimaryStateCode' );
	}

	public function sqlPrimaryStateCode() {
		return ( true == isset( $this->m_strPrimaryStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStateCode ) : '\'' . addslashes( $this->m_strPrimaryStateCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryPostalCode( $strPrimaryPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPrimaryPostalCode, $strAddressKey = 'default', 'm_strPrimaryPostalCode' );
	}

	public function getPrimaryPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPrimaryPostalCode' );
	}

	public function sqlPrimaryPostalCode() {
		return ( true == isset( $this->m_strPrimaryPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryPostalCode ) : '\'' . addslashes( $this->m_strPrimaryPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryCountryCode( $strPrimaryCountryCode ) {
		$this->setPostalAddressField( 'country', $strPrimaryCountryCode, $strAddressKey = 'default', 'm_strPrimaryCountryCode' );
	}

	public function getPrimaryCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strPrimaryCountryCode' );
	}

	public function sqlPrimaryCountryCode() {
		return ( true == isset( $this->m_strPrimaryCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryCountryCode ) : '\'' . addslashes( $this->m_strPrimaryCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryIsVerified( $boolPrimaryIsVerified ) {
		$this->set( 'm_boolPrimaryIsVerified', CStrings::strToBool( $boolPrimaryIsVerified ) );
	}

	public function getPrimaryIsVerified() {
		return $this->m_boolPrimaryIsVerified;
	}

	public function sqlPrimaryIsVerified() {
		return ( true == isset( $this->m_boolPrimaryIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolPrimaryIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPreviousStreetLine1( $strPreviousStreetLine1 ) {
		$this->set( 'm_strPreviousStreetLine1', CStrings::strTrimDef( $strPreviousStreetLine1, 100, NULL, true ) );
	}

	public function getPreviousStreetLine1() {
		return $this->m_strPreviousStreetLine1;
	}

	public function sqlPreviousStreetLine1() {
		return ( true == isset( $this->m_strPreviousStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousStreetLine1 ) : '\'' . addslashes( $this->m_strPreviousStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setPreviousStreetLine2( $strPreviousStreetLine2 ) {
		$this->set( 'm_strPreviousStreetLine2', CStrings::strTrimDef( $strPreviousStreetLine2, 100, NULL, true ) );
	}

	public function getPreviousStreetLine2() {
		return $this->m_strPreviousStreetLine2;
	}

	public function sqlPreviousStreetLine2() {
		return ( true == isset( $this->m_strPreviousStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousStreetLine2 ) : '\'' . addslashes( $this->m_strPreviousStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setPreviousStreetLine3( $strPreviousStreetLine3 ) {
		$this->set( 'm_strPreviousStreetLine3', CStrings::strTrimDef( $strPreviousStreetLine3, 100, NULL, true ) );
	}

	public function getPreviousStreetLine3() {
		return $this->m_strPreviousStreetLine3;
	}

	public function sqlPreviousStreetLine3() {
		return ( true == isset( $this->m_strPreviousStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousStreetLine3 ) : '\'' . addslashes( $this->m_strPreviousStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setPreviousCity( $strPreviousCity ) {
		$this->set( 'm_strPreviousCity', CStrings::strTrimDef( $strPreviousCity, 50, NULL, true ) );
	}

	public function getPreviousCity() {
		return $this->m_strPreviousCity;
	}

	public function sqlPreviousCity() {
		return ( true == isset( $this->m_strPreviousCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousCity ) : '\'' . addslashes( $this->m_strPreviousCity ) . '\'' ) : 'NULL';
	}

	public function setPreviousStateCode( $strPreviousStateCode ) {
		$this->set( 'm_strPreviousStateCode', CStrings::strTrimDef( $strPreviousStateCode, 250, NULL, true ) );
	}

	public function getPreviousStateCode() {
		return $this->m_strPreviousStateCode;
	}

	public function sqlPreviousStateCode() {
		return ( true == isset( $this->m_strPreviousStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousStateCode ) : '\'' . addslashes( $this->m_strPreviousStateCode ) . '\'' ) : 'NULL';
	}

	public function setPreviousPostalCode( $strPreviousPostalCode ) {
		$this->set( 'm_strPreviousPostalCode', CStrings::strTrimDef( $strPreviousPostalCode, 20, NULL, true ) );
	}

	public function getPreviousPostalCode() {
		return $this->m_strPreviousPostalCode;
	}

	public function sqlPreviousPostalCode() {
		return ( true == isset( $this->m_strPreviousPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousPostalCode ) : '\'' . addslashes( $this->m_strPreviousPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPreviousCountryCode( $strPreviousCountryCode ) {
		$this->set( 'm_strPreviousCountryCode', CStrings::strTrimDef( $strPreviousCountryCode, 2, NULL, true ) );
	}

	public function getPreviousCountryCode() {
		return $this->m_strPreviousCountryCode;
	}

	public function sqlPreviousCountryCode() {
		return ( true == isset( $this->m_strPreviousCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousCountryCode ) : '\'' . addslashes( $this->m_strPreviousCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPreviousProvince( $strPreviousProvince ) {
		$this->set( 'm_strPreviousProvince', CStrings::strTrimDef( $strPreviousProvince, 50, NULL, true ) );
	}

	public function getPreviousProvince() {
		return $this->m_strPreviousProvince;
	}

	public function sqlPreviousProvince() {
		return ( true == isset( $this->m_strPreviousProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousProvince ) : '\'' . addslashes( $this->m_strPreviousProvince ) . '\'' ) : 'NULL';
	}

	public function setPreviousIsVerified( $boolPreviousIsVerified ) {
		$this->set( 'm_boolPreviousIsVerified', CStrings::strToBool( $boolPreviousIsVerified ) );
	}

	public function getPreviousIsVerified() {
		return $this->m_boolPreviousIsVerified;
	}

	public function sqlPreviousIsVerified() {
		return ( true == isset( $this->m_boolPreviousIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolPreviousIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPermanentStreetLine1( $strPermanentStreetLine1 ) {
		$this->set( 'm_strPermanentStreetLine1', CStrings::strTrimDef( $strPermanentStreetLine1, 100, NULL, true ) );
	}

	public function getPermanentStreetLine1() {
		return $this->m_strPermanentStreetLine1;
	}

	public function sqlPermanentStreetLine1() {
		return ( true == isset( $this->m_strPermanentStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentStreetLine1 ) : '\'' . addslashes( $this->m_strPermanentStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setPermanentStreetLine2( $strPermanentStreetLine2 ) {
		$this->set( 'm_strPermanentStreetLine2', CStrings::strTrimDef( $strPermanentStreetLine2, 100, NULL, true ) );
	}

	public function getPermanentStreetLine2() {
		return $this->m_strPermanentStreetLine2;
	}

	public function sqlPermanentStreetLine2() {
		return ( true == isset( $this->m_strPermanentStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentStreetLine2 ) : '\'' . addslashes( $this->m_strPermanentStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setPermanentStreetLine3( $strPermanentStreetLine3 ) {
		$this->set( 'm_strPermanentStreetLine3', CStrings::strTrimDef( $strPermanentStreetLine3, 100, NULL, true ) );
	}

	public function getPermanentStreetLine3() {
		return $this->m_strPermanentStreetLine3;
	}

	public function sqlPermanentStreetLine3() {
		return ( true == isset( $this->m_strPermanentStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentStreetLine3 ) : '\'' . addslashes( $this->m_strPermanentStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setPermanentCity( $strPermanentCity ) {
		$this->set( 'm_strPermanentCity', CStrings::strTrimDef( $strPermanentCity, 50, NULL, true ) );
	}

	public function getPermanentCity() {
		return $this->m_strPermanentCity;
	}

	public function sqlPermanentCity() {
		return ( true == isset( $this->m_strPermanentCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentCity ) : '\'' . addslashes( $this->m_strPermanentCity ) . '\'' ) : 'NULL';
	}

	public function setPermanentStateCode( $strPermanentStateCode ) {
		$this->set( 'm_strPermanentStateCode', CStrings::strTrimDef( $strPermanentStateCode, 250, NULL, true ) );
	}

	public function getPermanentStateCode() {
		return $this->m_strPermanentStateCode;
	}

	public function sqlPermanentStateCode() {
		return ( true == isset( $this->m_strPermanentStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentStateCode ) : '\'' . addslashes( $this->m_strPermanentStateCode ) . '\'' ) : 'NULL';
	}

	public function setPermanentPostalCode( $strPermanentPostalCode ) {
		$this->set( 'm_strPermanentPostalCode', CStrings::strTrimDef( $strPermanentPostalCode, 20, NULL, true ) );
	}

	public function getPermanentPostalCode() {
		return $this->m_strPermanentPostalCode;
	}

	public function sqlPermanentPostalCode() {
		return ( true == isset( $this->m_strPermanentPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentPostalCode ) : '\'' . addslashes( $this->m_strPermanentPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPermanentCountryCode( $strPermanentCountryCode ) {
		$this->set( 'm_strPermanentCountryCode', CStrings::strTrimDef( $strPermanentCountryCode, 2, NULL, true ) );
	}

	public function getPermanentCountryCode() {
		return $this->m_strPermanentCountryCode;
	}

	public function sqlPermanentCountryCode() {
		return ( true == isset( $this->m_strPermanentCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentCountryCode ) : '\'' . addslashes( $this->m_strPermanentCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPermanentProvince( $strPermanentProvince ) {
		$this->set( 'm_strPermanentProvince', CStrings::strTrimDef( $strPermanentProvince, 50, NULL, true ) );
	}

	public function getPermanentProvince() {
		return $this->m_strPermanentProvince;
	}

	public function sqlPermanentProvince() {
		return ( true == isset( $this->m_strPermanentProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentProvince ) : '\'' . addslashes( $this->m_strPermanentProvince ) . '\'' ) : 'NULL';
	}

	public function setPermanentIsVerified( $boolPermanentIsVerified ) {
		$this->set( 'm_boolPermanentIsVerified', CStrings::strToBool( $boolPermanentIsVerified ) );
	}

	public function getPermanentIsVerified() {
		return $this->m_boolPermanentIsVerified;
	}

	public function sqlPermanentIsVerified() {
		return ( true == isset( $this->m_boolPermanentIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolPermanentIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPrimaryVehicleMake( $strPrimaryVehicleMake ) {
		$this->set( 'm_strPrimaryVehicleMake', CStrings::strTrimDef( $strPrimaryVehicleMake, 50, NULL, true ) );
	}

	public function getPrimaryVehicleMake() {
		return $this->m_strPrimaryVehicleMake;
	}

	public function sqlPrimaryVehicleMake() {
		return ( true == isset( $this->m_strPrimaryVehicleMake ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleMake ) : '\'' . addslashes( $this->m_strPrimaryVehicleMake ) . '\'' ) : 'NULL';
	}

	public function setPrimaryVehicleModel( $strPrimaryVehicleModel ) {
		$this->set( 'm_strPrimaryVehicleModel', CStrings::strTrimDef( $strPrimaryVehicleModel, 50, NULL, true ) );
	}

	public function getPrimaryVehicleModel() {
		return $this->m_strPrimaryVehicleModel;
	}

	public function sqlPrimaryVehicleModel() {
		return ( true == isset( $this->m_strPrimaryVehicleModel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleModel ) : '\'' . addslashes( $this->m_strPrimaryVehicleModel ) . '\'' ) : 'NULL';
	}

	public function setPrimaryVehicleYear( $intPrimaryVehicleYear ) {
		$this->set( 'm_intPrimaryVehicleYear', CStrings::strToIntDef( $intPrimaryVehicleYear, NULL, false ) );
	}

	public function getPrimaryVehicleYear() {
		return $this->m_intPrimaryVehicleYear;
	}

	public function sqlPrimaryVehicleYear() {
		return ( true == isset( $this->m_intPrimaryVehicleYear ) ) ? ( string ) $this->m_intPrimaryVehicleYear : 'NULL';
	}

	public function setPrimaryVehicleColor( $strPrimaryVehicleColor ) {
		$this->set( 'm_strPrimaryVehicleColor', CStrings::strTrimDef( $strPrimaryVehicleColor, 20, NULL, true ) );
	}

	public function getPrimaryVehicleColor() {
		return $this->m_strPrimaryVehicleColor;
	}

	public function sqlPrimaryVehicleColor() {
		return ( true == isset( $this->m_strPrimaryVehicleColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleColor ) : '\'' . addslashes( $this->m_strPrimaryVehicleColor ) . '\'' ) : 'NULL';
	}

	public function setPrimaryVehicleLicensePlateNumber( $strPrimaryVehicleLicensePlateNumber ) {
		$this->set( 'm_strPrimaryVehicleLicensePlateNumber', CStrings::strTrimDef( $strPrimaryVehicleLicensePlateNumber, 20, NULL, true ) );
	}

	public function getPrimaryVehicleLicensePlateNumber() {
		return $this->m_strPrimaryVehicleLicensePlateNumber;
	}

	public function sqlPrimaryVehicleLicensePlateNumber() {
		return ( true == isset( $this->m_strPrimaryVehicleLicensePlateNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleLicensePlateNumber ) : '\'' . addslashes( $this->m_strPrimaryVehicleLicensePlateNumber ) . '\'' ) : 'NULL';
	}

	public function setPrimaryVehicleStateCode( $strPrimaryVehicleStateCode ) {
		$this->set( 'm_strPrimaryVehicleStateCode', CStrings::strTrimDef( $strPrimaryVehicleStateCode, 250, NULL, true ) );
	}

	public function getPrimaryVehicleStateCode() {
		return $this->m_strPrimaryVehicleStateCode;
	}

	public function sqlPrimaryVehicleStateCode() {
		return ( true == isset( $this->m_strPrimaryVehicleStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleStateCode ) : '\'' . addslashes( $this->m_strPrimaryVehicleStateCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryVehicleProvince( $strPrimaryVehicleProvince ) {
		$this->set( 'm_strPrimaryVehicleProvince', CStrings::strTrimDef( $strPrimaryVehicleProvince, 50, NULL, true ) );
	}

	public function getPrimaryVehicleProvince() {
		return $this->m_strPrimaryVehicleProvince;
	}

	public function sqlPrimaryVehicleProvince() {
		return ( true == isset( $this->m_strPrimaryVehicleProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryVehicleProvince ) : '\'' . addslashes( $this->m_strPrimaryVehicleProvince ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleMake( $strSecondaryVehicleMake ) {
		$this->set( 'm_strSecondaryVehicleMake', CStrings::strTrimDef( $strSecondaryVehicleMake, 50, NULL, true ) );
	}

	public function getSecondaryVehicleMake() {
		return $this->m_strSecondaryVehicleMake;
	}

	public function sqlSecondaryVehicleMake() {
		return ( true == isset( $this->m_strSecondaryVehicleMake ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleMake ) : '\'' . addslashes( $this->m_strSecondaryVehicleMake ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleModel( $strSecondaryVehicleModel ) {
		$this->set( 'm_strSecondaryVehicleModel', CStrings::strTrimDef( $strSecondaryVehicleModel, 50, NULL, true ) );
	}

	public function getSecondaryVehicleModel() {
		return $this->m_strSecondaryVehicleModel;
	}

	public function sqlSecondaryVehicleModel() {
		return ( true == isset( $this->m_strSecondaryVehicleModel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleModel ) : '\'' . addslashes( $this->m_strSecondaryVehicleModel ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleYear( $intSecondaryVehicleYear ) {
		$this->set( 'm_intSecondaryVehicleYear', CStrings::strToIntDef( $intSecondaryVehicleYear, NULL, false ) );
	}

	public function getSecondaryVehicleYear() {
		return $this->m_intSecondaryVehicleYear;
	}

	public function sqlSecondaryVehicleYear() {
		return ( true == isset( $this->m_intSecondaryVehicleYear ) ) ? ( string ) $this->m_intSecondaryVehicleYear : 'NULL';
	}

	public function setSecondaryVehicleColor( $strSecondaryVehicleColor ) {
		$this->set( 'm_strSecondaryVehicleColor', CStrings::strTrimDef( $strSecondaryVehicleColor, 20, NULL, true ) );
	}

	public function getSecondaryVehicleColor() {
		return $this->m_strSecondaryVehicleColor;
	}

	public function sqlSecondaryVehicleColor() {
		return ( true == isset( $this->m_strSecondaryVehicleColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleColor ) : '\'' . addslashes( $this->m_strSecondaryVehicleColor ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleLicensePlateNumber( $strSecondaryVehicleLicensePlateNumber ) {
		$this->set( 'm_strSecondaryVehicleLicensePlateNumber', CStrings::strTrimDef( $strSecondaryVehicleLicensePlateNumber, 20, NULL, true ) );
	}

	public function getSecondaryVehicleLicensePlateNumber() {
		return $this->m_strSecondaryVehicleLicensePlateNumber;
	}

	public function sqlSecondaryVehicleLicensePlateNumber() {
		return ( true == isset( $this->m_strSecondaryVehicleLicensePlateNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleLicensePlateNumber ) : '\'' . addslashes( $this->m_strSecondaryVehicleLicensePlateNumber ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleStateCode( $strSecondaryVehicleStateCode ) {
		$this->set( 'm_strSecondaryVehicleStateCode', CStrings::strTrimDef( $strSecondaryVehicleStateCode, 250, NULL, true ) );
	}

	public function getSecondaryVehicleStateCode() {
		return $this->m_strSecondaryVehicleStateCode;
	}

	public function sqlSecondaryVehicleStateCode() {
		return ( true == isset( $this->m_strSecondaryVehicleStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleStateCode ) : '\'' . addslashes( $this->m_strSecondaryVehicleStateCode ) . '\'' ) : 'NULL';
	}

	public function setSecondaryVehicleProvince( $strSecondaryVehicleProvince ) {
		$this->set( 'm_strSecondaryVehicleProvince', CStrings::strTrimDef( $strSecondaryVehicleProvince, 50, NULL, true ) );
	}

	public function getSecondaryVehicleProvince() {
		return $this->m_strSecondaryVehicleProvince;
	}

	public function sqlSecondaryVehicleProvince() {
		return ( true == isset( $this->m_strSecondaryVehicleProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryVehicleProvince ) : '\'' . addslashes( $this->m_strSecondaryVehicleProvince ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMobileNumber ) : '\'' . addslashes( $this->m_strMobileNumber ) . '\'' ) : 'NULL';
	}

	public function setWorkNumber( $strWorkNumber ) {
		$this->set( 'm_strWorkNumber', CStrings::strTrimDef( $strWorkNumber, 30, NULL, true ) );
	}

	public function getWorkNumber() {
		return $this->m_strWorkNumber;
	}

	public function sqlWorkNumber() {
		return ( true == isset( $this->m_strWorkNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorkNumber ) : '\'' . addslashes( $this->m_strWorkNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->set( 'm_strTaxNumberMasked', CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true ) );
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function sqlTaxNumberMasked() {
		return ( true == isset( $this->m_strTaxNumberMasked ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberMasked ) : '\'' . addslashes( $this->m_strTaxNumberMasked ) . '\'' ) : 'NULL';
	}

	public function setReturnedPaymentsCount( $intReturnedPaymentsCount ) {
		$this->set( 'm_intReturnedPaymentsCount', CStrings::strToIntDef( $intReturnedPaymentsCount, NULL, false ) );
	}

	public function getReturnedPaymentsCount() {
		return $this->m_intReturnedPaymentsCount;
	}

	public function sqlReturnedPaymentsCount() {
		return ( true == isset( $this->m_intReturnedPaymentsCount ) ) ? ( string ) $this->m_intReturnedPaymentsCount : '0';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setDlNumberEncrypted( $strDlNumberEncrypted ) {
		$this->set( 'm_strDlNumberEncrypted', CStrings::strTrimDef( $strDlNumberEncrypted, 240, NULL, true ) );
	}

	public function getDlNumberEncrypted() {
		return $this->m_strDlNumberEncrypted;
	}

	public function sqlDlNumberEncrypted() {
		return ( true == isset( $this->m_strDlNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlNumberEncrypted ) : '\'' . addslashes( $this->m_strDlNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDlStateCode( $strDlStateCode ) {
		$this->set( 'm_strDlStateCode', CStrings::strTrimDef( $strDlStateCode, 2, NULL, true ) );
	}

	public function getDlStateCode() {
		return $this->m_strDlStateCode;
	}

	public function sqlDlStateCode() {
		return ( true == isset( $this->m_strDlStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlStateCode ) : '\'' . addslashes( $this->m_strDlStateCode ) . '\'' ) : 'NULL';
	}

	public function setDlProvince( $strDlProvince ) {
		$this->set( 'm_strDlProvince', CStrings::strTrimDef( $strDlProvince, 50, NULL, true ) );
	}

	public function getDlProvince() {
		return $this->m_strDlProvince;
	}

	public function sqlDlProvince() {
		return ( true == isset( $this->m_strDlProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlProvince ) : '\'' . addslashes( $this->m_strDlProvince ) . '\'' ) : 'NULL';
	}

	public function setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId ) {
		$this->set( 'm_intCompanyIdentificationTypeId', CStrings::strToIntDef( $intCompanyIdentificationTypeId, NULL, false ) );
	}

	public function getCompanyIdentificationTypeId() {
		return $this->m_intCompanyIdentificationTypeId;
	}

	public function sqlCompanyIdentificationTypeId() {
		return ( true == isset( $this->m_intCompanyIdentificationTypeId ) ) ? ( string ) $this->m_intCompanyIdentificationTypeId : 'NULL';
	}

	public function setIdentificationValue( $strIdentificationValue ) {
		$this->set( 'm_strIdentificationValue', CStrings::strTrimDef( $strIdentificationValue, 240, NULL, true ) );
	}

	public function getIdentificationValue() {
		return $this->m_strIdentificationValue;
	}

	public function sqlIdentificationValue() {
		return ( true == isset( $this->m_strIdentificationValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationValue ) : '\'' . addslashes( $this->m_strIdentificationValue ) . '\'' ) : 'NULL';
	}

	public function setIdentificationExpiration( $strIdentificationExpiration ) {
		$this->set( 'm_strIdentificationExpiration', CStrings::strTrimDef( $strIdentificationExpiration, -1, NULL, true ) );
	}

	public function getIdentificationExpiration() {
		return $this->m_strIdentificationExpiration;
	}

	public function sqlIdentificationExpiration() {
		return ( true == isset( $this->m_strIdentificationExpiration ) ) ? '\'' . $this->m_strIdentificationExpiration . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPasswordQuestion( $strPasswordQuestion ) {
		$this->set( 'm_strPasswordQuestion', CStrings::strTrimDef( $strPasswordQuestion, 2000, NULL, true ) );
	}

	public function getPasswordQuestion() {
		return $this->m_strPasswordQuestion;
	}

	public function sqlPasswordQuestion() {
		return ( true == isset( $this->m_strPasswordQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordQuestion ) : '\'' . addslashes( $this->m_strPasswordQuestion ) . '\'' ) : 'NULL';
	}

	public function setPasswordAnswerEncrypted( $strPasswordAnswerEncrypted ) {
		$this->set( 'm_strPasswordAnswerEncrypted', CStrings::strTrimDef( $strPasswordAnswerEncrypted, 240, NULL, true ) );
	}

	public function getPasswordAnswerEncrypted() {
		return $this->m_strPasswordAnswerEncrypted;
	}

	public function sqlPasswordAnswerEncrypted() {
		return ( true == isset( $this->m_strPasswordAnswerEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordAnswerEncrypted ) : '\'' . addslashes( $this->m_strPasswordAnswerEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPasswordExpiration( $strPasswordExpiration ) {
		$this->set( 'm_strPasswordExpiration', CStrings::strTrimDef( $strPasswordExpiration, -1, NULL, true ) );
	}

	public function getPasswordExpiration() {
		return $this->m_strPasswordExpiration;
	}

	public function sqlPasswordExpiration() {
		return ( true == isset( $this->m_strPasswordExpiration ) ) ? '\'' . $this->m_strPasswordExpiration . '\'' : 'NULL';
	}

	public function setFacebookUser( $strFacebookUser ) {
		$this->set( 'm_strFacebookUser', CStrings::strTrimDef( $strFacebookUser, 240, NULL, true ) );
	}

	public function getFacebookUser() {
		return $this->m_strFacebookUser;
	}

	public function sqlFacebookUser() {
		return ( true == isset( $this->m_strFacebookUser ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFacebookUser ) : '\'' . addslashes( $this->m_strFacebookUser ) . '\'' ) : 'NULL';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setDontAcceptPayments( $intDontAcceptPayments ) {
		$this->set( 'm_intDontAcceptPayments', CStrings::strToIntDef( $intDontAcceptPayments, NULL, false ) );
	}

	public function getDontAcceptPayments() {
		return $this->m_intDontAcceptPayments;
	}

	public function sqlDontAcceptPayments() {
		return ( true == isset( $this->m_intDontAcceptPayments ) ) ? ( string ) $this->m_intDontAcceptPayments : 'NULL';
	}

	public function setBlockPosting( $intBlockPosting ) {
		$this->set( 'm_intBlockPosting', CStrings::strToIntDef( $intBlockPosting, NULL, false ) );
	}

	public function getBlockPosting() {
		return $this->m_intBlockPosting;
	}

	public function sqlBlockPosting() {
		return ( true == isset( $this->m_intBlockPosting ) ) ? ( string ) $this->m_intBlockPosting : 'NULL';
	}

	public function setBlockNetworking( $intBlockNetworking ) {
		$this->set( 'm_intBlockNetworking', CStrings::strToIntDef( $intBlockNetworking, NULL, false ) );
	}

	public function getBlockNetworking() {
		return $this->m_intBlockNetworking;
	}

	public function sqlBlockNetworking() {
		return ( true == isset( $this->m_intBlockNetworking ) ) ? ( string ) $this->m_intBlockNetworking : 'NULL';
	}

	public function setBlockOnlinePayments( $intBlockOnlinePayments ) {
		$this->set( 'm_intBlockOnlinePayments', CStrings::strToIntDef( $intBlockOnlinePayments, NULL, false ) );
	}

	public function getBlockOnlinePayments() {
		return $this->m_intBlockOnlinePayments;
	}

	public function sqlBlockOnlinePayments() {
		return ( true == isset( $this->m_intBlockOnlinePayments ) ) ? ( string ) $this->m_intBlockOnlinePayments : 'NULL';
	}

	public function setIsNetworkingEnrolled( $intIsNetworkingEnrolled ) {
		$this->set( 'm_intIsNetworkingEnrolled', CStrings::strToIntDef( $intIsNetworkingEnrolled, NULL, false ) );
	}

	public function getIsNetworkingEnrolled() {
		return $this->m_intIsNetworkingEnrolled;
	}

	public function sqlIsNetworkingEnrolled() {
		return ( true == isset( $this->m_intIsNetworkingEnrolled ) ) ? ( string ) $this->m_intIsNetworkingEnrolled : 'NULL';
	}

	public function setApprovalIpAddress( $strApprovalIpAddress ) {
		$this->set( 'm_strApprovalIpAddress', CStrings::strTrimDef( $strApprovalIpAddress, 23, NULL, true ) );
	}

	public function getApprovalIpAddress() {
		return $this->m_strApprovalIpAddress;
	}

	public function sqlApprovalIpAddress() {
		return ( true == isset( $this->m_strApprovalIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalIpAddress ) : '\'' . addslashes( $this->m_strApprovalIpAddress ) . '\'' ) : 'NULL';
	}

	public function setTermsApprovalIp( $strTermsApprovalIp ) {
		$this->set( 'm_strTermsApprovalIp', CStrings::strTrimDef( $strTermsApprovalIp, 23, NULL, true ) );
	}

	public function getTermsApprovalIp() {
		return $this->m_strTermsApprovalIp;
	}

	public function sqlTermsApprovalIp() {
		return ( true == isset( $this->m_strTermsApprovalIp ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTermsApprovalIp ) : '\'' . addslashes( $this->m_strTermsApprovalIp ) . '\'' ) : 'NULL';
	}

	public function setTermsApprovedOn( $strTermsApprovedOn ) {
		$this->set( 'm_strTermsApprovedOn', CStrings::strTrimDef( $strTermsApprovedOn, -1, NULL, true ) );
	}

	public function getTermsApprovedOn() {
		return $this->m_strTermsApprovedOn;
	}

	public function sqlTermsApprovedOn() {
		return ( true == isset( $this->m_strTermsApprovedOn ) ) ? '\'' . $this->m_strTermsApprovedOn . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setProfilePublishedBy( $intProfilePublishedBy ) {
		$this->set( 'm_intProfilePublishedBy', CStrings::strToIntDef( $intProfilePublishedBy, NULL, false ) );
	}

	public function getProfilePublishedBy() {
		return $this->m_intProfilePublishedBy;
	}

	public function sqlProfilePublishedBy() {
		return ( true == isset( $this->m_intProfilePublishedBy ) ) ? ( string ) $this->m_intProfilePublishedBy : 'NULL';
	}

	public function setProfilePublishedOn( $strProfilePublishedOn ) {
		$this->set( 'm_strProfilePublishedOn', CStrings::strTrimDef( $strProfilePublishedOn, -1, NULL, true ) );
	}

	public function getProfilePublishedOn() {
		return $this->m_strProfilePublishedOn;
	}

	public function sqlProfilePublishedOn() {
		return ( true == isset( $this->m_strProfilePublishedOn ) ) ? '\'' . $this->m_strProfilePublishedOn . '\'' : 'NULL';
	}

	public function setMobileNumberAuthorizedOn( $strMobileNumberAuthorizedOn ) {
		$this->set( 'm_strMobileNumberAuthorizedOn', CStrings::strTrimDef( $strMobileNumberAuthorizedOn, -1, NULL, true ) );
	}

	public function getMobileNumberAuthorizedOn() {
		return $this->m_strMobileNumberAuthorizedOn;
	}

	public function sqlMobileNumberAuthorizedOn() {
		return ( true == isset( $this->m_strMobileNumberAuthorizedOn ) ) ? '\'' . $this->m_strMobileNumberAuthorizedOn . '\'' : 'NULL';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsOrganization( $boolIsOrganization ) {
		$this->set( 'm_boolIsOrganization', CStrings::strToBool( $boolIsOrganization ) );
	}

	public function getIsOrganization() {
		return $this->m_boolIsOrganization;
	}

	public function sqlIsOrganization() {
		return ( true == isset( $this->m_boolIsOrganization ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOrganization ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAltNameFirst( $strAltNameFirst ) {
		$this->set( 'm_strAltNameFirst', CStrings::strTrimDef( $strAltNameFirst, 1000, NULL, true ) );
	}

	public function getAltNameFirst() {
		return $this->m_strAltNameFirst;
	}

	public function sqlAltNameFirst() {
		return ( true == isset( $this->m_strAltNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameFirst ) : '\'' . addslashes( $this->m_strAltNameFirst ) . '\'' ) : 'NULL';
	}

	public function setAltNameMiddle( $strAltNameMiddle ) {
		$this->set( 'm_strAltNameMiddle', CStrings::strTrimDef( $strAltNameMiddle, 1000, NULL, true ) );
	}

	public function getAltNameMiddle() {
		return $this->m_strAltNameMiddle;
	}

	public function sqlAltNameMiddle() {
		return ( true == isset( $this->m_strAltNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameMiddle ) : '\'' . addslashes( $this->m_strAltNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setAltNameLast( $strAltNameLast ) {
		$this->set( 'm_strAltNameLast', CStrings::strTrimDef( $strAltNameLast, 1000, NULL, true ) );
	}

	public function getAltNameLast() {
		return $this->m_strAltNameLast;
	}

	public function sqlAltNameLast() {
		return ( true == isset( $this->m_strAltNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameLast ) : '\'' . addslashes( $this->m_strAltNameLast ) . '\'' ) : 'NULL';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 1000, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->set( 'm_strPreferredLocaleCode', CStrings::strTrimDef( $strPreferredLocaleCode, 100, NULL, true ) );
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function sqlPreferredLocaleCode() {
		return ( true == isset( $this->m_strPreferredLocaleCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredLocaleCode ) : '\'' . addslashes( $this->m_strPreferredLocaleCode ) . '\'' ) : 'NULL';
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->set( 'm_strNameLastMatronymic', CStrings::strTrimDef( $strNameLastMatronymic, 1000, NULL, true ) );
	}

	public function getNameLastMatronymic() {
		return $this->m_strNameLastMatronymic;
	}

	public function sqlNameLastMatronymic() {
		return ( true == isset( $this->m_strNameLastMatronymic ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLastMatronymic ) : '\'' . addslashes( $this->m_strNameLastMatronymic ) . '\'' ) : 'NULL';
	}

	public function setHasNameLastMatronymic( $intHasNameLastMatronymic ) {
		$this->set( 'm_intHasNameLastMatronymic', CStrings::strToIntDef( $intHasNameLastMatronymic, NULL, false ) );
	}

	public function getHasNameLastMatronymic() {
		return $this->m_intHasNameLastMatronymic;
	}

	public function sqlHasNameLastMatronymic() {
		return ( true == isset( $this->m_intHasNameLastMatronymic ) ) ? ( string ) $this->m_intHasNameLastMatronymic : 'NULL';
	}

	public function setCitizenshipCountryCode( $strCitizenshipCountryCode ) {
		$this->set( 'm_strCitizenshipCountryCode', CStrings::strTrimDef( $strCitizenshipCountryCode, 2, NULL, true ) );
	}

	public function getCitizenshipCountryCode() {
		return $this->m_strCitizenshipCountryCode;
	}

	public function sqlCitizenshipCountryCode() {
		return ( true == isset( $this->m_strCitizenshipCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCitizenshipCountryCode ) : '\'' . addslashes( $this->m_strCitizenshipCountryCode ) . '\'' ) : 'NULL';
	}

	public function setTaxIdTypeId( $intTaxIdTypeId ) {
		$this->set( 'm_intTaxIdTypeId', CStrings::strToIntDef( $intTaxIdTypeId, NULL, false ) );
	}

	public function getTaxIdTypeId() {
		return $this->m_intTaxIdTypeId;
	}

	public function sqlTaxIdTypeId() {
		return ( true == isset( $this->m_intTaxIdTypeId ) ) ? ( string ) $this->m_intTaxIdTypeId : 'NULL';
	}

	public function setTaxNumberBlindIndex( $strTaxNumberBlindIndex ) {
		$this->set( 'm_strTaxNumberBlindIndex', CStrings::strTrimDef( $strTaxNumberBlindIndex, 250, NULL, true ) );
	}

	public function getTaxNumberBlindIndex() {
		return $this->m_strTaxNumberBlindIndex;
	}

	public function sqlTaxNumberBlindIndex() {
		return ( true == isset( $this->m_strTaxNumberBlindIndex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberBlindIndex ) : '\'' . addslashes( $this->m_strTaxNumberBlindIndex ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert customer record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlMaritalStatusTypeId() . ', ' .
						$this->sqlLeasingAgentId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlParentNameFirst() . ', ' .
						$this->sqlParentNameLast() . ', ' .
						$this->sqlParentEmailAddress() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameFull() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlNameSpouse() . ', ' .
						$this->sqlPrimaryStreetLine1() . ', ' .
						$this->sqlPrimaryStreetLine2() . ', ' .
						$this->sqlPrimaryStreetLine3() . ', ' .
						$this->sqlPrimaryCity() . ', ' .
						$this->sqlPrimaryStateCode() . ', ' .
						$this->sqlPrimaryPostalCode() . ', ' .
						$this->sqlPrimaryCountryCode() . ', ' .
						$this->sqlPrimaryIsVerified() . ', ' .
						$this->sqlPreviousStreetLine1() . ', ' .
						$this->sqlPreviousStreetLine2() . ', ' .
						$this->sqlPreviousStreetLine3() . ', ' .
						$this->sqlPreviousCity() . ', ' .
						$this->sqlPreviousStateCode() . ', ' .
						$this->sqlPreviousPostalCode() . ', ' .
						$this->sqlPreviousCountryCode() . ', ' .
						$this->sqlPreviousProvince() . ', ' .
						$this->sqlPreviousIsVerified() . ', ' .
						$this->sqlPermanentStreetLine1() . ', ' .
						$this->sqlPermanentStreetLine2() . ', ' .
						$this->sqlPermanentStreetLine3() . ', ' .
						$this->sqlPermanentCity() . ', ' .
						$this->sqlPermanentStateCode() . ', ' .
						$this->sqlPermanentPostalCode() . ', ' .
						$this->sqlPermanentCountryCode() . ', ' .
						$this->sqlPermanentProvince() . ', ' .
						$this->sqlPermanentIsVerified() . ', ' .
						$this->sqlPrimaryVehicleMake() . ', ' .
						$this->sqlPrimaryVehicleModel() . ', ' .
						$this->sqlPrimaryVehicleYear() . ', ' .
						$this->sqlPrimaryVehicleColor() . ', ' .
						$this->sqlPrimaryVehicleLicensePlateNumber() . ', ' .
						$this->sqlPrimaryVehicleStateCode() . ', ' .
						$this->sqlPrimaryVehicleProvince() . ', ' .
						$this->sqlSecondaryVehicleMake() . ', ' .
						$this->sqlSecondaryVehicleModel() . ', ' .
						$this->sqlSecondaryVehicleYear() . ', ' .
						$this->sqlSecondaryVehicleColor() . ', ' .
						$this->sqlSecondaryVehicleLicensePlateNumber() . ', ' .
						$this->sqlSecondaryVehicleStateCode() . ', ' .
						$this->sqlSecondaryVehicleProvince() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlReturnedPaymentsCount() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlDlNumberEncrypted() . ', ' .
						$this->sqlDlStateCode() . ', ' .
						$this->sqlDlProvince() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlPasswordQuestion() . ', ' .
						$this->sqlPasswordAnswerEncrypted() . ', ' .
						$this->sqlPasswordExpiration() . ', ' .
						$this->sqlFacebookUser() . ', ' .
						$this->sqlDontAllowLogin() . ', ' .
						$this->sqlDontAcceptPayments() . ', ' .
						$this->sqlBlockPosting() . ', ' .
						$this->sqlBlockNetworking() . ', ' .
						$this->sqlBlockOnlinePayments() . ', ' .
						$this->sqlIsNetworkingEnrolled() . ', ' .
						$this->sqlApprovalIpAddress() . ', ' .
						$this->sqlTermsApprovalIp() . ', ' .
						$this->sqlTermsApprovedOn() . ', ' .
						$this->sqlLoginAttemptCount() . ', ' .
						$this->sqlLastLoginAttemptOn() . ', ' .
						$this->sqlProfilePublishedBy() . ', ' .
						$this->sqlProfilePublishedOn() . ', ' .
						$this->sqlMobileNumberAuthorizedOn() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlIsOrganization() . ', ' .
						$this->sqlAltNameFirst() . ', ' .
						$this->sqlAltNameMiddle() . ', ' .
						$this->sqlAltNameLast() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlPreferredLocaleCode() . ', ' .
						$this->sqlNameLastMatronymic() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
						$this->sqlCitizenshipCountryCode() . ', ' .
						$this->sqlTaxIdTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTaxNumberBlindIndex() . ', ' .
						( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert customer record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert customer record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlMaritalStatusTypeId() . ', ' .
						$this->sqlLeasingAgentId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlParentNameFirst() . ', ' .
						$this->sqlParentNameLast() . ', ' .
						$this->sqlParentEmailAddress() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameFull() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlNameSpouse() . ', ' .
						$this->sqlPrimaryStreetLine1() . ', ' .
						$this->sqlPrimaryStreetLine2() . ', ' .
						$this->sqlPrimaryStreetLine3() . ', ' .
						$this->sqlPrimaryCity() . ', ' .
						$this->sqlPrimaryStateCode() . ', ' .
						$this->sqlPrimaryPostalCode() . ', ' .
						$this->sqlPrimaryCountryCode() . ', ' .
						$this->sqlPrimaryIsVerified() . ', ' .
						$this->sqlPreviousStreetLine1() . ', ' .
						$this->sqlPreviousStreetLine2() . ', ' .
						$this->sqlPreviousStreetLine3() . ', ' .
						$this->sqlPreviousCity() . ', ' .
						$this->sqlPreviousStateCode() . ', ' .
						$this->sqlPreviousPostalCode() . ', ' .
						$this->sqlPreviousCountryCode() . ', ' .
						$this->sqlPreviousProvince() . ', ' .
						$this->sqlPreviousIsVerified() . ', ' .
						$this->sqlPermanentStreetLine1() . ', ' .
						$this->sqlPermanentStreetLine2() . ', ' .
						$this->sqlPermanentStreetLine3() . ', ' .
						$this->sqlPermanentCity() . ', ' .
						$this->sqlPermanentStateCode() . ', ' .
						$this->sqlPermanentPostalCode() . ', ' .
						$this->sqlPermanentCountryCode() . ', ' .
						$this->sqlPermanentProvince() . ', ' .
						$this->sqlPermanentIsVerified() . ', ' .
						$this->sqlPrimaryVehicleMake() . ', ' .
						$this->sqlPrimaryVehicleModel() . ', ' .
						$this->sqlPrimaryVehicleYear() . ', ' .
						$this->sqlPrimaryVehicleColor() . ', ' .
						$this->sqlPrimaryVehicleLicensePlateNumber() . ', ' .
						$this->sqlPrimaryVehicleStateCode() . ', ' .
						$this->sqlPrimaryVehicleProvince() . ', ' .
						$this->sqlSecondaryVehicleMake() . ', ' .
						$this->sqlSecondaryVehicleModel() . ', ' .
						$this->sqlSecondaryVehicleYear() . ', ' .
						$this->sqlSecondaryVehicleColor() . ', ' .
						$this->sqlSecondaryVehicleLicensePlateNumber() . ', ' .
						$this->sqlSecondaryVehicleStateCode() . ', ' .
						$this->sqlSecondaryVehicleProvince() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlReturnedPaymentsCount() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlDlNumberEncrypted() . ', ' .
						$this->sqlDlStateCode() . ', ' .
						$this->sqlDlProvince() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlPasswordQuestion() . ', ' .
						$this->sqlPasswordAnswerEncrypted() . ', ' .
						$this->sqlPasswordExpiration() . ', ' .
						$this->sqlFacebookUser() . ', ' .
						$this->sqlDontAllowLogin() . ', ' .
						$this->sqlDontAcceptPayments() . ', ' .
						$this->sqlBlockPosting() . ', ' .
						$this->sqlBlockNetworking() . ', ' .
						$this->sqlBlockOnlinePayments() . ', ' .
						$this->sqlIsNetworkingEnrolled() . ', ' .
						$this->sqlApprovalIpAddress() . ', ' .
						$this->sqlTermsApprovalIp() . ', ' .
						$this->sqlTermsApprovedOn() . ', ' .
						$this->sqlLoginAttemptCount() . ', ' .
						$this->sqlLastLoginAttemptOn() . ', ' .
						$this->sqlProfilePublishedBy() . ', ' .
						$this->sqlProfilePublishedOn() . ', ' .
						$this->sqlMobileNumberAuthorizedOn() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlIsOrganization() . ', ' .
						$this->sqlAltNameFirst() . ', ' .
						$this->sqlAltNameMiddle() . ', ' .
						$this->sqlAltNameLast() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlPreferredLocaleCode() . ', ' .
						$this->sqlNameLastMatronymic() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
						$this->sqlCitizenshipCountryCode() . ', ' .
						$this->sqlTaxIdTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTaxNumberBlindIndex() . ', ' .
						 ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			 return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update customer record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update customer record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$objDataset = $objDatabase->createDataset();

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_delete( ' .
		$this->sqlId() . ', ' .
		$this->sqlCid() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete customer record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete customer record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'payment_allowance_type_id' => $this->getPaymentAllowanceTypeId(),
			'marital_status_type_id' => $this->getMaritalStatusTypeId(),
			'leasing_agent_id' => $this->getLeasingAgentId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'student_id_number' => $this->getStudentIdNumber(),
			'parent_name_first' => $this->getParentNameFirst(),
			'parent_name_last' => $this->getParentNameLast(),
			'parent_email_address' => $this->getParentEmailAddress(),
			'primary_phone_number_type_id' => $this->getPrimaryPhoneNumberTypeId(),
			'secondary_phone_number_type_id' => $this->getSecondaryPhoneNumberTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'secondary_number' => $this->getSecondaryNumber(),
			'company_name' => $this->getCompanyName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_full' => $this->getNameFull(),
			'name_suffix' => $this->getNameSuffix(),
			'name_maiden' => $this->getNameMaiden(),
			'name_spouse' => $this->getNameSpouse(),
			'primary_street_line1' => $this->getPrimaryStreetLine1(),
			'primary_street_line2' => $this->getPrimaryStreetLine2(),
			'primary_street_line3' => $this->getPrimaryStreetLine3(),
			'primary_city' => $this->getPrimaryCity(),
			'primary_state_code' => $this->getPrimaryStateCode(),
			'primary_postal_code' => $this->getPrimaryPostalCode(),
			'primary_country_code' => $this->getPrimaryCountryCode(),
			'primary_is_verified' => $this->getPrimaryIsVerified(),
			'previous_street_line1' => $this->getPreviousStreetLine1(),
			'previous_street_line2' => $this->getPreviousStreetLine2(),
			'previous_street_line3' => $this->getPreviousStreetLine3(),
			'previous_city' => $this->getPreviousCity(),
			'previous_state_code' => $this->getPreviousStateCode(),
			'previous_postal_code' => $this->getPreviousPostalCode(),
			'previous_country_code' => $this->getPreviousCountryCode(),
			'previous_province' => $this->getPreviousProvince(),
			'previous_is_verified' => $this->getPreviousIsVerified(),
			'permanent_street_line1' => $this->getPermanentStreetLine1(),
			'permanent_street_line2' => $this->getPermanentStreetLine2(),
			'permanent_street_line3' => $this->getPermanentStreetLine3(),
			'permanent_city' => $this->getPermanentCity(),
			'permanent_state_code' => $this->getPermanentStateCode(),
			'permanent_postal_code' => $this->getPermanentPostalCode(),
			'permanent_country_code' => $this->getPermanentCountryCode(),
			'permanent_province' => $this->getPermanentProvince(),
			'permanent_is_verified' => $this->getPermanentIsVerified(),
			'primary_vehicle_make' => $this->getPrimaryVehicleMake(),
			'primary_vehicle_model' => $this->getPrimaryVehicleModel(),
			'primary_vehicle_year' => $this->getPrimaryVehicleYear(),
			'primary_vehicle_color' => $this->getPrimaryVehicleColor(),
			'primary_vehicle_license_plate_number' => $this->getPrimaryVehicleLicensePlateNumber(),
			'primary_vehicle_state_code' => $this->getPrimaryVehicleStateCode(),
			'primary_vehicle_province' => $this->getPrimaryVehicleProvince(),
			'secondary_vehicle_make' => $this->getSecondaryVehicleMake(),
			'secondary_vehicle_model' => $this->getSecondaryVehicleModel(),
			'secondary_vehicle_year' => $this->getSecondaryVehicleYear(),
			'secondary_vehicle_color' => $this->getSecondaryVehicleColor(),
			'secondary_vehicle_license_plate_number' => $this->getSecondaryVehicleLicensePlateNumber(),
			'secondary_vehicle_state_code' => $this->getSecondaryVehicleStateCode(),
			'secondary_vehicle_province' => $this->getSecondaryVehicleProvince(),
			'phone_number' => $this->getPhoneNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'work_number' => $this->getWorkNumber(),
			'fax_number' => $this->getFaxNumber(),
			'email_address' => $this->getEmailAddress(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'tax_number_masked' => $this->getTaxNumberMasked(),
			'returned_payments_count' => $this->getReturnedPaymentsCount(),
			'birth_date' => $this->getBirthDate(),
			'gender' => $this->getGender(),
			'dl_number_encrypted' => $this->getDlNumberEncrypted(),
			'dl_state_code' => $this->getDlStateCode(),
			'dl_province' => $this->getDlProvince(),
			'company_identification_type_id' => $this->getCompanyIdentificationTypeId(),
			'identification_value' => $this->getIdentificationValue(),
			'identification_expiration' => $this->getIdentificationExpiration(),
			'notes' => $this->getNotes(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'password_question' => $this->getPasswordQuestion(),
			'password_answer_encrypted' => $this->getPasswordAnswerEncrypted(),
			'password_expiration' => $this->getPasswordExpiration(),
			'facebook_user' => $this->getFacebookUser(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'dont_accept_payments' => $this->getDontAcceptPayments(),
			'block_posting' => $this->getBlockPosting(),
			'block_networking' => $this->getBlockNetworking(),
			'block_online_payments' => $this->getBlockOnlinePayments(),
			'is_networking_enrolled' => $this->getIsNetworkingEnrolled(),
			'approval_ip_address' => $this->getApprovalIpAddress(),
			'terms_approval_ip' => $this->getTermsApprovalIp(),
			'terms_approved_on' => $this->getTermsApprovedOn(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'profile_published_by' => $this->getProfilePublishedBy(),
			'profile_published_on' => $this->getProfilePublishedOn(),
			'mobile_number_authorized_on' => $this->getMobileNumberAuthorizedOn(),
			'imported_on' => $this->getImportedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_organization' => $this->getIsOrganization(),
			'alt_name_first' => $this->getAltNameFirst(),
			'alt_name_middle' => $this->getAltNameMiddle(),
			'alt_name_last' => $this->getAltNameLast(),
			'preferred_name' => $this->getPreferredName(),
			'preferred_locale_code' => $this->getPreferredLocaleCode(),
			'name_last_matronymic' => $this->getNameLastMatronymic(),
			'has_name_last_matronymic' => $this->getHasNameLastMatronymic(),
			'citizenship_country_code' => $this->getCitizenshipCountryCode(),
			'tax_id_type_id' => $this->getTaxIdTypeId(),
			'details' => $this->getDetails(),
			'tax_number_blind_index' => $this->getTaxNumberBlindIndex()
		);
	}

}
?>