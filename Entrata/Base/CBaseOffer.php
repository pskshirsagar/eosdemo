<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOffer extends CEosSingularBase {

	const TABLE_NAME = 'public.offers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intOfferTemplateId;
	protected $m_intTemplateOffset;
	protected $m_intDaysOffset;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intMaxOfferCount;
	protected $m_boolIsActive;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImportOfferId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['offer_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOfferTemplateId', trim( $arrValues['offer_template_id'] ) ); elseif( isset( $arrValues['offer_template_id'] ) ) $this->setOfferTemplateId( $arrValues['offer_template_id'] );
		if( isset( $arrValues['template_offset'] ) && $boolDirectSet ) $this->set( 'm_intTemplateOffset', trim( $arrValues['template_offset'] ) ); elseif( isset( $arrValues['template_offset'] ) ) $this->setTemplateOffset( $arrValues['template_offset'] );
		if( isset( $arrValues['days_offset'] ) && $boolDirectSet ) $this->set( 'm_intDaysOffset', trim( $arrValues['days_offset'] ) ); elseif( isset( $arrValues['days_offset'] ) ) $this->setDaysOffset( $arrValues['days_offset'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['max_offer_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxOfferCount', trim( $arrValues['max_offer_count'] ) ); elseif( isset( $arrValues['max_offer_count'] ) ) $this->setMaxOfferCount( $arrValues['max_offer_count'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['import_offer_id'] ) && $boolDirectSet ) $this->set( 'm_intImportOfferId', trim( $arrValues['import_offer_id'] ) ); elseif( isset( $arrValues['import_offer_id'] ) ) $this->setImportOfferId( $arrValues['import_offer_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setOfferTemplateId( $intOfferTemplateId ) {
		$this->set( 'm_intOfferTemplateId', CStrings::strToIntDef( $intOfferTemplateId, NULL, false ) );
	}

	public function getOfferTemplateId() {
		return $this->m_intOfferTemplateId;
	}

	public function sqlOfferTemplateId() {
		return ( true == isset( $this->m_intOfferTemplateId ) ) ? ( string ) $this->m_intOfferTemplateId : 'NULL';
	}

	public function setTemplateOffset( $intTemplateOffset ) {
		$this->set( 'm_intTemplateOffset', CStrings::strToIntDef( $intTemplateOffset, NULL, false ) );
	}

	public function getTemplateOffset() {
		return $this->m_intTemplateOffset;
	}

	public function sqlTemplateOffset() {
		return ( true == isset( $this->m_intTemplateOffset ) ) ? ( string ) $this->m_intTemplateOffset : 'NULL';
	}

	public function setDaysOffset( $intDaysOffset ) {
		$this->set( 'm_intDaysOffset', CStrings::strToIntDef( $intDaysOffset, NULL, false ) );
	}

	public function getDaysOffset() {
		return $this->m_intDaysOffset;
	}

	public function sqlDaysOffset() {
		return ( true == isset( $this->m_intDaysOffset ) ) ? ( string ) $this->m_intDaysOffset : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setMaxOfferCount( $intMaxOfferCount ) {
		$this->set( 'm_intMaxOfferCount', CStrings::strToIntDef( $intMaxOfferCount, NULL, false ) );
	}

	public function getMaxOfferCount() {
		return $this->m_intMaxOfferCount;
	}

	public function sqlMaxOfferCount() {
		return ( true == isset( $this->m_intMaxOfferCount ) ) ? ( string ) $this->m_intMaxOfferCount : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImportOfferId( $intImportOfferId ) {
		$this->set( 'm_intImportOfferId', CStrings::strToIntDef( $intImportOfferId, NULL, false ) );
	}

	public function getImportOfferId() {
		return $this->m_intImportOfferId;
	}

	public function sqlImportOfferId() {
		return ( true == isset( $this->m_intImportOfferId ) ) ? ( string ) $this->m_intImportOfferId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, offer_template_id, template_offset, days_offset, start_date, end_date, max_offer_count, is_active, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, import_offer_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlOfferTemplateId() . ', ' .
 						$this->sqlTemplateOffset() . ', ' .
 						$this->sqlDaysOffset() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlMaxOfferCount() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlImportOfferId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_template_id = ' . $this->sqlOfferTemplateId() . ','; } elseif( true == array_key_exists( 'OfferTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' offer_template_id = ' . $this->sqlOfferTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_offset = ' . $this->sqlTemplateOffset() . ','; } elseif( true == array_key_exists( 'TemplateOffset', $this->getChangedColumns() ) ) { $strSql .= ' template_offset = ' . $this->sqlTemplateOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_offset = ' . $this->sqlDaysOffset() . ','; } elseif( true == array_key_exists( 'DaysOffset', $this->getChangedColumns() ) ) { $strSql .= ' days_offset = ' . $this->sqlDaysOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_offer_count = ' . $this->sqlMaxOfferCount() . ','; } elseif( true == array_key_exists( 'MaxOfferCount', $this->getChangedColumns() ) ) { $strSql .= ' max_offer_count = ' . $this->sqlMaxOfferCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_offer_id = ' . $this->sqlImportOfferId() . ','; } elseif( true == array_key_exists( 'ImportOfferId', $this->getChangedColumns() ) ) { $strSql .= ' import_offer_id = ' . $this->sqlImportOfferId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'offer_template_id' => $this->getOfferTemplateId(),
			'template_offset' => $this->getTemplateOffset(),
			'days_offset' => $this->getDaysOffset(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'max_offer_count' => $this->getMaxOfferCount(),
			'is_active' => $this->getIsActive(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'import_offer_id' => $this->getImportOfferId()
		);
	}

}
?>