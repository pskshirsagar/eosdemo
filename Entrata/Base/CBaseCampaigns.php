<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCampaigns
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCampaigns extends CEosPluralBase {

	/**
	 * @return CCampaign[]
	 */
	public static function fetchCampaigns( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCampaign', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCampaign
	 */
	public static function fetchCampaign( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCampaign', $objDatabase );
	}

	public static function fetchCampaignCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'campaigns', $objDatabase );
	}

	public static function fetchCampaignByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCampaign( sprintf( 'SELECT * FROM campaigns WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByCid( $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByCampaignTypeIdByCid( $intCampaignTypeId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE campaign_type_id = %d AND cid = %d', ( int ) $intCampaignTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByLeadSourceIdByCid( $intLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE lead_source_id = %d AND cid = %d', ( int ) $intLeadSourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByEmailTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE system_message_template_id = %d AND cid = %d', ( int ) $intSystemMessageTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsByOriginCampaignIdByCid( $intOriginCampaignId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE origin_campaign_id = %d AND cid = %d', ( int ) $intOriginCampaignId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignsBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {
		return self::fetchCampaigns( sprintf( 'SELECT * FROM campaigns WHERE system_email_id = %d AND cid = %d', ( int ) $intSystemEmailId, ( int ) $intCid ), $objDatabase );
	}

}
?>