<?php

class CBaseWebsitePageType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.website_page_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strUrlSlug;
	protected $m_strSeoUrlSlug;
	protected $m_boolIsPrimary;
	protected $m_strReferenceTable;
	protected $m_boolIsMultiProperty;
	protected $m_boolIsSingleProperty;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPrimary = false;
		$this->m_boolIsMultiProperty = true;
		$this->m_boolIsSingleProperty = true;
		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['url_slug'] ) && $boolDirectSet ) $this->set( 'm_strUrlSlug', trim( stripcslashes( $arrValues['url_slug'] ) ) ); elseif( isset( $arrValues['url_slug'] ) ) $this->setUrlSlug( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url_slug'] ) : $arrValues['url_slug'] );
		if( isset( $arrValues['seo_url_slug'] ) && $boolDirectSet ) $this->set( 'm_strSeoUrlSlug', trim( stripcslashes( $arrValues['seo_url_slug'] ) ) ); elseif( isset( $arrValues['seo_url_slug'] ) ) $this->setSeoUrlSlug( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['seo_url_slug'] ) : $arrValues['seo_url_slug'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrimary', trim( stripcslashes( $arrValues['is_primary'] ) ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_primary'] ) : $arrValues['is_primary'] );
		if( isset( $arrValues['reference_table'] ) && $boolDirectSet ) $this->set( 'm_strReferenceTable', trim( stripcslashes( $arrValues['reference_table'] ) ) ); elseif( isset( $arrValues['reference_table'] ) ) $this->setReferenceTable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_table'] ) : $arrValues['reference_table'] );
		if( isset( $arrValues['is_multi_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsMultiProperty', trim( stripcslashes( $arrValues['is_multi_property'] ) ) ); elseif( isset( $arrValues['is_multi_property'] ) ) $this->setIsMultiProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_multi_property'] ) : $arrValues['is_multi_property'] );
		if( isset( $arrValues['is_single_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsSingleProperty', trim( stripcslashes( $arrValues['is_single_property'] ) ) ); elseif( isset( $arrValues['is_single_property'] ) ) $this->setIsSingleProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_single_property'] ) : $arrValues['is_single_property'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setUrlSlug( $strUrlSlug ) {
		$this->set( 'm_strUrlSlug', CStrings::strTrimDef( $strUrlSlug, 255, NULL, true ) );
	}

	public function getUrlSlug() {
		return $this->m_strUrlSlug;
	}

	public function sqlUrlSlug() {
		return ( true == isset( $this->m_strUrlSlug ) ) ? '\'' . addslashes( $this->m_strUrlSlug ) . '\'' : 'NULL';
	}

	public function setSeoUrlSlug( $strSeoUrlSlug ) {
		$this->set( 'm_strSeoUrlSlug', CStrings::strTrimDef( $strSeoUrlSlug, 255, NULL, true ) );
	}

	public function getSeoUrlSlug() {
		return $this->m_strSeoUrlSlug;
	}

	public function sqlSeoUrlSlug() {
		return ( true == isset( $this->m_strSeoUrlSlug ) ) ? '\'' . addslashes( $this->m_strSeoUrlSlug ) . '\'' : 'NULL';
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->set( 'm_boolIsPrimary', CStrings::strToBool( $boolIsPrimary ) );
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_boolIsPrimary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrimary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReferenceTable( $strReferenceTable ) {
		$this->set( 'm_strReferenceTable', CStrings::strTrimDef( $strReferenceTable, 255, NULL, true ) );
	}

	public function getReferenceTable() {
		return $this->m_strReferenceTable;
	}

	public function sqlReferenceTable() {
		return ( true == isset( $this->m_strReferenceTable ) ) ? '\'' . addslashes( $this->m_strReferenceTable ) . '\'' : 'NULL';
	}

	public function setIsMultiProperty( $boolIsMultiProperty ) {
		$this->set( 'm_boolIsMultiProperty', CStrings::strToBool( $boolIsMultiProperty ) );
	}

	public function getIsMultiProperty() {
		return $this->m_boolIsMultiProperty;
	}

	public function sqlIsMultiProperty() {
		return ( true == isset( $this->m_boolIsMultiProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMultiProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSingleProperty( $boolIsSingleProperty ) {
		$this->set( 'm_boolIsSingleProperty', CStrings::strToBool( $boolIsSingleProperty ) );
	}

	public function getIsSingleProperty() {
		return $this->m_boolIsSingleProperty;
	}

	public function sqlIsSingleProperty() {
		return ( true == isset( $this->m_boolIsSingleProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSingleProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'url_slug' => $this->getUrlSlug(),
			'seo_url_slug' => $this->getSeoUrlSlug(),
			'is_primary' => $this->getIsPrimary(),
			'reference_table' => $this->getReferenceTable(),
			'is_multi_property' => $this->getIsMultiProperty(),
			'is_single_property' => $this->getIsSingleProperty(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>