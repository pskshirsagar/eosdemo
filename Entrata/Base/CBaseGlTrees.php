<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTrees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlTrees extends CEosPluralBase {

	/**
	 * @return CGlTree[]
	 */
	public static function fetchGlTrees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlTree::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlTree
	 */
	public static function fetchGlTree( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlTree::class, $objDatabase );
	}

	public static function fetchGlTreeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_trees', $objDatabase );
	}

	public static function fetchGlTreeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlTree( sprintf( 'SELECT * FROM gl_trees WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlTreesByCid( $intCid, $objDatabase ) {
		return self::fetchGlTrees( sprintf( 'SELECT * FROM gl_trees WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlTreesByGlTreeTypeIdByCid( $intGlTreeTypeId, $intCid, $objDatabase ) {
		return self::fetchGlTrees( sprintf( 'SELECT * FROM gl_trees WHERE gl_tree_type_id = %d AND cid = %d', $intGlTreeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlTreesByModeTypeIdByCid( $intModeTypeId, $intCid, $objDatabase ) {
		return self::fetchGlTrees( sprintf( 'SELECT * FROM gl_trees WHERE mode_type_id = %d AND cid = %d', $intModeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlTreesByRestrictedGlTreeIdByCid( $intRestrictedGlTreeId, $intCid, $objDatabase ) {
		return self::fetchGlTrees( sprintf( 'SELECT * FROM gl_trees WHERE restricted_gl_tree_id = %d AND cid = %d', $intRestrictedGlTreeId, $intCid ), $objDatabase );
	}

	public static function fetchGlTreesByGlChartIdByCid( $intGlChartId, $intCid, $objDatabase ) {
		return self::fetchGlTrees( sprintf( 'SELECT * FROM gl_trees WHERE gl_chart_id = %d AND cid = %d', $intGlChartId, $intCid ), $objDatabase );
	}

}
?>