<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAddendaTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentAddendaTypes extends CEosPluralBase {

	/**
	 * @return CDocumentAddendaType[]
	 */
	public static function fetchDocumentAddendaTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentAddendaType', $objDatabase );
	}

	/**
	 * @return CDocumentAddendaType
	 */
	public static function fetchDocumentAddendaType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentAddendaType', $objDatabase );
	}

	public static function fetchDocumentAddendaTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_addenda_types', $objDatabase );
	}

	public static function fetchDocumentAddendaTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentAddendaType( sprintf( 'SELECT * FROM document_addenda_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentAddendaTypesByDocumentAddendaTypeId( $intDocumentAddendaTypeId, $objDatabase ) {
		return self::fetchDocumentAddendaTypes( sprintf( 'SELECT * FROM document_addenda_types WHERE document_addenda_type_id = %d', ( int ) $intDocumentAddendaTypeId ), $objDatabase );
	}

}
?>