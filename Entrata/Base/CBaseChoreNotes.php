<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreNotes extends CEosPluralBase {

	/**
	 * @return CChoreNote[]
	 */
	public static function fetchChoreNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CChoreNote', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChoreNote
	 */
	public static function fetchChoreNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChoreNote', $objDatabase );
	}

	public static function fetchChoreNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_notes', $objDatabase );
	}

	public static function fetchChoreNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChoreNote( sprintf( 'SELECT * FROM chore_notes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreNotesByCid( $intCid, $objDatabase ) {
		return self::fetchChoreNotes( sprintf( 'SELECT * FROM chore_notes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreNotesByChoreIdByCid( $intChoreId, $intCid, $objDatabase ) {
		return self::fetchChoreNotes( sprintf( 'SELECT * FROM chore_notes WHERE chore_id = %d AND cid = %d', ( int ) $intChoreId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreNotesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchChoreNotes( sprintf( 'SELECT * FROM chore_notes WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreNotesByTaskNoteIdByCid( $intTaskNoteId, $intCid, $objDatabase ) {
		return self::fetchChoreNotes( sprintf( 'SELECT * FROM chore_notes WHERE task_note_id = %d AND cid = %d', ( int ) $intTaskNoteId, ( int ) $intCid ), $objDatabase );
	}

}
?>