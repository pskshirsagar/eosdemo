<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactAvailabilityTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyEmployeeContactAvailabilityTypes extends CEosPluralBase {

	/**
	 * @return CCompanyEmployeeContactAvailabilityType[]
	 */
	public static function fetchCompanyEmployeeContactAvailabilityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyEmployeeContactAvailabilityType::class, $objDatabase );
	}

	/**
	 * @return CCompanyEmployeeContactAvailabilityType
	 */
	public static function fetchCompanyEmployeeContactAvailabilityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEmployeeContactAvailabilityType::class, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_employee_contact_availability_types', $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilityTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyEmployeeContactAvailabilityType( sprintf( 'SELECT * FROM company_employee_contact_availability_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>