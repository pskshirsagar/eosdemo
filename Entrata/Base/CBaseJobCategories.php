<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobCategories extends CEosPluralBase {

	/**
	 * @return CJobCategory[]
	 */
	public static function fetchJobCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobCategory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobCategory
	 */
	public static function fetchJobCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobCategory::class, $objDatabase );
	}

	public static function fetchJobCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_categories', $objDatabase );
	}

	public static function fetchJobCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobCategory( sprintf( 'SELECT * FROM job_categories WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchJobCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchJobCategories( sprintf( 'SELECT * FROM job_categories WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchJobCategoriesByDefaultGlAccountIdByCid( $intDefaultGlAccountId, $intCid, $objDatabase ) {
		return self::fetchJobCategories( sprintf( 'SELECT * FROM job_categories WHERE default_gl_account_id = %d AND cid = %d', $intDefaultGlAccountId, $intCid ), $objDatabase );
	}

}
?>