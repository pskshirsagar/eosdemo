<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApCodeCategory extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ap_code_categories';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultApCodeCategoryId;
	protected $m_intApCodeTypeId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intConsumptionGlAccountId;
	protected $m_intVendorProductCategoryId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsDisabled;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_ap_code_category_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultApCodeCategoryId', trim( $arrValues['default_ap_code_category_id'] ) ); elseif( isset( $arrValues['default_ap_code_category_id'] ) ) $this->setDefaultApCodeCategoryId( $arrValues['default_ap_code_category_id'] );
		if( isset( $arrValues['ap_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeTypeId', trim( $arrValues['ap_code_type_id'] ) ); elseif( isset( $arrValues['ap_code_type_id'] ) ) $this->setApCodeTypeId( $arrValues['ap_code_type_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['consumption_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intConsumptionGlAccountId', trim( $arrValues['consumption_gl_account_id'] ) ); elseif( isset( $arrValues['consumption_gl_account_id'] ) ) $this->setConsumptionGlAccountId( $arrValues['consumption_gl_account_id'] );
		if( isset( $arrValues['vendor_product_category_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorProductCategoryId', trim( $arrValues['vendor_product_category_id'] ) ); elseif( isset( $arrValues['vendor_product_category_id'] ) ) $this->setVendorProductCategoryId( $arrValues['vendor_product_category_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultApCodeCategoryId( $intDefaultApCodeCategoryId ) {
		$this->set( 'm_intDefaultApCodeCategoryId', CStrings::strToIntDef( $intDefaultApCodeCategoryId, NULL, false ) );
	}

	public function getDefaultApCodeCategoryId() {
		return $this->m_intDefaultApCodeCategoryId;
	}

	public function sqlDefaultApCodeCategoryId() {
		return ( true == isset( $this->m_intDefaultApCodeCategoryId ) ) ? ( string ) $this->m_intDefaultApCodeCategoryId : 'NULL';
	}

	public function setApCodeTypeId( $intApCodeTypeId ) {
		$this->set( 'm_intApCodeTypeId', CStrings::strToIntDef( $intApCodeTypeId, NULL, false ) );
	}

	public function getApCodeTypeId() {
		return $this->m_intApCodeTypeId;
	}

	public function sqlApCodeTypeId() {
		return ( true == isset( $this->m_intApCodeTypeId ) ) ? ( string ) $this->m_intApCodeTypeId : 'NULL';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setConsumptionGlAccountId( $intConsumptionGlAccountId ) {
		$this->set( 'm_intConsumptionGlAccountId', CStrings::strToIntDef( $intConsumptionGlAccountId, NULL, false ) );
	}

	public function getConsumptionGlAccountId() {
		return $this->m_intConsumptionGlAccountId;
	}

	public function sqlConsumptionGlAccountId() {
		return ( true == isset( $this->m_intConsumptionGlAccountId ) ) ? ( string ) $this->m_intConsumptionGlAccountId : 'NULL';
	}

	public function setVendorProductCategoryId( $intVendorProductCategoryId ) {
		$this->set( 'm_intVendorProductCategoryId', CStrings::strToIntDef( $intVendorProductCategoryId, NULL, false ) );
	}

	public function getVendorProductCategoryId() {
		return $this->m_intVendorProductCategoryId;
	}

	public function sqlVendorProductCategoryId() {
		return ( true == isset( $this->m_intVendorProductCategoryId ) ) ? ( string ) $this->m_intVendorProductCategoryId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_ap_code_category_id, ap_code_type_id, debit_gl_account_id, credit_gl_account_id, consumption_gl_account_id, vendor_product_category_id, name, description, is_disabled, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultApCodeCategoryId() . ', ' .
						$this->sqlApCodeTypeId() . ', ' .
						$this->sqlDebitGlAccountId() . ', ' .
						$this->sqlCreditGlAccountId() . ', ' .
						$this->sqlConsumptionGlAccountId() . ', ' .
						$this->sqlVendorProductCategoryId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_ap_code_category_id = ' . $this->sqlDefaultApCodeCategoryId(). ',' ; } elseif( true == array_key_exists( 'DefaultApCodeCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' default_ap_code_category_id = ' . $this->sqlDefaultApCodeCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_gl_account_id = ' . $this->sqlConsumptionGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ConsumptionGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' consumption_gl_account_id = ' . $this->sqlConsumptionGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_product_category_id = ' . $this->sqlVendorProductCategoryId(). ',' ; } elseif( true == array_key_exists( 'VendorProductCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_product_category_id = ' . $this->sqlVendorProductCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_ap_code_category_id' => $this->getDefaultApCodeCategoryId(),
			'ap_code_type_id' => $this->getApCodeTypeId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'consumption_gl_account_id' => $this->getConsumptionGlAccountId(),
			'vendor_product_category_id' => $this->getVendorProductCategoryId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_disabled' => $this->getIsDisabled(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>