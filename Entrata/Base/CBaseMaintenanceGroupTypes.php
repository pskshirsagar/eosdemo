<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceGroupTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceGroupType[]
	 */
	public static function fetchMaintenanceGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceGroupType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceGroupType
	 */
	public static function fetchMaintenanceGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceGroupType::class, $objDatabase );
	}

	public static function fetchMaintenanceGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_group_types', $objDatabase );
	}

	public static function fetchMaintenanceGroupTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceGroupType( sprintf( 'SELECT * FROM maintenance_group_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>