<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryUnitCdmpStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseMilitaryUnitCdmpStatusTypes extends CEosPluralBase {

	/**
	 * @return CMilitaryUnitCdmpStatusType[]
	 */
	public static function fetchMilitaryUnitCdmpStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryUnitCdmpStatusType::class, $objDatabase );
	}

	/**
	 * @return CMilitaryUnitCdmpStatusType
	 */
	public static function fetchMilitaryUnitCdmpStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryUnitCdmpStatusType::class, $objDatabase );
	}

	public static function fetchMilitaryUnitCdmpStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_unit_cdmp_status_types', $objDatabase );
	}

	public static function fetchMilitaryUnitCdmpStatusTypeById( $intId, $objDatabase ) {
		return self::fetchMilitaryUnitCdmpStatusType( sprintf( 'SELECT * FROM military_unit_cdmp_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>