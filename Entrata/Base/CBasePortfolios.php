<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPortfolios
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePortfolios extends CEosPluralBase {

	/**
	 * @return CPortfolio[]
	 */
	public static function fetchPortfolios( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPortfolio', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPortfolio
	 */
	public static function fetchPortfolio( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPortfolio', $objDatabase );
	}

	public static function fetchPortfolioCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'portfolios', $objDatabase );
	}

	public static function fetchPortfolioByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPortfolio( sprintf( 'SELECT * FROM portfolios WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPortfoliosByCid( $intCid, $objDatabase ) {
		return self::fetchPortfolios( sprintf( 'SELECT * FROM portfolios WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>