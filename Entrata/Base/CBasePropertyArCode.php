<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyArCode extends CEosSingularBase {

	const TABLE_NAME = 'public.property_ar_codes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intArCodeId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intModifiedDebitGlAccountId;
	protected $m_intWriteOffArCodeId;
	protected $m_intRecoveryArCodeId;
	protected $m_strRemotePrimaryKey;
	protected $m_intIsDisabled;
	protected $m_intOrderNum;
	protected $m_intPriorityNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intArCodeAllocationTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDisabled = '0';
		$this->m_intOrderNum = '0';
		$this->m_intPriorityNum = '0';
		$this->m_intArCodeAllocationTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['modified_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intModifiedDebitGlAccountId', trim( $arrValues['modified_debit_gl_account_id'] ) ); elseif( isset( $arrValues['modified_debit_gl_account_id'] ) ) $this->setModifiedDebitGlAccountId( $arrValues['modified_debit_gl_account_id'] );
		if( isset( $arrValues['write_off_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intWriteOffArCodeId', trim( $arrValues['write_off_ar_code_id'] ) ); elseif( isset( $arrValues['write_off_ar_code_id'] ) ) $this->setWriteOffArCodeId( $arrValues['write_off_ar_code_id'] );
		if( isset( $arrValues['recovery_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecoveryArCodeId', trim( $arrValues['recovery_ar_code_id'] ) ); elseif( isset( $arrValues['recovery_ar_code_id'] ) ) $this->setRecoveryArCodeId( $arrValues['recovery_ar_code_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['priority_num'] ) && $boolDirectSet ) $this->set( 'm_intPriorityNum', trim( $arrValues['priority_num'] ) ); elseif( isset( $arrValues['priority_num'] ) ) $this->setPriorityNum( $arrValues['priority_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ar_code_allocation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeAllocationTypeId', trim( $arrValues['ar_code_allocation_type_id'] ) ); elseif( isset( $arrValues['ar_code_allocation_type_id'] ) ) $this->setArCodeAllocationTypeId( $arrValues['ar_code_allocation_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setModifiedDebitGlAccountId( $intModifiedDebitGlAccountId ) {
		$this->set( 'm_intModifiedDebitGlAccountId', CStrings::strToIntDef( $intModifiedDebitGlAccountId, NULL, false ) );
	}

	public function getModifiedDebitGlAccountId() {
		return $this->m_intModifiedDebitGlAccountId;
	}

	public function sqlModifiedDebitGlAccountId() {
		return ( true == isset( $this->m_intModifiedDebitGlAccountId ) ) ? ( string ) $this->m_intModifiedDebitGlAccountId : 'NULL';
	}

	public function setWriteOffArCodeId( $intWriteOffArCodeId ) {
		$this->set( 'm_intWriteOffArCodeId', CStrings::strToIntDef( $intWriteOffArCodeId, NULL, false ) );
	}

	public function getWriteOffArCodeId() {
		return $this->m_intWriteOffArCodeId;
	}

	public function sqlWriteOffArCodeId() {
		return ( true == isset( $this->m_intWriteOffArCodeId ) ) ? ( string ) $this->m_intWriteOffArCodeId : 'NULL';
	}

	public function setRecoveryArCodeId( $intRecoveryArCodeId ) {
		$this->set( 'm_intRecoveryArCodeId', CStrings::strToIntDef( $intRecoveryArCodeId, NULL, false ) );
	}

	public function getRecoveryArCodeId() {
		return $this->m_intRecoveryArCodeId;
	}

	public function sqlRecoveryArCodeId() {
		return ( true == isset( $this->m_intRecoveryArCodeId ) ) ? ( string ) $this->m_intRecoveryArCodeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPriorityNum( $intPriorityNum ) {
		$this->set( 'm_intPriorityNum', CStrings::strToIntDef( $intPriorityNum, NULL, false ) );
	}

	public function getPriorityNum() {
		return $this->m_intPriorityNum;
	}

	public function sqlPriorityNum() {
		return ( true == isset( $this->m_intPriorityNum ) ) ? ( string ) $this->m_intPriorityNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setArCodeAllocationTypeId( $intArCodeAllocationTypeId ) {
		$this->set( 'm_intArCodeAllocationTypeId', CStrings::strToIntDef( $intArCodeAllocationTypeId, NULL, false ) );
	}

	public function getArCodeAllocationTypeId() {
		return $this->m_intArCodeAllocationTypeId;
	}

	public function sqlArCodeAllocationTypeId() {
		return ( true == isset( $this->m_intArCodeAllocationTypeId ) ) ? ( string ) $this->m_intArCodeAllocationTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ar_code_id, debit_gl_account_id, credit_gl_account_id, modified_debit_gl_account_id, write_off_ar_code_id, recovery_ar_code_id, remote_primary_key, is_disabled, order_num, priority_num, updated_by, updated_on, created_by, created_on, ar_code_allocation_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlDebitGlAccountId() . ', ' .
						$this->sqlCreditGlAccountId() . ', ' .
						$this->sqlModifiedDebitGlAccountId() . ', ' .
						$this->sqlWriteOffArCodeId() . ', ' .
						$this->sqlRecoveryArCodeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlPriorityNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlArCodeAllocationTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_gl_account_id = ' . $this->sqlDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' modified_debit_gl_account_id = ' . $this->sqlModifiedDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ModifiedDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' modified_debit_gl_account_id = ' . $this->sqlModifiedDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' write_off_ar_code_id = ' . $this->sqlWriteOffArCodeId(). ',' ; } elseif( true == array_key_exists( 'WriteOffArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' write_off_ar_code_id = ' . $this->sqlWriteOffArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recovery_ar_code_id = ' . $this->sqlRecoveryArCodeId(). ',' ; } elseif( true == array_key_exists( 'RecoveryArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' recovery_ar_code_id = ' . $this->sqlRecoveryArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority_num = ' . $this->sqlPriorityNum(). ',' ; } elseif( true == array_key_exists( 'PriorityNum', $this->getChangedColumns() ) ) { $strSql .= ' priority_num = ' . $this->sqlPriorityNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_allocation_type_id = ' . $this->sqlArCodeAllocationTypeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeAllocationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_allocation_type_id = ' . $this->sqlArCodeAllocationTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ar_code_id' => $this->getArCodeId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'modified_debit_gl_account_id' => $this->getModifiedDebitGlAccountId(),
			'write_off_ar_code_id' => $this->getWriteOffArCodeId(),
			'recovery_ar_code_id' => $this->getRecoveryArCodeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'is_disabled' => $this->getIsDisabled(),
			'order_num' => $this->getOrderNum(),
			'priority_num' => $this->getPriorityNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ar_code_allocation_type_id' => $this->getArCodeAllocationTypeId()
		);
	}

}
?>