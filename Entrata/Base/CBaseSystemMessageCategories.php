<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemMessageCategories
 * Do not add any new functions to this class.
 */

class CBaseSystemMessageCategories extends CEosPluralBase {

	/**
	 * @return CSystemMessageCategory[]
	 */
	public static function fetchSystemMessageCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemMessageCategory::class, $objDatabase );
	}

	/**
	 * @return CSystemMessageCategory
	 */
	public static function fetchSystemMessageCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemMessageCategory::class, $objDatabase );
	}

	public static function fetchSystemMessageCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_message_categories', $objDatabase );
	}

	public static function fetchSystemMessageCategoryById( $intId, $objDatabase ) {
		return self::fetchSystemMessageCategory( sprintf( 'SELECT * FROM system_message_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>