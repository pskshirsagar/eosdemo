<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwners
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwners extends CEosPluralBase {

	/**
	 * @return COwner[]
	 */
	public static function fetchOwners( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COwner', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COwner
	 */
	public static function fetchOwner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COwner', $objDatabase );
	}

	public static function fetchOwnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'owners', $objDatabase );
	}

	public static function fetchOwnerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOwner( sprintf( 'SELECT * FROM owners WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByCid( $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByOwnerTypeIdByCid( $intOwnerTypeId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE owner_type_id = %d AND cid = %d', ( int ) $intOwnerTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchOwner( sprintf( 'SELECT * FROM owners WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByBusinessCompanyUserIdByCid( $intBusinessCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE business_company_user_id = %d AND cid = %d', ( int ) $intBusinessCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByAccountingCompanyUserIdByCid( $intAccountingCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE accounting_company_user_id = %d AND cid = %d', ( int ) $intAccountingCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByDistributionGlAccountIdByCid( $intDistributionGlAccountId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE distribution_gl_account_id = %d AND cid = %d', ( int ) $intDistributionGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnersByContributionGlAccountIdByCid( $intContributionGlAccountId, $intCid, $objDatabase ) {
		return self::fetchOwners( sprintf( 'SELECT * FROM owners WHERE contribution_gl_account_id = %d AND cid = %d', ( int ) $intContributionGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>