<?php

class CBaseEvictionTypes extends CEosPluralBase {

    const TABLE_EVICTION_TYPES = 'public.eviction_types';

    public static function fetchEvictionTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CEvictionType', $objDatabase );
    }

    public static function fetchEvictionType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CEvictionType', $objDatabase );
    }

    public static function fetchEvictionTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'eviction_types', $objDatabase );
    }

    public static function fetchEvictionTypeById( $intId, $objDatabase ) {
        return self::fetchEvictionType( sprintf( 'SELECT * FROM eviction_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>