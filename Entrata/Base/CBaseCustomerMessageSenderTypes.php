<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessageSenderTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerMessageSenderTypes extends CEosPluralBase {

	/**
	 * @return CCustomerMessageSenderType[]
	 */
	public static function fetchCustomerMessageSenderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerMessageSenderType::class, $objDatabase );
	}

	/**
	 * @return CCustomerMessageSenderType
	 */
	public static function fetchCustomerMessageSenderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerMessageSenderType::class, $objDatabase );
	}

	public static function fetchCustomerMessageSenderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_message_sender_types', $objDatabase );
	}

	public static function fetchCustomerMessageSenderTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerMessageSenderType( sprintf( 'SELECT * FROM customer_message_sender_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>