<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportBatchDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportBatchDetails extends CEosPluralBase {

	/**
	 * @return CAccountingExportBatchDetail[]
	 */
	public static function fetchAccountingExportBatchDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountingExportBatchDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountingExportBatchDetail
	 */
	public static function fetchAccountingExportBatchDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportBatchDetail::class, $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_batch_details', $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetail( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetails( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetails( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailsByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetails( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE accounting_export_batch_id = %d AND cid = %d', ( int ) $intAccountingExportBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailsByAccrualGlAccountIdByCid( $intAccrualGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetails( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE accrual_gl_account_id = %d AND cid = %d', ( int ) $intAccrualGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchDetailsByCashGlAccountIdByCid( $intCashGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatchDetails( sprintf( 'SELECT * FROM accounting_export_batch_details WHERE cash_gl_account_id = %d AND cid = %d', ( int ) $intCashGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>