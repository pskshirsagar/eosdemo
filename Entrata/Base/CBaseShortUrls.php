<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CShortUrls
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseShortUrls extends CEosPluralBase {

	/**
	 * @return CShortUrl[]
	 */
	public static function fetchShortUrls( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CShortUrl', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CShortUrl
	 */
	public static function fetchShortUrl( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CShortUrl', $objDatabase );
	}

	public static function fetchShortUrlCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'short_urls', $objDatabase );
	}

	public static function fetchShortUrlByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchShortUrl( sprintf( 'SELECT * FROM short_urls WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchShortUrlsByCid( $intCid, $objDatabase ) {
		return self::fetchShortUrls( sprintf( 'SELECT * FROM short_urls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>