<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.inspection_responses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInspectionId;
	protected $m_intInspectionFormLocationProblemId;
	protected $m_intPropertyUnitMaintenanceLocationId;
	protected $m_intMaintenanceProblemId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intIsFailed;
	protected $m_boolIsInsuranceClaim;
	protected $m_intMaintenanceRequestId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsInsuranceClaim = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['inspection_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionId', trim( $arrValues['inspection_id'] ) ); elseif( isset( $arrValues['inspection_id'] ) ) $this->setInspectionId( $arrValues['inspection_id'] );
		if( isset( $arrValues['inspection_form_location_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionFormLocationProblemId', trim( $arrValues['inspection_form_location_problem_id'] ) ); elseif( isset( $arrValues['inspection_form_location_problem_id'] ) ) $this->setInspectionFormLocationProblemId( $arrValues['inspection_form_location_problem_id'] );
		if( isset( $arrValues['property_unit_maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitMaintenanceLocationId', trim( $arrValues['property_unit_maintenance_location_id'] ) ); elseif( isset( $arrValues['property_unit_maintenance_location_id'] ) ) $this->setPropertyUnitMaintenanceLocationId( $arrValues['property_unit_maintenance_location_id'] );
		if( isset( $arrValues['maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemId', trim( $arrValues['maintenance_problem_id'] ) ); elseif( isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_intIsFailed', trim( $arrValues['is_failed'] ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( $arrValues['is_failed'] );
		if( isset( $arrValues['is_insurance_claim'] ) && $boolDirectSet ) $this->set( 'm_boolIsInsuranceClaim', trim( stripcslashes( $arrValues['is_insurance_claim'] ) ) ); elseif( isset( $arrValues['is_insurance_claim'] ) ) $this->setIsInsuranceClaim( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_insurance_claim'] ) : $arrValues['is_insurance_claim'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInspectionId( $intInspectionId ) {
		$this->set( 'm_intInspectionId', CStrings::strToIntDef( $intInspectionId, NULL, false ) );
	}

	public function getInspectionId() {
		return $this->m_intInspectionId;
	}

	public function sqlInspectionId() {
		return ( true == isset( $this->m_intInspectionId ) ) ? ( string ) $this->m_intInspectionId : 'NULL';
	}

	public function setInspectionFormLocationProblemId( $intInspectionFormLocationProblemId ) {
		$this->set( 'm_intInspectionFormLocationProblemId', CStrings::strToIntDef( $intInspectionFormLocationProblemId, NULL, false ) );
	}

	public function getInspectionFormLocationProblemId() {
		return $this->m_intInspectionFormLocationProblemId;
	}

	public function sqlInspectionFormLocationProblemId() {
		return ( true == isset( $this->m_intInspectionFormLocationProblemId ) ) ? ( string ) $this->m_intInspectionFormLocationProblemId : 'NULL';
	}

	public function setPropertyUnitMaintenanceLocationId( $intPropertyUnitMaintenanceLocationId ) {
		$this->set( 'm_intPropertyUnitMaintenanceLocationId', CStrings::strToIntDef( $intPropertyUnitMaintenanceLocationId, NULL, false ) );
	}

	public function getPropertyUnitMaintenanceLocationId() {
		return $this->m_intPropertyUnitMaintenanceLocationId;
	}

	public function sqlPropertyUnitMaintenanceLocationId() {
		return ( true == isset( $this->m_intPropertyUnitMaintenanceLocationId ) ) ? ( string ) $this->m_intPropertyUnitMaintenanceLocationId : 'NULL';
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->set( 'm_intMaintenanceProblemId', CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false ) );
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function sqlMaintenanceProblemId() {
		return ( true == isset( $this->m_intMaintenanceProblemId ) ) ? ( string ) $this->m_intMaintenanceProblemId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setIsFailed( $intIsFailed ) {
		$this->set( 'm_intIsFailed', CStrings::strToIntDef( $intIsFailed, NULL, false ) );
	}

	public function getIsFailed() {
		return $this->m_intIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_intIsFailed ) ) ? ( string ) $this->m_intIsFailed : 'NULL';
	}

	public function setIsInsuranceClaim( $boolIsInsuranceClaim ) {
		$this->set( 'm_boolIsInsuranceClaim', CStrings::strToBool( $boolIsInsuranceClaim ) );
	}

	public function getIsInsuranceClaim() {
		return $this->m_boolIsInsuranceClaim;
	}

	public function sqlIsInsuranceClaim() {
		return ( true == isset( $this->m_boolIsInsuranceClaim ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInsuranceClaim ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, inspection_id, inspection_form_location_problem_id, property_unit_maintenance_location_id, maintenance_problem_id, customer_id, lease_id, is_failed, is_insurance_claim, maintenance_request_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlInspectionId() . ', ' .
 						$this->sqlInspectionFormLocationProblemId() . ', ' .
 						$this->sqlPropertyUnitMaintenanceLocationId() . ', ' .
 						$this->sqlMaintenanceProblemId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlIsInsuranceClaim() . ', ' .
 						$this->sqlMaintenanceRequestId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_id = ' . $this->sqlInspectionId() . ','; } elseif( true == array_key_exists( 'InspectionId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_id = ' . $this->sqlInspectionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_form_location_problem_id = ' . $this->sqlInspectionFormLocationProblemId() . ','; } elseif( true == array_key_exists( 'InspectionFormLocationProblemId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_form_location_problem_id = ' . $this->sqlInspectionFormLocationProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_maintenance_location_id = ' . $this->sqlPropertyUnitMaintenanceLocationId() . ','; } elseif( true == array_key_exists( 'PropertyUnitMaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_maintenance_location_id = ' . $this->sqlPropertyUnitMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; } elseif( true == array_key_exists( 'MaintenanceProblemId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_insurance_claim = ' . $this->sqlIsInsuranceClaim() . ','; } elseif( true == array_key_exists( 'IsInsuranceClaim', $this->getChangedColumns() ) ) { $strSql .= ' is_insurance_claim = ' . $this->sqlIsInsuranceClaim() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'inspection_id' => $this->getInspectionId(),
			'inspection_form_location_problem_id' => $this->getInspectionFormLocationProblemId(),
			'property_unit_maintenance_location_id' => $this->getPropertyUnitMaintenanceLocationId(),
			'maintenance_problem_id' => $this->getMaintenanceProblemId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'is_failed' => $this->getIsFailed(),
			'is_insurance_claim' => $this->getIsInsuranceClaim(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>