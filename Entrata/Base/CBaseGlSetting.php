<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApGlAccountId;
	protected $m_intRetainedEarningsGlAccountId;
	protected $m_intMarketRentGlAccountId;
	protected $m_intGainToLeaseGlAccountId;
	protected $m_intLossToLeaseGlAccountId;
	protected $m_intVacancyLossGlAccountId;
	protected $m_intInterCompanyArGlAccountId;
	protected $m_intInterCompanyApGlAccountId;
	protected $m_intFloatingReimbursementsGlAccountId;
	protected $m_intRetentionPayableGlAccountId;
	protected $m_intAccelRentArCodeId;
	protected $m_intRentArCodeId;
	protected $m_intDepositArCodeId;
	protected $m_intMtmRentArCodeId;
	protected $m_intReimbursementArCodeId;
	protected $m_intManagementFeeArCodeId;
	protected $m_intOwnerContributionArCodeId;
	protected $m_intLenderContributionArCodeId;
	protected $m_intGlDimensionsAllowanceTypeId;
	protected $m_intDepartmentAllowanceTypeId;
	protected $m_intConfidentialityAllowanceTypeId;
	protected $m_intApMerchantAccountId;
	protected $m_intArAdvanceDay;
	protected $m_intApAdvanceDay;
	protected $m_intGlAdvanceDay;
	protected $m_intIgnoreHolidaysAndWeekends;
	protected $m_intAutoPostRefundInvoice;
	protected $m_intArAutoAdvancePostMonth;
	protected $m_intApAutoAdvancePostMonth;
	protected $m_intGlAutoAdvancePostMonth;
	protected $m_intAllowApConsolidation;
	protected $m_intShowVendorNumbersInAp;
	protected $m_intShowPropertyCodesInAp;
	protected $m_boolActivateStandardPosting;
	protected $m_boolActivatePoPosting;
	protected $m_boolActivateAssetPosting;
	protected $m_intIsCashBasis;
	protected $m_intAllocateRentFirst;
	protected $m_boolAllowItemConfidentiality;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intGlDimensionsAllowanceTypeId = '1';
		$this->m_intDepartmentAllowanceTypeId = '1';
		$this->m_intConfidentialityAllowanceTypeId = '1';
		$this->m_intArAdvanceDay = '25';
		$this->m_intApAdvanceDay = '25';
		$this->m_intGlAdvanceDay = '25';
		$this->m_intIgnoreHolidaysAndWeekends = '0';
		$this->m_intAutoPostRefundInvoice = '0';
		$this->m_intArAutoAdvancePostMonth = '0';
		$this->m_intApAutoAdvancePostMonth = '0';
		$this->m_intGlAutoAdvancePostMonth = '0';
		$this->m_intAllowApConsolidation = '1';
		$this->m_intShowVendorNumbersInAp = '0';
		$this->m_intShowPropertyCodesInAp = '0';
		$this->m_boolActivateStandardPosting = false;
		$this->m_boolActivatePoPosting = false;
		$this->m_boolActivateAssetPosting = false;
		$this->m_intIsCashBasis = '0';
		$this->m_intAllocateRentFirst = '0';
		$this->m_boolAllowItemConfidentiality = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApGlAccountId', trim( $arrValues['ap_gl_account_id'] ) ); elseif( isset( $arrValues['ap_gl_account_id'] ) ) $this->setApGlAccountId( $arrValues['ap_gl_account_id'] );
		if( isset( $arrValues['retained_earnings_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRetainedEarningsGlAccountId', trim( $arrValues['retained_earnings_gl_account_id'] ) ); elseif( isset( $arrValues['retained_earnings_gl_account_id'] ) ) $this->setRetainedEarningsGlAccountId( $arrValues['retained_earnings_gl_account_id'] );
		if( isset( $arrValues['market_rent_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketRentGlAccountId', trim( $arrValues['market_rent_gl_account_id'] ) ); elseif( isset( $arrValues['market_rent_gl_account_id'] ) ) $this->setMarketRentGlAccountId( $arrValues['market_rent_gl_account_id'] );
		if( isset( $arrValues['gain_to_lease_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGainToLeaseGlAccountId', trim( $arrValues['gain_to_lease_gl_account_id'] ) ); elseif( isset( $arrValues['gain_to_lease_gl_account_id'] ) ) $this->setGainToLeaseGlAccountId( $arrValues['gain_to_lease_gl_account_id'] );
		if( isset( $arrValues['loss_to_lease_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intLossToLeaseGlAccountId', trim( $arrValues['loss_to_lease_gl_account_id'] ) ); elseif( isset( $arrValues['loss_to_lease_gl_account_id'] ) ) $this->setLossToLeaseGlAccountId( $arrValues['loss_to_lease_gl_account_id'] );
		if( isset( $arrValues['vacancy_loss_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intVacancyLossGlAccountId', trim( $arrValues['vacancy_loss_gl_account_id'] ) ); elseif( isset( $arrValues['vacancy_loss_gl_account_id'] ) ) $this->setVacancyLossGlAccountId( $arrValues['vacancy_loss_gl_account_id'] );
		if( isset( $arrValues['inter_company_ar_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCompanyArGlAccountId', trim( $arrValues['inter_company_ar_gl_account_id'] ) ); elseif( isset( $arrValues['inter_company_ar_gl_account_id'] ) ) $this->setInterCompanyArGlAccountId( $arrValues['inter_company_ar_gl_account_id'] );
		if( isset( $arrValues['inter_company_ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCompanyApGlAccountId', trim( $arrValues['inter_company_ap_gl_account_id'] ) ); elseif( isset( $arrValues['inter_company_ap_gl_account_id'] ) ) $this->setInterCompanyApGlAccountId( $arrValues['inter_company_ap_gl_account_id'] );
		if( isset( $arrValues['floating_reimbursements_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intFloatingReimbursementsGlAccountId', trim( $arrValues['floating_reimbursements_gl_account_id'] ) ); elseif( isset( $arrValues['floating_reimbursements_gl_account_id'] ) ) $this->setFloatingReimbursementsGlAccountId( $arrValues['floating_reimbursements_gl_account_id'] );
		if( isset( $arrValues['retention_payable_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRetentionPayableGlAccountId', trim( $arrValues['retention_payable_gl_account_id'] ) ); elseif( isset( $arrValues['retention_payable_gl_account_id'] ) ) $this->setRetentionPayableGlAccountId( $arrValues['retention_payable_gl_account_id'] );
		if( isset( $arrValues['accel_rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intAccelRentArCodeId', trim( $arrValues['accel_rent_ar_code_id'] ) ); elseif( isset( $arrValues['accel_rent_ar_code_id'] ) ) $this->setAccelRentArCodeId( $arrValues['accel_rent_ar_code_id'] );
		if( isset( $arrValues['rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRentArCodeId', trim( $arrValues['rent_ar_code_id'] ) ); elseif( isset( $arrValues['rent_ar_code_id'] ) ) $this->setRentArCodeId( $arrValues['rent_ar_code_id'] );
		if( isset( $arrValues['deposit_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositArCodeId', trim( $arrValues['deposit_ar_code_id'] ) ); elseif( isset( $arrValues['deposit_ar_code_id'] ) ) $this->setDepositArCodeId( $arrValues['deposit_ar_code_id'] );
		if( isset( $arrValues['mtm_rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intMtmRentArCodeId', trim( $arrValues['mtm_rent_ar_code_id'] ) ); elseif( isset( $arrValues['mtm_rent_ar_code_id'] ) ) $this->setMtmRentArCodeId( $arrValues['mtm_rent_ar_code_id'] );
		if( isset( $arrValues['reimbursement_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementArCodeId', trim( $arrValues['reimbursement_ar_code_id'] ) ); elseif( isset( $arrValues['reimbursement_ar_code_id'] ) ) $this->setReimbursementArCodeId( $arrValues['reimbursement_ar_code_id'] );
		if( isset( $arrValues['management_fee_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intManagementFeeArCodeId', trim( $arrValues['management_fee_ar_code_id'] ) ); elseif( isset( $arrValues['management_fee_ar_code_id'] ) ) $this->setManagementFeeArCodeId( $arrValues['management_fee_ar_code_id'] );
		if( isset( $arrValues['owner_contribution_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerContributionArCodeId', trim( $arrValues['owner_contribution_ar_code_id'] ) ); elseif( isset( $arrValues['owner_contribution_ar_code_id'] ) ) $this->setOwnerContributionArCodeId( $arrValues['owner_contribution_ar_code_id'] );
		if( isset( $arrValues['lender_contribution_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intLenderContributionArCodeId', trim( $arrValues['lender_contribution_ar_code_id'] ) ); elseif( isset( $arrValues['lender_contribution_ar_code_id'] ) ) $this->setLenderContributionArCodeId( $arrValues['lender_contribution_ar_code_id'] );
		if( isset( $arrValues['gl_dimensions_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDimensionsAllowanceTypeId', trim( $arrValues['gl_dimensions_allowance_type_id'] ) ); elseif( isset( $arrValues['gl_dimensions_allowance_type_id'] ) ) $this->setGlDimensionsAllowanceTypeId( $arrValues['gl_dimensions_allowance_type_id'] );
		if( isset( $arrValues['department_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentAllowanceTypeId', trim( $arrValues['department_allowance_type_id'] ) ); elseif( isset( $arrValues['department_allowance_type_id'] ) ) $this->setDepartmentAllowanceTypeId( $arrValues['department_allowance_type_id'] );
		if( isset( $arrValues['confidentiality_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConfidentialityAllowanceTypeId', trim( $arrValues['confidentiality_allowance_type_id'] ) ); elseif( isset( $arrValues['confidentiality_allowance_type_id'] ) ) $this->setConfidentialityAllowanceTypeId( $arrValues['confidentiality_allowance_type_id'] );
		if( isset( $arrValues['ap_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApMerchantAccountId', trim( $arrValues['ap_merchant_account_id'] ) ); elseif( isset( $arrValues['ap_merchant_account_id'] ) ) $this->setApMerchantAccountId( $arrValues['ap_merchant_account_id'] );
		if( isset( $arrValues['ar_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intArAdvanceDay', trim( $arrValues['ar_advance_day'] ) ); elseif( isset( $arrValues['ar_advance_day'] ) ) $this->setArAdvanceDay( $arrValues['ar_advance_day'] );
		if( isset( $arrValues['ap_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intApAdvanceDay', trim( $arrValues['ap_advance_day'] ) ); elseif( isset( $arrValues['ap_advance_day'] ) ) $this->setApAdvanceDay( $arrValues['ap_advance_day'] );
		if( isset( $arrValues['gl_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intGlAdvanceDay', trim( $arrValues['gl_advance_day'] ) ); elseif( isset( $arrValues['gl_advance_day'] ) ) $this->setGlAdvanceDay( $arrValues['gl_advance_day'] );
		if( isset( $arrValues['ignore_holidays_and_weekends'] ) && $boolDirectSet ) $this->set( 'm_intIgnoreHolidaysAndWeekends', trim( $arrValues['ignore_holidays_and_weekends'] ) ); elseif( isset( $arrValues['ignore_holidays_and_weekends'] ) ) $this->setIgnoreHolidaysAndWeekends( $arrValues['ignore_holidays_and_weekends'] );
		if( isset( $arrValues['auto_post_refund_invoice'] ) && $boolDirectSet ) $this->set( 'm_intAutoPostRefundInvoice', trim( $arrValues['auto_post_refund_invoice'] ) ); elseif( isset( $arrValues['auto_post_refund_invoice'] ) ) $this->setAutoPostRefundInvoice( $arrValues['auto_post_refund_invoice'] );
		if( isset( $arrValues['ar_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intArAutoAdvancePostMonth', trim( $arrValues['ar_auto_advance_post_month'] ) ); elseif( isset( $arrValues['ar_auto_advance_post_month'] ) ) $this->setArAutoAdvancePostMonth( $arrValues['ar_auto_advance_post_month'] );
		if( isset( $arrValues['ap_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intApAutoAdvancePostMonth', trim( $arrValues['ap_auto_advance_post_month'] ) ); elseif( isset( $arrValues['ap_auto_advance_post_month'] ) ) $this->setApAutoAdvancePostMonth( $arrValues['ap_auto_advance_post_month'] );
		if( isset( $arrValues['gl_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intGlAutoAdvancePostMonth', trim( $arrValues['gl_auto_advance_post_month'] ) ); elseif( isset( $arrValues['gl_auto_advance_post_month'] ) ) $this->setGlAutoAdvancePostMonth( $arrValues['gl_auto_advance_post_month'] );
		if( isset( $arrValues['allow_ap_consolidation'] ) && $boolDirectSet ) $this->set( 'm_intAllowApConsolidation', trim( $arrValues['allow_ap_consolidation'] ) ); elseif( isset( $arrValues['allow_ap_consolidation'] ) ) $this->setAllowApConsolidation( $arrValues['allow_ap_consolidation'] );
		if( isset( $arrValues['show_vendor_numbers_in_ap'] ) && $boolDirectSet ) $this->set( 'm_intShowVendorNumbersInAp', trim( $arrValues['show_vendor_numbers_in_ap'] ) ); elseif( isset( $arrValues['show_vendor_numbers_in_ap'] ) ) $this->setShowVendorNumbersInAp( $arrValues['show_vendor_numbers_in_ap'] );
		if( isset( $arrValues['show_property_codes_in_ap'] ) && $boolDirectSet ) $this->set( 'm_intShowPropertyCodesInAp', trim( $arrValues['show_property_codes_in_ap'] ) ); elseif( isset( $arrValues['show_property_codes_in_ap'] ) ) $this->setShowPropertyCodesInAp( $arrValues['show_property_codes_in_ap'] );
		if( isset( $arrValues['activate_standard_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivateStandardPosting', trim( stripcslashes( $arrValues['activate_standard_posting'] ) ) ); elseif( isset( $arrValues['activate_standard_posting'] ) ) $this->setActivateStandardPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_standard_posting'] ) : $arrValues['activate_standard_posting'] );
		if( isset( $arrValues['activate_po_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivatePoPosting', trim( stripcslashes( $arrValues['activate_po_posting'] ) ) ); elseif( isset( $arrValues['activate_po_posting'] ) ) $this->setActivatePoPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_po_posting'] ) : $arrValues['activate_po_posting'] );
		if( isset( $arrValues['activate_asset_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivateAssetPosting', trim( stripcslashes( $arrValues['activate_asset_posting'] ) ) ); elseif( isset( $arrValues['activate_asset_posting'] ) ) $this->setActivateAssetPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_asset_posting'] ) : $arrValues['activate_asset_posting'] );
		if( isset( $arrValues['is_cash_basis'] ) && $boolDirectSet ) $this->set( 'm_intIsCashBasis', trim( $arrValues['is_cash_basis'] ) ); elseif( isset( $arrValues['is_cash_basis'] ) ) $this->setIsCashBasis( $arrValues['is_cash_basis'] );
		if( isset( $arrValues['allocate_rent_first'] ) && $boolDirectSet ) $this->set( 'm_intAllocateRentFirst', trim( $arrValues['allocate_rent_first'] ) ); elseif( isset( $arrValues['allocate_rent_first'] ) ) $this->setAllocateRentFirst( $arrValues['allocate_rent_first'] );
		if( isset( $arrValues['allow_item_confidentiality'] ) && $boolDirectSet ) $this->set( 'm_boolAllowItemConfidentiality', trim( stripcslashes( $arrValues['allow_item_confidentiality'] ) ) ); elseif( isset( $arrValues['allow_item_confidentiality'] ) ) $this->setAllowItemConfidentiality( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_item_confidentiality'] ) : $arrValues['allow_item_confidentiality'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApGlAccountId( $intApGlAccountId ) {
		$this->set( 'm_intApGlAccountId', CStrings::strToIntDef( $intApGlAccountId, NULL, false ) );
	}

	public function getApGlAccountId() {
		return $this->m_intApGlAccountId;
	}

	public function sqlApGlAccountId() {
		return ( true == isset( $this->m_intApGlAccountId ) ) ? ( string ) $this->m_intApGlAccountId : 'NULL';
	}

	public function setRetainedEarningsGlAccountId( $intRetainedEarningsGlAccountId ) {
		$this->set( 'm_intRetainedEarningsGlAccountId', CStrings::strToIntDef( $intRetainedEarningsGlAccountId, NULL, false ) );
	}

	public function getRetainedEarningsGlAccountId() {
		return $this->m_intRetainedEarningsGlAccountId;
	}

	public function sqlRetainedEarningsGlAccountId() {
		return ( true == isset( $this->m_intRetainedEarningsGlAccountId ) ) ? ( string ) $this->m_intRetainedEarningsGlAccountId : 'NULL';
	}

	public function setMarketRentGlAccountId( $intMarketRentGlAccountId ) {
		$this->set( 'm_intMarketRentGlAccountId', CStrings::strToIntDef( $intMarketRentGlAccountId, NULL, false ) );
	}

	public function getMarketRentGlAccountId() {
		return $this->m_intMarketRentGlAccountId;
	}

	public function sqlMarketRentGlAccountId() {
		return ( true == isset( $this->m_intMarketRentGlAccountId ) ) ? ( string ) $this->m_intMarketRentGlAccountId : 'NULL';
	}

	public function setGainToLeaseGlAccountId( $intGainToLeaseGlAccountId ) {
		$this->set( 'm_intGainToLeaseGlAccountId', CStrings::strToIntDef( $intGainToLeaseGlAccountId, NULL, false ) );
	}

	public function getGainToLeaseGlAccountId() {
		return $this->m_intGainToLeaseGlAccountId;
	}

	public function sqlGainToLeaseGlAccountId() {
		return ( true == isset( $this->m_intGainToLeaseGlAccountId ) ) ? ( string ) $this->m_intGainToLeaseGlAccountId : 'NULL';
	}

	public function setLossToLeaseGlAccountId( $intLossToLeaseGlAccountId ) {
		$this->set( 'm_intLossToLeaseGlAccountId', CStrings::strToIntDef( $intLossToLeaseGlAccountId, NULL, false ) );
	}

	public function getLossToLeaseGlAccountId() {
		return $this->m_intLossToLeaseGlAccountId;
	}

	public function sqlLossToLeaseGlAccountId() {
		return ( true == isset( $this->m_intLossToLeaseGlAccountId ) ) ? ( string ) $this->m_intLossToLeaseGlAccountId : 'NULL';
	}

	public function setVacancyLossGlAccountId( $intVacancyLossGlAccountId ) {
		$this->set( 'm_intVacancyLossGlAccountId', CStrings::strToIntDef( $intVacancyLossGlAccountId, NULL, false ) );
	}

	public function getVacancyLossGlAccountId() {
		return $this->m_intVacancyLossGlAccountId;
	}

	public function sqlVacancyLossGlAccountId() {
		return ( true == isset( $this->m_intVacancyLossGlAccountId ) ) ? ( string ) $this->m_intVacancyLossGlAccountId : 'NULL';
	}

	public function setInterCompanyArGlAccountId( $intInterCompanyArGlAccountId ) {
		$this->set( 'm_intInterCompanyArGlAccountId', CStrings::strToIntDef( $intInterCompanyArGlAccountId, NULL, false ) );
	}

	public function getInterCompanyArGlAccountId() {
		return $this->m_intInterCompanyArGlAccountId;
	}

	public function sqlInterCompanyArGlAccountId() {
		return ( true == isset( $this->m_intInterCompanyArGlAccountId ) ) ? ( string ) $this->m_intInterCompanyArGlAccountId : 'NULL';
	}

	public function setInterCompanyApGlAccountId( $intInterCompanyApGlAccountId ) {
		$this->set( 'm_intInterCompanyApGlAccountId', CStrings::strToIntDef( $intInterCompanyApGlAccountId, NULL, false ) );
	}

	public function getInterCompanyApGlAccountId() {
		return $this->m_intInterCompanyApGlAccountId;
	}

	public function sqlInterCompanyApGlAccountId() {
		return ( true == isset( $this->m_intInterCompanyApGlAccountId ) ) ? ( string ) $this->m_intInterCompanyApGlAccountId : 'NULL';
	}

	public function setFloatingReimbursementsGlAccountId( $intFloatingReimbursementsGlAccountId ) {
		$this->set( 'm_intFloatingReimbursementsGlAccountId', CStrings::strToIntDef( $intFloatingReimbursementsGlAccountId, NULL, false ) );
	}

	public function getFloatingReimbursementsGlAccountId() {
		return $this->m_intFloatingReimbursementsGlAccountId;
	}

	public function sqlFloatingReimbursementsGlAccountId() {
		return ( true == isset( $this->m_intFloatingReimbursementsGlAccountId ) ) ? ( string ) $this->m_intFloatingReimbursementsGlAccountId : 'NULL';
	}

	public function setRetentionPayableGlAccountId( $intRetentionPayableGlAccountId ) {
		$this->set( 'm_intRetentionPayableGlAccountId', CStrings::strToIntDef( $intRetentionPayableGlAccountId, NULL, false ) );
	}

	public function getRetentionPayableGlAccountId() {
		return $this->m_intRetentionPayableGlAccountId;
	}

	public function sqlRetentionPayableGlAccountId() {
		return ( true == isset( $this->m_intRetentionPayableGlAccountId ) ) ? ( string ) $this->m_intRetentionPayableGlAccountId : 'NULL';
	}

	public function setAccelRentArCodeId( $intAccelRentArCodeId ) {
		$this->set( 'm_intAccelRentArCodeId', CStrings::strToIntDef( $intAccelRentArCodeId, NULL, false ) );
	}

	public function getAccelRentArCodeId() {
		return $this->m_intAccelRentArCodeId;
	}

	public function sqlAccelRentArCodeId() {
		return ( true == isset( $this->m_intAccelRentArCodeId ) ) ? ( string ) $this->m_intAccelRentArCodeId : 'NULL';
	}

	public function setRentArCodeId( $intRentArCodeId ) {
		$this->set( 'm_intRentArCodeId', CStrings::strToIntDef( $intRentArCodeId, NULL, false ) );
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	public function sqlRentArCodeId() {
		return ( true == isset( $this->m_intRentArCodeId ) ) ? ( string ) $this->m_intRentArCodeId : 'NULL';
	}

	public function setDepositArCodeId( $intDepositArCodeId ) {
		$this->set( 'm_intDepositArCodeId', CStrings::strToIntDef( $intDepositArCodeId, NULL, false ) );
	}

	public function getDepositArCodeId() {
		return $this->m_intDepositArCodeId;
	}

	public function sqlDepositArCodeId() {
		return ( true == isset( $this->m_intDepositArCodeId ) ) ? ( string ) $this->m_intDepositArCodeId : 'NULL';
	}

	public function setMtmRentArCodeId( $intMtmRentArCodeId ) {
		$this->set( 'm_intMtmRentArCodeId', CStrings::strToIntDef( $intMtmRentArCodeId, NULL, false ) );
	}

	public function getMtmRentArCodeId() {
		return $this->m_intMtmRentArCodeId;
	}

	public function sqlMtmRentArCodeId() {
		return ( true == isset( $this->m_intMtmRentArCodeId ) ) ? ( string ) $this->m_intMtmRentArCodeId : 'NULL';
	}

	public function setReimbursementArCodeId( $intReimbursementArCodeId ) {
		$this->set( 'm_intReimbursementArCodeId', CStrings::strToIntDef( $intReimbursementArCodeId, NULL, false ) );
	}

	public function getReimbursementArCodeId() {
		return $this->m_intReimbursementArCodeId;
	}

	public function sqlReimbursementArCodeId() {
		return ( true == isset( $this->m_intReimbursementArCodeId ) ) ? ( string ) $this->m_intReimbursementArCodeId : 'NULL';
	}

	public function setManagementFeeArCodeId( $intManagementFeeArCodeId ) {
		$this->set( 'm_intManagementFeeArCodeId', CStrings::strToIntDef( $intManagementFeeArCodeId, NULL, false ) );
	}

	public function getManagementFeeArCodeId() {
		return $this->m_intManagementFeeArCodeId;
	}

	public function sqlManagementFeeArCodeId() {
		return ( true == isset( $this->m_intManagementFeeArCodeId ) ) ? ( string ) $this->m_intManagementFeeArCodeId : 'NULL';
	}

	public function setOwnerContributionArCodeId( $intOwnerContributionArCodeId ) {
		$this->set( 'm_intOwnerContributionArCodeId', CStrings::strToIntDef( $intOwnerContributionArCodeId, NULL, false ) );
	}

	public function getOwnerContributionArCodeId() {
		return $this->m_intOwnerContributionArCodeId;
	}

	public function sqlOwnerContributionArCodeId() {
		return ( true == isset( $this->m_intOwnerContributionArCodeId ) ) ? ( string ) $this->m_intOwnerContributionArCodeId : 'NULL';
	}

	public function setLenderContributionArCodeId( $intLenderContributionArCodeId ) {
		$this->set( 'm_intLenderContributionArCodeId', CStrings::strToIntDef( $intLenderContributionArCodeId, NULL, false ) );
	}

	public function getLenderContributionArCodeId() {
		return $this->m_intLenderContributionArCodeId;
	}

	public function sqlLenderContributionArCodeId() {
		return ( true == isset( $this->m_intLenderContributionArCodeId ) ) ? ( string ) $this->m_intLenderContributionArCodeId : 'NULL';
	}

	public function setGlDimensionsAllowanceTypeId( $intGlDimensionsAllowanceTypeId ) {
		$this->set( 'm_intGlDimensionsAllowanceTypeId', CStrings::strToIntDef( $intGlDimensionsAllowanceTypeId, NULL, false ) );
	}

	public function getGlDimensionsAllowanceTypeId() {
		return $this->m_intGlDimensionsAllowanceTypeId;
	}

	public function sqlGlDimensionsAllowanceTypeId() {
		return ( true == isset( $this->m_intGlDimensionsAllowanceTypeId ) ) ? ( string ) $this->m_intGlDimensionsAllowanceTypeId : '1';
	}

	public function setDepartmentAllowanceTypeId( $intDepartmentAllowanceTypeId ) {
		$this->set( 'm_intDepartmentAllowanceTypeId', CStrings::strToIntDef( $intDepartmentAllowanceTypeId, NULL, false ) );
	}

	public function getDepartmentAllowanceTypeId() {
		return $this->m_intDepartmentAllowanceTypeId;
	}

	public function sqlDepartmentAllowanceTypeId() {
		return ( true == isset( $this->m_intDepartmentAllowanceTypeId ) ) ? ( string ) $this->m_intDepartmentAllowanceTypeId : '1';
	}

	public function setConfidentialityAllowanceTypeId( $intConfidentialityAllowanceTypeId ) {
		$this->set( 'm_intConfidentialityAllowanceTypeId', CStrings::strToIntDef( $intConfidentialityAllowanceTypeId, NULL, false ) );
	}

	public function getConfidentialityAllowanceTypeId() {
		return $this->m_intConfidentialityAllowanceTypeId;
	}

	public function sqlConfidentialityAllowanceTypeId() {
		return ( true == isset( $this->m_intConfidentialityAllowanceTypeId ) ) ? ( string ) $this->m_intConfidentialityAllowanceTypeId : '1';
	}

	public function setApMerchantAccountId( $intApMerchantAccountId ) {
		$this->set( 'm_intApMerchantAccountId', CStrings::strToIntDef( $intApMerchantAccountId, NULL, false ) );
	}

	public function getApMerchantAccountId() {
		return $this->m_intApMerchantAccountId;
	}

	public function sqlApMerchantAccountId() {
		return ( true == isset( $this->m_intApMerchantAccountId ) ) ? ( string ) $this->m_intApMerchantAccountId : 'NULL';
	}

	public function setArAdvanceDay( $intArAdvanceDay ) {
		$this->set( 'm_intArAdvanceDay', CStrings::strToIntDef( $intArAdvanceDay, NULL, false ) );
	}

	public function getArAdvanceDay() {
		return $this->m_intArAdvanceDay;
	}

	public function sqlArAdvanceDay() {
		return ( true == isset( $this->m_intArAdvanceDay ) ) ? ( string ) $this->m_intArAdvanceDay : '25';
	}

	public function setApAdvanceDay( $intApAdvanceDay ) {
		$this->set( 'm_intApAdvanceDay', CStrings::strToIntDef( $intApAdvanceDay, NULL, false ) );
	}

	public function getApAdvanceDay() {
		return $this->m_intApAdvanceDay;
	}

	public function sqlApAdvanceDay() {
		return ( true == isset( $this->m_intApAdvanceDay ) ) ? ( string ) $this->m_intApAdvanceDay : '25';
	}

	public function setGlAdvanceDay( $intGlAdvanceDay ) {
		$this->set( 'm_intGlAdvanceDay', CStrings::strToIntDef( $intGlAdvanceDay, NULL, false ) );
	}

	public function getGlAdvanceDay() {
		return $this->m_intGlAdvanceDay;
	}

	public function sqlGlAdvanceDay() {
		return ( true == isset( $this->m_intGlAdvanceDay ) ) ? ( string ) $this->m_intGlAdvanceDay : '25';
	}

	public function setIgnoreHolidaysAndWeekends( $intIgnoreHolidaysAndWeekends ) {
		$this->set( 'm_intIgnoreHolidaysAndWeekends', CStrings::strToIntDef( $intIgnoreHolidaysAndWeekends, NULL, false ) );
	}

	public function getIgnoreHolidaysAndWeekends() {
		return $this->m_intIgnoreHolidaysAndWeekends;
	}

	public function sqlIgnoreHolidaysAndWeekends() {
		return ( true == isset( $this->m_intIgnoreHolidaysAndWeekends ) ) ? ( string ) $this->m_intIgnoreHolidaysAndWeekends : '0';
	}

	public function setAutoPostRefundInvoice( $intAutoPostRefundInvoice ) {
		$this->set( 'm_intAutoPostRefundInvoice', CStrings::strToIntDef( $intAutoPostRefundInvoice, NULL, false ) );
	}

	public function getAutoPostRefundInvoice() {
		return $this->m_intAutoPostRefundInvoice;
	}

	public function sqlAutoPostRefundInvoice() {
		return ( true == isset( $this->m_intAutoPostRefundInvoice ) ) ? ( string ) $this->m_intAutoPostRefundInvoice : '0';
	}

	public function setArAutoAdvancePostMonth( $intArAutoAdvancePostMonth ) {
		$this->set( 'm_intArAutoAdvancePostMonth', CStrings::strToIntDef( $intArAutoAdvancePostMonth, NULL, false ) );
	}

	public function getArAutoAdvancePostMonth() {
		return $this->m_intArAutoAdvancePostMonth;
	}

	public function sqlArAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intArAutoAdvancePostMonth ) ) ? ( string ) $this->m_intArAutoAdvancePostMonth : '0';
	}

	public function setApAutoAdvancePostMonth( $intApAutoAdvancePostMonth ) {
		$this->set( 'm_intApAutoAdvancePostMonth', CStrings::strToIntDef( $intApAutoAdvancePostMonth, NULL, false ) );
	}

	public function getApAutoAdvancePostMonth() {
		return $this->m_intApAutoAdvancePostMonth;
	}

	public function sqlApAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intApAutoAdvancePostMonth ) ) ? ( string ) $this->m_intApAutoAdvancePostMonth : '0';
	}

	public function setGlAutoAdvancePostMonth( $intGlAutoAdvancePostMonth ) {
		$this->set( 'm_intGlAutoAdvancePostMonth', CStrings::strToIntDef( $intGlAutoAdvancePostMonth, NULL, false ) );
	}

	public function getGlAutoAdvancePostMonth() {
		return $this->m_intGlAutoAdvancePostMonth;
	}

	public function sqlGlAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intGlAutoAdvancePostMonth ) ) ? ( string ) $this->m_intGlAutoAdvancePostMonth : '0';
	}

	public function setAllowApConsolidation( $intAllowApConsolidation ) {
		$this->set( 'm_intAllowApConsolidation', CStrings::strToIntDef( $intAllowApConsolidation, NULL, false ) );
	}

	public function getAllowApConsolidation() {
		return $this->m_intAllowApConsolidation;
	}

	public function sqlAllowApConsolidation() {
		return ( true == isset( $this->m_intAllowApConsolidation ) ) ? ( string ) $this->m_intAllowApConsolidation : '1';
	}

	public function setShowVendorNumbersInAp( $intShowVendorNumbersInAp ) {
		$this->set( 'm_intShowVendorNumbersInAp', CStrings::strToIntDef( $intShowVendorNumbersInAp, NULL, false ) );
	}

	public function getShowVendorNumbersInAp() {
		return $this->m_intShowVendorNumbersInAp;
	}

	public function sqlShowVendorNumbersInAp() {
		return ( true == isset( $this->m_intShowVendorNumbersInAp ) ) ? ( string ) $this->m_intShowVendorNumbersInAp : '0';
	}

	public function setShowPropertyCodesInAp( $intShowPropertyCodesInAp ) {
		$this->set( 'm_intShowPropertyCodesInAp', CStrings::strToIntDef( $intShowPropertyCodesInAp, NULL, false ) );
	}

	public function getShowPropertyCodesInAp() {
		return $this->m_intShowPropertyCodesInAp;
	}

	public function sqlShowPropertyCodesInAp() {
		return ( true == isset( $this->m_intShowPropertyCodesInAp ) ) ? ( string ) $this->m_intShowPropertyCodesInAp : '0';
	}

	public function setActivateStandardPosting( $boolActivateStandardPosting ) {
		$this->set( 'm_boolActivateStandardPosting', CStrings::strToBool( $boolActivateStandardPosting ) );
	}

	public function getActivateStandardPosting() {
		return $this->m_boolActivateStandardPosting;
	}

	public function sqlActivateStandardPosting() {
		return ( true == isset( $this->m_boolActivateStandardPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivateStandardPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setActivatePoPosting( $boolActivatePoPosting ) {
		$this->set( 'm_boolActivatePoPosting', CStrings::strToBool( $boolActivatePoPosting ) );
	}

	public function getActivatePoPosting() {
		return $this->m_boolActivatePoPosting;
	}

	public function sqlActivatePoPosting() {
		return ( true == isset( $this->m_boolActivatePoPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivatePoPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setActivateAssetPosting( $boolActivateAssetPosting ) {
		$this->set( 'm_boolActivateAssetPosting', CStrings::strToBool( $boolActivateAssetPosting ) );
	}

	public function getActivateAssetPosting() {
		return $this->m_boolActivateAssetPosting;
	}

	public function sqlActivateAssetPosting() {
		return ( true == isset( $this->m_boolActivateAssetPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivateAssetPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCashBasis( $intIsCashBasis ) {
		$this->set( 'm_intIsCashBasis', CStrings::strToIntDef( $intIsCashBasis, NULL, false ) );
	}

	public function getIsCashBasis() {
		return $this->m_intIsCashBasis;
	}

	public function sqlIsCashBasis() {
		return ( true == isset( $this->m_intIsCashBasis ) ) ? ( string ) $this->m_intIsCashBasis : '0';
	}

	public function setAllocateRentFirst( $intAllocateRentFirst ) {
		$this->set( 'm_intAllocateRentFirst', CStrings::strToIntDef( $intAllocateRentFirst, NULL, false ) );
	}

	public function getAllocateRentFirst() {
		return $this->m_intAllocateRentFirst;
	}

	public function sqlAllocateRentFirst() {
		return ( true == isset( $this->m_intAllocateRentFirst ) ) ? ( string ) $this->m_intAllocateRentFirst : '0';
	}

	public function setAllowItemConfidentiality( $boolAllowItemConfidentiality ) {
		$this->set( 'm_boolAllowItemConfidentiality', CStrings::strToBool( $boolAllowItemConfidentiality ) );
	}

	public function getAllowItemConfidentiality() {
		return $this->m_boolAllowItemConfidentiality;
	}

	public function sqlAllowItemConfidentiality() {
		return ( true == isset( $this->m_boolAllowItemConfidentiality ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowItemConfidentiality ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_gl_account_id, retained_earnings_gl_account_id, market_rent_gl_account_id, gain_to_lease_gl_account_id, loss_to_lease_gl_account_id, vacancy_loss_gl_account_id, inter_company_ar_gl_account_id, inter_company_ap_gl_account_id, floating_reimbursements_gl_account_id, retention_payable_gl_account_id, accel_rent_ar_code_id, rent_ar_code_id, deposit_ar_code_id, mtm_rent_ar_code_id, reimbursement_ar_code_id, management_fee_ar_code_id, owner_contribution_ar_code_id, lender_contribution_ar_code_id, gl_dimensions_allowance_type_id, department_allowance_type_id, confidentiality_allowance_type_id, ap_merchant_account_id, ar_advance_day, ap_advance_day, gl_advance_day, ignore_holidays_and_weekends, auto_post_refund_invoice, ar_auto_advance_post_month, ap_auto_advance_post_month, gl_auto_advance_post_month, allow_ap_consolidation, show_vendor_numbers_in_ap, show_property_codes_in_ap, activate_standard_posting, activate_po_posting, activate_asset_posting, is_cash_basis, allocate_rent_first, allow_item_confidentiality, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApGlAccountId() . ', ' .
						$this->sqlRetainedEarningsGlAccountId() . ', ' .
						$this->sqlMarketRentGlAccountId() . ', ' .
						$this->sqlGainToLeaseGlAccountId() . ', ' .
						$this->sqlLossToLeaseGlAccountId() . ', ' .
						$this->sqlVacancyLossGlAccountId() . ', ' .
						$this->sqlInterCompanyArGlAccountId() . ', ' .
						$this->sqlInterCompanyApGlAccountId() . ', ' .
						$this->sqlFloatingReimbursementsGlAccountId() . ', ' .
						$this->sqlRetentionPayableGlAccountId() . ', ' .
						$this->sqlAccelRentArCodeId() . ', ' .
						$this->sqlRentArCodeId() . ', ' .
						$this->sqlDepositArCodeId() . ', ' .
						$this->sqlMtmRentArCodeId() . ', ' .
						$this->sqlReimbursementArCodeId() . ', ' .
						$this->sqlManagementFeeArCodeId() . ', ' .
						$this->sqlOwnerContributionArCodeId() . ', ' .
						$this->sqlLenderContributionArCodeId() . ', ' .
						$this->sqlGlDimensionsAllowanceTypeId() . ', ' .
						$this->sqlDepartmentAllowanceTypeId() . ', ' .
						$this->sqlConfidentialityAllowanceTypeId() . ', ' .
						$this->sqlApMerchantAccountId() . ', ' .
						$this->sqlArAdvanceDay() . ', ' .
						$this->sqlApAdvanceDay() . ', ' .
						$this->sqlGlAdvanceDay() . ', ' .
						$this->sqlIgnoreHolidaysAndWeekends() . ', ' .
						$this->sqlAutoPostRefundInvoice() . ', ' .
						$this->sqlArAutoAdvancePostMonth() . ', ' .
						$this->sqlApAutoAdvancePostMonth() . ', ' .
						$this->sqlGlAutoAdvancePostMonth() . ', ' .
						$this->sqlAllowApConsolidation() . ', ' .
						$this->sqlShowVendorNumbersInAp() . ', ' .
						$this->sqlShowPropertyCodesInAp() . ', ' .
						$this->sqlActivateStandardPosting() . ', ' .
						$this->sqlActivatePoPosting() . ', ' .
						$this->sqlActivateAssetPosting() . ', ' .
						$this->sqlIsCashBasis() . ', ' .
						$this->sqlAllocateRentFirst() . ', ' .
						$this->sqlAllowItemConfidentiality() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retained_earnings_gl_account_id = ' . $this->sqlRetainedEarningsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'RetainedEarningsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' retained_earnings_gl_account_id = ' . $this->sqlRetainedEarningsGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_gl_account_id = ' . $this->sqlMarketRentGlAccountId(). ',' ; } elseif( true == array_key_exists( 'MarketRentGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_gl_account_id = ' . $this->sqlMarketRentGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gain_to_lease_gl_account_id = ' . $this->sqlGainToLeaseGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GainToLeaseGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gain_to_lease_gl_account_id = ' . $this->sqlGainToLeaseGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_to_lease_gl_account_id = ' . $this->sqlLossToLeaseGlAccountId(). ',' ; } elseif( true == array_key_exists( 'LossToLeaseGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' loss_to_lease_gl_account_id = ' . $this->sqlLossToLeaseGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_loss_gl_account_id = ' . $this->sqlVacancyLossGlAccountId(). ',' ; } elseif( true == array_key_exists( 'VacancyLossGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_loss_gl_account_id = ' . $this->sqlVacancyLossGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_company_ar_gl_account_id = ' . $this->sqlInterCompanyArGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCompanyArGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_company_ar_gl_account_id = ' . $this->sqlInterCompanyArGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_company_ap_gl_account_id = ' . $this->sqlInterCompanyApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCompanyApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_company_ap_gl_account_id = ' . $this->sqlInterCompanyApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floating_reimbursements_gl_account_id = ' . $this->sqlFloatingReimbursementsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'FloatingReimbursementsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' floating_reimbursements_gl_account_id = ' . $this->sqlFloatingReimbursementsGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retention_payable_gl_account_id = ' . $this->sqlRetentionPayableGlAccountId(). ',' ; } elseif( true == array_key_exists( 'RetentionPayableGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' retention_payable_gl_account_id = ' . $this->sqlRetentionPayableGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accel_rent_ar_code_id = ' . $this->sqlAccelRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'AccelRentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' accel_rent_ar_code_id = ' . $this->sqlAccelRentArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'RentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId(). ',' ; } elseif( true == array_key_exists( 'DepositArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mtm_rent_ar_code_id = ' . $this->sqlMtmRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'MtmRentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' mtm_rent_ar_code_id = ' . $this->sqlMtmRentArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_ar_code_id = ' . $this->sqlReimbursementArCodeId(). ',' ; } elseif( true == array_key_exists( 'ReimbursementArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_ar_code_id = ' . $this->sqlReimbursementArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' management_fee_ar_code_id = ' . $this->sqlManagementFeeArCodeId(). ',' ; } elseif( true == array_key_exists( 'ManagementFeeArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' management_fee_ar_code_id = ' . $this->sqlManagementFeeArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_contribution_ar_code_id = ' . $this->sqlOwnerContributionArCodeId(). ',' ; } elseif( true == array_key_exists( 'OwnerContributionArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' owner_contribution_ar_code_id = ' . $this->sqlOwnerContributionArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lender_contribution_ar_code_id = ' . $this->sqlLenderContributionArCodeId(). ',' ; } elseif( true == array_key_exists( 'LenderContributionArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' lender_contribution_ar_code_id = ' . $this->sqlLenderContributionArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_dimensions_allowance_type_id = ' . $this->sqlGlDimensionsAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'GlDimensionsAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_dimensions_allowance_type_id = ' . $this->sqlGlDimensionsAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_allowance_type_id = ' . $this->sqlDepartmentAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'DepartmentAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' department_allowance_type_id = ' . $this->sqlDepartmentAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confidentiality_allowance_type_id = ' . $this->sqlConfidentialityAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'ConfidentialityAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' confidentiality_allowance_type_id = ' . $this->sqlConfidentialityAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_merchant_account_id = ' . $this->sqlApMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'ApMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_merchant_account_id = ' . $this->sqlApMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'ArAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'ApAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'GlAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_holidays_and_weekends = ' . $this->sqlIgnoreHolidaysAndWeekends(). ',' ; } elseif( true == array_key_exists( 'IgnoreHolidaysAndWeekends', $this->getChangedColumns() ) ) { $strSql .= ' ignore_holidays_and_weekends = ' . $this->sqlIgnoreHolidaysAndWeekends() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_refund_invoice = ' . $this->sqlAutoPostRefundInvoice(). ',' ; } elseif( true == array_key_exists( 'AutoPostRefundInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_refund_invoice = ' . $this->sqlAutoPostRefundInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_auto_advance_post_month = ' . $this->sqlArAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'ArAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ar_auto_advance_post_month = ' . $this->sqlArAutoAdvancePostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_auto_advance_post_month = ' . $this->sqlApAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'ApAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ap_auto_advance_post_month = ' . $this->sqlApAutoAdvancePostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_auto_advance_post_month = ' . $this->sqlGlAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'GlAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' gl_auto_advance_post_month = ' . $this->sqlGlAutoAdvancePostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_ap_consolidation = ' . $this->sqlAllowApConsolidation(). ',' ; } elseif( true == array_key_exists( 'AllowApConsolidation', $this->getChangedColumns() ) ) { $strSql .= ' allow_ap_consolidation = ' . $this->sqlAllowApConsolidation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_vendor_numbers_in_ap = ' . $this->sqlShowVendorNumbersInAp(). ',' ; } elseif( true == array_key_exists( 'ShowVendorNumbersInAp', $this->getChangedColumns() ) ) { $strSql .= ' show_vendor_numbers_in_ap = ' . $this->sqlShowVendorNumbersInAp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_property_codes_in_ap = ' . $this->sqlShowPropertyCodesInAp(). ',' ; } elseif( true == array_key_exists( 'ShowPropertyCodesInAp', $this->getChangedColumns() ) ) { $strSql .= ' show_property_codes_in_ap = ' . $this->sqlShowPropertyCodesInAp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_standard_posting = ' . $this->sqlActivateStandardPosting(). ',' ; } elseif( true == array_key_exists( 'ActivateStandardPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_standard_posting = ' . $this->sqlActivateStandardPosting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_po_posting = ' . $this->sqlActivatePoPosting(). ',' ; } elseif( true == array_key_exists( 'ActivatePoPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_po_posting = ' . $this->sqlActivatePoPosting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_asset_posting = ' . $this->sqlActivateAssetPosting(). ',' ; } elseif( true == array_key_exists( 'ActivateAssetPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_asset_posting = ' . $this->sqlActivateAssetPosting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis(). ',' ; } elseif( true == array_key_exists( 'IsCashBasis', $this->getChangedColumns() ) ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocate_rent_first = ' . $this->sqlAllocateRentFirst(). ',' ; } elseif( true == array_key_exists( 'AllocateRentFirst', $this->getChangedColumns() ) ) { $strSql .= ' allocate_rent_first = ' . $this->sqlAllocateRentFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_item_confidentiality = ' . $this->sqlAllowItemConfidentiality(). ',' ; } elseif( true == array_key_exists( 'AllowItemConfidentiality', $this->getChangedColumns() ) ) { $strSql .= ' allow_item_confidentiality = ' . $this->sqlAllowItemConfidentiality() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_gl_account_id' => $this->getApGlAccountId(),
			'retained_earnings_gl_account_id' => $this->getRetainedEarningsGlAccountId(),
			'market_rent_gl_account_id' => $this->getMarketRentGlAccountId(),
			'gain_to_lease_gl_account_id' => $this->getGainToLeaseGlAccountId(),
			'loss_to_lease_gl_account_id' => $this->getLossToLeaseGlAccountId(),
			'vacancy_loss_gl_account_id' => $this->getVacancyLossGlAccountId(),
			'inter_company_ar_gl_account_id' => $this->getInterCompanyArGlAccountId(),
			'inter_company_ap_gl_account_id' => $this->getInterCompanyApGlAccountId(),
			'floating_reimbursements_gl_account_id' => $this->getFloatingReimbursementsGlAccountId(),
			'retention_payable_gl_account_id' => $this->getRetentionPayableGlAccountId(),
			'accel_rent_ar_code_id' => $this->getAccelRentArCodeId(),
			'rent_ar_code_id' => $this->getRentArCodeId(),
			'deposit_ar_code_id' => $this->getDepositArCodeId(),
			'mtm_rent_ar_code_id' => $this->getMtmRentArCodeId(),
			'reimbursement_ar_code_id' => $this->getReimbursementArCodeId(),
			'management_fee_ar_code_id' => $this->getManagementFeeArCodeId(),
			'owner_contribution_ar_code_id' => $this->getOwnerContributionArCodeId(),
			'lender_contribution_ar_code_id' => $this->getLenderContributionArCodeId(),
			'gl_dimensions_allowance_type_id' => $this->getGlDimensionsAllowanceTypeId(),
			'department_allowance_type_id' => $this->getDepartmentAllowanceTypeId(),
			'confidentiality_allowance_type_id' => $this->getConfidentialityAllowanceTypeId(),
			'ap_merchant_account_id' => $this->getApMerchantAccountId(),
			'ar_advance_day' => $this->getArAdvanceDay(),
			'ap_advance_day' => $this->getApAdvanceDay(),
			'gl_advance_day' => $this->getGlAdvanceDay(),
			'ignore_holidays_and_weekends' => $this->getIgnoreHolidaysAndWeekends(),
			'auto_post_refund_invoice' => $this->getAutoPostRefundInvoice(),
			'ar_auto_advance_post_month' => $this->getArAutoAdvancePostMonth(),
			'ap_auto_advance_post_month' => $this->getApAutoAdvancePostMonth(),
			'gl_auto_advance_post_month' => $this->getGlAutoAdvancePostMonth(),
			'allow_ap_consolidation' => $this->getAllowApConsolidation(),
			'show_vendor_numbers_in_ap' => $this->getShowVendorNumbersInAp(),
			'show_property_codes_in_ap' => $this->getShowPropertyCodesInAp(),
			'activate_standard_posting' => $this->getActivateStandardPosting(),
			'activate_po_posting' => $this->getActivatePoPosting(),
			'activate_asset_posting' => $this->getActivateAssetPosting(),
			'is_cash_basis' => $this->getIsCashBasis(),
			'allocate_rent_first' => $this->getAllocateRentFirst(),
			'allow_item_confidentiality' => $this->getAllowItemConfidentiality(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>