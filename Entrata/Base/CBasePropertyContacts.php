<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyContacts extends CEosPluralBase {

	/**
	 * @return CPropertyContact[]
	 */
	public static function fetchPropertyContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyContact::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyContact
	 */
	public static function fetchPropertyContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyContact::class, $objDatabase );
	}

	public static function fetchPropertyContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_contacts', $objDatabase );
	}

	public static function fetchPropertyContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyContact( sprintf( 'SELECT * FROM property_contacts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyContactsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyContacts( sprintf( 'SELECT * FROM property_contacts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyContactsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyContacts( sprintf( 'SELECT * FROM property_contacts WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyContactsByPropertyContactTypeIdByCid( $intPropertyContactTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyContacts( sprintf( 'SELECT * FROM property_contacts WHERE property_contact_type_id = %d AND cid = %d', $intPropertyContactTypeId, $intCid ), $objDatabase );
	}

}
?>