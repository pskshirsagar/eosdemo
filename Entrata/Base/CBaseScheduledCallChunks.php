<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallChunks
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCallChunks extends CEosPluralBase {

	/**
	 * @return CScheduledCallChunk[]
	 */
	public static function fetchScheduledCallChunks( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledCallChunk', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledCallChunk
	 */
	public static function fetchScheduledCallChunk( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledCallChunk', $objDatabase );
	}

	public static function fetchScheduledCallChunkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_call_chunks', $objDatabase );
	}

	public static function fetchScheduledCallChunkByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallChunk( sprintf( 'SELECT * FROM scheduled_call_chunks WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallChunksByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledCallChunks( sprintf( 'SELECT * FROM scheduled_call_chunks WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallChunksByScheduledCallIdByCid( $intScheduledCallId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallChunks( sprintf( 'SELECT * FROM scheduled_call_chunks WHERE scheduled_call_id = %d AND cid = %d', ( int ) $intScheduledCallId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallChunksByCallChunkTypeIdByCid( $intCallChunkTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallChunks( sprintf( 'SELECT * FROM scheduled_call_chunks WHERE call_chunk_type_id = %d AND cid = %d', ( int ) $intCallChunkTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallChunksByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallChunks( sprintf( 'SELECT * FROM scheduled_call_chunks WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>