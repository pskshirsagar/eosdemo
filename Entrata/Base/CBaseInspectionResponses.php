<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionResponses extends CEosPluralBase {

	/**
	 * @return CInspectionResponse[]
	 */
	public static function fetchInspectionResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CInspectionResponse', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionResponse
	 */
	public static function fetchInspectionResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInspectionResponse', $objDatabase );
	}

	public static function fetchInspectionResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_responses', $objDatabase );
	}

	public static function fetchInspectionResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponse( sprintf( 'SELECT * FROM inspection_responses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByInspectionIdByCid( $intInspectionId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE inspection_id = %d AND cid = %d', ( int ) $intInspectionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByInspectionFormLocationProblemIdByCid( $intInspectionFormLocationProblemId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE inspection_form_location_problem_id = %d AND cid = %d', ( int ) $intInspectionFormLocationProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByPropertyUnitMaintenanceLocationIdByCid( $intPropertyUnitMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE property_unit_maintenance_location_id = %d AND cid = %d', ( int ) $intPropertyUnitMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInspectionResponsesByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchInspectionResponses( sprintf( 'SELECT * FROM inspection_responses WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

}
?>