<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNoticeBatchDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.notice_batch_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intNoticeBatchId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intNoticeDeliveryTypeId;
	protected $m_intDelinquencyPolicyId;
	protected $m_intDelinquencyPolicyDocumentId;
	protected $m_intFileId;
	protected $m_intDocumentId;
	protected $m_intLateNoticeTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intSystemEmailId;
	protected $m_strCustomerEmailAddress;
	protected $m_intEmailEventTypeId;
	protected $m_strSystemEmailResponse;
	protected $m_intIsArchived;
	protected $m_intIsHandDelivered;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsArchived = '0';
		$this->m_intIsHandDelivered = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['notice_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intNoticeBatchId', trim( $arrValues['notice_batch_id'] ) ); elseif( isset( $arrValues['notice_batch_id'] ) ) $this->setNoticeBatchId( $arrValues['notice_batch_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['notice_delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNoticeDeliveryTypeId', trim( $arrValues['notice_delivery_type_id'] ) ); elseif( isset( $arrValues['notice_delivery_type_id'] ) ) $this->setNoticeDeliveryTypeId( $arrValues['notice_delivery_type_id'] );
		if( isset( $arrValues['delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPolicyId', trim( $arrValues['delinquency_policy_id'] ) ); elseif( isset( $arrValues['delinquency_policy_id'] ) ) $this->setDelinquencyPolicyId( $arrValues['delinquency_policy_id'] );
		if( isset( $arrValues['delinquency_policy_document_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPolicyDocumentId', trim( $arrValues['delinquency_policy_document_id'] ) ); elseif( isset( $arrValues['delinquency_policy_document_id'] ) ) $this->setDelinquencyPolicyDocumentId( $arrValues['delinquency_policy_document_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['late_notice_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLateNoticeTypeId', trim( $arrValues['late_notice_type_id'] ) ); elseif( isset( $arrValues['late_notice_type_id'] ) ) $this->setLateNoticeTypeId( $arrValues['late_notice_type_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['customer_email_address'] ) && $boolDirectSet ) $this->set( 'm_strCustomerEmailAddress', trim( stripcslashes( $arrValues['customer_email_address'] ) ) ); elseif( isset( $arrValues['customer_email_address'] ) ) $this->setCustomerEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_email_address'] ) : $arrValues['customer_email_address'] );
		if( isset( $arrValues['email_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailEventTypeId', trim( $arrValues['email_event_type_id'] ) ); elseif( isset( $arrValues['email_event_type_id'] ) ) $this->setEmailEventTypeId( $arrValues['email_event_type_id'] );
		if( isset( $arrValues['system_email_response'] ) && $boolDirectSet ) $this->set( 'm_strSystemEmailResponse', trim( stripcslashes( $arrValues['system_email_response'] ) ) ); elseif( isset( $arrValues['system_email_response'] ) ) $this->setSystemEmailResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_email_response'] ) : $arrValues['system_email_response'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['is_hand_delivered'] ) && $boolDirectSet ) $this->set( 'm_intIsHandDelivered', trim( $arrValues['is_hand_delivered'] ) ); elseif( isset( $arrValues['is_hand_delivered'] ) ) $this->setIsHandDelivered( $arrValues['is_hand_delivered'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setNoticeBatchId( $intNoticeBatchId ) {
		$this->set( 'm_intNoticeBatchId', CStrings::strToIntDef( $intNoticeBatchId, NULL, false ) );
	}

	public function getNoticeBatchId() {
		return $this->m_intNoticeBatchId;
	}

	public function sqlNoticeBatchId() {
		return ( true == isset( $this->m_intNoticeBatchId ) ) ? ( string ) $this->m_intNoticeBatchId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setNoticeDeliveryTypeId( $intNoticeDeliveryTypeId ) {
		$this->set( 'm_intNoticeDeliveryTypeId', CStrings::strToIntDef( $intNoticeDeliveryTypeId, NULL, false ) );
	}

	public function getNoticeDeliveryTypeId() {
		return $this->m_intNoticeDeliveryTypeId;
	}

	public function sqlNoticeDeliveryTypeId() {
		return ( true == isset( $this->m_intNoticeDeliveryTypeId ) ) ? ( string ) $this->m_intNoticeDeliveryTypeId : 'NULL';
	}

	public function setDelinquencyPolicyId( $intDelinquencyPolicyId ) {
		$this->set( 'm_intDelinquencyPolicyId', CStrings::strToIntDef( $intDelinquencyPolicyId, NULL, false ) );
	}

	public function getDelinquencyPolicyId() {
		return $this->m_intDelinquencyPolicyId;
	}

	public function sqlDelinquencyPolicyId() {
		return ( true == isset( $this->m_intDelinquencyPolicyId ) ) ? ( string ) $this->m_intDelinquencyPolicyId : 'NULL';
	}

	public function setDelinquencyPolicyDocumentId( $intDelinquencyPolicyDocumentId ) {
		$this->set( 'm_intDelinquencyPolicyDocumentId', CStrings::strToIntDef( $intDelinquencyPolicyDocumentId, NULL, false ) );
	}

	public function getDelinquencyPolicyDocumentId() {
		return $this->m_intDelinquencyPolicyDocumentId;
	}

	public function sqlDelinquencyPolicyDocumentId() {
		return ( true == isset( $this->m_intDelinquencyPolicyDocumentId ) ) ? ( string ) $this->m_intDelinquencyPolicyDocumentId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setLateNoticeTypeId( $intLateNoticeTypeId ) {
		$this->set( 'm_intLateNoticeTypeId', CStrings::strToIntDef( $intLateNoticeTypeId, NULL, false ) );
	}

	public function getLateNoticeTypeId() {
		return $this->m_intLateNoticeTypeId;
	}

	public function sqlLateNoticeTypeId() {
		return ( true == isset( $this->m_intLateNoticeTypeId ) ) ? ( string ) $this->m_intLateNoticeTypeId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
		$this->set( 'm_strCustomerEmailAddress', CStrings::strTrimDef( $strCustomerEmailAddress, 100, NULL, true ) );
	}

	public function getCustomerEmailAddress() {
		return $this->m_strCustomerEmailAddress;
	}

	public function sqlCustomerEmailAddress() {
		return ( true == isset( $this->m_strCustomerEmailAddress ) ) ? '\'' . addslashes( $this->m_strCustomerEmailAddress ) . '\'' : 'NULL';
	}

	public function setEmailEventTypeId( $intEmailEventTypeId ) {
		$this->set( 'm_intEmailEventTypeId', CStrings::strToIntDef( $intEmailEventTypeId, NULL, false ) );
	}

	public function getEmailEventTypeId() {
		return $this->m_intEmailEventTypeId;
	}

	public function sqlEmailEventTypeId() {
		return ( true == isset( $this->m_intEmailEventTypeId ) ) ? ( string ) $this->m_intEmailEventTypeId : 'NULL';
	}

	public function setSystemEmailResponse( $strSystemEmailResponse ) {
		$this->set( 'm_strSystemEmailResponse', CStrings::strTrimDef( $strSystemEmailResponse, -1, NULL, true ) );
	}

	public function getSystemEmailResponse() {
		return $this->m_strSystemEmailResponse;
	}

	public function sqlSystemEmailResponse() {
		return ( true == isset( $this->m_strSystemEmailResponse ) ) ? '\'' . addslashes( $this->m_strSystemEmailResponse ) . '\'' : 'NULL';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : '0';
	}

	public function setIsHandDelivered( $intIsHandDelivered ) {
		$this->set( 'm_intIsHandDelivered', CStrings::strToIntDef( $intIsHandDelivered, NULL, false ) );
	}

	public function getIsHandDelivered() {
		return $this->m_intIsHandDelivered;
	}

	public function sqlIsHandDelivered() {
		return ( true == isset( $this->m_intIsHandDelivered ) ) ? ( string ) $this->m_intIsHandDelivered : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, notice_batch_id, lease_id, customer_id, notice_delivery_type_id, delinquency_policy_id, delinquency_policy_document_id, file_id, document_id, late_notice_type_id, customer_type_id, system_email_id, customer_email_address, email_event_type_id, system_email_response, is_archived, is_hand_delivered, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlNoticeBatchId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlNoticeDeliveryTypeId() . ', ' .
 						$this->sqlDelinquencyPolicyId() . ', ' .
 						$this->sqlDelinquencyPolicyDocumentId() . ', ' .
 						$this->sqlFileId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlLateNoticeTypeId() . ', ' .
 						$this->sqlCustomerTypeId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlCustomerEmailAddress() . ', ' .
 						$this->sqlEmailEventTypeId() . ', ' .
 						$this->sqlSystemEmailResponse() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlIsHandDelivered() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_batch_id = ' . $this->sqlNoticeBatchId() . ','; } elseif( true == array_key_exists( 'NoticeBatchId', $this->getChangedColumns() ) ) { $strSql .= ' notice_batch_id = ' . $this->sqlNoticeBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_delivery_type_id = ' . $this->sqlNoticeDeliveryTypeId() . ','; } elseif( true == array_key_exists( 'NoticeDeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' notice_delivery_type_id = ' . $this->sqlNoticeDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId() . ','; } elseif( true == array_key_exists( 'DelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_policy_id = ' . $this->sqlDelinquencyPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_policy_document_id = ' . $this->sqlDelinquencyPolicyDocumentId() . ','; } elseif( true == array_key_exists( 'DelinquencyPolicyDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_policy_document_id = ' . $this->sqlDelinquencyPolicyDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_notice_type_id = ' . $this->sqlLateNoticeTypeId() . ','; } elseif( true == array_key_exists( 'LateNoticeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' late_notice_type_id = ' . $this->sqlLateNoticeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_email_address = ' . $this->sqlCustomerEmailAddress() . ','; } elseif( true == array_key_exists( 'CustomerEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' customer_email_address = ' . $this->sqlCustomerEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_event_type_id = ' . $this->sqlEmailEventTypeId() . ','; } elseif( true == array_key_exists( 'EmailEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' email_event_type_id = ' . $this->sqlEmailEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_response = ' . $this->sqlSystemEmailResponse() . ','; } elseif( true == array_key_exists( 'SystemEmailResponse', $this->getChangedColumns() ) ) { $strSql .= ' system_email_response = ' . $this->sqlSystemEmailResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hand_delivered = ' . $this->sqlIsHandDelivered() . ','; } elseif( true == array_key_exists( 'IsHandDelivered', $this->getChangedColumns() ) ) { $strSql .= ' is_hand_delivered = ' . $this->sqlIsHandDelivered() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'notice_batch_id' => $this->getNoticeBatchId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'notice_delivery_type_id' => $this->getNoticeDeliveryTypeId(),
			'delinquency_policy_id' => $this->getDelinquencyPolicyId(),
			'delinquency_policy_document_id' => $this->getDelinquencyPolicyDocumentId(),
			'file_id' => $this->getFileId(),
			'document_id' => $this->getDocumentId(),
			'late_notice_type_id' => $this->getLateNoticeTypeId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'system_email_id' => $this->getSystemEmailId(),
			'customer_email_address' => $this->getCustomerEmailAddress(),
			'email_event_type_id' => $this->getEmailEventTypeId(),
			'system_email_response' => $this->getSystemEmailResponse(),
			'is_archived' => $this->getIsArchived(),
			'is_hand_delivered' => $this->getIsHandDelivered(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>