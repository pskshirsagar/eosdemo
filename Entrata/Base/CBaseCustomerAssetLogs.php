<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssetLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAssetLogs extends CEosPluralBase {

	/**
	 * @return CCustomerAssetLog[]
	 */
	public static function fetchCustomerAssetLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerAssetLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAssetLog
	 */
	public static function fetchCustomerAssetLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAssetLog::class, $objDatabase );
	}

	public static function fetchCustomerAssetLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_asset_logs', $objDatabase );
	}

	public static function fetchCustomerAssetLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLog( sprintf( 'SELECT * FROM customer_asset_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByCustomerAssetIdByCid( $intCustomerAssetId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE customer_asset_id = %d AND cid = %d', $intCustomerAssetId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByCustomerAssetTypeIdByCid( $intCustomerAssetTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE customer_asset_type_id = %d AND cid = %d', $intCustomerAssetTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetLogsByPriorCustomerAssetLogIdByCid( $intPriorCustomerAssetLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssetLogs( sprintf( 'SELECT * FROM customer_asset_logs WHERE prior_customer_asset_log_id = %d AND cid = %d', $intPriorCustomerAssetLogId, $intCid ), $objDatabase );
	}

}
?>