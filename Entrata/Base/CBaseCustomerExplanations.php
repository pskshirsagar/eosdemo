<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExplanations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerExplanations extends CEosPluralBase {

	/**
	 * @return CCustomerExplanation[]
	 */
	public static function fetchCustomerExplanations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerExplanation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerExplanation
	 */
	public static function fetchCustomerExplanation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerExplanation', $objDatabase );
	}

	public static function fetchCustomerExplanationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_explanations', $objDatabase );
	}

	public static function fetchCustomerExplanationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerExplanation( sprintf( 'SELECT * FROM customer_explanations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerExplanationsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerExplanations( sprintf( 'SELECT * FROM customer_explanations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerExplanationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerExplanations( sprintf( 'SELECT * FROM customer_explanations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerExplanationsByCustomerDataTypeIdByCid( $intCustomerDataTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerExplanations( sprintf( 'SELECT * FROM customer_explanations WHERE customer_data_type_id = %d AND cid = %d', ( int ) $intCustomerDataTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerExplanationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerExplanations( sprintf( 'SELECT * FROM customer_explanations WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>