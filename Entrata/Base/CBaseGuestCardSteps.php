<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestCardSteps
 * Do not add any new functions to this class.
 */

class CBaseGuestCardSteps extends CEosPluralBase {

	/**
	 * @return CGuestCardStep[]
	 */
	public static function fetchGuestCardSteps( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGuestCardStep::class, $objDatabase );
	}

	/**
	 * @return CGuestCardStep
	 */
	public static function fetchGuestCardStep( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGuestCardStep::class, $objDatabase );
	}

	public static function fetchGuestCardStepCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'guest_card_steps', $objDatabase );
	}

	public static function fetchGuestCardStepById( $intId, $objDatabase ) {
		return self::fetchGuestCardStep( sprintf( 'SELECT * FROM guest_card_steps WHERE id = %d', $intId ), $objDatabase );
	}

}
?>