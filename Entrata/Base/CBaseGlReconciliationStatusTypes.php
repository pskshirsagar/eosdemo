<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseGlReconciliationStatusTypes extends CEosPluralBase {

	/**
	 * @return CGlReconciliationStatusType[]
	 */
	public static function fetchGlReconciliationStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlReconciliationStatusType::class, $objDatabase );
	}

	/**
	 * @return CGlReconciliationStatusType
	 */
	public static function fetchGlReconciliationStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlReconciliationStatusType::class, $objDatabase );
	}

	public static function fetchGlReconciliationStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_reconciliation_status_types', $objDatabase );
	}

	public static function fetchGlReconciliationStatusTypeById( $intId, $objDatabase ) {
		return self::fetchGlReconciliationStatusType( sprintf( 'SELECT * FROM gl_reconciliation_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>