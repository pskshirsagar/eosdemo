<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportItemDescriptions
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportItemDescriptions extends CEosPluralBase {

	/**
	 * @return CDefaultReportItemDescription[]
	 */
	public static function fetchDefaultReportItemDescriptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReportItemDescription::class, $objDatabase );
	}

	/**
	 * @return CDefaultReportItemDescription
	 */
	public static function fetchDefaultReportItemDescription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReportItemDescription::class, $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_item_descriptions', $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionById( $intId, $objDatabase ) {
		return self::fetchDefaultReportItemDescription( sprintf( 'SELECT * FROM default_report_item_descriptions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchDefaultReportItemDescriptions( sprintf( 'SELECT * FROM default_report_item_descriptions WHERE default_cid = %d', $intDefaultCid ), $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionsByReportItemTypeId( $intReportItemTypeId, $objDatabase ) {
		return self::fetchDefaultReportItemDescriptions( sprintf( 'SELECT * FROM default_report_item_descriptions WHERE report_item_type_id = %d', $intReportItemTypeId ), $objDatabase );
	}

}
?>