<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyRoommateInterests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyRoommateInterests extends CEosPluralBase {

	/**
	 * @return CPropertyRoommateInterest[]
	 */
	public static function fetchPropertyRoommateInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyRoommateInterest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyRoommateInterest
	 */
	public static function fetchPropertyRoommateInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyRoommateInterest', $objDatabase );
	}

	public static function fetchPropertyRoommateInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_roommate_interests', $objDatabase );
	}

	public static function fetchPropertyRoommateInterestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoommateInterest( sprintf( 'SELECT * FROM property_roommate_interests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoommateInterestsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyRoommateInterests( sprintf( 'SELECT * FROM property_roommate_interests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoommateInterestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoommateInterests( sprintf( 'SELECT * FROM property_roommate_interests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoommateInterestsByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoommateInterests( sprintf( 'SELECT * FROM property_roommate_interests WHERE roommate_interest_id = %d AND cid = %d', ( int ) $intRoommateInterestId, ( int ) $intCid ), $objDatabase );
	}

}
?>