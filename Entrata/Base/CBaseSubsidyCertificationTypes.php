<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCertificationTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCertificationType[]
	 */
	public static function fetchSubsidyCertificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCertificationType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCertificationType
	 */
	public static function fetchSubsidyCertificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCertificationType::class, $objDatabase );
	}

	public static function fetchSubsidyCertificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certification_types', $objDatabase );
	}

	public static function fetchSubsidyCertificationTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCertificationType( sprintf( 'SELECT * FROM subsidy_certification_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>