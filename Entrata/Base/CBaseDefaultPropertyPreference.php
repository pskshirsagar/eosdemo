<?php

class CBaseDefaultPropertyPreference extends CEosSingularBase {

	const TABLE_NAME = 'public.default_property_preferences';

	protected $m_intId;
	protected $m_strKey;
	protected $m_strValue;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( stripcslashes( $arrValues['value'] ) ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'key' => $this->getKey(),
			'value' => $this->getValue()
		);
	}

}
?>