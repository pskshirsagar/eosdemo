<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransferProperties extends CEosPluralBase {

	/**
	 * @return CPropertyTransferProperty[]
	 */
	public static function fetchPropertyTransferProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyTransferProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyTransferProperty
	 */
	public static function fetchPropertyTransferProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyTransferProperty', $objDatabase );
	}

	public static function fetchPropertyTransferPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_transfer_properties', $objDatabase );
	}

	public static function fetchPropertyTransferPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferProperty( sprintf( 'SELECT * FROM property_transfer_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyTransferProperties( sprintf( 'SELECT * FROM property_transfer_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferPropertiesByPropertyTransferIdByCid( $intPropertyTransferId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferProperties( sprintf( 'SELECT * FROM property_transfer_properties WHERE property_transfer_id = %d AND cid = %d', ( int ) $intPropertyTransferId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferProperties( sprintf( 'SELECT * FROM property_transfer_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>