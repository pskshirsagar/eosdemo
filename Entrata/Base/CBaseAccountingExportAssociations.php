<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportAssociations extends CEosPluralBase {

	/**
	 * @return CAccountingExportAssociation[]
	 */
	public static function fetchAccountingExportAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountingExportAssociation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountingExportAssociation
	 */
	public static function fetchAccountingExportAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportAssociation::class, $objDatabase );
	}

	public static function fetchAccountingExportAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_associations', $objDatabase );
	}

	public static function fetchAccountingExportAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportAssociation( sprintf( 'SELECT * FROM accounting_export_associations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchAccountingExportAssociations( sprintf( 'SELECT * FROM accounting_export_associations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportAssociationsByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportAssociations( sprintf( 'SELECT * FROM accounting_export_associations WHERE accounting_export_batch_id = %d AND cid = %d', $intAccountingExportBatchId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportAssociationsByGlDetailIdByCid( $intGlDetailId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportAssociations( sprintf( 'SELECT * FROM accounting_export_associations WHERE gl_detail_id = %d AND cid = %d', $intGlDetailId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportAssociationsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportAssociations( sprintf( 'SELECT * FROM accounting_export_associations WHERE ar_transaction_id = %d AND cid = %d', $intArTransactionId, $intCid ), $objDatabase );
	}

}
?>