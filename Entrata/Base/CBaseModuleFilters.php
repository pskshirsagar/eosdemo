<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CModuleFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseModuleFilters extends CEosPluralBase {

	/**
	 * @return CModuleFilter[]
	 */
	public static function fetchModuleFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CModuleFilter', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CModuleFilter
	 */
	public static function fetchModuleFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CModuleFilter', $objDatabase );
	}

	public static function fetchModuleFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'module_filters', $objDatabase );
	}

	public static function fetchModuleFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchModuleFilter( sprintf( 'SELECT * FROM module_filters WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchModuleFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchModuleFilters( sprintf( 'SELECT * FROM module_filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchModuleFiltersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchModuleFilters( sprintf( 'SELECT * FROM module_filters WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>