<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCamPeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCamPeriods extends CEosPluralBase {

	/**
	 * @return CCamPeriod[]
	 */
	public static function fetchCamPeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCamPeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCamPeriod
	 */
	public static function fetchCamPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCamPeriod', $objDatabase );
	}

	public static function fetchCamPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cam_periods', $objDatabase );
	}

	public static function fetchCamPeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCamPeriod( sprintf( 'SELECT * FROM cam_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCamPeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchCamPeriods( sprintf( 'SELECT * FROM cam_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCamPeriodsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCamPeriods( sprintf( 'SELECT * FROM cam_periods WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>