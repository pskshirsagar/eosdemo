<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportLog extends CEosSingularBase {

	const TABLE_NAME = 'public.import_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intImportTypeId;
	protected $m_intImportDataTypeId;
	protected $m_intDocumentReferenceId;
	protected $m_strFileName;
	protected $m_boolUseAmazonFileStorage;
	protected $m_strSummary;
	protected $m_strStartedOn;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intReferenceId;

	public function __construct() {
		parent::__construct();

		$this->m_boolUseAmazonFileStorage = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['import_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTypeId', trim( $arrValues['import_type_id'] ) ); elseif( isset( $arrValues['import_type_id'] ) ) $this->setImportTypeId( $arrValues['import_type_id'] );
		if( isset( $arrValues['import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportDataTypeId', trim( $arrValues['import_data_type_id'] ) ); elseif( isset( $arrValues['import_data_type_id'] ) ) $this->setImportDataTypeId( $arrValues['import_data_type_id'] );
		if( isset( $arrValues['document_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentReferenceId', trim( $arrValues['document_reference_id'] ) ); elseif( isset( $arrValues['document_reference_id'] ) ) $this->setDocumentReferenceId( $arrValues['document_reference_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['use_amazon_file_storage'] ) && $boolDirectSet ) $this->set( 'm_boolUseAmazonFileStorage', trim( stripcslashes( $arrValues['use_amazon_file_storage'] ) ) ); elseif( isset( $arrValues['use_amazon_file_storage'] ) ) $this->setUseAmazonFileStorage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_amazon_file_storage'] ) : $arrValues['use_amazon_file_storage'] );
		if( isset( $arrValues['summary'] ) && $boolDirectSet ) $this->set( 'm_strSummary', trim( $arrValues['summary'] ) ); elseif( isset( $arrValues['summary'] ) ) $this->setSummary( $arrValues['summary'] );
		if( isset( $arrValues['started_on'] ) && $boolDirectSet ) $this->set( 'm_strStartedOn', trim( $arrValues['started_on'] ) ); elseif( isset( $arrValues['started_on'] ) ) $this->setStartedOn( $arrValues['started_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setImportTypeId( $intImportTypeId ) {
		$this->set( 'm_intImportTypeId', CStrings::strToIntDef( $intImportTypeId, NULL, false ) );
	}

	public function getImportTypeId() {
		return $this->m_intImportTypeId;
	}

	public function sqlImportTypeId() {
		return ( true == isset( $this->m_intImportTypeId ) ) ? ( string ) $this->m_intImportTypeId : 'NULL';
	}

	public function setImportDataTypeId( $intImportDataTypeId ) {
		$this->set( 'm_intImportDataTypeId', CStrings::strToIntDef( $intImportDataTypeId, NULL, false ) );
	}

	public function getImportDataTypeId() {
		return $this->m_intImportDataTypeId;
	}

	public function sqlImportDataTypeId() {
		return ( true == isset( $this->m_intImportDataTypeId ) ) ? ( string ) $this->m_intImportDataTypeId : 'NULL';
	}

	public function setDocumentReferenceId( $intDocumentReferenceId ) {
		$this->set( 'm_intDocumentReferenceId', CStrings::strToIntDef( $intDocumentReferenceId, NULL, false ) );
	}

	public function getDocumentReferenceId() {
		return $this->m_intDocumentReferenceId;
	}

	public function sqlDocumentReferenceId() {
		return ( true == isset( $this->m_intDocumentReferenceId ) ) ? ( string ) $this->m_intDocumentReferenceId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setUseAmazonFileStorage( $boolUseAmazonFileStorage ) {
		$this->set( 'm_boolUseAmazonFileStorage', CStrings::strToBool( $boolUseAmazonFileStorage ) );
	}

	public function getUseAmazonFileStorage() {
		return $this->m_boolUseAmazonFileStorage;
	}

	public function sqlUseAmazonFileStorage() {
		return ( true == isset( $this->m_boolUseAmazonFileStorage ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseAmazonFileStorage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSummary( $strSummary ) {
		$this->set( 'm_strSummary', CStrings::strTrimDef( $strSummary, -1, NULL, true ) );
	}

	public function getSummary() {
		return $this->m_strSummary;
	}

	public function sqlSummary() {
		return ( true == isset( $this->m_strSummary ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSummary ) : '\'' . addslashes( $this->m_strSummary ) . '\'' ) : 'NULL';
	}

	public function setStartedOn( $strStartedOn ) {
		$this->set( 'm_strStartedOn', CStrings::strTrimDef( $strStartedOn, -1, NULL, true ) );
	}

	public function getStartedOn() {
		return $this->m_strStartedOn;
	}

	public function sqlStartedOn() {
		return ( true == isset( $this->m_strStartedOn ) ) ? '\'' . $this->m_strStartedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, import_type_id, import_data_type_id, document_reference_id, file_name, use_amazon_file_storage, summary, started_on, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, reference_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlImportTypeId() . ', ' .
						$this->sqlImportDataTypeId() . ', ' .
						$this->sqlDocumentReferenceId() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlUseAmazonFileStorage() . ', ' .
						$this->sqlSummary() . ', ' .
						$this->sqlStartedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlReferenceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_reference_id = ' . $this->sqlDocumentReferenceId(). ',' ; } elseif( true == array_key_exists( 'DocumentReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' document_reference_id = ' . $this->sqlDocumentReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage(). ',' ; } elseif( true == array_key_exists( 'UseAmazonFileStorage', $this->getChangedColumns() ) ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary = ' . $this->sqlSummary(). ',' ; } elseif( true == array_key_exists( 'Summary', $this->getChangedColumns() ) ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_on = ' . $this->sqlStartedOn(). ',' ; } elseif( true == array_key_exists( 'StartedOn', $this->getChangedColumns() ) ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'import_type_id' => $this->getImportTypeId(),
			'import_data_type_id' => $this->getImportDataTypeId(),
			'document_reference_id' => $this->getDocumentReferenceId(),
			'file_name' => $this->getFileName(),
			'use_amazon_file_storage' => $this->getUseAmazonFileStorage(),
			'summary' => $this->getSummary(),
			'started_on' => $this->getStartedOn(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'reference_id' => $this->getReferenceId()
		);
	}

}
?>