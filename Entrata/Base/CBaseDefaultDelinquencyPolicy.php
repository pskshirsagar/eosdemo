<?php

class CBaseDefaultDelinquencyPolicy extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_delinquency_policies';

	protected $m_intId;
	protected $m_intOccupancyTypeId;
	protected $m_strName;
	protected $m_intDelinquencyThresholdTypeId;
	protected $m_strDescription;
	protected $m_fltThresholdAmount;
	protected $m_fltSmallBalanceAmount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['delinquency_threshold_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyThresholdTypeId', trim( $arrValues['delinquency_threshold_type_id'] ) ); elseif( isset( $arrValues['delinquency_threshold_type_id'] ) ) $this->setDelinquencyThresholdTypeId( $arrValues['delinquency_threshold_type_id'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['threshold_amount'] ) && $boolDirectSet ) $this->set( 'm_fltThresholdAmount', trim( $arrValues['threshold_amount'] ) ); elseif( isset( $arrValues['threshold_amount'] ) ) $this->setThresholdAmount( $arrValues['threshold_amount'] );
		if( isset( $arrValues['small_balance_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSmallBalanceAmount', trim( $arrValues['small_balance_amount'] ) ); elseif( isset( $arrValues['small_balance_amount'] ) ) $this->setSmallBalanceAmount( $arrValues['small_balance_amount'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDelinquencyThresholdTypeId( $intDelinquencyThresholdTypeId ) {
		$this->set( 'm_intDelinquencyThresholdTypeId', CStrings::strToIntDef( $intDelinquencyThresholdTypeId, NULL, false ) );
	}

	public function getDelinquencyThresholdTypeId() {
		return $this->m_intDelinquencyThresholdTypeId;
	}

	public function sqlDelinquencyThresholdTypeId() {
		return ( true == isset( $this->m_intDelinquencyThresholdTypeId ) ) ? ( string ) $this->m_intDelinquencyThresholdTypeId : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 200, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setThresholdAmount( $fltThresholdAmount ) {
		$this->set( 'm_fltThresholdAmount', CStrings::strToFloatDef( $fltThresholdAmount, NULL, false, 2 ) );
	}

	public function getThresholdAmount() {
		return $this->m_fltThresholdAmount;
	}

	public function sqlThresholdAmount() {
		return ( true == isset( $this->m_fltThresholdAmount ) ) ? ( string ) $this->m_fltThresholdAmount : 'NULL';
	}

	public function setSmallBalanceAmount( $fltSmallBalanceAmount ) {
		$this->set( 'm_fltSmallBalanceAmount', CStrings::strToFloatDef( $fltSmallBalanceAmount, NULL, false, 2 ) );
	}

	public function getSmallBalanceAmount() {
		return $this->m_fltSmallBalanceAmount;
	}

	public function sqlSmallBalanceAmount() {
		return ( true == isset( $this->m_fltSmallBalanceAmount ) ) ? ( string ) $this->m_fltSmallBalanceAmount : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'name' => $this->getName(),
			'delinquency_threshold_type_id' => $this->getDelinquencyThresholdTypeId(),
			'description' => $this->getDescription(),
			'threshold_amount' => $this->getThresholdAmount(),
			'small_balance_amount' => $this->getSmallBalanceAmount(),
			'details' => $this->getDetails()
		);
	}

}
?>