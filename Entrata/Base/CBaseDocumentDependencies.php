<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentDependencies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentDependencies extends CEosPluralBase {

	/**
	 * @return CDocumentDependency[]
	 */
	public static function fetchDocumentDependencies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDocumentDependency', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocumentDependency
	 */
	public static function fetchDocumentDependency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentDependency', $objDatabase );
	}

	public static function fetchDocumentDependencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_dependencies', $objDatabase );
	}

	public static function fetchDocumentDependencyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocumentDependency( sprintf( 'SELECT * FROM document_dependencies WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentDependenciesByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentDependencies( sprintf( 'SELECT * FROM document_dependencies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentDependenciesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentDependencies( sprintf( 'SELECT * FROM document_dependencies WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentDependenciesByDocumentAddendaIdByCid( $intDocumentAddendaId, $intCid, $objDatabase ) {
		return self::fetchDocumentDependencies( sprintf( 'SELECT * FROM document_dependencies WHERE document_addenda_id = %d AND cid = %d', ( int ) $intDocumentAddendaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentDependenciesByDocumentDependencyTypeIdByCid( $intDocumentDependencyTypeId, $intCid, $objDatabase ) {
		return self::fetchDocumentDependencies( sprintf( 'SELECT * FROM document_dependencies WHERE document_dependency_type_id = %d AND cid = %d', ( int ) $intDocumentDependencyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentDependenciesByIncludeDocumentAddendaIdByCid( $intIncludeDocumentAddendaId, $intCid, $objDatabase ) {
		return self::fetchDocumentDependencies( sprintf( 'SELECT * FROM document_dependencies WHERE include_document_addenda_id = %d AND cid = %d', ( int ) $intIncludeDocumentAddendaId, ( int ) $intCid ), $objDatabase );
	}

}
?>