<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifieds
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerClassifieds extends CEosPluralBase {

	/**
	 * @return CCustomerClassified[]
	 */
	public static function fetchCustomerClassifieds( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerClassified::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerClassified
	 */
	public static function fetchCustomerClassified( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerClassified::class, $objDatabase );
	}

	public static function fetchCustomerClassifiedCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_classifieds', $objDatabase );
	}

	public static function fetchCustomerClassifiedByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassified( sprintf( 'SELECT * FROM customer_classifieds WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerClassifieds( sprintf( 'SELECT * FROM customer_classifieds WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifieds( sprintf( 'SELECT * FROM customer_classifieds WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifieds( sprintf( 'SELECT * FROM customer_classifieds WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedsByCompanyClassifiedCategoryIdByCid( $intCompanyClassifiedCategoryId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifieds( sprintf( 'SELECT * FROM customer_classifieds WHERE company_classified_category_id = %d AND cid = %d', $intCompanyClassifiedCategoryId, $intCid ), $objDatabase );
	}

}
?>