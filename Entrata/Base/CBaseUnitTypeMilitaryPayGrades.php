<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitTypeMilitaryPayGrades
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitTypeMilitaryPayGrades extends CEosPluralBase {

	/**
	 * @return CUnitTypeMilitaryPayGrade[]
	 */
	public static function fetchUnitTypeMilitaryPayGrades( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitTypeMilitaryPayGrade', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitTypeMilitaryPayGrade
	 */
	public static function fetchUnitTypeMilitaryPayGrade( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitTypeMilitaryPayGrade', $objDatabase );
	}

	public static function fetchUnitTypeMilitaryPayGradeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_type_military_pay_grades', $objDatabase );
	}

	public static function fetchUnitTypeMilitaryPayGradeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryPayGrade( sprintf( 'SELECT * FROM unit_type_military_pay_grades WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryPayGradesByCid( $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryPayGrades( sprintf( 'SELECT * FROM unit_type_military_pay_grades WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryPayGradesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryPayGrades( sprintf( 'SELECT * FROM unit_type_military_pay_grades WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitTypeMilitaryPayGradesByMilitaryPayGradeIdByCid( $intMilitaryPayGradeId, $intCid, $objDatabase ) {
		return self::fetchUnitTypeMilitaryPayGrades( sprintf( 'SELECT * FROM unit_type_military_pay_grades WHERE military_pay_grade_id = %d AND cid = %d', ( int ) $intMilitaryPayGradeId, ( int ) $intCid ), $objDatabase );
	}

}
?>