<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerActivityPoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerActivityPoints extends CEosPluralBase {

	/**
	 * @return CCustomerActivityPoint[]
	 */
	public static function fetchCustomerActivityPoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerActivityPoint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerActivityPoint
	 */
	public static function fetchCustomerActivityPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerActivityPoint', $objDatabase );
	}

	public static function fetchCustomerActivityPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_activity_points', $objDatabase );
	}

	public static function fetchCustomerActivityPointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPoint( sprintf( 'SELECT * FROM customer_activity_points WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerActivityPointsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPoints( sprintf( 'SELECT * FROM customer_activity_points WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerActivityPointsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPoints( sprintf( 'SELECT * FROM customer_activity_points WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerActivityPointsByCompanyActivityPointIdByCid( $intCompanyActivityPointId, $intCid, $objDatabase ) {
		return self::fetchCustomerActivityPoints( sprintf( 'SELECT * FROM customer_activity_points WHERE company_activity_point_id = %d AND cid = %d', ( int ) $intCompanyActivityPointId, ( int ) $intCid ), $objDatabase );
	}

}
?>