<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeContacts extends CEosPluralBase {

	/**
	 * @return CApPayeeContact[]
	 */
	public static function fetchApPayeeContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayeeContact::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeContact
	 */
	public static function fetchApPayeeContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeContact::class, $objDatabase );
	}

	public static function fetchApPayeeContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_contacts', $objDatabase );
	}

	public static function fetchApPayeeContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContact( sprintf( 'SELECT * FROM ap_payee_contacts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeContacts( sprintf( 'SELECT * FROM ap_payee_contacts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContacts( sprintf( 'SELECT * FROM ap_payee_contacts WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeContactsByWorkerIdByCid( $intWorkerId, $intCid, $objDatabase ) {
		return self::fetchApPayeeContacts( sprintf( 'SELECT * FROM ap_payee_contacts WHERE worker_id = %d AND cid = %d', $intWorkerId, $intCid ), $objDatabase );
	}

}
?>