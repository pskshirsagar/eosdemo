<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCashFlowTypes
 * Do not add any new functions to this class.
 */

class CBaseCashFlowTypes extends CEosPluralBase {

	/**
	 * @return CCashFlowType[]
	 */
	public static function fetchCashFlowTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCashFlowType::class, $objDatabase );
	}

	/**
	 * @return CCashFlowType
	 */
	public static function fetchCashFlowType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCashFlowType::class, $objDatabase );
	}

	public static function fetchCashFlowTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cash_flow_types', $objDatabase );
	}

	public static function fetchCashFlowTypeById( $intId, $objDatabase ) {
		return self::fetchCashFlowType( sprintf( 'SELECT * FROM cash_flow_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>