<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewInstances
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportNewInstances extends CEosPluralBase {

	/**
	 * @return CReportNewInstance[]
	 */
	public static function fetchReportNewInstances( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportNewInstance::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportNewInstance
	 */
	public static function fetchReportNewInstance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportNewInstance::class, $objDatabase );
	}

	public static function fetchReportNewInstanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_new_instances', $objDatabase );
	}

	public static function fetchReportNewInstanceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstance( sprintf( 'SELECT * FROM report_new_instances WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancesByCid( $intCid, $objDatabase ) {
		return self::fetchReportNewInstances( sprintf( 'SELECT * FROM report_new_instances WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancesByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstances( sprintf( 'SELECT * FROM report_new_instances WHERE report_id = %d AND cid = %d', $intReportId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancesByReportVersionIdByCid( $intReportVersionId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstances( sprintf( 'SELECT * FROM report_new_instances WHERE report_version_id = %d AND cid = %d', $intReportVersionId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstances( sprintf( 'SELECT * FROM report_new_instances WHERE module_id = %d AND cid = %d', $intModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstances( sprintf( 'SELECT * FROM report_new_instances WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>