<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserNotification extends CEosSingularBase {

	const TABLE_NAME = 'public.company_user_notifications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_strMessage;
	protected $m_arrstrTags;
	protected $m_strSentOn;
	protected $m_strReadOn;
	protected $m_strExpiresOn;
	protected $m_strSnoozeUntil;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['tags'] ) && $boolDirectSet ) $this->set( 'm_arrstrTags', trim( $arrValues['tags'] ) ); elseif( isset( $arrValues['tags'] ) ) $this->setTags( $arrValues['tags'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['read_on'] ) && $boolDirectSet ) $this->set( 'm_strReadOn', trim( $arrValues['read_on'] ) ); elseif( isset( $arrValues['read_on'] ) ) $this->setReadOn( $arrValues['read_on'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['snooze_until'] ) && $boolDirectSet ) $this->set( 'm_strSnoozeUntil', trim( $arrValues['snooze_until'] ) ); elseif( isset( $arrValues['snooze_until'] ) ) $this->setSnoozeUntil( $arrValues['snooze_until'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setTags( $arrstrTags ) {
		$this->set( 'm_arrstrTags', CStrings::strToArrIntDef( $arrstrTags, NULL ) );
	}

	public function getTags() {
		return $this->m_arrstrTags;
	}

	public function sqlTags() {
		return ( true == isset( $this->m_arrstrTags ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrTags, NULL ) . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setReadOn( $strReadOn ) {
		$this->set( 'm_strReadOn', CStrings::strTrimDef( $strReadOn, -1, NULL, true ) );
	}

	public function getReadOn() {
		return $this->m_strReadOn;
	}

	public function sqlReadOn() {
		return ( true == isset( $this->m_strReadOn ) ) ? '\'' . $this->m_strReadOn . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setSnoozeUntil( $strSnoozeUntil ) {
		$this->set( 'm_strSnoozeUntil', CStrings::strTrimDef( $strSnoozeUntil, -1, NULL, true ) );
	}

	public function getSnoozeUntil() {
		return $this->m_strSnoozeUntil;
	}

	public function sqlSnoozeUntil() {
		return ( true == isset( $this->m_strSnoozeUntil ) ) ? '\'' . $this->m_strSnoozeUntil . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, message, tags, sent_on, read_on, expires_on, snooze_until, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlTags() . ', ' .
 						$this->sqlSentOn() . ', ' .
 						$this->sqlReadOn() . ', ' .
 						$this->sqlExpiresOn() . ', ' .
 						$this->sqlSnoozeUntil() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tags = ' . $this->sqlTags() . ','; } elseif( true == array_key_exists( 'Tags', $this->getChangedColumns() ) ) { $strSql .= ' tags = ' . $this->sqlTags() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' read_on = ' . $this->sqlReadOn() . ','; } elseif( true == array_key_exists( 'ReadOn', $this->getChangedColumns() ) ) { $strSql .= ' read_on = ' . $this->sqlReadOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' snooze_until = ' . $this->sqlSnoozeUntil() . ','; } elseif( true == array_key_exists( 'SnoozeUntil', $this->getChangedColumns() ) ) { $strSql .= ' snooze_until = ' . $this->sqlSnoozeUntil() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'message' => $this->getMessage(),
			'tags' => $this->getTags(),
			'sent_on' => $this->getSentOn(),
			'read_on' => $this->getReadOn(),
			'expires_on' => $this->getExpiresOn(),
			'snooze_until' => $this->getSnoozeUntil(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>