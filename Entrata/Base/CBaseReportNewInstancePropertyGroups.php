<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewInstancePropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportNewInstancePropertyGroups extends CEosPluralBase {

	/**
	 * @return CReportNewInstancePropertyGroup[]
	 */
	public static function fetchReportNewInstancePropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportNewInstancePropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportNewInstancePropertyGroup
	 */
	public static function fetchReportNewInstancePropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportNewInstancePropertyGroup::class, $objDatabase );
	}

	public static function fetchReportNewInstancePropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_new_instance_property_groups', $objDatabase );
	}

	public static function fetchReportNewInstancePropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstancePropertyGroup( sprintf( 'SELECT * FROM report_new_instance_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancePropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchReportNewInstancePropertyGroups( sprintf( 'SELECT * FROM report_new_instance_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancePropertyGroupsByReportNewInstanceIdByCid( $intReportNewInstanceId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstancePropertyGroups( sprintf( 'SELECT * FROM report_new_instance_property_groups WHERE report_new_instance_id = %d AND cid = %d', ( int ) $intReportNewInstanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportNewInstancePropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchReportNewInstancePropertyGroups( sprintf( 'SELECT * FROM report_new_instance_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>