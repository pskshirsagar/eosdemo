<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCampaignTarget extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.campaign_targets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intApplicantId;
	protected $m_intCampaignId;
	protected $m_strCompanyName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strNameMaiden;
	protected $m_strPhoneNumber;
	protected $m_strMobileNumber;
	protected $m_strWorkNumber;
	protected $m_strFaxNumber;
	protected $m_strEmailAddress;
	protected $m_strBirthDate;
	protected $m_strGender;
	protected $m_intHeight;
	protected $m_intWeight;
	protected $m_strEyeColor;
	protected $m_strHairColor;
	protected $m_intCompanyIdentificationTypeId;
	protected $m_strIdentificationValue;
	protected $m_strIdentificationExpiration;
	protected $m_strResponseEmail;
	protected $m_strNotes;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intArchivedBy;
	protected $m_strArchivedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intCampaignId', trim( $arrValues['campaign_id'] ) ); elseif( isset( $arrValues['campaign_id'] ) ) $this->setCampaignId( $arrValues['campaign_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( $arrValues['mobile_number'] ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( $arrValues['mobile_number'] );
		if( isset( $arrValues['work_number'] ) && $boolDirectSet ) $this->set( 'm_strWorkNumber', trim( $arrValues['work_number'] ) ); elseif( isset( $arrValues['work_number'] ) ) $this->setWorkNumber( $arrValues['work_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		if( isset( $arrValues['weight'] ) && $boolDirectSet ) $this->set( 'm_intWeight', trim( $arrValues['weight'] ) ); elseif( isset( $arrValues['weight'] ) ) $this->setWeight( $arrValues['weight'] );
		if( isset( $arrValues['eye_color'] ) && $boolDirectSet ) $this->set( 'm_strEyeColor', trim( $arrValues['eye_color'] ) ); elseif( isset( $arrValues['eye_color'] ) ) $this->setEyeColor( $arrValues['eye_color'] );
		if( isset( $arrValues['hair_color'] ) && $boolDirectSet ) $this->set( 'm_strHairColor', trim( $arrValues['hair_color'] ) ); elseif( isset( $arrValues['hair_color'] ) ) $this->setHairColor( $arrValues['hair_color'] );
		if( isset( $arrValues['company_identification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyIdentificationTypeId', trim( $arrValues['company_identification_type_id'] ) ); elseif( isset( $arrValues['company_identification_type_id'] ) ) $this->setCompanyIdentificationTypeId( $arrValues['company_identification_type_id'] );
		if( isset( $arrValues['identification_value'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationValue', trim( $arrValues['identification_value'] ) ); elseif( isset( $arrValues['identification_value'] ) ) $this->setIdentificationValue( $arrValues['identification_value'] );
		if( isset( $arrValues['identification_expiration'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationExpiration', trim( $arrValues['identification_expiration'] ) ); elseif( isset( $arrValues['identification_expiration'] ) ) $this->setIdentificationExpiration( $arrValues['identification_expiration'] );
		if( isset( $arrValues['response_email'] ) && $boolDirectSet ) $this->set( 'm_strResponseEmail', trim( stripcslashes( $arrValues['response_email'] ) ) ); elseif( isset( $arrValues['response_email'] ) ) $this->setResponseEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_email'] ) : $arrValues['response_email'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['archived_by'] ) && $boolDirectSet ) $this->set( 'm_intArchivedBy', trim( $arrValues['archived_by'] ) ); elseif( isset( $arrValues['archived_by'] ) ) $this->setArchivedBy( $arrValues['archived_by'] );
		if( isset( $arrValues['archived_on'] ) && $boolDirectSet ) $this->set( 'm_strArchivedOn', trim( $arrValues['archived_on'] ) ); elseif( isset( $arrValues['archived_on'] ) ) $this->setArchivedOn( $arrValues['archived_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setCampaignId( $intCampaignId ) {
		$this->set( 'm_intCampaignId', CStrings::strToIntDef( $intCampaignId, NULL, false ) );
	}

	public function getCampaignId() {
		return $this->m_intCampaignId;
	}

	public function sqlCampaignId() {
		return ( true == isset( $this->m_intCampaignId ) ) ? ( string ) $this->m_intCampaignId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 100, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 50, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMobileNumber ) : '\'' . addslashes( $this->m_strMobileNumber ) . '\'' ) : 'NULL';
	}

	public function setWorkNumber( $strWorkNumber ) {
		$this->set( 'm_strWorkNumber', CStrings::strTrimDef( $strWorkNumber, 30, NULL, true ) );
	}

	public function getWorkNumber() {
		return $this->m_strWorkNumber;
	}

	public function sqlWorkNumber() {
		return ( true == isset( $this->m_strWorkNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorkNumber ) : '\'' . addslashes( $this->m_strWorkNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function setWeight( $intWeight ) {
		$this->set( 'm_intWeight', CStrings::strToIntDef( $intWeight, NULL, false ) );
	}

	public function getWeight() {
		return $this->m_intWeight;
	}

	public function sqlWeight() {
		return ( true == isset( $this->m_intWeight ) ) ? ( string ) $this->m_intWeight : 'NULL';
	}

	public function setEyeColor( $strEyeColor ) {
		$this->set( 'm_strEyeColor', CStrings::strTrimDef( $strEyeColor, 20, NULL, true ) );
	}

	public function getEyeColor() {
		return $this->m_strEyeColor;
	}

	public function sqlEyeColor() {
		return ( true == isset( $this->m_strEyeColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEyeColor ) : '\'' . addslashes( $this->m_strEyeColor ) . '\'' ) : 'NULL';
	}

	public function setHairColor( $strHairColor ) {
		$this->set( 'm_strHairColor', CStrings::strTrimDef( $strHairColor, 20, NULL, true ) );
	}

	public function getHairColor() {
		return $this->m_strHairColor;
	}

	public function sqlHairColor() {
		return ( true == isset( $this->m_strHairColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHairColor ) : '\'' . addslashes( $this->m_strHairColor ) . '\'' ) : 'NULL';
	}

	public function setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId ) {
		$this->set( 'm_intCompanyIdentificationTypeId', CStrings::strToIntDef( $intCompanyIdentificationTypeId, NULL, false ) );
	}

	public function getCompanyIdentificationTypeId() {
		return $this->m_intCompanyIdentificationTypeId;
	}

	public function sqlCompanyIdentificationTypeId() {
		return ( true == isset( $this->m_intCompanyIdentificationTypeId ) ) ? ( string ) $this->m_intCompanyIdentificationTypeId : 'NULL';
	}

	public function setIdentificationValue( $strIdentificationValue ) {
		$this->set( 'm_strIdentificationValue', CStrings::strTrimDef( $strIdentificationValue, 240, NULL, true ) );
	}

	public function getIdentificationValue() {
		return $this->m_strIdentificationValue;
	}

	public function sqlIdentificationValue() {
		return ( true == isset( $this->m_strIdentificationValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationValue ) : '\'' . addslashes( $this->m_strIdentificationValue ) . '\'' ) : 'NULL';
	}

	public function setIdentificationExpiration( $strIdentificationExpiration ) {
		$this->set( 'm_strIdentificationExpiration', CStrings::strTrimDef( $strIdentificationExpiration, -1, NULL, true ) );
	}

	public function getIdentificationExpiration() {
		return $this->m_strIdentificationExpiration;
	}

	public function sqlIdentificationExpiration() {
		return ( true == isset( $this->m_strIdentificationExpiration ) ) ? '\'' . $this->m_strIdentificationExpiration . '\'' : 'NULL';
	}

	public function setResponseEmail( $strResponseEmail ) {
		$this->set( 'm_strResponseEmail', CStrings::strTrimDef( $strResponseEmail, -1, NULL, true ) );
	}

	public function getResponseEmail() {
		return $this->m_strResponseEmail;
	}

	public function sqlResponseEmail() {
		return ( true == isset( $this->m_strResponseEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponseEmail ) : '\'' . addslashes( $this->m_strResponseEmail ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setArchivedBy( $intArchivedBy ) {
		$this->set( 'm_intArchivedBy', CStrings::strToIntDef( $intArchivedBy, NULL, false ) );
	}

	public function getArchivedBy() {
		return $this->m_intArchivedBy;
	}

	public function sqlArchivedBy() {
		return ( true == isset( $this->m_intArchivedBy ) ) ? ( string ) $this->m_intArchivedBy : 'NULL';
	}

	public function setArchivedOn( $strArchivedOn ) {
		$this->set( 'm_strArchivedOn', CStrings::strTrimDef( $strArchivedOn, -1, NULL, true ) );
	}

	public function getArchivedOn() {
		return $this->m_strArchivedOn;
	}

	public function sqlArchivedOn() {
		return ( true == isset( $this->m_strArchivedOn ) ) ? '\'' . $this->m_strArchivedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, applicant_id, campaign_id, company_name, name_prefix, name_first, name_middle, name_last, name_suffix, name_maiden, phone_number, mobile_number, work_number, fax_number, email_address, birth_date, gender, height, weight, eye_color, hair_color, company_identification_type_id, identification_value, identification_expiration, response_email, notes, order_num, imported_on, archived_by, archived_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlCampaignId() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlHeight() . ', ' .
						$this->sqlWeight() . ', ' .
						$this->sqlEyeColor() . ', ' .
						$this->sqlHairColor() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlResponseEmail() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlArchivedBy() . ', ' .
						$this->sqlArchivedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' campaign_id = ' . $this->sqlCampaignId(). ',' ; } elseif( true == array_key_exists( 'CampaignId', $this->getChangedColumns() ) ) { $strSql .= ' campaign_id = ' . $this->sqlCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden(). ',' ; } elseif( true == array_key_exists( 'NameMaiden', $this->getChangedColumns() ) ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber(). ',' ; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber(). ',' ; } elseif( true == array_key_exists( 'WorkNumber', $this->getChangedColumns() ) ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' height = ' . $this->sqlHeight(). ',' ; } elseif( true == array_key_exists( 'Height', $this->getChangedColumns() ) ) { $strSql .= ' height = ' . $this->sqlHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weight = ' . $this->sqlWeight(). ',' ; } elseif( true == array_key_exists( 'Weight', $this->getChangedColumns() ) ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eye_color = ' . $this->sqlEyeColor(). ',' ; } elseif( true == array_key_exists( 'EyeColor', $this->getChangedColumns() ) ) { $strSql .= ' eye_color = ' . $this->sqlEyeColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hair_color = ' . $this->sqlHairColor(). ',' ; } elseif( true == array_key_exists( 'HairColor', $this->getChangedColumns() ) ) { $strSql .= ' hair_color = ' . $this->sqlHairColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyIdentificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue(). ',' ; } elseif( true == array_key_exists( 'IdentificationValue', $this->getChangedColumns() ) ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration(). ',' ; } elseif( true == array_key_exists( 'IdentificationExpiration', $this->getChangedColumns() ) ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_email = ' . $this->sqlResponseEmail(). ',' ; } elseif( true == array_key_exists( 'ResponseEmail', $this->getChangedColumns() ) ) { $strSql .= ' response_email = ' . $this->sqlResponseEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy(). ',' ; } elseif( true == array_key_exists( 'ArchivedBy', $this->getChangedColumns() ) ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn(). ',' ; } elseif( true == array_key_exists( 'ArchivedOn', $this->getChangedColumns() ) ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'applicant_id' => $this->getApplicantId(),
			'campaign_id' => $this->getCampaignId(),
			'company_name' => $this->getCompanyName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'name_maiden' => $this->getNameMaiden(),
			'phone_number' => $this->getPhoneNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'work_number' => $this->getWorkNumber(),
			'fax_number' => $this->getFaxNumber(),
			'email_address' => $this->getEmailAddress(),
			'birth_date' => $this->getBirthDate(),
			'gender' => $this->getGender(),
			'height' => $this->getHeight(),
			'weight' => $this->getWeight(),
			'eye_color' => $this->getEyeColor(),
			'hair_color' => $this->getHairColor(),
			'company_identification_type_id' => $this->getCompanyIdentificationTypeId(),
			'identification_value' => $this->getIdentificationValue(),
			'identification_expiration' => $this->getIdentificationExpiration(),
			'response_email' => $this->getResponseEmail(),
			'notes' => $this->getNotes(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'archived_by' => $this->getArchivedBy(),
			'archived_on' => $this->getArchivedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>