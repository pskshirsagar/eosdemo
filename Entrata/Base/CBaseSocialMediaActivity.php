<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSocialMediaActivity extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.social_media_activities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSocialMediaAccountDetailId;
	protected $m_strActivityOn;
	protected $m_intLikesCount;
	protected $m_intReachCount;
	protected $m_intEngagementCount;
	protected $m_intFollowersCount;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['social_media_account_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialMediaAccountDetailId', trim( $arrValues['social_media_account_detail_id'] ) ); elseif( isset( $arrValues['social_media_account_detail_id'] ) ) $this->setSocialMediaAccountDetailId( $arrValues['social_media_account_detail_id'] );
		if( isset( $arrValues['activity_on'] ) && $boolDirectSet ) $this->set( 'm_strActivityOn', trim( $arrValues['activity_on'] ) ); elseif( isset( $arrValues['activity_on'] ) ) $this->setActivityOn( $arrValues['activity_on'] );
		if( isset( $arrValues['likes_count'] ) && $boolDirectSet ) $this->set( 'm_intLikesCount', trim( $arrValues['likes_count'] ) ); elseif( isset( $arrValues['likes_count'] ) ) $this->setLikesCount( $arrValues['likes_count'] );
		if( isset( $arrValues['reach_count'] ) && $boolDirectSet ) $this->set( 'm_intReachCount', trim( $arrValues['reach_count'] ) ); elseif( isset( $arrValues['reach_count'] ) ) $this->setReachCount( $arrValues['reach_count'] );
		if( isset( $arrValues['engagement_count'] ) && $boolDirectSet ) $this->set( 'm_intEngagementCount', trim( $arrValues['engagement_count'] ) ); elseif( isset( $arrValues['engagement_count'] ) ) $this->setEngagementCount( $arrValues['engagement_count'] );
		if( isset( $arrValues['followers_count'] ) && $boolDirectSet ) $this->set( 'm_intFollowersCount', trim( $arrValues['followers_count'] ) ); elseif( isset( $arrValues['followers_count'] ) ) $this->setFollowersCount( $arrValues['followers_count'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSocialMediaAccountDetailId( $intSocialMediaAccountDetailId ) {
		$this->set( 'm_intSocialMediaAccountDetailId', CStrings::strToIntDef( $intSocialMediaAccountDetailId, NULL, false ) );
	}

	public function getSocialMediaAccountDetailId() {
		return $this->m_intSocialMediaAccountDetailId;
	}

	public function sqlSocialMediaAccountDetailId() {
		return ( true == isset( $this->m_intSocialMediaAccountDetailId ) ) ? ( string ) $this->m_intSocialMediaAccountDetailId : 'NULL';
	}

	public function setActivityOn( $strActivityOn ) {
		$this->set( 'm_strActivityOn', CStrings::strTrimDef( $strActivityOn, -1, NULL, true ) );
	}

	public function getActivityOn() {
		return $this->m_strActivityOn;
	}

	public function sqlActivityOn() {
		return ( true == isset( $this->m_strActivityOn ) ) ? '\'' . $this->m_strActivityOn . '\'' : 'NULL';
	}

	public function setLikesCount( $intLikesCount ) {
		$this->set( 'm_intLikesCount', CStrings::strToIntDef( $intLikesCount, NULL, false ) );
	}

	public function getLikesCount() {
		return $this->m_intLikesCount;
	}

	public function sqlLikesCount() {
		return ( true == isset( $this->m_intLikesCount ) ) ? ( string ) $this->m_intLikesCount : 'NULL';
	}

	public function setReachCount( $intReachCount ) {
		$this->set( 'm_intReachCount', CStrings::strToIntDef( $intReachCount, NULL, false ) );
	}

	public function getReachCount() {
		return $this->m_intReachCount;
	}

	public function sqlReachCount() {
		return ( true == isset( $this->m_intReachCount ) ) ? ( string ) $this->m_intReachCount : 'NULL';
	}

	public function setEngagementCount( $intEngagementCount ) {
		$this->set( 'm_intEngagementCount', CStrings::strToIntDef( $intEngagementCount, NULL, false ) );
	}

	public function getEngagementCount() {
		return $this->m_intEngagementCount;
	}

	public function sqlEngagementCount() {
		return ( true == isset( $this->m_intEngagementCount ) ) ? ( string ) $this->m_intEngagementCount : 'NULL';
	}

	public function setFollowersCount( $intFollowersCount ) {
		$this->set( 'm_intFollowersCount', CStrings::strToIntDef( $intFollowersCount, NULL, false ) );
	}

	public function getFollowersCount() {
		return $this->m_intFollowersCount;
	}

	public function sqlFollowersCount() {
		return ( true == isset( $this->m_intFollowersCount ) ) ? ( string ) $this->m_intFollowersCount : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, social_media_account_detail_id, activity_on, likes_count, reach_count, engagement_count, followers_count, disabled_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSocialMediaAccountDetailId() . ', ' .
						$this->sqlActivityOn() . ', ' .
						$this->sqlLikesCount() . ', ' .
						$this->sqlReachCount() . ', ' .
						$this->sqlEngagementCount() . ', ' .
						$this->sqlFollowersCount() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_media_account_detail_id = ' . $this->sqlSocialMediaAccountDetailId(). ',' ; } elseif( true == array_key_exists( 'SocialMediaAccountDetailId', $this->getChangedColumns() ) ) { $strSql .= ' social_media_account_detail_id = ' . $this->sqlSocialMediaAccountDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activity_on = ' . $this->sqlActivityOn(). ',' ; } elseif( true == array_key_exists( 'ActivityOn', $this->getChangedColumns() ) ) { $strSql .= ' activity_on = ' . $this->sqlActivityOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' likes_count = ' . $this->sqlLikesCount(). ',' ; } elseif( true == array_key_exists( 'LikesCount', $this->getChangedColumns() ) ) { $strSql .= ' likes_count = ' . $this->sqlLikesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reach_count = ' . $this->sqlReachCount(). ',' ; } elseif( true == array_key_exists( 'ReachCount', $this->getChangedColumns() ) ) { $strSql .= ' reach_count = ' . $this->sqlReachCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' engagement_count = ' . $this->sqlEngagementCount(). ',' ; } elseif( true == array_key_exists( 'EngagementCount', $this->getChangedColumns() ) ) { $strSql .= ' engagement_count = ' . $this->sqlEngagementCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' followers_count = ' . $this->sqlFollowersCount(). ',' ; } elseif( true == array_key_exists( 'FollowersCount', $this->getChangedColumns() ) ) { $strSql .= ' followers_count = ' . $this->sqlFollowersCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'social_media_account_detail_id' => $this->getSocialMediaAccountDetailId(),
			'activity_on' => $this->getActivityOn(),
			'likes_count' => $this->getLikesCount(),
			'reach_count' => $this->getReachCount(),
			'engagement_count' => $this->getEngagementCount(),
			'followers_count' => $this->getFollowersCount(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>