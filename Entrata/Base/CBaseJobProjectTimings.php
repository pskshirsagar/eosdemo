<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobProjectTimings
 * Do not add any new functions to this class.
 */

class CBaseJobProjectTimings extends CEosPluralBase {

	/**
	 * @return CJobProjectTiming[]
	 */
	public static function fetchJobProjectTimings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CJobProjectTiming::class, $objDatabase );
	}

	/**
	 * @return CJobProjectTiming
	 */
	public static function fetchJobProjectTiming( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobProjectTiming::class, $objDatabase );
	}

	public static function fetchJobProjectTimingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_project_timings', $objDatabase );
	}

	public static function fetchJobProjectTimingById( $intId, $objDatabase ) {
		return self::fetchJobProjectTiming( sprintf( 'SELECT * FROM job_project_timings WHERE id = %d', $intId ), $objDatabase );
	}

}
?>