<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerReferrals
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerReferrals extends CEosPluralBase {

	/**
	 * @return CCustomerReferral[]
	 */
	public static function fetchCustomerReferrals( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerReferral', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerReferral
	 */
	public static function fetchCustomerReferral( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerReferral', $objDatabase );
	}

	public static function fetchCustomerReferralCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_referrals', $objDatabase );
	}

	public static function fetchCustomerReferralByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerReferral( sprintf( 'SELECT * FROM customer_referrals WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerReferralsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerReferrals( sprintf( 'SELECT * FROM customer_referrals WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerReferralsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerReferrals( sprintf( 'SELECT * FROM customer_referrals WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerReferralsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerReferrals( sprintf( 'SELECT * FROM customer_referrals WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerReferralsByCustomerApplicationIdByCid( $intCustomerApplicationId, $intCid, $objDatabase ) {
		return self::fetchCustomerReferrals( sprintf( 'SELECT * FROM customer_referrals WHERE customer_application_id = %d AND cid = %d', ( int ) $intCustomerApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerReferralsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchCustomerReferrals( sprintf( 'SELECT * FROM customer_referrals WHERE ar_transaction_id = %d AND cid = %d', ( int ) $intArTransactionId, ( int ) $intCid ), $objDatabase );
	}

}
?>