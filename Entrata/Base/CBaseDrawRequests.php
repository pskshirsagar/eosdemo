<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDrawRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDrawRequests extends CEosPluralBase {

	/**
	 * @return CDrawRequest[]
	 */
	public static function fetchDrawRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDrawRequest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDrawRequest
	 */
	public static function fetchDrawRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDrawRequest', $objDatabase );
	}

	public static function fetchDrawRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'draw_requests', $objDatabase );
	}

	public static function fetchDrawRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDrawRequest( sprintf( 'SELECT * FROM draw_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE ar_transaction_id = %d AND cid = %d', ( int ) $intArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDrawRequestsByArDepositTransactionIdByCid( $intArDepositTransactionId, $intCid, $objDatabase ) {
		return self::fetchDrawRequests( sprintf( 'SELECT * FROM draw_requests WHERE ar_deposit_transaction_id = %d AND cid = %d', ( int ) $intArDepositTransactionId, ( int ) $intCid ), $objDatabase );
	}

}
?>