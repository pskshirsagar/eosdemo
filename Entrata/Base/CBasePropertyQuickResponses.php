<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyQuickResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyQuickResponses extends CEosPluralBase {

	/**
	 * @return CPropertyQuickResponse[]
	 */
	public static function fetchPropertyQuickResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyQuickResponse', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyQuickResponse
	 */
	public static function fetchPropertyQuickResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyQuickResponse', $objDatabase );
	}

	public static function fetchPropertyQuickResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_quick_responses', $objDatabase );
	}

	public static function fetchPropertyQuickResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyQuickResponse( sprintf( 'SELECT * FROM property_quick_responses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyQuickResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyQuickResponses( sprintf( 'SELECT * FROM property_quick_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyQuickResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyQuickResponses( sprintf( 'SELECT * FROM property_quick_responses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyQuickResponsesByQuickResponseIdByCid( $intQuickResponseId, $intCid, $objDatabase ) {
		return self::fetchPropertyQuickResponses( sprintf( 'SELECT * FROM property_quick_responses WHERE quick_response_id = %d AND cid = %d', ( int ) $intQuickResponseId, ( int ) $intCid ), $objDatabase );
	}

}
?>