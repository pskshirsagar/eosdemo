<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTerms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseTerms extends CEosPluralBase {

	/**
	 * @return CLeaseTerm[]
	 */
	public static function fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseTerm::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseTerm
	 */
	public static function fetchLeaseTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseTerm::class, $objDatabase );
	}

	public static function fetchLeaseTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_terms', $objDatabase );
	}

	public static function fetchLeaseTermByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerm( sprintf( 'SELECT * FROM lease_terms WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByLeaseTermStructureIdByCid( $intLeaseTermStructureId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE lease_term_structure_id = %d AND cid = %d', $intLeaseTermStructureId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByDefaultLeaseTermIdByCid( $intDefaultLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE default_lease_term_id = %d AND cid = %d', $intDefaultLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByMappingPltIdByCid( $intMappingPltId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE mapping_plt_id = %d AND cid = %d', $intMappingPltId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseTermsByLeaseTermTypeIdByCid( $intLeaseTermTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseTerms( sprintf( 'SELECT * FROM lease_terms WHERE lease_term_type_id = %d AND cid = %d', $intLeaseTermTypeId, $intCid ), $objDatabase );
	}

}
?>