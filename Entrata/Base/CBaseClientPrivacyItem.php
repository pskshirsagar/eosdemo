<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClientPrivacyItem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.client_privacy_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPrivacyItemId;
	protected $m_intProductReferenceId;
	protected $m_strToken;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intProductId;
	protected $m_intPrivacyItemTypeId;
	protected $m_intPrivacyItemGroupId;
	protected $m_boolIsDefaultState;
	protected $m_boolIsStateEditable;
	protected $m_boolIsDetailsEditable;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDefaultState = false;
		$this->m_boolIsStateEditable = false;
		$this->m_boolIsDetailsEditable = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['privacy_item_id'] ) && $boolDirectSet ) $this->set( 'm_intPrivacyItemId', trim( $arrValues['privacy_item_id'] ) ); elseif( isset( $arrValues['privacy_item_id'] ) ) $this->setPrivacyItemId( $arrValues['privacy_item_id'] );
		if( isset( $arrValues['product_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intProductReferenceId', trim( $arrValues['product_reference_id'] ) ); elseif( isset( $arrValues['product_reference_id'] ) ) $this->setProductReferenceId( $arrValues['product_reference_id'] );
		if( isset( $arrValues['token'] ) && $boolDirectSet ) $this->set( 'm_strToken', trim( stripcslashes( $arrValues['token'] ) ) ); elseif( isset( $arrValues['token'] ) ) $this->setToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['token'] ) : $arrValues['token'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['product_id'] ) && $boolDirectSet ) $this->set( 'm_intProductId', trim( $arrValues['product_id'] ) ); elseif( isset( $arrValues['product_id'] ) ) $this->setProductId( $arrValues['product_id'] );
		if( isset( $arrValues['privacy_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPrivacyItemTypeId', trim( $arrValues['privacy_item_type_id'] ) ); elseif( isset( $arrValues['privacy_item_type_id'] ) ) $this->setPrivacyItemTypeId( $arrValues['privacy_item_type_id'] );
		if( isset( $arrValues['privacy_item_group_id'] ) && $boolDirectSet ) $this->set( 'm_intPrivacyItemGroupId', trim( $arrValues['privacy_item_group_id'] ) ); elseif( isset( $arrValues['privacy_item_group_id'] ) ) $this->setPrivacyItemGroupId( $arrValues['privacy_item_group_id'] );
		if( isset( $arrValues['is_default_state'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefaultState', trim( stripcslashes( $arrValues['is_default_state'] ) ) ); elseif( isset( $arrValues['is_default_state'] ) ) $this->setIsDefaultState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default_state'] ) : $arrValues['is_default_state'] );
		if( isset( $arrValues['is_state_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsStateEditable', trim( stripcslashes( $arrValues['is_state_editable'] ) ) ); elseif( isset( $arrValues['is_state_editable'] ) ) $this->setIsStateEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_state_editable'] ) : $arrValues['is_state_editable'] );
		if( isset( $arrValues['is_details_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsDetailsEditable', trim( stripcslashes( $arrValues['is_details_editable'] ) ) ); elseif( isset( $arrValues['is_details_editable'] ) ) $this->setIsDetailsEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_details_editable'] ) : $arrValues['is_details_editable'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPrivacyItemId( $intPrivacyItemId ) {
		$this->set( 'm_intPrivacyItemId', CStrings::strToIntDef( $intPrivacyItemId, NULL, false ) );
	}

	public function getPrivacyItemId() {
		return $this->m_intPrivacyItemId;
	}

	public function sqlPrivacyItemId() {
		return ( true == isset( $this->m_intPrivacyItemId ) ) ? ( string ) $this->m_intPrivacyItemId : 'NULL';
	}

	public function setProductReferenceId( $intProductReferenceId ) {
		$this->set( 'm_intProductReferenceId', CStrings::strToIntDef( $intProductReferenceId, NULL, false ) );
	}

	public function getProductReferenceId() {
		return $this->m_intProductReferenceId;
	}

	public function sqlProductReferenceId() {
		return ( true == isset( $this->m_intProductReferenceId ) ) ? ( string ) $this->m_intProductReferenceId : 'NULL';
	}

	public function setToken( $strToken ) {
		$this->set( 'm_strToken', CStrings::strTrimDef( $strToken, 100, NULL, true ) );
	}

	public function getToken() {
		return $this->m_strToken;
	}

	public function sqlToken() {
		return ( true == isset( $this->m_strToken ) ) ? '\'' . addslashes( $this->m_strToken ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setProductId( $intProductId ) {
		$this->set( 'm_intProductId', CStrings::strToIntDef( $intProductId, NULL, false ) );
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function sqlProductId() {
		return ( true == isset( $this->m_intProductId ) ) ? ( string ) $this->m_intProductId : 'NULL';
	}

	public function setPrivacyItemTypeId( $intPrivacyItemTypeId ) {
		$this->set( 'm_intPrivacyItemTypeId', CStrings::strToIntDef( $intPrivacyItemTypeId, NULL, false ) );
	}

	public function getPrivacyItemTypeId() {
		return $this->m_intPrivacyItemTypeId;
	}

	public function sqlPrivacyItemTypeId() {
		return ( true == isset( $this->m_intPrivacyItemTypeId ) ) ? ( string ) $this->m_intPrivacyItemTypeId : 'NULL';
	}

	public function setPrivacyItemGroupId( $intPrivacyItemGroupId ) {
		$this->set( 'm_intPrivacyItemGroupId', CStrings::strToIntDef( $intPrivacyItemGroupId, NULL, false ) );
	}

	public function getPrivacyItemGroupId() {
		return $this->m_intPrivacyItemGroupId;
	}

	public function sqlPrivacyItemGroupId() {
		return ( true == isset( $this->m_intPrivacyItemGroupId ) ) ? ( string ) $this->m_intPrivacyItemGroupId : 'NULL';
	}

	public function setIsDefaultState( $boolIsDefaultState ) {
		$this->set( 'm_boolIsDefaultState', CStrings::strToBool( $boolIsDefaultState ) );
	}

	public function getIsDefaultState() {
		return $this->m_boolIsDefaultState;
	}

	public function sqlIsDefaultState() {
		return ( true == isset( $this->m_boolIsDefaultState ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefaultState ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsStateEditable( $boolIsStateEditable ) {
		$this->set( 'm_boolIsStateEditable', CStrings::strToBool( $boolIsStateEditable ) );
	}

	public function getIsStateEditable() {
		return $this->m_boolIsStateEditable;
	}

	public function sqlIsStateEditable() {
		return ( true == isset( $this->m_boolIsStateEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStateEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDetailsEditable( $boolIsDetailsEditable ) {
		$this->set( 'm_boolIsDetailsEditable', CStrings::strToBool( $boolIsDetailsEditable ) );
	}

	public function getIsDetailsEditable() {
		return $this->m_boolIsDetailsEditable;
	}

	public function sqlIsDetailsEditable() {
		return ( true == isset( $this->m_boolIsDetailsEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDetailsEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, privacy_item_id, product_reference_id, token, name, description, product_id, privacy_item_type_id, privacy_item_group_id, is_default_state, is_state_editable, is_details_editable, details, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPrivacyItemId() . ', ' .
						$this->sqlProductReferenceId() . ', ' .
						$this->sqlToken() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlProductId() . ', ' .
						$this->sqlPrivacyItemTypeId() . ', ' .
						$this->sqlPrivacyItemGroupId() . ', ' .
						$this->sqlIsDefaultState() . ', ' .
						$this->sqlIsStateEditable() . ', ' .
						$this->sqlIsDetailsEditable() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' privacy_item_id = ' . $this->sqlPrivacyItemId(). ',' ; } elseif( true == array_key_exists( 'PrivacyItemId', $this->getChangedColumns() ) ) { $strSql .= ' privacy_item_id = ' . $this->sqlPrivacyItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_reference_id = ' . $this->sqlProductReferenceId(). ',' ; } elseif( true == array_key_exists( 'ProductReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' product_reference_id = ' . $this->sqlProductReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token = ' . $this->sqlToken(). ',' ; } elseif( true == array_key_exists( 'Token', $this->getChangedColumns() ) ) { $strSql .= ' token = ' . $this->sqlToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_id = ' . $this->sqlProductId(). ',' ; } elseif( true == array_key_exists( 'ProductId', $this->getChangedColumns() ) ) { $strSql .= ' product_id = ' . $this->sqlProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' privacy_item_type_id = ' . $this->sqlPrivacyItemTypeId(). ',' ; } elseif( true == array_key_exists( 'PrivacyItemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' privacy_item_type_id = ' . $this->sqlPrivacyItemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' privacy_item_group_id = ' . $this->sqlPrivacyItemGroupId(). ',' ; } elseif( true == array_key_exists( 'PrivacyItemGroupId', $this->getChangedColumns() ) ) { $strSql .= ' privacy_item_group_id = ' . $this->sqlPrivacyItemGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_state = ' . $this->sqlIsDefaultState(). ',' ; } elseif( true == array_key_exists( 'IsDefaultState', $this->getChangedColumns() ) ) { $strSql .= ' is_default_state = ' . $this->sqlIsDefaultState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_state_editable = ' . $this->sqlIsStateEditable(). ',' ; } elseif( true == array_key_exists( 'IsStateEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_state_editable = ' . $this->sqlIsStateEditable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_details_editable = ' . $this->sqlIsDetailsEditable(). ',' ; } elseif( true == array_key_exists( 'IsDetailsEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_details_editable = ' . $this->sqlIsDetailsEditable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'privacy_item_id' => $this->getPrivacyItemId(),
			'product_reference_id' => $this->getProductReferenceId(),
			'token' => $this->getToken(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'product_id' => $this->getProductId(),
			'privacy_item_type_id' => $this->getPrivacyItemTypeId(),
			'privacy_item_group_id' => $this->getPrivacyItemGroupId(),
			'is_default_state' => $this->getIsDefaultState(),
			'is_state_editable' => $this->getIsStateEditable(),
			'is_details_editable' => $this->getIsDetailsEditable(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>