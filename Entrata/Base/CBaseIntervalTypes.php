<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntervalTypes
 * Do not add any new functions to this class.
 */

class CBaseIntervalTypes extends CEosPluralBase {

	/**
	 * @return CIntervalType[]
	 */
	public static function fetchIntervalTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntervalType::class, $objDatabase );
	}

	/**
	 * @return CIntervalType
	 */
	public static function fetchIntervalType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntervalType::class, $objDatabase );
	}

	public static function fetchIntervalTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'interval_types', $objDatabase );
	}

	public static function fetchIntervalTypeById( $intId, $objDatabase ) {
		return self::fetchIntervalType( sprintf( 'SELECT * FROM interval_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>