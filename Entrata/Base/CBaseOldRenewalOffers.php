<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COldRenewalOffers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOldRenewalOffers extends CEosPluralBase {

	/**
	 * @return COldRenewalOffer[]
	 */
	public static function fetchOldRenewalOffers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COldRenewalOffer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COldRenewalOffer
	 */
	public static function fetchOldRenewalOffer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COldRenewalOffer', $objDatabase );
	}

	public static function fetchOldRenewalOfferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'old_renewal_offers', $objDatabase );
	}

	public static function fetchOldRenewalOfferByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffer( sprintf( 'SELECT * FROM old_renewal_offers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByCid( $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByPropertyRenewalOfferSettingIdByCid( $intPropertyRenewalOfferSettingId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE property_renewal_offer_setting_id = %d AND cid = %d', ( int ) $intPropertyRenewalOfferSettingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByPropertyLeasePeriodIdByCid( $intPropertyLeasePeriodId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE property_lease_period_id = %d AND cid = %d', ( int ) $intPropertyLeasePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOldRenewalOffersByConcessionArCodeIdByCid( $intConcessionArCodeId, $intCid, $objDatabase ) {
		return self::fetchOldRenewalOffers( sprintf( 'SELECT * FROM old_renewal_offers WHERE concession_ar_code_id = %d AND cid = %d', ( int ) $intConcessionArCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>