<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIpsChecklistQuestionLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ips_checklist_question_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intIpsChecklistQuestionId;
	protected $m_intIpsChecklistQuestionLogTypeId;
	protected $m_strValue;
	protected $m_boolIsClientSpecific;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ips_checklist_question_id'] ) && $boolDirectSet ) $this->set( 'm_intIpsChecklistQuestionId', trim( $arrValues['ips_checklist_question_id'] ) ); elseif( isset( $arrValues['ips_checklist_question_id'] ) ) $this->setIpsChecklistQuestionId( $arrValues['ips_checklist_question_id'] );
		if( isset( $arrValues['ips_checklist_question_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIpsChecklistQuestionLogTypeId', trim( $arrValues['ips_checklist_question_log_type_id'] ) ); elseif( isset( $arrValues['ips_checklist_question_log_type_id'] ) ) $this->setIpsChecklistQuestionLogTypeId( $arrValues['ips_checklist_question_log_type_id'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( $arrValues['value'] ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( $arrValues['value'] );
		if( isset( $arrValues['is_client_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientSpecific', trim( stripcslashes( $arrValues['is_client_specific'] ) ) ); elseif( isset( $arrValues['is_client_specific'] ) ) $this->setIsClientSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_specific'] ) : $arrValues['is_client_specific'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setIpsChecklistQuestionId( $intIpsChecklistQuestionId ) {
		$this->set( 'm_intIpsChecklistQuestionId', CStrings::strToIntDef( $intIpsChecklistQuestionId, NULL, false ) );
	}

	public function getIpsChecklistQuestionId() {
		return $this->m_intIpsChecklistQuestionId;
	}

	public function sqlIpsChecklistQuestionId() {
		return ( true == isset( $this->m_intIpsChecklistQuestionId ) ) ? ( string ) $this->m_intIpsChecklistQuestionId : 'NULL';
	}

	public function setIpsChecklistQuestionLogTypeId( $intIpsChecklistQuestionLogTypeId ) {
		$this->set( 'm_intIpsChecklistQuestionLogTypeId', CStrings::strToIntDef( $intIpsChecklistQuestionLogTypeId, NULL, false ) );
	}

	public function getIpsChecklistQuestionLogTypeId() {
		return $this->m_intIpsChecklistQuestionLogTypeId;
	}

	public function sqlIpsChecklistQuestionLogTypeId() {
		return ( true == isset( $this->m_intIpsChecklistQuestionLogTypeId ) ) ? ( string ) $this->m_intIpsChecklistQuestionLogTypeId : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValue ) : '\'' . addslashes( $this->m_strValue ) . '\'' ) : 'NULL';
	}

	public function setIsClientSpecific( $boolIsClientSpecific ) {
		$this->set( 'm_boolIsClientSpecific', CStrings::strToBool( $boolIsClientSpecific ) );
	}

	public function getIsClientSpecific() {
		return $this->m_boolIsClientSpecific;
	}

	public function sqlIsClientSpecific() {
		return ( true == isset( $this->m_boolIsClientSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ips_checklist_question_id, ips_checklist_question_log_type_id, value, is_client_specific, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlIpsChecklistQuestionId() . ', ' .
						$this->sqlIpsChecklistQuestionLogTypeId() . ', ' .
						$this->sqlValue() . ', ' .
						$this->sqlIsClientSpecific() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ips_checklist_question_id = ' . $this->sqlIpsChecklistQuestionId(). ',' ; } elseif( true == array_key_exists( 'IpsChecklistQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' ips_checklist_question_id = ' . $this->sqlIpsChecklistQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ips_checklist_question_log_type_id = ' . $this->sqlIpsChecklistQuestionLogTypeId(). ',' ; } elseif( true == array_key_exists( 'IpsChecklistQuestionLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ips_checklist_question_log_type_id = ' . $this->sqlIpsChecklistQuestionLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue(). ',' ; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() ; } elseif( true == array_key_exists( 'IsClientSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ips_checklist_question_id' => $this->getIpsChecklistQuestionId(),
			'ips_checklist_question_log_type_id' => $this->getIpsChecklistQuestionLogTypeId(),
			'value' => $this->getValue(),
			'is_client_specific' => $this->getIsClientSpecific(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>