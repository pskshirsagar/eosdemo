<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuotePets extends CEosPluralBase {

    const TABLE_QUOTE_PETS = 'public.quote_pets';

    public static function fetchQuotePets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CQuotePet', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchQuotePet( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CQuotePet', $objDatabase );
    }

    public static function fetchQuotePetCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'quote_pets', $objDatabase );
    }

    public static function fetchQuotePetByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchQuotePet( sprintf( 'SELECT * FROM quote_pets WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchQuotePetsByCid( $intCid, $objDatabase ) {
        return self::fetchQuotePets( sprintf( 'SELECT * FROM quote_pets WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchQuotePetsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
        return self::fetchQuotePets( sprintf( 'SELECT * FROM quote_pets WHERE quote_id = %d AND cid = %d', (int) $intQuoteId, (int) $intCid ), $objDatabase );
    }

    public static function fetchQuotePetsByPetTypeIdByCid( $intPetTypeId, $intCid, $objDatabase ) {
        return self::fetchQuotePets( sprintf( 'SELECT * FROM quote_pets WHERE pet_type_id = %d AND cid = %d', (int) $intPetTypeId, (int) $intCid ), $objDatabase );
    }

}
?>