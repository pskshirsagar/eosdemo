<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyScheduledCalls
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyScheduledCalls extends CEosPluralBase {

	/**
	 * @return CPropertyScheduledCall[]
	 */
	public static function fetchPropertyScheduledCalls( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyScheduledCall', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyScheduledCall
	 */
	public static function fetchPropertyScheduledCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyScheduledCall', $objDatabase );
	}

	public static function fetchPropertyScheduledCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_scheduled_calls', $objDatabase );
	}

	public static function fetchPropertyScheduledCallByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledCall( sprintf( 'SELECT * FROM property_scheduled_calls WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledCallsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledCalls( sprintf( 'SELECT * FROM property_scheduled_calls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledCallsByScheduledCallIdByCid( $intScheduledCallId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledCalls( sprintf( 'SELECT * FROM property_scheduled_calls WHERE scheduled_call_id = %d AND cid = %d', ( int ) $intScheduledCallId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledCallsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledCalls( sprintf( 'SELECT * FROM property_scheduled_calls WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>