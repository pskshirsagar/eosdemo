<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOverrideRentsLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueOverrideRentsLogs extends CEosPluralBase {

	/**
	 * @return CRevenueOverrideRentsLog[]
	 */
	public static function fetchRevenueOverrideRentsLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenueOverrideRentsLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueOverrideRentsLog
	 */
	public static function fetchRevenueOverrideRentsLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueOverrideRentsLog::class, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_override_rents_logs', $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLog( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsLogsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRentsLogs( sprintf( 'SELECT * FROM revenue_override_rents_logs WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>