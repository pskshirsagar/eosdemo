<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeaderScheduleLog extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_header_schedule_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlHeaderScheduleId;
	protected $m_intFrequencyId;
	protected $m_intFrequencyInterval;
	protected $m_intDayOfWeek;
	protected $m_strAction;
	protected $m_strDaysOfMonth;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intNumberOfOccurrences;
	protected $m_strLastPostedDate;
	protected $m_strNextPostDate;
	protected $m_intIsReverseNextMonth;
	protected $m_intIsPaused;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReverseNextMonth = '0';
		$this->m_intIsPaused = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_header_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderScheduleId', trim( $arrValues['gl_header_schedule_id'] ) ); elseif( isset( $arrValues['gl_header_schedule_id'] ) ) $this->setGlHeaderScheduleId( $arrValues['gl_header_schedule_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['frequency_interval'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyInterval', trim( $arrValues['frequency_interval'] ) ); elseif( isset( $arrValues['frequency_interval'] ) ) $this->setFrequencyInterval( $arrValues['frequency_interval'] );
		if( isset( $arrValues['day_of_week'] ) && $boolDirectSet ) $this->set( 'm_intDayOfWeek', trim( $arrValues['day_of_week'] ) ); elseif( isset( $arrValues['day_of_week'] ) ) $this->setDayOfWeek( $arrValues['day_of_week'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['days_of_month'] ) && $boolDirectSet ) $this->set( 'm_strDaysOfMonth', trim( stripcslashes( $arrValues['days_of_month'] ) ) ); elseif( isset( $arrValues['days_of_month'] ) ) $this->setDaysOfMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['days_of_month'] ) : $arrValues['days_of_month'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['number_of_occurrences'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOccurrences', trim( $arrValues['number_of_occurrences'] ) ); elseif( isset( $arrValues['number_of_occurrences'] ) ) $this->setNumberOfOccurrences( $arrValues['number_of_occurrences'] );
		if( isset( $arrValues['last_posted_date'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedDate', trim( $arrValues['last_posted_date'] ) ); elseif( isset( $arrValues['last_posted_date'] ) ) $this->setLastPostedDate( $arrValues['last_posted_date'] );
		if( isset( $arrValues['next_post_date'] ) && $boolDirectSet ) $this->set( 'm_strNextPostDate', trim( $arrValues['next_post_date'] ) ); elseif( isset( $arrValues['next_post_date'] ) ) $this->setNextPostDate( $arrValues['next_post_date'] );
		if( isset( $arrValues['is_reverse_next_month'] ) && $boolDirectSet ) $this->set( 'm_intIsReverseNextMonth', trim( $arrValues['is_reverse_next_month'] ) ); elseif( isset( $arrValues['is_reverse_next_month'] ) ) $this->setIsReverseNextMonth( $arrValues['is_reverse_next_month'] );
		if( isset( $arrValues['is_paused'] ) && $boolDirectSet ) $this->set( 'm_intIsPaused', trim( $arrValues['is_paused'] ) ); elseif( isset( $arrValues['is_paused'] ) ) $this->setIsPaused( $arrValues['is_paused'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlHeaderScheduleId( $intGlHeaderScheduleId ) {
		$this->set( 'm_intGlHeaderScheduleId', CStrings::strToIntDef( $intGlHeaderScheduleId, NULL, false ) );
	}

	public function getGlHeaderScheduleId() {
		return $this->m_intGlHeaderScheduleId;
	}

	public function sqlGlHeaderScheduleId() {
		return ( true == isset( $this->m_intGlHeaderScheduleId ) ) ? ( string ) $this->m_intGlHeaderScheduleId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->set( 'm_intFrequencyInterval', CStrings::strToIntDef( $intFrequencyInterval, NULL, false ) );
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function sqlFrequencyInterval() {
		return ( true == isset( $this->m_intFrequencyInterval ) ) ? ( string ) $this->m_intFrequencyInterval : 'NULL';
	}

	public function setDayOfWeek( $intDayOfWeek ) {
		$this->set( 'm_intDayOfWeek', CStrings::strToIntDef( $intDayOfWeek, NULL, false ) );
	}

	public function getDayOfWeek() {
		return $this->m_intDayOfWeek;
	}

	public function sqlDayOfWeek() {
		return ( true == isset( $this->m_intDayOfWeek ) ) ? ( string ) $this->m_intDayOfWeek : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setDaysOfMonth( $strDaysOfMonth ) {
		$this->set( 'm_strDaysOfMonth', CStrings::strTrimDef( $strDaysOfMonth, 255, NULL, true ) );
	}

	public function getDaysOfMonth() {
		return $this->m_strDaysOfMonth;
	}

	public function sqlDaysOfMonth() {
		return ( true == isset( $this->m_strDaysOfMonth ) ) ? '\'' . addslashes( $this->m_strDaysOfMonth ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->set( 'm_intNumberOfOccurrences', CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false ) );
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function sqlNumberOfOccurrences() {
		return ( true == isset( $this->m_intNumberOfOccurrences ) ) ? ( string ) $this->m_intNumberOfOccurrences : 'NULL';
	}

	public function setLastPostedDate( $strLastPostedDate ) {
		$this->set( 'm_strLastPostedDate', CStrings::strTrimDef( $strLastPostedDate, -1, NULL, true ) );
	}

	public function getLastPostedDate() {
		return $this->m_strLastPostedDate;
	}

	public function sqlLastPostedDate() {
		return ( true == isset( $this->m_strLastPostedDate ) ) ? '\'' . $this->m_strLastPostedDate . '\'' : 'NULL';
	}

	public function setNextPostDate( $strNextPostDate ) {
		$this->set( 'm_strNextPostDate', CStrings::strTrimDef( $strNextPostDate, -1, NULL, true ) );
	}

	public function getNextPostDate() {
		return $this->m_strNextPostDate;
	}

	public function sqlNextPostDate() {
		return ( true == isset( $this->m_strNextPostDate ) ) ? '\'' . $this->m_strNextPostDate . '\'' : 'NULL';
	}

	public function setIsReverseNextMonth( $intIsReverseNextMonth ) {
		$this->set( 'm_intIsReverseNextMonth', CStrings::strToIntDef( $intIsReverseNextMonth, NULL, false ) );
	}

	public function getIsReverseNextMonth() {
		return $this->m_intIsReverseNextMonth;
	}

	public function sqlIsReverseNextMonth() {
		return ( true == isset( $this->m_intIsReverseNextMonth ) ) ? ( string ) $this->m_intIsReverseNextMonth : '0';
	}

	public function setIsPaused( $intIsPaused ) {
		$this->set( 'm_intIsPaused', CStrings::strToIntDef( $intIsPaused, NULL, false ) );
	}

	public function getIsPaused() {
		return $this->m_intIsPaused;
	}

	public function sqlIsPaused() {
		return ( true == isset( $this->m_intIsPaused ) ) ? ( string ) $this->m_intIsPaused : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_header_schedule_id, frequency_id, frequency_interval, day_of_week, action, days_of_month, start_date, end_date, number_of_occurrences, last_posted_date, next_post_date, is_reverse_next_month, is_paused, approved_by, approved_on, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlGlHeaderScheduleId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlFrequencyInterval() . ', ' .
 						$this->sqlDayOfWeek() . ', ' .
 						$this->sqlAction() . ', ' .
 						$this->sqlDaysOfMonth() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlNumberOfOccurrences() . ', ' .
 						$this->sqlLastPostedDate() . ', ' .
 						$this->sqlNextPostDate() . ', ' .
 						$this->sqlIsReverseNextMonth() . ', ' .
 						$this->sqlIsPaused() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDisabledBy() . ', ' .
 						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId() . ','; } elseif( true == array_key_exists( 'GlHeaderScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; } elseif( true == array_key_exists( 'FrequencyInterval', $this->getChangedColumns() ) ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek() . ','; } elseif( true == array_key_exists( 'DayOfWeek', $this->getChangedColumns() ) ) { $strSql .= ' day_of_week = ' . $this->sqlDayOfWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction() . ','; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_of_month = ' . $this->sqlDaysOfMonth() . ','; } elseif( true == array_key_exists( 'DaysOfMonth', $this->getChangedColumns() ) ) { $strSql .= ' days_of_month = ' . $this->sqlDaysOfMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; } elseif( true == array_key_exists( 'NumberOfOccurrences', $this->getChangedColumns() ) ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; } elseif( true == array_key_exists( 'LastPostedDate', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; } elseif( true == array_key_exists( 'NextPostDate', $this->getChangedColumns() ) ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reverse_next_month = ' . $this->sqlIsReverseNextMonth() . ','; } elseif( true == array_key_exists( 'IsReverseNextMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_reverse_next_month = ' . $this->sqlIsReverseNextMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused() . ','; } elseif( true == array_key_exists( 'IsPaused', $this->getChangedColumns() ) ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_header_schedule_id' => $this->getGlHeaderScheduleId(),
			'frequency_id' => $this->getFrequencyId(),
			'frequency_interval' => $this->getFrequencyInterval(),
			'day_of_week' => $this->getDayOfWeek(),
			'action' => $this->getAction(),
			'days_of_month' => $this->getDaysOfMonth(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'number_of_occurrences' => $this->getNumberOfOccurrences(),
			'last_posted_date' => $this->getLastPostedDate(),
			'next_post_date' => $this->getNextPostDate(),
			'is_reverse_next_month' => $this->getIsReverseNextMonth(),
			'is_paused' => $this->getIsPaused(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>