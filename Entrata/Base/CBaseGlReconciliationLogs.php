<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationLogs extends CEosPluralBase {

	/**
	 * @return CGlReconciliationLog[]
	 */
	public static function fetchGlReconciliationLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlReconciliationLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlReconciliationLog
	 */
	public static function fetchGlReconciliationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlReconciliationLog', $objDatabase );
	}

	public static function fetchGlReconciliationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_reconciliation_logs', $objDatabase );
	}

	public static function fetchGlReconciliationLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationLog( sprintf( 'SELECT * FROM gl_reconciliation_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationLogsByCid( $intCid, $objDatabase ) {
		return self::fetchGlReconciliationLogs( sprintf( 'SELECT * FROM gl_reconciliation_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationLogsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationLogs( sprintf( 'SELECT * FROM gl_reconciliation_logs WHERE gl_reconciliation_id = %d AND cid = %d', ( int ) $intGlReconciliationId, ( int ) $intCid ), $objDatabase );
	}

}
?>