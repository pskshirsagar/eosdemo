<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenanceBoards
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenanceBoards extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenanceBoard[]
	 */
	public static function fetchDefaultMaintenanceBoards( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenanceBoard::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenanceBoard
	 */
	public static function fetchDefaultMaintenanceBoard( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenanceBoard::class, $objDatabase );
	}

	public static function fetchDefaultMaintenanceBoardCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_boards', $objDatabase );
	}

	public static function fetchDefaultMaintenanceBoardById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenanceBoard( sprintf( 'SELECT * FROM default_maintenance_boards WHERE id = %d', $intId ), $objDatabase );
	}

}
?>