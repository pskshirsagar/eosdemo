<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationChanges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationChanges extends CEosPluralBase {

	/**
	 * @return CApplicantApplicationChange[]
	 */
	public static function fetchApplicantApplicationChanges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantApplicationChange', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantApplicationChange
	 */
	public static function fetchApplicantApplicationChange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantApplicationChange', $objDatabase );
	}

	public static function fetchApplicantApplicationChangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_application_changes', $objDatabase );
	}

	public static function fetchApplicantApplicationChangeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChange( sprintf( 'SELECT * FROM applicant_application_changes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChanges( sprintf( 'SELECT * FROM applicant_application_changes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByEventIdByCid( $intEventId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChanges( sprintf( 'SELECT * FROM applicant_application_changes WHERE event_id = %d AND cid = %d', ( int ) $intEventId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChanges( sprintf( 'SELECT * FROM applicant_application_changes WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChanges( sprintf( 'SELECT * FROM applicant_application_changes WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByApplicantApplicationChangeTypeIdByCid( $intApplicantApplicationChangeTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationChanges( sprintf( 'SELECT * FROM applicant_application_changes WHERE applicant_application_change_type_id = %d AND cid = %d', ( int ) $intApplicantApplicationChangeTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>