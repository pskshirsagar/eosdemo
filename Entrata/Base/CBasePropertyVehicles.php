<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyVehicles
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyVehicles extends CEosPluralBase {

	/**
	 * @return CPropertyVehicle[]
	 */
	public static function fetchPropertyVehicles( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyVehicle', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyVehicle
	 */
	public static function fetchPropertyVehicle( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyVehicle', $objDatabase );
	}

	public static function fetchPropertyVehicleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_vehicles', $objDatabase );
	}

	public static function fetchPropertyVehicleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyVehicle( sprintf( 'SELECT * FROM property_vehicles WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyVehiclesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyVehicles( sprintf( 'SELECT * FROM property_vehicles WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyVehiclesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyVehicles( sprintf( 'SELECT * FROM property_vehicles WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyVehiclesByVehicleIdByCid( $intVehicleId, $intCid, $objDatabase ) {
		return self::fetchPropertyVehicles( sprintf( 'SELECT * FROM property_vehicles WHERE vehicle_id = %d AND cid = %d', ( int ) $intVehicleId, ( int ) $intCid ), $objDatabase );
	}

}
?>