<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataExportFrequency extends CEosSingularBase {

	const TABLE_NAME = 'public.data_export_frequencies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDataExportFrequencyTypeId;
	protected $m_strEvery;
	protected $m_strScheduledDays;
	protected $m_strStartOn;
	protected $m_intStartOnNextScheduledDay;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intStartOnNextScheduledDay = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['data_export_frequency_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportFrequencyTypeId', trim( $arrValues['data_export_frequency_type_id'] ) ); elseif( isset( $arrValues['data_export_frequency_type_id'] ) ) $this->setDataExportFrequencyTypeId( $arrValues['data_export_frequency_type_id'] );
		if( isset( $arrValues['every'] ) && $boolDirectSet ) $this->set( 'm_strEvery', trim( stripcslashes( $arrValues['every'] ) ) ); elseif( isset( $arrValues['every'] ) ) $this->setEvery( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['every'] ) : $arrValues['every'] );
		if( isset( $arrValues['scheduled_days'] ) && $boolDirectSet ) $this->set( 'm_strScheduledDays', trim( stripcslashes( $arrValues['scheduled_days'] ) ) ); elseif( isset( $arrValues['scheduled_days'] ) ) $this->setScheduledDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_days'] ) : $arrValues['scheduled_days'] );
		if( isset( $arrValues['start_on'] ) && $boolDirectSet ) $this->set( 'm_strStartOn', trim( $arrValues['start_on'] ) ); elseif( isset( $arrValues['start_on'] ) ) $this->setStartOn( $arrValues['start_on'] );
		if( isset( $arrValues['start_on_next_scheduled_day'] ) && $boolDirectSet ) $this->set( 'm_intStartOnNextScheduledDay', trim( $arrValues['start_on_next_scheduled_day'] ) ); elseif( isset( $arrValues['start_on_next_scheduled_day'] ) ) $this->setStartOnNextScheduledDay( $arrValues['start_on_next_scheduled_day'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDataExportFrequencyTypeId( $intDataExportFrequencyTypeId ) {
		$this->set( 'm_intDataExportFrequencyTypeId', CStrings::strToIntDef( $intDataExportFrequencyTypeId, NULL, false ) );
	}

	public function getDataExportFrequencyTypeId() {
		return $this->m_intDataExportFrequencyTypeId;
	}

	public function sqlDataExportFrequencyTypeId() {
		return ( true == isset( $this->m_intDataExportFrequencyTypeId ) ) ? ( string ) $this->m_intDataExportFrequencyTypeId : 'NULL';
	}

	public function setEvery( $strEvery ) {
		$this->set( 'm_strEvery', CStrings::strTrimDef( $strEvery, 50, NULL, true ) );
	}

	public function getEvery() {
		return $this->m_strEvery;
	}

	public function sqlEvery() {
		return ( true == isset( $this->m_strEvery ) ) ? '\'' . addslashes( $this->m_strEvery ) . '\'' : 'NULL';
	}

	public function setScheduledDays( $strScheduledDays ) {
		$this->set( 'm_strScheduledDays', CStrings::strTrimDef( $strScheduledDays, 240, NULL, true ) );
	}

	public function getScheduledDays() {
		return $this->m_strScheduledDays;
	}

	public function sqlScheduledDays() {
		return ( true == isset( $this->m_strScheduledDays ) ) ? '\'' . addslashes( $this->m_strScheduledDays ) . '\'' : 'NULL';
	}

	public function setStartOn( $strStartOn ) {
		$this->set( 'm_strStartOn', CStrings::strTrimDef( $strStartOn, -1, NULL, true ) );
	}

	public function getStartOn() {
		return $this->m_strStartOn;
	}

	public function sqlStartOn() {
		return ( true == isset( $this->m_strStartOn ) ) ? '\'' . $this->m_strStartOn . '\'' : 'NULL';
	}

	public function setStartOnNextScheduledDay( $intStartOnNextScheduledDay ) {
		$this->set( 'm_intStartOnNextScheduledDay', CStrings::strToIntDef( $intStartOnNextScheduledDay, NULL, false ) );
	}

	public function getStartOnNextScheduledDay() {
		return $this->m_intStartOnNextScheduledDay;
	}

	public function sqlStartOnNextScheduledDay() {
		return ( true == isset( $this->m_intStartOnNextScheduledDay ) ) ? ( string ) $this->m_intStartOnNextScheduledDay : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, data_export_frequency_type_id, every, scheduled_days, start_on, start_on_next_scheduled_day, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDataExportFrequencyTypeId() . ', ' .
 						$this->sqlEvery() . ', ' .
 						$this->sqlScheduledDays() . ', ' .
 						$this->sqlStartOn() . ', ' .
 						$this->sqlStartOnNextScheduledDay() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_export_frequency_type_id = ' . $this->sqlDataExportFrequencyTypeId() . ','; } elseif( true == array_key_exists( 'DataExportFrequencyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_export_frequency_type_id = ' . $this->sqlDataExportFrequencyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' every = ' . $this->sqlEvery() . ','; } elseif( true == array_key_exists( 'Every', $this->getChangedColumns() ) ) { $strSql .= ' every = ' . $this->sqlEvery() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_days = ' . $this->sqlScheduledDays() . ','; } elseif( true == array_key_exists( 'ScheduledDays', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_days = ' . $this->sqlScheduledDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_on = ' . $this->sqlStartOn() . ','; } elseif( true == array_key_exists( 'StartOn', $this->getChangedColumns() ) ) { $strSql .= ' start_on = ' . $this->sqlStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_on_next_scheduled_day = ' . $this->sqlStartOnNextScheduledDay() . ','; } elseif( true == array_key_exists( 'StartOnNextScheduledDay', $this->getChangedColumns() ) ) { $strSql .= ' start_on_next_scheduled_day = ' . $this->sqlStartOnNextScheduledDay() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'data_export_frequency_type_id' => $this->getDataExportFrequencyTypeId(),
			'every' => $this->getEvery(),
			'scheduled_days' => $this->getScheduledDays(),
			'start_on' => $this->getStartOn(),
			'start_on_next_scheduled_day' => $this->getStartOnNextScheduledDay(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>