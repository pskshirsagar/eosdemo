<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestLabors
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestLabors extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestLabor[]
	 */
	public static function fetchMaintenanceRequestLabors( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestLabor::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestLabor
	 */
	public static function fetchMaintenanceRequestLabor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestLabor::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_labors', $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLabor( sprintf( 'SELECT * FROM maintenance_request_labors WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLabors( sprintf( 'SELECT * FROM maintenance_request_labors WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLabors( sprintf( 'SELECT * FROM maintenance_request_labors WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLabors( sprintf( 'SELECT * FROM maintenance_request_labors WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLaborsByMaintenanceRequestLaborTypeIdByCid( $intMaintenanceRequestLaborTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLabors( sprintf( 'SELECT * FROM maintenance_request_labors WHERE maintenance_request_labor_type_id = %d AND cid = %d', $intMaintenanceRequestLaborTypeId, $intCid ), $objDatabase );
	}

}
?>