<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySpecialClaimTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidySpecialClaimTypes extends CEosPluralBase {

	/**
	 * @return CSubsidySpecialClaimType[]
	 */
	public static function fetchSubsidySpecialClaimTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidySpecialClaimType::class, $objDatabase );
	}

	/**
	 * @return CSubsidySpecialClaimType
	 */
	public static function fetchSubsidySpecialClaimType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidySpecialClaimType::class, $objDatabase );
	}

	public static function fetchSubsidySpecialClaimTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_special_claim_types', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidySpecialClaimType( sprintf( 'SELECT * FROM subsidy_special_claim_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>