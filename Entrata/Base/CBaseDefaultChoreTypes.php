<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChoreTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultChoreTypes extends CEosPluralBase {

	/**
	 * @return CDefaultChoreType[]
	 */
	public static function fetchDefaultChoreTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultChoreType::class, $objDatabase );
	}

	/**
	 * @return CDefaultChoreType
	 */
	public static function fetchDefaultChoreType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultChoreType::class, $objDatabase );
	}

	public static function fetchDefaultChoreTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_chore_types', $objDatabase );
	}

	public static function fetchDefaultChoreTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultChoreType( sprintf( 'SELECT * FROM default_chore_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultChoreTypesByChoreCategoryId( $intChoreCategoryId, $objDatabase ) {
		return self::fetchDefaultChoreTypes( sprintf( 'SELECT * FROM default_chore_types WHERE chore_category_id = %d', $intChoreCategoryId ), $objDatabase );
	}

}
?>