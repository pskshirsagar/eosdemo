<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceUnions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceUnions extends CEosPluralBase {

	/**
	 * @return CUnitSpaceUnion[]
	 */
	public static function fetchUnitSpaceUnions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitSpaceUnion::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceUnion
	 */
	public static function fetchUnitSpaceUnion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceUnion::class, $objDatabase );
	}

	public static function fetchUnitSpaceUnionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_unions', $objDatabase );
	}

	public static function fetchUnitSpaceUnionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnion( sprintf( 'SELECT * FROM unit_space_unions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByGroupUnitSpaceIdByCid( $intGroupUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE group_unit_space_id = %d AND cid = %d', ( int ) $intGroupUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceUnionsBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceUnions( sprintf( 'SELECT * FROM unit_space_unions WHERE space_configuration_id = %d AND cid = %d', ( int ) $intSpaceConfigurationId, ( int ) $intCid ), $objDatabase );
	}

}
?>