<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledBlackoutDate extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_blackout_dates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intFrequencyId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strSchedule;
	protected $m_boolIsLeaseStartDate;
	protected $m_boolIsLeaseEndDate;
	protected $m_intIsMoveIn;
	protected $m_intIsMoveOut;
	protected $m_intIsAmenity;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strStartDate = '( now() )';
		$this->m_boolIsLeaseStartDate = false;
		$this->m_boolIsLeaseEndDate = false;
		$this->m_intIsMoveIn = '0';
		$this->m_intIsMoveOut = '0';
		$this->m_intIsAmenity = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['schedule'] ) && $boolDirectSet ) $this->set( 'm_strSchedule', trim( stripcslashes( $arrValues['schedule'] ) ) ); elseif( isset( $arrValues['schedule'] ) ) $this->setSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schedule'] ) : $arrValues['schedule'] );
		if( isset( $arrValues['is_lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeaseStartDate', trim( stripcslashes( $arrValues['is_lease_start_date'] ) ) ); elseif( isset( $arrValues['is_lease_start_date'] ) ) $this->setIsLeaseStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_start_date'] ) : $arrValues['is_lease_start_date'] );
		if( isset( $arrValues['is_lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeaseEndDate', trim( stripcslashes( $arrValues['is_lease_end_date'] ) ) ); elseif( isset( $arrValues['is_lease_end_date'] ) ) $this->setIsLeaseEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_end_date'] ) : $arrValues['is_lease_end_date'] );
		if( isset( $arrValues['is_move_in'] ) && $boolDirectSet ) $this->set( 'm_intIsMoveIn', trim( $arrValues['is_move_in'] ) ); elseif( isset( $arrValues['is_move_in'] ) ) $this->setIsMoveIn( $arrValues['is_move_in'] );
		if( isset( $arrValues['is_move_out'] ) && $boolDirectSet ) $this->set( 'm_intIsMoveOut', trim( $arrValues['is_move_out'] ) ); elseif( isset( $arrValues['is_move_out'] ) ) $this->setIsMoveOut( $arrValues['is_move_out'] );
		if( isset( $arrValues['is_amenity'] ) && $boolDirectSet ) $this->set( 'm_intIsAmenity', trim( $arrValues['is_amenity'] ) ); elseif( isset( $arrValues['is_amenity'] ) ) $this->setIsAmenity( $arrValues['is_amenity'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setSchedule( $strSchedule ) {
		$this->set( 'm_strSchedule', CStrings::strTrimDef( $strSchedule, 500, NULL, true ) );
	}

	public function getSchedule() {
		return $this->m_strSchedule;
	}

	public function sqlSchedule() {
		return ( true == isset( $this->m_strSchedule ) ) ? '\'' . addslashes( $this->m_strSchedule ) . '\'' : 'NULL';
	}

	public function setIsLeaseStartDate( $boolIsLeaseStartDate ) {
		$this->set( 'm_boolIsLeaseStartDate', CStrings::strToBool( $boolIsLeaseStartDate ) );
	}

	public function getIsLeaseStartDate() {
		return $this->m_boolIsLeaseStartDate;
	}

	public function sqlIsLeaseStartDate() {
		return ( true == isset( $this->m_boolIsLeaseStartDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeaseStartDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLeaseEndDate( $boolIsLeaseEndDate ) {
		$this->set( 'm_boolIsLeaseEndDate', CStrings::strToBool( $boolIsLeaseEndDate ) );
	}

	public function getIsLeaseEndDate() {
		return $this->m_boolIsLeaseEndDate;
	}

	public function sqlIsLeaseEndDate() {
		return ( true == isset( $this->m_boolIsLeaseEndDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeaseEndDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMoveIn( $intIsMoveIn ) {
		$this->set( 'm_intIsMoveIn', CStrings::strToIntDef( $intIsMoveIn, NULL, false ) );
	}

	public function getIsMoveIn() {
		return $this->m_intIsMoveIn;
	}

	public function sqlIsMoveIn() {
		return ( true == isset( $this->m_intIsMoveIn ) ) ? ( string ) $this->m_intIsMoveIn : '0';
	}

	public function setIsMoveOut( $intIsMoveOut ) {
		$this->set( 'm_intIsMoveOut', CStrings::strToIntDef( $intIsMoveOut, NULL, false ) );
	}

	public function getIsMoveOut() {
		return $this->m_intIsMoveOut;
	}

	public function sqlIsMoveOut() {
		return ( true == isset( $this->m_intIsMoveOut ) ) ? ( string ) $this->m_intIsMoveOut : '0';
	}

	public function setIsAmenity( $intIsAmenity ) {
		$this->set( 'm_intIsAmenity', CStrings::strToIntDef( $intIsAmenity, NULL, false ) );
	}

	public function getIsAmenity() {
		return $this->m_intIsAmenity;
	}

	public function sqlIsAmenity() {
		return ( true == isset( $this->m_intIsAmenity ) ) ? ( string ) $this->m_intIsAmenity : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, frequency_id, start_date, end_date, schedule, is_lease_start_date, is_lease_end_date, is_move_in, is_move_out, is_amenity, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlSchedule() . ', ' .
 						$this->sqlIsLeaseStartDate() . ', ' .
 						$this->sqlIsLeaseEndDate() . ', ' .
 						$this->sqlIsMoveIn() . ', ' .
 						$this->sqlIsMoveOut() . ', ' .
 						$this->sqlIsAmenity() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; } elseif( true == array_key_exists( 'Schedule', $this->getChangedColumns() ) ) { $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lease_start_date = ' . $this->sqlIsLeaseStartDate() . ','; } elseif( true == array_key_exists( 'IsLeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' is_lease_start_date = ' . $this->sqlIsLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lease_end_date = ' . $this->sqlIsLeaseEndDate() . ','; } elseif( true == array_key_exists( 'IsLeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' is_lease_end_date = ' . $this->sqlIsLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_in = ' . $this->sqlIsMoveIn() . ','; } elseif( true == array_key_exists( 'IsMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' is_move_in = ' . $this->sqlIsMoveIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; } elseif( true == array_key_exists( 'IsMoveOut', $this->getChangedColumns() ) ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_amenity = ' . $this->sqlIsAmenity() . ','; } elseif( true == array_key_exists( 'IsAmenity', $this->getChangedColumns() ) ) { $strSql .= ' is_amenity = ' . $this->sqlIsAmenity() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'frequency_id' => $this->getFrequencyId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'schedule' => $this->getSchedule(),
			'is_lease_start_date' => $this->getIsLeaseStartDate(),
			'is_lease_end_date' => $this->getIsLeaseEndDate(),
			'is_move_in' => $this->getIsMoveIn(),
			'is_move_out' => $this->getIsMoveOut(),
			'is_amenity' => $this->getIsAmenity(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>