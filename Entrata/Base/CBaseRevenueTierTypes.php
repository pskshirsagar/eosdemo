<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueTierTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueTierTypes extends CEosPluralBase {

	/**
	 * @return CRevenueTierType[]
	 */
	public static function fetchRevenueTierTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueTierType::class, $objDatabase );
	}

	/**
	 * @return CRevenueTierType
	 */
	public static function fetchRevenueTierType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueTierType::class, $objDatabase );
	}

	public static function fetchRevenueTierTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_tier_types', $objDatabase );
	}

	public static function fetchRevenueTierTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueTierType( sprintf( 'SELECT * FROM revenue_tier_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>