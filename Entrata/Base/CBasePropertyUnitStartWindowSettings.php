<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUnitStartWindowSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUnitStartWindowSettings extends CEosPluralBase {

	/**
	 * @return CPropertyUnitStartWindowSetting[]
	 */
	public static function fetchPropertyUnitStartWindowSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyUnitStartWindowSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyUnitStartWindowSetting
	 */
	public static function fetchPropertyUnitStartWindowSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyUnitStartWindowSetting', $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_unit_start_window_settings', $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSetting( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitStartWindowSettingsByGenderIdByCid( $intGenderId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitStartWindowSettings( sprintf( 'SELECT * FROM property_unit_start_window_settings WHERE gender_id = %d AND cid = %d', ( int ) $intGenderId, ( int ) $intCid ), $objDatabase );
	}

}
?>