<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCharities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCharities extends CEosPluralBase {

	/**
	 * @return CPropertyCharity[]
	 */
	public static function fetchPropertyCharities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCharity', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCharity
	 */
	public static function fetchPropertyCharity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCharity', $objDatabase );
	}

	public static function fetchPropertyCharityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_charities', $objDatabase );
	}

	public static function fetchPropertyCharityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCharity( sprintf( 'SELECT * FROM property_charities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCharitiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCharities( sprintf( 'SELECT * FROM property_charities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCharitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCharities( sprintf( 'SELECT * FROM property_charities WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCharitiesByCompanyCharityIdByCid( $intCompanyCharityId, $intCid, $objDatabase ) {
		return self::fetchPropertyCharities( sprintf( 'SELECT * FROM property_charities WHERE company_charity_id = %d AND cid = %d', ( int ) $intCompanyCharityId, ( int ) $intCid ), $objDatabase );
	}

}
?>