<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyRaceTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidyRaceTypes extends CEosPluralBase {

	/**
	 * @return CCustomerSubsidyRaceType[]
	 */
	public static function fetchCustomerSubsidyRaceTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerSubsidyRaceType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSubsidyRaceType
	 */
	public static function fetchCustomerSubsidyRaceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerSubsidyRaceType', $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_subsidy_race_types', $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyRaceType( sprintf( 'SELECT * FROM customer_subsidy_race_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyRaceTypes( sprintf( 'SELECT * FROM customer_subsidy_race_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyRaceTypes( sprintf( 'SELECT * FROM customer_subsidy_race_types WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypesBySubsidyRaceTypeIdByCid( $intSubsidyRaceTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyRaceTypes( sprintf( 'SELECT * FROM customer_subsidy_race_types WHERE subsidy_race_type_id = %d AND cid = %d', ( int ) $intSubsidyRaceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyRaceTypesBySubsidyRaceSubTypeIdByCid( $intSubsidyRaceSubTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyRaceTypes( sprintf( 'SELECT * FROM customer_subsidy_race_types WHERE subsidy_race_sub_type_id = %d AND cid = %d', ( int ) $intSubsidyRaceSubTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>