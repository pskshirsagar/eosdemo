<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePackage extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.packages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitSpaceId;
	protected $m_intPackageTypeId;
	protected $m_intPackageBatchId;
	protected $m_intPackageStatusTypeId;
	protected $m_intSignatureFileId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_strPackageDatetime;
	protected $m_strBarcode;
	protected $m_strDescription;
	protected $m_strNotes;
	protected $m_strSignature;
	protected $m_intEmailCount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intIsNotePublished;
	protected $m_intIsLobbyDisplayVisible;
	protected $m_intIsPerishable;
	protected $m_strMessagedOn;
	protected $m_strCalledOn;
	protected $m_strEmailedOn;
	protected $m_strPrintedOn;
	protected $m_strReceivedOn;
	protected $m_intDeliveredBy;
	protected $m_strDeliveredOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsNotePublished = '0';
		$this->m_intIsLobbyDisplayVisible = '0';
		$this->m_intIsPerishable = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['package_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageTypeId', trim( $arrValues['package_type_id'] ) ); elseif( isset( $arrValues['package_type_id'] ) ) $this->setPackageTypeId( $arrValues['package_type_id'] );
		if( isset( $arrValues['package_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageBatchId', trim( $arrValues['package_batch_id'] ) ); elseif( isset( $arrValues['package_batch_id'] ) ) $this->setPackageBatchId( $arrValues['package_batch_id'] );
		if( isset( $arrValues['package_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageStatusTypeId', trim( $arrValues['package_status_type_id'] ) ); elseif( isset( $arrValues['package_status_type_id'] ) ) $this->setPackageStatusTypeId( $arrValues['package_status_type_id'] );
		if( isset( $arrValues['signature_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSignatureFileId', trim( $arrValues['signature_file_id'] ) ); elseif( isset( $arrValues['signature_file_id'] ) ) $this->setSignatureFileId( $arrValues['signature_file_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['package_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPackageDatetime', trim( $arrValues['package_datetime'] ) ); elseif( isset( $arrValues['package_datetime'] ) ) $this->setPackageDatetime( $arrValues['package_datetime'] );
		if( isset( $arrValues['barcode'] ) && $boolDirectSet ) $this->set( 'm_strBarcode', trim( stripcslashes( $arrValues['barcode'] ) ) ); elseif( isset( $arrValues['barcode'] ) ) $this->setBarcode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['barcode'] ) : $arrValues['barcode'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( stripcslashes( $arrValues['signature'] ) ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature'] ) : $arrValues['signature'] );
		if( isset( $arrValues['email_count'] ) && $boolDirectSet ) $this->set( 'm_intEmailCount', trim( $arrValues['email_count'] ) ); elseif( isset( $arrValues['email_count'] ) ) $this->setEmailCount( $arrValues['email_count'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_note_published'] ) && $boolDirectSet ) $this->set( 'm_intIsNotePublished', trim( $arrValues['is_note_published'] ) ); elseif( isset( $arrValues['is_note_published'] ) ) $this->setIsNotePublished( $arrValues['is_note_published'] );
		if( isset( $arrValues['is_lobby_display_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsLobbyDisplayVisible', trim( $arrValues['is_lobby_display_visible'] ) ); elseif( isset( $arrValues['is_lobby_display_visible'] ) ) $this->setIsLobbyDisplayVisible( $arrValues['is_lobby_display_visible'] );
		if( isset( $arrValues['is_perishable'] ) && $boolDirectSet ) $this->set( 'm_intIsPerishable', trim( $arrValues['is_perishable'] ) ); elseif( isset( $arrValues['is_perishable'] ) ) $this->setIsPerishable( $arrValues['is_perishable'] );
		if( isset( $arrValues['messaged_on'] ) && $boolDirectSet ) $this->set( 'm_strMessagedOn', trim( $arrValues['messaged_on'] ) ); elseif( isset( $arrValues['messaged_on'] ) ) $this->setMessagedOn( $arrValues['messaged_on'] );
		if( isset( $arrValues['called_on'] ) && $boolDirectSet ) $this->set( 'm_strCalledOn', trim( $arrValues['called_on'] ) ); elseif( isset( $arrValues['called_on'] ) ) $this->setCalledOn( $arrValues['called_on'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['printed_on'] ) && $boolDirectSet ) $this->set( 'm_strPrintedOn', trim( $arrValues['printed_on'] ) ); elseif( isset( $arrValues['printed_on'] ) ) $this->setPrintedOn( $arrValues['printed_on'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['delivered_by'] ) && $boolDirectSet ) $this->set( 'm_intDeliveredBy', trim( $arrValues['delivered_by'] ) ); elseif( isset( $arrValues['delivered_by'] ) ) $this->setDeliveredBy( $arrValues['delivered_by'] );
		if( isset( $arrValues['delivered_on'] ) && $boolDirectSet ) $this->set( 'm_strDeliveredOn', trim( $arrValues['delivered_on'] ) ); elseif( isset( $arrValues['delivered_on'] ) ) $this->setDeliveredOn( $arrValues['delivered_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPackageTypeId( $intPackageTypeId ) {
		$this->set( 'm_intPackageTypeId', CStrings::strToIntDef( $intPackageTypeId, NULL, false ) );
	}

	public function getPackageTypeId() {
		return $this->m_intPackageTypeId;
	}

	public function sqlPackageTypeId() {
		return ( true == isset( $this->m_intPackageTypeId ) ) ? ( string ) $this->m_intPackageTypeId : 'NULL';
	}

	public function setPackageBatchId( $intPackageBatchId ) {
		$this->set( 'm_intPackageBatchId', CStrings::strToIntDef( $intPackageBatchId, NULL, false ) );
	}

	public function getPackageBatchId() {
		return $this->m_intPackageBatchId;
	}

	public function sqlPackageBatchId() {
		return ( true == isset( $this->m_intPackageBatchId ) ) ? ( string ) $this->m_intPackageBatchId : 'NULL';
	}

	public function setPackageStatusTypeId( $intPackageStatusTypeId ) {
		$this->set( 'm_intPackageStatusTypeId', CStrings::strToIntDef( $intPackageStatusTypeId, NULL, false ) );
	}

	public function getPackageStatusTypeId() {
		return $this->m_intPackageStatusTypeId;
	}

	public function sqlPackageStatusTypeId() {
		return ( true == isset( $this->m_intPackageStatusTypeId ) ) ? ( string ) $this->m_intPackageStatusTypeId : 'NULL';
	}

	public function setSignatureFileId( $intSignatureFileId ) {
		$this->set( 'm_intSignatureFileId', CStrings::strToIntDef( $intSignatureFileId, NULL, false ) );
	}

	public function getSignatureFileId() {
		return $this->m_intSignatureFileId;
	}

	public function sqlSignatureFileId() {
		return ( true == isset( $this->m_intSignatureFileId ) ) ? ( string ) $this->m_intSignatureFileId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPackageDatetime( $strPackageDatetime ) {
		$this->set( 'm_strPackageDatetime', CStrings::strTrimDef( $strPackageDatetime, -1, NULL, true ) );
	}

	public function getPackageDatetime() {
		return $this->m_strPackageDatetime;
	}

	public function sqlPackageDatetime() {
		return ( true == isset( $this->m_strPackageDatetime ) ) ? '\'' . $this->m_strPackageDatetime . '\'' : 'NOW()';
	}

	public function setBarcode( $strBarcode ) {
		$this->set( 'm_strBarcode', CStrings::strTrimDef( $strBarcode, 64, NULL, true ) );
	}

	public function getBarcode() {
		return $this->m_strBarcode;
	}

	public function sqlBarcode() {
		return ( true == isset( $this->m_strBarcode ) ) ? '\'' . addslashes( $this->m_strBarcode ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 255, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 255, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 240, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? '\'' . addslashes( $this->m_strSignature ) . '\'' : 'NULL';
	}

	public function setEmailCount( $intEmailCount ) {
		$this->set( 'm_intEmailCount', CStrings::strToIntDef( $intEmailCount, NULL, false ) );
	}

	public function getEmailCount() {
		return $this->m_intEmailCount;
	}

	public function sqlEmailCount() {
		return ( true == isset( $this->m_intEmailCount ) ) ? ( string ) $this->m_intEmailCount : 'NULL';
	}

	public function setIsNotePublished( $intIsNotePublished ) {
		$this->set( 'm_intIsNotePublished', CStrings::strToIntDef( $intIsNotePublished, NULL, false ) );
	}

	public function getIsNotePublished() {
		return $this->m_intIsNotePublished;
	}

	public function sqlIsNotePublished() {
		return ( true == isset( $this->m_intIsNotePublished ) ) ? ( string ) $this->m_intIsNotePublished : '0';
	}

	public function setIsLobbyDisplayVisible( $intIsLobbyDisplayVisible ) {
		$this->set( 'm_intIsLobbyDisplayVisible', CStrings::strToIntDef( $intIsLobbyDisplayVisible, NULL, false ) );
	}

	public function getIsLobbyDisplayVisible() {
		return $this->m_intIsLobbyDisplayVisible;
	}

	public function sqlIsLobbyDisplayVisible() {
		return ( true == isset( $this->m_intIsLobbyDisplayVisible ) ) ? ( string ) $this->m_intIsLobbyDisplayVisible : '0';
	}

	public function setIsPerishable( $intIsPerishable ) {
		$this->set( 'm_intIsPerishable', CStrings::strToIntDef( $intIsPerishable, NULL, false ) );
	}

	public function getIsPerishable() {
		return $this->m_intIsPerishable;
	}

	public function sqlIsPerishable() {
		return ( true == isset( $this->m_intIsPerishable ) ) ? ( string ) $this->m_intIsPerishable : '0';
	}

	public function setMessagedOn( $strMessagedOn ) {
		$this->set( 'm_strMessagedOn', CStrings::strTrimDef( $strMessagedOn, -1, NULL, true ) );
	}

	public function getMessagedOn() {
		return $this->m_strMessagedOn;
	}

	public function sqlMessagedOn() {
		return ( true == isset( $this->m_strMessagedOn ) ) ? '\'' . $this->m_strMessagedOn . '\'' : 'NULL';
	}

	public function setCalledOn( $strCalledOn ) {
		$this->set( 'm_strCalledOn', CStrings::strTrimDef( $strCalledOn, -1, NULL, true ) );
	}

	public function getCalledOn() {
		return $this->m_strCalledOn;
	}

	public function sqlCalledOn() {
		return ( true == isset( $this->m_strCalledOn ) ) ? '\'' . $this->m_strCalledOn . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setPrintedOn( $strPrintedOn ) {
		$this->set( 'm_strPrintedOn', CStrings::strTrimDef( $strPrintedOn, -1, NULL, true ) );
	}

	public function getPrintedOn() {
		return $this->m_strPrintedOn;
	}

	public function sqlPrintedOn() {
		return ( true == isset( $this->m_strPrintedOn ) ) ? '\'' . $this->m_strPrintedOn . '\'' : 'NULL';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setDeliveredBy( $intDeliveredBy ) {
		$this->set( 'm_intDeliveredBy', CStrings::strToIntDef( $intDeliveredBy, NULL, false ) );
	}

	public function getDeliveredBy() {
		return $this->m_intDeliveredBy;
	}

	public function sqlDeliveredBy() {
		return ( true == isset( $this->m_intDeliveredBy ) ) ? ( string ) $this->m_intDeliveredBy : 'NULL';
	}

	public function setDeliveredOn( $strDeliveredOn ) {
		$this->set( 'm_strDeliveredOn', CStrings::strTrimDef( $strDeliveredOn, -1, NULL, true ) );
	}

	public function getDeliveredOn() {
		return $this->m_strDeliveredOn;
	}

	public function sqlDeliveredOn() {
		return ( true == isset( $this->m_strDeliveredOn ) ) ? '\'' . $this->m_strDeliveredOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_space_id, package_type_id, package_batch_id, package_status_type_id, signature_file_id, customer_id, lease_id, package_datetime, barcode, description, notes, signature, email_count, details, is_note_published, is_lobby_display_visible, is_perishable, messaged_on, called_on, emailed_on, printed_on, received_on, delivered_by, delivered_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlPackageTypeId() . ', ' .
						$this->sqlPackageBatchId() . ', ' .
						$this->sqlPackageStatusTypeId() . ', ' .
						$this->sqlSignatureFileId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlPackageDatetime() . ', ' .
						$this->sqlBarcode() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlSignature() . ', ' .
						$this->sqlEmailCount() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsNotePublished() . ', ' .
						$this->sqlIsLobbyDisplayVisible() . ', ' .
						$this->sqlIsPerishable() . ', ' .
						$this->sqlMessagedOn() . ', ' .
						$this->sqlCalledOn() . ', ' .
						$this->sqlEmailedOn() . ', ' .
						$this->sqlPrintedOn() . ', ' .
						$this->sqlReceivedOn() . ', ' .
						$this->sqlDeliveredBy() . ', ' .
						$this->sqlDeliveredOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_type_id = ' . $this->sqlPackageTypeId(). ',' ; } elseif( true == array_key_exists( 'PackageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' package_type_id = ' . $this->sqlPackageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_batch_id = ' . $this->sqlPackageBatchId(). ',' ; } elseif( true == array_key_exists( 'PackageBatchId', $this->getChangedColumns() ) ) { $strSql .= ' package_batch_id = ' . $this->sqlPackageBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_status_type_id = ' . $this->sqlPackageStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PackageStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' package_status_type_id = ' . $this->sqlPackageStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId(). ',' ; } elseif( true == array_key_exists( 'SignatureFileId', $this->getChangedColumns() ) ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_datetime = ' . $this->sqlPackageDatetime(). ',' ; } elseif( true == array_key_exists( 'PackageDatetime', $this->getChangedColumns() ) ) { $strSql .= ' package_datetime = ' . $this->sqlPackageDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' barcode = ' . $this->sqlBarcode(). ',' ; } elseif( true == array_key_exists( 'Barcode', $this->getChangedColumns() ) ) { $strSql .= ' barcode = ' . $this->sqlBarcode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature(). ',' ; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_count = ' . $this->sqlEmailCount(). ',' ; } elseif( true == array_key_exists( 'EmailCount', $this->getChangedColumns() ) ) { $strSql .= ' email_count = ' . $this->sqlEmailCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_note_published = ' . $this->sqlIsNotePublished(). ',' ; } elseif( true == array_key_exists( 'IsNotePublished', $this->getChangedColumns() ) ) { $strSql .= ' is_note_published = ' . $this->sqlIsNotePublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lobby_display_visible = ' . $this->sqlIsLobbyDisplayVisible(). ',' ; } elseif( true == array_key_exists( 'IsLobbyDisplayVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_lobby_display_visible = ' . $this->sqlIsLobbyDisplayVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_perishable = ' . $this->sqlIsPerishable(). ',' ; } elseif( true == array_key_exists( 'IsPerishable', $this->getChangedColumns() ) ) { $strSql .= ' is_perishable = ' . $this->sqlIsPerishable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' messaged_on = ' . $this->sqlMessagedOn(). ',' ; } elseif( true == array_key_exists( 'MessagedOn', $this->getChangedColumns() ) ) { $strSql .= ' messaged_on = ' . $this->sqlMessagedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' called_on = ' . $this->sqlCalledOn(). ',' ; } elseif( true == array_key_exists( 'CalledOn', $this->getChangedColumns() ) ) { $strSql .= ' called_on = ' . $this->sqlCalledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn(). ',' ; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' printed_on = ' . $this->sqlPrintedOn(). ',' ; } elseif( true == array_key_exists( 'PrintedOn', $this->getChangedColumns() ) ) { $strSql .= ' printed_on = ' . $this->sqlPrintedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn(). ',' ; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivered_by = ' . $this->sqlDeliveredBy(). ',' ; } elseif( true == array_key_exists( 'DeliveredBy', $this->getChangedColumns() ) ) { $strSql .= ' delivered_by = ' . $this->sqlDeliveredBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn(). ',' ; } elseif( true == array_key_exists( 'DeliveredOn', $this->getChangedColumns() ) ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'package_type_id' => $this->getPackageTypeId(),
			'package_batch_id' => $this->getPackageBatchId(),
			'package_status_type_id' => $this->getPackageStatusTypeId(),
			'signature_file_id' => $this->getSignatureFileId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'package_datetime' => $this->getPackageDatetime(),
			'barcode' => $this->getBarcode(),
			'description' => $this->getDescription(),
			'notes' => $this->getNotes(),
			'signature' => $this->getSignature(),
			'email_count' => $this->getEmailCount(),
			'details' => $this->getDetails(),
			'is_note_published' => $this->getIsNotePublished(),
			'is_lobby_display_visible' => $this->getIsLobbyDisplayVisible(),
			'is_perishable' => $this->getIsPerishable(),
			'messaged_on' => $this->getMessagedOn(),
			'called_on' => $this->getCalledOn(),
			'emailed_on' => $this->getEmailedOn(),
			'printed_on' => $this->getPrintedOn(),
			'received_on' => $this->getReceivedOn(),
			'delivered_by' => $this->getDeliveredBy(),
			'delivered_on' => $this->getDeliveredOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>