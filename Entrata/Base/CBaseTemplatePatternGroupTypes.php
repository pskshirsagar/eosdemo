<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTemplatePatternGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseTemplatePatternGroupTypes extends CEosPluralBase {

	/**
	 * @return CTemplatePatternGroupType[]
	 */
	public static function fetchTemplatePatternGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTemplatePatternGroupType', $objDatabase );
	}

	/**
	 * @return CTemplatePatternGroupType
	 */
	public static function fetchTemplatePatternGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTemplatePatternGroupType', $objDatabase );
	}

	public static function fetchTemplatePatternGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'template_pattern_group_types', $objDatabase );
	}

	public static function fetchTemplatePatternGroupTypeById( $intId, $objDatabase ) {
		return self::fetchTemplatePatternGroupType( sprintf( 'SELECT * FROM template_pattern_group_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>