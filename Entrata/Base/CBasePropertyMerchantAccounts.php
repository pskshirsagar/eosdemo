<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMerchantAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMerchantAccounts extends CEosPluralBase {

	/**
	 * @return CPropertyMerchantAccount[]
	 */
	public static function fetchPropertyMerchantAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyMerchantAccount', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMerchantAccount
	 */
	public static function fetchPropertyMerchantAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyMerchantAccount', $objDatabase );
	}

	public static function fetchPropertyMerchantAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_merchant_accounts', $objDatabase );
	}

	public static function fetchPropertyMerchantAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMerchantAccount( sprintf( 'SELECT * FROM property_merchant_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM property_merchant_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM property_merchant_accounts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM property_merchant_accounts WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyMerchantAccountsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyMerchantAccounts( sprintf( 'SELECT * FROM property_merchant_accounts WHERE company_merchant_account_id = %d AND cid = %d', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>