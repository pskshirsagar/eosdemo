<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyHapRequests extends CEosPluralBase {

	/**
	 * @return CSubsidyHapRequest[]
	 */
	public static function fetchSubsidyHapRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyHapRequest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyHapRequest
	 */
	public static function fetchSubsidyHapRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyHapRequest', $objDatabase );
	}

	public static function fetchSubsidyHapRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_hap_requests', $objDatabase );
	}

	public static function fetchSubsidyHapRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequest( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsBySubsidyContractIdByCid( $intSubsidyContractId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE subsidy_contract_id = %d AND cid = %d', ( int ) $intSubsidyContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsBySubsidyHapRequestStatusTypeIdByCid( $intSubsidyHapRequestStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE subsidy_hap_request_status_type_id = %d AND cid = %d', ( int ) $intSubsidyHapRequestStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByPreviousSubsidyHapRequestIdByCid( $intPreviousSubsidyHapRequestId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE previous_subsidy_hap_request_id = %d AND cid = %d', ( int ) $intPreviousSubsidyHapRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequests( sprintf( 'SELECT * FROM subsidy_hap_requests WHERE ar_payment_id = %d AND cid = %d', ( int ) $intArPaymentId, ( int ) $intCid ), $objDatabase );
	}

}
?>