<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitOfMeasureConversion extends CEosSingularBase {

	const TABLE_NAME = 'public.unit_of_measure_conversions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultUnitOfMeasureConversionId;
	protected $m_intFromUnitOfMeasureId;
	protected $m_intToUnitOfMeasureId;
	protected $m_fltConversionRate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_unit_of_measure_conversion_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultUnitOfMeasureConversionId', trim( $arrValues['default_unit_of_measure_conversion_id'] ) ); elseif( isset( $arrValues['default_unit_of_measure_conversion_id'] ) ) $this->setDefaultUnitOfMeasureConversionId( $arrValues['default_unit_of_measure_conversion_id'] );
		if( isset( $arrValues['from_unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intFromUnitOfMeasureId', trim( $arrValues['from_unit_of_measure_id'] ) ); elseif( isset( $arrValues['from_unit_of_measure_id'] ) ) $this->setFromUnitOfMeasureId( $arrValues['from_unit_of_measure_id'] );
		if( isset( $arrValues['to_unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intToUnitOfMeasureId', trim( $arrValues['to_unit_of_measure_id'] ) ); elseif( isset( $arrValues['to_unit_of_measure_id'] ) ) $this->setToUnitOfMeasureId( $arrValues['to_unit_of_measure_id'] );
		if( isset( $arrValues['conversion_rate'] ) && $boolDirectSet ) $this->set( 'm_fltConversionRate', trim( $arrValues['conversion_rate'] ) ); elseif( isset( $arrValues['conversion_rate'] ) ) $this->setConversionRate( $arrValues['conversion_rate'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultUnitOfMeasureConversionId( $intDefaultUnitOfMeasureConversionId ) {
		$this->set( 'm_intDefaultUnitOfMeasureConversionId', CStrings::strToIntDef( $intDefaultUnitOfMeasureConversionId, NULL, false ) );
	}

	public function getDefaultUnitOfMeasureConversionId() {
		return $this->m_intDefaultUnitOfMeasureConversionId;
	}

	public function sqlDefaultUnitOfMeasureConversionId() {
		return ( true == isset( $this->m_intDefaultUnitOfMeasureConversionId ) ) ? ( string ) $this->m_intDefaultUnitOfMeasureConversionId : 'NULL';
	}

	public function setFromUnitOfMeasureId( $intFromUnitOfMeasureId ) {
		$this->set( 'm_intFromUnitOfMeasureId', CStrings::strToIntDef( $intFromUnitOfMeasureId, NULL, false ) );
	}

	public function getFromUnitOfMeasureId() {
		return $this->m_intFromUnitOfMeasureId;
	}

	public function sqlFromUnitOfMeasureId() {
		return ( true == isset( $this->m_intFromUnitOfMeasureId ) ) ? ( string ) $this->m_intFromUnitOfMeasureId : 'NULL';
	}

	public function setToUnitOfMeasureId( $intToUnitOfMeasureId ) {
		$this->set( 'm_intToUnitOfMeasureId', CStrings::strToIntDef( $intToUnitOfMeasureId, NULL, false ) );
	}

	public function getToUnitOfMeasureId() {
		return $this->m_intToUnitOfMeasureId;
	}

	public function sqlToUnitOfMeasureId() {
		return ( true == isset( $this->m_intToUnitOfMeasureId ) ) ? ( string ) $this->m_intToUnitOfMeasureId : 'NULL';
	}

	public function setConversionRate( $fltConversionRate ) {
		$this->set( 'm_fltConversionRate', CStrings::strToFloatDef( $fltConversionRate, NULL, false, 2 ) );
	}

	public function getConversionRate() {
		return $this->m_fltConversionRate;
	}

	public function sqlConversionRate() {
		return ( true == isset( $this->m_fltConversionRate ) ) ? ( string ) $this->m_fltConversionRate : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_unit_of_measure_conversion_id, from_unit_of_measure_id, to_unit_of_measure_id, conversion_rate, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDefaultUnitOfMeasureConversionId() . ', ' .
 						$this->sqlFromUnitOfMeasureId() . ', ' .
 						$this->sqlToUnitOfMeasureId() . ', ' .
 						$this->sqlConversionRate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_unit_of_measure_conversion_id = ' . $this->sqlDefaultUnitOfMeasureConversionId() . ','; } elseif( true == array_key_exists( 'DefaultUnitOfMeasureConversionId', $this->getChangedColumns() ) ) { $strSql .= ' default_unit_of_measure_conversion_id = ' . $this->sqlDefaultUnitOfMeasureConversionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_unit_of_measure_id = ' . $this->sqlFromUnitOfMeasureId() . ','; } elseif( true == array_key_exists( 'FromUnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' from_unit_of_measure_id = ' . $this->sqlFromUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_unit_of_measure_id = ' . $this->sqlToUnitOfMeasureId() . ','; } elseif( true == array_key_exists( 'ToUnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' to_unit_of_measure_id = ' . $this->sqlToUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conversion_rate = ' . $this->sqlConversionRate() . ','; } elseif( true == array_key_exists( 'ConversionRate', $this->getChangedColumns() ) ) { $strSql .= ' conversion_rate = ' . $this->sqlConversionRate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_unit_of_measure_conversion_id' => $this->getDefaultUnitOfMeasureConversionId(),
			'from_unit_of_measure_id' => $this->getFromUnitOfMeasureId(),
			'to_unit_of_measure_id' => $this->getToUnitOfMeasureId(),
			'conversion_rate' => $this->getConversionRate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>