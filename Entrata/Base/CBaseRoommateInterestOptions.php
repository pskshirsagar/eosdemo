<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterestOptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoommateInterestOptions extends CEosPluralBase {

	/**
	 * @return CRoommateInterestOption[]
	 */
	public static function fetchRoommateInterestOptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRoommateInterestOption::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRoommateInterestOption
	 */
	public static function fetchRoommateInterestOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRoommateInterestOption::class, $objDatabase );
	}

	public static function fetchRoommateInterestOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roommate_interest_options', $objDatabase );
	}

	public static function fetchRoommateInterestOptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRoommateInterestOption( sprintf( 'SELECT * FROM roommate_interest_options WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRoommateInterestOptionsByCid( $intCid, $objDatabase ) {
		return self::fetchRoommateInterestOptions( sprintf( 'SELECT * FROM roommate_interest_options WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRoommateInterestOptionsByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {
		return self::fetchRoommateInterestOptions( sprintf( 'SELECT * FROM roommate_interest_options WHERE roommate_interest_id = %d AND cid = %d', $intRoommateInterestId, $intCid ), $objDatabase );
	}

}
?>