<?php

class CBaseChecklistItemTypeOption extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.checklist_item_type_options';

	protected $m_intId;
	protected $m_intChecklistItemTypeId;
	protected $m_arrintChecklistTriggerIds;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['checklist_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistItemTypeId', trim( $arrValues['checklist_item_type_id'] ) ); elseif( isset( $arrValues['checklist_item_type_id'] ) ) $this->setChecklistItemTypeId( $arrValues['checklist_item_type_id'] );
		if( isset( $arrValues['checklist_trigger_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintChecklistTriggerIds', trim( $arrValues['checklist_trigger_ids'] ) ); elseif( isset( $arrValues['checklist_trigger_ids'] ) ) $this->setChecklistTriggerIds( $arrValues['checklist_trigger_ids'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChecklistItemTypeId( $intChecklistItemTypeId ) {
		$this->set( 'm_intChecklistItemTypeId', CStrings::strToIntDef( $intChecklistItemTypeId, NULL, false ) );
	}

	public function getChecklistItemTypeId() {
		return $this->m_intChecklistItemTypeId;
	}

	public function sqlChecklistItemTypeId() {
		return ( true == isset( $this->m_intChecklistItemTypeId ) ) ? ( string ) $this->m_intChecklistItemTypeId : 'NULL';
	}

	public function setChecklistTriggerIds( $arrintChecklistTriggerIds ) {
		$this->set( 'm_arrintChecklistTriggerIds', CStrings::strToArrIntDef( $arrintChecklistTriggerIds, NULL ) );
	}

	public function getChecklistTriggerIds() {
		return $this->m_arrintChecklistTriggerIds;
	}

	public function sqlChecklistTriggerIds() {
		return ( true == isset( $this->m_arrintChecklistTriggerIds ) && true == valArr( $this->m_arrintChecklistTriggerIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintChecklistTriggerIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'checklist_item_type_id' => $this->getChecklistItemTypeId(),
			'checklist_trigger_ids' => $this->getChecklistTriggerIds(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>