<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitOfMeasureConversions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitOfMeasureConversions extends CEosPluralBase {

	/**
	 * @return CUnitOfMeasureConversion[]
	 */
	public static function fetchUnitOfMeasureConversions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitOfMeasureConversion', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitOfMeasureConversion
	 */
	public static function fetchUnitOfMeasureConversion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitOfMeasureConversion', $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_of_measure_conversions', $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasureConversion( sprintf( 'SELECT * FROM unit_of_measure_conversions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasureConversions( sprintf( 'SELECT * FROM unit_of_measure_conversions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionsByDefaultUnitOfMeasureConversionIdByCid( $intDefaultUnitOfMeasureConversionId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasureConversions( sprintf( 'SELECT * FROM unit_of_measure_conversions WHERE default_unit_of_measure_conversion_id = %d AND cid = %d', ( int ) $intDefaultUnitOfMeasureConversionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionsByFromUnitOfMeasureIdByCid( $intFromUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasureConversions( sprintf( 'SELECT * FROM unit_of_measure_conversions WHERE from_unit_of_measure_id = %d AND cid = %d', ( int ) $intFromUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitOfMeasureConversionsByToUnitOfMeasureIdByCid( $intToUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchUnitOfMeasureConversions( sprintf( 'SELECT * FROM unit_of_measure_conversions WHERE to_unit_of_measure_id = %d AND cid = %d', ( int ) $intToUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

}
?>