<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataBlob extends CEosSingularBase {

	const TABLE_NAME = 'public.data_blobs';

	protected $m_intCid;
	protected $m_intDataBlobTypeId;
	protected $m_intPropertyId;
	protected $m_intLeaseCustomerId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intEmployeeId;
	protected $m_intApplicantApplicationId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intApPayeeId;
	protected $m_strDataField1;
	protected $m_strDataField2;
	protected $m_strDataField3;
	protected $m_intOrderNum;
	protected $m_strBlob;
	protected $m_strUpdatedOn;
	protected $m_intId;

	public function __construct() {
		parent::__construct();

		$this->m_strDataField2 = NULL;
		$this->m_strBlob = '\'NULL::character varying\'::character varying';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['data_blob_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataBlobTypeId', trim( $arrValues['data_blob_type_id'] ) ); elseif( isset( $arrValues['data_blob_type_id'] ) ) $this->setDataBlobTypeId( $arrValues['data_blob_type_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCustomerId', trim( $arrValues['lease_customer_id'] ) ); elseif( isset( $arrValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrValues['lease_customer_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['data_field1'] ) && $boolDirectSet ) $this->set( 'm_strDataField1', trim( $arrValues['data_field1'] ) ); elseif( isset( $arrValues['data_field1'] ) ) $this->setDataField1( $arrValues['data_field1'] );
		if( isset( $arrValues['data_field2'] ) && $boolDirectSet ) $this->set( 'm_strDataField2', trim( $arrValues['data_field2'] ) ); elseif( isset( $arrValues['data_field2'] ) ) $this->setDataField2( $arrValues['data_field2'] );
		if( isset( $arrValues['data_field3'] ) && $boolDirectSet ) $this->set( 'm_strDataField3', trim( $arrValues['data_field3'] ) ); elseif( isset( $arrValues['data_field3'] ) ) $this->setDataField3( $arrValues['data_field3'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['blob'] ) && $boolDirectSet ) $this->set( 'm_strBlob', trim( $arrValues['blob'] ) ); elseif( isset( $arrValues['blob'] ) ) $this->setBlob( $arrValues['blob'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		$this->m_boolInitialized = true;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDataBlobTypeId( $intDataBlobTypeId ) {
		$this->set( 'm_intDataBlobTypeId', CStrings::strToIntDef( $intDataBlobTypeId, NULL, false ) );
	}

	public function getDataBlobTypeId() {
		return $this->m_intDataBlobTypeId;
	}

	public function sqlDataBlobTypeId() {
		return ( true == isset( $this->m_intDataBlobTypeId ) ) ? ( string ) $this->m_intDataBlobTypeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->set( 'm_intLeaseCustomerId', CStrings::strToIntDef( $intLeaseCustomerId, NULL, false ) );
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function sqlLeaseCustomerId() {
		return ( true == isset( $this->m_intLeaseCustomerId ) ) ? ( string ) $this->m_intLeaseCustomerId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setDataField1( $strDataField1 ) {
		$this->set( 'm_strDataField1', CStrings::strTrimDef( $strDataField1, -1, NULL, true ) );
	}

	public function getDataField1() {
		return $this->m_strDataField1;
	}

	public function sqlDataField1() {
		return ( true == isset( $this->m_strDataField1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataField1 ) : '\'' . addslashes( $this->m_strDataField1 ) . '\'' ) : 'NULL';
	}

	public function setDataField2( $strDataField2 ) {
		$this->set( 'm_strDataField2', CStrings::strTrimDef( $strDataField2, -1, NULL, true ) );
	}

	public function getDataField2() {
		return $this->m_strDataField2;
	}

	public function sqlDataField2() {
		return ( true == isset( $this->m_strDataField2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataField2 ) : '\'' . addslashes( $this->m_strDataField2 ) . '\'' ) : '\'NULL\'';
	}

	public function setDataField3( $strDataField3 ) {
		$this->set( 'm_strDataField3', CStrings::strTrimDef( $strDataField3, -1, NULL, true ) );
	}

	public function getDataField3() {
		return $this->m_strDataField3;
	}

	public function sqlDataField3() {
		return ( true == isset( $this->m_strDataField3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataField3 ) : '\'' . addslashes( $this->m_strDataField3 ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setBlob( $strBlob ) {
		$this->set( 'm_strBlob', CStrings::strTrimDef( $strBlob, -1, NULL, true ) );
	}

	public function getBlob() {
		return $this->m_strBlob;
	}

	public function sqlBlob() {
		return ( true == isset( $this->m_strBlob ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBlob ) : '\'' . addslashes( $this->m_strBlob ) . '\'' ) : '\'NULL\'';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( cid, data_blob_type_id, property_id, lease_customer_id, customer_id, lease_id, employee_id, applicant_application_id, applicant_id, application_id, ap_payee_id, data_field1, data_field2, data_field3, order_num, blob, updated_on, id )
					VALUES ( ' .
						$this->sqlCid() . ', ' .
						$this->sqlDataBlobTypeId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseCustomerId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlApplicantApplicationId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlDataField1() . ', ' .
						$this->sqlDataField2() . ', ' .
						$this->sqlDataField3() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlBlob() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$strId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_blob_type_id = ' . $this->sqlDataBlobTypeId(). ',' ; } elseif( true == array_key_exists( 'DataBlobTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_blob_type_id = ' . $this->sqlDataBlobTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId(). ',' ; } elseif( true == array_key_exists( 'LeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_field1 = ' . $this->sqlDataField1(). ',' ; } elseif( true == array_key_exists( 'DataField1', $this->getChangedColumns() ) ) { $strSql .= ' data_field1 = ' . $this->sqlDataField1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_field2 = ' . $this->sqlDataField2(). ',' ; } elseif( true == array_key_exists( 'DataField2', $this->getChangedColumns() ) ) { $strSql .= ' data_field2 = ' . $this->sqlDataField2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_field3 = ' . $this->sqlDataField3(). ',' ; } elseif( true == array_key_exists( 'DataField3', $this->getChangedColumns() ) ) { $strSql .= ' data_field3 = ' . $this->sqlDataField3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blob = ' . $this->sqlBlob(). ',' ; } elseif( true == array_key_exists( 'Blob', $this->getChangedColumns() ) ) { $strSql .= ' blob = ' . $this->sqlBlob() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'cid' => $this->getCid(),
			'data_blob_type_id' => $this->getDataBlobTypeId(),
			'property_id' => $this->getPropertyId(),
			'lease_customer_id' => $this->getLeaseCustomerId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'employee_id' => $this->getEmployeeId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'data_field1' => $this->getDataField1(),
			'data_field2' => $this->getDataField2(),
			'data_field3' => $this->getDataField3(),
			'order_num' => $this->getOrderNum(),
			'blob' => $this->getBlob(),
			'updated_on' => $this->getUpdatedOn(),
			'id' => $this->getId()
		);
	}

}
?>