<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenanceStatuses
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenanceStatuses extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenanceStatus[]
	 */
	public static function fetchDefaultMaintenanceStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenanceStatus::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenanceStatus
	 */
	public static function fetchDefaultMaintenanceStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenanceStatus::class, $objDatabase );
	}

	public static function fetchDefaultMaintenanceStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_statuses', $objDatabase );
	}

	public static function fetchDefaultMaintenanceStatusById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenanceStatus( sprintf( 'SELECT * FROM default_maintenance_statuses WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceStatusesByMaintenanceStatusTypeId( $intMaintenanceStatusTypeId, $objDatabase ) {
		return self::fetchDefaultMaintenanceStatuses( sprintf( 'SELECT * FROM default_maintenance_statuses WHERE maintenance_status_type_id = %d', $intMaintenanceStatusTypeId ), $objDatabase );
	}

}
?>