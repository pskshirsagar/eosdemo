<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactSubmissionTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContactSubmissionTypes extends CEosPluralBase {

	/**
	 * @return CContactSubmissionType[]
	 */
	public static function fetchContactSubmissionTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CContactSubmissionType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CContactSubmissionType
	 */
	public static function fetchContactSubmissionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CContactSubmissionType::class, $objDatabase );
	}

	public static function fetchContactSubmissionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_submission_types', $objDatabase );
	}

	public static function fetchContactSubmissionTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissionType( sprintf( 'SELECT * FROM contact_submission_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionTypesByCid( $intCid, $objDatabase ) {
		return self::fetchContactSubmissionTypes( sprintf( 'SELECT * FROM contact_submission_types WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>