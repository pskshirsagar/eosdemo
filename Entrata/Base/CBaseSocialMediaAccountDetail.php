<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSocialMediaAccountDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.social_media_account_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSocialPostTypeId;
	protected $m_strPageId;
	protected $m_strUserAccessToken;
	protected $m_strTokenSecret;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intSocialMediaAccountId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['social_post_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostTypeId', trim( $arrValues['social_post_type_id'] ) ); elseif( isset( $arrValues['social_post_type_id'] ) ) $this->setSocialPostTypeId( $arrValues['social_post_type_id'] );
		if( isset( $arrValues['page_id'] ) && $boolDirectSet ) $this->set( 'm_strPageId', trim( stripcslashes( $arrValues['page_id'] ) ) ); elseif( isset( $arrValues['page_id'] ) ) $this->setPageId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['page_id'] ) : $arrValues['page_id'] );
		if( isset( $arrValues['user_access_token'] ) && $boolDirectSet ) $this->set( 'm_strUserAccessToken', trim( stripcslashes( $arrValues['user_access_token'] ) ) ); elseif( isset( $arrValues['user_access_token'] ) ) $this->setUserAccessToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_access_token'] ) : $arrValues['user_access_token'] );
		if( isset( $arrValues['token_secret'] ) && $boolDirectSet ) $this->set( 'm_strTokenSecret', trim( stripcslashes( $arrValues['token_secret'] ) ) ); elseif( isset( $arrValues['token_secret'] ) ) $this->setTokenSecret( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['token_secret'] ) : $arrValues['token_secret'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['social_media_account_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialMediaAccountId', trim( $arrValues['social_media_account_id'] ) ); elseif( isset( $arrValues['social_media_account_id'] ) ) $this->setSocialMediaAccountId( $arrValues['social_media_account_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSocialPostTypeId( $intSocialPostTypeId ) {
		$this->set( 'm_intSocialPostTypeId', CStrings::strToIntDef( $intSocialPostTypeId, NULL, false ) );
	}

	public function getSocialPostTypeId() {
		return $this->m_intSocialPostTypeId;
	}

	public function sqlSocialPostTypeId() {
		return ( true == isset( $this->m_intSocialPostTypeId ) ) ? ( string ) $this->m_intSocialPostTypeId : 'NULL';
	}

	public function setPageId( $strPageId ) {
		$this->set( 'm_strPageId', CStrings::strTrimDef( $strPageId, 250, NULL, true ) );
	}

	public function getPageId() {
		return $this->m_strPageId;
	}

	public function sqlPageId() {
		return ( true == isset( $this->m_strPageId ) ) ? '\'' . addslashes( $this->m_strPageId ) . '\'' : 'NULL';
	}

	public function setUserAccessToken( $strUserAccessToken ) {
		$this->set( 'm_strUserAccessToken', CStrings::strTrimDef( $strUserAccessToken, 500, NULL, true ) );
	}

	public function getUserAccessToken() {
		return $this->m_strUserAccessToken;
	}

	public function sqlUserAccessToken() {
		return ( true == isset( $this->m_strUserAccessToken ) ) ? '\'' . addslashes( $this->m_strUserAccessToken ) . '\'' : 'NULL';
	}

	public function setTokenSecret( $strTokenSecret ) {
		$this->set( 'm_strTokenSecret', CStrings::strTrimDef( $strTokenSecret, 500, NULL, true ) );
	}

	public function getTokenSecret() {
		return $this->m_strTokenSecret;
	}

	public function sqlTokenSecret() {
		return ( true == isset( $this->m_strTokenSecret ) ) ? '\'' . addslashes( $this->m_strTokenSecret ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setSocialMediaAccountId( $intSocialMediaAccountId ) {
		$this->set( 'm_intSocialMediaAccountId', CStrings::strToIntDef( $intSocialMediaAccountId, NULL, false ) );
	}

	public function getSocialMediaAccountId() {
		return $this->m_intSocialMediaAccountId;
	}

	public function sqlSocialMediaAccountId() {
		return ( true == isset( $this->m_intSocialMediaAccountId ) ) ? ( string ) $this->m_intSocialMediaAccountId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, social_post_type_id, page_id, user_access_token, token_secret, deleted_by, deleted_on, created_by, created_on, details, updated_by, updated_on, social_media_account_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSocialPostTypeId() . ', ' .
						$this->sqlPageId() . ', ' .
						$this->sqlUserAccessToken() . ', ' .
						$this->sqlTokenSecret() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlSocialMediaAccountId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_type_id = ' . $this->sqlSocialPostTypeId(). ',' ; } elseif( true == array_key_exists( 'SocialPostTypeId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_type_id = ' . $this->sqlSocialPostTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_id = ' . $this->sqlPageId(). ',' ; } elseif( true == array_key_exists( 'PageId', $this->getChangedColumns() ) ) { $strSql .= ' page_id = ' . $this->sqlPageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_access_token = ' . $this->sqlUserAccessToken(). ',' ; } elseif( true == array_key_exists( 'UserAccessToken', $this->getChangedColumns() ) ) { $strSql .= ' user_access_token = ' . $this->sqlUserAccessToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token_secret = ' . $this->sqlTokenSecret(). ',' ; } elseif( true == array_key_exists( 'TokenSecret', $this->getChangedColumns() ) ) { $strSql .= ' token_secret = ' . $this->sqlTokenSecret() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_media_account_id = ' . $this->sqlSocialMediaAccountId(). ',' ; } elseif( true == array_key_exists( 'SocialMediaAccountId', $this->getChangedColumns() ) ) { $strSql .= ' social_media_account_id = ' . $this->sqlSocialMediaAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'social_post_type_id' => $this->getSocialPostTypeId(),
			'page_id' => $this->getPageId(),
			'user_access_token' => $this->getUserAccessToken(),
			'token_secret' => $this->getTokenSecret(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'social_media_account_id' => $this->getSocialMediaAccountId()
		);
	}

}
?>