<?php

class CBaseCheckComponentType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.check_component_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltXPosition;
	protected $m_fltYPosition;
	protected $m_intWidth;
	protected $m_intHeight;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['x_position'] ) && $boolDirectSet ) $this->set( 'm_fltXPosition', trim( $arrValues['x_position'] ) ); elseif( isset( $arrValues['x_position'] ) ) $this->setXPosition( $arrValues['x_position'] );
		if( isset( $arrValues['y_position'] ) && $boolDirectSet ) $this->set( 'm_fltYPosition', trim( $arrValues['y_position'] ) ); elseif( isset( $arrValues['y_position'] ) ) $this->setYPosition( $arrValues['y_position'] );
		if( isset( $arrValues['width'] ) && $boolDirectSet ) $this->set( 'm_intWidth', trim( $arrValues['width'] ) ); elseif( isset( $arrValues['width'] ) ) $this->setWidth( $arrValues['width'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setXPosition( $fltXPosition ) {
		$this->set( 'm_fltXPosition', CStrings::strToFloatDef( $fltXPosition, NULL, false, 4 ) );
	}

	public function getXPosition() {
		return $this->m_fltXPosition;
	}

	public function sqlXPosition() {
		return ( true == isset( $this->m_fltXPosition ) ) ? ( string ) $this->m_fltXPosition : 'NULL';
	}

	public function setYPosition( $fltYPosition ) {
		$this->set( 'm_fltYPosition', CStrings::strToFloatDef( $fltYPosition, NULL, false, 4 ) );
	}

	public function getYPosition() {
		return $this->m_fltYPosition;
	}

	public function sqlYPosition() {
		return ( true == isset( $this->m_fltYPosition ) ) ? ( string ) $this->m_fltYPosition : 'NULL';
	}

	public function setWidth( $intWidth ) {
		$this->set( 'm_intWidth', CStrings::strToIntDef( $intWidth, NULL, false ) );
	}

	public function getWidth() {
		return $this->m_intWidth;
	}

	public function sqlWidth() {
		return ( true == isset( $this->m_intWidth ) ) ? ( string ) $this->m_intWidth : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'x_position' => $this->getXPosition(),
			'y_position' => $this->getYPosition(),
			'width' => $this->getWidth(),
			'height' => $this->getHeight(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>