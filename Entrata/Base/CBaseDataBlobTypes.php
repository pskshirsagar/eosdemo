<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataBlobTypes
 * Do not add any new functions to this class.
 */

class CBaseDataBlobTypes extends CEosPluralBase {

	/**
	 * @return CDataBlobType[]
	 */
	public static function fetchDataBlobTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDataBlobType::class, $objDatabase );
	}

	/**
	 * @return CDataBlobType
	 */
	public static function fetchDataBlobType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDataBlobType::class, $objDatabase );
	}

	public static function fetchDataBlobTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'data_blob_types', $objDatabase );
	}

	public static function fetchDataBlobTypeById( $intId, $objDatabase ) {
		return self::fetchDataBlobType( sprintf( 'SELECT * FROM data_blob_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>