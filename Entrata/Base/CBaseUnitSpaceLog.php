<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.unit_space_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitKindId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyFloorId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intUnitSpaceLogTypeId;
	protected $m_intUnitSpaceStatusTypeId;
	protected $m_intUnitExclusionReasonTypeId;
	protected $m_intOccupancyTypeId;
	protected $m_intApplicationLeaseId;
	protected $m_intHoldLeaseId;
	protected $m_intFutureLeaseId;
	protected $m_intCurrentLeaseId;
	protected $m_intPastLeaseId;
	protected $m_intPriorUnitSpaceLogId;
	protected $m_intReferenceId;
	protected $m_intReferenceLogId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strBuildingName;
	protected $m_strMarketingUnitNumber;
	protected $m_strMarketingUnitNumberCache;
	protected $m_strUnitNumber;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_fltInitialRent;
	protected $m_fltBudgetedBaseRent;
	protected $m_fltBudgetedAmenityRent;
	protected $m_fltBudgetedRent;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strMoveOutDate;
	protected $m_strReserveUntil;
	protected $m_strAvailableOn;
	protected $m_strMakeReadyDate;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_intShowOnWebsite;
	protected $m_intIsMarketed;
	protected $m_boolIsAvailable;
	protected $m_boolIsReportable;
	protected $m_intIsOpeningLog;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_fltInitialRent = '0';
		$this->m_fltBudgetedBaseRent = '0';
		$this->m_fltBudgetedAmenityRent = '0';
		$this->m_fltBudgetedRent = '0';
		$this->m_intShowOnWebsite = '0';
		$this->m_intIsMarketed = '0';
		$this->m_boolIsAvailable = false;
		$this->m_boolIsReportable = false;
		$this->m_intIsOpeningLog = '0';
		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['unit_space_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceLogTypeId', trim( $arrValues['unit_space_log_type_id'] ) ); elseif( isset( $arrValues['unit_space_log_type_id'] ) ) $this->setUnitSpaceLogTypeId( $arrValues['unit_space_log_type_id'] );
		if( isset( $arrValues['unit_space_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceStatusTypeId', trim( $arrValues['unit_space_status_type_id'] ) ); elseif( isset( $arrValues['unit_space_status_type_id'] ) ) $this->setUnitSpaceStatusTypeId( $arrValues['unit_space_status_type_id'] );
		if( isset( $arrValues['unit_exclusion_reason_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitExclusionReasonTypeId', trim( $arrValues['unit_exclusion_reason_type_id'] ) ); elseif( isset( $arrValues['unit_exclusion_reason_type_id'] ) ) $this->setUnitExclusionReasonTypeId( $arrValues['unit_exclusion_reason_type_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['application_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationLeaseId', trim( $arrValues['application_lease_id'] ) ); elseif( isset( $arrValues['application_lease_id'] ) ) $this->setApplicationLeaseId( $arrValues['application_lease_id'] );
		if( isset( $arrValues['hold_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intHoldLeaseId', trim( $arrValues['hold_lease_id'] ) ); elseif( isset( $arrValues['hold_lease_id'] ) ) $this->setHoldLeaseId( $arrValues['hold_lease_id'] );
		if( isset( $arrValues['future_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intFutureLeaseId', trim( $arrValues['future_lease_id'] ) ); elseif( isset( $arrValues['future_lease_id'] ) ) $this->setFutureLeaseId( $arrValues['future_lease_id'] );
		if( isset( $arrValues['current_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrentLeaseId', trim( $arrValues['current_lease_id'] ) ); elseif( isset( $arrValues['current_lease_id'] ) ) $this->setCurrentLeaseId( $arrValues['current_lease_id'] );
		if( isset( $arrValues['past_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intPastLeaseId', trim( $arrValues['past_lease_id'] ) ); elseif( isset( $arrValues['past_lease_id'] ) ) $this->setPastLeaseId( $arrValues['past_lease_id'] );
		if( isset( $arrValues['prior_unit_space_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorUnitSpaceLogId', trim( $arrValues['prior_unit_space_log_id'] ) ); elseif( isset( $arrValues['prior_unit_space_log_id'] ) ) $this->setPriorUnitSpaceLogId( $arrValues['prior_unit_space_log_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['reference_log_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceLogId', trim( $arrValues['reference_log_id'] ) ); elseif( isset( $arrValues['reference_log_id'] ) ) $this->setReferenceLogId( $arrValues['reference_log_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( stripcslashes( $arrValues['building_name'] ) ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_name'] ) : $arrValues['building_name'] );
		if( isset( $arrValues['marketing_unit_number'] ) && $boolDirectSet ) $this->set( 'm_strMarketingUnitNumber', trim( stripcslashes( $arrValues['marketing_unit_number'] ) ) ); elseif( isset( $arrValues['marketing_unit_number'] ) ) $this->setMarketingUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_unit_number'] ) : $arrValues['marketing_unit_number'] );
		if( isset( $arrValues['marketing_unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strMarketingUnitNumberCache', trim( stripcslashes( $arrValues['marketing_unit_number_cache'] ) ) ); elseif( isset( $arrValues['marketing_unit_number_cache'] ) ) $this->setMarketingUnitNumberCache( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_unit_number_cache'] ) : $arrValues['marketing_unit_number_cache'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( stripcslashes( $arrValues['unit_number_cache'] ) ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_cache'] ) : $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['initial_rent'] ) && $boolDirectSet ) $this->set( 'm_fltInitialRent', trim( $arrValues['initial_rent'] ) ); elseif( isset( $arrValues['initial_rent'] ) ) $this->setInitialRent( $arrValues['initial_rent'] );
		if( isset( $arrValues['budgeted_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetedBaseRent', trim( $arrValues['budgeted_base_rent'] ) ); elseif( isset( $arrValues['budgeted_base_rent'] ) ) $this->setBudgetedBaseRent( $arrValues['budgeted_base_rent'] );
		if( isset( $arrValues['budgeted_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetedAmenityRent', trim( $arrValues['budgeted_amenity_rent'] ) ); elseif( isset( $arrValues['budgeted_amenity_rent'] ) ) $this->setBudgetedAmenityRent( $arrValues['budgeted_amenity_rent'] );
		if( isset( $arrValues['budgeted_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetedRent', trim( $arrValues['budgeted_rent'] ) ); elseif( isset( $arrValues['budgeted_rent'] ) ) $this->setBudgetedRent( $arrValues['budgeted_rent'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['reserve_until'] ) && $boolDirectSet ) $this->set( 'm_strReserveUntil', trim( $arrValues['reserve_until'] ) ); elseif( isset( $arrValues['reserve_until'] ) ) $this->setReserveUntil( $arrValues['reserve_until'] );
		if( isset( $arrValues['available_on'] ) && $boolDirectSet ) $this->set( 'm_strAvailableOn', trim( $arrValues['available_on'] ) ); elseif( isset( $arrValues['available_on'] ) ) $this->setAvailableOn( $arrValues['available_on'] );
		if( isset( $arrValues['make_ready_date'] ) && $boolDirectSet ) $this->set( 'm_strMakeReadyDate', trim( $arrValues['make_ready_date'] ) ); elseif( isset( $arrValues['make_ready_date'] ) ) $this->setMakeReadyDate( $arrValues['make_ready_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_intShowOnWebsite', trim( $arrValues['show_on_website'] ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( $arrValues['show_on_website'] );
		if( isset( $arrValues['is_marketed'] ) && $boolDirectSet ) $this->set( 'm_intIsMarketed', trim( $arrValues['is_marketed'] ) ); elseif( isset( $arrValues['is_marketed'] ) ) $this->setIsMarketed( $arrValues['is_marketed'] );
		if( isset( $arrValues['is_available'] ) && $boolDirectSet ) $this->set( 'm_boolIsAvailable', trim( stripcslashes( $arrValues['is_available'] ) ) ); elseif( isset( $arrValues['is_available'] ) ) $this->setIsAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_available'] ) : $arrValues['is_available'] );
		if( isset( $arrValues['is_reportable'] ) && $boolDirectSet ) $this->set( 'm_boolIsReportable', trim( stripcslashes( $arrValues['is_reportable'] ) ) ); elseif( isset( $arrValues['is_reportable'] ) ) $this->setIsReportable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reportable'] ) : $arrValues['is_reportable'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setUnitSpaceLogTypeId( $intUnitSpaceLogTypeId ) {
		$this->set( 'm_intUnitSpaceLogTypeId', CStrings::strToIntDef( $intUnitSpaceLogTypeId, NULL, false ) );
	}

	public function getUnitSpaceLogTypeId() {
		return $this->m_intUnitSpaceLogTypeId;
	}

	public function sqlUnitSpaceLogTypeId() {
		return ( true == isset( $this->m_intUnitSpaceLogTypeId ) ) ? ( string ) $this->m_intUnitSpaceLogTypeId : 'NULL';
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->set( 'm_intUnitSpaceStatusTypeId', CStrings::strToIntDef( $intUnitSpaceStatusTypeId, NULL, false ) );
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

	public function sqlUnitSpaceStatusTypeId() {
		return ( true == isset( $this->m_intUnitSpaceStatusTypeId ) ) ? ( string ) $this->m_intUnitSpaceStatusTypeId : 'NULL';
	}

	public function setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$this->set( 'm_intUnitExclusionReasonTypeId', CStrings::strToIntDef( $intUnitExclusionReasonTypeId, NULL, false ) );
	}

	public function getUnitExclusionReasonTypeId() {
		return $this->m_intUnitExclusionReasonTypeId;
	}

	public function sqlUnitExclusionReasonTypeId() {
		return ( true == isset( $this->m_intUnitExclusionReasonTypeId ) ) ? ( string ) $this->m_intUnitExclusionReasonTypeId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setApplicationLeaseId( $intApplicationLeaseId ) {
		$this->set( 'm_intApplicationLeaseId', CStrings::strToIntDef( $intApplicationLeaseId, NULL, false ) );
	}

	public function getApplicationLeaseId() {
		return $this->m_intApplicationLeaseId;
	}

	public function sqlApplicationLeaseId() {
		return ( true == isset( $this->m_intApplicationLeaseId ) ) ? ( string ) $this->m_intApplicationLeaseId : 'NULL';
	}

	public function setHoldLeaseId( $intHoldLeaseId ) {
		$this->set( 'm_intHoldLeaseId', CStrings::strToIntDef( $intHoldLeaseId, NULL, false ) );
	}

	public function getHoldLeaseId() {
		return $this->m_intHoldLeaseId;
	}

	public function sqlHoldLeaseId() {
		return ( true == isset( $this->m_intHoldLeaseId ) ) ? ( string ) $this->m_intHoldLeaseId : 'NULL';
	}

	public function setFutureLeaseId( $intFutureLeaseId ) {
		$this->set( 'm_intFutureLeaseId', CStrings::strToIntDef( $intFutureLeaseId, NULL, false ) );
	}

	public function getFutureLeaseId() {
		return $this->m_intFutureLeaseId;
	}

	public function sqlFutureLeaseId() {
		return ( true == isset( $this->m_intFutureLeaseId ) ) ? ( string ) $this->m_intFutureLeaseId : 'NULL';
	}

	public function setCurrentLeaseId( $intCurrentLeaseId ) {
		$this->set( 'm_intCurrentLeaseId', CStrings::strToIntDef( $intCurrentLeaseId, NULL, false ) );
	}

	public function getCurrentLeaseId() {
		return $this->m_intCurrentLeaseId;
	}

	public function sqlCurrentLeaseId() {
		return ( true == isset( $this->m_intCurrentLeaseId ) ) ? ( string ) $this->m_intCurrentLeaseId : 'NULL';
	}

	public function setPastLeaseId( $intPastLeaseId ) {
		$this->set( 'm_intPastLeaseId', CStrings::strToIntDef( $intPastLeaseId, NULL, false ) );
	}

	public function getPastLeaseId() {
		return $this->m_intPastLeaseId;
	}

	public function sqlPastLeaseId() {
		return ( true == isset( $this->m_intPastLeaseId ) ) ? ( string ) $this->m_intPastLeaseId : 'NULL';
	}

	public function setPriorUnitSpaceLogId( $intPriorUnitSpaceLogId ) {
		$this->set( 'm_intPriorUnitSpaceLogId', CStrings::strToIntDef( $intPriorUnitSpaceLogId, NULL, false ) );
	}

	public function getPriorUnitSpaceLogId() {
		return $this->m_intPriorUnitSpaceLogId;
	}

	public function sqlPriorUnitSpaceLogId() {
		return ( true == isset( $this->m_intPriorUnitSpaceLogId ) ) ? ( string ) $this->m_intPriorUnitSpaceLogId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setReferenceLogId( $intReferenceLogId ) {
		$this->set( 'm_intReferenceLogId', CStrings::strToIntDef( $intReferenceLogId, NULL, false ) );
	}

	public function getReferenceLogId() {
		return $this->m_intReferenceLogId;
	}

	public function sqlReferenceLogId() {
		return ( true == isset( $this->m_intReferenceLogId ) ) ? ( string ) $this->m_intReferenceLogId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? '\'' . addslashes( $this->m_strBuildingName ) . '\'' : 'NULL';
	}

	public function setMarketingUnitNumber( $strMarketingUnitNumber ) {
		$this->set( 'm_strMarketingUnitNumber', CStrings::strTrimDef( $strMarketingUnitNumber, 50, NULL, true ) );
	}

	public function getMarketingUnitNumber() {
		return $this->m_strMarketingUnitNumber;
	}

	public function sqlMarketingUnitNumber() {
		return ( true == isset( $this->m_strMarketingUnitNumber ) ) ? '\'' . addslashes( $this->m_strMarketingUnitNumber ) . '\'' : 'NULL';
	}

	public function setMarketingUnitNumberCache( $strMarketingUnitNumberCache ) {
		$this->set( 'm_strMarketingUnitNumberCache', CStrings::strTrimDef( $strMarketingUnitNumberCache, 50, NULL, true ) );
	}

	public function getMarketingUnitNumberCache() {
		return $this->m_strMarketingUnitNumberCache;
	}

	public function sqlMarketingUnitNumberCache() {
		return ( true == isset( $this->m_strMarketingUnitNumberCache ) ) ? '\'' . addslashes( $this->m_strMarketingUnitNumberCache ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setInitialRent( $fltInitialRent ) {
		$this->set( 'm_fltInitialRent', CStrings::strToFloatDef( $fltInitialRent, NULL, false, 2 ) );
	}

	public function getInitialRent() {
		return $this->m_fltInitialRent;
	}

	public function sqlInitialRent() {
		return ( true == isset( $this->m_fltInitialRent ) ) ? ( string ) $this->m_fltInitialRent : '0';
	}

	public function setBudgetedBaseRent( $fltBudgetedBaseRent ) {
		$this->set( 'm_fltBudgetedBaseRent', CStrings::strToFloatDef( $fltBudgetedBaseRent, NULL, false, 2 ) );
	}

	public function getBudgetedBaseRent() {
		return $this->m_fltBudgetedBaseRent;
	}

	public function sqlBudgetedBaseRent() {
		return ( true == isset( $this->m_fltBudgetedBaseRent ) ) ? ( string ) $this->m_fltBudgetedBaseRent : '0';
	}

	public function setBudgetedAmenityRent( $fltBudgetedAmenityRent ) {
		$this->set( 'm_fltBudgetedAmenityRent', CStrings::strToFloatDef( $fltBudgetedAmenityRent, NULL, false, 2 ) );
	}

	public function getBudgetedAmenityRent() {
		return $this->m_fltBudgetedAmenityRent;
	}

	public function sqlBudgetedAmenityRent() {
		return ( true == isset( $this->m_fltBudgetedAmenityRent ) ) ? ( string ) $this->m_fltBudgetedAmenityRent : '0';
	}

	public function setBudgetedRent( $fltBudgetedRent ) {
		$this->set( 'm_fltBudgetedRent', CStrings::strToFloatDef( $fltBudgetedRent, NULL, false, 2 ) );
	}

	public function getBudgetedRent() {
		return $this->m_fltBudgetedRent;
	}

	public function sqlBudgetedRent() {
		return ( true == isset( $this->m_fltBudgetedRent ) ) ? ( string ) $this->m_fltBudgetedRent : '0';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setReserveUntil( $strReserveUntil ) {
		$this->set( 'm_strReserveUntil', CStrings::strTrimDef( $strReserveUntil, -1, NULL, true ) );
	}

	public function getReserveUntil() {
		return $this->m_strReserveUntil;
	}

	public function sqlReserveUntil() {
		return ( true == isset( $this->m_strReserveUntil ) ) ? '\'' . $this->m_strReserveUntil . '\'' : 'NOW()';
	}

	public function setAvailableOn( $strAvailableOn ) {
		$this->set( 'm_strAvailableOn', CStrings::strTrimDef( $strAvailableOn, -1, NULL, true ) );
	}

	public function getAvailableOn() {
		return $this->m_strAvailableOn;
	}

	public function sqlAvailableOn() {
		return ( true == isset( $this->m_strAvailableOn ) ) ? '\'' . $this->m_strAvailableOn . '\'' : 'NOW()';
	}

	public function setMakeReadyDate( $strMakeReadyDate ) {
		$this->set( 'm_strMakeReadyDate', CStrings::strTrimDef( $strMakeReadyDate, -1, NULL, true ) );
	}

	public function getMakeReadyDate() {
		return $this->m_strMakeReadyDate;
	}

	public function sqlMakeReadyDate() {
		return ( true == isset( $this->m_strMakeReadyDate ) ) ? '\'' . $this->m_strMakeReadyDate . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setShowOnWebsite( $intShowOnWebsite ) {
		$this->set( 'm_intShowOnWebsite', CStrings::strToIntDef( $intShowOnWebsite, NULL, false ) );
	}

	public function getShowOnWebsite() {
		return $this->m_intShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_intShowOnWebsite ) ) ? ( string ) $this->m_intShowOnWebsite : '0';
	}

	public function setIsMarketed( $intIsMarketed ) {
		$this->set( 'm_intIsMarketed', CStrings::strToIntDef( $intIsMarketed, NULL, false ) );
	}

	public function getIsMarketed() {
		return $this->m_intIsMarketed;
	}

	public function sqlIsMarketed() {
		return ( true == isset( $this->m_intIsMarketed ) ) ? ( string ) $this->m_intIsMarketed : '0';
	}

	public function setIsAvailable( $boolIsAvailable ) {
		$this->set( 'm_boolIsAvailable', CStrings::strToBool( $boolIsAvailable ) );
	}

	public function getIsAvailable() {
		return $this->m_boolIsAvailable;
	}

	public function sqlIsAvailable() {
		return ( true == isset( $this->m_boolIsAvailable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAvailable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReportable( $boolIsReportable ) {
		$this->set( 'm_boolIsReportable', CStrings::strToBool( $boolIsReportable ) );
	}

	public function getIsReportable() {
		return $this->m_boolIsReportable;
	}

	public function sqlIsReportable() {
		return ( true == isset( $this->m_boolIsReportable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReportable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_kind_id, unit_type_id, property_building_id, property_floorplan_id, property_floor_id, property_unit_id, unit_space_id, unit_space_log_type_id, unit_space_status_type_id, unit_exclusion_reason_type_id, occupancy_type_id, application_lease_id, hold_lease_id, future_lease_id, current_lease_id, past_lease_id, prior_unit_space_log_id, reference_id, reference_log_id, period_id, reporting_period_id, effective_period_id, original_period_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, building_name, marketing_unit_number, marketing_unit_number_cache, unit_number, unit_number_cache, display_number, initial_rent, budgeted_base_rent, budgeted_amenity_rent, budgeted_rent, details, move_out_date, reserve_until, available_on, make_ready_date, log_datetime, effective_date, show_on_website, is_marketed, is_available, is_reportable, is_opening_log, is_post_month_ignored, is_post_date_ignored, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitKindId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlPropertyFloorId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlUnitSpaceLogTypeId() . ', ' .
						$this->sqlUnitSpaceStatusTypeId() . ', ' .
						$this->sqlUnitExclusionReasonTypeId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlApplicationLeaseId() . ', ' .
						$this->sqlHoldLeaseId() . ', ' .
						$this->sqlFutureLeaseId() . ', ' .
						$this->sqlCurrentLeaseId() . ', ' .
						$this->sqlPastLeaseId() . ', ' .
						$this->sqlPriorUnitSpaceLogId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlReferenceLogId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlMarketingUnitNumber() . ', ' .
						$this->sqlMarketingUnitNumberCache() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlInitialRent() . ', ' .
						$this->sqlBudgetedBaseRent() . ', ' .
						$this->sqlBudgetedAmenityRent() . ', ' .
						$this->sqlBudgetedRent() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlReserveUntil() . ', ' .
						$this->sqlAvailableOn() . ', ' .
						$this->sqlMakeReadyDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlIsMarketed() . ', ' .
						$this->sqlIsAvailable() . ', ' .
						$this->sqlIsReportable() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_log_type_id = ' . $this->sqlUnitSpaceLogTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_log_type_id = ' . $this->sqlUnitSpaceLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitExclusionReasonTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_lease_id = ' . $this->sqlApplicationLeaseId(). ',' ; } elseif( true == array_key_exists( 'ApplicationLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' application_lease_id = ' . $this->sqlApplicationLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hold_lease_id = ' . $this->sqlHoldLeaseId(). ',' ; } elseif( true == array_key_exists( 'HoldLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' hold_lease_id = ' . $this->sqlHoldLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' future_lease_id = ' . $this->sqlFutureLeaseId(). ',' ; } elseif( true == array_key_exists( 'FutureLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' future_lease_id = ' . $this->sqlFutureLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_lease_id = ' . $this->sqlCurrentLeaseId(). ',' ; } elseif( true == array_key_exists( 'CurrentLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' current_lease_id = ' . $this->sqlCurrentLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' past_lease_id = ' . $this->sqlPastLeaseId(). ',' ; } elseif( true == array_key_exists( 'PastLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' past_lease_id = ' . $this->sqlPastLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_unit_space_log_id = ' . $this->sqlPriorUnitSpaceLogId(). ',' ; } elseif( true == array_key_exists( 'PriorUnitSpaceLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_unit_space_log_id = ' . $this->sqlPriorUnitSpaceLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_log_id = ' . $this->sqlReferenceLogId(). ',' ; } elseif( true == array_key_exists( 'ReferenceLogId', $this->getChangedColumns() ) ) { $strSql .= ' reference_log_id = ' . $this->sqlReferenceLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber(). ',' ; } elseif( true == array_key_exists( 'MarketingUnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_unit_number_cache = ' . $this->sqlMarketingUnitNumberCache(). ',' ; } elseif( true == array_key_exists( 'MarketingUnitNumberCache', $this->getChangedColumns() ) ) { $strSql .= ' marketing_unit_number_cache = ' . $this->sqlMarketingUnitNumberCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache(). ',' ; } elseif( true == array_key_exists( 'UnitNumberCache', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber(). ',' ; } elseif( true == array_key_exists( 'DisplayNumber', $this->getChangedColumns() ) ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_rent = ' . $this->sqlInitialRent(). ',' ; } elseif( true == array_key_exists( 'InitialRent', $this->getChangedColumns() ) ) { $strSql .= ' initial_rent = ' . $this->sqlInitialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budgeted_base_rent = ' . $this->sqlBudgetedBaseRent(). ',' ; } elseif( true == array_key_exists( 'BudgetedBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' budgeted_base_rent = ' . $this->sqlBudgetedBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budgeted_amenity_rent = ' . $this->sqlBudgetedAmenityRent(). ',' ; } elseif( true == array_key_exists( 'BudgetedAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' budgeted_amenity_rent = ' . $this->sqlBudgetedAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budgeted_rent = ' . $this->sqlBudgetedRent(). ',' ; } elseif( true == array_key_exists( 'BudgetedRent', $this->getChangedColumns() ) ) { $strSql .= ' budgeted_rent = ' . $this->sqlBudgetedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserve_until = ' . $this->sqlReserveUntil(). ',' ; } elseif( true == array_key_exists( 'ReserveUntil', $this->getChangedColumns() ) ) { $strSql .= ' reserve_until = ' . $this->sqlReserveUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn(). ',' ; } elseif( true == array_key_exists( 'AvailableOn', $this->getChangedColumns() ) ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' make_ready_date = ' . $this->sqlMakeReadyDate(). ',' ; } elseif( true == array_key_exists( 'MakeReadyDate', $this->getChangedColumns() ) ) { $strSql .= ' make_ready_date = ' . $this->sqlMakeReadyDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed(). ',' ; } elseif( true == array_key_exists( 'IsMarketed', $this->getChangedColumns() ) ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable(). ',' ; } elseif( true == array_key_exists( 'IsAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reportable = ' . $this->sqlIsReportable(). ',' ; } elseif( true == array_key_exists( 'IsReportable', $this->getChangedColumns() ) ) { $strSql .= ' is_reportable = ' . $this->sqlIsReportable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'unit_space_log_type_id' => $this->getUnitSpaceLogTypeId(),
			'unit_space_status_type_id' => $this->getUnitSpaceStatusTypeId(),
			'unit_exclusion_reason_type_id' => $this->getUnitExclusionReasonTypeId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'application_lease_id' => $this->getApplicationLeaseId(),
			'hold_lease_id' => $this->getHoldLeaseId(),
			'future_lease_id' => $this->getFutureLeaseId(),
			'current_lease_id' => $this->getCurrentLeaseId(),
			'past_lease_id' => $this->getPastLeaseId(),
			'prior_unit_space_log_id' => $this->getPriorUnitSpaceLogId(),
			'reference_id' => $this->getReferenceId(),
			'reference_log_id' => $this->getReferenceLogId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'building_name' => $this->getBuildingName(),
			'marketing_unit_number' => $this->getMarketingUnitNumber(),
			'marketing_unit_number_cache' => $this->getMarketingUnitNumberCache(),
			'unit_number' => $this->getUnitNumber(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'initial_rent' => $this->getInitialRent(),
			'budgeted_base_rent' => $this->getBudgetedBaseRent(),
			'budgeted_amenity_rent' => $this->getBudgetedAmenityRent(),
			'budgeted_rent' => $this->getBudgetedRent(),
			'details' => $this->getDetails(),
			'move_out_date' => $this->getMoveOutDate(),
			'reserve_until' => $this->getReserveUntil(),
			'available_on' => $this->getAvailableOn(),
			'make_ready_date' => $this->getMakeReadyDate(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'show_on_website' => $this->getShowOnWebsite(),
			'is_marketed' => $this->getIsMarketed(),
			'is_available' => $this->getIsAvailable(),
			'is_reportable' => $this->getIsReportable(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>