<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApExportBatches extends CEosPluralBase {

	/**
	 * @return CApExportBatch[]
	 */
	public static function fetchApExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApExportBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApExportBatch
	 */
	public static function fetchApExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApExportBatch', $objDatabase );
	}

	public static function fetchApExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_export_batches', $objDatabase );
	}

	public static function fetchApExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApExportBatch( sprintf( 'SELECT * FROM ap_export_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchApExportBatches( sprintf( 'SELECT * FROM ap_export_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApExportBatchesByApExportBatchTypeIdByCid( $intApExportBatchTypeId, $intCid, $objDatabase ) {
		return self::fetchApExportBatches( sprintf( 'SELECT * FROM ap_export_batches WHERE ap_export_batch_type_id = %d AND cid = %d', ( int ) $intApExportBatchTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApExportBatchesByScheduledApExportBatchIdByCid( $intScheduledApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApExportBatches( sprintf( 'SELECT * FROM ap_export_batches WHERE scheduled_ap_export_batch_id = %d AND cid = %d', ( int ) $intScheduledApExportBatchId, ( int ) $intCid ), $objDatabase );
	}

}
?>