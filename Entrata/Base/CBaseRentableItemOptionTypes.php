<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItemOptionTypes extends CEosPluralBase {

    const TABLE_RENTABLE_ITEM_OPTION_TYPES = 'public.rentable_item_option_types';

    public static function fetchRentableItemOptionTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CRentableItemOptionType', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchRentableItemOptionType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRentableItemOptionType', $objDatabase );
    }

    public static function fetchRentableItemOptionTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'rentable_item_option_types', $objDatabase );
    }

    public static function fetchRentableItemOptionTypeByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOptionType( sprintf( 'SELECT * FROM rentable_item_option_types WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionTypesByCid( $intCid, $objDatabase ) {
        return self::fetchRentableItemOptionTypes( sprintf( 'SELECT * FROM rentable_item_option_types WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionTypesByRentableItemTypeIdByCid( $intRentableItemTypeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOptionTypes( sprintf( 'SELECT * FROM rentable_item_option_types WHERE rentable_item_type_id = %d AND cid = %d', (int) $intRentableItemTypeId, (int) $intCid ), $objDatabase );
    }

}
?>