<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWaitListApplication extends CEosSingularBase {

	const TABLE_NAME = 'public.wait_list_applications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWaitListId;
	protected $m_intApplicationId;
	protected $m_intWaitListPointId;
	protected $m_intRejectionCount;
	protected $m_boolIsException;
	protected $m_strWaitStartOn;
	protected $m_strWaitEndOn;
	protected $m_intLockedBy;
	protected $m_strLockedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsException = false;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['wait_list_id'] ) && $boolDirectSet ) $this->set( 'm_intWaitListId', trim( $arrValues['wait_list_id'] ) ); elseif( isset( $arrValues['wait_list_id'] ) ) $this->setWaitListId( $arrValues['wait_list_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['wait_list_point_id'] ) && $boolDirectSet ) $this->set( 'm_intWaitListPointId', trim( $arrValues['wait_list_point_id'] ) ); elseif( isset( $arrValues['wait_list_point_id'] ) ) $this->setWaitListPointId( $arrValues['wait_list_point_id'] );
		if( isset( $arrValues['rejection_count'] ) && $boolDirectSet ) $this->set( 'm_intRejectionCount', trim( $arrValues['rejection_count'] ) ); elseif( isset( $arrValues['rejection_count'] ) ) $this->setRejectionCount( $arrValues['rejection_count'] );
		if( isset( $arrValues['is_exception'] ) && $boolDirectSet ) $this->set( 'm_boolIsException', trim( stripcslashes( $arrValues['is_exception'] ) ) ); elseif( isset( $arrValues['is_exception'] ) ) $this->setIsException( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_exception'] ) : $arrValues['is_exception'] );
		if( isset( $arrValues['wait_start_on'] ) && $boolDirectSet ) $this->set( 'm_strWaitStartOn', trim( $arrValues['wait_start_on'] ) ); elseif( isset( $arrValues['wait_start_on'] ) ) $this->setWaitStartOn( $arrValues['wait_start_on'] );
		if( isset( $arrValues['wait_end_on'] ) && $boolDirectSet ) $this->set( 'm_strWaitEndOn', trim( $arrValues['wait_end_on'] ) ); elseif( isset( $arrValues['wait_end_on'] ) ) $this->setWaitEndOn( $arrValues['wait_end_on'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWaitListId( $intWaitListId ) {
		$this->set( 'm_intWaitListId', CStrings::strToIntDef( $intWaitListId, NULL, false ) );
	}

	public function getWaitListId() {
		return $this->m_intWaitListId;
	}

	public function sqlWaitListId() {
		return ( true == isset( $this->m_intWaitListId ) ) ? ( string ) $this->m_intWaitListId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setWaitListPointId( $intWaitListPointId ) {
		$this->set( 'm_intWaitListPointId', CStrings::strToIntDef( $intWaitListPointId, NULL, false ) );
	}

	public function getWaitListPointId() {
		return $this->m_intWaitListPointId;
	}

	public function sqlWaitListPointId() {
		return ( true == isset( $this->m_intWaitListPointId ) ) ? ( string ) $this->m_intWaitListPointId : 'NULL';
	}

	public function setRejectionCount( $intRejectionCount ) {
		$this->set( 'm_intRejectionCount', CStrings::strToIntDef( $intRejectionCount, NULL, false ) );
	}

	public function getRejectionCount() {
		return $this->m_intRejectionCount;
	}

	public function sqlRejectionCount() {
		return ( true == isset( $this->m_intRejectionCount ) ) ? ( string ) $this->m_intRejectionCount : 'NULL';
	}

	public function setIsException( $boolIsException ) {
		$this->set( 'm_boolIsException', CStrings::strToBool( $boolIsException ) );
	}

	public function getIsException() {
		return $this->m_boolIsException;
	}

	public function sqlIsException() {
		return ( true == isset( $this->m_boolIsException ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsException ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWaitStartOn( $strWaitStartOn ) {
		$this->set( 'm_strWaitStartOn', CStrings::strTrimDef( $strWaitStartOn, -1, NULL, true ) );
	}

	public function getWaitStartOn() {
		return $this->m_strWaitStartOn;
	}

	public function sqlWaitStartOn() {
		return ( true == isset( $this->m_strWaitStartOn ) ) ? '\'' . $this->m_strWaitStartOn . '\'' : 'NULL';
	}

	public function setWaitEndOn( $strWaitEndOn ) {
		$this->set( 'm_strWaitEndOn', CStrings::strTrimDef( $strWaitEndOn, -1, NULL, true ) );
	}

	public function getWaitEndOn() {
		return $this->m_strWaitEndOn;
	}

	public function sqlWaitEndOn() {
		return ( true == isset( $this->m_strWaitEndOn ) ) ? '\'' . $this->m_strWaitEndOn . '\'' : 'NULL';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, wait_list_id, application_id, wait_list_point_id, rejection_count, is_exception, wait_start_on, wait_end_on, locked_by, locked_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWaitListId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlWaitListPointId() . ', ' .
 						$this->sqlRejectionCount() . ', ' .
 						$this->sqlIsException() . ', ' .
 						$this->sqlWaitStartOn() . ', ' .
 						$this->sqlWaitEndOn() . ', ' .
 						$this->sqlLockedBy() . ', ' .
 						$this->sqlLockedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_id = ' . $this->sqlWaitListId() . ','; } elseif( true == array_key_exists( 'WaitListId', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_id = ' . $this->sqlWaitListId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_point_id = ' . $this->sqlWaitListPointId() . ','; } elseif( true == array_key_exists( 'WaitListPointId', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_point_id = ' . $this->sqlWaitListPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_count = ' . $this->sqlRejectionCount() . ','; } elseif( true == array_key_exists( 'RejectionCount', $this->getChangedColumns() ) ) { $strSql .= ' rejection_count = ' . $this->sqlRejectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exception = ' . $this->sqlIsException() . ','; } elseif( true == array_key_exists( 'IsException', $this->getChangedColumns() ) ) { $strSql .= ' is_exception = ' . $this->sqlIsException() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_start_on = ' . $this->sqlWaitStartOn() . ','; } elseif( true == array_key_exists( 'WaitStartOn', $this->getChangedColumns() ) ) { $strSql .= ' wait_start_on = ' . $this->sqlWaitStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_end_on = ' . $this->sqlWaitEndOn() . ','; } elseif( true == array_key_exists( 'WaitEndOn', $this->getChangedColumns() ) ) { $strSql .= ' wait_end_on = ' . $this->sqlWaitEndOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'wait_list_id' => $this->getWaitListId(),
			'application_id' => $this->getApplicationId(),
			'wait_list_point_id' => $this->getWaitListPointId(),
			'rejection_count' => $this->getRejectionCount(),
			'is_exception' => $this->getIsException(),
			'wait_start_on' => $this->getWaitStartOn(),
			'wait_end_on' => $this->getWaitEndOn(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>