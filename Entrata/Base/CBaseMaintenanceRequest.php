<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.maintenance_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intParentMaintenanceRequestId;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intJobProjectId;
	protected $m_intMaintenancePriorityId;
	protected $m_intMaintenanceStatusId;
	protected $m_intMaintenanceLocationId;
	protected $m_intPropertyUnitMaintenanceLocationId;
	protected $m_intMaintenanceProblemId;
	protected $m_intSubMaintenanceProblemId;
	protected $m_intRecurringMaintenanceRequestId;
	protected $m_intCompanyEmployeeId;
	protected $m_intApPayeeId;
	protected $m_intPsProductId;
	protected $m_intMaintenanceTemplateId;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_intMainPhoneNumberTypeId;
	protected $m_intAltPhoneNumberTypeId;
	protected $m_intAssetId;
	protected $m_intAddOnId;
	protected $m_intMaintenanceGroupId;
	protected $m_strRemotePrimaryKey;
	protected $m_strDueDate;
	protected $m_strProblemDescription;
	protected $m_strAdditionalInfo;
	protected $m_strLocationSpecifics;
	protected $m_intPermissionToEnter;
	protected $m_strRequestedDatetime;
	protected $m_strScheduledStartDatetime;
	protected $m_strScheduledEndDatetime;
	protected $m_strActualStartDatetime;
	protected $m_strCompletedDatetime;
	protected $m_boolIgnoreResponseTimes;
	protected $m_strIgnoreResponseReason;
	protected $m_strCustomerNameFirst;
	protected $m_strCustomerNameFirstEncrypted;
	protected $m_strCustomerNameLast;
	protected $m_strCustomerNameLastEncrypted;
	protected $m_strUnitNumber;
	protected $m_strMainPhoneNumber;
	protected $m_strMainPhoneNumberEncrypted;
	protected $m_strAltPhoneNumber;
	protected $m_strAltPhoneNumberEncrypted;
	protected $m_strEmailAddress;
	protected $m_strRequiredResponseDatetime;
	protected $m_intResponseDuration;
	protected $m_strSmsConfirmedOn;
	protected $m_intIsPet;
	protected $m_intIsResidentVisible;
	protected $m_strImportedOn;
	protected $m_strExportedOn;
	protected $m_strPrintedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMaintenanceExceptionId;
	protected $m_intUpdatedPsProductId;
	protected $m_boolIsRequestedAm;
	protected $m_strRequestedDate;

	public function __construct() {
		parent::__construct();

		$this->m_intMaintenanceRequestTypeId = '2';
		$this->m_intIsResidentVisible = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['parent_maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intParentMaintenanceRequestId', trim( $arrValues['parent_maintenance_request_id'] ) ); elseif( isset( $arrValues['parent_maintenance_request_id'] ) ) $this->setParentMaintenanceRequestId( $arrValues['parent_maintenance_request_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['job_project_id'] ) && $boolDirectSet ) $this->set( 'm_intJobProjectId', trim( $arrValues['job_project_id'] ) ); elseif( isset( $arrValues['job_project_id'] ) ) $this->setJobProjectId( $arrValues['job_project_id'] );
		if( isset( $arrValues['maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenancePriorityId', trim( $arrValues['maintenance_priority_id'] ) ); elseif( isset( $arrValues['maintenance_priority_id'] ) ) $this->setMaintenancePriorityId( $arrValues['maintenance_priority_id'] );
		if( isset( $arrValues['maintenance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceStatusId', trim( $arrValues['maintenance_status_id'] ) ); elseif( isset( $arrValues['maintenance_status_id'] ) ) $this->setMaintenanceStatusId( $arrValues['maintenance_status_id'] );
		if( isset( $arrValues['maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationId', trim( $arrValues['maintenance_location_id'] ) ); elseif( isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( isset( $arrValues['property_unit_maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitMaintenanceLocationId', trim( $arrValues['property_unit_maintenance_location_id'] ) ); elseif( isset( $arrValues['property_unit_maintenance_location_id'] ) ) $this->setPropertyUnitMaintenanceLocationId( $arrValues['property_unit_maintenance_location_id'] );
		if( isset( $arrValues['maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemId', trim( $arrValues['maintenance_problem_id'] ) ); elseif( isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( isset( $arrValues['sub_maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intSubMaintenanceProblemId', trim( $arrValues['sub_maintenance_problem_id'] ) ); elseif( isset( $arrValues['sub_maintenance_problem_id'] ) ) $this->setSubMaintenanceProblemId( $arrValues['sub_maintenance_problem_id'] );
		if( isset( $arrValues['recurring_maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringMaintenanceRequestId', trim( $arrValues['recurring_maintenance_request_id'] ) ); elseif( isset( $arrValues['recurring_maintenance_request_id'] ) ) $this->setRecurringMaintenanceRequestId( $arrValues['recurring_maintenance_request_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceTemplateId', trim( $arrValues['maintenance_template_id'] ) ); elseif( isset( $arrValues['maintenance_template_id'] ) ) $this->setMaintenanceTemplateId( $arrValues['maintenance_template_id'] );
		if( isset( $arrValues['maintenance_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestTypeId', trim( $arrValues['maintenance_request_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrValues['maintenance_request_type_id'] );
		if( isset( $arrValues['main_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMainPhoneNumberTypeId', trim( $arrValues['main_phone_number_type_id'] ) ); elseif( isset( $arrValues['main_phone_number_type_id'] ) ) $this->setMainPhoneNumberTypeId( $arrValues['main_phone_number_type_id'] );
		if( isset( $arrValues['alt_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAltPhoneNumberTypeId', trim( $arrValues['alt_phone_number_type_id'] ) ); elseif( isset( $arrValues['alt_phone_number_type_id'] ) ) $this->setAltPhoneNumberTypeId( $arrValues['alt_phone_number_type_id'] );
		if( isset( $arrValues['asset_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetId', trim( $arrValues['asset_id'] ) ); elseif( isset( $arrValues['asset_id'] ) ) $this->setAssetId( $arrValues['asset_id'] );
		if( isset( $arrValues['add_on_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnId', trim( $arrValues['add_on_id'] ) ); elseif( isset( $arrValues['add_on_id'] ) ) $this->setAddOnId( $arrValues['add_on_id'] );
		if( isset( $arrValues['maintenance_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceGroupId', trim( $arrValues['maintenance_group_id'] ) ); elseif( isset( $arrValues['maintenance_group_id'] ) ) $this->setMaintenanceGroupId( $arrValues['maintenance_group_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['problem_description'] ) && $boolDirectSet ) $this->set( 'm_strProblemDescription', trim( $arrValues['problem_description'] ) ); elseif( isset( $arrValues['problem_description'] ) ) $this->setProblemDescription( $arrValues['problem_description'] );
		if( isset( $arrValues['additional_info'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInfo', trim( $arrValues['additional_info'] ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( $arrValues['additional_info'] );
		if( isset( $arrValues['location_specifics'] ) && $boolDirectSet ) $this->set( 'm_strLocationSpecifics', trim( $arrValues['location_specifics'] ) ); elseif( isset( $arrValues['location_specifics'] ) ) $this->setLocationSpecifics( $arrValues['location_specifics'] );
		if( isset( $arrValues['permission_to_enter'] ) && $boolDirectSet ) $this->set( 'm_intPermissionToEnter', trim( $arrValues['permission_to_enter'] ) ); elseif( isset( $arrValues['permission_to_enter'] ) ) $this->setPermissionToEnter( $arrValues['permission_to_enter'] );
		if( isset( $arrValues['requested_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestedDatetime', trim( $arrValues['requested_datetime'] ) ); elseif( isset( $arrValues['requested_datetime'] ) ) $this->setRequestedDatetime( $arrValues['requested_datetime'] );
		if( isset( $arrValues['scheduled_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartDatetime', trim( $arrValues['scheduled_start_datetime'] ) ); elseif( isset( $arrValues['scheduled_start_datetime'] ) ) $this->setScheduledStartDatetime( $arrValues['scheduled_start_datetime'] );
		if( isset( $arrValues['scheduled_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndDatetime', trim( $arrValues['scheduled_end_datetime'] ) ); elseif( isset( $arrValues['scheduled_end_datetime'] ) ) $this->setScheduledEndDatetime( $arrValues['scheduled_end_datetime'] );
		if( isset( $arrValues['actual_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDatetime', trim( $arrValues['actual_start_datetime'] ) ); elseif( isset( $arrValues['actual_start_datetime'] ) ) $this->setActualStartDatetime( $arrValues['actual_start_datetime'] );
		if( isset( $arrValues['completed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCompletedDatetime', trim( $arrValues['completed_datetime'] ) ); elseif( isset( $arrValues['completed_datetime'] ) ) $this->setCompletedDatetime( $arrValues['completed_datetime'] );
		if( isset( $arrValues['ignore_response_times'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreResponseTimes', trim( stripcslashes( $arrValues['ignore_response_times'] ) ) ); elseif( isset( $arrValues['ignore_response_times'] ) ) $this->setIgnoreResponseTimes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_response_times'] ) : $arrValues['ignore_response_times'] );
		if( isset( $arrValues['ignore_response_reason'] ) && $boolDirectSet ) $this->set( 'm_strIgnoreResponseReason', trim( $arrValues['ignore_response_reason'] ) ); elseif( isset( $arrValues['ignore_response_reason'] ) ) $this->setIgnoreResponseReason( $arrValues['ignore_response_reason'] );
		if( isset( $arrValues['customer_name_first'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameFirst', trim( $arrValues['customer_name_first'] ) ); elseif( isset( $arrValues['customer_name_first'] ) ) $this->setCustomerNameFirst( $arrValues['customer_name_first'] );
		if( isset( $arrValues['customer_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameFirstEncrypted', trim( $arrValues['customer_name_first_encrypted'] ) ); elseif( isset( $arrValues['customer_name_first_encrypted'] ) ) $this->setCustomerNameFirstEncrypted( $arrValues['customer_name_first_encrypted'] );
		if( isset( $arrValues['customer_name_last'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameLast', trim( $arrValues['customer_name_last'] ) ); elseif( isset( $arrValues['customer_name_last'] ) ) $this->setCustomerNameLast( $arrValues['customer_name_last'] );
		if( isset( $arrValues['customer_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameLastEncrypted', trim( $arrValues['customer_name_last_encrypted'] ) ); elseif( isset( $arrValues['customer_name_last_encrypted'] ) ) $this->setCustomerNameLastEncrypted( $arrValues['customer_name_last_encrypted'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( $arrValues['unit_number'] ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( $arrValues['unit_number'] );
		if( isset( $arrValues['main_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strMainPhoneNumber', trim( $arrValues['main_phone_number'] ) ); elseif( isset( $arrValues['main_phone_number'] ) ) $this->setMainPhoneNumber( $arrValues['main_phone_number'] );
		if( isset( $arrValues['main_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strMainPhoneNumberEncrypted', trim( $arrValues['main_phone_number_encrypted'] ) ); elseif( isset( $arrValues['main_phone_number_encrypted'] ) ) $this->setMainPhoneNumberEncrypted( $arrValues['main_phone_number_encrypted'] );
		if( isset( $arrValues['alt_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAltPhoneNumber', trim( $arrValues['alt_phone_number'] ) ); elseif( isset( $arrValues['alt_phone_number'] ) ) $this->setAltPhoneNumber( $arrValues['alt_phone_number'] );
		if( isset( $arrValues['alt_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAltPhoneNumberEncrypted', trim( $arrValues['alt_phone_number_encrypted'] ) ); elseif( isset( $arrValues['alt_phone_number_encrypted'] ) ) $this->setAltPhoneNumberEncrypted( $arrValues['alt_phone_number_encrypted'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['required_response_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequiredResponseDatetime', trim( $arrValues['required_response_datetime'] ) ); elseif( isset( $arrValues['required_response_datetime'] ) ) $this->setRequiredResponseDatetime( $arrValues['required_response_datetime'] );
		if( isset( $arrValues['response_duration'] ) && $boolDirectSet ) $this->set( 'm_intResponseDuration', trim( $arrValues['response_duration'] ) ); elseif( isset( $arrValues['response_duration'] ) ) $this->setResponseDuration( $arrValues['response_duration'] );
		if( isset( $arrValues['sms_confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsConfirmedOn', trim( $arrValues['sms_confirmed_on'] ) ); elseif( isset( $arrValues['sms_confirmed_on'] ) ) $this->setSmsConfirmedOn( $arrValues['sms_confirmed_on'] );
		if( isset( $arrValues['is_pet'] ) && $boolDirectSet ) $this->set( 'm_intIsPet', trim( $arrValues['is_pet'] ) ); elseif( isset( $arrValues['is_pet'] ) ) $this->setIsPet( $arrValues['is_pet'] );
		if( isset( $arrValues['is_resident_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsResidentVisible', trim( $arrValues['is_resident_visible'] ) ); elseif( isset( $arrValues['is_resident_visible'] ) ) $this->setIsResidentVisible( $arrValues['is_resident_visible'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['printed_on'] ) && $boolDirectSet ) $this->set( 'm_strPrintedOn', trim( $arrValues['printed_on'] ) ); elseif( isset( $arrValues['printed_on'] ) ) $this->setPrintedOn( $arrValues['printed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['maintenance_exception_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceExceptionId', trim( $arrValues['maintenance_exception_id'] ) ); elseif( isset( $arrValues['maintenance_exception_id'] ) ) $this->setMaintenanceExceptionId( $arrValues['maintenance_exception_id'] );
		if( isset( $arrValues['updated_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedPsProductId', trim( $arrValues['updated_ps_product_id'] ) ); elseif( isset( $arrValues['updated_ps_product_id'] ) ) $this->setUpdatedPsProductId( $arrValues['updated_ps_product_id'] );
		if( isset( $arrValues['is_requested_am'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequestedAm', trim( stripcslashes( $arrValues['is_requested_am'] ) ) ); elseif( isset( $arrValues['is_requested_am'] ) ) $this->setIsRequestedAm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_requested_am'] ) : $arrValues['is_requested_am'] );
		if( isset( $arrValues['requested_date'] ) && $boolDirectSet ) $this->set( 'm_strRequestedDate', trim( $arrValues['requested_date'] ) ); elseif( isset( $arrValues['requested_date'] ) ) $this->setRequestedDate( $arrValues['requested_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setParentMaintenanceRequestId( $intParentMaintenanceRequestId ) {
		$this->set( 'm_intParentMaintenanceRequestId', CStrings::strToIntDef( $intParentMaintenanceRequestId, NULL, false ) );
	}

	public function getParentMaintenanceRequestId() {
		return $this->m_intParentMaintenanceRequestId;
	}

	public function sqlParentMaintenanceRequestId() {
		return ( true == isset( $this->m_intParentMaintenanceRequestId ) ) ? ( string ) $this->m_intParentMaintenanceRequestId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setJobProjectId( $intJobProjectId ) {
		$this->set( 'm_intJobProjectId', CStrings::strToIntDef( $intJobProjectId, NULL, false ) );
	}

	public function getJobProjectId() {
		return $this->m_intJobProjectId;
	}

	public function sqlJobProjectId() {
		return ( true == isset( $this->m_intJobProjectId ) ) ? ( string ) $this->m_intJobProjectId : 'NULL';
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->set( 'm_intMaintenancePriorityId', CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false ) );
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function sqlMaintenancePriorityId() {
		return ( true == isset( $this->m_intMaintenancePriorityId ) ) ? ( string ) $this->m_intMaintenancePriorityId : 'NULL';
	}

	public function setMaintenanceStatusId( $intMaintenanceStatusId ) {
		$this->set( 'm_intMaintenanceStatusId', CStrings::strToIntDef( $intMaintenanceStatusId, NULL, false ) );
	}

	public function getMaintenanceStatusId() {
		return $this->m_intMaintenanceStatusId;
	}

	public function sqlMaintenanceStatusId() {
		return ( true == isset( $this->m_intMaintenanceStatusId ) ) ? ( string ) $this->m_intMaintenanceStatusId : 'NULL';
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->set( 'm_intMaintenanceLocationId', CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false ) );
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function sqlMaintenanceLocationId() {
		return ( true == isset( $this->m_intMaintenanceLocationId ) ) ? ( string ) $this->m_intMaintenanceLocationId : 'NULL';
	}

	public function setPropertyUnitMaintenanceLocationId( $intPropertyUnitMaintenanceLocationId ) {
		$this->set( 'm_intPropertyUnitMaintenanceLocationId', CStrings::strToIntDef( $intPropertyUnitMaintenanceLocationId, NULL, false ) );
	}

	public function getPropertyUnitMaintenanceLocationId() {
		return $this->m_intPropertyUnitMaintenanceLocationId;
	}

	public function sqlPropertyUnitMaintenanceLocationId() {
		return ( true == isset( $this->m_intPropertyUnitMaintenanceLocationId ) ) ? ( string ) $this->m_intPropertyUnitMaintenanceLocationId : 'NULL';
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->set( 'm_intMaintenanceProblemId', CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false ) );
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function sqlMaintenanceProblemId() {
		return ( true == isset( $this->m_intMaintenanceProblemId ) ) ? ( string ) $this->m_intMaintenanceProblemId : 'NULL';
	}

	public function setSubMaintenanceProblemId( $intSubMaintenanceProblemId ) {
		$this->set( 'm_intSubMaintenanceProblemId', CStrings::strToIntDef( $intSubMaintenanceProblemId, NULL, false ) );
	}

	public function getSubMaintenanceProblemId() {
		return $this->m_intSubMaintenanceProblemId;
	}

	public function sqlSubMaintenanceProblemId() {
		return ( true == isset( $this->m_intSubMaintenanceProblemId ) ) ? ( string ) $this->m_intSubMaintenanceProblemId : 'NULL';
	}

	public function setRecurringMaintenanceRequestId( $intRecurringMaintenanceRequestId ) {
		$this->set( 'm_intRecurringMaintenanceRequestId', CStrings::strToIntDef( $intRecurringMaintenanceRequestId, NULL, false ) );
	}

	public function getRecurringMaintenanceRequestId() {
		return $this->m_intRecurringMaintenanceRequestId;
	}

	public function sqlRecurringMaintenanceRequestId() {
		return ( true == isset( $this->m_intRecurringMaintenanceRequestId ) ) ? ( string ) $this->m_intRecurringMaintenanceRequestId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->set( 'm_intMaintenanceTemplateId', CStrings::strToIntDef( $intMaintenanceTemplateId, NULL, false ) );
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function sqlMaintenanceTemplateId() {
		return ( true == isset( $this->m_intMaintenanceTemplateId ) ) ? ( string ) $this->m_intMaintenanceTemplateId : 'NULL';
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->set( 'm_intMaintenanceRequestTypeId', CStrings::strToIntDef( $intMaintenanceRequestTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function sqlMaintenanceRequestTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestTypeId : '2';
	}

	public function setMainPhoneNumberTypeId( $intMainPhoneNumberTypeId ) {
		$this->set( 'm_intMainPhoneNumberTypeId', CStrings::strToIntDef( $intMainPhoneNumberTypeId, NULL, false ) );
	}

	public function getMainPhoneNumberTypeId() {
		return $this->m_intMainPhoneNumberTypeId;
	}

	public function sqlMainPhoneNumberTypeId() {
		return ( true == isset( $this->m_intMainPhoneNumberTypeId ) ) ? ( string ) $this->m_intMainPhoneNumberTypeId : 'NULL';
	}

	public function setAltPhoneNumberTypeId( $intAltPhoneNumberTypeId ) {
		$this->set( 'm_intAltPhoneNumberTypeId', CStrings::strToIntDef( $intAltPhoneNumberTypeId, NULL, false ) );
	}

	public function getAltPhoneNumberTypeId() {
		return $this->m_intAltPhoneNumberTypeId;
	}

	public function sqlAltPhoneNumberTypeId() {
		return ( true == isset( $this->m_intAltPhoneNumberTypeId ) ) ? ( string ) $this->m_intAltPhoneNumberTypeId : 'NULL';
	}

	public function setAssetId( $intAssetId ) {
		$this->set( 'm_intAssetId', CStrings::strToIntDef( $intAssetId, NULL, false ) );
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function sqlAssetId() {
		return ( true == isset( $this->m_intAssetId ) ) ? ( string ) $this->m_intAssetId : 'NULL';
	}

	public function setAddOnId( $intAddOnId ) {
		$this->set( 'm_intAddOnId', CStrings::strToIntDef( $intAddOnId, NULL, false ) );
	}

	public function getAddOnId() {
		return $this->m_intAddOnId;
	}

	public function sqlAddOnId() {
		return ( true == isset( $this->m_intAddOnId ) ) ? ( string ) $this->m_intAddOnId : 'NULL';
	}

	public function setMaintenanceGroupId( $intMaintenanceGroupId ) {
		$this->set( 'm_intMaintenanceGroupId', CStrings::strToIntDef( $intMaintenanceGroupId, NULL, false ) );
	}

	public function getMaintenanceGroupId() {
		return $this->m_intMaintenanceGroupId;
	}

	public function sqlMaintenanceGroupId() {
		return ( true == isset( $this->m_intMaintenanceGroupId ) ) ? ( string ) $this->m_intMaintenanceGroupId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setProblemDescription( $strProblemDescription ) {
		$this->set( 'm_strProblemDescription', CStrings::strTrimDef( $strProblemDescription, -1, NULL, true ) );
	}

	public function getProblemDescription() {
		return $this->m_strProblemDescription;
	}

	public function sqlProblemDescription() {
		return ( true == isset( $this->m_strProblemDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProblemDescription ) : '\'' . addslashes( $this->m_strProblemDescription ) . '\'' ) : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->set( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ) );
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAdditionalInfo ) : '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' ) : 'NULL';
	}

	public function setLocationSpecifics( $strLocationSpecifics ) {
		$this->set( 'm_strLocationSpecifics', CStrings::strTrimDef( $strLocationSpecifics, -1, NULL, true ) );
	}

	public function getLocationSpecifics() {
		return $this->m_strLocationSpecifics;
	}

	public function sqlLocationSpecifics() {
		return ( true == isset( $this->m_strLocationSpecifics ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLocationSpecifics ) : '\'' . addslashes( $this->m_strLocationSpecifics ) . '\'' ) : 'NULL';
	}

	public function setPermissionToEnter( $intPermissionToEnter ) {
		$this->set( 'm_intPermissionToEnter', CStrings::strToIntDef( $intPermissionToEnter, NULL, false ) );
	}

	public function getPermissionToEnter() {
		return $this->m_intPermissionToEnter;
	}

	public function sqlPermissionToEnter() {
		return ( true == isset( $this->m_intPermissionToEnter ) ) ? ( string ) $this->m_intPermissionToEnter : 'NULL';
	}

	public function setRequestedDatetime( $strRequestedDatetime ) {
		$this->set( 'm_strRequestedDatetime', CStrings::strTrimDef( $strRequestedDatetime, -1, NULL, true ) );
	}

	public function getRequestedDatetime() {
		return $this->m_strRequestedDatetime;
	}

	public function sqlRequestedDatetime() {
		return ( true == isset( $this->m_strRequestedDatetime ) ) ? '\'' . $this->m_strRequestedDatetime . '\'' : 'NULL';
	}

	public function setScheduledStartDatetime( $strScheduledStartDatetime ) {
		$this->set( 'm_strScheduledStartDatetime', CStrings::strTrimDef( $strScheduledStartDatetime, -1, NULL, true ) );
	}

	public function getScheduledStartDatetime() {
		return $this->m_strScheduledStartDatetime;
	}

	public function sqlScheduledStartDatetime() {
		return ( true == isset( $this->m_strScheduledStartDatetime ) ) ? '\'' . $this->m_strScheduledStartDatetime . '\'' : 'NULL';
	}

	public function setScheduledEndDatetime( $strScheduledEndDatetime ) {
		$this->set( 'm_strScheduledEndDatetime', CStrings::strTrimDef( $strScheduledEndDatetime, -1, NULL, true ) );
	}

	public function getScheduledEndDatetime() {
		return $this->m_strScheduledEndDatetime;
	}

	public function sqlScheduledEndDatetime() {
		return ( true == isset( $this->m_strScheduledEndDatetime ) ) ? '\'' . $this->m_strScheduledEndDatetime . '\'' : 'NULL';
	}

	public function setActualStartDatetime( $strActualStartDatetime ) {
		$this->set( 'm_strActualStartDatetime', CStrings::strTrimDef( $strActualStartDatetime, -1, NULL, true ) );
	}

	public function getActualStartDatetime() {
		return $this->m_strActualStartDatetime;
	}

	public function sqlActualStartDatetime() {
		return ( true == isset( $this->m_strActualStartDatetime ) ) ? '\'' . $this->m_strActualStartDatetime . '\'' : 'NULL';
	}

	public function setCompletedDatetime( $strCompletedDatetime ) {
		$this->set( 'm_strCompletedDatetime', CStrings::strTrimDef( $strCompletedDatetime, -1, NULL, true ) );
	}

	public function getCompletedDatetime() {
		return $this->m_strCompletedDatetime;
	}

	public function sqlCompletedDatetime() {
		return ( true == isset( $this->m_strCompletedDatetime ) ) ? '\'' . $this->m_strCompletedDatetime . '\'' : 'NULL';
	}

	public function setIgnoreResponseTimes( $boolIgnoreResponseTimes ) {
		$this->set( 'm_boolIgnoreResponseTimes', CStrings::strToBool( $boolIgnoreResponseTimes ) );
	}

	public function getIgnoreResponseTimes() {
		return $this->m_boolIgnoreResponseTimes;
	}

	public function sqlIgnoreResponseTimes() {
		return ( true == isset( $this->m_boolIgnoreResponseTimes ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreResponseTimes ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIgnoreResponseReason( $strIgnoreResponseReason ) {
		$this->set( 'm_strIgnoreResponseReason', CStrings::strTrimDef( $strIgnoreResponseReason, 255, NULL, true ) );
	}

	public function getIgnoreResponseReason() {
		return $this->m_strIgnoreResponseReason;
	}

	public function sqlIgnoreResponseReason() {
		return ( true == isset( $this->m_strIgnoreResponseReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIgnoreResponseReason ) : '\'' . addslashes( $this->m_strIgnoreResponseReason ) . '\'' ) : 'NULL';
	}

	public function setCustomerNameFirst( $strCustomerNameFirst ) {
		$this->set( 'm_strCustomerNameFirst', CStrings::strTrimDef( $strCustomerNameFirst, 50, NULL, true ) );
	}

	public function getCustomerNameFirst() {
		return $this->m_strCustomerNameFirst;
	}

	public function sqlCustomerNameFirst() {
		return ( true == isset( $this->m_strCustomerNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerNameFirst ) : '\'' . addslashes( $this->m_strCustomerNameFirst ) . '\'' ) : 'NULL';
	}

	public function setCustomerNameFirstEncrypted( $strCustomerNameFirstEncrypted ) {
		$this->set( 'm_strCustomerNameFirstEncrypted', CStrings::strTrimDef( $strCustomerNameFirstEncrypted, -1, NULL, true ) );
	}

	public function getCustomerNameFirstEncrypted() {
		return $this->m_strCustomerNameFirstEncrypted;
	}

	public function sqlCustomerNameFirstEncrypted() {
		return ( true == isset( $this->m_strCustomerNameFirstEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerNameFirstEncrypted ) : '\'' . addslashes( $this->m_strCustomerNameFirstEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCustomerNameLast( $strCustomerNameLast ) {
		$this->set( 'm_strCustomerNameLast', CStrings::strTrimDef( $strCustomerNameLast, 50, NULL, true ) );
	}

	public function getCustomerNameLast() {
		return $this->m_strCustomerNameLast;
	}

	public function sqlCustomerNameLast() {
		return ( true == isset( $this->m_strCustomerNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerNameLast ) : '\'' . addslashes( $this->m_strCustomerNameLast ) . '\'' ) : 'NULL';
	}

	public function setCustomerNameLastEncrypted( $strCustomerNameLastEncrypted ) {
		$this->set( 'm_strCustomerNameLastEncrypted', CStrings::strTrimDef( $strCustomerNameLastEncrypted, -1, NULL, true ) );
	}

	public function getCustomerNameLastEncrypted() {
		return $this->m_strCustomerNameLastEncrypted;
	}

	public function sqlCustomerNameLastEncrypted() {
		return ( true == isset( $this->m_strCustomerNameLastEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerNameLastEncrypted ) : '\'' . addslashes( $this->m_strCustomerNameLastEncrypted ) . '\'' ) : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumber ) : '\'' . addslashes( $this->m_strUnitNumber ) . '\'' ) : 'NULL';
	}

	public function setMainPhoneNumber( $strMainPhoneNumber ) {
		$this->set( 'm_strMainPhoneNumber', CStrings::strTrimDef( $strMainPhoneNumber, 30, NULL, true ) );
	}

	public function getMainPhoneNumber() {
		return $this->m_strMainPhoneNumber;
	}

	public function sqlMainPhoneNumber() {
		return ( true == isset( $this->m_strMainPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMainPhoneNumber ) : '\'' . addslashes( $this->m_strMainPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMainPhoneNumberEncrypted( $strMainPhoneNumberEncrypted ) {
		$this->set( 'm_strMainPhoneNumberEncrypted', CStrings::strTrimDef( $strMainPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getMainPhoneNumberEncrypted() {
		return $this->m_strMainPhoneNumberEncrypted;
	}

	public function sqlMainPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strMainPhoneNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMainPhoneNumberEncrypted ) : '\'' . addslashes( $this->m_strMainPhoneNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setAltPhoneNumber( $strAltPhoneNumber ) {
		$this->set( 'm_strAltPhoneNumber', CStrings::strTrimDef( $strAltPhoneNumber, 30, NULL, true ) );
	}

	public function getAltPhoneNumber() {
		return $this->m_strAltPhoneNumber;
	}

	public function sqlAltPhoneNumber() {
		return ( true == isset( $this->m_strAltPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltPhoneNumber ) : '\'' . addslashes( $this->m_strAltPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setAltPhoneNumberEncrypted( $strAltPhoneNumberEncrypted ) {
		$this->set( 'm_strAltPhoneNumberEncrypted', CStrings::strTrimDef( $strAltPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getAltPhoneNumberEncrypted() {
		return $this->m_strAltPhoneNumberEncrypted;
	}

	public function sqlAltPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strAltPhoneNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltPhoneNumberEncrypted ) : '\'' . addslashes( $this->m_strAltPhoneNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setRequiredResponseDatetime( $strRequiredResponseDatetime ) {
		$this->set( 'm_strRequiredResponseDatetime', CStrings::strTrimDef( $strRequiredResponseDatetime, -1, NULL, true ) );
	}

	public function getRequiredResponseDatetime() {
		return $this->m_strRequiredResponseDatetime;
	}

	public function sqlRequiredResponseDatetime() {
		return ( true == isset( $this->m_strRequiredResponseDatetime ) ) ? '\'' . $this->m_strRequiredResponseDatetime . '\'' : 'NULL';
	}

	public function setResponseDuration( $intResponseDuration ) {
		$this->set( 'm_intResponseDuration', CStrings::strToIntDef( $intResponseDuration, NULL, false ) );
	}

	public function getResponseDuration() {
		return $this->m_intResponseDuration;
	}

	public function sqlResponseDuration() {
		return ( true == isset( $this->m_intResponseDuration ) ) ? ( string ) $this->m_intResponseDuration : 'NULL';
	}

	public function setSmsConfirmedOn( $strSmsConfirmedOn ) {
		$this->set( 'm_strSmsConfirmedOn', CStrings::strTrimDef( $strSmsConfirmedOn, -1, NULL, true ) );
	}

	public function getSmsConfirmedOn() {
		return $this->m_strSmsConfirmedOn;
	}

	public function sqlSmsConfirmedOn() {
		return ( true == isset( $this->m_strSmsConfirmedOn ) ) ? '\'' . $this->m_strSmsConfirmedOn . '\'' : 'NULL';
	}

	public function setIsPet( $intIsPet ) {
		$this->set( 'm_intIsPet', CStrings::strToIntDef( $intIsPet, NULL, false ) );
	}

	public function getIsPet() {
		return $this->m_intIsPet;
	}

	public function sqlIsPet() {
		return ( true == isset( $this->m_intIsPet ) ) ? ( string ) $this->m_intIsPet : 'NULL';
	}

	public function setIsResidentVisible( $intIsResidentVisible ) {
		$this->set( 'm_intIsResidentVisible', CStrings::strToIntDef( $intIsResidentVisible, NULL, false ) );
	}

	public function getIsResidentVisible() {
		return $this->m_intIsResidentVisible;
	}

	public function sqlIsResidentVisible() {
		return ( true == isset( $this->m_intIsResidentVisible ) ) ? ( string ) $this->m_intIsResidentVisible : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setPrintedOn( $strPrintedOn ) {
		$this->set( 'm_strPrintedOn', CStrings::strTrimDef( $strPrintedOn, -1, NULL, true ) );
	}

	public function getPrintedOn() {
		return $this->m_strPrintedOn;
	}

	public function sqlPrintedOn() {
		return ( true == isset( $this->m_strPrintedOn ) ) ? '\'' . $this->m_strPrintedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMaintenanceExceptionId( $intMaintenanceExceptionId ) {
		$this->set( 'm_intMaintenanceExceptionId', CStrings::strToIntDef( $intMaintenanceExceptionId, NULL, false ) );
	}

	public function getMaintenanceExceptionId() {
		return $this->m_intMaintenanceExceptionId;
	}

	public function sqlMaintenanceExceptionId() {
		return ( true == isset( $this->m_intMaintenanceExceptionId ) ) ? ( string ) $this->m_intMaintenanceExceptionId : 'NULL';
	}

	public function setUpdatedPsProductId( $intUpdatedPsProductId ) {
		$this->set( 'm_intUpdatedPsProductId', CStrings::strToIntDef( $intUpdatedPsProductId, NULL, false ) );
	}

	public function getUpdatedPsProductId() {
		return $this->m_intUpdatedPsProductId;
	}

	public function sqlUpdatedPsProductId() {
		return ( true == isset( $this->m_intUpdatedPsProductId ) ) ? ( string ) $this->m_intUpdatedPsProductId : 'NULL';
	}

	public function setIsRequestedAm( $boolIsRequestedAm ) {
		$this->set( 'm_boolIsRequestedAm', CStrings::strToBool( $boolIsRequestedAm ) );
	}

	public function getIsRequestedAm() {
		return $this->m_boolIsRequestedAm;
	}

	public function sqlIsRequestedAm() {
		return ( true == isset( $this->m_boolIsRequestedAm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequestedAm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequestedDate( $strRequestedDate ) {
		$this->set( 'm_strRequestedDate', CStrings::strTrimDef( $strRequestedDate, -1, NULL, true ) );
	}

	public function getRequestedDate() {
		return $this->m_strRequestedDate;
	}

	public function sqlRequestedDate() {
		return ( true == isset( $this->m_strRequestedDate ) ) ? '\'' . $this->m_strRequestedDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert maintenance request record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlParentMaintenanceRequestId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlJobProjectId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlMaintenanceStatusId() . ', ' .
						$this->sqlMaintenanceLocationId() . ', ' .
						$this->sqlPropertyUnitMaintenanceLocationId() . ', ' .
						$this->sqlMaintenanceProblemId() . ', ' .
						$this->sqlSubMaintenanceProblemId() . ', ' .
						$this->sqlRecurringMaintenanceRequestId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlMaintenanceTemplateId() . ', ' .
						$this->sqlMaintenanceRequestTypeId() . ', ' .
						$this->sqlMainPhoneNumberTypeId() . ', ' .
						$this->sqlAltPhoneNumberTypeId() . ', ' .
						$this->sqlAssetId() . ', ' .
						$this->sqlAddOnId() . ', ' .
						$this->sqlMaintenanceGroupId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlProblemDescription() . ', ' .
						$this->sqlAdditionalInfo() . ', ' .
						$this->sqlLocationSpecifics() . ', ' .
						$this->sqlPermissionToEnter() . ', ' .
						$this->sqlRequestedDatetime() . ', ' .
						$this->sqlScheduledStartDatetime() . ', ' .
						$this->sqlScheduledEndDatetime() . ', ' .
						$this->sqlActualStartDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ', ' .
						$this->sqlIgnoreResponseTimes() . ', ' .
						$this->sqlIgnoreResponseReason() . ', ' .
						$this->sqlCustomerNameFirst() . ', ' .
						$this->sqlCustomerNameFirstEncrypted() . ', ' .
						$this->sqlCustomerNameLast() . ', ' .
						$this->sqlCustomerNameLastEncrypted() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlMainPhoneNumber() . ', ' .
						$this->sqlMainPhoneNumberEncrypted() . ', ' .
						$this->sqlAltPhoneNumber() . ', ' .
						$this->sqlAltPhoneNumberEncrypted() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlRequiredResponseDatetime() . ', ' .
						$this->sqlResponseDuration() . ', ' .
						$this->sqlSmsConfirmedOn() . ', ' .
						$this->sqlIsPet() . ', ' .
						$this->sqlIsResidentVisible() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlPrintedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMaintenanceExceptionId() . ', ' .
						$this->sqlUpdatedPsProductId() . ', ' .
		                $this->sqlIsRequestedAm() . ', ' .
		                $this->sqlRequestedDate() . ', ' .
						( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert maintenance request record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert maintenance request record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' . 
					'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlParentMaintenanceRequestId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlJobProjectId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlMaintenanceStatusId() . ', ' .
						$this->sqlMaintenanceLocationId() . ', ' .
						$this->sqlPropertyUnitMaintenanceLocationId() . ', ' .
						$this->sqlMaintenanceProblemId() . ', ' .
						$this->sqlSubMaintenanceProblemId() . ', ' .
						$this->sqlRecurringMaintenanceRequestId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlMaintenanceTemplateId() . ', ' .
						$this->sqlMaintenanceRequestTypeId() . ', ' .
						$this->sqlMainPhoneNumberTypeId() . ', ' .
						$this->sqlAltPhoneNumberTypeId() . ', ' .
						$this->sqlAssetId() . ', ' .
						$this->sqlAddOnId() . ', ' .
						$this->sqlMaintenanceGroupId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlProblemDescription() . ', ' .
						$this->sqlAdditionalInfo() . ', ' .
						$this->sqlLocationSpecifics() . ', ' .
						$this->sqlPermissionToEnter() . ', ' .
						$this->sqlRequestedDatetime() . ', ' .
						$this->sqlScheduledStartDatetime() . ', ' .
						$this->sqlScheduledEndDatetime() . ', ' .
						$this->sqlActualStartDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ', ' .
						$this->sqlIgnoreResponseTimes() . ', ' .
						$this->sqlIgnoreResponseReason() . ', ' .
						$this->sqlCustomerNameFirst() . ', ' .
						$this->sqlCustomerNameFirstEncrypted() . ', ' .
						$this->sqlCustomerNameLast() . ', ' .
						$this->sqlCustomerNameLastEncrypted() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlMainPhoneNumber() . ', ' .
						$this->sqlMainPhoneNumberEncrypted() . ', ' .
						$this->sqlAltPhoneNumber() . ', ' .
						$this->sqlAltPhoneNumberEncrypted() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlRequiredResponseDatetime() . ', ' .
						$this->sqlResponseDuration() . ', ' .
						$this->sqlSmsConfirmedOn() . ', ' .
						$this->sqlIsPet() . ', ' .
						$this->sqlIsResidentVisible() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlPrintedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMaintenanceExceptionId() . ', ' .
						$this->sqlUpdatedPsProductId() . ', ' .
		                $this->sqlIsRequestedAm() . ', ' .
		                $this->sqlRequestedDate() . ', ' .
						 ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			 return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update maintenance request record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update maintenance request record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_delete( ' .
		$this->sqlId() . ', ' .
		$this->sqlCid() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete maintenance request record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete maintenance request record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'parent_maintenance_request_id' => $this->getParentMaintenanceRequestId(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'job_project_id' => $this->getJobProjectId(),
			'maintenance_priority_id' => $this->getMaintenancePriorityId(),
			'maintenance_status_id' => $this->getMaintenanceStatusId(),
			'maintenance_location_id' => $this->getMaintenanceLocationId(),
			'property_unit_maintenance_location_id' => $this->getPropertyUnitMaintenanceLocationId(),
			'maintenance_problem_id' => $this->getMaintenanceProblemId(),
			'sub_maintenance_problem_id' => $this->getSubMaintenanceProblemId(),
			'recurring_maintenance_request_id' => $this->getRecurringMaintenanceRequestId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ps_product_id' => $this->getPsProductId(),
			'maintenance_template_id' => $this->getMaintenanceTemplateId(),
			'maintenance_request_type_id' => $this->getMaintenanceRequestTypeId(),
			'main_phone_number_type_id' => $this->getMainPhoneNumberTypeId(),
			'alt_phone_number_type_id' => $this->getAltPhoneNumberTypeId(),
			'asset_id' => $this->getAssetId(),
			'add_on_id' => $this->getAddOnId(),
			'maintenance_group_id' => $this->getMaintenanceGroupId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'due_date' => $this->getDueDate(),
			'problem_description' => $this->getProblemDescription(),
			'additional_info' => $this->getAdditionalInfo(),
			'location_specifics' => $this->getLocationSpecifics(),
			'permission_to_enter' => $this->getPermissionToEnter(),
			'requested_datetime' => $this->getRequestedDatetime(),
			'scheduled_start_datetime' => $this->getScheduledStartDatetime(),
			'scheduled_end_datetime' => $this->getScheduledEndDatetime(),
			'actual_start_datetime' => $this->getActualStartDatetime(),
			'completed_datetime' => $this->getCompletedDatetime(),
			'ignore_response_times' => $this->getIgnoreResponseTimes(),
			'ignore_response_reason' => $this->getIgnoreResponseReason(),
			'customer_name_first' => $this->getCustomerNameFirst(),
			'customer_name_first_encrypted' => $this->getCustomerNameFirstEncrypted(),
			'customer_name_last' => $this->getCustomerNameLast(),
			'customer_name_last_encrypted' => $this->getCustomerNameLastEncrypted(),
			'unit_number' => $this->getUnitNumber(),
			'main_phone_number' => $this->getMainPhoneNumber(),
			'main_phone_number_encrypted' => $this->getMainPhoneNumberEncrypted(),
			'alt_phone_number' => $this->getAltPhoneNumber(),
			'alt_phone_number_encrypted' => $this->getAltPhoneNumberEncrypted(),
			'email_address' => $this->getEmailAddress(),
			'required_response_datetime' => $this->getRequiredResponseDatetime(),
			'response_duration' => $this->getResponseDuration(),
			'sms_confirmed_on' => $this->getSmsConfirmedOn(),
			'is_pet' => $this->getIsPet(),
			'is_resident_visible' => $this->getIsResidentVisible(),
			'imported_on' => $this->getImportedOn(),
			'exported_on' => $this->getExportedOn(),
			'printed_on' => $this->getPrintedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'maintenance_exception_id' => $this->getMaintenanceExceptionId(),
			'updated_ps_product_id' => $this->getUpdatedPsProductId(),
			'is_requested_am' => $this->getIsRequestedAm(),
			'requested_date' => $this->getRequestedDate()
		);
	}

}
?>