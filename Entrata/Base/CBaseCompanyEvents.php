<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEvents extends CEosPluralBase {

	/**
	 * @return CCompanyEvent[]
	 */
	public static function fetchCompanyEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyEvent::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEvent
	 */
	public static function fetchCompanyEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyEvent::class, $objDatabase );
	}

	public static function fetchCompanyEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_events', $objDatabase );
	}

	public static function fetchCompanyEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvent( sprintf( 'SELECT * FROM company_events WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByCompanyEventTypeIdByCid( $intCompanyEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE company_event_type_id = %d AND cid = %d', $intCompanyEventTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByClubIdByCid( $intClubId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE club_id = %d AND cid = %d', $intClubId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCompanyEvents( sprintf( 'SELECT * FROM company_events WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

}
?>