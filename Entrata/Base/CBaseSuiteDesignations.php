<?php

class CBaseSuiteDesignations extends CEosPluralBase {

	public static function fetchSuiteDesignations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSuiteDesignation', $objDatabase );
	}

	public static function fetchSuiteDesignation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSuiteDesignation', $objDatabase );
	}

	public static function fetchSuiteDesignationCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'suite_designations', $objDatabase );
	}

	public static function fetchSuiteDesignationById( $intId, $objDatabase ) {
		return self::fetchSuiteDesignation( sprintf( 'SELECT * FROM suite_designations WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>