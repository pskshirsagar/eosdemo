<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseIntegrationClientStatusTypes extends CEosPluralBase {

	/**
	 * @return CIntegrationClientStatusType[]
	 */
	public static function fetchIntegrationClientStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationClientStatusType::class, $objDatabase );
	}

	/**
	 * @return CIntegrationClientStatusType
	 */
	public static function fetchIntegrationClientStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationClientStatusType::class, $objDatabase );
	}

	public static function fetchIntegrationClientStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_client_status_types', $objDatabase );
	}

	public static function fetchIntegrationClientStatusTypeById( $intId, $objDatabase ) {
		return self::fetchIntegrationClientStatusType( sprintf( 'SELECT * FROM integration_client_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>