<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClients
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationClients extends CEosPluralBase {

	/**
	 * @return CIntegrationClient[]
	 */
	public static function fetchIntegrationClients( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationClient', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationClient
	 */
	public static function fetchIntegrationClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationClient', $objDatabase );
	}

	public static function fetchIntegrationClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_clients', $objDatabase );
	}

	public static function fetchIntegrationClientByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClient( sprintf( 'SELECT * FROM integration_clients WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationClientTypeIdByCid( $intIntegrationClientTypeId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_client_type_id = %d AND cid = %d', ( int ) $intIntegrationClientTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationClientStatusTypeIdByCid( $intIntegrationClientStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_client_status_type_id = %d AND cid = %d', ( int ) $intIntegrationClientStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationSyncTypeIdByCid( $intIntegrationSyncTypeId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_sync_type_id = %d AND cid = %d', ( int ) $intIntegrationSyncTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationServiceIdByCid( $intIntegrationServiceId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_service_id = %d AND cid = %d', ( int ) $intIntegrationServiceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientsByIntegrationPeriodIdByCid( $intIntegrationPeriodId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClients( sprintf( 'SELECT * FROM integration_clients WHERE integration_period_id = %d AND cid = %d', ( int ) $intIntegrationPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>