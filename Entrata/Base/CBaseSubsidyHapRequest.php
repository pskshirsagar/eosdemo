<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyHapRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.subsidy_hap_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyContractId;
	protected $m_intSubsidyHapRequestStatusTypeId;
	protected $m_intPreviousSubsidyHapRequestId;
	protected $m_intArPaymentId;
	protected $m_strVoucherDate;
	protected $m_strDateFinalized;
	protected $m_strIncludeAdjustmentsThroughDatetime;
	protected $m_fltAssistancePaymentAmount;
	protected $m_fltAdjustmentPaymentAmount;
	protected $m_fltRepaymentAgreementAmount;
	protected $m_fltMiscellaneousRequestAmount;
	protected $m_fltSpecialClaimsAmount;
	protected $m_fltTotalAmount;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltAssistancePaymentAmount = '0.00';
		$this->m_fltAdjustmentPaymentAmount = '0.00';
		$this->m_fltRepaymentAgreementAmount = '0.00';
		$this->m_fltMiscellaneousRequestAmount = '0.00';
		$this->m_fltSpecialClaimsAmount = '0.00';
		$this->m_fltTotalAmount = '0.00';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractId', trim( $arrValues['subsidy_contract_id'] ) ); elseif( isset( $arrValues['subsidy_contract_id'] ) ) $this->setSubsidyContractId( $arrValues['subsidy_contract_id'] );
		if( isset( $arrValues['subsidy_hap_request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyHapRequestStatusTypeId', trim( $arrValues['subsidy_hap_request_status_type_id'] ) ); elseif( isset( $arrValues['subsidy_hap_request_status_type_id'] ) ) $this->setSubsidyHapRequestStatusTypeId( $arrValues['subsidy_hap_request_status_type_id'] );
		if( isset( $arrValues['previous_subsidy_hap_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousSubsidyHapRequestId', trim( $arrValues['previous_subsidy_hap_request_id'] ) ); elseif( isset( $arrValues['previous_subsidy_hap_request_id'] ) ) $this->setPreviousSubsidyHapRequestId( $arrValues['previous_subsidy_hap_request_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['voucher_date'] ) && $boolDirectSet ) $this->set( 'm_strVoucherDate', trim( $arrValues['voucher_date'] ) ); elseif( isset( $arrValues['voucher_date'] ) ) $this->setVoucherDate( $arrValues['voucher_date'] );
		if( isset( $arrValues['date_finalized'] ) && $boolDirectSet ) $this->set( 'm_strDateFinalized', trim( $arrValues['date_finalized'] ) ); elseif( isset( $arrValues['date_finalized'] ) ) $this->setDateFinalized( $arrValues['date_finalized'] );
		if( isset( $arrValues['include_adjustments_through_datetime'] ) && $boolDirectSet ) $this->set( 'm_strIncludeAdjustmentsThroughDatetime', trim( $arrValues['include_adjustments_through_datetime'] ) ); elseif( isset( $arrValues['include_adjustments_through_datetime'] ) ) $this->setIncludeAdjustmentsThroughDatetime( $arrValues['include_adjustments_through_datetime'] );
		if( isset( $arrValues['assistance_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAssistancePaymentAmount', trim( $arrValues['assistance_payment_amount'] ) ); elseif( isset( $arrValues['assistance_payment_amount'] ) ) $this->setAssistancePaymentAmount( $arrValues['assistance_payment_amount'] );
		if( isset( $arrValues['adjustment_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentPaymentAmount', trim( $arrValues['adjustment_payment_amount'] ) ); elseif( isset( $arrValues['adjustment_payment_amount'] ) ) $this->setAdjustmentPaymentAmount( $arrValues['adjustment_payment_amount'] );
		if( isset( $arrValues['repayment_agreement_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRepaymentAgreementAmount', trim( $arrValues['repayment_agreement_amount'] ) ); elseif( isset( $arrValues['repayment_agreement_amount'] ) ) $this->setRepaymentAgreementAmount( $arrValues['repayment_agreement_amount'] );
		if( isset( $arrValues['miscellaneous_request_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMiscellaneousRequestAmount', trim( $arrValues['miscellaneous_request_amount'] ) ); elseif( isset( $arrValues['miscellaneous_request_amount'] ) ) $this->setMiscellaneousRequestAmount( $arrValues['miscellaneous_request_amount'] );
		if( isset( $arrValues['special_claims_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialClaimsAmount', trim( $arrValues['special_claims_amount'] ) ); elseif( isset( $arrValues['special_claims_amount'] ) ) $this->setSpecialClaimsAmount( $arrValues['special_claims_amount'] );
		if( isset( $arrValues['total_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmount', trim( $arrValues['total_amount'] ) ); elseif( isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyContractId( $intSubsidyContractId ) {
		$this->set( 'm_intSubsidyContractId', CStrings::strToIntDef( $intSubsidyContractId, NULL, false ) );
	}

	public function getSubsidyContractId() {
		return $this->m_intSubsidyContractId;
	}

	public function sqlSubsidyContractId() {
		return ( true == isset( $this->m_intSubsidyContractId ) ) ? ( string ) $this->m_intSubsidyContractId : 'NULL';
	}

	public function setSubsidyHapRequestStatusTypeId( $intSubsidyHapRequestStatusTypeId ) {
		$this->set( 'm_intSubsidyHapRequestStatusTypeId', CStrings::strToIntDef( $intSubsidyHapRequestStatusTypeId, NULL, false ) );
	}

	public function getSubsidyHapRequestStatusTypeId() {
		return $this->m_intSubsidyHapRequestStatusTypeId;
	}

	public function sqlSubsidyHapRequestStatusTypeId() {
		return ( true == isset( $this->m_intSubsidyHapRequestStatusTypeId ) ) ? ( string ) $this->m_intSubsidyHapRequestStatusTypeId : 'NULL';
	}

	public function setPreviousSubsidyHapRequestId( $intPreviousSubsidyHapRequestId ) {
		$this->set( 'm_intPreviousSubsidyHapRequestId', CStrings::strToIntDef( $intPreviousSubsidyHapRequestId, NULL, false ) );
	}

	public function getPreviousSubsidyHapRequestId() {
		return $this->m_intPreviousSubsidyHapRequestId;
	}

	public function sqlPreviousSubsidyHapRequestId() {
		return ( true == isset( $this->m_intPreviousSubsidyHapRequestId ) ) ? ( string ) $this->m_intPreviousSubsidyHapRequestId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setVoucherDate( $strVoucherDate ) {
		$this->set( 'm_strVoucherDate', CStrings::strTrimDef( $strVoucherDate, -1, NULL, true ) );
	}

	public function getVoucherDate() {
		return $this->m_strVoucherDate;
	}

	public function sqlVoucherDate() {
		return ( true == isset( $this->m_strVoucherDate ) ) ? '\'' . $this->m_strVoucherDate . '\'' : 'NOW()';
	}

	public function setDateFinalized( $strDateFinalized ) {
		$this->set( 'm_strDateFinalized', CStrings::strTrimDef( $strDateFinalized, -1, NULL, true ) );
	}

	public function getDateFinalized() {
		return $this->m_strDateFinalized;
	}

	public function sqlDateFinalized() {
		return ( true == isset( $this->m_strDateFinalized ) ) ? '\'' . $this->m_strDateFinalized . '\'' : 'NULL';
	}

	public function setIncludeAdjustmentsThroughDatetime( $strIncludeAdjustmentsThroughDatetime ) {
		$this->set( 'm_strIncludeAdjustmentsThroughDatetime', CStrings::strTrimDef( $strIncludeAdjustmentsThroughDatetime, -1, NULL, true ) );
	}

	public function getIncludeAdjustmentsThroughDatetime() {
		return $this->m_strIncludeAdjustmentsThroughDatetime;
	}

	public function sqlIncludeAdjustmentsThroughDatetime() {
		return ( true == isset( $this->m_strIncludeAdjustmentsThroughDatetime ) ) ? '\'' . $this->m_strIncludeAdjustmentsThroughDatetime . '\'' : 'NOW()';
	}

	public function setAssistancePaymentAmount( $fltAssistancePaymentAmount ) {
		$this->set( 'm_fltAssistancePaymentAmount', CStrings::strToFloatDef( $fltAssistancePaymentAmount, NULL, false, 2 ) );
	}

	public function getAssistancePaymentAmount() {
		return $this->m_fltAssistancePaymentAmount;
	}

	public function sqlAssistancePaymentAmount() {
		return ( true == isset( $this->m_fltAssistancePaymentAmount ) ) ? ( string ) $this->m_fltAssistancePaymentAmount : '0.00';
	}

	public function setAdjustmentPaymentAmount( $fltAdjustmentPaymentAmount ) {
		$this->set( 'm_fltAdjustmentPaymentAmount', CStrings::strToFloatDef( $fltAdjustmentPaymentAmount, NULL, false, 2 ) );
	}

	public function getAdjustmentPaymentAmount() {
		return $this->m_fltAdjustmentPaymentAmount;
	}

	public function sqlAdjustmentPaymentAmount() {
		return ( true == isset( $this->m_fltAdjustmentPaymentAmount ) ) ? ( string ) $this->m_fltAdjustmentPaymentAmount : '0.00';
	}

	public function setRepaymentAgreementAmount( $fltRepaymentAgreementAmount ) {
		$this->set( 'm_fltRepaymentAgreementAmount', CStrings::strToFloatDef( $fltRepaymentAgreementAmount, NULL, false, 2 ) );
	}

	public function getRepaymentAgreementAmount() {
		return $this->m_fltRepaymentAgreementAmount;
	}

	public function sqlRepaymentAgreementAmount() {
		return ( true == isset( $this->m_fltRepaymentAgreementAmount ) ) ? ( string ) $this->m_fltRepaymentAgreementAmount : '0.00';
	}

	public function setMiscellaneousRequestAmount( $fltMiscellaneousRequestAmount ) {
		$this->set( 'm_fltMiscellaneousRequestAmount', CStrings::strToFloatDef( $fltMiscellaneousRequestAmount, NULL, false, 2 ) );
	}

	public function getMiscellaneousRequestAmount() {
		return $this->m_fltMiscellaneousRequestAmount;
	}

	public function sqlMiscellaneousRequestAmount() {
		return ( true == isset( $this->m_fltMiscellaneousRequestAmount ) ) ? ( string ) $this->m_fltMiscellaneousRequestAmount : '0.00';
	}

	public function setSpecialClaimsAmount( $fltSpecialClaimsAmount ) {
		$this->set( 'm_fltSpecialClaimsAmount', CStrings::strToFloatDef( $fltSpecialClaimsAmount, NULL, false, 2 ) );
	}

	public function getSpecialClaimsAmount() {
		return $this->m_fltSpecialClaimsAmount;
	}

	public function sqlSpecialClaimsAmount() {
		return ( true == isset( $this->m_fltSpecialClaimsAmount ) ) ? ( string ) $this->m_fltSpecialClaimsAmount : '0.00';
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->set( 'm_fltTotalAmount', CStrings::strToFloatDef( $fltTotalAmount, NULL, false, 2 ) );
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function sqlTotalAmount() {
		return ( true == isset( $this->m_fltTotalAmount ) ) ? ( string ) $this->m_fltTotalAmount : '0.00';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_contract_id, subsidy_hap_request_status_type_id, previous_subsidy_hap_request_id, ar_payment_id, voucher_date, date_finalized, include_adjustments_through_datetime, assistance_payment_amount, adjustment_payment_amount, repayment_agreement_amount, miscellaneous_request_amount, special_claims_amount, total_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSubsidyContractId() . ', ' .
						$this->sqlSubsidyHapRequestStatusTypeId() . ', ' .
						$this->sqlPreviousSubsidyHapRequestId() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlVoucherDate() . ', ' .
						$this->sqlDateFinalized() . ', ' .
						$this->sqlIncludeAdjustmentsThroughDatetime() . ', ' .
						$this->sqlAssistancePaymentAmount() . ', ' .
						$this->sqlAdjustmentPaymentAmount() . ', ' .
						$this->sqlRepaymentAgreementAmount() . ', ' .
						$this->sqlMiscellaneousRequestAmount() . ', ' .
						$this->sqlSpecialClaimsAmount() . ', ' .
						$this->sqlTotalAmount() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_hap_request_status_type_id = ' . $this->sqlSubsidyHapRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyHapRequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_hap_request_status_type_id = ' . $this->sqlSubsidyHapRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_subsidy_hap_request_id = ' . $this->sqlPreviousSubsidyHapRequestId(). ',' ; } elseif( true == array_key_exists( 'PreviousSubsidyHapRequestId', $this->getChangedColumns() ) ) { $strSql .= ' previous_subsidy_hap_request_id = ' . $this->sqlPreviousSubsidyHapRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voucher_date = ' . $this->sqlVoucherDate(). ',' ; } elseif( true == array_key_exists( 'VoucherDate', $this->getChangedColumns() ) ) { $strSql .= ' voucher_date = ' . $this->sqlVoucherDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_finalized = ' . $this->sqlDateFinalized(). ',' ; } elseif( true == array_key_exists( 'DateFinalized', $this->getChangedColumns() ) ) { $strSql .= ' date_finalized = ' . $this->sqlDateFinalized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_adjustments_through_datetime = ' . $this->sqlIncludeAdjustmentsThroughDatetime(). ',' ; } elseif( true == array_key_exists( 'IncludeAdjustmentsThroughDatetime', $this->getChangedColumns() ) ) { $strSql .= ' include_adjustments_through_datetime = ' . $this->sqlIncludeAdjustmentsThroughDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assistance_payment_amount = ' . $this->sqlAssistancePaymentAmount(). ',' ; } elseif( true == array_key_exists( 'AssistancePaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' assistance_payment_amount = ' . $this->sqlAssistancePaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_payment_amount = ' . $this->sqlAdjustmentPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'AdjustmentPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_payment_amount = ' . $this->sqlAdjustmentPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_agreement_amount = ' . $this->sqlRepaymentAgreementAmount(). ',' ; } elseif( true == array_key_exists( 'RepaymentAgreementAmount', $this->getChangedColumns() ) ) { $strSql .= ' repayment_agreement_amount = ' . $this->sqlRepaymentAgreementAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' miscellaneous_request_amount = ' . $this->sqlMiscellaneousRequestAmount(). ',' ; } elseif( true == array_key_exists( 'MiscellaneousRequestAmount', $this->getChangedColumns() ) ) { $strSql .= ' miscellaneous_request_amount = ' . $this->sqlMiscellaneousRequestAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_claims_amount = ' . $this->sqlSpecialClaimsAmount(). ',' ; } elseif( true == array_key_exists( 'SpecialClaimsAmount', $this->getChangedColumns() ) ) { $strSql .= ' special_claims_amount = ' . $this->sqlSpecialClaimsAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount(). ',' ; } elseif( true == array_key_exists( 'TotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_contract_id' => $this->getSubsidyContractId(),
			'subsidy_hap_request_status_type_id' => $this->getSubsidyHapRequestStatusTypeId(),
			'previous_subsidy_hap_request_id' => $this->getPreviousSubsidyHapRequestId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'voucher_date' => $this->getVoucherDate(),
			'date_finalized' => $this->getDateFinalized(),
			'include_adjustments_through_datetime' => $this->getIncludeAdjustmentsThroughDatetime(),
			'assistance_payment_amount' => $this->getAssistancePaymentAmount(),
			'adjustment_payment_amount' => $this->getAdjustmentPaymentAmount(),
			'repayment_agreement_amount' => $this->getRepaymentAgreementAmount(),
			'miscellaneous_request_amount' => $this->getMiscellaneousRequestAmount(),
			'special_claims_amount' => $this->getSpecialClaimsAmount(),
			'total_amount' => $this->getTotalAmount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>