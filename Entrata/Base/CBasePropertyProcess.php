<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyProcess extends CEosSingularBase {

	const TABLE_NAME = 'public.property_processes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intRefreshLeaseArchives;
	protected $m_intRefreshArTransactions;
	protected $m_intRefreshArBalances;
	protected $m_intRefreshGlTotals;
	protected $m_intRefreshInitialImports;
	protected $m_intRefreshUnitSpaceDisplayNumbers;
	protected $m_strLeaseArchivesRefreshedOn;
	protected $m_strArTransactionsRefreshedOn;
	protected $m_strArBalancesRefreshedOn;
	protected $m_strGlTotalsRefreshedOn;
	protected $m_strInitialImportsRefreshedOn;
	protected $m_strUnitSpaceDisplayNumbersRefreshedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRefreshLeaseArchives = '0';
		$this->m_intRefreshArTransactions = '0';
		$this->m_intRefreshArBalances = '0';
		$this->m_intRefreshGlTotals = '0';
		$this->m_intRefreshInitialImports = '0';
		$this->m_intRefreshUnitSpaceDisplayNumbers = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['refresh_lease_archives'] ) && $boolDirectSet ) $this->set( 'm_intRefreshLeaseArchives', trim( $arrValues['refresh_lease_archives'] ) ); elseif( isset( $arrValues['refresh_lease_archives'] ) ) $this->setRefreshLeaseArchives( $arrValues['refresh_lease_archives'] );
		if( isset( $arrValues['refresh_ar_transactions'] ) && $boolDirectSet ) $this->set( 'm_intRefreshArTransactions', trim( $arrValues['refresh_ar_transactions'] ) ); elseif( isset( $arrValues['refresh_ar_transactions'] ) ) $this->setRefreshArTransactions( $arrValues['refresh_ar_transactions'] );
		if( isset( $arrValues['refresh_ar_balances'] ) && $boolDirectSet ) $this->set( 'm_intRefreshArBalances', trim( $arrValues['refresh_ar_balances'] ) ); elseif( isset( $arrValues['refresh_ar_balances'] ) ) $this->setRefreshArBalances( $arrValues['refresh_ar_balances'] );
		if( isset( $arrValues['refresh_gl_totals'] ) && $boolDirectSet ) $this->set( 'm_intRefreshGlTotals', trim( $arrValues['refresh_gl_totals'] ) ); elseif( isset( $arrValues['refresh_gl_totals'] ) ) $this->setRefreshGlTotals( $arrValues['refresh_gl_totals'] );
		if( isset( $arrValues['refresh_initial_imports'] ) && $boolDirectSet ) $this->set( 'm_intRefreshInitialImports', trim( $arrValues['refresh_initial_imports'] ) ); elseif( isset( $arrValues['refresh_initial_imports'] ) ) $this->setRefreshInitialImports( $arrValues['refresh_initial_imports'] );
		if( isset( $arrValues['refresh_unit_space_display_numbers'] ) && $boolDirectSet ) $this->set( 'm_intRefreshUnitSpaceDisplayNumbers', trim( $arrValues['refresh_unit_space_display_numbers'] ) ); elseif( isset( $arrValues['refresh_unit_space_display_numbers'] ) ) $this->setRefreshUnitSpaceDisplayNumbers( $arrValues['refresh_unit_space_display_numbers'] );
		if( isset( $arrValues['lease_archives_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseArchivesRefreshedOn', trim( $arrValues['lease_archives_refreshed_on'] ) ); elseif( isset( $arrValues['lease_archives_refreshed_on'] ) ) $this->setLeaseArchivesRefreshedOn( $arrValues['lease_archives_refreshed_on'] );
		if( isset( $arrValues['ar_transactions_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strArTransactionsRefreshedOn', trim( $arrValues['ar_transactions_refreshed_on'] ) ); elseif( isset( $arrValues['ar_transactions_refreshed_on'] ) ) $this->setArTransactionsRefreshedOn( $arrValues['ar_transactions_refreshed_on'] );
		if( isset( $arrValues['ar_balances_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strArBalancesRefreshedOn', trim( $arrValues['ar_balances_refreshed_on'] ) ); elseif( isset( $arrValues['ar_balances_refreshed_on'] ) ) $this->setArBalancesRefreshedOn( $arrValues['ar_balances_refreshed_on'] );
		if( isset( $arrValues['gl_totals_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strGlTotalsRefreshedOn', trim( $arrValues['gl_totals_refreshed_on'] ) ); elseif( isset( $arrValues['gl_totals_refreshed_on'] ) ) $this->setGlTotalsRefreshedOn( $arrValues['gl_totals_refreshed_on'] );
		if( isset( $arrValues['initial_imports_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strInitialImportsRefreshedOn', trim( $arrValues['initial_imports_refreshed_on'] ) ); elseif( isset( $arrValues['initial_imports_refreshed_on'] ) ) $this->setInitialImportsRefreshedOn( $arrValues['initial_imports_refreshed_on'] );
		if( isset( $arrValues['unit_space_display_numbers_refreshed_on'] ) && $boolDirectSet ) $this->set( 'm_strUnitSpaceDisplayNumbersRefreshedOn', trim( $arrValues['unit_space_display_numbers_refreshed_on'] ) ); elseif( isset( $arrValues['unit_space_display_numbers_refreshed_on'] ) ) $this->setUnitSpaceDisplayNumbersRefreshedOn( $arrValues['unit_space_display_numbers_refreshed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRefreshLeaseArchives( $intRefreshLeaseArchives ) {
		$this->set( 'm_intRefreshLeaseArchives', CStrings::strToIntDef( $intRefreshLeaseArchives, NULL, false ) );
	}

	public function getRefreshLeaseArchives() {
		return $this->m_intRefreshLeaseArchives;
	}

	public function sqlRefreshLeaseArchives() {
		return ( true == isset( $this->m_intRefreshLeaseArchives ) ) ? ( string ) $this->m_intRefreshLeaseArchives : '0';
	}

	public function setRefreshArTransactions( $intRefreshArTransactions ) {
		$this->set( 'm_intRefreshArTransactions', CStrings::strToIntDef( $intRefreshArTransactions, NULL, false ) );
	}

	public function getRefreshArTransactions() {
		return $this->m_intRefreshArTransactions;
	}

	public function sqlRefreshArTransactions() {
		return ( true == isset( $this->m_intRefreshArTransactions ) ) ? ( string ) $this->m_intRefreshArTransactions : '0';
	}

	public function setRefreshArBalances( $intRefreshArBalances ) {
		$this->set( 'm_intRefreshArBalances', CStrings::strToIntDef( $intRefreshArBalances, NULL, false ) );
	}

	public function getRefreshArBalances() {
		return $this->m_intRefreshArBalances;
	}

	public function sqlRefreshArBalances() {
		return ( true == isset( $this->m_intRefreshArBalances ) ) ? ( string ) $this->m_intRefreshArBalances : '0';
	}

	public function setRefreshGlTotals( $intRefreshGlTotals ) {
		$this->set( 'm_intRefreshGlTotals', CStrings::strToIntDef( $intRefreshGlTotals, NULL, false ) );
	}

	public function getRefreshGlTotals() {
		return $this->m_intRefreshGlTotals;
	}

	public function sqlRefreshGlTotals() {
		return ( true == isset( $this->m_intRefreshGlTotals ) ) ? ( string ) $this->m_intRefreshGlTotals : '0';
	}

	public function setRefreshInitialImports( $intRefreshInitialImports ) {
		$this->set( 'm_intRefreshInitialImports', CStrings::strToIntDef( $intRefreshInitialImports, NULL, false ) );
	}

	public function getRefreshInitialImports() {
		return $this->m_intRefreshInitialImports;
	}

	public function sqlRefreshInitialImports() {
		return ( true == isset( $this->m_intRefreshInitialImports ) ) ? ( string ) $this->m_intRefreshInitialImports : '0';
	}

	public function setRefreshUnitSpaceDisplayNumbers( $intRefreshUnitSpaceDisplayNumbers ) {
		$this->set( 'm_intRefreshUnitSpaceDisplayNumbers', CStrings::strToIntDef( $intRefreshUnitSpaceDisplayNumbers, NULL, false ) );
	}

	public function getRefreshUnitSpaceDisplayNumbers() {
		return $this->m_intRefreshUnitSpaceDisplayNumbers;
	}

	public function sqlRefreshUnitSpaceDisplayNumbers() {
		return ( true == isset( $this->m_intRefreshUnitSpaceDisplayNumbers ) ) ? ( string ) $this->m_intRefreshUnitSpaceDisplayNumbers : '0';
	}

	public function setLeaseArchivesRefreshedOn( $strLeaseArchivesRefreshedOn ) {
		$this->set( 'm_strLeaseArchivesRefreshedOn', CStrings::strTrimDef( $strLeaseArchivesRefreshedOn, -1, NULL, true ) );
	}

	public function getLeaseArchivesRefreshedOn() {
		return $this->m_strLeaseArchivesRefreshedOn;
	}

	public function sqlLeaseArchivesRefreshedOn() {
		return ( true == isset( $this->m_strLeaseArchivesRefreshedOn ) ) ? '\'' . $this->m_strLeaseArchivesRefreshedOn . '\'' : 'NULL';
	}

	public function setArTransactionsRefreshedOn( $strArTransactionsRefreshedOn ) {
		$this->set( 'm_strArTransactionsRefreshedOn', CStrings::strTrimDef( $strArTransactionsRefreshedOn, -1, NULL, true ) );
	}

	public function getArTransactionsRefreshedOn() {
		return $this->m_strArTransactionsRefreshedOn;
	}

	public function sqlArTransactionsRefreshedOn() {
		return ( true == isset( $this->m_strArTransactionsRefreshedOn ) ) ? '\'' . $this->m_strArTransactionsRefreshedOn . '\'' : 'NULL';
	}

	public function setArBalancesRefreshedOn( $strArBalancesRefreshedOn ) {
		$this->set( 'm_strArBalancesRefreshedOn', CStrings::strTrimDef( $strArBalancesRefreshedOn, -1, NULL, true ) );
	}

	public function getArBalancesRefreshedOn() {
		return $this->m_strArBalancesRefreshedOn;
	}

	public function sqlArBalancesRefreshedOn() {
		return ( true == isset( $this->m_strArBalancesRefreshedOn ) ) ? '\'' . $this->m_strArBalancesRefreshedOn . '\'' : 'NULL';
	}

	public function setGlTotalsRefreshedOn( $strGlTotalsRefreshedOn ) {
		$this->set( 'm_strGlTotalsRefreshedOn', CStrings::strTrimDef( $strGlTotalsRefreshedOn, -1, NULL, true ) );
	}

	public function getGlTotalsRefreshedOn() {
		return $this->m_strGlTotalsRefreshedOn;
	}

	public function sqlGlTotalsRefreshedOn() {
		return ( true == isset( $this->m_strGlTotalsRefreshedOn ) ) ? '\'' . $this->m_strGlTotalsRefreshedOn . '\'' : 'NULL';
	}

	public function setInitialImportsRefreshedOn( $strInitialImportsRefreshedOn ) {
		$this->set( 'm_strInitialImportsRefreshedOn', CStrings::strTrimDef( $strInitialImportsRefreshedOn, -1, NULL, true ) );
	}

	public function getInitialImportsRefreshedOn() {
		return $this->m_strInitialImportsRefreshedOn;
	}

	public function sqlInitialImportsRefreshedOn() {
		return ( true == isset( $this->m_strInitialImportsRefreshedOn ) ) ? '\'' . $this->m_strInitialImportsRefreshedOn . '\'' : 'NULL';
	}

	public function setUnitSpaceDisplayNumbersRefreshedOn( $strUnitSpaceDisplayNumbersRefreshedOn ) {
		$this->set( 'm_strUnitSpaceDisplayNumbersRefreshedOn', CStrings::strTrimDef( $strUnitSpaceDisplayNumbersRefreshedOn, -1, NULL, true ) );
	}

	public function getUnitSpaceDisplayNumbersRefreshedOn() {
		return $this->m_strUnitSpaceDisplayNumbersRefreshedOn;
	}

	public function sqlUnitSpaceDisplayNumbersRefreshedOn() {
		return ( true == isset( $this->m_strUnitSpaceDisplayNumbersRefreshedOn ) ) ? '\'' . $this->m_strUnitSpaceDisplayNumbersRefreshedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, refresh_lease_archives, refresh_ar_transactions, refresh_ar_balances, refresh_gl_totals, refresh_initial_imports, refresh_unit_space_display_numbers, lease_archives_refreshed_on, ar_transactions_refreshed_on, ar_balances_refreshed_on, gl_totals_refreshed_on, initial_imports_refreshed_on, unit_space_display_numbers_refreshed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlRefreshLeaseArchives() . ', ' .
 						$this->sqlRefreshArTransactions() . ', ' .
 						$this->sqlRefreshArBalances() . ', ' .
 						$this->sqlRefreshGlTotals() . ', ' .
 						$this->sqlRefreshInitialImports() . ', ' .
 						$this->sqlRefreshUnitSpaceDisplayNumbers() . ', ' .
 						$this->sqlLeaseArchivesRefreshedOn() . ', ' .
 						$this->sqlArTransactionsRefreshedOn() . ', ' .
 						$this->sqlArBalancesRefreshedOn() . ', ' .
 						$this->sqlGlTotalsRefreshedOn() . ', ' .
 						$this->sqlInitialImportsRefreshedOn() . ', ' .
 						$this->sqlUnitSpaceDisplayNumbersRefreshedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_lease_archives = ' . $this->sqlRefreshLeaseArchives() . ','; } elseif( true == array_key_exists( 'RefreshLeaseArchives', $this->getChangedColumns() ) ) { $strSql .= ' refresh_lease_archives = ' . $this->sqlRefreshLeaseArchives() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_ar_transactions = ' . $this->sqlRefreshArTransactions() . ','; } elseif( true == array_key_exists( 'RefreshArTransactions', $this->getChangedColumns() ) ) { $strSql .= ' refresh_ar_transactions = ' . $this->sqlRefreshArTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_ar_balances = ' . $this->sqlRefreshArBalances() . ','; } elseif( true == array_key_exists( 'RefreshArBalances', $this->getChangedColumns() ) ) { $strSql .= ' refresh_ar_balances = ' . $this->sqlRefreshArBalances() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_gl_totals = ' . $this->sqlRefreshGlTotals() . ','; } elseif( true == array_key_exists( 'RefreshGlTotals', $this->getChangedColumns() ) ) { $strSql .= ' refresh_gl_totals = ' . $this->sqlRefreshGlTotals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_initial_imports = ' . $this->sqlRefreshInitialImports() . ','; } elseif( true == array_key_exists( 'RefreshInitialImports', $this->getChangedColumns() ) ) { $strSql .= ' refresh_initial_imports = ' . $this->sqlRefreshInitialImports() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_unit_space_display_numbers = ' . $this->sqlRefreshUnitSpaceDisplayNumbers() . ','; } elseif( true == array_key_exists( 'RefreshUnitSpaceDisplayNumbers', $this->getChangedColumns() ) ) { $strSql .= ' refresh_unit_space_display_numbers = ' . $this->sqlRefreshUnitSpaceDisplayNumbers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_archives_refreshed_on = ' . $this->sqlLeaseArchivesRefreshedOn() . ','; } elseif( true == array_key_exists( 'LeaseArchivesRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_archives_refreshed_on = ' . $this->sqlLeaseArchivesRefreshedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transactions_refreshed_on = ' . $this->sqlArTransactionsRefreshedOn() . ','; } elseif( true == array_key_exists( 'ArTransactionsRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' ar_transactions_refreshed_on = ' . $this->sqlArTransactionsRefreshedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_balances_refreshed_on = ' . $this->sqlArBalancesRefreshedOn() . ','; } elseif( true == array_key_exists( 'ArBalancesRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' ar_balances_refreshed_on = ' . $this->sqlArBalancesRefreshedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_totals_refreshed_on = ' . $this->sqlGlTotalsRefreshedOn() . ','; } elseif( true == array_key_exists( 'GlTotalsRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' gl_totals_refreshed_on = ' . $this->sqlGlTotalsRefreshedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_imports_refreshed_on = ' . $this->sqlInitialImportsRefreshedOn() . ','; } elseif( true == array_key_exists( 'InitialImportsRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' initial_imports_refreshed_on = ' . $this->sqlInitialImportsRefreshedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_display_numbers_refreshed_on = ' . $this->sqlUnitSpaceDisplayNumbersRefreshedOn() . ','; } elseif( true == array_key_exists( 'UnitSpaceDisplayNumbersRefreshedOn', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_display_numbers_refreshed_on = ' . $this->sqlUnitSpaceDisplayNumbersRefreshedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'refresh_lease_archives' => $this->getRefreshLeaseArchives(),
			'refresh_ar_transactions' => $this->getRefreshArTransactions(),
			'refresh_ar_balances' => $this->getRefreshArBalances(),
			'refresh_gl_totals' => $this->getRefreshGlTotals(),
			'refresh_initial_imports' => $this->getRefreshInitialImports(),
			'refresh_unit_space_display_numbers' => $this->getRefreshUnitSpaceDisplayNumbers(),
			'lease_archives_refreshed_on' => $this->getLeaseArchivesRefreshedOn(),
			'ar_transactions_refreshed_on' => $this->getArTransactionsRefreshedOn(),
			'ar_balances_refreshed_on' => $this->getArBalancesRefreshedOn(),
			'gl_totals_refreshed_on' => $this->getGlTotalsRefreshedOn(),
			'initial_imports_refreshed_on' => $this->getInitialImportsRefreshedOn(),
			'unit_space_display_numbers_refreshed_on' => $this->getUnitSpaceDisplayNumbersRefreshedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>