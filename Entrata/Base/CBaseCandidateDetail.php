<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.candidate_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCandidateId;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_intMaxLoginAttempts;
	protected $m_intLoginErrorCount;
	protected $m_intRequiresPasswordChange;
	protected $m_intDontAllowLogin;
	protected $m_strDlNumberEncrypted;
	protected $m_strDlStateCode;
	protected $m_strDlProvince;
	protected $m_strDigitalSignature;
	protected $m_strAcceptanceDatetime;
	protected $m_strRefusalDatetime;
	protected $m_strStartDatetime;
	protected $m_strApprovalDatetime;
	protected $m_strApprovalIpAddress;
	protected $m_intInquiryBy;
	protected $m_strInquiryOn;
	protected $m_strInquiryApprovedOn;
	protected $m_strInquiryDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMaxLoginAttempts = '50';
		$this->m_intLoginErrorCount = '0';
		$this->m_intRequiresPasswordChange = '0';
		$this->m_intDontAllowLogin = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['candidate_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateId', trim( $arrValues['candidate_id'] ) ); elseif( isset( $arrValues['candidate_id'] ) ) $this->setCandidateId( $arrValues['candidate_id'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['max_login_attempts'] ) && $boolDirectSet ) $this->set( 'm_intMaxLoginAttempts', trim( $arrValues['max_login_attempts'] ) ); elseif( isset( $arrValues['max_login_attempts'] ) ) $this->setMaxLoginAttempts( $arrValues['max_login_attempts'] );
		if( isset( $arrValues['login_error_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginErrorCount', trim( $arrValues['login_error_count'] ) ); elseif( isset( $arrValues['login_error_count'] ) ) $this->setLoginErrorCount( $arrValues['login_error_count'] );
		if( isset( $arrValues['requires_password_change'] ) && $boolDirectSet ) $this->set( 'm_intRequiresPasswordChange', trim( $arrValues['requires_password_change'] ) ); elseif( isset( $arrValues['requires_password_change'] ) ) $this->setRequiresPasswordChange( $arrValues['requires_password_change'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['dl_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDlNumberEncrypted', trim( stripcslashes( $arrValues['dl_number_encrypted'] ) ) ); elseif( isset( $arrValues['dl_number_encrypted'] ) ) $this->setDlNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dl_number_encrypted'] ) : $arrValues['dl_number_encrypted'] );
		if( isset( $arrValues['dl_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDlStateCode', trim( stripcslashes( $arrValues['dl_state_code'] ) ) ); elseif( isset( $arrValues['dl_state_code'] ) ) $this->setDlStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dl_state_code'] ) : $arrValues['dl_state_code'] );
		if( isset( $arrValues['dl_province'] ) && $boolDirectSet ) $this->set( 'm_strDlProvince', trim( stripcslashes( $arrValues['dl_province'] ) ) ); elseif( isset( $arrValues['dl_province'] ) ) $this->setDlProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dl_province'] ) : $arrValues['dl_province'] );
		if( isset( $arrValues['digital_signature'] ) && $boolDirectSet ) $this->set( 'm_strDigitalSignature', trim( stripcslashes( $arrValues['digital_signature'] ) ) ); elseif( isset( $arrValues['digital_signature'] ) ) $this->setDigitalSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['digital_signature'] ) : $arrValues['digital_signature'] );
		if( isset( $arrValues['acceptance_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAcceptanceDatetime', trim( $arrValues['acceptance_datetime'] ) ); elseif( isset( $arrValues['acceptance_datetime'] ) ) $this->setAcceptanceDatetime( $arrValues['acceptance_datetime'] );
		if( isset( $arrValues['refusal_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRefusalDatetime', trim( $arrValues['refusal_datetime'] ) ); elseif( isset( $arrValues['refusal_datetime'] ) ) $this->setRefusalDatetime( $arrValues['refusal_datetime'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['approval_datetime'] ) && $boolDirectSet ) $this->set( 'm_strApprovalDatetime', trim( $arrValues['approval_datetime'] ) ); elseif( isset( $arrValues['approval_datetime'] ) ) $this->setApprovalDatetime( $arrValues['approval_datetime'] );
		if( isset( $arrValues['approval_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strApprovalIpAddress', trim( stripcslashes( $arrValues['approval_ip_address'] ) ) ); elseif( isset( $arrValues['approval_ip_address'] ) ) $this->setApprovalIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approval_ip_address'] ) : $arrValues['approval_ip_address'] );
		if( isset( $arrValues['inquiry_by'] ) && $boolDirectSet ) $this->set( 'm_intInquiryBy', trim( $arrValues['inquiry_by'] ) ); elseif( isset( $arrValues['inquiry_by'] ) ) $this->setInquiryBy( $arrValues['inquiry_by'] );
		if( isset( $arrValues['inquiry_on'] ) && $boolDirectSet ) $this->set( 'm_strInquiryOn', trim( $arrValues['inquiry_on'] ) ); elseif( isset( $arrValues['inquiry_on'] ) ) $this->setInquiryOn( $arrValues['inquiry_on'] );
		if( isset( $arrValues['inquiry_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strInquiryApprovedOn', trim( $arrValues['inquiry_approved_on'] ) ); elseif( isset( $arrValues['inquiry_approved_on'] ) ) $this->setInquiryApprovedOn( $arrValues['inquiry_approved_on'] );
		if( isset( $arrValues['inquiry_denied_on'] ) && $boolDirectSet ) $this->set( 'm_strInquiryDeniedOn', trim( $arrValues['inquiry_denied_on'] ) ); elseif( isset( $arrValues['inquiry_denied_on'] ) ) $this->setInquiryDeniedOn( $arrValues['inquiry_denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCandidateId( $intCandidateId ) {
		$this->set( 'm_intCandidateId', CStrings::strToIntDef( $intCandidateId, NULL, false ) );
	}

	public function getCandidateId() {
		return $this->m_intCandidateId;
	}

	public function sqlCandidateId() {
		return ( true == isset( $this->m_intCandidateId ) ) ? ( string ) $this->m_intCandidateId : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setMaxLoginAttempts( $intMaxLoginAttempts ) {
		$this->set( 'm_intMaxLoginAttempts', CStrings::strToIntDef( $intMaxLoginAttempts, NULL, false ) );
	}

	public function getMaxLoginAttempts() {
		return $this->m_intMaxLoginAttempts;
	}

	public function sqlMaxLoginAttempts() {
		return ( true == isset( $this->m_intMaxLoginAttempts ) ) ? ( string ) $this->m_intMaxLoginAttempts : '50';
	}

	public function setLoginErrorCount( $intLoginErrorCount ) {
		$this->set( 'm_intLoginErrorCount', CStrings::strToIntDef( $intLoginErrorCount, NULL, false ) );
	}

	public function getLoginErrorCount() {
		return $this->m_intLoginErrorCount;
	}

	public function sqlLoginErrorCount() {
		return ( true == isset( $this->m_intLoginErrorCount ) ) ? ( string ) $this->m_intLoginErrorCount : '0';
	}

	public function setRequiresPasswordChange( $intRequiresPasswordChange ) {
		$this->set( 'm_intRequiresPasswordChange', CStrings::strToIntDef( $intRequiresPasswordChange, NULL, false ) );
	}

	public function getRequiresPasswordChange() {
		return $this->m_intRequiresPasswordChange;
	}

	public function sqlRequiresPasswordChange() {
		return ( true == isset( $this->m_intRequiresPasswordChange ) ) ? ( string ) $this->m_intRequiresPasswordChange : '0';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setDlNumberEncrypted( $strDlNumberEncrypted ) {
		$this->set( 'm_strDlNumberEncrypted', CStrings::strTrimDef( $strDlNumberEncrypted, 240, NULL, true ) );
	}

	public function getDlNumberEncrypted() {
		return $this->m_strDlNumberEncrypted;
	}

	public function sqlDlNumberEncrypted() {
		return ( true == isset( $this->m_strDlNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strDlNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setDlStateCode( $strDlStateCode ) {
		$this->set( 'm_strDlStateCode', CStrings::strTrimDef( $strDlStateCode, 2, NULL, true ) );
	}

	public function getDlStateCode() {
		return $this->m_strDlStateCode;
	}

	public function sqlDlStateCode() {
		return ( true == isset( $this->m_strDlStateCode ) ) ? '\'' . addslashes( $this->m_strDlStateCode ) . '\'' : 'NULL';
	}

	public function setDlProvince( $strDlProvince ) {
		$this->set( 'm_strDlProvince', CStrings::strTrimDef( $strDlProvince, 50, NULL, true ) );
	}

	public function getDlProvince() {
		return $this->m_strDlProvince;
	}

	public function sqlDlProvince() {
		return ( true == isset( $this->m_strDlProvince ) ) ? '\'' . addslashes( $this->m_strDlProvince ) . '\'' : 'NULL';
	}

	public function setDigitalSignature( $strDigitalSignature ) {
		$this->set( 'm_strDigitalSignature', CStrings::strTrimDef( $strDigitalSignature, 240, NULL, true ) );
	}

	public function getDigitalSignature() {
		return $this->m_strDigitalSignature;
	}

	public function sqlDigitalSignature() {
		return ( true == isset( $this->m_strDigitalSignature ) ) ? '\'' . addslashes( $this->m_strDigitalSignature ) . '\'' : 'NULL';
	}

	public function setAcceptanceDatetime( $strAcceptanceDatetime ) {
		$this->set( 'm_strAcceptanceDatetime', CStrings::strTrimDef( $strAcceptanceDatetime, -1, NULL, true ) );
	}

	public function getAcceptanceDatetime() {
		return $this->m_strAcceptanceDatetime;
	}

	public function sqlAcceptanceDatetime() {
		return ( true == isset( $this->m_strAcceptanceDatetime ) ) ? '\'' . $this->m_strAcceptanceDatetime . '\'' : 'NULL';
	}

	public function setRefusalDatetime( $strRefusalDatetime ) {
		$this->set( 'm_strRefusalDatetime', CStrings::strTrimDef( $strRefusalDatetime, -1, NULL, true ) );
	}

	public function getRefusalDatetime() {
		return $this->m_strRefusalDatetime;
	}

	public function sqlRefusalDatetime() {
		return ( true == isset( $this->m_strRefusalDatetime ) ) ? '\'' . $this->m_strRefusalDatetime . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setApprovalDatetime( $strApprovalDatetime ) {
		$this->set( 'm_strApprovalDatetime', CStrings::strTrimDef( $strApprovalDatetime, -1, NULL, true ) );
	}

	public function getApprovalDatetime() {
		return $this->m_strApprovalDatetime;
	}

	public function sqlApprovalDatetime() {
		return ( true == isset( $this->m_strApprovalDatetime ) ) ? '\'' . $this->m_strApprovalDatetime . '\'' : 'NULL';
	}

	public function setApprovalIpAddress( $strApprovalIpAddress ) {
		$this->set( 'm_strApprovalIpAddress', CStrings::strTrimDef( $strApprovalIpAddress, 23, NULL, true ) );
	}

	public function getApprovalIpAddress() {
		return $this->m_strApprovalIpAddress;
	}

	public function sqlApprovalIpAddress() {
		return ( true == isset( $this->m_strApprovalIpAddress ) ) ? '\'' . addslashes( $this->m_strApprovalIpAddress ) . '\'' : 'NULL';
	}

	public function setInquiryBy( $intInquiryBy ) {
		$this->set( 'm_intInquiryBy', CStrings::strToIntDef( $intInquiryBy, NULL, false ) );
	}

	public function getInquiryBy() {
		return $this->m_intInquiryBy;
	}

	public function sqlInquiryBy() {
		return ( true == isset( $this->m_intInquiryBy ) ) ? ( string ) $this->m_intInquiryBy : 'NULL';
	}

	public function setInquiryOn( $strInquiryOn ) {
		$this->set( 'm_strInquiryOn', CStrings::strTrimDef( $strInquiryOn, -1, NULL, true ) );
	}

	public function getInquiryOn() {
		return $this->m_strInquiryOn;
	}

	public function sqlInquiryOn() {
		return ( true == isset( $this->m_strInquiryOn ) ) ? '\'' . $this->m_strInquiryOn . '\'' : 'NULL';
	}

	public function setInquiryApprovedOn( $strInquiryApprovedOn ) {
		$this->set( 'm_strInquiryApprovedOn', CStrings::strTrimDef( $strInquiryApprovedOn, -1, NULL, true ) );
	}

	public function getInquiryApprovedOn() {
		return $this->m_strInquiryApprovedOn;
	}

	public function sqlInquiryApprovedOn() {
		return ( true == isset( $this->m_strInquiryApprovedOn ) ) ? '\'' . $this->m_strInquiryApprovedOn . '\'' : 'NULL';
	}

	public function setInquiryDeniedOn( $strInquiryDeniedOn ) {
		$this->set( 'm_strInquiryDeniedOn', CStrings::strTrimDef( $strInquiryDeniedOn, -1, NULL, true ) );
	}

	public function getInquiryDeniedOn() {
		return $this->m_strInquiryDeniedOn;
	}

	public function sqlInquiryDeniedOn() {
		return ( true == isset( $this->m_strInquiryDeniedOn ) ) ? '\'' . $this->m_strInquiryDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, candidate_id, username, password_encrypted, max_login_attempts, login_error_count, requires_password_change, dont_allow_login, dl_number_encrypted, dl_state_code, dl_province, digital_signature, acceptance_datetime, refusal_datetime, start_datetime, approval_datetime, approval_ip_address, inquiry_by, inquiry_on, inquiry_approved_on, inquiry_denied_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCandidateId() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlMaxLoginAttempts() . ', ' .
 						$this->sqlLoginErrorCount() . ', ' .
 						$this->sqlRequiresPasswordChange() . ', ' .
 						$this->sqlDontAllowLogin() . ', ' .
 						$this->sqlDlNumberEncrypted() . ', ' .
 						$this->sqlDlStateCode() . ', ' .
 						$this->sqlDlProvince() . ', ' .
 						$this->sqlDigitalSignature() . ', ' .
 						$this->sqlAcceptanceDatetime() . ', ' .
 						$this->sqlRefusalDatetime() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlApprovalDatetime() . ', ' .
 						$this->sqlApprovalIpAddress() . ', ' .
 						$this->sqlInquiryBy() . ', ' .
 						$this->sqlInquiryOn() . ', ' .
 						$this->sqlInquiryApprovedOn() . ', ' .
 						$this->sqlInquiryDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; } elseif( true == array_key_exists( 'CandidateId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_login_attempts = ' . $this->sqlMaxLoginAttempts() . ','; } elseif( true == array_key_exists( 'MaxLoginAttempts', $this->getChangedColumns() ) ) { $strSql .= ' max_login_attempts = ' . $this->sqlMaxLoginAttempts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_error_count = ' . $this->sqlLoginErrorCount() . ','; } elseif( true == array_key_exists( 'LoginErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' login_error_count = ' . $this->sqlLoginErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_password_change = ' . $this->sqlRequiresPasswordChange() . ','; } elseif( true == array_key_exists( 'RequiresPasswordChange', $this->getChangedColumns() ) ) { $strSql .= ' requires_password_change = ' . $this->sqlRequiresPasswordChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; } elseif( true == array_key_exists( 'DontAllowLogin', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted() . ','; } elseif( true == array_key_exists( 'DlNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode() . ','; } elseif( true == array_key_exists( 'DlStateCode', $this->getChangedColumns() ) ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_province = ' . $this->sqlDlProvince() . ','; } elseif( true == array_key_exists( 'DlProvince', $this->getChangedColumns() ) ) { $strSql .= ' dl_province = ' . $this->sqlDlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature() . ','; } elseif( true == array_key_exists( 'DigitalSignature', $this->getChangedColumns() ) ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acceptance_datetime = ' . $this->sqlAcceptanceDatetime() . ','; } elseif( true == array_key_exists( 'AcceptanceDatetime', $this->getChangedColumns() ) ) { $strSql .= ' acceptance_datetime = ' . $this->sqlAcceptanceDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refusal_datetime = ' . $this->sqlRefusalDatetime() . ','; } elseif( true == array_key_exists( 'RefusalDatetime', $this->getChangedColumns() ) ) { $strSql .= ' refusal_datetime = ' . $this->sqlRefusalDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_datetime = ' . $this->sqlApprovalDatetime() . ','; } elseif( true == array_key_exists( 'ApprovalDatetime', $this->getChangedColumns() ) ) { $strSql .= ' approval_datetime = ' . $this->sqlApprovalDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; } elseif( true == array_key_exists( 'ApprovalIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_by = ' . $this->sqlInquiryBy() . ','; } elseif( true == array_key_exists( 'InquiryBy', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_by = ' . $this->sqlInquiryBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_on = ' . $this->sqlInquiryOn() . ','; } elseif( true == array_key_exists( 'InquiryOn', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_on = ' . $this->sqlInquiryOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_approved_on = ' . $this->sqlInquiryApprovedOn() . ','; } elseif( true == array_key_exists( 'InquiryApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_approved_on = ' . $this->sqlInquiryApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inquiry_denied_on = ' . $this->sqlInquiryDeniedOn() . ','; } elseif( true == array_key_exists( 'InquiryDeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' inquiry_denied_on = ' . $this->sqlInquiryDeniedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'candidate_id' => $this->getCandidateId(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'max_login_attempts' => $this->getMaxLoginAttempts(),
			'login_error_count' => $this->getLoginErrorCount(),
			'requires_password_change' => $this->getRequiresPasswordChange(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'dl_number_encrypted' => $this->getDlNumberEncrypted(),
			'dl_state_code' => $this->getDlStateCode(),
			'dl_province' => $this->getDlProvince(),
			'digital_signature' => $this->getDigitalSignature(),
			'acceptance_datetime' => $this->getAcceptanceDatetime(),
			'refusal_datetime' => $this->getRefusalDatetime(),
			'start_datetime' => $this->getStartDatetime(),
			'approval_datetime' => $this->getApprovalDatetime(),
			'approval_ip_address' => $this->getApprovalIpAddress(),
			'inquiry_by' => $this->getInquiryBy(),
			'inquiry_on' => $this->getInquiryOn(),
			'inquiry_approved_on' => $this->getInquiryApprovedOn(),
			'inquiry_denied_on' => $this->getInquiryDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>