<?php

class CBaseTemplateThemeDefaultColorsType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.template_theme_default_colors_types';

	protected $m_intId;
	protected $m_strColorName;
	protected $m_strColorKey;
	protected $m_strColorValue;
	protected $m_boolIsPrivate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strColorValue = 'E6E6E6';
		$this->m_boolIsPrivate = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['color_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strColorName', trim( stripcslashes( $arrValues['color_name'] ) ) ); elseif( isset( $arrValues['color_name'] ) ) $this->setColorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color_name'] ) : $arrValues['color_name'] );
		if( isset( $arrValues['color_key'] ) && $boolDirectSet ) $this->set( 'm_strColorKey', trim( stripcslashes( $arrValues['color_key'] ) ) ); elseif( isset( $arrValues['color_key'] ) ) $this->setColorKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color_key'] ) : $arrValues['color_key'] );
		if( isset( $arrValues['color_value'] ) && $boolDirectSet ) $this->set( 'm_strColorValue', trim( stripcslashes( $arrValues['color_value'] ) ) ); elseif( isset( $arrValues['color_value'] ) ) $this->setColorValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color_value'] ) : $arrValues['color_value'] );
		if( isset( $arrValues['is_private'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrivate', trim( stripcslashes( $arrValues['is_private'] ) ) ); elseif( isset( $arrValues['is_private'] ) ) $this->setIsPrivate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_private'] ) : $arrValues['is_private'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setColorName( $strColorName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strColorName', CStrings::strTrimDef( $strColorName, 50, NULL, true ), $strLocaleCode );
	}

	public function getColorName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strColorName', $strLocaleCode );
	}

	public function sqlColorName() {
		return ( true == isset( $this->m_strColorName ) ) ? '\'' . addslashes( $this->m_strColorName ) . '\'' : 'NULL';
	}

	public function setColorKey( $strColorKey ) {
		$this->set( 'm_strColorKey', CStrings::strTrimDef( $strColorKey, 50, NULL, true ) );
	}

	public function getColorKey() {
		return $this->m_strColorKey;
	}

	public function sqlColorKey() {
		return ( true == isset( $this->m_strColorKey ) ) ? '\'' . addslashes( $this->m_strColorKey ) . '\'' : 'NULL';
	}

	public function setColorValue( $strColorValue ) {
		$this->set( 'm_strColorValue', CStrings::strTrimDef( $strColorValue, 7, NULL, true ) );
	}

	public function getColorValue() {
		return $this->m_strColorValue;
	}

	public function sqlColorValue() {
		return ( true == isset( $this->m_strColorValue ) ) ? '\'' . addslashes( $this->m_strColorValue ) . '\'' : '\'E6E6E6\'';
	}

	public function setIsPrivate( $boolIsPrivate ) {
		$this->set( 'm_boolIsPrivate', CStrings::strToBool( $boolIsPrivate ) );
	}

	public function getIsPrivate() {
		return $this->m_boolIsPrivate;
	}

	public function sqlIsPrivate() {
		return ( true == isset( $this->m_boolIsPrivate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrivate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, color_name, color_key, color_value, is_private, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlColorName() . ', ' .
						$this->sqlColorKey() . ', ' .
						$this->sqlColorValue() . ', ' .
						$this->sqlIsPrivate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color_name = ' . $this->sqlColorName(). ',' ; } elseif( true == array_key_exists( 'ColorName', $this->getChangedColumns() ) ) { $strSql .= ' color_name = ' . $this->sqlColorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color_key = ' . $this->sqlColorKey(). ',' ; } elseif( true == array_key_exists( 'ColorKey', $this->getChangedColumns() ) ) { $strSql .= ' color_key = ' . $this->sqlColorKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color_value = ' . $this->sqlColorValue(). ',' ; } elseif( true == array_key_exists( 'ColorValue', $this->getChangedColumns() ) ) { $strSql .= ' color_value = ' . $this->sqlColorValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate(). ',' ; } elseif( true == array_key_exists( 'IsPrivate', $this->getChangedColumns() ) ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'color_name' => $this->getColorName(),
			'color_key' => $this->getColorKey(),
			'color_value' => $this->getColorValue(),
			'is_private' => $this->getIsPrivate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>