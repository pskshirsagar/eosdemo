<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportFilters extends CEosPluralBase {

	/**
	 * @return CReportFilter[]
	 */
	public static function fetchReportFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportFilter::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportFilter
	 */
	public static function fetchReportFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportFilter::class, $objDatabase );
	}

	public static function fetchReportFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_filters', $objDatabase );
	}

	public static function fetchReportFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportFilter( sprintf( 'SELECT * FROM report_filters WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchReportFilters( sprintf( 'SELECT * FROM report_filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFiltersByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReportFilters( sprintf( 'SELECT * FROM report_filters WHERE report_id = %d AND cid = %d', ( int ) $intReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFiltersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchReportFilters( sprintf( 'SELECT * FROM report_filters WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>