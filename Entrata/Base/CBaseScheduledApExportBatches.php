<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledApExportBatches extends CEosPluralBase {

	/**
	 * @return CScheduledApExportBatch[]
	 */
	public static function fetchScheduledApExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScheduledApExportBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledApExportBatch
	 */
	public static function fetchScheduledApExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledApExportBatch::class, $objDatabase );
	}

	public static function fetchScheduledApExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_ap_export_batches', $objDatabase );
	}

	public static function fetchScheduledApExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledApExportBatch( sprintf( 'SELECT * FROM scheduled_ap_export_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledApExportBatches( sprintf( 'SELECT * FROM scheduled_ap_export_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApExportBatchesByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchScheduledApExportBatches( sprintf( 'SELECT * FROM scheduled_ap_export_batches WHERE company_transmission_vendor_id = %d AND cid = %d', ( int ) $intCompanyTransmissionVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApExportBatchesByApExportBatchTypeIdByCid( $intApExportBatchTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledApExportBatches( sprintf( 'SELECT * FROM scheduled_ap_export_batches WHERE ap_export_batch_type_id = %d AND cid = %d', ( int ) $intApExportBatchTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApExportBatchesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchScheduledApExportBatches( sprintf( 'SELECT * FROM scheduled_ap_export_batches WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>