<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteBlog extends CEosSingularBase {

	const TABLE_NAME = 'public.website_blogs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteDomainId;
	protected $m_intIsMigrated;
	protected $m_strInitHandle;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strPassPhrase;
	protected $m_strPreparedOn;
	protected $m_strInitializedOn;
	protected $m_strMigratedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMigrated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_domain_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteDomainId', trim( $arrValues['website_domain_id'] ) ); elseif( isset( $arrValues['website_domain_id'] ) ) $this->setWebsiteDomainId( $arrValues['website_domain_id'] );
		if( isset( $arrValues['is_migrated'] ) && $boolDirectSet ) $this->set( 'm_intIsMigrated', trim( $arrValues['is_migrated'] ) ); elseif( isset( $arrValues['is_migrated'] ) ) $this->setIsMigrated( $arrValues['is_migrated'] );
		if( isset( $arrValues['init_handle'] ) && $boolDirectSet ) $this->set( 'm_strInitHandle', trim( stripcslashes( $arrValues['init_handle'] ) ) ); elseif( isset( $arrValues['init_handle'] ) ) $this->setInitHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['init_handle'] ) : $arrValues['init_handle'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['pass_phrase'] ) && $boolDirectSet ) $this->set( 'm_strPassPhrase', trim( stripcslashes( $arrValues['pass_phrase'] ) ) ); elseif( isset( $arrValues['pass_phrase'] ) ) $this->setPassPhrase( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pass_phrase'] ) : $arrValues['pass_phrase'] );
		if( isset( $arrValues['prepared_on'] ) && $boolDirectSet ) $this->set( 'm_strPreparedOn', trim( $arrValues['prepared_on'] ) ); elseif( isset( $arrValues['prepared_on'] ) ) $this->setPreparedOn( $arrValues['prepared_on'] );
		if( isset( $arrValues['initialized_on'] ) && $boolDirectSet ) $this->set( 'm_strInitializedOn', trim( $arrValues['initialized_on'] ) ); elseif( isset( $arrValues['initialized_on'] ) ) $this->setInitializedOn( $arrValues['initialized_on'] );
		if( isset( $arrValues['migrated_on'] ) && $boolDirectSet ) $this->set( 'm_strMigratedOn', trim( $arrValues['migrated_on'] ) ); elseif( isset( $arrValues['migrated_on'] ) ) $this->setMigratedOn( $arrValues['migrated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteDomainId( $intWebsiteDomainId ) {
		$this->set( 'm_intWebsiteDomainId', CStrings::strToIntDef( $intWebsiteDomainId, NULL, false ) );
	}

	public function getWebsiteDomainId() {
		return $this->m_intWebsiteDomainId;
	}

	public function sqlWebsiteDomainId() {
		return ( true == isset( $this->m_intWebsiteDomainId ) ) ? ( string ) $this->m_intWebsiteDomainId : 'NULL';
	}

	public function setIsMigrated( $intIsMigrated ) {
		$this->set( 'm_intIsMigrated', CStrings::strToIntDef( $intIsMigrated, NULL, false ) );
	}

	public function getIsMigrated() {
		return $this->m_intIsMigrated;
	}

	public function sqlIsMigrated() {
		return ( true == isset( $this->m_intIsMigrated ) ) ? ( string ) $this->m_intIsMigrated : '0';
	}

	public function setInitHandle( $strInitHandle ) {
		$this->set( 'm_strInitHandle', CStrings::strTrimDef( $strInitHandle, 64, NULL, true ) );
	}

	public function getInitHandle() {
		return $this->m_strInitHandle;
	}

	public function sqlInitHandle() {
		return ( true == isset( $this->m_strInitHandle ) ) ? '\'' . addslashes( $this->m_strInitHandle ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setPassPhrase( $strPassPhrase ) {
		$this->set( 'm_strPassPhrase', CStrings::strTrimDef( $strPassPhrase, 240, NULL, true ) );
	}

	public function getPassPhrase() {
		return $this->m_strPassPhrase;
	}

	public function sqlPassPhrase() {
		return ( true == isset( $this->m_strPassPhrase ) ) ? '\'' . addslashes( $this->m_strPassPhrase ) . '\'' : 'NULL';
	}

	public function setPreparedOn( $strPreparedOn ) {
		$this->set( 'm_strPreparedOn', CStrings::strTrimDef( $strPreparedOn, -1, NULL, true ) );
	}

	public function getPreparedOn() {
		return $this->m_strPreparedOn;
	}

	public function sqlPreparedOn() {
		return ( true == isset( $this->m_strPreparedOn ) ) ? '\'' . $this->m_strPreparedOn . '\'' : 'NULL';
	}

	public function setInitializedOn( $strInitializedOn ) {
		$this->set( 'm_strInitializedOn', CStrings::strTrimDef( $strInitializedOn, -1, NULL, true ) );
	}

	public function getInitializedOn() {
		return $this->m_strInitializedOn;
	}

	public function sqlInitializedOn() {
		return ( true == isset( $this->m_strInitializedOn ) ) ? '\'' . $this->m_strInitializedOn . '\'' : 'NULL';
	}

	public function setMigratedOn( $strMigratedOn ) {
		$this->set( 'm_strMigratedOn', CStrings::strTrimDef( $strMigratedOn, -1, NULL, true ) );
	}

	public function getMigratedOn() {
		return $this->m_strMigratedOn;
	}

	public function sqlMigratedOn() {
		return ( true == isset( $this->m_strMigratedOn ) ) ? '\'' . $this->m_strMigratedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_domain_id, is_migrated, init_handle, username, password_encrypted, pass_phrase, prepared_on, initialized_on, migrated_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlWebsiteId() . ', ' .
			$this->sqlWebsiteDomainId() . ', ' .
			$this->sqlIsMigrated() . ', ' .
			$this->sqlInitHandle() . ', ' .
			$this->sqlUsername() . ', ' .
			$this->sqlPasswordEncrypted() . ', ' .
			$this->sqlPassPhrase() . ', ' .
			$this->sqlPreparedOn() . ', ' .
			$this->sqlInitializedOn() . ', ' .
			$this->sqlMigratedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_domain_id = ' . $this->sqlWebsiteDomainId() . ','; } elseif( true == array_key_exists( 'WebsiteDomainId', $this->getChangedColumns() ) ) { $strSql .= ' website_domain_id = ' . $this->sqlWebsiteDomainId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated() . ','; } elseif( true == array_key_exists( 'IsMigrated', $this->getChangedColumns() ) ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' init_handle = ' . $this->sqlInitHandle() . ','; } elseif( true == array_key_exists( 'InitHandle', $this->getChangedColumns() ) ) { $strSql .= ' init_handle = ' . $this->sqlInitHandle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pass_phrase = ' . $this->sqlPassPhrase() . ','; } elseif( true == array_key_exists( 'PassPhrase', $this->getChangedColumns() ) ) { $strSql .= ' pass_phrase = ' . $this->sqlPassPhrase() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prepared_on = ' . $this->sqlPreparedOn() . ','; } elseif( true == array_key_exists( 'PreparedOn', $this->getChangedColumns() ) ) { $strSql .= ' prepared_on = ' . $this->sqlPreparedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initialized_on = ' . $this->sqlInitializedOn() . ','; } elseif( true == array_key_exists( 'InitializedOn', $this->getChangedColumns() ) ) { $strSql .= ' initialized_on = ' . $this->sqlInitializedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migrated_on = ' . $this->sqlMigratedOn() . ','; } elseif( true == array_key_exists( 'MigratedOn', $this->getChangedColumns() ) ) { $strSql .= ' migrated_on = ' . $this->sqlMigratedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_domain_id' => $this->getWebsiteDomainId(),
			'is_migrated' => $this->getIsMigrated(),
			'init_handle' => $this->getInitHandle(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'pass_phrase' => $this->getPassPhrase(),
			'prepared_on' => $this->getPreparedOn(),
			'initialized_on' => $this->getInitializedOn(),
			'migrated_on' => $this->getMigratedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>