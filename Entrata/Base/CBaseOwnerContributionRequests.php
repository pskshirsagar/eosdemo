<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwnerContributionRequests extends CEosPluralBase {

    public static function fetchOwnerContributionRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'COwnerContributionRequest', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchOwnerContributionRequest( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'COwnerContributionRequest', $objDatabase );
    }

    public static function fetchOwnerContributionRequestCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'owner_contribution_requests', $objDatabase );
    }

    public static function fetchOwnerContributionRequestByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequest( sprintf( 'SELECT * FROM owner_contribution_requests WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchOwnerContributionRequestsByCid( $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequests( sprintf( 'SELECT * FROM owner_contribution_requests WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchOwnerContributionRequestsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequests( sprintf( 'SELECT * FROM owner_contribution_requests WHERE job_id = %d AND cid = %d', (int) $intJobId, (int) $intCid ), $objDatabase );
    }

    public static function fetchOwnerContributionRequestsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequests( sprintf( 'SELECT * FROM owner_contribution_requests WHERE job_phase_id = %d AND cid = %d', (int) $intJobPhaseId, (int) $intCid ), $objDatabase );
    }

    public static function fetchOwnerContributionRequestsByOwnerIdByCid( $intOwnerId, $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequests( sprintf( 'SELECT * FROM owner_contribution_requests WHERE owner_id = %d AND cid = %d', (int) $intOwnerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchOwnerContributionRequestsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
        return self::fetchOwnerContributionRequests( sprintf( 'SELECT * FROM owner_contribution_requests WHERE ar_transaction_id = %d AND cid = %d', (int) $intArTransactionId, (int) $intCid ), $objDatabase );
    }

}
?>