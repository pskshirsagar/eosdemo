<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryPlanPromoDetails
 * Do not add any new functions to this class.
 */

class CBaseAncillaryPlanPromoDetails extends CEosPluralBase {

	/**
	 * @return CAncillaryPlanPromoDetail[]
	 */
	public static function fetchAncillaryPlanPromoDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAncillaryPlanPromoDetail', $objDatabase );
	}

	/**
	 * @return CAncillaryPlanPromoDetail
	 */
	public static function fetchAncillaryPlanPromoDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAncillaryPlanPromoDetail', $objDatabase );
	}

	public static function fetchAncillaryPlanPromoDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_plan_promo_details', $objDatabase );
	}

	public static function fetchAncillaryPlanPromoDetailById( $intId, $objDatabase ) {
		return self::fetchAncillaryPlanPromoDetail( sprintf( 'SELECT * FROM ancillary_plan_promo_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAncillaryPlanPromoDetailsByAncillaryVendorId( $intAncillaryVendorId, $objDatabase ) {
		return self::fetchAncillaryPlanPromoDetails( sprintf( 'SELECT * FROM ancillary_plan_promo_details WHERE ancillary_vendor_id = %d', ( int ) $intAncillaryVendorId ), $objDatabase );
	}

	public static function fetchAncillaryPlanPromoDetailsByExternalPlanId( $strExternalPlanId, $objDatabase ) {
		return self::fetchAncillaryPlanPromoDetails( sprintf( 'SELECT * FROM ancillary_plan_promo_details WHERE external_plan_id = \'%s\'', $strExternalPlanId ), $objDatabase );
	}

}
?>