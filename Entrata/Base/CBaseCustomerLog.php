<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerLog extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intPriorCustomerLogId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intLeadSourceId;
	protected $m_intLeasingAgentId;
	protected $m_intMessageOperatorId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_intMaritalStatusTypeId;
	protected $m_intCompanyIdentificationTypeId;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intSecondaryPhoneNumberTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSecondaryNumber;
	protected $m_strCompanyName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strNameMaiden;
	protected $m_strNameSpouse;
	protected $m_strPrimaryStreetLine1;
	protected $m_strPrimaryStreetLine2;
	protected $m_strPrimaryStreetLine3;
	protected $m_strPrimaryCity;
	protected $m_strPrimaryStateCode;
	protected $m_strPrimaryPostalCode;
	protected $m_strPrimaryCountryCode;
	protected $m_boolPrimaryIsVerified;
	protected $m_strPhoneNumber;
	protected $m_strMobileNumber;
	protected $m_strWorkNumber;
	protected $m_strFaxNumber;
	protected $m_strEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTaxNumberMasked;
	protected $m_intReturnedPaymentsCount;
	protected $m_strBirthDate;
	protected $m_strGender;
	protected $m_strDlNumberEncrypted;
	protected $m_strDlStateCode;
	protected $m_strDlProvince;
	protected $m_strIdentificationValue;
	protected $m_strIdentificationExpiration;
	protected $m_strNotes;
	protected $m_intDontAllowLogin;
	protected $m_strImportedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strLogDatetime;
	protected $m_boolIsPostDateIgnored;
	protected $m_boolIsOpeningLog;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_intTaxIdTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intDontAllowLogin = '0';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_boolIsPostDateIgnored = false;
		$this->m_boolIsOpeningLog = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['prior_customer_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerLogId', trim( $arrValues['prior_customer_log_id'] ) ); elseif( isset( $arrValues['prior_customer_log_id'] ) ) $this->setPriorCustomerLogId( $arrValues['prior_customer_log_id'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['leasing_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingAgentId', trim( $arrValues['leasing_agent_id'] ) ); elseif( isset( $arrValues['leasing_agent_id'] ) ) $this->setLeasingAgentId( $arrValues['leasing_agent_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['payment_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAllowanceTypeId', trim( $arrValues['payment_allowance_type_id'] ) ); elseif( isset( $arrValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrValues['payment_allowance_type_id'] );
		if( isset( $arrValues['marital_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaritalStatusTypeId', trim( $arrValues['marital_status_type_id'] ) ); elseif( isset( $arrValues['marital_status_type_id'] ) ) $this->setMaritalStatusTypeId( $arrValues['marital_status_type_id'] );
		if( isset( $arrValues['company_identification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyIdentificationTypeId', trim( $arrValues['company_identification_type_id'] ) ); elseif( isset( $arrValues['company_identification_type_id'] ) ) $this->setCompanyIdentificationTypeId( $arrValues['company_identification_type_id'] );
		if( isset( $arrValues['primary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryPhoneNumberTypeId', trim( $arrValues['primary_phone_number_type_id'] ) ); elseif( isset( $arrValues['primary_phone_number_type_id'] ) ) $this->setPrimaryPhoneNumberTypeId( $arrValues['primary_phone_number_type_id'] );
		if( isset( $arrValues['secondary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryPhoneNumberTypeId', trim( $arrValues['secondary_phone_number_type_id'] ) ); elseif( isset( $arrValues['secondary_phone_number_type_id'] ) ) $this->setSecondaryPhoneNumberTypeId( $arrValues['secondary_phone_number_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( $arrValues['secondary_number'] ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrValues['secondary_number'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['name_spouse'] ) && $boolDirectSet ) $this->set( 'm_strNameSpouse', trim( $arrValues['name_spouse'] ) ); elseif( isset( $arrValues['name_spouse'] ) ) $this->setNameSpouse( $arrValues['name_spouse'] );
		if( isset( $arrValues['primary_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryStreetLine1', trim( $arrValues['primary_street_line1'] ) ); elseif( isset( $arrValues['primary_street_line1'] ) ) $this->setPrimaryStreetLine1( $arrValues['primary_street_line1'] );
		if( isset( $arrValues['primary_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryStreetLine2', trim( $arrValues['primary_street_line2'] ) ); elseif( isset( $arrValues['primary_street_line2'] ) ) $this->setPrimaryStreetLine2( $arrValues['primary_street_line2'] );
		if( isset( $arrValues['primary_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryStreetLine3', trim( $arrValues['primary_street_line3'] ) ); elseif( isset( $arrValues['primary_street_line3'] ) ) $this->setPrimaryStreetLine3( $arrValues['primary_street_line3'] );
		if( isset( $arrValues['primary_city'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryCity', trim( $arrValues['primary_city'] ) ); elseif( isset( $arrValues['primary_city'] ) ) $this->setPrimaryCity( $arrValues['primary_city'] );
		if( isset( $arrValues['primary_state_code'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryStateCode', trim( $arrValues['primary_state_code'] ) ); elseif( isset( $arrValues['primary_state_code'] ) ) $this->setPrimaryStateCode( $arrValues['primary_state_code'] );
		if( isset( $arrValues['primary_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryPostalCode', trim( $arrValues['primary_postal_code'] ) ); elseif( isset( $arrValues['primary_postal_code'] ) ) $this->setPrimaryPostalCode( $arrValues['primary_postal_code'] );
		if( isset( $arrValues['primary_country_code'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryCountryCode', trim( $arrValues['primary_country_code'] ) ); elseif( isset( $arrValues['primary_country_code'] ) ) $this->setPrimaryCountryCode( $arrValues['primary_country_code'] );
		if( isset( $arrValues['primary_is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolPrimaryIsVerified', trim( stripcslashes( $arrValues['primary_is_verified'] ) ) ); elseif( isset( $arrValues['primary_is_verified'] ) ) $this->setPrimaryIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_is_verified'] ) : $arrValues['primary_is_verified'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( $arrValues['mobile_number'] ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( $arrValues['mobile_number'] );
		if( isset( $arrValues['work_number'] ) && $boolDirectSet ) $this->set( 'm_strWorkNumber', trim( $arrValues['work_number'] ) ); elseif( isset( $arrValues['work_number'] ) ) $this->setWorkNumber( $arrValues['work_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberMasked', trim( $arrValues['tax_number_masked'] ) ); elseif( isset( $arrValues['tax_number_masked'] ) ) $this->setTaxNumberMasked( $arrValues['tax_number_masked'] );
		if( isset( $arrValues['returned_payments_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedPaymentsCount', trim( $arrValues['returned_payments_count'] ) ); elseif( isset( $arrValues['returned_payments_count'] ) ) $this->setReturnedPaymentsCount( $arrValues['returned_payments_count'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['dl_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDlNumberEncrypted', trim( $arrValues['dl_number_encrypted'] ) ); elseif( isset( $arrValues['dl_number_encrypted'] ) ) $this->setDlNumberEncrypted( $arrValues['dl_number_encrypted'] );
		if( isset( $arrValues['dl_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDlStateCode', trim( $arrValues['dl_state_code'] ) ); elseif( isset( $arrValues['dl_state_code'] ) ) $this->setDlStateCode( $arrValues['dl_state_code'] );
		if( isset( $arrValues['dl_province'] ) && $boolDirectSet ) $this->set( 'm_strDlProvince', trim( $arrValues['dl_province'] ) ); elseif( isset( $arrValues['dl_province'] ) ) $this->setDlProvince( $arrValues['dl_province'] );
		if( isset( $arrValues['identification_value'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationValue', trim( $arrValues['identification_value'] ) ); elseif( isset( $arrValues['identification_value'] ) ) $this->setIdentificationValue( $arrValues['identification_value'] );
		if( isset( $arrValues['identification_expiration'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationExpiration', trim( $arrValues['identification_expiration'] ) ); elseif( isset( $arrValues['identification_expiration'] ) ) $this->setIdentificationExpiration( $arrValues['identification_expiration'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostDateIgnored', trim( stripcslashes( $arrValues['is_post_date_ignored'] ) ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_date_ignored'] ) : $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_boolIsOpeningLog', trim( stripcslashes( $arrValues['is_opening_log'] ) ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_opening_log'] ) : $arrValues['is_opening_log'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['tax_id_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxIdTypeId', trim( $arrValues['tax_id_type_id'] ) ); elseif( isset( $arrValues['tax_id_type_id'] ) ) $this->setTaxIdTypeId( $arrValues['tax_id_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPriorCustomerLogId( $intPriorCustomerLogId ) {
		$this->set( 'm_intPriorCustomerLogId', CStrings::strToIntDef( $intPriorCustomerLogId, NULL, false ) );
	}

	public function getPriorCustomerLogId() {
		return $this->m_intPriorCustomerLogId;
	}

	public function sqlPriorCustomerLogId() {
		return ( true == isset( $this->m_intPriorCustomerLogId ) ) ? ( string ) $this->m_intPriorCustomerLogId : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->set( 'm_intLeasingAgentId', CStrings::strToIntDef( $intLeasingAgentId, NULL, false ) );
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function sqlLeasingAgentId() {
		return ( true == isset( $this->m_intLeasingAgentId ) ) ? ( string ) $this->m_intLeasingAgentId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
		$this->set( 'm_intPaymentAllowanceTypeId', CStrings::strToIntDef( $intPaymentAllowanceTypeId, NULL, false ) );
	}

	public function getPaymentAllowanceTypeId() {
		return $this->m_intPaymentAllowanceTypeId;
	}

	public function sqlPaymentAllowanceTypeId() {
		return ( true == isset( $this->m_intPaymentAllowanceTypeId ) ) ? ( string ) $this->m_intPaymentAllowanceTypeId : 'NULL';
	}

	public function setMaritalStatusTypeId( $intMaritalStatusTypeId ) {
		$this->set( 'm_intMaritalStatusTypeId', CStrings::strToIntDef( $intMaritalStatusTypeId, NULL, false ) );
	}

	public function getMaritalStatusTypeId() {
		return $this->m_intMaritalStatusTypeId;
	}

	public function sqlMaritalStatusTypeId() {
		return ( true == isset( $this->m_intMaritalStatusTypeId ) ) ? ( string ) $this->m_intMaritalStatusTypeId : 'NULL';
	}

	public function setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId ) {
		$this->set( 'm_intCompanyIdentificationTypeId', CStrings::strToIntDef( $intCompanyIdentificationTypeId, NULL, false ) );
	}

	public function getCompanyIdentificationTypeId() {
		return $this->m_intCompanyIdentificationTypeId;
	}

	public function sqlCompanyIdentificationTypeId() {
		return ( true == isset( $this->m_intCompanyIdentificationTypeId ) ) ? ( string ) $this->m_intCompanyIdentificationTypeId : 'NULL';
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->set( 'm_intPrimaryPhoneNumberTypeId', CStrings::strToIntDef( $intPrimaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getPrimaryPhoneNumberTypeId() {
		return $this->m_intPrimaryPhoneNumberTypeId;
	}

	public function sqlPrimaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPrimaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intPrimaryPhoneNumberTypeId : 'NULL';
	}

	public function setSecondaryPhoneNumberTypeId( $intSecondaryPhoneNumberTypeId ) {
		$this->set( 'm_intSecondaryPhoneNumberTypeId', CStrings::strToIntDef( $intSecondaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getSecondaryPhoneNumberTypeId() {
		return $this->m_intSecondaryPhoneNumberTypeId;
	}

	public function sqlSecondaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intSecondaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intSecondaryPhoneNumberTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 50, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryNumber ) : '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 1000, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setNameSpouse( $strNameSpouse ) {
		$this->set( 'm_strNameSpouse', CStrings::strTrimDef( $strNameSpouse, 1000, NULL, true ) );
	}

	public function getNameSpouse() {
		return $this->m_strNameSpouse;
	}

	public function sqlNameSpouse() {
		return ( true == isset( $this->m_strNameSpouse ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSpouse ) : '\'' . addslashes( $this->m_strNameSpouse ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine1( $strPrimaryStreetLine1 ) {
		$this->set( 'm_strPrimaryStreetLine1', CStrings::strTrimDef( $strPrimaryStreetLine1, 100, NULL, true ) );
	}

	public function getPrimaryStreetLine1() {
		return $this->m_strPrimaryStreetLine1;
	}

	public function sqlPrimaryStreetLine1() {
		return ( true == isset( $this->m_strPrimaryStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine1 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine2( $strPrimaryStreetLine2 ) {
		$this->set( 'm_strPrimaryStreetLine2', CStrings::strTrimDef( $strPrimaryStreetLine2, 100, NULL, true ) );
	}

	public function getPrimaryStreetLine2() {
		return $this->m_strPrimaryStreetLine2;
	}

	public function sqlPrimaryStreetLine2() {
		return ( true == isset( $this->m_strPrimaryStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine2 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStreetLine3( $strPrimaryStreetLine3 ) {
		$this->set( 'm_strPrimaryStreetLine3', CStrings::strTrimDef( $strPrimaryStreetLine3, 100, NULL, true ) );
	}

	public function getPrimaryStreetLine3() {
		return $this->m_strPrimaryStreetLine3;
	}

	public function sqlPrimaryStreetLine3() {
		return ( true == isset( $this->m_strPrimaryStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStreetLine3 ) : '\'' . addslashes( $this->m_strPrimaryStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setPrimaryCity( $strPrimaryCity ) {
		$this->set( 'm_strPrimaryCity', CStrings::strTrimDef( $strPrimaryCity, 50, NULL, true ) );
	}

	public function getPrimaryCity() {
		return $this->m_strPrimaryCity;
	}

	public function sqlPrimaryCity() {
		return ( true == isset( $this->m_strPrimaryCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryCity ) : '\'' . addslashes( $this->m_strPrimaryCity ) . '\'' ) : 'NULL';
	}

	public function setPrimaryStateCode( $strPrimaryStateCode ) {
		$this->set( 'm_strPrimaryStateCode', CStrings::strTrimDef( $strPrimaryStateCode, 250, NULL, true ) );
	}

	public function getPrimaryStateCode() {
		return $this->m_strPrimaryStateCode;
	}

	public function sqlPrimaryStateCode() {
		return ( true == isset( $this->m_strPrimaryStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryStateCode ) : '\'' . addslashes( $this->m_strPrimaryStateCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryPostalCode( $strPrimaryPostalCode ) {
		$this->set( 'm_strPrimaryPostalCode', CStrings::strTrimDef( $strPrimaryPostalCode, 20, NULL, true ) );
	}

	public function getPrimaryPostalCode() {
		return $this->m_strPrimaryPostalCode;
	}

	public function sqlPrimaryPostalCode() {
		return ( true == isset( $this->m_strPrimaryPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryPostalCode ) : '\'' . addslashes( $this->m_strPrimaryPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryCountryCode( $strPrimaryCountryCode ) {
		$this->set( 'm_strPrimaryCountryCode', CStrings::strTrimDef( $strPrimaryCountryCode, 2, NULL, true ) );
	}

	public function getPrimaryCountryCode() {
		return $this->m_strPrimaryCountryCode;
	}

	public function sqlPrimaryCountryCode() {
		return ( true == isset( $this->m_strPrimaryCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryCountryCode ) : '\'' . addslashes( $this->m_strPrimaryCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryIsVerified( $boolPrimaryIsVerified ) {
		$this->set( 'm_boolPrimaryIsVerified', CStrings::strToBool( $boolPrimaryIsVerified ) );
	}

	public function getPrimaryIsVerified() {
		return $this->m_boolPrimaryIsVerified;
	}

	public function sqlPrimaryIsVerified() {
		return ( true == isset( $this->m_boolPrimaryIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolPrimaryIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMobileNumber ) : '\'' . addslashes( $this->m_strMobileNumber ) . '\'' ) : 'NULL';
	}

	public function setWorkNumber( $strWorkNumber ) {
		$this->set( 'm_strWorkNumber', CStrings::strTrimDef( $strWorkNumber, 30, NULL, true ) );
	}

	public function getWorkNumber() {
		return $this->m_strWorkNumber;
	}

	public function sqlWorkNumber() {
		return ( true == isset( $this->m_strWorkNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorkNumber ) : '\'' . addslashes( $this->m_strWorkNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->set( 'm_strTaxNumberMasked', CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true ) );
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function sqlTaxNumberMasked() {
		return ( true == isset( $this->m_strTaxNumberMasked ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberMasked ) : '\'' . addslashes( $this->m_strTaxNumberMasked ) . '\'' ) : 'NULL';
	}

	public function setReturnedPaymentsCount( $intReturnedPaymentsCount ) {
		$this->set( 'm_intReturnedPaymentsCount', CStrings::strToIntDef( $intReturnedPaymentsCount, NULL, false ) );
	}

	public function getReturnedPaymentsCount() {
		return $this->m_intReturnedPaymentsCount;
	}

	public function sqlReturnedPaymentsCount() {
		return ( true == isset( $this->m_intReturnedPaymentsCount ) ) ? ( string ) $this->m_intReturnedPaymentsCount : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setDlNumberEncrypted( $strDlNumberEncrypted ) {
		$this->set( 'm_strDlNumberEncrypted', CStrings::strTrimDef( $strDlNumberEncrypted, 240, NULL, true ) );
	}

	public function getDlNumberEncrypted() {
		return $this->m_strDlNumberEncrypted;
	}

	public function sqlDlNumberEncrypted() {
		return ( true == isset( $this->m_strDlNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlNumberEncrypted ) : '\'' . addslashes( $this->m_strDlNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDlStateCode( $strDlStateCode ) {
		$this->set( 'm_strDlStateCode', CStrings::strTrimDef( $strDlStateCode, 2, NULL, true ) );
	}

	public function getDlStateCode() {
		return $this->m_strDlStateCode;
	}

	public function sqlDlStateCode() {
		return ( true == isset( $this->m_strDlStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlStateCode ) : '\'' . addslashes( $this->m_strDlStateCode ) . '\'' ) : 'NULL';
	}

	public function setDlProvince( $strDlProvince ) {
		$this->set( 'm_strDlProvince', CStrings::strTrimDef( $strDlProvince, 50, NULL, true ) );
	}

	public function getDlProvince() {
		return $this->m_strDlProvince;
	}

	public function sqlDlProvince() {
		return ( true == isset( $this->m_strDlProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlProvince ) : '\'' . addslashes( $this->m_strDlProvince ) . '\'' ) : 'NULL';
	}

	public function setIdentificationValue( $strIdentificationValue ) {
		$this->set( 'm_strIdentificationValue', CStrings::strTrimDef( $strIdentificationValue, 240, NULL, true ) );
	}

	public function getIdentificationValue() {
		return $this->m_strIdentificationValue;
	}

	public function sqlIdentificationValue() {
		return ( true == isset( $this->m_strIdentificationValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationValue ) : '\'' . addslashes( $this->m_strIdentificationValue ) . '\'' ) : 'NULL';
	}

	public function setIdentificationExpiration( $strIdentificationExpiration ) {
		$this->set( 'm_strIdentificationExpiration', CStrings::strTrimDef( $strIdentificationExpiration, -1, NULL, true ) );
	}

	public function getIdentificationExpiration() {
		return $this->m_strIdentificationExpiration;
	}

	public function sqlIdentificationExpiration() {
		return ( true == isset( $this->m_strIdentificationExpiration ) ) ? '\'' . $this->m_strIdentificationExpiration . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NULL';
	}

	public function setIsPostDateIgnored( $boolIsPostDateIgnored ) {
		$this->set( 'm_boolIsPostDateIgnored', CStrings::strToBool( $boolIsPostDateIgnored ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_boolIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_boolIsPostDateIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostDateIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOpeningLog( $boolIsOpeningLog ) {
		$this->set( 'm_boolIsOpeningLog', CStrings::strToBool( $boolIsOpeningLog ) );
	}

	public function getIsOpeningLog() {
		return $this->m_boolIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_boolIsOpeningLog ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOpeningLog ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setTaxIdTypeId( $intTaxIdTypeId ) {
		$this->set( 'm_intTaxIdTypeId', CStrings::strToIntDef( $intTaxIdTypeId, NULL, false ) );
	}

	public function getTaxIdTypeId() {
		return $this->m_intTaxIdTypeId;
	}

	public function sqlTaxIdTypeId() {
		return ( true == isset( $this->m_intTaxIdTypeId ) ) ? ( string ) $this->m_intTaxIdTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, prior_customer_log_id, integration_database_id, lead_source_id, leasing_agent_id, message_operator_id, payment_allowance_type_id, marital_status_type_id, company_identification_type_id, primary_phone_number_type_id, secondary_phone_number_type_id, remote_primary_key, secondary_number, company_name, name_prefix, name_first, name_middle, name_last, name_suffix, name_maiden, name_spouse, primary_street_line1, primary_street_line2, primary_street_line3, primary_city, primary_state_code, primary_postal_code, primary_country_code, primary_is_verified, phone_number, mobile_number, work_number, fax_number, email_address, tax_number_encrypted, tax_number_masked, returned_payments_count, birth_date, gender, dl_number_encrypted, dl_state_code, dl_province, identification_value, identification_expiration, notes, dont_allow_login, imported_on, exported_by, exported_on, post_date, apply_through_post_date, log_datetime, is_post_date_ignored, is_opening_log, updated_by, updated_on, created_on, created_by, tax_id_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlPriorCustomerLogId() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlLeasingAgentId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlMaritalStatusTypeId() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlNameSpouse() . ', ' .
						$this->sqlPrimaryStreetLine1() . ', ' .
						$this->sqlPrimaryStreetLine2() . ', ' .
						$this->sqlPrimaryStreetLine3() . ', ' .
						$this->sqlPrimaryCity() . ', ' .
						$this->sqlPrimaryStateCode() . ', ' .
						$this->sqlPrimaryPostalCode() . ', ' .
						$this->sqlPrimaryCountryCode() . ', ' .
						$this->sqlPrimaryIsVerified() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlReturnedPaymentsCount() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlDlNumberEncrypted() . ', ' .
						$this->sqlDlStateCode() . ', ' .
						$this->sqlDlProvince() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlDontAllowLogin() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlTaxIdTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_log_id = ' . $this->sqlPriorCustomerLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCustomerLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_log_id = ' . $this->sqlPriorCustomerLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId(). ',' ; } elseif( true == array_key_exists( 'LeasingAgentId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId(). ',' ; } elseif( true == array_key_exists( 'MessageOperatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marital_status_type_id = ' . $this->sqlMaritalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MaritalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marital_status_type_id = ' . $this->sqlMaritalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyIdentificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number_type_id = ' . $this->sqlPrimaryPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'PrimaryPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number_type_id = ' . $this->sqlPrimaryPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_phone_number_type_id = ' . $this->sqlSecondaryPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'SecondaryPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' secondary_phone_number_type_id = ' . $this->sqlSecondaryPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber(). ',' ; } elseif( true == array_key_exists( 'SecondaryNumber', $this->getChangedColumns() ) ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden(). ',' ; } elseif( true == array_key_exists( 'NameMaiden', $this->getChangedColumns() ) ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_spouse = ' . $this->sqlNameSpouse(). ',' ; } elseif( true == array_key_exists( 'NameSpouse', $this->getChangedColumns() ) ) { $strSql .= ' name_spouse = ' . $this->sqlNameSpouse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_street_line1 = ' . $this->sqlPrimaryStreetLine1(). ',' ; } elseif( true == array_key_exists( 'PrimaryStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' primary_street_line1 = ' . $this->sqlPrimaryStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_street_line2 = ' . $this->sqlPrimaryStreetLine2(). ',' ; } elseif( true == array_key_exists( 'PrimaryStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' primary_street_line2 = ' . $this->sqlPrimaryStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_street_line3 = ' . $this->sqlPrimaryStreetLine3(). ',' ; } elseif( true == array_key_exists( 'PrimaryStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' primary_street_line3 = ' . $this->sqlPrimaryStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_city = ' . $this->sqlPrimaryCity(). ',' ; } elseif( true == array_key_exists( 'PrimaryCity', $this->getChangedColumns() ) ) { $strSql .= ' primary_city = ' . $this->sqlPrimaryCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_state_code = ' . $this->sqlPrimaryStateCode(). ',' ; } elseif( true == array_key_exists( 'PrimaryStateCode', $this->getChangedColumns() ) ) { $strSql .= ' primary_state_code = ' . $this->sqlPrimaryStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_postal_code = ' . $this->sqlPrimaryPostalCode(). ',' ; } elseif( true == array_key_exists( 'PrimaryPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' primary_postal_code = ' . $this->sqlPrimaryPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_country_code = ' . $this->sqlPrimaryCountryCode(). ',' ; } elseif( true == array_key_exists( 'PrimaryCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' primary_country_code = ' . $this->sqlPrimaryCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_is_verified = ' . $this->sqlPrimaryIsVerified(). ',' ; } elseif( true == array_key_exists( 'PrimaryIsVerified', $this->getChangedColumns() ) ) { $strSql .= ' primary_is_verified = ' . $this->sqlPrimaryIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber(). ',' ; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber(). ',' ; } elseif( true == array_key_exists( 'WorkNumber', $this->getChangedColumns() ) ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked(). ',' ; } elseif( true == array_key_exists( 'TaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_payments_count = ' . $this->sqlReturnedPaymentsCount(). ',' ; } elseif( true == array_key_exists( 'ReturnedPaymentsCount', $this->getChangedColumns() ) ) { $strSql .= ' returned_payments_count = ' . $this->sqlReturnedPaymentsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'DlNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode(). ',' ; } elseif( true == array_key_exists( 'DlStateCode', $this->getChangedColumns() ) ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_province = ' . $this->sqlDlProvince(). ',' ; } elseif( true == array_key_exists( 'DlProvince', $this->getChangedColumns() ) ) { $strSql .= ' dl_province = ' . $this->sqlDlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue(). ',' ; } elseif( true == array_key_exists( 'IdentificationValue', $this->getChangedColumns() ) ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration(). ',' ; } elseif( true == array_key_exists( 'IdentificationExpiration', $this->getChangedColumns() ) ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin(). ',' ; } elseif( true == array_key_exists( 'DontAllowLogin', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_id_type_id = ' . $this->sqlTaxIdTypeId(). ',' ; } elseif( true == array_key_exists( 'TaxIdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_id_type_id = ' . $this->sqlTaxIdTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'prior_customer_log_id' => $this->getPriorCustomerLogId(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'leasing_agent_id' => $this->getLeasingAgentId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'payment_allowance_type_id' => $this->getPaymentAllowanceTypeId(),
			'marital_status_type_id' => $this->getMaritalStatusTypeId(),
			'company_identification_type_id' => $this->getCompanyIdentificationTypeId(),
			'primary_phone_number_type_id' => $this->getPrimaryPhoneNumberTypeId(),
			'secondary_phone_number_type_id' => $this->getSecondaryPhoneNumberTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'secondary_number' => $this->getSecondaryNumber(),
			'company_name' => $this->getCompanyName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'name_maiden' => $this->getNameMaiden(),
			'name_spouse' => $this->getNameSpouse(),
			'primary_street_line1' => $this->getPrimaryStreetLine1(),
			'primary_street_line2' => $this->getPrimaryStreetLine2(),
			'primary_street_line3' => $this->getPrimaryStreetLine3(),
			'primary_city' => $this->getPrimaryCity(),
			'primary_state_code' => $this->getPrimaryStateCode(),
			'primary_postal_code' => $this->getPrimaryPostalCode(),
			'primary_country_code' => $this->getPrimaryCountryCode(),
			'primary_is_verified' => $this->getPrimaryIsVerified(),
			'phone_number' => $this->getPhoneNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'work_number' => $this->getWorkNumber(),
			'fax_number' => $this->getFaxNumber(),
			'email_address' => $this->getEmailAddress(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'tax_number_masked' => $this->getTaxNumberMasked(),
			'returned_payments_count' => $this->getReturnedPaymentsCount(),
			'birth_date' => $this->getBirthDate(),
			'gender' => $this->getGender(),
			'dl_number_encrypted' => $this->getDlNumberEncrypted(),
			'dl_state_code' => $this->getDlStateCode(),
			'dl_province' => $this->getDlProvince(),
			'identification_value' => $this->getIdentificationValue(),
			'identification_expiration' => $this->getIdentificationExpiration(),
			'notes' => $this->getNotes(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'imported_on' => $this->getImportedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'tax_id_type_id' => $this->getTaxIdTypeId()
		);
	}

}
?>