<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueArCodes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueArCodes extends CEosPluralBase {

	/**
	 * @return CRevenueArCode[]
	 */
	public static function fetchRevenueArCodes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueArCode', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueArCode
	 */
	public static function fetchRevenueArCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueArCode', $objDatabase );
	}

	public static function fetchRevenueArCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_ar_codes', $objDatabase );
	}

	public static function fetchRevenueArCodeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueArCode( sprintf( 'SELECT * FROM revenue_ar_codes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueArCodesByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueArCodes( sprintf( 'SELECT * FROM revenue_ar_codes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueArCodesByRevenueArCodeTypeIdByCid( $intRevenueArCodeTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueArCodes( sprintf( 'SELECT * FROM revenue_ar_codes WHERE revenue_ar_code_type_id = %d AND cid = %d', ( int ) $intRevenueArCodeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueArCodesByRevenueScheduledChargeTypeIdByCid( $intRevenueScheduledChargeTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueArCodes( sprintf( 'SELECT * FROM revenue_ar_codes WHERE revenue_scheduled_charge_type_id = %d AND cid = %d', ( int ) $intRevenueScheduledChargeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueArCodesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchRevenueArCodes( sprintf( 'SELECT * FROM revenue_ar_codes WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>