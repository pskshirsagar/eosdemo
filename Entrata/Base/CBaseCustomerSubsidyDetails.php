<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidyDetails extends CEosPluralBase {

	/**
	 * @return CCustomerSubsidyDetail[]
	 */
	public static function fetchCustomerSubsidyDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerSubsidyDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSubsidyDetail
	 */
	public static function fetchCustomerSubsidyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerSubsidyDetail::class, $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_subsidy_details', $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetail( sprintf( 'SELECT * FROM customer_subsidy_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsBySubsidyCitizenshipTypeIdByCid( $intSubsidyCitizenshipTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE subsidy_citizenship_type_id = %d AND cid = %d', $intSubsidyCitizenshipTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsBySubsidyEthnicityTypeIdByCid( $intSubsidyEthnicityTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE subsidy_ethnicity_type_id = %d AND cid = %d', $intSubsidyEthnicityTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsBySubsidyEthnicitySubTypeIdByCid( $intSubsidyEthnicitySubTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE subsidy_ethnicity_sub_type_id = %d AND cid = %d', $intSubsidyEthnicitySubTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsBySubsidySsnExceptionTypeIdByCid( $intSubsidySsnExceptionTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE subsidy_ssn_exception_type_id = %d AND cid = %d', $intSubsidySsnExceptionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailsBySubsidyDependentTypeIdByCid( $intSubsidyDependentTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidyDetails( sprintf( 'SELECT * FROM customer_subsidy_details WHERE subsidy_dependent_type_id = %d AND cid = %d', $intSubsidyDependentTypeId, $intCid ), $objDatabase );
	}

}
?>