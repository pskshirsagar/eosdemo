<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyContractUnitSpaces
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyContractUnitSpaces extends CEosPluralBase {

	/**
	 * @return CSubsidyContractUnitSpace[]
	 */
	public static function fetchSubsidyContractUnitSpaces( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyContractUnitSpace', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyContractUnitSpace
	 */
	public static function fetchSubsidyContractUnitSpace( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyContractUnitSpace', $objDatabase );
	}

	public static function fetchSubsidyContractUnitSpaceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_contract_unit_spaces', $objDatabase );
	}

	public static function fetchSubsidyContractUnitSpaceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContractUnitSpace( sprintf( 'SELECT * FROM subsidy_contract_unit_spaces WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractUnitSpacesByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyContractUnitSpaces( sprintf( 'SELECT * FROM subsidy_contract_unit_spaces WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractUnitSpacesBySubsidyContractIdByCid( $intSubsidyContractId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContractUnitSpaces( sprintf( 'SELECT * FROM subsidy_contract_unit_spaces WHERE subsidy_contract_id = %d AND cid = %d', ( int ) $intSubsidyContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyContractUnitSpacesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchSubsidyContractUnitSpaces( sprintf( 'SELECT * FROM subsidy_contract_unit_spaces WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>