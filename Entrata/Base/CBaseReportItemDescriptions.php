<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportItemDescriptions
 * Do not add any new functions to this class.
 */

class CBaseReportItemDescriptions extends CEosPluralBase {

	/**
	 * @return CReportItemDescription[]
	 */
	public static function fetchReportItemDescriptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportItemDescription::class, $objDatabase );
	}

	/**
	 * @return CReportItemDescription
	 */
	public static function fetchReportItemDescription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportItemDescription::class, $objDatabase );
	}

	public static function fetchReportItemDescriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_item_descriptions', $objDatabase );
	}

	public static function fetchReportItemDescriptionById( $intId, $objDatabase ) {
		return self::fetchReportItemDescription( sprintf( 'SELECT * FROM report_item_descriptions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchReportItemDescriptionsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchReportItemDescriptions( sprintf( 'SELECT * FROM report_item_descriptions WHERE default_cid = %d', $intDefaultCid ), $objDatabase );
	}

	public static function fetchReportItemDescriptionsByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		return self::fetchReportItemDescriptions( sprintf( 'SELECT * FROM report_item_descriptions WHERE default_report_version_id = %d', $intDefaultReportVersionId ), $objDatabase );
	}

	public static function fetchReportItemDescriptionsByReportItemTypeId( $intReportItemTypeId, $objDatabase ) {
		return self::fetchReportItemDescriptions( sprintf( 'SELECT * FROM report_item_descriptions WHERE report_item_type_id = %d', $intReportItemTypeId ), $objDatabase );
	}

}
?>