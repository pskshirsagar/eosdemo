<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportFilterDescriptions
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportFilterDescriptions extends CEosPluralBase {

	/**
	 * @return CDefaultReportFilterDescription[]
	 */
	public static function fetchDefaultReportFilterDescriptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultReportFilterDescription::class, $objDatabase );
	}

	/**
	 * @return CDefaultReportFilterDescription
	 */
	public static function fetchDefaultReportFilterDescription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultReportFilterDescription::class, $objDatabase );
	}

	public static function fetchDefaultReportFilterDescriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_filter_descriptions', $objDatabase );
	}

	public static function fetchDefaultReportFilterDescriptionById( $intId, $objDatabase ) {
		return self::fetchDefaultReportFilterDescription( sprintf( 'SELECT * FROM default_report_filter_descriptions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultReportFilterDescriptionsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchDefaultReportFilterDescriptions( sprintf( 'SELECT * FROM default_report_filter_descriptions WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

}
?>