<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduledPaymentTypes extends CEosPluralBase {

	/**
	 * @return CScheduledPaymentType[]
	 */
	public static function fetchScheduledPaymentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledPaymentType::class, $objDatabase );
	}

	/**
	 * @return CScheduledPaymentType
	 */
	public static function fetchScheduledPaymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledPaymentType::class, $objDatabase );
	}

	public static function fetchScheduledPaymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_payment_types', $objDatabase );
	}

	public static function fetchScheduledPaymentTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledPaymentType( sprintf( 'SELECT * FROM scheduled_payment_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>