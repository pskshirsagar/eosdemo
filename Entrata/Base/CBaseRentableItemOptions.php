<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base class again, please do so by checking the composite key checkbox. ***
 */

class CBaseRentableItemOptions extends CEosPluralBase {

    const TABLE_RENTABLE_ITEM_OPTIONS = 'public.rentable_item_options';

    public static function fetchRentableItemOptions( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRentableItemOption', $objDatabase );
    }

    public static function fetchRentableItemOption( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRentableItemOption', $objDatabase );
    }

    public static function fetchRentableItemOptionCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'rentable_item_options', $objDatabase );
    }

    public static function fetchRentableItemOptionByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOption( sprintf( 'SELECT * FROM rentable_item_options WHERE id = %d AND cid = %d ', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionsByCid( $intCid, $objDatabase ) {
        return self::fetchRentableItemOptions( sprintf( 'SELECT * FROM rentable_item_options WHERE cid = %d ', (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionsByPropertyRentableItemTypeIdByCid( $intPropertyRentableItemOptionTypeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOptions( sprintf( 'SELECT * FROM rentable_item_options WHERE property_rentable_item_option_type_id = %d AND cid = %d', (int) $intPropertyRentableItemOptionTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOptions( sprintf( 'SELECT * FROM rentable_item_options WHERE company_media_file_id = %d AND cid = %d', (int) $intCompanyMediaFileId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemOptionsBySitePlanCompanyMediaFileIdByCid( $intSitePlanCompanyMediaFileId, $intCid, $objDatabase ) {
        return self::fetchRentableItemOptions( sprintf( 'SELECT * FROM rentable_item_options WHERE site_plan_company_media_file_id = %d AND cid = %d', (int) $intSitePlanCompanyMediaFileId, (int) $intCid ), $objDatabase );
    }
}
?>