<?php

class CBaseDefaultArCode extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_ar_codes';

	protected $m_intId;
	protected $m_intModeTypeId;
	protected $m_intArCodeTypeId;
	protected $m_intArOriginId;
	protected $m_intArTriggerTypeId;
	protected $m_intArTriggerId;
	protected $m_intGlGroupTypeId;
	protected $m_intDefaultDebitGlAccountId;
	protected $m_intDefaultCreditGlAccountId;
	protected $m_intModifiedDebitGlAccountId;
	protected $m_intWriteOffArCodeId;
	protected $m_intDefaultLedgerFilterId;
	protected $m_intRoundTypeId;
	protected $m_intRecoveryArCodeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltDefaultAmount;
	protected $m_intCaptureDelayDays;
	protected $m_boolHideCharges;
	protected $m_boolHideCredits;
	protected $m_boolWaiveLateFees;
	protected $m_boolPostToCash;
	protected $m_boolShowMoveOutReminder;
	protected $m_boolProrateCharges;
	protected $m_boolIsPaymentInKind;
	protected $m_boolIsAutoAdded;
	protected $m_boolIsReserved;
	protected $m_boolIsSystem;
	protected $m_intOrderNum;
	protected $m_intPriorityNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIncludeInEffectiveRent;
	protected $m_intGlAccountTypeId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intModeTypeId = '1';
		$this->m_fltDefaultAmount = '0';
		$this->m_boolHideCharges = false;
		$this->m_boolHideCredits = false;
		$this->m_boolWaiveLateFees = false;
		$this->m_boolPostToCash = false;
		$this->m_boolShowMoveOutReminder = false;
		$this->m_boolIsPaymentInKind = false;
		$this->m_boolIsAutoAdded = false;
		$this->m_boolIsReserved = false;
		$this->m_boolIsSystem = false;
		$this->m_intOrderNum = '0';
		$this->m_intPriorityNum = '0';
		$this->m_boolIncludeInEffectiveRent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['mode_type_id'] ) && $boolDirectSet ) $this->set( 'm_intModeTypeId', trim( $arrValues['mode_type_id'] ) ); elseif( isset( $arrValues['mode_type_id'] ) ) $this->setModeTypeId( $arrValues['mode_type_id'] );
		if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_trigger_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerTypeId', trim( $arrValues['ar_trigger_type_id'] ) ); elseif( isset( $arrValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrValues['ar_trigger_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['default_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultDebitGlAccountId', trim( $arrValues['default_debit_gl_account_id'] ) ); elseif( isset( $arrValues['default_debit_gl_account_id'] ) ) $this->setDefaultDebitGlAccountId( $arrValues['default_debit_gl_account_id'] );
		if( isset( $arrValues['default_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCreditGlAccountId', trim( $arrValues['default_credit_gl_account_id'] ) ); elseif( isset( $arrValues['default_credit_gl_account_id'] ) ) $this->setDefaultCreditGlAccountId( $arrValues['default_credit_gl_account_id'] );
		if( isset( $arrValues['modified_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intModifiedDebitGlAccountId', trim( $arrValues['modified_debit_gl_account_id'] ) ); elseif( isset( $arrValues['modified_debit_gl_account_id'] ) ) $this->setModifiedDebitGlAccountId( $arrValues['modified_debit_gl_account_id'] );
		if( isset( $arrValues['write_off_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intWriteOffArCodeId', trim( $arrValues['write_off_ar_code_id'] ) ); elseif( isset( $arrValues['write_off_ar_code_id'] ) ) $this->setWriteOffArCodeId( $arrValues['write_off_ar_code_id'] );
		if( isset( $arrValues['default_ledger_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLedgerFilterId', trim( $arrValues['default_ledger_filter_id'] ) ); elseif( isset( $arrValues['default_ledger_filter_id'] ) ) $this->setDefaultLedgerFilterId( $arrValues['default_ledger_filter_id'] );
		if( isset( $arrValues['round_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRoundTypeId', trim( $arrValues['round_type_id'] ) ); elseif( isset( $arrValues['round_type_id'] ) ) $this->setRoundTypeId( $arrValues['round_type_id'] );
		if( isset( $arrValues['recovery_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecoveryArCodeId', trim( $arrValues['recovery_ar_code_id'] ) ); elseif( isset( $arrValues['recovery_ar_code_id'] ) ) $this->setRecoveryArCodeId( $arrValues['recovery_ar_code_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['capture_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intCaptureDelayDays', trim( $arrValues['capture_delay_days'] ) ); elseif( isset( $arrValues['capture_delay_days'] ) ) $this->setCaptureDelayDays( $arrValues['capture_delay_days'] );
		if( isset( $arrValues['hide_charges'] ) && $boolDirectSet ) $this->set( 'm_boolHideCharges', trim( stripcslashes( $arrValues['hide_charges'] ) ) ); elseif( isset( $arrValues['hide_charges'] ) ) $this->setHideCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_charges'] ) : $arrValues['hide_charges'] );
		if( isset( $arrValues['hide_credits'] ) && $boolDirectSet ) $this->set( 'm_boolHideCredits', trim( stripcslashes( $arrValues['hide_credits'] ) ) ); elseif( isset( $arrValues['hide_credits'] ) ) $this->setHideCredits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_credits'] ) : $arrValues['hide_credits'] );
		if( isset( $arrValues['waive_late_fees'] ) && $boolDirectSet ) $this->set( 'm_boolWaiveLateFees', trim( stripcslashes( $arrValues['waive_late_fees'] ) ) ); elseif( isset( $arrValues['waive_late_fees'] ) ) $this->setWaiveLateFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['waive_late_fees'] ) : $arrValues['waive_late_fees'] );
		if( isset( $arrValues['post_to_cash'] ) && $boolDirectSet ) $this->set( 'm_boolPostToCash', trim( stripcslashes( $arrValues['post_to_cash'] ) ) ); elseif( isset( $arrValues['post_to_cash'] ) ) $this->setPostToCash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_to_cash'] ) : $arrValues['post_to_cash'] );
		if( isset( $arrValues['show_move_out_reminder'] ) && $boolDirectSet ) $this->set( 'm_boolShowMoveOutReminder', trim( stripcslashes( $arrValues['show_move_out_reminder'] ) ) ); elseif( isset( $arrValues['show_move_out_reminder'] ) ) $this->setShowMoveOutReminder( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_move_out_reminder'] ) : $arrValues['show_move_out_reminder'] );
		if( isset( $arrValues['prorate_charges'] ) && $boolDirectSet ) $this->set( 'm_boolProrateCharges', trim( stripcslashes( $arrValues['prorate_charges'] ) ) ); elseif( isset( $arrValues['prorate_charges'] ) ) $this->setProrateCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prorate_charges'] ) : $arrValues['prorate_charges'] );
		if( isset( $arrValues['is_payment_in_kind'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaymentInKind', trim( stripcslashes( $arrValues['is_payment_in_kind'] ) ) ); elseif( isset( $arrValues['is_payment_in_kind'] ) ) $this->setIsPaymentInKind( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payment_in_kind'] ) : $arrValues['is_payment_in_kind'] );
		if( isset( $arrValues['is_auto_added'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoAdded', trim( stripcslashes( $arrValues['is_auto_added'] ) ) ); elseif( isset( $arrValues['is_auto_added'] ) ) $this->setIsAutoAdded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_added'] ) : $arrValues['is_auto_added'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_boolIsReserved', trim( stripcslashes( $arrValues['is_reserved'] ) ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reserved'] ) : $arrValues['is_reserved'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['priority_num'] ) && $boolDirectSet ) $this->set( 'm_intPriorityNum', trim( $arrValues['priority_num'] ) ); elseif( isset( $arrValues['priority_num'] ) ) $this->setPriorityNum( $arrValues['priority_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['include_in_effective_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeInEffectiveRent', trim( stripcslashes( $arrValues['include_in_effective_rent'] ) ) ); elseif( isset( $arrValues['include_in_effective_rent'] ) ) $this->setIncludeInEffectiveRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_in_effective_rent'] ) : $arrValues['include_in_effective_rent'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setModeTypeId( $intModeTypeId ) {
		$this->set( 'm_intModeTypeId', CStrings::strToIntDef( $intModeTypeId, NULL, false ) );
	}

	public function getModeTypeId() {
		return $this->m_intModeTypeId;
	}

	public function sqlModeTypeId() {
		return ( true == isset( $this->m_intModeTypeId ) ) ? ( string ) $this->m_intModeTypeId : '1';
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function sqlArCodeTypeId() {
		return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->set( 'm_intArTriggerTypeId', CStrings::strToIntDef( $intArTriggerTypeId, NULL, false ) );
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function sqlArTriggerTypeId() {
		return ( true == isset( $this->m_intArTriggerTypeId ) ) ? ( string ) $this->m_intArTriggerTypeId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setDefaultDebitGlAccountId( $intDefaultDebitGlAccountId ) {
		$this->set( 'm_intDefaultDebitGlAccountId', CStrings::strToIntDef( $intDefaultDebitGlAccountId, NULL, false ) );
	}

	public function getDefaultDebitGlAccountId() {
		return $this->m_intDefaultDebitGlAccountId;
	}

	public function sqlDefaultDebitGlAccountId() {
		return ( true == isset( $this->m_intDefaultDebitGlAccountId ) ) ? ( string ) $this->m_intDefaultDebitGlAccountId : 'NULL';
	}

	public function setDefaultCreditGlAccountId( $intDefaultCreditGlAccountId ) {
		$this->set( 'm_intDefaultCreditGlAccountId', CStrings::strToIntDef( $intDefaultCreditGlAccountId, NULL, false ) );
	}

	public function getDefaultCreditGlAccountId() {
		return $this->m_intDefaultCreditGlAccountId;
	}

	public function sqlDefaultCreditGlAccountId() {
		return ( true == isset( $this->m_intDefaultCreditGlAccountId ) ) ? ( string ) $this->m_intDefaultCreditGlAccountId : 'NULL';
	}

	public function setModifiedDebitGlAccountId( $intModifiedDebitGlAccountId ) {
		$this->set( 'm_intModifiedDebitGlAccountId', CStrings::strToIntDef( $intModifiedDebitGlAccountId, NULL, false ) );
	}

	public function getModifiedDebitGlAccountId() {
		return $this->m_intModifiedDebitGlAccountId;
	}

	public function sqlModifiedDebitGlAccountId() {
		return ( true == isset( $this->m_intModifiedDebitGlAccountId ) ) ? ( string ) $this->m_intModifiedDebitGlAccountId : 'NULL';
	}

	public function setWriteOffArCodeId( $intWriteOffArCodeId ) {
		$this->set( 'm_intWriteOffArCodeId', CStrings::strToIntDef( $intWriteOffArCodeId, NULL, false ) );
	}

	public function getWriteOffArCodeId() {
		return $this->m_intWriteOffArCodeId;
	}

	public function sqlWriteOffArCodeId() {
		return ( true == isset( $this->m_intWriteOffArCodeId ) ) ? ( string ) $this->m_intWriteOffArCodeId : 'NULL';
	}

	public function setDefaultLedgerFilterId( $intDefaultLedgerFilterId ) {
		$this->set( 'm_intDefaultLedgerFilterId', CStrings::strToIntDef( $intDefaultLedgerFilterId, NULL, false ) );
	}

	public function getDefaultLedgerFilterId() {
		return $this->m_intDefaultLedgerFilterId;
	}

	public function sqlDefaultLedgerFilterId() {
		return ( true == isset( $this->m_intDefaultLedgerFilterId ) ) ? ( string ) $this->m_intDefaultLedgerFilterId : 'NULL';
	}

	public function setRoundTypeId( $intRoundTypeId ) {
		$this->set( 'm_intRoundTypeId', CStrings::strToIntDef( $intRoundTypeId, NULL, false ) );
	}

	public function getRoundTypeId() {
		return $this->m_intRoundTypeId;
	}

	public function sqlRoundTypeId() {
		return ( true == isset( $this->m_intRoundTypeId ) ) ? ( string ) $this->m_intRoundTypeId : 'NULL';
	}

	public function setRecoveryArCodeId( $intRecoveryArCodeId ) {
		$this->set( 'm_intRecoveryArCodeId', CStrings::strToIntDef( $intRecoveryArCodeId, NULL, false ) );
	}

	public function getRecoveryArCodeId() {
		return $this->m_intRecoveryArCodeId;
	}

	public function sqlRecoveryArCodeId() {
		return ( true == isset( $this->m_intRecoveryArCodeId ) ) ? ( string ) $this->m_intRecoveryArCodeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 4 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : '0';
	}

	public function setCaptureDelayDays( $intCaptureDelayDays ) {
		$this->set( 'm_intCaptureDelayDays', CStrings::strToIntDef( $intCaptureDelayDays, NULL, false ) );
	}

	public function getCaptureDelayDays() {
		return $this->m_intCaptureDelayDays;
	}

	public function sqlCaptureDelayDays() {
		return ( true == isset( $this->m_intCaptureDelayDays ) ) ? ( string ) $this->m_intCaptureDelayDays : 'NULL';
	}

	public function setHideCharges( $boolHideCharges ) {
		$this->set( 'm_boolHideCharges', CStrings::strToBool( $boolHideCharges ) );
	}

	public function getHideCharges() {
		return $this->m_boolHideCharges;
	}

	public function sqlHideCharges() {
		return ( true == isset( $this->m_boolHideCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideCredits( $boolHideCredits ) {
		$this->set( 'm_boolHideCredits', CStrings::strToBool( $boolHideCredits ) );
	}

	public function getHideCredits() {
		return $this->m_boolHideCredits;
	}

	public function sqlHideCredits() {
		return ( true == isset( $this->m_boolHideCredits ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideCredits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWaiveLateFees( $boolWaiveLateFees ) {
		$this->set( 'm_boolWaiveLateFees', CStrings::strToBool( $boolWaiveLateFees ) );
	}

	public function getWaiveLateFees() {
		return $this->m_boolWaiveLateFees;
	}

	public function sqlWaiveLateFees() {
		return ( true == isset( $this->m_boolWaiveLateFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolWaiveLateFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostToCash( $boolPostToCash ) {
		$this->set( 'm_boolPostToCash', CStrings::strToBool( $boolPostToCash ) );
	}

	public function getPostToCash() {
		return $this->m_boolPostToCash;
	}

	public function sqlPostToCash() {
		return ( true == isset( $this->m_boolPostToCash ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostToCash ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowMoveOutReminder( $boolShowMoveOutReminder ) {
		$this->set( 'm_boolShowMoveOutReminder', CStrings::strToBool( $boolShowMoveOutReminder ) );
	}

	public function getShowMoveOutReminder() {
		return $this->m_boolShowMoveOutReminder;
	}

	public function sqlShowMoveOutReminder() {
		return ( true == isset( $this->m_boolShowMoveOutReminder ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowMoveOutReminder ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProrateCharges( $boolProrateCharges ) {
		$this->set( 'm_boolProrateCharges', CStrings::strToBool( $boolProrateCharges ) );
	}

	public function getProrateCharges() {
		return $this->m_boolProrateCharges;
	}

	public function sqlProrateCharges() {
		return ( true == isset( $this->m_boolProrateCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolProrateCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPaymentInKind( $boolIsPaymentInKind ) {
		$this->set( 'm_boolIsPaymentInKind', CStrings::strToBool( $boolIsPaymentInKind ) );
	}

	public function getIsPaymentInKind() {
		return $this->m_boolIsPaymentInKind;
	}

	public function sqlIsPaymentInKind() {
		return ( true == isset( $this->m_boolIsPaymentInKind ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentInKind ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoAdded( $boolIsAutoAdded ) {
		$this->set( 'm_boolIsAutoAdded', CStrings::strToBool( $boolIsAutoAdded ) );
	}

	public function getIsAutoAdded() {
		return $this->m_boolIsAutoAdded;
	}

	public function sqlIsAutoAdded() {
		return ( true == isset( $this->m_boolIsAutoAdded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoAdded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->set( 'm_boolIsReserved', CStrings::strToBool( $boolIsReserved ) );
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_boolIsReserved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReserved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPriorityNum( $intPriorityNum ) {
		$this->set( 'm_intPriorityNum', CStrings::strToIntDef( $intPriorityNum, NULL, false ) );
	}

	public function getPriorityNum() {
		return $this->m_intPriorityNum;
	}

	public function sqlPriorityNum() {
		return ( true == isset( $this->m_intPriorityNum ) ) ? ( string ) $this->m_intPriorityNum : '0';
	}

	public function setIncludeInEffectiveRent( $boolIncludeInEffectiveRent ) {
		$this->set( 'm_boolIncludeInEffectiveRent', CStrings::strToBool( $boolIncludeInEffectiveRent ) );
	}

	public function getIncludeInEffectiveRent() {
		return $this->m_boolIncludeInEffectiveRent;
	}

	public function sqlIncludeInEffectiveRent() {
		return ( true == isset( $this->m_boolIncludeInEffectiveRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeInEffectiveRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'mode_type_id' => $this->getModeTypeId(),
			'ar_code_type_id' => $this->getArCodeTypeId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_trigger_type_id' => $this->getArTriggerTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'default_debit_gl_account_id' => $this->getDefaultDebitGlAccountId(),
			'default_credit_gl_account_id' => $this->getDefaultCreditGlAccountId(),
			'modified_debit_gl_account_id' => $this->getModifiedDebitGlAccountId(),
			'write_off_ar_code_id' => $this->getWriteOffArCodeId(),
			'default_ledger_filter_id' => $this->getDefaultLedgerFilterId(),
			'round_type_id' => $this->getRoundTypeId(),
			'recovery_ar_code_id' => $this->getRecoveryArCodeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'default_amount' => $this->getDefaultAmount(),
			'capture_delay_days' => $this->getCaptureDelayDays(),
			'hide_charges' => $this->getHideCharges(),
			'hide_credits' => $this->getHideCredits(),
			'waive_late_fees' => $this->getWaiveLateFees(),
			'post_to_cash' => $this->getPostToCash(),
			'show_move_out_reminder' => $this->getShowMoveOutReminder(),
			'prorate_charges' => $this->getProrateCharges(),
			'is_payment_in_kind' => $this->getIsPaymentInKind(),
			'is_auto_added' => $this->getIsAutoAdded(),
			'is_reserved' => $this->getIsReserved(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'priority_num' => $this->getPriorityNum(),
			'details' => $this->getDetails(),
			'include_in_effective_rent' => $this->getIncludeInEffectiveRent(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>