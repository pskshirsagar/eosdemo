<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileTypes extends CEosPluralBase {

	/**
	 * @return CFileType[]
	 */
	public static function fetchFileTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileType
	 */
	public static function fetchFileType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileType::class, $objDatabase );
	}

	public static function fetchFileTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_types', $objDatabase );
	}

	public static function fetchFileTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileType( sprintf( 'SELECT * FROM file_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileTypesByCid( $intCid, $objDatabase ) {
		return self::fetchFileTypes( sprintf( 'SELECT * FROM file_types WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFileTypesByFileTypeGroupIdByCid( $intFileTypeGroupId, $intCid, $objDatabase ) {
		return self::fetchFileTypes( sprintf( 'SELECT * FROM file_types WHERE file_type_group_id = %d AND cid = %d', $intFileTypeGroupId, $intCid ), $objDatabase );
	}

}
?>