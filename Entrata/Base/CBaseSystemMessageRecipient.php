<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSystemMessageRecipient extends CEosSingularBase {

	const TABLE_NAME = 'public.system_message_recipients';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSystemMessageCategoryId;
	protected $m_strSystemMessageKey;
	protected $m_intCompanyEmployeeId;
	protected $m_strCustomerEmailAddresses;
	protected $m_strPreferredLocaleCode;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['system_message_category_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageCategoryId', trim( $arrValues['system_message_category_id'] ) ); elseif( isset( $arrValues['system_message_category_id'] ) ) $this->setSystemMessageCategoryId( $arrValues['system_message_category_id'] );
		if( isset( $arrValues['system_message_key'] ) && $boolDirectSet ) $this->set( 'm_strSystemMessageKey', trim( stripcslashes( $arrValues['system_message_key'] ) ) ); elseif( isset( $arrValues['system_message_key'] ) ) $this->setSystemMessageKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_message_key'] ) : $arrValues['system_message_key'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['customer_email_addresses'] ) && $boolDirectSet ) $this->set( 'm_strCustomerEmailAddresses', trim( stripcslashes( $arrValues['customer_email_addresses'] ) ) ); elseif( isset( $arrValues['customer_email_addresses'] ) ) $this->setCustomerEmailAddresses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_email_addresses'] ) : $arrValues['customer_email_addresses'] );
		if( isset( $arrValues['preferred_locale_code'] ) && $boolDirectSet ) $this->set( 'm_strPreferredLocaleCode', trim( stripcslashes( $arrValues['preferred_locale_code'] ) ) ); elseif( isset( $arrValues['preferred_locale_code'] ) ) $this->setPreferredLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preferred_locale_code'] ) : $arrValues['preferred_locale_code'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSystemMessageCategoryId( $intSystemMessageCategoryId ) {
		$this->set( 'm_intSystemMessageCategoryId', CStrings::strToIntDef( $intSystemMessageCategoryId, NULL, false ) );
	}

	public function getSystemMessageCategoryId() {
		return $this->m_intSystemMessageCategoryId;
	}

	public function sqlSystemMessageCategoryId() {
		return ( true == isset( $this->m_intSystemMessageCategoryId ) ) ? ( string ) $this->m_intSystemMessageCategoryId : 'NULL';
	}

	public function setSystemMessageKey( $strSystemMessageKey ) {
		$this->set( 'm_strSystemMessageKey', CStrings::strTrimDef( $strSystemMessageKey, 100, NULL, true ) );
	}

	public function getSystemMessageKey() {
		return $this->m_strSystemMessageKey;
	}

	public function sqlSystemMessageKey() {
		return ( true == isset( $this->m_strSystemMessageKey ) ) ? '\'' . addslashes( $this->m_strSystemMessageKey ) . '\'' : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCustomerEmailAddresses( $strCustomerEmailAddresses ) {
		$this->set( 'm_strCustomerEmailAddresses', CStrings::strTrimDef( $strCustomerEmailAddresses, 240, NULL, true ) );
	}

	public function getCustomerEmailAddresses() {
		return $this->m_strCustomerEmailAddresses;
	}

	public function sqlCustomerEmailAddresses() {
		return ( true == isset( $this->m_strCustomerEmailAddresses ) ) ? '\'' . addslashes( $this->m_strCustomerEmailAddresses ) . '\'' : 'NULL';
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->set( 'm_strPreferredLocaleCode', CStrings::strTrimDef( $strPreferredLocaleCode, 100, NULL, true ) );
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function sqlPreferredLocaleCode() {
		return ( true == isset( $this->m_strPreferredLocaleCode ) ) ? '\'' . addslashes( $this->m_strPreferredLocaleCode ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, system_message_category_id, system_message_key, company_employee_id, customer_email_addresses, preferred_locale_code, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSystemMessageCategoryId() . ', ' .
 						$this->sqlSystemMessageKey() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlCustomerEmailAddresses() . ', ' .
 						$this->sqlPreferredLocaleCode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_category_id = ' . $this->sqlSystemMessageCategoryId() . ','; } elseif( true == array_key_exists( 'SystemMessageCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_category_id = ' . $this->sqlSystemMessageCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_key = ' . $this->sqlSystemMessageKey() . ','; } elseif( true == array_key_exists( 'SystemMessageKey', $this->getChangedColumns() ) ) { $strSql .= ' system_message_key = ' . $this->sqlSystemMessageKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_email_addresses = ' . $this->sqlCustomerEmailAddresses() . ','; } elseif( true == array_key_exists( 'CustomerEmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' customer_email_addresses = ' . $this->sqlCustomerEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_locale_code = ' . $this->sqlPreferredLocaleCode() . ','; } elseif( true == array_key_exists( 'PreferredLocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' preferred_locale_code = ' . $this->sqlPreferredLocaleCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'system_message_category_id' => $this->getSystemMessageCategoryId(),
			'system_message_key' => $this->getSystemMessageKey(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'customer_email_addresses' => $this->getCustomerEmailAddresses(),
			'preferred_locale_code' => $this->getPreferredLocaleCode(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>