<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDashboardPriority extends CEosSingularBase {

	const TABLE_NAME = 'public.dashboard_priorities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strApprovalsApplications;
	protected $m_strApprovalsInvoices;
	protected $m_strApprovalsReversals;
	protected $m_strApprovalsEsignDocs;
	protected $m_strApprovalsRenewalOffers;
	protected $m_strApprovalsTransfers;
	protected $m_strApprovalsPurchaseOrders;
	protected $m_strApprovalsChecks;
	protected $m_strApprovalsUtilities;
	protected $m_strApprovalsFinancialMoveOuts;
	protected $m_strApprovalsTransactions;
	protected $m_strLeadsAssignAgents;
	protected $m_strLeadsNeverContacted;
	protected $m_strLeadsFollowUp;
	protected $m_strLeadsNotProgressing;
	protected $m_strLeadsResidentReferrals;
	protected $m_strLeadsAnsweredCalls;
	protected $m_strLeadsUnknownEmail;
	protected $m_strApplicantsFollowUp;
	protected $m_strApplicantsNotProgressing;
	protected $m_strApplicantsGenerateLeases;
	protected $m_strApplicantsMissingInsurance;
	protected $m_strResidentsFollowUp;
	protected $m_strResidentsMoveInReviews;
	protected $m_strResidentsMoveIns;
	protected $m_strResidentsEvictions;
	protected $m_strResidentsMoveOuts;
	protected $m_strResidentsRenewals;
	protected $m_strResidentsTransfers;
	protected $m_strResidentsRentableItems;
	protected $m_strResidentsInsurance;
	protected $m_strResidentsAdditionalSettings;
	protected $m_strResidentsNotProgressing;
	protected $m_strResidentPortalAmenityReservations;
	protected $m_strResidentPortalClassifieds;
	protected $m_strResidentPortalClubs;
	protected $m_strResidentPortalConcierge;
	protected $m_strResidentPortalEvents;
	protected $m_strMaintenanceAssignWorkOrders;
	protected $m_strMaintenanceWorkOrders;
	protected $m_strMaintenanceRecurringWorkOrders;
	protected $m_strMaintenanceMakeReadies;
	protected $m_strMaintenanceInspections;
	protected $m_strMaintenanceInspectionReviews;
	protected $m_strFinancialPayments;
	protected $m_strFinancialCheck21;
	protected $m_strFinancialDeposits;
	protected $m_strFinancialNotices;
	protected $m_strFinancialCollections;
	protected $m_strFinancialDeliveryErrors;
	protected $m_strFinancialMoveOuts;
	protected $m_strFinancialAcceleratedRentCredits;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strApprovalsScheduledCharges;
	protected $m_strApprovalsDocumentVerification;
	protected $m_strResidentsViolations;
	protected $m_strApprovalsReasonableAccommodations;
	protected $m_strResidentsReasonableAccommodations;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['approvals_applications'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsApplications', trim( stripcslashes( $arrValues['approvals_applications'] ) ) ); elseif( isset( $arrValues['approvals_applications'] ) ) $this->setApprovalsApplications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_applications'] ) : $arrValues['approvals_applications'] );
		if( isset( $arrValues['approvals_invoices'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsInvoices', trim( stripcslashes( $arrValues['approvals_invoices'] ) ) ); elseif( isset( $arrValues['approvals_invoices'] ) ) $this->setApprovalsInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_invoices'] ) : $arrValues['approvals_invoices'] );
		if( isset( $arrValues['approvals_reversals'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsReversals', trim( stripcslashes( $arrValues['approvals_reversals'] ) ) ); elseif( isset( $arrValues['approvals_reversals'] ) ) $this->setApprovalsReversals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_reversals'] ) : $arrValues['approvals_reversals'] );
		if( isset( $arrValues['approvals_esign_docs'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsEsignDocs', trim( stripcslashes( $arrValues['approvals_esign_docs'] ) ) ); elseif( isset( $arrValues['approvals_esign_docs'] ) ) $this->setApprovalsEsignDocs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_esign_docs'] ) : $arrValues['approvals_esign_docs'] );
		if( isset( $arrValues['approvals_renewal_offers'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsRenewalOffers', trim( stripcslashes( $arrValues['approvals_renewal_offers'] ) ) ); elseif( isset( $arrValues['approvals_renewal_offers'] ) ) $this->setApprovalsRenewalOffers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_renewal_offers'] ) : $arrValues['approvals_renewal_offers'] );
		if( isset( $arrValues['approvals_transfers'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsTransfers', trim( stripcslashes( $arrValues['approvals_transfers'] ) ) ); elseif( isset( $arrValues['approvals_transfers'] ) ) $this->setApprovalsTransfers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_transfers'] ) : $arrValues['approvals_transfers'] );
		if( isset( $arrValues['approvals_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsPurchaseOrders', trim( stripcslashes( $arrValues['approvals_purchase_orders'] ) ) ); elseif( isset( $arrValues['approvals_purchase_orders'] ) ) $this->setApprovalsPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_purchase_orders'] ) : $arrValues['approvals_purchase_orders'] );
		if( isset( $arrValues['approvals_checks'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsChecks', trim( stripcslashes( $arrValues['approvals_checks'] ) ) ); elseif( isset( $arrValues['approvals_checks'] ) ) $this->setApprovalsChecks( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_checks'] ) : $arrValues['approvals_checks'] );
		if( isset( $arrValues['approvals_utilities'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsUtilities', trim( stripcslashes( $arrValues['approvals_utilities'] ) ) ); elseif( isset( $arrValues['approvals_utilities'] ) ) $this->setApprovalsUtilities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_utilities'] ) : $arrValues['approvals_utilities'] );
		if( isset( $arrValues['approvals_financial_move_outs'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsFinancialMoveOuts', trim( stripcslashes( $arrValues['approvals_financial_move_outs'] ) ) ); elseif( isset( $arrValues['approvals_financial_move_outs'] ) ) $this->setApprovalsFinancialMoveOuts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_financial_move_outs'] ) : $arrValues['approvals_financial_move_outs'] );
		if( isset( $arrValues['approvals_transactions'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsTransactions', trim( stripcslashes( $arrValues['approvals_transactions'] ) ) ); elseif( isset( $arrValues['approvals_transactions'] ) ) $this->setApprovalsTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_transactions'] ) : $arrValues['approvals_transactions'] );
		if( isset( $arrValues['leads_assign_agents'] ) && $boolDirectSet ) $this->set( 'm_strLeadsAssignAgents', trim( stripcslashes( $arrValues['leads_assign_agents'] ) ) ); elseif( isset( $arrValues['leads_assign_agents'] ) ) $this->setLeadsAssignAgents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_assign_agents'] ) : $arrValues['leads_assign_agents'] );
		if( isset( $arrValues['leads_never_contacted'] ) && $boolDirectSet ) $this->set( 'm_strLeadsNeverContacted', trim( stripcslashes( $arrValues['leads_never_contacted'] ) ) ); elseif( isset( $arrValues['leads_never_contacted'] ) ) $this->setLeadsNeverContacted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_never_contacted'] ) : $arrValues['leads_never_contacted'] );
		if( isset( $arrValues['leads_follow_up'] ) && $boolDirectSet ) $this->set( 'm_strLeadsFollowUp', trim( stripcslashes( $arrValues['leads_follow_up'] ) ) ); elseif( isset( $arrValues['leads_follow_up'] ) ) $this->setLeadsFollowUp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_follow_up'] ) : $arrValues['leads_follow_up'] );
		if( isset( $arrValues['leads_not_progressing'] ) && $boolDirectSet ) $this->set( 'm_strLeadsNotProgressing', trim( stripcslashes( $arrValues['leads_not_progressing'] ) ) ); elseif( isset( $arrValues['leads_not_progressing'] ) ) $this->setLeadsNotProgressing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_not_progressing'] ) : $arrValues['leads_not_progressing'] );
		if( isset( $arrValues['leads_resident_referrals'] ) && $boolDirectSet ) $this->set( 'm_strLeadsResidentReferrals', trim( stripcslashes( $arrValues['leads_resident_referrals'] ) ) ); elseif( isset( $arrValues['leads_resident_referrals'] ) ) $this->setLeadsResidentReferrals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_resident_referrals'] ) : $arrValues['leads_resident_referrals'] );
		if( isset( $arrValues['leads_answered_calls'] ) && $boolDirectSet ) $this->set( 'm_strLeadsAnsweredCalls', trim( stripcslashes( $arrValues['leads_answered_calls'] ) ) ); elseif( isset( $arrValues['leads_answered_calls'] ) ) $this->setLeadsAnsweredCalls( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_answered_calls'] ) : $arrValues['leads_answered_calls'] );
		if( isset( $arrValues['leads_unknown_email'] ) && $boolDirectSet ) $this->set( 'm_strLeadsUnknownEmail', trim( stripcslashes( $arrValues['leads_unknown_email'] ) ) ); elseif( isset( $arrValues['leads_unknown_email'] ) ) $this->setLeadsUnknownEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leads_unknown_email'] ) : $arrValues['leads_unknown_email'] );
		if( isset( $arrValues['applicants_follow_up'] ) && $boolDirectSet ) $this->set( 'm_strApplicantsFollowUp', trim( stripcslashes( $arrValues['applicants_follow_up'] ) ) ); elseif( isset( $arrValues['applicants_follow_up'] ) ) $this->setApplicantsFollowUp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applicants_follow_up'] ) : $arrValues['applicants_follow_up'] );
		if( isset( $arrValues['applicants_not_progressing'] ) && $boolDirectSet ) $this->set( 'm_strApplicantsNotProgressing', trim( stripcslashes( $arrValues['applicants_not_progressing'] ) ) ); elseif( isset( $arrValues['applicants_not_progressing'] ) ) $this->setApplicantsNotProgressing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applicants_not_progressing'] ) : $arrValues['applicants_not_progressing'] );
		if( isset( $arrValues['applicants_generate_leases'] ) && $boolDirectSet ) $this->set( 'm_strApplicantsGenerateLeases', trim( stripcslashes( $arrValues['applicants_generate_leases'] ) ) ); elseif( isset( $arrValues['applicants_generate_leases'] ) ) $this->setApplicantsGenerateLeases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applicants_generate_leases'] ) : $arrValues['applicants_generate_leases'] );
		if( isset( $arrValues['applicants_missing_insurance'] ) && $boolDirectSet ) $this->set( 'm_strApplicantsMissingInsurance', trim( stripcslashes( $arrValues['applicants_missing_insurance'] ) ) ); elseif( isset( $arrValues['applicants_missing_insurance'] ) ) $this->setApplicantsMissingInsurance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applicants_missing_insurance'] ) : $arrValues['applicants_missing_insurance'] );
		if( isset( $arrValues['residents_follow_up'] ) && $boolDirectSet ) $this->set( 'm_strResidentsFollowUp', trim( stripcslashes( $arrValues['residents_follow_up'] ) ) ); elseif( isset( $arrValues['residents_follow_up'] ) ) $this->setResidentsFollowUp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_follow_up'] ) : $arrValues['residents_follow_up'] );
		if( isset( $arrValues['residents_move_in_reviews'] ) && $boolDirectSet ) $this->set( 'm_strResidentsMoveInReviews', trim( stripcslashes( $arrValues['residents_move_in_reviews'] ) ) ); elseif( isset( $arrValues['residents_move_in_reviews'] ) ) $this->setResidentsMoveInReviews( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_move_in_reviews'] ) : $arrValues['residents_move_in_reviews'] );
		if( isset( $arrValues['residents_move_ins'] ) && $boolDirectSet ) $this->set( 'm_strResidentsMoveIns', trim( stripcslashes( $arrValues['residents_move_ins'] ) ) ); elseif( isset( $arrValues['residents_move_ins'] ) ) $this->setResidentsMoveIns( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_move_ins'] ) : $arrValues['residents_move_ins'] );
		if( isset( $arrValues['residents_evictions'] ) && $boolDirectSet ) $this->set( 'm_strResidentsEvictions', trim( stripcslashes( $arrValues['residents_evictions'] ) ) ); elseif( isset( $arrValues['residents_evictions'] ) ) $this->setResidentsEvictions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_evictions'] ) : $arrValues['residents_evictions'] );
		if( isset( $arrValues['residents_move_outs'] ) && $boolDirectSet ) $this->set( 'm_strResidentsMoveOuts', trim( stripcslashes( $arrValues['residents_move_outs'] ) ) ); elseif( isset( $arrValues['residents_move_outs'] ) ) $this->setResidentsMoveOuts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_move_outs'] ) : $arrValues['residents_move_outs'] );
		if( isset( $arrValues['residents_renewals'] ) && $boolDirectSet ) $this->set( 'm_strResidentsRenewals', trim( stripcslashes( $arrValues['residents_renewals'] ) ) ); elseif( isset( $arrValues['residents_renewals'] ) ) $this->setResidentsRenewals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_renewals'] ) : $arrValues['residents_renewals'] );
		if( isset( $arrValues['residents_transfers'] ) && $boolDirectSet ) $this->set( 'm_strResidentsTransfers', trim( stripcslashes( $arrValues['residents_transfers'] ) ) ); elseif( isset( $arrValues['residents_transfers'] ) ) $this->setResidentsTransfers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_transfers'] ) : $arrValues['residents_transfers'] );
		if( isset( $arrValues['residents_rentable_items'] ) && $boolDirectSet ) $this->set( 'm_strResidentsRentableItems', trim( stripcslashes( $arrValues['residents_rentable_items'] ) ) ); elseif( isset( $arrValues['residents_rentable_items'] ) ) $this->setResidentsRentableItems( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_rentable_items'] ) : $arrValues['residents_rentable_items'] );
		if( isset( $arrValues['residents_insurance'] ) && $boolDirectSet ) $this->set( 'm_strResidentsInsurance', trim( stripcslashes( $arrValues['residents_insurance'] ) ) ); elseif( isset( $arrValues['residents_insurance'] ) ) $this->setResidentsInsurance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_insurance'] ) : $arrValues['residents_insurance'] );
		if( isset( $arrValues['residents_additional_settings'] ) && $boolDirectSet ) $this->set( 'm_strResidentsAdditionalSettings', trim( stripcslashes( $arrValues['residents_additional_settings'] ) ) ); elseif( isset( $arrValues['residents_additional_settings'] ) ) $this->setResidentsAdditionalSettings( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_additional_settings'] ) : $arrValues['residents_additional_settings'] );
		if( isset( $arrValues['residents_not_progressing'] ) && $boolDirectSet ) $this->set( 'm_strResidentsNotProgressing', trim( stripcslashes( $arrValues['residents_not_progressing'] ) ) ); elseif( isset( $arrValues['residents_not_progressing'] ) ) $this->setResidentsNotProgressing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_not_progressing'] ) : $arrValues['residents_not_progressing'] );
		if( isset( $arrValues['resident_portal_amenity_reservations'] ) && $boolDirectSet ) $this->set( 'm_strResidentPortalAmenityReservations', trim( stripcslashes( $arrValues['resident_portal_amenity_reservations'] ) ) ); elseif( isset( $arrValues['resident_portal_amenity_reservations'] ) ) $this->setResidentPortalAmenityReservations( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resident_portal_amenity_reservations'] ) : $arrValues['resident_portal_amenity_reservations'] );
		if( isset( $arrValues['resident_portal_classifieds'] ) && $boolDirectSet ) $this->set( 'm_strResidentPortalClassifieds', trim( stripcslashes( $arrValues['resident_portal_classifieds'] ) ) ); elseif( isset( $arrValues['resident_portal_classifieds'] ) ) $this->setResidentPortalClassifieds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resident_portal_classifieds'] ) : $arrValues['resident_portal_classifieds'] );
		if( isset( $arrValues['resident_portal_clubs'] ) && $boolDirectSet ) $this->set( 'm_strResidentPortalClubs', trim( stripcslashes( $arrValues['resident_portal_clubs'] ) ) ); elseif( isset( $arrValues['resident_portal_clubs'] ) ) $this->setResidentPortalClubs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resident_portal_clubs'] ) : $arrValues['resident_portal_clubs'] );
		if( isset( $arrValues['resident_portal_concierge'] ) && $boolDirectSet ) $this->set( 'm_strResidentPortalConcierge', trim( stripcslashes( $arrValues['resident_portal_concierge'] ) ) ); elseif( isset( $arrValues['resident_portal_concierge'] ) ) $this->setResidentPortalConcierge( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resident_portal_concierge'] ) : $arrValues['resident_portal_concierge'] );
		if( isset( $arrValues['resident_portal_events'] ) && $boolDirectSet ) $this->set( 'm_strResidentPortalEvents', trim( stripcslashes( $arrValues['resident_portal_events'] ) ) ); elseif( isset( $arrValues['resident_portal_events'] ) ) $this->setResidentPortalEvents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resident_portal_events'] ) : $arrValues['resident_portal_events'] );
		if( isset( $arrValues['maintenance_assign_work_orders'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceAssignWorkOrders', trim( stripcslashes( $arrValues['maintenance_assign_work_orders'] ) ) ); elseif( isset( $arrValues['maintenance_assign_work_orders'] ) ) $this->setMaintenanceAssignWorkOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_assign_work_orders'] ) : $arrValues['maintenance_assign_work_orders'] );
		if( isset( $arrValues['maintenance_work_orders'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceWorkOrders', trim( stripcslashes( $arrValues['maintenance_work_orders'] ) ) ); elseif( isset( $arrValues['maintenance_work_orders'] ) ) $this->setMaintenanceWorkOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_work_orders'] ) : $arrValues['maintenance_work_orders'] );
		if( isset( $arrValues['maintenance_recurring_work_orders'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceRecurringWorkOrders', trim( stripcslashes( $arrValues['maintenance_recurring_work_orders'] ) ) ); elseif( isset( $arrValues['maintenance_recurring_work_orders'] ) ) $this->setMaintenanceRecurringWorkOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_recurring_work_orders'] ) : $arrValues['maintenance_recurring_work_orders'] );
		if( isset( $arrValues['maintenance_make_readies'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceMakeReadies', trim( stripcslashes( $arrValues['maintenance_make_readies'] ) ) ); elseif( isset( $arrValues['maintenance_make_readies'] ) ) $this->setMaintenanceMakeReadies( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_make_readies'] ) : $arrValues['maintenance_make_readies'] );
		if( isset( $arrValues['maintenance_inspections'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceInspections', trim( stripcslashes( $arrValues['maintenance_inspections'] ) ) ); elseif( isset( $arrValues['maintenance_inspections'] ) ) $this->setMaintenanceInspections( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_inspections'] ) : $arrValues['maintenance_inspections'] );
		if( isset( $arrValues['maintenance_inspection_reviews'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceInspectionReviews', trim( stripcslashes( $arrValues['maintenance_inspection_reviews'] ) ) ); elseif( isset( $arrValues['maintenance_inspection_reviews'] ) ) $this->setMaintenanceInspectionReviews( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_inspection_reviews'] ) : $arrValues['maintenance_inspection_reviews'] );
		if( isset( $arrValues['financial_payments'] ) && $boolDirectSet ) $this->set( 'm_strFinancialPayments', trim( stripcslashes( $arrValues['financial_payments'] ) ) ); elseif( isset( $arrValues['financial_payments'] ) ) $this->setFinancialPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_payments'] ) : $arrValues['financial_payments'] );
		if( isset( $arrValues['financial_check21'] ) && $boolDirectSet ) $this->set( 'm_strFinancialCheck21', trim( stripcslashes( $arrValues['financial_check21'] ) ) ); elseif( isset( $arrValues['financial_check21'] ) ) $this->setFinancialCheck21( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_check21'] ) : $arrValues['financial_check21'] );
		if( isset( $arrValues['financial_deposits'] ) && $boolDirectSet ) $this->set( 'm_strFinancialDeposits', trim( stripcslashes( $arrValues['financial_deposits'] ) ) ); elseif( isset( $arrValues['financial_deposits'] ) ) $this->setFinancialDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_deposits'] ) : $arrValues['financial_deposits'] );
		if( isset( $arrValues['financial_notices'] ) && $boolDirectSet ) $this->set( 'm_strFinancialNotices', trim( stripcslashes( $arrValues['financial_notices'] ) ) ); elseif( isset( $arrValues['financial_notices'] ) ) $this->setFinancialNotices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_notices'] ) : $arrValues['financial_notices'] );
		if( isset( $arrValues['financial_collections'] ) && $boolDirectSet ) $this->set( 'm_strFinancialCollections', trim( stripcslashes( $arrValues['financial_collections'] ) ) ); elseif( isset( $arrValues['financial_collections'] ) ) $this->setFinancialCollections( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_collections'] ) : $arrValues['financial_collections'] );
		if( isset( $arrValues['financial_delivery_errors'] ) && $boolDirectSet ) $this->set( 'm_strFinancialDeliveryErrors', trim( stripcslashes( $arrValues['financial_delivery_errors'] ) ) ); elseif( isset( $arrValues['financial_delivery_errors'] ) ) $this->setFinancialDeliveryErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_delivery_errors'] ) : $arrValues['financial_delivery_errors'] );
		if( isset( $arrValues['financial_move_outs'] ) && $boolDirectSet ) $this->set( 'm_strFinancialMoveOuts', trim( stripcslashes( $arrValues['financial_move_outs'] ) ) ); elseif( isset( $arrValues['financial_move_outs'] ) ) $this->setFinancialMoveOuts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_move_outs'] ) : $arrValues['financial_move_outs'] );
		if( isset( $arrValues['financial_accelerated_rent_credits'] ) && $boolDirectSet ) $this->set( 'm_strFinancialAcceleratedRentCredits', trim( stripcslashes( $arrValues['financial_accelerated_rent_credits'] ) ) ); elseif( isset( $arrValues['financial_accelerated_rent_credits'] ) ) $this->setFinancialAcceleratedRentCredits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['financial_accelerated_rent_credits'] ) : $arrValues['financial_accelerated_rent_credits'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['approvals_scheduled_charges'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsScheduledCharges', trim( stripcslashes( $arrValues['approvals_scheduled_charges'] ) ) ); elseif( isset( $arrValues['approvals_scheduled_charges'] ) ) $this->setApprovalsScheduledCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_scheduled_charges'] ) : $arrValues['approvals_scheduled_charges'] );
		if( isset( $arrValues['approvals_document_verification'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsDocumentVerification', trim( stripcslashes( $arrValues['approvals_document_verification'] ) ) ); elseif( isset( $arrValues['approvals_document_verification'] ) ) $this->setApprovalsDocumentVerification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_document_verification'] ) : $arrValues['approvals_document_verification'] );
		if( isset( $arrValues['residents_violations'] ) && $boolDirectSet ) $this->set( 'm_strResidentsViolations', trim( stripcslashes( $arrValues['residents_violations'] ) ) ); elseif( isset( $arrValues['residents_violations'] ) ) $this->setResidentsViolations( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_violations'] ) : $arrValues['residents_violations'] );
		if( isset( $arrValues['approvals_reasonable_accommodations'] ) && $boolDirectSet ) $this->set( 'm_strApprovalsReasonableAccommodations', trim( stripcslashes( $arrValues['approvals_reasonable_accommodations'] ) ) ); elseif( isset( $arrValues['approvals_reasonable_accommodations'] ) ) $this->setApprovalsReasonableAccommodations( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approvals_reasonable_accommodations'] ) : $arrValues['approvals_reasonable_accommodations'] );
		if( isset( $arrValues['residents_reasonable_accommodations'] ) && $boolDirectSet ) $this->set( 'm_strResidentsReasonableAccommodations', trim( stripcslashes( $arrValues['residents_reasonable_accommodations'] ) ) ); elseif( isset( $arrValues['residents_reasonable_accommodations'] ) ) $this->setResidentsReasonableAccommodations( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_reasonable_accommodations'] ) : $arrValues['residents_reasonable_accommodations'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApprovalsApplications( $strApprovalsApplications ) {
		$this->set( 'm_strApprovalsApplications', CStrings::strTrimDef( $strApprovalsApplications, -1, NULL, true ) );
	}

	public function getApprovalsApplications() {
		return $this->m_strApprovalsApplications;
	}

	public function sqlApprovalsApplications() {
		return ( true == isset( $this->m_strApprovalsApplications ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsApplications ) : '\'' . addslashes( $this->m_strApprovalsApplications ) . '\'' ) : 'NULL';
	}

	public function setApprovalsInvoices( $strApprovalsInvoices ) {
		$this->set( 'm_strApprovalsInvoices', CStrings::strTrimDef( $strApprovalsInvoices, -1, NULL, true ) );
	}

	public function getApprovalsInvoices() {
		return $this->m_strApprovalsInvoices;
	}

	public function sqlApprovalsInvoices() {
		return ( true == isset( $this->m_strApprovalsInvoices ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsInvoices ) : '\'' . addslashes( $this->m_strApprovalsInvoices ) . '\'' ) : 'NULL';
	}

	public function setApprovalsReversals( $strApprovalsReversals ) {
		$this->set( 'm_strApprovalsReversals', CStrings::strTrimDef( $strApprovalsReversals, -1, NULL, true ) );
	}

	public function getApprovalsReversals() {
		return $this->m_strApprovalsReversals;
	}

	public function sqlApprovalsReversals() {
		return ( true == isset( $this->m_strApprovalsReversals ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsReversals ) : '\'' . addslashes( $this->m_strApprovalsReversals ) . '\'' ) : 'NULL';
	}

	public function setApprovalsEsignDocs( $strApprovalsEsignDocs ) {
		$this->set( 'm_strApprovalsEsignDocs', CStrings::strTrimDef( $strApprovalsEsignDocs, -1, NULL, true ) );
	}

	public function getApprovalsEsignDocs() {
		return $this->m_strApprovalsEsignDocs;
	}

	public function sqlApprovalsEsignDocs() {
		return ( true == isset( $this->m_strApprovalsEsignDocs ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsEsignDocs ) : '\'' . addslashes( $this->m_strApprovalsEsignDocs ) . '\'' ) : 'NULL';
	}

	public function setApprovalsRenewalOffers( $strApprovalsRenewalOffers ) {
		$this->set( 'm_strApprovalsRenewalOffers', CStrings::strTrimDef( $strApprovalsRenewalOffers, -1, NULL, true ) );
	}

	public function getApprovalsRenewalOffers() {
		return $this->m_strApprovalsRenewalOffers;
	}

	public function sqlApprovalsRenewalOffers() {
		return ( true == isset( $this->m_strApprovalsRenewalOffers ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsRenewalOffers ) : '\'' . addslashes( $this->m_strApprovalsRenewalOffers ) . '\'' ) : 'NULL';
	}

	public function setApprovalsTransfers( $strApprovalsTransfers ) {
		$this->set( 'm_strApprovalsTransfers', CStrings::strTrimDef( $strApprovalsTransfers, -1, NULL, true ) );
	}

	public function getApprovalsTransfers() {
		return $this->m_strApprovalsTransfers;
	}

	public function sqlApprovalsTransfers() {
		return ( true == isset( $this->m_strApprovalsTransfers ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsTransfers ) : '\'' . addslashes( $this->m_strApprovalsTransfers ) . '\'' ) : 'NULL';
	}

	public function setApprovalsPurchaseOrders( $strApprovalsPurchaseOrders ) {
		$this->set( 'm_strApprovalsPurchaseOrders', CStrings::strTrimDef( $strApprovalsPurchaseOrders, -1, NULL, true ) );
	}

	public function getApprovalsPurchaseOrders() {
		return $this->m_strApprovalsPurchaseOrders;
	}

	public function sqlApprovalsPurchaseOrders() {
		return ( true == isset( $this->m_strApprovalsPurchaseOrders ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsPurchaseOrders ) : '\'' . addslashes( $this->m_strApprovalsPurchaseOrders ) . '\'' ) : 'NULL';
	}

	public function setApprovalsChecks( $strApprovalsChecks ) {
		$this->set( 'm_strApprovalsChecks', CStrings::strTrimDef( $strApprovalsChecks, -1, NULL, true ) );
	}

	public function getApprovalsChecks() {
		return $this->m_strApprovalsChecks;
	}

	public function sqlApprovalsChecks() {
		return ( true == isset( $this->m_strApprovalsChecks ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsChecks ) : '\'' . addslashes( $this->m_strApprovalsChecks ) . '\'' ) : 'NULL';
	}

	public function setApprovalsUtilities( $strApprovalsUtilities ) {
		$this->set( 'm_strApprovalsUtilities', CStrings::strTrimDef( $strApprovalsUtilities, -1, NULL, true ) );
	}

	public function getApprovalsUtilities() {
		return $this->m_strApprovalsUtilities;
	}

	public function sqlApprovalsUtilities() {
		return ( true == isset( $this->m_strApprovalsUtilities ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsUtilities ) : '\'' . addslashes( $this->m_strApprovalsUtilities ) . '\'' ) : 'NULL';
	}

	public function setApprovalsFinancialMoveOuts( $strApprovalsFinancialMoveOuts ) {
		$this->set( 'm_strApprovalsFinancialMoveOuts', CStrings::strTrimDef( $strApprovalsFinancialMoveOuts, -1, NULL, true ) );
	}

	public function getApprovalsFinancialMoveOuts() {
		return $this->m_strApprovalsFinancialMoveOuts;
	}

	public function sqlApprovalsFinancialMoveOuts() {
		return ( true == isset( $this->m_strApprovalsFinancialMoveOuts ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsFinancialMoveOuts ) : '\'' . addslashes( $this->m_strApprovalsFinancialMoveOuts ) . '\'' ) : 'NULL';
	}

	public function setApprovalsTransactions( $strApprovalsTransactions ) {
		$this->set( 'm_strApprovalsTransactions', CStrings::strTrimDef( $strApprovalsTransactions, -1, NULL, true ) );
	}

	public function getApprovalsTransactions() {
		return $this->m_strApprovalsTransactions;
	}

	public function sqlApprovalsTransactions() {
		return ( true == isset( $this->m_strApprovalsTransactions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsTransactions ) : '\'' . addslashes( $this->m_strApprovalsTransactions ) . '\'' ) : 'NULL';
	}

	public function setLeadsAssignAgents( $strLeadsAssignAgents ) {
		$this->set( 'm_strLeadsAssignAgents', CStrings::strTrimDef( $strLeadsAssignAgents, -1, NULL, true ) );
	}

	public function getLeadsAssignAgents() {
		return $this->m_strLeadsAssignAgents;
	}

	public function sqlLeadsAssignAgents() {
		return ( true == isset( $this->m_strLeadsAssignAgents ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsAssignAgents ) : '\'' . addslashes( $this->m_strLeadsAssignAgents ) . '\'' ) : 'NULL';
	}

	public function setLeadsNeverContacted( $strLeadsNeverContacted ) {
		$this->set( 'm_strLeadsNeverContacted', CStrings::strTrimDef( $strLeadsNeverContacted, -1, NULL, true ) );
	}

	public function getLeadsNeverContacted() {
		return $this->m_strLeadsNeverContacted;
	}

	public function sqlLeadsNeverContacted() {
		return ( true == isset( $this->m_strLeadsNeverContacted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsNeverContacted ) : '\'' . addslashes( $this->m_strLeadsNeverContacted ) . '\'' ) : 'NULL';
	}

	public function setLeadsFollowUp( $strLeadsFollowUp ) {
		$this->set( 'm_strLeadsFollowUp', CStrings::strTrimDef( $strLeadsFollowUp, -1, NULL, true ) );
	}

	public function getLeadsFollowUp() {
		return $this->m_strLeadsFollowUp;
	}

	public function sqlLeadsFollowUp() {
		return ( true == isset( $this->m_strLeadsFollowUp ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsFollowUp ) : '\'' . addslashes( $this->m_strLeadsFollowUp ) . '\'' ) : 'NULL';
	}

	public function setLeadsNotProgressing( $strLeadsNotProgressing ) {
		$this->set( 'm_strLeadsNotProgressing', CStrings::strTrimDef( $strLeadsNotProgressing, -1, NULL, true ) );
	}

	public function getLeadsNotProgressing() {
		return $this->m_strLeadsNotProgressing;
	}

	public function sqlLeadsNotProgressing() {
		return ( true == isset( $this->m_strLeadsNotProgressing ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsNotProgressing ) : '\'' . addslashes( $this->m_strLeadsNotProgressing ) . '\'' ) : 'NULL';
	}

	public function setLeadsResidentReferrals( $strLeadsResidentReferrals ) {
		$this->set( 'm_strLeadsResidentReferrals', CStrings::strTrimDef( $strLeadsResidentReferrals, -1, NULL, true ) );
	}

	public function getLeadsResidentReferrals() {
		return $this->m_strLeadsResidentReferrals;
	}

	public function sqlLeadsResidentReferrals() {
		return ( true == isset( $this->m_strLeadsResidentReferrals ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsResidentReferrals ) : '\'' . addslashes( $this->m_strLeadsResidentReferrals ) . '\'' ) : 'NULL';
	}

	public function setLeadsAnsweredCalls( $strLeadsAnsweredCalls ) {
		$this->set( 'm_strLeadsAnsweredCalls', CStrings::strTrimDef( $strLeadsAnsweredCalls, -1, NULL, true ) );
	}

	public function getLeadsAnsweredCalls() {
		return $this->m_strLeadsAnsweredCalls;
	}

	public function sqlLeadsAnsweredCalls() {
		return ( true == isset( $this->m_strLeadsAnsweredCalls ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsAnsweredCalls ) : '\'' . addslashes( $this->m_strLeadsAnsweredCalls ) . '\'' ) : 'NULL';
	}

	public function setLeadsUnknownEmail( $strLeadsUnknownEmail ) {
		$this->set( 'm_strLeadsUnknownEmail', CStrings::strTrimDef( $strLeadsUnknownEmail, -1, NULL, true ) );
	}

	public function getLeadsUnknownEmail() {
		return $this->m_strLeadsUnknownEmail;
	}

	public function sqlLeadsUnknownEmail() {
		return ( true == isset( $this->m_strLeadsUnknownEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadsUnknownEmail ) : '\'' . addslashes( $this->m_strLeadsUnknownEmail ) . '\'' ) : 'NULL';
	}

	public function setApplicantsFollowUp( $strApplicantsFollowUp ) {
		$this->set( 'm_strApplicantsFollowUp', CStrings::strTrimDef( $strApplicantsFollowUp, -1, NULL, true ) );
	}

	public function getApplicantsFollowUp() {
		return $this->m_strApplicantsFollowUp;
	}

	public function sqlApplicantsFollowUp() {
		return ( true == isset( $this->m_strApplicantsFollowUp ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApplicantsFollowUp ) : '\'' . addslashes( $this->m_strApplicantsFollowUp ) . '\'' ) : 'NULL';
	}

	public function setApplicantsNotProgressing( $strApplicantsNotProgressing ) {
		$this->set( 'm_strApplicantsNotProgressing', CStrings::strTrimDef( $strApplicantsNotProgressing, -1, NULL, true ) );
	}

	public function getApplicantsNotProgressing() {
		return $this->m_strApplicantsNotProgressing;
	}

	public function sqlApplicantsNotProgressing() {
		return ( true == isset( $this->m_strApplicantsNotProgressing ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApplicantsNotProgressing ) : '\'' . addslashes( $this->m_strApplicantsNotProgressing ) . '\'' ) : 'NULL';
	}

	public function setApplicantsGenerateLeases( $strApplicantsGenerateLeases ) {
		$this->set( 'm_strApplicantsGenerateLeases', CStrings::strTrimDef( $strApplicantsGenerateLeases, -1, NULL, true ) );
	}

	public function getApplicantsGenerateLeases() {
		return $this->m_strApplicantsGenerateLeases;
	}

	public function sqlApplicantsGenerateLeases() {
		return ( true == isset( $this->m_strApplicantsGenerateLeases ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApplicantsGenerateLeases ) : '\'' . addslashes( $this->m_strApplicantsGenerateLeases ) . '\'' ) : 'NULL';
	}

	public function setApplicantsMissingInsurance( $strApplicantsMissingInsurance ) {
		$this->set( 'm_strApplicantsMissingInsurance', CStrings::strTrimDef( $strApplicantsMissingInsurance, -1, NULL, true ) );
	}

	public function getApplicantsMissingInsurance() {
		return $this->m_strApplicantsMissingInsurance;
	}

	public function sqlApplicantsMissingInsurance() {
		return ( true == isset( $this->m_strApplicantsMissingInsurance ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApplicantsMissingInsurance ) : '\'' . addslashes( $this->m_strApplicantsMissingInsurance ) . '\'' ) : 'NULL';
	}

	public function setResidentsFollowUp( $strResidentsFollowUp ) {
		$this->set( 'm_strResidentsFollowUp', CStrings::strTrimDef( $strResidentsFollowUp, -1, NULL, true ) );
	}

	public function getResidentsFollowUp() {
		return $this->m_strResidentsFollowUp;
	}

	public function sqlResidentsFollowUp() {
		return ( true == isset( $this->m_strResidentsFollowUp ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsFollowUp ) : '\'' . addslashes( $this->m_strResidentsFollowUp ) . '\'' ) : 'NULL';
	}

	public function setResidentsMoveInReviews( $strResidentsMoveInReviews ) {
		$this->set( 'm_strResidentsMoveInReviews', CStrings::strTrimDef( $strResidentsMoveInReviews, -1, NULL, true ) );
	}

	public function getResidentsMoveInReviews() {
		return $this->m_strResidentsMoveInReviews;
	}

	public function sqlResidentsMoveInReviews() {
		return ( true == isset( $this->m_strResidentsMoveInReviews ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsMoveInReviews ) : '\'' . addslashes( $this->m_strResidentsMoveInReviews ) . '\'' ) : 'NULL';
	}

	public function setResidentsMoveIns( $strResidentsMoveIns ) {
		$this->set( 'm_strResidentsMoveIns', CStrings::strTrimDef( $strResidentsMoveIns, -1, NULL, true ) );
	}

	public function getResidentsMoveIns() {
		return $this->m_strResidentsMoveIns;
	}

	public function sqlResidentsMoveIns() {
		return ( true == isset( $this->m_strResidentsMoveIns ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsMoveIns ) : '\'' . addslashes( $this->m_strResidentsMoveIns ) . '\'' ) : 'NULL';
	}

	public function setResidentsEvictions( $strResidentsEvictions ) {
		$this->set( 'm_strResidentsEvictions', CStrings::strTrimDef( $strResidentsEvictions, -1, NULL, true ) );
	}

	public function getResidentsEvictions() {
		return $this->m_strResidentsEvictions;
	}

	public function sqlResidentsEvictions() {
		return ( true == isset( $this->m_strResidentsEvictions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsEvictions ) : '\'' . addslashes( $this->m_strResidentsEvictions ) . '\'' ) : 'NULL';
	}

	public function setResidentsMoveOuts( $strResidentsMoveOuts ) {
		$this->set( 'm_strResidentsMoveOuts', CStrings::strTrimDef( $strResidentsMoveOuts, -1, NULL, true ) );
	}

	public function getResidentsMoveOuts() {
		return $this->m_strResidentsMoveOuts;
	}

	public function sqlResidentsMoveOuts() {
		return ( true == isset( $this->m_strResidentsMoveOuts ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsMoveOuts ) : '\'' . addslashes( $this->m_strResidentsMoveOuts ) . '\'' ) : 'NULL';
	}

	public function setResidentsRenewals( $strResidentsRenewals ) {
		$this->set( 'm_strResidentsRenewals', CStrings::strTrimDef( $strResidentsRenewals, -1, NULL, true ) );
	}

	public function getResidentsRenewals() {
		return $this->m_strResidentsRenewals;
	}

	public function sqlResidentsRenewals() {
		return ( true == isset( $this->m_strResidentsRenewals ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsRenewals ) : '\'' . addslashes( $this->m_strResidentsRenewals ) . '\'' ) : 'NULL';
	}

	public function setResidentsTransfers( $strResidentsTransfers ) {
		$this->set( 'm_strResidentsTransfers', CStrings::strTrimDef( $strResidentsTransfers, -1, NULL, true ) );
	}

	public function getResidentsTransfers() {
		return $this->m_strResidentsTransfers;
	}

	public function sqlResidentsTransfers() {
		return ( true == isset( $this->m_strResidentsTransfers ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsTransfers ) : '\'' . addslashes( $this->m_strResidentsTransfers ) . '\'' ) : 'NULL';
	}

	public function setResidentsRentableItems( $strResidentsRentableItems ) {
		$this->set( 'm_strResidentsRentableItems', CStrings::strTrimDef( $strResidentsRentableItems, -1, NULL, true ) );
	}

	public function getResidentsRentableItems() {
		return $this->m_strResidentsRentableItems;
	}

	public function sqlResidentsRentableItems() {
		return ( true == isset( $this->m_strResidentsRentableItems ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsRentableItems ) : '\'' . addslashes( $this->m_strResidentsRentableItems ) . '\'' ) : 'NULL';
	}

	public function setResidentsInsurance( $strResidentsInsurance ) {
		$this->set( 'm_strResidentsInsurance', CStrings::strTrimDef( $strResidentsInsurance, -1, NULL, true ) );
	}

	public function getResidentsInsurance() {
		return $this->m_strResidentsInsurance;
	}

	public function sqlResidentsInsurance() {
		return ( true == isset( $this->m_strResidentsInsurance ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsInsurance ) : '\'' . addslashes( $this->m_strResidentsInsurance ) . '\'' ) : 'NULL';
	}

	public function setResidentsAdditionalSettings( $strResidentsAdditionalSettings ) {
		$this->set( 'm_strResidentsAdditionalSettings', CStrings::strTrimDef( $strResidentsAdditionalSettings, -1, NULL, true ) );
	}

	public function getResidentsAdditionalSettings() {
		return $this->m_strResidentsAdditionalSettings;
	}

	public function sqlResidentsAdditionalSettings() {
		return ( true == isset( $this->m_strResidentsAdditionalSettings ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsAdditionalSettings ) : '\'' . addslashes( $this->m_strResidentsAdditionalSettings ) . '\'' ) : 'NULL';
	}

	public function setResidentsNotProgressing( $strResidentsNotProgressing ) {
		$this->set( 'm_strResidentsNotProgressing', CStrings::strTrimDef( $strResidentsNotProgressing, -1, NULL, true ) );
	}

	public function getResidentsNotProgressing() {
		return $this->m_strResidentsNotProgressing;
	}

	public function sqlResidentsNotProgressing() {
		return ( true == isset( $this->m_strResidentsNotProgressing ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsNotProgressing ) : '\'' . addslashes( $this->m_strResidentsNotProgressing ) . '\'' ) : 'NULL';
	}

	public function setResidentPortalAmenityReservations( $strResidentPortalAmenityReservations ) {
		$this->set( 'm_strResidentPortalAmenityReservations', CStrings::strTrimDef( $strResidentPortalAmenityReservations, -1, NULL, true ) );
	}

	public function getResidentPortalAmenityReservations() {
		return $this->m_strResidentPortalAmenityReservations;
	}

	public function sqlResidentPortalAmenityReservations() {
		return ( true == isset( $this->m_strResidentPortalAmenityReservations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentPortalAmenityReservations ) : '\'' . addslashes( $this->m_strResidentPortalAmenityReservations ) . '\'' ) : 'NULL';
	}

	public function setResidentPortalClassifieds( $strResidentPortalClassifieds ) {
		$this->set( 'm_strResidentPortalClassifieds', CStrings::strTrimDef( $strResidentPortalClassifieds, -1, NULL, true ) );
	}

	public function getResidentPortalClassifieds() {
		return $this->m_strResidentPortalClassifieds;
	}

	public function sqlResidentPortalClassifieds() {
		return ( true == isset( $this->m_strResidentPortalClassifieds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentPortalClassifieds ) : '\'' . addslashes( $this->m_strResidentPortalClassifieds ) . '\'' ) : 'NULL';
	}

	public function setResidentPortalClubs( $strResidentPortalClubs ) {
		$this->set( 'm_strResidentPortalClubs', CStrings::strTrimDef( $strResidentPortalClubs, -1, NULL, true ) );
	}

	public function getResidentPortalClubs() {
		return $this->m_strResidentPortalClubs;
	}

	public function sqlResidentPortalClubs() {
		return ( true == isset( $this->m_strResidentPortalClubs ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentPortalClubs ) : '\'' . addslashes( $this->m_strResidentPortalClubs ) . '\'' ) : 'NULL';
	}

	public function setResidentPortalConcierge( $strResidentPortalConcierge ) {
		$this->set( 'm_strResidentPortalConcierge', CStrings::strTrimDef( $strResidentPortalConcierge, -1, NULL, true ) );
	}

	public function getResidentPortalConcierge() {
		return $this->m_strResidentPortalConcierge;
	}

	public function sqlResidentPortalConcierge() {
		return ( true == isset( $this->m_strResidentPortalConcierge ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentPortalConcierge ) : '\'' . addslashes( $this->m_strResidentPortalConcierge ) . '\'' ) : 'NULL';
	}

	public function setResidentPortalEvents( $strResidentPortalEvents ) {
		$this->set( 'm_strResidentPortalEvents', CStrings::strTrimDef( $strResidentPortalEvents, -1, NULL, true ) );
	}

	public function getResidentPortalEvents() {
		return $this->m_strResidentPortalEvents;
	}

	public function sqlResidentPortalEvents() {
		return ( true == isset( $this->m_strResidentPortalEvents ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentPortalEvents ) : '\'' . addslashes( $this->m_strResidentPortalEvents ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceAssignWorkOrders( $strMaintenanceAssignWorkOrders ) {
		$this->set( 'm_strMaintenanceAssignWorkOrders', CStrings::strTrimDef( $strMaintenanceAssignWorkOrders, -1, NULL, true ) );
	}

	public function getMaintenanceAssignWorkOrders() {
		return $this->m_strMaintenanceAssignWorkOrders;
	}

	public function sqlMaintenanceAssignWorkOrders() {
		return ( true == isset( $this->m_strMaintenanceAssignWorkOrders ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceAssignWorkOrders ) : '\'' . addslashes( $this->m_strMaintenanceAssignWorkOrders ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceWorkOrders( $strMaintenanceWorkOrders ) {
		$this->set( 'm_strMaintenanceWorkOrders', CStrings::strTrimDef( $strMaintenanceWorkOrders, -1, NULL, true ) );
	}

	public function getMaintenanceWorkOrders() {
		return $this->m_strMaintenanceWorkOrders;
	}

	public function sqlMaintenanceWorkOrders() {
		return ( true == isset( $this->m_strMaintenanceWorkOrders ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceWorkOrders ) : '\'' . addslashes( $this->m_strMaintenanceWorkOrders ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceRecurringWorkOrders( $strMaintenanceRecurringWorkOrders ) {
		$this->set( 'm_strMaintenanceRecurringWorkOrders', CStrings::strTrimDef( $strMaintenanceRecurringWorkOrders, -1, NULL, true ) );
	}

	public function getMaintenanceRecurringWorkOrders() {
		return $this->m_strMaintenanceRecurringWorkOrders;
	}

	public function sqlMaintenanceRecurringWorkOrders() {
		return ( true == isset( $this->m_strMaintenanceRecurringWorkOrders ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceRecurringWorkOrders ) : '\'' . addslashes( $this->m_strMaintenanceRecurringWorkOrders ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceMakeReadies( $strMaintenanceMakeReadies ) {
		$this->set( 'm_strMaintenanceMakeReadies', CStrings::strTrimDef( $strMaintenanceMakeReadies, -1, NULL, true ) );
	}

	public function getMaintenanceMakeReadies() {
		return $this->m_strMaintenanceMakeReadies;
	}

	public function sqlMaintenanceMakeReadies() {
		return ( true == isset( $this->m_strMaintenanceMakeReadies ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceMakeReadies ) : '\'' . addslashes( $this->m_strMaintenanceMakeReadies ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceInspections( $strMaintenanceInspections ) {
		$this->set( 'm_strMaintenanceInspections', CStrings::strTrimDef( $strMaintenanceInspections, -1, NULL, true ) );
	}

	public function getMaintenanceInspections() {
		return $this->m_strMaintenanceInspections;
	}

	public function sqlMaintenanceInspections() {
		return ( true == isset( $this->m_strMaintenanceInspections ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceInspections ) : '\'' . addslashes( $this->m_strMaintenanceInspections ) . '\'' ) : 'NULL';
	}

	public function setMaintenanceInspectionReviews( $strMaintenanceInspectionReviews ) {
		$this->set( 'm_strMaintenanceInspectionReviews', CStrings::strTrimDef( $strMaintenanceInspectionReviews, -1, NULL, true ) );
	}

	public function getMaintenanceInspectionReviews() {
		return $this->m_strMaintenanceInspectionReviews;
	}

	public function sqlMaintenanceInspectionReviews() {
		return ( true == isset( $this->m_strMaintenanceInspectionReviews ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMaintenanceInspectionReviews ) : '\'' . addslashes( $this->m_strMaintenanceInspectionReviews ) . '\'' ) : 'NULL';
	}

	public function setFinancialPayments( $strFinancialPayments ) {
		$this->set( 'm_strFinancialPayments', CStrings::strTrimDef( $strFinancialPayments, -1, NULL, true ) );
	}

	public function getFinancialPayments() {
		return $this->m_strFinancialPayments;
	}

	public function sqlFinancialPayments() {
		return ( true == isset( $this->m_strFinancialPayments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialPayments ) : '\'' . addslashes( $this->m_strFinancialPayments ) . '\'' ) : 'NULL';
	}

	public function setFinancialCheck21( $strFinancialCheck21 ) {
		$this->set( 'm_strFinancialCheck21', CStrings::strTrimDef( $strFinancialCheck21, -1, NULL, true ) );
	}

	public function getFinancialCheck21() {
		return $this->m_strFinancialCheck21;
	}

	public function sqlFinancialCheck21() {
		return ( true == isset( $this->m_strFinancialCheck21 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialCheck21 ) : '\'' . addslashes( $this->m_strFinancialCheck21 ) . '\'' ) : 'NULL';
	}

	public function setFinancialDeposits( $strFinancialDeposits ) {
		$this->set( 'm_strFinancialDeposits', CStrings::strTrimDef( $strFinancialDeposits, -1, NULL, true ) );
	}

	public function getFinancialDeposits() {
		return $this->m_strFinancialDeposits;
	}

	public function sqlFinancialDeposits() {
		return ( true == isset( $this->m_strFinancialDeposits ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialDeposits ) : '\'' . addslashes( $this->m_strFinancialDeposits ) . '\'' ) : 'NULL';
	}

	public function setFinancialNotices( $strFinancialNotices ) {
		$this->set( 'm_strFinancialNotices', CStrings::strTrimDef( $strFinancialNotices, -1, NULL, true ) );
	}

	public function getFinancialNotices() {
		return $this->m_strFinancialNotices;
	}

	public function sqlFinancialNotices() {
		return ( true == isset( $this->m_strFinancialNotices ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialNotices ) : '\'' . addslashes( $this->m_strFinancialNotices ) . '\'' ) : 'NULL';
	}

	public function setFinancialCollections( $strFinancialCollections ) {
		$this->set( 'm_strFinancialCollections', CStrings::strTrimDef( $strFinancialCollections, -1, NULL, true ) );
	}

	public function getFinancialCollections() {
		return $this->m_strFinancialCollections;
	}

	public function sqlFinancialCollections() {
		return ( true == isset( $this->m_strFinancialCollections ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialCollections ) : '\'' . addslashes( $this->m_strFinancialCollections ) . '\'' ) : 'NULL';
	}

	public function setFinancialDeliveryErrors( $strFinancialDeliveryErrors ) {
		$this->set( 'm_strFinancialDeliveryErrors', CStrings::strTrimDef( $strFinancialDeliveryErrors, -1, NULL, true ) );
	}

	public function getFinancialDeliveryErrors() {
		return $this->m_strFinancialDeliveryErrors;
	}

	public function sqlFinancialDeliveryErrors() {
		return ( true == isset( $this->m_strFinancialDeliveryErrors ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialDeliveryErrors ) : '\'' . addslashes( $this->m_strFinancialDeliveryErrors ) . '\'' ) : 'NULL';
	}

	public function setFinancialMoveOuts( $strFinancialMoveOuts ) {
		$this->set( 'm_strFinancialMoveOuts', CStrings::strTrimDef( $strFinancialMoveOuts, -1, NULL, true ) );
	}

	public function getFinancialMoveOuts() {
		return $this->m_strFinancialMoveOuts;
	}

	public function sqlFinancialMoveOuts() {
		return ( true == isset( $this->m_strFinancialMoveOuts ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialMoveOuts ) : '\'' . addslashes( $this->m_strFinancialMoveOuts ) . '\'' ) : 'NULL';
	}

	public function setFinancialAcceleratedRentCredits( $strFinancialAcceleratedRentCredits ) {
		$this->set( 'm_strFinancialAcceleratedRentCredits', CStrings::strTrimDef( $strFinancialAcceleratedRentCredits, -1, NULL, true ) );
	}

	public function getFinancialAcceleratedRentCredits() {
		return $this->m_strFinancialAcceleratedRentCredits;
	}

	public function sqlFinancialAcceleratedRentCredits() {
		return ( true == isset( $this->m_strFinancialAcceleratedRentCredits ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialAcceleratedRentCredits ) : '\'' . addslashes( $this->m_strFinancialAcceleratedRentCredits ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setApprovalsScheduledCharges( $strApprovalsScheduledCharges ) {
		$this->set( 'm_strApprovalsScheduledCharges', CStrings::strTrimDef( $strApprovalsScheduledCharges, -1, NULL, true ) );
	}

	public function getApprovalsScheduledCharges() {
		return $this->m_strApprovalsScheduledCharges;
	}

	public function sqlApprovalsScheduledCharges() {
		return ( true == isset( $this->m_strApprovalsScheduledCharges ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsScheduledCharges ) : '\'' . addslashes( $this->m_strApprovalsScheduledCharges ) . '\'' ) : 'NULL';
	}

	public function setApprovalsDocumentVerification( $strApprovalsDocumentVerification ) {
		$this->set( 'm_strApprovalsDocumentVerification', CStrings::strTrimDef( $strApprovalsDocumentVerification, -1, NULL, true ) );
	}

	public function getApprovalsDocumentVerification() {
		return $this->m_strApprovalsDocumentVerification;
	}

	public function sqlApprovalsDocumentVerification() {
		return ( true == isset( $this->m_strApprovalsDocumentVerification ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsDocumentVerification ) : '\'' . addslashes( $this->m_strApprovalsDocumentVerification ) . '\'' ) : 'NULL';
	}

	public function setResidentsViolations( $strResidentsViolations ) {
		$this->set( 'm_strResidentsViolations', CStrings::strTrimDef( $strResidentsViolations, -1, NULL, true ) );
	}

	public function getResidentsViolations() {
		return $this->m_strResidentsViolations;
	}

	public function sqlResidentsViolations() {
		return ( true == isset( $this->m_strResidentsViolations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsViolations ) : '\'' . addslashes( $this->m_strResidentsViolations ) . '\'' ) : 'NULL';
	}

	public function setApprovalsReasonableAccommodations( $strApprovalsReasonableAccommodations ) {
		$this->set( 'm_strApprovalsReasonableAccommodations', CStrings::strTrimDef( $strApprovalsReasonableAccommodations, -1, NULL, true ) );
	}

	public function getApprovalsReasonableAccommodations() {
		return $this->m_strApprovalsReasonableAccommodations;
	}

	public function sqlApprovalsReasonableAccommodations() {
		return ( true == isset( $this->m_strApprovalsReasonableAccommodations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovalsReasonableAccommodations ) : '\'' . addslashes( $this->m_strApprovalsReasonableAccommodations ) . '\'' ) : 'NULL';
	}

	public function setResidentsReasonableAccommodations( $strResidentsReasonableAccommodations ) {
		$this->set( 'm_strResidentsReasonableAccommodations', CStrings::strTrimDef( $strResidentsReasonableAccommodations, -1, NULL, true ) );
	}

	public function getResidentsReasonableAccommodations() {
		return $this->m_strResidentsReasonableAccommodations;
	}

	public function sqlResidentsReasonableAccommodations() {
		return ( true == isset( $this->m_strResidentsReasonableAccommodations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResidentsReasonableAccommodations ) : '\'' . addslashes( $this->m_strResidentsReasonableAccommodations ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, approvals_applications, approvals_invoices, approvals_reversals, approvals_esign_docs, approvals_renewal_offers, approvals_transfers, approvals_purchase_orders, approvals_checks, approvals_utilities, approvals_financial_move_outs, approvals_transactions, leads_assign_agents, leads_never_contacted, leads_follow_up, leads_not_progressing, leads_resident_referrals, leads_answered_calls, leads_unknown_email, applicants_follow_up, applicants_not_progressing, applicants_generate_leases, applicants_missing_insurance, residents_follow_up, residents_move_in_reviews, residents_move_ins, residents_evictions, residents_move_outs, residents_renewals, residents_transfers, residents_rentable_items, residents_insurance, residents_additional_settings, residents_not_progressing, resident_portal_amenity_reservations, resident_portal_classifieds, resident_portal_clubs, resident_portal_concierge, resident_portal_events, maintenance_assign_work_orders, maintenance_work_orders, maintenance_recurring_work_orders, maintenance_make_readies, maintenance_inspections, maintenance_inspection_reviews, financial_payments, financial_check21, financial_deposits, financial_notices, financial_collections, financial_delivery_errors, financial_move_outs, financial_accelerated_rent_credits, updated_by, updated_on, created_by, created_on, approvals_scheduled_charges, approvals_document_verification, residents_violations, approvals_reasonable_accommodations, residents_reasonable_accommodations )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApprovalsApplications() . ', ' .
						$this->sqlApprovalsInvoices() . ', ' .
						$this->sqlApprovalsReversals() . ', ' .
						$this->sqlApprovalsEsignDocs() . ', ' .
						$this->sqlApprovalsRenewalOffers() . ', ' .
						$this->sqlApprovalsTransfers() . ', ' .
						$this->sqlApprovalsPurchaseOrders() . ', ' .
						$this->sqlApprovalsChecks() . ', ' .
						$this->sqlApprovalsUtilities() . ', ' .
						$this->sqlApprovalsFinancialMoveOuts() . ', ' .
						$this->sqlApprovalsTransactions() . ', ' .
						$this->sqlLeadsAssignAgents() . ', ' .
						$this->sqlLeadsNeverContacted() . ', ' .
						$this->sqlLeadsFollowUp() . ', ' .
						$this->sqlLeadsNotProgressing() . ', ' .
						$this->sqlLeadsResidentReferrals() . ', ' .
						$this->sqlLeadsAnsweredCalls() . ', ' .
						$this->sqlLeadsUnknownEmail() . ', ' .
						$this->sqlApplicantsFollowUp() . ', ' .
						$this->sqlApplicantsNotProgressing() . ', ' .
						$this->sqlApplicantsGenerateLeases() . ', ' .
						$this->sqlApplicantsMissingInsurance() . ', ' .
						$this->sqlResidentsFollowUp() . ', ' .
						$this->sqlResidentsMoveInReviews() . ', ' .
						$this->sqlResidentsMoveIns() . ', ' .
						$this->sqlResidentsEvictions() . ', ' .
						$this->sqlResidentsMoveOuts() . ', ' .
						$this->sqlResidentsRenewals() . ', ' .
						$this->sqlResidentsTransfers() . ', ' .
						$this->sqlResidentsRentableItems() . ', ' .
						$this->sqlResidentsInsurance() . ', ' .
						$this->sqlResidentsAdditionalSettings() . ', ' .
						$this->sqlResidentsNotProgressing() . ', ' .
						$this->sqlResidentPortalAmenityReservations() . ', ' .
						$this->sqlResidentPortalClassifieds() . ', ' .
						$this->sqlResidentPortalClubs() . ', ' .
						$this->sqlResidentPortalConcierge() . ', ' .
						$this->sqlResidentPortalEvents() . ', ' .
						$this->sqlMaintenanceAssignWorkOrders() . ', ' .
						$this->sqlMaintenanceWorkOrders() . ', ' .
						$this->sqlMaintenanceRecurringWorkOrders() . ', ' .
						$this->sqlMaintenanceMakeReadies() . ', ' .
						$this->sqlMaintenanceInspections() . ', ' .
						$this->sqlMaintenanceInspectionReviews() . ', ' .
						$this->sqlFinancialPayments() . ', ' .
						$this->sqlFinancialCheck21() . ', ' .
						$this->sqlFinancialDeposits() . ', ' .
						$this->sqlFinancialNotices() . ', ' .
						$this->sqlFinancialCollections() . ', ' .
						$this->sqlFinancialDeliveryErrors() . ', ' .
						$this->sqlFinancialMoveOuts() . ', ' .
						$this->sqlFinancialAcceleratedRentCredits() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApprovalsScheduledCharges() . ', ' .
						$this->sqlApprovalsDocumentVerification() . ', ' .
						$this->sqlResidentsViolations() . ', ' .
						$this->sqlApprovalsReasonableAccommodations() . ', ' .
						$this->sqlResidentsReasonableAccommodations() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_applications = ' . $this->sqlApprovalsApplications(). ',' ; } elseif( true == array_key_exists( 'ApprovalsApplications', $this->getChangedColumns() ) ) { $strSql .= ' approvals_applications = ' . $this->sqlApprovalsApplications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_invoices = ' . $this->sqlApprovalsInvoices(). ',' ; } elseif( true == array_key_exists( 'ApprovalsInvoices', $this->getChangedColumns() ) ) { $strSql .= ' approvals_invoices = ' . $this->sqlApprovalsInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_reversals = ' . $this->sqlApprovalsReversals(). ',' ; } elseif( true == array_key_exists( 'ApprovalsReversals', $this->getChangedColumns() ) ) { $strSql .= ' approvals_reversals = ' . $this->sqlApprovalsReversals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_esign_docs = ' . $this->sqlApprovalsEsignDocs(). ',' ; } elseif( true == array_key_exists( 'ApprovalsEsignDocs', $this->getChangedColumns() ) ) { $strSql .= ' approvals_esign_docs = ' . $this->sqlApprovalsEsignDocs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_renewal_offers = ' . $this->sqlApprovalsRenewalOffers(). ',' ; } elseif( true == array_key_exists( 'ApprovalsRenewalOffers', $this->getChangedColumns() ) ) { $strSql .= ' approvals_renewal_offers = ' . $this->sqlApprovalsRenewalOffers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_transfers = ' . $this->sqlApprovalsTransfers(). ',' ; } elseif( true == array_key_exists( 'ApprovalsTransfers', $this->getChangedColumns() ) ) { $strSql .= ' approvals_transfers = ' . $this->sqlApprovalsTransfers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_purchase_orders = ' . $this->sqlApprovalsPurchaseOrders(). ',' ; } elseif( true == array_key_exists( 'ApprovalsPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' approvals_purchase_orders = ' . $this->sqlApprovalsPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_checks = ' . $this->sqlApprovalsChecks(). ',' ; } elseif( true == array_key_exists( 'ApprovalsChecks', $this->getChangedColumns() ) ) { $strSql .= ' approvals_checks = ' . $this->sqlApprovalsChecks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_utilities = ' . $this->sqlApprovalsUtilities(). ',' ; } elseif( true == array_key_exists( 'ApprovalsUtilities', $this->getChangedColumns() ) ) { $strSql .= ' approvals_utilities = ' . $this->sqlApprovalsUtilities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_financial_move_outs = ' . $this->sqlApprovalsFinancialMoveOuts(). ',' ; } elseif( true == array_key_exists( 'ApprovalsFinancialMoveOuts', $this->getChangedColumns() ) ) { $strSql .= ' approvals_financial_move_outs = ' . $this->sqlApprovalsFinancialMoveOuts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_transactions = ' . $this->sqlApprovalsTransactions(). ',' ; } elseif( true == array_key_exists( 'ApprovalsTransactions', $this->getChangedColumns() ) ) { $strSql .= ' approvals_transactions = ' . $this->sqlApprovalsTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_assign_agents = ' . $this->sqlLeadsAssignAgents(). ',' ; } elseif( true == array_key_exists( 'LeadsAssignAgents', $this->getChangedColumns() ) ) { $strSql .= ' leads_assign_agents = ' . $this->sqlLeadsAssignAgents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_never_contacted = ' . $this->sqlLeadsNeverContacted(). ',' ; } elseif( true == array_key_exists( 'LeadsNeverContacted', $this->getChangedColumns() ) ) { $strSql .= ' leads_never_contacted = ' . $this->sqlLeadsNeverContacted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_follow_up = ' . $this->sqlLeadsFollowUp(). ',' ; } elseif( true == array_key_exists( 'LeadsFollowUp', $this->getChangedColumns() ) ) { $strSql .= ' leads_follow_up = ' . $this->sqlLeadsFollowUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_not_progressing = ' . $this->sqlLeadsNotProgressing(). ',' ; } elseif( true == array_key_exists( 'LeadsNotProgressing', $this->getChangedColumns() ) ) { $strSql .= ' leads_not_progressing = ' . $this->sqlLeadsNotProgressing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_resident_referrals = ' . $this->sqlLeadsResidentReferrals(). ',' ; } elseif( true == array_key_exists( 'LeadsResidentReferrals', $this->getChangedColumns() ) ) { $strSql .= ' leads_resident_referrals = ' . $this->sqlLeadsResidentReferrals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_answered_calls = ' . $this->sqlLeadsAnsweredCalls(). ',' ; } elseif( true == array_key_exists( 'LeadsAnsweredCalls', $this->getChangedColumns() ) ) { $strSql .= ' leads_answered_calls = ' . $this->sqlLeadsAnsweredCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_unknown_email = ' . $this->sqlLeadsUnknownEmail(). ',' ; } elseif( true == array_key_exists( 'LeadsUnknownEmail', $this->getChangedColumns() ) ) { $strSql .= ' leads_unknown_email = ' . $this->sqlLeadsUnknownEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicants_follow_up = ' . $this->sqlApplicantsFollowUp(). ',' ; } elseif( true == array_key_exists( 'ApplicantsFollowUp', $this->getChangedColumns() ) ) { $strSql .= ' applicants_follow_up = ' . $this->sqlApplicantsFollowUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicants_not_progressing = ' . $this->sqlApplicantsNotProgressing(). ',' ; } elseif( true == array_key_exists( 'ApplicantsNotProgressing', $this->getChangedColumns() ) ) { $strSql .= ' applicants_not_progressing = ' . $this->sqlApplicantsNotProgressing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicants_generate_leases = ' . $this->sqlApplicantsGenerateLeases(). ',' ; } elseif( true == array_key_exists( 'ApplicantsGenerateLeases', $this->getChangedColumns() ) ) { $strSql .= ' applicants_generate_leases = ' . $this->sqlApplicantsGenerateLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicants_missing_insurance = ' . $this->sqlApplicantsMissingInsurance(). ',' ; } elseif( true == array_key_exists( 'ApplicantsMissingInsurance', $this->getChangedColumns() ) ) { $strSql .= ' applicants_missing_insurance = ' . $this->sqlApplicantsMissingInsurance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_follow_up = ' . $this->sqlResidentsFollowUp(). ',' ; } elseif( true == array_key_exists( 'ResidentsFollowUp', $this->getChangedColumns() ) ) { $strSql .= ' residents_follow_up = ' . $this->sqlResidentsFollowUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_move_in_reviews = ' . $this->sqlResidentsMoveInReviews(). ',' ; } elseif( true == array_key_exists( 'ResidentsMoveInReviews', $this->getChangedColumns() ) ) { $strSql .= ' residents_move_in_reviews = ' . $this->sqlResidentsMoveInReviews() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_move_ins = ' . $this->sqlResidentsMoveIns(). ',' ; } elseif( true == array_key_exists( 'ResidentsMoveIns', $this->getChangedColumns() ) ) { $strSql .= ' residents_move_ins = ' . $this->sqlResidentsMoveIns() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_evictions = ' . $this->sqlResidentsEvictions(). ',' ; } elseif( true == array_key_exists( 'ResidentsEvictions', $this->getChangedColumns() ) ) { $strSql .= ' residents_evictions = ' . $this->sqlResidentsEvictions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_move_outs = ' . $this->sqlResidentsMoveOuts(). ',' ; } elseif( true == array_key_exists( 'ResidentsMoveOuts', $this->getChangedColumns() ) ) { $strSql .= ' residents_move_outs = ' . $this->sqlResidentsMoveOuts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_renewals = ' . $this->sqlResidentsRenewals(). ',' ; } elseif( true == array_key_exists( 'ResidentsRenewals', $this->getChangedColumns() ) ) { $strSql .= ' residents_renewals = ' . $this->sqlResidentsRenewals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_transfers = ' . $this->sqlResidentsTransfers(). ',' ; } elseif( true == array_key_exists( 'ResidentsTransfers', $this->getChangedColumns() ) ) { $strSql .= ' residents_transfers = ' . $this->sqlResidentsTransfers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_rentable_items = ' . $this->sqlResidentsRentableItems(). ',' ; } elseif( true == array_key_exists( 'ResidentsRentableItems', $this->getChangedColumns() ) ) { $strSql .= ' residents_rentable_items = ' . $this->sqlResidentsRentableItems() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_insurance = ' . $this->sqlResidentsInsurance(). ',' ; } elseif( true == array_key_exists( 'ResidentsInsurance', $this->getChangedColumns() ) ) { $strSql .= ' residents_insurance = ' . $this->sqlResidentsInsurance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_additional_settings = ' . $this->sqlResidentsAdditionalSettings(). ',' ; } elseif( true == array_key_exists( 'ResidentsAdditionalSettings', $this->getChangedColumns() ) ) { $strSql .= ' residents_additional_settings = ' . $this->sqlResidentsAdditionalSettings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_not_progressing = ' . $this->sqlResidentsNotProgressing(). ',' ; } elseif( true == array_key_exists( 'ResidentsNotProgressing', $this->getChangedColumns() ) ) { $strSql .= ' residents_not_progressing = ' . $this->sqlResidentsNotProgressing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_portal_amenity_reservations = ' . $this->sqlResidentPortalAmenityReservations(). ',' ; } elseif( true == array_key_exists( 'ResidentPortalAmenityReservations', $this->getChangedColumns() ) ) { $strSql .= ' resident_portal_amenity_reservations = ' . $this->sqlResidentPortalAmenityReservations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_portal_classifieds = ' . $this->sqlResidentPortalClassifieds(). ',' ; } elseif( true == array_key_exists( 'ResidentPortalClassifieds', $this->getChangedColumns() ) ) { $strSql .= ' resident_portal_classifieds = ' . $this->sqlResidentPortalClassifieds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_portal_clubs = ' . $this->sqlResidentPortalClubs(). ',' ; } elseif( true == array_key_exists( 'ResidentPortalClubs', $this->getChangedColumns() ) ) { $strSql .= ' resident_portal_clubs = ' . $this->sqlResidentPortalClubs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_portal_concierge = ' . $this->sqlResidentPortalConcierge(). ',' ; } elseif( true == array_key_exists( 'ResidentPortalConcierge', $this->getChangedColumns() ) ) { $strSql .= ' resident_portal_concierge = ' . $this->sqlResidentPortalConcierge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_portal_events = ' . $this->sqlResidentPortalEvents(). ',' ; } elseif( true == array_key_exists( 'ResidentPortalEvents', $this->getChangedColumns() ) ) { $strSql .= ' resident_portal_events = ' . $this->sqlResidentPortalEvents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_assign_work_orders = ' . $this->sqlMaintenanceAssignWorkOrders(). ',' ; } elseif( true == array_key_exists( 'MaintenanceAssignWorkOrders', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_assign_work_orders = ' . $this->sqlMaintenanceAssignWorkOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_work_orders = ' . $this->sqlMaintenanceWorkOrders(). ',' ; } elseif( true == array_key_exists( 'MaintenanceWorkOrders', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_work_orders = ' . $this->sqlMaintenanceWorkOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_recurring_work_orders = ' . $this->sqlMaintenanceRecurringWorkOrders(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRecurringWorkOrders', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_recurring_work_orders = ' . $this->sqlMaintenanceRecurringWorkOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_make_readies = ' . $this->sqlMaintenanceMakeReadies(). ',' ; } elseif( true == array_key_exists( 'MaintenanceMakeReadies', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_make_readies = ' . $this->sqlMaintenanceMakeReadies() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_inspections = ' . $this->sqlMaintenanceInspections(). ',' ; } elseif( true == array_key_exists( 'MaintenanceInspections', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_inspections = ' . $this->sqlMaintenanceInspections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_inspection_reviews = ' . $this->sqlMaintenanceInspectionReviews(). ',' ; } elseif( true == array_key_exists( 'MaintenanceInspectionReviews', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_inspection_reviews = ' . $this->sqlMaintenanceInspectionReviews() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_payments = ' . $this->sqlFinancialPayments(). ',' ; } elseif( true == array_key_exists( 'FinancialPayments', $this->getChangedColumns() ) ) { $strSql .= ' financial_payments = ' . $this->sqlFinancialPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_check21 = ' . $this->sqlFinancialCheck21(). ',' ; } elseif( true == array_key_exists( 'FinancialCheck21', $this->getChangedColumns() ) ) { $strSql .= ' financial_check21 = ' . $this->sqlFinancialCheck21() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_deposits = ' . $this->sqlFinancialDeposits(). ',' ; } elseif( true == array_key_exists( 'FinancialDeposits', $this->getChangedColumns() ) ) { $strSql .= ' financial_deposits = ' . $this->sqlFinancialDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_notices = ' . $this->sqlFinancialNotices(). ',' ; } elseif( true == array_key_exists( 'FinancialNotices', $this->getChangedColumns() ) ) { $strSql .= ' financial_notices = ' . $this->sqlFinancialNotices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_collections = ' . $this->sqlFinancialCollections(). ',' ; } elseif( true == array_key_exists( 'FinancialCollections', $this->getChangedColumns() ) ) { $strSql .= ' financial_collections = ' . $this->sqlFinancialCollections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_delivery_errors = ' . $this->sqlFinancialDeliveryErrors(). ',' ; } elseif( true == array_key_exists( 'FinancialDeliveryErrors', $this->getChangedColumns() ) ) { $strSql .= ' financial_delivery_errors = ' . $this->sqlFinancialDeliveryErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_move_outs = ' . $this->sqlFinancialMoveOuts(). ',' ; } elseif( true == array_key_exists( 'FinancialMoveOuts', $this->getChangedColumns() ) ) { $strSql .= ' financial_move_outs = ' . $this->sqlFinancialMoveOuts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_accelerated_rent_credits = ' . $this->sqlFinancialAcceleratedRentCredits(). ',' ; } elseif( true == array_key_exists( 'FinancialAcceleratedRentCredits', $this->getChangedColumns() ) ) { $strSql .= ' financial_accelerated_rent_credits = ' . $this->sqlFinancialAcceleratedRentCredits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_scheduled_charges = ' . $this->sqlApprovalsScheduledCharges(). ',' ; } elseif( true == array_key_exists( 'ApprovalsScheduledCharges', $this->getChangedColumns() ) ) { $strSql .= ' approvals_scheduled_charges = ' . $this->sqlApprovalsScheduledCharges() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_document_verification = ' . $this->sqlApprovalsDocumentVerification(). ',' ; } elseif( true == array_key_exists( 'ApprovalsDocumentVerification', $this->getChangedColumns() ) ) { $strSql .= ' approvals_document_verification = ' . $this->sqlApprovalsDocumentVerification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_violations = ' . $this->sqlResidentsViolations(). ',' ; } elseif( true == array_key_exists( 'ResidentsViolations', $this->getChangedColumns() ) ) { $strSql .= ' residents_violations = ' . $this->sqlResidentsViolations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approvals_reasonable_accommodations = ' . $this->sqlApprovalsReasonableAccommodations(). ',' ; } elseif( true == array_key_exists( 'ApprovalsReasonableAccommodations', $this->getChangedColumns() ) ) { $strSql .= ' approvals_reasonable_accommodations = ' . $this->sqlApprovalsReasonableAccommodations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_reasonable_accommodations = ' . $this->sqlResidentsReasonableAccommodations(). ',' ; } elseif( true == array_key_exists( 'ResidentsReasonableAccommodations', $this->getChangedColumns() ) ) { $strSql .= ' residents_reasonable_accommodations = ' . $this->sqlResidentsReasonableAccommodations() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'approvals_applications' => $this->getApprovalsApplications(),
			'approvals_invoices' => $this->getApprovalsInvoices(),
			'approvals_reversals' => $this->getApprovalsReversals(),
			'approvals_esign_docs' => $this->getApprovalsEsignDocs(),
			'approvals_renewal_offers' => $this->getApprovalsRenewalOffers(),
			'approvals_transfers' => $this->getApprovalsTransfers(),
			'approvals_purchase_orders' => $this->getApprovalsPurchaseOrders(),
			'approvals_checks' => $this->getApprovalsChecks(),
			'approvals_utilities' => $this->getApprovalsUtilities(),
			'approvals_financial_move_outs' => $this->getApprovalsFinancialMoveOuts(),
			'approvals_transactions' => $this->getApprovalsTransactions(),
			'leads_assign_agents' => $this->getLeadsAssignAgents(),
			'leads_never_contacted' => $this->getLeadsNeverContacted(),
			'leads_follow_up' => $this->getLeadsFollowUp(),
			'leads_not_progressing' => $this->getLeadsNotProgressing(),
			'leads_resident_referrals' => $this->getLeadsResidentReferrals(),
			'leads_answered_calls' => $this->getLeadsAnsweredCalls(),
			'leads_unknown_email' => $this->getLeadsUnknownEmail(),
			'applicants_follow_up' => $this->getApplicantsFollowUp(),
			'applicants_not_progressing' => $this->getApplicantsNotProgressing(),
			'applicants_generate_leases' => $this->getApplicantsGenerateLeases(),
			'applicants_missing_insurance' => $this->getApplicantsMissingInsurance(),
			'residents_follow_up' => $this->getResidentsFollowUp(),
			'residents_move_in_reviews' => $this->getResidentsMoveInReviews(),
			'residents_move_ins' => $this->getResidentsMoveIns(),
			'residents_evictions' => $this->getResidentsEvictions(),
			'residents_move_outs' => $this->getResidentsMoveOuts(),
			'residents_renewals' => $this->getResidentsRenewals(),
			'residents_transfers' => $this->getResidentsTransfers(),
			'residents_rentable_items' => $this->getResidentsRentableItems(),
			'residents_insurance' => $this->getResidentsInsurance(),
			'residents_additional_settings' => $this->getResidentsAdditionalSettings(),
			'residents_not_progressing' => $this->getResidentsNotProgressing(),
			'resident_portal_amenity_reservations' => $this->getResidentPortalAmenityReservations(),
			'resident_portal_classifieds' => $this->getResidentPortalClassifieds(),
			'resident_portal_clubs' => $this->getResidentPortalClubs(),
			'resident_portal_concierge' => $this->getResidentPortalConcierge(),
			'resident_portal_events' => $this->getResidentPortalEvents(),
			'maintenance_assign_work_orders' => $this->getMaintenanceAssignWorkOrders(),
			'maintenance_work_orders' => $this->getMaintenanceWorkOrders(),
			'maintenance_recurring_work_orders' => $this->getMaintenanceRecurringWorkOrders(),
			'maintenance_make_readies' => $this->getMaintenanceMakeReadies(),
			'maintenance_inspections' => $this->getMaintenanceInspections(),
			'maintenance_inspection_reviews' => $this->getMaintenanceInspectionReviews(),
			'financial_payments' => $this->getFinancialPayments(),
			'financial_check21' => $this->getFinancialCheck21(),
			'financial_deposits' => $this->getFinancialDeposits(),
			'financial_notices' => $this->getFinancialNotices(),
			'financial_collections' => $this->getFinancialCollections(),
			'financial_delivery_errors' => $this->getFinancialDeliveryErrors(),
			'financial_move_outs' => $this->getFinancialMoveOuts(),
			'financial_accelerated_rent_credits' => $this->getFinancialAcceleratedRentCredits(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'approvals_scheduled_charges' => $this->getApprovalsScheduledCharges(),
			'approvals_document_verification' => $this->getApprovalsDocumentVerification(),
			'residents_violations' => $this->getResidentsViolations(),
			'approvals_reasonable_accommodations' => $this->getApprovalsReasonableAccommodations(),
			'residents_reasonable_accommodations' => $this->getResidentsReasonableAccommodations()
		);
	}

}
?>