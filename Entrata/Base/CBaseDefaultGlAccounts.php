<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlAccounts
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlAccounts extends CEosPluralBase {

	/**
	 * @return CDefaultGlAccount[]
	 */
	public static function fetchDefaultGlAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultGlAccount', $objDatabase );
	}

	/**
	 * @return CDefaultGlAccount
	 */
	public static function fetchDefaultGlAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultGlAccount', $objDatabase );
	}

	public static function fetchDefaultGlAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_accounts', $objDatabase );
	}

	public static function fetchDefaultGlAccountById( $intId, $objDatabase ) {
		return self::fetchDefaultGlAccount( sprintf( 'SELECT * FROM default_gl_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountsByDefaultGlChartId( $intDefaultGlChartId, $objDatabase ) {
		return self::fetchDefaultGlAccounts( sprintf( 'SELECT * FROM default_gl_accounts WHERE default_gl_chart_id = %d', ( int ) $intDefaultGlChartId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountsByGlAccountTypeId( $intGlAccountTypeId, $objDatabase ) {
		return self::fetchDefaultGlAccounts( sprintf( 'SELECT * FROM default_gl_accounts WHERE gl_account_type_id = %d', ( int ) $intGlAccountTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountsByCashFlowTypeId( $intCashFlowTypeId, $objDatabase ) {
		return self::fetchDefaultGlAccounts( sprintf( 'SELECT * FROM default_gl_accounts WHERE cash_flow_type_id = %d', ( int ) $intCashFlowTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountsByGlAccountUsageTypeId( $intGlAccountUsageTypeId, $objDatabase ) {
		return self::fetchDefaultGlAccounts( sprintf( 'SELECT * FROM default_gl_accounts WHERE gl_account_usage_type_id = %d', ( int ) $intGlAccountUsageTypeId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountsByCamAllowanceTypeId( $intCamAllowanceTypeId, $objDatabase ) {
		return self::fetchDefaultGlAccounts( sprintf( 'SELECT * FROM default_gl_accounts WHERE cam_allowance_type_id = %d', ( int ) $intCamAllowanceTypeId ), $objDatabase );
	}

}
?>