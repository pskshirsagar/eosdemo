<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentCenterServices
 * Do not add any new functions to this class.
 */

class CBaseResidentCenterServices extends CEosPluralBase {

	/**
	 * @return CResidentCenterService[]
	 */
	public static function fetchResidentCenterServices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CResidentCenterService::class, $objDatabase );
	}

	/**
	 * @return CResidentCenterService
	 */
	public static function fetchResidentCenterService( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CResidentCenterService::class, $objDatabase );
	}

	public static function fetchResidentCenterServiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resident_center_services', $objDatabase );
	}

	public static function fetchResidentCenterServiceById( $intId, $objDatabase ) {
		return self::fetchResidentCenterService( sprintf( 'SELECT * FROM resident_center_services WHERE id = %d', $intId ), $objDatabase );
	}

}
?>