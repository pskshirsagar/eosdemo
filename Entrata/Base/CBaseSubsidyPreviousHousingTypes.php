<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyPreviousHousingTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyPreviousHousingTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyPreviousHousingType[]
	 */
	public static function fetchSubsidyPreviousHousingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyPreviousHousingType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyPreviousHousingType
	 */
	public static function fetchSubsidyPreviousHousingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyPreviousHousingType::class, $objDatabase );
	}

	public static function fetchSubsidyPreviousHousingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_previous_housing_types', $objDatabase );
	}

	public static function fetchSubsidyPreviousHousingTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyPreviousHousingType( sprintf( 'SELECT * FROM subsidy_previous_housing_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>