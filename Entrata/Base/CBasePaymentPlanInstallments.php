<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePaymentPlanInstallments extends CEosPluralBase {

    const TABLE_PAYMENT_PLAN_INSTALLMENTS = 'public.payment_plan_installments';

    public static function fetchPaymentPlanInstallments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CPaymentPlanInstallment', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchPaymentPlanInstallment( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPaymentPlanInstallment', $objDatabase );
    }

    public static function fetchPaymentPlanInstallmentCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'payment_plan_installments', $objDatabase );
    }

    public static function fetchPaymentPlanInstallmentByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchPaymentPlanInstallment( sprintf( 'SELECT * FROM payment_plan_installments WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPaymentPlanInstallmentsByCid( $intCid, $objDatabase ) {
        return self::fetchPaymentPlanInstallments( sprintf( 'SELECT * FROM payment_plan_installments WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchPaymentPlanInstallmentsByPaymentPlanIdByCid( $intPaymentPlanId, $intCid, $objDatabase ) {
        return self::fetchPaymentPlanInstallments( sprintf( 'SELECT * FROM payment_plan_installments WHERE payment_plan_id = %d AND cid = %d', (int) $intPaymentPlanId, (int) $intCid ), $objDatabase );
    }

}
?>