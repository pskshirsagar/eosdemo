<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantAssets extends CEosPluralBase {

    public static function fetchApplicantAssets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CApplicantAsset', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchApplicantAsset( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApplicantAsset', $objDatabase );
    }

    public static function fetchApplicantAssetCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'applicant_assets', $objDatabase );
    }

    public static function fetchApplicantAssetByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchApplicantAsset( sprintf( 'SELECT * FROM applicant_assets WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantAssetsByCid( $intCid, $objDatabase ) {
        return self::fetchApplicantAssets( sprintf( 'SELECT * FROM applicant_assets WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantAssetsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
        return self::fetchApplicantAssets( sprintf( 'SELECT * FROM applicant_assets WHERE applicant_id = %d AND cid = %d', (int) $intApplicantId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantAssetsByApplicantAssetTypeIdByCid( $intApplicantAssetTypeId, $intCid, $objDatabase ) {
        return self::fetchApplicantAssets( sprintf( 'SELECT * FROM applicant_assets WHERE applicant_asset_type_id = %d AND cid = %d', (int) $intApplicantAssetTypeId, (int) $intCid ), $objDatabase );
    }

}
?>