<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetAssumptionTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetAssumptionTypes extends CEosPluralBase {

	/**
	 * @return CBudgetAssumptionType[]
	 */
	public static function fetchBudgetAssumptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetAssumptionType::class, $objDatabase );
	}

	/**
	 * @return CBudgetAssumptionType
	 */
	public static function fetchBudgetAssumptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetAssumptionType::class, $objDatabase );
	}

	public static function fetchBudgetAssumptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_assumption_types', $objDatabase );
	}

	public static function fetchBudgetAssumptionTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetAssumptionType( sprintf( 'SELECT * FROM budget_assumption_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>