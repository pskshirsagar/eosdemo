<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartDeviceTypes
 * Do not add any new functions to this class.
 */

class CBaseSmartDeviceTypes extends CEosPluralBase {

	/**
	 * @return CSmartDeviceType[]
	 */
	public static function fetchSmartDeviceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSmartDeviceType::class, $objDatabase );
	}

	/**
	 * @return CSmartDeviceType
	 */
	public static function fetchSmartDeviceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSmartDeviceType::class, $objDatabase );
	}

	public static function fetchSmartDeviceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'smart_device_types', $objDatabase );
	}

	public static function fetchSmartDeviceTypeById( $intId, $objDatabase ) {
		return self::fetchSmartDeviceType( sprintf( 'SELECT * FROM smart_device_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>