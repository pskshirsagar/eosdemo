<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialSuiteLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialSuiteLogs extends CEosPluralBase {

	/**
	 * @return CCommercialSuiteLog[]
	 */
	public static function fetchCommercialSuiteLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCommercialSuiteLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialSuiteLog
	 */
	public static function fetchCommercialSuiteLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommercialSuiteLog', $objDatabase );
	}

	public static function fetchCommercialSuiteLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_suite_logs', $objDatabase );
	}

	public static function fetchCommercialSuiteLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLog( sprintf( 'SELECT * FROM commercial_suite_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE property_floor_id = %d AND cid = %d', ( int ) $intPropertyFloorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByCommercialSuiteIdByCid( $intCommercialSuiteId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE commercial_suite_id = %d AND cid = %d', ( int ) $intCommercialSuiteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuiteLogsByPriorCommercialSuiteLogIdByCid( $intPriorCommercialSuiteLogId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuiteLogs( sprintf( 'SELECT * FROM commercial_suite_logs WHERE prior_commercial_suite_log_id = %d AND cid = %d', ( int ) $intPriorCommercialSuiteLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>