<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocument extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintCompanyApplicationIds;
	protected $m_intDocumentTypeId;
	protected $m_intDocumentSubTypeId;
	protected $m_intFileTypeId;
	protected $m_intArTriggerId;
	protected $m_arrintLeaseIntervalTypeIds;
	protected $m_intDocumentId;
	protected $m_intOriginalDocumentId;
	protected $m_intRestoredDocumentId;
	protected $m_intDocumentInclusionTypeId;
	protected $m_intCompanyMediaFileId;
	protected $m_intOccupancyTypeId;
	protected $m_intTransmissionVendorId;
	protected $m_strTitle;
	protected $m_strName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strDescription;
	protected $m_strInstructions;
	protected $m_strKeywords;
	protected $m_strContent;
	protected $m_strSeoUri;
	protected $m_strLinkUri;
	protected $m_strRedirectUrl;
	protected $m_strTarget;
	protected $m_strRemotePrimaryKey;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strActivateOn;
	protected $m_strDeactivateOn;
	protected $m_strArchivedOn;
	protected $m_intRequirePrintedForm;
	protected $m_intRequireLogin;
	protected $m_boolIsAdhoc;
	protected $m_boolIsPacket;
	protected $m_boolIsBluemoon;
	protected $m_intIsArchitect;
	protected $m_intIsPublished;
	protected $m_intIsSystem;
	protected $m_boolIsEntrataDefault;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intLocaleDocumentId;
	protected $m_strLocaleCode;

	public function __construct() {
		parent::__construct();

		$this->m_intRequirePrintedForm = '0';
		$this->m_intRequireLogin = '0';
		$this->m_boolIsAdhoc = false;
		$this->m_boolIsPacket = false;
		$this->m_boolIsBluemoon = false;
		$this->m_intIsArchitect = '1';
		$this->m_intIsPublished = '1';
		$this->m_intIsSystem = '0';
		$this->m_boolIsEntrataDefault = false;
		$this->m_intOrderNum = '0';
		$this->m_strLocaleCode = 'en_US';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_application_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintCompanyApplicationIds', trim( $arrValues['company_application_ids'] ) ); elseif( isset( $arrValues['company_application_ids'] ) ) $this->setCompanyApplicationIds( $arrValues['company_application_ids'] );
		if( isset( $arrValues['document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTypeId', trim( $arrValues['document_type_id'] ) ); elseif( isset( $arrValues['document_type_id'] ) ) $this->setDocumentTypeId( $arrValues['document_type_id'] );
		if( isset( $arrValues['document_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentSubTypeId', trim( $arrValues['document_sub_type_id'] ) ); elseif( isset( $arrValues['document_sub_type_id'] ) ) $this->setDocumentSubTypeId( $arrValues['document_sub_type_id'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['lease_interval_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintLeaseIntervalTypeIds', trim( $arrValues['lease_interval_type_ids'] ) ); elseif( isset( $arrValues['lease_interval_type_ids'] ) ) $this->setLeaseIntervalTypeIds( $arrValues['lease_interval_type_ids'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['original_document_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalDocumentId', trim( $arrValues['original_document_id'] ) ); elseif( isset( $arrValues['original_document_id'] ) ) $this->setOriginalDocumentId( $arrValues['original_document_id'] );
		if( isset( $arrValues['restored_document_id'] ) && $boolDirectSet ) $this->set( 'm_intRestoredDocumentId', trim( $arrValues['restored_document_id'] ) ); elseif( isset( $arrValues['restored_document_id'] ) ) $this->setRestoredDocumentId( $arrValues['restored_document_id'] );
		if( isset( $arrValues['document_inclusion_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentInclusionTypeId', trim( $arrValues['document_inclusion_type_id'] ) ); elseif( isset( $arrValues['document_inclusion_type_id'] ) ) $this->setDocumentInclusionTypeId( $arrValues['document_inclusion_type_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['instructions'] ) && $boolDirectSet ) $this->set( 'm_strInstructions', trim( stripcslashes( $arrValues['instructions'] ) ) ); elseif( isset( $arrValues['instructions'] ) ) $this->setInstructions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructions'] ) : $arrValues['instructions'] );
		if( isset( $arrValues['keywords'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strKeywords', trim( stripcslashes( $arrValues['keywords'] ) ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keywords'] ) : $arrValues['keywords'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['seo_uri'] ) && $boolDirectSet ) $this->set( 'm_strSeoUri', trim( stripcslashes( $arrValues['seo_uri'] ) ) ); elseif( isset( $arrValues['seo_uri'] ) ) $this->setSeoUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['seo_uri'] ) : $arrValues['seo_uri'] );
		if( isset( $arrValues['link_uri'] ) && $boolDirectSet ) $this->set( 'm_strLinkUri', trim( stripcslashes( $arrValues['link_uri'] ) ) ); elseif( isset( $arrValues['link_uri'] ) ) $this->setLinkUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_uri'] ) : $arrValues['link_uri'] );
		if( isset( $arrValues['redirect_url'] ) && $boolDirectSet ) $this->set( 'm_strRedirectUrl', trim( stripcslashes( $arrValues['redirect_url'] ) ) ); elseif( isset( $arrValues['redirect_url'] ) ) $this->setRedirectUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['redirect_url'] ) : $arrValues['redirect_url'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( stripcslashes( $arrValues['target'] ) ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['target'] ) : $arrValues['target'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['activate_on'] ) && $boolDirectSet ) $this->set( 'm_strActivateOn', trim( $arrValues['activate_on'] ) ); elseif( isset( $arrValues['activate_on'] ) ) $this->setActivateOn( $arrValues['activate_on'] );
		if( isset( $arrValues['deactivate_on'] ) && $boolDirectSet ) $this->set( 'm_strDeactivateOn', trim( $arrValues['deactivate_on'] ) ); elseif( isset( $arrValues['deactivate_on'] ) ) $this->setDeactivateOn( $arrValues['deactivate_on'] );
		if( isset( $arrValues['archived_on'] ) && $boolDirectSet ) $this->set( 'm_strArchivedOn', trim( $arrValues['archived_on'] ) ); elseif( isset( $arrValues['archived_on'] ) ) $this->setArchivedOn( $arrValues['archived_on'] );
		if( isset( $arrValues['require_printed_form'] ) && $boolDirectSet ) $this->set( 'm_intRequirePrintedForm', trim( $arrValues['require_printed_form'] ) ); elseif( isset( $arrValues['require_printed_form'] ) ) $this->setRequirePrintedForm( $arrValues['require_printed_form'] );
		if( isset( $arrValues['require_login'] ) && $boolDirectSet ) $this->set( 'm_intRequireLogin', trim( $arrValues['require_login'] ) ); elseif( isset( $arrValues['require_login'] ) ) $this->setRequireLogin( $arrValues['require_login'] );
		if( isset( $arrValues['is_adhoc'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdhoc', trim( stripcslashes( $arrValues['is_adhoc'] ) ) ); elseif( isset( $arrValues['is_adhoc'] ) ) $this->setIsAdhoc( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_adhoc'] ) : $arrValues['is_adhoc'] );
		if( isset( $arrValues['is_packet'] ) && $boolDirectSet ) $this->set( 'm_boolIsPacket', trim( stripcslashes( $arrValues['is_packet'] ) ) ); elseif( isset( $arrValues['is_packet'] ) ) $this->setIsPacket( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_packet'] ) : $arrValues['is_packet'] );
		if( isset( $arrValues['is_bluemoon'] ) && $boolDirectSet ) $this->set( 'm_boolIsBluemoon', trim( stripcslashes( $arrValues['is_bluemoon'] ) ) ); elseif( isset( $arrValues['is_bluemoon'] ) ) $this->setIsBluemoon( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_bluemoon'] ) : $arrValues['is_bluemoon'] );
		if( isset( $arrValues['is_architect'] ) && $boolDirectSet ) $this->set( 'm_intIsArchitect', trim( $arrValues['is_architect'] ) ); elseif( isset( $arrValues['is_architect'] ) ) $this->setIsArchitect( $arrValues['is_architect'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_entrata_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsEntrataDefault', trim( stripcslashes( $arrValues['is_entrata_default'] ) ) ); elseif( isset( $arrValues['is_entrata_default'] ) ) $this->setIsEntrataDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_entrata_default'] ) : $arrValues['is_entrata_default'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['locale_document_id'] ) && $boolDirectSet ) $this->set( 'm_intLocaleDocumentId', trim( $arrValues['locale_document_id'] ) ); elseif( isset( $arrValues['locale_document_id'] ) ) $this->setLocaleDocumentId( $arrValues['locale_document_id'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyApplicationIds( $arrintCompanyApplicationIds ) {
		$this->set( 'm_arrintCompanyApplicationIds', CStrings::strToArrIntDef( $arrintCompanyApplicationIds, NULL ) );
	}

	public function getCompanyApplicationIds() {
		return $this->m_arrintCompanyApplicationIds;
	}

	public function sqlCompanyApplicationIds() {
		return ( true == isset( $this->m_arrintCompanyApplicationIds ) && true == valArr( $this->m_arrintCompanyApplicationIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCompanyApplicationIds, NULL ) . '\'' : 'NULL';
	}

	public function setDocumentTypeId( $intDocumentTypeId ) {
		$this->set( 'm_intDocumentTypeId', CStrings::strToIntDef( $intDocumentTypeId, NULL, false ) );
	}

	public function getDocumentTypeId() {
		return $this->m_intDocumentTypeId;
	}

	public function sqlDocumentTypeId() {
		return ( true == isset( $this->m_intDocumentTypeId ) ) ? ( string ) $this->m_intDocumentTypeId : 'NULL';
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->set( 'm_intDocumentSubTypeId', CStrings::strToIntDef( $intDocumentSubTypeId, NULL, false ) );
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function sqlDocumentSubTypeId() {
		return ( true == isset( $this->m_intDocumentSubTypeId ) ) ? ( string ) $this->m_intDocumentSubTypeId : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setLeaseIntervalTypeIds( $arrintLeaseIntervalTypeIds ) {
		$this->set( 'm_arrintLeaseIntervalTypeIds', CStrings::strToArrIntDef( $arrintLeaseIntervalTypeIds, NULL ) );
	}

	public function getLeaseIntervalTypeIds() {
		return $this->m_arrintLeaseIntervalTypeIds;
	}

	public function sqlLeaseIntervalTypeIds() {
		return ( true == isset( $this->m_arrintLeaseIntervalTypeIds ) && true == valArr( $this->m_arrintLeaseIntervalTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintLeaseIntervalTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setOriginalDocumentId( $intOriginalDocumentId ) {
		$this->set( 'm_intOriginalDocumentId', CStrings::strToIntDef( $intOriginalDocumentId, NULL, false ) );
	}

	public function getOriginalDocumentId() {
		return $this->m_intOriginalDocumentId;
	}

	public function sqlOriginalDocumentId() {
		return ( true == isset( $this->m_intOriginalDocumentId ) ) ? ( string ) $this->m_intOriginalDocumentId : 'NULL';
	}

	public function setRestoredDocumentId( $intRestoredDocumentId ) {
		$this->set( 'm_intRestoredDocumentId', CStrings::strToIntDef( $intRestoredDocumentId, NULL, false ) );
	}

	public function getRestoredDocumentId() {
		return $this->m_intRestoredDocumentId;
	}

	public function sqlRestoredDocumentId() {
		return ( true == isset( $this->m_intRestoredDocumentId ) ) ? ( string ) $this->m_intRestoredDocumentId : 'NULL';
	}

	public function setDocumentInclusionTypeId( $intDocumentInclusionTypeId ) {
		$this->set( 'm_intDocumentInclusionTypeId', CStrings::strToIntDef( $intDocumentInclusionTypeId, NULL, false ) );
	}

	public function getDocumentInclusionTypeId() {
		return $this->m_intDocumentInclusionTypeId;
	}

	public function sqlDocumentInclusionTypeId() {
		return ( true == isset( $this->m_intDocumentInclusionTypeId ) ) ? ( string ) $this->m_intDocumentInclusionTypeId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2500, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstructions( $strInstructions ) {
		$this->set( 'm_strInstructions', CStrings::strTrimDef( $strInstructions, 2000, NULL, true ) );
	}

	public function getInstructions() {
		return $this->m_strInstructions;
	}

	public function sqlInstructions() {
		return ( true == isset( $this->m_strInstructions ) ) ? '\'' . addslashes( $this->m_strInstructions ) . '\'' : 'NULL';
	}

	public function setKeywords( $strKeywords, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 240, NULL, true ), $strLocaleCode );
	}

	public function getKeywords( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strKeywords', $strLocaleCode );
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? '\'' . addslashes( $this->m_strKeywords ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setSeoUri( $strSeoUri ) {
		$this->set( 'm_strSeoUri', CStrings::strTrimDef( $strSeoUri, 4096, NULL, true ) );
	}

	public function getSeoUri() {
		return $this->m_strSeoUri;
	}

	public function sqlSeoUri() {
		return ( true == isset( $this->m_strSeoUri ) ) ? '\'' . addslashes( $this->m_strSeoUri ) . '\'' : 'NULL';
	}

	public function setLinkUri( $strLinkUri ) {
		$this->set( 'm_strLinkUri', CStrings::strTrimDef( $strLinkUri, 4096, NULL, true ) );
	}

	public function getLinkUri() {
		return $this->m_strLinkUri;
	}

	public function sqlLinkUri() {
		return ( true == isset( $this->m_strLinkUri ) ) ? '\'' . addslashes( $this->m_strLinkUri ) . '\'' : 'NULL';
	}

	public function setRedirectUrl( $strRedirectUrl ) {
		$this->set( 'm_strRedirectUrl', CStrings::strTrimDef( $strRedirectUrl, 4096, NULL, true ) );
	}

	public function getRedirectUrl() {
		return $this->m_strRedirectUrl;
	}

	public function sqlRedirectUrl() {
		return ( true == isset( $this->m_strRedirectUrl ) ) ? '\'' . addslashes( $this->m_strRedirectUrl ) . '\'' : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, 50, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? '\'' . addslashes( $this->m_strTarget ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setActivateOn( $strActivateOn ) {
		$this->set( 'm_strActivateOn', CStrings::strTrimDef( $strActivateOn, -1, NULL, true ) );
	}

	public function getActivateOn() {
		return $this->m_strActivateOn;
	}

	public function sqlActivateOn() {
		return ( true == isset( $this->m_strActivateOn ) ) ? '\'' . $this->m_strActivateOn . '\'' : 'NULL';
	}

	public function setDeactivateOn( $strDeactivateOn ) {
		$this->set( 'm_strDeactivateOn', CStrings::strTrimDef( $strDeactivateOn, -1, NULL, true ) );
	}

	public function getDeactivateOn() {
		return $this->m_strDeactivateOn;
	}

	public function sqlDeactivateOn() {
		return ( true == isset( $this->m_strDeactivateOn ) ) ? '\'' . $this->m_strDeactivateOn . '\'' : 'NULL';
	}

	public function setArchivedOn( $strArchivedOn ) {
		$this->set( 'm_strArchivedOn', CStrings::strTrimDef( $strArchivedOn, -1, NULL, true ) );
	}

	public function getArchivedOn() {
		return $this->m_strArchivedOn;
	}

	public function sqlArchivedOn() {
		return ( true == isset( $this->m_strArchivedOn ) ) ? '\'' . $this->m_strArchivedOn . '\'' : 'NULL';
	}

	public function setRequirePrintedForm( $intRequirePrintedForm ) {
		$this->set( 'm_intRequirePrintedForm', CStrings::strToIntDef( $intRequirePrintedForm, NULL, false ) );
	}

	public function getRequirePrintedForm() {
		return $this->m_intRequirePrintedForm;
	}

	public function sqlRequirePrintedForm() {
		return ( true == isset( $this->m_intRequirePrintedForm ) ) ? ( string ) $this->m_intRequirePrintedForm : '0';
	}

	public function setRequireLogin( $intRequireLogin ) {
		$this->set( 'm_intRequireLogin', CStrings::strToIntDef( $intRequireLogin, NULL, false ) );
	}

	public function getRequireLogin() {
		return $this->m_intRequireLogin;
	}

	public function sqlRequireLogin() {
		return ( true == isset( $this->m_intRequireLogin ) ) ? ( string ) $this->m_intRequireLogin : '0';
	}

	public function setIsAdhoc( $boolIsAdhoc ) {
		$this->set( 'm_boolIsAdhoc', CStrings::strToBool( $boolIsAdhoc ) );
	}

	public function getIsAdhoc() {
		return $this->m_boolIsAdhoc;
	}

	public function sqlIsAdhoc() {
		return ( true == isset( $this->m_boolIsAdhoc ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdhoc ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPacket( $boolIsPacket ) {
		$this->set( 'm_boolIsPacket', CStrings::strToBool( $boolIsPacket ) );
	}

	public function getIsPacket() {
		return $this->m_boolIsPacket;
	}

	public function sqlIsPacket() {
		return ( true == isset( $this->m_boolIsPacket ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPacket ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBluemoon( $boolIsBluemoon ) {
		$this->set( 'm_boolIsBluemoon', CStrings::strToBool( $boolIsBluemoon ) );
	}

	public function getIsBluemoon() {
		return $this->m_boolIsBluemoon;
	}

	public function sqlIsBluemoon() {
		return ( true == isset( $this->m_boolIsBluemoon ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBluemoon ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsArchitect( $intIsArchitect ) {
		$this->set( 'm_intIsArchitect', CStrings::strToIntDef( $intIsArchitect, NULL, false ) );
	}

	public function getIsArchitect() {
		return $this->m_intIsArchitect;
	}

	public function sqlIsArchitect() {
		return ( true == isset( $this->m_intIsArchitect ) ) ? ( string ) $this->m_intIsArchitect : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsEntrataDefault( $boolIsEntrataDefault ) {
		$this->set( 'm_boolIsEntrataDefault', CStrings::strToBool( $boolIsEntrataDefault ) );
	}

	public function getIsEntrataDefault() {
		return $this->m_boolIsEntrataDefault;
	}

	public function sqlIsEntrataDefault() {
		return ( true == isset( $this->m_boolIsEntrataDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEntrataDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLocaleDocumentId( $intLocaleDocumentId ) {
		$this->set( 'm_intLocaleDocumentId', CStrings::strToIntDef( $intLocaleDocumentId, NULL, false ) );
	}

	public function getLocaleDocumentId() {
		return $this->m_intLocaleDocumentId;
	}

	public function sqlLocaleDocumentId() {
		return ( true == isset( $this->m_intLocaleDocumentId ) ) ? ( string ) $this->m_intLocaleDocumentId : 'NULL';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 16, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : '\'en_US\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_application_ids, document_type_id, document_sub_type_id, file_type_id, ar_trigger_id, lease_interval_type_ids, document_id, original_document_id, restored_document_id, document_inclusion_type_id, company_media_file_id, occupancy_type_id, transmission_vendor_id, title, name, details, description, instructions, keywords, content, seo_uri, link_uri, redirect_url, target, remote_primary_key, file_name, file_path, activate_on, deactivate_on, archived_on, require_printed_form, require_login, is_adhoc, is_packet, is_bluemoon, is_architect, is_published, is_system, is_entrata_default, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, locale_document_id, locale_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyApplicationIds() . ', ' .
						$this->sqlDocumentTypeId() . ', ' .
						$this->sqlDocumentSubTypeId() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlLeaseIntervalTypeIds() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlOriginalDocumentId() . ', ' .
						$this->sqlRestoredDocumentId() . ', ' .
						$this->sqlDocumentInclusionTypeId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlInstructions() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlContent() . ', ' .
						$this->sqlSeoUri() . ', ' .
						$this->sqlLinkUri() . ', ' .
						$this->sqlRedirectUrl() . ', ' .
						$this->sqlTarget() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlActivateOn() . ', ' .
						$this->sqlDeactivateOn() . ', ' .
						$this->sqlArchivedOn() . ', ' .
						$this->sqlRequirePrintedForm() . ', ' .
						$this->sqlRequireLogin() . ', ' .
						$this->sqlIsAdhoc() . ', ' .
						$this->sqlIsPacket() . ', ' .
						$this->sqlIsBluemoon() . ', ' .
						$this->sqlIsArchitect() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsEntrataDefault() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLocaleDocumentId() . ', ' .
						$this->sqlLocaleCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_ids = ' . $this->sqlCompanyApplicationIds(). ',' ; } elseif( true == array_key_exists( 'CompanyApplicationIds', $this->getChangedColumns() ) ) { $strSql .= ' company_application_ids = ' . $this->sqlCompanyApplicationIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'DocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId(). ',' ; } elseif( true == array_key_exists( 'DocumentSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId(). ',' ; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_ids = ' . $this->sqlLeaseIntervalTypeIds(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_ids = ' . $this->sqlLeaseIntervalTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_document_id = ' . $this->sqlOriginalDocumentId(). ',' ; } elseif( true == array_key_exists( 'OriginalDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' original_document_id = ' . $this->sqlOriginalDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restored_document_id = ' . $this->sqlRestoredDocumentId(). ',' ; } elseif( true == array_key_exists( 'RestoredDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' restored_document_id = ' . $this->sqlRestoredDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_inclusion_type_id = ' . $this->sqlDocumentInclusionTypeId(). ',' ; } elseif( true == array_key_exists( 'DocumentInclusionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_inclusion_type_id = ' . $this->sqlDocumentInclusionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions = ' . $this->sqlInstructions(). ',' ; } elseif( true == array_key_exists( 'Instructions', $this->getChangedColumns() ) ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent(). ',' ; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_uri = ' . $this->sqlSeoUri(). ',' ; } elseif( true == array_key_exists( 'SeoUri', $this->getChangedColumns() ) ) { $strSql .= ' seo_uri = ' . $this->sqlSeoUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_uri = ' . $this->sqlLinkUri(). ',' ; } elseif( true == array_key_exists( 'LinkUri', $this->getChangedColumns() ) ) { $strSql .= ' link_uri = ' . $this->sqlLinkUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl(). ',' ; } elseif( true == array_key_exists( 'RedirectUrl', $this->getChangedColumns() ) ) { $strSql .= ' redirect_url = ' . $this->sqlRedirectUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget(). ',' ; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_on = ' . $this->sqlActivateOn(). ',' ; } elseif( true == array_key_exists( 'ActivateOn', $this->getChangedColumns() ) ) { $strSql .= ' activate_on = ' . $this->sqlActivateOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivate_on = ' . $this->sqlDeactivateOn(). ',' ; } elseif( true == array_key_exists( 'DeactivateOn', $this->getChangedColumns() ) ) { $strSql .= ' deactivate_on = ' . $this->sqlDeactivateOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn(). ',' ; } elseif( true == array_key_exists( 'ArchivedOn', $this->getChangedColumns() ) ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm(). ',' ; } elseif( true == array_key_exists( 'RequirePrintedForm', $this->getChangedColumns() ) ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_login = ' . $this->sqlRequireLogin(). ',' ; } elseif( true == array_key_exists( 'RequireLogin', $this->getChangedColumns() ) ) { $strSql .= ' require_login = ' . $this->sqlRequireLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adhoc = ' . $this->sqlIsAdhoc(). ',' ; } elseif( true == array_key_exists( 'IsAdhoc', $this->getChangedColumns() ) ) { $strSql .= ' is_adhoc = ' . $this->sqlIsAdhoc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_packet = ' . $this->sqlIsPacket(). ',' ; } elseif( true == array_key_exists( 'IsPacket', $this->getChangedColumns() ) ) { $strSql .= ' is_packet = ' . $this->sqlIsPacket() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bluemoon = ' . $this->sqlIsBluemoon(). ',' ; } elseif( true == array_key_exists( 'IsBluemoon', $this->getChangedColumns() ) ) { $strSql .= ' is_bluemoon = ' . $this->sqlIsBluemoon() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_architect = ' . $this->sqlIsArchitect(). ',' ; } elseif( true == array_key_exists( 'IsArchitect', $this->getChangedColumns() ) ) { $strSql .= ' is_architect = ' . $this->sqlIsArchitect() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_entrata_default = ' . $this->sqlIsEntrataDefault(). ',' ; } elseif( true == array_key_exists( 'IsEntrataDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_entrata_default = ' . $this->sqlIsEntrataDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_document_id = ' . $this->sqlLocaleDocumentId(). ',' ; } elseif( true == array_key_exists( 'LocaleDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' locale_document_id = ' . $this->sqlLocaleDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_application_ids' => $this->getCompanyApplicationIds(),
			'document_type_id' => $this->getDocumentTypeId(),
			'document_sub_type_id' => $this->getDocumentSubTypeId(),
			'file_type_id' => $this->getFileTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'lease_interval_type_ids' => $this->getLeaseIntervalTypeIds(),
			'document_id' => $this->getDocumentId(),
			'original_document_id' => $this->getOriginalDocumentId(),
			'restored_document_id' => $this->getRestoredDocumentId(),
			'document_inclusion_type_id' => $this->getDocumentInclusionTypeId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'title' => $this->getTitle(),
			'name' => $this->getName(),
			'details' => $this->getDetails(),
			'description' => $this->getDescription(),
			'instructions' => $this->getInstructions(),
			'keywords' => $this->getKeywords(),
			'content' => $this->getContent(),
			'seo_uri' => $this->getSeoUri(),
			'link_uri' => $this->getLinkUri(),
			'redirect_url' => $this->getRedirectUrl(),
			'target' => $this->getTarget(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'activate_on' => $this->getActivateOn(),
			'deactivate_on' => $this->getDeactivateOn(),
			'archived_on' => $this->getArchivedOn(),
			'require_printed_form' => $this->getRequirePrintedForm(),
			'require_login' => $this->getRequireLogin(),
			'is_adhoc' => $this->getIsAdhoc(),
			'is_packet' => $this->getIsPacket(),
			'is_bluemoon' => $this->getIsBluemoon(),
			'is_architect' => $this->getIsArchitect(),
			'is_published' => $this->getIsPublished(),
			'is_system' => $this->getIsSystem(),
			'is_entrata_default' => $this->getIsEntrataDefault(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'locale_document_id' => $this->getLocaleDocumentId(),
			'locale_code' => $this->getLocaleCode()
		);
	}

}
?>