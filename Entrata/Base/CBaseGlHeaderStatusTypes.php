<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseGlHeaderStatusTypes extends CEosPluralBase {

	/**
	 * @return CGlHeaderStatusType[]
	 */
	public static function fetchGlHeaderStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlHeaderStatusType::class, $objDatabase );
	}

	/**
	 * @return CGlHeaderStatusType
	 */
	public static function fetchGlHeaderStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlHeaderStatusType::class, $objDatabase );
	}

	public static function fetchGlHeaderStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_status_types', $objDatabase );
	}

	public static function fetchGlHeaderStatusTypeById( $intId, $objDatabase ) {
		return self::fetchGlHeaderStatusType( sprintf( 'SELECT * FROM gl_header_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>