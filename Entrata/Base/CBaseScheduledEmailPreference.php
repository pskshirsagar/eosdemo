<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledEmailPreference extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intScheduledEmailId;
    protected $m_strKey;
    protected $m_strValue;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->m_intScheduledEmailId = trim( $arrValues['scheduled_email_id'] ); else if( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
        if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->m_strKey = trim( stripcslashes( $arrValues['key'] ) ); else if( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
        if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->m_strValue = trim( stripcslashes( $arrValues['value'] ) ); else if( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setScheduledEmailId( $intScheduledEmailId ) {
        $this->m_intScheduledEmailId = CStrings::strToIntDef( $intScheduledEmailId, NULL, false );
    }

    public function getScheduledEmailId() {
        return $this->m_intScheduledEmailId;
    }

    public function sqlScheduledEmailId() {
        return ( true == isset( $this->m_intScheduledEmailId ) ) ? (string) $this->m_intScheduledEmailId : 'NULL';
    }

    public function setKey( $strKey ) {
        $this->m_strKey = CStrings::strTrimDef( $strKey, 64, NULL, true );
    }

    public function getKey() {
        return $this->m_strKey;
    }

    public function sqlKey() {
        return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
    }

    public function setValue( $strValue ) {
        $this->m_strValue = CStrings::strTrimDef( $strValue, -1, NULL, true );
    }

    public function getValue() {
        return $this->m_strValue;
    }

    public function sqlValue() {
        return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.scheduled_email_preferences_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.scheduled_email_preferences					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlScheduledEmailId() . ', ' . 		                $this->sqlKey() . ', ' . 		                $this->sqlValue() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.scheduled_email_preferences
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlScheduledEmailId() ) != $this->getOriginalValueByFieldName ( 'scheduled_email_id' ) ) { $arrstrOriginalValueChanges['scheduled_email_id'] = $this->sqlScheduledEmailId(); $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlKey() ) != $this->getOriginalValueByFieldName ( 'key' ) ) { $arrstrOriginalValueChanges['key'] = $this->sqlKey(); $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlValue() ) != $this->getOriginalValueByFieldName ( 'value' ) ) { $arrstrOriginalValueChanges['value'] = $this->sqlValue(); $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.scheduled_email_preferences WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.scheduled_email_preferences_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'scheduled_email_id' => $this->getScheduledEmailId(),
        	'key' => $this->getKey(),
        	'value' => $this->getValue(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>