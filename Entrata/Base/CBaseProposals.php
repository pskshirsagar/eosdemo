<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposals
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProposals extends CEosPluralBase {

	/**
	 * @return CProposal[]
	 */
	public static function fetchProposals( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CProposal', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CProposal
	 */
	public static function fetchProposal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProposal', $objDatabase );
	}

	public static function fetchProposalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'proposals', $objDatabase );
	}

	public static function fetchProposalByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchProposal( sprintf( 'SELECT * FROM proposals WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalsByCid( $intCid, $objDatabase ) {
		return self::fetchProposals( sprintf( 'SELECT * FROM proposals WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalsByProposalStatusTypeIdByCid( $intProposalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchProposals( sprintf( 'SELECT * FROM proposals WHERE proposal_status_type_id = %d AND cid = %d', ( int ) $intProposalStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchProposals( sprintf( 'SELECT * FROM proposals WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchProposals( sprintf( 'SELECT * FROM proposals WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

}
?>