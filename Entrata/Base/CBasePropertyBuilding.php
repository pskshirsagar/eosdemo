<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBuilding extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_buildings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intBuildingTypeId;
	protected $m_intCompanyMediaFileId;
	protected $m_intTimeZoneId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSecondaryNumber;
	protected $m_strBuildingName;
	protected $m_strMarketingName;
	protected $m_strDescription;
	protected $m_strDrivingDirections;
	protected $m_intNumberOfUnits;
	protected $m_fltTotalSquareFeet;
	protected $m_strBuildingCoordinates;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_fltRentableSquareFeet;
	protected $m_fltUsableSquareFeet;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['building_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBuildingTypeId', trim( $arrValues['building_type_id'] ) ); elseif( isset( $arrValues['building_type_id'] ) ) $this->setBuildingTypeId( $arrValues['building_type_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( $arrValues['secondary_number'] ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrValues['secondary_number'] );
		if( isset( $arrValues['building_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['marketing_name'] ) && $boolDirectSet ) $this->set( 'm_strMarketingName', trim( $arrValues['marketing_name'] ) ); elseif( isset( $arrValues['marketing_name'] ) ) $this->setMarketingName( $arrValues['marketing_name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['driving_directions'] ) && $boolDirectSet ) $this->set( 'm_strDrivingDirections', trim( $arrValues['driving_directions'] ) ); elseif( isset( $arrValues['driving_directions'] ) ) $this->setDrivingDirections( $arrValues['driving_directions'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['total_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSquareFeet', trim( $arrValues['total_square_feet'] ) ); elseif( isset( $arrValues['total_square_feet'] ) ) $this->setTotalSquareFeet( $arrValues['total_square_feet'] );
		if( isset( $arrValues['building_coordinates'] ) && $boolDirectSet ) $this->set( 'm_strBuildingCoordinates', trim( $arrValues['building_coordinates'] ) ); elseif( isset( $arrValues['building_coordinates'] ) ) $this->setBuildingCoordinates( $arrValues['building_coordinates'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['rentable_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltRentableSquareFeet', trim( $arrValues['rentable_square_feet'] ) ); elseif( isset( $arrValues['rentable_square_feet'] ) ) $this->setRentableSquareFeet( $arrValues['rentable_square_feet'] );
		if( isset( $arrValues['usable_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltUsableSquareFeet', trim( $arrValues['usable_square_feet'] ) ); elseif( isset( $arrValues['usable_square_feet'] ) ) $this->setUsableSquareFeet( $arrValues['usable_square_feet'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBuildingTypeId( $intBuildingTypeId ) {
		$this->set( 'm_intBuildingTypeId', CStrings::strToIntDef( $intBuildingTypeId, NULL, false ) );
	}

	public function getBuildingTypeId() {
		return $this->m_intBuildingTypeId;
	}

	public function sqlBuildingTypeId() {
		return ( true == isset( $this->m_intBuildingTypeId ) ) ? ( string ) $this->m_intBuildingTypeId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 64, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecondaryNumber ) : '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ), $strLocaleCode );
	}

	public function getBuildingName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBuildingName', $strLocaleCode );
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setMarketingName( $strMarketingName ) {
		$this->set( 'm_strMarketingName', CStrings::strTrimDef( $strMarketingName, 50, NULL, true ) );
	}

	public function getMarketingName() {
		return $this->m_strMarketingName;
	}

	public function sqlMarketingName() {
		return ( true == isset( $this->m_strMarketingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingName ) : '\'' . addslashes( $this->m_strMarketingName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDrivingDirections( $strDrivingDirections ) {
		$this->set( 'm_strDrivingDirections', CStrings::strTrimDef( $strDrivingDirections, -1, NULL, true ) );
	}

	public function getDrivingDirections() {
		return $this->m_strDrivingDirections;
	}

	public function sqlDrivingDirections() {
		return ( true == isset( $this->m_strDrivingDirections ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDrivingDirections ) : '\'' . addslashes( $this->m_strDrivingDirections ) . '\'' ) : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setTotalSquareFeet( $fltTotalSquareFeet ) {
		$this->set( 'm_fltTotalSquareFeet', CStrings::strToFloatDef( $fltTotalSquareFeet, NULL, false, 4 ) );
	}

	public function getTotalSquareFeet() {
		return $this->m_fltTotalSquareFeet;
	}

	public function sqlTotalSquareFeet() {
		return ( true == isset( $this->m_fltTotalSquareFeet ) ) ? ( string ) $this->m_fltTotalSquareFeet : 'NULL';
	}

	public function setBuildingCoordinates( $strBuildingCoordinates ) {
		$this->set( 'm_strBuildingCoordinates', CStrings::strTrimDef( $strBuildingCoordinates, 2000, NULL, true ) );
	}

	public function getBuildingCoordinates() {
		return $this->m_strBuildingCoordinates;
	}

	public function sqlBuildingCoordinates() {
		return ( true == isset( $this->m_strBuildingCoordinates ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingCoordinates ) : '\'' . addslashes( $this->m_strBuildingCoordinates ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRentableSquareFeet( $fltRentableSquareFeet ) {
		$this->set( 'm_fltRentableSquareFeet', CStrings::strToFloatDef( $fltRentableSquareFeet, NULL, false, 4 ) );
	}

	public function getRentableSquareFeet() {
		return $this->m_fltRentableSquareFeet;
	}

	public function sqlRentableSquareFeet() {
		return ( true == isset( $this->m_fltRentableSquareFeet ) ) ? ( string ) $this->m_fltRentableSquareFeet : 'NULL';
	}

	public function setUsableSquareFeet( $fltUsableSquareFeet ) {
		$this->set( 'm_fltUsableSquareFeet', CStrings::strToFloatDef( $fltUsableSquareFeet, NULL, false, 4 ) );
	}

	public function getUsableSquareFeet() {
		return $this->m_fltUsableSquareFeet;
	}

	public function sqlUsableSquareFeet() {
		return ( true == isset( $this->m_fltUsableSquareFeet ) ) ? ( string ) $this->m_fltUsableSquareFeet : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, building_type_id, company_media_file_id, time_zone_id, remote_primary_key, secondary_number, building_name, marketing_name, description, driving_directions, number_of_units, total_square_feet, building_coordinates, order_num, imported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, rentable_square_feet, usable_square_feet )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlBuildingTypeId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlTimeZoneId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSecondaryNumber() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlMarketingName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDrivingDirections() . ', ' .
						$this->sqlNumberOfUnits() . ', ' .
						$this->sqlTotalSquareFeet() . ', ' .
						$this->sqlBuildingCoordinates() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlRentableSquareFeet() . ', ' .
						$this->sqlUsableSquareFeet() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_type_id = ' . $this->sqlBuildingTypeId(). ',' ; } elseif( true == array_key_exists( 'BuildingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' building_type_id = ' . $this->sqlBuildingTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId(). ',' ; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber(). ',' ; } elseif( true == array_key_exists( 'SecondaryNumber', $this->getChangedColumns() ) ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName(). ',' ; } elseif( true == array_key_exists( 'MarketingName', $this->getChangedColumns() ) ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections(). ',' ; } elseif( true == array_key_exists( 'DrivingDirections', $this->getChangedColumns() ) ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_square_feet = ' . $this->sqlTotalSquareFeet(). ',' ; } elseif( true == array_key_exists( 'TotalSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' total_square_feet = ' . $this->sqlTotalSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_coordinates = ' . $this->sqlBuildingCoordinates(). ',' ; } elseif( true == array_key_exists( 'BuildingCoordinates', $this->getChangedColumns() ) ) { $strSql .= ' building_coordinates = ' . $this->sqlBuildingCoordinates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_square_feet = ' . $this->sqlRentableSquareFeet(). ',' ; } elseif( true == array_key_exists( 'RentableSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' rentable_square_feet = ' . $this->sqlRentableSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usable_square_feet = ' . $this->sqlUsableSquareFeet(). ',' ; } elseif( true == array_key_exists( 'UsableSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' usable_square_feet = ' . $this->sqlUsableSquareFeet() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'building_type_id' => $this->getBuildingTypeId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'secondary_number' => $this->getSecondaryNumber(),
			'building_name' => $this->getBuildingName(),
			'marketing_name' => $this->getMarketingName(),
			'description' => $this->getDescription(),
			'driving_directions' => $this->getDrivingDirections(),
			'number_of_units' => $this->getNumberOfUnits(),
			'total_square_feet' => $this->getTotalSquareFeet(),
			'building_coordinates' => $this->getBuildingCoordinates(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'rentable_square_feet' => $this->getRentableSquareFeet(),
			'usable_square_feet' => $this->getUsableSquareFeet()
		);
	}

}
?>