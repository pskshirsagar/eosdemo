<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlDetailLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlDetailLogs extends CEosPluralBase {

	/**
	 * @return CGlDetailLog[]
	 */
	public static function fetchGlDetailLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlDetailLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlDetailLog
	 */
	public static function fetchGlDetailLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlDetailLog::class, $objDatabase );
	}

	public static function fetchGlDetailLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_detail_logs', $objDatabase );
	}

	public static function fetchGlDetailLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLog( sprintf( 'SELECT * FROM gl_detail_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByCid( $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlHeaderLogIdByCid( $intGlHeaderLogId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_header_log_id = %d AND cid = %d', $intGlHeaderLogId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_header_id = %d AND cid = %d', $intGlHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlDetailIdByCid( $intGlDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_detail_id = %d AND cid = %d', $intGlDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_transaction_type_id = %d AND cid = %d', $intGlTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByAccrualGlAccountIdByCid( $intAccrualGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE accrual_gl_account_id = %d AND cid = %d', $intAccrualGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByCashGlAccountIdByCid( $intCashGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE cash_gl_account_id = %d AND cid = %d', $intCashGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_reconciliation_id = %d AND cid = %d', $intGlReconciliationId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByApDetailIdByCid( $intApDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE ap_detail_id = %d AND cid = %d', $intApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByOffsettingGlDetailIdByCid( $intOffsettingGlDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE offsetting_gl_detail_id = %d AND cid = %d', $intOffsettingGlDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByCompanyDepartmentIdByCid( $intCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE company_department_id = %d AND cid = %d', $intCompanyDepartmentId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByGlDimensionIdByCid( $intGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE gl_dimension_id = %d AND cid = %d', $intGlDimensionId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE ap_code_id = %d AND cid = %d', $intApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByJobPhaseApDetailIdByCid( $intJobPhaseApDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE job_phase_ap_detail_id = %d AND cid = %d', $intJobPhaseApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByApContractApDetailIdByCid( $intApContractApDetailId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE ap_contract_ap_detail_id = %d AND cid = %d', $intApContractApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchGlDetailLogsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchGlDetailLogs( sprintf( 'SELECT * FROM gl_detail_logs WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

}
?>