<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorkbooks
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorkbooks extends CEosPluralBase {

	/**
	 * @return CBudgetWorkbook[]
	 */
	public static function fetchBudgetWorkbooks( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorkbook::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorkbook
	 */
	public static function fetchBudgetWorkbook( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorkbook::class, $objDatabase );
	}

	public static function fetchBudgetWorkbookCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_workbooks', $objDatabase );
	}

	public static function fetchBudgetWorkbookByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbook( sprintf( 'SELECT * FROM budget_workbooks WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbooksByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbooks( sprintf( 'SELECT * FROM budget_workbooks WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>