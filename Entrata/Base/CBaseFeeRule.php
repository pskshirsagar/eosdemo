<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeRule extends CEosSingularBase {

	const TABLE_NAME = 'public.fee_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFeeId;
	protected $m_intFeeTemplateId;
	protected $m_intFeeTemplateRuleTypeId;
	protected $m_intFeeTemplateRuleId;
	protected $m_intGlBranchId;
	protected $m_intGlAccountId;
	protected $m_intArCodeId;
	protected $m_strDescription;
	protected $m_fltFixedAmount;
	protected $m_fltPercent;
	protected $m_intIsAdjustsRevenue;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intGlGroupId;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['fee_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeId', trim( $arrValues['fee_id'] ) ); elseif( isset( $arrValues['fee_id'] ) ) $this->setFeeId( $arrValues['fee_id'] );
		if( isset( $arrValues['fee_template_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTemplateId', trim( $arrValues['fee_template_id'] ) ); elseif( isset( $arrValues['fee_template_id'] ) ) $this->setFeeTemplateId( $arrValues['fee_template_id'] );
		if( isset( $arrValues['fee_template_rule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTemplateRuleTypeId', trim( $arrValues['fee_template_rule_type_id'] ) ); elseif( isset( $arrValues['fee_template_rule_type_id'] ) ) $this->setFeeTemplateRuleTypeId( $arrValues['fee_template_rule_type_id'] );
		if( isset( $arrValues['fee_template_rule_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTemplateRuleId', trim( $arrValues['fee_template_rule_id'] ) ); elseif( isset( $arrValues['fee_template_rule_id'] ) ) $this->setFeeTemplateRuleId( $arrValues['fee_template_rule_id'] );
		if( isset( $arrValues['gl_branch_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBranchId', trim( $arrValues['gl_branch_id'] ) ); elseif( isset( $arrValues['gl_branch_id'] ) ) $this->setGlBranchId( $arrValues['gl_branch_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFixedAmount', trim( $arrValues['fixed_amount'] ) ); elseif( isset( $arrValues['fixed_amount'] ) ) $this->setFixedAmount( $arrValues['fixed_amount'] );
		if( isset( $arrValues['percent'] ) && $boolDirectSet ) $this->set( 'm_fltPercent', trim( $arrValues['percent'] ) ); elseif( isset( $arrValues['percent'] ) ) $this->setPercent( $arrValues['percent'] );
		if( isset( $arrValues['is_adjusts_revenue'] ) && $boolDirectSet ) $this->set( 'm_intIsAdjustsRevenue', trim( $arrValues['is_adjusts_revenue'] ) ); elseif( isset( $arrValues['is_adjusts_revenue'] ) ) $this->setIsAdjustsRevenue( $arrValues['is_adjusts_revenue'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupId', trim( $arrValues['gl_group_id'] ) ); elseif( isset( $arrValues['gl_group_id'] ) ) $this->setGlGroupId( $arrValues['gl_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFeeId( $intFeeId ) {
		$this->set( 'm_intFeeId', CStrings::strToIntDef( $intFeeId, NULL, false ) );
	}

	public function getFeeId() {
		return $this->m_intFeeId;
	}

	public function sqlFeeId() {
		return ( true == isset( $this->m_intFeeId ) ) ? ( string ) $this->m_intFeeId : 'NULL';
	}

	public function setFeeTemplateId( $intFeeTemplateId ) {
		$this->set( 'm_intFeeTemplateId', CStrings::strToIntDef( $intFeeTemplateId, NULL, false ) );
	}

	public function getFeeTemplateId() {
		return $this->m_intFeeTemplateId;
	}

	public function sqlFeeTemplateId() {
		return ( true == isset( $this->m_intFeeTemplateId ) ) ? ( string ) $this->m_intFeeTemplateId : 'NULL';
	}

	public function setFeeTemplateRuleTypeId( $intFeeTemplateRuleTypeId ) {
		$this->set( 'm_intFeeTemplateRuleTypeId', CStrings::strToIntDef( $intFeeTemplateRuleTypeId, NULL, false ) );
	}

	public function getFeeTemplateRuleTypeId() {
		return $this->m_intFeeTemplateRuleTypeId;
	}

	public function sqlFeeTemplateRuleTypeId() {
		return ( true == isset( $this->m_intFeeTemplateRuleTypeId ) ) ? ( string ) $this->m_intFeeTemplateRuleTypeId : 'NULL';
	}

	public function setFeeTemplateRuleId( $intFeeTemplateRuleId ) {
		$this->set( 'm_intFeeTemplateRuleId', CStrings::strToIntDef( $intFeeTemplateRuleId, NULL, false ) );
	}

	public function getFeeTemplateRuleId() {
		return $this->m_intFeeTemplateRuleId;
	}

	public function sqlFeeTemplateRuleId() {
		return ( true == isset( $this->m_intFeeTemplateRuleId ) ) ? ( string ) $this->m_intFeeTemplateRuleId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlBranchId( $intGlBranchId ) {
		$this->set( 'm_intGlBranchId', CStrings::strToIntDef( $intGlBranchId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlBranchId() {
		return $this->m_intGlBranchId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlBranchId() {
		return ( true == isset( $this->m_intGlBranchId ) ) ? ( string ) $this->m_intGlBranchId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setFixedAmount( $fltFixedAmount ) {
		$this->set( 'm_fltFixedAmount', CStrings::strToFloatDef( $fltFixedAmount, NULL, false, 2 ) );
	}

	public function getFixedAmount() {
		return $this->m_fltFixedAmount;
	}

	public function sqlFixedAmount() {
		return ( true == isset( $this->m_fltFixedAmount ) ) ? ( string ) $this->m_fltFixedAmount : 'NULL';
	}

	public function setPercent( $fltPercent ) {
		$this->set( 'm_fltPercent', CStrings::strToFloatDef( $fltPercent, NULL, false, 2 ) );
	}

	public function getPercent() {
		return $this->m_fltPercent;
	}

	public function sqlPercent() {
		return ( true == isset( $this->m_fltPercent ) ) ? ( string ) $this->m_fltPercent : 'NULL';
	}

	public function setIsAdjustsRevenue( $intIsAdjustsRevenue ) {
		$this->set( 'm_intIsAdjustsRevenue', CStrings::strToIntDef( $intIsAdjustsRevenue, NULL, false ) );
	}

	public function getIsAdjustsRevenue() {
		return $this->m_intIsAdjustsRevenue;
	}

	public function sqlIsAdjustsRevenue() {
		return ( true == isset( $this->m_intIsAdjustsRevenue ) ) ? ( string ) $this->m_intIsAdjustsRevenue : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGlGroupId( $intGlGroupId ) {
		$this->set( 'm_intGlGroupId', CStrings::strToIntDef( $intGlGroupId, NULL, false ) );
	}

	public function getGlGroupId() {
		return $this->m_intGlGroupId;
	}

	public function sqlGlGroupId() {
		return ( true == isset( $this->m_intGlGroupId ) ) ? ( string ) $this->m_intGlGroupId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, fee_id, fee_template_id, fee_template_rule_type_id, fee_template_rule_id, gl_branch_id, gl_account_id, ar_code_id, description, fixed_amount, percent, is_adjusts_revenue, order_num, deleted_by, deleted_on, created_by, created_on, gl_group_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlFeeId() . ', ' .
						$this->sqlFeeTemplateId() . ', ' .
						$this->sqlFeeTemplateRuleTypeId() . ', ' .
						$this->sqlFeeTemplateRuleId() . ', ' .
						$this->sqlGlBranchId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlFixedAmount() . ', ' .
						$this->sqlPercent() . ', ' .
						$this->sqlIsAdjustsRevenue() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlGlGroupId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_id = ' . $this->sqlFeeId(). ',' ; } elseif( true == array_key_exists( 'FeeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_id = ' . $this->sqlFeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_template_id = ' . $this->sqlFeeTemplateId(). ',' ; } elseif( true == array_key_exists( 'FeeTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' fee_template_id = ' . $this->sqlFeeTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_template_rule_type_id = ' . $this->sqlFeeTemplateRuleTypeId(). ',' ; } elseif( true == array_key_exists( 'FeeTemplateRuleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_template_rule_type_id = ' . $this->sqlFeeTemplateRuleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_template_rule_id = ' . $this->sqlFeeTemplateRuleId(). ',' ; } elseif( true == array_key_exists( 'FeeTemplateRuleId', $this->getChangedColumns() ) ) { $strSql .= ' fee_template_rule_id = ' . $this->sqlFeeTemplateRuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId(). ',' ; } elseif( true == array_key_exists( 'GlBranchId', $this->getChangedColumns() ) ) { $strSql .= ' gl_branch_id = ' . $this->sqlGlBranchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount(). ',' ; } elseif( true == array_key_exists( 'FixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent = ' . $this->sqlPercent(). ',' ; } elseif( true == array_key_exists( 'Percent', $this->getChangedColumns() ) ) { $strSql .= ' percent = ' . $this->sqlPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjusts_revenue = ' . $this->sqlIsAdjustsRevenue(). ',' ; } elseif( true == array_key_exists( 'IsAdjustsRevenue', $this->getChangedColumns() ) ) { $strSql .= ' is_adjusts_revenue = ' . $this->sqlIsAdjustsRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId() ; } elseif( true == array_key_exists( 'GlGroupId', $this->getChangedColumns() ) ) { $strSql .= ' gl_group_id = ' . $this->sqlGlGroupId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'fee_id' => $this->getFeeId(),
			'fee_template_id' => $this->getFeeTemplateId(),
			'fee_template_rule_type_id' => $this->getFeeTemplateRuleTypeId(),
			'fee_template_rule_id' => $this->getFeeTemplateRuleId(),
			'gl_branch_id' => $this->getGlBranchId(),
			'gl_account_id' => $this->getGlAccountId(),
			'ar_code_id' => $this->getArCodeId(),
			'description' => $this->getDescription(),
			'fixed_amount' => $this->getFixedAmount(),
			'percent' => $this->getPercent(),
			'is_adjusts_revenue' => $this->getIsAdjustsRevenue(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'gl_group_id' => $this->getGlGroupId()
		);
	}

}
?>