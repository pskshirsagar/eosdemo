<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderScheduleLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeaderScheduleLogs extends CEosPluralBase {

	/**
	 * @return CGlHeaderScheduleLog[]
	 */
	public static function fetchGlHeaderScheduleLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlHeaderScheduleLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlHeaderScheduleLog
	 */
	public static function fetchGlHeaderScheduleLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlHeaderScheduleLog', $objDatabase );
	}

	public static function fetchGlHeaderScheduleLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_schedule_logs', $objDatabase );
	}

	public static function fetchGlHeaderScheduleLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderScheduleLog( sprintf( 'SELECT * FROM gl_header_schedule_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderScheduleLogsByCid( $intCid, $objDatabase ) {
		return self::fetchGlHeaderScheduleLogs( sprintf( 'SELECT * FROM gl_header_schedule_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderScheduleLogsByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderScheduleLogs( sprintf( 'SELECT * FROM gl_header_schedule_logs WHERE gl_header_schedule_id = %d AND cid = %d', ( int ) $intGlHeaderScheduleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeaderScheduleLogsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaderScheduleLogs( sprintf( 'SELECT * FROM gl_header_schedule_logs WHERE frequency_id = %d AND cid = %d', ( int ) $intFrequencyId, ( int ) $intCid ), $objDatabase );
	}

}
?>