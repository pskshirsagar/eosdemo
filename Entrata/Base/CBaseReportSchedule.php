<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportSchedule extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.report_schedules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReportScheduleTypeId;
	protected $m_intCompanyUserId;
	protected $m_intReportGroupId;
	protected $m_intReportNewGroupId;
	protected $m_intReportNewInstanceId;
	protected $m_intReportFilterId;
	protected $m_intReportVersionId;
	protected $m_intScheduledTaskId;
	protected $m_strDownloadOptions;
	protected $m_jsonDownloadOptions;
	protected $m_boolSaveToDocuments;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolSaveToDocuments = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['report_schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportScheduleTypeId', trim( $arrValues['report_schedule_type_id'] ) ); elseif( isset( $arrValues['report_schedule_type_id'] ) ) $this->setReportScheduleTypeId( $arrValues['report_schedule_type_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['report_group_id'] ) && $boolDirectSet ) $this->set( 'm_intReportGroupId', trim( $arrValues['report_group_id'] ) ); elseif( isset( $arrValues['report_group_id'] ) ) $this->setReportGroupId( $arrValues['report_group_id'] );
		if( isset( $arrValues['report_new_group_id'] ) && $boolDirectSet ) $this->set( 'm_intReportNewGroupId', trim( $arrValues['report_new_group_id'] ) ); elseif( isset( $arrValues['report_new_group_id'] ) ) $this->setReportNewGroupId( $arrValues['report_new_group_id'] );
		if( isset( $arrValues['report_new_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intReportNewInstanceId', trim( $arrValues['report_new_instance_id'] ) ); elseif( isset( $arrValues['report_new_instance_id'] ) ) $this->setReportNewInstanceId( $arrValues['report_new_instance_id'] );
		if( isset( $arrValues['report_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intReportFilterId', trim( $arrValues['report_filter_id'] ) ); elseif( isset( $arrValues['report_filter_id'] ) ) $this->setReportFilterId( $arrValues['report_filter_id'] );
		if( isset( $arrValues['report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intReportVersionId', trim( $arrValues['report_version_id'] ) ); elseif( isset( $arrValues['report_version_id'] ) ) $this->setReportVersionId( $arrValues['report_version_id'] );
		if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
		if( isset( $arrValues['download_options'] ) ) $this->set( 'm_strDownloadOptions', trim( $arrValues['download_options'] ) );
		if( isset( $arrValues['save_to_documents'] ) && $boolDirectSet ) $this->set( 'm_boolSaveToDocuments', trim( stripcslashes( $arrValues['save_to_documents'] ) ) ); elseif( isset( $arrValues['save_to_documents'] ) ) $this->setSaveToDocuments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['save_to_documents'] ) : $arrValues['save_to_documents'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReportScheduleTypeId( $intReportScheduleTypeId ) {
		$this->set( 'm_intReportScheduleTypeId', CStrings::strToIntDef( $intReportScheduleTypeId, NULL, false ) );
	}

	public function getReportScheduleTypeId() {
		return $this->m_intReportScheduleTypeId;
	}

	public function sqlReportScheduleTypeId() {
		return ( true == isset( $this->m_intReportScheduleTypeId ) ) ? ( string ) $this->m_intReportScheduleTypeId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->set( 'm_intReportGroupId', CStrings::strToIntDef( $intReportGroupId, NULL, false ) );
	}

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function sqlReportGroupId() {
		return ( true == isset( $this->m_intReportGroupId ) ) ? ( string ) $this->m_intReportGroupId : 'NULL';
	}

	public function setReportNewGroupId( $intReportNewGroupId ) {
		$this->set( 'm_intReportNewGroupId', CStrings::strToIntDef( $intReportNewGroupId, NULL, false ) );
	}

	public function getReportNewGroupId() {
		return $this->m_intReportNewGroupId;
	}

	public function sqlReportNewGroupId() {
		return ( true == isset( $this->m_intReportNewGroupId ) ) ? ( string ) $this->m_intReportNewGroupId : 'NULL';
	}

	public function setReportNewInstanceId( $intReportNewInstanceId ) {
		$this->set( 'm_intReportNewInstanceId', CStrings::strToIntDef( $intReportNewInstanceId, NULL, false ) );
	}

	public function getReportNewInstanceId() {
		return $this->m_intReportNewInstanceId;
	}

	public function sqlReportNewInstanceId() {
		return ( true == isset( $this->m_intReportNewInstanceId ) ) ? ( string ) $this->m_intReportNewInstanceId : 'NULL';
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->set( 'm_intReportFilterId', CStrings::strToIntDef( $intReportFilterId, NULL, false ) );
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function sqlReportFilterId() {
		return ( true == isset( $this->m_intReportFilterId ) ) ? ( string ) $this->m_intReportFilterId : 'NULL';
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->set( 'm_intReportVersionId', CStrings::strToIntDef( $intReportVersionId, NULL, false ) );
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function sqlReportVersionId() {
		return ( true == isset( $this->m_intReportVersionId ) ) ? ( string ) $this->m_intReportVersionId : 'NULL';
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function sqlScheduledTaskId() {
		return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
	}

	public function setDownloadOptions( $jsonDownloadOptions ) {
		if( true == valObj( $jsonDownloadOptions, 'stdClass' ) ) {
			$this->set( 'm_jsonDownloadOptions', $jsonDownloadOptions );
		} elseif( true == valJsonString( $jsonDownloadOptions ) ) {
			$this->set( 'm_jsonDownloadOptions', CStrings::strToJson( $jsonDownloadOptions ) );
		} else {
			$this->set( 'm_jsonDownloadOptions', NULL ); 
		}
		unset( $this->m_strDownloadOptions );
	}

	public function getDownloadOptions() {
		if( true == isset( $this->m_strDownloadOptions ) ) {
			$this->m_jsonDownloadOptions = CStrings::strToJson( $this->m_strDownloadOptions );
			unset( $this->m_strDownloadOptions );
		}
		return $this->m_jsonDownloadOptions;
	}

	public function sqlDownloadOptions() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDownloadOptions() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getDownloadOptions() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setSaveToDocuments( $boolSaveToDocuments ) {
		$this->set( 'm_boolSaveToDocuments', CStrings::strToBool( $boolSaveToDocuments ) );
	}

	public function getSaveToDocuments() {
		return $this->m_boolSaveToDocuments;
	}

	public function sqlSaveToDocuments() {
		return ( true == isset( $this->m_boolSaveToDocuments ) ) ? '\'' . ( true == ( bool ) $this->m_boolSaveToDocuments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, report_schedule_type_id, company_user_id, report_group_id, report_new_group_id, report_new_instance_id, report_filter_id, report_version_id, scheduled_task_id, download_options, save_to_documents, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReportScheduleTypeId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlReportGroupId() . ', ' .
						$this->sqlReportNewGroupId() . ', ' .
						$this->sqlReportNewInstanceId() . ', ' .
						$this->sqlReportFilterId() . ', ' .
						$this->sqlReportVersionId() . ', ' .
						$this->sqlScheduledTaskId() . ', ' .
						$this->sqlDownloadOptions() . ', ' .
						$this->sqlSaveToDocuments() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_schedule_type_id = ' . $this->sqlReportScheduleTypeId() . ','; } elseif( true == array_key_exists( 'ReportScheduleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_schedule_type_id = ' . $this->sqlReportScheduleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId() . ','; } elseif( true == array_key_exists( 'ReportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_new_group_id = ' . $this->sqlReportNewGroupId() . ','; } elseif( true == array_key_exists( 'ReportNewGroupId', $this->getChangedColumns() ) ) { $strSql .= ' report_new_group_id = ' . $this->sqlReportNewGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId() . ','; } elseif( true == array_key_exists( 'ReportNewInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' report_new_instance_id = ' . $this->sqlReportNewInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; } elseif( true == array_key_exists( 'ReportFilterId', $this->getChangedColumns() ) ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId() . ','; } elseif( true == array_key_exists( 'ReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' download_options = ' . $this->sqlDownloadOptions() . ','; } elseif( true == array_key_exists( 'DownloadOptions', $this->getChangedColumns() ) ) { $strSql .= ' download_options = ' . $this->sqlDownloadOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' save_to_documents = ' . $this->sqlSaveToDocuments() . ','; } elseif( true == array_key_exists( 'SaveToDocuments', $this->getChangedColumns() ) ) { $strSql .= ' save_to_documents = ' . $this->sqlSaveToDocuments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'report_schedule_type_id' => $this->getReportScheduleTypeId(),
			'company_user_id' => $this->getCompanyUserId(),
			'report_group_id' => $this->getReportGroupId(),
			'report_new_group_id' => $this->getReportNewGroupId(),
			'report_new_instance_id' => $this->getReportNewInstanceId(),
			'report_filter_id' => $this->getReportFilterId(),
			'report_version_id' => $this->getReportVersionId(),
			'scheduled_task_id' => $this->getScheduledTaskId(),
			'download_options' => $this->getDownloadOptions(),
			'save_to_documents' => $this->getSaveToDocuments(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>