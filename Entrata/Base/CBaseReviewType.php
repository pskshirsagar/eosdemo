<?php

class CBaseReviewType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.review_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDisplayName;
	protected $m_strDescription;
	protected $m_strSource;
	protected $m_intIsPublished;
	protected $m_boolIsDirectReply;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsHideReplyOnSourceLink;
	protected $m_boolIsHideReviewUpdates;
	protected $m_boolHasReviewTitle;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strRestrictedToCids;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_boolIsDirectReply = false;
		$this->m_intOrderNum = '0';
		$this->m_strCreatedOn = 'now()';
		$this->m_boolIsHideReplyOnSourceLink = false;
		$this->m_boolIsHideReviewUpdates = false;
		$this->m_boolHasReviewTitle = true;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['display_name'] ) && $boolDirectSet ) $this->set( 'm_strDisplayName', trim( $arrValues['display_name'] ) ); elseif( isset( $arrValues['display_name'] ) ) $this->setDisplayName( $arrValues['display_name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['source'] ) && $boolDirectSet ) $this->set( 'm_strSource', trim( $arrValues['source'] ) ); elseif( isset( $arrValues['source'] ) ) $this->setSource( $arrValues['source'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_direct_reply'] ) && $boolDirectSet ) $this->set( 'm_boolIsDirectReply', trim( stripcslashes( $arrValues['is_direct_reply'] ) ) ); elseif( isset( $arrValues['is_direct_reply'] ) ) $this->setIsDirectReply( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_direct_reply'] ) : $arrValues['is_direct_reply'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_hide_reply_on_source_link'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideReplyOnSourceLink', trim( stripcslashes( $arrValues['is_hide_reply_on_source_link'] ) ) ); elseif( isset( $arrValues['is_hide_reply_on_source_link'] ) ) $this->setIsHideReplyOnSourceLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_reply_on_source_link'] ) : $arrValues['is_hide_reply_on_source_link'] );
		if( isset( $arrValues['is_hide_review_updates'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideReviewUpdates', trim( stripcslashes( $arrValues['is_hide_review_updates'] ) ) ); elseif( isset( $arrValues['is_hide_review_updates'] ) ) $this->setIsHideReviewUpdates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_review_updates'] ) : $arrValues['is_hide_review_updates'] );
		if( isset( $arrValues['has_review_title'] ) && $boolDirectSet ) $this->set( 'm_boolHasReviewTitle', trim( stripcslashes( $arrValues['has_review_title'] ) ) ); elseif( isset( $arrValues['has_review_title'] ) ) $this->setHasReviewTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_review_title'] ) : $arrValues['has_review_title'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['restricted_to_cids'] ) && $boolDirectSet ) $this->set( 'm_strRestrictedToCids', trim( $arrValues['restricted_to_cids'] ) ); elseif( isset( $arrValues['restricted_to_cids'] ) ) $this->setRestrictedToCids( $arrValues['restricted_to_cids'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDisplayName( $strDisplayName ) {
		$this->set( 'm_strDisplayName', CStrings::strTrimDef( $strDisplayName, 240, NULL, true ) );
	}

	public function getDisplayName() {
		return $this->m_strDisplayName;
	}

	public function sqlDisplayName() {
		return ( true == isset( $this->m_strDisplayName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDisplayName ) : '\'' . addslashes( $this->m_strDisplayName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setSource( $strSource ) {
		$this->set( 'm_strSource', CStrings::strTrimDef( $strSource, 64, NULL, true ) );
	}

	public function getSource() {
		return $this->m_strSource;
	}

	public function sqlSource() {
		return ( true == isset( $this->m_strSource ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSource ) : '\'' . addslashes( $this->m_strSource ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsDirectReply( $boolIsDirectReply ) {
		$this->set( 'm_boolIsDirectReply', CStrings::strToBool( $boolIsDirectReply ) );
	}

	public function getIsDirectReply() {
		return $this->m_boolIsDirectReply;
	}

	public function sqlIsDirectReply() {
		return ( true == isset( $this->m_boolIsDirectReply ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDirectReply ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setIsHideReplyOnSourceLink( $boolIsHideReplyOnSourceLink ) {
		$this->set( 'm_boolIsHideReplyOnSourceLink', CStrings::strToBool( $boolIsHideReplyOnSourceLink ) );
	}

	public function getIsHideReplyOnSourceLink() {
		return $this->m_boolIsHideReplyOnSourceLink;
	}

	public function sqlIsHideReplyOnSourceLink() {
		return ( true == isset( $this->m_boolIsHideReplyOnSourceLink ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideReplyOnSourceLink ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHideReviewUpdates( $boolIsHideReviewUpdates ) {
		$this->set( 'm_boolIsHideReviewUpdates', CStrings::strToBool( $boolIsHideReviewUpdates ) );
	}

	public function getIsHideReviewUpdates() {
		return $this->m_boolIsHideReviewUpdates;
	}

	public function sqlIsHideReviewUpdates() {
		return ( true == isset( $this->m_boolIsHideReviewUpdates ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideReviewUpdates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasReviewTitle( $boolHasReviewTitle ) {
		$this->set( 'm_boolHasReviewTitle', CStrings::strToBool( $boolHasReviewTitle ) );
	}

	public function getHasReviewTitle() {
		return $this->m_boolHasReviewTitle;
	}

	public function sqlHasReviewTitle() {
		return ( true == isset( $this->m_boolHasReviewTitle ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasReviewTitle ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRestrictedToCids( $strRestrictedToCids ) {
		$this->set( 'm_strRestrictedToCids', CStrings::strTrimDef( $strRestrictedToCids, -1, NULL, true ) );
	}

	public function getRestrictedToCids() {
		return $this->m_strRestrictedToCids;
	}

	public function sqlRestrictedToCids() {
		return ( true == isset( $this->m_strRestrictedToCids ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRestrictedToCids ) : '\'' . addslashes( $this->m_strRestrictedToCids ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, display_name, description, source, is_published, is_direct_reply, order_num, created_by, created_on, is_hide_reply_on_source_link, is_hide_review_updates, has_review_title, details, restricted_to_cids, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDisplayName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlSource() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsDirectReply() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsHideReplyOnSourceLink() . ', ' .
						$this->sqlIsHideReviewUpdates() . ', ' .
						$this->sqlHasReviewTitle() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlRestrictedToCids() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_name = ' . $this->sqlDisplayName(). ',' ; } elseif( true == array_key_exists( 'DisplayName', $this->getChangedColumns() ) ) { $strSql .= ' display_name = ' . $this->sqlDisplayName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source = ' . $this->sqlSource(). ',' ; } elseif( true == array_key_exists( 'Source', $this->getChangedColumns() ) ) { $strSql .= ' source = ' . $this->sqlSource() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_direct_reply = ' . $this->sqlIsDirectReply(). ',' ; } elseif( true == array_key_exists( 'IsDirectReply', $this->getChangedColumns() ) ) { $strSql .= ' is_direct_reply = ' . $this->sqlIsDirectReply() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_reply_on_source_link = ' . $this->sqlIsHideReplyOnSourceLink(). ',' ; } elseif( true == array_key_exists( 'IsHideReplyOnSourceLink', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_reply_on_source_link = ' . $this->sqlIsHideReplyOnSourceLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_review_updates = ' . $this->sqlIsHideReviewUpdates(). ',' ; } elseif( true == array_key_exists( 'IsHideReviewUpdates', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_review_updates = ' . $this->sqlIsHideReviewUpdates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_review_title = ' . $this->sqlHasReviewTitle(). ',' ; } elseif( true == array_key_exists( 'HasReviewTitle', $this->getChangedColumns() ) ) { $strSql .= ' has_review_title = ' . $this->sqlHasReviewTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' restricted_to_cids = ' . $this->sqlRestrictedToCids(). ',' ; } elseif( true == array_key_exists( 'RestrictedToCids', $this->getChangedColumns() ) ) { $strSql .= ' restricted_to_cids = ' . $this->sqlRestrictedToCids() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'display_name' => $this->getDisplayName(),
			'description' => $this->getDescription(),
			'source' => $this->getSource(),
			'is_published' => $this->getIsPublished(),
			'is_direct_reply' => $this->getIsDirectReply(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_hide_reply_on_source_link' => $this->getIsHideReplyOnSourceLink(),
			'is_hide_review_updates' => $this->getIsHideReviewUpdates(),
			'has_review_title' => $this->getHasReviewTitle(),
			'details' => $this->getDetails(),
			'restricted_to_cids' => $this->getRestrictedToCids(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>