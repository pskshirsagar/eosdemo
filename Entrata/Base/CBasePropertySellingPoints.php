<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySellingPoints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySellingPoints extends CEosPluralBase {

	/**
	 * @return CPropertySellingPoint[]
	 */
	public static function fetchPropertySellingPoints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertySellingPoint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertySellingPoint
	 */
	public static function fetchPropertySellingPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertySellingPoint', $objDatabase );
	}

	public static function fetchPropertySellingPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_selling_points', $objDatabase );
	}

	public static function fetchPropertySellingPointByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertySellingPoint( sprintf( 'SELECT * FROM property_selling_points WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySellingPointsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertySellingPoints( sprintf( 'SELECT * FROM property_selling_points WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySellingPointsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySellingPoints( sprintf( 'SELECT * FROM property_selling_points WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySellingPointsByPropertySellingPointCategoryIdByCid( $intPropertySellingPointCategoryId, $intCid, $objDatabase ) {
		return self::fetchPropertySellingPoints( sprintf( 'SELECT * FROM property_selling_points WHERE property_selling_point_category_id = %d AND cid = %d', ( int ) $intPropertySellingPointCategoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySellingPointsByRateAssociationIdByCid( $intRateAssociationId, $intCid, $objDatabase ) {
		return self::fetchPropertySellingPoints( sprintf( 'SELECT * FROM property_selling_points WHERE rate_association_id = %d AND cid = %d', ( int ) $intRateAssociationId, ( int ) $intCid ), $objDatabase );
	}

}
?>