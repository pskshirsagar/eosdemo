<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserPropertyGroups extends CEosPluralBase {

	/**
	 * @return CCompanyUserPropertyGroup[]
	 */
	public static function fetchCompanyUserPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserPropertyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserPropertyGroup
	 */
	public static function fetchCompanyUserPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserPropertyGroup', $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_property_groups', $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPropertyGroup( sprintf( 'SELECT * FROM company_user_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserPropertyGroups( sprintf( 'SELECT * FROM company_user_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPropertyGroups( sprintf( 'SELECT * FROM company_user_property_groups WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPropertyGroups( sprintf( 'SELECT * FROM company_user_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>