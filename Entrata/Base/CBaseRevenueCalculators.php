<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueCalculators
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueCalculators extends CEosPluralBase {

	/**
	 * @return CRevenueCalculator[]
	 */
	public static function fetchRevenueCalculators( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenueCalculator::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueCalculator
	 */
	public static function fetchRevenueCalculator( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueCalculator::class, $objDatabase );
	}

	public static function fetchRevenueCalculatorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_calculators', $objDatabase );
	}

	public static function fetchRevenueCalculatorByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueCalculator( sprintf( 'SELECT * FROM revenue_calculators WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueCalculatorsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueCalculators( sprintf( 'SELECT * FROM revenue_calculators WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRevenueCalculatorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueCalculators( sprintf( 'SELECT * FROM revenue_calculators WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>