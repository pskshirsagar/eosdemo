<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseUnitSpaceStatusTypes extends CEosPluralBase {

	/**
	 * @return CUnitSpaceStatusType[]
	 */
	public static function fetchUnitSpaceStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CUnitSpaceStatusType::class, $objDatabase );
	}

	/**
	 * @return CUnitSpaceStatusType
	 */
	public static function fetchUnitSpaceStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceStatusType::class, $objDatabase );
	}

	public static function fetchUnitSpaceStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_status_types', $objDatabase );
	}

	public static function fetchUnitSpaceStatusTypeById( $intId, $objDatabase ) {
		return self::fetchUnitSpaceStatusType( sprintf( 'SELECT * FROM unit_space_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>