<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeePolicies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeePolicies extends CEosPluralBase {

	/**
	 * @return CApPayeePolicy[]
	 */
	public static function fetchApPayeePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayeePolicy', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeePolicy
	 */
	public static function fetchApPayeePolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayeePolicy', $objDatabase );
	}

	public static function fetchApPayeePolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_policies', $objDatabase );
	}

	public static function fetchApPayeePolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeePolicy( sprintf( 'SELECT * FROM ap_payee_policies WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeePolicies( sprintf( 'SELECT * FROM ap_payee_policies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePoliciesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeePolicies( sprintf( 'SELECT * FROM ap_payee_policies WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePoliciesByApPayeePolicyTypeIdByCid( $intApPayeePolicyTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayeePolicies( sprintf( 'SELECT * FROM ap_payee_policies WHERE ap_payee_policy_type_id = %d AND cid = %d', ( int ) $intApPayeePolicyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePoliciesByVpPolicyIdByCid( $intVpPolicyId, $intCid, $objDatabase ) {
		return self::fetchApPayeePolicies( sprintf( 'SELECT * FROM ap_payee_policies WHERE vp_policy_id = %d AND cid = %d', ( int ) $intVpPolicyId, ( int ) $intCid ), $objDatabase );
	}

}
?>