<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEventLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEventLogs extends CEosPluralBase {

	/**
	 * @return CEventLog[]
	 */
	public static function fetchEventLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CEventLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEventLog
	 */
	public static function fetchEventLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEventLog::class, $objDatabase );
	}

	public static function fetchEventLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'event_logs', $objDatabase );
	}

	public static function fetchEventLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEventLog( sprintf( 'SELECT * FROM event_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByCid( $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByPriorEventLogIdByCid( $intPriorEventLogId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE prior_event_log_id = %d AND cid = %d', $intPriorEventLogId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE event_type_id = %d AND cid = %d', $intEventTypeId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByEventSubTypeIdByCid( $intEventSubTypeId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE event_sub_type_id = %d AND cid = %d', $intEventSubTypeId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByEventResultIdByCid( $intEventResultId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE event_result_id = %d AND cid = %d', $intEventResultId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByDefaultEventResultIdByCid( $intDefaultEventResultId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE default_event_result_id = %d AND cid = %d', $intDefaultEventResultId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByEventIdByCid( $intEventId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE event_id = %d AND cid = %d', $intEventId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByAssociatedEventIdByCid( $intAssociatedEventId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE associated_event_id = %d AND cid = %d', $intAssociatedEventId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByOldStageIdByCid( $intOldStageId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE old_stage_id = %d AND cid = %d', $intOldStageId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByNewStageIdByCid( $intNewStageId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE new_stage_id = %d AND cid = %d', $intNewStageId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByOldStatusIdByCid( $intOldStatusId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE old_status_id = %d AND cid = %d', $intOldStatusId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByNewStatusIdByCid( $intNewStatusId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE new_status_id = %d AND cid = %d', $intNewStatusId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByDataReferenceIdByCid( $intDataReferenceId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE data_reference_id = %d AND cid = %d', $intDataReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByIntegrationResultIdByCid( $intIntegrationResultId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE integration_result_id = %d AND cid = %d', $intIntegrationResultId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE reporting_period_id = %d AND cid = %d', $intReportingPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE effective_period_id = %d AND cid = %d', $intEffectivePeriodId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE original_period_id = %d AND cid = %d', $intOriginalPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE lease_interval_id = %d AND cid = %d', $intLeaseIntervalId, $intCid ), $objDatabase );
	}

	public static function fetchEventLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchEventLogs( sprintf( 'SELECT * FROM event_logs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

}
?>