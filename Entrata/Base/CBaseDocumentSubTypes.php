<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentSubTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentSubTypes extends CEosPluralBase {

	/**
	 * @return CDocumentSubType[]
	 */
	public static function fetchDocumentSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDocumentSubType::class, $objDatabase );
	}

	/**
	 * @return CDocumentSubType
	 */
	public static function fetchDocumentSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentSubType::class, $objDatabase );
	}

	public static function fetchDocumentSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_sub_types', $objDatabase );
	}

	public static function fetchDocumentSubTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentSubType( sprintf( 'SELECT * FROM document_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDocumentSubTypesByDocumentTypeId( $intDocumentTypeId, $objDatabase ) {
		return self::fetchDocumentSubTypes( sprintf( 'SELECT * FROM document_sub_types WHERE document_type_id = %d', $intDocumentTypeId ), $objDatabase );
	}

}
?>