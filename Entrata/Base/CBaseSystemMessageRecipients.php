<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSystemMessageRecipients
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSystemMessageRecipients extends CEosPluralBase {

	/**
	 * @return CSystemMessageRecipient[]
	 */
	public static function fetchSystemMessageRecipients( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSystemMessageRecipient::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSystemMessageRecipient
	 */
	public static function fetchSystemMessageRecipient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemMessageRecipient::class, $objDatabase );
	}

	public static function fetchSystemMessageRecipientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_message_recipients', $objDatabase );
	}

	public static function fetchSystemMessageRecipientByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSystemMessageRecipient( sprintf( 'SELECT * FROM system_message_recipients WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessageRecipientsByCid( $intCid, $objDatabase ) {
		return self::fetchSystemMessageRecipients( sprintf( 'SELECT * FROM system_message_recipients WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSystemMessageRecipientsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSystemMessageRecipients( sprintf( 'SELECT * FROM system_message_recipients WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessageRecipientsBySystemMessageCategoryIdByCid( $intSystemMessageCategoryId, $intCid, $objDatabase ) {
		return self::fetchSystemMessageRecipients( sprintf( 'SELECT * FROM system_message_recipients WHERE system_message_category_id = %d AND cid = %d', $intSystemMessageCategoryId, $intCid ), $objDatabase );
	}

	public static function fetchSystemMessageRecipientsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchSystemMessageRecipients( sprintf( 'SELECT * FROM system_message_recipients WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

}
?>