<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApSettings extends CEosPluralBase {

	/**
	 * @return CPropertyApSetting[]
	 */
	public static function fetchPropertyApSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyApSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApSetting
	 */
	public static function fetchPropertyApSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyApSetting', $objDatabase );
	}

	public static function fetchPropertyApSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_ap_settings', $objDatabase );
	}

	public static function fetchPropertyApSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApSetting( sprintf( 'SELECT * FROM property_ap_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApSettings( sprintf( 'SELECT * FROM property_ap_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApSettings( sprintf( 'SELECT * FROM property_ap_settings WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>