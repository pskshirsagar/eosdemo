<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantRoommateInterests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantRoommateInterests extends CEosPluralBase {

	/**
	 * @return CApplicantRoommateInterest[]
	 */
	public static function fetchApplicantRoommateInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApplicantRoommateInterest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantRoommateInterest
	 */
	public static function fetchApplicantRoommateInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicantRoommateInterest::class, $objDatabase );
	}

	public static function fetchApplicantRoommateInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_roommate_interests', $objDatabase );
	}

	public static function fetchApplicantRoommateInterestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterest( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterests( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterests( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterests( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterests( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE roommate_interest_id = %d AND cid = %d', ( int ) $intRoommateInterestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByRoommateInterestOptionIdByCid( $intRoommateInterestOptionId, $intCid, $objDatabase ) {
		return self::fetchApplicantRoommateInterests( sprintf( 'SELECT * FROM applicant_roommate_interests WHERE roommate_interest_option_id = %d AND cid = %d', ( int ) $intRoommateInterestOptionId, ( int ) $intCid ), $objDatabase );
	}

}
?>