<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMiscellaneousRequestTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyMiscellaneousRequestTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyMiscellaneousRequestType[]
	 */
	public static function fetchSubsidyMiscellaneousRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyMiscellaneousRequestType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyMiscellaneousRequestType
	 */
	public static function fetchSubsidyMiscellaneousRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyMiscellaneousRequestType::class, $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_miscellaneous_request_types', $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequestType( sprintf( 'SELECT * FROM subsidy_miscellaneous_request_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>