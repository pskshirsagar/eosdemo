<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetAdjustments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetAdjustments extends CEosPluralBase {

	/**
	 * @return CBudgetAdjustment[]
	 */
	public static function fetchBudgetAdjustments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetAdjustment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetAdjustment
	 */
	public static function fetchBudgetAdjustment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetAdjustment::class, $objDatabase );
	}

	public static function fetchBudgetAdjustmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_adjustments', $objDatabase );
	}

	public static function fetchBudgetAdjustmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetAdjustment( sprintf( 'SELECT * FROM budget_adjustments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetAdjustmentsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetAdjustments( sprintf( 'SELECT * FROM budget_adjustments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetAdjustmentsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchBudgetAdjustments( sprintf( 'SELECT * FROM budget_adjustments WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetAdjustmentsByFromApHeaderIdByCid( $intFromApHeaderId, $intCid, $objDatabase ) {
		return self::fetchBudgetAdjustments( sprintf( 'SELECT * FROM budget_adjustments WHERE from_ap_header_id = %d AND cid = %d', ( int ) $intFromApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetAdjustmentsByToApHeaderIdByCid( $intToApHeaderId, $intCid, $objDatabase ) {
		return self::fetchBudgetAdjustments( sprintf( 'SELECT * FROM budget_adjustments WHERE to_ap_header_id = %d AND cid = %d', ( int ) $intToApHeaderId, ( int ) $intCid ), $objDatabase );
	}

}
?>