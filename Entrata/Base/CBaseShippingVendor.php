<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseShippingVendor extends CEosSingularBase {

	const TABLE_NAME = 'public.shipping_vendors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strVendorName;
	protected $m_strColor;
	protected $m_intCompanyMediaFileId;
	protected $m_boolIsVendorIcon;
	protected $m_boolIsEnabled;
	protected $m_boolIsCustom;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVendorIcon = false;
		$this->m_boolIsEnabled = true;
		$this->m_boolIsCustom = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['vendor_name'] ) && $boolDirectSet ) $this->set( 'm_strVendorName', trim( stripcslashes( $arrValues['vendor_name'] ) ) ); elseif( isset( $arrValues['vendor_name'] ) ) $this->setVendorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vendor_name'] ) : $arrValues['vendor_name'] );
		if( isset( $arrValues['color'] ) && $boolDirectSet ) $this->set( 'm_strColor', trim( stripcslashes( $arrValues['color'] ) ) ); elseif( isset( $arrValues['color'] ) ) $this->setColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color'] ) : $arrValues['color'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['is_vendor_icon'] ) && $boolDirectSet ) $this->set( 'm_boolIsVendorIcon', trim( stripcslashes( $arrValues['is_vendor_icon'] ) ) ); elseif( isset( $arrValues['is_vendor_icon'] ) ) $this->setIsVendorIcon( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_vendor_icon'] ) : $arrValues['is_vendor_icon'] );
		if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsEnabled', trim( stripcslashes( $arrValues['is_enabled'] ) ) ); elseif( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_enabled'] ) : $arrValues['is_enabled'] );
		if( isset( $arrValues['is_custom'] ) && $boolDirectSet ) $this->set( 'm_boolIsCustom', trim( stripcslashes( $arrValues['is_custom'] ) ) ); elseif( isset( $arrValues['is_custom'] ) ) $this->setIsCustom( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_custom'] ) : $arrValues['is_custom'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setVendorName( $strVendorName ) {
		$this->set( 'm_strVendorName', CStrings::strTrimDef( $strVendorName, 50, NULL, true ) );
	}

	public function getVendorName() {
		return $this->m_strVendorName;
	}

	public function sqlVendorName() {
		return ( true == isset( $this->m_strVendorName ) ) ? '\'' . addslashes( $this->m_strVendorName ) . '\'' : 'NULL';
	}

	public function setColor( $strColor ) {
		$this->set( 'm_strColor', CStrings::strTrimDef( $strColor, 10, NULL, true ) );
	}

	public function getColor() {
		return $this->m_strColor;
	}

	public function sqlColor() {
		return ( true == isset( $this->m_strColor ) ) ? '\'' . addslashes( $this->m_strColor ) . '\'' : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setIsVendorIcon( $boolIsVendorIcon ) {
		$this->set( 'm_boolIsVendorIcon', CStrings::strToBool( $boolIsVendorIcon ) );
	}

	public function getIsVendorIcon() {
		return $this->m_boolIsVendorIcon;
	}

	public function sqlIsVendorIcon() {
		return ( true == isset( $this->m_boolIsVendorIcon ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVendorIcon ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEnabled( $boolIsEnabled ) {
		$this->set( 'm_boolIsEnabled', CStrings::strToBool( $boolIsEnabled ) );
	}

	public function getIsEnabled() {
		return $this->m_boolIsEnabled;
	}

	public function sqlIsEnabled() {
		return ( true == isset( $this->m_boolIsEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCustom( $boolIsCustom ) {
		$this->set( 'm_boolIsCustom', CStrings::strToBool( $boolIsCustom ) );
	}

	public function getIsCustom() {
		return $this->m_boolIsCustom;
	}

	public function sqlIsCustom() {
		return ( true == isset( $this->m_boolIsCustom ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCustom ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, vendor_name, color, company_media_file_id, is_vendor_icon, is_enabled, is_custom, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlVendorName() . ', ' .
 						$this->sqlColor() . ', ' .
 						$this->sqlCompanyMediaFileId() . ', ' .
 						$this->sqlIsVendorIcon() . ', ' .
 						$this->sqlIsEnabled() . ', ' .
 						$this->sqlIsCustom() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName() . ','; } elseif( true == array_key_exists( 'VendorName', $this->getChangedColumns() ) ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color = ' . $this->sqlColor() . ','; } elseif( true == array_key_exists( 'Color', $this->getChangedColumns() ) ) { $strSql .= ' color = ' . $this->sqlColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vendor_icon = ' . $this->sqlIsVendorIcon() . ','; } elseif( true == array_key_exists( 'IsVendorIcon', $this->getChangedColumns() ) ) { $strSql .= ' is_vendor_icon = ' . $this->sqlIsVendorIcon() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; } elseif( true == array_key_exists( 'IsEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom() . ','; } elseif( true == array_key_exists( 'IsCustom', $this->getChangedColumns() ) ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'vendor_name' => $this->getVendorName(),
			'color' => $this->getColor(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'is_vendor_icon' => $this->getIsVendorIcon(),
			'is_enabled' => $this->getIsEnabled(),
			'is_custom' => $this->getIsCustom(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>