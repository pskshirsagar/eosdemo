<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSubsidyDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationSubsidyDetails extends CEosPluralBase {

	/**
	 * @return CApplicationSubsidyDetail[]
	 */
	public static function fetchApplicationSubsidyDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationSubsidyDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationSubsidyDetail
	 */
	public static function fetchApplicationSubsidyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationSubsidyDetail', $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_subsidy_details', $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetail( sprintf( 'SELECT * FROM application_subsidy_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetail( sprintf( 'SELECT * FROM application_subsidy_details WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByPrimaryApplicantIdByCid( $intPrimaryApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE primary_applicant_id = %d AND cid = %d', ( int ) $intPrimaryApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByPreviousSubsidyContractTypeIdByCid( $intPreviousSubsidyContractTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE previous_subsidy_contract_type_id = %d AND cid = %d', ( int ) $intPreviousSubsidyContractTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByMoveInIncomeLevelTypeIdByCid( $intMoveInIncomeLevelTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE move_in_income_level_type_id = %d AND cid = %d', ( int ) $intMoveInIncomeLevelTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsByCurrentIncomeLevelTypeIdByCid( $intCurrentIncomeLevelTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE current_income_level_type_id = %d AND cid = %d', ( int ) $intCurrentIncomeLevelTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyWaiverTypeIdByCid( $intSubsidyWaiverTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_waiver_type_id = %d AND cid = %d', ( int ) $intSubsidyWaiverTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyCitizenshipEligibilityTypeIdByCid( $intSubsidyCitizenshipEligibilityTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_citizenship_eligibility_type_id = %d AND cid = %d', ( int ) $intSubsidyCitizenshipEligibilityTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyMinRentExemptionTypeIdByCid( $intSubsidyMinRentExemptionTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_min_rent_exemption_type_id = %d AND cid = %d', ( int ) $intSubsidyMinRentExemptionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyIncomeExceptionTypeIdByCid( $intSubsidyIncomeExceptionTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_income_exception_type_id = %d AND cid = %d', ( int ) $intSubsidyIncomeExceptionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyDisplacementStatusTypeIdByCid( $intSubsidyDisplacementStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_displacement_status_type_id = %d AND cid = %d', ( int ) $intSubsidyDisplacementStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationSubsidyDetailsBySubsidyPreviousHousingTypeIdByCid( $intSubsidyPreviousHousingTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationSubsidyDetails( sprintf( 'SELECT * FROM application_subsidy_details WHERE subsidy_previous_housing_type_id = %d AND cid = %d', ( int ) $intSubsidyPreviousHousingTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>