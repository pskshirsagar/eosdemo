<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultImplementationChecklists
 * Do not add any new functions to this class.
 */

class CBaseDefaultImplementationChecklists extends CEosPluralBase {

	/**
	 * @return CDefaultImplementationChecklist[]
	 */
	public static function fetchDefaultImplementationChecklists( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultImplementationChecklist', $objDatabase );
	}

	/**
	 * @return CDefaultImplementationChecklist
	 */
	public static function fetchDefaultImplementationChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultImplementationChecklist', $objDatabase );
	}

	public static function fetchDefaultImplementationChecklistCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_implementation_checklists', $objDatabase );
	}

	public static function fetchDefaultImplementationChecklistById( $intId, $objDatabase ) {
		return self::fetchDefaultImplementationChecklist( sprintf( 'SELECT * FROM default_implementation_checklists WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultImplementationChecklistsByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchDefaultImplementationChecklists( sprintf( 'SELECT * FROM default_implementation_checklists WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

}
?>