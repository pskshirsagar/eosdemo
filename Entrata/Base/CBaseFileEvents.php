<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileEvents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileEvents extends CEosPluralBase {

	/**
	 * @return CFileEvent[]
	 */
	public static function fetchFileEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileEvent::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileEvent
	 */
	public static function fetchFileEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileEvent::class, $objDatabase );
	}

	public static function fetchFileEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_events', $objDatabase );
	}

	public static function fetchFileEventByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileEvent( sprintf( 'SELECT * FROM file_events WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileEventsByCid( $intCid, $objDatabase ) {
		return self::fetchFileEvents( sprintf( 'SELECT * FROM file_events WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFileEventsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileEvents( sprintf( 'SELECT * FROM file_events WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchFileEventsByFileEventTypeIdByCid( $intFileEventTypeId, $intCid, $objDatabase ) {
		return self::fetchFileEvents( sprintf( 'SELECT * FROM file_events WHERE file_event_type_id = %d AND cid = %d', $intFileEventTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFileEventsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchFileEvents( sprintf( 'SELECT * FROM file_events WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>