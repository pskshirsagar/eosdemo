<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApplication extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_applications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPrimaryDocumentId;
	protected $m_intCoApplicantDocumentId;
	protected $m_intCoSignerDocumentId;
	protected $m_intPrimaryLeaseDocumentId;
	protected $m_intCoApplicantLeaseDocumentId;
	protected $m_intCoSignerLeaseDocumentId;
	protected $m_intOccupancyTypeId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strInstructions;
	protected $m_intRequirePrintedForm;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupancyTypeId = '1';
		$this->m_intRequirePrintedForm = '0';
		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['primary_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryDocumentId', trim( $arrValues['primary_document_id'] ) ); elseif( isset( $arrValues['primary_document_id'] ) ) $this->setPrimaryDocumentId( $arrValues['primary_document_id'] );
		if( isset( $arrValues['co_applicant_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCoApplicantDocumentId', trim( $arrValues['co_applicant_document_id'] ) ); elseif( isset( $arrValues['co_applicant_document_id'] ) ) $this->setCoApplicantDocumentId( $arrValues['co_applicant_document_id'] );
		if( isset( $arrValues['co_signer_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCoSignerDocumentId', trim( $arrValues['co_signer_document_id'] ) ); elseif( isset( $arrValues['co_signer_document_id'] ) ) $this->setCoSignerDocumentId( $arrValues['co_signer_document_id'] );
		if( isset( $arrValues['primary_lease_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryLeaseDocumentId', trim( $arrValues['primary_lease_document_id'] ) ); elseif( isset( $arrValues['primary_lease_document_id'] ) ) $this->setPrimaryLeaseDocumentId( $arrValues['primary_lease_document_id'] );
		if( isset( $arrValues['co_applicant_lease_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCoApplicantLeaseDocumentId', trim( $arrValues['co_applicant_lease_document_id'] ) ); elseif( isset( $arrValues['co_applicant_lease_document_id'] ) ) $this->setCoApplicantLeaseDocumentId( $arrValues['co_applicant_lease_document_id'] );
		if( isset( $arrValues['co_signer_lease_document_id'] ) && $boolDirectSet ) $this->set( 'm_intCoSignerLeaseDocumentId', trim( $arrValues['co_signer_lease_document_id'] ) ); elseif( isset( $arrValues['co_signer_lease_document_id'] ) ) $this->setCoSignerLeaseDocumentId( $arrValues['co_signer_lease_document_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['instructions'] ) && $boolDirectSet ) $this->set( 'm_strInstructions', trim( stripcslashes( $arrValues['instructions'] ) ) ); elseif( isset( $arrValues['instructions'] ) ) $this->setInstructions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructions'] ) : $arrValues['instructions'] );
		if( isset( $arrValues['require_printed_form'] ) && $boolDirectSet ) $this->set( 'm_intRequirePrintedForm', trim( $arrValues['require_printed_form'] ) ); elseif( isset( $arrValues['require_printed_form'] ) ) $this->setRequirePrintedForm( $arrValues['require_printed_form'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPrimaryDocumentId( $intPrimaryDocumentId ) {
		$this->set( 'm_intPrimaryDocumentId', CStrings::strToIntDef( $intPrimaryDocumentId, NULL, false ) );
	}

	public function getPrimaryDocumentId() {
		return $this->m_intPrimaryDocumentId;
	}

	public function sqlPrimaryDocumentId() {
		return ( true == isset( $this->m_intPrimaryDocumentId ) ) ? ( string ) $this->m_intPrimaryDocumentId : 'NULL';
	}

	public function setCoApplicantDocumentId( $intCoApplicantDocumentId ) {
		$this->set( 'm_intCoApplicantDocumentId', CStrings::strToIntDef( $intCoApplicantDocumentId, NULL, false ) );
	}

	public function getCoApplicantDocumentId() {
		return $this->m_intCoApplicantDocumentId;
	}

	public function sqlCoApplicantDocumentId() {
		return ( true == isset( $this->m_intCoApplicantDocumentId ) ) ? ( string ) $this->m_intCoApplicantDocumentId : 'NULL';
	}

	public function setCoSignerDocumentId( $intCoSignerDocumentId ) {
		$this->set( 'm_intCoSignerDocumentId', CStrings::strToIntDef( $intCoSignerDocumentId, NULL, false ) );
	}

	public function getCoSignerDocumentId() {
		return $this->m_intCoSignerDocumentId;
	}

	public function sqlCoSignerDocumentId() {
		return ( true == isset( $this->m_intCoSignerDocumentId ) ) ? ( string ) $this->m_intCoSignerDocumentId : 'NULL';
	}

	public function setPrimaryLeaseDocumentId( $intPrimaryLeaseDocumentId ) {
		$this->set( 'm_intPrimaryLeaseDocumentId', CStrings::strToIntDef( $intPrimaryLeaseDocumentId, NULL, false ) );
	}

	public function getPrimaryLeaseDocumentId() {
		return $this->m_intPrimaryLeaseDocumentId;
	}

	public function sqlPrimaryLeaseDocumentId() {
		return ( true == isset( $this->m_intPrimaryLeaseDocumentId ) ) ? ( string ) $this->m_intPrimaryLeaseDocumentId : 'NULL';
	}

	public function setCoApplicantLeaseDocumentId( $intCoApplicantLeaseDocumentId ) {
		$this->set( 'm_intCoApplicantLeaseDocumentId', CStrings::strToIntDef( $intCoApplicantLeaseDocumentId, NULL, false ) );
	}

	public function getCoApplicantLeaseDocumentId() {
		return $this->m_intCoApplicantLeaseDocumentId;
	}

	public function sqlCoApplicantLeaseDocumentId() {
		return ( true == isset( $this->m_intCoApplicantLeaseDocumentId ) ) ? ( string ) $this->m_intCoApplicantLeaseDocumentId : 'NULL';
	}

	public function setCoSignerLeaseDocumentId( $intCoSignerLeaseDocumentId ) {
		$this->set( 'm_intCoSignerLeaseDocumentId', CStrings::strToIntDef( $intCoSignerLeaseDocumentId, NULL, false ) );
	}

	public function getCoSignerLeaseDocumentId() {
		return $this->m_intCoSignerLeaseDocumentId;
	}

	public function sqlCoSignerLeaseDocumentId() {
		return ( true == isset( $this->m_intCoSignerLeaseDocumentId ) ) ? ( string ) $this->m_intCoSignerLeaseDocumentId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstructions( $strInstructions ) {
		$this->set( 'm_strInstructions', CStrings::strTrimDef( $strInstructions, 2000, NULL, true ) );
	}

	public function getInstructions() {
		return $this->m_strInstructions;
	}

	public function sqlInstructions() {
		return ( true == isset( $this->m_strInstructions ) ) ? '\'' . addslashes( $this->m_strInstructions ) . '\'' : 'NULL';
	}

	public function setRequirePrintedForm( $intRequirePrintedForm ) {
		$this->set( 'm_intRequirePrintedForm', CStrings::strToIntDef( $intRequirePrintedForm, NULL, false ) );
	}

	public function getRequirePrintedForm() {
		return $this->m_intRequirePrintedForm;
	}

	public function sqlRequirePrintedForm() {
		return ( true == isset( $this->m_intRequirePrintedForm ) ) ? ( string ) $this->m_intRequirePrintedForm : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, primary_document_id, co_applicant_document_id, co_signer_document_id, primary_lease_document_id, co_applicant_lease_document_id, co_signer_lease_document_id, occupancy_type_id, title, description, instructions, require_printed_form, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPrimaryDocumentId() . ', ' .
						$this->sqlCoApplicantDocumentId() . ', ' .
						$this->sqlCoSignerDocumentId() . ', ' .
						$this->sqlPrimaryLeaseDocumentId() . ', ' .
						$this->sqlCoApplicantLeaseDocumentId() . ', ' .
						$this->sqlCoSignerLeaseDocumentId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlInstructions() . ', ' .
						$this->sqlRequirePrintedForm() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_document_id = ' . $this->sqlPrimaryDocumentId(). ',' ; } elseif( true == array_key_exists( 'PrimaryDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' primary_document_id = ' . $this->sqlPrimaryDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' co_applicant_document_id = ' . $this->sqlCoApplicantDocumentId(). ',' ; } elseif( true == array_key_exists( 'CoApplicantDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' co_applicant_document_id = ' . $this->sqlCoApplicantDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' co_signer_document_id = ' . $this->sqlCoSignerDocumentId(). ',' ; } elseif( true == array_key_exists( 'CoSignerDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' co_signer_document_id = ' . $this->sqlCoSignerDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_lease_document_id = ' . $this->sqlPrimaryLeaseDocumentId(). ',' ; } elseif( true == array_key_exists( 'PrimaryLeaseDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' primary_lease_document_id = ' . $this->sqlPrimaryLeaseDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' co_applicant_lease_document_id = ' . $this->sqlCoApplicantLeaseDocumentId(). ',' ; } elseif( true == array_key_exists( 'CoApplicantLeaseDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' co_applicant_lease_document_id = ' . $this->sqlCoApplicantLeaseDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' co_signer_lease_document_id = ' . $this->sqlCoSignerLeaseDocumentId(). ',' ; } elseif( true == array_key_exists( 'CoSignerLeaseDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' co_signer_lease_document_id = ' . $this->sqlCoSignerLeaseDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions = ' . $this->sqlInstructions(). ',' ; } elseif( true == array_key_exists( 'Instructions', $this->getChangedColumns() ) ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm(). ',' ; } elseif( true == array_key_exists( 'RequirePrintedForm', $this->getChangedColumns() ) ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'primary_document_id' => $this->getPrimaryDocumentId(),
			'co_applicant_document_id' => $this->getCoApplicantDocumentId(),
			'co_signer_document_id' => $this->getCoSignerDocumentId(),
			'primary_lease_document_id' => $this->getPrimaryLeaseDocumentId(),
			'co_applicant_lease_document_id' => $this->getCoApplicantLeaseDocumentId(),
			'co_signer_lease_document_id' => $this->getCoSignerLeaseDocumentId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'instructions' => $this->getInstructions(),
			'require_printed_form' => $this->getRequirePrintedForm(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>