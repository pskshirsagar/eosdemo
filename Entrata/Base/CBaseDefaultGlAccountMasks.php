<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlAccountMasks
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlAccountMasks extends CEosPluralBase {

	/**
	 * @return CDefaultGlAccountMask[]
	 */
	public static function fetchDefaultGlAccountMasks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlAccountMask::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlAccountMask
	 */
	public static function fetchDefaultGlAccountMask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlAccountMask::class, $objDatabase );
	}

	public static function fetchDefaultGlAccountMaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_account_masks', $objDatabase );
	}

	public static function fetchDefaultGlAccountMaskById( $intId, $objDatabase ) {
		return self::fetchDefaultGlAccountMask( sprintf( 'SELECT * FROM default_gl_account_masks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountMasksByDefaultGlChartId( $intDefaultGlChartId, $objDatabase ) {
		return self::fetchDefaultGlAccountMasks( sprintf( 'SELECT * FROM default_gl_account_masks WHERE default_gl_chart_id = %d', $intDefaultGlChartId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountMasksByDefaultGlTreeId( $intDefaultGlTreeId, $objDatabase ) {
		return self::fetchDefaultGlAccountMasks( sprintf( 'SELECT * FROM default_gl_account_masks WHERE default_gl_tree_id = %d', $intDefaultGlTreeId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountMasksByDefaultGlGroupId( $intDefaultGlGroupId, $objDatabase ) {
		return self::fetchDefaultGlAccountMasks( sprintf( 'SELECT * FROM default_gl_account_masks WHERE default_gl_group_id = %d', $intDefaultGlGroupId ), $objDatabase );
	}

	public static function fetchDefaultGlAccountMasksByDefaultGlAccountId( $intDefaultGlAccountId, $objDatabase ) {
		return self::fetchDefaultGlAccountMasks( sprintf( 'SELECT * FROM default_gl_account_masks WHERE default_gl_account_id = %d', $intDefaultGlAccountId ), $objDatabase );
	}

}
?>