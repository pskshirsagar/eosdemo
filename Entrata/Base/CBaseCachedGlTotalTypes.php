<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedGlTotalTypes
 * Do not add any new functions to this class.
 */

class CBaseCachedGlTotalTypes extends CEosPluralBase {

	/**
	 * @return CCachedGlTotalType[]
	 */
	public static function fetchCachedGlTotalTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCachedGlTotalType::class, $objDatabase );
	}

	/**
	 * @return CCachedGlTotalType
	 */
	public static function fetchCachedGlTotalType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCachedGlTotalType::class, $objDatabase );
	}

	public static function fetchCachedGlTotalTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_gl_total_types', $objDatabase );
	}

	public static function fetchCachedGlTotalTypeById( $intId, $objDatabase ) {
		return self::fetchCachedGlTotalType( sprintf( 'SELECT * FROM cached_gl_total_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>