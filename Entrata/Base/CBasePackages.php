<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePackages extends CEosPluralBase {

	/**
	 * @return CPackage[]
	 */
	public static function fetchPackages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPackage::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPackage
	 */
	public static function fetchPackage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPackage::class, $objDatabase );
	}

	public static function fetchPackageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'packages', $objDatabase );
	}

	public static function fetchPackageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPackage( sprintf( 'SELECT * FROM packages WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByCid( $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPackagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByPackageTypeIdByCid( $intPackageTypeId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE package_type_id = %d AND cid = %d', $intPackageTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByPackageBatchIdByCid( $intPackageBatchId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE package_batch_id = %d AND cid = %d', $intPackageBatchId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByPackageStatusTypeIdByCid( $intPackageStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE package_status_type_id = %d AND cid = %d', $intPackageStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesBySignatureFileIdByCid( $intSignatureFileId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE signature_file_id = %d AND cid = %d', $intSignatureFileId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchPackagesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchPackages( sprintf( 'SELECT * FROM packages WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

}
?>