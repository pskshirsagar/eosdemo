<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedApplicationLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.cached_application_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intApplicationId;
	protected $m_intPriorCachedApplicationLogId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLeaseTypeId;
	protected $m_strLeadConversionType;
	protected $m_intOldApplicationStatusTypeId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intApplicationStepId;
	protected $m_intMaxApplicationStageId;
	protected $m_intMaxApplicationStatusId;
	protected $m_intMaxApplicationStepId;
	protected $m_intCancellationListTypeId;
	protected $m_intCancellationListItemId;
	protected $m_intPrimaryApplicantId;
	protected $m_intUnitKindId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intSpaceConfigurationId;
	protected $m_intDesiredSpaceConfigurationId;
	protected $m_intScreeningId;
	protected $m_intPsProductId;
	protected $m_intApplicationPsProductId;
	protected $m_intLeasePsProductId;
	protected $m_intLeasingAgentId;
	protected $m_intInternetListingServiceId;
	protected $m_intOriginatingLeadSourceId;
	protected $m_intConvertingLeadSourceId;
	protected $m_intCompanyApplicationId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intLateFeeFormulaId;
	protected $m_intCombinedApplicationId;
	protected $m_intQuoteId;
	protected $m_intSemAdGroupId;
	protected $m_intSemKeywordId;
	protected $m_intSemSourceId;
	protected $m_intCallId;
	protected $m_intWebsiteId;
	protected $m_intCraigslistPostId;
	protected $m_intClAdId;
	protected $m_intTrafficCookieId;
	protected $m_intFirstEventId;
	protected $m_intFirstEventMediumId;
	protected $m_intFirstLeasingCenterEventId;
	protected $m_intResponseEventId;
	protected $m_intLastEventId;
	protected $m_intLeaseSignatureTypeId;
	protected $m_intInsurancePolicyId;
	protected $m_intInsurancePolicyStatusTypeId;
	protected $m_intOccupancyTypeId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_intWaitListId;
	protected $m_intWaitListApplicationId;
	protected $m_arrintWaitListPointIds;
	protected $m_intWaitListPosition;
	protected $m_intWaitListRejectionCount;
	protected $m_strGuestRemotePrimaryKey;
	protected $m_strAppRemotePrimaryKey;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_fltUnitSquareFeet;
	protected $m_intUnitPriorApplicationId;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strPrimaryPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_intTermMonth;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_fltOptimalMonthlyRentBase;
	protected $m_fltOptimalMonthlyRentTotal;
	protected $m_fltAdvertisedMonthlyRentBase;
	protected $m_fltAdvertisedMonthlyRentAmenity;
	protected $m_fltAdvertisedMonthlyRentAddOn;
	protected $m_fltAdvertisedMonthlyRentSpecial;
	protected $m_fltAdvertisedMonthlyRentTotal;
	protected $m_fltAdvertisedMonthlyOtherBase;
	protected $m_fltAdvertisedMonthlyOtherAmenity;
	protected $m_fltAdvertisedMonthlyOtherAddOn;
	protected $m_fltAdvertisedMonthlyOtherSpecial;
	protected $m_fltAdvertisedMonthlyOtherTotal;
	protected $m_fltAdvertisedOneTimeDepositBase;
	protected $m_fltAdvertisedOneTimeDepositAmenity;
	protected $m_fltAdvertisedOneTimeDepositAddOn;
	protected $m_fltAdvertisedOneTimeDepositSpecial;
	protected $m_fltAdvertisedOneTimeDepositTotal;
	protected $m_fltAdvertisedOneTimeApplicationBase;
	protected $m_fltAdvertisedOneTimeApplicationAmenity;
	protected $m_fltAdvertisedOneTimeApplicationAddOn;
	protected $m_fltAdvertisedOneTimeApplicationSpecial;
	protected $m_fltAdvertisedOneTimeApplicationTotal;
	protected $m_fltAdvertisedOneTimeOtherBase;
	protected $m_fltAdvertisedOneTimeOtherAmenity;
	protected $m_fltAdvertisedOneTimeOtherAddOn;
	protected $m_fltAdvertisedOneTimeOtherSpecial;
	protected $m_fltAdvertisedOneTimeOtherTotal;
	protected $m_fltExecutedMonthlyRentBase;
	protected $m_fltExecutedMonthlyRentAmenity;
	protected $m_fltExecutedMonthlyRentAddOn;
	protected $m_fltExecutedMonthlyRentSpecial;
	protected $m_fltExecutedMonthlyRentRiskPremium;
	protected $m_fltExecutedMonthlyRentPet;
	protected $m_fltExecutedMonthlyRentTotal;
	protected $m_fltExecutedMonthlyOtherBase;
	protected $m_fltExecutedMonthlyOtherAmenity;
	protected $m_fltExecutedMonthlyOtherAddOn;
	protected $m_fltExecutedMonthlyOtherSpecial;
	protected $m_fltExecutedMonthlyOtherRiskPremium;
	protected $m_fltExecutedMonthlyOtherPet;
	protected $m_fltExecutedMonthlyOtherTotal;
	protected $m_fltExecutedOneTimeDepositBase;
	protected $m_fltExecutedOneTimeDepositAmenity;
	protected $m_fltExecutedOneTimeDepositAddOn;
	protected $m_fltExecutedOneTimeDepositSpecial;
	protected $m_fltExecutedOneTimeDepositRiskPremium;
	protected $m_fltExecutedOneTimeDepositPet;
	protected $m_fltExecutedOneTimeDepositTotal;
	protected $m_fltExecutedOneTimeApplicationBase;
	protected $m_fltExecutedOneTimeApplicationAmenity;
	protected $m_fltExecutedOneTimeApplicationAddOn;
	protected $m_fltExecutedOneTimeApplicationSpecial;
	protected $m_fltExecutedOneTimeApplicationRiskPremium;
	protected $m_fltExecutedOneTimeApplicationPet;
	protected $m_fltExecutedOneTimeApplicationTotal;
	protected $m_fltExecutedOneTimeOtherBase;
	protected $m_fltExecutedOneTimeOtherAmenity;
	protected $m_fltExecutedOneTimeOtherAddOn;
	protected $m_fltExecutedOneTimeOtherSpecial;
	protected $m_fltExecutedOneTimeOtherRiskPremium;
	protected $m_fltExecutedOneTimeOtherPet;
	protected $m_fltExecutedOneTimeOtherTotal;
	protected $m_fltExecutedOptionalMonthlyRentDog;
	protected $m_fltExecutedOptionalMonthlyRentCat;
	protected $m_fltExecutedOptionalMonthlyRentOtherPet;
	protected $m_fltExecutedOptionalMonthlyRentGarage;
	protected $m_fltExecutedOptionalMonthlyRentAppliances;
	protected $m_fltExecutedOptionalMonthlyRentFurniture;
	protected $m_fltExecutedOptionalMonthlyRentFurnished;
	protected $m_fltExecutedOptionalMonthlyRentStorage;
	protected $m_fltExecutedOptionalMonthlyRentParking;
	protected $m_fltExecutedOptionalMonthlyRentMealPlans;
	protected $m_fltExecutedOptionalMonthlyRentOtherAddOn;
	protected $m_intVehicleCount;
	protected $m_intHouseholdIncome;
	protected $m_fltDesiredRentMin;
	protected $m_fltDesiredRentMax;
	protected $m_intDesiredBedrooms;
	protected $m_fltDesiredBathrooms;
	protected $m_intDesiredPets;
	protected $m_intDesiredOccupants;
	protected $m_arrintDesiredAmenities;
	protected $m_intTotalResponsible;
	protected $m_intTotalNotResponsible;
	protected $m_intTotalGuarantors;
	protected $m_intTotalNonLeasingApplicants;
	protected $m_intTotalApplicants;
	protected $m_intGuestCardFieldCompletions;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_strApplicationDatetime;
	protected $m_strUrlReferer;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intQuotesCount;
	protected $m_fltConversionProbability;
	protected $m_strFirstContactedOn;
	protected $m_strTimeToFirstResponse;
	protected $m_strSiteVisitedOn;
	protected $m_strPropertyTouredOn;
	protected $m_strUnitTouredOn;
	protected $m_strBlueMoonApplicationSentOn;
	protected $m_strInfoApprovedOn;
	protected $m_strPreQualificationCompletedOn;
	protected $m_strPreQualificationApprovedOn;
	protected $m_strApplicationStartedOn;
	protected $m_strApplicationCompletedOn;
	protected $m_strApplicationApprovedOn;
	protected $m_strScreeningApprovedOn;
	protected $m_strLeaseGeneratedOn;
	protected $m_strLeaseCompletedOn;
	protected $m_strLeaseApprovedOn;
	protected $m_strCancelledOn;
	protected $m_strTransferredInOn;
	protected $m_strTransferredOutOn;
	protected $m_strMuteFollowupsUntil;
	protected $m_strQuotedRentLockedUntil;
	protected $m_strHoldUnitUntil;
	protected $m_strWaitListStartOn;
	protected $m_strWaitListEndOn;
	protected $m_boolRequiresCapture;
	protected $m_boolIsPetPolicyConflicted;
	protected $m_boolIsDeleted;
	protected $m_boolIsPostMonthIgnored;
	protected $m_boolIsPostDateIgnored;
	protected $m_boolIsOpeningLog;
	protected $m_intLockedBy;
	protected $m_strLockedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltWaitListPoints;
	protected $m_strApplicationCompletedOnInitial;
	protected $m_strApplicationApprovedOnInitial;
	protected $m_strApplicationReopenedOn;
	protected $m_strLeaseIntervalType;
	protected $m_strLeaseStatusType;
	protected $m_strLeaseTerm;
	protected $m_strLeaseStartWindowStartDate;
	protected $m_strLeaseStartWindowEndDate;
	protected $m_fltContractBaseRent;
	protected $m_fltContractAmenityRent;
	protected $m_fltContractPetRent;
	protected $m_fltContractAddOnRent;
	protected $m_fltContractRiskPremiumRent;
	protected $m_fltContractSpecialRent;
	protected $m_fltContractTotalRent;
	protected $m_fltContractBaseDeposit;
	protected $m_fltContractAmenityDeposit;
	protected $m_fltContractPetDeposit;
	protected $m_fltContractAddOnDeposit;
	protected $m_fltContractRiskPremiumDeposit;
	protected $m_fltContractSpecialDeposit;
	protected $m_fltContractBaseOther;
	protected $m_fltContractAmenityOther;
	protected $m_fltContractPetOther;
	protected $m_fltContractAddOnOther;
	protected $m_fltContractRiskPremiumOther;
	protected $m_fltContractSpecialOther;
	protected $m_fltContractTotalOther;
	protected $m_fltContractTotalDeposit;
	protected $m_strApplicationPartiallyCompletedOn;
	protected $m_strLeaseCompletedOnInitial;
	protected $m_strLeasePartiallyCompletedOnInitial;
	protected $m_strLeaseApprovedOnInitial;
	protected $m_strUnitSpaceRentedOn;
	protected $m_intDefaultListItemId;

	public function __construct() {
		parent::__construct();

		$this->m_intLeaseIntervalTypeId = '1';
		$this->m_intLeaseStatusTypeId = '1';
		$this->m_strLeadConversionType = '\'Incomplete\'::text';
		$this->m_intApplicationStageId = '1';
		$this->m_intApplicationStatusId = '1';
		$this->m_intApplicationStepId = '1';
		$this->m_intMaxApplicationStageId = '1';
		$this->m_intMaxApplicationStatusId = '1';
		$this->m_intMaxApplicationStepId = '1';
		$this->m_intLeaseSignatureTypeId = '1';
		$this->m_intOccupancyTypeId = '1';
		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intTermMonth = '0';
		$this->m_fltOptimalMonthlyRentBase = '0';
		$this->m_fltOptimalMonthlyRentTotal = '0';
		$this->m_fltAdvertisedMonthlyRentBase = '0';
		$this->m_fltAdvertisedMonthlyRentAmenity = '0';
		$this->m_fltAdvertisedMonthlyRentAddOn = '0';
		$this->m_fltAdvertisedMonthlyRentSpecial = '0';
		$this->m_fltAdvertisedMonthlyRentTotal = '0';
		$this->m_fltAdvertisedMonthlyOtherBase = '0';
		$this->m_fltAdvertisedMonthlyOtherAmenity = '0';
		$this->m_fltAdvertisedMonthlyOtherAddOn = '0';
		$this->m_fltAdvertisedMonthlyOtherSpecial = '0';
		$this->m_fltAdvertisedMonthlyOtherTotal = '0';
		$this->m_fltAdvertisedOneTimeDepositBase = '0';
		$this->m_fltAdvertisedOneTimeDepositAmenity = '0';
		$this->m_fltAdvertisedOneTimeDepositAddOn = '0';
		$this->m_fltAdvertisedOneTimeDepositSpecial = '0';
		$this->m_fltAdvertisedOneTimeDepositTotal = '0';
		$this->m_fltAdvertisedOneTimeApplicationBase = '0';
		$this->m_fltAdvertisedOneTimeApplicationAmenity = '0';
		$this->m_fltAdvertisedOneTimeApplicationAddOn = '0';
		$this->m_fltAdvertisedOneTimeApplicationSpecial = '0';
		$this->m_fltAdvertisedOneTimeApplicationTotal = '0';
		$this->m_fltAdvertisedOneTimeOtherBase = '0';
		$this->m_fltAdvertisedOneTimeOtherAmenity = '0';
		$this->m_fltAdvertisedOneTimeOtherAddOn = '0';
		$this->m_fltAdvertisedOneTimeOtherSpecial = '0';
		$this->m_fltAdvertisedOneTimeOtherTotal = '0';
		$this->m_fltExecutedMonthlyRentBase = '0';
		$this->m_fltExecutedMonthlyRentAmenity = '0';
		$this->m_fltExecutedMonthlyRentAddOn = '0';
		$this->m_fltExecutedMonthlyRentSpecial = '0';
		$this->m_fltExecutedMonthlyRentRiskPremium = '0';
		$this->m_fltExecutedMonthlyRentPet = '0';
		$this->m_fltExecutedMonthlyRentTotal = '0';
		$this->m_fltExecutedMonthlyOtherBase = '0';
		$this->m_fltExecutedMonthlyOtherAmenity = '0';
		$this->m_fltExecutedMonthlyOtherAddOn = '0';
		$this->m_fltExecutedMonthlyOtherSpecial = '0';
		$this->m_fltExecutedMonthlyOtherRiskPremium = '0';
		$this->m_fltExecutedMonthlyOtherPet = '0';
		$this->m_fltExecutedMonthlyOtherTotal = '0';
		$this->m_fltExecutedOneTimeDepositBase = '0';
		$this->m_fltExecutedOneTimeDepositAmenity = '0';
		$this->m_fltExecutedOneTimeDepositAddOn = '0';
		$this->m_fltExecutedOneTimeDepositSpecial = '0';
		$this->m_fltExecutedOneTimeDepositRiskPremium = '0';
		$this->m_fltExecutedOneTimeDepositPet = '0';
		$this->m_fltExecutedOneTimeDepositTotal = '0';
		$this->m_fltExecutedOneTimeApplicationBase = '0';
		$this->m_fltExecutedOneTimeApplicationAmenity = '0';
		$this->m_fltExecutedOneTimeApplicationAddOn = '0';
		$this->m_fltExecutedOneTimeApplicationSpecial = '0';
		$this->m_fltExecutedOneTimeApplicationRiskPremium = '0';
		$this->m_fltExecutedOneTimeApplicationPet = '0';
		$this->m_fltExecutedOneTimeApplicationTotal = '0';
		$this->m_fltExecutedOneTimeOtherBase = '0';
		$this->m_fltExecutedOneTimeOtherAmenity = '0';
		$this->m_fltExecutedOneTimeOtherAddOn = '0';
		$this->m_fltExecutedOneTimeOtherSpecial = '0';
		$this->m_fltExecutedOneTimeOtherRiskPremium = '0';
		$this->m_fltExecutedOneTimeOtherPet = '0';
		$this->m_fltExecutedOneTimeOtherTotal = '0';
		$this->m_fltExecutedOptionalMonthlyRentDog = '0';
		$this->m_fltExecutedOptionalMonthlyRentCat = '0';
		$this->m_fltExecutedOptionalMonthlyRentOtherPet = '0';
		$this->m_fltExecutedOptionalMonthlyRentGarage = '0';
		$this->m_fltExecutedOptionalMonthlyRentAppliances = '0';
		$this->m_fltExecutedOptionalMonthlyRentFurniture = '0';
		$this->m_fltExecutedOptionalMonthlyRentFurnished = '0';
		$this->m_fltExecutedOptionalMonthlyRentStorage = '0';
		$this->m_fltExecutedOptionalMonthlyRentParking = '0';
		$this->m_fltExecutedOptionalMonthlyRentMealPlans = '0';
		$this->m_fltExecutedOptionalMonthlyRentOtherAddOn = '0';
		$this->m_intVehicleCount = '0';
		$this->m_intHouseholdIncome = '0';
		$this->m_intTotalResponsible = '0';
		$this->m_intTotalNotResponsible = '0';
		$this->m_intTotalGuarantors = '0';
		$this->m_intTotalNonLeasingApplicants = '0';
		$this->m_intTotalApplicants = '0';
		$this->m_intGuestCardFieldCompletions = '0';
		$this->m_intQuotesCount = '0';
		$this->m_fltConversionProbability = '0';
		$this->m_boolRequiresCapture = false;
		$this->m_boolIsPetPolicyConflicted = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsPostMonthIgnored = false;
		$this->m_boolIsPostDateIgnored = false;
		$this->m_boolIsOpeningLog = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['prior_cached_application_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCachedApplicationLogId', trim( $arrValues['prior_cached_application_log_id'] ) ); elseif( isset( $arrValues['prior_cached_application_log_id'] ) ) $this->setPriorCachedApplicationLogId( $arrValues['prior_cached_application_log_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTypeId', trim( $arrValues['lease_type_id'] ) ); elseif( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
		if( isset( $arrValues['lead_conversion_type'] ) && $boolDirectSet ) $this->set( 'm_strLeadConversionType', trim( $arrValues['lead_conversion_type'] ) ); elseif( isset( $arrValues['lead_conversion_type'] ) ) $this->setLeadConversionType( $arrValues['lead_conversion_type'] );
		if( isset( $arrValues['old_application_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldApplicationStatusTypeId', trim( $arrValues['old_application_status_type_id'] ) ); elseif( isset( $arrValues['old_application_status_type_id'] ) ) $this->setOldApplicationStatusTypeId( $arrValues['old_application_status_type_id'] );
		if( isset( $arrValues['application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageId', trim( $arrValues['application_stage_id'] ) ); elseif( isset( $arrValues['application_stage_id'] ) ) $this->setApplicationStageId( $arrValues['application_stage_id'] );
		if( isset( $arrValues['application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStatusId', trim( $arrValues['application_status_id'] ) ); elseif( isset( $arrValues['application_status_id'] ) ) $this->setApplicationStatusId( $arrValues['application_status_id'] );
		if( isset( $arrValues['application_step_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStepId', trim( $arrValues['application_step_id'] ) ); elseif( isset( $arrValues['application_step_id'] ) ) $this->setApplicationStepId( $arrValues['application_step_id'] );
		if( isset( $arrValues['max_application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStageId', trim( $arrValues['max_application_stage_id'] ) ); elseif( isset( $arrValues['max_application_stage_id'] ) ) $this->setMaxApplicationStageId( $arrValues['max_application_stage_id'] );
		if( isset( $arrValues['max_application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStatusId', trim( $arrValues['max_application_status_id'] ) ); elseif( isset( $arrValues['max_application_status_id'] ) ) $this->setMaxApplicationStatusId( $arrValues['max_application_status_id'] );
		if( isset( $arrValues['max_application_step_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxApplicationStepId', trim( $arrValues['max_application_step_id'] ) ); elseif( isset( $arrValues['max_application_step_id'] ) ) $this->setMaxApplicationStepId( $arrValues['max_application_step_id'] );
		if( isset( $arrValues['cancellation_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCancellationListTypeId', trim( $arrValues['cancellation_list_type_id'] ) ); elseif( isset( $arrValues['cancellation_list_type_id'] ) ) $this->setCancellationListTypeId( $arrValues['cancellation_list_type_id'] );
		if( isset( $arrValues['cancellation_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intCancellationListItemId', trim( $arrValues['cancellation_list_item_id'] ) ); elseif( isset( $arrValues['cancellation_list_item_id'] ) ) $this->setCancellationListItemId( $arrValues['cancellation_list_item_id'] );
		if( isset( $arrValues['primary_applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryApplicantId', trim( $arrValues['primary_applicant_id'] ) ); elseif( isset( $arrValues['primary_applicant_id'] ) ) $this->setPrimaryApplicantId( $arrValues['primary_applicant_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['desired_space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intDesiredSpaceConfigurationId', trim( $arrValues['desired_space_configuration_id'] ) ); elseif( isset( $arrValues['desired_space_configuration_id'] ) ) $this->setDesiredSpaceConfigurationId( $arrValues['desired_space_configuration_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['application_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationPsProductId', trim( $arrValues['application_ps_product_id'] ) ); elseif( isset( $arrValues['application_ps_product_id'] ) ) $this->setApplicationPsProductId( $arrValues['application_ps_product_id'] );
		if( isset( $arrValues['lease_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasePsProductId', trim( $arrValues['lease_ps_product_id'] ) ); elseif( isset( $arrValues['lease_ps_product_id'] ) ) $this->setLeasePsProductId( $arrValues['lease_ps_product_id'] );
		if( isset( $arrValues['leasing_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingAgentId', trim( $arrValues['leasing_agent_id'] ) ); elseif( isset( $arrValues['leasing_agent_id'] ) ) $this->setLeasingAgentId( $arrValues['leasing_agent_id'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['originating_lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginatingLeadSourceId', trim( $arrValues['originating_lead_source_id'] ) ); elseif( isset( $arrValues['originating_lead_source_id'] ) ) $this->setOriginatingLeadSourceId( $arrValues['originating_lead_source_id'] );
		if( isset( $arrValues['converting_lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intConvertingLeadSourceId', trim( $arrValues['converting_lead_source_id'] ) ); elseif( isset( $arrValues['converting_lead_source_id'] ) ) $this->setConvertingLeadSourceId( $arrValues['converting_lead_source_id'] );
		if( isset( $arrValues['company_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyApplicationId', trim( $arrValues['company_application_id'] ) ); elseif( isset( $arrValues['company_application_id'] ) ) $this->setCompanyApplicationId( $arrValues['company_application_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeFormulaId', trim( $arrValues['late_fee_formula_id'] ) ); elseif( isset( $arrValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrValues['late_fee_formula_id'] );
		if( isset( $arrValues['combined_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCombinedApplicationId', trim( $arrValues['combined_application_id'] ) ); elseif( isset( $arrValues['combined_application_id'] ) ) $this->setCombinedApplicationId( $arrValues['combined_application_id'] );
		if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['craigslist_post_id'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistPostId', trim( $arrValues['craigslist_post_id'] ) ); elseif( isset( $arrValues['craigslist_post_id'] ) ) $this->setCraigslistPostId( $arrValues['craigslist_post_id'] );
		if( isset( $arrValues['cl_ad_id'] ) && $boolDirectSet ) $this->set( 'm_intClAdId', trim( $arrValues['cl_ad_id'] ) ); elseif( isset( $arrValues['cl_ad_id'] ) ) $this->setClAdId( $arrValues['cl_ad_id'] );
		if( isset( $arrValues['traffic_cookie_id'] ) && $boolDirectSet ) $this->set( 'm_intTrafficCookieId', trim( $arrValues['traffic_cookie_id'] ) ); elseif( isset( $arrValues['traffic_cookie_id'] ) ) $this->setTrafficCookieId( $arrValues['traffic_cookie_id'] );
		if( isset( $arrValues['first_event_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstEventId', trim( $arrValues['first_event_id'] ) ); elseif( isset( $arrValues['first_event_id'] ) ) $this->setFirstEventId( $arrValues['first_event_id'] );
		if( isset( $arrValues['first_event_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstEventMediumId', trim( $arrValues['first_event_medium_id'] ) ); elseif( isset( $arrValues['first_event_medium_id'] ) ) $this->setFirstEventMediumId( $arrValues['first_event_medium_id'] );
		if( isset( $arrValues['first_leasing_center_event_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstLeasingCenterEventId', trim( $arrValues['first_leasing_center_event_id'] ) ); elseif( isset( $arrValues['first_leasing_center_event_id'] ) ) $this->setFirstLeasingCenterEventId( $arrValues['first_leasing_center_event_id'] );
		if( isset( $arrValues['response_event_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseEventId', trim( $arrValues['response_event_id'] ) ); elseif( isset( $arrValues['response_event_id'] ) ) $this->setResponseEventId( $arrValues['response_event_id'] );
		if( isset( $arrValues['last_event_id'] ) && $boolDirectSet ) $this->set( 'm_intLastEventId', trim( $arrValues['last_event_id'] ) ); elseif( isset( $arrValues['last_event_id'] ) ) $this->setLastEventId( $arrValues['last_event_id'] );
		if( isset( $arrValues['lease_signature_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseSignatureTypeId', trim( $arrValues['lease_signature_type_id'] ) ); elseif( isset( $arrValues['lease_signature_type_id'] ) ) $this->setLeaseSignatureTypeId( $arrValues['lease_signature_type_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['insurance_policy_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyStatusTypeId', trim( $arrValues['insurance_policy_status_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_status_type_id'] ) ) $this->setInsurancePolicyStatusTypeId( $arrValues['insurance_policy_status_type_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['wait_list_id'] ) && $boolDirectSet ) $this->set( 'm_intWaitListId', trim( $arrValues['wait_list_id'] ) ); elseif( isset( $arrValues['wait_list_id'] ) ) $this->setWaitListId( $arrValues['wait_list_id'] );
		if( isset( $arrValues['wait_list_application_id'] ) && $boolDirectSet ) $this->set( 'm_intWaitListApplicationId', trim( $arrValues['wait_list_application_id'] ) ); elseif( isset( $arrValues['wait_list_application_id'] ) ) $this->setWaitListApplicationId( $arrValues['wait_list_application_id'] );
		if( isset( $arrValues['wait_list_point_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintWaitListPointIds', trim( $arrValues['wait_list_point_ids'] ) ); elseif( isset( $arrValues['wait_list_point_ids'] ) ) $this->setWaitListPointIds( $arrValues['wait_list_point_ids'] );
		if( isset( $arrValues['wait_list_position'] ) && $boolDirectSet ) $this->set( 'm_intWaitListPosition', trim( $arrValues['wait_list_position'] ) ); elseif( isset( $arrValues['wait_list_position'] ) ) $this->setWaitListPosition( $arrValues['wait_list_position'] );
		if( isset( $arrValues['wait_list_rejection_count'] ) && $boolDirectSet ) $this->set( 'm_intWaitListRejectionCount', trim( $arrValues['wait_list_rejection_count'] ) ); elseif( isset( $arrValues['wait_list_rejection_count'] ) ) $this->setWaitListRejectionCount( $arrValues['wait_list_rejection_count'] );
		if( isset( $arrValues['guest_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strGuestRemotePrimaryKey', trim( $arrValues['guest_remote_primary_key'] ) ); elseif( isset( $arrValues['guest_remote_primary_key'] ) ) $this->setGuestRemotePrimaryKey( $arrValues['guest_remote_primary_key'] );
		if( isset( $arrValues['app_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strAppRemotePrimaryKey', trim( $arrValues['app_remote_primary_key'] ) ); elseif( isset( $arrValues['app_remote_primary_key'] ) ) $this->setAppRemotePrimaryKey( $arrValues['app_remote_primary_key'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( $arrValues['unit_number_cache'] ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['unit_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltUnitSquareFeet', trim( $arrValues['unit_square_feet'] ) ); elseif( isset( $arrValues['unit_square_feet'] ) ) $this->setUnitSquareFeet( $arrValues['unit_square_feet'] );
		if( isset( $arrValues['unit_prior_application_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitPriorApplicationId', trim( $arrValues['unit_prior_application_id'] ) ); elseif( isset( $arrValues['unit_prior_application_id'] ) ) $this->setUnitPriorApplicationId( $arrValues['unit_prior_application_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['primary_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryPhoneNumber', trim( $arrValues['primary_phone_number'] ) ); elseif( isset( $arrValues['primary_phone_number'] ) ) $this->setPrimaryPhoneNumber( $arrValues['primary_phone_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['term_month'] ) && $boolDirectSet ) $this->set( 'm_intTermMonth', trim( $arrValues['term_month'] ) ); elseif( isset( $arrValues['term_month'] ) ) $this->setTermMonth( $arrValues['term_month'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['optimal_monthly_rent_base'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalMonthlyRentBase', trim( $arrValues['optimal_monthly_rent_base'] ) ); elseif( isset( $arrValues['optimal_monthly_rent_base'] ) ) $this->setOptimalMonthlyRentBase( $arrValues['optimal_monthly_rent_base'] );
		if( isset( $arrValues['optimal_monthly_rent_total'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalMonthlyRentTotal', trim( $arrValues['optimal_monthly_rent_total'] ) ); elseif( isset( $arrValues['optimal_monthly_rent_total'] ) ) $this->setOptimalMonthlyRentTotal( $arrValues['optimal_monthly_rent_total'] );
		if( isset( $arrValues['advertised_monthly_rent_base'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyRentBase', trim( $arrValues['advertised_monthly_rent_base'] ) ); elseif( isset( $arrValues['advertised_monthly_rent_base'] ) ) $this->setAdvertisedMonthlyRentBase( $arrValues['advertised_monthly_rent_base'] );
		if( isset( $arrValues['advertised_monthly_rent_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyRentAmenity', trim( $arrValues['advertised_monthly_rent_amenity'] ) ); elseif( isset( $arrValues['advertised_monthly_rent_amenity'] ) ) $this->setAdvertisedMonthlyRentAmenity( $arrValues['advertised_monthly_rent_amenity'] );
		if( isset( $arrValues['advertised_monthly_rent_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyRentAddOn', trim( $arrValues['advertised_monthly_rent_add_on'] ) ); elseif( isset( $arrValues['advertised_monthly_rent_add_on'] ) ) $this->setAdvertisedMonthlyRentAddOn( $arrValues['advertised_monthly_rent_add_on'] );
		if( isset( $arrValues['advertised_monthly_rent_special'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyRentSpecial', trim( $arrValues['advertised_monthly_rent_special'] ) ); elseif( isset( $arrValues['advertised_monthly_rent_special'] ) ) $this->setAdvertisedMonthlyRentSpecial( $arrValues['advertised_monthly_rent_special'] );
		if( isset( $arrValues['advertised_monthly_rent_total'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyRentTotal', trim( $arrValues['advertised_monthly_rent_total'] ) ); elseif( isset( $arrValues['advertised_monthly_rent_total'] ) ) $this->setAdvertisedMonthlyRentTotal( $arrValues['advertised_monthly_rent_total'] );
		if( isset( $arrValues['advertised_monthly_other_base'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyOtherBase', trim( $arrValues['advertised_monthly_other_base'] ) ); elseif( isset( $arrValues['advertised_monthly_other_base'] ) ) $this->setAdvertisedMonthlyOtherBase( $arrValues['advertised_monthly_other_base'] );
		if( isset( $arrValues['advertised_monthly_other_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyOtherAmenity', trim( $arrValues['advertised_monthly_other_amenity'] ) ); elseif( isset( $arrValues['advertised_monthly_other_amenity'] ) ) $this->setAdvertisedMonthlyOtherAmenity( $arrValues['advertised_monthly_other_amenity'] );
		if( isset( $arrValues['advertised_monthly_other_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyOtherAddOn', trim( $arrValues['advertised_monthly_other_add_on'] ) ); elseif( isset( $arrValues['advertised_monthly_other_add_on'] ) ) $this->setAdvertisedMonthlyOtherAddOn( $arrValues['advertised_monthly_other_add_on'] );
		if( isset( $arrValues['advertised_monthly_other_special'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyOtherSpecial', trim( $arrValues['advertised_monthly_other_special'] ) ); elseif( isset( $arrValues['advertised_monthly_other_special'] ) ) $this->setAdvertisedMonthlyOtherSpecial( $arrValues['advertised_monthly_other_special'] );
		if( isset( $arrValues['advertised_monthly_other_total'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedMonthlyOtherTotal', trim( $arrValues['advertised_monthly_other_total'] ) ); elseif( isset( $arrValues['advertised_monthly_other_total'] ) ) $this->setAdvertisedMonthlyOtherTotal( $arrValues['advertised_monthly_other_total'] );
		if( isset( $arrValues['advertised_one_time_deposit_base'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeDepositBase', trim( $arrValues['advertised_one_time_deposit_base'] ) ); elseif( isset( $arrValues['advertised_one_time_deposit_base'] ) ) $this->setAdvertisedOneTimeDepositBase( $arrValues['advertised_one_time_deposit_base'] );
		if( isset( $arrValues['advertised_one_time_deposit_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeDepositAmenity', trim( $arrValues['advertised_one_time_deposit_amenity'] ) ); elseif( isset( $arrValues['advertised_one_time_deposit_amenity'] ) ) $this->setAdvertisedOneTimeDepositAmenity( $arrValues['advertised_one_time_deposit_amenity'] );
		if( isset( $arrValues['advertised_one_time_deposit_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeDepositAddOn', trim( $arrValues['advertised_one_time_deposit_add_on'] ) ); elseif( isset( $arrValues['advertised_one_time_deposit_add_on'] ) ) $this->setAdvertisedOneTimeDepositAddOn( $arrValues['advertised_one_time_deposit_add_on'] );
		if( isset( $arrValues['advertised_one_time_deposit_special'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeDepositSpecial', trim( $arrValues['advertised_one_time_deposit_special'] ) ); elseif( isset( $arrValues['advertised_one_time_deposit_special'] ) ) $this->setAdvertisedOneTimeDepositSpecial( $arrValues['advertised_one_time_deposit_special'] );
		if( isset( $arrValues['advertised_one_time_deposit_total'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeDepositTotal', trim( $arrValues['advertised_one_time_deposit_total'] ) ); elseif( isset( $arrValues['advertised_one_time_deposit_total'] ) ) $this->setAdvertisedOneTimeDepositTotal( $arrValues['advertised_one_time_deposit_total'] );
		if( isset( $arrValues['advertised_one_time_application_base'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeApplicationBase', trim( $arrValues['advertised_one_time_application_base'] ) ); elseif( isset( $arrValues['advertised_one_time_application_base'] ) ) $this->setAdvertisedOneTimeApplicationBase( $arrValues['advertised_one_time_application_base'] );
		if( isset( $arrValues['advertised_one_time_application_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeApplicationAmenity', trim( $arrValues['advertised_one_time_application_amenity'] ) ); elseif( isset( $arrValues['advertised_one_time_application_amenity'] ) ) $this->setAdvertisedOneTimeApplicationAmenity( $arrValues['advertised_one_time_application_amenity'] );
		if( isset( $arrValues['advertised_one_time_application_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeApplicationAddOn', trim( $arrValues['advertised_one_time_application_add_on'] ) ); elseif( isset( $arrValues['advertised_one_time_application_add_on'] ) ) $this->setAdvertisedOneTimeApplicationAddOn( $arrValues['advertised_one_time_application_add_on'] );
		if( isset( $arrValues['advertised_one_time_application_special'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeApplicationSpecial', trim( $arrValues['advertised_one_time_application_special'] ) ); elseif( isset( $arrValues['advertised_one_time_application_special'] ) ) $this->setAdvertisedOneTimeApplicationSpecial( $arrValues['advertised_one_time_application_special'] );
		if( isset( $arrValues['advertised_one_time_application_total'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeApplicationTotal', trim( $arrValues['advertised_one_time_application_total'] ) ); elseif( isset( $arrValues['advertised_one_time_application_total'] ) ) $this->setAdvertisedOneTimeApplicationTotal( $arrValues['advertised_one_time_application_total'] );
		if( isset( $arrValues['advertised_one_time_other_base'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeOtherBase', trim( $arrValues['advertised_one_time_other_base'] ) ); elseif( isset( $arrValues['advertised_one_time_other_base'] ) ) $this->setAdvertisedOneTimeOtherBase( $arrValues['advertised_one_time_other_base'] );
		if( isset( $arrValues['advertised_one_time_other_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeOtherAmenity', trim( $arrValues['advertised_one_time_other_amenity'] ) ); elseif( isset( $arrValues['advertised_one_time_other_amenity'] ) ) $this->setAdvertisedOneTimeOtherAmenity( $arrValues['advertised_one_time_other_amenity'] );
		if( isset( $arrValues['advertised_one_time_other_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeOtherAddOn', trim( $arrValues['advertised_one_time_other_add_on'] ) ); elseif( isset( $arrValues['advertised_one_time_other_add_on'] ) ) $this->setAdvertisedOneTimeOtherAddOn( $arrValues['advertised_one_time_other_add_on'] );
		if( isset( $arrValues['advertised_one_time_other_special'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeOtherSpecial', trim( $arrValues['advertised_one_time_other_special'] ) ); elseif( isset( $arrValues['advertised_one_time_other_special'] ) ) $this->setAdvertisedOneTimeOtherSpecial( $arrValues['advertised_one_time_other_special'] );
		if( isset( $arrValues['advertised_one_time_other_total'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOneTimeOtherTotal', trim( $arrValues['advertised_one_time_other_total'] ) ); elseif( isset( $arrValues['advertised_one_time_other_total'] ) ) $this->setAdvertisedOneTimeOtherTotal( $arrValues['advertised_one_time_other_total'] );
		if( isset( $arrValues['executed_monthly_rent_base'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentBase', trim( $arrValues['executed_monthly_rent_base'] ) ); elseif( isset( $arrValues['executed_monthly_rent_base'] ) ) $this->setExecutedMonthlyRentBase( $arrValues['executed_monthly_rent_base'] );
		if( isset( $arrValues['executed_monthly_rent_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentAmenity', trim( $arrValues['executed_monthly_rent_amenity'] ) ); elseif( isset( $arrValues['executed_monthly_rent_amenity'] ) ) $this->setExecutedMonthlyRentAmenity( $arrValues['executed_monthly_rent_amenity'] );
		if( isset( $arrValues['executed_monthly_rent_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentAddOn', trim( $arrValues['executed_monthly_rent_add_on'] ) ); elseif( isset( $arrValues['executed_monthly_rent_add_on'] ) ) $this->setExecutedMonthlyRentAddOn( $arrValues['executed_monthly_rent_add_on'] );
		if( isset( $arrValues['executed_monthly_rent_special'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentSpecial', trim( $arrValues['executed_monthly_rent_special'] ) ); elseif( isset( $arrValues['executed_monthly_rent_special'] ) ) $this->setExecutedMonthlyRentSpecial( $arrValues['executed_monthly_rent_special'] );
		if( isset( $arrValues['executed_monthly_rent_risk_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentRiskPremium', trim( $arrValues['executed_monthly_rent_risk_premium'] ) ); elseif( isset( $arrValues['executed_monthly_rent_risk_premium'] ) ) $this->setExecutedMonthlyRentRiskPremium( $arrValues['executed_monthly_rent_risk_premium'] );
		if( isset( $arrValues['executed_monthly_rent_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentPet', trim( $arrValues['executed_monthly_rent_pet'] ) ); elseif( isset( $arrValues['executed_monthly_rent_pet'] ) ) $this->setExecutedMonthlyRentPet( $arrValues['executed_monthly_rent_pet'] );
		if( isset( $arrValues['executed_monthly_rent_total'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyRentTotal', trim( $arrValues['executed_monthly_rent_total'] ) ); elseif( isset( $arrValues['executed_monthly_rent_total'] ) ) $this->setExecutedMonthlyRentTotal( $arrValues['executed_monthly_rent_total'] );
		if( isset( $arrValues['executed_monthly_other_base'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherBase', trim( $arrValues['executed_monthly_other_base'] ) ); elseif( isset( $arrValues['executed_monthly_other_base'] ) ) $this->setExecutedMonthlyOtherBase( $arrValues['executed_monthly_other_base'] );
		if( isset( $arrValues['executed_monthly_other_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherAmenity', trim( $arrValues['executed_monthly_other_amenity'] ) ); elseif( isset( $arrValues['executed_monthly_other_amenity'] ) ) $this->setExecutedMonthlyOtherAmenity( $arrValues['executed_monthly_other_amenity'] );
		if( isset( $arrValues['executed_monthly_other_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherAddOn', trim( $arrValues['executed_monthly_other_add_on'] ) ); elseif( isset( $arrValues['executed_monthly_other_add_on'] ) ) $this->setExecutedMonthlyOtherAddOn( $arrValues['executed_monthly_other_add_on'] );
		if( isset( $arrValues['executed_monthly_other_special'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherSpecial', trim( $arrValues['executed_monthly_other_special'] ) ); elseif( isset( $arrValues['executed_monthly_other_special'] ) ) $this->setExecutedMonthlyOtherSpecial( $arrValues['executed_monthly_other_special'] );
		if( isset( $arrValues['executed_monthly_other_risk_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherRiskPremium', trim( $arrValues['executed_monthly_other_risk_premium'] ) ); elseif( isset( $arrValues['executed_monthly_other_risk_premium'] ) ) $this->setExecutedMonthlyOtherRiskPremium( $arrValues['executed_monthly_other_risk_premium'] );
		if( isset( $arrValues['executed_monthly_other_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherPet', trim( $arrValues['executed_monthly_other_pet'] ) ); elseif( isset( $arrValues['executed_monthly_other_pet'] ) ) $this->setExecutedMonthlyOtherPet( $arrValues['executed_monthly_other_pet'] );
		if( isset( $arrValues['executed_monthly_other_total'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedMonthlyOtherTotal', trim( $arrValues['executed_monthly_other_total'] ) ); elseif( isset( $arrValues['executed_monthly_other_total'] ) ) $this->setExecutedMonthlyOtherTotal( $arrValues['executed_monthly_other_total'] );
		if( isset( $arrValues['executed_one_time_deposit_base'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositBase', trim( $arrValues['executed_one_time_deposit_base'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_base'] ) ) $this->setExecutedOneTimeDepositBase( $arrValues['executed_one_time_deposit_base'] );
		if( isset( $arrValues['executed_one_time_deposit_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositAmenity', trim( $arrValues['executed_one_time_deposit_amenity'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_amenity'] ) ) $this->setExecutedOneTimeDepositAmenity( $arrValues['executed_one_time_deposit_amenity'] );
		if( isset( $arrValues['executed_one_time_deposit_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositAddOn', trim( $arrValues['executed_one_time_deposit_add_on'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_add_on'] ) ) $this->setExecutedOneTimeDepositAddOn( $arrValues['executed_one_time_deposit_add_on'] );
		if( isset( $arrValues['executed_one_time_deposit_special'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositSpecial', trim( $arrValues['executed_one_time_deposit_special'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_special'] ) ) $this->setExecutedOneTimeDepositSpecial( $arrValues['executed_one_time_deposit_special'] );
		if( isset( $arrValues['executed_one_time_deposit_risk_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositRiskPremium', trim( $arrValues['executed_one_time_deposit_risk_premium'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_risk_premium'] ) ) $this->setExecutedOneTimeDepositRiskPremium( $arrValues['executed_one_time_deposit_risk_premium'] );
		if( isset( $arrValues['executed_one_time_deposit_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositPet', trim( $arrValues['executed_one_time_deposit_pet'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_pet'] ) ) $this->setExecutedOneTimeDepositPet( $arrValues['executed_one_time_deposit_pet'] );
		if( isset( $arrValues['executed_one_time_deposit_total'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeDepositTotal', trim( $arrValues['executed_one_time_deposit_total'] ) ); elseif( isset( $arrValues['executed_one_time_deposit_total'] ) ) $this->setExecutedOneTimeDepositTotal( $arrValues['executed_one_time_deposit_total'] );
		if( isset( $arrValues['executed_one_time_application_base'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationBase', trim( $arrValues['executed_one_time_application_base'] ) ); elseif( isset( $arrValues['executed_one_time_application_base'] ) ) $this->setExecutedOneTimeApplicationBase( $arrValues['executed_one_time_application_base'] );
		if( isset( $arrValues['executed_one_time_application_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationAmenity', trim( $arrValues['executed_one_time_application_amenity'] ) ); elseif( isset( $arrValues['executed_one_time_application_amenity'] ) ) $this->setExecutedOneTimeApplicationAmenity( $arrValues['executed_one_time_application_amenity'] );
		if( isset( $arrValues['executed_one_time_application_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationAddOn', trim( $arrValues['executed_one_time_application_add_on'] ) ); elseif( isset( $arrValues['executed_one_time_application_add_on'] ) ) $this->setExecutedOneTimeApplicationAddOn( $arrValues['executed_one_time_application_add_on'] );
		if( isset( $arrValues['executed_one_time_application_special'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationSpecial', trim( $arrValues['executed_one_time_application_special'] ) ); elseif( isset( $arrValues['executed_one_time_application_special'] ) ) $this->setExecutedOneTimeApplicationSpecial( $arrValues['executed_one_time_application_special'] );
		if( isset( $arrValues['executed_one_time_application_risk_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationRiskPremium', trim( $arrValues['executed_one_time_application_risk_premium'] ) ); elseif( isset( $arrValues['executed_one_time_application_risk_premium'] ) ) $this->setExecutedOneTimeApplicationRiskPremium( $arrValues['executed_one_time_application_risk_premium'] );
		if( isset( $arrValues['executed_one_time_application_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationPet', trim( $arrValues['executed_one_time_application_pet'] ) ); elseif( isset( $arrValues['executed_one_time_application_pet'] ) ) $this->setExecutedOneTimeApplicationPet( $arrValues['executed_one_time_application_pet'] );
		if( isset( $arrValues['executed_one_time_application_total'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeApplicationTotal', trim( $arrValues['executed_one_time_application_total'] ) ); elseif( isset( $arrValues['executed_one_time_application_total'] ) ) $this->setExecutedOneTimeApplicationTotal( $arrValues['executed_one_time_application_total'] );
		if( isset( $arrValues['executed_one_time_other_base'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherBase', trim( $arrValues['executed_one_time_other_base'] ) ); elseif( isset( $arrValues['executed_one_time_other_base'] ) ) $this->setExecutedOneTimeOtherBase( $arrValues['executed_one_time_other_base'] );
		if( isset( $arrValues['executed_one_time_other_amenity'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherAmenity', trim( $arrValues['executed_one_time_other_amenity'] ) ); elseif( isset( $arrValues['executed_one_time_other_amenity'] ) ) $this->setExecutedOneTimeOtherAmenity( $arrValues['executed_one_time_other_amenity'] );
		if( isset( $arrValues['executed_one_time_other_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherAddOn', trim( $arrValues['executed_one_time_other_add_on'] ) ); elseif( isset( $arrValues['executed_one_time_other_add_on'] ) ) $this->setExecutedOneTimeOtherAddOn( $arrValues['executed_one_time_other_add_on'] );
		if( isset( $arrValues['executed_one_time_other_special'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherSpecial', trim( $arrValues['executed_one_time_other_special'] ) ); elseif( isset( $arrValues['executed_one_time_other_special'] ) ) $this->setExecutedOneTimeOtherSpecial( $arrValues['executed_one_time_other_special'] );
		if( isset( $arrValues['executed_one_time_other_risk_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherRiskPremium', trim( $arrValues['executed_one_time_other_risk_premium'] ) ); elseif( isset( $arrValues['executed_one_time_other_risk_premium'] ) ) $this->setExecutedOneTimeOtherRiskPremium( $arrValues['executed_one_time_other_risk_premium'] );
		if( isset( $arrValues['executed_one_time_other_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherPet', trim( $arrValues['executed_one_time_other_pet'] ) ); elseif( isset( $arrValues['executed_one_time_other_pet'] ) ) $this->setExecutedOneTimeOtherPet( $arrValues['executed_one_time_other_pet'] );
		if( isset( $arrValues['executed_one_time_other_total'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOneTimeOtherTotal', trim( $arrValues['executed_one_time_other_total'] ) ); elseif( isset( $arrValues['executed_one_time_other_total'] ) ) $this->setExecutedOneTimeOtherTotal( $arrValues['executed_one_time_other_total'] );
		if( isset( $arrValues['executed_optional_monthly_rent_dog'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentDog', trim( $arrValues['executed_optional_monthly_rent_dog'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_dog'] ) ) $this->setExecutedOptionalMonthlyRentDog( $arrValues['executed_optional_monthly_rent_dog'] );
		if( isset( $arrValues['executed_optional_monthly_rent_cat'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentCat', trim( $arrValues['executed_optional_monthly_rent_cat'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_cat'] ) ) $this->setExecutedOptionalMonthlyRentCat( $arrValues['executed_optional_monthly_rent_cat'] );
		if( isset( $arrValues['executed_optional_monthly_rent_other_pet'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentOtherPet', trim( $arrValues['executed_optional_monthly_rent_other_pet'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_other_pet'] ) ) $this->setExecutedOptionalMonthlyRentOtherPet( $arrValues['executed_optional_monthly_rent_other_pet'] );
		if( isset( $arrValues['executed_optional_monthly_rent_garage'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentGarage', trim( $arrValues['executed_optional_monthly_rent_garage'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_garage'] ) ) $this->setExecutedOptionalMonthlyRentGarage( $arrValues['executed_optional_monthly_rent_garage'] );
		if( isset( $arrValues['executed_optional_monthly_rent_appliances'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentAppliances', trim( $arrValues['executed_optional_monthly_rent_appliances'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_appliances'] ) ) $this->setExecutedOptionalMonthlyRentAppliances( $arrValues['executed_optional_monthly_rent_appliances'] );
		if( isset( $arrValues['executed_optional_monthly_rent_furniture'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentFurniture', trim( $arrValues['executed_optional_monthly_rent_furniture'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_furniture'] ) ) $this->setExecutedOptionalMonthlyRentFurniture( $arrValues['executed_optional_monthly_rent_furniture'] );
		if( isset( $arrValues['executed_optional_monthly_rent_furnished'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentFurnished', trim( $arrValues['executed_optional_monthly_rent_furnished'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_furnished'] ) ) $this->setExecutedOptionalMonthlyRentFurnished( $arrValues['executed_optional_monthly_rent_furnished'] );
		if( isset( $arrValues['executed_optional_monthly_rent_storage'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentStorage', trim( $arrValues['executed_optional_monthly_rent_storage'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_storage'] ) ) $this->setExecutedOptionalMonthlyRentStorage( $arrValues['executed_optional_monthly_rent_storage'] );
		if( isset( $arrValues['executed_optional_monthly_rent_parking'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentParking', trim( $arrValues['executed_optional_monthly_rent_parking'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_parking'] ) ) $this->setExecutedOptionalMonthlyRentParking( $arrValues['executed_optional_monthly_rent_parking'] );
		if( isset( $arrValues['executed_optional_monthly_rent_meal_plans'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentMealPlans', trim( $arrValues['executed_optional_monthly_rent_meal_plans'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_meal_plans'] ) ) $this->setExecutedOptionalMonthlyRentMealPlans( $arrValues['executed_optional_monthly_rent_meal_plans'] );
		if( isset( $arrValues['executed_optional_monthly_rent_other_add_on'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedOptionalMonthlyRentOtherAddOn', trim( $arrValues['executed_optional_monthly_rent_other_add_on'] ) ); elseif( isset( $arrValues['executed_optional_monthly_rent_other_add_on'] ) ) $this->setExecutedOptionalMonthlyRentOtherAddOn( $arrValues['executed_optional_monthly_rent_other_add_on'] );
		if( isset( $arrValues['vehicle_count'] ) && $boolDirectSet ) $this->set( 'm_intVehicleCount', trim( $arrValues['vehicle_count'] ) ); elseif( isset( $arrValues['vehicle_count'] ) ) $this->setVehicleCount( $arrValues['vehicle_count'] );
		if( isset( $arrValues['household_income'] ) && $boolDirectSet ) $this->set( 'm_intHouseholdIncome', trim( $arrValues['household_income'] ) ); elseif( isset( $arrValues['household_income'] ) ) $this->setHouseholdIncome( $arrValues['household_income'] );
		if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMin', trim( $arrValues['desired_rent_min'] ) ); elseif( isset( $arrValues['desired_rent_min'] ) ) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
		if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMax', trim( $arrValues['desired_rent_max'] ) ); elseif( isset( $arrValues['desired_rent_max'] ) ) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
		if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intDesiredBedrooms', trim( $arrValues['desired_bedrooms'] ) ); elseif( isset( $arrValues['desired_bedrooms'] ) ) $this->setDesiredBedrooms( $arrValues['desired_bedrooms'] );
		if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredBathrooms', trim( $arrValues['desired_bathrooms'] ) ); elseif( isset( $arrValues['desired_bathrooms'] ) ) $this->setDesiredBathrooms( $arrValues['desired_bathrooms'] );
		if( isset( $arrValues['desired_pets'] ) && $boolDirectSet ) $this->set( 'm_intDesiredPets', trim( $arrValues['desired_pets'] ) ); elseif( isset( $arrValues['desired_pets'] ) ) $this->setDesiredPets( $arrValues['desired_pets'] );
		if( isset( $arrValues['desired_occupants'] ) && $boolDirectSet ) $this->set( 'm_intDesiredOccupants', trim( $arrValues['desired_occupants'] ) ); elseif( isset( $arrValues['desired_occupants'] ) ) $this->setDesiredOccupants( $arrValues['desired_occupants'] );
		if( isset( $arrValues['desired_amenities'] ) && $boolDirectSet ) $this->set( 'm_arrintDesiredAmenities', trim( $arrValues['desired_amenities'] ) ); elseif( isset( $arrValues['desired_amenities'] ) ) $this->setDesiredAmenities( $arrValues['desired_amenities'] );
		if( isset( $arrValues['total_responsible'] ) && $boolDirectSet ) $this->set( 'm_intTotalResponsible', trim( $arrValues['total_responsible'] ) ); elseif( isset( $arrValues['total_responsible'] ) ) $this->setTotalResponsible( $arrValues['total_responsible'] );
		if( isset( $arrValues['total_not_responsible'] ) && $boolDirectSet ) $this->set( 'm_intTotalNotResponsible', trim( $arrValues['total_not_responsible'] ) ); elseif( isset( $arrValues['total_not_responsible'] ) ) $this->setTotalNotResponsible( $arrValues['total_not_responsible'] );
		if( isset( $arrValues['total_guarantors'] ) && $boolDirectSet ) $this->set( 'm_intTotalGuarantors', trim( $arrValues['total_guarantors'] ) ); elseif( isset( $arrValues['total_guarantors'] ) ) $this->setTotalGuarantors( $arrValues['total_guarantors'] );
		if( isset( $arrValues['total_non_leasing_applicants'] ) && $boolDirectSet ) $this->set( 'm_intTotalNonLeasingApplicants', trim( $arrValues['total_non_leasing_applicants'] ) ); elseif( isset( $arrValues['total_non_leasing_applicants'] ) ) $this->setTotalNonLeasingApplicants( $arrValues['total_non_leasing_applicants'] );
		if( isset( $arrValues['total_applicants'] ) && $boolDirectSet ) $this->set( 'm_intTotalApplicants', trim( $arrValues['total_applicants'] ) ); elseif( isset( $arrValues['total_applicants'] ) ) $this->setTotalApplicants( $arrValues['total_applicants'] );
		if( isset( $arrValues['guest_card_field_completions'] ) && $boolDirectSet ) $this->set( 'm_intGuestCardFieldCompletions', trim( $arrValues['guest_card_field_completions'] ) ); elseif( isset( $arrValues['guest_card_field_completions'] ) ) $this->setGuestCardFieldCompletions( $arrValues['guest_card_field_completions'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['application_datetime'] ) && $boolDirectSet ) $this->set( 'm_strApplicationDatetime', trim( $arrValues['application_datetime'] ) ); elseif( isset( $arrValues['application_datetime'] ) ) $this->setApplicationDatetime( $arrValues['application_datetime'] );
		if( isset( $arrValues['url_referer'] ) && $boolDirectSet ) $this->set( 'm_strUrlReferer', trim( $arrValues['url_referer'] ) ); elseif( isset( $arrValues['url_referer'] ) ) $this->setUrlReferer( $arrValues['url_referer'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['quotes_count'] ) && $boolDirectSet ) $this->set( 'm_intQuotesCount', trim( $arrValues['quotes_count'] ) ); elseif( isset( $arrValues['quotes_count'] ) ) $this->setQuotesCount( $arrValues['quotes_count'] );
		if( isset( $arrValues['conversion_probability'] ) && $boolDirectSet ) $this->set( 'm_fltConversionProbability', trim( $arrValues['conversion_probability'] ) ); elseif( isset( $arrValues['conversion_probability'] ) ) $this->setConversionProbability( $arrValues['conversion_probability'] );
		if( isset( $arrValues['first_contacted_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstContactedOn', trim( $arrValues['first_contacted_on'] ) ); elseif( isset( $arrValues['first_contacted_on'] ) ) $this->setFirstContactedOn( $arrValues['first_contacted_on'] );
		if( isset( $arrValues['time_to_first_response'] ) && $boolDirectSet ) $this->set( 'm_strTimeToFirstResponse', trim( $arrValues['time_to_first_response'] ) ); elseif( isset( $arrValues['time_to_first_response'] ) ) $this->setTimeToFirstResponse( $arrValues['time_to_first_response'] );
		if( isset( $arrValues['site_visited_on'] ) && $boolDirectSet ) $this->set( 'm_strSiteVisitedOn', trim( $arrValues['site_visited_on'] ) ); elseif( isset( $arrValues['site_visited_on'] ) ) $this->setSiteVisitedOn( $arrValues['site_visited_on'] );
		if( isset( $arrValues['property_toured_on'] ) && $boolDirectSet ) $this->set( 'm_strPropertyTouredOn', trim( $arrValues['property_toured_on'] ) ); elseif( isset( $arrValues['property_toured_on'] ) ) $this->setPropertyTouredOn( $arrValues['property_toured_on'] );
		if( isset( $arrValues['unit_toured_on'] ) && $boolDirectSet ) $this->set( 'm_strUnitTouredOn', trim( $arrValues['unit_toured_on'] ) ); elseif( isset( $arrValues['unit_toured_on'] ) ) $this->setUnitTouredOn( $arrValues['unit_toured_on'] );
		if( isset( $arrValues['blue_moon_application_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strBlueMoonApplicationSentOn', trim( $arrValues['blue_moon_application_sent_on'] ) ); elseif( isset( $arrValues['blue_moon_application_sent_on'] ) ) $this->setBlueMoonApplicationSentOn( $arrValues['blue_moon_application_sent_on'] );
		if( isset( $arrValues['info_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strInfoApprovedOn', trim( $arrValues['info_approved_on'] ) ); elseif( isset( $arrValues['info_approved_on'] ) ) $this->setInfoApprovedOn( $arrValues['info_approved_on'] );
		if( isset( $arrValues['pre_qualification_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strPreQualificationCompletedOn', trim( $arrValues['pre_qualification_completed_on'] ) ); elseif( isset( $arrValues['pre_qualification_completed_on'] ) ) $this->setPreQualificationCompletedOn( $arrValues['pre_qualification_completed_on'] );
		if( isset( $arrValues['pre_qualification_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strPreQualificationApprovedOn', trim( $arrValues['pre_qualification_approved_on'] ) ); elseif( isset( $arrValues['pre_qualification_approved_on'] ) ) $this->setPreQualificationApprovedOn( $arrValues['pre_qualification_approved_on'] );
		if( isset( $arrValues['application_started_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationStartedOn', trim( $arrValues['application_started_on'] ) ); elseif( isset( $arrValues['application_started_on'] ) ) $this->setApplicationStartedOn( $arrValues['application_started_on'] );
		if( isset( $arrValues['application_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationCompletedOn', trim( $arrValues['application_completed_on'] ) ); elseif( isset( $arrValues['application_completed_on'] ) ) $this->setApplicationCompletedOn( $arrValues['application_completed_on'] );
		if( isset( $arrValues['application_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationApprovedOn', trim( $arrValues['application_approved_on'] ) ); elseif( isset( $arrValues['application_approved_on'] ) ) $this->setApplicationApprovedOn( $arrValues['application_approved_on'] );
		if( isset( $arrValues['screening_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningApprovedOn', trim( $arrValues['screening_approved_on'] ) ); elseif( isset( $arrValues['screening_approved_on'] ) ) $this->setScreeningApprovedOn( $arrValues['screening_approved_on'] );
		if( isset( $arrValues['lease_generated_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseGeneratedOn', trim( $arrValues['lease_generated_on'] ) ); elseif( isset( $arrValues['lease_generated_on'] ) ) $this->setLeaseGeneratedOn( $arrValues['lease_generated_on'] );
		if( isset( $arrValues['lease_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseCompletedOn', trim( $arrValues['lease_completed_on'] ) ); elseif( isset( $arrValues['lease_completed_on'] ) ) $this->setLeaseCompletedOn( $arrValues['lease_completed_on'] );
		if( isset( $arrValues['lease_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseApprovedOn', trim( $arrValues['lease_approved_on'] ) ); elseif( isset( $arrValues['lease_approved_on'] ) ) $this->setLeaseApprovedOn( $arrValues['lease_approved_on'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['transferred_in_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredInOn', trim( $arrValues['transferred_in_on'] ) ); elseif( isset( $arrValues['transferred_in_on'] ) ) $this->setTransferredInOn( $arrValues['transferred_in_on'] );
		if( isset( $arrValues['transferred_out_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredOutOn', trim( $arrValues['transferred_out_on'] ) ); elseif( isset( $arrValues['transferred_out_on'] ) ) $this->setTransferredOutOn( $arrValues['transferred_out_on'] );
		if( isset( $arrValues['mute_followups_until'] ) && $boolDirectSet ) $this->set( 'm_strMuteFollowupsUntil', trim( $arrValues['mute_followups_until'] ) ); elseif( isset( $arrValues['mute_followups_until'] ) ) $this->setMuteFollowupsUntil( $arrValues['mute_followups_until'] );
		if( isset( $arrValues['quoted_rent_locked_until'] ) && $boolDirectSet ) $this->set( 'm_strQuotedRentLockedUntil', trim( $arrValues['quoted_rent_locked_until'] ) ); elseif( isset( $arrValues['quoted_rent_locked_until'] ) ) $this->setQuotedRentLockedUntil( $arrValues['quoted_rent_locked_until'] );
		if( isset( $arrValues['hold_unit_until'] ) && $boolDirectSet ) $this->set( 'm_strHoldUnitUntil', trim( $arrValues['hold_unit_until'] ) ); elseif( isset( $arrValues['hold_unit_until'] ) ) $this->setHoldUnitUntil( $arrValues['hold_unit_until'] );
		if( isset( $arrValues['wait_list_start_on'] ) && $boolDirectSet ) $this->set( 'm_strWaitListStartOn', trim( $arrValues['wait_list_start_on'] ) ); elseif( isset( $arrValues['wait_list_start_on'] ) ) $this->setWaitListStartOn( $arrValues['wait_list_start_on'] );
		if( isset( $arrValues['wait_list_end_on'] ) && $boolDirectSet ) $this->set( 'm_strWaitListEndOn', trim( $arrValues['wait_list_end_on'] ) ); elseif( isset( $arrValues['wait_list_end_on'] ) ) $this->setWaitListEndOn( $arrValues['wait_list_end_on'] );
		if( isset( $arrValues['requires_capture'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresCapture', trim( stripcslashes( $arrValues['requires_capture'] ) ) ); elseif( isset( $arrValues['requires_capture'] ) ) $this->setRequiresCapture( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_capture'] ) : $arrValues['requires_capture'] );
		if( isset( $arrValues['is_pet_policy_conflicted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPetPolicyConflicted', trim( stripcslashes( $arrValues['is_pet_policy_conflicted'] ) ) ); elseif( isset( $arrValues['is_pet_policy_conflicted'] ) ) $this->setIsPetPolicyConflicted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pet_policy_conflicted'] ) : $arrValues['is_pet_policy_conflicted'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostMonthIgnored', trim( stripcslashes( $arrValues['is_post_month_ignored'] ) ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_month_ignored'] ) : $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostDateIgnored', trim( stripcslashes( $arrValues['is_post_date_ignored'] ) ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_date_ignored'] ) : $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_boolIsOpeningLog', trim( stripcslashes( $arrValues['is_opening_log'] ) ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_opening_log'] ) : $arrValues['is_opening_log'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['wait_list_points'] ) && $boolDirectSet ) $this->set( 'm_fltWaitListPoints', trim( $arrValues['wait_list_points'] ) ); elseif( isset( $arrValues['wait_list_points'] ) ) $this->setWaitListPoints( $arrValues['wait_list_points'] );
		if( isset( $arrValues['application_completed_on_initial'] ) && $boolDirectSet ) $this->set( 'm_strApplicationCompletedOnInitial', trim( $arrValues['application_completed_on_initial'] ) ); elseif( isset( $arrValues['application_completed_on_initial'] ) ) $this->setApplicationCompletedOnInitial( $arrValues['application_completed_on_initial'] );
		if( isset( $arrValues['application_approved_on_initial'] ) && $boolDirectSet ) $this->set( 'm_strApplicationApprovedOnInitial', trim( $arrValues['application_approved_on_initial'] ) ); elseif( isset( $arrValues['application_approved_on_initial'] ) ) $this->setApplicationApprovedOnInitial( $arrValues['application_approved_on_initial'] );
		if( isset( $arrValues['application_reopened_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationReopenedOn', trim( $arrValues['application_reopened_on'] ) ); elseif( isset( $arrValues['application_reopened_on'] ) ) $this->setApplicationReopenedOn( $arrValues['application_reopened_on'] );
		if( isset( $arrValues['lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalType', trim( $arrValues['lease_interval_type'] ) ); elseif( isset( $arrValues['lease_interval_type'] ) ) $this->setLeaseIntervalType( $arrValues['lease_interval_type'] );
		if( isset( $arrValues['lease_status_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStatusType', trim( $arrValues['lease_status_type'] ) ); elseif( isset( $arrValues['lease_status_type'] ) ) $this->setLeaseStatusType( $arrValues['lease_status_type'] );
		if( isset( $arrValues['lease_term'] ) && $boolDirectSet ) $this->set( 'm_strLeaseTerm', trim( $arrValues['lease_term'] ) ); elseif( isset( $arrValues['lease_term'] ) ) $this->setLeaseTerm( $arrValues['lease_term'] );
		if( isset( $arrValues['lease_start_window_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartWindowStartDate', trim( $arrValues['lease_start_window_start_date'] ) ); elseif( isset( $arrValues['lease_start_window_start_date'] ) ) $this->setLeaseStartWindowStartDate( $arrValues['lease_start_window_start_date'] );
		if( isset( $arrValues['lease_start_window_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartWindowEndDate', trim( $arrValues['lease_start_window_end_date'] ) ); elseif( isset( $arrValues['lease_start_window_end_date'] ) ) $this->setLeaseStartWindowEndDate( $arrValues['lease_start_window_end_date'] );
		if( isset( $arrValues['contract_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractBaseRent', trim( $arrValues['contract_base_rent'] ) ); elseif( isset( $arrValues['contract_base_rent'] ) ) $this->setContractBaseRent( $arrValues['contract_base_rent'] );
		if( isset( $arrValues['contract_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractAmenityRent', trim( $arrValues['contract_amenity_rent'] ) ); elseif( isset( $arrValues['contract_amenity_rent'] ) ) $this->setContractAmenityRent( $arrValues['contract_amenity_rent'] );
		if( isset( $arrValues['contract_pet_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractPetRent', trim( $arrValues['contract_pet_rent'] ) ); elseif( isset( $arrValues['contract_pet_rent'] ) ) $this->setContractPetRent( $arrValues['contract_pet_rent'] );
		if( isset( $arrValues['contract_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractAddOnRent', trim( $arrValues['contract_add_on_rent'] ) ); elseif( isset( $arrValues['contract_add_on_rent'] ) ) $this->setContractAddOnRent( $arrValues['contract_add_on_rent'] );
		if( isset( $arrValues['contract_risk_premium_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractRiskPremiumRent', trim( $arrValues['contract_risk_premium_rent'] ) ); elseif( isset( $arrValues['contract_risk_premium_rent'] ) ) $this->setContractRiskPremiumRent( $arrValues['contract_risk_premium_rent'] );
		if( isset( $arrValues['contract_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractSpecialRent', trim( $arrValues['contract_special_rent'] ) ); elseif( isset( $arrValues['contract_special_rent'] ) ) $this->setContractSpecialRent( $arrValues['contract_special_rent'] );
		if( isset( $arrValues['contract_total_rent'] ) && $boolDirectSet ) $this->set( 'm_fltContractTotalRent', trim( $arrValues['contract_total_rent'] ) ); elseif( isset( $arrValues['contract_total_rent'] ) ) $this->setContractTotalRent( $arrValues['contract_total_rent'] );
		if( isset( $arrValues['contract_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractBaseDeposit', trim( $arrValues['contract_base_deposit'] ) ); elseif( isset( $arrValues['contract_base_deposit'] ) ) $this->setContractBaseDeposit( $arrValues['contract_base_deposit'] );
		if( isset( $arrValues['contract_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractAmenityDeposit', trim( $arrValues['contract_amenity_deposit'] ) ); elseif( isset( $arrValues['contract_amenity_deposit'] ) ) $this->setContractAmenityDeposit( $arrValues['contract_amenity_deposit'] );
		if( isset( $arrValues['contract_pet_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractPetDeposit', trim( $arrValues['contract_pet_deposit'] ) ); elseif( isset( $arrValues['contract_pet_deposit'] ) ) $this->setContractPetDeposit( $arrValues['contract_pet_deposit'] );
		if( isset( $arrValues['contract_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractAddOnDeposit', trim( $arrValues['contract_add_on_deposit'] ) ); elseif( isset( $arrValues['contract_add_on_deposit'] ) ) $this->setContractAddOnDeposit( $arrValues['contract_add_on_deposit'] );
		if( isset( $arrValues['contract_risk_premium_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractRiskPremiumDeposit', trim( $arrValues['contract_risk_premium_deposit'] ) ); elseif( isset( $arrValues['contract_risk_premium_deposit'] ) ) $this->setContractRiskPremiumDeposit( $arrValues['contract_risk_premium_deposit'] );
		if( isset( $arrValues['contract_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractSpecialDeposit', trim( $arrValues['contract_special_deposit'] ) ); elseif( isset( $arrValues['contract_special_deposit'] ) ) $this->setContractSpecialDeposit( $arrValues['contract_special_deposit'] );
		if( isset( $arrValues['contract_base_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractBaseOther', trim( $arrValues['contract_base_other'] ) ); elseif( isset( $arrValues['contract_base_other'] ) ) $this->setContractBaseOther( $arrValues['contract_base_other'] );
		if( isset( $arrValues['contract_amenity_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractAmenityOther', trim( $arrValues['contract_amenity_other'] ) ); elseif( isset( $arrValues['contract_amenity_other'] ) ) $this->setContractAmenityOther( $arrValues['contract_amenity_other'] );
		if( isset( $arrValues['contract_pet_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractPetOther', trim( $arrValues['contract_pet_other'] ) ); elseif( isset( $arrValues['contract_pet_other'] ) ) $this->setContractPetOther( $arrValues['contract_pet_other'] );
		if( isset( $arrValues['contract_add_on_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractAddOnOther', trim( $arrValues['contract_add_on_other'] ) ); elseif( isset( $arrValues['contract_add_on_other'] ) ) $this->setContractAddOnOther( $arrValues['contract_add_on_other'] );
		if( isset( $arrValues['contract_risk_premium_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractRiskPremiumOther', trim( $arrValues['contract_risk_premium_other'] ) ); elseif( isset( $arrValues['contract_risk_premium_other'] ) ) $this->setContractRiskPremiumOther( $arrValues['contract_risk_premium_other'] );
		if( isset( $arrValues['contract_special_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractSpecialOther', trim( $arrValues['contract_special_other'] ) ); elseif( isset( $arrValues['contract_special_other'] ) ) $this->setContractSpecialOther( $arrValues['contract_special_other'] );
		if( isset( $arrValues['contract_total_other'] ) && $boolDirectSet ) $this->set( 'm_fltContractTotalOther', trim( $arrValues['contract_total_other'] ) ); elseif( isset( $arrValues['contract_total_other'] ) ) $this->setContractTotalOther( $arrValues['contract_total_other'] );
		if( isset( $arrValues['contract_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltContractTotalDeposit', trim( $arrValues['contract_total_deposit'] ) ); elseif( isset( $arrValues['contract_total_deposit'] ) ) $this->setContractTotalDeposit( $arrValues['contract_total_deposit'] );
		if( isset( $arrValues['application_partially_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationPartiallyCompletedOn', trim( $arrValues['application_partially_completed_on'] ) ); elseif( isset( $arrValues['application_partially_completed_on'] ) ) $this->setApplicationPartiallyCompletedOn( $arrValues['application_partially_completed_on'] );
		if( isset( $arrValues['lease_completed_on_initial'] ) && $boolDirectSet ) $this->set( 'm_strLeaseCompletedOnInitial', trim( $arrValues['lease_completed_on_initial'] ) ); elseif( isset( $arrValues['lease_completed_on_initial'] ) ) $this->setLeaseCompletedOnInitial( $arrValues['lease_completed_on_initial'] );
		if( isset( $arrValues['lease_partially_completed_on_initial'] ) && $boolDirectSet ) $this->set( 'm_strLeasePartiallyCompletedOnInitial', trim( $arrValues['lease_partially_completed_on_initial'] ) ); elseif( isset( $arrValues['lease_partially_completed_on_initial'] ) ) $this->setLeasePartiallyCompletedOnInitial( $arrValues['lease_partially_completed_on_initial'] );
		if( isset( $arrValues['lease_approved_on_initial'] ) && $boolDirectSet ) $this->set( 'm_strLeaseApprovedOnInitial', trim( $arrValues['lease_approved_on_initial'] ) ); elseif( isset( $arrValues['lease_approved_on_initial'] ) ) $this->setLeaseApprovedOnInitial( $arrValues['lease_approved_on_initial'] );
		if( isset( $arrValues['unit_space_rented_on'] ) && $boolDirectSet ) $this->set( 'm_strUnitSpaceRentedOn', trim( $arrValues['unit_space_rented_on'] ) ); elseif( isset( $arrValues['unit_space_rented_on'] ) ) $this->setUnitSpaceRentedOn( $arrValues['unit_space_rented_on'] );
		if( isset( $arrValues['default_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultListItemId', trim( $arrValues['default_list_item_id'] ) ); elseif( isset( $arrValues['default_list_item_id'] ) ) $this->setDefaultListItemId( $arrValues['default_list_item_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setPriorCachedApplicationLogId( $intPriorCachedApplicationLogId ) {
		$this->set( 'm_intPriorCachedApplicationLogId', CStrings::strToIntDef( $intPriorCachedApplicationLogId, NULL, false ) );
	}

	public function getPriorCachedApplicationLogId() {
		return $this->m_intPriorCachedApplicationLogId;
	}

	public function sqlPriorCachedApplicationLogId() {
		return ( true == isset( $this->m_intPriorCachedApplicationLogId ) ) ? ( string ) $this->m_intPriorCachedApplicationLogId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : '1';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : '1';
	}

	public function setLeaseTypeId( $intLeaseTypeId ) {
		$this->set( 'm_intLeaseTypeId', CStrings::strToIntDef( $intLeaseTypeId, NULL, false ) );
	}

	public function getLeaseTypeId() {
		return $this->m_intLeaseTypeId;
	}

	public function sqlLeaseTypeId() {
		return ( true == isset( $this->m_intLeaseTypeId ) ) ? ( string ) $this->m_intLeaseTypeId : 'NULL';
	}

	public function setLeadConversionType( $strLeadConversionType ) {
		$this->set( 'm_strLeadConversionType', CStrings::strTrimDef( $strLeadConversionType, -1, NULL, true ) );
	}

	public function getLeadConversionType() {
		return $this->m_strLeadConversionType;
	}

	public function sqlLeadConversionType() {
		return ( true == isset( $this->m_strLeadConversionType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeadConversionType ) : '\'' . addslashes( $this->m_strLeadConversionType ) . '\'' ) : '\'Incomplete\'';
	}

	public function setOldApplicationStatusTypeId( $intOldApplicationStatusTypeId ) {
		$this->set( 'm_intOldApplicationStatusTypeId', CStrings::strToIntDef( $intOldApplicationStatusTypeId, NULL, false ) );
	}

	public function getOldApplicationStatusTypeId() {
		return $this->m_intOldApplicationStatusTypeId;
	}

	public function sqlOldApplicationStatusTypeId() {
		return ( true == isset( $this->m_intOldApplicationStatusTypeId ) ) ? ( string ) $this->m_intOldApplicationStatusTypeId : 'NULL';
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->set( 'm_intApplicationStageId', CStrings::strToIntDef( $intApplicationStageId, NULL, false ) );
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function sqlApplicationStageId() {
		return ( true == isset( $this->m_intApplicationStageId ) ) ? ( string ) $this->m_intApplicationStageId : '1';
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->set( 'm_intApplicationStatusId', CStrings::strToIntDef( $intApplicationStatusId, NULL, false ) );
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function sqlApplicationStatusId() {
		return ( true == isset( $this->m_intApplicationStatusId ) ) ? ( string ) $this->m_intApplicationStatusId : '1';
	}

	public function setApplicationStepId( $intApplicationStepId ) {
		$this->set( 'm_intApplicationStepId', CStrings::strToIntDef( $intApplicationStepId, NULL, false ) );
	}

	public function getApplicationStepId() {
		return $this->m_intApplicationStepId;
	}

	public function sqlApplicationStepId() {
		return ( true == isset( $this->m_intApplicationStepId ) ) ? ( string ) $this->m_intApplicationStepId : '1';
	}

	public function setMaxApplicationStageId( $intMaxApplicationStageId ) {
		$this->set( 'm_intMaxApplicationStageId', CStrings::strToIntDef( $intMaxApplicationStageId, NULL, false ) );
	}

	public function getMaxApplicationStageId() {
		return $this->m_intMaxApplicationStageId;
	}

	public function sqlMaxApplicationStageId() {
		return ( true == isset( $this->m_intMaxApplicationStageId ) ) ? ( string ) $this->m_intMaxApplicationStageId : '1';
	}

	public function setMaxApplicationStatusId( $intMaxApplicationStatusId ) {
		$this->set( 'm_intMaxApplicationStatusId', CStrings::strToIntDef( $intMaxApplicationStatusId, NULL, false ) );
	}

	public function getMaxApplicationStatusId() {
		return $this->m_intMaxApplicationStatusId;
	}

	public function sqlMaxApplicationStatusId() {
		return ( true == isset( $this->m_intMaxApplicationStatusId ) ) ? ( string ) $this->m_intMaxApplicationStatusId : '1';
	}

	public function setMaxApplicationStepId( $intMaxApplicationStepId ) {
		$this->set( 'm_intMaxApplicationStepId', CStrings::strToIntDef( $intMaxApplicationStepId, NULL, false ) );
	}

	public function getMaxApplicationStepId() {
		return $this->m_intMaxApplicationStepId;
	}

	public function sqlMaxApplicationStepId() {
		return ( true == isset( $this->m_intMaxApplicationStepId ) ) ? ( string ) $this->m_intMaxApplicationStepId : '1';
	}

	public function setCancellationListTypeId( $intCancellationListTypeId ) {
		$this->set( 'm_intCancellationListTypeId', CStrings::strToIntDef( $intCancellationListTypeId, NULL, false ) );
	}

	public function getCancellationListTypeId() {
		return $this->m_intCancellationListTypeId;
	}

	public function sqlCancellationListTypeId() {
		return ( true == isset( $this->m_intCancellationListTypeId ) ) ? ( string ) $this->m_intCancellationListTypeId : 'NULL';
	}

	public function setCancellationListItemId( $intCancellationListItemId ) {
		$this->set( 'm_intCancellationListItemId', CStrings::strToIntDef( $intCancellationListItemId, NULL, false ) );
	}

	public function getCancellationListItemId() {
		return $this->m_intCancellationListItemId;
	}

	public function sqlCancellationListItemId() {
		return ( true == isset( $this->m_intCancellationListItemId ) ) ? ( string ) $this->m_intCancellationListItemId : 'NULL';
	}

	public function setPrimaryApplicantId( $intPrimaryApplicantId ) {
		$this->set( 'm_intPrimaryApplicantId', CStrings::strToIntDef( $intPrimaryApplicantId, NULL, false ) );
	}

	public function getPrimaryApplicantId() {
		return $this->m_intPrimaryApplicantId;
	}

	public function sqlPrimaryApplicantId() {
		return ( true == isset( $this->m_intPrimaryApplicantId ) ) ? ( string ) $this->m_intPrimaryApplicantId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : 'NULL';
	}

	public function setDesiredSpaceConfigurationId( $intDesiredSpaceConfigurationId ) {
		$this->set( 'm_intDesiredSpaceConfigurationId', CStrings::strToIntDef( $intDesiredSpaceConfigurationId, NULL, false ) );
	}

	public function getDesiredSpaceConfigurationId() {
		return $this->m_intDesiredSpaceConfigurationId;
	}

	public function sqlDesiredSpaceConfigurationId() {
		return ( true == isset( $this->m_intDesiredSpaceConfigurationId ) ) ? ( string ) $this->m_intDesiredSpaceConfigurationId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setApplicationPsProductId( $intApplicationPsProductId ) {
		$this->set( 'm_intApplicationPsProductId', CStrings::strToIntDef( $intApplicationPsProductId, NULL, false ) );
	}

	public function getApplicationPsProductId() {
		return $this->m_intApplicationPsProductId;
	}

	public function sqlApplicationPsProductId() {
		return ( true == isset( $this->m_intApplicationPsProductId ) ) ? ( string ) $this->m_intApplicationPsProductId : 'NULL';
	}

	public function setLeasePsProductId( $intLeasePsProductId ) {
		$this->set( 'm_intLeasePsProductId', CStrings::strToIntDef( $intLeasePsProductId, NULL, false ) );
	}

	public function getLeasePsProductId() {
		return $this->m_intLeasePsProductId;
	}

	public function sqlLeasePsProductId() {
		return ( true == isset( $this->m_intLeasePsProductId ) ) ? ( string ) $this->m_intLeasePsProductId : 'NULL';
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->set( 'm_intLeasingAgentId', CStrings::strToIntDef( $intLeasingAgentId, NULL, false ) );
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function sqlLeasingAgentId() {
		return ( true == isset( $this->m_intLeasingAgentId ) ) ? ( string ) $this->m_intLeasingAgentId : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setOriginatingLeadSourceId( $intOriginatingLeadSourceId ) {
		$this->set( 'm_intOriginatingLeadSourceId', CStrings::strToIntDef( $intOriginatingLeadSourceId, NULL, false ) );
	}

	public function getOriginatingLeadSourceId() {
		return $this->m_intOriginatingLeadSourceId;
	}

	public function sqlOriginatingLeadSourceId() {
		return ( true == isset( $this->m_intOriginatingLeadSourceId ) ) ? ( string ) $this->m_intOriginatingLeadSourceId : 'NULL';
	}

	public function setConvertingLeadSourceId( $intConvertingLeadSourceId ) {
		$this->set( 'm_intConvertingLeadSourceId', CStrings::strToIntDef( $intConvertingLeadSourceId, NULL, false ) );
	}

	public function getConvertingLeadSourceId() {
		return $this->m_intConvertingLeadSourceId;
	}

	public function sqlConvertingLeadSourceId() {
		return ( true == isset( $this->m_intConvertingLeadSourceId ) ) ? ( string ) $this->m_intConvertingLeadSourceId : 'NULL';
	}

	public function setCompanyApplicationId( $intCompanyApplicationId ) {
		$this->set( 'm_intCompanyApplicationId', CStrings::strToIntDef( $intCompanyApplicationId, NULL, false ) );
	}

	public function getCompanyApplicationId() {
		return $this->m_intCompanyApplicationId;
	}

	public function sqlCompanyApplicationId() {
		return ( true == isset( $this->m_intCompanyApplicationId ) ) ? ( string ) $this->m_intCompanyApplicationId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->set( 'm_intLateFeeFormulaId', CStrings::strToIntDef( $intLateFeeFormulaId, NULL, false ) );
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function sqlLateFeeFormulaId() {
		return ( true == isset( $this->m_intLateFeeFormulaId ) ) ? ( string ) $this->m_intLateFeeFormulaId : 'NULL';
	}

	public function setCombinedApplicationId( $intCombinedApplicationId ) {
		$this->set( 'm_intCombinedApplicationId', CStrings::strToIntDef( $intCombinedApplicationId, NULL, false ) );
	}

	public function getCombinedApplicationId() {
		return $this->m_intCombinedApplicationId;
	}

	public function sqlCombinedApplicationId() {
		return ( true == isset( $this->m_intCombinedApplicationId ) ) ? ( string ) $this->m_intCombinedApplicationId : 'NULL';
	}

	public function setQuoteId( $intQuoteId ) {
		$this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function sqlQuoteId() {
		return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setCraigslistPostId( $intCraigslistPostId ) {
		$this->set( 'm_intCraigslistPostId', CStrings::strToIntDef( $intCraigslistPostId, NULL, false ) );
	}

	public function getCraigslistPostId() {
		return $this->m_intCraigslistPostId;
	}

	public function sqlCraigslistPostId() {
		return ( true == isset( $this->m_intCraigslistPostId ) ) ? ( string ) $this->m_intCraigslistPostId : 'NULL';
	}

	public function setClAdId( $intClAdId ) {
		$this->set( 'm_intClAdId', CStrings::strToIntDef( $intClAdId, NULL, false ) );
	}

	public function getClAdId() {
		return $this->m_intClAdId;
	}

	public function sqlClAdId() {
		return ( true == isset( $this->m_intClAdId ) ) ? ( string ) $this->m_intClAdId : 'NULL';
	}

	public function setTrafficCookieId( $intTrafficCookieId ) {
		$this->set( 'm_intTrafficCookieId', CStrings::strToIntDef( $intTrafficCookieId, NULL, false ) );
	}

	public function getTrafficCookieId() {
		return $this->m_intTrafficCookieId;
	}

	public function sqlTrafficCookieId() {
		return ( true == isset( $this->m_intTrafficCookieId ) ) ? ( string ) $this->m_intTrafficCookieId : 'NULL';
	}

	public function setFirstEventId( $intFirstEventId ) {
		$this->set( 'm_intFirstEventId', CStrings::strToIntDef( $intFirstEventId, NULL, false ) );
	}

	public function getFirstEventId() {
		return $this->m_intFirstEventId;
	}

	public function sqlFirstEventId() {
		return ( true == isset( $this->m_intFirstEventId ) ) ? ( string ) $this->m_intFirstEventId : 'NULL';
	}

	public function setFirstEventMediumId( $intFirstEventMediumId ) {
		$this->set( 'm_intFirstEventMediumId', CStrings::strToIntDef( $intFirstEventMediumId, NULL, false ) );
	}

	public function getFirstEventMediumId() {
		return $this->m_intFirstEventMediumId;
	}

	public function sqlFirstEventMediumId() {
		return ( true == isset( $this->m_intFirstEventMediumId ) ) ? ( string ) $this->m_intFirstEventMediumId : 'NULL';
	}

	public function setFirstLeasingCenterEventId( $intFirstLeasingCenterEventId ) {
		$this->set( 'm_intFirstLeasingCenterEventId', CStrings::strToIntDef( $intFirstLeasingCenterEventId, NULL, false ) );
	}

	public function getFirstLeasingCenterEventId() {
		return $this->m_intFirstLeasingCenterEventId;
	}

	public function sqlFirstLeasingCenterEventId() {
		return ( true == isset( $this->m_intFirstLeasingCenterEventId ) ) ? ( string ) $this->m_intFirstLeasingCenterEventId : 'NULL';
	}

	public function setResponseEventId( $intResponseEventId ) {
		$this->set( 'm_intResponseEventId', CStrings::strToIntDef( $intResponseEventId, NULL, false ) );
	}

	public function getResponseEventId() {
		return $this->m_intResponseEventId;
	}

	public function sqlResponseEventId() {
		return ( true == isset( $this->m_intResponseEventId ) ) ? ( string ) $this->m_intResponseEventId : 'NULL';
	}

	public function setLastEventId( $intLastEventId ) {
		$this->set( 'm_intLastEventId', CStrings::strToIntDef( $intLastEventId, NULL, false ) );
	}

	public function getLastEventId() {
		return $this->m_intLastEventId;
	}

	public function sqlLastEventId() {
		return ( true == isset( $this->m_intLastEventId ) ) ? ( string ) $this->m_intLastEventId : 'NULL';
	}

	public function setLeaseSignatureTypeId( $intLeaseSignatureTypeId ) {
		$this->set( 'm_intLeaseSignatureTypeId', CStrings::strToIntDef( $intLeaseSignatureTypeId, NULL, false ) );
	}

	public function getLeaseSignatureTypeId() {
		return $this->m_intLeaseSignatureTypeId;
	}

	public function sqlLeaseSignatureTypeId() {
		return ( true == isset( $this->m_intLeaseSignatureTypeId ) ) ? ( string ) $this->m_intLeaseSignatureTypeId : '1';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId ) {
		$this->set( 'm_intInsurancePolicyStatusTypeId', CStrings::strToIntDef( $intInsurancePolicyStatusTypeId, NULL, false ) );
	}

	public function getInsurancePolicyStatusTypeId() {
		return $this->m_intInsurancePolicyStatusTypeId;
	}

	public function sqlInsurancePolicyStatusTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyStatusTypeId ) ) ? ( string ) $this->m_intInsurancePolicyStatusTypeId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setWaitListId( $intWaitListId ) {
		$this->set( 'm_intWaitListId', CStrings::strToIntDef( $intWaitListId, NULL, false ) );
	}

	public function getWaitListId() {
		return $this->m_intWaitListId;
	}

	public function sqlWaitListId() {
		return ( true == isset( $this->m_intWaitListId ) ) ? ( string ) $this->m_intWaitListId : 'NULL';
	}

	public function setWaitListApplicationId( $intWaitListApplicationId ) {
		$this->set( 'm_intWaitListApplicationId', CStrings::strToIntDef( $intWaitListApplicationId, NULL, false ) );
	}

	public function getWaitListApplicationId() {
		return $this->m_intWaitListApplicationId;
	}

	public function sqlWaitListApplicationId() {
		return ( true == isset( $this->m_intWaitListApplicationId ) ) ? ( string ) $this->m_intWaitListApplicationId : 'NULL';
	}

	public function setWaitListPointIds( $arrintWaitListPointIds ) {
		$this->set( 'm_arrintWaitListPointIds', CStrings::strToArrIntDef( $arrintWaitListPointIds, NULL ) );
	}

	public function getWaitListPointIds() {
		return $this->m_arrintWaitListPointIds;
	}

	public function sqlWaitListPointIds() {
		return ( true == isset( $this->m_arrintWaitListPointIds ) && true == valArr( $this->m_arrintWaitListPointIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintWaitListPointIds, NULL ) . '\'' : 'NULL';
	}

	public function setWaitListPosition( $intWaitListPosition ) {
		$this->set( 'm_intWaitListPosition', CStrings::strToIntDef( $intWaitListPosition, NULL, false ) );
	}

	public function getWaitListPosition() {
		return $this->m_intWaitListPosition;
	}

	public function sqlWaitListPosition() {
		return ( true == isset( $this->m_intWaitListPosition ) ) ? ( string ) $this->m_intWaitListPosition : 'NULL';
	}

	public function setWaitListRejectionCount( $intWaitListRejectionCount ) {
		$this->set( 'm_intWaitListRejectionCount', CStrings::strToIntDef( $intWaitListRejectionCount, NULL, false ) );
	}

	public function getWaitListRejectionCount() {
		return $this->m_intWaitListRejectionCount;
	}

	public function sqlWaitListRejectionCount() {
		return ( true == isset( $this->m_intWaitListRejectionCount ) ) ? ( string ) $this->m_intWaitListRejectionCount : 'NULL';
	}

	public function setGuestRemotePrimaryKey( $strGuestRemotePrimaryKey ) {
		$this->set( 'm_strGuestRemotePrimaryKey', CStrings::strTrimDef( $strGuestRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getGuestRemotePrimaryKey() {
		return $this->m_strGuestRemotePrimaryKey;
	}

	public function sqlGuestRemotePrimaryKey() {
		return ( true == isset( $this->m_strGuestRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGuestRemotePrimaryKey ) : '\'' . addslashes( $this->m_strGuestRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setAppRemotePrimaryKey( $strAppRemotePrimaryKey ) {
		$this->set( 'm_strAppRemotePrimaryKey', CStrings::strTrimDef( $strAppRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getAppRemotePrimaryKey() {
		return $this->m_strAppRemotePrimaryKey;
	}

	public function sqlAppRemotePrimaryKey() {
		return ( true == isset( $this->m_strAppRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAppRemotePrimaryKey ) : '\'' . addslashes( $this->m_strAppRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumberCache ) : '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' ) : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setUnitSquareFeet( $fltUnitSquareFeet ) {
		$this->set( 'm_fltUnitSquareFeet', CStrings::strToFloatDef( $fltUnitSquareFeet, NULL, false, 4 ) );
	}

	public function getUnitSquareFeet() {
		return $this->m_fltUnitSquareFeet;
	}

	public function sqlUnitSquareFeet() {
		return ( true == isset( $this->m_fltUnitSquareFeet ) ) ? ( string ) $this->m_fltUnitSquareFeet : 'NULL';
	}

	public function setUnitPriorApplicationId( $intUnitPriorApplicationId ) {
		$this->set( 'm_intUnitPriorApplicationId', CStrings::strToIntDef( $intUnitPriorApplicationId, NULL, false ) );
	}

	public function getUnitPriorApplicationId() {
		return $this->m_intUnitPriorApplicationId;
	}

	public function sqlUnitPriorApplicationId() {
		return ( true == isset( $this->m_intUnitPriorApplicationId ) ) ? ( string ) $this->m_intUnitPriorApplicationId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->set( 'm_strPrimaryPhoneNumber', CStrings::strTrimDef( $strPrimaryPhoneNumber, 30, NULL, true ) );
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function sqlPrimaryPhoneNumber() {
		return ( true == isset( $this->m_strPrimaryPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryPhoneNumber ) : '\'' . addslashes( $this->m_strPrimaryPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTermMonth( $intTermMonth ) {
		$this->set( 'm_intTermMonth', CStrings::strToIntDef( $intTermMonth, NULL, false ) );
	}

	public function getTermMonth() {
		return $this->m_intTermMonth;
	}

	public function sqlTermMonth() {
		return ( true == isset( $this->m_intTermMonth ) ) ? ( string ) $this->m_intTermMonth : '0';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NOW()';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setOptimalMonthlyRentBase( $fltOptimalMonthlyRentBase ) {
		$this->set( 'm_fltOptimalMonthlyRentBase', CStrings::strToFloatDef( $fltOptimalMonthlyRentBase, NULL, false, 2 ) );
	}

	public function getOptimalMonthlyRentBase() {
		return $this->m_fltOptimalMonthlyRentBase;
	}

	public function sqlOptimalMonthlyRentBase() {
		return ( true == isset( $this->m_fltOptimalMonthlyRentBase ) ) ? ( string ) $this->m_fltOptimalMonthlyRentBase : '0';
	}

	public function setOptimalMonthlyRentTotal( $fltOptimalMonthlyRentTotal ) {
		$this->set( 'm_fltOptimalMonthlyRentTotal', CStrings::strToFloatDef( $fltOptimalMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function getOptimalMonthlyRentTotal() {
		return $this->m_fltOptimalMonthlyRentTotal;
	}

	public function sqlOptimalMonthlyRentTotal() {
		return ( true == isset( $this->m_fltOptimalMonthlyRentTotal ) ) ? ( string ) $this->m_fltOptimalMonthlyRentTotal : '0';
	}

	public function setAdvertisedMonthlyRentBase( $fltAdvertisedMonthlyRentBase ) {
		$this->set( 'm_fltAdvertisedMonthlyRentBase', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentBase, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyRentBase() {
		return $this->m_fltAdvertisedMonthlyRentBase;
	}

	public function sqlAdvertisedMonthlyRentBase() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyRentBase ) ) ? ( string ) $this->m_fltAdvertisedMonthlyRentBase : '0';
	}

	public function setAdvertisedMonthlyRentAmenity( $fltAdvertisedMonthlyRentAmenity ) {
		$this->set( 'm_fltAdvertisedMonthlyRentAmenity', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentAmenity, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyRentAmenity() {
		return $this->m_fltAdvertisedMonthlyRentAmenity;
	}

	public function sqlAdvertisedMonthlyRentAmenity() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyRentAmenity ) ) ? ( string ) $this->m_fltAdvertisedMonthlyRentAmenity : '0';
	}

	public function setAdvertisedMonthlyRentAddOn( $fltAdvertisedMonthlyRentAddOn ) {
		$this->set( 'm_fltAdvertisedMonthlyRentAddOn', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentAddOn, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyRentAddOn() {
		return $this->m_fltAdvertisedMonthlyRentAddOn;
	}

	public function sqlAdvertisedMonthlyRentAddOn() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyRentAddOn ) ) ? ( string ) $this->m_fltAdvertisedMonthlyRentAddOn : '0';
	}

	public function setAdvertisedMonthlyRentSpecial( $fltAdvertisedMonthlyRentSpecial ) {
		$this->set( 'm_fltAdvertisedMonthlyRentSpecial', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentSpecial, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyRentSpecial() {
		return $this->m_fltAdvertisedMonthlyRentSpecial;
	}

	public function sqlAdvertisedMonthlyRentSpecial() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyRentSpecial ) ) ? ( string ) $this->m_fltAdvertisedMonthlyRentSpecial : '0';
	}

	public function setAdvertisedMonthlyRentTotal( $fltAdvertisedMonthlyRentTotal ) {
		$this->set( 'm_fltAdvertisedMonthlyRentTotal', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyRentTotal() {
		return $this->m_fltAdvertisedMonthlyRentTotal;
	}

	public function sqlAdvertisedMonthlyRentTotal() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyRentTotal ) ) ? ( string ) $this->m_fltAdvertisedMonthlyRentTotal : '0';
	}

	public function setAdvertisedMonthlyOtherBase( $fltAdvertisedMonthlyOtherBase ) {
		$this->set( 'm_fltAdvertisedMonthlyOtherBase', CStrings::strToFloatDef( $fltAdvertisedMonthlyOtherBase, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyOtherBase() {
		return $this->m_fltAdvertisedMonthlyOtherBase;
	}

	public function sqlAdvertisedMonthlyOtherBase() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyOtherBase ) ) ? ( string ) $this->m_fltAdvertisedMonthlyOtherBase : '0';
	}

	public function setAdvertisedMonthlyOtherAmenity( $fltAdvertisedMonthlyOtherAmenity ) {
		$this->set( 'm_fltAdvertisedMonthlyOtherAmenity', CStrings::strToFloatDef( $fltAdvertisedMonthlyOtherAmenity, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyOtherAmenity() {
		return $this->m_fltAdvertisedMonthlyOtherAmenity;
	}

	public function sqlAdvertisedMonthlyOtherAmenity() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyOtherAmenity ) ) ? ( string ) $this->m_fltAdvertisedMonthlyOtherAmenity : '0';
	}

	public function setAdvertisedMonthlyOtherAddOn( $fltAdvertisedMonthlyOtherAddOn ) {
		$this->set( 'm_fltAdvertisedMonthlyOtherAddOn', CStrings::strToFloatDef( $fltAdvertisedMonthlyOtherAddOn, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyOtherAddOn() {
		return $this->m_fltAdvertisedMonthlyOtherAddOn;
	}

	public function sqlAdvertisedMonthlyOtherAddOn() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyOtherAddOn ) ) ? ( string ) $this->m_fltAdvertisedMonthlyOtherAddOn : '0';
	}

	public function setAdvertisedMonthlyOtherSpecial( $fltAdvertisedMonthlyOtherSpecial ) {
		$this->set( 'm_fltAdvertisedMonthlyOtherSpecial', CStrings::strToFloatDef( $fltAdvertisedMonthlyOtherSpecial, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyOtherSpecial() {
		return $this->m_fltAdvertisedMonthlyOtherSpecial;
	}

	public function sqlAdvertisedMonthlyOtherSpecial() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyOtherSpecial ) ) ? ( string ) $this->m_fltAdvertisedMonthlyOtherSpecial : '0';
	}

	public function setAdvertisedMonthlyOtherTotal( $fltAdvertisedMonthlyOtherTotal ) {
		$this->set( 'm_fltAdvertisedMonthlyOtherTotal', CStrings::strToFloatDef( $fltAdvertisedMonthlyOtherTotal, NULL, false, 2 ) );
	}

	public function getAdvertisedMonthlyOtherTotal() {
		return $this->m_fltAdvertisedMonthlyOtherTotal;
	}

	public function sqlAdvertisedMonthlyOtherTotal() {
		return ( true == isset( $this->m_fltAdvertisedMonthlyOtherTotal ) ) ? ( string ) $this->m_fltAdvertisedMonthlyOtherTotal : '0';
	}

	public function setAdvertisedOneTimeDepositBase( $fltAdvertisedOneTimeDepositBase ) {
		$this->set( 'm_fltAdvertisedOneTimeDepositBase', CStrings::strToFloatDef( $fltAdvertisedOneTimeDepositBase, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeDepositBase() {
		return $this->m_fltAdvertisedOneTimeDepositBase;
	}

	public function sqlAdvertisedOneTimeDepositBase() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeDepositBase ) ) ? ( string ) $this->m_fltAdvertisedOneTimeDepositBase : '0';
	}

	public function setAdvertisedOneTimeDepositAmenity( $fltAdvertisedOneTimeDepositAmenity ) {
		$this->set( 'm_fltAdvertisedOneTimeDepositAmenity', CStrings::strToFloatDef( $fltAdvertisedOneTimeDepositAmenity, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeDepositAmenity() {
		return $this->m_fltAdvertisedOneTimeDepositAmenity;
	}

	public function sqlAdvertisedOneTimeDepositAmenity() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeDepositAmenity ) ) ? ( string ) $this->m_fltAdvertisedOneTimeDepositAmenity : '0';
	}

	public function setAdvertisedOneTimeDepositAddOn( $fltAdvertisedOneTimeDepositAddOn ) {
		$this->set( 'm_fltAdvertisedOneTimeDepositAddOn', CStrings::strToFloatDef( $fltAdvertisedOneTimeDepositAddOn, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeDepositAddOn() {
		return $this->m_fltAdvertisedOneTimeDepositAddOn;
	}

	public function sqlAdvertisedOneTimeDepositAddOn() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeDepositAddOn ) ) ? ( string ) $this->m_fltAdvertisedOneTimeDepositAddOn : '0';
	}

	public function setAdvertisedOneTimeDepositSpecial( $fltAdvertisedOneTimeDepositSpecial ) {
		$this->set( 'm_fltAdvertisedOneTimeDepositSpecial', CStrings::strToFloatDef( $fltAdvertisedOneTimeDepositSpecial, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeDepositSpecial() {
		return $this->m_fltAdvertisedOneTimeDepositSpecial;
	}

	public function sqlAdvertisedOneTimeDepositSpecial() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeDepositSpecial ) ) ? ( string ) $this->m_fltAdvertisedOneTimeDepositSpecial : '0';
	}

	public function setAdvertisedOneTimeDepositTotal( $fltAdvertisedOneTimeDepositTotal ) {
		$this->set( 'm_fltAdvertisedOneTimeDepositTotal', CStrings::strToFloatDef( $fltAdvertisedOneTimeDepositTotal, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeDepositTotal() {
		return $this->m_fltAdvertisedOneTimeDepositTotal;
	}

	public function sqlAdvertisedOneTimeDepositTotal() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeDepositTotal ) ) ? ( string ) $this->m_fltAdvertisedOneTimeDepositTotal : '0';
	}

	public function setAdvertisedOneTimeApplicationBase( $fltAdvertisedOneTimeApplicationBase ) {
		$this->set( 'm_fltAdvertisedOneTimeApplicationBase', CStrings::strToFloatDef( $fltAdvertisedOneTimeApplicationBase, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeApplicationBase() {
		return $this->m_fltAdvertisedOneTimeApplicationBase;
	}

	public function sqlAdvertisedOneTimeApplicationBase() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeApplicationBase ) ) ? ( string ) $this->m_fltAdvertisedOneTimeApplicationBase : '0';
	}

	public function setAdvertisedOneTimeApplicationAmenity( $fltAdvertisedOneTimeApplicationAmenity ) {
		$this->set( 'm_fltAdvertisedOneTimeApplicationAmenity', CStrings::strToFloatDef( $fltAdvertisedOneTimeApplicationAmenity, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeApplicationAmenity() {
		return $this->m_fltAdvertisedOneTimeApplicationAmenity;
	}

	public function sqlAdvertisedOneTimeApplicationAmenity() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeApplicationAmenity ) ) ? ( string ) $this->m_fltAdvertisedOneTimeApplicationAmenity : '0';
	}

	public function setAdvertisedOneTimeApplicationAddOn( $fltAdvertisedOneTimeApplicationAddOn ) {
		$this->set( 'm_fltAdvertisedOneTimeApplicationAddOn', CStrings::strToFloatDef( $fltAdvertisedOneTimeApplicationAddOn, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeApplicationAddOn() {
		return $this->m_fltAdvertisedOneTimeApplicationAddOn;
	}

	public function sqlAdvertisedOneTimeApplicationAddOn() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeApplicationAddOn ) ) ? ( string ) $this->m_fltAdvertisedOneTimeApplicationAddOn : '0';
	}

	public function setAdvertisedOneTimeApplicationSpecial( $fltAdvertisedOneTimeApplicationSpecial ) {
		$this->set( 'm_fltAdvertisedOneTimeApplicationSpecial', CStrings::strToFloatDef( $fltAdvertisedOneTimeApplicationSpecial, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeApplicationSpecial() {
		return $this->m_fltAdvertisedOneTimeApplicationSpecial;
	}

	public function sqlAdvertisedOneTimeApplicationSpecial() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeApplicationSpecial ) ) ? ( string ) $this->m_fltAdvertisedOneTimeApplicationSpecial : '0';
	}

	public function setAdvertisedOneTimeApplicationTotal( $fltAdvertisedOneTimeApplicationTotal ) {
		$this->set( 'm_fltAdvertisedOneTimeApplicationTotal', CStrings::strToFloatDef( $fltAdvertisedOneTimeApplicationTotal, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeApplicationTotal() {
		return $this->m_fltAdvertisedOneTimeApplicationTotal;
	}

	public function sqlAdvertisedOneTimeApplicationTotal() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeApplicationTotal ) ) ? ( string ) $this->m_fltAdvertisedOneTimeApplicationTotal : '0';
	}

	public function setAdvertisedOneTimeOtherBase( $fltAdvertisedOneTimeOtherBase ) {
		$this->set( 'm_fltAdvertisedOneTimeOtherBase', CStrings::strToFloatDef( $fltAdvertisedOneTimeOtherBase, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeOtherBase() {
		return $this->m_fltAdvertisedOneTimeOtherBase;
	}

	public function sqlAdvertisedOneTimeOtherBase() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeOtherBase ) ) ? ( string ) $this->m_fltAdvertisedOneTimeOtherBase : '0';
	}

	public function setAdvertisedOneTimeOtherAmenity( $fltAdvertisedOneTimeOtherAmenity ) {
		$this->set( 'm_fltAdvertisedOneTimeOtherAmenity', CStrings::strToFloatDef( $fltAdvertisedOneTimeOtherAmenity, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeOtherAmenity() {
		return $this->m_fltAdvertisedOneTimeOtherAmenity;
	}

	public function sqlAdvertisedOneTimeOtherAmenity() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeOtherAmenity ) ) ? ( string ) $this->m_fltAdvertisedOneTimeOtherAmenity : '0';
	}

	public function setAdvertisedOneTimeOtherAddOn( $fltAdvertisedOneTimeOtherAddOn ) {
		$this->set( 'm_fltAdvertisedOneTimeOtherAddOn', CStrings::strToFloatDef( $fltAdvertisedOneTimeOtherAddOn, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeOtherAddOn() {
		return $this->m_fltAdvertisedOneTimeOtherAddOn;
	}

	public function sqlAdvertisedOneTimeOtherAddOn() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeOtherAddOn ) ) ? ( string ) $this->m_fltAdvertisedOneTimeOtherAddOn : '0';
	}

	public function setAdvertisedOneTimeOtherSpecial( $fltAdvertisedOneTimeOtherSpecial ) {
		$this->set( 'm_fltAdvertisedOneTimeOtherSpecial', CStrings::strToFloatDef( $fltAdvertisedOneTimeOtherSpecial, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeOtherSpecial() {
		return $this->m_fltAdvertisedOneTimeOtherSpecial;
	}

	public function sqlAdvertisedOneTimeOtherSpecial() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeOtherSpecial ) ) ? ( string ) $this->m_fltAdvertisedOneTimeOtherSpecial : '0';
	}

	public function setAdvertisedOneTimeOtherTotal( $fltAdvertisedOneTimeOtherTotal ) {
		$this->set( 'm_fltAdvertisedOneTimeOtherTotal', CStrings::strToFloatDef( $fltAdvertisedOneTimeOtherTotal, NULL, false, 2 ) );
	}

	public function getAdvertisedOneTimeOtherTotal() {
		return $this->m_fltAdvertisedOneTimeOtherTotal;
	}

	public function sqlAdvertisedOneTimeOtherTotal() {
		return ( true == isset( $this->m_fltAdvertisedOneTimeOtherTotal ) ) ? ( string ) $this->m_fltAdvertisedOneTimeOtherTotal : '0';
	}

	public function setExecutedMonthlyRentBase( $fltExecutedMonthlyRentBase ) {
		$this->set( 'm_fltExecutedMonthlyRentBase', CStrings::strToFloatDef( $fltExecutedMonthlyRentBase, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentBase() {
		return $this->m_fltExecutedMonthlyRentBase;
	}

	public function sqlExecutedMonthlyRentBase() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentBase ) ) ? ( string ) $this->m_fltExecutedMonthlyRentBase : '0';
	}

	public function setExecutedMonthlyRentAmenity( $fltExecutedMonthlyRentAmenity ) {
		$this->set( 'm_fltExecutedMonthlyRentAmenity', CStrings::strToFloatDef( $fltExecutedMonthlyRentAmenity, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentAmenity() {
		return $this->m_fltExecutedMonthlyRentAmenity;
	}

	public function sqlExecutedMonthlyRentAmenity() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentAmenity ) ) ? ( string ) $this->m_fltExecutedMonthlyRentAmenity : '0';
	}

	public function setExecutedMonthlyRentAddOn( $fltExecutedMonthlyRentAddOn ) {
		$this->set( 'm_fltExecutedMonthlyRentAddOn', CStrings::strToFloatDef( $fltExecutedMonthlyRentAddOn, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentAddOn() {
		return $this->m_fltExecutedMonthlyRentAddOn;
	}

	public function sqlExecutedMonthlyRentAddOn() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentAddOn ) ) ? ( string ) $this->m_fltExecutedMonthlyRentAddOn : '0';
	}

	public function setExecutedMonthlyRentSpecial( $fltExecutedMonthlyRentSpecial ) {
		$this->set( 'm_fltExecutedMonthlyRentSpecial', CStrings::strToFloatDef( $fltExecutedMonthlyRentSpecial, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentSpecial() {
		return $this->m_fltExecutedMonthlyRentSpecial;
	}

	public function sqlExecutedMonthlyRentSpecial() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentSpecial ) ) ? ( string ) $this->m_fltExecutedMonthlyRentSpecial : '0';
	}

	public function setExecutedMonthlyRentRiskPremium( $fltExecutedMonthlyRentRiskPremium ) {
		$this->set( 'm_fltExecutedMonthlyRentRiskPremium', CStrings::strToFloatDef( $fltExecutedMonthlyRentRiskPremium, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentRiskPremium() {
		return $this->m_fltExecutedMonthlyRentRiskPremium;
	}

	public function sqlExecutedMonthlyRentRiskPremium() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentRiskPremium ) ) ? ( string ) $this->m_fltExecutedMonthlyRentRiskPremium : '0';
	}

	public function setExecutedMonthlyRentPet( $fltExecutedMonthlyRentPet ) {
		$this->set( 'm_fltExecutedMonthlyRentPet', CStrings::strToFloatDef( $fltExecutedMonthlyRentPet, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentPet() {
		return $this->m_fltExecutedMonthlyRentPet;
	}

	public function sqlExecutedMonthlyRentPet() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentPet ) ) ? ( string ) $this->m_fltExecutedMonthlyRentPet : '0';
	}

	public function setExecutedMonthlyRentTotal( $fltExecutedMonthlyRentTotal ) {
		$this->set( 'm_fltExecutedMonthlyRentTotal', CStrings::strToFloatDef( $fltExecutedMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyRentTotal() {
		return $this->m_fltExecutedMonthlyRentTotal;
	}

	public function sqlExecutedMonthlyRentTotal() {
		return ( true == isset( $this->m_fltExecutedMonthlyRentTotal ) ) ? ( string ) $this->m_fltExecutedMonthlyRentTotal : '0';
	}

	public function setExecutedMonthlyOtherBase( $fltExecutedMonthlyOtherBase ) {
		$this->set( 'm_fltExecutedMonthlyOtherBase', CStrings::strToFloatDef( $fltExecutedMonthlyOtherBase, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherBase() {
		return $this->m_fltExecutedMonthlyOtherBase;
	}

	public function sqlExecutedMonthlyOtherBase() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherBase ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherBase : '0';
	}

	public function setExecutedMonthlyOtherAmenity( $fltExecutedMonthlyOtherAmenity ) {
		$this->set( 'm_fltExecutedMonthlyOtherAmenity', CStrings::strToFloatDef( $fltExecutedMonthlyOtherAmenity, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherAmenity() {
		return $this->m_fltExecutedMonthlyOtherAmenity;
	}

	public function sqlExecutedMonthlyOtherAmenity() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherAmenity ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherAmenity : '0';
	}

	public function setExecutedMonthlyOtherAddOn( $fltExecutedMonthlyOtherAddOn ) {
		$this->set( 'm_fltExecutedMonthlyOtherAddOn', CStrings::strToFloatDef( $fltExecutedMonthlyOtherAddOn, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherAddOn() {
		return $this->m_fltExecutedMonthlyOtherAddOn;
	}

	public function sqlExecutedMonthlyOtherAddOn() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherAddOn ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherAddOn : '0';
	}

	public function setExecutedMonthlyOtherSpecial( $fltExecutedMonthlyOtherSpecial ) {
		$this->set( 'm_fltExecutedMonthlyOtherSpecial', CStrings::strToFloatDef( $fltExecutedMonthlyOtherSpecial, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherSpecial() {
		return $this->m_fltExecutedMonthlyOtherSpecial;
	}

	public function sqlExecutedMonthlyOtherSpecial() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherSpecial ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherSpecial : '0';
	}

	public function setExecutedMonthlyOtherRiskPremium( $fltExecutedMonthlyOtherRiskPremium ) {
		$this->set( 'm_fltExecutedMonthlyOtherRiskPremium', CStrings::strToFloatDef( $fltExecutedMonthlyOtherRiskPremium, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherRiskPremium() {
		return $this->m_fltExecutedMonthlyOtherRiskPremium;
	}

	public function sqlExecutedMonthlyOtherRiskPremium() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherRiskPremium ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherRiskPremium : '0';
	}

	public function setExecutedMonthlyOtherPet( $fltExecutedMonthlyOtherPet ) {
		$this->set( 'm_fltExecutedMonthlyOtherPet', CStrings::strToFloatDef( $fltExecutedMonthlyOtherPet, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherPet() {
		return $this->m_fltExecutedMonthlyOtherPet;
	}

	public function sqlExecutedMonthlyOtherPet() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherPet ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherPet : '0';
	}

	public function setExecutedMonthlyOtherTotal( $fltExecutedMonthlyOtherTotal ) {
		$this->set( 'm_fltExecutedMonthlyOtherTotal', CStrings::strToFloatDef( $fltExecutedMonthlyOtherTotal, NULL, false, 2 ) );
	}

	public function getExecutedMonthlyOtherTotal() {
		return $this->m_fltExecutedMonthlyOtherTotal;
	}

	public function sqlExecutedMonthlyOtherTotal() {
		return ( true == isset( $this->m_fltExecutedMonthlyOtherTotal ) ) ? ( string ) $this->m_fltExecutedMonthlyOtherTotal : '0';
	}

	public function setExecutedOneTimeDepositBase( $fltExecutedOneTimeDepositBase ) {
		$this->set( 'm_fltExecutedOneTimeDepositBase', CStrings::strToFloatDef( $fltExecutedOneTimeDepositBase, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositBase() {
		return $this->m_fltExecutedOneTimeDepositBase;
	}

	public function sqlExecutedOneTimeDepositBase() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositBase ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositBase : '0';
	}

	public function setExecutedOneTimeDepositAmenity( $fltExecutedOneTimeDepositAmenity ) {
		$this->set( 'm_fltExecutedOneTimeDepositAmenity', CStrings::strToFloatDef( $fltExecutedOneTimeDepositAmenity, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositAmenity() {
		return $this->m_fltExecutedOneTimeDepositAmenity;
	}

	public function sqlExecutedOneTimeDepositAmenity() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositAmenity ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositAmenity : '0';
	}

	public function setExecutedOneTimeDepositAddOn( $fltExecutedOneTimeDepositAddOn ) {
		$this->set( 'm_fltExecutedOneTimeDepositAddOn', CStrings::strToFloatDef( $fltExecutedOneTimeDepositAddOn, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositAddOn() {
		return $this->m_fltExecutedOneTimeDepositAddOn;
	}

	public function sqlExecutedOneTimeDepositAddOn() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositAddOn ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositAddOn : '0';
	}

	public function setExecutedOneTimeDepositSpecial( $fltExecutedOneTimeDepositSpecial ) {
		$this->set( 'm_fltExecutedOneTimeDepositSpecial', CStrings::strToFloatDef( $fltExecutedOneTimeDepositSpecial, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositSpecial() {
		return $this->m_fltExecutedOneTimeDepositSpecial;
	}

	public function sqlExecutedOneTimeDepositSpecial() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositSpecial ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositSpecial : '0';
	}

	public function setExecutedOneTimeDepositRiskPremium( $fltExecutedOneTimeDepositRiskPremium ) {
		$this->set( 'm_fltExecutedOneTimeDepositRiskPremium', CStrings::strToFloatDef( $fltExecutedOneTimeDepositRiskPremium, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositRiskPremium() {
		return $this->m_fltExecutedOneTimeDepositRiskPremium;
	}

	public function sqlExecutedOneTimeDepositRiskPremium() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositRiskPremium ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositRiskPremium : '0';
	}

	public function setExecutedOneTimeDepositPet( $fltExecutedOneTimeDepositPet ) {
		$this->set( 'm_fltExecutedOneTimeDepositPet', CStrings::strToFloatDef( $fltExecutedOneTimeDepositPet, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositPet() {
		return $this->m_fltExecutedOneTimeDepositPet;
	}

	public function sqlExecutedOneTimeDepositPet() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositPet ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositPet : '0';
	}

	public function setExecutedOneTimeDepositTotal( $fltExecutedOneTimeDepositTotal ) {
		$this->set( 'm_fltExecutedOneTimeDepositTotal', CStrings::strToFloatDef( $fltExecutedOneTimeDepositTotal, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeDepositTotal() {
		return $this->m_fltExecutedOneTimeDepositTotal;
	}

	public function sqlExecutedOneTimeDepositTotal() {
		return ( true == isset( $this->m_fltExecutedOneTimeDepositTotal ) ) ? ( string ) $this->m_fltExecutedOneTimeDepositTotal : '0';
	}

	public function setExecutedOneTimeApplicationBase( $fltExecutedOneTimeApplicationBase ) {
		$this->set( 'm_fltExecutedOneTimeApplicationBase', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationBase, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationBase() {
		return $this->m_fltExecutedOneTimeApplicationBase;
	}

	public function sqlExecutedOneTimeApplicationBase() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationBase ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationBase : '0';
	}

	public function setExecutedOneTimeApplicationAmenity( $fltExecutedOneTimeApplicationAmenity ) {
		$this->set( 'm_fltExecutedOneTimeApplicationAmenity', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationAmenity, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationAmenity() {
		return $this->m_fltExecutedOneTimeApplicationAmenity;
	}

	public function sqlExecutedOneTimeApplicationAmenity() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationAmenity ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationAmenity : '0';
	}

	public function setExecutedOneTimeApplicationAddOn( $fltExecutedOneTimeApplicationAddOn ) {
		$this->set( 'm_fltExecutedOneTimeApplicationAddOn', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationAddOn, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationAddOn() {
		return $this->m_fltExecutedOneTimeApplicationAddOn;
	}

	public function sqlExecutedOneTimeApplicationAddOn() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationAddOn ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationAddOn : '0';
	}

	public function setExecutedOneTimeApplicationSpecial( $fltExecutedOneTimeApplicationSpecial ) {
		$this->set( 'm_fltExecutedOneTimeApplicationSpecial', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationSpecial, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationSpecial() {
		return $this->m_fltExecutedOneTimeApplicationSpecial;
	}

	public function sqlExecutedOneTimeApplicationSpecial() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationSpecial ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationSpecial : '0';
	}

	public function setExecutedOneTimeApplicationRiskPremium( $fltExecutedOneTimeApplicationRiskPremium ) {
		$this->set( 'm_fltExecutedOneTimeApplicationRiskPremium', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationRiskPremium, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationRiskPremium() {
		return $this->m_fltExecutedOneTimeApplicationRiskPremium;
	}

	public function sqlExecutedOneTimeApplicationRiskPremium() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationRiskPremium ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationRiskPremium : '0';
	}

	public function setExecutedOneTimeApplicationPet( $fltExecutedOneTimeApplicationPet ) {
		$this->set( 'm_fltExecutedOneTimeApplicationPet', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationPet, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationPet() {
		return $this->m_fltExecutedOneTimeApplicationPet;
	}

	public function sqlExecutedOneTimeApplicationPet() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationPet ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationPet : '0';
	}

	public function setExecutedOneTimeApplicationTotal( $fltExecutedOneTimeApplicationTotal ) {
		$this->set( 'm_fltExecutedOneTimeApplicationTotal', CStrings::strToFloatDef( $fltExecutedOneTimeApplicationTotal, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeApplicationTotal() {
		return $this->m_fltExecutedOneTimeApplicationTotal;
	}

	public function sqlExecutedOneTimeApplicationTotal() {
		return ( true == isset( $this->m_fltExecutedOneTimeApplicationTotal ) ) ? ( string ) $this->m_fltExecutedOneTimeApplicationTotal : '0';
	}

	public function setExecutedOneTimeOtherBase( $fltExecutedOneTimeOtherBase ) {
		$this->set( 'm_fltExecutedOneTimeOtherBase', CStrings::strToFloatDef( $fltExecutedOneTimeOtherBase, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherBase() {
		return $this->m_fltExecutedOneTimeOtherBase;
	}

	public function sqlExecutedOneTimeOtherBase() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherBase ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherBase : '0';
	}

	public function setExecutedOneTimeOtherAmenity( $fltExecutedOneTimeOtherAmenity ) {
		$this->set( 'm_fltExecutedOneTimeOtherAmenity', CStrings::strToFloatDef( $fltExecutedOneTimeOtherAmenity, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherAmenity() {
		return $this->m_fltExecutedOneTimeOtherAmenity;
	}

	public function sqlExecutedOneTimeOtherAmenity() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherAmenity ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherAmenity : '0';
	}

	public function setExecutedOneTimeOtherAddOn( $fltExecutedOneTimeOtherAddOn ) {
		$this->set( 'm_fltExecutedOneTimeOtherAddOn', CStrings::strToFloatDef( $fltExecutedOneTimeOtherAddOn, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherAddOn() {
		return $this->m_fltExecutedOneTimeOtherAddOn;
	}

	public function sqlExecutedOneTimeOtherAddOn() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherAddOn ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherAddOn : '0';
	}

	public function setExecutedOneTimeOtherSpecial( $fltExecutedOneTimeOtherSpecial ) {
		$this->set( 'm_fltExecutedOneTimeOtherSpecial', CStrings::strToFloatDef( $fltExecutedOneTimeOtherSpecial, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherSpecial() {
		return $this->m_fltExecutedOneTimeOtherSpecial;
	}

	public function sqlExecutedOneTimeOtherSpecial() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherSpecial ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherSpecial : '0';
	}

	public function setExecutedOneTimeOtherRiskPremium( $fltExecutedOneTimeOtherRiskPremium ) {
		$this->set( 'm_fltExecutedOneTimeOtherRiskPremium', CStrings::strToFloatDef( $fltExecutedOneTimeOtherRiskPremium, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherRiskPremium() {
		return $this->m_fltExecutedOneTimeOtherRiskPremium;
	}

	public function sqlExecutedOneTimeOtherRiskPremium() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherRiskPremium ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherRiskPremium : '0';
	}

	public function setExecutedOneTimeOtherPet( $fltExecutedOneTimeOtherPet ) {
		$this->set( 'm_fltExecutedOneTimeOtherPet', CStrings::strToFloatDef( $fltExecutedOneTimeOtherPet, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherPet() {
		return $this->m_fltExecutedOneTimeOtherPet;
	}

	public function sqlExecutedOneTimeOtherPet() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherPet ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherPet : '0';
	}

	public function setExecutedOneTimeOtherTotal( $fltExecutedOneTimeOtherTotal ) {
		$this->set( 'm_fltExecutedOneTimeOtherTotal', CStrings::strToFloatDef( $fltExecutedOneTimeOtherTotal, NULL, false, 2 ) );
	}

	public function getExecutedOneTimeOtherTotal() {
		return $this->m_fltExecutedOneTimeOtherTotal;
	}

	public function sqlExecutedOneTimeOtherTotal() {
		return ( true == isset( $this->m_fltExecutedOneTimeOtherTotal ) ) ? ( string ) $this->m_fltExecutedOneTimeOtherTotal : '0';
	}

	public function setExecutedOptionalMonthlyRentDog( $fltExecutedOptionalMonthlyRentDog ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentDog', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentDog, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentDog() {
		return $this->m_fltExecutedOptionalMonthlyRentDog;
	}

	public function sqlExecutedOptionalMonthlyRentDog() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentDog ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentDog : '0';
	}

	public function setExecutedOptionalMonthlyRentCat( $fltExecutedOptionalMonthlyRentCat ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentCat', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentCat, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentCat() {
		return $this->m_fltExecutedOptionalMonthlyRentCat;
	}

	public function sqlExecutedOptionalMonthlyRentCat() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentCat ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentCat : '0';
	}

	public function setExecutedOptionalMonthlyRentOtherPet( $fltExecutedOptionalMonthlyRentOtherPet ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentOtherPet', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentOtherPet, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentOtherPet() {
		return $this->m_fltExecutedOptionalMonthlyRentOtherPet;
	}

	public function sqlExecutedOptionalMonthlyRentOtherPet() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentOtherPet ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentOtherPet : '0';
	}

	public function setExecutedOptionalMonthlyRentGarage( $fltExecutedOptionalMonthlyRentGarage ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentGarage', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentGarage, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentGarage() {
		return $this->m_fltExecutedOptionalMonthlyRentGarage;
	}

	public function sqlExecutedOptionalMonthlyRentGarage() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentGarage ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentGarage : '0';
	}

	public function setExecutedOptionalMonthlyRentAppliances( $fltExecutedOptionalMonthlyRentAppliances ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentAppliances', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentAppliances, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentAppliances() {
		return $this->m_fltExecutedOptionalMonthlyRentAppliances;
	}

	public function sqlExecutedOptionalMonthlyRentAppliances() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentAppliances ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentAppliances : '0';
	}

	public function setExecutedOptionalMonthlyRentFurniture( $fltExecutedOptionalMonthlyRentFurniture ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentFurniture', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentFurniture, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentFurniture() {
		return $this->m_fltExecutedOptionalMonthlyRentFurniture;
	}

	public function sqlExecutedOptionalMonthlyRentFurniture() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentFurniture ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentFurniture : '0';
	}

	public function setExecutedOptionalMonthlyRentFurnished( $fltExecutedOptionalMonthlyRentFurnished ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentFurnished', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentFurnished, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentFurnished() {
		return $this->m_fltExecutedOptionalMonthlyRentFurnished;
	}

	public function sqlExecutedOptionalMonthlyRentFurnished() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentFurnished ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentFurnished : '0';
	}

	public function setExecutedOptionalMonthlyRentStorage( $fltExecutedOptionalMonthlyRentStorage ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentStorage', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentStorage, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentStorage() {
		return $this->m_fltExecutedOptionalMonthlyRentStorage;
	}

	public function sqlExecutedOptionalMonthlyRentStorage() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentStorage ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentStorage : '0';
	}

	public function setExecutedOptionalMonthlyRentParking( $fltExecutedOptionalMonthlyRentParking ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentParking', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentParking, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentParking() {
		return $this->m_fltExecutedOptionalMonthlyRentParking;
	}

	public function sqlExecutedOptionalMonthlyRentParking() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentParking ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentParking : '0';
	}

	public function setExecutedOptionalMonthlyRentMealPlans( $fltExecutedOptionalMonthlyRentMealPlans ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentMealPlans', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentMealPlans, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentMealPlans() {
		return $this->m_fltExecutedOptionalMonthlyRentMealPlans;
	}

	public function sqlExecutedOptionalMonthlyRentMealPlans() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentMealPlans ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentMealPlans : '0';
	}

	public function setExecutedOptionalMonthlyRentOtherAddOn( $fltExecutedOptionalMonthlyRentOtherAddOn ) {
		$this->set( 'm_fltExecutedOptionalMonthlyRentOtherAddOn', CStrings::strToFloatDef( $fltExecutedOptionalMonthlyRentOtherAddOn, NULL, false, 2 ) );
	}

	public function getExecutedOptionalMonthlyRentOtherAddOn() {
		return $this->m_fltExecutedOptionalMonthlyRentOtherAddOn;
	}

	public function sqlExecutedOptionalMonthlyRentOtherAddOn() {
		return ( true == isset( $this->m_fltExecutedOptionalMonthlyRentOtherAddOn ) ) ? ( string ) $this->m_fltExecutedOptionalMonthlyRentOtherAddOn : '0';
	}

	public function setVehicleCount( $intVehicleCount ) {
		$this->set( 'm_intVehicleCount', CStrings::strToIntDef( $intVehicleCount, NULL, false ) );
	}

	public function getVehicleCount() {
		return $this->m_intVehicleCount;
	}

	public function sqlVehicleCount() {
		return ( true == isset( $this->m_intVehicleCount ) ) ? ( string ) $this->m_intVehicleCount : '0';
	}

	public function setHouseholdIncome( $intHouseholdIncome ) {
		$this->set( 'm_intHouseholdIncome', CStrings::strToIntDef( $intHouseholdIncome, NULL, false ) );
	}

	public function getHouseholdIncome() {
		return $this->m_intHouseholdIncome;
	}

	public function sqlHouseholdIncome() {
		return ( true == isset( $this->m_intHouseholdIncome ) ) ? ( string ) $this->m_intHouseholdIncome : '0';
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		$this->set( 'm_fltDesiredRentMin', CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2 ) );
	}

	public function getDesiredRentMin() {
		return $this->m_fltDesiredRentMin;
	}

	public function sqlDesiredRentMin() {
		return ( true == isset( $this->m_fltDesiredRentMin ) ) ? ( string ) $this->m_fltDesiredRentMin : 'NULL';
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		$this->set( 'm_fltDesiredRentMax', CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2 ) );
	}

	public function getDesiredRentMax() {
		return $this->m_fltDesiredRentMax;
	}

	public function sqlDesiredRentMax() {
		return ( true == isset( $this->m_fltDesiredRentMax ) ) ? ( string ) $this->m_fltDesiredRentMax : 'NULL';
	}

	public function setDesiredBedrooms( $intDesiredBedrooms ) {
		$this->set( 'm_intDesiredBedrooms', CStrings::strToIntDef( $intDesiredBedrooms, NULL, false ) );
	}

	public function getDesiredBedrooms() {
		return $this->m_intDesiredBedrooms;
	}

	public function sqlDesiredBedrooms() {
		return ( true == isset( $this->m_intDesiredBedrooms ) ) ? ( string ) $this->m_intDesiredBedrooms : 'NULL';
	}

	public function setDesiredBathrooms( $fltDesiredBathrooms ) {
		$this->set( 'm_fltDesiredBathrooms', CStrings::strToFloatDef( $fltDesiredBathrooms, NULL, false, 2 ) );
	}

	public function getDesiredBathrooms() {
		return $this->m_fltDesiredBathrooms;
	}

	public function sqlDesiredBathrooms() {
		return ( true == isset( $this->m_fltDesiredBathrooms ) ) ? ( string ) $this->m_fltDesiredBathrooms : 'NULL';
	}

	public function setDesiredPets( $intDesiredPets ) {
		$this->set( 'm_intDesiredPets', CStrings::strToIntDef( $intDesiredPets, NULL, false ) );
	}

	public function getDesiredPets() {
		return $this->m_intDesiredPets;
	}

	public function sqlDesiredPets() {
		return ( true == isset( $this->m_intDesiredPets ) ) ? ( string ) $this->m_intDesiredPets : 'NULL';
	}

	public function setDesiredOccupants( $intDesiredOccupants ) {
		$this->set( 'm_intDesiredOccupants', CStrings::strToIntDef( $intDesiredOccupants, NULL, false ) );
	}

	public function getDesiredOccupants() {
		return $this->m_intDesiredOccupants;
	}

	public function sqlDesiredOccupants() {
		return ( true == isset( $this->m_intDesiredOccupants ) ) ? ( string ) $this->m_intDesiredOccupants : 'NULL';
	}

	public function setDesiredAmenities( $arrintDesiredAmenities ) {
		$this->set( 'm_arrintDesiredAmenities', CStrings::strToArrIntDef( $arrintDesiredAmenities, NULL ) );
	}

	public function getDesiredAmenities() {
		return $this->m_arrintDesiredAmenities;
	}

	public function sqlDesiredAmenities() {
		return ( true == isset( $this->m_arrintDesiredAmenities ) && true == valArr( $this->m_arrintDesiredAmenities ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDesiredAmenities, NULL ) . '\'' : 'NULL';
	}

	public function setTotalResponsible( $intTotalResponsible ) {
		$this->set( 'm_intTotalResponsible', CStrings::strToIntDef( $intTotalResponsible, NULL, false ) );
	}

	public function getTotalResponsible() {
		return $this->m_intTotalResponsible;
	}

	public function sqlTotalResponsible() {
		return ( true == isset( $this->m_intTotalResponsible ) ) ? ( string ) $this->m_intTotalResponsible : '0';
	}

	public function setTotalNotResponsible( $intTotalNotResponsible ) {
		$this->set( 'm_intTotalNotResponsible', CStrings::strToIntDef( $intTotalNotResponsible, NULL, false ) );
	}

	public function getTotalNotResponsible() {
		return $this->m_intTotalNotResponsible;
	}

	public function sqlTotalNotResponsible() {
		return ( true == isset( $this->m_intTotalNotResponsible ) ) ? ( string ) $this->m_intTotalNotResponsible : '0';
	}

	public function setTotalGuarantors( $intTotalGuarantors ) {
		$this->set( 'm_intTotalGuarantors', CStrings::strToIntDef( $intTotalGuarantors, NULL, false ) );
	}

	public function getTotalGuarantors() {
		return $this->m_intTotalGuarantors;
	}

	public function sqlTotalGuarantors() {
		return ( true == isset( $this->m_intTotalGuarantors ) ) ? ( string ) $this->m_intTotalGuarantors : '0';
	}

	public function setTotalNonLeasingApplicants( $intTotalNonLeasingApplicants ) {
		$this->set( 'm_intTotalNonLeasingApplicants', CStrings::strToIntDef( $intTotalNonLeasingApplicants, NULL, false ) );
	}

	public function getTotalNonLeasingApplicants() {
		return $this->m_intTotalNonLeasingApplicants;
	}

	public function sqlTotalNonLeasingApplicants() {
		return ( true == isset( $this->m_intTotalNonLeasingApplicants ) ) ? ( string ) $this->m_intTotalNonLeasingApplicants : '0';
	}

	public function setTotalApplicants( $intTotalApplicants ) {
		$this->set( 'm_intTotalApplicants', CStrings::strToIntDef( $intTotalApplicants, NULL, false ) );
	}

	public function getTotalApplicants() {
		return $this->m_intTotalApplicants;
	}

	public function sqlTotalApplicants() {
		return ( true == isset( $this->m_intTotalApplicants ) ) ? ( string ) $this->m_intTotalApplicants : '0';
	}

	public function setGuestCardFieldCompletions( $intGuestCardFieldCompletions ) {
		$this->set( 'm_intGuestCardFieldCompletions', CStrings::strToIntDef( $intGuestCardFieldCompletions, NULL, false ) );
	}

	public function getGuestCardFieldCompletions() {
		return $this->m_intGuestCardFieldCompletions;
	}

	public function sqlGuestCardFieldCompletions() {
		return ( true == isset( $this->m_intGuestCardFieldCompletions ) ) ? ( string ) $this->m_intGuestCardFieldCompletions : '0';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setApplicationDatetime( $strApplicationDatetime ) {
		$this->set( 'm_strApplicationDatetime', CStrings::strTrimDef( $strApplicationDatetime, -1, NULL, true ) );
	}

	public function getApplicationDatetime() {
		return $this->m_strApplicationDatetime;
	}

	public function sqlApplicationDatetime() {
		return ( true == isset( $this->m_strApplicationDatetime ) ) ? '\'' . $this->m_strApplicationDatetime . '\'' : 'NOW()';
	}

	public function setUrlReferer( $strUrlReferer ) {
		$this->set( 'm_strUrlReferer', CStrings::strTrimDef( $strUrlReferer, 2500, NULL, true ) );
	}

	public function getUrlReferer() {
		return $this->m_strUrlReferer;
	}

	public function sqlUrlReferer() {
		return ( true == isset( $this->m_strUrlReferer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrlReferer ) : '\'' . addslashes( $this->m_strUrlReferer ) . '\'' ) : 'NULL';
	}

	public function setQuotesCount( $intQuotesCount ) {
		$this->set( 'm_intQuotesCount', CStrings::strToIntDef( $intQuotesCount, NULL, false ) );
	}

	public function getQuotesCount() {
		return $this->m_intQuotesCount;
	}

	public function sqlQuotesCount() {
		return ( true == isset( $this->m_intQuotesCount ) ) ? ( string ) $this->m_intQuotesCount : '0';
	}

	public function setConversionProbability( $fltConversionProbability ) {
		$this->set( 'm_fltConversionProbability', CStrings::strToFloatDef( $fltConversionProbability, NULL, false, 2 ) );
	}

	public function getConversionProbability() {
		return $this->m_fltConversionProbability;
	}

	public function sqlConversionProbability() {
		return ( true == isset( $this->m_fltConversionProbability ) ) ? ( string ) $this->m_fltConversionProbability : '0';
	}

	public function setFirstContactedOn( $strFirstContactedOn ) {
		$this->set( 'm_strFirstContactedOn', CStrings::strTrimDef( $strFirstContactedOn, -1, NULL, true ) );
	}

	public function getFirstContactedOn() {
		return $this->m_strFirstContactedOn;
	}

	public function sqlFirstContactedOn() {
		return ( true == isset( $this->m_strFirstContactedOn ) ) ? '\'' . $this->m_strFirstContactedOn . '\'' : 'NULL';
	}

	public function setTimeToFirstResponse( $strTimeToFirstResponse ) {
		$this->set( 'm_strTimeToFirstResponse', CStrings::strTrimDef( $strTimeToFirstResponse, NULL, NULL, true ) );
	}

	public function getTimeToFirstResponse() {
		return $this->m_strTimeToFirstResponse;
	}

	public function sqlTimeToFirstResponse() {
		return ( true == isset( $this->m_strTimeToFirstResponse ) ) ? '\'' . $this->m_strTimeToFirstResponse . '\'' : 'NULL';
	}

	public function setSiteVisitedOn( $strSiteVisitedOn ) {
		$this->set( 'm_strSiteVisitedOn', CStrings::strTrimDef( $strSiteVisitedOn, -1, NULL, true ) );
	}

	public function getSiteVisitedOn() {
		return $this->m_strSiteVisitedOn;
	}

	public function sqlSiteVisitedOn() {
		return ( true == isset( $this->m_strSiteVisitedOn ) ) ? '\'' . $this->m_strSiteVisitedOn . '\'' : 'NULL';
	}

	public function setPropertyTouredOn( $strPropertyTouredOn ) {
		$this->set( 'm_strPropertyTouredOn', CStrings::strTrimDef( $strPropertyTouredOn, -1, NULL, true ) );
	}

	public function getPropertyTouredOn() {
		return $this->m_strPropertyTouredOn;
	}

	public function sqlPropertyTouredOn() {
		return ( true == isset( $this->m_strPropertyTouredOn ) ) ? '\'' . $this->m_strPropertyTouredOn . '\'' : 'NULL';
	}

	public function setUnitTouredOn( $strUnitTouredOn ) {
		$this->set( 'm_strUnitTouredOn', CStrings::strTrimDef( $strUnitTouredOn, -1, NULL, true ) );
	}

	public function getUnitTouredOn() {
		return $this->m_strUnitTouredOn;
	}

	public function sqlUnitTouredOn() {
		return ( true == isset( $this->m_strUnitTouredOn ) ) ? '\'' . $this->m_strUnitTouredOn . '\'' : 'NULL';
	}

	public function setBlueMoonApplicationSentOn( $strBlueMoonApplicationSentOn ) {
		$this->set( 'm_strBlueMoonApplicationSentOn', CStrings::strTrimDef( $strBlueMoonApplicationSentOn, -1, NULL, true ) );
	}

	public function getBlueMoonApplicationSentOn() {
		return $this->m_strBlueMoonApplicationSentOn;
	}

	public function sqlBlueMoonApplicationSentOn() {
		return ( true == isset( $this->m_strBlueMoonApplicationSentOn ) ) ? '\'' . $this->m_strBlueMoonApplicationSentOn . '\'' : 'NULL';
	}

	public function setInfoApprovedOn( $strInfoApprovedOn ) {
		$this->set( 'm_strInfoApprovedOn', CStrings::strTrimDef( $strInfoApprovedOn, -1, NULL, true ) );
	}

	public function getInfoApprovedOn() {
		return $this->m_strInfoApprovedOn;
	}

	public function sqlInfoApprovedOn() {
		return ( true == isset( $this->m_strInfoApprovedOn ) ) ? '\'' . $this->m_strInfoApprovedOn . '\'' : 'NULL';
	}

	public function setPreQualificationCompletedOn( $strPreQualificationCompletedOn ) {
		$this->set( 'm_strPreQualificationCompletedOn', CStrings::strTrimDef( $strPreQualificationCompletedOn, -1, NULL, true ) );
	}

	public function getPreQualificationCompletedOn() {
		return $this->m_strPreQualificationCompletedOn;
	}

	public function sqlPreQualificationCompletedOn() {
		return ( true == isset( $this->m_strPreQualificationCompletedOn ) ) ? '\'' . $this->m_strPreQualificationCompletedOn . '\'' : 'NULL';
	}

	public function setPreQualificationApprovedOn( $strPreQualificationApprovedOn ) {
		$this->set( 'm_strPreQualificationApprovedOn', CStrings::strTrimDef( $strPreQualificationApprovedOn, -1, NULL, true ) );
	}

	public function getPreQualificationApprovedOn() {
		return $this->m_strPreQualificationApprovedOn;
	}

	public function sqlPreQualificationApprovedOn() {
		return ( true == isset( $this->m_strPreQualificationApprovedOn ) ) ? '\'' . $this->m_strPreQualificationApprovedOn . '\'' : 'NULL';
	}

	public function setApplicationStartedOn( $strApplicationStartedOn ) {
		$this->set( 'm_strApplicationStartedOn', CStrings::strTrimDef( $strApplicationStartedOn, -1, NULL, true ) );
	}

	public function getApplicationStartedOn() {
		return $this->m_strApplicationStartedOn;
	}

	public function sqlApplicationStartedOn() {
		return ( true == isset( $this->m_strApplicationStartedOn ) ) ? '\'' . $this->m_strApplicationStartedOn . '\'' : 'NULL';
	}

	public function setApplicationCompletedOn( $strApplicationCompletedOn ) {
		$this->set( 'm_strApplicationCompletedOn', CStrings::strTrimDef( $strApplicationCompletedOn, -1, NULL, true ) );
	}

	public function getApplicationCompletedOn() {
		return $this->m_strApplicationCompletedOn;
	}

	public function sqlApplicationCompletedOn() {
		return ( true == isset( $this->m_strApplicationCompletedOn ) ) ? '\'' . $this->m_strApplicationCompletedOn . '\'' : 'NULL';
	}

	public function setApplicationApprovedOn( $strApplicationApprovedOn ) {
		$this->set( 'm_strApplicationApprovedOn', CStrings::strTrimDef( $strApplicationApprovedOn, -1, NULL, true ) );
	}

	public function getApplicationApprovedOn() {
		return $this->m_strApplicationApprovedOn;
	}

	public function sqlApplicationApprovedOn() {
		return ( true == isset( $this->m_strApplicationApprovedOn ) ) ? '\'' . $this->m_strApplicationApprovedOn . '\'' : 'NULL';
	}

	public function setScreeningApprovedOn( $strScreeningApprovedOn ) {
		$this->set( 'm_strScreeningApprovedOn', CStrings::strTrimDef( $strScreeningApprovedOn, -1, NULL, true ) );
	}

	public function getScreeningApprovedOn() {
		return $this->m_strScreeningApprovedOn;
	}

	public function sqlScreeningApprovedOn() {
		return ( true == isset( $this->m_strScreeningApprovedOn ) ) ? '\'' . $this->m_strScreeningApprovedOn . '\'' : 'NULL';
	}

	public function setLeaseGeneratedOn( $strLeaseGeneratedOn ) {
		$this->set( 'm_strLeaseGeneratedOn', CStrings::strTrimDef( $strLeaseGeneratedOn, -1, NULL, true ) );
	}

	public function getLeaseGeneratedOn() {
		return $this->m_strLeaseGeneratedOn;
	}

	public function sqlLeaseGeneratedOn() {
		return ( true == isset( $this->m_strLeaseGeneratedOn ) ) ? '\'' . $this->m_strLeaseGeneratedOn . '\'' : 'NULL';
	}

	public function setLeaseCompletedOn( $strLeaseCompletedOn ) {
		$this->set( 'm_strLeaseCompletedOn', CStrings::strTrimDef( $strLeaseCompletedOn, -1, NULL, true ) );
	}

	public function getLeaseCompletedOn() {
		return $this->m_strLeaseCompletedOn;
	}

	public function sqlLeaseCompletedOn() {
		return ( true == isset( $this->m_strLeaseCompletedOn ) ) ? '\'' . $this->m_strLeaseCompletedOn . '\'' : 'NULL';
	}

	public function setLeaseApprovedOn( $strLeaseApprovedOn ) {
		$this->set( 'm_strLeaseApprovedOn', CStrings::strTrimDef( $strLeaseApprovedOn, -1, NULL, true ) );
	}

	public function getLeaseApprovedOn() {
		return $this->m_strLeaseApprovedOn;
	}

	public function sqlLeaseApprovedOn() {
		return ( true == isset( $this->m_strLeaseApprovedOn ) ) ? '\'' . $this->m_strLeaseApprovedOn . '\'' : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setTransferredInOn( $strTransferredInOn ) {
		$this->set( 'm_strTransferredInOn', CStrings::strTrimDef( $strTransferredInOn, -1, NULL, true ) );
	}

	public function getTransferredInOn() {
		return $this->m_strTransferredInOn;
	}

	public function sqlTransferredInOn() {
		return ( true == isset( $this->m_strTransferredInOn ) ) ? '\'' . $this->m_strTransferredInOn . '\'' : 'NULL';
	}

	public function setTransferredOutOn( $strTransferredOutOn ) {
		$this->set( 'm_strTransferredOutOn', CStrings::strTrimDef( $strTransferredOutOn, -1, NULL, true ) );
	}

	public function getTransferredOutOn() {
		return $this->m_strTransferredOutOn;
	}

	public function sqlTransferredOutOn() {
		return ( true == isset( $this->m_strTransferredOutOn ) ) ? '\'' . $this->m_strTransferredOutOn . '\'' : 'NULL';
	}

	public function setMuteFollowupsUntil( $strMuteFollowupsUntil ) {
		$this->set( 'm_strMuteFollowupsUntil', CStrings::strTrimDef( $strMuteFollowupsUntil, -1, NULL, true ) );
	}

	public function getMuteFollowupsUntil() {
		return $this->m_strMuteFollowupsUntil;
	}

	public function sqlMuteFollowupsUntil() {
		return ( true == isset( $this->m_strMuteFollowupsUntil ) ) ? '\'' . $this->m_strMuteFollowupsUntil . '\'' : 'NULL';
	}

	public function setQuotedRentLockedUntil( $strQuotedRentLockedUntil ) {
		$this->set( 'm_strQuotedRentLockedUntil', CStrings::strTrimDef( $strQuotedRentLockedUntil, -1, NULL, true ) );
	}

	public function getQuotedRentLockedUntil() {
		return $this->m_strQuotedRentLockedUntil;
	}

	public function sqlQuotedRentLockedUntil() {
		return ( true == isset( $this->m_strQuotedRentLockedUntil ) ) ? '\'' . $this->m_strQuotedRentLockedUntil . '\'' : 'NULL';
	}

	public function setHoldUnitUntil( $strHoldUnitUntil ) {
		$this->set( 'm_strHoldUnitUntil', CStrings::strTrimDef( $strHoldUnitUntil, -1, NULL, true ) );
	}

	public function getHoldUnitUntil() {
		return $this->m_strHoldUnitUntil;
	}

	public function sqlHoldUnitUntil() {
		return ( true == isset( $this->m_strHoldUnitUntil ) ) ? '\'' . $this->m_strHoldUnitUntil . '\'' : 'NULL';
	}

	public function setWaitListStartOn( $strWaitListStartOn ) {
		$this->set( 'm_strWaitListStartOn', CStrings::strTrimDef( $strWaitListStartOn, -1, NULL, true ) );
	}

	public function getWaitListStartOn() {
		return $this->m_strWaitListStartOn;
	}

	public function sqlWaitListStartOn() {
		return ( true == isset( $this->m_strWaitListStartOn ) ) ? '\'' . $this->m_strWaitListStartOn . '\'' : 'NULL';
	}

	public function setWaitListEndOn( $strWaitListEndOn ) {
		$this->set( 'm_strWaitListEndOn', CStrings::strTrimDef( $strWaitListEndOn, -1, NULL, true ) );
	}

	public function getWaitListEndOn() {
		return $this->m_strWaitListEndOn;
	}

	public function sqlWaitListEndOn() {
		return ( true == isset( $this->m_strWaitListEndOn ) ) ? '\'' . $this->m_strWaitListEndOn . '\'' : 'NULL';
	}

	public function setRequiresCapture( $boolRequiresCapture ) {
		$this->set( 'm_boolRequiresCapture', CStrings::strToBool( $boolRequiresCapture ) );
	}

	public function getRequiresCapture() {
		return $this->m_boolRequiresCapture;
	}

	public function sqlRequiresCapture() {
		return ( true == isset( $this->m_boolRequiresCapture ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresCapture ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPetPolicyConflicted( $boolIsPetPolicyConflicted ) {
		$this->set( 'm_boolIsPetPolicyConflicted', CStrings::strToBool( $boolIsPetPolicyConflicted ) );
	}

	public function getIsPetPolicyConflicted() {
		return $this->m_boolIsPetPolicyConflicted;
	}

	public function sqlIsPetPolicyConflicted() {
		return ( true == isset( $this->m_boolIsPetPolicyConflicted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPetPolicyConflicted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostMonthIgnored( $boolIsPostMonthIgnored ) {
		$this->set( 'm_boolIsPostMonthIgnored', CStrings::strToBool( $boolIsPostMonthIgnored ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_boolIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_boolIsPostMonthIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostMonthIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostDateIgnored( $boolIsPostDateIgnored ) {
		$this->set( 'm_boolIsPostDateIgnored', CStrings::strToBool( $boolIsPostDateIgnored ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_boolIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_boolIsPostDateIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostDateIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOpeningLog( $boolIsOpeningLog ) {
		$this->set( 'm_boolIsOpeningLog', CStrings::strToBool( $boolIsOpeningLog ) );
	}

	public function getIsOpeningLog() {
		return $this->m_boolIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_boolIsOpeningLog ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOpeningLog ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWaitListPoints( $fltWaitListPoints ) {
		$this->set( 'm_fltWaitListPoints', CStrings::strToFloatDef( $fltWaitListPoints, NULL, false, 2 ) );
	}

	public function getWaitListPoints() {
		return $this->m_fltWaitListPoints;
	}

	public function sqlWaitListPoints() {
		return ( true == isset( $this->m_fltWaitListPoints ) ) ? ( string ) $this->m_fltWaitListPoints : 'NULL';
	}

	public function setApplicationCompletedOnInitial( $strApplicationCompletedOnInitial ) {
		$this->set( 'm_strApplicationCompletedOnInitial', CStrings::strTrimDef( $strApplicationCompletedOnInitial, -1, NULL, true ) );
	}

	public function getApplicationCompletedOnInitial() {
		return $this->m_strApplicationCompletedOnInitial;
	}

	public function sqlApplicationCompletedOnInitial() {
		return ( true == isset( $this->m_strApplicationCompletedOnInitial ) ) ? '\'' . $this->m_strApplicationCompletedOnInitial . '\'' : 'NULL';
	}

	public function setApplicationApprovedOnInitial( $strApplicationApprovedOnInitial ) {
		$this->set( 'm_strApplicationApprovedOnInitial', CStrings::strTrimDef( $strApplicationApprovedOnInitial, -1, NULL, true ) );
	}

	public function getApplicationApprovedOnInitial() {
		return $this->m_strApplicationApprovedOnInitial;
	}

	public function sqlApplicationApprovedOnInitial() {
		return ( true == isset( $this->m_strApplicationApprovedOnInitial ) ) ? '\'' . $this->m_strApplicationApprovedOnInitial . '\'' : 'NULL';
	}

	public function setApplicationReopenedOn( $strApplicationReopenedOn ) {
		$this->set( 'm_strApplicationReopenedOn', CStrings::strTrimDef( $strApplicationReopenedOn, -1, NULL, true ) );
	}

	public function getApplicationReopenedOn() {
		return $this->m_strApplicationReopenedOn;
	}

	public function sqlApplicationReopenedOn() {
		return ( true == isset( $this->m_strApplicationReopenedOn ) ) ? '\'' . $this->m_strApplicationReopenedOn . '\'' : 'NULL';
	}

	public function setLeaseIntervalType( $strLeaseIntervalType ) {
		$this->set( 'm_strLeaseIntervalType', CStrings::strTrimDef( $strLeaseIntervalType, 50, NULL, true ) );
	}

	public function getLeaseIntervalType() {
		return $this->m_strLeaseIntervalType;
	}

	public function sqlLeaseIntervalType() {
		return ( true == isset( $this->m_strLeaseIntervalType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseIntervalType ) : '\'' . addslashes( $this->m_strLeaseIntervalType ) . '\'' ) : 'NULL';
	}

	public function setLeaseStatusType( $strLeaseStatusType ) {
		$this->set( 'm_strLeaseStatusType', CStrings::strTrimDef( $strLeaseStatusType, 50, NULL, true ) );
	}

	public function getLeaseStatusType() {
		return $this->m_strLeaseStatusType;
	}

	public function sqlLeaseStatusType() {
		return ( true == isset( $this->m_strLeaseStatusType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseStatusType ) : '\'' . addslashes( $this->m_strLeaseStatusType ) . '\'' ) : 'NULL';
	}

	public function setLeaseTerm( $strLeaseTerm ) {
		$this->set( 'm_strLeaseTerm', CStrings::strTrimDef( $strLeaseTerm, 50, NULL, true ) );
	}

	public function getLeaseTerm() {
		return $this->m_strLeaseTerm;
	}

	public function sqlLeaseTerm() {
		return ( true == isset( $this->m_strLeaseTerm ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseTerm ) : '\'' . addslashes( $this->m_strLeaseTerm ) . '\'' ) : 'NULL';
	}

	public function setLeaseStartWindowStartDate( $strLeaseStartWindowStartDate ) {
		$this->set( 'm_strLeaseStartWindowStartDate', CStrings::strTrimDef( $strLeaseStartWindowStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartWindowStartDate() {
		return $this->m_strLeaseStartWindowStartDate;
	}

	public function sqlLeaseStartWindowStartDate() {
		return ( true == isset( $this->m_strLeaseStartWindowStartDate ) ) ? '\'' . $this->m_strLeaseStartWindowStartDate . '\'' : 'NULL';
	}

	public function setLeaseStartWindowEndDate( $strLeaseStartWindowEndDate ) {
		$this->set( 'm_strLeaseStartWindowEndDate', CStrings::strTrimDef( $strLeaseStartWindowEndDate, -1, NULL, true ) );
	}

	public function getLeaseStartWindowEndDate() {
		return $this->m_strLeaseStartWindowEndDate;
	}

	public function sqlLeaseStartWindowEndDate() {
		return ( true == isset( $this->m_strLeaseStartWindowEndDate ) ) ? '\'' . $this->m_strLeaseStartWindowEndDate . '\'' : 'NULL';
	}

	public function setContractBaseRent( $fltContractBaseRent ) {
		$this->set( 'm_fltContractBaseRent', CStrings::strToFloatDef( $fltContractBaseRent, NULL, false, 2 ) );
	}

	public function getContractBaseRent() {
		return $this->m_fltContractBaseRent;
	}

	public function sqlContractBaseRent() {
		return ( true == isset( $this->m_fltContractBaseRent ) ) ? ( string ) $this->m_fltContractBaseRent : 'NULL';
	}

	public function setContractAmenityRent( $fltContractAmenityRent ) {
		$this->set( 'm_fltContractAmenityRent', CStrings::strToFloatDef( $fltContractAmenityRent, NULL, false, 2 ) );
	}

	public function getContractAmenityRent() {
		return $this->m_fltContractAmenityRent;
	}

	public function sqlContractAmenityRent() {
		return ( true == isset( $this->m_fltContractAmenityRent ) ) ? ( string ) $this->m_fltContractAmenityRent : 'NULL';
	}

	public function setContractPetRent( $fltContractPetRent ) {
		$this->set( 'm_fltContractPetRent', CStrings::strToFloatDef( $fltContractPetRent, NULL, false, 2 ) );
	}

	public function getContractPetRent() {
		return $this->m_fltContractPetRent;
	}

	public function sqlContractPetRent() {
		return ( true == isset( $this->m_fltContractPetRent ) ) ? ( string ) $this->m_fltContractPetRent : 'NULL';
	}

	public function setContractAddOnRent( $fltContractAddOnRent ) {
		$this->set( 'm_fltContractAddOnRent', CStrings::strToFloatDef( $fltContractAddOnRent, NULL, false, 2 ) );
	}

	public function getContractAddOnRent() {
		return $this->m_fltContractAddOnRent;
	}

	public function sqlContractAddOnRent() {
		return ( true == isset( $this->m_fltContractAddOnRent ) ) ? ( string ) $this->m_fltContractAddOnRent : 'NULL';
	}

	public function setContractRiskPremiumRent( $fltContractRiskPremiumRent ) {
		$this->set( 'm_fltContractRiskPremiumRent', CStrings::strToFloatDef( $fltContractRiskPremiumRent, NULL, false, 2 ) );
	}

	public function getContractRiskPremiumRent() {
		return $this->m_fltContractRiskPremiumRent;
	}

	public function sqlContractRiskPremiumRent() {
		return ( true == isset( $this->m_fltContractRiskPremiumRent ) ) ? ( string ) $this->m_fltContractRiskPremiumRent : 'NULL';
	}

	public function setContractSpecialRent( $fltContractSpecialRent ) {
		$this->set( 'm_fltContractSpecialRent', CStrings::strToFloatDef( $fltContractSpecialRent, NULL, false, 2 ) );
	}

	public function getContractSpecialRent() {
		return $this->m_fltContractSpecialRent;
	}

	public function sqlContractSpecialRent() {
		return ( true == isset( $this->m_fltContractSpecialRent ) ) ? ( string ) $this->m_fltContractSpecialRent : 'NULL';
	}

	public function setContractTotalRent( $fltContractTotalRent ) {
		$this->set( 'm_fltContractTotalRent', CStrings::strToFloatDef( $fltContractTotalRent, NULL, false, 2 ) );
	}

	public function getContractTotalRent() {
		return $this->m_fltContractTotalRent;
	}

	public function sqlContractTotalRent() {
		return ( true == isset( $this->m_fltContractTotalRent ) ) ? ( string ) $this->m_fltContractTotalRent : 'NULL';
	}

	public function setContractBaseDeposit( $fltContractBaseDeposit ) {
		$this->set( 'm_fltContractBaseDeposit', CStrings::strToFloatDef( $fltContractBaseDeposit, NULL, false, 2 ) );
	}

	public function getContractBaseDeposit() {
		return $this->m_fltContractBaseDeposit;
	}

	public function sqlContractBaseDeposit() {
		return ( true == isset( $this->m_fltContractBaseDeposit ) ) ? ( string ) $this->m_fltContractBaseDeposit : 'NULL';
	}

	public function setContractAmenityDeposit( $fltContractAmenityDeposit ) {
		$this->set( 'm_fltContractAmenityDeposit', CStrings::strToFloatDef( $fltContractAmenityDeposit, NULL, false, 2 ) );
	}

	public function getContractAmenityDeposit() {
		return $this->m_fltContractAmenityDeposit;
	}

	public function sqlContractAmenityDeposit() {
		return ( true == isset( $this->m_fltContractAmenityDeposit ) ) ? ( string ) $this->m_fltContractAmenityDeposit : 'NULL';
	}

	public function setContractPetDeposit( $fltContractPetDeposit ) {
		$this->set( 'm_fltContractPetDeposit', CStrings::strToFloatDef( $fltContractPetDeposit, NULL, false, 2 ) );
	}

	public function getContractPetDeposit() {
		return $this->m_fltContractPetDeposit;
	}

	public function sqlContractPetDeposit() {
		return ( true == isset( $this->m_fltContractPetDeposit ) ) ? ( string ) $this->m_fltContractPetDeposit : 'NULL';
	}

	public function setContractAddOnDeposit( $fltContractAddOnDeposit ) {
		$this->set( 'm_fltContractAddOnDeposit', CStrings::strToFloatDef( $fltContractAddOnDeposit, NULL, false, 2 ) );
	}

	public function getContractAddOnDeposit() {
		return $this->m_fltContractAddOnDeposit;
	}

	public function sqlContractAddOnDeposit() {
		return ( true == isset( $this->m_fltContractAddOnDeposit ) ) ? ( string ) $this->m_fltContractAddOnDeposit : 'NULL';
	}

	public function setContractRiskPremiumDeposit( $fltContractRiskPremiumDeposit ) {
		$this->set( 'm_fltContractRiskPremiumDeposit', CStrings::strToFloatDef( $fltContractRiskPremiumDeposit, NULL, false, 2 ) );
	}

	public function getContractRiskPremiumDeposit() {
		return $this->m_fltContractRiskPremiumDeposit;
	}

	public function sqlContractRiskPremiumDeposit() {
		return ( true == isset( $this->m_fltContractRiskPremiumDeposit ) ) ? ( string ) $this->m_fltContractRiskPremiumDeposit : 'NULL';
	}

	public function setContractSpecialDeposit( $fltContractSpecialDeposit ) {
		$this->set( 'm_fltContractSpecialDeposit', CStrings::strToFloatDef( $fltContractSpecialDeposit, NULL, false, 2 ) );
	}

	public function getContractSpecialDeposit() {
		return $this->m_fltContractSpecialDeposit;
	}

	public function sqlContractSpecialDeposit() {
		return ( true == isset( $this->m_fltContractSpecialDeposit ) ) ? ( string ) $this->m_fltContractSpecialDeposit : 'NULL';
	}

	public function setContractBaseOther( $fltContractBaseOther ) {
		$this->set( 'm_fltContractBaseOther', CStrings::strToFloatDef( $fltContractBaseOther, NULL, false, 2 ) );
	}

	public function getContractBaseOther() {
		return $this->m_fltContractBaseOther;
	}

	public function sqlContractBaseOther() {
		return ( true == isset( $this->m_fltContractBaseOther ) ) ? ( string ) $this->m_fltContractBaseOther : 'NULL';
	}

	public function setContractAmenityOther( $fltContractAmenityOther ) {
		$this->set( 'm_fltContractAmenityOther', CStrings::strToFloatDef( $fltContractAmenityOther, NULL, false, 2 ) );
	}

	public function getContractAmenityOther() {
		return $this->m_fltContractAmenityOther;
	}

	public function sqlContractAmenityOther() {
		return ( true == isset( $this->m_fltContractAmenityOther ) ) ? ( string ) $this->m_fltContractAmenityOther : 'NULL';
	}

	public function setContractPetOther( $fltContractPetOther ) {
		$this->set( 'm_fltContractPetOther', CStrings::strToFloatDef( $fltContractPetOther, NULL, false, 2 ) );
	}

	public function getContractPetOther() {
		return $this->m_fltContractPetOther;
	}

	public function sqlContractPetOther() {
		return ( true == isset( $this->m_fltContractPetOther ) ) ? ( string ) $this->m_fltContractPetOther : 'NULL';
	}

	public function setContractAddOnOther( $fltContractAddOnOther ) {
		$this->set( 'm_fltContractAddOnOther', CStrings::strToFloatDef( $fltContractAddOnOther, NULL, false, 2 ) );
	}

	public function getContractAddOnOther() {
		return $this->m_fltContractAddOnOther;
	}

	public function sqlContractAddOnOther() {
		return ( true == isset( $this->m_fltContractAddOnOther ) ) ? ( string ) $this->m_fltContractAddOnOther : 'NULL';
	}

	public function setContractRiskPremiumOther( $fltContractRiskPremiumOther ) {
		$this->set( 'm_fltContractRiskPremiumOther', CStrings::strToFloatDef( $fltContractRiskPremiumOther, NULL, false, 2 ) );
	}

	public function getContractRiskPremiumOther() {
		return $this->m_fltContractRiskPremiumOther;
	}

	public function sqlContractRiskPremiumOther() {
		return ( true == isset( $this->m_fltContractRiskPremiumOther ) ) ? ( string ) $this->m_fltContractRiskPremiumOther : 'NULL';
	}

	public function setContractSpecialOther( $fltContractSpecialOther ) {
		$this->set( 'm_fltContractSpecialOther', CStrings::strToFloatDef( $fltContractSpecialOther, NULL, false, 2 ) );
	}

	public function getContractSpecialOther() {
		return $this->m_fltContractSpecialOther;
	}

	public function sqlContractSpecialOther() {
		return ( true == isset( $this->m_fltContractSpecialOther ) ) ? ( string ) $this->m_fltContractSpecialOther : 'NULL';
	}

	public function setContractTotalOther( $fltContractTotalOther ) {
		$this->set( 'm_fltContractTotalOther', CStrings::strToFloatDef( $fltContractTotalOther, NULL, false, 2 ) );
	}

	public function getContractTotalOther() {
		return $this->m_fltContractTotalOther;
	}

	public function sqlContractTotalOther() {
		return ( true == isset( $this->m_fltContractTotalOther ) ) ? ( string ) $this->m_fltContractTotalOther : 'NULL';
	}

	public function setContractTotalDeposit( $fltContractTotalDeposit ) {
		$this->set( 'm_fltContractTotalDeposit', CStrings::strToFloatDef( $fltContractTotalDeposit, NULL, false, 2 ) );
	}

	public function getContractTotalDeposit() {
		return $this->m_fltContractTotalDeposit;
	}

	public function sqlContractTotalDeposit() {
		return ( true == isset( $this->m_fltContractTotalDeposit ) ) ? ( string ) $this->m_fltContractTotalDeposit : 'NULL';
	}

	public function setApplicationPartiallyCompletedOn( $strApplicationPartiallyCompletedOn ) {
		$this->set( 'm_strApplicationPartiallyCompletedOn', CStrings::strTrimDef( $strApplicationPartiallyCompletedOn, -1, NULL, true ) );
	}

	public function getApplicationPartiallyCompletedOn() {
		return $this->m_strApplicationPartiallyCompletedOn;
	}

	public function sqlApplicationPartiallyCompletedOn() {
		return ( true == isset( $this->m_strApplicationPartiallyCompletedOn ) ) ? '\'' . $this->m_strApplicationPartiallyCompletedOn . '\'' : 'NULL';
	}

	public function setLeaseCompletedOnInitial( $strLeaseCompletedOnInitial ) {
		$this->set( 'm_strLeaseCompletedOnInitial', CStrings::strTrimDef( $strLeaseCompletedOnInitial, -1, NULL, true ) );
	}

	public function getLeaseCompletedOnInitial() {
		return $this->m_strLeaseCompletedOnInitial;
	}

	public function sqlLeaseCompletedOnInitial() {
		return ( true == isset( $this->m_strLeaseCompletedOnInitial ) ) ? '\'' . $this->m_strLeaseCompletedOnInitial . '\'' : 'NULL';
	}

	public function setLeasePartiallyCompletedOnInitial( $strLeasePartiallyCompletedOnInitial ) {
		$this->set( 'm_strLeasePartiallyCompletedOnInitial', CStrings::strTrimDef( $strLeasePartiallyCompletedOnInitial, -1, NULL, true ) );
	}

	public function getLeasePartiallyCompletedOnInitial() {
		return $this->m_strLeasePartiallyCompletedOnInitial;
	}

	public function sqlLeasePartiallyCompletedOnInitial() {
		return ( true == isset( $this->m_strLeasePartiallyCompletedOnInitial ) ) ? '\'' . $this->m_strLeasePartiallyCompletedOnInitial . '\'' : 'NULL';
	}

	public function setLeaseApprovedOnInitial( $strLeaseApprovedOnInitial ) {
		$this->set( 'm_strLeaseApprovedOnInitial', CStrings::strTrimDef( $strLeaseApprovedOnInitial, -1, NULL, true ) );
	}

	public function getLeaseApprovedOnInitial() {
		return $this->m_strLeaseApprovedOnInitial;
	}

	public function sqlLeaseApprovedOnInitial() {
		return ( true == isset( $this->m_strLeaseApprovedOnInitial ) ) ? '\'' . $this->m_strLeaseApprovedOnInitial . '\'' : 'NULL';
	}

	public function setUnitSpaceRentedOn( $strUnitSpaceRentedOn ) {
		$this->set( 'm_strUnitSpaceRentedOn', CStrings::strTrimDef( $strUnitSpaceRentedOn, -1, NULL, true ) );
	}

	public function getUnitSpaceRentedOn() {
		return $this->m_strUnitSpaceRentedOn;
	}

	public function sqlUnitSpaceRentedOn() {
		return ( true == isset( $this->m_strUnitSpaceRentedOn ) ) ? '\'' . $this->m_strUnitSpaceRentedOn . '\'' : 'NULL';
	}

	public function setDefaultListItemId( $intDefaultListItemId ) {
		$this->set( 'm_intDefaultListItemId', CStrings::strToIntDef( $intDefaultListItemId, NULL, false ) );
	}

	public function getDefaultListItemId() {
		return $this->m_intDefaultListItemId;
	}

	public function sqlDefaultListItemId() {
		return ( true == isset( $this->m_intDefaultListItemId ) ) ? ( string ) $this->m_intDefaultListItemId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, application_id, prior_cached_application_log_id, lease_id, lease_interval_id, lease_interval_type_id, lease_status_type_id, lease_type_id, lead_conversion_type, old_application_status_type_id, application_stage_id, application_status_id, application_step_id, max_application_stage_id, max_application_status_id, max_application_step_id, cancellation_list_type_id, cancellation_list_item_id, primary_applicant_id, unit_kind_id, unit_type_id, property_floorplan_id, property_building_id, property_floor_id, property_unit_id, unit_space_id, space_configuration_id, desired_space_configuration_id, screening_id, ps_product_id, application_ps_product_id, lease_ps_product_id, leasing_agent_id, internet_listing_service_id, originating_lead_source_id, converting_lead_source_id, company_application_id, lease_term_id, lease_start_window_id, late_fee_formula_id, combined_application_id, quote_id, sem_ad_group_id, sem_keyword_id, sem_source_id, call_id, website_id, craigslist_post_id, cl_ad_id, traffic_cookie_id, first_event_id, first_event_medium_id, first_leasing_center_event_id, response_event_id, last_event_id, lease_signature_type_id, insurance_policy_id, insurance_policy_status_type_id, occupancy_type_id, period_id, reporting_period_id, effective_period_id, original_period_id, wait_list_id, wait_list_application_id, wait_list_point_ids, wait_list_position, wait_list_rejection_count, guest_remote_primary_key, app_remote_primary_key, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, property_name, building_name, unit_number_cache, display_number, unit_square_feet, unit_prior_application_id, name_first, name_middle, name_last, primary_phone_number, email_address, term_month, lease_start_date, lease_end_date, optimal_monthly_rent_base, optimal_monthly_rent_total, advertised_monthly_rent_base, advertised_monthly_rent_amenity, advertised_monthly_rent_add_on, advertised_monthly_rent_special, advertised_monthly_rent_total, advertised_monthly_other_base, advertised_monthly_other_amenity, advertised_monthly_other_add_on, advertised_monthly_other_special, advertised_monthly_other_total, advertised_one_time_deposit_base, advertised_one_time_deposit_amenity, advertised_one_time_deposit_add_on, advertised_one_time_deposit_special, advertised_one_time_deposit_total, advertised_one_time_application_base, advertised_one_time_application_amenity, advertised_one_time_application_add_on, advertised_one_time_application_special, advertised_one_time_application_total, advertised_one_time_other_base, advertised_one_time_other_amenity, advertised_one_time_other_add_on, advertised_one_time_other_special, advertised_one_time_other_total, executed_monthly_rent_base, executed_monthly_rent_amenity, executed_monthly_rent_add_on, executed_monthly_rent_special, executed_monthly_rent_risk_premium, executed_monthly_rent_pet, executed_monthly_rent_total, executed_monthly_other_base, executed_monthly_other_amenity, executed_monthly_other_add_on, executed_monthly_other_special, executed_monthly_other_risk_premium, executed_monthly_other_pet, executed_monthly_other_total, executed_one_time_deposit_base, executed_one_time_deposit_amenity, executed_one_time_deposit_add_on, executed_one_time_deposit_special, executed_one_time_deposit_risk_premium, executed_one_time_deposit_pet, executed_one_time_deposit_total, executed_one_time_application_base, executed_one_time_application_amenity, executed_one_time_application_add_on, executed_one_time_application_special, executed_one_time_application_risk_premium, executed_one_time_application_pet, executed_one_time_application_total, executed_one_time_other_base, executed_one_time_other_amenity, executed_one_time_other_add_on, executed_one_time_other_special, executed_one_time_other_risk_premium, executed_one_time_other_pet, executed_one_time_other_total, executed_optional_monthly_rent_dog, executed_optional_monthly_rent_cat, executed_optional_monthly_rent_other_pet, executed_optional_monthly_rent_garage, executed_optional_monthly_rent_appliances, executed_optional_monthly_rent_furniture, executed_optional_monthly_rent_furnished, executed_optional_monthly_rent_storage, executed_optional_monthly_rent_parking, executed_optional_monthly_rent_meal_plans, executed_optional_monthly_rent_other_add_on, vehicle_count, household_income, desired_rent_min, desired_rent_max, desired_bedrooms, desired_bathrooms, desired_pets, desired_occupants, desired_amenities, total_responsible, total_not_responsible, total_guarantors, total_non_leasing_applicants, total_applicants, guest_card_field_completions, log_datetime, effective_date, application_datetime, url_referer, details, quotes_count, conversion_probability, first_contacted_on, time_to_first_response, site_visited_on, property_toured_on, unit_toured_on, blue_moon_application_sent_on, info_approved_on, pre_qualification_completed_on, pre_qualification_approved_on, application_started_on, application_completed_on, application_approved_on, screening_approved_on, lease_generated_on, lease_completed_on, lease_approved_on, cancelled_on, transferred_in_on, transferred_out_on, mute_followups_until, quoted_rent_locked_until, hold_unit_until, wait_list_start_on, wait_list_end_on, requires_capture, is_pet_policy_conflicted, is_deleted, is_post_month_ignored, is_post_date_ignored, is_opening_log, locked_by, locked_on, updated_by, updated_on, created_by, created_on, wait_list_points, application_completed_on_initial, application_approved_on_initial, application_reopened_on, lease_interval_type, lease_status_type, lease_term, lease_start_window_start_date, lease_start_window_end_date, contract_base_rent, contract_amenity_rent, contract_pet_rent, contract_add_on_rent, contract_risk_premium_rent, contract_special_rent, contract_total_rent, contract_base_deposit, contract_amenity_deposit, contract_pet_deposit, contract_add_on_deposit, contract_risk_premium_deposit, contract_special_deposit, contract_base_other, contract_amenity_other, contract_pet_other, contract_add_on_other, contract_risk_premium_other, contract_special_other, contract_total_other, contract_total_deposit, application_partially_completed_on, lease_completed_on_initial, lease_partially_completed_on_initial, lease_approved_on_initial, unit_space_rented_on, default_list_item_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlPriorCachedApplicationLogId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlLeaseTypeId() . ', ' .
						$this->sqlLeadConversionType() . ', ' .
						$this->sqlOldApplicationStatusTypeId() . ', ' .
						$this->sqlApplicationStageId() . ', ' .
						$this->sqlApplicationStatusId() . ', ' .
						$this->sqlApplicationStepId() . ', ' .
						$this->sqlMaxApplicationStageId() . ', ' .
						$this->sqlMaxApplicationStatusId() . ', ' .
						$this->sqlMaxApplicationStepId() . ', ' .
						$this->sqlCancellationListTypeId() . ', ' .
						$this->sqlCancellationListItemId() . ', ' .
						$this->sqlPrimaryApplicantId() . ', ' .
						$this->sqlUnitKindId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPropertyFloorId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlSpaceConfigurationId() . ', ' .
						$this->sqlDesiredSpaceConfigurationId() . ', ' .
						$this->sqlScreeningId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlApplicationPsProductId() . ', ' .
						$this->sqlLeasePsProductId() . ', ' .
						$this->sqlLeasingAgentId() . ', ' .
						$this->sqlInternetListingServiceId() . ', ' .
						$this->sqlOriginatingLeadSourceId() . ', ' .
						$this->sqlConvertingLeadSourceId() . ', ' .
						$this->sqlCompanyApplicationId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlLateFeeFormulaId() . ', ' .
						$this->sqlCombinedApplicationId() . ', ' .
						$this->sqlQuoteId() . ', ' .
						$this->sqlSemAdGroupId() . ', ' .
						$this->sqlSemKeywordId() . ', ' .
						$this->sqlSemSourceId() . ', ' .
						$this->sqlCallId() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlCraigslistPostId() . ', ' .
						$this->sqlClAdId() . ', ' .
						$this->sqlTrafficCookieId() . ', ' .
						$this->sqlFirstEventId() . ', ' .
						$this->sqlFirstEventMediumId() . ', ' .
						$this->sqlFirstLeasingCenterEventId() . ', ' .
						$this->sqlResponseEventId() . ', ' .
						$this->sqlLastEventId() . ', ' .
						$this->sqlLeaseSignatureTypeId() . ', ' .
						$this->sqlInsurancePolicyId() . ', ' .
						$this->sqlInsurancePolicyStatusTypeId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlWaitListId() . ', ' .
						$this->sqlWaitListApplicationId() . ', ' .
						$this->sqlWaitListPointIds() . ', ' .
						$this->sqlWaitListPosition() . ', ' .
						$this->sqlWaitListRejectionCount() . ', ' .
						$this->sqlGuestRemotePrimaryKey() . ', ' .
						$this->sqlAppRemotePrimaryKey() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlUnitSquareFeet() . ', ' .
						$this->sqlUnitPriorApplicationId() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlPrimaryPhoneNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTermMonth() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlOptimalMonthlyRentBase() . ', ' .
						$this->sqlOptimalMonthlyRentTotal() . ', ' .
						$this->sqlAdvertisedMonthlyRentBase() . ', ' .
						$this->sqlAdvertisedMonthlyRentAmenity() . ', ' .
						$this->sqlAdvertisedMonthlyRentAddOn() . ', ' .
						$this->sqlAdvertisedMonthlyRentSpecial() . ', ' .
						$this->sqlAdvertisedMonthlyRentTotal() . ', ' .
						$this->sqlAdvertisedMonthlyOtherBase() . ', ' .
						$this->sqlAdvertisedMonthlyOtherAmenity() . ', ' .
						$this->sqlAdvertisedMonthlyOtherAddOn() . ', ' .
						$this->sqlAdvertisedMonthlyOtherSpecial() . ', ' .
						$this->sqlAdvertisedMonthlyOtherTotal() . ', ' .
						$this->sqlAdvertisedOneTimeDepositBase() . ', ' .
						$this->sqlAdvertisedOneTimeDepositAmenity() . ', ' .
						$this->sqlAdvertisedOneTimeDepositAddOn() . ', ' .
						$this->sqlAdvertisedOneTimeDepositSpecial() . ', ' .
						$this->sqlAdvertisedOneTimeDepositTotal() . ', ' .
						$this->sqlAdvertisedOneTimeApplicationBase() . ', ' .
						$this->sqlAdvertisedOneTimeApplicationAmenity() . ', ' .
						$this->sqlAdvertisedOneTimeApplicationAddOn() . ', ' .
						$this->sqlAdvertisedOneTimeApplicationSpecial() . ', ' .
						$this->sqlAdvertisedOneTimeApplicationTotal() . ', ' .
						$this->sqlAdvertisedOneTimeOtherBase() . ', ' .
						$this->sqlAdvertisedOneTimeOtherAmenity() . ', ' .
						$this->sqlAdvertisedOneTimeOtherAddOn() . ', ' .
						$this->sqlAdvertisedOneTimeOtherSpecial() . ', ' .
						$this->sqlAdvertisedOneTimeOtherTotal() . ', ' .
						$this->sqlExecutedMonthlyRentBase() . ', ' .
						$this->sqlExecutedMonthlyRentAmenity() . ', ' .
						$this->sqlExecutedMonthlyRentAddOn() . ', ' .
						$this->sqlExecutedMonthlyRentSpecial() . ', ' .
						$this->sqlExecutedMonthlyRentRiskPremium() . ', ' .
						$this->sqlExecutedMonthlyRentPet() . ', ' .
						$this->sqlExecutedMonthlyRentTotal() . ', ' .
						$this->sqlExecutedMonthlyOtherBase() . ', ' .
						$this->sqlExecutedMonthlyOtherAmenity() . ', ' .
						$this->sqlExecutedMonthlyOtherAddOn() . ', ' .
						$this->sqlExecutedMonthlyOtherSpecial() . ', ' .
						$this->sqlExecutedMonthlyOtherRiskPremium() . ', ' .
						$this->sqlExecutedMonthlyOtherPet() . ', ' .
						$this->sqlExecutedMonthlyOtherTotal() . ', ' .
						$this->sqlExecutedOneTimeDepositBase() . ', ' .
						$this->sqlExecutedOneTimeDepositAmenity() . ', ' .
						$this->sqlExecutedOneTimeDepositAddOn() . ', ' .
						$this->sqlExecutedOneTimeDepositSpecial() . ', ' .
						$this->sqlExecutedOneTimeDepositRiskPremium() . ', ' .
						$this->sqlExecutedOneTimeDepositPet() . ', ' .
						$this->sqlExecutedOneTimeDepositTotal() . ', ' .
						$this->sqlExecutedOneTimeApplicationBase() . ', ' .
						$this->sqlExecutedOneTimeApplicationAmenity() . ', ' .
						$this->sqlExecutedOneTimeApplicationAddOn() . ', ' .
						$this->sqlExecutedOneTimeApplicationSpecial() . ', ' .
						$this->sqlExecutedOneTimeApplicationRiskPremium() . ', ' .
						$this->sqlExecutedOneTimeApplicationPet() . ', ' .
						$this->sqlExecutedOneTimeApplicationTotal() . ', ' .
						$this->sqlExecutedOneTimeOtherBase() . ', ' .
						$this->sqlExecutedOneTimeOtherAmenity() . ', ' .
						$this->sqlExecutedOneTimeOtherAddOn() . ', ' .
						$this->sqlExecutedOneTimeOtherSpecial() . ', ' .
						$this->sqlExecutedOneTimeOtherRiskPremium() . ', ' .
						$this->sqlExecutedOneTimeOtherPet() . ', ' .
						$this->sqlExecutedOneTimeOtherTotal() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentDog() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentCat() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentOtherPet() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentGarage() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentAppliances() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentFurniture() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentFurnished() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentStorage() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentParking() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentMealPlans() . ', ' .
						$this->sqlExecutedOptionalMonthlyRentOtherAddOn() . ', ' .
						$this->sqlVehicleCount() . ', ' .
						$this->sqlHouseholdIncome() . ', ' .
						$this->sqlDesiredRentMin() . ', ' .
						$this->sqlDesiredRentMax() . ', ' .
						$this->sqlDesiredBedrooms() . ', ' .
						$this->sqlDesiredBathrooms() . ', ' .
						$this->sqlDesiredPets() . ', ' .
						$this->sqlDesiredOccupants() . ', ' .
						$this->sqlDesiredAmenities() . ', ' .
						$this->sqlTotalResponsible() . ', ' .
						$this->sqlTotalNotResponsible() . ', ' .
						$this->sqlTotalGuarantors() . ', ' .
						$this->sqlTotalNonLeasingApplicants() . ', ' .
						$this->sqlTotalApplicants() . ', ' .
						$this->sqlGuestCardFieldCompletions() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlApplicationDatetime() . ', ' .
						$this->sqlUrlReferer() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlQuotesCount() . ', ' .
						$this->sqlConversionProbability() . ', ' .
						$this->sqlFirstContactedOn() . ', ' .
						$this->sqlTimeToFirstResponse() . ', ' .
						$this->sqlSiteVisitedOn() . ', ' .
						$this->sqlPropertyTouredOn() . ', ' .
						$this->sqlUnitTouredOn() . ', ' .
						$this->sqlBlueMoonApplicationSentOn() . ', ' .
						$this->sqlInfoApprovedOn() . ', ' .
						$this->sqlPreQualificationCompletedOn() . ', ' .
						$this->sqlPreQualificationApprovedOn() . ', ' .
						$this->sqlApplicationStartedOn() . ', ' .
						$this->sqlApplicationCompletedOn() . ', ' .
						$this->sqlApplicationApprovedOn() . ', ' .
						$this->sqlScreeningApprovedOn() . ', ' .
						$this->sqlLeaseGeneratedOn() . ', ' .
						$this->sqlLeaseCompletedOn() . ', ' .
						$this->sqlLeaseApprovedOn() . ', ' .
						$this->sqlCancelledOn() . ', ' .
						$this->sqlTransferredInOn() . ', ' .
						$this->sqlTransferredOutOn() . ', ' .
						$this->sqlMuteFollowupsUntil() . ', ' .
						$this->sqlQuotedRentLockedUntil() . ', ' .
						$this->sqlHoldUnitUntil() . ', ' .
						$this->sqlWaitListStartOn() . ', ' .
						$this->sqlWaitListEndOn() . ', ' .
						$this->sqlRequiresCapture() . ', ' .
						$this->sqlIsPetPolicyConflicted() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						$this->sqlLockedBy() . ', ' .
						$this->sqlLockedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlWaitListPoints() . ', ' .
						$this->sqlApplicationCompletedOnInitial() . ', ' .
						$this->sqlApplicationApprovedOnInitial() . ', ' .
						$this->sqlApplicationReopenedOn() . ', ' .
						$this->sqlLeaseIntervalType() . ', ' .
						$this->sqlLeaseStatusType() . ', ' .
						$this->sqlLeaseTerm() . ', ' .
						$this->sqlLeaseStartWindowStartDate() . ', ' .
						$this->sqlLeaseStartWindowEndDate() . ', ' .
						$this->sqlContractBaseRent() . ', ' .
						$this->sqlContractAmenityRent() . ', ' .
						$this->sqlContractPetRent() . ', ' .
						$this->sqlContractAddOnRent() . ', ' .
						$this->sqlContractRiskPremiumRent() . ', ' .
						$this->sqlContractSpecialRent() . ', ' .
						$this->sqlContractTotalRent() . ', ' .
						$this->sqlContractBaseDeposit() . ', ' .
						$this->sqlContractAmenityDeposit() . ', ' .
						$this->sqlContractPetDeposit() . ', ' .
						$this->sqlContractAddOnDeposit() . ', ' .
						$this->sqlContractRiskPremiumDeposit() . ', ' .
						$this->sqlContractSpecialDeposit() . ', ' .
						$this->sqlContractBaseOther() . ', ' .
						$this->sqlContractAmenityOther() . ', ' .
						$this->sqlContractPetOther() . ', ' .
						$this->sqlContractAddOnOther() . ', ' .
						$this->sqlContractRiskPremiumOther() . ', ' .
						$this->sqlContractSpecialOther() . ', ' .
						$this->sqlContractTotalOther() . ', ' .
						$this->sqlContractTotalDeposit() . ', ' .
						$this->sqlApplicationPartiallyCompletedOn() . ', ' .
						$this->sqlLeaseCompletedOnInitial() . ', ' .
						$this->sqlLeasePartiallyCompletedOnInitial() . ', ' .
						$this->sqlLeaseApprovedOnInitial() . ', ' .
						$this->sqlUnitSpaceRentedOn() . ', ' .
						$this->sqlDefaultListItemId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_cached_application_log_id = ' . $this->sqlPriorCachedApplicationLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCachedApplicationLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_cached_application_log_id = ' . $this->sqlPriorCachedApplicationLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_type_id = ' . $this->sqlLeaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_conversion_type = ' . $this->sqlLeadConversionType(). ',' ; } elseif( true == array_key_exists( 'LeadConversionType', $this->getChangedColumns() ) ) { $strSql .= ' lead_conversion_type = ' . $this->sqlLeadConversionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_application_status_type_id = ' . $this->sqlOldApplicationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OldApplicationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_application_status_type_id = ' . $this->sqlOldApplicationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_id = ' . $this->sqlApplicationStageId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStageId', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_id = ' . $this->sqlApplicationStageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_status_id = ' . $this->sqlApplicationStatusId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' application_status_id = ' . $this->sqlApplicationStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId(). ',' ; } elseif( true == array_key_exists( 'ApplicationStepId', $this->getChangedColumns() ) ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_stage_id = ' . $this->sqlMaxApplicationStageId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStageId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_stage_id = ' . $this->sqlMaxApplicationStageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_status_id = ' . $this->sqlMaxApplicationStatusId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_status_id = ' . $this->sqlMaxApplicationStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_application_step_id = ' . $this->sqlMaxApplicationStepId(). ',' ; } elseif( true == array_key_exists( 'MaxApplicationStepId', $this->getChangedColumns() ) ) { $strSql .= ' max_application_step_id = ' . $this->sqlMaxApplicationStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancellation_list_type_id = ' . $this->sqlCancellationListTypeId(). ',' ; } elseif( true == array_key_exists( 'CancellationListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cancellation_list_type_id = ' . $this->sqlCancellationListTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancellation_list_item_id = ' . $this->sqlCancellationListItemId(). ',' ; } elseif( true == array_key_exists( 'CancellationListItemId', $this->getChangedColumns() ) ) { $strSql .= ' cancellation_list_item_id = ' . $this->sqlCancellationListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId(). ',' ; } elseif( true == array_key_exists( 'PrimaryApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' primary_applicant_id = ' . $this->sqlPrimaryApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_space_configuration_id = ' . $this->sqlDesiredSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'DesiredSpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' desired_space_configuration_id = ' . $this->sqlDesiredSpaceConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId(). ',' ; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_ps_product_id = ' . $this->sqlApplicationPsProductId(). ',' ; } elseif( true == array_key_exists( 'ApplicationPsProductId', $this->getChangedColumns() ) ) { $strSql .= ' application_ps_product_id = ' . $this->sqlApplicationPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_ps_product_id = ' . $this->sqlLeasePsProductId(). ',' ; } elseif( true == array_key_exists( 'LeasePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' lease_ps_product_id = ' . $this->sqlLeasePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId(). ',' ; } elseif( true == array_key_exists( 'LeasingAgentId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_agent_id = ' . $this->sqlLeasingAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_lead_source_id = ' . $this->sqlOriginatingLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'OriginatingLeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' originating_lead_source_id = ' . $this->sqlOriginatingLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' converting_lead_source_id = ' . $this->sqlConvertingLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'ConvertingLeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' converting_lead_source_id = ' . $this->sqlConvertingLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId(). ',' ; } elseif( true == array_key_exists( 'CompanyApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' company_application_id = ' . $this->sqlCompanyApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId(). ',' ; } elseif( true == array_key_exists( 'LateFeeFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' combined_application_id = ' . $this->sqlCombinedApplicationId(). ',' ; } elseif( true == array_key_exists( 'CombinedApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' combined_application_id = ' . $this->sqlCombinedApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId(). ',' ; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId(). ',' ; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId(). ',' ; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId(). ',' ; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId(). ',' ; } elseif( true == array_key_exists( 'CraigslistPostId', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId(). ',' ; } elseif( true == array_key_exists( 'ClAdId', $this->getChangedColumns() ) ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId(). ',' ; } elseif( true == array_key_exists( 'TrafficCookieId', $this->getChangedColumns() ) ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_event_id = ' . $this->sqlFirstEventId(). ',' ; } elseif( true == array_key_exists( 'FirstEventId', $this->getChangedColumns() ) ) { $strSql .= ' first_event_id = ' . $this->sqlFirstEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_event_medium_id = ' . $this->sqlFirstEventMediumId(). ',' ; } elseif( true == array_key_exists( 'FirstEventMediumId', $this->getChangedColumns() ) ) { $strSql .= ' first_event_medium_id = ' . $this->sqlFirstEventMediumId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_leasing_center_event_id = ' . $this->sqlFirstLeasingCenterEventId(). ',' ; } elseif( true == array_key_exists( 'FirstLeasingCenterEventId', $this->getChangedColumns() ) ) { $strSql .= ' first_leasing_center_event_id = ' . $this->sqlFirstLeasingCenterEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_event_id = ' . $this->sqlResponseEventId(). ',' ; } elseif( true == array_key_exists( 'ResponseEventId', $this->getChangedColumns() ) ) { $strSql .= ' response_event_id = ' . $this->sqlResponseEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_event_id = ' . $this->sqlLastEventId(). ',' ; } elseif( true == array_key_exists( 'LastEventId', $this->getChangedColumns() ) ) { $strSql .= ' last_event_id = ' . $this->sqlLastEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_signature_type_id = ' . $this->sqlLeaseSignatureTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseSignatureTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_signature_type_id = ' . $this->sqlLeaseSignatureTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_id = ' . $this->sqlWaitListId(). ',' ; } elseif( true == array_key_exists( 'WaitListId', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_id = ' . $this->sqlWaitListId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_application_id = ' . $this->sqlWaitListApplicationId(). ',' ; } elseif( true == array_key_exists( 'WaitListApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_application_id = ' . $this->sqlWaitListApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_point_ids = ' . $this->sqlWaitListPointIds(). ',' ; } elseif( true == array_key_exists( 'WaitListPointIds', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_point_ids = ' . $this->sqlWaitListPointIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_position = ' . $this->sqlWaitListPosition(). ',' ; } elseif( true == array_key_exists( 'WaitListPosition', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_position = ' . $this->sqlWaitListPosition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_rejection_count = ' . $this->sqlWaitListRejectionCount(). ',' ; } elseif( true == array_key_exists( 'WaitListRejectionCount', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_rejection_count = ' . $this->sqlWaitListRejectionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'GuestRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'AppRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache(). ',' ; } elseif( true == array_key_exists( 'UnitNumberCache', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber(). ',' ; } elseif( true == array_key_exists( 'DisplayNumber', $this->getChangedColumns() ) ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_square_feet = ' . $this->sqlUnitSquareFeet(). ',' ; } elseif( true == array_key_exists( 'UnitSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' unit_square_feet = ' . $this->sqlUnitSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_prior_application_id = ' . $this->sqlUnitPriorApplicationId(). ',' ; } elseif( true == array_key_exists( 'UnitPriorApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' unit_prior_application_id = ' . $this->sqlUnitPriorApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number = ' . $this->sqlPrimaryPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PrimaryPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number = ' . $this->sqlPrimaryPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' term_month = ' . $this->sqlTermMonth(). ',' ; } elseif( true == array_key_exists( 'TermMonth', $this->getChangedColumns() ) ) { $strSql .= ' term_month = ' . $this->sqlTermMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_monthly_rent_base = ' . $this->sqlOptimalMonthlyRentBase(). ',' ; } elseif( true == array_key_exists( 'OptimalMonthlyRentBase', $this->getChangedColumns() ) ) { $strSql .= ' optimal_monthly_rent_base = ' . $this->sqlOptimalMonthlyRentBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_monthly_rent_total = ' . $this->sqlOptimalMonthlyRentTotal(). ',' ; } elseif( true == array_key_exists( 'OptimalMonthlyRentTotal', $this->getChangedColumns() ) ) { $strSql .= ' optimal_monthly_rent_total = ' . $this->sqlOptimalMonthlyRentTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_rent_base = ' . $this->sqlAdvertisedMonthlyRentBase(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyRentBase', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_rent_base = ' . $this->sqlAdvertisedMonthlyRentBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_rent_amenity = ' . $this->sqlAdvertisedMonthlyRentAmenity(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyRentAmenity', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_rent_amenity = ' . $this->sqlAdvertisedMonthlyRentAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_rent_add_on = ' . $this->sqlAdvertisedMonthlyRentAddOn(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyRentAddOn', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_rent_add_on = ' . $this->sqlAdvertisedMonthlyRentAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_rent_special = ' . $this->sqlAdvertisedMonthlyRentSpecial(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyRentSpecial', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_rent_special = ' . $this->sqlAdvertisedMonthlyRentSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_rent_total = ' . $this->sqlAdvertisedMonthlyRentTotal(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyRentTotal', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_rent_total = ' . $this->sqlAdvertisedMonthlyRentTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_other_base = ' . $this->sqlAdvertisedMonthlyOtherBase(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyOtherBase', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_other_base = ' . $this->sqlAdvertisedMonthlyOtherBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_other_amenity = ' . $this->sqlAdvertisedMonthlyOtherAmenity(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyOtherAmenity', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_other_amenity = ' . $this->sqlAdvertisedMonthlyOtherAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_other_add_on = ' . $this->sqlAdvertisedMonthlyOtherAddOn(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyOtherAddOn', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_other_add_on = ' . $this->sqlAdvertisedMonthlyOtherAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_other_special = ' . $this->sqlAdvertisedMonthlyOtherSpecial(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyOtherSpecial', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_other_special = ' . $this->sqlAdvertisedMonthlyOtherSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_monthly_other_total = ' . $this->sqlAdvertisedMonthlyOtherTotal(). ',' ; } elseif( true == array_key_exists( 'AdvertisedMonthlyOtherTotal', $this->getChangedColumns() ) ) { $strSql .= ' advertised_monthly_other_total = ' . $this->sqlAdvertisedMonthlyOtherTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_deposit_base = ' . $this->sqlAdvertisedOneTimeDepositBase(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeDepositBase', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_deposit_base = ' . $this->sqlAdvertisedOneTimeDepositBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_deposit_amenity = ' . $this->sqlAdvertisedOneTimeDepositAmenity(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeDepositAmenity', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_deposit_amenity = ' . $this->sqlAdvertisedOneTimeDepositAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_deposit_add_on = ' . $this->sqlAdvertisedOneTimeDepositAddOn(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeDepositAddOn', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_deposit_add_on = ' . $this->sqlAdvertisedOneTimeDepositAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_deposit_special = ' . $this->sqlAdvertisedOneTimeDepositSpecial(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeDepositSpecial', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_deposit_special = ' . $this->sqlAdvertisedOneTimeDepositSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_deposit_total = ' . $this->sqlAdvertisedOneTimeDepositTotal(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeDepositTotal', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_deposit_total = ' . $this->sqlAdvertisedOneTimeDepositTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_application_base = ' . $this->sqlAdvertisedOneTimeApplicationBase(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeApplicationBase', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_application_base = ' . $this->sqlAdvertisedOneTimeApplicationBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_application_amenity = ' . $this->sqlAdvertisedOneTimeApplicationAmenity(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeApplicationAmenity', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_application_amenity = ' . $this->sqlAdvertisedOneTimeApplicationAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_application_add_on = ' . $this->sqlAdvertisedOneTimeApplicationAddOn(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeApplicationAddOn', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_application_add_on = ' . $this->sqlAdvertisedOneTimeApplicationAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_application_special = ' . $this->sqlAdvertisedOneTimeApplicationSpecial(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeApplicationSpecial', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_application_special = ' . $this->sqlAdvertisedOneTimeApplicationSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_application_total = ' . $this->sqlAdvertisedOneTimeApplicationTotal(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeApplicationTotal', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_application_total = ' . $this->sqlAdvertisedOneTimeApplicationTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_other_base = ' . $this->sqlAdvertisedOneTimeOtherBase(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeOtherBase', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_other_base = ' . $this->sqlAdvertisedOneTimeOtherBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_other_amenity = ' . $this->sqlAdvertisedOneTimeOtherAmenity(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeOtherAmenity', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_other_amenity = ' . $this->sqlAdvertisedOneTimeOtherAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_other_add_on = ' . $this->sqlAdvertisedOneTimeOtherAddOn(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeOtherAddOn', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_other_add_on = ' . $this->sqlAdvertisedOneTimeOtherAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_other_special = ' . $this->sqlAdvertisedOneTimeOtherSpecial(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeOtherSpecial', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_other_special = ' . $this->sqlAdvertisedOneTimeOtherSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_one_time_other_total = ' . $this->sqlAdvertisedOneTimeOtherTotal(). ',' ; } elseif( true == array_key_exists( 'AdvertisedOneTimeOtherTotal', $this->getChangedColumns() ) ) { $strSql .= ' advertised_one_time_other_total = ' . $this->sqlAdvertisedOneTimeOtherTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_base = ' . $this->sqlExecutedMonthlyRentBase(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentBase', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_base = ' . $this->sqlExecutedMonthlyRentBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_amenity = ' . $this->sqlExecutedMonthlyRentAmenity(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentAmenity', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_amenity = ' . $this->sqlExecutedMonthlyRentAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_add_on = ' . $this->sqlExecutedMonthlyRentAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_add_on = ' . $this->sqlExecutedMonthlyRentAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_special = ' . $this->sqlExecutedMonthlyRentSpecial(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentSpecial', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_special = ' . $this->sqlExecutedMonthlyRentSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_risk_premium = ' . $this->sqlExecutedMonthlyRentRiskPremium(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentRiskPremium', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_risk_premium = ' . $this->sqlExecutedMonthlyRentRiskPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_pet = ' . $this->sqlExecutedMonthlyRentPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_pet = ' . $this->sqlExecutedMonthlyRentPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_rent_total = ' . $this->sqlExecutedMonthlyRentTotal(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyRentTotal', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_rent_total = ' . $this->sqlExecutedMonthlyRentTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_base = ' . $this->sqlExecutedMonthlyOtherBase(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherBase', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_base = ' . $this->sqlExecutedMonthlyOtherBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_amenity = ' . $this->sqlExecutedMonthlyOtherAmenity(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherAmenity', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_amenity = ' . $this->sqlExecutedMonthlyOtherAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_add_on = ' . $this->sqlExecutedMonthlyOtherAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_add_on = ' . $this->sqlExecutedMonthlyOtherAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_special = ' . $this->sqlExecutedMonthlyOtherSpecial(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherSpecial', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_special = ' . $this->sqlExecutedMonthlyOtherSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_risk_premium = ' . $this->sqlExecutedMonthlyOtherRiskPremium(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherRiskPremium', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_risk_premium = ' . $this->sqlExecutedMonthlyOtherRiskPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_pet = ' . $this->sqlExecutedMonthlyOtherPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_pet = ' . $this->sqlExecutedMonthlyOtherPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_monthly_other_total = ' . $this->sqlExecutedMonthlyOtherTotal(). ',' ; } elseif( true == array_key_exists( 'ExecutedMonthlyOtherTotal', $this->getChangedColumns() ) ) { $strSql .= ' executed_monthly_other_total = ' . $this->sqlExecutedMonthlyOtherTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_base = ' . $this->sqlExecutedOneTimeDepositBase(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositBase', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_base = ' . $this->sqlExecutedOneTimeDepositBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_amenity = ' . $this->sqlExecutedOneTimeDepositAmenity(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositAmenity', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_amenity = ' . $this->sqlExecutedOneTimeDepositAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_add_on = ' . $this->sqlExecutedOneTimeDepositAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_add_on = ' . $this->sqlExecutedOneTimeDepositAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_special = ' . $this->sqlExecutedOneTimeDepositSpecial(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositSpecial', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_special = ' . $this->sqlExecutedOneTimeDepositSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_risk_premium = ' . $this->sqlExecutedOneTimeDepositRiskPremium(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositRiskPremium', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_risk_premium = ' . $this->sqlExecutedOneTimeDepositRiskPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_pet = ' . $this->sqlExecutedOneTimeDepositPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_pet = ' . $this->sqlExecutedOneTimeDepositPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_deposit_total = ' . $this->sqlExecutedOneTimeDepositTotal(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeDepositTotal', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_deposit_total = ' . $this->sqlExecutedOneTimeDepositTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_base = ' . $this->sqlExecutedOneTimeApplicationBase(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationBase', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_base = ' . $this->sqlExecutedOneTimeApplicationBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_amenity = ' . $this->sqlExecutedOneTimeApplicationAmenity(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationAmenity', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_amenity = ' . $this->sqlExecutedOneTimeApplicationAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_add_on = ' . $this->sqlExecutedOneTimeApplicationAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_add_on = ' . $this->sqlExecutedOneTimeApplicationAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_special = ' . $this->sqlExecutedOneTimeApplicationSpecial(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationSpecial', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_special = ' . $this->sqlExecutedOneTimeApplicationSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_risk_premium = ' . $this->sqlExecutedOneTimeApplicationRiskPremium(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationRiskPremium', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_risk_premium = ' . $this->sqlExecutedOneTimeApplicationRiskPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_pet = ' . $this->sqlExecutedOneTimeApplicationPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_pet = ' . $this->sqlExecutedOneTimeApplicationPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_application_total = ' . $this->sqlExecutedOneTimeApplicationTotal(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeApplicationTotal', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_application_total = ' . $this->sqlExecutedOneTimeApplicationTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_base = ' . $this->sqlExecutedOneTimeOtherBase(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherBase', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_base = ' . $this->sqlExecutedOneTimeOtherBase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_amenity = ' . $this->sqlExecutedOneTimeOtherAmenity(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherAmenity', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_amenity = ' . $this->sqlExecutedOneTimeOtherAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_add_on = ' . $this->sqlExecutedOneTimeOtherAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_add_on = ' . $this->sqlExecutedOneTimeOtherAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_special = ' . $this->sqlExecutedOneTimeOtherSpecial(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherSpecial', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_special = ' . $this->sqlExecutedOneTimeOtherSpecial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_risk_premium = ' . $this->sqlExecutedOneTimeOtherRiskPremium(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherRiskPremium', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_risk_premium = ' . $this->sqlExecutedOneTimeOtherRiskPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_pet = ' . $this->sqlExecutedOneTimeOtherPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_pet = ' . $this->sqlExecutedOneTimeOtherPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_one_time_other_total = ' . $this->sqlExecutedOneTimeOtherTotal(). ',' ; } elseif( true == array_key_exists( 'ExecutedOneTimeOtherTotal', $this->getChangedColumns() ) ) { $strSql .= ' executed_one_time_other_total = ' . $this->sqlExecutedOneTimeOtherTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_dog = ' . $this->sqlExecutedOptionalMonthlyRentDog(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentDog', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_dog = ' . $this->sqlExecutedOptionalMonthlyRentDog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_cat = ' . $this->sqlExecutedOptionalMonthlyRentCat(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentCat', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_cat = ' . $this->sqlExecutedOptionalMonthlyRentCat() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_other_pet = ' . $this->sqlExecutedOptionalMonthlyRentOtherPet(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentOtherPet', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_other_pet = ' . $this->sqlExecutedOptionalMonthlyRentOtherPet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_garage = ' . $this->sqlExecutedOptionalMonthlyRentGarage(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentGarage', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_garage = ' . $this->sqlExecutedOptionalMonthlyRentGarage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_appliances = ' . $this->sqlExecutedOptionalMonthlyRentAppliances(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentAppliances', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_appliances = ' . $this->sqlExecutedOptionalMonthlyRentAppliances() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_furniture = ' . $this->sqlExecutedOptionalMonthlyRentFurniture(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentFurniture', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_furniture = ' . $this->sqlExecutedOptionalMonthlyRentFurniture() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_furnished = ' . $this->sqlExecutedOptionalMonthlyRentFurnished(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentFurnished', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_furnished = ' . $this->sqlExecutedOptionalMonthlyRentFurnished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_storage = ' . $this->sqlExecutedOptionalMonthlyRentStorage(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentStorage', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_storage = ' . $this->sqlExecutedOptionalMonthlyRentStorage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_parking = ' . $this->sqlExecutedOptionalMonthlyRentParking(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentParking', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_parking = ' . $this->sqlExecutedOptionalMonthlyRentParking() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_meal_plans = ' . $this->sqlExecutedOptionalMonthlyRentMealPlans(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentMealPlans', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_meal_plans = ' . $this->sqlExecutedOptionalMonthlyRentMealPlans() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_optional_monthly_rent_other_add_on = ' . $this->sqlExecutedOptionalMonthlyRentOtherAddOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedOptionalMonthlyRentOtherAddOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_optional_monthly_rent_other_add_on = ' . $this->sqlExecutedOptionalMonthlyRentOtherAddOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vehicle_count = ' . $this->sqlVehicleCount(). ',' ; } elseif( true == array_key_exists( 'VehicleCount', $this->getChangedColumns() ) ) { $strSql .= ' vehicle_count = ' . $this->sqlVehicleCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' household_income = ' . $this->sqlHouseholdIncome(). ',' ; } elseif( true == array_key_exists( 'HouseholdIncome', $this->getChangedColumns() ) ) { $strSql .= ' household_income = ' . $this->sqlHouseholdIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMin', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax(). ',' ; } elseif( true == array_key_exists( 'DesiredRentMax', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms(). ',' ; } elseif( true == array_key_exists( 'DesiredBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets(). ',' ; } elseif( true == array_key_exists( 'DesiredPets', $this->getChangedColumns() ) ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants(). ',' ; } elseif( true == array_key_exists( 'DesiredOccupants', $this->getChangedColumns() ) ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_amenities = ' . $this->sqlDesiredAmenities(). ',' ; } elseif( true == array_key_exists( 'DesiredAmenities', $this->getChangedColumns() ) ) { $strSql .= ' desired_amenities = ' . $this->sqlDesiredAmenities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_responsible = ' . $this->sqlTotalResponsible(). ',' ; } elseif( true == array_key_exists( 'TotalResponsible', $this->getChangedColumns() ) ) { $strSql .= ' total_responsible = ' . $this->sqlTotalResponsible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_not_responsible = ' . $this->sqlTotalNotResponsible(). ',' ; } elseif( true == array_key_exists( 'TotalNotResponsible', $this->getChangedColumns() ) ) { $strSql .= ' total_not_responsible = ' . $this->sqlTotalNotResponsible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_guarantors = ' . $this->sqlTotalGuarantors(). ',' ; } elseif( true == array_key_exists( 'TotalGuarantors', $this->getChangedColumns() ) ) { $strSql .= ' total_guarantors = ' . $this->sqlTotalGuarantors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_non_leasing_applicants = ' . $this->sqlTotalNonLeasingApplicants(). ',' ; } elseif( true == array_key_exists( 'TotalNonLeasingApplicants', $this->getChangedColumns() ) ) { $strSql .= ' total_non_leasing_applicants = ' . $this->sqlTotalNonLeasingApplicants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_applicants = ' . $this->sqlTotalApplicants(). ',' ; } elseif( true == array_key_exists( 'TotalApplicants', $this->getChangedColumns() ) ) { $strSql .= ' total_applicants = ' . $this->sqlTotalApplicants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_card_field_completions = ' . $this->sqlGuestCardFieldCompletions(). ',' ; } elseif( true == array_key_exists( 'GuestCardFieldCompletions', $this->getChangedColumns() ) ) { $strSql .= ' guest_card_field_completions = ' . $this->sqlGuestCardFieldCompletions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_datetime = ' . $this->sqlApplicationDatetime(). ',' ; } elseif( true == array_key_exists( 'ApplicationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' application_datetime = ' . $this->sqlApplicationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url_referer = ' . $this->sqlUrlReferer(). ',' ; } elseif( true == array_key_exists( 'UrlReferer', $this->getChangedColumns() ) ) { $strSql .= ' url_referer = ' . $this->sqlUrlReferer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quotes_count = ' . $this->sqlQuotesCount(). ',' ; } elseif( true == array_key_exists( 'QuotesCount', $this->getChangedColumns() ) ) { $strSql .= ' quotes_count = ' . $this->sqlQuotesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conversion_probability = ' . $this->sqlConversionProbability(). ',' ; } elseif( true == array_key_exists( 'ConversionProbability', $this->getChangedColumns() ) ) { $strSql .= ' conversion_probability = ' . $this->sqlConversionProbability() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_contacted_on = ' . $this->sqlFirstContactedOn(). ',' ; } elseif( true == array_key_exists( 'FirstContactedOn', $this->getChangedColumns() ) ) { $strSql .= ' first_contacted_on = ' . $this->sqlFirstContactedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_to_first_response = ' . $this->sqlTimeToFirstResponse(). ',' ; } elseif( true == array_key_exists( 'TimeToFirstResponse', $this->getChangedColumns() ) ) { $strSql .= ' time_to_first_response = ' . $this->sqlTimeToFirstResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_visited_on = ' . $this->sqlSiteVisitedOn(). ',' ; } elseif( true == array_key_exists( 'SiteVisitedOn', $this->getChangedColumns() ) ) { $strSql .= ' site_visited_on = ' . $this->sqlSiteVisitedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_toured_on = ' . $this->sqlPropertyTouredOn(). ',' ; } elseif( true == array_key_exists( 'PropertyTouredOn', $this->getChangedColumns() ) ) { $strSql .= ' property_toured_on = ' . $this->sqlPropertyTouredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_toured_on = ' . $this->sqlUnitTouredOn(). ',' ; } elseif( true == array_key_exists( 'UnitTouredOn', $this->getChangedColumns() ) ) { $strSql .= ' unit_toured_on = ' . $this->sqlUnitTouredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn(). ',' ; } elseif( true == array_key_exists( 'BlueMoonApplicationSentOn', $this->getChangedColumns() ) ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' info_approved_on = ' . $this->sqlInfoApprovedOn(). ',' ; } elseif( true == array_key_exists( 'InfoApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' info_approved_on = ' . $this->sqlInfoApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_qualification_completed_on = ' . $this->sqlPreQualificationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'PreQualificationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' pre_qualification_completed_on = ' . $this->sqlPreQualificationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_qualification_approved_on = ' . $this->sqlPreQualificationApprovedOn(). ',' ; } elseif( true == array_key_exists( 'PreQualificationApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' pre_qualification_approved_on = ' . $this->sqlPreQualificationApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_started_on = ' . $this->sqlApplicationStartedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_started_on = ' . $this->sqlApplicationStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_completed_on = ' . $this->sqlApplicationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_completed_on = ' . $this->sqlApplicationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_approved_on = ' . $this->sqlApplicationApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_approved_on = ' . $this->sqlApplicationApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_approved_on = ' . $this->sqlScreeningApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_approved_on = ' . $this->sqlScreeningApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseGeneratedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_completed_on = ' . $this->sqlLeaseCompletedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_completed_on = ' . $this->sqlLeaseCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_approved_on = ' . $this->sqlLeaseApprovedOn(). ',' ; } elseif( true == array_key_exists( 'LeaseApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_approved_on = ' . $this->sqlLeaseApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_in_on = ' . $this->sqlTransferredInOn(). ',' ; } elseif( true == array_key_exists( 'TransferredInOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_in_on = ' . $this->sqlTransferredInOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_out_on = ' . $this->sqlTransferredOutOn(). ',' ; } elseif( true == array_key_exists( 'TransferredOutOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_out_on = ' . $this->sqlTransferredOutOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mute_followups_until = ' . $this->sqlMuteFollowupsUntil(). ',' ; } elseif( true == array_key_exists( 'MuteFollowupsUntil', $this->getChangedColumns() ) ) { $strSql .= ' mute_followups_until = ' . $this->sqlMuteFollowupsUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quoted_rent_locked_until = ' . $this->sqlQuotedRentLockedUntil(). ',' ; } elseif( true == array_key_exists( 'QuotedRentLockedUntil', $this->getChangedColumns() ) ) { $strSql .= ' quoted_rent_locked_until = ' . $this->sqlQuotedRentLockedUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hold_unit_until = ' . $this->sqlHoldUnitUntil(). ',' ; } elseif( true == array_key_exists( 'HoldUnitUntil', $this->getChangedColumns() ) ) { $strSql .= ' hold_unit_until = ' . $this->sqlHoldUnitUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_start_on = ' . $this->sqlWaitListStartOn(). ',' ; } elseif( true == array_key_exists( 'WaitListStartOn', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_start_on = ' . $this->sqlWaitListStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_end_on = ' . $this->sqlWaitListEndOn(). ',' ; } elseif( true == array_key_exists( 'WaitListEndOn', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_end_on = ' . $this->sqlWaitListEndOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_capture = ' . $this->sqlRequiresCapture(). ',' ; } elseif( true == array_key_exists( 'RequiresCapture', $this->getChangedColumns() ) ) { $strSql .= ' requires_capture = ' . $this->sqlRequiresCapture() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pet_policy_conflicted = ' . $this->sqlIsPetPolicyConflicted(). ',' ; } elseif( true == array_key_exists( 'IsPetPolicyConflicted', $this->getChangedColumns() ) ) { $strSql .= ' is_pet_policy_conflicted = ' . $this->sqlIsPetPolicyConflicted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy(). ',' ; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wait_list_points = ' . $this->sqlWaitListPoints(). ',' ; } elseif( true == array_key_exists( 'WaitListPoints', $this->getChangedColumns() ) ) { $strSql .= ' wait_list_points = ' . $this->sqlWaitListPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_completed_on_initial = ' . $this->sqlApplicationCompletedOnInitial(). ',' ; } elseif( true == array_key_exists( 'ApplicationCompletedOnInitial', $this->getChangedColumns() ) ) { $strSql .= ' application_completed_on_initial = ' . $this->sqlApplicationCompletedOnInitial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_approved_on_initial = ' . $this->sqlApplicationApprovedOnInitial(). ',' ; } elseif( true == array_key_exists( 'ApplicationApprovedOnInitial', $this->getChangedColumns() ) ) { $strSql .= ' application_approved_on_initial = ' . $this->sqlApplicationApprovedOnInitial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_reopened_on = ' . $this->sqlApplicationReopenedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationReopenedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_reopened_on = ' . $this->sqlApplicationReopenedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalType', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusType', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term = ' . $this->sqlLeaseTerm(). ',' ; } elseif( true == array_key_exists( 'LeaseTerm', $this->getChangedColumns() ) ) { $strSql .= ' lease_term = ' . $this->sqlLeaseTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_start_date = ' . $this->sqlLeaseStartWindowStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_start_date = ' . $this->sqlLeaseStartWindowStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_end_date = ' . $this->sqlLeaseStartWindowEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_end_date = ' . $this->sqlLeaseStartWindowEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_base_rent = ' . $this->sqlContractBaseRent(). ',' ; } elseif( true == array_key_exists( 'ContractBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_base_rent = ' . $this->sqlContractBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_amenity_rent = ' . $this->sqlContractAmenityRent(). ',' ; } elseif( true == array_key_exists( 'ContractAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_amenity_rent = ' . $this->sqlContractAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_pet_rent = ' . $this->sqlContractPetRent(). ',' ; } elseif( true == array_key_exists( 'ContractPetRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_pet_rent = ' . $this->sqlContractPetRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_add_on_rent = ' . $this->sqlContractAddOnRent(). ',' ; } elseif( true == array_key_exists( 'ContractAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_add_on_rent = ' . $this->sqlContractAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_risk_premium_rent = ' . $this->sqlContractRiskPremiumRent(). ',' ; } elseif( true == array_key_exists( 'ContractRiskPremiumRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_risk_premium_rent = ' . $this->sqlContractRiskPremiumRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_special_rent = ' . $this->sqlContractSpecialRent(). ',' ; } elseif( true == array_key_exists( 'ContractSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_special_rent = ' . $this->sqlContractSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_total_rent = ' . $this->sqlContractTotalRent(). ',' ; } elseif( true == array_key_exists( 'ContractTotalRent', $this->getChangedColumns() ) ) { $strSql .= ' contract_total_rent = ' . $this->sqlContractTotalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_base_deposit = ' . $this->sqlContractBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_base_deposit = ' . $this->sqlContractBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_amenity_deposit = ' . $this->sqlContractAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_amenity_deposit = ' . $this->sqlContractAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_pet_deposit = ' . $this->sqlContractPetDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractPetDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_pet_deposit = ' . $this->sqlContractPetDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_add_on_deposit = ' . $this->sqlContractAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_add_on_deposit = ' . $this->sqlContractAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_risk_premium_deposit = ' . $this->sqlContractRiskPremiumDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractRiskPremiumDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_risk_premium_deposit = ' . $this->sqlContractRiskPremiumDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_special_deposit = ' . $this->sqlContractSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_special_deposit = ' . $this->sqlContractSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_base_other = ' . $this->sqlContractBaseOther(). ',' ; } elseif( true == array_key_exists( 'ContractBaseOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_base_other = ' . $this->sqlContractBaseOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_amenity_other = ' . $this->sqlContractAmenityOther(). ',' ; } elseif( true == array_key_exists( 'ContractAmenityOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_amenity_other = ' . $this->sqlContractAmenityOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_pet_other = ' . $this->sqlContractPetOther(). ',' ; } elseif( true == array_key_exists( 'ContractPetOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_pet_other = ' . $this->sqlContractPetOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_add_on_other = ' . $this->sqlContractAddOnOther(). ',' ; } elseif( true == array_key_exists( 'ContractAddOnOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_add_on_other = ' . $this->sqlContractAddOnOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_risk_premium_other = ' . $this->sqlContractRiskPremiumOther(). ',' ; } elseif( true == array_key_exists( 'ContractRiskPremiumOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_risk_premium_other = ' . $this->sqlContractRiskPremiumOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_special_other = ' . $this->sqlContractSpecialOther(). ',' ; } elseif( true == array_key_exists( 'ContractSpecialOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_special_other = ' . $this->sqlContractSpecialOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_total_other = ' . $this->sqlContractTotalOther(). ',' ; } elseif( true == array_key_exists( 'ContractTotalOther', $this->getChangedColumns() ) ) { $strSql .= ' contract_total_other = ' . $this->sqlContractTotalOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_total_deposit = ' . $this->sqlContractTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'ContractTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' contract_total_deposit = ' . $this->sqlContractTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_partially_completed_on = ' . $this->sqlApplicationPartiallyCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicationPartiallyCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_partially_completed_on = ' . $this->sqlApplicationPartiallyCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_completed_on_initial = ' . $this->sqlLeaseCompletedOnInitial(). ',' ; } elseif( true == array_key_exists( 'LeaseCompletedOnInitial', $this->getChangedColumns() ) ) { $strSql .= ' lease_completed_on_initial = ' . $this->sqlLeaseCompletedOnInitial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_partially_completed_on_initial = ' . $this->sqlLeasePartiallyCompletedOnInitial(). ',' ; } elseif( true == array_key_exists( 'LeasePartiallyCompletedOnInitial', $this->getChangedColumns() ) ) { $strSql .= ' lease_partially_completed_on_initial = ' . $this->sqlLeasePartiallyCompletedOnInitial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_approved_on_initial = ' . $this->sqlLeaseApprovedOnInitial(). ',' ; } elseif( true == array_key_exists( 'LeaseApprovedOnInitial', $this->getChangedColumns() ) ) { $strSql .= ' lease_approved_on_initial = ' . $this->sqlLeaseApprovedOnInitial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_rented_on = ' . $this->sqlUnitSpaceRentedOn(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceRentedOn', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_rented_on = ' . $this->sqlUnitSpaceRentedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_list_item_id = ' . $this->sqlDefaultListItemId(). ',' ; } elseif( true == array_key_exists( 'DefaultListItemId', $this->getChangedColumns() ) ) { $strSql .= ' default_list_item_id = ' . $this->sqlDefaultListItemId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'application_id' => $this->getApplicationId(),
			'prior_cached_application_log_id' => $this->getPriorCachedApplicationLogId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'lease_type_id' => $this->getLeaseTypeId(),
			'lead_conversion_type' => $this->getLeadConversionType(),
			'old_application_status_type_id' => $this->getOldApplicationStatusTypeId(),
			'application_stage_id' => $this->getApplicationStageId(),
			'application_status_id' => $this->getApplicationStatusId(),
			'application_step_id' => $this->getApplicationStepId(),
			'max_application_stage_id' => $this->getMaxApplicationStageId(),
			'max_application_status_id' => $this->getMaxApplicationStatusId(),
			'max_application_step_id' => $this->getMaxApplicationStepId(),
			'cancellation_list_type_id' => $this->getCancellationListTypeId(),
			'cancellation_list_item_id' => $this->getCancellationListItemId(),
			'primary_applicant_id' => $this->getPrimaryApplicantId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'desired_space_configuration_id' => $this->getDesiredSpaceConfigurationId(),
			'screening_id' => $this->getScreeningId(),
			'ps_product_id' => $this->getPsProductId(),
			'application_ps_product_id' => $this->getApplicationPsProductId(),
			'lease_ps_product_id' => $this->getLeasePsProductId(),
			'leasing_agent_id' => $this->getLeasingAgentId(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'originating_lead_source_id' => $this->getOriginatingLeadSourceId(),
			'converting_lead_source_id' => $this->getConvertingLeadSourceId(),
			'company_application_id' => $this->getCompanyApplicationId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'late_fee_formula_id' => $this->getLateFeeFormulaId(),
			'combined_application_id' => $this->getCombinedApplicationId(),
			'quote_id' => $this->getQuoteId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'sem_source_id' => $this->getSemSourceId(),
			'call_id' => $this->getCallId(),
			'website_id' => $this->getWebsiteId(),
			'craigslist_post_id' => $this->getCraigslistPostId(),
			'cl_ad_id' => $this->getClAdId(),
			'traffic_cookie_id' => $this->getTrafficCookieId(),
			'first_event_id' => $this->getFirstEventId(),
			'first_event_medium_id' => $this->getFirstEventMediumId(),
			'first_leasing_center_event_id' => $this->getFirstLeasingCenterEventId(),
			'response_event_id' => $this->getResponseEventId(),
			'last_event_id' => $this->getLastEventId(),
			'lease_signature_type_id' => $this->getLeaseSignatureTypeId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'insurance_policy_status_type_id' => $this->getInsurancePolicyStatusTypeId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'wait_list_id' => $this->getWaitListId(),
			'wait_list_application_id' => $this->getWaitListApplicationId(),
			'wait_list_point_ids' => $this->getWaitListPointIds(),
			'wait_list_position' => $this->getWaitListPosition(),
			'wait_list_rejection_count' => $this->getWaitListRejectionCount(),
			'guest_remote_primary_key' => $this->getGuestRemotePrimaryKey(),
			'app_remote_primary_key' => $this->getAppRemotePrimaryKey(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'property_name' => $this->getPropertyName(),
			'building_name' => $this->getBuildingName(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'unit_square_feet' => $this->getUnitSquareFeet(),
			'unit_prior_application_id' => $this->getUnitPriorApplicationId(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'primary_phone_number' => $this->getPrimaryPhoneNumber(),
			'email_address' => $this->getEmailAddress(),
			'term_month' => $this->getTermMonth(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'optimal_monthly_rent_base' => $this->getOptimalMonthlyRentBase(),
			'optimal_monthly_rent_total' => $this->getOptimalMonthlyRentTotal(),
			'advertised_monthly_rent_base' => $this->getAdvertisedMonthlyRentBase(),
			'advertised_monthly_rent_amenity' => $this->getAdvertisedMonthlyRentAmenity(),
			'advertised_monthly_rent_add_on' => $this->getAdvertisedMonthlyRentAddOn(),
			'advertised_monthly_rent_special' => $this->getAdvertisedMonthlyRentSpecial(),
			'advertised_monthly_rent_total' => $this->getAdvertisedMonthlyRentTotal(),
			'advertised_monthly_other_base' => $this->getAdvertisedMonthlyOtherBase(),
			'advertised_monthly_other_amenity' => $this->getAdvertisedMonthlyOtherAmenity(),
			'advertised_monthly_other_add_on' => $this->getAdvertisedMonthlyOtherAddOn(),
			'advertised_monthly_other_special' => $this->getAdvertisedMonthlyOtherSpecial(),
			'advertised_monthly_other_total' => $this->getAdvertisedMonthlyOtherTotal(),
			'advertised_one_time_deposit_base' => $this->getAdvertisedOneTimeDepositBase(),
			'advertised_one_time_deposit_amenity' => $this->getAdvertisedOneTimeDepositAmenity(),
			'advertised_one_time_deposit_add_on' => $this->getAdvertisedOneTimeDepositAddOn(),
			'advertised_one_time_deposit_special' => $this->getAdvertisedOneTimeDepositSpecial(),
			'advertised_one_time_deposit_total' => $this->getAdvertisedOneTimeDepositTotal(),
			'advertised_one_time_application_base' => $this->getAdvertisedOneTimeApplicationBase(),
			'advertised_one_time_application_amenity' => $this->getAdvertisedOneTimeApplicationAmenity(),
			'advertised_one_time_application_add_on' => $this->getAdvertisedOneTimeApplicationAddOn(),
			'advertised_one_time_application_special' => $this->getAdvertisedOneTimeApplicationSpecial(),
			'advertised_one_time_application_total' => $this->getAdvertisedOneTimeApplicationTotal(),
			'advertised_one_time_other_base' => $this->getAdvertisedOneTimeOtherBase(),
			'advertised_one_time_other_amenity' => $this->getAdvertisedOneTimeOtherAmenity(),
			'advertised_one_time_other_add_on' => $this->getAdvertisedOneTimeOtherAddOn(),
			'advertised_one_time_other_special' => $this->getAdvertisedOneTimeOtherSpecial(),
			'advertised_one_time_other_total' => $this->getAdvertisedOneTimeOtherTotal(),
			'executed_monthly_rent_base' => $this->getExecutedMonthlyRentBase(),
			'executed_monthly_rent_amenity' => $this->getExecutedMonthlyRentAmenity(),
			'executed_monthly_rent_add_on' => $this->getExecutedMonthlyRentAddOn(),
			'executed_monthly_rent_special' => $this->getExecutedMonthlyRentSpecial(),
			'executed_monthly_rent_risk_premium' => $this->getExecutedMonthlyRentRiskPremium(),
			'executed_monthly_rent_pet' => $this->getExecutedMonthlyRentPet(),
			'executed_monthly_rent_total' => $this->getExecutedMonthlyRentTotal(),
			'executed_monthly_other_base' => $this->getExecutedMonthlyOtherBase(),
			'executed_monthly_other_amenity' => $this->getExecutedMonthlyOtherAmenity(),
			'executed_monthly_other_add_on' => $this->getExecutedMonthlyOtherAddOn(),
			'executed_monthly_other_special' => $this->getExecutedMonthlyOtherSpecial(),
			'executed_monthly_other_risk_premium' => $this->getExecutedMonthlyOtherRiskPremium(),
			'executed_monthly_other_pet' => $this->getExecutedMonthlyOtherPet(),
			'executed_monthly_other_total' => $this->getExecutedMonthlyOtherTotal(),
			'executed_one_time_deposit_base' => $this->getExecutedOneTimeDepositBase(),
			'executed_one_time_deposit_amenity' => $this->getExecutedOneTimeDepositAmenity(),
			'executed_one_time_deposit_add_on' => $this->getExecutedOneTimeDepositAddOn(),
			'executed_one_time_deposit_special' => $this->getExecutedOneTimeDepositSpecial(),
			'executed_one_time_deposit_risk_premium' => $this->getExecutedOneTimeDepositRiskPremium(),
			'executed_one_time_deposit_pet' => $this->getExecutedOneTimeDepositPet(),
			'executed_one_time_deposit_total' => $this->getExecutedOneTimeDepositTotal(),
			'executed_one_time_application_base' => $this->getExecutedOneTimeApplicationBase(),
			'executed_one_time_application_amenity' => $this->getExecutedOneTimeApplicationAmenity(),
			'executed_one_time_application_add_on' => $this->getExecutedOneTimeApplicationAddOn(),
			'executed_one_time_application_special' => $this->getExecutedOneTimeApplicationSpecial(),
			'executed_one_time_application_risk_premium' => $this->getExecutedOneTimeApplicationRiskPremium(),
			'executed_one_time_application_pet' => $this->getExecutedOneTimeApplicationPet(),
			'executed_one_time_application_total' => $this->getExecutedOneTimeApplicationTotal(),
			'executed_one_time_other_base' => $this->getExecutedOneTimeOtherBase(),
			'executed_one_time_other_amenity' => $this->getExecutedOneTimeOtherAmenity(),
			'executed_one_time_other_add_on' => $this->getExecutedOneTimeOtherAddOn(),
			'executed_one_time_other_special' => $this->getExecutedOneTimeOtherSpecial(),
			'executed_one_time_other_risk_premium' => $this->getExecutedOneTimeOtherRiskPremium(),
			'executed_one_time_other_pet' => $this->getExecutedOneTimeOtherPet(),
			'executed_one_time_other_total' => $this->getExecutedOneTimeOtherTotal(),
			'executed_optional_monthly_rent_dog' => $this->getExecutedOptionalMonthlyRentDog(),
			'executed_optional_monthly_rent_cat' => $this->getExecutedOptionalMonthlyRentCat(),
			'executed_optional_monthly_rent_other_pet' => $this->getExecutedOptionalMonthlyRentOtherPet(),
			'executed_optional_monthly_rent_garage' => $this->getExecutedOptionalMonthlyRentGarage(),
			'executed_optional_monthly_rent_appliances' => $this->getExecutedOptionalMonthlyRentAppliances(),
			'executed_optional_monthly_rent_furniture' => $this->getExecutedOptionalMonthlyRentFurniture(),
			'executed_optional_monthly_rent_furnished' => $this->getExecutedOptionalMonthlyRentFurnished(),
			'executed_optional_monthly_rent_storage' => $this->getExecutedOptionalMonthlyRentStorage(),
			'executed_optional_monthly_rent_parking' => $this->getExecutedOptionalMonthlyRentParking(),
			'executed_optional_monthly_rent_meal_plans' => $this->getExecutedOptionalMonthlyRentMealPlans(),
			'executed_optional_monthly_rent_other_add_on' => $this->getExecutedOptionalMonthlyRentOtherAddOn(),
			'vehicle_count' => $this->getVehicleCount(),
			'household_income' => $this->getHouseholdIncome(),
			'desired_rent_min' => $this->getDesiredRentMin(),
			'desired_rent_max' => $this->getDesiredRentMax(),
			'desired_bedrooms' => $this->getDesiredBedrooms(),
			'desired_bathrooms' => $this->getDesiredBathrooms(),
			'desired_pets' => $this->getDesiredPets(),
			'desired_occupants' => $this->getDesiredOccupants(),
			'desired_amenities' => $this->getDesiredAmenities(),
			'total_responsible' => $this->getTotalResponsible(),
			'total_not_responsible' => $this->getTotalNotResponsible(),
			'total_guarantors' => $this->getTotalGuarantors(),
			'total_non_leasing_applicants' => $this->getTotalNonLeasingApplicants(),
			'total_applicants' => $this->getTotalApplicants(),
			'guest_card_field_completions' => $this->getGuestCardFieldCompletions(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'application_datetime' => $this->getApplicationDatetime(),
			'url_referer' => $this->getUrlReferer(),
			'details' => $this->getDetails(),
			'quotes_count' => $this->getQuotesCount(),
			'conversion_probability' => $this->getConversionProbability(),
			'first_contacted_on' => $this->getFirstContactedOn(),
			'time_to_first_response' => $this->getTimeToFirstResponse(),
			'site_visited_on' => $this->getSiteVisitedOn(),
			'property_toured_on' => $this->getPropertyTouredOn(),
			'unit_toured_on' => $this->getUnitTouredOn(),
			'blue_moon_application_sent_on' => $this->getBlueMoonApplicationSentOn(),
			'info_approved_on' => $this->getInfoApprovedOn(),
			'pre_qualification_completed_on' => $this->getPreQualificationCompletedOn(),
			'pre_qualification_approved_on' => $this->getPreQualificationApprovedOn(),
			'application_started_on' => $this->getApplicationStartedOn(),
			'application_completed_on' => $this->getApplicationCompletedOn(),
			'application_approved_on' => $this->getApplicationApprovedOn(),
			'screening_approved_on' => $this->getScreeningApprovedOn(),
			'lease_generated_on' => $this->getLeaseGeneratedOn(),
			'lease_completed_on' => $this->getLeaseCompletedOn(),
			'lease_approved_on' => $this->getLeaseApprovedOn(),
			'cancelled_on' => $this->getCancelledOn(),
			'transferred_in_on' => $this->getTransferredInOn(),
			'transferred_out_on' => $this->getTransferredOutOn(),
			'mute_followups_until' => $this->getMuteFollowupsUntil(),
			'quoted_rent_locked_until' => $this->getQuotedRentLockedUntil(),
			'hold_unit_until' => $this->getHoldUnitUntil(),
			'wait_list_start_on' => $this->getWaitListStartOn(),
			'wait_list_end_on' => $this->getWaitListEndOn(),
			'requires_capture' => $this->getRequiresCapture(),
			'is_pet_policy_conflicted' => $this->getIsPetPolicyConflicted(),
			'is_deleted' => $this->getIsDeleted(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'wait_list_points' => $this->getWaitListPoints(),
			'application_completed_on_initial' => $this->getApplicationCompletedOnInitial(),
			'application_approved_on_initial' => $this->getApplicationApprovedOnInitial(),
			'application_reopened_on' => $this->getApplicationReopenedOn(),
			'lease_interval_type' => $this->getLeaseIntervalType(),
			'lease_status_type' => $this->getLeaseStatusType(),
			'lease_term' => $this->getLeaseTerm(),
			'lease_start_window_start_date' => $this->getLeaseStartWindowStartDate(),
			'lease_start_window_end_date' => $this->getLeaseStartWindowEndDate(),
			'contract_base_rent' => $this->getContractBaseRent(),
			'contract_amenity_rent' => $this->getContractAmenityRent(),
			'contract_pet_rent' => $this->getContractPetRent(),
			'contract_add_on_rent' => $this->getContractAddOnRent(),
			'contract_risk_premium_rent' => $this->getContractRiskPremiumRent(),
			'contract_special_rent' => $this->getContractSpecialRent(),
			'contract_total_rent' => $this->getContractTotalRent(),
			'contract_base_deposit' => $this->getContractBaseDeposit(),
			'contract_amenity_deposit' => $this->getContractAmenityDeposit(),
			'contract_pet_deposit' => $this->getContractPetDeposit(),
			'contract_add_on_deposit' => $this->getContractAddOnDeposit(),
			'contract_risk_premium_deposit' => $this->getContractRiskPremiumDeposit(),
			'contract_special_deposit' => $this->getContractSpecialDeposit(),
			'contract_base_other' => $this->getContractBaseOther(),
			'contract_amenity_other' => $this->getContractAmenityOther(),
			'contract_pet_other' => $this->getContractPetOther(),
			'contract_add_on_other' => $this->getContractAddOnOther(),
			'contract_risk_premium_other' => $this->getContractRiskPremiumOther(),
			'contract_special_other' => $this->getContractSpecialOther(),
			'contract_total_other' => $this->getContractTotalOther(),
			'contract_total_deposit' => $this->getContractTotalDeposit(),
			'application_partially_completed_on' => $this->getApplicationPartiallyCompletedOn(),
			'lease_completed_on_initial' => $this->getLeaseCompletedOnInitial(),
			'lease_partially_completed_on_initial' => $this->getLeasePartiallyCompletedOnInitial(),
			'lease_approved_on_initial' => $this->getLeaseApprovedOnInitial(),
			'unit_space_rented_on' => $this->getUnitSpaceRentedOn(),
			'default_list_item_id' => $this->getDefaultListItemId()
		);
	}

}
?>