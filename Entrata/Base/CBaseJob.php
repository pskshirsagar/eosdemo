<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJob extends CEosSingularBase {

	const TABLE_NAME = 'public.jobs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobTypeId;
	protected $m_intJobStatusId;
	protected $m_intPropertyId;
	protected $m_intBudgetSummaryApHeaderId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltDefaultRetentionPercent;
	protected $m_strOriginalStartDate;
	protected $m_strOriginalEndDate;
	protected $m_strActualStartDate;
	protected $m_strActualEndDate;
	protected $m_boolUseRetention;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intCancelledBy;
	protected $m_strCancelledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsBudgetByUnitLocation;
	protected $m_intJobCategoryId;
	protected $m_intMaintenanceTemplateId;

	public function __construct() {
		parent::__construct();

		$this->m_intJobStatusId = '1';
		$this->m_fltDefaultRetentionPercent = '0';
		$this->m_boolUseRetention = false;
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';
		$this->m_boolIsBudgetByUnitLocation = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_type_id'] ) && $boolDirectSet ) $this->set( 'm_intJobTypeId', trim( $arrValues['job_type_id'] ) ); elseif( isset( $arrValues['job_type_id'] ) ) $this->setJobTypeId( $arrValues['job_type_id'] );
		if( isset( $arrValues['job_status_id'] ) && $boolDirectSet ) $this->set( 'm_intJobStatusId', trim( $arrValues['job_status_id'] ) ); elseif( isset( $arrValues['job_status_id'] ) ) $this->setJobStatusId( $arrValues['job_status_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['budget_summary_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetSummaryApHeaderId', trim( $arrValues['budget_summary_ap_header_id'] ) ); elseif( isset( $arrValues['budget_summary_ap_header_id'] ) ) $this->setBudgetSummaryApHeaderId( $arrValues['budget_summary_ap_header_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_retention_percent'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultRetentionPercent', trim( $arrValues['default_retention_percent'] ) ); elseif( isset( $arrValues['default_retention_percent'] ) ) $this->setDefaultRetentionPercent( $arrValues['default_retention_percent'] );
		if( isset( $arrValues['original_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalStartDate', trim( $arrValues['original_start_date'] ) ); elseif( isset( $arrValues['original_start_date'] ) ) $this->setOriginalStartDate( $arrValues['original_start_date'] );
		if( isset( $arrValues['original_end_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalEndDate', trim( $arrValues['original_end_date'] ) ); elseif( isset( $arrValues['original_end_date'] ) ) $this->setOriginalEndDate( $arrValues['original_end_date'] );
		if( isset( $arrValues['actual_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDate', trim( $arrValues['actual_start_date'] ) ); elseif( isset( $arrValues['actual_start_date'] ) ) $this->setActualStartDate( $arrValues['actual_start_date'] );
		if( isset( $arrValues['actual_end_date'] ) && $boolDirectSet ) $this->set( 'm_strActualEndDate', trim( $arrValues['actual_end_date'] ) ); elseif( isset( $arrValues['actual_end_date'] ) ) $this->setActualEndDate( $arrValues['actual_end_date'] );
		if( isset( $arrValues['use_retention'] ) && $boolDirectSet ) $this->set( 'm_boolUseRetention', trim( stripcslashes( $arrValues['use_retention'] ) ) ); elseif( isset( $arrValues['use_retention'] ) ) $this->setUseRetention( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_retention'] ) : $arrValues['use_retention'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['cancelled_by'] ) && $boolDirectSet ) $this->set( 'm_intCancelledBy', trim( $arrValues['cancelled_by'] ) ); elseif( isset( $arrValues['cancelled_by'] ) ) $this->setCancelledBy( $arrValues['cancelled_by'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_budget_by_unit_location'] ) && $boolDirectSet ) $this->set( 'm_boolIsBudgetByUnitLocation', trim( stripcslashes( $arrValues['is_budget_by_unit_location'] ) ) ); elseif( isset( $arrValues['is_budget_by_unit_location'] ) ) $this->setIsBudgetByUnitLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_budget_by_unit_location'] ) : $arrValues['is_budget_by_unit_location'] );
		if( isset( $arrValues['job_category_id'] ) && $boolDirectSet ) $this->set( 'm_intJobCategoryId', trim( $arrValues['job_category_id'] ) ); elseif( isset( $arrValues['job_category_id'] ) ) $this->setJobCategoryId( $arrValues['job_category_id'] );
		if( isset( $arrValues['maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceTemplateId', trim( $arrValues['maintenance_template_id'] ) ); elseif( isset( $arrValues['maintenance_template_id'] ) ) $this->setMaintenanceTemplateId( $arrValues['maintenance_template_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobTypeId( $intJobTypeId ) {
		$this->set( 'm_intJobTypeId', CStrings::strToIntDef( $intJobTypeId, NULL, false ) );
	}

	public function getJobTypeId() {
		return $this->m_intJobTypeId;
	}

	public function sqlJobTypeId() {
		return ( true == isset( $this->m_intJobTypeId ) ) ? ( string ) $this->m_intJobTypeId : 'NULL';
	}

	public function setJobStatusId( $intJobStatusId ) {
		$this->set( 'm_intJobStatusId', CStrings::strToIntDef( $intJobStatusId, NULL, false ) );
	}

	public function getJobStatusId() {
		return $this->m_intJobStatusId;
	}

	public function sqlJobStatusId() {
		return ( true == isset( $this->m_intJobStatusId ) ) ? ( string ) $this->m_intJobStatusId : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBudgetSummaryApHeaderId( $intBudgetSummaryApHeaderId ) {
		$this->set( 'm_intBudgetSummaryApHeaderId', CStrings::strToIntDef( $intBudgetSummaryApHeaderId, NULL, false ) );
	}

	public function getBudgetSummaryApHeaderId() {
		return $this->m_intBudgetSummaryApHeaderId;
	}

	public function sqlBudgetSummaryApHeaderId() {
		return ( true == isset( $this->m_intBudgetSummaryApHeaderId ) ) ? ( string ) $this->m_intBudgetSummaryApHeaderId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultRetentionPercent( $fltDefaultRetentionPercent ) {
		$this->set( 'm_fltDefaultRetentionPercent', CStrings::strToFloatDef( $fltDefaultRetentionPercent, NULL, false, 4 ) );
	}

	public function getDefaultRetentionPercent() {
		return $this->m_fltDefaultRetentionPercent;
	}

	public function sqlDefaultRetentionPercent() {
		return ( true == isset( $this->m_fltDefaultRetentionPercent ) ) ? ( string ) $this->m_fltDefaultRetentionPercent : '0';
	}

	public function setOriginalStartDate( $strOriginalStartDate ) {
		$this->set( 'm_strOriginalStartDate', CStrings::strTrimDef( $strOriginalStartDate, -1, NULL, true ) );
	}

	public function getOriginalStartDate() {
		return $this->m_strOriginalStartDate;
	}

	public function sqlOriginalStartDate() {
		return ( true == isset( $this->m_strOriginalStartDate ) ) ? '\'' . $this->m_strOriginalStartDate . '\'' : 'NOW()';
	}

	public function setOriginalEndDate( $strOriginalEndDate ) {
		$this->set( 'm_strOriginalEndDate', CStrings::strTrimDef( $strOriginalEndDate, -1, NULL, true ) );
	}

	public function getOriginalEndDate() {
		return $this->m_strOriginalEndDate;
	}

	public function sqlOriginalEndDate() {
		return ( true == isset( $this->m_strOriginalEndDate ) ) ? '\'' . $this->m_strOriginalEndDate . '\'' : 'NOW()';
	}

	public function setActualStartDate( $strActualStartDate ) {
		$this->set( 'm_strActualStartDate', CStrings::strTrimDef( $strActualStartDate, -1, NULL, true ) );
	}

	public function getActualStartDate() {
		return $this->m_strActualStartDate;
	}

	public function sqlActualStartDate() {
		return ( true == isset( $this->m_strActualStartDate ) ) ? '\'' . $this->m_strActualStartDate . '\'' : 'NULL';
	}

	public function setActualEndDate( $strActualEndDate ) {
		$this->set( 'm_strActualEndDate', CStrings::strTrimDef( $strActualEndDate, -1, NULL, true ) );
	}

	public function getActualEndDate() {
		return $this->m_strActualEndDate;
	}

	public function sqlActualEndDate() {
		return ( true == isset( $this->m_strActualEndDate ) ) ? '\'' . $this->m_strActualEndDate . '\'' : 'NULL';
	}

	public function setUseRetention( $boolUseRetention ) {
		$this->set( 'm_boolUseRetention', CStrings::strToBool( $boolUseRetention ) );
	}

	public function getUseRetention() {
		return $this->m_boolUseRetention;
	}

	public function sqlUseRetention() {
		return ( true == isset( $this->m_boolUseRetention ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseRetention ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setCancelledBy( $intCancelledBy ) {
		$this->set( 'm_intCancelledBy', CStrings::strToIntDef( $intCancelledBy, NULL, false ) );
	}

	public function getCancelledBy() {
		return $this->m_intCancelledBy;
	}

	public function sqlCancelledBy() {
		return ( true == isset( $this->m_intCancelledBy ) ) ? ( string ) $this->m_intCancelledBy : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsBudgetByUnitLocation( $boolIsBudgetByUnitLocation ) {
		$this->set( 'm_boolIsBudgetByUnitLocation', CStrings::strToBool( $boolIsBudgetByUnitLocation ) );
	}

	public function getIsBudgetByUnitLocation() {
		return $this->m_boolIsBudgetByUnitLocation;
	}

	public function sqlIsBudgetByUnitLocation() {
		return ( true == isset( $this->m_boolIsBudgetByUnitLocation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBudgetByUnitLocation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setJobCategoryId( $intJobCategoryId ) {
		$this->set( 'm_intJobCategoryId', CStrings::strToIntDef( $intJobCategoryId, NULL, false ) );
	}

	public function getJobCategoryId() {
		return $this->m_intJobCategoryId;
	}

	public function sqlJobCategoryId() {
		return ( true == isset( $this->m_intJobCategoryId ) ) ? ( string ) $this->m_intJobCategoryId : 'NULL';
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->set( 'm_intMaintenanceTemplateId', CStrings::strToIntDef( $intMaintenanceTemplateId, NULL, false ) );
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function sqlMaintenanceTemplateId() {
		return ( true == isset( $this->m_intMaintenanceTemplateId ) ) ? ( string ) $this->m_intMaintenanceTemplateId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_type_id, job_status_id, property_id, budget_summary_ap_header_id, name, description, default_retention_percent, original_start_date, original_end_date, actual_start_date, actual_end_date, use_retention, approved_by, approved_on, completed_by, completed_on, cancelled_by, cancelled_on, updated_by, updated_on, created_by, created_on, is_budget_by_unit_location, job_category_id, maintenance_template_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlJobTypeId() . ', ' .
						$this->sqlJobStatusId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlBudgetSummaryApHeaderId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDefaultRetentionPercent() . ', ' .
						$this->sqlOriginalStartDate() . ', ' .
						$this->sqlOriginalEndDate() . ', ' .
						$this->sqlActualStartDate() . ', ' .
						$this->sqlActualEndDate() . ', ' .
						$this->sqlUseRetention() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlCancelledBy() . ', ' .
						$this->sqlCancelledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsBudgetByUnitLocation() . ', ' .
						$this->sqlJobCategoryId() . ', ' .
						$this->sqlMaintenanceTemplateId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_type_id = ' . $this->sqlJobTypeId(). ',' ; } elseif( true == array_key_exists( 'JobTypeId', $this->getChangedColumns() ) ) { $strSql .= ' job_type_id = ' . $this->sqlJobTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_status_id = ' . $this->sqlJobStatusId(). ',' ; } elseif( true == array_key_exists( 'JobStatusId', $this->getChangedColumns() ) ) { $strSql .= ' job_status_id = ' . $this->sqlJobStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_summary_ap_header_id = ' . $this->sqlBudgetSummaryApHeaderId(). ',' ; } elseif( true == array_key_exists( 'BudgetSummaryApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' budget_summary_ap_header_id = ' . $this->sqlBudgetSummaryApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_retention_percent = ' . $this->sqlDefaultRetentionPercent(). ',' ; } elseif( true == array_key_exists( 'DefaultRetentionPercent', $this->getChangedColumns() ) ) { $strSql .= ' default_retention_percent = ' . $this->sqlDefaultRetentionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate(). ',' ; } elseif( true == array_key_exists( 'OriginalStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate(). ',' ; } elseif( true == array_key_exists( 'OriginalEndDate', $this->getChangedColumns() ) ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate(). ',' ; } elseif( true == array_key_exists( 'ActualStartDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate(). ',' ; } elseif( true == array_key_exists( 'ActualEndDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_retention = ' . $this->sqlUseRetention(). ',' ; } elseif( true == array_key_exists( 'UseRetention', $this->getChangedColumns() ) ) { $strSql .= ' use_retention = ' . $this->sqlUseRetention() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy(). ',' ; } elseif( true == array_key_exists( 'CancelledBy', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_budget_by_unit_location = ' . $this->sqlIsBudgetByUnitLocation(). ',' ; } elseif( true == array_key_exists( 'IsBudgetByUnitLocation', $this->getChangedColumns() ) ) { $strSql .= ' is_budget_by_unit_location = ' . $this->sqlIsBudgetByUnitLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_category_id = ' . $this->sqlJobCategoryId(). ',' ; } elseif( true == array_key_exists( 'JobCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' job_category_id = ' . $this->sqlJobCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_type_id' => $this->getJobTypeId(),
			'job_status_id' => $this->getJobStatusId(),
			'property_id' => $this->getPropertyId(),
			'budget_summary_ap_header_id' => $this->getBudgetSummaryApHeaderId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'default_retention_percent' => $this->getDefaultRetentionPercent(),
			'original_start_date' => $this->getOriginalStartDate(),
			'original_end_date' => $this->getOriginalEndDate(),
			'actual_start_date' => $this->getActualStartDate(),
			'actual_end_date' => $this->getActualEndDate(),
			'use_retention' => $this->getUseRetention(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'cancelled_by' => $this->getCancelledBy(),
			'cancelled_on' => $this->getCancelledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_budget_by_unit_location' => $this->getIsBudgetByUnitLocation(),
			'job_category_id' => $this->getJobCategoryId(),
			'maintenance_template_id' => $this->getMaintenanceTemplateId()
		);
	}

}
?>