<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialFloorShare extends CEosSingularBase {

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCommercialLeaseDetailsId;
	protected $m_intPropertyFloorId;
	protected $m_fltSquareFootageShare;
	protected $m_boolIsResponsibleUtilities;
	protected $m_boolIsResponsibleJanitorial;
	protected $m_boolIsResponsibleSecurity;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsResponsibleUtilities = false;
		$this->m_boolIsResponsibleJanitorial = false;
		$this->m_boolIsResponsibleSecurity = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['commercial_lease_details_id'] ) && $boolDirectSet ) $this->m_intCommercialLeaseDetailsId = trim( $arrValues['commercial_lease_details_id'] ); else if( isset( $arrValues['commercial_lease_details_id'] ) ) $this->setCommercialLeaseDetailsId( $arrValues['commercial_lease_details_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->m_intPropertyFloorId = trim( $arrValues['property_floor_id'] ); else if( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['square_footage_share'] ) && $boolDirectSet ) $this->m_fltSquareFootageShare = trim( $arrValues['square_footage_share'] ); else if( isset( $arrValues['square_footage_share'] ) ) $this->setSquareFootageShare( $arrValues['square_footage_share'] );
		if( isset( $arrValues['is_responsible_utilities'] ) && $boolDirectSet ) $this->m_boolIsResponsibleUtilities = trim( stripcslashes( $arrValues['is_responsible_utilities'] ) ); else if( isset( $arrValues['is_responsible_utilities'] ) ) $this->setIsResponsibleUtilities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_utilities'] ) : $arrValues['is_responsible_utilities'] );
		if( isset( $arrValues['is_responsible_janitorial'] ) && $boolDirectSet ) $this->m_boolIsResponsibleJanitorial = trim( stripcslashes( $arrValues['is_responsible_janitorial'] ) ); else if( isset( $arrValues['is_responsible_janitorial'] ) ) $this->setIsResponsibleJanitorial( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_janitorial'] ) : $arrValues['is_responsible_janitorial'] );
		if( isset( $arrValues['is_responsible_security'] ) && $boolDirectSet ) $this->m_boolIsResponsibleSecurity = trim( stripcslashes( $arrValues['is_responsible_security'] ) ); else if( isset( $arrValues['is_responsible_security'] ) ) $this->setIsResponsibleSecurity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responsible_security'] ) : $arrValues['is_responsible_security'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
	}

	public function setCommercialLeaseDetailsId( $intCommercialLeaseDetailsId ) {
		$this->m_intCommercialLeaseDetailsId = CStrings::strToIntDef( $intCommercialLeaseDetailsId, NULL, false );
	}

	public function getCommercialLeaseDetailsId() {
		return $this->m_intCommercialLeaseDetailsId;
	}

	public function sqlCommercialLeaseDetailsId() {
		return ( true == isset( $this->m_intCommercialLeaseDetailsId ) ) ? (string) $this->m_intCommercialLeaseDetailsId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->m_intPropertyFloorId = CStrings::strToIntDef( $intPropertyFloorId, NULL, false );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? (string) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setSquareFootageShare( $fltSquareFootageShare ) {
		$this->m_fltSquareFootageShare = CStrings::strToFloatDef( $fltSquareFootageShare, NULL, false, 2 );
	}

	public function getSquareFootageShare() {
		return $this->m_fltSquareFootageShare;
	}

	public function sqlSquareFootageShare() {
		return ( true == isset( $this->m_fltSquareFootageShare ) ) ? (string) $this->m_fltSquareFootageShare : 'NULL';
	}

	public function setIsResponsibleUtilities( $boolIsResponsibleUtilities ) {
		$this->m_boolIsResponsibleUtilities = CStrings::strToBool( $boolIsResponsibleUtilities );
	}

	public function getIsResponsibleUtilities() {
		return $this->m_boolIsResponsibleUtilities;
	}

	public function sqlIsResponsibleUtilities() {
		return ( true == isset( $this->m_boolIsResponsibleUtilities ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleUtilities ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponsibleJanitorial( $boolIsResponsibleJanitorial ) {
		$this->m_boolIsResponsibleJanitorial = CStrings::strToBool( $boolIsResponsibleJanitorial );
	}

	public function getIsResponsibleJanitorial() {
		return $this->m_boolIsResponsibleJanitorial;
	}

	public function sqlIsResponsibleJanitorial() {
		return ( true == isset( $this->m_boolIsResponsibleJanitorial ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleJanitorial ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponsibleSecurity( $boolIsResponsibleSecurity ) {
		$this->m_boolIsResponsibleSecurity = CStrings::strToBool( $boolIsResponsibleSecurity );
	}

	public function getIsResponsibleSecurity() {
		return $this->m_boolIsResponsibleSecurity;
	}

	public function sqlIsResponsibleSecurity() {
		return ( true == isset( $this->m_boolIsResponsibleSecurity ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponsibleSecurity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.commercial_floor_shares_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.commercial_floor_shares
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCommercialLeaseDetailsId() . ', ' .
 						$this->sqlPropertyFloorId() . ', ' .
 						$this->sqlSquareFootageShare() . ', ' .
 						$this->sqlIsResponsibleUtilities() . ', ' .
 						$this->sqlIsResponsibleJanitorial() . ', ' .
 						$this->sqlIsResponsibleSecurity() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.commercial_floor_shares
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_lease_details_id = ' . $this->sqlCommercialLeaseDetailsId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCommercialLeaseDetailsId() ) != $this->getOriginalValueByFieldName ( 'commercial_lease_details_id' ) ) { $arrstrOriginalValueChanges['commercial_lease_details_id'] = $this->sqlCommercialLeaseDetailsId(); $strSql .= ' commercial_lease_details_id = ' . $this->sqlCommercialLeaseDetailsId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyFloorId() ) != $this->getOriginalValueByFieldName ( 'property_floor_id' ) ) { $arrstrOriginalValueChanges['property_floor_id'] = $this->sqlPropertyFloorId(); $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_footage_share = ' . $this->sqlSquareFootageShare() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlSquareFootageShare() ), $this->getOriginalValueByFieldName ( 'square_footage_share' ), 2 ) ) ) { $arrstrOriginalValueChanges['square_footage_share'] = $this->sqlSquareFootageShare(); $strSql .= ' square_footage_share = ' . $this->sqlSquareFootageShare() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_utilities = ' . $this->sqlIsResponsibleUtilities() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsResponsibleUtilities() ) != $this->getOriginalValueByFieldName ( 'is_responsible_utilities' ) ) { $arrstrOriginalValueChanges['is_responsible_utilities'] = $this->sqlIsResponsibleUtilities(); $strSql .= ' is_responsible_utilities = ' . $this->sqlIsResponsibleUtilities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_janitorial = ' . $this->sqlIsResponsibleJanitorial() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsResponsibleJanitorial() ) != $this->getOriginalValueByFieldName ( 'is_responsible_janitorial' ) ) { $arrstrOriginalValueChanges['is_responsible_janitorial'] = $this->sqlIsResponsibleJanitorial(); $strSql .= ' is_responsible_janitorial = ' . $this->sqlIsResponsibleJanitorial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsible_security = ' . $this->sqlIsResponsibleSecurity() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsResponsibleSecurity() ) != $this->getOriginalValueByFieldName ( 'is_responsible_security' ) ) { $arrstrOriginalValueChanges['is_responsible_security'] = $this->sqlIsResponsibleSecurity(); $strSql .= ' is_responsible_security = ' . $this->sqlIsResponsibleSecurity() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.commercial_floor_shares WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.commercial_floor_shares_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'commercial_lease_details_id' => $this->getCommercialLeaseDetailsId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'square_footage_share' => $this->getSquareFootageShare(),
			'is_responsible_utilities' => $this->getIsResponsibleUtilities(),
			'is_responsible_janitorial' => $this->getIsResponsibleJanitorial(),
			'is_responsible_security' => $this->getIsResponsibleSecurity(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>