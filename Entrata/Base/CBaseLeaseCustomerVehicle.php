<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCustomerVehicle extends CEosSingularBase {

	const TABLE_NAME = 'public.lease_customer_vehicles';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intCustomerVehicleTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strMake;
	protected $m_strModel;
	protected $m_intYear;
	protected $m_strColor;
	protected $m_strPermitNumber;
	protected $m_strNotes;
	protected $m_strLicensePlateNumber;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCountryCode;

	public function __construct() {
		parent::__construct();

		$this->m_strRemotePrimaryKey = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_vehicle_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerVehicleTypeId', trim( $arrValues['customer_vehicle_type_id'] ) ); elseif( isset( $arrValues['customer_vehicle_type_id'] ) ) $this->setCustomerVehicleTypeId( $arrValues['customer_vehicle_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['make'] ) && $boolDirectSet ) $this->set( 'm_strMake', trim( stripcslashes( $arrValues['make'] ) ) ); elseif( isset( $arrValues['make'] ) ) $this->setMake( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['make'] ) : $arrValues['make'] );
		if( isset( $arrValues['model'] ) && $boolDirectSet ) $this->set( 'm_strModel', trim( stripcslashes( $arrValues['model'] ) ) ); elseif( isset( $arrValues['model'] ) ) $this->setModel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['model'] ) : $arrValues['model'] );
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->set( 'm_intYear', trim( $arrValues['year'] ) ); elseif( isset( $arrValues['year'] ) ) $this->setYear( $arrValues['year'] );
		if( isset( $arrValues['color'] ) && $boolDirectSet ) $this->set( 'm_strColor', trim( stripcslashes( $arrValues['color'] ) ) ); elseif( isset( $arrValues['color'] ) ) $this->setColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['color'] ) : $arrValues['color'] );
		if( isset( $arrValues['permit_number'] ) && $boolDirectSet ) $this->set( 'm_strPermitNumber', trim( stripcslashes( $arrValues['permit_number'] ) ) ); elseif( isset( $arrValues['permit_number'] ) ) $this->setPermitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['permit_number'] ) : $arrValues['permit_number'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['license_plate_number'] ) && $boolDirectSet ) $this->set( 'm_strLicensePlateNumber', trim( stripcslashes( $arrValues['license_plate_number'] ) ) ); elseif( isset( $arrValues['license_plate_number'] ) ) $this->setLicensePlateNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['license_plate_number'] ) : $arrValues['license_plate_number'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerVehicleTypeId( $intCustomerVehicleTypeId ) {
		$this->set( 'm_intCustomerVehicleTypeId', CStrings::strToIntDef( $intCustomerVehicleTypeId, NULL, false ) );
	}

	public function getCustomerVehicleTypeId() {
		return $this->m_intCustomerVehicleTypeId;
	}

	public function sqlCustomerVehicleTypeId() {
		return ( true == isset( $this->m_intCustomerVehicleTypeId ) ) ? ( string ) $this->m_intCustomerVehicleTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : '\'NULL\'';
	}

	public function setMake( $strMake ) {
		$this->set( 'm_strMake', CStrings::strTrimDef( $strMake, 50, NULL, true ) );
	}

	public function getMake() {
		return $this->m_strMake;
	}

	public function sqlMake() {
		return ( true == isset( $this->m_strMake ) ) ? '\'' . addslashes( $this->m_strMake ) . '\'' : 'NULL';
	}

	public function setModel( $strModel ) {
		$this->set( 'm_strModel', CStrings::strTrimDef( $strModel, 50, NULL, true ) );
	}

	public function getModel() {
		return $this->m_strModel;
	}

	public function sqlModel() {
		return ( true == isset( $this->m_strModel ) ) ? '\'' . addslashes( $this->m_strModel ) . '\'' : 'NULL';
	}

	public function setYear( $intYear ) {
		$this->set( 'm_intYear', CStrings::strToIntDef( $intYear, NULL, false ) );
	}

	public function getYear() {
		return $this->m_intYear;
	}

	public function sqlYear() {
		return ( true == isset( $this->m_intYear ) ) ? ( string ) $this->m_intYear : 'NULL';
	}

	public function setColor( $strColor ) {
		$this->set( 'm_strColor', CStrings::strTrimDef( $strColor, 20, NULL, true ) );
	}

	public function getColor() {
		return $this->m_strColor;
	}

	public function sqlColor() {
		return ( true == isset( $this->m_strColor ) ) ? '\'' . addslashes( $this->m_strColor ) . '\'' : 'NULL';
	}

	public function setPermitNumber( $strPermitNumber ) {
		$this->set( 'm_strPermitNumber', CStrings::strTrimDef( $strPermitNumber, 100, NULL, true ) );
	}

	public function getPermitNumber() {
		return $this->m_strPermitNumber;
	}

	public function sqlPermitNumber() {
		return ( true == isset( $this->m_strPermitNumber ) ) ? '\'' . addslashes( $this->m_strPermitNumber ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setLicensePlateNumber( $strLicensePlateNumber ) {
		$this->set( 'm_strLicensePlateNumber', CStrings::strTrimDef( $strLicensePlateNumber, 20, NULL, true ) );
	}

	public function getLicensePlateNumber() {
		return $this->m_strLicensePlateNumber;
	}

	public function sqlLicensePlateNumber() {
		return ( true == isset( $this->m_strLicensePlateNumber ) ) ? '\'' . addslashes( $this->m_strLicensePlateNumber ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 250, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, customer_id, customer_vehicle_type_id, remote_primary_key, make, model, year, color, permit_number, notes, license_plate_number, state_code, province, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, country_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerVehicleTypeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlMake() . ', ' .
						$this->sqlModel() . ', ' .
						$this->sqlYear() . ', ' .
						$this->sqlColor() . ', ' .
						$this->sqlPermitNumber() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlLicensePlateNumber() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCountryCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_vehicle_type_id = ' . $this->sqlCustomerVehicleTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerVehicleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_vehicle_type_id = ' . $this->sqlCustomerVehicleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' make = ' . $this->sqlMake(). ',' ; } elseif( true == array_key_exists( 'Make', $this->getChangedColumns() ) ) { $strSql .= ' make = ' . $this->sqlMake() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' model = ' . $this->sqlModel(). ',' ; } elseif( true == array_key_exists( 'Model', $this->getChangedColumns() ) ) { $strSql .= ' model = ' . $this->sqlModel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year = ' . $this->sqlYear(). ',' ; } elseif( true == array_key_exists( 'Year', $this->getChangedColumns() ) ) { $strSql .= ' year = ' . $this->sqlYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' color = ' . $this->sqlColor(). ',' ; } elseif( true == array_key_exists( 'Color', $this->getChangedColumns() ) ) { $strSql .= ' color = ' . $this->sqlColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permit_number = ' . $this->sqlPermitNumber(). ',' ; } elseif( true == array_key_exists( 'PermitNumber', $this->getChangedColumns() ) ) { $strSql .= ' permit_number = ' . $this->sqlPermitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' license_plate_number = ' . $this->sqlLicensePlateNumber(). ',' ; } elseif( true == array_key_exists( 'LicensePlateNumber', $this->getChangedColumns() ) ) { $strSql .= ' license_plate_number = ' . $this->sqlLicensePlateNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'customer_vehicle_type_id' => $this->getCustomerVehicleTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'make' => $this->getMake(),
			'model' => $this->getModel(),
			'year' => $this->getYear(),
			'color' => $this->getColor(),
			'permit_number' => $this->getPermitNumber(),
			'notes' => $this->getNotes(),
			'license_plate_number' => $this->getLicensePlateNumber(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'country_code' => $this->getCountryCode()
		);
	}

}
?>