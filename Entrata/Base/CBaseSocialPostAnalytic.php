<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSocialPostAnalytic extends CEosSingularBase {

	const TABLE_NAME = 'public.social_post_analytics';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSocialPostId;
	protected $m_intLikesCount;
	protected $m_intCommentCount;
	protected $m_intShareCount;
	protected $m_intViewsCount;
	protected $m_intClicksCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['social_post_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostId', trim( $arrValues['social_post_id'] ) ); elseif( isset( $arrValues['social_post_id'] ) ) $this->setSocialPostId( $arrValues['social_post_id'] );
		if( isset( $arrValues['likes_count'] ) && $boolDirectSet ) $this->set( 'm_intLikesCount', trim( $arrValues['likes_count'] ) ); elseif( isset( $arrValues['likes_count'] ) ) $this->setLikesCount( $arrValues['likes_count'] );
		if( isset( $arrValues['comment_count'] ) && $boolDirectSet ) $this->set( 'm_intCommentCount', trim( $arrValues['comment_count'] ) ); elseif( isset( $arrValues['comment_count'] ) ) $this->setCommentCount( $arrValues['comment_count'] );
		if( isset( $arrValues['share_count'] ) && $boolDirectSet ) $this->set( 'm_intShareCount', trim( $arrValues['share_count'] ) ); elseif( isset( $arrValues['share_count'] ) ) $this->setShareCount( $arrValues['share_count'] );
		if( isset( $arrValues['views_count'] ) && $boolDirectSet ) $this->set( 'm_intViewsCount', trim( $arrValues['views_count'] ) ); elseif( isset( $arrValues['views_count'] ) ) $this->setViewsCount( $arrValues['views_count'] );
		if( isset( $arrValues['clicks_count'] ) && $boolDirectSet ) $this->set( 'm_intClicksCount', trim( $arrValues['clicks_count'] ) ); elseif( isset( $arrValues['clicks_count'] ) ) $this->setClicksCount( $arrValues['clicks_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSocialPostId( $intSocialPostId ) {
		$this->set( 'm_intSocialPostId', CStrings::strToIntDef( $intSocialPostId, NULL, false ) );
	}

	public function getSocialPostId() {
		return $this->m_intSocialPostId;
	}

	public function sqlSocialPostId() {
		return ( true == isset( $this->m_intSocialPostId ) ) ? ( string ) $this->m_intSocialPostId : 'NULL';
	}

	public function setLikesCount( $intLikesCount ) {
		$this->set( 'm_intLikesCount', CStrings::strToIntDef( $intLikesCount, NULL, false ) );
	}

	public function getLikesCount() {
		return $this->m_intLikesCount;
	}

	public function sqlLikesCount() {
		return ( true == isset( $this->m_intLikesCount ) ) ? ( string ) $this->m_intLikesCount : 'NULL';
	}

	public function setCommentCount( $intCommentCount ) {
		$this->set( 'm_intCommentCount', CStrings::strToIntDef( $intCommentCount, NULL, false ) );
	}

	public function getCommentCount() {
		return $this->m_intCommentCount;
	}

	public function sqlCommentCount() {
		return ( true == isset( $this->m_intCommentCount ) ) ? ( string ) $this->m_intCommentCount : 'NULL';
	}

	public function setShareCount( $intShareCount ) {
		$this->set( 'm_intShareCount', CStrings::strToIntDef( $intShareCount, NULL, false ) );
	}

	public function getShareCount() {
		return $this->m_intShareCount;
	}

	public function sqlShareCount() {
		return ( true == isset( $this->m_intShareCount ) ) ? ( string ) $this->m_intShareCount : 'NULL';
	}

	public function setViewsCount( $intViewsCount ) {
		$this->set( 'm_intViewsCount', CStrings::strToIntDef( $intViewsCount, NULL, false ) );
	}

	public function getViewsCount() {
		return $this->m_intViewsCount;
	}

	public function sqlViewsCount() {
		return ( true == isset( $this->m_intViewsCount ) ) ? ( string ) $this->m_intViewsCount : 'NULL';
	}

	public function setClicksCount( $intClicksCount ) {
		$this->set( 'm_intClicksCount', CStrings::strToIntDef( $intClicksCount, NULL, false ) );
	}

	public function getClicksCount() {
		return $this->m_intClicksCount;
	}

	public function sqlClicksCount() {
		return ( true == isset( $this->m_intClicksCount ) ) ? ( string ) $this->m_intClicksCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, social_post_id, likes_count, comment_count, share_count, views_count, clicks_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSocialPostId() . ', ' .
 						$this->sqlLikesCount() . ', ' .
 						$this->sqlCommentCount() . ', ' .
 						$this->sqlShareCount() . ', ' .
 						$this->sqlViewsCount() . ', ' .
 						$this->sqlClicksCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId() . ','; } elseif( true == array_key_exists( 'SocialPostId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' likes_count = ' . $this->sqlLikesCount() . ','; } elseif( true == array_key_exists( 'LikesCount', $this->getChangedColumns() ) ) { $strSql .= ' likes_count = ' . $this->sqlLikesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comment_count = ' . $this->sqlCommentCount() . ','; } elseif( true == array_key_exists( 'CommentCount', $this->getChangedColumns() ) ) { $strSql .= ' comment_count = ' . $this->sqlCommentCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' share_count = ' . $this->sqlShareCount() . ','; } elseif( true == array_key_exists( 'ShareCount', $this->getChangedColumns() ) ) { $strSql .= ' share_count = ' . $this->sqlShareCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' views_count = ' . $this->sqlViewsCount() . ','; } elseif( true == array_key_exists( 'ViewsCount', $this->getChangedColumns() ) ) { $strSql .= ' views_count = ' . $this->sqlViewsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clicks_count = ' . $this->sqlClicksCount() . ','; } elseif( true == array_key_exists( 'ClicksCount', $this->getChangedColumns() ) ) { $strSql .= ' clicks_count = ' . $this->sqlClicksCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'social_post_id' => $this->getSocialPostId(),
			'likes_count' => $this->getLikesCount(),
			'comment_count' => $this->getCommentCount(),
			'share_count' => $this->getShareCount(),
			'views_count' => $this->getViewsCount(),
			'clicks_count' => $this->getClicksCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>