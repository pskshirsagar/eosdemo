<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClientUserDefinedFields
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClientUserDefinedFields extends CEosPluralBase {

	/**
	 * @return CClientUserDefinedField[]
	 */
	public static function fetchClientUserDefinedFields( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CClientUserDefinedField::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CClientUserDefinedField
	 */
	public static function fetchClientUserDefinedField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClientUserDefinedField::class, $objDatabase );
	}

	public static function fetchClientUserDefinedFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_user_defined_fields', $objDatabase );
	}

	public static function fetchClientUserDefinedFieldByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchClientUserDefinedField( sprintf( 'SELECT * FROM client_user_defined_fields WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchClientUserDefinedFieldsByCid( $intCid, $objDatabase ) {
		return self::fetchClientUserDefinedFields( sprintf( 'SELECT * FROM client_user_defined_fields WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchClientUserDefinedFieldsByUdfTableIdByCid( $intUdfTableId, $intCid, $objDatabase ) {
		return self::fetchClientUserDefinedFields( sprintf( 'SELECT * FROM client_user_defined_fields WHERE udf_table_id = %d AND cid = %d', $intUdfTableId, $intCid ), $objDatabase );
	}

}
?>