<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCheckComponents
 * Do not add any new functions to this class.
 */

class CBaseDefaultCheckComponents extends CEosPluralBase {

	/**
	 * @return CDefaultCheckComponent[]
	 */
	public static function fetchDefaultCheckComponents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultCheckComponent', $objDatabase );
	}

	/**
	 * @return CDefaultCheckComponent
	 */
	public static function fetchDefaultCheckComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultCheckComponent', $objDatabase );
	}

	public static function fetchDefaultCheckComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_check_components', $objDatabase );
	}

	public static function fetchDefaultCheckComponentById( $intId, $objDatabase ) {
		return self::fetchDefaultCheckComponent( sprintf( 'SELECT * FROM default_check_components WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultCheckComponentsByDefaultCheckTemplateId( $intDefaultCheckTemplateId, $objDatabase ) {
		return self::fetchDefaultCheckComponents( sprintf( 'SELECT * FROM default_check_components WHERE default_check_template_id = %d', ( int ) $intDefaultCheckTemplateId ), $objDatabase );
	}

	public static function fetchDefaultCheckComponentsByCheckComponentTypeId( $intCheckComponentTypeId, $objDatabase ) {
		return self::fetchDefaultCheckComponents( sprintf( 'SELECT * FROM default_check_components WHERE check_component_type_id = %d', ( int ) $intCheckComponentTypeId ), $objDatabase );
	}

}
?>