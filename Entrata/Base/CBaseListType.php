<?php

class CBaseListType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.list_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_boolHasWebsiteDisplayOption;
	protected $m_boolHasEntrataDisplayOption;
	protected $m_boolRequireDefaultItem;
	protected $m_boolIsLeaseViolation;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasWebsiteDisplayOption = true;
		$this->m_boolHasEntrataDisplayOption = true;
		$this->m_boolRequireDefaultItem = true;
		$this->m_boolIsLeaseViolation = false;
		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['has_website_display_option'] ) && $boolDirectSet ) $this->set( 'm_boolHasWebsiteDisplayOption', trim( stripcslashes( $arrValues['has_website_display_option'] ) ) ); elseif( isset( $arrValues['has_website_display_option'] ) ) $this->setHasWebsiteDisplayOption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_website_display_option'] ) : $arrValues['has_website_display_option'] );
		if( isset( $arrValues['has_entrata_display_option'] ) && $boolDirectSet ) $this->set( 'm_boolHasEntrataDisplayOption', trim( stripcslashes( $arrValues['has_entrata_display_option'] ) ) ); elseif( isset( $arrValues['has_entrata_display_option'] ) ) $this->setHasEntrataDisplayOption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_entrata_display_option'] ) : $arrValues['has_entrata_display_option'] );
		if( isset( $arrValues['require_default_item'] ) && $boolDirectSet ) $this->set( 'm_boolRequireDefaultItem', trim( stripcslashes( $arrValues['require_default_item'] ) ) ); elseif( isset( $arrValues['require_default_item'] ) ) $this->setRequireDefaultItem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_default_item'] ) : $arrValues['require_default_item'] );
		if( isset( $arrValues['is_lease_violation'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeaseViolation', trim( stripcslashes( $arrValues['is_lease_violation'] ) ) ); elseif( isset( $arrValues['is_lease_violation'] ) ) $this->setIsLeaseViolation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_violation'] ) : $arrValues['is_lease_violation'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setHasWebsiteDisplayOption( $boolHasWebsiteDisplayOption ) {
		$this->set( 'm_boolHasWebsiteDisplayOption', CStrings::strToBool( $boolHasWebsiteDisplayOption ) );
	}

	public function getHasWebsiteDisplayOption() {
		return $this->m_boolHasWebsiteDisplayOption;
	}

	public function sqlHasWebsiteDisplayOption() {
		return ( true == isset( $this->m_boolHasWebsiteDisplayOption ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasWebsiteDisplayOption ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasEntrataDisplayOption( $boolHasEntrataDisplayOption ) {
		$this->set( 'm_boolHasEntrataDisplayOption', CStrings::strToBool( $boolHasEntrataDisplayOption ) );
	}

	public function getHasEntrataDisplayOption() {
		return $this->m_boolHasEntrataDisplayOption;
	}

	public function sqlHasEntrataDisplayOption() {
		return ( true == isset( $this->m_boolHasEntrataDisplayOption ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasEntrataDisplayOption ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireDefaultItem( $boolRequireDefaultItem ) {
		$this->set( 'm_boolRequireDefaultItem', CStrings::strToBool( $boolRequireDefaultItem ) );
	}

	public function getRequireDefaultItem() {
		return $this->m_boolRequireDefaultItem;
	}

	public function sqlRequireDefaultItem() {
		return ( true == isset( $this->m_boolRequireDefaultItem ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireDefaultItem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLeaseViolation( $boolIsLeaseViolation ) {
		$this->set( 'm_boolIsLeaseViolation', CStrings::strToBool( $boolIsLeaseViolation ) );
	}

	public function getIsLeaseViolation() {
		return $this->m_boolIsLeaseViolation;
	}

	public function sqlIsLeaseViolation() {
		return ( true == isset( $this->m_boolIsLeaseViolation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeaseViolation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'has_website_display_option' => $this->getHasWebsiteDisplayOption(),
			'has_entrata_display_option' => $this->getHasEntrataDisplayOption(),
			'require_default_item' => $this->getRequireDefaultItem(),
			'is_lease_violation' => $this->getIsLeaseViolation(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>