<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPermissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserPermissions extends CEosPluralBase {

	/**
	 * @return CCompanyUserPermission[]
	 */
	public static function fetchCompanyUserPermissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserPermission', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserPermission
	 */
	public static function fetchCompanyUserPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserPermission', $objDatabase );
	}

	public static function fetchCompanyUserPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_permissions', $objDatabase );
	}

	public static function fetchCompanyUserPermissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPermission( sprintf( 'SELECT * FROM company_user_permissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserPermissions( sprintf( 'SELECT * FROM company_user_permissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPermissions( sprintf( 'SELECT * FROM company_user_permissions WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPermissions( sprintf( 'SELECT * FROM company_user_permissions WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>