<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyHours
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyHours extends CEosPluralBase {

	/**
	 * @return CPropertyHour[]
	 */
	public static function fetchPropertyHours( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyHour::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyHour
	 */
	public static function fetchPropertyHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyHour::class, $objDatabase );
	}

	public static function fetchPropertyHourCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_hours', $objDatabase );
	}

	public static function fetchPropertyHourByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyHour( sprintf( 'SELECT * FROM property_hours WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyHoursByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyHoursByPropertyHourTypeIdByCid( $intPropertyHourTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE property_hour_type_id = %d AND cid = %d', $intPropertyHourTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyHoursByPropertyHolidayIdByCid( $intPropertyHolidayId, $intCid, $objDatabase ) {
		return self::fetchPropertyHours( sprintf( 'SELECT * FROM property_hours WHERE property_holiday_id = %d AND cid = %d', $intPropertyHolidayId, $intCid ), $objDatabase );
	}

}
?>