<?php

class CBaseApplicationStageStatus extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.application_stage_statuses';

	protected $m_intId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intNextApplicationStageId;
	protected $m_intNextApplicationStatusId;
	protected $m_strStageName;
	protected $m_strStatusName;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageId', trim( $arrValues['application_stage_id'] ) ); elseif( isset( $arrValues['application_stage_id'] ) ) $this->setApplicationStageId( $arrValues['application_stage_id'] );
		if( isset( $arrValues['application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStatusId', trim( $arrValues['application_status_id'] ) ); elseif( isset( $arrValues['application_status_id'] ) ) $this->setApplicationStatusId( $arrValues['application_status_id'] );
		if( isset( $arrValues['next_application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intNextApplicationStageId', trim( $arrValues['next_application_stage_id'] ) ); elseif( isset( $arrValues['next_application_stage_id'] ) ) $this->setNextApplicationStageId( $arrValues['next_application_stage_id'] );
		if( isset( $arrValues['next_application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intNextApplicationStatusId', trim( $arrValues['next_application_status_id'] ) ); elseif( isset( $arrValues['next_application_status_id'] ) ) $this->setNextApplicationStatusId( $arrValues['next_application_status_id'] );
		if( isset( $arrValues['stage_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStageName', trim( stripcslashes( $arrValues['stage_name'] ) ) ); elseif( isset( $arrValues['stage_name'] ) ) $this->setStageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['stage_name'] ) : $arrValues['stage_name'] );
		if( isset( $arrValues['status_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStatusName', trim( stripcslashes( $arrValues['status_name'] ) ) ); elseif( isset( $arrValues['status_name'] ) ) $this->setStatusName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_name'] ) : $arrValues['status_name'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->set( 'm_intApplicationStageId', CStrings::strToIntDef( $intApplicationStageId, NULL, false ) );
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function sqlApplicationStageId() {
		return ( true == isset( $this->m_intApplicationStageId ) ) ? ( string ) $this->m_intApplicationStageId : 'NULL';
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->set( 'm_intApplicationStatusId', CStrings::strToIntDef( $intApplicationStatusId, NULL, false ) );
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function sqlApplicationStatusId() {
		return ( true == isset( $this->m_intApplicationStatusId ) ) ? ( string ) $this->m_intApplicationStatusId : 'NULL';
	}

	public function setNextApplicationStageId( $intNextApplicationStageId ) {
		$this->set( 'm_intNextApplicationStageId', CStrings::strToIntDef( $intNextApplicationStageId, NULL, false ) );
	}

	public function getNextApplicationStageId() {
		return $this->m_intNextApplicationStageId;
	}

	public function sqlNextApplicationStageId() {
		return ( true == isset( $this->m_intNextApplicationStageId ) ) ? ( string ) $this->m_intNextApplicationStageId : 'NULL';
	}

	public function setNextApplicationStatusId( $intNextApplicationStatusId ) {
		$this->set( 'm_intNextApplicationStatusId', CStrings::strToIntDef( $intNextApplicationStatusId, NULL, false ) );
	}

	public function getNextApplicationStatusId() {
		return $this->m_intNextApplicationStatusId;
	}

	public function sqlNextApplicationStatusId() {
		return ( true == isset( $this->m_intNextApplicationStatusId ) ) ? ( string ) $this->m_intNextApplicationStatusId : 'NULL';
	}

	public function setStageName( $strStageName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strStageName', CStrings::strTrimDef( $strStageName, 50, NULL, true ), $strLocaleCode );
	}

	public function getStageName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strStageName', $strLocaleCode );
	}

	public function sqlStageName() {
		return ( true == isset( $this->m_strStageName ) ) ? '\'' . addslashes( $this->m_strStageName ) . '\'' : 'NULL';
	}

	public function setStatusName( $strStatusName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strStatusName', CStrings::strTrimDef( $strStatusName, 50, NULL, true ), $strLocaleCode );
	}

	public function getStatusName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strStatusName', $strLocaleCode );
	}

	public function sqlStatusName() {
		return ( true == isset( $this->m_strStatusName ) ) ? '\'' . addslashes( $this->m_strStatusName ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'application_stage_id' => $this->getApplicationStageId(),
			'application_status_id' => $this->getApplicationStatusId(),
			'next_application_stage_id' => $this->getNextApplicationStageId(),
			'next_application_status_id' => $this->getNextApplicationStatusId(),
			'stage_name' => $this->getStageName(),
			'status_name' => $this->getStatusName(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>