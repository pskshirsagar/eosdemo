<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidySpecialClaimAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidySpecialClaimAssociations extends CEosPluralBase {

	/**
	 * @return CSubsidySpecialClaimAssociation[]
	 */
	public static function fetchSubsidySpecialClaimAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidySpecialClaimAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidySpecialClaimAssociation
	 */
	public static function fetchSubsidySpecialClaimAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidySpecialClaimAssociation', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_special_claim_associations', $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociation( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociations( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociations( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationsBySubsidySpecialClaimIdByCid( $intSubsidySpecialClaimId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociations( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE subsidy_special_claim_id = %d AND cid = %d', ( int ) $intSubsidySpecialClaimId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociations( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidySpecialClaimAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchSubsidySpecialClaimAssociations( sprintf( 'SELECT * FROM subsidy_special_claim_associations WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>