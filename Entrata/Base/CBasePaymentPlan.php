<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePaymentPlan extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPaymentArCodeId;
    protected $m_intSpecialId;
    protected $m_strName;
    protected $m_strDescription;
    protected $m_strNotes;
    protected $m_intIsPublished;
    protected $m_intOrderNum;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intIsPublished = '1';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['payment_ar_code_id'] ) && $boolDirectSet ) $this->m_intPaymentArCodeId = trim( $arrValues['payment_ar_code_id'] ); else if( isset( $arrValues['payment_ar_code_id'] ) ) $this->setPaymentArCodeId( $arrValues['payment_ar_code_id'] );
        if( isset( $arrValues['special_id'] ) && $boolDirectSet ) $this->m_intSpecialId = trim( $arrValues['special_id'] ); else if( isset( $arrValues['special_id'] ) ) $this->setSpecialId( $arrValues['special_id'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->m_strNotes = trim( stripcslashes( $arrValues['notes'] ) ); else if( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPaymentArCodeId( $intPaymentArCodeId ) {
        $this->m_intPaymentArCodeId = CStrings::strToIntDef( $intPaymentArCodeId, NULL, false );
    }

    public function getPaymentArCodeId() {
        return $this->m_intPaymentArCodeId;
    }

    public function sqlPaymentArCodeId() {
        return ( true == isset( $this->m_intPaymentArCodeId ) ) ? (string) $this->m_intPaymentArCodeId : 'NULL';
    }

    public function setSpecialId( $intSpecialId ) {
        $this->m_intSpecialId = CStrings::strToIntDef( $intSpecialId, NULL, false );
    }

    public function getSpecialId() {
        return $this->m_intSpecialId;
    }

    public function sqlSpecialId() {
        return ( true == isset( $this->m_intSpecialId ) ) ? (string) $this->m_intSpecialId : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 60, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setNotes( $strNotes ) {
        $this->m_strNotes = CStrings::strTrimDef( $strNotes, 2000, NULL, true );
    }

    public function getNotes() {
        return $this->m_strNotes;
    }

    public function sqlNotes() {
        return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return ( true == isset( $this->m_intDeletedBy ) ) ? (string) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.payment_plans_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.payment_plans( id, cid, payment_ar_code_id, special_id, name, description, notes, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPaymentArCodeId() . ', ' . 		                $this->sqlSpecialId() . ', ' . 		                $this->sqlName() . ', ' . 		                $this->sqlDescription() . ', ' . 		                $this->sqlNotes() . ', ' . 		                $this->sqlIsPublished() . ', ' . 		                $this->sqlOrderNum() . ', ' . 		                $this->sqlDeletedBy() . ', ' . 		                $this->sqlDeletedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.payment_plans
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ar_code_id = ' . $this->sqlPaymentArCodeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPaymentArCodeId() ) != $this->getOriginalValueByFieldName ( 'payment_ar_code_id' ) ) { $arrstrOriginalValueChanges['payment_ar_code_id'] = $this->sqlPaymentArCodeId(); $strSql .= ' payment_ar_code_id = ' . $this->sqlPaymentArCodeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_id = ' . $this->sqlSpecialId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSpecialId() ) != $this->getOriginalValueByFieldName ( 'special_id' ) ) { $arrstrOriginalValueChanges['special_id'] = $this->sqlSpecialId(); $strSql .= ' special_id = ' . $this->sqlSpecialId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName ( 'description' ) ) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNotes() ) != $this->getOriginalValueByFieldName ( 'notes' ) ) { $arrstrOriginalValueChanges['notes'] = $this->sqlNotes(); $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName ( 'order_num' ) ) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName ( 'deleted_by' ) ) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName ( 'deleted_on' ) ) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.payment_plans WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.payment_plans_id_seq', $objDatabase );
    }

}
?>