<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInstallmentPlans
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInstallmentPlans extends CEosPluralBase {

	/**
	 * @return CInstallmentPlan[]
	 */
	public static function fetchInstallmentPlans( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInstallmentPlan::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInstallmentPlan
	 */
	public static function fetchInstallmentPlan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInstallmentPlan::class, $objDatabase );
	}

	public static function fetchInstallmentPlanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'installment_plans', $objDatabase );
	}

	public static function fetchInstallmentPlanByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlan( sprintf( 'SELECT * FROM installment_plans WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByCid( $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByPaymentArCodeIdByCid( $intPaymentArCodeId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE payment_ar_code_id = %d AND cid = %d', $intPaymentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansBySpecialIdByCid( $intSpecialId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE special_id = %d AND cid = %d', $intSpecialId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentPlansByLedgerFilterIdByCid( $intLedgerFilterId, $intCid, $objDatabase ) {
		return self::fetchInstallmentPlans( sprintf( 'SELECT * FROM installment_plans WHERE ledger_filter_id = %d AND cid = %d', $intLedgerFilterId, $intCid ), $objDatabase );
	}

}
?>