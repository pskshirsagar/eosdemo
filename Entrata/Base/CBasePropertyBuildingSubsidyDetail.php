<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBuildingSubsidyDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.property_building_subsidy_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_strBuildingIdentificationNumber;
	protected $m_fltFirstYearLevelPercent;
	protected $m_strCreditStartDate;
	protected $m_strComplianceStartDate;
	protected $m_strExtendedUseStartDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltFirstYearLevelPercent = '0.00';
		$this->m_strCreditStartDate = '01/01/1970';
		$this->m_strComplianceStartDate = '01/01/1970';
		$this->m_strExtendedUseStartDate = '01/01/1970';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['building_identification_number'] ) && $boolDirectSet ) $this->set( 'm_strBuildingIdentificationNumber', trim( $arrValues['building_identification_number'] ) ); elseif( isset( $arrValues['building_identification_number'] ) ) $this->setBuildingIdentificationNumber( $arrValues['building_identification_number'] );
		if( isset( $arrValues['first_year_level_percent'] ) && $boolDirectSet ) $this->set( 'm_fltFirstYearLevelPercent', trim( $arrValues['first_year_level_percent'] ) ); elseif( isset( $arrValues['first_year_level_percent'] ) ) $this->setFirstYearLevelPercent( $arrValues['first_year_level_percent'] );
		if( isset( $arrValues['credit_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCreditStartDate', trim( $arrValues['credit_start_date'] ) ); elseif( isset( $arrValues['credit_start_date'] ) ) $this->setCreditStartDate( $arrValues['credit_start_date'] );
		if( isset( $arrValues['compliance_start_date'] ) && $boolDirectSet ) $this->set( 'm_strComplianceStartDate', trim( $arrValues['compliance_start_date'] ) ); elseif( isset( $arrValues['compliance_start_date'] ) ) $this->setComplianceStartDate( $arrValues['compliance_start_date'] );
		if( isset( $arrValues['extended_use_start_date'] ) && $boolDirectSet ) $this->set( 'm_strExtendedUseStartDate', trim( $arrValues['extended_use_start_date'] ) ); elseif( isset( $arrValues['extended_use_start_date'] ) ) $this->setExtendedUseStartDate( $arrValues['extended_use_start_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setBuildingIdentificationNumber( $strBuildingIdentificationNumber ) {
		$this->set( 'm_strBuildingIdentificationNumber', CStrings::strTrimDef( $strBuildingIdentificationNumber, 9, NULL, true ) );
	}

	public function getBuildingIdentificationNumber() {
		return $this->m_strBuildingIdentificationNumber;
	}

	public function sqlBuildingIdentificationNumber() {
		return ( true == isset( $this->m_strBuildingIdentificationNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingIdentificationNumber ) : '\'' . addslashes( $this->m_strBuildingIdentificationNumber ) . '\'' ) : 'NULL';
	}

	public function setFirstYearLevelPercent( $fltFirstYearLevelPercent ) {
		$this->set( 'm_fltFirstYearLevelPercent', CStrings::strToFloatDef( $fltFirstYearLevelPercent, NULL, false, 2 ) );
	}

	public function getFirstYearLevelPercent() {
		return $this->m_fltFirstYearLevelPercent;
	}

	public function sqlFirstYearLevelPercent() {
		return ( true == isset( $this->m_fltFirstYearLevelPercent ) ) ? ( string ) $this->m_fltFirstYearLevelPercent : '0.00';
	}

	public function setCreditStartDate( $strCreditStartDate ) {
		$this->set( 'm_strCreditStartDate', CStrings::strTrimDef( $strCreditStartDate, -1, NULL, true ) );
	}

	public function getCreditStartDate() {
		return $this->m_strCreditStartDate;
	}

	public function sqlCreditStartDate() {
		return ( true == isset( $this->m_strCreditStartDate ) ) ? '\'' . $this->m_strCreditStartDate . '\'' : 'NOW()';
	}

	public function setComplianceStartDate( $strComplianceStartDate ) {
		$this->set( 'm_strComplianceStartDate', CStrings::strTrimDef( $strComplianceStartDate, -1, NULL, true ) );
	}

	public function getComplianceStartDate() {
		return $this->m_strComplianceStartDate;
	}

	public function sqlComplianceStartDate() {
		return ( true == isset( $this->m_strComplianceStartDate ) ) ? '\'' . $this->m_strComplianceStartDate . '\'' : 'NOW()';
	}

	public function setExtendedUseStartDate( $strExtendedUseStartDate ) {
		$this->set( 'm_strExtendedUseStartDate', CStrings::strTrimDef( $strExtendedUseStartDate, -1, NULL, true ) );
	}

	public function getExtendedUseStartDate() {
		return $this->m_strExtendedUseStartDate;
	}

	public function sqlExtendedUseStartDate() {
		return ( true == isset( $this->m_strExtendedUseStartDate ) ) ? '\'' . $this->m_strExtendedUseStartDate . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_building_id, building_identification_number, first_year_level_percent, credit_start_date, compliance_start_date, extended_use_start_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlBuildingIdentificationNumber() . ', ' .
						$this->sqlFirstYearLevelPercent() . ', ' .
						$this->sqlCreditStartDate() . ', ' .
						$this->sqlComplianceStartDate() . ', ' .
						$this->sqlExtendedUseStartDate() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_identification_number = ' . $this->sqlBuildingIdentificationNumber(). ',' ; } elseif( true == array_key_exists( 'BuildingIdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' building_identification_number = ' . $this->sqlBuildingIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_year_level_percent = ' . $this->sqlFirstYearLevelPercent(). ',' ; } elseif( true == array_key_exists( 'FirstYearLevelPercent', $this->getChangedColumns() ) ) { $strSql .= ' first_year_level_percent = ' . $this->sqlFirstYearLevelPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_start_date = ' . $this->sqlCreditStartDate(). ',' ; } elseif( true == array_key_exists( 'CreditStartDate', $this->getChangedColumns() ) ) { $strSql .= ' credit_start_date = ' . $this->sqlCreditStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_start_date = ' . $this->sqlComplianceStartDate(). ',' ; } elseif( true == array_key_exists( 'ComplianceStartDate', $this->getChangedColumns() ) ) { $strSql .= ' compliance_start_date = ' . $this->sqlComplianceStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extended_use_start_date = ' . $this->sqlExtendedUseStartDate(). ',' ; } elseif( true == array_key_exists( 'ExtendedUseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' extended_use_start_date = ' . $this->sqlExtendedUseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'building_identification_number' => $this->getBuildingIdentificationNumber(),
			'first_year_level_percent' => $this->getFirstYearLevelPercent(),
			'credit_start_date' => $this->getCreditStartDate(),
			'compliance_start_date' => $this->getComplianceStartDate(),
			'extended_use_start_date' => $this->getExtendedUseStartDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>