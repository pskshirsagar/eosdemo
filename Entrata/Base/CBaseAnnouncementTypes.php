<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncementTypes
 * Do not add any new functions to this class.
 */

class CBaseAnnouncementTypes extends CEosPluralBase {

	/**
	 * @return CAnnouncementType[]
	 */
	public static function fetchAnnouncementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAnnouncementType::class, $objDatabase );
	}

	/**
	 * @return CAnnouncementType
	 */
	public static function fetchAnnouncementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAnnouncementType::class, $objDatabase );
	}

	public static function fetchAnnouncementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'announcement_types', $objDatabase );
	}

	public static function fetchAnnouncementTypeById( $intId, $objDatabase ) {
		return self::fetchAnnouncementType( sprintf( 'SELECT * FROM announcement_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>