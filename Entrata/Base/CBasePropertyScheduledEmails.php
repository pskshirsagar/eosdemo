<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyScheduledEmails extends CEosPluralBase {

	/**
	 * @return CPropertyScheduledEmail[]
	 */
	public static function fetchPropertyScheduledEmails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyScheduledEmail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyScheduledEmail
	 */
	public static function fetchPropertyScheduledEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyScheduledEmail', $objDatabase );
	}

	public static function fetchPropertyScheduledEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_scheduled_emails', $objDatabase );
	}

	public static function fetchPropertyScheduledEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledEmail( sprintf( 'SELECT * FROM property_scheduled_emails WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledEmails( sprintf( 'SELECT * FROM property_scheduled_emails WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledEmailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledEmails( sprintf( 'SELECT * FROM property_scheduled_emails WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledEmailsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledEmails( sprintf( 'SELECT * FROM property_scheduled_emails WHERE scheduled_email_id = %d AND cid = %d', ( int ) $intScheduledEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScheduledEmailsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchPropertyScheduledEmails( sprintf( 'SELECT * FROM property_scheduled_emails WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

}
?>