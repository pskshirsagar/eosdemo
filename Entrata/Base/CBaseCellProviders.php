<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCellProviders
 * Do not add any new functions to this class.
 */

class CBaseCellProviders extends CEosPluralBase {

	/**
	 * @return CCellProvider[]
	 */
	public static function fetchCellProviders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCellProvider::class, $objDatabase );
	}

	/**
	 * @return CCellProvider
	 */
	public static function fetchCellProvider( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCellProvider::class, $objDatabase );
	}

	public static function fetchCellProviderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cell_providers', $objDatabase );
	}

	public static function fetchCellProviderById( $intId, $objDatabase ) {
		return self::fetchCellProvider( sprintf( 'SELECT * FROM cell_providers WHERE id = %d', $intId ), $objDatabase );
	}

}
?>