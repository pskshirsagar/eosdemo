<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessageTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyMessageTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyMessageType[]
	 */
	public static function fetchSubsidyMessageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyMessageType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyMessageType
	 */
	public static function fetchSubsidyMessageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyMessageType::class, $objDatabase );
	}

	public static function fetchSubsidyMessageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_message_types', $objDatabase );
	}

	public static function fetchSubsidyMessageTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyMessageType( sprintf( 'SELECT * FROM subsidy_message_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>