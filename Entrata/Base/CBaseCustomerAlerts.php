<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAlerts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAlerts extends CEosPluralBase {

	/**
	 * @return CCustomerAlert[]
	 */
	public static function fetchCustomerAlerts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerAlert::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAlert
	 */
	public static function fetchCustomerAlert( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAlert::class, $objDatabase );
	}

	public static function fetchCustomerAlertCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_alerts', $objDatabase );
	}

	public static function fetchCustomerAlertByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAlert( sprintf( 'SELECT * FROM customer_alerts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAlertsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAlerts( sprintf( 'SELECT * FROM customer_alerts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAlertsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerAlerts( sprintf( 'SELECT * FROM customer_alerts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAlertsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAlerts( sprintf( 'SELECT * FROM customer_alerts WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAlertsByCustomerAlertTypeIdByCid( $intCustomerAlertTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAlerts( sprintf( 'SELECT * FROM customer_alerts WHERE customer_alert_type_id = %d AND cid = %d', ( int ) $intCustomerAlertTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAlertsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchCustomerAlerts( sprintf( 'SELECT * FROM customer_alerts WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>