<?php

class CBaseAncillaryPlanPromoDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.ancillary_plan_promo_details';

	protected $m_intId;
	protected $m_intAncillaryVendorId;
	protected $m_strExternalPlanId;
	protected $m_strPlanName;
	protected $m_strPromoTitle;
	protected $m_strPromoDescription;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAncillaryVendorId = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ancillary_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intAncillaryVendorId', trim( $arrValues['ancillary_vendor_id'] ) ); elseif( isset( $arrValues['ancillary_vendor_id'] ) ) $this->setAncillaryVendorId( $arrValues['ancillary_vendor_id'] );
		if( isset( $arrValues['external_plan_id'] ) && $boolDirectSet ) $this->set( 'm_strExternalPlanId', trim( stripcslashes( $arrValues['external_plan_id'] ) ) ); elseif( isset( $arrValues['external_plan_id'] ) ) $this->setExternalPlanId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_plan_id'] ) : $arrValues['external_plan_id'] );
		if( isset( $arrValues['plan_name'] ) && $boolDirectSet ) $this->set( 'm_strPlanName', trim( stripcslashes( $arrValues['plan_name'] ) ) ); elseif( isset( $arrValues['plan_name'] ) ) $this->setPlanName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['plan_name'] ) : $arrValues['plan_name'] );
		if( isset( $arrValues['promo_title'] ) && $boolDirectSet ) $this->set( 'm_strPromoTitle', trim( stripcslashes( $arrValues['promo_title'] ) ) ); elseif( isset( $arrValues['promo_title'] ) ) $this->setPromoTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['promo_title'] ) : $arrValues['promo_title'] );
		if( isset( $arrValues['promo_description'] ) && $boolDirectSet ) $this->set( 'm_strPromoDescription', trim( stripcslashes( $arrValues['promo_description'] ) ) ); elseif( isset( $arrValues['promo_description'] ) ) $this->setPromoDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['promo_description'] ) : $arrValues['promo_description'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAncillaryVendorId( $intAncillaryVendorId ) {
		$this->set( 'm_intAncillaryVendorId', CStrings::strToIntDef( $intAncillaryVendorId, NULL, false ) );
	}

	public function getAncillaryVendorId() {
		return $this->m_intAncillaryVendorId;
	}

	public function sqlAncillaryVendorId() {
		return ( true == isset( $this->m_intAncillaryVendorId ) ) ? ( string ) $this->m_intAncillaryVendorId : '0';
	}

	public function setExternalPlanId( $strExternalPlanId ) {
		$this->set( 'm_strExternalPlanId', CStrings::strTrimDef( $strExternalPlanId, 50, NULL, true ) );
	}

	public function getExternalPlanId() {
		return $this->m_strExternalPlanId;
	}

	public function sqlExternalPlanId() {
		return ( true == isset( $this->m_strExternalPlanId ) ) ? '\'' . addslashes( $this->m_strExternalPlanId ) . '\'' : 'NULL';
	}

	public function setPlanName( $strPlanName ) {
		$this->set( 'm_strPlanName', CStrings::strTrimDef( $strPlanName, 50, NULL, true ) );
	}

	public function getPlanName() {
		return $this->m_strPlanName;
	}

	public function sqlPlanName() {
		return ( true == isset( $this->m_strPlanName ) ) ? '\'' . addslashes( $this->m_strPlanName ) . '\'' : 'NULL';
	}

	public function setPromoTitle( $strPromoTitle ) {
		$this->set( 'm_strPromoTitle', CStrings::strTrimDef( $strPromoTitle, 50, NULL, true ) );
	}

	public function getPromoTitle() {
		return $this->m_strPromoTitle;
	}

	public function sqlPromoTitle() {
		return ( true == isset( $this->m_strPromoTitle ) ) ? '\'' . addslashes( $this->m_strPromoTitle ) . '\'' : 'NULL';
	}

	public function setPromoDescription( $strPromoDescription ) {
		$this->set( 'm_strPromoDescription', CStrings::strTrimDef( $strPromoDescription, 500, NULL, true ) );
	}

	public function getPromoDescription() {
		return $this->m_strPromoDescription;
	}

	public function sqlPromoDescription() {
		return ( true == isset( $this->m_strPromoDescription ) ) ? '\'' . addslashes( $this->m_strPromoDescription ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ancillary_vendor_id, external_plan_id, plan_name, promo_title, promo_description, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlAncillaryVendorId() . ', ' .
 						$this->sqlExternalPlanId() . ', ' .
 						$this->sqlPlanName() . ', ' .
 						$this->sqlPromoTitle() . ', ' .
 						$this->sqlPromoDescription() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ancillary_vendor_id = ' . $this->sqlAncillaryVendorId() . ','; } elseif( true == array_key_exists( 'AncillaryVendorId', $this->getChangedColumns() ) ) { $strSql .= ' ancillary_vendor_id = ' . $this->sqlAncillaryVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_plan_id = ' . $this->sqlExternalPlanId() . ','; } elseif( true == array_key_exists( 'ExternalPlanId', $this->getChangedColumns() ) ) { $strSql .= ' external_plan_id = ' . $this->sqlExternalPlanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' plan_name = ' . $this->sqlPlanName() . ','; } elseif( true == array_key_exists( 'PlanName', $this->getChangedColumns() ) ) { $strSql .= ' plan_name = ' . $this->sqlPlanName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_title = ' . $this->sqlPromoTitle() . ','; } elseif( true == array_key_exists( 'PromoTitle', $this->getChangedColumns() ) ) { $strSql .= ' promo_title = ' . $this->sqlPromoTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promo_description = ' . $this->sqlPromoDescription() . ','; } elseif( true == array_key_exists( 'PromoDescription', $this->getChangedColumns() ) ) { $strSql .= ' promo_description = ' . $this->sqlPromoDescription() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ancillary_vendor_id' => $this->getAncillaryVendorId(),
			'external_plan_id' => $this->getExternalPlanId(),
			'plan_name' => $this->getPlanName(),
			'promo_title' => $this->getPromoTitle(),
			'promo_description' => $this->getPromoDescription(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>