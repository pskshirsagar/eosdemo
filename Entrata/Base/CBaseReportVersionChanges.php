<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportVersionChanges
 * Do not add any new functions to this class.
 */

class CBaseReportVersionChanges extends CEosPluralBase {

	/**
	 * @return CReportVersionChange[]
	 */
	public static function fetchReportVersionChanges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportVersionChange::class, $objDatabase );
	}

	/**
	 * @return CReportVersionChange
	 */
	public static function fetchReportVersionChange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportVersionChange::class, $objDatabase );
	}

	public static function fetchReportVersionChangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_version_changes', $objDatabase );
	}

	public static function fetchReportVersionChangeById( $intId, $objDatabase ) {
		return self::fetchReportVersionChange( sprintf( 'SELECT * FROM report_version_changes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportVersionChangesByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchReportVersionChanges( sprintf( 'SELECT * FROM report_version_changes WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

	public static function fetchReportVersionChangesByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		return self::fetchReportVersionChanges( sprintf( 'SELECT * FROM report_version_changes WHERE default_report_version_id = %d', ( int ) $intDefaultReportVersionId ), $objDatabase );
	}

	public static function fetchReportVersionChangesByReportDatasetId( $intReportDatasetId, $objDatabase ) {
		return self::fetchReportVersionChanges( sprintf( 'SELECT * FROM report_version_changes WHERE report_dataset_id = %d', ( int ) $intReportDatasetId ), $objDatabase );
	}

}
?>