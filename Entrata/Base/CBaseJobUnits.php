<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnits
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobUnits extends CEosPluralBase {

	/**
	 * @return CJobUnit[]
	 */
	public static function fetchJobUnits( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobUnit::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobUnit
	 */
	public static function fetchJobUnit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobUnit::class, $objDatabase );
	}

	public static function fetchJobUnitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_units', $objDatabase );
	}

	public static function fetchJobUnitByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobUnit( sprintf( 'SELECT * FROM job_units WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitsByCid( $intCid, $objDatabase ) {
		return self::fetchJobUnits( sprintf( 'SELECT * FROM job_units WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobUnits( sprintf( 'SELECT * FROM job_units WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchJobUnits( sprintf( 'SELECT * FROM job_units WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchJobUnits( sprintf( 'SELECT * FROM job_units WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchJobUnits( sprintf( 'SELECT * FROM job_units WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

}
?>