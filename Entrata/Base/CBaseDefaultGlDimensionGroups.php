<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlDimensionGroups
 * Do not add any new functions to this class.
 */

class CBaseDefaultGlDimensionGroups extends CEosPluralBase {

	/**
	 * @return CDefaultGlDimensionGroup[]
	 */
	public static function fetchDefaultGlDimensionGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultGlDimensionGroup::class, $objDatabase );
	}

	/**
	 * @return CDefaultGlDimensionGroup
	 */
	public static function fetchDefaultGlDimensionGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultGlDimensionGroup::class, $objDatabase );
	}

	public static function fetchDefaultGlDimensionGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_gl_dimension_groups', $objDatabase );
	}

	public static function fetchDefaultGlDimensionGroupById( $intId, $objDatabase ) {
		return self::fetchDefaultGlDimensionGroup( sprintf( 'SELECT * FROM default_gl_dimension_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>