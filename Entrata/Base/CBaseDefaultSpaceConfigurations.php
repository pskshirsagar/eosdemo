<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultSpaceConfigurations
 * Do not add any new functions to this class.
 */

class CBaseDefaultSpaceConfigurations extends CEosPluralBase {

	/**
	 * @return CDefaultSpaceConfiguration[]
	 */
	public static function fetchDefaultSpaceConfigurations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultSpaceConfiguration::class, $objDatabase );
	}

	/**
	 * @return CDefaultSpaceConfiguration
	 */
	public static function fetchDefaultSpaceConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultSpaceConfiguration::class, $objDatabase );
	}

	public static function fetchDefaultSpaceConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_space_configurations', $objDatabase );
	}

	public static function fetchDefaultSpaceConfigurationById( $intId, $objDatabase ) {
		return self::fetchDefaultSpaceConfiguration( sprintf( 'SELECT * FROM default_space_configurations WHERE id = %d', $intId ), $objDatabase );
	}

}
?>