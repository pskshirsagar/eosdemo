<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryPromoRegions
 * Do not add any new functions to this class.
 */

class CBaseAncillaryPromoRegions extends CEosPluralBase {

	/**
	 * @return CAncillaryPromoRegion[]
	 */
	public static function fetchAncillaryPromoRegions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAncillaryPromoRegion', $objDatabase );
	}

	/**
	 * @return CAncillaryPromoRegion
	 */
	public static function fetchAncillaryPromoRegion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAncillaryPromoRegion', $objDatabase );
	}

	public static function fetchAncillaryPromoRegionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_promo_regions', $objDatabase );
	}

	public static function fetchAncillaryPromoRegionById( $intId, $objDatabase ) {
		return self::fetchAncillaryPromoRegion( sprintf( 'SELECT * FROM ancillary_promo_regions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAncillaryPromoRegionsByAncillaryPlanPromoDetailsId( $intAncillaryPlanPromoDetailsId, $objDatabase ) {
		return self::fetchAncillaryPromoRegions( sprintf( 'SELECT * FROM ancillary_promo_regions WHERE ancillary_plan_promo_details_id = %d', ( int ) $intAncillaryPlanPromoDetailsId ), $objDatabase );
	}

}
?>