<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseReportGroupTypes extends CEosPluralBase {

	/**
	 * @return CReportGroupType[]
	 */
	public static function fetchReportGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportGroupType::class, $objDatabase );
	}

	/**
	 * @return CReportGroupType
	 */
	public static function fetchReportGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportGroupType::class, $objDatabase );
	}

	public static function fetchReportGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_group_types', $objDatabase );
	}

	public static function fetchReportGroupTypeById( $intId, $objDatabase ) {
		return self::fetchReportGroupType( sprintf( 'SELECT * FROM report_group_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>