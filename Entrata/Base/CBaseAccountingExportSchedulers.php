<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportSchedulers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportSchedulers extends CEosPluralBase {

	/**
	 * @return CAccountingExportScheduler[]
	 */
	public static function fetchAccountingExportSchedulers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountingExportScheduler::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountingExportScheduler
	 */
	public static function fetchAccountingExportScheduler( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportScheduler::class, $objDatabase );
	}

	public static function fetchAccountingExportSchedulerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_schedulers', $objDatabase );
	}

	public static function fetchAccountingExportSchedulerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportScheduler( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportSchedulersByCid( $intCid, $objDatabase ) {
		return self::fetchAccountingExportSchedulers( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportSchedulersByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportSchedulers( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE transmission_vendor_id = %d AND cid = %d', $intTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportSchedulersByAccountingExportFileFormatTypeIdByCid( $intAccountingExportFileFormatTypeId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportSchedulers( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE accounting_export_file_format_type_id = %d AND cid = %d', $intAccountingExportFileFormatTypeId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportSchedulersByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportSchedulers( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE company_transmission_vendor_id = %d AND cid = %d', $intCompanyTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportSchedulersByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportSchedulers( sprintf( 'SELECT * FROM accounting_export_schedulers WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

}
?>