<?php

class CBaseFileMetadataKey extends CEosSingularBase {

	const TABLE_NAME = 'public.file_metadata_keys';

	protected $m_intId;
	protected $m_strKeyName;
	protected $m_strDatatype;
	protected $m_intAllowMultipleValues;
	protected $m_intIsEnabled;

	public function __construct() {
		parent::__construct();

		$this->m_intAllowMultipleValues = '0';
		$this->m_intIsEnabled = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['key_name'] ) && $boolDirectSet ) $this->set( 'm_strKeyName', trim( stripcslashes( $arrValues['key_name'] ) ) ); elseif( isset( $arrValues['key_name'] ) ) $this->setKeyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key_name'] ) : $arrValues['key_name'] );
		if( isset( $arrValues['datatype'] ) && $boolDirectSet ) $this->set( 'm_strDatatype', trim( stripcslashes( $arrValues['datatype'] ) ) ); elseif( isset( $arrValues['datatype'] ) ) $this->setDatatype( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['datatype'] ) : $arrValues['datatype'] );
		if( isset( $arrValues['allow_multiple_values'] ) && $boolDirectSet ) $this->set( 'm_intAllowMultipleValues', trim( $arrValues['allow_multiple_values'] ) ); elseif( isset( $arrValues['allow_multiple_values'] ) ) $this->setAllowMultipleValues( $arrValues['allow_multiple_values'] );
		if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsEnabled', trim( $arrValues['is_enabled'] ) ); elseif( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( $arrValues['is_enabled'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setKeyName( $strKeyName ) {
		$this->set( 'm_strKeyName', CStrings::strTrimDef( $strKeyName, 200, NULL, true ) );
	}

	public function getKeyName() {
		return $this->m_strKeyName;
	}

	public function sqlKeyName() {
		return ( true == isset( $this->m_strKeyName ) ) ? '\'' . addslashes( $this->m_strKeyName ) . '\'' : 'NULL';
	}

	public function setDatatype( $strDatatype ) {
		$this->set( 'm_strDatatype', CStrings::strTrimDef( $strDatatype, 50, NULL, true ) );
	}

	public function getDatatype() {
		return $this->m_strDatatype;
	}

	public function sqlDatatype() {
		return ( true == isset( $this->m_strDatatype ) ) ? '\'' . addslashes( $this->m_strDatatype ) . '\'' : 'NULL';
	}

	public function setAllowMultipleValues( $intAllowMultipleValues ) {
		$this->set( 'm_intAllowMultipleValues', CStrings::strToIntDef( $intAllowMultipleValues, NULL, false ) );
	}

	public function getAllowMultipleValues() {
		return $this->m_intAllowMultipleValues;
	}

	public function sqlAllowMultipleValues() {
		return ( true == isset( $this->m_intAllowMultipleValues ) ) ? ( string ) $this->m_intAllowMultipleValues : '0';
	}

	public function setIsEnabled( $intIsEnabled ) {
		$this->set( 'm_intIsEnabled', CStrings::strToIntDef( $intIsEnabled, NULL, false ) );
	}

	public function getIsEnabled() {
		return $this->m_intIsEnabled;
	}

	public function sqlIsEnabled() {
		return ( true == isset( $this->m_intIsEnabled ) ) ? ( string ) $this->m_intIsEnabled : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'key_name' => $this->getKeyName(),
			'datatype' => $this->getDatatype(),
			'allow_multiple_values' => $this->getAllowMultipleValues(),
			'is_enabled' => $this->getIsEnabled()
		);
	}

}
?>