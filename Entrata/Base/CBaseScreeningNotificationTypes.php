<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotificationTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningNotificationTypes extends CEosPluralBase {

	/**
	 * @return CScreeningNotificationType[]
	 */
	public static function fetchScreeningNotificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningNotificationType::class, $objDatabase );
	}

	/**
	 * @return CScreeningNotificationType
	 */
	public static function fetchScreeningNotificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningNotificationType::class, $objDatabase );
	}

	public static function fetchScreeningNotificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_notification_types', $objDatabase );
	}

	public static function fetchScreeningNotificationTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningNotificationType( sprintf( 'SELECT * FROM screening_notification_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchScreeningNotificationTypesByScreeningEventTypeId( $intScreeningEventTypeId, $objDatabase ) {
		return self::fetchScreeningNotificationTypes( sprintf( 'SELECT * FROM screening_notification_types WHERE screening_event_type_id = %d', $intScreeningEventTypeId ), $objDatabase );
	}

	public static function fetchScreeningNotificationTypesByScreeningMessageTypeId( $intScreeningMessageTypeId, $objDatabase ) {
		return self::fetchScreeningNotificationTypes( sprintf( 'SELECT * FROM screening_notification_types WHERE screening_message_type_id = %d', $intScreeningMessageTypeId ), $objDatabase );
	}

}
?>