<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyInterestFormula extends CEosSingularBase {

	const TABLE_NAME = 'public.property_interest_formulas';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intOccupancyTypeId;
	protected $m_intInterestFormulaTypeId;
	protected $m_intParentPropertyInterestFormulaId;
	protected $m_intArCodeId;
	protected $m_intArTriggerId;
	protected $m_intRateIntervalOffset;
	protected $m_intInterestQualificationDays;
	protected $m_intInterestPeriodForfeitDays;
	protected $m_intInterestPaymentGraceDays;
	protected $m_intPostingQualificationDays;
	protected $m_boolDenyInterestForLeaseViolations;
	protected $m_boolGenerateRefundCheck;
	protected $m_boolAccrueInterestDuringQualificationPeriod;
	protected $m_fltInterestPercent;
	protected $m_boolCalculateInterest;
	protected $m_strEffectiveOn;
	protected $m_strEffectiveThroughDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intInterestFormulaTypeId = '1';
		$this->m_intRateIntervalOffset = '0';
		$this->m_intInterestQualificationDays = '0';
		$this->m_intInterestPeriodForfeitDays = '0';
		$this->m_intInterestPaymentGraceDays = '0';
		$this->m_boolDenyInterestForLeaseViolations = false;
		$this->m_boolGenerateRefundCheck = false;
		$this->m_boolAccrueInterestDuringQualificationPeriod = false;
		$this->m_fltInterestPercent = '0';
		$this->m_boolCalculateInterest = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['interest_formula_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInterestFormulaTypeId', trim( $arrValues['interest_formula_type_id'] ) ); elseif( isset( $arrValues['interest_formula_type_id'] ) ) $this->setInterestFormulaTypeId( $arrValues['interest_formula_type_id'] );
		if( isset( $arrValues['parent_property_interest_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intParentPropertyInterestFormulaId', trim( $arrValues['parent_property_interest_formula_id'] ) ); elseif( isset( $arrValues['parent_property_interest_formula_id'] ) ) $this->setParentPropertyInterestFormulaId( $arrValues['parent_property_interest_formula_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['rate_interval_offset'] ) && $boolDirectSet ) $this->set( 'm_intRateIntervalOffset', trim( $arrValues['rate_interval_offset'] ) ); elseif( isset( $arrValues['rate_interval_offset'] ) ) $this->setRateIntervalOffset( $arrValues['rate_interval_offset'] );
		if( isset( $arrValues['interest_qualification_days'] ) && $boolDirectSet ) $this->set( 'm_intInterestQualificationDays', trim( $arrValues['interest_qualification_days'] ) ); elseif( isset( $arrValues['interest_qualification_days'] ) ) $this->setInterestQualificationDays( $arrValues['interest_qualification_days'] );
		if( isset( $arrValues['interest_period_forfeit_days'] ) && $boolDirectSet ) $this->set( 'm_intInterestPeriodForfeitDays', trim( $arrValues['interest_period_forfeit_days'] ) ); elseif( isset( $arrValues['interest_period_forfeit_days'] ) ) $this->setInterestPeriodForfeitDays( $arrValues['interest_period_forfeit_days'] );
		if( isset( $arrValues['interest_payment_grace_days'] ) && $boolDirectSet ) $this->set( 'm_intInterestPaymentGraceDays', trim( $arrValues['interest_payment_grace_days'] ) ); elseif( isset( $arrValues['interest_payment_grace_days'] ) ) $this->setInterestPaymentGraceDays( $arrValues['interest_payment_grace_days'] );
		if( isset( $arrValues['posting_qualification_days'] ) && $boolDirectSet ) $this->set( 'm_intPostingQualificationDays', trim( $arrValues['posting_qualification_days'] ) ); elseif( isset( $arrValues['posting_qualification_days'] ) ) $this->setPostingQualificationDays( $arrValues['posting_qualification_days'] );
		if( isset( $arrValues['deny_interest_for_lease_violations'] ) && $boolDirectSet ) $this->set( 'm_boolDenyInterestForLeaseViolations', trim( stripcslashes( $arrValues['deny_interest_for_lease_violations'] ) ) ); elseif( isset( $arrValues['deny_interest_for_lease_violations'] ) ) $this->setDenyInterestForLeaseViolations( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deny_interest_for_lease_violations'] ) : $arrValues['deny_interest_for_lease_violations'] );
		if( isset( $arrValues['generate_refund_check'] ) && $boolDirectSet ) $this->set( 'm_boolGenerateRefundCheck', trim( stripcslashes( $arrValues['generate_refund_check'] ) ) ); elseif( isset( $arrValues['generate_refund_check'] ) ) $this->setGenerateRefundCheck( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['generate_refund_check'] ) : $arrValues['generate_refund_check'] );
		if( isset( $arrValues['accrue_interest_during_qualification_period'] ) && $boolDirectSet ) $this->set( 'm_boolAccrueInterestDuringQualificationPeriod', trim( stripcslashes( $arrValues['accrue_interest_during_qualification_period'] ) ) ); elseif( isset( $arrValues['accrue_interest_during_qualification_period'] ) ) $this->setAccrueInterestDuringQualificationPeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['accrue_interest_during_qualification_period'] ) : $arrValues['accrue_interest_during_qualification_period'] );
		if( isset( $arrValues['interest_percent'] ) && $boolDirectSet ) $this->set( 'm_fltInterestPercent', trim( $arrValues['interest_percent'] ) ); elseif( isset( $arrValues['interest_percent'] ) ) $this->setInterestPercent( $arrValues['interest_percent'] );
		if( isset( $arrValues['calculate_interest'] ) && $boolDirectSet ) $this->set( 'm_boolCalculateInterest', trim( stripcslashes( $arrValues['calculate_interest'] ) ) ); elseif( isset( $arrValues['calculate_interest'] ) ) $this->setCalculateInterest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['calculate_interest'] ) : $arrValues['calculate_interest'] );
		if( isset( $arrValues['effective_on'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveOn', trim( $arrValues['effective_on'] ) ); elseif( isset( $arrValues['effective_on'] ) ) $this->setEffectiveOn( $arrValues['effective_on'] );
		if( isset( $arrValues['effective_through_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveThroughDate', trim( $arrValues['effective_through_date'] ) ); elseif( isset( $arrValues['effective_through_date'] ) ) $this->setEffectiveThroughDate( $arrValues['effective_through_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setInterestFormulaTypeId( $intInterestFormulaTypeId ) {
		$this->set( 'm_intInterestFormulaTypeId', CStrings::strToIntDef( $intInterestFormulaTypeId, NULL, false ) );
	}

	public function getInterestFormulaTypeId() {
		return $this->m_intInterestFormulaTypeId;
	}

	public function sqlInterestFormulaTypeId() {
		return ( true == isset( $this->m_intInterestFormulaTypeId ) ) ? ( string ) $this->m_intInterestFormulaTypeId : '1';
	}

	public function setParentPropertyInterestFormulaId( $intParentPropertyInterestFormulaId ) {
		$this->set( 'm_intParentPropertyInterestFormulaId', CStrings::strToIntDef( $intParentPropertyInterestFormulaId, NULL, false ) );
	}

	public function getParentPropertyInterestFormulaId() {
		return $this->m_intParentPropertyInterestFormulaId;
	}

	public function sqlParentPropertyInterestFormulaId() {
		return ( true == isset( $this->m_intParentPropertyInterestFormulaId ) ) ? ( string ) $this->m_intParentPropertyInterestFormulaId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setRateIntervalOffset( $intRateIntervalOffset ) {
		$this->set( 'm_intRateIntervalOffset', CStrings::strToIntDef( $intRateIntervalOffset, NULL, false ) );
	}

	public function getRateIntervalOffset() {
		return $this->m_intRateIntervalOffset;
	}

	public function sqlRateIntervalOffset() {
		return ( true == isset( $this->m_intRateIntervalOffset ) ) ? ( string ) $this->m_intRateIntervalOffset : '0';
	}

	public function setInterestQualificationDays( $intInterestQualificationDays ) {
		$this->set( 'm_intInterestQualificationDays', CStrings::strToIntDef( $intInterestQualificationDays, NULL, false ) );
	}

	public function getInterestQualificationDays() {
		return $this->m_intInterestQualificationDays;
	}

	public function sqlInterestQualificationDays() {
		return ( true == isset( $this->m_intInterestQualificationDays ) ) ? ( string ) $this->m_intInterestQualificationDays : '0';
	}

	public function setInterestPeriodForfeitDays( $intInterestPeriodForfeitDays ) {
		$this->set( 'm_intInterestPeriodForfeitDays', CStrings::strToIntDef( $intInterestPeriodForfeitDays, NULL, false ) );
	}

	public function getInterestPeriodForfeitDays() {
		return $this->m_intInterestPeriodForfeitDays;
	}

	public function sqlInterestPeriodForfeitDays() {
		return ( true == isset( $this->m_intInterestPeriodForfeitDays ) ) ? ( string ) $this->m_intInterestPeriodForfeitDays : '0';
	}

	public function setInterestPaymentGraceDays( $intInterestPaymentGraceDays ) {
		$this->set( 'm_intInterestPaymentGraceDays', CStrings::strToIntDef( $intInterestPaymentGraceDays, NULL, false ) );
	}

	public function getInterestPaymentGraceDays() {
		return $this->m_intInterestPaymentGraceDays;
	}

	public function sqlInterestPaymentGraceDays() {
		return ( true == isset( $this->m_intInterestPaymentGraceDays ) ) ? ( string ) $this->m_intInterestPaymentGraceDays : '0';
	}

	public function setPostingQualificationDays( $intPostingQualificationDays ) {
		$this->set( 'm_intPostingQualificationDays', CStrings::strToIntDef( $intPostingQualificationDays, NULL, false ) );
	}

	public function getPostingQualificationDays() {
		return $this->m_intPostingQualificationDays;
	}

	public function sqlPostingQualificationDays() {
		return ( true == isset( $this->m_intPostingQualificationDays ) ) ? ( string ) $this->m_intPostingQualificationDays : 'NULL';
	}

	public function setDenyInterestForLeaseViolations( $boolDenyInterestForLeaseViolations ) {
		$this->set( 'm_boolDenyInterestForLeaseViolations', CStrings::strToBool( $boolDenyInterestForLeaseViolations ) );
	}

	public function getDenyInterestForLeaseViolations() {
		return $this->m_boolDenyInterestForLeaseViolations;
	}

	public function sqlDenyInterestForLeaseViolations() {
		return ( true == isset( $this->m_boolDenyInterestForLeaseViolations ) ) ? '\'' . ( true == ( bool ) $this->m_boolDenyInterestForLeaseViolations ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGenerateRefundCheck( $boolGenerateRefundCheck ) {
		$this->set( 'm_boolGenerateRefundCheck', CStrings::strToBool( $boolGenerateRefundCheck ) );
	}

	public function getGenerateRefundCheck() {
		return $this->m_boolGenerateRefundCheck;
	}

	public function sqlGenerateRefundCheck() {
		return ( true == isset( $this->m_boolGenerateRefundCheck ) ) ? '\'' . ( true == ( bool ) $this->m_boolGenerateRefundCheck ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAccrueInterestDuringQualificationPeriod( $boolAccrueInterestDuringQualificationPeriod ) {
		$this->set( 'm_boolAccrueInterestDuringQualificationPeriod', CStrings::strToBool( $boolAccrueInterestDuringQualificationPeriod ) );
	}

	public function getAccrueInterestDuringQualificationPeriod() {
		return $this->m_boolAccrueInterestDuringQualificationPeriod;
	}

	public function sqlAccrueInterestDuringQualificationPeriod() {
		return ( true == isset( $this->m_boolAccrueInterestDuringQualificationPeriod ) ) ? '\'' . ( true == ( bool ) $this->m_boolAccrueInterestDuringQualificationPeriod ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInterestPercent( $fltInterestPercent ) {
		$this->set( 'm_fltInterestPercent', CStrings::strToFloatDef( $fltInterestPercent, NULL, false, 6 ) );
	}

	public function getInterestPercent() {
		return $this->m_fltInterestPercent;
	}

	public function sqlInterestPercent() {
		return ( true == isset( $this->m_fltInterestPercent ) ) ? ( string ) $this->m_fltInterestPercent : '0';
	}

	public function setCalculateInterest( $boolCalculateInterest ) {
		$this->set( 'm_boolCalculateInterest', CStrings::strToBool( $boolCalculateInterest ) );
	}

	public function getCalculateInterest() {
		return $this->m_boolCalculateInterest;
	}

	public function sqlCalculateInterest() {
		return ( true == isset( $this->m_boolCalculateInterest ) ) ? '\'' . ( true == ( bool ) $this->m_boolCalculateInterest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEffectiveOn( $strEffectiveOn ) {
		$this->set( 'm_strEffectiveOn', CStrings::strTrimDef( $strEffectiveOn, -1, NULL, true ) );
	}

	public function getEffectiveOn() {
		return $this->m_strEffectiveOn;
	}

	public function sqlEffectiveOn() {
		return ( true == isset( $this->m_strEffectiveOn ) ) ? '\'' . $this->m_strEffectiveOn . '\'' : 'NOW()';
	}

	public function setEffectiveThroughDate( $strEffectiveThroughDate ) {
		$this->set( 'm_strEffectiveThroughDate', CStrings::strTrimDef( $strEffectiveThroughDate, -1, NULL, true ) );
	}

	public function getEffectiveThroughDate() {
		return $this->m_strEffectiveThroughDate;
	}

	public function sqlEffectiveThroughDate() {
		return ( true == isset( $this->m_strEffectiveThroughDate ) ) ? '\'' . $this->m_strEffectiveThroughDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, occupancy_type_id, interest_formula_type_id, parent_property_interest_formula_id, ar_code_id, ar_trigger_id, rate_interval_offset, interest_qualification_days, interest_period_forfeit_days, interest_payment_grace_days, posting_qualification_days, deny_interest_for_lease_violations, generate_refund_check, accrue_interest_during_qualification_period, interest_percent, calculate_interest, effective_on, effective_through_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlOccupancyTypeId() . ', ' .
 						$this->sqlInterestFormulaTypeId() . ', ' .
 						$this->sqlParentPropertyInterestFormulaId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlArTriggerId() . ', ' .
 						$this->sqlRateIntervalOffset() . ', ' .
 						$this->sqlInterestQualificationDays() . ', ' .
 						$this->sqlInterestPeriodForfeitDays() . ', ' .
 						$this->sqlInterestPaymentGraceDays() . ', ' .
 						$this->sqlPostingQualificationDays() . ', ' .
 						$this->sqlDenyInterestForLeaseViolations() . ', ' .
 						$this->sqlGenerateRefundCheck() . ', ' .
 						$this->sqlAccrueInterestDuringQualificationPeriod() . ', ' .
 						$this->sqlInterestPercent() . ', ' .
 						$this->sqlCalculateInterest() . ', ' .
 						$this->sqlEffectiveOn() . ', ' .
 						$this->sqlEffectiveThroughDate() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_formula_type_id = ' . $this->sqlInterestFormulaTypeId() . ','; } elseif( true == array_key_exists( 'InterestFormulaTypeId', $this->getChangedColumns() ) ) { $strSql .= ' interest_formula_type_id = ' . $this->sqlInterestFormulaTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_property_interest_formula_id = ' . $this->sqlParentPropertyInterestFormulaId() . ','; } elseif( true == array_key_exists( 'ParentPropertyInterestFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' parent_property_interest_formula_id = ' . $this->sqlParentPropertyInterestFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset() . ','; } elseif( true == array_key_exists( 'RateIntervalOffset', $this->getChangedColumns() ) ) { $strSql .= ' rate_interval_offset = ' . $this->sqlRateIntervalOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_qualification_days = ' . $this->sqlInterestQualificationDays() . ','; } elseif( true == array_key_exists( 'InterestQualificationDays', $this->getChangedColumns() ) ) { $strSql .= ' interest_qualification_days = ' . $this->sqlInterestQualificationDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_period_forfeit_days = ' . $this->sqlInterestPeriodForfeitDays() . ','; } elseif( true == array_key_exists( 'InterestPeriodForfeitDays', $this->getChangedColumns() ) ) { $strSql .= ' interest_period_forfeit_days = ' . $this->sqlInterestPeriodForfeitDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_payment_grace_days = ' . $this->sqlInterestPaymentGraceDays() . ','; } elseif( true == array_key_exists( 'InterestPaymentGraceDays', $this->getChangedColumns() ) ) { $strSql .= ' interest_payment_grace_days = ' . $this->sqlInterestPaymentGraceDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posting_qualification_days = ' . $this->sqlPostingQualificationDays() . ','; } elseif( true == array_key_exists( 'PostingQualificationDays', $this->getChangedColumns() ) ) { $strSql .= ' posting_qualification_days = ' . $this->sqlPostingQualificationDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deny_interest_for_lease_violations = ' . $this->sqlDenyInterestForLeaseViolations() . ','; } elseif( true == array_key_exists( 'DenyInterestForLeaseViolations', $this->getChangedColumns() ) ) { $strSql .= ' deny_interest_for_lease_violations = ' . $this->sqlDenyInterestForLeaseViolations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' generate_refund_check = ' . $this->sqlGenerateRefundCheck() . ','; } elseif( true == array_key_exists( 'GenerateRefundCheck', $this->getChangedColumns() ) ) { $strSql .= ' generate_refund_check = ' . $this->sqlGenerateRefundCheck() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrue_interest_during_qualification_period = ' . $this->sqlAccrueInterestDuringQualificationPeriod() . ','; } elseif( true == array_key_exists( 'AccrueInterestDuringQualificationPeriod', $this->getChangedColumns() ) ) { $strSql .= ' accrue_interest_during_qualification_period = ' . $this->sqlAccrueInterestDuringQualificationPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_percent = ' . $this->sqlInterestPercent() . ','; } elseif( true == array_key_exists( 'InterestPercent', $this->getChangedColumns() ) ) { $strSql .= ' interest_percent = ' . $this->sqlInterestPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calculate_interest = ' . $this->sqlCalculateInterest() . ','; } elseif( true == array_key_exists( 'CalculateInterest', $this->getChangedColumns() ) ) { $strSql .= ' calculate_interest = ' . $this->sqlCalculateInterest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; } elseif( true == array_key_exists( 'EffectiveOn', $this->getChangedColumns() ) ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate() . ','; } elseif( true == array_key_exists( 'EffectiveThroughDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_through_date = ' . $this->sqlEffectiveThroughDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'interest_formula_type_id' => $this->getInterestFormulaTypeId(),
			'parent_property_interest_formula_id' => $this->getParentPropertyInterestFormulaId(),
			'ar_code_id' => $this->getArCodeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'rate_interval_offset' => $this->getRateIntervalOffset(),
			'interest_qualification_days' => $this->getInterestQualificationDays(),
			'interest_period_forfeit_days' => $this->getInterestPeriodForfeitDays(),
			'interest_payment_grace_days' => $this->getInterestPaymentGraceDays(),
			'posting_qualification_days' => $this->getPostingQualificationDays(),
			'deny_interest_for_lease_violations' => $this->getDenyInterestForLeaseViolations(),
			'generate_refund_check' => $this->getGenerateRefundCheck(),
			'accrue_interest_during_qualification_period' => $this->getAccrueInterestDuringQualificationPeriod(),
			'interest_percent' => $this->getInterestPercent(),
			'calculate_interest' => $this->getCalculateInterest(),
			'effective_on' => $this->getEffectiveOn(),
			'effective_through_date' => $this->getEffectiveThroughDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>