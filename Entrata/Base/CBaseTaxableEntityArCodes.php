<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxableEntityArCodes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTaxableEntityArCodes extends CEosPluralBase {

	/**
	 * @return CTaxableEntityArCode[]
	 */
	public static function fetchTaxableEntityArCodes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTaxableEntityArCode::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTaxableEntityArCode
	 */
	public static function fetchTaxableEntityArCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaxableEntityArCode::class, $objDatabase );
	}

	public static function fetchTaxableEntityArCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'taxable_entity_ar_codes', $objDatabase );
	}

	public static function fetchTaxableEntityArCodeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntityArCode( sprintf( 'SELECT * FROM taxable_entity_ar_codes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntityArCodesByCid( $intCid, $objDatabase ) {
		return self::fetchTaxableEntityArCodes( sprintf( 'SELECT * FROM taxable_entity_ar_codes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntityArCodesByTaxableEntityIdByCid( $intTaxableEntityId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntityArCodes( sprintf( 'SELECT * FROM taxable_entity_ar_codes WHERE taxable_entity_id = %d AND cid = %d', $intTaxableEntityId, $intCid ), $objDatabase );
	}

	public static function fetchTaxableEntityArCodesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchTaxableEntityArCodes( sprintf( 'SELECT * FROM taxable_entity_ar_codes WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

}
?>