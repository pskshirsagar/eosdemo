<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationTemplateNotice extends CEosSingularBase {

	const TABLE_NAME = 'public.violation_template_notices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intViolationTemplateId;
	protected $m_intViolationWarningTypeId;
	protected $m_intArCodeId;
	protected $m_intDocumentId;
	protected $m_fltAmount;
	protected $m_intNextFollowUpDays;
	protected $m_boolIsStartEviction;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsStartEviction = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['violation_template_id'] ) && $boolDirectSet ) $this->set( 'm_intViolationTemplateId', trim( $arrValues['violation_template_id'] ) ); elseif( isset( $arrValues['violation_template_id'] ) ) $this->setViolationTemplateId( $arrValues['violation_template_id'] );
		if( isset( $arrValues['violation_warning_type_id'] ) && $boolDirectSet ) $this->set( 'm_intViolationWarningTypeId', trim( $arrValues['violation_warning_type_id'] ) ); elseif( isset( $arrValues['violation_warning_type_id'] ) ) $this->setViolationWarningTypeId( $arrValues['violation_warning_type_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['next_follow_up_days'] ) && $boolDirectSet ) $this->set( 'm_intNextFollowUpDays', trim( $arrValues['next_follow_up_days'] ) ); elseif( isset( $arrValues['next_follow_up_days'] ) ) $this->setNextFollowUpDays( $arrValues['next_follow_up_days'] );
		if( isset( $arrValues['is_start_eviction'] ) && $boolDirectSet ) $this->set( 'm_boolIsStartEviction', trim( stripcslashes( $arrValues['is_start_eviction'] ) ) ); elseif( isset( $arrValues['is_start_eviction'] ) ) $this->setIsStartEviction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_start_eviction'] ) : $arrValues['is_start_eviction'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setViolationTemplateId( $intViolationTemplateId ) {
		$this->set( 'm_intViolationTemplateId', CStrings::strToIntDef( $intViolationTemplateId, NULL, false ) );
	}

	public function getViolationTemplateId() {
		return $this->m_intViolationTemplateId;
	}

	public function sqlViolationTemplateId() {
		return ( true == isset( $this->m_intViolationTemplateId ) ) ? ( string ) $this->m_intViolationTemplateId : 'NULL';
	}

	public function setViolationWarningTypeId( $intViolationWarningTypeId ) {
		$this->set( 'm_intViolationWarningTypeId', CStrings::strToIntDef( $intViolationWarningTypeId, NULL, false ) );
	}

	public function getViolationWarningTypeId() {
		return $this->m_intViolationWarningTypeId;
	}

	public function sqlViolationWarningTypeId() {
		return ( true == isset( $this->m_intViolationWarningTypeId ) ) ? ( string ) $this->m_intViolationWarningTypeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setNextFollowUpDays( $intNextFollowUpDays ) {
		$this->set( 'm_intNextFollowUpDays', CStrings::strToIntDef( $intNextFollowUpDays, NULL, false ) );
	}

	public function getNextFollowUpDays() {
		return $this->m_intNextFollowUpDays;
	}

	public function sqlNextFollowUpDays() {
		return ( true == isset( $this->m_intNextFollowUpDays ) ) ? ( string ) $this->m_intNextFollowUpDays : 'NULL';
	}

	public function setIsStartEviction( $boolIsStartEviction ) {
		$this->set( 'm_boolIsStartEviction', CStrings::strToBool( $boolIsStartEviction ) );
	}

	public function getIsStartEviction() {
		return $this->m_boolIsStartEviction;
	}

	public function sqlIsStartEviction() {
		return ( true == isset( $this->m_boolIsStartEviction ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStartEviction ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, violation_template_id, violation_warning_type_id, ar_code_id, document_id, amount, next_follow_up_days, is_start_eviction, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlViolationTemplateId() . ', ' .
 						$this->sqlViolationWarningTypeId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlNextFollowUpDays() . ', ' .
 						$this->sqlIsStartEviction() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_template_id = ' . $this->sqlViolationTemplateId() . ','; } elseif( true == array_key_exists( 'ViolationTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' violation_template_id = ' . $this->sqlViolationTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_warning_type_id = ' . $this->sqlViolationWarningTypeId() . ','; } elseif( true == array_key_exists( 'ViolationWarningTypeId', $this->getChangedColumns() ) ) { $strSql .= ' violation_warning_type_id = ' . $this->sqlViolationWarningTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_follow_up_days = ' . $this->sqlNextFollowUpDays() . ','; } elseif( true == array_key_exists( 'NextFollowUpDays', $this->getChangedColumns() ) ) { $strSql .= ' next_follow_up_days = ' . $this->sqlNextFollowUpDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_start_eviction = ' . $this->sqlIsStartEviction() . ','; } elseif( true == array_key_exists( 'IsStartEviction', $this->getChangedColumns() ) ) { $strSql .= ' is_start_eviction = ' . $this->sqlIsStartEviction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'violation_template_id' => $this->getViolationTemplateId(),
			'violation_warning_type_id' => $this->getViolationWarningTypeId(),
			'ar_code_id' => $this->getArCodeId(),
			'document_id' => $this->getDocumentId(),
			'amount' => $this->getAmount(),
			'next_follow_up_days' => $this->getNextFollowUpDays(),
			'is_start_eviction' => $this->getIsStartEviction(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>