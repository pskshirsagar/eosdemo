<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleTypes
 * Do not add any new functions to this class.
 */

class CBaseReportScheduleTypes extends CEosPluralBase {

	/**
	 * @return CReportScheduleType[]
	 */
	public static function fetchReportScheduleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportScheduleType::class, $objDatabase );
	}

	/**
	 * @return CReportScheduleType
	 */
	public static function fetchReportScheduleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportScheduleType::class, $objDatabase );
	}

	public static function fetchReportScheduleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_schedule_types', $objDatabase );
	}

	public static function fetchReportScheduleTypeById( $intId, $objDatabase ) {
		return self::fetchReportScheduleType( sprintf( 'SELECT * FROM report_schedule_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>