<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySpaceConfigurations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySpaceConfigurations extends CEosPluralBase {

	/**
	 * @return CPropertySpaceConfiguration[]
	 */
	public static function fetchPropertySpaceConfigurations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertySpaceConfiguration', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertySpaceConfiguration
	 */
	public static function fetchPropertySpaceConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertySpaceConfiguration', $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_space_configurations', $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertySpaceConfiguration( sprintf( 'SELECT * FROM property_space_configurations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertySpaceConfigurations( sprintf( 'SELECT * FROM property_space_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySpaceConfigurations( sprintf( 'SELECT * FROM property_space_configurations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySpaceConfigurationsBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchPropertySpaceConfigurations( sprintf( 'SELECT * FROM property_space_configurations WHERE space_configuration_id = %d AND cid = %d', ( int ) $intSpaceConfigurationId, ( int ) $intCid ), $objDatabase );
	}

}
?>