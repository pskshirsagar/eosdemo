<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCandidateStatusTypes extends CEosPluralBase {

	/**
	 * @return CCandidateStatusType[]
	 */
	public static function fetchCandidateStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCandidateStatusType::class, $objDatabase );
	}

	/**
	 * @return CCandidateStatusType
	 */
	public static function fetchCandidateStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCandidateStatusType::class, $objDatabase );
	}

	public static function fetchCandidateStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_status_types', $objDatabase );
	}

	public static function fetchCandidateStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCandidateStatusType( sprintf( 'SELECT * FROM candidate_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>