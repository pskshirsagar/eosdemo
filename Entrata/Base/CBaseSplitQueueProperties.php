<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSplitQueueProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSplitQueueProperties extends CEosPluralBase {

	/**
	 * @return CSplitQueueProperty[]
	 */
	public static function fetchSplitQueueProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSplitQueueProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSplitQueueProperty
	 */
	public static function fetchSplitQueueProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSplitQueueProperty', $objDatabase );
	}

	public static function fetchSplitQueuePropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'split_queue_properties', $objDatabase );
	}

	public static function fetchSplitQueuePropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSplitQueueProperty( sprintf( 'SELECT * FROM split_queue_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSplitQueuePropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchSplitQueueProperties( sprintf( 'SELECT * FROM split_queue_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSplitQueuePropertiesByOldPropertyIdByCid( $intOldPropertyId, $intCid, $objDatabase ) {
		return self::fetchSplitQueueProperties( sprintf( 'SELECT * FROM split_queue_properties WHERE old_property_id = %d AND cid = %d', ( int ) $intOldPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSplitQueuePropertiesByNewPropertyIdByCid( $intNewPropertyId, $intCid, $objDatabase ) {
		return self::fetchSplitQueueProperties( sprintf( 'SELECT * FROM split_queue_properties WHERE new_property_id = %d AND cid = %d', ( int ) $intNewPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>