<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDrawRequestDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.draw_request_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobPhaseId;
	protected $m_intDrawRequestId;
	protected $m_intApDetailId;
	protected $m_intGlDetailId;
	protected $m_fltDrawAmount;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltDrawAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPhaseId', trim( $arrValues['job_phase_id'] ) ); elseif( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
		if( isset( $arrValues['draw_request_id'] ) && $boolDirectSet ) $this->set( 'm_intDrawRequestId', trim( $arrValues['draw_request_id'] ) ); elseif( isset( $arrValues['draw_request_id'] ) ) $this->setDrawRequestId( $arrValues['draw_request_id'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['gl_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDetailId', trim( $arrValues['gl_detail_id'] ) ); elseif( isset( $arrValues['gl_detail_id'] ) ) $this->setGlDetailId( $arrValues['gl_detail_id'] );
		if( isset( $arrValues['draw_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDrawAmount', trim( $arrValues['draw_amount'] ) ); elseif( isset( $arrValues['draw_amount'] ) ) $this->setDrawAmount( $arrValues['draw_amount'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobPhaseId( $intJobPhaseId ) {
		$this->set( 'm_intJobPhaseId', CStrings::strToIntDef( $intJobPhaseId, NULL, false ) );
	}

	public function getJobPhaseId() {
		return $this->m_intJobPhaseId;
	}

	public function sqlJobPhaseId() {
		return ( true == isset( $this->m_intJobPhaseId ) ) ? ( string ) $this->m_intJobPhaseId : 'NULL';
	}

	public function setDrawRequestId( $intDrawRequestId ) {
		$this->set( 'm_intDrawRequestId', CStrings::strToIntDef( $intDrawRequestId, NULL, false ) );
	}

	public function getDrawRequestId() {
		return $this->m_intDrawRequestId;
	}

	public function sqlDrawRequestId() {
		return ( true == isset( $this->m_intDrawRequestId ) ) ? ( string ) $this->m_intDrawRequestId : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setGlDetailId( $intGlDetailId ) {
		$this->set( 'm_intGlDetailId', CStrings::strToIntDef( $intGlDetailId, NULL, false ) );
	}

	public function getGlDetailId() {
		return $this->m_intGlDetailId;
	}

	public function sqlGlDetailId() {
		return ( true == isset( $this->m_intGlDetailId ) ) ? ( string ) $this->m_intGlDetailId : 'NULL';
	}

	public function setDrawAmount( $fltDrawAmount ) {
		$this->set( 'm_fltDrawAmount', CStrings::strToFloatDef( $fltDrawAmount, NULL, false, 2 ) );
	}

	public function getDrawAmount() {
		return $this->m_fltDrawAmount;
	}

	public function sqlDrawAmount() {
		return ( true == isset( $this->m_fltDrawAmount ) ) ? ( string ) $this->m_fltDrawAmount : '0';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_phase_id, draw_request_id, ap_detail_id, gl_detail_id, draw_amount, rejected_by, rejected_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlJobPhaseId() . ', ' .
 						$this->sqlDrawRequestId() . ', ' .
 						$this->sqlApDetailId() . ', ' .
 						$this->sqlGlDetailId() . ', ' .
 						$this->sqlDrawAmount() . ', ' .
 						$this->sqlRejectedBy() . ', ' .
 						$this->sqlRejectedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; } elseif( true == array_key_exists( 'JobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' draw_request_id = ' . $this->sqlDrawRequestId() . ','; } elseif( true == array_key_exists( 'DrawRequestId', $this->getChangedColumns() ) ) { $strSql .= ' draw_request_id = ' . $this->sqlDrawRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_detail_id = ' . $this->sqlGlDetailId() . ','; } elseif( true == array_key_exists( 'GlDetailId', $this->getChangedColumns() ) ) { $strSql .= ' gl_detail_id = ' . $this->sqlGlDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' draw_amount = ' . $this->sqlDrawAmount() . ','; } elseif( true == array_key_exists( 'DrawAmount', $this->getChangedColumns() ) ) { $strSql .= ' draw_amount = ' . $this->sqlDrawAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_phase_id' => $this->getJobPhaseId(),
			'draw_request_id' => $this->getDrawRequestId(),
			'ap_detail_id' => $this->getApDetailId(),
			'gl_detail_id' => $this->getGlDetailId(),
			'draw_amount' => $this->getDrawAmount(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>