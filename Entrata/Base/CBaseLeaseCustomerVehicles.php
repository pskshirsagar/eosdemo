<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseCustomerVehicles
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCustomerVehicles extends CEosPluralBase {

	/**
	 * @return CLeaseCustomerVehicle[]
	 */
	public static function fetchLeaseCustomerVehicles( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseCustomerVehicle::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseCustomerVehicle
	 */
	public static function fetchLeaseCustomerVehicle( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseCustomerVehicle::class, $objDatabase );
	}

	public static function fetchLeaseCustomerVehicleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_customer_vehicles', $objDatabase );
	}

	public static function fetchLeaseCustomerVehicleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicle( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicles( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicles( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicles( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerVehicleTypeIdByCid( $intCustomerVehicleTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicles( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE customer_vehicle_type_id = %d AND cid = %d', ( int ) $intCustomerVehicleTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>