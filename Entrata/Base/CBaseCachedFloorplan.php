<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedFloorplan extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_floorplans';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intOccupancyTypeId;
	protected $m_intSpaceConfigurationId;
	protected $m_intLeaseTermId;
	protected $m_intTotalUnitCount;
	protected $m_intRentableUnitCount;
	protected $m_intAvailableUnitCount;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_fltAvgSquareFeet;
	protected $m_fltMinBaseRent;
	protected $m_fltMaxBaseRent;
	protected $m_fltAvgBaseRent;
	protected $m_fltMinAmenityRent;
	protected $m_fltMaxAmenityRent;
	protected $m_fltAvgAmenityRent;
	protected $m_fltMinSpecialRent;
	protected $m_fltMaxSpecialRent;
	protected $m_fltAvgSpecialRent;
	protected $m_fltMinAddOnRent;
	protected $m_fltMaxAddOnRent;
	protected $m_fltAvgAddOnRent;
	protected $m_fltMinMarketRent;
	protected $m_fltMaxMarketRent;
	protected $m_fltAvgMarketRent;
	protected $m_fltManualMinMarketRent;
	protected $m_fltManualMaxMarketRent;
	protected $m_fltMinBaseDeposit;
	protected $m_fltMaxBaseDeposit;
	protected $m_fltAvgBaseDeposit;
	protected $m_fltMinAmenityDeposit;
	protected $m_fltMaxAmenityDeposit;
	protected $m_fltAvgAmenityDeposit;
	protected $m_fltMinSpecialDeposit;
	protected $m_fltMaxSpecialDeposit;
	protected $m_fltAvgSpecialDeposit;
	protected $m_fltMinAddOnDeposit;
	protected $m_fltMaxAddOnDeposit;
	protected $m_fltAvgAddOnDeposit;
	protected $m_fltMinTotalDeposit;
	protected $m_fltMaxTotalDeposit;
	protected $m_fltAvgTotalDeposit;
	protected $m_fltManualMinTotalDeposit;
	protected $m_fltManualMaxTotalDeposit;
	protected $m_fltEntrataMinBaseRent;
	protected $m_fltEntrataMaxBaseRent;
	protected $m_fltEntrataAvgBaseRent;
	protected $m_fltEntrataMinAmenityRent;
	protected $m_fltEntrataMaxAmenityRent;
	protected $m_fltEntrataAvgAmenityRent;
	protected $m_fltEntrataMinSpecialRent;
	protected $m_fltEntrataMaxSpecialRent;
	protected $m_fltEntrataAvgSpecialRent;
	protected $m_fltEntrataMinAddOnRent;
	protected $m_fltEntrataMaxAddOnRent;
	protected $m_fltEntrataAvgAddOnRent;
	protected $m_fltEntrataMinMarketRent;
	protected $m_fltEntrataMaxMarketRent;
	protected $m_fltEntrataAvgMarketRent;
	protected $m_fltEntrataMinBaseDeposit;
	protected $m_fltEntrataMaxBaseDeposit;
	protected $m_fltEntrataAvgBaseDeposit;
	protected $m_fltEntrataMinAmenityDeposit;
	protected $m_fltEntrataMaxAmenityDeposit;
	protected $m_fltEntrataAvgAmenityDeposit;
	protected $m_fltEntrataMinSpecialDeposit;
	protected $m_fltEntrataMaxSpecialDeposit;
	protected $m_fltEntrataAvgSpecialDeposit;
	protected $m_fltEntrataMinAddOnDeposit;
	protected $m_fltEntrataMaxAddOnDeposit;
	protected $m_fltEntrataAvgAddOnDeposit;
	protected $m_fltEntrataMinTotalDeposit;
	protected $m_fltEntrataMaxTotalDeposit;
	protected $m_fltEntrataAvgTotalDeposit;
	protected $m_arrintUnitSpaceIds;
	protected $m_arrintRentableUnitSpaceIds;
	protected $m_arrintAvailableUnitSpaceIds;
	protected $m_boolIsManualRentRange;
	protected $m_boolIsManualDepositRange;
	protected $m_strEffectiveDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intArTriggerId;
	protected $m_fltMinBaseRentMonthly;
	protected $m_fltMaxBaseRentMonthly;
	protected $m_fltAvgBaseRentMonthly;
	protected $m_fltMinAmenityRentMonthly;
	protected $m_fltMaxAmenityRentMonthly;
	protected $m_fltAvgAmenityRentMonthly;
	protected $m_fltMinSpecialRentMonthly;
	protected $m_fltMaxSpecialRentMonthly;
	protected $m_fltAvgSpecialRentMonthly;
	protected $m_fltMinAddOnRentMonthly;
	protected $m_fltMaxAddOnRentMonthly;
	protected $m_fltAvgAddOnRentMonthly;
	protected $m_fltMinMarketRentMonthly;
	protected $m_fltMaxMarketRentMonthly;
	protected $m_fltAvgMarketRentMonthly;
	protected $m_fltEntrataMinBaseRentMonthly;
	protected $m_fltEntrataMaxBaseRentMonthly;
	protected $m_fltEntrataAvgBaseRentMonthly;
	protected $m_fltEntrataMinAmenityRentMonthly;
	protected $m_fltEntrataMaxAmenityRentMonthly;
	protected $m_fltEntrataAvgAmenityRentMonthly;
	protected $m_fltEntrataMinSpecialRentMonthly;
	protected $m_fltEntrataMaxSpecialRentMonthly;
	protected $m_fltEntrataAvgSpecialRentMonthly;
	protected $m_fltEntrataMinAddOnRentMonthly;
	protected $m_fltEntrataMaxAddOnRentMonthly;
	protected $m_fltEntrataAvgAddOnRentMonthly;
	protected $m_fltEntrataMinMarketRentMonthly;
	protected $m_fltEntrataMaxMarketRentMonthly;
	protected $m_fltEntrataAvgMarketRentMonthly;
	protected $m_boolIncludeTaxInAdvertisedRent;
	protected $m_boolIncludeOtherInAdvertisedRent;
	protected $m_fltMinBaseAmount;
	protected $m_fltMaxBaseAmount;
	protected $m_fltAvgBaseAmount;
	protected $m_fltMinAmenityAmount;
	protected $m_fltMaxAmenityAmount;
	protected $m_fltAvgAmenityAmount;
	protected $m_fltMinSpecialAmount;
	protected $m_fltMaxSpecialAmount;
	protected $m_fltAvgSpecialAmount;
	protected $m_fltMinAddOnAmount;
	protected $m_fltMaxAddOnAmount;
	protected $m_fltAvgAddOnAmount;
	protected $m_fltEntrataMinBaseAmount;
	protected $m_fltEntrataMaxBaseAmount;
	protected $m_fltEntrataAvgBaseAmount;
	protected $m_fltEntrataMinAmenityAmount;
	protected $m_fltEntrataMaxAmenityAmount;
	protected $m_fltEntrataAvgAmenityAmount;
	protected $m_fltEntrataMinSpecialAmount;
	protected $m_fltEntrataMaxSpecialAmount;
	protected $m_fltEntrataAvgSpecialAmount;
	protected $m_fltEntrataMinAddOnAmount;
	protected $m_fltEntrataMaxAddOnAmount;
	protected $m_fltEntrataAvgAddOnAmount;
	protected $m_fltMinBaseTax;
	protected $m_fltMaxBaseTax;
	protected $m_fltAvgBaseTax;
	protected $m_fltMinAmenityTax;
	protected $m_fltMaxAmenityTax;
	protected $m_fltAvgAmenityTax;
	protected $m_fltMinSpecialTax;
	protected $m_fltMaxSpecialTax;
	protected $m_fltAvgSpecialTax;
	protected $m_fltMinAddOnTax;
	protected $m_fltMaxAddOnTax;
	protected $m_fltAvgAddOnTax;
	protected $m_fltEntrataMinBaseTax;
	protected $m_fltEntrataMaxBaseTax;
	protected $m_fltEntrataAvgBaseTax;
	protected $m_fltEntrataMinAmenityTax;
	protected $m_fltEntrataMaxAmenityTax;
	protected $m_fltEntrataAvgAmenityTax;
	protected $m_fltEntrataMinSpecialTax;
	protected $m_fltEntrataMaxSpecialTax;
	protected $m_fltEntrataAvgSpecialTax;
	protected $m_fltEntrataMinAddOnTax;
	protected $m_fltEntrataMaxAddOnTax;
	protected $m_fltEntrataAvgAddOnTax;
	protected $m_fltMinBaseAmountMonthly;
	protected $m_fltMaxBaseAmountMonthly;
	protected $m_fltAvgBaseAmountMonthly;
	protected $m_fltMinBaseTaxMonthly;
	protected $m_fltMaxBaseTaxMonthly;
	protected $m_fltAvgBaseTaxMonthly;
	protected $m_fltMinAmenityAmountMonthly;
	protected $m_fltMaxAmenityAmountMonthly;
	protected $m_fltAvgAmenityAmountMonthly;
	protected $m_fltMinAmenityTaxMonthly;
	protected $m_fltMaxAmenityTaxMonthly;
	protected $m_fltAvgAmenityTaxMonthly;
	protected $m_fltMinSpecialAmountMonthly;
	protected $m_fltMaxSpecialAmountMonthly;
	protected $m_fltAvgSpecialAmountMonthly;
	protected $m_fltMinSpecialTaxMonthly;
	protected $m_fltMaxSpecialTaxMonthly;
	protected $m_fltAvgSpecialTaxMonthly;
	protected $m_fltMinAddOnAmountMonthly;
	protected $m_fltMaxAddOnAmountMonthly;
	protected $m_fltAvgAddOnAmountMonthly;
	protected $m_fltMinAddOnTaxMonthly;
	protected $m_fltMaxAddOnTaxMonthly;
	protected $m_fltAvgAddOnTaxMonthly;
	protected $m_fltEntrataMinBaseAmountMonthly;
	protected $m_fltEntrataMaxBaseAmountMonthly;
	protected $m_fltEntrataAvgBaseAmountMonthly;
	protected $m_fltEntrataMinBaseTaxMonthly;
	protected $m_fltEntrataMaxBaseTaxMonthly;
	protected $m_fltEntrataAvgBaseTaxMonthly;
	protected $m_fltEntrataMinAmenityAmountMonthly;
	protected $m_fltEntrataMaxAmenityAmountMonthly;
	protected $m_fltEntrataAvgAmenityAmountMonthly;
	protected $m_fltEntrataMinAmenityTaxMonthly;
	protected $m_fltEntrataMaxAmenityTaxMonthly;
	protected $m_fltEntrataAvgAmenityTaxMonthly;
	protected $m_fltEntrataMinSpecialAmountMonthly;
	protected $m_fltEntrataMaxSpecialAmountMonthly;
	protected $m_fltEntrataAvgSpecialAmountMonthly;
	protected $m_fltEntrataMinSpecialTaxMonthly;
	protected $m_fltEntrataMaxSpecialTaxMonthly;
	protected $m_fltEntrataAvgSpecialTaxMonthly;
	protected $m_fltEntrataMinAddOnAmountMonthly;
	protected $m_fltEntrataMaxAddOnAmountMonthly;
	protected $m_fltEntrataAvgAddOnAmountMonthly;
	protected $m_fltEntrataMinAddOnTaxMonthly;
	protected $m_fltEntrataMaxAddOnTaxMonthly;
	protected $m_fltEntrataAvgAddOnTaxMonthly;
	protected $m_strEffectiveRange;

	public function __construct() {
		parent::__construct();

		$this->m_intSpaceConfigurationId = '1';
		$this->m_boolIsManualRentRange = false;
		$this->m_boolIsManualDepositRange = false;
		$this->m_boolIncludeTaxInAdvertisedRent = false;
		$this->m_boolIncludeOtherInAdvertisedRent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['space_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intSpaceConfigurationId', trim( $arrValues['space_configuration_id'] ) ); elseif( isset( $arrValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrValues['space_configuration_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['total_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitCount', trim( $arrValues['total_unit_count'] ) ); elseif( isset( $arrValues['total_unit_count'] ) ) $this->setTotalUnitCount( $arrValues['total_unit_count'] );
		if( isset( $arrValues['rentable_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intRentableUnitCount', trim( $arrValues['rentable_unit_count'] ) ); elseif( isset( $arrValues['rentable_unit_count'] ) ) $this->setRentableUnitCount( $arrValues['rentable_unit_count'] );
		if( isset( $arrValues['available_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intAvailableUnitCount', trim( $arrValues['available_unit_count'] ) ); elseif( isset( $arrValues['available_unit_count'] ) ) $this->setAvailableUnitCount( $arrValues['available_unit_count'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['avg_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSquareFeet', trim( $arrValues['avg_square_feet'] ) ); elseif( isset( $arrValues['avg_square_feet'] ) ) $this->setAvgSquareFeet( $arrValues['avg_square_feet'] );
		if( isset( $arrValues['min_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseRent', trim( $arrValues['min_base_rent'] ) ); elseif( isset( $arrValues['min_base_rent'] ) ) $this->setMinBaseRent( $arrValues['min_base_rent'] );
		if( isset( $arrValues['max_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseRent', trim( $arrValues['max_base_rent'] ) ); elseif( isset( $arrValues['max_base_rent'] ) ) $this->setMaxBaseRent( $arrValues['max_base_rent'] );
		if( isset( $arrValues['avg_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseRent', trim( $arrValues['avg_base_rent'] ) ); elseif( isset( $arrValues['avg_base_rent'] ) ) $this->setAvgBaseRent( $arrValues['avg_base_rent'] );
		if( isset( $arrValues['min_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityRent', trim( $arrValues['min_amenity_rent'] ) ); elseif( isset( $arrValues['min_amenity_rent'] ) ) $this->setMinAmenityRent( $arrValues['min_amenity_rent'] );
		if( isset( $arrValues['max_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityRent', trim( $arrValues['max_amenity_rent'] ) ); elseif( isset( $arrValues['max_amenity_rent'] ) ) $this->setMaxAmenityRent( $arrValues['max_amenity_rent'] );
		if( isset( $arrValues['avg_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityRent', trim( $arrValues['avg_amenity_rent'] ) ); elseif( isset( $arrValues['avg_amenity_rent'] ) ) $this->setAvgAmenityRent( $arrValues['avg_amenity_rent'] );
		if( isset( $arrValues['min_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialRent', trim( $arrValues['min_special_rent'] ) ); elseif( isset( $arrValues['min_special_rent'] ) ) $this->setMinSpecialRent( $arrValues['min_special_rent'] );
		if( isset( $arrValues['max_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialRent', trim( $arrValues['max_special_rent'] ) ); elseif( isset( $arrValues['max_special_rent'] ) ) $this->setMaxSpecialRent( $arrValues['max_special_rent'] );
		if( isset( $arrValues['avg_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialRent', trim( $arrValues['avg_special_rent'] ) ); elseif( isset( $arrValues['avg_special_rent'] ) ) $this->setAvgSpecialRent( $arrValues['avg_special_rent'] );
		if( isset( $arrValues['min_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnRent', trim( $arrValues['min_add_on_rent'] ) ); elseif( isset( $arrValues['min_add_on_rent'] ) ) $this->setMinAddOnRent( $arrValues['min_add_on_rent'] );
		if( isset( $arrValues['max_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnRent', trim( $arrValues['max_add_on_rent'] ) ); elseif( isset( $arrValues['max_add_on_rent'] ) ) $this->setMaxAddOnRent( $arrValues['max_add_on_rent'] );
		if( isset( $arrValues['avg_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnRent', trim( $arrValues['avg_add_on_rent'] ) ); elseif( isset( $arrValues['avg_add_on_rent'] ) ) $this->setAvgAddOnRent( $arrValues['avg_add_on_rent'] );
		if( isset( $arrValues['min_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinMarketRent', trim( $arrValues['min_market_rent'] ) ); elseif( isset( $arrValues['min_market_rent'] ) ) $this->setMinMarketRent( $arrValues['min_market_rent'] );
		if( isset( $arrValues['max_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMarketRent', trim( $arrValues['max_market_rent'] ) ); elseif( isset( $arrValues['max_market_rent'] ) ) $this->setMaxMarketRent( $arrValues['max_market_rent'] );
		if( isset( $arrValues['avg_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgMarketRent', trim( $arrValues['avg_market_rent'] ) ); elseif( isset( $arrValues['avg_market_rent'] ) ) $this->setAvgMarketRent( $arrValues['avg_market_rent'] );
		if( isset( $arrValues['manual_min_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltManualMinMarketRent', trim( $arrValues['manual_min_market_rent'] ) ); elseif( isset( $arrValues['manual_min_market_rent'] ) ) $this->setManualMinMarketRent( $arrValues['manual_min_market_rent'] );
		if( isset( $arrValues['manual_max_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltManualMaxMarketRent', trim( $arrValues['manual_max_market_rent'] ) ); elseif( isset( $arrValues['manual_max_market_rent'] ) ) $this->setManualMaxMarketRent( $arrValues['manual_max_market_rent'] );
		if( isset( $arrValues['min_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseDeposit', trim( $arrValues['min_base_deposit'] ) ); elseif( isset( $arrValues['min_base_deposit'] ) ) $this->setMinBaseDeposit( $arrValues['min_base_deposit'] );
		if( isset( $arrValues['max_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseDeposit', trim( $arrValues['max_base_deposit'] ) ); elseif( isset( $arrValues['max_base_deposit'] ) ) $this->setMaxBaseDeposit( $arrValues['max_base_deposit'] );
		if( isset( $arrValues['avg_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseDeposit', trim( $arrValues['avg_base_deposit'] ) ); elseif( isset( $arrValues['avg_base_deposit'] ) ) $this->setAvgBaseDeposit( $arrValues['avg_base_deposit'] );
		if( isset( $arrValues['min_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityDeposit', trim( $arrValues['min_amenity_deposit'] ) ); elseif( isset( $arrValues['min_amenity_deposit'] ) ) $this->setMinAmenityDeposit( $arrValues['min_amenity_deposit'] );
		if( isset( $arrValues['max_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityDeposit', trim( $arrValues['max_amenity_deposit'] ) ); elseif( isset( $arrValues['max_amenity_deposit'] ) ) $this->setMaxAmenityDeposit( $arrValues['max_amenity_deposit'] );
		if( isset( $arrValues['avg_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityDeposit', trim( $arrValues['avg_amenity_deposit'] ) ); elseif( isset( $arrValues['avg_amenity_deposit'] ) ) $this->setAvgAmenityDeposit( $arrValues['avg_amenity_deposit'] );
		if( isset( $arrValues['min_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialDeposit', trim( $arrValues['min_special_deposit'] ) ); elseif( isset( $arrValues['min_special_deposit'] ) ) $this->setMinSpecialDeposit( $arrValues['min_special_deposit'] );
		if( isset( $arrValues['max_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialDeposit', trim( $arrValues['max_special_deposit'] ) ); elseif( isset( $arrValues['max_special_deposit'] ) ) $this->setMaxSpecialDeposit( $arrValues['max_special_deposit'] );
		if( isset( $arrValues['avg_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialDeposit', trim( $arrValues['avg_special_deposit'] ) ); elseif( isset( $arrValues['avg_special_deposit'] ) ) $this->setAvgSpecialDeposit( $arrValues['avg_special_deposit'] );
		if( isset( $arrValues['min_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnDeposit', trim( $arrValues['min_add_on_deposit'] ) ); elseif( isset( $arrValues['min_add_on_deposit'] ) ) $this->setMinAddOnDeposit( $arrValues['min_add_on_deposit'] );
		if( isset( $arrValues['max_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnDeposit', trim( $arrValues['max_add_on_deposit'] ) ); elseif( isset( $arrValues['max_add_on_deposit'] ) ) $this->setMaxAddOnDeposit( $arrValues['max_add_on_deposit'] );
		if( isset( $arrValues['avg_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnDeposit', trim( $arrValues['avg_add_on_deposit'] ) ); elseif( isset( $arrValues['avg_add_on_deposit'] ) ) $this->setAvgAddOnDeposit( $arrValues['avg_add_on_deposit'] );
		if( isset( $arrValues['min_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinTotalDeposit', trim( $arrValues['min_total_deposit'] ) ); elseif( isset( $arrValues['min_total_deposit'] ) ) $this->setMinTotalDeposit( $arrValues['min_total_deposit'] );
		if( isset( $arrValues['max_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxTotalDeposit', trim( $arrValues['max_total_deposit'] ) ); elseif( isset( $arrValues['max_total_deposit'] ) ) $this->setMaxTotalDeposit( $arrValues['max_total_deposit'] );
		if( isset( $arrValues['avg_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAvgTotalDeposit', trim( $arrValues['avg_total_deposit'] ) ); elseif( isset( $arrValues['avg_total_deposit'] ) ) $this->setAvgTotalDeposit( $arrValues['avg_total_deposit'] );
		if( isset( $arrValues['manual_min_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltManualMinTotalDeposit', trim( $arrValues['manual_min_total_deposit'] ) ); elseif( isset( $arrValues['manual_min_total_deposit'] ) ) $this->setManualMinTotalDeposit( $arrValues['manual_min_total_deposit'] );
		if( isset( $arrValues['manual_max_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltManualMaxTotalDeposit', trim( $arrValues['manual_max_total_deposit'] ) ); elseif( isset( $arrValues['manual_max_total_deposit'] ) ) $this->setManualMaxTotalDeposit( $arrValues['manual_max_total_deposit'] );
		if( isset( $arrValues['entrata_min_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseRent', trim( $arrValues['entrata_min_base_rent'] ) ); elseif( isset( $arrValues['entrata_min_base_rent'] ) ) $this->setEntrataMinBaseRent( $arrValues['entrata_min_base_rent'] );
		if( isset( $arrValues['entrata_max_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseRent', trim( $arrValues['entrata_max_base_rent'] ) ); elseif( isset( $arrValues['entrata_max_base_rent'] ) ) $this->setEntrataMaxBaseRent( $arrValues['entrata_max_base_rent'] );
		if( isset( $arrValues['entrata_avg_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseRent', trim( $arrValues['entrata_avg_base_rent'] ) ); elseif( isset( $arrValues['entrata_avg_base_rent'] ) ) $this->setEntrataAvgBaseRent( $arrValues['entrata_avg_base_rent'] );
		if( isset( $arrValues['entrata_min_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityRent', trim( $arrValues['entrata_min_amenity_rent'] ) ); elseif( isset( $arrValues['entrata_min_amenity_rent'] ) ) $this->setEntrataMinAmenityRent( $arrValues['entrata_min_amenity_rent'] );
		if( isset( $arrValues['entrata_max_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityRent', trim( $arrValues['entrata_max_amenity_rent'] ) ); elseif( isset( $arrValues['entrata_max_amenity_rent'] ) ) $this->setEntrataMaxAmenityRent( $arrValues['entrata_max_amenity_rent'] );
		if( isset( $arrValues['entrata_avg_amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityRent', trim( $arrValues['entrata_avg_amenity_rent'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_rent'] ) ) $this->setEntrataAvgAmenityRent( $arrValues['entrata_avg_amenity_rent'] );
		if( isset( $arrValues['entrata_min_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialRent', trim( $arrValues['entrata_min_special_rent'] ) ); elseif( isset( $arrValues['entrata_min_special_rent'] ) ) $this->setEntrataMinSpecialRent( $arrValues['entrata_min_special_rent'] );
		if( isset( $arrValues['entrata_max_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialRent', trim( $arrValues['entrata_max_special_rent'] ) ); elseif( isset( $arrValues['entrata_max_special_rent'] ) ) $this->setEntrataMaxSpecialRent( $arrValues['entrata_max_special_rent'] );
		if( isset( $arrValues['entrata_avg_special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialRent', trim( $arrValues['entrata_avg_special_rent'] ) ); elseif( isset( $arrValues['entrata_avg_special_rent'] ) ) $this->setEntrataAvgSpecialRent( $arrValues['entrata_avg_special_rent'] );
		if( isset( $arrValues['entrata_min_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnRent', trim( $arrValues['entrata_min_add_on_rent'] ) ); elseif( isset( $arrValues['entrata_min_add_on_rent'] ) ) $this->setEntrataMinAddOnRent( $arrValues['entrata_min_add_on_rent'] );
		if( isset( $arrValues['entrata_max_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnRent', trim( $arrValues['entrata_max_add_on_rent'] ) ); elseif( isset( $arrValues['entrata_max_add_on_rent'] ) ) $this->setEntrataMaxAddOnRent( $arrValues['entrata_max_add_on_rent'] );
		if( isset( $arrValues['entrata_avg_add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnRent', trim( $arrValues['entrata_avg_add_on_rent'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_rent'] ) ) $this->setEntrataAvgAddOnRent( $arrValues['entrata_avg_add_on_rent'] );
		if( isset( $arrValues['entrata_min_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinMarketRent', trim( $arrValues['entrata_min_market_rent'] ) ); elseif( isset( $arrValues['entrata_min_market_rent'] ) ) $this->setEntrataMinMarketRent( $arrValues['entrata_min_market_rent'] );
		if( isset( $arrValues['entrata_max_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxMarketRent', trim( $arrValues['entrata_max_market_rent'] ) ); elseif( isset( $arrValues['entrata_max_market_rent'] ) ) $this->setEntrataMaxMarketRent( $arrValues['entrata_max_market_rent'] );
		if( isset( $arrValues['entrata_avg_market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgMarketRent', trim( $arrValues['entrata_avg_market_rent'] ) ); elseif( isset( $arrValues['entrata_avg_market_rent'] ) ) $this->setEntrataAvgMarketRent( $arrValues['entrata_avg_market_rent'] );
		if( isset( $arrValues['entrata_min_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseDeposit', trim( $arrValues['entrata_min_base_deposit'] ) ); elseif( isset( $arrValues['entrata_min_base_deposit'] ) ) $this->setEntrataMinBaseDeposit( $arrValues['entrata_min_base_deposit'] );
		if( isset( $arrValues['entrata_max_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseDeposit', trim( $arrValues['entrata_max_base_deposit'] ) ); elseif( isset( $arrValues['entrata_max_base_deposit'] ) ) $this->setEntrataMaxBaseDeposit( $arrValues['entrata_max_base_deposit'] );
		if( isset( $arrValues['entrata_avg_base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseDeposit', trim( $arrValues['entrata_avg_base_deposit'] ) ); elseif( isset( $arrValues['entrata_avg_base_deposit'] ) ) $this->setEntrataAvgBaseDeposit( $arrValues['entrata_avg_base_deposit'] );
		if( isset( $arrValues['entrata_min_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityDeposit', trim( $arrValues['entrata_min_amenity_deposit'] ) ); elseif( isset( $arrValues['entrata_min_amenity_deposit'] ) ) $this->setEntrataMinAmenityDeposit( $arrValues['entrata_min_amenity_deposit'] );
		if( isset( $arrValues['entrata_max_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityDeposit', trim( $arrValues['entrata_max_amenity_deposit'] ) ); elseif( isset( $arrValues['entrata_max_amenity_deposit'] ) ) $this->setEntrataMaxAmenityDeposit( $arrValues['entrata_max_amenity_deposit'] );
		if( isset( $arrValues['entrata_avg_amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityDeposit', trim( $arrValues['entrata_avg_amenity_deposit'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_deposit'] ) ) $this->setEntrataAvgAmenityDeposit( $arrValues['entrata_avg_amenity_deposit'] );
		if( isset( $arrValues['entrata_min_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialDeposit', trim( $arrValues['entrata_min_special_deposit'] ) ); elseif( isset( $arrValues['entrata_min_special_deposit'] ) ) $this->setEntrataMinSpecialDeposit( $arrValues['entrata_min_special_deposit'] );
		if( isset( $arrValues['entrata_max_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialDeposit', trim( $arrValues['entrata_max_special_deposit'] ) ); elseif( isset( $arrValues['entrata_max_special_deposit'] ) ) $this->setEntrataMaxSpecialDeposit( $arrValues['entrata_max_special_deposit'] );
		if( isset( $arrValues['entrata_avg_special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialDeposit', trim( $arrValues['entrata_avg_special_deposit'] ) ); elseif( isset( $arrValues['entrata_avg_special_deposit'] ) ) $this->setEntrataAvgSpecialDeposit( $arrValues['entrata_avg_special_deposit'] );
		if( isset( $arrValues['entrata_min_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnDeposit', trim( $arrValues['entrata_min_add_on_deposit'] ) ); elseif( isset( $arrValues['entrata_min_add_on_deposit'] ) ) $this->setEntrataMinAddOnDeposit( $arrValues['entrata_min_add_on_deposit'] );
		if( isset( $arrValues['entrata_max_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnDeposit', trim( $arrValues['entrata_max_add_on_deposit'] ) ); elseif( isset( $arrValues['entrata_max_add_on_deposit'] ) ) $this->setEntrataMaxAddOnDeposit( $arrValues['entrata_max_add_on_deposit'] );
		if( isset( $arrValues['entrata_avg_add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnDeposit', trim( $arrValues['entrata_avg_add_on_deposit'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_deposit'] ) ) $this->setEntrataAvgAddOnDeposit( $arrValues['entrata_avg_add_on_deposit'] );
		if( isset( $arrValues['entrata_min_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinTotalDeposit', trim( $arrValues['entrata_min_total_deposit'] ) ); elseif( isset( $arrValues['entrata_min_total_deposit'] ) ) $this->setEntrataMinTotalDeposit( $arrValues['entrata_min_total_deposit'] );
		if( isset( $arrValues['entrata_max_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxTotalDeposit', trim( $arrValues['entrata_max_total_deposit'] ) ); elseif( isset( $arrValues['entrata_max_total_deposit'] ) ) $this->setEntrataMaxTotalDeposit( $arrValues['entrata_max_total_deposit'] );
		if( isset( $arrValues['entrata_avg_total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgTotalDeposit', trim( $arrValues['entrata_avg_total_deposit'] ) ); elseif( isset( $arrValues['entrata_avg_total_deposit'] ) ) $this->setEntrataAvgTotalDeposit( $arrValues['entrata_avg_total_deposit'] );
		if( isset( $arrValues['unit_space_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUnitSpaceIds', trim( $arrValues['unit_space_ids'] ) ); elseif( isset( $arrValues['unit_space_ids'] ) ) $this->setUnitSpaceIds( $arrValues['unit_space_ids'] );
		if( isset( $arrValues['rentable_unit_space_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintRentableUnitSpaceIds', trim( $arrValues['rentable_unit_space_ids'] ) ); elseif( isset( $arrValues['rentable_unit_space_ids'] ) ) $this->setRentableUnitSpaceIds( $arrValues['rentable_unit_space_ids'] );
		if( isset( $arrValues['available_unit_space_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintAvailableUnitSpaceIds', trim( $arrValues['available_unit_space_ids'] ) ); elseif( isset( $arrValues['available_unit_space_ids'] ) ) $this->setAvailableUnitSpaceIds( $arrValues['available_unit_space_ids'] );
		if( isset( $arrValues['is_manual_rent_range'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualRentRange', trim( stripcslashes( $arrValues['is_manual_rent_range'] ) ) ); elseif( isset( $arrValues['is_manual_rent_range'] ) ) $this->setIsManualRentRange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_rent_range'] ) : $arrValues['is_manual_rent_range'] );
		if( isset( $arrValues['is_manual_deposit_range'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualDepositRange', trim( stripcslashes( $arrValues['is_manual_deposit_range'] ) ) ); elseif( isset( $arrValues['is_manual_deposit_range'] ) ) $this->setIsManualDepositRange( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_deposit_range'] ) : $arrValues['is_manual_deposit_range'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['min_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseRentMonthly', trim( $arrValues['min_base_rent_monthly'] ) ); elseif( isset( $arrValues['min_base_rent_monthly'] ) ) $this->setMinBaseRentMonthly( $arrValues['min_base_rent_monthly'] );
		if( isset( $arrValues['max_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseRentMonthly', trim( $arrValues['max_base_rent_monthly'] ) ); elseif( isset( $arrValues['max_base_rent_monthly'] ) ) $this->setMaxBaseRentMonthly( $arrValues['max_base_rent_monthly'] );
		if( isset( $arrValues['avg_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseRentMonthly', trim( $arrValues['avg_base_rent_monthly'] ) ); elseif( isset( $arrValues['avg_base_rent_monthly'] ) ) $this->setAvgBaseRentMonthly( $arrValues['avg_base_rent_monthly'] );
		if( isset( $arrValues['min_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityRentMonthly', trim( $arrValues['min_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['min_amenity_rent_monthly'] ) ) $this->setMinAmenityRentMonthly( $arrValues['min_amenity_rent_monthly'] );
		if( isset( $arrValues['max_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityRentMonthly', trim( $arrValues['max_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['max_amenity_rent_monthly'] ) ) $this->setMaxAmenityRentMonthly( $arrValues['max_amenity_rent_monthly'] );
		if( isset( $arrValues['avg_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityRentMonthly', trim( $arrValues['avg_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['avg_amenity_rent_monthly'] ) ) $this->setAvgAmenityRentMonthly( $arrValues['avg_amenity_rent_monthly'] );
		if( isset( $arrValues['min_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialRentMonthly', trim( $arrValues['min_special_rent_monthly'] ) ); elseif( isset( $arrValues['min_special_rent_monthly'] ) ) $this->setMinSpecialRentMonthly( $arrValues['min_special_rent_monthly'] );
		if( isset( $arrValues['max_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialRentMonthly', trim( $arrValues['max_special_rent_monthly'] ) ); elseif( isset( $arrValues['max_special_rent_monthly'] ) ) $this->setMaxSpecialRentMonthly( $arrValues['max_special_rent_monthly'] );
		if( isset( $arrValues['avg_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialRentMonthly', trim( $arrValues['avg_special_rent_monthly'] ) ); elseif( isset( $arrValues['avg_special_rent_monthly'] ) ) $this->setAvgSpecialRentMonthly( $arrValues['avg_special_rent_monthly'] );
		if( isset( $arrValues['min_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnRentMonthly', trim( $arrValues['min_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['min_add_on_rent_monthly'] ) ) $this->setMinAddOnRentMonthly( $arrValues['min_add_on_rent_monthly'] );
		if( isset( $arrValues['max_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnRentMonthly', trim( $arrValues['max_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['max_add_on_rent_monthly'] ) ) $this->setMaxAddOnRentMonthly( $arrValues['max_add_on_rent_monthly'] );
		if( isset( $arrValues['avg_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnRentMonthly', trim( $arrValues['avg_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['avg_add_on_rent_monthly'] ) ) $this->setAvgAddOnRentMonthly( $arrValues['avg_add_on_rent_monthly'] );
		if( isset( $arrValues['min_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinMarketRentMonthly', trim( $arrValues['min_market_rent_monthly'] ) ); elseif( isset( $arrValues['min_market_rent_monthly'] ) ) $this->setMinMarketRentMonthly( $arrValues['min_market_rent_monthly'] );
		if( isset( $arrValues['max_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMarketRentMonthly', trim( $arrValues['max_market_rent_monthly'] ) ); elseif( isset( $arrValues['max_market_rent_monthly'] ) ) $this->setMaxMarketRentMonthly( $arrValues['max_market_rent_monthly'] );
		if( isset( $arrValues['avg_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgMarketRentMonthly', trim( $arrValues['avg_market_rent_monthly'] ) ); elseif( isset( $arrValues['avg_market_rent_monthly'] ) ) $this->setAvgMarketRentMonthly( $arrValues['avg_market_rent_monthly'] );
		if( isset( $arrValues['entrata_min_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseRentMonthly', trim( $arrValues['entrata_min_base_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_min_base_rent_monthly'] ) ) $this->setEntrataMinBaseRentMonthly( $arrValues['entrata_min_base_rent_monthly'] );
		if( isset( $arrValues['entrata_max_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseRentMonthly', trim( $arrValues['entrata_max_base_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_max_base_rent_monthly'] ) ) $this->setEntrataMaxBaseRentMonthly( $arrValues['entrata_max_base_rent_monthly'] );
		if( isset( $arrValues['entrata_avg_base_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseRentMonthly', trim( $arrValues['entrata_avg_base_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_base_rent_monthly'] ) ) $this->setEntrataAvgBaseRentMonthly( $arrValues['entrata_avg_base_rent_monthly'] );
		if( isset( $arrValues['entrata_min_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityRentMonthly', trim( $arrValues['entrata_min_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_min_amenity_rent_monthly'] ) ) $this->setEntrataMinAmenityRentMonthly( $arrValues['entrata_min_amenity_rent_monthly'] );
		if( isset( $arrValues['entrata_max_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityRentMonthly', trim( $arrValues['entrata_max_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_max_amenity_rent_monthly'] ) ) $this->setEntrataMaxAmenityRentMonthly( $arrValues['entrata_max_amenity_rent_monthly'] );
		if( isset( $arrValues['entrata_avg_amenity_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityRentMonthly', trim( $arrValues['entrata_avg_amenity_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_rent_monthly'] ) ) $this->setEntrataAvgAmenityRentMonthly( $arrValues['entrata_avg_amenity_rent_monthly'] );
		if( isset( $arrValues['entrata_min_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialRentMonthly', trim( $arrValues['entrata_min_special_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_min_special_rent_monthly'] ) ) $this->setEntrataMinSpecialRentMonthly( $arrValues['entrata_min_special_rent_monthly'] );
		if( isset( $arrValues['entrata_max_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialRentMonthly', trim( $arrValues['entrata_max_special_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_max_special_rent_monthly'] ) ) $this->setEntrataMaxSpecialRentMonthly( $arrValues['entrata_max_special_rent_monthly'] );
		if( isset( $arrValues['entrata_avg_special_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialRentMonthly', trim( $arrValues['entrata_avg_special_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_special_rent_monthly'] ) ) $this->setEntrataAvgSpecialRentMonthly( $arrValues['entrata_avg_special_rent_monthly'] );
		if( isset( $arrValues['entrata_min_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnRentMonthly', trim( $arrValues['entrata_min_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_min_add_on_rent_monthly'] ) ) $this->setEntrataMinAddOnRentMonthly( $arrValues['entrata_min_add_on_rent_monthly'] );
		if( isset( $arrValues['entrata_max_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnRentMonthly', trim( $arrValues['entrata_max_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_max_add_on_rent_monthly'] ) ) $this->setEntrataMaxAddOnRentMonthly( $arrValues['entrata_max_add_on_rent_monthly'] );
		if( isset( $arrValues['entrata_avg_add_on_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnRentMonthly', trim( $arrValues['entrata_avg_add_on_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_rent_monthly'] ) ) $this->setEntrataAvgAddOnRentMonthly( $arrValues['entrata_avg_add_on_rent_monthly'] );
		if( isset( $arrValues['entrata_min_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinMarketRentMonthly', trim( $arrValues['entrata_min_market_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_min_market_rent_monthly'] ) ) $this->setEntrataMinMarketRentMonthly( $arrValues['entrata_min_market_rent_monthly'] );
		if( isset( $arrValues['entrata_max_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxMarketRentMonthly', trim( $arrValues['entrata_max_market_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_max_market_rent_monthly'] ) ) $this->setEntrataMaxMarketRentMonthly( $arrValues['entrata_max_market_rent_monthly'] );
		if( isset( $arrValues['entrata_avg_market_rent_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgMarketRentMonthly', trim( $arrValues['entrata_avg_market_rent_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_market_rent_monthly'] ) ) $this->setEntrataAvgMarketRentMonthly( $arrValues['entrata_avg_market_rent_monthly'] );
		if( isset( $arrValues['include_tax_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeTaxInAdvertisedRent', trim( stripcslashes( $arrValues['include_tax_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_tax_in_advertised_rent'] ) ) $this->setIncludeTaxInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_tax_in_advertised_rent'] ) : $arrValues['include_tax_in_advertised_rent'] );
		if( isset( $arrValues['include_other_in_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeOtherInAdvertisedRent', trim( stripcslashes( $arrValues['include_other_in_advertised_rent'] ) ) ); elseif( isset( $arrValues['include_other_in_advertised_rent'] ) ) $this->setIncludeOtherInAdvertisedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_other_in_advertised_rent'] ) : $arrValues['include_other_in_advertised_rent'] );
		if( isset( $arrValues['min_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseAmount', trim( $arrValues['min_base_amount'] ) ); elseif( isset( $arrValues['min_base_amount'] ) ) $this->setMinBaseAmount( $arrValues['min_base_amount'] );
		if( isset( $arrValues['max_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseAmount', trim( $arrValues['max_base_amount'] ) ); elseif( isset( $arrValues['max_base_amount'] ) ) $this->setMaxBaseAmount( $arrValues['max_base_amount'] );
		if( isset( $arrValues['avg_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseAmount', trim( $arrValues['avg_base_amount'] ) ); elseif( isset( $arrValues['avg_base_amount'] ) ) $this->setAvgBaseAmount( $arrValues['avg_base_amount'] );
		if( isset( $arrValues['min_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityAmount', trim( $arrValues['min_amenity_amount'] ) ); elseif( isset( $arrValues['min_amenity_amount'] ) ) $this->setMinAmenityAmount( $arrValues['min_amenity_amount'] );
		if( isset( $arrValues['max_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityAmount', trim( $arrValues['max_amenity_amount'] ) ); elseif( isset( $arrValues['max_amenity_amount'] ) ) $this->setMaxAmenityAmount( $arrValues['max_amenity_amount'] );
		if( isset( $arrValues['avg_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityAmount', trim( $arrValues['avg_amenity_amount'] ) ); elseif( isset( $arrValues['avg_amenity_amount'] ) ) $this->setAvgAmenityAmount( $arrValues['avg_amenity_amount'] );
		if( isset( $arrValues['min_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialAmount', trim( $arrValues['min_special_amount'] ) ); elseif( isset( $arrValues['min_special_amount'] ) ) $this->setMinSpecialAmount( $arrValues['min_special_amount'] );
		if( isset( $arrValues['max_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialAmount', trim( $arrValues['max_special_amount'] ) ); elseif( isset( $arrValues['max_special_amount'] ) ) $this->setMaxSpecialAmount( $arrValues['max_special_amount'] );
		if( isset( $arrValues['avg_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialAmount', trim( $arrValues['avg_special_amount'] ) ); elseif( isset( $arrValues['avg_special_amount'] ) ) $this->setAvgSpecialAmount( $arrValues['avg_special_amount'] );
		if( isset( $arrValues['min_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnAmount', trim( $arrValues['min_add_on_amount'] ) ); elseif( isset( $arrValues['min_add_on_amount'] ) ) $this->setMinAddOnAmount( $arrValues['min_add_on_amount'] );
		if( isset( $arrValues['max_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnAmount', trim( $arrValues['max_add_on_amount'] ) ); elseif( isset( $arrValues['max_add_on_amount'] ) ) $this->setMaxAddOnAmount( $arrValues['max_add_on_amount'] );
		if( isset( $arrValues['avg_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnAmount', trim( $arrValues['avg_add_on_amount'] ) ); elseif( isset( $arrValues['avg_add_on_amount'] ) ) $this->setAvgAddOnAmount( $arrValues['avg_add_on_amount'] );
		if( isset( $arrValues['entrata_min_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseAmount', trim( $arrValues['entrata_min_base_amount'] ) ); elseif( isset( $arrValues['entrata_min_base_amount'] ) ) $this->setEntrataMinBaseAmount( $arrValues['entrata_min_base_amount'] );
		if( isset( $arrValues['entrata_max_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseAmount', trim( $arrValues['entrata_max_base_amount'] ) ); elseif( isset( $arrValues['entrata_max_base_amount'] ) ) $this->setEntrataMaxBaseAmount( $arrValues['entrata_max_base_amount'] );
		if( isset( $arrValues['entrata_avg_base_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseAmount', trim( $arrValues['entrata_avg_base_amount'] ) ); elseif( isset( $arrValues['entrata_avg_base_amount'] ) ) $this->setEntrataAvgBaseAmount( $arrValues['entrata_avg_base_amount'] );
		if( isset( $arrValues['entrata_min_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityAmount', trim( $arrValues['entrata_min_amenity_amount'] ) ); elseif( isset( $arrValues['entrata_min_amenity_amount'] ) ) $this->setEntrataMinAmenityAmount( $arrValues['entrata_min_amenity_amount'] );
		if( isset( $arrValues['entrata_max_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityAmount', trim( $arrValues['entrata_max_amenity_amount'] ) ); elseif( isset( $arrValues['entrata_max_amenity_amount'] ) ) $this->setEntrataMaxAmenityAmount( $arrValues['entrata_max_amenity_amount'] );
		if( isset( $arrValues['entrata_avg_amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityAmount', trim( $arrValues['entrata_avg_amenity_amount'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_amount'] ) ) $this->setEntrataAvgAmenityAmount( $arrValues['entrata_avg_amenity_amount'] );
		if( isset( $arrValues['entrata_min_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialAmount', trim( $arrValues['entrata_min_special_amount'] ) ); elseif( isset( $arrValues['entrata_min_special_amount'] ) ) $this->setEntrataMinSpecialAmount( $arrValues['entrata_min_special_amount'] );
		if( isset( $arrValues['entrata_max_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialAmount', trim( $arrValues['entrata_max_special_amount'] ) ); elseif( isset( $arrValues['entrata_max_special_amount'] ) ) $this->setEntrataMaxSpecialAmount( $arrValues['entrata_max_special_amount'] );
		if( isset( $arrValues['entrata_avg_special_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialAmount', trim( $arrValues['entrata_avg_special_amount'] ) ); elseif( isset( $arrValues['entrata_avg_special_amount'] ) ) $this->setEntrataAvgSpecialAmount( $arrValues['entrata_avg_special_amount'] );
		if( isset( $arrValues['entrata_min_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnAmount', trim( $arrValues['entrata_min_add_on_amount'] ) ); elseif( isset( $arrValues['entrata_min_add_on_amount'] ) ) $this->setEntrataMinAddOnAmount( $arrValues['entrata_min_add_on_amount'] );
		if( isset( $arrValues['entrata_max_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnAmount', trim( $arrValues['entrata_max_add_on_amount'] ) ); elseif( isset( $arrValues['entrata_max_add_on_amount'] ) ) $this->setEntrataMaxAddOnAmount( $arrValues['entrata_max_add_on_amount'] );
		if( isset( $arrValues['entrata_avg_add_on_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnAmount', trim( $arrValues['entrata_avg_add_on_amount'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_amount'] ) ) $this->setEntrataAvgAddOnAmount( $arrValues['entrata_avg_add_on_amount'] );
		if( isset( $arrValues['min_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseTax', trim( $arrValues['min_base_tax'] ) ); elseif( isset( $arrValues['min_base_tax'] ) ) $this->setMinBaseTax( $arrValues['min_base_tax'] );
		if( isset( $arrValues['max_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseTax', trim( $arrValues['max_base_tax'] ) ); elseif( isset( $arrValues['max_base_tax'] ) ) $this->setMaxBaseTax( $arrValues['max_base_tax'] );
		if( isset( $arrValues['avg_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseTax', trim( $arrValues['avg_base_tax'] ) ); elseif( isset( $arrValues['avg_base_tax'] ) ) $this->setAvgBaseTax( $arrValues['avg_base_tax'] );
		if( isset( $arrValues['min_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityTax', trim( $arrValues['min_amenity_tax'] ) ); elseif( isset( $arrValues['min_amenity_tax'] ) ) $this->setMinAmenityTax( $arrValues['min_amenity_tax'] );
		if( isset( $arrValues['max_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityTax', trim( $arrValues['max_amenity_tax'] ) ); elseif( isset( $arrValues['max_amenity_tax'] ) ) $this->setMaxAmenityTax( $arrValues['max_amenity_tax'] );
		if( isset( $arrValues['avg_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityTax', trim( $arrValues['avg_amenity_tax'] ) ); elseif( isset( $arrValues['avg_amenity_tax'] ) ) $this->setAvgAmenityTax( $arrValues['avg_amenity_tax'] );
		if( isset( $arrValues['min_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialTax', trim( $arrValues['min_special_tax'] ) ); elseif( isset( $arrValues['min_special_tax'] ) ) $this->setMinSpecialTax( $arrValues['min_special_tax'] );
		if( isset( $arrValues['max_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialTax', trim( $arrValues['max_special_tax'] ) ); elseif( isset( $arrValues['max_special_tax'] ) ) $this->setMaxSpecialTax( $arrValues['max_special_tax'] );
		if( isset( $arrValues['avg_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialTax', trim( $arrValues['avg_special_tax'] ) ); elseif( isset( $arrValues['avg_special_tax'] ) ) $this->setAvgSpecialTax( $arrValues['avg_special_tax'] );
		if( isset( $arrValues['min_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnTax', trim( $arrValues['min_add_on_tax'] ) ); elseif( isset( $arrValues['min_add_on_tax'] ) ) $this->setMinAddOnTax( $arrValues['min_add_on_tax'] );
		if( isset( $arrValues['max_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnTax', trim( $arrValues['max_add_on_tax'] ) ); elseif( isset( $arrValues['max_add_on_tax'] ) ) $this->setMaxAddOnTax( $arrValues['max_add_on_tax'] );
		if( isset( $arrValues['avg_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnTax', trim( $arrValues['avg_add_on_tax'] ) ); elseif( isset( $arrValues['avg_add_on_tax'] ) ) $this->setAvgAddOnTax( $arrValues['avg_add_on_tax'] );
		if( isset( $arrValues['entrata_min_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseTax', trim( $arrValues['entrata_min_base_tax'] ) ); elseif( isset( $arrValues['entrata_min_base_tax'] ) ) $this->setEntrataMinBaseTax( $arrValues['entrata_min_base_tax'] );
		if( isset( $arrValues['entrata_max_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseTax', trim( $arrValues['entrata_max_base_tax'] ) ); elseif( isset( $arrValues['entrata_max_base_tax'] ) ) $this->setEntrataMaxBaseTax( $arrValues['entrata_max_base_tax'] );
		if( isset( $arrValues['entrata_avg_base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseTax', trim( $arrValues['entrata_avg_base_tax'] ) ); elseif( isset( $arrValues['entrata_avg_base_tax'] ) ) $this->setEntrataAvgBaseTax( $arrValues['entrata_avg_base_tax'] );
		if( isset( $arrValues['entrata_min_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityTax', trim( $arrValues['entrata_min_amenity_tax'] ) ); elseif( isset( $arrValues['entrata_min_amenity_tax'] ) ) $this->setEntrataMinAmenityTax( $arrValues['entrata_min_amenity_tax'] );
		if( isset( $arrValues['entrata_max_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityTax', trim( $arrValues['entrata_max_amenity_tax'] ) ); elseif( isset( $arrValues['entrata_max_amenity_tax'] ) ) $this->setEntrataMaxAmenityTax( $arrValues['entrata_max_amenity_tax'] );
		if( isset( $arrValues['entrata_avg_amenity_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityTax', trim( $arrValues['entrata_avg_amenity_tax'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_tax'] ) ) $this->setEntrataAvgAmenityTax( $arrValues['entrata_avg_amenity_tax'] );
		if( isset( $arrValues['entrata_min_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialTax', trim( $arrValues['entrata_min_special_tax'] ) ); elseif( isset( $arrValues['entrata_min_special_tax'] ) ) $this->setEntrataMinSpecialTax( $arrValues['entrata_min_special_tax'] );
		if( isset( $arrValues['entrata_max_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialTax', trim( $arrValues['entrata_max_special_tax'] ) ); elseif( isset( $arrValues['entrata_max_special_tax'] ) ) $this->setEntrataMaxSpecialTax( $arrValues['entrata_max_special_tax'] );
		if( isset( $arrValues['entrata_avg_special_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialTax', trim( $arrValues['entrata_avg_special_tax'] ) ); elseif( isset( $arrValues['entrata_avg_special_tax'] ) ) $this->setEntrataAvgSpecialTax( $arrValues['entrata_avg_special_tax'] );
		if( isset( $arrValues['entrata_min_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnTax', trim( $arrValues['entrata_min_add_on_tax'] ) ); elseif( isset( $arrValues['entrata_min_add_on_tax'] ) ) $this->setEntrataMinAddOnTax( $arrValues['entrata_min_add_on_tax'] );
		if( isset( $arrValues['entrata_max_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnTax', trim( $arrValues['entrata_max_add_on_tax'] ) ); elseif( isset( $arrValues['entrata_max_add_on_tax'] ) ) $this->setEntrataMaxAddOnTax( $arrValues['entrata_max_add_on_tax'] );
		if( isset( $arrValues['entrata_avg_add_on_tax'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnTax', trim( $arrValues['entrata_avg_add_on_tax'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_tax'] ) ) $this->setEntrataAvgAddOnTax( $arrValues['entrata_avg_add_on_tax'] );
		if( isset( $arrValues['min_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseAmountMonthly', trim( $arrValues['min_base_amount_monthly'] ) ); elseif( isset( $arrValues['min_base_amount_monthly'] ) ) $this->setMinBaseAmountMonthly( $arrValues['min_base_amount_monthly'] );
		if( isset( $arrValues['max_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseAmountMonthly', trim( $arrValues['max_base_amount_monthly'] ) ); elseif( isset( $arrValues['max_base_amount_monthly'] ) ) $this->setMaxBaseAmountMonthly( $arrValues['max_base_amount_monthly'] );
		if( isset( $arrValues['avg_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseAmountMonthly', trim( $arrValues['avg_base_amount_monthly'] ) ); elseif( isset( $arrValues['avg_base_amount_monthly'] ) ) $this->setAvgBaseAmountMonthly( $arrValues['avg_base_amount_monthly'] );
		if( isset( $arrValues['min_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinBaseTaxMonthly', trim( $arrValues['min_base_tax_monthly'] ) ); elseif( isset( $arrValues['min_base_tax_monthly'] ) ) $this->setMinBaseTaxMonthly( $arrValues['min_base_tax_monthly'] );
		if( isset( $arrValues['max_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBaseTaxMonthly', trim( $arrValues['max_base_tax_monthly'] ) ); elseif( isset( $arrValues['max_base_tax_monthly'] ) ) $this->setMaxBaseTaxMonthly( $arrValues['max_base_tax_monthly'] );
		if( isset( $arrValues['avg_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgBaseTaxMonthly', trim( $arrValues['avg_base_tax_monthly'] ) ); elseif( isset( $arrValues['avg_base_tax_monthly'] ) ) $this->setAvgBaseTaxMonthly( $arrValues['avg_base_tax_monthly'] );
		if( isset( $arrValues['min_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityAmountMonthly', trim( $arrValues['min_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['min_amenity_amount_monthly'] ) ) $this->setMinAmenityAmountMonthly( $arrValues['min_amenity_amount_monthly'] );
		if( isset( $arrValues['max_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityAmountMonthly', trim( $arrValues['max_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['max_amenity_amount_monthly'] ) ) $this->setMaxAmenityAmountMonthly( $arrValues['max_amenity_amount_monthly'] );
		if( isset( $arrValues['avg_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityAmountMonthly', trim( $arrValues['avg_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['avg_amenity_amount_monthly'] ) ) $this->setAvgAmenityAmountMonthly( $arrValues['avg_amenity_amount_monthly'] );
		if( isset( $arrValues['min_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAmenityTaxMonthly', trim( $arrValues['min_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['min_amenity_tax_monthly'] ) ) $this->setMinAmenityTaxMonthly( $arrValues['min_amenity_tax_monthly'] );
		if( isset( $arrValues['max_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAmenityTaxMonthly', trim( $arrValues['max_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['max_amenity_tax_monthly'] ) ) $this->setMaxAmenityTaxMonthly( $arrValues['max_amenity_tax_monthly'] );
		if( isset( $arrValues['avg_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAmenityTaxMonthly', trim( $arrValues['avg_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['avg_amenity_tax_monthly'] ) ) $this->setAvgAmenityTaxMonthly( $arrValues['avg_amenity_tax_monthly'] );
		if( isset( $arrValues['min_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialAmountMonthly', trim( $arrValues['min_special_amount_monthly'] ) ); elseif( isset( $arrValues['min_special_amount_monthly'] ) ) $this->setMinSpecialAmountMonthly( $arrValues['min_special_amount_monthly'] );
		if( isset( $arrValues['max_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialAmountMonthly', trim( $arrValues['max_special_amount_monthly'] ) ); elseif( isset( $arrValues['max_special_amount_monthly'] ) ) $this->setMaxSpecialAmountMonthly( $arrValues['max_special_amount_monthly'] );
		if( isset( $arrValues['avg_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialAmountMonthly', trim( $arrValues['avg_special_amount_monthly'] ) ); elseif( isset( $arrValues['avg_special_amount_monthly'] ) ) $this->setAvgSpecialAmountMonthly( $arrValues['avg_special_amount_monthly'] );
		if( isset( $arrValues['min_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinSpecialTaxMonthly', trim( $arrValues['min_special_tax_monthly'] ) ); elseif( isset( $arrValues['min_special_tax_monthly'] ) ) $this->setMinSpecialTaxMonthly( $arrValues['min_special_tax_monthly'] );
		if( isset( $arrValues['max_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSpecialTaxMonthly', trim( $arrValues['max_special_tax_monthly'] ) ); elseif( isset( $arrValues['max_special_tax_monthly'] ) ) $this->setMaxSpecialTaxMonthly( $arrValues['max_special_tax_monthly'] );
		if( isset( $arrValues['avg_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgSpecialTaxMonthly', trim( $arrValues['avg_special_tax_monthly'] ) ); elseif( isset( $arrValues['avg_special_tax_monthly'] ) ) $this->setAvgSpecialTaxMonthly( $arrValues['avg_special_tax_monthly'] );
		if( isset( $arrValues['min_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnAmountMonthly', trim( $arrValues['min_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['min_add_on_amount_monthly'] ) ) $this->setMinAddOnAmountMonthly( $arrValues['min_add_on_amount_monthly'] );
		if( isset( $arrValues['max_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnAmountMonthly', trim( $arrValues['max_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['max_add_on_amount_monthly'] ) ) $this->setMaxAddOnAmountMonthly( $arrValues['max_add_on_amount_monthly'] );
		if( isset( $arrValues['avg_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnAmountMonthly', trim( $arrValues['avg_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['avg_add_on_amount_monthly'] ) ) $this->setAvgAddOnAmountMonthly( $arrValues['avg_add_on_amount_monthly'] );
		if( isset( $arrValues['min_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMinAddOnTaxMonthly', trim( $arrValues['min_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['min_add_on_tax_monthly'] ) ) $this->setMinAddOnTaxMonthly( $arrValues['min_add_on_tax_monthly'] );
		if( isset( $arrValues['max_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAddOnTaxMonthly', trim( $arrValues['max_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['max_add_on_tax_monthly'] ) ) $this->setMaxAddOnTaxMonthly( $arrValues['max_add_on_tax_monthly'] );
		if( isset( $arrValues['avg_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAddOnTaxMonthly', trim( $arrValues['avg_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['avg_add_on_tax_monthly'] ) ) $this->setAvgAddOnTaxMonthly( $arrValues['avg_add_on_tax_monthly'] );
		if( isset( $arrValues['entrata_min_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseAmountMonthly', trim( $arrValues['entrata_min_base_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_min_base_amount_monthly'] ) ) $this->setEntrataMinBaseAmountMonthly( $arrValues['entrata_min_base_amount_monthly'] );
		if( isset( $arrValues['entrata_max_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseAmountMonthly', trim( $arrValues['entrata_max_base_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_max_base_amount_monthly'] ) ) $this->setEntrataMaxBaseAmountMonthly( $arrValues['entrata_max_base_amount_monthly'] );
		if( isset( $arrValues['entrata_avg_base_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseAmountMonthly', trim( $arrValues['entrata_avg_base_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_base_amount_monthly'] ) ) $this->setEntrataAvgBaseAmountMonthly( $arrValues['entrata_avg_base_amount_monthly'] );
		if( isset( $arrValues['entrata_min_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinBaseTaxMonthly', trim( $arrValues['entrata_min_base_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_min_base_tax_monthly'] ) ) $this->setEntrataMinBaseTaxMonthly( $arrValues['entrata_min_base_tax_monthly'] );
		if( isset( $arrValues['entrata_max_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxBaseTaxMonthly', trim( $arrValues['entrata_max_base_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_max_base_tax_monthly'] ) ) $this->setEntrataMaxBaseTaxMonthly( $arrValues['entrata_max_base_tax_monthly'] );
		if( isset( $arrValues['entrata_avg_base_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgBaseTaxMonthly', trim( $arrValues['entrata_avg_base_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_base_tax_monthly'] ) ) $this->setEntrataAvgBaseTaxMonthly( $arrValues['entrata_avg_base_tax_monthly'] );
		if( isset( $arrValues['entrata_min_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityAmountMonthly', trim( $arrValues['entrata_min_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_min_amenity_amount_monthly'] ) ) $this->setEntrataMinAmenityAmountMonthly( $arrValues['entrata_min_amenity_amount_monthly'] );
		if( isset( $arrValues['entrata_max_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityAmountMonthly', trim( $arrValues['entrata_max_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_max_amenity_amount_monthly'] ) ) $this->setEntrataMaxAmenityAmountMonthly( $arrValues['entrata_max_amenity_amount_monthly'] );
		if( isset( $arrValues['entrata_avg_amenity_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityAmountMonthly', trim( $arrValues['entrata_avg_amenity_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_amount_monthly'] ) ) $this->setEntrataAvgAmenityAmountMonthly( $arrValues['entrata_avg_amenity_amount_monthly'] );
		if( isset( $arrValues['entrata_min_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAmenityTaxMonthly', trim( $arrValues['entrata_min_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_min_amenity_tax_monthly'] ) ) $this->setEntrataMinAmenityTaxMonthly( $arrValues['entrata_min_amenity_tax_monthly'] );
		if( isset( $arrValues['entrata_max_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAmenityTaxMonthly', trim( $arrValues['entrata_max_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_max_amenity_tax_monthly'] ) ) $this->setEntrataMaxAmenityTaxMonthly( $arrValues['entrata_max_amenity_tax_monthly'] );
		if( isset( $arrValues['entrata_avg_amenity_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAmenityTaxMonthly', trim( $arrValues['entrata_avg_amenity_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_amenity_tax_monthly'] ) ) $this->setEntrataAvgAmenityTaxMonthly( $arrValues['entrata_avg_amenity_tax_monthly'] );
		if( isset( $arrValues['entrata_min_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialAmountMonthly', trim( $arrValues['entrata_min_special_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_min_special_amount_monthly'] ) ) $this->setEntrataMinSpecialAmountMonthly( $arrValues['entrata_min_special_amount_monthly'] );
		if( isset( $arrValues['entrata_max_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialAmountMonthly', trim( $arrValues['entrata_max_special_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_max_special_amount_monthly'] ) ) $this->setEntrataMaxSpecialAmountMonthly( $arrValues['entrata_max_special_amount_monthly'] );
		if( isset( $arrValues['entrata_avg_special_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialAmountMonthly', trim( $arrValues['entrata_avg_special_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_special_amount_monthly'] ) ) $this->setEntrataAvgSpecialAmountMonthly( $arrValues['entrata_avg_special_amount_monthly'] );
		if( isset( $arrValues['entrata_min_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinSpecialTaxMonthly', trim( $arrValues['entrata_min_special_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_min_special_tax_monthly'] ) ) $this->setEntrataMinSpecialTaxMonthly( $arrValues['entrata_min_special_tax_monthly'] );
		if( isset( $arrValues['entrata_max_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxSpecialTaxMonthly', trim( $arrValues['entrata_max_special_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_max_special_tax_monthly'] ) ) $this->setEntrataMaxSpecialTaxMonthly( $arrValues['entrata_max_special_tax_monthly'] );
		if( isset( $arrValues['entrata_avg_special_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgSpecialTaxMonthly', trim( $arrValues['entrata_avg_special_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_special_tax_monthly'] ) ) $this->setEntrataAvgSpecialTaxMonthly( $arrValues['entrata_avg_special_tax_monthly'] );
		if( isset( $arrValues['entrata_min_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnAmountMonthly', trim( $arrValues['entrata_min_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_min_add_on_amount_monthly'] ) ) $this->setEntrataMinAddOnAmountMonthly( $arrValues['entrata_min_add_on_amount_monthly'] );
		if( isset( $arrValues['entrata_max_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnAmountMonthly', trim( $arrValues['entrata_max_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_max_add_on_amount_monthly'] ) ) $this->setEntrataMaxAddOnAmountMonthly( $arrValues['entrata_max_add_on_amount_monthly'] );
		if( isset( $arrValues['entrata_avg_add_on_amount_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnAmountMonthly', trim( $arrValues['entrata_avg_add_on_amount_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_amount_monthly'] ) ) $this->setEntrataAvgAddOnAmountMonthly( $arrValues['entrata_avg_add_on_amount_monthly'] );
		if( isset( $arrValues['entrata_min_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMinAddOnTaxMonthly', trim( $arrValues['entrata_min_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_min_add_on_tax_monthly'] ) ) $this->setEntrataMinAddOnTaxMonthly( $arrValues['entrata_min_add_on_tax_monthly'] );
		if( isset( $arrValues['entrata_max_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataMaxAddOnTaxMonthly', trim( $arrValues['entrata_max_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_max_add_on_tax_monthly'] ) ) $this->setEntrataMaxAddOnTaxMonthly( $arrValues['entrata_max_add_on_tax_monthly'] );
		if( isset( $arrValues['entrata_avg_add_on_tax_monthly'] ) && $boolDirectSet ) $this->set( 'm_fltEntrataAvgAddOnTaxMonthly', trim( $arrValues['entrata_avg_add_on_tax_monthly'] ) ); elseif( isset( $arrValues['entrata_avg_add_on_tax_monthly'] ) ) $this->setEntrataAvgAddOnTaxMonthly( $arrValues['entrata_avg_add_on_tax_monthly'] );
		if( isset( $arrValues['effective_range'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveRange', trim( $arrValues['effective_range'] ) ); elseif( isset( $arrValues['effective_range'] ) ) $this->setEffectiveRange( $arrValues['effective_range'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->set( 'm_intSpaceConfigurationId', CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false ) );
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function sqlSpaceConfigurationId() {
		return ( true == isset( $this->m_intSpaceConfigurationId ) ) ? ( string ) $this->m_intSpaceConfigurationId : '1';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setTotalUnitCount( $intTotalUnitCount ) {
		$this->set( 'm_intTotalUnitCount', CStrings::strToIntDef( $intTotalUnitCount, NULL, false ) );
	}

	public function getTotalUnitCount() {
		return $this->m_intTotalUnitCount;
	}

	public function sqlTotalUnitCount() {
		return ( true == isset( $this->m_intTotalUnitCount ) ) ? ( string ) $this->m_intTotalUnitCount : 'NULL';
	}

	public function setRentableUnitCount( $intRentableUnitCount ) {
		$this->set( 'm_intRentableUnitCount', CStrings::strToIntDef( $intRentableUnitCount, NULL, false ) );
	}

	public function getRentableUnitCount() {
		return $this->m_intRentableUnitCount;
	}

	public function sqlRentableUnitCount() {
		return ( true == isset( $this->m_intRentableUnitCount ) ) ? ( string ) $this->m_intRentableUnitCount : 'NULL';
	}

	public function setAvailableUnitCount( $intAvailableUnitCount ) {
		$this->set( 'm_intAvailableUnitCount', CStrings::strToIntDef( $intAvailableUnitCount, NULL, false ) );
	}

	public function getAvailableUnitCount() {
		return $this->m_intAvailableUnitCount;
	}

	public function sqlAvailableUnitCount() {
		return ( true == isset( $this->m_intAvailableUnitCount ) ) ? ( string ) $this->m_intAvailableUnitCount : 'NULL';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 0 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 0 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setAvgSquareFeet( $fltAvgSquareFeet ) {
		$this->set( 'm_fltAvgSquareFeet', CStrings::strToFloatDef( $fltAvgSquareFeet, NULL, false, 0 ) );
	}

	public function getAvgSquareFeet() {
		return $this->m_fltAvgSquareFeet;
	}

	public function sqlAvgSquareFeet() {
		return ( true == isset( $this->m_fltAvgSquareFeet ) ) ? ( string ) $this->m_fltAvgSquareFeet : 'NULL';
	}

	public function setMinBaseRent( $fltMinBaseRent ) {
		$this->set( 'm_fltMinBaseRent', CStrings::strToFloatDef( $fltMinBaseRent, NULL, false, 0 ) );
	}

	public function getMinBaseRent() {
		return $this->m_fltMinBaseRent;
	}

	public function sqlMinBaseRent() {
		return ( true == isset( $this->m_fltMinBaseRent ) ) ? ( string ) $this->m_fltMinBaseRent : 'NULL';
	}

	public function setMaxBaseRent( $fltMaxBaseRent ) {
		$this->set( 'm_fltMaxBaseRent', CStrings::strToFloatDef( $fltMaxBaseRent, NULL, false, 0 ) );
	}

	public function getMaxBaseRent() {
		return $this->m_fltMaxBaseRent;
	}

	public function sqlMaxBaseRent() {
		return ( true == isset( $this->m_fltMaxBaseRent ) ) ? ( string ) $this->m_fltMaxBaseRent : 'NULL';
	}

	public function setAvgBaseRent( $fltAvgBaseRent ) {
		$this->set( 'm_fltAvgBaseRent', CStrings::strToFloatDef( $fltAvgBaseRent, NULL, false, 0 ) );
	}

	public function getAvgBaseRent() {
		return $this->m_fltAvgBaseRent;
	}

	public function sqlAvgBaseRent() {
		return ( true == isset( $this->m_fltAvgBaseRent ) ) ? ( string ) $this->m_fltAvgBaseRent : 'NULL';
	}

	public function setMinAmenityRent( $fltMinAmenityRent ) {
		$this->set( 'm_fltMinAmenityRent', CStrings::strToFloatDef( $fltMinAmenityRent, NULL, false, 0 ) );
	}

	public function getMinAmenityRent() {
		return $this->m_fltMinAmenityRent;
	}

	public function sqlMinAmenityRent() {
		return ( true == isset( $this->m_fltMinAmenityRent ) ) ? ( string ) $this->m_fltMinAmenityRent : 'NULL';
	}

	public function setMaxAmenityRent( $fltMaxAmenityRent ) {
		$this->set( 'm_fltMaxAmenityRent', CStrings::strToFloatDef( $fltMaxAmenityRent, NULL, false, 0 ) );
	}

	public function getMaxAmenityRent() {
		return $this->m_fltMaxAmenityRent;
	}

	public function sqlMaxAmenityRent() {
		return ( true == isset( $this->m_fltMaxAmenityRent ) ) ? ( string ) $this->m_fltMaxAmenityRent : 'NULL';
	}

	public function setAvgAmenityRent( $fltAvgAmenityRent ) {
		$this->set( 'm_fltAvgAmenityRent', CStrings::strToFloatDef( $fltAvgAmenityRent, NULL, false, 0 ) );
	}

	public function getAvgAmenityRent() {
		return $this->m_fltAvgAmenityRent;
	}

	public function sqlAvgAmenityRent() {
		return ( true == isset( $this->m_fltAvgAmenityRent ) ) ? ( string ) $this->m_fltAvgAmenityRent : 'NULL';
	}

	public function setMinSpecialRent( $fltMinSpecialRent ) {
		$this->set( 'm_fltMinSpecialRent', CStrings::strToFloatDef( $fltMinSpecialRent, NULL, false, 0 ) );
	}

	public function getMinSpecialRent() {
		return $this->m_fltMinSpecialRent;
	}

	public function sqlMinSpecialRent() {
		return ( true == isset( $this->m_fltMinSpecialRent ) ) ? ( string ) $this->m_fltMinSpecialRent : 'NULL';
	}

	public function setMaxSpecialRent( $fltMaxSpecialRent ) {
		$this->set( 'm_fltMaxSpecialRent', CStrings::strToFloatDef( $fltMaxSpecialRent, NULL, false, 0 ) );
	}

	public function getMaxSpecialRent() {
		return $this->m_fltMaxSpecialRent;
	}

	public function sqlMaxSpecialRent() {
		return ( true == isset( $this->m_fltMaxSpecialRent ) ) ? ( string ) $this->m_fltMaxSpecialRent : 'NULL';
	}

	public function setAvgSpecialRent( $fltAvgSpecialRent ) {
		$this->set( 'm_fltAvgSpecialRent', CStrings::strToFloatDef( $fltAvgSpecialRent, NULL, false, 0 ) );
	}

	public function getAvgSpecialRent() {
		return $this->m_fltAvgSpecialRent;
	}

	public function sqlAvgSpecialRent() {
		return ( true == isset( $this->m_fltAvgSpecialRent ) ) ? ( string ) $this->m_fltAvgSpecialRent : 'NULL';
	}

	public function setMinAddOnRent( $fltMinAddOnRent ) {
		$this->set( 'm_fltMinAddOnRent', CStrings::strToFloatDef( $fltMinAddOnRent, NULL, false, 0 ) );
	}

	public function getMinAddOnRent() {
		return $this->m_fltMinAddOnRent;
	}

	public function sqlMinAddOnRent() {
		return ( true == isset( $this->m_fltMinAddOnRent ) ) ? ( string ) $this->m_fltMinAddOnRent : 'NULL';
	}

	public function setMaxAddOnRent( $fltMaxAddOnRent ) {
		$this->set( 'm_fltMaxAddOnRent', CStrings::strToFloatDef( $fltMaxAddOnRent, NULL, false, 0 ) );
	}

	public function getMaxAddOnRent() {
		return $this->m_fltMaxAddOnRent;
	}

	public function sqlMaxAddOnRent() {
		return ( true == isset( $this->m_fltMaxAddOnRent ) ) ? ( string ) $this->m_fltMaxAddOnRent : 'NULL';
	}

	public function setAvgAddOnRent( $fltAvgAddOnRent ) {
		$this->set( 'm_fltAvgAddOnRent', CStrings::strToFloatDef( $fltAvgAddOnRent, NULL, false, 0 ) );
	}

	public function getAvgAddOnRent() {
		return $this->m_fltAvgAddOnRent;
	}

	public function sqlAvgAddOnRent() {
		return ( true == isset( $this->m_fltAvgAddOnRent ) ) ? ( string ) $this->m_fltAvgAddOnRent : 'NULL';
	}

	public function setMinMarketRent( $fltMinMarketRent ) {
		$this->set( 'm_fltMinMarketRent', CStrings::strToFloatDef( $fltMinMarketRent, NULL, false, 0 ) );
	}

	public function getMinMarketRent() {
		return $this->m_fltMinMarketRent;
	}

	public function sqlMinMarketRent() {
		return ( true == isset( $this->m_fltMinMarketRent ) ) ? ( string ) $this->m_fltMinMarketRent : 'NULL';
	}

	public function setMaxMarketRent( $fltMaxMarketRent ) {
		$this->set( 'm_fltMaxMarketRent', CStrings::strToFloatDef( $fltMaxMarketRent, NULL, false, 0 ) );
	}

	public function getMaxMarketRent() {
		return $this->m_fltMaxMarketRent;
	}

	public function sqlMaxMarketRent() {
		return ( true == isset( $this->m_fltMaxMarketRent ) ) ? ( string ) $this->m_fltMaxMarketRent : 'NULL';
	}

	public function setAvgMarketRent( $fltAvgMarketRent ) {
		$this->set( 'm_fltAvgMarketRent', CStrings::strToFloatDef( $fltAvgMarketRent, NULL, false, 0 ) );
	}

	public function getAvgMarketRent() {
		return $this->m_fltAvgMarketRent;
	}

	public function sqlAvgMarketRent() {
		return ( true == isset( $this->m_fltAvgMarketRent ) ) ? ( string ) $this->m_fltAvgMarketRent : 'NULL';
	}

	public function setManualMinMarketRent( $fltManualMinMarketRent ) {
		$this->set( 'm_fltManualMinMarketRent', CStrings::strToFloatDef( $fltManualMinMarketRent, NULL, false, 0 ) );
	}

	public function getManualMinMarketRent() {
		return $this->m_fltManualMinMarketRent;
	}

	public function sqlManualMinMarketRent() {
		return ( true == isset( $this->m_fltManualMinMarketRent ) ) ? ( string ) $this->m_fltManualMinMarketRent : 'NULL';
	}

	public function setManualMaxMarketRent( $fltManualMaxMarketRent ) {
		$this->set( 'm_fltManualMaxMarketRent', CStrings::strToFloatDef( $fltManualMaxMarketRent, NULL, false, 0 ) );
	}

	public function getManualMaxMarketRent() {
		return $this->m_fltManualMaxMarketRent;
	}

	public function sqlManualMaxMarketRent() {
		return ( true == isset( $this->m_fltManualMaxMarketRent ) ) ? ( string ) $this->m_fltManualMaxMarketRent : 'NULL';
	}

	public function setMinBaseDeposit( $fltMinBaseDeposit ) {
		$this->set( 'm_fltMinBaseDeposit', CStrings::strToFloatDef( $fltMinBaseDeposit, NULL, false, 0 ) );
	}

	public function getMinBaseDeposit() {
		return $this->m_fltMinBaseDeposit;
	}

	public function sqlMinBaseDeposit() {
		return ( true == isset( $this->m_fltMinBaseDeposit ) ) ? ( string ) $this->m_fltMinBaseDeposit : 'NULL';
	}

	public function setMaxBaseDeposit( $fltMaxBaseDeposit ) {
		$this->set( 'm_fltMaxBaseDeposit', CStrings::strToFloatDef( $fltMaxBaseDeposit, NULL, false, 0 ) );
	}

	public function getMaxBaseDeposit() {
		return $this->m_fltMaxBaseDeposit;
	}

	public function sqlMaxBaseDeposit() {
		return ( true == isset( $this->m_fltMaxBaseDeposit ) ) ? ( string ) $this->m_fltMaxBaseDeposit : 'NULL';
	}

	public function setAvgBaseDeposit( $fltAvgBaseDeposit ) {
		$this->set( 'm_fltAvgBaseDeposit', CStrings::strToFloatDef( $fltAvgBaseDeposit, NULL, false, 0 ) );
	}

	public function getAvgBaseDeposit() {
		return $this->m_fltAvgBaseDeposit;
	}

	public function sqlAvgBaseDeposit() {
		return ( true == isset( $this->m_fltAvgBaseDeposit ) ) ? ( string ) $this->m_fltAvgBaseDeposit : 'NULL';
	}

	public function setMinAmenityDeposit( $fltMinAmenityDeposit ) {
		$this->set( 'm_fltMinAmenityDeposit', CStrings::strToFloatDef( $fltMinAmenityDeposit, NULL, false, 0 ) );
	}

	public function getMinAmenityDeposit() {
		return $this->m_fltMinAmenityDeposit;
	}

	public function sqlMinAmenityDeposit() {
		return ( true == isset( $this->m_fltMinAmenityDeposit ) ) ? ( string ) $this->m_fltMinAmenityDeposit : 'NULL';
	}

	public function setMaxAmenityDeposit( $fltMaxAmenityDeposit ) {
		$this->set( 'm_fltMaxAmenityDeposit', CStrings::strToFloatDef( $fltMaxAmenityDeposit, NULL, false, 0 ) );
	}

	public function getMaxAmenityDeposit() {
		return $this->m_fltMaxAmenityDeposit;
	}

	public function sqlMaxAmenityDeposit() {
		return ( true == isset( $this->m_fltMaxAmenityDeposit ) ) ? ( string ) $this->m_fltMaxAmenityDeposit : 'NULL';
	}

	public function setAvgAmenityDeposit( $fltAvgAmenityDeposit ) {
		$this->set( 'm_fltAvgAmenityDeposit', CStrings::strToFloatDef( $fltAvgAmenityDeposit, NULL, false, 0 ) );
	}

	public function getAvgAmenityDeposit() {
		return $this->m_fltAvgAmenityDeposit;
	}

	public function sqlAvgAmenityDeposit() {
		return ( true == isset( $this->m_fltAvgAmenityDeposit ) ) ? ( string ) $this->m_fltAvgAmenityDeposit : 'NULL';
	}

	public function setMinSpecialDeposit( $fltMinSpecialDeposit ) {
		$this->set( 'm_fltMinSpecialDeposit', CStrings::strToFloatDef( $fltMinSpecialDeposit, NULL, false, 0 ) );
	}

	public function getMinSpecialDeposit() {
		return $this->m_fltMinSpecialDeposit;
	}

	public function sqlMinSpecialDeposit() {
		return ( true == isset( $this->m_fltMinSpecialDeposit ) ) ? ( string ) $this->m_fltMinSpecialDeposit : 'NULL';
	}

	public function setMaxSpecialDeposit( $fltMaxSpecialDeposit ) {
		$this->set( 'm_fltMaxSpecialDeposit', CStrings::strToFloatDef( $fltMaxSpecialDeposit, NULL, false, 0 ) );
	}

	public function getMaxSpecialDeposit() {
		return $this->m_fltMaxSpecialDeposit;
	}

	public function sqlMaxSpecialDeposit() {
		return ( true == isset( $this->m_fltMaxSpecialDeposit ) ) ? ( string ) $this->m_fltMaxSpecialDeposit : 'NULL';
	}

	public function setAvgSpecialDeposit( $fltAvgSpecialDeposit ) {
		$this->set( 'm_fltAvgSpecialDeposit', CStrings::strToFloatDef( $fltAvgSpecialDeposit, NULL, false, 0 ) );
	}

	public function getAvgSpecialDeposit() {
		return $this->m_fltAvgSpecialDeposit;
	}

	public function sqlAvgSpecialDeposit() {
		return ( true == isset( $this->m_fltAvgSpecialDeposit ) ) ? ( string ) $this->m_fltAvgSpecialDeposit : 'NULL';
	}

	public function setMinAddOnDeposit( $fltMinAddOnDeposit ) {
		$this->set( 'm_fltMinAddOnDeposit', CStrings::strToFloatDef( $fltMinAddOnDeposit, NULL, false, 0 ) );
	}

	public function getMinAddOnDeposit() {
		return $this->m_fltMinAddOnDeposit;
	}

	public function sqlMinAddOnDeposit() {
		return ( true == isset( $this->m_fltMinAddOnDeposit ) ) ? ( string ) $this->m_fltMinAddOnDeposit : 'NULL';
	}

	public function setMaxAddOnDeposit( $fltMaxAddOnDeposit ) {
		$this->set( 'm_fltMaxAddOnDeposit', CStrings::strToFloatDef( $fltMaxAddOnDeposit, NULL, false, 0 ) );
	}

	public function getMaxAddOnDeposit() {
		return $this->m_fltMaxAddOnDeposit;
	}

	public function sqlMaxAddOnDeposit() {
		return ( true == isset( $this->m_fltMaxAddOnDeposit ) ) ? ( string ) $this->m_fltMaxAddOnDeposit : 'NULL';
	}

	public function setAvgAddOnDeposit( $fltAvgAddOnDeposit ) {
		$this->set( 'm_fltAvgAddOnDeposit', CStrings::strToFloatDef( $fltAvgAddOnDeposit, NULL, false, 0 ) );
	}

	public function getAvgAddOnDeposit() {
		return $this->m_fltAvgAddOnDeposit;
	}

	public function sqlAvgAddOnDeposit() {
		return ( true == isset( $this->m_fltAvgAddOnDeposit ) ) ? ( string ) $this->m_fltAvgAddOnDeposit : 'NULL';
	}

	public function setMinTotalDeposit( $fltMinTotalDeposit ) {
		$this->set( 'm_fltMinTotalDeposit', CStrings::strToFloatDef( $fltMinTotalDeposit, NULL, false, 0 ) );
	}

	public function getMinTotalDeposit() {
		return $this->m_fltMinTotalDeposit;
	}

	public function sqlMinTotalDeposit() {
		return ( true == isset( $this->m_fltMinTotalDeposit ) ) ? ( string ) $this->m_fltMinTotalDeposit : 'NULL';
	}

	public function setMaxTotalDeposit( $fltMaxTotalDeposit ) {
		$this->set( 'm_fltMaxTotalDeposit', CStrings::strToFloatDef( $fltMaxTotalDeposit, NULL, false, 0 ) );
	}

	public function getMaxTotalDeposit() {
		return $this->m_fltMaxTotalDeposit;
	}

	public function sqlMaxTotalDeposit() {
		return ( true == isset( $this->m_fltMaxTotalDeposit ) ) ? ( string ) $this->m_fltMaxTotalDeposit : 'NULL';
	}

	public function setAvgTotalDeposit( $fltAvgTotalDeposit ) {
		$this->set( 'm_fltAvgTotalDeposit', CStrings::strToFloatDef( $fltAvgTotalDeposit, NULL, false, 0 ) );
	}

	public function getAvgTotalDeposit() {
		return $this->m_fltAvgTotalDeposit;
	}

	public function sqlAvgTotalDeposit() {
		return ( true == isset( $this->m_fltAvgTotalDeposit ) ) ? ( string ) $this->m_fltAvgTotalDeposit : 'NULL';
	}

	public function setManualMinTotalDeposit( $fltManualMinTotalDeposit ) {
		$this->set( 'm_fltManualMinTotalDeposit', CStrings::strToFloatDef( $fltManualMinTotalDeposit, NULL, false, 0 ) );
	}

	public function getManualMinTotalDeposit() {
		return $this->m_fltManualMinTotalDeposit;
	}

	public function sqlManualMinTotalDeposit() {
		return ( true == isset( $this->m_fltManualMinTotalDeposit ) ) ? ( string ) $this->m_fltManualMinTotalDeposit : 'NULL';
	}

	public function setManualMaxTotalDeposit( $fltManualMaxTotalDeposit ) {
		$this->set( 'm_fltManualMaxTotalDeposit', CStrings::strToFloatDef( $fltManualMaxTotalDeposit, NULL, false, 0 ) );
	}

	public function getManualMaxTotalDeposit() {
		return $this->m_fltManualMaxTotalDeposit;
	}

	public function sqlManualMaxTotalDeposit() {
		return ( true == isset( $this->m_fltManualMaxTotalDeposit ) ) ? ( string ) $this->m_fltManualMaxTotalDeposit : 'NULL';
	}

	public function setEntrataMinBaseRent( $fltEntrataMinBaseRent ) {
		$this->set( 'm_fltEntrataMinBaseRent', CStrings::strToFloatDef( $fltEntrataMinBaseRent, NULL, false, 0 ) );
	}

	public function getEntrataMinBaseRent() {
		return $this->m_fltEntrataMinBaseRent;
	}

	public function sqlEntrataMinBaseRent() {
		return ( true == isset( $this->m_fltEntrataMinBaseRent ) ) ? ( string ) $this->m_fltEntrataMinBaseRent : 'NULL';
	}

	public function setEntrataMaxBaseRent( $fltEntrataMaxBaseRent ) {
		$this->set( 'm_fltEntrataMaxBaseRent', CStrings::strToFloatDef( $fltEntrataMaxBaseRent, NULL, false, 0 ) );
	}

	public function getEntrataMaxBaseRent() {
		return $this->m_fltEntrataMaxBaseRent;
	}

	public function sqlEntrataMaxBaseRent() {
		return ( true == isset( $this->m_fltEntrataMaxBaseRent ) ) ? ( string ) $this->m_fltEntrataMaxBaseRent : 'NULL';
	}

	public function setEntrataAvgBaseRent( $fltEntrataAvgBaseRent ) {
		$this->set( 'm_fltEntrataAvgBaseRent', CStrings::strToFloatDef( $fltEntrataAvgBaseRent, NULL, false, 0 ) );
	}

	public function getEntrataAvgBaseRent() {
		return $this->m_fltEntrataAvgBaseRent;
	}

	public function sqlEntrataAvgBaseRent() {
		return ( true == isset( $this->m_fltEntrataAvgBaseRent ) ) ? ( string ) $this->m_fltEntrataAvgBaseRent : 'NULL';
	}

	public function setEntrataMinAmenityRent( $fltEntrataMinAmenityRent ) {
		$this->set( 'm_fltEntrataMinAmenityRent', CStrings::strToFloatDef( $fltEntrataMinAmenityRent, NULL, false, 0 ) );
	}

	public function getEntrataMinAmenityRent() {
		return $this->m_fltEntrataMinAmenityRent;
	}

	public function sqlEntrataMinAmenityRent() {
		return ( true == isset( $this->m_fltEntrataMinAmenityRent ) ) ? ( string ) $this->m_fltEntrataMinAmenityRent : 'NULL';
	}

	public function setEntrataMaxAmenityRent( $fltEntrataMaxAmenityRent ) {
		$this->set( 'm_fltEntrataMaxAmenityRent', CStrings::strToFloatDef( $fltEntrataMaxAmenityRent, NULL, false, 0 ) );
	}

	public function getEntrataMaxAmenityRent() {
		return $this->m_fltEntrataMaxAmenityRent;
	}

	public function sqlEntrataMaxAmenityRent() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityRent ) ) ? ( string ) $this->m_fltEntrataMaxAmenityRent : 'NULL';
	}

	public function setEntrataAvgAmenityRent( $fltEntrataAvgAmenityRent ) {
		$this->set( 'm_fltEntrataAvgAmenityRent', CStrings::strToFloatDef( $fltEntrataAvgAmenityRent, NULL, false, 0 ) );
	}

	public function getEntrataAvgAmenityRent() {
		return $this->m_fltEntrataAvgAmenityRent;
	}

	public function sqlEntrataAvgAmenityRent() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityRent ) ) ? ( string ) $this->m_fltEntrataAvgAmenityRent : 'NULL';
	}

	public function setEntrataMinSpecialRent( $fltEntrataMinSpecialRent ) {
		$this->set( 'm_fltEntrataMinSpecialRent', CStrings::strToFloatDef( $fltEntrataMinSpecialRent, NULL, false, 0 ) );
	}

	public function getEntrataMinSpecialRent() {
		return $this->m_fltEntrataMinSpecialRent;
	}

	public function sqlEntrataMinSpecialRent() {
		return ( true == isset( $this->m_fltEntrataMinSpecialRent ) ) ? ( string ) $this->m_fltEntrataMinSpecialRent : 'NULL';
	}

	public function setEntrataMaxSpecialRent( $fltEntrataMaxSpecialRent ) {
		$this->set( 'm_fltEntrataMaxSpecialRent', CStrings::strToFloatDef( $fltEntrataMaxSpecialRent, NULL, false, 0 ) );
	}

	public function getEntrataMaxSpecialRent() {
		return $this->m_fltEntrataMaxSpecialRent;
	}

	public function sqlEntrataMaxSpecialRent() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialRent ) ) ? ( string ) $this->m_fltEntrataMaxSpecialRent : 'NULL';
	}

	public function setEntrataAvgSpecialRent( $fltEntrataAvgSpecialRent ) {
		$this->set( 'm_fltEntrataAvgSpecialRent', CStrings::strToFloatDef( $fltEntrataAvgSpecialRent, NULL, false, 0 ) );
	}

	public function getEntrataAvgSpecialRent() {
		return $this->m_fltEntrataAvgSpecialRent;
	}

	public function sqlEntrataAvgSpecialRent() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialRent ) ) ? ( string ) $this->m_fltEntrataAvgSpecialRent : 'NULL';
	}

	public function setEntrataMinAddOnRent( $fltEntrataMinAddOnRent ) {
		$this->set( 'm_fltEntrataMinAddOnRent', CStrings::strToFloatDef( $fltEntrataMinAddOnRent, NULL, false, 0 ) );
	}

	public function getEntrataMinAddOnRent() {
		return $this->m_fltEntrataMinAddOnRent;
	}

	public function sqlEntrataMinAddOnRent() {
		return ( true == isset( $this->m_fltEntrataMinAddOnRent ) ) ? ( string ) $this->m_fltEntrataMinAddOnRent : 'NULL';
	}

	public function setEntrataMaxAddOnRent( $fltEntrataMaxAddOnRent ) {
		$this->set( 'm_fltEntrataMaxAddOnRent', CStrings::strToFloatDef( $fltEntrataMaxAddOnRent, NULL, false, 0 ) );
	}

	public function getEntrataMaxAddOnRent() {
		return $this->m_fltEntrataMaxAddOnRent;
	}

	public function sqlEntrataMaxAddOnRent() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnRent ) ) ? ( string ) $this->m_fltEntrataMaxAddOnRent : 'NULL';
	}

	public function setEntrataAvgAddOnRent( $fltEntrataAvgAddOnRent ) {
		$this->set( 'm_fltEntrataAvgAddOnRent', CStrings::strToFloatDef( $fltEntrataAvgAddOnRent, NULL, false, 0 ) );
	}

	public function getEntrataAvgAddOnRent() {
		return $this->m_fltEntrataAvgAddOnRent;
	}

	public function sqlEntrataAvgAddOnRent() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnRent ) ) ? ( string ) $this->m_fltEntrataAvgAddOnRent : 'NULL';
	}

	public function setEntrataMinMarketRent( $fltEntrataMinMarketRent ) {
		$this->set( 'm_fltEntrataMinMarketRent', CStrings::strToFloatDef( $fltEntrataMinMarketRent, NULL, false, 0 ) );
	}

	public function getEntrataMinMarketRent() {
		return $this->m_fltEntrataMinMarketRent;
	}

	public function sqlEntrataMinMarketRent() {
		return ( true == isset( $this->m_fltEntrataMinMarketRent ) ) ? ( string ) $this->m_fltEntrataMinMarketRent : 'NULL';
	}

	public function setEntrataMaxMarketRent( $fltEntrataMaxMarketRent ) {
		$this->set( 'm_fltEntrataMaxMarketRent', CStrings::strToFloatDef( $fltEntrataMaxMarketRent, NULL, false, 0 ) );
	}

	public function getEntrataMaxMarketRent() {
		return $this->m_fltEntrataMaxMarketRent;
	}

	public function sqlEntrataMaxMarketRent() {
		return ( true == isset( $this->m_fltEntrataMaxMarketRent ) ) ? ( string ) $this->m_fltEntrataMaxMarketRent : 'NULL';
	}

	public function setEntrataAvgMarketRent( $fltEntrataAvgMarketRent ) {
		$this->set( 'm_fltEntrataAvgMarketRent', CStrings::strToFloatDef( $fltEntrataAvgMarketRent, NULL, false, 0 ) );
	}

	public function getEntrataAvgMarketRent() {
		return $this->m_fltEntrataAvgMarketRent;
	}

	public function sqlEntrataAvgMarketRent() {
		return ( true == isset( $this->m_fltEntrataAvgMarketRent ) ) ? ( string ) $this->m_fltEntrataAvgMarketRent : 'NULL';
	}

	public function setEntrataMinBaseDeposit( $fltEntrataMinBaseDeposit ) {
		$this->set( 'm_fltEntrataMinBaseDeposit', CStrings::strToFloatDef( $fltEntrataMinBaseDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMinBaseDeposit() {
		return $this->m_fltEntrataMinBaseDeposit;
	}

	public function sqlEntrataMinBaseDeposit() {
		return ( true == isset( $this->m_fltEntrataMinBaseDeposit ) ) ? ( string ) $this->m_fltEntrataMinBaseDeposit : 'NULL';
	}

	public function setEntrataMaxBaseDeposit( $fltEntrataMaxBaseDeposit ) {
		$this->set( 'm_fltEntrataMaxBaseDeposit', CStrings::strToFloatDef( $fltEntrataMaxBaseDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMaxBaseDeposit() {
		return $this->m_fltEntrataMaxBaseDeposit;
	}

	public function sqlEntrataMaxBaseDeposit() {
		return ( true == isset( $this->m_fltEntrataMaxBaseDeposit ) ) ? ( string ) $this->m_fltEntrataMaxBaseDeposit : 'NULL';
	}

	public function setEntrataAvgBaseDeposit( $fltEntrataAvgBaseDeposit ) {
		$this->set( 'm_fltEntrataAvgBaseDeposit', CStrings::strToFloatDef( $fltEntrataAvgBaseDeposit, NULL, false, 0 ) );
	}

	public function getEntrataAvgBaseDeposit() {
		return $this->m_fltEntrataAvgBaseDeposit;
	}

	public function sqlEntrataAvgBaseDeposit() {
		return ( true == isset( $this->m_fltEntrataAvgBaseDeposit ) ) ? ( string ) $this->m_fltEntrataAvgBaseDeposit : 'NULL';
	}

	public function setEntrataMinAmenityDeposit( $fltEntrataMinAmenityDeposit ) {
		$this->set( 'm_fltEntrataMinAmenityDeposit', CStrings::strToFloatDef( $fltEntrataMinAmenityDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMinAmenityDeposit() {
		return $this->m_fltEntrataMinAmenityDeposit;
	}

	public function sqlEntrataMinAmenityDeposit() {
		return ( true == isset( $this->m_fltEntrataMinAmenityDeposit ) ) ? ( string ) $this->m_fltEntrataMinAmenityDeposit : 'NULL';
	}

	public function setEntrataMaxAmenityDeposit( $fltEntrataMaxAmenityDeposit ) {
		$this->set( 'm_fltEntrataMaxAmenityDeposit', CStrings::strToFloatDef( $fltEntrataMaxAmenityDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMaxAmenityDeposit() {
		return $this->m_fltEntrataMaxAmenityDeposit;
	}

	public function sqlEntrataMaxAmenityDeposit() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityDeposit ) ) ? ( string ) $this->m_fltEntrataMaxAmenityDeposit : 'NULL';
	}

	public function setEntrataAvgAmenityDeposit( $fltEntrataAvgAmenityDeposit ) {
		$this->set( 'm_fltEntrataAvgAmenityDeposit', CStrings::strToFloatDef( $fltEntrataAvgAmenityDeposit, NULL, false, 0 ) );
	}

	public function getEntrataAvgAmenityDeposit() {
		return $this->m_fltEntrataAvgAmenityDeposit;
	}

	public function sqlEntrataAvgAmenityDeposit() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityDeposit ) ) ? ( string ) $this->m_fltEntrataAvgAmenityDeposit : 'NULL';
	}

	public function setEntrataMinSpecialDeposit( $fltEntrataMinSpecialDeposit ) {
		$this->set( 'm_fltEntrataMinSpecialDeposit', CStrings::strToFloatDef( $fltEntrataMinSpecialDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMinSpecialDeposit() {
		return $this->m_fltEntrataMinSpecialDeposit;
	}

	public function sqlEntrataMinSpecialDeposit() {
		return ( true == isset( $this->m_fltEntrataMinSpecialDeposit ) ) ? ( string ) $this->m_fltEntrataMinSpecialDeposit : 'NULL';
	}

	public function setEntrataMaxSpecialDeposit( $fltEntrataMaxSpecialDeposit ) {
		$this->set( 'm_fltEntrataMaxSpecialDeposit', CStrings::strToFloatDef( $fltEntrataMaxSpecialDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMaxSpecialDeposit() {
		return $this->m_fltEntrataMaxSpecialDeposit;
	}

	public function sqlEntrataMaxSpecialDeposit() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialDeposit ) ) ? ( string ) $this->m_fltEntrataMaxSpecialDeposit : 'NULL';
	}

	public function setEntrataAvgSpecialDeposit( $fltEntrataAvgSpecialDeposit ) {
		$this->set( 'm_fltEntrataAvgSpecialDeposit', CStrings::strToFloatDef( $fltEntrataAvgSpecialDeposit, NULL, false, 0 ) );
	}

	public function getEntrataAvgSpecialDeposit() {
		return $this->m_fltEntrataAvgSpecialDeposit;
	}

	public function sqlEntrataAvgSpecialDeposit() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialDeposit ) ) ? ( string ) $this->m_fltEntrataAvgSpecialDeposit : 'NULL';
	}

	public function setEntrataMinAddOnDeposit( $fltEntrataMinAddOnDeposit ) {
		$this->set( 'm_fltEntrataMinAddOnDeposit', CStrings::strToFloatDef( $fltEntrataMinAddOnDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMinAddOnDeposit() {
		return $this->m_fltEntrataMinAddOnDeposit;
	}

	public function sqlEntrataMinAddOnDeposit() {
		return ( true == isset( $this->m_fltEntrataMinAddOnDeposit ) ) ? ( string ) $this->m_fltEntrataMinAddOnDeposit : 'NULL';
	}

	public function setEntrataMaxAddOnDeposit( $fltEntrataMaxAddOnDeposit ) {
		$this->set( 'm_fltEntrataMaxAddOnDeposit', CStrings::strToFloatDef( $fltEntrataMaxAddOnDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMaxAddOnDeposit() {
		return $this->m_fltEntrataMaxAddOnDeposit;
	}

	public function sqlEntrataMaxAddOnDeposit() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnDeposit ) ) ? ( string ) $this->m_fltEntrataMaxAddOnDeposit : 'NULL';
	}

	public function setEntrataAvgAddOnDeposit( $fltEntrataAvgAddOnDeposit ) {
		$this->set( 'm_fltEntrataAvgAddOnDeposit', CStrings::strToFloatDef( $fltEntrataAvgAddOnDeposit, NULL, false, 0 ) );
	}

	public function getEntrataAvgAddOnDeposit() {
		return $this->m_fltEntrataAvgAddOnDeposit;
	}

	public function sqlEntrataAvgAddOnDeposit() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnDeposit ) ) ? ( string ) $this->m_fltEntrataAvgAddOnDeposit : 'NULL';
	}

	public function setEntrataMinTotalDeposit( $fltEntrataMinTotalDeposit ) {
		$this->set( 'm_fltEntrataMinTotalDeposit', CStrings::strToFloatDef( $fltEntrataMinTotalDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMinTotalDeposit() {
		return $this->m_fltEntrataMinTotalDeposit;
	}

	public function sqlEntrataMinTotalDeposit() {
		return ( true == isset( $this->m_fltEntrataMinTotalDeposit ) ) ? ( string ) $this->m_fltEntrataMinTotalDeposit : 'NULL';
	}

	public function setEntrataMaxTotalDeposit( $fltEntrataMaxTotalDeposit ) {
		$this->set( 'm_fltEntrataMaxTotalDeposit', CStrings::strToFloatDef( $fltEntrataMaxTotalDeposit, NULL, false, 0 ) );
	}

	public function getEntrataMaxTotalDeposit() {
		return $this->m_fltEntrataMaxTotalDeposit;
	}

	public function sqlEntrataMaxTotalDeposit() {
		return ( true == isset( $this->m_fltEntrataMaxTotalDeposit ) ) ? ( string ) $this->m_fltEntrataMaxTotalDeposit : 'NULL';
	}

	public function setEntrataAvgTotalDeposit( $fltEntrataAvgTotalDeposit ) {
		$this->set( 'm_fltEntrataAvgTotalDeposit', CStrings::strToFloatDef( $fltEntrataAvgTotalDeposit, NULL, false, 0 ) );
	}

	public function getEntrataAvgTotalDeposit() {
		return $this->m_fltEntrataAvgTotalDeposit;
	}

	public function sqlEntrataAvgTotalDeposit() {
		return ( true == isset( $this->m_fltEntrataAvgTotalDeposit ) ) ? ( string ) $this->m_fltEntrataAvgTotalDeposit : 'NULL';
	}

	public function setUnitSpaceIds( $arrintUnitSpaceIds ) {
		$this->set( 'm_arrintUnitSpaceIds', CStrings::strToArrIntDef( $arrintUnitSpaceIds, NULL ) );
	}

	public function getUnitSpaceIds() {
		return $this->m_arrintUnitSpaceIds;
	}

	public function sqlUnitSpaceIds() {
		return ( true == isset( $this->m_arrintUnitSpaceIds ) && true == valArr( $this->m_arrintUnitSpaceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUnitSpaceIds, NULL ) . '\'' : 'NULL';
	}

	public function setRentableUnitSpaceIds( $arrintRentableUnitSpaceIds ) {
		$this->set( 'm_arrintRentableUnitSpaceIds', CStrings::strToArrIntDef( $arrintRentableUnitSpaceIds, NULL ) );
	}

	public function getRentableUnitSpaceIds() {
		return $this->m_arrintRentableUnitSpaceIds;
	}

	public function sqlRentableUnitSpaceIds() {
		return ( true == isset( $this->m_arrintRentableUnitSpaceIds ) && true == valArr( $this->m_arrintRentableUnitSpaceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintRentableUnitSpaceIds, NULL ) . '\'' : 'NULL';
	}

	public function setAvailableUnitSpaceIds( $arrintAvailableUnitSpaceIds ) {
		$this->set( 'm_arrintAvailableUnitSpaceIds', CStrings::strToArrIntDef( $arrintAvailableUnitSpaceIds, NULL ) );
	}

	public function getAvailableUnitSpaceIds() {
		return $this->m_arrintAvailableUnitSpaceIds;
	}

	public function sqlAvailableUnitSpaceIds() {
		return ( true == isset( $this->m_arrintAvailableUnitSpaceIds ) && true == valArr( $this->m_arrintAvailableUnitSpaceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintAvailableUnitSpaceIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsManualRentRange( $boolIsManualRentRange ) {
		$this->set( 'm_boolIsManualRentRange', CStrings::strToBool( $boolIsManualRentRange ) );
	}

	public function getIsManualRentRange() {
		return $this->m_boolIsManualRentRange;
	}

	public function sqlIsManualRentRange() {
		return ( true == isset( $this->m_boolIsManualRentRange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualRentRange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManualDepositRange( $boolIsManualDepositRange ) {
		$this->set( 'm_boolIsManualDepositRange', CStrings::strToBool( $boolIsManualDepositRange ) );
	}

	public function getIsManualDepositRange() {
		return $this->m_boolIsManualDepositRange;
	}

	public function sqlIsManualDepositRange() {
		return ( true == isset( $this->m_boolIsManualDepositRange ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualDepositRange ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setMinBaseRentMonthly( $fltMinBaseRentMonthly ) {
		$this->set( 'm_fltMinBaseRentMonthly', CStrings::strToFloatDef( $fltMinBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getMinBaseRentMonthly() {
		return $this->m_fltMinBaseRentMonthly;
	}

	public function sqlMinBaseRentMonthly() {
		return ( true == isset( $this->m_fltMinBaseRentMonthly ) ) ? ( string ) $this->m_fltMinBaseRentMonthly : 'NULL';
	}

	public function setMaxBaseRentMonthly( $fltMaxBaseRentMonthly ) {
		$this->set( 'm_fltMaxBaseRentMonthly', CStrings::strToFloatDef( $fltMaxBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getMaxBaseRentMonthly() {
		return $this->m_fltMaxBaseRentMonthly;
	}

	public function sqlMaxBaseRentMonthly() {
		return ( true == isset( $this->m_fltMaxBaseRentMonthly ) ) ? ( string ) $this->m_fltMaxBaseRentMonthly : 'NULL';
	}

	public function setAvgBaseRentMonthly( $fltAvgBaseRentMonthly ) {
		$this->set( 'm_fltAvgBaseRentMonthly', CStrings::strToFloatDef( $fltAvgBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getAvgBaseRentMonthly() {
		return $this->m_fltAvgBaseRentMonthly;
	}

	public function sqlAvgBaseRentMonthly() {
		return ( true == isset( $this->m_fltAvgBaseRentMonthly ) ) ? ( string ) $this->m_fltAvgBaseRentMonthly : 'NULL';
	}

	public function setMinAmenityRentMonthly( $fltMinAmenityRentMonthly ) {
		$this->set( 'm_fltMinAmenityRentMonthly', CStrings::strToFloatDef( $fltMinAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getMinAmenityRentMonthly() {
		return $this->m_fltMinAmenityRentMonthly;
	}

	public function sqlMinAmenityRentMonthly() {
		return ( true == isset( $this->m_fltMinAmenityRentMonthly ) ) ? ( string ) $this->m_fltMinAmenityRentMonthly : 'NULL';
	}

	public function setMaxAmenityRentMonthly( $fltMaxAmenityRentMonthly ) {
		$this->set( 'm_fltMaxAmenityRentMonthly', CStrings::strToFloatDef( $fltMaxAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getMaxAmenityRentMonthly() {
		return $this->m_fltMaxAmenityRentMonthly;
	}

	public function sqlMaxAmenityRentMonthly() {
		return ( true == isset( $this->m_fltMaxAmenityRentMonthly ) ) ? ( string ) $this->m_fltMaxAmenityRentMonthly : 'NULL';
	}

	public function setAvgAmenityRentMonthly( $fltAvgAmenityRentMonthly ) {
		$this->set( 'm_fltAvgAmenityRentMonthly', CStrings::strToFloatDef( $fltAvgAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getAvgAmenityRentMonthly() {
		return $this->m_fltAvgAmenityRentMonthly;
	}

	public function sqlAvgAmenityRentMonthly() {
		return ( true == isset( $this->m_fltAvgAmenityRentMonthly ) ) ? ( string ) $this->m_fltAvgAmenityRentMonthly : 'NULL';
	}

	public function setMinSpecialRentMonthly( $fltMinSpecialRentMonthly ) {
		$this->set( 'm_fltMinSpecialRentMonthly', CStrings::strToFloatDef( $fltMinSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getMinSpecialRentMonthly() {
		return $this->m_fltMinSpecialRentMonthly;
	}

	public function sqlMinSpecialRentMonthly() {
		return ( true == isset( $this->m_fltMinSpecialRentMonthly ) ) ? ( string ) $this->m_fltMinSpecialRentMonthly : 'NULL';
	}

	public function setMaxSpecialRentMonthly( $fltMaxSpecialRentMonthly ) {
		$this->set( 'm_fltMaxSpecialRentMonthly', CStrings::strToFloatDef( $fltMaxSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getMaxSpecialRentMonthly() {
		return $this->m_fltMaxSpecialRentMonthly;
	}

	public function sqlMaxSpecialRentMonthly() {
		return ( true == isset( $this->m_fltMaxSpecialRentMonthly ) ) ? ( string ) $this->m_fltMaxSpecialRentMonthly : 'NULL';
	}

	public function setAvgSpecialRentMonthly( $fltAvgSpecialRentMonthly ) {
		$this->set( 'm_fltAvgSpecialRentMonthly', CStrings::strToFloatDef( $fltAvgSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getAvgSpecialRentMonthly() {
		return $this->m_fltAvgSpecialRentMonthly;
	}

	public function sqlAvgSpecialRentMonthly() {
		return ( true == isset( $this->m_fltAvgSpecialRentMonthly ) ) ? ( string ) $this->m_fltAvgSpecialRentMonthly : 'NULL';
	}

	public function setMinAddOnRentMonthly( $fltMinAddOnRentMonthly ) {
		$this->set( 'm_fltMinAddOnRentMonthly', CStrings::strToFloatDef( $fltMinAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getMinAddOnRentMonthly() {
		return $this->m_fltMinAddOnRentMonthly;
	}

	public function sqlMinAddOnRentMonthly() {
		return ( true == isset( $this->m_fltMinAddOnRentMonthly ) ) ? ( string ) $this->m_fltMinAddOnRentMonthly : 'NULL';
	}

	public function setMaxAddOnRentMonthly( $fltMaxAddOnRentMonthly ) {
		$this->set( 'm_fltMaxAddOnRentMonthly', CStrings::strToFloatDef( $fltMaxAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getMaxAddOnRentMonthly() {
		return $this->m_fltMaxAddOnRentMonthly;
	}

	public function sqlMaxAddOnRentMonthly() {
		return ( true == isset( $this->m_fltMaxAddOnRentMonthly ) ) ? ( string ) $this->m_fltMaxAddOnRentMonthly : 'NULL';
	}

	public function setAvgAddOnRentMonthly( $fltAvgAddOnRentMonthly ) {
		$this->set( 'm_fltAvgAddOnRentMonthly', CStrings::strToFloatDef( $fltAvgAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getAvgAddOnRentMonthly() {
		return $this->m_fltAvgAddOnRentMonthly;
	}

	public function sqlAvgAddOnRentMonthly() {
		return ( true == isset( $this->m_fltAvgAddOnRentMonthly ) ) ? ( string ) $this->m_fltAvgAddOnRentMonthly : 'NULL';
	}

	public function setMinMarketRentMonthly( $fltMinMarketRentMonthly ) {
		$this->set( 'm_fltMinMarketRentMonthly', CStrings::strToFloatDef( $fltMinMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getMinMarketRentMonthly() {
		return $this->m_fltMinMarketRentMonthly;
	}

	public function sqlMinMarketRentMonthly() {
		return ( true == isset( $this->m_fltMinMarketRentMonthly ) ) ? ( string ) $this->m_fltMinMarketRentMonthly : 'NULL';
	}

	public function setMaxMarketRentMonthly( $fltMaxMarketRentMonthly ) {
		$this->set( 'm_fltMaxMarketRentMonthly', CStrings::strToFloatDef( $fltMaxMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getMaxMarketRentMonthly() {
		return $this->m_fltMaxMarketRentMonthly;
	}

	public function sqlMaxMarketRentMonthly() {
		return ( true == isset( $this->m_fltMaxMarketRentMonthly ) ) ? ( string ) $this->m_fltMaxMarketRentMonthly : 'NULL';
	}

	public function setAvgMarketRentMonthly( $fltAvgMarketRentMonthly ) {
		$this->set( 'm_fltAvgMarketRentMonthly', CStrings::strToFloatDef( $fltAvgMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getAvgMarketRentMonthly() {
		return $this->m_fltAvgMarketRentMonthly;
	}

	public function sqlAvgMarketRentMonthly() {
		return ( true == isset( $this->m_fltAvgMarketRentMonthly ) ) ? ( string ) $this->m_fltAvgMarketRentMonthly : 'NULL';
	}

	public function setEntrataMinBaseRentMonthly( $fltEntrataMinBaseRentMonthly ) {
		$this->set( 'm_fltEntrataMinBaseRentMonthly', CStrings::strToFloatDef( $fltEntrataMinBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinBaseRentMonthly() {
		return $this->m_fltEntrataMinBaseRentMonthly;
	}

	public function sqlEntrataMinBaseRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMinBaseRentMonthly ) ) ? ( string ) $this->m_fltEntrataMinBaseRentMonthly : 'NULL';
	}

	public function setEntrataMaxBaseRentMonthly( $fltEntrataMaxBaseRentMonthly ) {
		$this->set( 'm_fltEntrataMaxBaseRentMonthly', CStrings::strToFloatDef( $fltEntrataMaxBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxBaseRentMonthly() {
		return $this->m_fltEntrataMaxBaseRentMonthly;
	}

	public function sqlEntrataMaxBaseRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxBaseRentMonthly ) ) ? ( string ) $this->m_fltEntrataMaxBaseRentMonthly : 'NULL';
	}

	public function setEntrataAvgBaseRentMonthly( $fltEntrataAvgBaseRentMonthly ) {
		$this->set( 'm_fltEntrataAvgBaseRentMonthly', CStrings::strToFloatDef( $fltEntrataAvgBaseRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgBaseRentMonthly() {
		return $this->m_fltEntrataAvgBaseRentMonthly;
	}

	public function sqlEntrataAvgBaseRentMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgBaseRentMonthly ) ) ? ( string ) $this->m_fltEntrataAvgBaseRentMonthly : 'NULL';
	}

	public function setEntrataMinAmenityRentMonthly( $fltEntrataMinAmenityRentMonthly ) {
		$this->set( 'm_fltEntrataMinAmenityRentMonthly', CStrings::strToFloatDef( $fltEntrataMinAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAmenityRentMonthly() {
		return $this->m_fltEntrataMinAmenityRentMonthly;
	}

	public function sqlEntrataMinAmenityRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAmenityRentMonthly ) ) ? ( string ) $this->m_fltEntrataMinAmenityRentMonthly : 'NULL';
	}

	public function setEntrataMaxAmenityRentMonthly( $fltEntrataMaxAmenityRentMonthly ) {
		$this->set( 'm_fltEntrataMaxAmenityRentMonthly', CStrings::strToFloatDef( $fltEntrataMaxAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAmenityRentMonthly() {
		return $this->m_fltEntrataMaxAmenityRentMonthly;
	}

	public function sqlEntrataMaxAmenityRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityRentMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAmenityRentMonthly : 'NULL';
	}

	public function setEntrataAvgAmenityRentMonthly( $fltEntrataAvgAmenityRentMonthly ) {
		$this->set( 'm_fltEntrataAvgAmenityRentMonthly', CStrings::strToFloatDef( $fltEntrataAvgAmenityRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAmenityRentMonthly() {
		return $this->m_fltEntrataAvgAmenityRentMonthly;
	}

	public function sqlEntrataAvgAmenityRentMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityRentMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAmenityRentMonthly : 'NULL';
	}

	public function setEntrataMinSpecialRentMonthly( $fltEntrataMinSpecialRentMonthly ) {
		$this->set( 'm_fltEntrataMinSpecialRentMonthly', CStrings::strToFloatDef( $fltEntrataMinSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinSpecialRentMonthly() {
		return $this->m_fltEntrataMinSpecialRentMonthly;
	}

	public function sqlEntrataMinSpecialRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMinSpecialRentMonthly ) ) ? ( string ) $this->m_fltEntrataMinSpecialRentMonthly : 'NULL';
	}

	public function setEntrataMaxSpecialRentMonthly( $fltEntrataMaxSpecialRentMonthly ) {
		$this->set( 'm_fltEntrataMaxSpecialRentMonthly', CStrings::strToFloatDef( $fltEntrataMaxSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxSpecialRentMonthly() {
		return $this->m_fltEntrataMaxSpecialRentMonthly;
	}

	public function sqlEntrataMaxSpecialRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialRentMonthly ) ) ? ( string ) $this->m_fltEntrataMaxSpecialRentMonthly : 'NULL';
	}

	public function setEntrataAvgSpecialRentMonthly( $fltEntrataAvgSpecialRentMonthly ) {
		$this->set( 'm_fltEntrataAvgSpecialRentMonthly', CStrings::strToFloatDef( $fltEntrataAvgSpecialRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgSpecialRentMonthly() {
		return $this->m_fltEntrataAvgSpecialRentMonthly;
	}

	public function sqlEntrataAvgSpecialRentMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialRentMonthly ) ) ? ( string ) $this->m_fltEntrataAvgSpecialRentMonthly : 'NULL';
	}

	public function setEntrataMinAddOnRentMonthly( $fltEntrataMinAddOnRentMonthly ) {
		$this->set( 'm_fltEntrataMinAddOnRentMonthly', CStrings::strToFloatDef( $fltEntrataMinAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAddOnRentMonthly() {
		return $this->m_fltEntrataMinAddOnRentMonthly;
	}

	public function sqlEntrataMinAddOnRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAddOnRentMonthly ) ) ? ( string ) $this->m_fltEntrataMinAddOnRentMonthly : 'NULL';
	}

	public function setEntrataMaxAddOnRentMonthly( $fltEntrataMaxAddOnRentMonthly ) {
		$this->set( 'm_fltEntrataMaxAddOnRentMonthly', CStrings::strToFloatDef( $fltEntrataMaxAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAddOnRentMonthly() {
		return $this->m_fltEntrataMaxAddOnRentMonthly;
	}

	public function sqlEntrataMaxAddOnRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnRentMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAddOnRentMonthly : 'NULL';
	}

	public function setEntrataAvgAddOnRentMonthly( $fltEntrataAvgAddOnRentMonthly ) {
		$this->set( 'm_fltEntrataAvgAddOnRentMonthly', CStrings::strToFloatDef( $fltEntrataAvgAddOnRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAddOnRentMonthly() {
		return $this->m_fltEntrataAvgAddOnRentMonthly;
	}

	public function sqlEntrataAvgAddOnRentMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnRentMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAddOnRentMonthly : 'NULL';
	}

	public function setEntrataMinMarketRentMonthly( $fltEntrataMinMarketRentMonthly ) {
		$this->set( 'm_fltEntrataMinMarketRentMonthly', CStrings::strToFloatDef( $fltEntrataMinMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinMarketRentMonthly() {
		return $this->m_fltEntrataMinMarketRentMonthly;
	}

	public function sqlEntrataMinMarketRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMinMarketRentMonthly ) ) ? ( string ) $this->m_fltEntrataMinMarketRentMonthly : 'NULL';
	}

	public function setEntrataMaxMarketRentMonthly( $fltEntrataMaxMarketRentMonthly ) {
		$this->set( 'm_fltEntrataMaxMarketRentMonthly', CStrings::strToFloatDef( $fltEntrataMaxMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxMarketRentMonthly() {
		return $this->m_fltEntrataMaxMarketRentMonthly;
	}

	public function sqlEntrataMaxMarketRentMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxMarketRentMonthly ) ) ? ( string ) $this->m_fltEntrataMaxMarketRentMonthly : 'NULL';
	}

	public function setEntrataAvgMarketRentMonthly( $fltEntrataAvgMarketRentMonthly ) {
		$this->set( 'm_fltEntrataAvgMarketRentMonthly', CStrings::strToFloatDef( $fltEntrataAvgMarketRentMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgMarketRentMonthly() {
		return $this->m_fltEntrataAvgMarketRentMonthly;
	}

	public function sqlEntrataAvgMarketRentMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgMarketRentMonthly ) ) ? ( string ) $this->m_fltEntrataAvgMarketRentMonthly : 'NULL';
	}

	public function setIncludeTaxInAdvertisedRent( $boolIncludeTaxInAdvertisedRent ) {
		$this->set( 'm_boolIncludeTaxInAdvertisedRent', CStrings::strToBool( $boolIncludeTaxInAdvertisedRent ) );
	}

	public function getIncludeTaxInAdvertisedRent() {
		return $this->m_boolIncludeTaxInAdvertisedRent;
	}

	public function sqlIncludeTaxInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeTaxInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeTaxInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeOtherInAdvertisedRent( $boolIncludeOtherInAdvertisedRent ) {
		$this->set( 'm_boolIncludeOtherInAdvertisedRent', CStrings::strToBool( $boolIncludeOtherInAdvertisedRent ) );
	}

	public function getIncludeOtherInAdvertisedRent() {
		return $this->m_boolIncludeOtherInAdvertisedRent;
	}

	public function sqlIncludeOtherInAdvertisedRent() {
		return ( true == isset( $this->m_boolIncludeOtherInAdvertisedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeOtherInAdvertisedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMinBaseAmount( $fltMinBaseAmount ) {
		$this->set( 'm_fltMinBaseAmount', CStrings::strToFloatDef( $fltMinBaseAmount, NULL, false, 2 ) );
	}

	public function getMinBaseAmount() {
		return $this->m_fltMinBaseAmount;
	}

	public function sqlMinBaseAmount() {
		return ( true == isset( $this->m_fltMinBaseAmount ) ) ? ( string ) $this->m_fltMinBaseAmount : 'NULL';
	}

	public function setMaxBaseAmount( $fltMaxBaseAmount ) {
		$this->set( 'm_fltMaxBaseAmount', CStrings::strToFloatDef( $fltMaxBaseAmount, NULL, false, 2 ) );
	}

	public function getMaxBaseAmount() {
		return $this->m_fltMaxBaseAmount;
	}

	public function sqlMaxBaseAmount() {
		return ( true == isset( $this->m_fltMaxBaseAmount ) ) ? ( string ) $this->m_fltMaxBaseAmount : 'NULL';
	}

	public function setAvgBaseAmount( $fltAvgBaseAmount ) {
		$this->set( 'm_fltAvgBaseAmount', CStrings::strToFloatDef( $fltAvgBaseAmount, NULL, false, 2 ) );
	}

	public function getAvgBaseAmount() {
		return $this->m_fltAvgBaseAmount;
	}

	public function sqlAvgBaseAmount() {
		return ( true == isset( $this->m_fltAvgBaseAmount ) ) ? ( string ) $this->m_fltAvgBaseAmount : 'NULL';
	}

	public function setMinAmenityAmount( $fltMinAmenityAmount ) {
		$this->set( 'm_fltMinAmenityAmount', CStrings::strToFloatDef( $fltMinAmenityAmount, NULL, false, 2 ) );
	}

	public function getMinAmenityAmount() {
		return $this->m_fltMinAmenityAmount;
	}

	public function sqlMinAmenityAmount() {
		return ( true == isset( $this->m_fltMinAmenityAmount ) ) ? ( string ) $this->m_fltMinAmenityAmount : 'NULL';
	}

	public function setMaxAmenityAmount( $fltMaxAmenityAmount ) {
		$this->set( 'm_fltMaxAmenityAmount', CStrings::strToFloatDef( $fltMaxAmenityAmount, NULL, false, 2 ) );
	}

	public function getMaxAmenityAmount() {
		return $this->m_fltMaxAmenityAmount;
	}

	public function sqlMaxAmenityAmount() {
		return ( true == isset( $this->m_fltMaxAmenityAmount ) ) ? ( string ) $this->m_fltMaxAmenityAmount : 'NULL';
	}

	public function setAvgAmenityAmount( $fltAvgAmenityAmount ) {
		$this->set( 'm_fltAvgAmenityAmount', CStrings::strToFloatDef( $fltAvgAmenityAmount, NULL, false, 2 ) );
	}

	public function getAvgAmenityAmount() {
		return $this->m_fltAvgAmenityAmount;
	}

	public function sqlAvgAmenityAmount() {
		return ( true == isset( $this->m_fltAvgAmenityAmount ) ) ? ( string ) $this->m_fltAvgAmenityAmount : 'NULL';
	}

	public function setMinSpecialAmount( $fltMinSpecialAmount ) {
		$this->set( 'm_fltMinSpecialAmount', CStrings::strToFloatDef( $fltMinSpecialAmount, NULL, false, 2 ) );
	}

	public function getMinSpecialAmount() {
		return $this->m_fltMinSpecialAmount;
	}

	public function sqlMinSpecialAmount() {
		return ( true == isset( $this->m_fltMinSpecialAmount ) ) ? ( string ) $this->m_fltMinSpecialAmount : 'NULL';
	}

	public function setMaxSpecialAmount( $fltMaxSpecialAmount ) {
		$this->set( 'm_fltMaxSpecialAmount', CStrings::strToFloatDef( $fltMaxSpecialAmount, NULL, false, 2 ) );
	}

	public function getMaxSpecialAmount() {
		return $this->m_fltMaxSpecialAmount;
	}

	public function sqlMaxSpecialAmount() {
		return ( true == isset( $this->m_fltMaxSpecialAmount ) ) ? ( string ) $this->m_fltMaxSpecialAmount : 'NULL';
	}

	public function setAvgSpecialAmount( $fltAvgSpecialAmount ) {
		$this->set( 'm_fltAvgSpecialAmount', CStrings::strToFloatDef( $fltAvgSpecialAmount, NULL, false, 2 ) );
	}

	public function getAvgSpecialAmount() {
		return $this->m_fltAvgSpecialAmount;
	}

	public function sqlAvgSpecialAmount() {
		return ( true == isset( $this->m_fltAvgSpecialAmount ) ) ? ( string ) $this->m_fltAvgSpecialAmount : 'NULL';
	}

	public function setMinAddOnAmount( $fltMinAddOnAmount ) {
		$this->set( 'm_fltMinAddOnAmount', CStrings::strToFloatDef( $fltMinAddOnAmount, NULL, false, 2 ) );
	}

	public function getMinAddOnAmount() {
		return $this->m_fltMinAddOnAmount;
	}

	public function sqlMinAddOnAmount() {
		return ( true == isset( $this->m_fltMinAddOnAmount ) ) ? ( string ) $this->m_fltMinAddOnAmount : 'NULL';
	}

	public function setMaxAddOnAmount( $fltMaxAddOnAmount ) {
		$this->set( 'm_fltMaxAddOnAmount', CStrings::strToFloatDef( $fltMaxAddOnAmount, NULL, false, 2 ) );
	}

	public function getMaxAddOnAmount() {
		return $this->m_fltMaxAddOnAmount;
	}

	public function sqlMaxAddOnAmount() {
		return ( true == isset( $this->m_fltMaxAddOnAmount ) ) ? ( string ) $this->m_fltMaxAddOnAmount : 'NULL';
	}

	public function setAvgAddOnAmount( $fltAvgAddOnAmount ) {
		$this->set( 'm_fltAvgAddOnAmount', CStrings::strToFloatDef( $fltAvgAddOnAmount, NULL, false, 2 ) );
	}

	public function getAvgAddOnAmount() {
		return $this->m_fltAvgAddOnAmount;
	}

	public function sqlAvgAddOnAmount() {
		return ( true == isset( $this->m_fltAvgAddOnAmount ) ) ? ( string ) $this->m_fltAvgAddOnAmount : 'NULL';
	}

	public function setEntrataMinBaseAmount( $fltEntrataMinBaseAmount ) {
		$this->set( 'm_fltEntrataMinBaseAmount', CStrings::strToFloatDef( $fltEntrataMinBaseAmount, NULL, false, 2 ) );
	}

	public function getEntrataMinBaseAmount() {
		return $this->m_fltEntrataMinBaseAmount;
	}

	public function sqlEntrataMinBaseAmount() {
		return ( true == isset( $this->m_fltEntrataMinBaseAmount ) ) ? ( string ) $this->m_fltEntrataMinBaseAmount : 'NULL';
	}

	public function setEntrataMaxBaseAmount( $fltEntrataMaxBaseAmount ) {
		$this->set( 'm_fltEntrataMaxBaseAmount', CStrings::strToFloatDef( $fltEntrataMaxBaseAmount, NULL, false, 2 ) );
	}

	public function getEntrataMaxBaseAmount() {
		return $this->m_fltEntrataMaxBaseAmount;
	}

	public function sqlEntrataMaxBaseAmount() {
		return ( true == isset( $this->m_fltEntrataMaxBaseAmount ) ) ? ( string ) $this->m_fltEntrataMaxBaseAmount : 'NULL';
	}

	public function setEntrataAvgBaseAmount( $fltEntrataAvgBaseAmount ) {
		$this->set( 'm_fltEntrataAvgBaseAmount', CStrings::strToFloatDef( $fltEntrataAvgBaseAmount, NULL, false, 2 ) );
	}

	public function getEntrataAvgBaseAmount() {
		return $this->m_fltEntrataAvgBaseAmount;
	}

	public function sqlEntrataAvgBaseAmount() {
		return ( true == isset( $this->m_fltEntrataAvgBaseAmount ) ) ? ( string ) $this->m_fltEntrataAvgBaseAmount : 'NULL';
	}

	public function setEntrataMinAmenityAmount( $fltEntrataMinAmenityAmount ) {
		$this->set( 'm_fltEntrataMinAmenityAmount', CStrings::strToFloatDef( $fltEntrataMinAmenityAmount, NULL, false, 2 ) );
	}

	public function getEntrataMinAmenityAmount() {
		return $this->m_fltEntrataMinAmenityAmount;
	}

	public function sqlEntrataMinAmenityAmount() {
		return ( true == isset( $this->m_fltEntrataMinAmenityAmount ) ) ? ( string ) $this->m_fltEntrataMinAmenityAmount : 'NULL';
	}

	public function setEntrataMaxAmenityAmount( $fltEntrataMaxAmenityAmount ) {
		$this->set( 'm_fltEntrataMaxAmenityAmount', CStrings::strToFloatDef( $fltEntrataMaxAmenityAmount, NULL, false, 2 ) );
	}

	public function getEntrataMaxAmenityAmount() {
		return $this->m_fltEntrataMaxAmenityAmount;
	}

	public function sqlEntrataMaxAmenityAmount() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityAmount ) ) ? ( string ) $this->m_fltEntrataMaxAmenityAmount : 'NULL';
	}

	public function setEntrataAvgAmenityAmount( $fltEntrataAvgAmenityAmount ) {
		$this->set( 'm_fltEntrataAvgAmenityAmount', CStrings::strToFloatDef( $fltEntrataAvgAmenityAmount, NULL, false, 2 ) );
	}

	public function getEntrataAvgAmenityAmount() {
		return $this->m_fltEntrataAvgAmenityAmount;
	}

	public function sqlEntrataAvgAmenityAmount() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityAmount ) ) ? ( string ) $this->m_fltEntrataAvgAmenityAmount : 'NULL';
	}

	public function setEntrataMinSpecialAmount( $fltEntrataMinSpecialAmount ) {
		$this->set( 'm_fltEntrataMinSpecialAmount', CStrings::strToFloatDef( $fltEntrataMinSpecialAmount, NULL, false, 2 ) );
	}

	public function getEntrataMinSpecialAmount() {
		return $this->m_fltEntrataMinSpecialAmount;
	}

	public function sqlEntrataMinSpecialAmount() {
		return ( true == isset( $this->m_fltEntrataMinSpecialAmount ) ) ? ( string ) $this->m_fltEntrataMinSpecialAmount : 'NULL';
	}

	public function setEntrataMaxSpecialAmount( $fltEntrataMaxSpecialAmount ) {
		$this->set( 'm_fltEntrataMaxSpecialAmount', CStrings::strToFloatDef( $fltEntrataMaxSpecialAmount, NULL, false, 2 ) );
	}

	public function getEntrataMaxSpecialAmount() {
		return $this->m_fltEntrataMaxSpecialAmount;
	}

	public function sqlEntrataMaxSpecialAmount() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialAmount ) ) ? ( string ) $this->m_fltEntrataMaxSpecialAmount : 'NULL';
	}

	public function setEntrataAvgSpecialAmount( $fltEntrataAvgSpecialAmount ) {
		$this->set( 'm_fltEntrataAvgSpecialAmount', CStrings::strToFloatDef( $fltEntrataAvgSpecialAmount, NULL, false, 2 ) );
	}

	public function getEntrataAvgSpecialAmount() {
		return $this->m_fltEntrataAvgSpecialAmount;
	}

	public function sqlEntrataAvgSpecialAmount() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialAmount ) ) ? ( string ) $this->m_fltEntrataAvgSpecialAmount : 'NULL';
	}

	public function setEntrataMinAddOnAmount( $fltEntrataMinAddOnAmount ) {
		$this->set( 'm_fltEntrataMinAddOnAmount', CStrings::strToFloatDef( $fltEntrataMinAddOnAmount, NULL, false, 2 ) );
	}

	public function getEntrataMinAddOnAmount() {
		return $this->m_fltEntrataMinAddOnAmount;
	}

	public function sqlEntrataMinAddOnAmount() {
		return ( true == isset( $this->m_fltEntrataMinAddOnAmount ) ) ? ( string ) $this->m_fltEntrataMinAddOnAmount : 'NULL';
	}

	public function setEntrataMaxAddOnAmount( $fltEntrataMaxAddOnAmount ) {
		$this->set( 'm_fltEntrataMaxAddOnAmount', CStrings::strToFloatDef( $fltEntrataMaxAddOnAmount, NULL, false, 2 ) );
	}

	public function getEntrataMaxAddOnAmount() {
		return $this->m_fltEntrataMaxAddOnAmount;
	}

	public function sqlEntrataMaxAddOnAmount() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnAmount ) ) ? ( string ) $this->m_fltEntrataMaxAddOnAmount : 'NULL';
	}

	public function setEntrataAvgAddOnAmount( $fltEntrataAvgAddOnAmount ) {
		$this->set( 'm_fltEntrataAvgAddOnAmount', CStrings::strToFloatDef( $fltEntrataAvgAddOnAmount, NULL, false, 2 ) );
	}

	public function getEntrataAvgAddOnAmount() {
		return $this->m_fltEntrataAvgAddOnAmount;
	}

	public function sqlEntrataAvgAddOnAmount() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnAmount ) ) ? ( string ) $this->m_fltEntrataAvgAddOnAmount : 'NULL';
	}

	public function setMinBaseTax( $fltMinBaseTax ) {
		$this->set( 'm_fltMinBaseTax', CStrings::strToFloatDef( $fltMinBaseTax, NULL, false, 2 ) );
	}

	public function getMinBaseTax() {
		return $this->m_fltMinBaseTax;
	}

	public function sqlMinBaseTax() {
		return ( true == isset( $this->m_fltMinBaseTax ) ) ? ( string ) $this->m_fltMinBaseTax : 'NULL';
	}

	public function setMaxBaseTax( $fltMaxBaseTax ) {
		$this->set( 'm_fltMaxBaseTax', CStrings::strToFloatDef( $fltMaxBaseTax, NULL, false, 2 ) );
	}

	public function getMaxBaseTax() {
		return $this->m_fltMaxBaseTax;
	}

	public function sqlMaxBaseTax() {
		return ( true == isset( $this->m_fltMaxBaseTax ) ) ? ( string ) $this->m_fltMaxBaseTax : 'NULL';
	}

	public function setAvgBaseTax( $fltAvgBaseTax ) {
		$this->set( 'm_fltAvgBaseTax', CStrings::strToFloatDef( $fltAvgBaseTax, NULL, false, 2 ) );
	}

	public function getAvgBaseTax() {
		return $this->m_fltAvgBaseTax;
	}

	public function sqlAvgBaseTax() {
		return ( true == isset( $this->m_fltAvgBaseTax ) ) ? ( string ) $this->m_fltAvgBaseTax : 'NULL';
	}

	public function setMinAmenityTax( $fltMinAmenityTax ) {
		$this->set( 'm_fltMinAmenityTax', CStrings::strToFloatDef( $fltMinAmenityTax, NULL, false, 2 ) );
	}

	public function getMinAmenityTax() {
		return $this->m_fltMinAmenityTax;
	}

	public function sqlMinAmenityTax() {
		return ( true == isset( $this->m_fltMinAmenityTax ) ) ? ( string ) $this->m_fltMinAmenityTax : 'NULL';
	}

	public function setMaxAmenityTax( $fltMaxAmenityTax ) {
		$this->set( 'm_fltMaxAmenityTax', CStrings::strToFloatDef( $fltMaxAmenityTax, NULL, false, 2 ) );
	}

	public function getMaxAmenityTax() {
		return $this->m_fltMaxAmenityTax;
	}

	public function sqlMaxAmenityTax() {
		return ( true == isset( $this->m_fltMaxAmenityTax ) ) ? ( string ) $this->m_fltMaxAmenityTax : 'NULL';
	}

	public function setAvgAmenityTax( $fltAvgAmenityTax ) {
		$this->set( 'm_fltAvgAmenityTax', CStrings::strToFloatDef( $fltAvgAmenityTax, NULL, false, 2 ) );
	}

	public function getAvgAmenityTax() {
		return $this->m_fltAvgAmenityTax;
	}

	public function sqlAvgAmenityTax() {
		return ( true == isset( $this->m_fltAvgAmenityTax ) ) ? ( string ) $this->m_fltAvgAmenityTax : 'NULL';
	}

	public function setMinSpecialTax( $fltMinSpecialTax ) {
		$this->set( 'm_fltMinSpecialTax', CStrings::strToFloatDef( $fltMinSpecialTax, NULL, false, 2 ) );
	}

	public function getMinSpecialTax() {
		return $this->m_fltMinSpecialTax;
	}

	public function sqlMinSpecialTax() {
		return ( true == isset( $this->m_fltMinSpecialTax ) ) ? ( string ) $this->m_fltMinSpecialTax : 'NULL';
	}

	public function setMaxSpecialTax( $fltMaxSpecialTax ) {
		$this->set( 'm_fltMaxSpecialTax', CStrings::strToFloatDef( $fltMaxSpecialTax, NULL, false, 2 ) );
	}

	public function getMaxSpecialTax() {
		return $this->m_fltMaxSpecialTax;
	}

	public function sqlMaxSpecialTax() {
		return ( true == isset( $this->m_fltMaxSpecialTax ) ) ? ( string ) $this->m_fltMaxSpecialTax : 'NULL';
	}

	public function setAvgSpecialTax( $fltAvgSpecialTax ) {
		$this->set( 'm_fltAvgSpecialTax', CStrings::strToFloatDef( $fltAvgSpecialTax, NULL, false, 2 ) );
	}

	public function getAvgSpecialTax() {
		return $this->m_fltAvgSpecialTax;
	}

	public function sqlAvgSpecialTax() {
		return ( true == isset( $this->m_fltAvgSpecialTax ) ) ? ( string ) $this->m_fltAvgSpecialTax : 'NULL';
	}

	public function setMinAddOnTax( $fltMinAddOnTax ) {
		$this->set( 'm_fltMinAddOnTax', CStrings::strToFloatDef( $fltMinAddOnTax, NULL, false, 2 ) );
	}

	public function getMinAddOnTax() {
		return $this->m_fltMinAddOnTax;
	}

	public function sqlMinAddOnTax() {
		return ( true == isset( $this->m_fltMinAddOnTax ) ) ? ( string ) $this->m_fltMinAddOnTax : 'NULL';
	}

	public function setMaxAddOnTax( $fltMaxAddOnTax ) {
		$this->set( 'm_fltMaxAddOnTax', CStrings::strToFloatDef( $fltMaxAddOnTax, NULL, false, 2 ) );
	}

	public function getMaxAddOnTax() {
		return $this->m_fltMaxAddOnTax;
	}

	public function sqlMaxAddOnTax() {
		return ( true == isset( $this->m_fltMaxAddOnTax ) ) ? ( string ) $this->m_fltMaxAddOnTax : 'NULL';
	}

	public function setAvgAddOnTax( $fltAvgAddOnTax ) {
		$this->set( 'm_fltAvgAddOnTax', CStrings::strToFloatDef( $fltAvgAddOnTax, NULL, false, 2 ) );
	}

	public function getAvgAddOnTax() {
		return $this->m_fltAvgAddOnTax;
	}

	public function sqlAvgAddOnTax() {
		return ( true == isset( $this->m_fltAvgAddOnTax ) ) ? ( string ) $this->m_fltAvgAddOnTax : 'NULL';
	}

	public function setEntrataMinBaseTax( $fltEntrataMinBaseTax ) {
		$this->set( 'm_fltEntrataMinBaseTax', CStrings::strToFloatDef( $fltEntrataMinBaseTax, NULL, false, 2 ) );
	}

	public function getEntrataMinBaseTax() {
		return $this->m_fltEntrataMinBaseTax;
	}

	public function sqlEntrataMinBaseTax() {
		return ( true == isset( $this->m_fltEntrataMinBaseTax ) ) ? ( string ) $this->m_fltEntrataMinBaseTax : 'NULL';
	}

	public function setEntrataMaxBaseTax( $fltEntrataMaxBaseTax ) {
		$this->set( 'm_fltEntrataMaxBaseTax', CStrings::strToFloatDef( $fltEntrataMaxBaseTax, NULL, false, 2 ) );
	}

	public function getEntrataMaxBaseTax() {
		return $this->m_fltEntrataMaxBaseTax;
	}

	public function sqlEntrataMaxBaseTax() {
		return ( true == isset( $this->m_fltEntrataMaxBaseTax ) ) ? ( string ) $this->m_fltEntrataMaxBaseTax : 'NULL';
	}

	public function setEntrataAvgBaseTax( $fltEntrataAvgBaseTax ) {
		$this->set( 'm_fltEntrataAvgBaseTax', CStrings::strToFloatDef( $fltEntrataAvgBaseTax, NULL, false, 2 ) );
	}

	public function getEntrataAvgBaseTax() {
		return $this->m_fltEntrataAvgBaseTax;
	}

	public function sqlEntrataAvgBaseTax() {
		return ( true == isset( $this->m_fltEntrataAvgBaseTax ) ) ? ( string ) $this->m_fltEntrataAvgBaseTax : 'NULL';
	}

	public function setEntrataMinAmenityTax( $fltEntrataMinAmenityTax ) {
		$this->set( 'm_fltEntrataMinAmenityTax', CStrings::strToFloatDef( $fltEntrataMinAmenityTax, NULL, false, 2 ) );
	}

	public function getEntrataMinAmenityTax() {
		return $this->m_fltEntrataMinAmenityTax;
	}

	public function sqlEntrataMinAmenityTax() {
		return ( true == isset( $this->m_fltEntrataMinAmenityTax ) ) ? ( string ) $this->m_fltEntrataMinAmenityTax : 'NULL';
	}

	public function setEntrataMaxAmenityTax( $fltEntrataMaxAmenityTax ) {
		$this->set( 'm_fltEntrataMaxAmenityTax', CStrings::strToFloatDef( $fltEntrataMaxAmenityTax, NULL, false, 2 ) );
	}

	public function getEntrataMaxAmenityTax() {
		return $this->m_fltEntrataMaxAmenityTax;
	}

	public function sqlEntrataMaxAmenityTax() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityTax ) ) ? ( string ) $this->m_fltEntrataMaxAmenityTax : 'NULL';
	}

	public function setEntrataAvgAmenityTax( $fltEntrataAvgAmenityTax ) {
		$this->set( 'm_fltEntrataAvgAmenityTax', CStrings::strToFloatDef( $fltEntrataAvgAmenityTax, NULL, false, 2 ) );
	}

	public function getEntrataAvgAmenityTax() {
		return $this->m_fltEntrataAvgAmenityTax;
	}

	public function sqlEntrataAvgAmenityTax() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityTax ) ) ? ( string ) $this->m_fltEntrataAvgAmenityTax : 'NULL';
	}

	public function setEntrataMinSpecialTax( $fltEntrataMinSpecialTax ) {
		$this->set( 'm_fltEntrataMinSpecialTax', CStrings::strToFloatDef( $fltEntrataMinSpecialTax, NULL, false, 2 ) );
	}

	public function getEntrataMinSpecialTax() {
		return $this->m_fltEntrataMinSpecialTax;
	}

	public function sqlEntrataMinSpecialTax() {
		return ( true == isset( $this->m_fltEntrataMinSpecialTax ) ) ? ( string ) $this->m_fltEntrataMinSpecialTax : 'NULL';
	}

	public function setEntrataMaxSpecialTax( $fltEntrataMaxSpecialTax ) {
		$this->set( 'm_fltEntrataMaxSpecialTax', CStrings::strToFloatDef( $fltEntrataMaxSpecialTax, NULL, false, 2 ) );
	}

	public function getEntrataMaxSpecialTax() {
		return $this->m_fltEntrataMaxSpecialTax;
	}

	public function sqlEntrataMaxSpecialTax() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialTax ) ) ? ( string ) $this->m_fltEntrataMaxSpecialTax : 'NULL';
	}

	public function setEntrataAvgSpecialTax( $fltEntrataAvgSpecialTax ) {
		$this->set( 'm_fltEntrataAvgSpecialTax', CStrings::strToFloatDef( $fltEntrataAvgSpecialTax, NULL, false, 2 ) );
	}

	public function getEntrataAvgSpecialTax() {
		return $this->m_fltEntrataAvgSpecialTax;
	}

	public function sqlEntrataAvgSpecialTax() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialTax ) ) ? ( string ) $this->m_fltEntrataAvgSpecialTax : 'NULL';
	}

	public function setEntrataMinAddOnTax( $fltEntrataMinAddOnTax ) {
		$this->set( 'm_fltEntrataMinAddOnTax', CStrings::strToFloatDef( $fltEntrataMinAddOnTax, NULL, false, 2 ) );
	}

	public function getEntrataMinAddOnTax() {
		return $this->m_fltEntrataMinAddOnTax;
	}

	public function sqlEntrataMinAddOnTax() {
		return ( true == isset( $this->m_fltEntrataMinAddOnTax ) ) ? ( string ) $this->m_fltEntrataMinAddOnTax : 'NULL';
	}

	public function setEntrataMaxAddOnTax( $fltEntrataMaxAddOnTax ) {
		$this->set( 'm_fltEntrataMaxAddOnTax', CStrings::strToFloatDef( $fltEntrataMaxAddOnTax, NULL, false, 2 ) );
	}

	public function getEntrataMaxAddOnTax() {
		return $this->m_fltEntrataMaxAddOnTax;
	}

	public function sqlEntrataMaxAddOnTax() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnTax ) ) ? ( string ) $this->m_fltEntrataMaxAddOnTax : 'NULL';
	}

	public function setEntrataAvgAddOnTax( $fltEntrataAvgAddOnTax ) {
		$this->set( 'm_fltEntrataAvgAddOnTax', CStrings::strToFloatDef( $fltEntrataAvgAddOnTax, NULL, false, 2 ) );
	}

	public function getEntrataAvgAddOnTax() {
		return $this->m_fltEntrataAvgAddOnTax;
	}

	public function sqlEntrataAvgAddOnTax() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnTax ) ) ? ( string ) $this->m_fltEntrataAvgAddOnTax : 'NULL';
	}

	public function setMinBaseAmountMonthly( $fltMinBaseAmountMonthly ) {
		$this->set( 'm_fltMinBaseAmountMonthly', CStrings::strToFloatDef( $fltMinBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getMinBaseAmountMonthly() {
		return $this->m_fltMinBaseAmountMonthly;
	}

	public function sqlMinBaseAmountMonthly() {
		return ( true == isset( $this->m_fltMinBaseAmountMonthly ) ) ? ( string ) $this->m_fltMinBaseAmountMonthly : 'NULL';
	}

	public function setMaxBaseAmountMonthly( $fltMaxBaseAmountMonthly ) {
		$this->set( 'm_fltMaxBaseAmountMonthly', CStrings::strToFloatDef( $fltMaxBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getMaxBaseAmountMonthly() {
		return $this->m_fltMaxBaseAmountMonthly;
	}

	public function sqlMaxBaseAmountMonthly() {
		return ( true == isset( $this->m_fltMaxBaseAmountMonthly ) ) ? ( string ) $this->m_fltMaxBaseAmountMonthly : 'NULL';
	}

	public function setAvgBaseAmountMonthly( $fltAvgBaseAmountMonthly ) {
		$this->set( 'm_fltAvgBaseAmountMonthly', CStrings::strToFloatDef( $fltAvgBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getAvgBaseAmountMonthly() {
		return $this->m_fltAvgBaseAmountMonthly;
	}

	public function sqlAvgBaseAmountMonthly() {
		return ( true == isset( $this->m_fltAvgBaseAmountMonthly ) ) ? ( string ) $this->m_fltAvgBaseAmountMonthly : 'NULL';
	}

	public function setMinBaseTaxMonthly( $fltMinBaseTaxMonthly ) {
		$this->set( 'm_fltMinBaseTaxMonthly', CStrings::strToFloatDef( $fltMinBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getMinBaseTaxMonthly() {
		return $this->m_fltMinBaseTaxMonthly;
	}

	public function sqlMinBaseTaxMonthly() {
		return ( true == isset( $this->m_fltMinBaseTaxMonthly ) ) ? ( string ) $this->m_fltMinBaseTaxMonthly : 'NULL';
	}

	public function setMaxBaseTaxMonthly( $fltMaxBaseTaxMonthly ) {
		$this->set( 'm_fltMaxBaseTaxMonthly', CStrings::strToFloatDef( $fltMaxBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getMaxBaseTaxMonthly() {
		return $this->m_fltMaxBaseTaxMonthly;
	}

	public function sqlMaxBaseTaxMonthly() {
		return ( true == isset( $this->m_fltMaxBaseTaxMonthly ) ) ? ( string ) $this->m_fltMaxBaseTaxMonthly : 'NULL';
	}

	public function setAvgBaseTaxMonthly( $fltAvgBaseTaxMonthly ) {
		$this->set( 'm_fltAvgBaseTaxMonthly', CStrings::strToFloatDef( $fltAvgBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getAvgBaseTaxMonthly() {
		return $this->m_fltAvgBaseTaxMonthly;
	}

	public function sqlAvgBaseTaxMonthly() {
		return ( true == isset( $this->m_fltAvgBaseTaxMonthly ) ) ? ( string ) $this->m_fltAvgBaseTaxMonthly : 'NULL';
	}

	public function setMinAmenityAmountMonthly( $fltMinAmenityAmountMonthly ) {
		$this->set( 'm_fltMinAmenityAmountMonthly', CStrings::strToFloatDef( $fltMinAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getMinAmenityAmountMonthly() {
		return $this->m_fltMinAmenityAmountMonthly;
	}

	public function sqlMinAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltMinAmenityAmountMonthly ) ) ? ( string ) $this->m_fltMinAmenityAmountMonthly : 'NULL';
	}

	public function setMaxAmenityAmountMonthly( $fltMaxAmenityAmountMonthly ) {
		$this->set( 'm_fltMaxAmenityAmountMonthly', CStrings::strToFloatDef( $fltMaxAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getMaxAmenityAmountMonthly() {
		return $this->m_fltMaxAmenityAmountMonthly;
	}

	public function sqlMaxAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltMaxAmenityAmountMonthly ) ) ? ( string ) $this->m_fltMaxAmenityAmountMonthly : 'NULL';
	}

	public function setAvgAmenityAmountMonthly( $fltAvgAmenityAmountMonthly ) {
		$this->set( 'm_fltAvgAmenityAmountMonthly', CStrings::strToFloatDef( $fltAvgAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getAvgAmenityAmountMonthly() {
		return $this->m_fltAvgAmenityAmountMonthly;
	}

	public function sqlAvgAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltAvgAmenityAmountMonthly ) ) ? ( string ) $this->m_fltAvgAmenityAmountMonthly : 'NULL';
	}

	public function setMinAmenityTaxMonthly( $fltMinAmenityTaxMonthly ) {
		$this->set( 'm_fltMinAmenityTaxMonthly', CStrings::strToFloatDef( $fltMinAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getMinAmenityTaxMonthly() {
		return $this->m_fltMinAmenityTaxMonthly;
	}

	public function sqlMinAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltMinAmenityTaxMonthly ) ) ? ( string ) $this->m_fltMinAmenityTaxMonthly : 'NULL';
	}

	public function setMaxAmenityTaxMonthly( $fltMaxAmenityTaxMonthly ) {
		$this->set( 'm_fltMaxAmenityTaxMonthly', CStrings::strToFloatDef( $fltMaxAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getMaxAmenityTaxMonthly() {
		return $this->m_fltMaxAmenityTaxMonthly;
	}

	public function sqlMaxAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltMaxAmenityTaxMonthly ) ) ? ( string ) $this->m_fltMaxAmenityTaxMonthly : 'NULL';
	}

	public function setAvgAmenityTaxMonthly( $fltAvgAmenityTaxMonthly ) {
		$this->set( 'm_fltAvgAmenityTaxMonthly', CStrings::strToFloatDef( $fltAvgAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getAvgAmenityTaxMonthly() {
		return $this->m_fltAvgAmenityTaxMonthly;
	}

	public function sqlAvgAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltAvgAmenityTaxMonthly ) ) ? ( string ) $this->m_fltAvgAmenityTaxMonthly : 'NULL';
	}

	public function setMinSpecialAmountMonthly( $fltMinSpecialAmountMonthly ) {
		$this->set( 'm_fltMinSpecialAmountMonthly', CStrings::strToFloatDef( $fltMinSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getMinSpecialAmountMonthly() {
		return $this->m_fltMinSpecialAmountMonthly;
	}

	public function sqlMinSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltMinSpecialAmountMonthly ) ) ? ( string ) $this->m_fltMinSpecialAmountMonthly : 'NULL';
	}

	public function setMaxSpecialAmountMonthly( $fltMaxSpecialAmountMonthly ) {
		$this->set( 'm_fltMaxSpecialAmountMonthly', CStrings::strToFloatDef( $fltMaxSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getMaxSpecialAmountMonthly() {
		return $this->m_fltMaxSpecialAmountMonthly;
	}

	public function sqlMaxSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltMaxSpecialAmountMonthly ) ) ? ( string ) $this->m_fltMaxSpecialAmountMonthly : 'NULL';
	}

	public function setAvgSpecialAmountMonthly( $fltAvgSpecialAmountMonthly ) {
		$this->set( 'm_fltAvgSpecialAmountMonthly', CStrings::strToFloatDef( $fltAvgSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getAvgSpecialAmountMonthly() {
		return $this->m_fltAvgSpecialAmountMonthly;
	}

	public function sqlAvgSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltAvgSpecialAmountMonthly ) ) ? ( string ) $this->m_fltAvgSpecialAmountMonthly : 'NULL';
	}

	public function setMinSpecialTaxMonthly( $fltMinSpecialTaxMonthly ) {
		$this->set( 'm_fltMinSpecialTaxMonthly', CStrings::strToFloatDef( $fltMinSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getMinSpecialTaxMonthly() {
		return $this->m_fltMinSpecialTaxMonthly;
	}

	public function sqlMinSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltMinSpecialTaxMonthly ) ) ? ( string ) $this->m_fltMinSpecialTaxMonthly : 'NULL';
	}

	public function setMaxSpecialTaxMonthly( $fltMaxSpecialTaxMonthly ) {
		$this->set( 'm_fltMaxSpecialTaxMonthly', CStrings::strToFloatDef( $fltMaxSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getMaxSpecialTaxMonthly() {
		return $this->m_fltMaxSpecialTaxMonthly;
	}

	public function sqlMaxSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltMaxSpecialTaxMonthly ) ) ? ( string ) $this->m_fltMaxSpecialTaxMonthly : 'NULL';
	}

	public function setAvgSpecialTaxMonthly( $fltAvgSpecialTaxMonthly ) {
		$this->set( 'm_fltAvgSpecialTaxMonthly', CStrings::strToFloatDef( $fltAvgSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getAvgSpecialTaxMonthly() {
		return $this->m_fltAvgSpecialTaxMonthly;
	}

	public function sqlAvgSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltAvgSpecialTaxMonthly ) ) ? ( string ) $this->m_fltAvgSpecialTaxMonthly : 'NULL';
	}

	public function setMinAddOnAmountMonthly( $fltMinAddOnAmountMonthly ) {
		$this->set( 'm_fltMinAddOnAmountMonthly', CStrings::strToFloatDef( $fltMinAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getMinAddOnAmountMonthly() {
		return $this->m_fltMinAddOnAmountMonthly;
	}

	public function sqlMinAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltMinAddOnAmountMonthly ) ) ? ( string ) $this->m_fltMinAddOnAmountMonthly : 'NULL';
	}

	public function setMaxAddOnAmountMonthly( $fltMaxAddOnAmountMonthly ) {
		$this->set( 'm_fltMaxAddOnAmountMonthly', CStrings::strToFloatDef( $fltMaxAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getMaxAddOnAmountMonthly() {
		return $this->m_fltMaxAddOnAmountMonthly;
	}

	public function sqlMaxAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltMaxAddOnAmountMonthly ) ) ? ( string ) $this->m_fltMaxAddOnAmountMonthly : 'NULL';
	}

	public function setAvgAddOnAmountMonthly( $fltAvgAddOnAmountMonthly ) {
		$this->set( 'm_fltAvgAddOnAmountMonthly', CStrings::strToFloatDef( $fltAvgAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getAvgAddOnAmountMonthly() {
		return $this->m_fltAvgAddOnAmountMonthly;
	}

	public function sqlAvgAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltAvgAddOnAmountMonthly ) ) ? ( string ) $this->m_fltAvgAddOnAmountMonthly : 'NULL';
	}

	public function setMinAddOnTaxMonthly( $fltMinAddOnTaxMonthly ) {
		$this->set( 'm_fltMinAddOnTaxMonthly', CStrings::strToFloatDef( $fltMinAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getMinAddOnTaxMonthly() {
		return $this->m_fltMinAddOnTaxMonthly;
	}

	public function sqlMinAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltMinAddOnTaxMonthly ) ) ? ( string ) $this->m_fltMinAddOnTaxMonthly : 'NULL';
	}

	public function setMaxAddOnTaxMonthly( $fltMaxAddOnTaxMonthly ) {
		$this->set( 'm_fltMaxAddOnTaxMonthly', CStrings::strToFloatDef( $fltMaxAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getMaxAddOnTaxMonthly() {
		return $this->m_fltMaxAddOnTaxMonthly;
	}

	public function sqlMaxAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltMaxAddOnTaxMonthly ) ) ? ( string ) $this->m_fltMaxAddOnTaxMonthly : 'NULL';
	}

	public function setAvgAddOnTaxMonthly( $fltAvgAddOnTaxMonthly ) {
		$this->set( 'm_fltAvgAddOnTaxMonthly', CStrings::strToFloatDef( $fltAvgAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getAvgAddOnTaxMonthly() {
		return $this->m_fltAvgAddOnTaxMonthly;
	}

	public function sqlAvgAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltAvgAddOnTaxMonthly ) ) ? ( string ) $this->m_fltAvgAddOnTaxMonthly : 'NULL';
	}

	public function setEntrataMinBaseAmountMonthly( $fltEntrataMinBaseAmountMonthly ) {
		$this->set( 'm_fltEntrataMinBaseAmountMonthly', CStrings::strToFloatDef( $fltEntrataMinBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinBaseAmountMonthly() {
		return $this->m_fltEntrataMinBaseAmountMonthly;
	}

	public function sqlEntrataMinBaseAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMinBaseAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMinBaseAmountMonthly : 'NULL';
	}

	public function setEntrataMaxBaseAmountMonthly( $fltEntrataMaxBaseAmountMonthly ) {
		$this->set( 'm_fltEntrataMaxBaseAmountMonthly', CStrings::strToFloatDef( $fltEntrataMaxBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxBaseAmountMonthly() {
		return $this->m_fltEntrataMaxBaseAmountMonthly;
	}

	public function sqlEntrataMaxBaseAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxBaseAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMaxBaseAmountMonthly : 'NULL';
	}

	public function setEntrataAvgBaseAmountMonthly( $fltEntrataAvgBaseAmountMonthly ) {
		$this->set( 'm_fltEntrataAvgBaseAmountMonthly', CStrings::strToFloatDef( $fltEntrataAvgBaseAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgBaseAmountMonthly() {
		return $this->m_fltEntrataAvgBaseAmountMonthly;
	}

	public function sqlEntrataAvgBaseAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgBaseAmountMonthly ) ) ? ( string ) $this->m_fltEntrataAvgBaseAmountMonthly : 'NULL';
	}

	public function setEntrataMinBaseTaxMonthly( $fltEntrataMinBaseTaxMonthly ) {
		$this->set( 'm_fltEntrataMinBaseTaxMonthly', CStrings::strToFloatDef( $fltEntrataMinBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinBaseTaxMonthly() {
		return $this->m_fltEntrataMinBaseTaxMonthly;
	}

	public function sqlEntrataMinBaseTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMinBaseTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMinBaseTaxMonthly : 'NULL';
	}

	public function setEntrataMaxBaseTaxMonthly( $fltEntrataMaxBaseTaxMonthly ) {
		$this->set( 'm_fltEntrataMaxBaseTaxMonthly', CStrings::strToFloatDef( $fltEntrataMaxBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxBaseTaxMonthly() {
		return $this->m_fltEntrataMaxBaseTaxMonthly;
	}

	public function sqlEntrataMaxBaseTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxBaseTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMaxBaseTaxMonthly : 'NULL';
	}

	public function setEntrataAvgBaseTaxMonthly( $fltEntrataAvgBaseTaxMonthly ) {
		$this->set( 'm_fltEntrataAvgBaseTaxMonthly', CStrings::strToFloatDef( $fltEntrataAvgBaseTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgBaseTaxMonthly() {
		return $this->m_fltEntrataAvgBaseTaxMonthly;
	}

	public function sqlEntrataAvgBaseTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgBaseTaxMonthly ) ) ? ( string ) $this->m_fltEntrataAvgBaseTaxMonthly : 'NULL';
	}

	public function setEntrataMinAmenityAmountMonthly( $fltEntrataMinAmenityAmountMonthly ) {
		$this->set( 'm_fltEntrataMinAmenityAmountMonthly', CStrings::strToFloatDef( $fltEntrataMinAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAmenityAmountMonthly() {
		return $this->m_fltEntrataMinAmenityAmountMonthly;
	}

	public function sqlEntrataMinAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAmenityAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMinAmenityAmountMonthly : 'NULL';
	}

	public function setEntrataMaxAmenityAmountMonthly( $fltEntrataMaxAmenityAmountMonthly ) {
		$this->set( 'm_fltEntrataMaxAmenityAmountMonthly', CStrings::strToFloatDef( $fltEntrataMaxAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAmenityAmountMonthly() {
		return $this->m_fltEntrataMaxAmenityAmountMonthly;
	}

	public function sqlEntrataMaxAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAmenityAmountMonthly : 'NULL';
	}

	public function setEntrataAvgAmenityAmountMonthly( $fltEntrataAvgAmenityAmountMonthly ) {
		$this->set( 'm_fltEntrataAvgAmenityAmountMonthly', CStrings::strToFloatDef( $fltEntrataAvgAmenityAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAmenityAmountMonthly() {
		return $this->m_fltEntrataAvgAmenityAmountMonthly;
	}

	public function sqlEntrataAvgAmenityAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityAmountMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAmenityAmountMonthly : 'NULL';
	}

	public function setEntrataMinAmenityTaxMonthly( $fltEntrataMinAmenityTaxMonthly ) {
		$this->set( 'm_fltEntrataMinAmenityTaxMonthly', CStrings::strToFloatDef( $fltEntrataMinAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAmenityTaxMonthly() {
		return $this->m_fltEntrataMinAmenityTaxMonthly;
	}

	public function sqlEntrataMinAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAmenityTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMinAmenityTaxMonthly : 'NULL';
	}

	public function setEntrataMaxAmenityTaxMonthly( $fltEntrataMaxAmenityTaxMonthly ) {
		$this->set( 'm_fltEntrataMaxAmenityTaxMonthly', CStrings::strToFloatDef( $fltEntrataMaxAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAmenityTaxMonthly() {
		return $this->m_fltEntrataMaxAmenityTaxMonthly;
	}

	public function sqlEntrataMaxAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAmenityTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAmenityTaxMonthly : 'NULL';
	}

	public function setEntrataAvgAmenityTaxMonthly( $fltEntrataAvgAmenityTaxMonthly ) {
		$this->set( 'm_fltEntrataAvgAmenityTaxMonthly', CStrings::strToFloatDef( $fltEntrataAvgAmenityTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAmenityTaxMonthly() {
		return $this->m_fltEntrataAvgAmenityTaxMonthly;
	}

	public function sqlEntrataAvgAmenityTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAmenityTaxMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAmenityTaxMonthly : 'NULL';
	}

	public function setEntrataMinSpecialAmountMonthly( $fltEntrataMinSpecialAmountMonthly ) {
		$this->set( 'm_fltEntrataMinSpecialAmountMonthly', CStrings::strToFloatDef( $fltEntrataMinSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinSpecialAmountMonthly() {
		return $this->m_fltEntrataMinSpecialAmountMonthly;
	}

	public function sqlEntrataMinSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMinSpecialAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMinSpecialAmountMonthly : 'NULL';
	}

	public function setEntrataMaxSpecialAmountMonthly( $fltEntrataMaxSpecialAmountMonthly ) {
		$this->set( 'm_fltEntrataMaxSpecialAmountMonthly', CStrings::strToFloatDef( $fltEntrataMaxSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxSpecialAmountMonthly() {
		return $this->m_fltEntrataMaxSpecialAmountMonthly;
	}

	public function sqlEntrataMaxSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMaxSpecialAmountMonthly : 'NULL';
	}

	public function setEntrataAvgSpecialAmountMonthly( $fltEntrataAvgSpecialAmountMonthly ) {
		$this->set( 'm_fltEntrataAvgSpecialAmountMonthly', CStrings::strToFloatDef( $fltEntrataAvgSpecialAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgSpecialAmountMonthly() {
		return $this->m_fltEntrataAvgSpecialAmountMonthly;
	}

	public function sqlEntrataAvgSpecialAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialAmountMonthly ) ) ? ( string ) $this->m_fltEntrataAvgSpecialAmountMonthly : 'NULL';
	}

	public function setEntrataMinSpecialTaxMonthly( $fltEntrataMinSpecialTaxMonthly ) {
		$this->set( 'm_fltEntrataMinSpecialTaxMonthly', CStrings::strToFloatDef( $fltEntrataMinSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinSpecialTaxMonthly() {
		return $this->m_fltEntrataMinSpecialTaxMonthly;
	}

	public function sqlEntrataMinSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMinSpecialTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMinSpecialTaxMonthly : 'NULL';
	}

	public function setEntrataMaxSpecialTaxMonthly( $fltEntrataMaxSpecialTaxMonthly ) {
		$this->set( 'm_fltEntrataMaxSpecialTaxMonthly', CStrings::strToFloatDef( $fltEntrataMaxSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxSpecialTaxMonthly() {
		return $this->m_fltEntrataMaxSpecialTaxMonthly;
	}

	public function sqlEntrataMaxSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxSpecialTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMaxSpecialTaxMonthly : 'NULL';
	}

	public function setEntrataAvgSpecialTaxMonthly( $fltEntrataAvgSpecialTaxMonthly ) {
		$this->set( 'm_fltEntrataAvgSpecialTaxMonthly', CStrings::strToFloatDef( $fltEntrataAvgSpecialTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgSpecialTaxMonthly() {
		return $this->m_fltEntrataAvgSpecialTaxMonthly;
	}

	public function sqlEntrataAvgSpecialTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgSpecialTaxMonthly ) ) ? ( string ) $this->m_fltEntrataAvgSpecialTaxMonthly : 'NULL';
	}

	public function setEntrataMinAddOnAmountMonthly( $fltEntrataMinAddOnAmountMonthly ) {
		$this->set( 'm_fltEntrataMinAddOnAmountMonthly', CStrings::strToFloatDef( $fltEntrataMinAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAddOnAmountMonthly() {
		return $this->m_fltEntrataMinAddOnAmountMonthly;
	}

	public function sqlEntrataMinAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAddOnAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMinAddOnAmountMonthly : 'NULL';
	}

	public function setEntrataMaxAddOnAmountMonthly( $fltEntrataMaxAddOnAmountMonthly ) {
		$this->set( 'm_fltEntrataMaxAddOnAmountMonthly', CStrings::strToFloatDef( $fltEntrataMaxAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAddOnAmountMonthly() {
		return $this->m_fltEntrataMaxAddOnAmountMonthly;
	}

	public function sqlEntrataMaxAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnAmountMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAddOnAmountMonthly : 'NULL';
	}

	public function setEntrataAvgAddOnAmountMonthly( $fltEntrataAvgAddOnAmountMonthly ) {
		$this->set( 'm_fltEntrataAvgAddOnAmountMonthly', CStrings::strToFloatDef( $fltEntrataAvgAddOnAmountMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAddOnAmountMonthly() {
		return $this->m_fltEntrataAvgAddOnAmountMonthly;
	}

	public function sqlEntrataAvgAddOnAmountMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnAmountMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAddOnAmountMonthly : 'NULL';
	}

	public function setEntrataMinAddOnTaxMonthly( $fltEntrataMinAddOnTaxMonthly ) {
		$this->set( 'm_fltEntrataMinAddOnTaxMonthly', CStrings::strToFloatDef( $fltEntrataMinAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMinAddOnTaxMonthly() {
		return $this->m_fltEntrataMinAddOnTaxMonthly;
	}

	public function sqlEntrataMinAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMinAddOnTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMinAddOnTaxMonthly : 'NULL';
	}

	public function setEntrataMaxAddOnTaxMonthly( $fltEntrataMaxAddOnTaxMonthly ) {
		$this->set( 'm_fltEntrataMaxAddOnTaxMonthly', CStrings::strToFloatDef( $fltEntrataMaxAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataMaxAddOnTaxMonthly() {
		return $this->m_fltEntrataMaxAddOnTaxMonthly;
	}

	public function sqlEntrataMaxAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataMaxAddOnTaxMonthly ) ) ? ( string ) $this->m_fltEntrataMaxAddOnTaxMonthly : 'NULL';
	}

	public function setEntrataAvgAddOnTaxMonthly( $fltEntrataAvgAddOnTaxMonthly ) {
		$this->set( 'm_fltEntrataAvgAddOnTaxMonthly', CStrings::strToFloatDef( $fltEntrataAvgAddOnTaxMonthly, NULL, false, 2 ) );
	}

	public function getEntrataAvgAddOnTaxMonthly() {
		return $this->m_fltEntrataAvgAddOnTaxMonthly;
	}

	public function sqlEntrataAvgAddOnTaxMonthly() {
		return ( true == isset( $this->m_fltEntrataAvgAddOnTaxMonthly ) ) ? ( string ) $this->m_fltEntrataAvgAddOnTaxMonthly : 'NULL';
	}

	public function setEffectiveRange( $strEffectiveRange ) {
		$this->set( 'm_strEffectiveRange', CStrings::strTrimDef( $strEffectiveRange, NULL, NULL, true ) );
	}

	public function getEffectiveRange() {
		return $this->m_strEffectiveRange;
	}

	public function sqlEffectiveRange() {
		return ( true == isset( $this->m_strEffectiveRange ) ) ? '\'' . addslashes( $this->m_strEffectiveRange ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, occupancy_type_id, space_configuration_id, lease_term_id, total_unit_count, rentable_unit_count, available_unit_count, min_square_feet, max_square_feet, avg_square_feet, min_base_rent, max_base_rent, avg_base_rent, min_amenity_rent, max_amenity_rent, avg_amenity_rent, min_special_rent, max_special_rent, avg_special_rent, min_add_on_rent, max_add_on_rent, avg_add_on_rent, min_market_rent, max_market_rent, avg_market_rent, manual_min_market_rent, manual_max_market_rent, min_base_deposit, max_base_deposit, avg_base_deposit, min_amenity_deposit, max_amenity_deposit, avg_amenity_deposit, min_special_deposit, max_special_deposit, avg_special_deposit, min_add_on_deposit, max_add_on_deposit, avg_add_on_deposit, min_total_deposit, max_total_deposit, avg_total_deposit, manual_min_total_deposit, manual_max_total_deposit, entrata_min_base_rent, entrata_max_base_rent, entrata_avg_base_rent, entrata_min_amenity_rent, entrata_max_amenity_rent, entrata_avg_amenity_rent, entrata_min_special_rent, entrata_max_special_rent, entrata_avg_special_rent, entrata_min_add_on_rent, entrata_max_add_on_rent, entrata_avg_add_on_rent, entrata_min_market_rent, entrata_max_market_rent, entrata_avg_market_rent, entrata_min_base_deposit, entrata_max_base_deposit, entrata_avg_base_deposit, entrata_min_amenity_deposit, entrata_max_amenity_deposit, entrata_avg_amenity_deposit, entrata_min_special_deposit, entrata_max_special_deposit, entrata_avg_special_deposit, entrata_min_add_on_deposit, entrata_max_add_on_deposit, entrata_avg_add_on_deposit, entrata_min_total_deposit, entrata_max_total_deposit, entrata_avg_total_deposit, unit_space_ids, rentable_unit_space_ids, available_unit_space_ids, is_manual_rent_range, is_manual_deposit_range, effective_date, created_by, created_on, ar_trigger_id, min_base_rent_monthly, max_base_rent_monthly, avg_base_rent_monthly, min_amenity_rent_monthly, max_amenity_rent_monthly, avg_amenity_rent_monthly, min_special_rent_monthly, max_special_rent_monthly, avg_special_rent_monthly, min_add_on_rent_monthly, max_add_on_rent_monthly, avg_add_on_rent_monthly, min_market_rent_monthly, max_market_rent_monthly, avg_market_rent_monthly, entrata_min_base_rent_monthly, entrata_max_base_rent_monthly, entrata_avg_base_rent_monthly, entrata_min_amenity_rent_monthly, entrata_max_amenity_rent_monthly, entrata_avg_amenity_rent_monthly, entrata_min_special_rent_monthly, entrata_max_special_rent_monthly, entrata_avg_special_rent_monthly, entrata_min_add_on_rent_monthly, entrata_max_add_on_rent_monthly, entrata_avg_add_on_rent_monthly, entrata_min_market_rent_monthly, entrata_max_market_rent_monthly, entrata_avg_market_rent_monthly, include_tax_in_advertised_rent, include_other_in_advertised_rent, min_base_amount, max_base_amount, avg_base_amount, min_amenity_amount, max_amenity_amount, avg_amenity_amount, min_special_amount, max_special_amount, avg_special_amount, min_add_on_amount, max_add_on_amount, avg_add_on_amount, entrata_min_base_amount, entrata_max_base_amount, entrata_avg_base_amount, entrata_min_amenity_amount, entrata_max_amenity_amount, entrata_avg_amenity_amount, entrata_min_special_amount, entrata_max_special_amount, entrata_avg_special_amount, entrata_min_add_on_amount, entrata_max_add_on_amount, entrata_avg_add_on_amount, min_base_tax, max_base_tax, avg_base_tax, min_amenity_tax, max_amenity_tax, avg_amenity_tax, min_special_tax, max_special_tax, avg_special_tax, min_add_on_tax, max_add_on_tax, avg_add_on_tax, entrata_min_base_tax, entrata_max_base_tax, entrata_avg_base_tax, entrata_min_amenity_tax, entrata_max_amenity_tax, entrata_avg_amenity_tax, entrata_min_special_tax, entrata_max_special_tax, entrata_avg_special_tax, entrata_min_add_on_tax, entrata_max_add_on_tax, entrata_avg_add_on_tax, min_base_amount_monthly, max_base_amount_monthly, avg_base_amount_monthly, min_base_tax_monthly, max_base_tax_monthly, avg_base_tax_monthly, min_amenity_amount_monthly, max_amenity_amount_monthly, avg_amenity_amount_monthly, min_amenity_tax_monthly, max_amenity_tax_monthly, avg_amenity_tax_monthly, min_special_amount_monthly, max_special_amount_monthly, avg_special_amount_monthly, min_special_tax_monthly, max_special_tax_monthly, avg_special_tax_monthly, min_add_on_amount_monthly, max_add_on_amount_monthly, avg_add_on_amount_monthly, min_add_on_tax_monthly, max_add_on_tax_monthly, avg_add_on_tax_monthly, entrata_min_base_amount_monthly, entrata_max_base_amount_monthly, entrata_avg_base_amount_monthly, entrata_min_base_tax_monthly, entrata_max_base_tax_monthly, entrata_avg_base_tax_monthly, entrata_min_amenity_amount_monthly, entrata_max_amenity_amount_monthly, entrata_avg_amenity_amount_monthly, entrata_min_amenity_tax_monthly, entrata_max_amenity_tax_monthly, entrata_avg_amenity_tax_monthly, entrata_min_special_amount_monthly, entrata_max_special_amount_monthly, entrata_avg_special_amount_monthly, entrata_min_special_tax_monthly, entrata_max_special_tax_monthly, entrata_avg_special_tax_monthly, entrata_min_add_on_amount_monthly, entrata_max_add_on_amount_monthly, entrata_avg_add_on_amount_monthly, entrata_min_add_on_tax_monthly, entrata_max_add_on_tax_monthly, entrata_avg_add_on_tax_monthly, effective_range )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlSpaceConfigurationId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlTotalUnitCount() . ', ' .
						$this->sqlRentableUnitCount() . ', ' .
						$this->sqlAvailableUnitCount() . ', ' .
						$this->sqlMinSquareFeet() . ', ' .
						$this->sqlMaxSquareFeet() . ', ' .
						$this->sqlAvgSquareFeet() . ', ' .
						$this->sqlMinBaseRent() . ', ' .
						$this->sqlMaxBaseRent() . ', ' .
						$this->sqlAvgBaseRent() . ', ' .
						$this->sqlMinAmenityRent() . ', ' .
						$this->sqlMaxAmenityRent() . ', ' .
						$this->sqlAvgAmenityRent() . ', ' .
						$this->sqlMinSpecialRent() . ', ' .
						$this->sqlMaxSpecialRent() . ', ' .
						$this->sqlAvgSpecialRent() . ', ' .
						$this->sqlMinAddOnRent() . ', ' .
						$this->sqlMaxAddOnRent() . ', ' .
						$this->sqlAvgAddOnRent() . ', ' .
						$this->sqlMinMarketRent() . ', ' .
						$this->sqlMaxMarketRent() . ', ' .
						$this->sqlAvgMarketRent() . ', ' .
						$this->sqlManualMinMarketRent() . ', ' .
						$this->sqlManualMaxMarketRent() . ', ' .
						$this->sqlMinBaseDeposit() . ', ' .
						$this->sqlMaxBaseDeposit() . ', ' .
						$this->sqlAvgBaseDeposit() . ', ' .
						$this->sqlMinAmenityDeposit() . ', ' .
						$this->sqlMaxAmenityDeposit() . ', ' .
						$this->sqlAvgAmenityDeposit() . ', ' .
						$this->sqlMinSpecialDeposit() . ', ' .
						$this->sqlMaxSpecialDeposit() . ', ' .
						$this->sqlAvgSpecialDeposit() . ', ' .
						$this->sqlMinAddOnDeposit() . ', ' .
						$this->sqlMaxAddOnDeposit() . ', ' .
						$this->sqlAvgAddOnDeposit() . ', ' .
						$this->sqlMinTotalDeposit() . ', ' .
						$this->sqlMaxTotalDeposit() . ', ' .
						$this->sqlAvgTotalDeposit() . ', ' .
						$this->sqlManualMinTotalDeposit() . ', ' .
						$this->sqlManualMaxTotalDeposit() . ', ' .
						$this->sqlEntrataMinBaseRent() . ', ' .
						$this->sqlEntrataMaxBaseRent() . ', ' .
						$this->sqlEntrataAvgBaseRent() . ', ' .
						$this->sqlEntrataMinAmenityRent() . ', ' .
						$this->sqlEntrataMaxAmenityRent() . ', ' .
						$this->sqlEntrataAvgAmenityRent() . ', ' .
						$this->sqlEntrataMinSpecialRent() . ', ' .
						$this->sqlEntrataMaxSpecialRent() . ', ' .
						$this->sqlEntrataAvgSpecialRent() . ', ' .
						$this->sqlEntrataMinAddOnRent() . ', ' .
						$this->sqlEntrataMaxAddOnRent() . ', ' .
						$this->sqlEntrataAvgAddOnRent() . ', ' .
						$this->sqlEntrataMinMarketRent() . ', ' .
						$this->sqlEntrataMaxMarketRent() . ', ' .
						$this->sqlEntrataAvgMarketRent() . ', ' .
						$this->sqlEntrataMinBaseDeposit() . ', ' .
						$this->sqlEntrataMaxBaseDeposit() . ', ' .
						$this->sqlEntrataAvgBaseDeposit() . ', ' .
						$this->sqlEntrataMinAmenityDeposit() . ', ' .
						$this->sqlEntrataMaxAmenityDeposit() . ', ' .
						$this->sqlEntrataAvgAmenityDeposit() . ', ' .
						$this->sqlEntrataMinSpecialDeposit() . ', ' .
						$this->sqlEntrataMaxSpecialDeposit() . ', ' .
						$this->sqlEntrataAvgSpecialDeposit() . ', ' .
						$this->sqlEntrataMinAddOnDeposit() . ', ' .
						$this->sqlEntrataMaxAddOnDeposit() . ', ' .
						$this->sqlEntrataAvgAddOnDeposit() . ', ' .
						$this->sqlEntrataMinTotalDeposit() . ', ' .
						$this->sqlEntrataMaxTotalDeposit() . ', ' .
						$this->sqlEntrataAvgTotalDeposit() . ', ' .
						$this->sqlUnitSpaceIds() . ', ' .
						$this->sqlRentableUnitSpaceIds() . ', ' .
						$this->sqlAvailableUnitSpaceIds() . ', ' .
						$this->sqlIsManualRentRange() . ', ' .
						$this->sqlIsManualDepositRange() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlArTriggerId() . ', ' .
						$this->sqlMinBaseRentMonthly() . ', ' .
						$this->sqlMaxBaseRentMonthly() . ', ' .
						$this->sqlAvgBaseRentMonthly() . ', ' .
						$this->sqlMinAmenityRentMonthly() . ', ' .
						$this->sqlMaxAmenityRentMonthly() . ', ' .
						$this->sqlAvgAmenityRentMonthly() . ', ' .
						$this->sqlMinSpecialRentMonthly() . ', ' .
						$this->sqlMaxSpecialRentMonthly() . ', ' .
						$this->sqlAvgSpecialRentMonthly() . ', ' .
						$this->sqlMinAddOnRentMonthly() . ', ' .
						$this->sqlMaxAddOnRentMonthly() . ', ' .
						$this->sqlAvgAddOnRentMonthly() . ', ' .
						$this->sqlMinMarketRentMonthly() . ', ' .
						$this->sqlMaxMarketRentMonthly() . ', ' .
						$this->sqlAvgMarketRentMonthly() . ', ' .
						$this->sqlEntrataMinBaseRentMonthly() . ', ' .
						$this->sqlEntrataMaxBaseRentMonthly() . ', ' .
						$this->sqlEntrataAvgBaseRentMonthly() . ', ' .
						$this->sqlEntrataMinAmenityRentMonthly() . ', ' .
						$this->sqlEntrataMaxAmenityRentMonthly() . ', ' .
						$this->sqlEntrataAvgAmenityRentMonthly() . ', ' .
						$this->sqlEntrataMinSpecialRentMonthly() . ', ' .
						$this->sqlEntrataMaxSpecialRentMonthly() . ', ' .
						$this->sqlEntrataAvgSpecialRentMonthly() . ', ' .
						$this->sqlEntrataMinAddOnRentMonthly() . ', ' .
						$this->sqlEntrataMaxAddOnRentMonthly() . ', ' .
						$this->sqlEntrataAvgAddOnRentMonthly() . ', ' .
						$this->sqlEntrataMinMarketRentMonthly() . ', ' .
						$this->sqlEntrataMaxMarketRentMonthly() . ', ' .
						$this->sqlEntrataAvgMarketRentMonthly() . ', ' .
						$this->sqlIncludeTaxInAdvertisedRent() . ', ' .
						$this->sqlIncludeOtherInAdvertisedRent() . ', ' .
						$this->sqlMinBaseAmount() . ', ' .
						$this->sqlMaxBaseAmount() . ', ' .
						$this->sqlAvgBaseAmount() . ', ' .
						$this->sqlMinAmenityAmount() . ', ' .
						$this->sqlMaxAmenityAmount() . ', ' .
						$this->sqlAvgAmenityAmount() . ', ' .
						$this->sqlMinSpecialAmount() . ', ' .
						$this->sqlMaxSpecialAmount() . ', ' .
						$this->sqlAvgSpecialAmount() . ', ' .
						$this->sqlMinAddOnAmount() . ', ' .
						$this->sqlMaxAddOnAmount() . ', ' .
						$this->sqlAvgAddOnAmount() . ', ' .
						$this->sqlEntrataMinBaseAmount() . ', ' .
						$this->sqlEntrataMaxBaseAmount() . ', ' .
						$this->sqlEntrataAvgBaseAmount() . ', ' .
						$this->sqlEntrataMinAmenityAmount() . ', ' .
						$this->sqlEntrataMaxAmenityAmount() . ', ' .
						$this->sqlEntrataAvgAmenityAmount() . ', ' .
						$this->sqlEntrataMinSpecialAmount() . ', ' .
						$this->sqlEntrataMaxSpecialAmount() . ', ' .
						$this->sqlEntrataAvgSpecialAmount() . ', ' .
						$this->sqlEntrataMinAddOnAmount() . ', ' .
						$this->sqlEntrataMaxAddOnAmount() . ', ' .
						$this->sqlEntrataAvgAddOnAmount() . ', ' .
						$this->sqlMinBaseTax() . ', ' .
						$this->sqlMaxBaseTax() . ', ' .
						$this->sqlAvgBaseTax() . ', ' .
						$this->sqlMinAmenityTax() . ', ' .
						$this->sqlMaxAmenityTax() . ', ' .
						$this->sqlAvgAmenityTax() . ', ' .
						$this->sqlMinSpecialTax() . ', ' .
						$this->sqlMaxSpecialTax() . ', ' .
						$this->sqlAvgSpecialTax() . ', ' .
						$this->sqlMinAddOnTax() . ', ' .
						$this->sqlMaxAddOnTax() . ', ' .
						$this->sqlAvgAddOnTax() . ', ' .
						$this->sqlEntrataMinBaseTax() . ', ' .
						$this->sqlEntrataMaxBaseTax() . ', ' .
						$this->sqlEntrataAvgBaseTax() . ', ' .
						$this->sqlEntrataMinAmenityTax() . ', ' .
						$this->sqlEntrataMaxAmenityTax() . ', ' .
						$this->sqlEntrataAvgAmenityTax() . ', ' .
						$this->sqlEntrataMinSpecialTax() . ', ' .
						$this->sqlEntrataMaxSpecialTax() . ', ' .
						$this->sqlEntrataAvgSpecialTax() . ', ' .
						$this->sqlEntrataMinAddOnTax() . ', ' .
						$this->sqlEntrataMaxAddOnTax() . ', ' .
						$this->sqlEntrataAvgAddOnTax() . ', ' .
						$this->sqlMinBaseAmountMonthly() . ', ' .
						$this->sqlMaxBaseAmountMonthly() . ', ' .
						$this->sqlAvgBaseAmountMonthly() . ', ' .
						$this->sqlMinBaseTaxMonthly() . ', ' .
						$this->sqlMaxBaseTaxMonthly() . ', ' .
						$this->sqlAvgBaseTaxMonthly() . ', ' .
						$this->sqlMinAmenityAmountMonthly() . ', ' .
						$this->sqlMaxAmenityAmountMonthly() . ', ' .
						$this->sqlAvgAmenityAmountMonthly() . ', ' .
						$this->sqlMinAmenityTaxMonthly() . ', ' .
						$this->sqlMaxAmenityTaxMonthly() . ', ' .
						$this->sqlAvgAmenityTaxMonthly() . ', ' .
						$this->sqlMinSpecialAmountMonthly() . ', ' .
						$this->sqlMaxSpecialAmountMonthly() . ', ' .
						$this->sqlAvgSpecialAmountMonthly() . ', ' .
						$this->sqlMinSpecialTaxMonthly() . ', ' .
						$this->sqlMaxSpecialTaxMonthly() . ', ' .
						$this->sqlAvgSpecialTaxMonthly() . ', ' .
						$this->sqlMinAddOnAmountMonthly() . ', ' .
						$this->sqlMaxAddOnAmountMonthly() . ', ' .
						$this->sqlAvgAddOnAmountMonthly() . ', ' .
						$this->sqlMinAddOnTaxMonthly() . ', ' .
						$this->sqlMaxAddOnTaxMonthly() . ', ' .
						$this->sqlAvgAddOnTaxMonthly() . ', ' .
						$this->sqlEntrataMinBaseAmountMonthly() . ', ' .
						$this->sqlEntrataMaxBaseAmountMonthly() . ', ' .
						$this->sqlEntrataAvgBaseAmountMonthly() . ', ' .
						$this->sqlEntrataMinBaseTaxMonthly() . ', ' .
						$this->sqlEntrataMaxBaseTaxMonthly() . ', ' .
						$this->sqlEntrataAvgBaseTaxMonthly() . ', ' .
						$this->sqlEntrataMinAmenityAmountMonthly() . ', ' .
						$this->sqlEntrataMaxAmenityAmountMonthly() . ', ' .
						$this->sqlEntrataAvgAmenityAmountMonthly() . ', ' .
						$this->sqlEntrataMinAmenityTaxMonthly() . ', ' .
						$this->sqlEntrataMaxAmenityTaxMonthly() . ', ' .
						$this->sqlEntrataAvgAmenityTaxMonthly() . ', ' .
						$this->sqlEntrataMinSpecialAmountMonthly() . ', ' .
						$this->sqlEntrataMaxSpecialAmountMonthly() . ', ' .
						$this->sqlEntrataAvgSpecialAmountMonthly() . ', ' .
						$this->sqlEntrataMinSpecialTaxMonthly() . ', ' .
						$this->sqlEntrataMaxSpecialTaxMonthly() . ', ' .
						$this->sqlEntrataAvgSpecialTaxMonthly() . ', ' .
						$this->sqlEntrataMinAddOnAmountMonthly() . ', ' .
						$this->sqlEntrataMaxAddOnAmountMonthly() . ', ' .
						$this->sqlEntrataAvgAddOnAmountMonthly() . ', ' .
						$this->sqlEntrataMinAddOnTaxMonthly() . ', ' .
						$this->sqlEntrataMaxAddOnTaxMonthly() . ', ' .
						$this->sqlEntrataAvgAddOnTaxMonthly() . ', ' .
						$this->sqlEffectiveRange() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId(). ',' ; } elseif( true == array_key_exists( 'SpaceConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' space_configuration_id = ' . $this->sqlSpaceConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_unit_count = ' . $this->sqlTotalUnitCount(). ',' ; } elseif( true == array_key_exists( 'TotalUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' total_unit_count = ' . $this->sqlTotalUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_unit_count = ' . $this->sqlRentableUnitCount(). ',' ; } elseif( true == array_key_exists( 'RentableUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' rentable_unit_count = ' . $this->sqlRentableUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_unit_count = ' . $this->sqlAvailableUnitCount(). ',' ; } elseif( true == array_key_exists( 'AvailableUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' available_unit_count = ' . $this->sqlAvailableUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_square_feet = ' . $this->sqlAvgSquareFeet(). ',' ; } elseif( true == array_key_exists( 'AvgSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' avg_square_feet = ' . $this->sqlAvgSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_rent = ' . $this->sqlMinBaseRent(). ',' ; } elseif( true == array_key_exists( 'MinBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' min_base_rent = ' . $this->sqlMinBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_rent = ' . $this->sqlMaxBaseRent(). ',' ; } elseif( true == array_key_exists( 'MaxBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' max_base_rent = ' . $this->sqlMaxBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_rent = ' . $this->sqlAvgBaseRent(). ',' ; } elseif( true == array_key_exists( 'AvgBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_rent = ' . $this->sqlAvgBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_rent = ' . $this->sqlMinAmenityRent(). ',' ; } elseif( true == array_key_exists( 'MinAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_rent = ' . $this->sqlMinAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_rent = ' . $this->sqlMaxAmenityRent(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_rent = ' . $this->sqlMaxAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_rent = ' . $this->sqlAvgAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_rent = ' . $this->sqlAvgAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_rent = ' . $this->sqlMinSpecialRent(). ',' ; } elseif( true == array_key_exists( 'MinSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' min_special_rent = ' . $this->sqlMinSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_rent = ' . $this->sqlMaxSpecialRent(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' max_special_rent = ' . $this->sqlMaxSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_rent = ' . $this->sqlAvgSpecialRent(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_rent = ' . $this->sqlAvgSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_rent = ' . $this->sqlMinAddOnRent(). ',' ; } elseif( true == array_key_exists( 'MinAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_rent = ' . $this->sqlMinAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_rent = ' . $this->sqlMaxAddOnRent(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_rent = ' . $this->sqlMaxAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_rent = ' . $this->sqlAvgAddOnRent(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_rent = ' . $this->sqlAvgAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_market_rent = ' . $this->sqlMinMarketRent(). ',' ; } elseif( true == array_key_exists( 'MinMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' min_market_rent = ' . $this->sqlMinMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_market_rent = ' . $this->sqlMaxMarketRent(). ',' ; } elseif( true == array_key_exists( 'MaxMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' max_market_rent = ' . $this->sqlMaxMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_market_rent = ' . $this->sqlAvgMarketRent(). ',' ; } elseif( true == array_key_exists( 'AvgMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_market_rent = ' . $this->sqlAvgMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manual_min_market_rent = ' . $this->sqlManualMinMarketRent(). ',' ; } elseif( true == array_key_exists( 'ManualMinMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' manual_min_market_rent = ' . $this->sqlManualMinMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manual_max_market_rent = ' . $this->sqlManualMaxMarketRent(). ',' ; } elseif( true == array_key_exists( 'ManualMaxMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' manual_max_market_rent = ' . $this->sqlManualMaxMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_deposit = ' . $this->sqlMinBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'MinBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_base_deposit = ' . $this->sqlMinBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_deposit = ' . $this->sqlMaxBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_base_deposit = ' . $this->sqlMaxBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_deposit = ' . $this->sqlAvgBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'AvgBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_deposit = ' . $this->sqlAvgBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_deposit = ' . $this->sqlMinAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'MinAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_deposit = ' . $this->sqlMinAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_deposit = ' . $this->sqlMaxAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_deposit = ' . $this->sqlMaxAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_deposit = ' . $this->sqlAvgAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_deposit = ' . $this->sqlAvgAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_deposit = ' . $this->sqlMinSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'MinSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_special_deposit = ' . $this->sqlMinSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_deposit = ' . $this->sqlMaxSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_special_deposit = ' . $this->sqlMaxSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_deposit = ' . $this->sqlAvgSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_deposit = ' . $this->sqlAvgSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_deposit = ' . $this->sqlMinAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'MinAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_deposit = ' . $this->sqlMinAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_deposit = ' . $this->sqlMaxAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_deposit = ' . $this->sqlMaxAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_deposit = ' . $this->sqlAvgAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_deposit = ' . $this->sqlAvgAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_total_deposit = ' . $this->sqlMinTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'MinTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_total_deposit = ' . $this->sqlMinTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_total_deposit = ' . $this->sqlMaxTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'MaxTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_total_deposit = ' . $this->sqlMaxTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_total_deposit = ' . $this->sqlAvgTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'AvgTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' avg_total_deposit = ' . $this->sqlAvgTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manual_min_total_deposit = ' . $this->sqlManualMinTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'ManualMinTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' manual_min_total_deposit = ' . $this->sqlManualMinTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manual_max_total_deposit = ' . $this->sqlManualMaxTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'ManualMaxTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' manual_max_total_deposit = ' . $this->sqlManualMaxTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_rent = ' . $this->sqlEntrataMinBaseRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_rent = ' . $this->sqlEntrataMinBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_rent = ' . $this->sqlEntrataMaxBaseRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_rent = ' . $this->sqlEntrataMaxBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_rent = ' . $this->sqlEntrataAvgBaseRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_rent = ' . $this->sqlEntrataAvgBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_rent = ' . $this->sqlEntrataMinAmenityRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_rent = ' . $this->sqlEntrataMinAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_rent = ' . $this->sqlEntrataMaxAmenityRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_rent = ' . $this->sqlEntrataMaxAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_rent = ' . $this->sqlEntrataAvgAmenityRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_rent = ' . $this->sqlEntrataAvgAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_rent = ' . $this->sqlEntrataMinSpecialRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_rent = ' . $this->sqlEntrataMinSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_rent = ' . $this->sqlEntrataMaxSpecialRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_rent = ' . $this->sqlEntrataMaxSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_rent = ' . $this->sqlEntrataAvgSpecialRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_rent = ' . $this->sqlEntrataAvgSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_rent = ' . $this->sqlEntrataMinAddOnRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_rent = ' . $this->sqlEntrataMinAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_rent = ' . $this->sqlEntrataMaxAddOnRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_rent = ' . $this->sqlEntrataMaxAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_rent = ' . $this->sqlEntrataAvgAddOnRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_rent = ' . $this->sqlEntrataAvgAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_market_rent = ' . $this->sqlEntrataMinMarketRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMinMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_market_rent = ' . $this->sqlEntrataMinMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_market_rent = ' . $this->sqlEntrataMaxMarketRent(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_market_rent = ' . $this->sqlEntrataMaxMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_market_rent = ' . $this->sqlEntrataAvgMarketRent(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_market_rent = ' . $this->sqlEntrataAvgMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_deposit = ' . $this->sqlEntrataMinBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_deposit = ' . $this->sqlEntrataMinBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_deposit = ' . $this->sqlEntrataMaxBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_deposit = ' . $this->sqlEntrataMaxBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_deposit = ' . $this->sqlEntrataAvgBaseDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_deposit = ' . $this->sqlEntrataAvgBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_deposit = ' . $this->sqlEntrataMinAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_deposit = ' . $this->sqlEntrataMinAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_deposit = ' . $this->sqlEntrataMaxAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_deposit = ' . $this->sqlEntrataMaxAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_deposit = ' . $this->sqlEntrataAvgAmenityDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_deposit = ' . $this->sqlEntrataAvgAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_deposit = ' . $this->sqlEntrataMinSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_deposit = ' . $this->sqlEntrataMinSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_deposit = ' . $this->sqlEntrataMaxSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_deposit = ' . $this->sqlEntrataMaxSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_deposit = ' . $this->sqlEntrataAvgSpecialDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_deposit = ' . $this->sqlEntrataAvgSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_deposit = ' . $this->sqlEntrataMinAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_deposit = ' . $this->sqlEntrataMinAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_deposit = ' . $this->sqlEntrataMaxAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_deposit = ' . $this->sqlEntrataMaxAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_deposit = ' . $this->sqlEntrataAvgAddOnDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_deposit = ' . $this->sqlEntrataAvgAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_total_deposit = ' . $this->sqlEntrataMinTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMinTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_total_deposit = ' . $this->sqlEntrataMinTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_total_deposit = ' . $this->sqlEntrataMaxTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_total_deposit = ' . $this->sqlEntrataMaxTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_total_deposit = ' . $this->sqlEntrataAvgTotalDeposit(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgTotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_total_deposit = ' . $this->sqlEntrataAvgTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_unit_space_ids = ' . $this->sqlRentableUnitSpaceIds(). ',' ; } elseif( true == array_key_exists( 'RentableUnitSpaceIds', $this->getChangedColumns() ) ) { $strSql .= ' rentable_unit_space_ids = ' . $this->sqlRentableUnitSpaceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_unit_space_ids = ' . $this->sqlAvailableUnitSpaceIds(). ',' ; } elseif( true == array_key_exists( 'AvailableUnitSpaceIds', $this->getChangedColumns() ) ) { $strSql .= ' available_unit_space_ids = ' . $this->sqlAvailableUnitSpaceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_rent_range = ' . $this->sqlIsManualRentRange(). ',' ; } elseif( true == array_key_exists( 'IsManualRentRange', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_rent_range = ' . $this->sqlIsManualRentRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_deposit_range = ' . $this->sqlIsManualDepositRange(). ',' ; } elseif( true == array_key_exists( 'IsManualDepositRange', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_deposit_range = ' . $this->sqlIsManualDepositRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId(). ',' ; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_rent_monthly = ' . $this->sqlMinBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MinBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_base_rent_monthly = ' . $this->sqlMinBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_rent_monthly = ' . $this->sqlMaxBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_base_rent_monthly = ' . $this->sqlMaxBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_rent_monthly = ' . $this->sqlAvgBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_rent_monthly = ' . $this->sqlAvgBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_rent_monthly = ' . $this->sqlMinAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_rent_monthly = ' . $this->sqlMinAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_rent_monthly = ' . $this->sqlMaxAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_rent_monthly = ' . $this->sqlMaxAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_rent_monthly = ' . $this->sqlAvgAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_rent_monthly = ' . $this->sqlAvgAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_rent_monthly = ' . $this->sqlMinSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MinSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_special_rent_monthly = ' . $this->sqlMinSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_rent_monthly = ' . $this->sqlMaxSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_special_rent_monthly = ' . $this->sqlMaxSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_rent_monthly = ' . $this->sqlAvgSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_rent_monthly = ' . $this->sqlAvgSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_rent_monthly = ' . $this->sqlMinAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_rent_monthly = ' . $this->sqlMinAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_rent_monthly = ' . $this->sqlMaxAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_rent_monthly = ' . $this->sqlMaxAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_rent_monthly = ' . $this->sqlAvgAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_rent_monthly = ' . $this->sqlAvgAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_market_rent_monthly = ' . $this->sqlMinMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MinMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_market_rent_monthly = ' . $this->sqlMinMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_market_rent_monthly = ' . $this->sqlMaxMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_market_rent_monthly = ' . $this->sqlMaxMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_market_rent_monthly = ' . $this->sqlAvgMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_market_rent_monthly = ' . $this->sqlAvgMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_rent_monthly = ' . $this->sqlEntrataMinBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_rent_monthly = ' . $this->sqlEntrataMinBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_rent_monthly = ' . $this->sqlEntrataMaxBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_rent_monthly = ' . $this->sqlEntrataMaxBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_rent_monthly = ' . $this->sqlEntrataAvgBaseRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_rent_monthly = ' . $this->sqlEntrataAvgBaseRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_rent_monthly = ' . $this->sqlEntrataMinAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_rent_monthly = ' . $this->sqlEntrataMinAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_rent_monthly = ' . $this->sqlEntrataMaxAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_rent_monthly = ' . $this->sqlEntrataMaxAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_rent_monthly = ' . $this->sqlEntrataAvgAmenityRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_rent_monthly = ' . $this->sqlEntrataAvgAmenityRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_rent_monthly = ' . $this->sqlEntrataMinSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_rent_monthly = ' . $this->sqlEntrataMinSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_rent_monthly = ' . $this->sqlEntrataMaxSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_rent_monthly = ' . $this->sqlEntrataMaxSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_rent_monthly = ' . $this->sqlEntrataAvgSpecialRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_rent_monthly = ' . $this->sqlEntrataAvgSpecialRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_rent_monthly = ' . $this->sqlEntrataMinAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_rent_monthly = ' . $this->sqlEntrataMinAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_rent_monthly = ' . $this->sqlEntrataMaxAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_rent_monthly = ' . $this->sqlEntrataMaxAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_rent_monthly = ' . $this->sqlEntrataAvgAddOnRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_rent_monthly = ' . $this->sqlEntrataAvgAddOnRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_market_rent_monthly = ' . $this->sqlEntrataMinMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_market_rent_monthly = ' . $this->sqlEntrataMinMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_market_rent_monthly = ' . $this->sqlEntrataMaxMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_market_rent_monthly = ' . $this->sqlEntrataMaxMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_market_rent_monthly = ' . $this->sqlEntrataAvgMarketRentMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgMarketRentMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_market_rent_monthly = ' . $this->sqlEntrataAvgMarketRentMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeTaxInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_tax_in_advertised_rent = ' . $this->sqlIncludeTaxInAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'IncludeOtherInAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' include_other_in_advertised_rent = ' . $this->sqlIncludeOtherInAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_amount = ' . $this->sqlMinBaseAmount(). ',' ; } elseif( true == array_key_exists( 'MinBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_base_amount = ' . $this->sqlMinBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_amount = ' . $this->sqlMaxBaseAmount(). ',' ; } elseif( true == array_key_exists( 'MaxBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_base_amount = ' . $this->sqlMaxBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_amount = ' . $this->sqlAvgBaseAmount(). ',' ; } elseif( true == array_key_exists( 'AvgBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_amount = ' . $this->sqlAvgBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_amount = ' . $this->sqlMinAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'MinAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_amount = ' . $this->sqlMinAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_amount = ' . $this->sqlMaxAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_amount = ' . $this->sqlMaxAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_amount = ' . $this->sqlAvgAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_amount = ' . $this->sqlAvgAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_amount = ' . $this->sqlMinSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'MinSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_special_amount = ' . $this->sqlMinSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_amount = ' . $this->sqlMaxSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_special_amount = ' . $this->sqlMaxSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_amount = ' . $this->sqlAvgSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_amount = ' . $this->sqlAvgSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_amount = ' . $this->sqlMinAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'MinAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_amount = ' . $this->sqlMinAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_amount = ' . $this->sqlMaxAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_amount = ' . $this->sqlMaxAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_amount = ' . $this->sqlAvgAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_amount = ' . $this->sqlAvgAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_amount = ' . $this->sqlEntrataMinBaseAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_amount = ' . $this->sqlEntrataMinBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_amount = ' . $this->sqlEntrataMaxBaseAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_amount = ' . $this->sqlEntrataMaxBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_amount = ' . $this->sqlEntrataAvgBaseAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_amount = ' . $this->sqlEntrataAvgBaseAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_amount = ' . $this->sqlEntrataMinAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_amount = ' . $this->sqlEntrataMinAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_amount = ' . $this->sqlEntrataMaxAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_amount = ' . $this->sqlEntrataMaxAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_amount = ' . $this->sqlEntrataAvgAmenityAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_amount = ' . $this->sqlEntrataAvgAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_amount = ' . $this->sqlEntrataMinSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_amount = ' . $this->sqlEntrataMinSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_amount = ' . $this->sqlEntrataMaxSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_amount = ' . $this->sqlEntrataMaxSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_amount = ' . $this->sqlEntrataAvgSpecialAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_amount = ' . $this->sqlEntrataAvgSpecialAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_amount = ' . $this->sqlEntrataMinAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_amount = ' . $this->sqlEntrataMinAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_amount = ' . $this->sqlEntrataMaxAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_amount = ' . $this->sqlEntrataMaxAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_amount = ' . $this->sqlEntrataAvgAddOnAmount(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnAmount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_amount = ' . $this->sqlEntrataAvgAddOnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_tax = ' . $this->sqlMinBaseTax(). ',' ; } elseif( true == array_key_exists( 'MinBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' min_base_tax = ' . $this->sqlMinBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_tax = ' . $this->sqlMaxBaseTax(). ',' ; } elseif( true == array_key_exists( 'MaxBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' max_base_tax = ' . $this->sqlMaxBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_tax = ' . $this->sqlAvgBaseTax(). ',' ; } elseif( true == array_key_exists( 'AvgBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_tax = ' . $this->sqlAvgBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_tax = ' . $this->sqlMinAmenityTax(). ',' ; } elseif( true == array_key_exists( 'MinAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_tax = ' . $this->sqlMinAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_tax = ' . $this->sqlMaxAmenityTax(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_tax = ' . $this->sqlMaxAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_tax = ' . $this->sqlAvgAmenityTax(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_tax = ' . $this->sqlAvgAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_tax = ' . $this->sqlMinSpecialTax(). ',' ; } elseif( true == array_key_exists( 'MinSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' min_special_tax = ' . $this->sqlMinSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_tax = ' . $this->sqlMaxSpecialTax(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' max_special_tax = ' . $this->sqlMaxSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_tax = ' . $this->sqlAvgSpecialTax(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_tax = ' . $this->sqlAvgSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_tax = ' . $this->sqlMinAddOnTax(). ',' ; } elseif( true == array_key_exists( 'MinAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_tax = ' . $this->sqlMinAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_tax = ' . $this->sqlMaxAddOnTax(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_tax = ' . $this->sqlMaxAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_tax = ' . $this->sqlAvgAddOnTax(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_tax = ' . $this->sqlAvgAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_tax = ' . $this->sqlEntrataMinBaseTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_tax = ' . $this->sqlEntrataMinBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_tax = ' . $this->sqlEntrataMaxBaseTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_tax = ' . $this->sqlEntrataMaxBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_tax = ' . $this->sqlEntrataAvgBaseTax(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_tax = ' . $this->sqlEntrataAvgBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_tax = ' . $this->sqlEntrataMinAmenityTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_tax = ' . $this->sqlEntrataMinAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_tax = ' . $this->sqlEntrataMaxAmenityTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_tax = ' . $this->sqlEntrataMaxAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_tax = ' . $this->sqlEntrataAvgAmenityTax(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_tax = ' . $this->sqlEntrataAvgAmenityTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_tax = ' . $this->sqlEntrataMinSpecialTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_tax = ' . $this->sqlEntrataMinSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_tax = ' . $this->sqlEntrataMaxSpecialTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_tax = ' . $this->sqlEntrataMaxSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_tax = ' . $this->sqlEntrataAvgSpecialTax(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_tax = ' . $this->sqlEntrataAvgSpecialTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_tax = ' . $this->sqlEntrataMinAddOnTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_tax = ' . $this->sqlEntrataMinAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_tax = ' . $this->sqlEntrataMaxAddOnTax(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_tax = ' . $this->sqlEntrataMaxAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_tax = ' . $this->sqlEntrataAvgAddOnTax(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnTax', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_tax = ' . $this->sqlEntrataAvgAddOnTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_amount_monthly = ' . $this->sqlMinBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MinBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_base_amount_monthly = ' . $this->sqlMinBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_amount_monthly = ' . $this->sqlMaxBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_base_amount_monthly = ' . $this->sqlMaxBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_amount_monthly = ' . $this->sqlAvgBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_amount_monthly = ' . $this->sqlAvgBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_base_tax_monthly = ' . $this->sqlMinBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MinBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_base_tax_monthly = ' . $this->sqlMinBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_base_tax_monthly = ' . $this->sqlMaxBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_base_tax_monthly = ' . $this->sqlMaxBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_base_tax_monthly = ' . $this->sqlAvgBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_base_tax_monthly = ' . $this->sqlAvgBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_amount_monthly = ' . $this->sqlMinAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_amount_monthly = ' . $this->sqlMinAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_amount_monthly = ' . $this->sqlMaxAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_amount_monthly = ' . $this->sqlMaxAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_amount_monthly = ' . $this->sqlAvgAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_amount_monthly = ' . $this->sqlAvgAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_amenity_tax_monthly = ' . $this->sqlMinAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_amenity_tax_monthly = ' . $this->sqlMinAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_amenity_tax_monthly = ' . $this->sqlMaxAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_amenity_tax_monthly = ' . $this->sqlMaxAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_amenity_tax_monthly = ' . $this->sqlAvgAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_amenity_tax_monthly = ' . $this->sqlAvgAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_amount_monthly = ' . $this->sqlMinSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MinSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_special_amount_monthly = ' . $this->sqlMinSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_amount_monthly = ' . $this->sqlMaxSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_special_amount_monthly = ' . $this->sqlMaxSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_amount_monthly = ' . $this->sqlAvgSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_amount_monthly = ' . $this->sqlAvgSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_special_tax_monthly = ' . $this->sqlMinSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MinSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_special_tax_monthly = ' . $this->sqlMinSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_special_tax_monthly = ' . $this->sqlMaxSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_special_tax_monthly = ' . $this->sqlMaxSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_special_tax_monthly = ' . $this->sqlAvgSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_special_tax_monthly = ' . $this->sqlAvgSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_amount_monthly = ' . $this->sqlMinAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_amount_monthly = ' . $this->sqlMinAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_amount_monthly = ' . $this->sqlMaxAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_amount_monthly = ' . $this->sqlMaxAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_amount_monthly = ' . $this->sqlAvgAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_amount_monthly = ' . $this->sqlAvgAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_add_on_tax_monthly = ' . $this->sqlMinAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MinAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' min_add_on_tax_monthly = ' . $this->sqlMinAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_add_on_tax_monthly = ' . $this->sqlMaxAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'MaxAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' max_add_on_tax_monthly = ' . $this->sqlMaxAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_add_on_tax_monthly = ' . $this->sqlAvgAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'AvgAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' avg_add_on_tax_monthly = ' . $this->sqlAvgAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_amount_monthly = ' . $this->sqlEntrataMinBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_amount_monthly = ' . $this->sqlEntrataMinBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_amount_monthly = ' . $this->sqlEntrataMaxBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_amount_monthly = ' . $this->sqlEntrataMaxBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_amount_monthly = ' . $this->sqlEntrataAvgBaseAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_amount_monthly = ' . $this->sqlEntrataAvgBaseAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_base_tax_monthly = ' . $this->sqlEntrataMinBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_base_tax_monthly = ' . $this->sqlEntrataMinBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_base_tax_monthly = ' . $this->sqlEntrataMaxBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_base_tax_monthly = ' . $this->sqlEntrataMaxBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_base_tax_monthly = ' . $this->sqlEntrataAvgBaseTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgBaseTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_base_tax_monthly = ' . $this->sqlEntrataAvgBaseTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_amount_monthly = ' . $this->sqlEntrataMinAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_amount_monthly = ' . $this->sqlEntrataMinAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_amount_monthly = ' . $this->sqlEntrataMaxAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_amount_monthly = ' . $this->sqlEntrataMaxAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_amount_monthly = ' . $this->sqlEntrataAvgAmenityAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_amount_monthly = ' . $this->sqlEntrataAvgAmenityAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_amenity_tax_monthly = ' . $this->sqlEntrataMinAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_amenity_tax_monthly = ' . $this->sqlEntrataMinAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_amenity_tax_monthly = ' . $this->sqlEntrataMaxAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_amenity_tax_monthly = ' . $this->sqlEntrataMaxAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_amenity_tax_monthly = ' . $this->sqlEntrataAvgAmenityTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAmenityTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_amenity_tax_monthly = ' . $this->sqlEntrataAvgAmenityTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_amount_monthly = ' . $this->sqlEntrataMinSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_amount_monthly = ' . $this->sqlEntrataMinSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_amount_monthly = ' . $this->sqlEntrataMaxSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_amount_monthly = ' . $this->sqlEntrataMaxSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_amount_monthly = ' . $this->sqlEntrataAvgSpecialAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_amount_monthly = ' . $this->sqlEntrataAvgSpecialAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_special_tax_monthly = ' . $this->sqlEntrataMinSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_special_tax_monthly = ' . $this->sqlEntrataMinSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_special_tax_monthly = ' . $this->sqlEntrataMaxSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_special_tax_monthly = ' . $this->sqlEntrataMaxSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_special_tax_monthly = ' . $this->sqlEntrataAvgSpecialTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgSpecialTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_special_tax_monthly = ' . $this->sqlEntrataAvgSpecialTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_amount_monthly = ' . $this->sqlEntrataMinAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_amount_monthly = ' . $this->sqlEntrataMinAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_amount_monthly = ' . $this->sqlEntrataMaxAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_amount_monthly = ' . $this->sqlEntrataMaxAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_amount_monthly = ' . $this->sqlEntrataAvgAddOnAmountMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnAmountMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_amount_monthly = ' . $this->sqlEntrataAvgAddOnAmountMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_min_add_on_tax_monthly = ' . $this->sqlEntrataMinAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMinAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_min_add_on_tax_monthly = ' . $this->sqlEntrataMinAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_max_add_on_tax_monthly = ' . $this->sqlEntrataMaxAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataMaxAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_max_add_on_tax_monthly = ' . $this->sqlEntrataMaxAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_avg_add_on_tax_monthly = ' . $this->sqlEntrataAvgAddOnTaxMonthly(). ',' ; } elseif( true == array_key_exists( 'EntrataAvgAddOnTaxMonthly', $this->getChangedColumns() ) ) { $strSql .= ' entrata_avg_add_on_tax_monthly = ' . $this->sqlEntrataAvgAddOnTaxMonthly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange() ; } elseif( true == array_key_exists( 'EffectiveRange', $this->getChangedColumns() ) ) { $strSql .= ' effective_range = ' . $this->sqlEffectiveRange() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'space_configuration_id' => $this->getSpaceConfigurationId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'total_unit_count' => $this->getTotalUnitCount(),
			'rentable_unit_count' => $this->getRentableUnitCount(),
			'available_unit_count' => $this->getAvailableUnitCount(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'avg_square_feet' => $this->getAvgSquareFeet(),
			'min_base_rent' => $this->getMinBaseRent(),
			'max_base_rent' => $this->getMaxBaseRent(),
			'avg_base_rent' => $this->getAvgBaseRent(),
			'min_amenity_rent' => $this->getMinAmenityRent(),
			'max_amenity_rent' => $this->getMaxAmenityRent(),
			'avg_amenity_rent' => $this->getAvgAmenityRent(),
			'min_special_rent' => $this->getMinSpecialRent(),
			'max_special_rent' => $this->getMaxSpecialRent(),
			'avg_special_rent' => $this->getAvgSpecialRent(),
			'min_add_on_rent' => $this->getMinAddOnRent(),
			'max_add_on_rent' => $this->getMaxAddOnRent(),
			'avg_add_on_rent' => $this->getAvgAddOnRent(),
			'min_market_rent' => $this->getMinMarketRent(),
			'max_market_rent' => $this->getMaxMarketRent(),
			'avg_market_rent' => $this->getAvgMarketRent(),
			'manual_min_market_rent' => $this->getManualMinMarketRent(),
			'manual_max_market_rent' => $this->getManualMaxMarketRent(),
			'min_base_deposit' => $this->getMinBaseDeposit(),
			'max_base_deposit' => $this->getMaxBaseDeposit(),
			'avg_base_deposit' => $this->getAvgBaseDeposit(),
			'min_amenity_deposit' => $this->getMinAmenityDeposit(),
			'max_amenity_deposit' => $this->getMaxAmenityDeposit(),
			'avg_amenity_deposit' => $this->getAvgAmenityDeposit(),
			'min_special_deposit' => $this->getMinSpecialDeposit(),
			'max_special_deposit' => $this->getMaxSpecialDeposit(),
			'avg_special_deposit' => $this->getAvgSpecialDeposit(),
			'min_add_on_deposit' => $this->getMinAddOnDeposit(),
			'max_add_on_deposit' => $this->getMaxAddOnDeposit(),
			'avg_add_on_deposit' => $this->getAvgAddOnDeposit(),
			'min_total_deposit' => $this->getMinTotalDeposit(),
			'max_total_deposit' => $this->getMaxTotalDeposit(),
			'avg_total_deposit' => $this->getAvgTotalDeposit(),
			'manual_min_total_deposit' => $this->getManualMinTotalDeposit(),
			'manual_max_total_deposit' => $this->getManualMaxTotalDeposit(),
			'entrata_min_base_rent' => $this->getEntrataMinBaseRent(),
			'entrata_max_base_rent' => $this->getEntrataMaxBaseRent(),
			'entrata_avg_base_rent' => $this->getEntrataAvgBaseRent(),
			'entrata_min_amenity_rent' => $this->getEntrataMinAmenityRent(),
			'entrata_max_amenity_rent' => $this->getEntrataMaxAmenityRent(),
			'entrata_avg_amenity_rent' => $this->getEntrataAvgAmenityRent(),
			'entrata_min_special_rent' => $this->getEntrataMinSpecialRent(),
			'entrata_max_special_rent' => $this->getEntrataMaxSpecialRent(),
			'entrata_avg_special_rent' => $this->getEntrataAvgSpecialRent(),
			'entrata_min_add_on_rent' => $this->getEntrataMinAddOnRent(),
			'entrata_max_add_on_rent' => $this->getEntrataMaxAddOnRent(),
			'entrata_avg_add_on_rent' => $this->getEntrataAvgAddOnRent(),
			'entrata_min_market_rent' => $this->getEntrataMinMarketRent(),
			'entrata_max_market_rent' => $this->getEntrataMaxMarketRent(),
			'entrata_avg_market_rent' => $this->getEntrataAvgMarketRent(),
			'entrata_min_base_deposit' => $this->getEntrataMinBaseDeposit(),
			'entrata_max_base_deposit' => $this->getEntrataMaxBaseDeposit(),
			'entrata_avg_base_deposit' => $this->getEntrataAvgBaseDeposit(),
			'entrata_min_amenity_deposit' => $this->getEntrataMinAmenityDeposit(),
			'entrata_max_amenity_deposit' => $this->getEntrataMaxAmenityDeposit(),
			'entrata_avg_amenity_deposit' => $this->getEntrataAvgAmenityDeposit(),
			'entrata_min_special_deposit' => $this->getEntrataMinSpecialDeposit(),
			'entrata_max_special_deposit' => $this->getEntrataMaxSpecialDeposit(),
			'entrata_avg_special_deposit' => $this->getEntrataAvgSpecialDeposit(),
			'entrata_min_add_on_deposit' => $this->getEntrataMinAddOnDeposit(),
			'entrata_max_add_on_deposit' => $this->getEntrataMaxAddOnDeposit(),
			'entrata_avg_add_on_deposit' => $this->getEntrataAvgAddOnDeposit(),
			'entrata_min_total_deposit' => $this->getEntrataMinTotalDeposit(),
			'entrata_max_total_deposit' => $this->getEntrataMaxTotalDeposit(),
			'entrata_avg_total_deposit' => $this->getEntrataAvgTotalDeposit(),
			'unit_space_ids' => $this->getUnitSpaceIds(),
			'rentable_unit_space_ids' => $this->getRentableUnitSpaceIds(),
			'available_unit_space_ids' => $this->getAvailableUnitSpaceIds(),
			'is_manual_rent_range' => $this->getIsManualRentRange(),
			'is_manual_deposit_range' => $this->getIsManualDepositRange(),
			'effective_date' => $this->getEffectiveDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'min_base_rent_monthly' => $this->getMinBaseRentMonthly(),
			'max_base_rent_monthly' => $this->getMaxBaseRentMonthly(),
			'avg_base_rent_monthly' => $this->getAvgBaseRentMonthly(),
			'min_amenity_rent_monthly' => $this->getMinAmenityRentMonthly(),
			'max_amenity_rent_monthly' => $this->getMaxAmenityRentMonthly(),
			'avg_amenity_rent_monthly' => $this->getAvgAmenityRentMonthly(),
			'min_special_rent_monthly' => $this->getMinSpecialRentMonthly(),
			'max_special_rent_monthly' => $this->getMaxSpecialRentMonthly(),
			'avg_special_rent_monthly' => $this->getAvgSpecialRentMonthly(),
			'min_add_on_rent_monthly' => $this->getMinAddOnRentMonthly(),
			'max_add_on_rent_monthly' => $this->getMaxAddOnRentMonthly(),
			'avg_add_on_rent_monthly' => $this->getAvgAddOnRentMonthly(),
			'min_market_rent_monthly' => $this->getMinMarketRentMonthly(),
			'max_market_rent_monthly' => $this->getMaxMarketRentMonthly(),
			'avg_market_rent_monthly' => $this->getAvgMarketRentMonthly(),
			'entrata_min_base_rent_monthly' => $this->getEntrataMinBaseRentMonthly(),
			'entrata_max_base_rent_monthly' => $this->getEntrataMaxBaseRentMonthly(),
			'entrata_avg_base_rent_monthly' => $this->getEntrataAvgBaseRentMonthly(),
			'entrata_min_amenity_rent_monthly' => $this->getEntrataMinAmenityRentMonthly(),
			'entrata_max_amenity_rent_monthly' => $this->getEntrataMaxAmenityRentMonthly(),
			'entrata_avg_amenity_rent_monthly' => $this->getEntrataAvgAmenityRentMonthly(),
			'entrata_min_special_rent_monthly' => $this->getEntrataMinSpecialRentMonthly(),
			'entrata_max_special_rent_monthly' => $this->getEntrataMaxSpecialRentMonthly(),
			'entrata_avg_special_rent_monthly' => $this->getEntrataAvgSpecialRentMonthly(),
			'entrata_min_add_on_rent_monthly' => $this->getEntrataMinAddOnRentMonthly(),
			'entrata_max_add_on_rent_monthly' => $this->getEntrataMaxAddOnRentMonthly(),
			'entrata_avg_add_on_rent_monthly' => $this->getEntrataAvgAddOnRentMonthly(),
			'entrata_min_market_rent_monthly' => $this->getEntrataMinMarketRentMonthly(),
			'entrata_max_market_rent_monthly' => $this->getEntrataMaxMarketRentMonthly(),
			'entrata_avg_market_rent_monthly' => $this->getEntrataAvgMarketRentMonthly(),
			'include_tax_in_advertised_rent' => $this->getIncludeTaxInAdvertisedRent(),
			'include_other_in_advertised_rent' => $this->getIncludeOtherInAdvertisedRent(),
			'min_base_amount' => $this->getMinBaseAmount(),
			'max_base_amount' => $this->getMaxBaseAmount(),
			'avg_base_amount' => $this->getAvgBaseAmount(),
			'min_amenity_amount' => $this->getMinAmenityAmount(),
			'max_amenity_amount' => $this->getMaxAmenityAmount(),
			'avg_amenity_amount' => $this->getAvgAmenityAmount(),
			'min_special_amount' => $this->getMinSpecialAmount(),
			'max_special_amount' => $this->getMaxSpecialAmount(),
			'avg_special_amount' => $this->getAvgSpecialAmount(),
			'min_add_on_amount' => $this->getMinAddOnAmount(),
			'max_add_on_amount' => $this->getMaxAddOnAmount(),
			'avg_add_on_amount' => $this->getAvgAddOnAmount(),
			'entrata_min_base_amount' => $this->getEntrataMinBaseAmount(),
			'entrata_max_base_amount' => $this->getEntrataMaxBaseAmount(),
			'entrata_avg_base_amount' => $this->getEntrataAvgBaseAmount(),
			'entrata_min_amenity_amount' => $this->getEntrataMinAmenityAmount(),
			'entrata_max_amenity_amount' => $this->getEntrataMaxAmenityAmount(),
			'entrata_avg_amenity_amount' => $this->getEntrataAvgAmenityAmount(),
			'entrata_min_special_amount' => $this->getEntrataMinSpecialAmount(),
			'entrata_max_special_amount' => $this->getEntrataMaxSpecialAmount(),
			'entrata_avg_special_amount' => $this->getEntrataAvgSpecialAmount(),
			'entrata_min_add_on_amount' => $this->getEntrataMinAddOnAmount(),
			'entrata_max_add_on_amount' => $this->getEntrataMaxAddOnAmount(),
			'entrata_avg_add_on_amount' => $this->getEntrataAvgAddOnAmount(),
			'min_base_tax' => $this->getMinBaseTax(),
			'max_base_tax' => $this->getMaxBaseTax(),
			'avg_base_tax' => $this->getAvgBaseTax(),
			'min_amenity_tax' => $this->getMinAmenityTax(),
			'max_amenity_tax' => $this->getMaxAmenityTax(),
			'avg_amenity_tax' => $this->getAvgAmenityTax(),
			'min_special_tax' => $this->getMinSpecialTax(),
			'max_special_tax' => $this->getMaxSpecialTax(),
			'avg_special_tax' => $this->getAvgSpecialTax(),
			'min_add_on_tax' => $this->getMinAddOnTax(),
			'max_add_on_tax' => $this->getMaxAddOnTax(),
			'avg_add_on_tax' => $this->getAvgAddOnTax(),
			'entrata_min_base_tax' => $this->getEntrataMinBaseTax(),
			'entrata_max_base_tax' => $this->getEntrataMaxBaseTax(),
			'entrata_avg_base_tax' => $this->getEntrataAvgBaseTax(),
			'entrata_min_amenity_tax' => $this->getEntrataMinAmenityTax(),
			'entrata_max_amenity_tax' => $this->getEntrataMaxAmenityTax(),
			'entrata_avg_amenity_tax' => $this->getEntrataAvgAmenityTax(),
			'entrata_min_special_tax' => $this->getEntrataMinSpecialTax(),
			'entrata_max_special_tax' => $this->getEntrataMaxSpecialTax(),
			'entrata_avg_special_tax' => $this->getEntrataAvgSpecialTax(),
			'entrata_min_add_on_tax' => $this->getEntrataMinAddOnTax(),
			'entrata_max_add_on_tax' => $this->getEntrataMaxAddOnTax(),
			'entrata_avg_add_on_tax' => $this->getEntrataAvgAddOnTax(),
			'min_base_amount_monthly' => $this->getMinBaseAmountMonthly(),
			'max_base_amount_monthly' => $this->getMaxBaseAmountMonthly(),
			'avg_base_amount_monthly' => $this->getAvgBaseAmountMonthly(),
			'min_base_tax_monthly' => $this->getMinBaseTaxMonthly(),
			'max_base_tax_monthly' => $this->getMaxBaseTaxMonthly(),
			'avg_base_tax_monthly' => $this->getAvgBaseTaxMonthly(),
			'min_amenity_amount_monthly' => $this->getMinAmenityAmountMonthly(),
			'max_amenity_amount_monthly' => $this->getMaxAmenityAmountMonthly(),
			'avg_amenity_amount_monthly' => $this->getAvgAmenityAmountMonthly(),
			'min_amenity_tax_monthly' => $this->getMinAmenityTaxMonthly(),
			'max_amenity_tax_monthly' => $this->getMaxAmenityTaxMonthly(),
			'avg_amenity_tax_monthly' => $this->getAvgAmenityTaxMonthly(),
			'min_special_amount_monthly' => $this->getMinSpecialAmountMonthly(),
			'max_special_amount_monthly' => $this->getMaxSpecialAmountMonthly(),
			'avg_special_amount_monthly' => $this->getAvgSpecialAmountMonthly(),
			'min_special_tax_monthly' => $this->getMinSpecialTaxMonthly(),
			'max_special_tax_monthly' => $this->getMaxSpecialTaxMonthly(),
			'avg_special_tax_monthly' => $this->getAvgSpecialTaxMonthly(),
			'min_add_on_amount_monthly' => $this->getMinAddOnAmountMonthly(),
			'max_add_on_amount_monthly' => $this->getMaxAddOnAmountMonthly(),
			'avg_add_on_amount_monthly' => $this->getAvgAddOnAmountMonthly(),
			'min_add_on_tax_monthly' => $this->getMinAddOnTaxMonthly(),
			'max_add_on_tax_monthly' => $this->getMaxAddOnTaxMonthly(),
			'avg_add_on_tax_monthly' => $this->getAvgAddOnTaxMonthly(),
			'entrata_min_base_amount_monthly' => $this->getEntrataMinBaseAmountMonthly(),
			'entrata_max_base_amount_monthly' => $this->getEntrataMaxBaseAmountMonthly(),
			'entrata_avg_base_amount_monthly' => $this->getEntrataAvgBaseAmountMonthly(),
			'entrata_min_base_tax_monthly' => $this->getEntrataMinBaseTaxMonthly(),
			'entrata_max_base_tax_monthly' => $this->getEntrataMaxBaseTaxMonthly(),
			'entrata_avg_base_tax_monthly' => $this->getEntrataAvgBaseTaxMonthly(),
			'entrata_min_amenity_amount_monthly' => $this->getEntrataMinAmenityAmountMonthly(),
			'entrata_max_amenity_amount_monthly' => $this->getEntrataMaxAmenityAmountMonthly(),
			'entrata_avg_amenity_amount_monthly' => $this->getEntrataAvgAmenityAmountMonthly(),
			'entrata_min_amenity_tax_monthly' => $this->getEntrataMinAmenityTaxMonthly(),
			'entrata_max_amenity_tax_monthly' => $this->getEntrataMaxAmenityTaxMonthly(),
			'entrata_avg_amenity_tax_monthly' => $this->getEntrataAvgAmenityTaxMonthly(),
			'entrata_min_special_amount_monthly' => $this->getEntrataMinSpecialAmountMonthly(),
			'entrata_max_special_amount_monthly' => $this->getEntrataMaxSpecialAmountMonthly(),
			'entrata_avg_special_amount_monthly' => $this->getEntrataAvgSpecialAmountMonthly(),
			'entrata_min_special_tax_monthly' => $this->getEntrataMinSpecialTaxMonthly(),
			'entrata_max_special_tax_monthly' => $this->getEntrataMaxSpecialTaxMonthly(),
			'entrata_avg_special_tax_monthly' => $this->getEntrataAvgSpecialTaxMonthly(),
			'entrata_min_add_on_amount_monthly' => $this->getEntrataMinAddOnAmountMonthly(),
			'entrata_max_add_on_amount_monthly' => $this->getEntrataMaxAddOnAmountMonthly(),
			'entrata_avg_add_on_amount_monthly' => $this->getEntrataAvgAddOnAmountMonthly(),
			'entrata_min_add_on_tax_monthly' => $this->getEntrataMinAddOnTaxMonthly(),
			'entrata_max_add_on_tax_monthly' => $this->getEntrataMaxAddOnTaxMonthly(),
			'entrata_avg_add_on_tax_monthly' => $this->getEntrataAvgAddOnTaxMonthly(),
			'effective_range' => $this->getEffectiveRange()
		);
	}

}
?>