<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlLedgerTypes
 * Do not add any new functions to this class.
 */

class CBaseGlLedgerTypes extends CEosPluralBase {

	/**
	 * @return CGlLedgerType[]
	 */
	public static function fetchGlLedgerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlLedgerType::class, $objDatabase );
	}

	/**
	 * @return CGlLedgerType
	 */
	public static function fetchGlLedgerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlLedgerType::class, $objDatabase );
	}

	public static function fetchGlLedgerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_ledger_types', $objDatabase );
	}

	public static function fetchGlLedgerTypeById( $intId, $objDatabase ) {
		return self::fetchGlLedgerType( sprintf( 'SELECT * FROM gl_ledger_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>