<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteWrapper extends CEosSingularBase {

	const TABLE_NAME = 'public.website_wrappers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intWebsiteWrapperTypeId;
	protected $m_strWrapperKey;
	protected $m_strWrapperUrl;
	protected $m_strWrapperCache;
	protected $m_strCustomCss;
	protected $m_strCachedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_wrapper_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteWrapperTypeId', trim( $arrValues['website_wrapper_type_id'] ) ); elseif( isset( $arrValues['website_wrapper_type_id'] ) ) $this->setWebsiteWrapperTypeId( $arrValues['website_wrapper_type_id'] );
		if( isset( $arrValues['wrapper_key'] ) && $boolDirectSet ) $this->set( 'm_strWrapperKey', trim( stripcslashes( $arrValues['wrapper_key'] ) ) ); elseif( isset( $arrValues['wrapper_key'] ) ) $this->setWrapperKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['wrapper_key'] ) : $arrValues['wrapper_key'] );
		if( isset( $arrValues['wrapper_url'] ) && $boolDirectSet ) $this->set( 'm_strWrapperUrl', trim( stripcslashes( $arrValues['wrapper_url'] ) ) ); elseif( isset( $arrValues['wrapper_url'] ) ) $this->setWrapperUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['wrapper_url'] ) : $arrValues['wrapper_url'] );
		if( isset( $arrValues['wrapper_cache'] ) && $boolDirectSet ) $this->set( 'm_strWrapperCache', trim( stripcslashes( $arrValues['wrapper_cache'] ) ) ); elseif( isset( $arrValues['wrapper_cache'] ) ) $this->setWrapperCache( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['wrapper_cache'] ) : $arrValues['wrapper_cache'] );
		if( isset( $arrValues['custom_css'] ) && $boolDirectSet ) $this->set( 'm_strCustomCss', trim( stripcslashes( $arrValues['custom_css'] ) ) ); elseif( isset( $arrValues['custom_css'] ) ) $this->setCustomCss( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_css'] ) : $arrValues['custom_css'] );
		if( isset( $arrValues['cached_on'] ) && $boolDirectSet ) $this->set( 'm_strCachedOn', trim( $arrValues['cached_on'] ) ); elseif( isset( $arrValues['cached_on'] ) ) $this->setCachedOn( $arrValues['cached_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteWrapperTypeId( $intWebsiteWrapperTypeId ) {
		$this->set( 'm_intWebsiteWrapperTypeId', CStrings::strToIntDef( $intWebsiteWrapperTypeId, NULL, false ) );
	}

	public function getWebsiteWrapperTypeId() {
		return $this->m_intWebsiteWrapperTypeId;
	}

	public function sqlWebsiteWrapperTypeId() {
		return ( true == isset( $this->m_intWebsiteWrapperTypeId ) ) ? ( string ) $this->m_intWebsiteWrapperTypeId : 'NULL';
	}

	public function setWrapperKey( $strWrapperKey ) {
		$this->set( 'm_strWrapperKey', CStrings::strTrimDef( $strWrapperKey, 64, NULL, true ) );
	}

	public function getWrapperKey() {
		return $this->m_strWrapperKey;
	}

	public function sqlWrapperKey() {
		return ( true == isset( $this->m_strWrapperKey ) ) ? '\'' . addslashes( $this->m_strWrapperKey ) . '\'' : 'NULL';
	}

	public function setWrapperUrl( $strWrapperUrl ) {
		$this->set( 'm_strWrapperUrl', CStrings::strTrimDef( $strWrapperUrl, 4096, NULL, true ) );
	}

	public function getWrapperUrl() {
		return $this->m_strWrapperUrl;
	}

	public function sqlWrapperUrl() {
		return ( true == isset( $this->m_strWrapperUrl ) ) ? '\'' . addslashes( $this->m_strWrapperUrl ) . '\'' : 'NULL';
	}

	public function setWrapperCache( $strWrapperCache ) {
		$this->set( 'm_strWrapperCache', CStrings::strTrimDef( $strWrapperCache, -1, NULL, true ) );
	}

	public function getWrapperCache() {
		return $this->m_strWrapperCache;
	}

	public function sqlWrapperCache() {
		return ( true == isset( $this->m_strWrapperCache ) ) ? '\'' . addslashes( $this->m_strWrapperCache ) . '\'' : 'NULL';
	}

	public function setCustomCss( $strCustomCss ) {
		$this->set( 'm_strCustomCss', CStrings::strTrimDef( $strCustomCss, -1, NULL, true ) );
	}

	public function getCustomCss() {
		return $this->m_strCustomCss;
	}

	public function sqlCustomCss() {
		return ( true == isset( $this->m_strCustomCss ) ) ? '\'' . addslashes( $this->m_strCustomCss ) . '\'' : 'NULL';
	}

	public function setCachedOn( $strCachedOn ) {
		$this->set( 'm_strCachedOn', CStrings::strTrimDef( $strCachedOn, -1, NULL, true ) );
	}

	public function getCachedOn() {
		return $this->m_strCachedOn;
	}

	public function sqlCachedOn() {
		return ( true == isset( $this->m_strCachedOn ) ) ? '\'' . $this->m_strCachedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, website_wrapper_type_id, wrapper_key, wrapper_url, wrapper_cache, custom_css, cached_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlWebsiteWrapperTypeId() . ', ' .
 						$this->sqlWrapperKey() . ', ' .
 						$this->sqlWrapperUrl() . ', ' .
 						$this->sqlWrapperCache() . ', ' .
 						$this->sqlCustomCss() . ', ' .
 						$this->sqlCachedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_wrapper_type_id = ' . $this->sqlWebsiteWrapperTypeId() . ','; } elseif( true == array_key_exists( 'WebsiteWrapperTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_wrapper_type_id = ' . $this->sqlWebsiteWrapperTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wrapper_key = ' . $this->sqlWrapperKey() . ','; } elseif( true == array_key_exists( 'WrapperKey', $this->getChangedColumns() ) ) { $strSql .= ' wrapper_key = ' . $this->sqlWrapperKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wrapper_url = ' . $this->sqlWrapperUrl() . ','; } elseif( true == array_key_exists( 'WrapperUrl', $this->getChangedColumns() ) ) { $strSql .= ' wrapper_url = ' . $this->sqlWrapperUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wrapper_cache = ' . $this->sqlWrapperCache() . ','; } elseif( true == array_key_exists( 'WrapperCache', $this->getChangedColumns() ) ) { $strSql .= ' wrapper_cache = ' . $this->sqlWrapperCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_css = ' . $this->sqlCustomCss() . ','; } elseif( true == array_key_exists( 'CustomCss', $this->getChangedColumns() ) ) { $strSql .= ' custom_css = ' . $this->sqlCustomCss() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_on = ' . $this->sqlCachedOn() . ','; } elseif( true == array_key_exists( 'CachedOn', $this->getChangedColumns() ) ) { $strSql .= ' cached_on = ' . $this->sqlCachedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'website_wrapper_type_id' => $this->getWebsiteWrapperTypeId(),
			'wrapper_key' => $this->getWrapperKey(),
			'wrapper_url' => $this->getWrapperUrl(),
			'wrapper_cache' => $this->getWrapperCache(),
			'custom_css' => $this->getCustomCss(),
			'cached_on' => $this->getCachedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>