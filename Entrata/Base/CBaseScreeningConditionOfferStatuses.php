<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOfferStatuses
 * Do not add any new functions to this class.
 */

class CBaseScreeningConditionOfferStatuses extends CEosPluralBase {

	/**
	 * @return CScreeningConditionOfferStatus[]
	 */
	public static function fetchScreeningConditionOfferStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningConditionOfferStatus::class, $objDatabase );
	}

	/**
	 * @return CScreeningConditionOfferStatus
	 */
	public static function fetchScreeningConditionOfferStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningConditionOfferStatus::class, $objDatabase );
	}

	public static function fetchScreeningConditionOfferStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_condition_offer_statuses', $objDatabase );
	}

	public static function fetchScreeningConditionOfferStatusById( $intId, $objDatabase ) {
		return self::fetchScreeningConditionOfferStatus( sprintf( 'SELECT * FROM screening_condition_offer_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>