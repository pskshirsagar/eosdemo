<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReviewDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.review_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intReviewId;
	protected $m_strName;
	protected $m_strTitle;
	protected $m_strReview;
	protected $m_strReviewUrl;
	protected $m_strEmailAddress;
	protected $m_strReviewDatetime;
	protected $m_intOverallRating;
	protected $m_strFinalResponse;
	protected $m_boolIsRecommended;
	protected $m_strIpAddress;
	protected $m_boolIsProspectPortalPublished;
	protected $m_boolIsRespondedTo;
	protected $m_boolHasAttributes;
	protected $m_boolIsCompleted;
	protected $m_boolIsLcResponseCompleted;
	protected $m_boolIsResponseApproved;
	protected $m_boolIsNotificationSent;
	protected $m_boolHideUnpublishedOnDashboard;
	protected $m_boolHideUnrespondedOnDashboard;
	protected $m_boolHideNeedsAttributesOnDashboard;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intReviewDetailStatusId;
	protected $m_intReviewRecommendationTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRecommended = false;
		$this->m_boolIsProspectPortalPublished = false;
		$this->m_boolIsRespondedTo = false;
		$this->m_boolHasAttributes = false;
		$this->m_boolIsCompleted = false;
		$this->m_boolIsLcResponseCompleted = false;
		$this->m_boolIsResponseApproved = false;
		$this->m_boolIsNotificationSent = false;
		$this->m_boolHideUnpublishedOnDashboard = false;
		$this->m_boolHideUnrespondedOnDashboard = false;
		$this->m_boolHideNeedsAttributesOnDashboard = false;
		$this->m_intReviewDetailStatusId = '1';
		$this->m_intReviewRecommendationTypeId = '3';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['review_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewId', trim( $arrValues['review_id'] ) ); elseif( isset( $arrValues['review_id'] ) ) $this->setReviewId( $arrValues['review_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['review'] ) && $boolDirectSet ) $this->set( 'm_strReview', trim( stripcslashes( $arrValues['review'] ) ) ); elseif( isset( $arrValues['review'] ) ) $this->setReview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['review'] ) : $arrValues['review'] );
		if( isset( $arrValues['review_url'] ) && $boolDirectSet ) $this->set( 'm_strReviewUrl', trim( stripcslashes( $arrValues['review_url'] ) ) ); elseif( isset( $arrValues['review_url'] ) ) $this->setReviewUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['review_url'] ) : $arrValues['review_url'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReviewDatetime', trim( $arrValues['review_datetime'] ) ); elseif( isset( $arrValues['review_datetime'] ) ) $this->setReviewDatetime( $arrValues['review_datetime'] );
		if( isset( $arrValues['overall_rating'] ) && $boolDirectSet ) $this->set( 'm_intOverallRating', trim( $arrValues['overall_rating'] ) ); elseif( isset( $arrValues['overall_rating'] ) ) $this->setOverallRating( $arrValues['overall_rating'] );
		if( isset( $arrValues['final_response'] ) && $boolDirectSet ) $this->set( 'm_strFinalResponse', trim( stripcslashes( $arrValues['final_response'] ) ) ); elseif( isset( $arrValues['final_response'] ) ) $this->setFinalResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['final_response'] ) : $arrValues['final_response'] );
		if( isset( $arrValues['is_recommended'] ) && $boolDirectSet ) $this->set( 'm_boolIsRecommended', trim( stripcslashes( $arrValues['is_recommended'] ) ) ); elseif( isset( $arrValues['is_recommended'] ) ) $this->setIsRecommended( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recommended'] ) : $arrValues['is_recommended'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['is_prospect_portal_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsProspectPortalPublished', trim( stripcslashes( $arrValues['is_prospect_portal_published'] ) ) ); elseif( isset( $arrValues['is_prospect_portal_published'] ) ) $this->setIsProspectPortalPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prospect_portal_published'] ) : $arrValues['is_prospect_portal_published'] );
		if( isset( $arrValues['is_responded_to'] ) && $boolDirectSet ) $this->set( 'm_boolIsRespondedTo', trim( stripcslashes( $arrValues['is_responded_to'] ) ) ); elseif( isset( $arrValues['is_responded_to'] ) ) $this->setIsRespondedTo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responded_to'] ) : $arrValues['is_responded_to'] );
		if( isset( $arrValues['has_attributes'] ) && $boolDirectSet ) $this->set( 'm_boolHasAttributes', trim( stripcslashes( $arrValues['has_attributes'] ) ) ); elseif( isset( $arrValues['has_attributes'] ) ) $this->setHasAttributes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_attributes'] ) : $arrValues['has_attributes'] );
		if( isset( $arrValues['is_completed'] ) && $boolDirectSet ) $this->set( 'm_boolIsCompleted', trim( stripcslashes( $arrValues['is_completed'] ) ) ); elseif( isset( $arrValues['is_completed'] ) ) $this->setIsCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_completed'] ) : $arrValues['is_completed'] );
		if( isset( $arrValues['is_lc_response_completed'] ) && $boolDirectSet ) $this->set( 'm_boolIsLcResponseCompleted', trim( stripcslashes( $arrValues['is_lc_response_completed'] ) ) ); elseif( isset( $arrValues['is_lc_response_completed'] ) ) $this->setIsLcResponseCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lc_response_completed'] ) : $arrValues['is_lc_response_completed'] );
		if( isset( $arrValues['is_response_approved'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponseApproved', trim( stripcslashes( $arrValues['is_response_approved'] ) ) ); elseif( isset( $arrValues['is_response_approved'] ) ) $this->setIsResponseApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_response_approved'] ) : $arrValues['is_response_approved'] );
		if( isset( $arrValues['is_notification_sent'] ) && $boolDirectSet ) $this->set( 'm_boolIsNotificationSent', trim( stripcslashes( $arrValues['is_notification_sent'] ) ) ); elseif( isset( $arrValues['is_notification_sent'] ) ) $this->setIsNotificationSent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_notification_sent'] ) : $arrValues['is_notification_sent'] );
		if( isset( $arrValues['hide_unpublished_on_dashboard'] ) && $boolDirectSet ) $this->set( 'm_boolHideUnpublishedOnDashboard', trim( stripcslashes( $arrValues['hide_unpublished_on_dashboard'] ) ) ); elseif( isset( $arrValues['hide_unpublished_on_dashboard'] ) ) $this->setHideUnpublishedOnDashboard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_unpublished_on_dashboard'] ) : $arrValues['hide_unpublished_on_dashboard'] );
		if( isset( $arrValues['hide_unresponded_on_dashboard'] ) && $boolDirectSet ) $this->set( 'm_boolHideUnrespondedOnDashboard', trim( stripcslashes( $arrValues['hide_unresponded_on_dashboard'] ) ) ); elseif( isset( $arrValues['hide_unresponded_on_dashboard'] ) ) $this->setHideUnrespondedOnDashboard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_unresponded_on_dashboard'] ) : $arrValues['hide_unresponded_on_dashboard'] );
		if( isset( $arrValues['hide_needs_attributes_on_dashboard'] ) && $boolDirectSet ) $this->set( 'm_boolHideNeedsAttributesOnDashboard', trim( stripcslashes( $arrValues['hide_needs_attributes_on_dashboard'] ) ) ); elseif( isset( $arrValues['hide_needs_attributes_on_dashboard'] ) ) $this->setHideNeedsAttributesOnDashboard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_needs_attributes_on_dashboard'] ) : $arrValues['hide_needs_attributes_on_dashboard'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['review_detail_status_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewDetailStatusId', trim( $arrValues['review_detail_status_id'] ) ); elseif( isset( $arrValues['review_detail_status_id'] ) ) $this->setReviewDetailStatusId( $arrValues['review_detail_status_id'] );
		if( isset( $arrValues['review_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewRecommendationTypeId', trim( $arrValues['review_recommendation_type_id'] ) ); elseif( isset( $arrValues['review_recommendation_type_id'] ) ) $this->setReviewRecommendationTypeId( $arrValues['review_recommendation_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setReviewId( $intReviewId ) {
		$this->set( 'm_intReviewId', CStrings::strToIntDef( $intReviewId, NULL, false ) );
	}

	public function getReviewId() {
		return $this->m_intReviewId;
	}

	public function sqlReviewId() {
		return ( true == isset( $this->m_intReviewId ) ) ? ( string ) $this->m_intReviewId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setReview( $strReview ) {
		$this->set( 'm_strReview', CStrings::strTrimDef( $strReview, -1, NULL, true ) );
	}

	public function getReview() {
		return $this->m_strReview;
	}

	public function sqlReview() {
		return ( true == isset( $this->m_strReview ) ) ? '\'' . addslashes( $this->m_strReview ) . '\'' : 'NULL';
	}

	public function setReviewUrl( $strReviewUrl ) {
		$this->set( 'm_strReviewUrl', CStrings::strTrimDef( $strReviewUrl, 240, NULL, true ) );
	}

	public function getReviewUrl() {
		return $this->m_strReviewUrl;
	}

	public function sqlReviewUrl() {
		return ( true == isset( $this->m_strReviewUrl ) ) ? '\'' . addslashes( $this->m_strReviewUrl ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setReviewDatetime( $strReviewDatetime ) {
		$this->set( 'm_strReviewDatetime', CStrings::strTrimDef( $strReviewDatetime, -1, NULL, true ) );
	}

	public function getReviewDatetime() {
		return $this->m_strReviewDatetime;
	}

	public function sqlReviewDatetime() {
		return ( true == isset( $this->m_strReviewDatetime ) ) ? '\'' . $this->m_strReviewDatetime . '\'' : 'NOW()';
	}

	public function setOverallRating( $intOverallRating ) {
		$this->set( 'm_intOverallRating', CStrings::strToIntDef( $intOverallRating, NULL, false ) );
	}

	public function getOverallRating() {
		return $this->m_intOverallRating;
	}

	public function sqlOverallRating() {
		return ( true == isset( $this->m_intOverallRating ) ) ? ( string ) $this->m_intOverallRating : 'NULL';
	}

	public function setFinalResponse( $strFinalResponse ) {
		$this->set( 'm_strFinalResponse', CStrings::strTrimDef( $strFinalResponse, -1, NULL, true ) );
	}

	public function getFinalResponse() {
		return $this->m_strFinalResponse;
	}

	public function sqlFinalResponse() {
		return ( true == isset( $this->m_strFinalResponse ) ) ? '\'' . addslashes( $this->m_strFinalResponse ) . '\'' : 'NULL';
	}

	public function setIsRecommended( $boolIsRecommended ) {
		$this->set( 'm_boolIsRecommended', CStrings::strToBool( $boolIsRecommended ) );
	}

	public function getIsRecommended() {
		return $this->m_boolIsRecommended;
	}

	public function sqlIsRecommended() {
		return ( true == isset( $this->m_boolIsRecommended ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRecommended ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, -1, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setIsProspectPortalPublished( $boolIsProspectPortalPublished ) {
		$this->set( 'm_boolIsProspectPortalPublished', CStrings::strToBool( $boolIsProspectPortalPublished ) );
	}

	public function getIsProspectPortalPublished() {
		return $this->m_boolIsProspectPortalPublished;
	}

	public function sqlIsProspectPortalPublished() {
		return ( true == isset( $this->m_boolIsProspectPortalPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProspectPortalPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRespondedTo( $boolIsRespondedTo ) {
		$this->set( 'm_boolIsRespondedTo', CStrings::strToBool( $boolIsRespondedTo ) );
	}

	public function getIsRespondedTo() {
		return $this->m_boolIsRespondedTo;
	}

	public function sqlIsRespondedTo() {
		return ( true == isset( $this->m_boolIsRespondedTo ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRespondedTo ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasAttributes( $boolHasAttributes ) {
		$this->set( 'm_boolHasAttributes', CStrings::strToBool( $boolHasAttributes ) );
	}

	public function getHasAttributes() {
		return $this->m_boolHasAttributes;
	}

	public function sqlHasAttributes() {
		return ( true == isset( $this->m_boolHasAttributes ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasAttributes ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCompleted( $boolIsCompleted ) {
		$this->set( 'm_boolIsCompleted', CStrings::strToBool( $boolIsCompleted ) );
	}

	public function getIsCompleted() {
		return $this->m_boolIsCompleted;
	}

	public function sqlIsCompleted() {
		return ( true == isset( $this->m_boolIsCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLcResponseCompleted( $boolIsLcResponseCompleted ) {
		$this->set( 'm_boolIsLcResponseCompleted', CStrings::strToBool( $boolIsLcResponseCompleted ) );
	}

	public function getIsLcResponseCompleted() {
		return $this->m_boolIsLcResponseCompleted;
	}

	public function sqlIsLcResponseCompleted() {
		return ( true == isset( $this->m_boolIsLcResponseCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLcResponseCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponseApproved( $boolIsResponseApproved ) {
		$this->set( 'm_boolIsResponseApproved', CStrings::strToBool( $boolIsResponseApproved ) );
	}

	public function getIsResponseApproved() {
		return $this->m_boolIsResponseApproved;
	}

	public function sqlIsResponseApproved() {
		return ( true == isset( $this->m_boolIsResponseApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponseApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNotificationSent( $boolIsNotificationSent ) {
		$this->set( 'm_boolIsNotificationSent', CStrings::strToBool( $boolIsNotificationSent ) );
	}

	public function getIsNotificationSent() {
		return $this->m_boolIsNotificationSent;
	}

	public function sqlIsNotificationSent() {
		return ( true == isset( $this->m_boolIsNotificationSent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNotificationSent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideUnpublishedOnDashboard( $boolHideUnpublishedOnDashboard ) {
		$this->set( 'm_boolHideUnpublishedOnDashboard', CStrings::strToBool( $boolHideUnpublishedOnDashboard ) );
	}

	public function getHideUnpublishedOnDashboard() {
		return $this->m_boolHideUnpublishedOnDashboard;
	}

	public function sqlHideUnpublishedOnDashboard() {
		return ( true == isset( $this->m_boolHideUnpublishedOnDashboard ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideUnpublishedOnDashboard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideUnrespondedOnDashboard( $boolHideUnrespondedOnDashboard ) {
		$this->set( 'm_boolHideUnrespondedOnDashboard', CStrings::strToBool( $boolHideUnrespondedOnDashboard ) );
	}

	public function getHideUnrespondedOnDashboard() {
		return $this->m_boolHideUnrespondedOnDashboard;
	}

	public function sqlHideUnrespondedOnDashboard() {
		return ( true == isset( $this->m_boolHideUnrespondedOnDashboard ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideUnrespondedOnDashboard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideNeedsAttributesOnDashboard( $boolHideNeedsAttributesOnDashboard ) {
		$this->set( 'm_boolHideNeedsAttributesOnDashboard', CStrings::strToBool( $boolHideNeedsAttributesOnDashboard ) );
	}

	public function getHideNeedsAttributesOnDashboard() {
		return $this->m_boolHideNeedsAttributesOnDashboard;
	}

	public function sqlHideNeedsAttributesOnDashboard() {
		return ( true == isset( $this->m_boolHideNeedsAttributesOnDashboard ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideNeedsAttributesOnDashboard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReviewDetailStatusId( $intReviewDetailStatusId ) {
		$this->set( 'm_intReviewDetailStatusId', CStrings::strToIntDef( $intReviewDetailStatusId, NULL, false ) );
	}

	public function getReviewDetailStatusId() {
		return $this->m_intReviewDetailStatusId;
	}

	public function sqlReviewDetailStatusId() {
		return ( true == isset( $this->m_intReviewDetailStatusId ) ) ? ( string ) $this->m_intReviewDetailStatusId : '1';
	}

	public function setReviewRecommendationTypeId( $intReviewRecommendationTypeId ) {
		$this->set( 'm_intReviewRecommendationTypeId', CStrings::strToIntDef( $intReviewRecommendationTypeId, NULL, false ) );
	}

	public function getReviewRecommendationTypeId() {
		return $this->m_intReviewRecommendationTypeId;
	}

	public function sqlReviewRecommendationTypeId() {
		return ( true == isset( $this->m_intReviewRecommendationTypeId ) ) ? ( string ) $this->m_intReviewRecommendationTypeId : '3';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, review_id, name, title, review, review_url, email_address, review_datetime, overall_rating, final_response, is_recommended, ip_address, is_prospect_portal_published, is_responded_to, has_attributes, is_completed, is_lc_response_completed, is_response_approved, is_notification_sent, hide_unpublished_on_dashboard, hide_unresponded_on_dashboard, hide_needs_attributes_on_dashboard, created_by, created_on, review_detail_status_id, review_recommendation_type_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReviewId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlReview() . ', ' .
						$this->sqlReviewUrl() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlReviewDatetime() . ', ' .
						$this->sqlOverallRating() . ', ' .
						$this->sqlFinalResponse() . ', ' .
						$this->sqlIsRecommended() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlIsProspectPortalPublished() . ', ' .
						$this->sqlIsRespondedTo() . ', ' .
						$this->sqlHasAttributes() . ', ' .
						$this->sqlIsCompleted() . ', ' .
						$this->sqlIsLcResponseCompleted() . ', ' .
						$this->sqlIsResponseApproved() . ', ' .
						$this->sqlIsNotificationSent() . ', ' .
						$this->sqlHideUnpublishedOnDashboard() . ', ' .
						$this->sqlHideUnrespondedOnDashboard() . ', ' .
						$this->sqlHideNeedsAttributesOnDashboard() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlReviewDetailStatusId() . ', ' .
						$this->sqlReviewRecommendationTypeId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_id = ' . $this->sqlReviewId(). ',' ; } elseif( true == array_key_exists( 'ReviewId', $this->getChangedColumns() ) ) { $strSql .= ' review_id = ' . $this->sqlReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review = ' . $this->sqlReview(). ',' ; } elseif( true == array_key_exists( 'Review', $this->getChangedColumns() ) ) { $strSql .= ' review = ' . $this->sqlReview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_url = ' . $this->sqlReviewUrl(). ',' ; } elseif( true == array_key_exists( 'ReviewUrl', $this->getChangedColumns() ) ) { $strSql .= ' review_url = ' . $this->sqlReviewUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_datetime = ' . $this->sqlReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'ReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' review_datetime = ' . $this->sqlReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' overall_rating = ' . $this->sqlOverallRating(). ',' ; } elseif( true == array_key_exists( 'OverallRating', $this->getChangedColumns() ) ) { $strSql .= ' overall_rating = ' . $this->sqlOverallRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' final_response = ' . $this->sqlFinalResponse(). ',' ; } elseif( true == array_key_exists( 'FinalResponse', $this->getChangedColumns() ) ) { $strSql .= ' final_response = ' . $this->sqlFinalResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recommended = ' . $this->sqlIsRecommended(). ',' ; } elseif( true == array_key_exists( 'IsRecommended', $this->getChangedColumns() ) ) { $strSql .= ' is_recommended = ' . $this->sqlIsRecommended() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_prospect_portal_published = ' . $this->sqlIsProspectPortalPublished(). ',' ; } elseif( true == array_key_exists( 'IsProspectPortalPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_prospect_portal_published = ' . $this->sqlIsProspectPortalPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responded_to = ' . $this->sqlIsRespondedTo(). ',' ; } elseif( true == array_key_exists( 'IsRespondedTo', $this->getChangedColumns() ) ) { $strSql .= ' is_responded_to = ' . $this->sqlIsRespondedTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_attributes = ' . $this->sqlHasAttributes(). ',' ; } elseif( true == array_key_exists( 'HasAttributes', $this->getChangedColumns() ) ) { $strSql .= ' has_attributes = ' . $this->sqlHasAttributes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted(). ',' ; } elseif( true == array_key_exists( 'IsCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_completed = ' . $this->sqlIsCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lc_response_completed = ' . $this->sqlIsLcResponseCompleted(). ',' ; } elseif( true == array_key_exists( 'IsLcResponseCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_lc_response_completed = ' . $this->sqlIsLcResponseCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_response_approved = ' . $this->sqlIsResponseApproved(). ',' ; } elseif( true == array_key_exists( 'IsResponseApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_response_approved = ' . $this->sqlIsResponseApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_notification_sent = ' . $this->sqlIsNotificationSent(). ',' ; } elseif( true == array_key_exists( 'IsNotificationSent', $this->getChangedColumns() ) ) { $strSql .= ' is_notification_sent = ' . $this->sqlIsNotificationSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_unpublished_on_dashboard = ' . $this->sqlHideUnpublishedOnDashboard(). ',' ; } elseif( true == array_key_exists( 'HideUnpublishedOnDashboard', $this->getChangedColumns() ) ) { $strSql .= ' hide_unpublished_on_dashboard = ' . $this->sqlHideUnpublishedOnDashboard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_unresponded_on_dashboard = ' . $this->sqlHideUnrespondedOnDashboard(). ',' ; } elseif( true == array_key_exists( 'HideUnrespondedOnDashboard', $this->getChangedColumns() ) ) { $strSql .= ' hide_unresponded_on_dashboard = ' . $this->sqlHideUnrespondedOnDashboard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_needs_attributes_on_dashboard = ' . $this->sqlHideNeedsAttributesOnDashboard(). ',' ; } elseif( true == array_key_exists( 'HideNeedsAttributesOnDashboard', $this->getChangedColumns() ) ) { $strSql .= ' hide_needs_attributes_on_dashboard = ' . $this->sqlHideNeedsAttributesOnDashboard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_detail_status_id = ' . $this->sqlReviewDetailStatusId(). ',' ; } elseif( true == array_key_exists( 'ReviewDetailStatusId', $this->getChangedColumns() ) ) { $strSql .= ' review_detail_status_id = ' . $this->sqlReviewDetailStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_recommendation_type_id = ' . $this->sqlReviewRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ReviewRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' review_recommendation_type_id = ' . $this->sqlReviewRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'review_id' => $this->getReviewId(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'review' => $this->getReview(),
			'review_url' => $this->getReviewUrl(),
			'email_address' => $this->getEmailAddress(),
			'review_datetime' => $this->getReviewDatetime(),
			'overall_rating' => $this->getOverallRating(),
			'final_response' => $this->getFinalResponse(),
			'is_recommended' => $this->getIsRecommended(),
			'ip_address' => $this->getIpAddress(),
			'is_prospect_portal_published' => $this->getIsProspectPortalPublished(),
			'is_responded_to' => $this->getIsRespondedTo(),
			'has_attributes' => $this->getHasAttributes(),
			'is_completed' => $this->getIsCompleted(),
			'is_lc_response_completed' => $this->getIsLcResponseCompleted(),
			'is_response_approved' => $this->getIsResponseApproved(),
			'is_notification_sent' => $this->getIsNotificationSent(),
			'hide_unpublished_on_dashboard' => $this->getHideUnpublishedOnDashboard(),
			'hide_unresponded_on_dashboard' => $this->getHideUnrespondedOnDashboard(),
			'hide_needs_attributes_on_dashboard' => $this->getHideNeedsAttributesOnDashboard(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'review_detail_status_id' => $this->getReviewDetailStatusId(),
			'review_recommendation_type_id' => $this->getReviewRecommendationTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>