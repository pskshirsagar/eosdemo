<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantIncomes extends CEosPluralBase {

    public static function fetchApplicantIncomes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CApplicantIncome', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchApplicantIncome( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApplicantIncome', $objDatabase );
    }

    public static function fetchApplicantIncomeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'applicant_incomes', $objDatabase );
    }

    public static function fetchApplicantIncomeByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchApplicantIncome( sprintf( 'SELECT * FROM applicant_incomes WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantIncomesByCid( $intCid, $objDatabase ) {
        return self::fetchApplicantIncomes( sprintf( 'SELECT * FROM applicant_incomes WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantIncomesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
        return self::fetchApplicantIncomes( sprintf( 'SELECT * FROM applicant_incomes WHERE applicant_id = %d AND cid = %d', (int) $intApplicantId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantIncomesByIncomeTypeIdByCid( $intIncomeTypeId, $intCid, $objDatabase ) {
        return self::fetchApplicantIncomes( sprintf( 'SELECT * FROM applicant_incomes WHERE income_type_id = %d AND cid = %d', (int) $intIncomeTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicantIncomesByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
        return self::fetchApplicantIncomes( sprintf( 'SELECT * FROM applicant_incomes WHERE frequency_id = %d AND cid = %d', (int) $intFrequencyId, (int) $intCid ), $objDatabase );
    }

}
?>