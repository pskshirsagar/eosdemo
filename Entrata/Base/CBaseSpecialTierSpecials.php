<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpecialTierSpecials
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpecialTierSpecials extends CEosPluralBase {

	/**
	 * @return CSpecialTierSpecial[]
	 */
	public static function fetchSpecialTierSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSpecialTierSpecial', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSpecialTierSpecial
	 */
	public static function fetchSpecialTierSpecial( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSpecialTierSpecial', $objDatabase );
	}

	public static function fetchSpecialTierSpecialCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'special_tier_specials', $objDatabase );
	}

	public static function fetchSpecialTierSpecialByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSpecialTierSpecial( sprintf( 'SELECT * FROM special_tier_specials WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialTierSpecialsByCid( $intCid, $objDatabase ) {
		return self::fetchSpecialTierSpecials( sprintf( 'SELECT * FROM special_tier_specials WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialTierSpecialsByTierSpecialIdByCid( $intTierSpecialId, $intCid, $objDatabase ) {
		return self::fetchSpecialTierSpecials( sprintf( 'SELECT * FROM special_tier_specials WHERE tier_special_id = %d AND cid = %d', ( int ) $intTierSpecialId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialTierSpecialsBySpecialIdByCid( $intSpecialId, $intCid, $objDatabase ) {
		return self::fetchSpecialTierSpecials( sprintf( 'SELECT * FROM special_tier_specials WHERE special_id = %d AND cid = %d', ( int ) $intSpecialId, ( int ) $intCid ), $objDatabase );
	}

}
?>