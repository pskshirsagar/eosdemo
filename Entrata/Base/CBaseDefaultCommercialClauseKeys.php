<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCommercialClauseKeys
 * Do not add any new functions to this class.
 */

class CBaseDefaultCommercialClauseKeys extends CEosPluralBase {

	/**
	 * @return CDefaultCommercialClauseKey[]
	 */
	public static function fetchDefaultCommercialClauseKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCommercialClauseKey::class, $objDatabase );
	}

	/**
	 * @return CDefaultCommercialClauseKey
	 */
	public static function fetchDefaultCommercialClauseKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCommercialClauseKey::class, $objDatabase );
	}

	public static function fetchDefaultCommercialClauseKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_commercial_clause_keys', $objDatabase );
	}

	public static function fetchDefaultCommercialClauseKeyById( $intId, $objDatabase ) {
		return self::fetchDefaultCommercialClauseKey( sprintf( 'SELECT * FROM default_commercial_clause_keys WHERE id = %d', $intId ), $objDatabase );
	}

}
?>