<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVoiceTypes
 * Do not add any new functions to this class.
 */

class CBaseVoiceTypes extends CEosPluralBase {

	/**
	 * @return CVoiceType[]
	 */
	public static function fetchVoiceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CVoiceType::class, $objDatabase );
	}

	/**
	 * @return CVoiceType
	 */
	public static function fetchVoiceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CVoiceType::class, $objDatabase );
	}

	public static function fetchVoiceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'voice_types', $objDatabase );
	}

	public static function fetchVoiceTypeById( $intId, $objDatabase ) {
		return self::fetchVoiceType( sprintf( 'SELECT * FROM voice_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>