<?php

class CBaseDefaultMaintenanceStatusTypes extends CEosPluralBase {

    const TABLE_DEFAULT_MAINTENANCE_STATUS_TYPES = 'public.default_maintenance_status_types';

    public static function fetchDefaultMaintenanceStatusTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CDefaultMaintenanceStatusType', $objDatabase );
    }

    public static function fetchDefaultMaintenanceStatusType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CDefaultMaintenanceStatusType', $objDatabase );
    }

    public static function fetchDefaultMaintenanceStatusTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'default_maintenance_status_types', $objDatabase );
    }

    public static function fetchDefaultMaintenanceStatusTypeById( $intId, $objDatabase ) {
        return self::fetchDefaultMaintenanceStatusType( sprintf( 'SELECT * FROM default_maintenance_status_types WHERE id = %d', (int) $intId ), $objDatabase );
    }
}
?>