<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePeriods extends CEosPluralBase {

	/**
	 * @return CPeriod[]
	 */
	public static function fetchPeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPeriod
	 */
	public static function fetchPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPeriod', $objDatabase );
	}

	public static function fetchPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'periods', $objDatabase );
	}

	public static function fetchPeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPeriod( sprintf( 'SELECT * FROM periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchPeriods( sprintf( 'SELECT * FROM periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPeriods( sprintf( 'SELECT * FROM periods WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>