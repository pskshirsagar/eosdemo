<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerBadges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerBadges extends CEosPluralBase {

	/**
	 * @return CCustomerBadge[]
	 */
	public static function fetchCustomerBadges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerBadge', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerBadge
	 */
	public static function fetchCustomerBadge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerBadge', $objDatabase );
	}

	public static function fetchCustomerBadgeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_badges', $objDatabase );
	}

	public static function fetchCustomerBadgeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerBadge( sprintf( 'SELECT * FROM customer_badges WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerBadgesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerBadges( sprintf( 'SELECT * FROM customer_badges WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerBadgesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerBadges( sprintf( 'SELECT * FROM customer_badges WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerBadgesByCompanyBadgeIdByCid( $intCompanyBadgeId, $intCid, $objDatabase ) {
		return self::fetchCustomerBadges( sprintf( 'SELECT * FROM customer_badges WHERE company_badge_id = %d AND cid = %d', ( int ) $intCompanyBadgeId, ( int ) $intCid ), $objDatabase );
	}

}
?>