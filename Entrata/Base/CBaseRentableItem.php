<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItem extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intUnitSpaceId;
    protected $m_intRentableItemOptionId;
    protected $m_intUnitSpaceStatusTypeId;
    protected $m_strRemotePrimaryKey;
    protected $m_strLookupCode;
    protected $m_strName;
    protected $m_strDescription;
    protected $m_intXPos;
    protected $m_intYPos;
    protected $m_strAvailableOn;
    protected $m_strHoldUntilDate;
    protected $m_intIsPublished;
    protected $m_intOrderNum;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intIsPublished = '1';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->m_intUnitSpaceId = trim( $arrValues['unit_space_id'] ); else if( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
        if( isset( $arrValues['rentable_item_option_id'] ) && $boolDirectSet ) $this->m_intRentableItemOptionId = trim( $arrValues['rentable_item_option_id'] ); else if( isset( $arrValues['rentable_item_option_id'] ) ) $this->setRentableItemOptionId( $arrValues['rentable_item_option_id'] );
        if( isset( $arrValues['unit_space_status_type_id'] ) && $boolDirectSet ) $this->m_intUnitSpaceStatusTypeId = trim( $arrValues['unit_space_status_type_id'] ); else if( isset( $arrValues['unit_space_status_type_id'] ) ) $this->setUnitSpaceStatusTypeId( $arrValues['unit_space_status_type_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->m_strRemotePrimaryKey = trim( stripcslashes( $arrValues['remote_primary_key'] ) ); else if( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
        if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->m_strLookupCode = trim( stripcslashes( $arrValues['lookup_code'] ) ); else if( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['x_pos'] ) && $boolDirectSet ) $this->m_intXPos = trim( $arrValues['x_pos'] ); else if( isset( $arrValues['x_pos'] ) ) $this->setXPos( $arrValues['x_pos'] );
        if( isset( $arrValues['y_pos'] ) && $boolDirectSet ) $this->m_intYPos = trim( $arrValues['y_pos'] ); else if( isset( $arrValues['y_pos'] ) ) $this->setYPos( $arrValues['y_pos'] );
        if( isset( $arrValues['available_on'] ) && $boolDirectSet ) $this->m_strAvailableOn = trim( $arrValues['available_on'] ); else if( isset( $arrValues['available_on'] ) ) $this->setAvailableOn( $arrValues['available_on'] );
        if( isset( $arrValues['hold_until_date'] ) && $boolDirectSet ) $this->m_strHoldUntilDate = trim( $arrValues['hold_until_date'] ); else if( isset( $arrValues['hold_until_date'] ) ) $this->setHoldUntilDate( $arrValues['hold_until_date'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setUnitSpaceId( $intUnitSpaceId ) {
        $this->m_intUnitSpaceId = CStrings::strToIntDef( $intUnitSpaceId, NULL, false );
    }

    public function getUnitSpaceId() {
        return $this->m_intUnitSpaceId;
    }

    public function sqlUnitSpaceId() {
        return ( true == isset( $this->m_intUnitSpaceId ) ) ? (string) $this->m_intUnitSpaceId : 'NULL';
    }

    public function setRentableItemOptionId( $intRentableItemOptionId ) {
        $this->m_intRentableItemOptionId = CStrings::strToIntDef( $intRentableItemOptionId, NULL, false );
    }

    public function getRentableItemOptionId() {
        return $this->m_intRentableItemOptionId;
    }

    public function sqlRentableItemOptionId() {
        return ( true == isset( $this->m_intRentableItemOptionId ) ) ? (string) $this->m_intRentableItemOptionId : 'NULL';
    }

    public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
        $this->m_intUnitSpaceStatusTypeId = CStrings::strToIntDef( $intUnitSpaceStatusTypeId, NULL, false );
    }

    public function getUnitSpaceStatusTypeId() {
        return $this->m_intUnitSpaceStatusTypeId;
    }

    public function sqlUnitSpaceStatusTypeId() {
        return ( true == isset( $this->m_intUnitSpaceStatusTypeId ) ) ? (string) $this->m_intUnitSpaceStatusTypeId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->m_strRemotePrimaryKey = CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
    }

    public function setLookupCode( $strLookupCode ) {
        $this->m_strLookupCode = CStrings::strTrimDef( $strLookupCode, 64, NULL, true );
    }

    public function getLookupCode() {
        return $this->m_strLookupCode;
    }

    public function sqlLookupCode() {
        return ( true == isset( $this->m_strLookupCode ) ) ? '\'' . addslashes( $this->m_strLookupCode ) . '\'' : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setXPos( $intXPos ) {
        $this->m_intXPos = CStrings::strToIntDef( $intXPos, NULL, false );
    }

    public function getXPos() {
        return $this->m_intXPos;
    }

    public function sqlXPos() {
        return ( true == isset( $this->m_intXPos ) ) ? (string) $this->m_intXPos : 'NULL';
    }

    public function setYPos( $intYPos ) {
        $this->m_intYPos = CStrings::strToIntDef( $intYPos, NULL, false );
    }

    public function getYPos() {
        return $this->m_intYPos;
    }

    public function sqlYPos() {
        return ( true == isset( $this->m_intYPos ) ) ? (string) $this->m_intYPos : 'NULL';
    }

    public function setAvailableOn( $strAvailableOn ) {
        $this->m_strAvailableOn = CStrings::strTrimDef( $strAvailableOn, -1, NULL, true );
    }

    public function getAvailableOn() {
        return $this->m_strAvailableOn;
    }

    public function sqlAvailableOn() {
        return ( true == isset( $this->m_strAvailableOn ) ) ? '\'' . $this->m_strAvailableOn . '\'' : 'NULL';
    }

    public function setHoldUntilDate( $strHoldUntilDate ) {
        $this->m_strHoldUntilDate = CStrings::strTrimDef( $strHoldUntilDate, -1, NULL, true );
    }

    public function getHoldUntilDate() {
        return $this->m_strHoldUntilDate;
    }

    public function sqlHoldUntilDate() {
        return ( true == isset( $this->m_strHoldUntilDate ) ) ? '\'' . $this->m_strHoldUntilDate . '\'' : 'NULL';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return ( true == isset( $this->m_intDeletedBy ) ) ? (string) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.rentable_items_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.rentable_items					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlUnitSpaceId() . ', ' . 		                $this->sqlRentableItemOptionId() . ', ' . 		                $this->sqlUnitSpaceStatusTypeId() . ', ' . 		                $this->sqlRemotePrimaryKey() . ', ' . 		                $this->sqlLookupCode() . ', ' . 		                $this->sqlName() . ', ' . 		                $this->sqlDescription() . ', ' . 		                $this->sqlXPos() . ', ' . 		                $this->sqlYPos() . ', ' . 		                $this->sqlAvailableOn() . ', ' . 		                $this->sqlHoldUntilDate() . ', ' . 		                $this->sqlIsPublished() . ', ' . 		                $this->sqlOrderNum() . ', ' . 		                $this->sqlDeletedBy() . ', ' . 		                $this->sqlDeletedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.rentable_items
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUnitSpaceId() ) != $this->getOriginalValueByFieldName ( 'unit_space_id' ) ) { $arrstrOriginalValueChanges['unit_space_id'] = $this->sqlUnitSpaceId(); $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_item_option_id = ' . $this->sqlRentableItemOptionId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRentableItemOptionId() ) != $this->getOriginalValueByFieldName ( 'rentable_item_option_id' ) ) { $arrstrOriginalValueChanges['rentable_item_option_id'] = $this->sqlRentableItemOptionId(); $strSql .= ' rentable_item_option_id = ' . $this->sqlRentableItemOptionId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUnitSpaceStatusTypeId() ) != $this->getOriginalValueByFieldName ( 'unit_space_status_type_id' ) ) { $arrstrOriginalValueChanges['unit_space_status_type_id'] = $this->sqlUnitSpaceStatusTypeId(); $strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRemotePrimaryKey() ) != $this->getOriginalValueByFieldName ( 'remote_primary_key' ) ) { $arrstrOriginalValueChanges['remote_primary_key'] = $this->sqlRemotePrimaryKey(); $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLookupCode() ) != $this->getOriginalValueByFieldName ( 'lookup_code' ) ) { $arrstrOriginalValueChanges['lookup_code'] = $this->sqlLookupCode(); $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName ( 'description' ) ) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x_pos = ' . $this->sqlXPos() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlXPos() ) != $this->getOriginalValueByFieldName ( 'x_pos' ) ) { $arrstrOriginalValueChanges['x_pos'] = $this->sqlXPos(); $strSql .= ' x_pos = ' . $this->sqlXPos() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' y_pos = ' . $this->sqlYPos() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlYPos() ) != $this->getOriginalValueByFieldName ( 'y_pos' ) ) { $arrstrOriginalValueChanges['y_pos'] = $this->sqlYPos(); $strSql .= ' y_pos = ' . $this->sqlYPos() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAvailableOn() ) != $this->getOriginalValueByFieldName ( 'available_on' ) ) { $arrstrOriginalValueChanges['available_on'] = $this->sqlAvailableOn(); $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hold_until_date = ' . $this->sqlHoldUntilDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHoldUntilDate() ) != $this->getOriginalValueByFieldName ( 'hold_until_date' ) ) { $arrstrOriginalValueChanges['hold_until_date'] = $this->sqlHoldUntilDate(); $strSql .= ' hold_until_date = ' . $this->sqlHoldUntilDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName ( 'order_num' ) ) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName ( 'deleted_by' ) ) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName ( 'deleted_on' ) ) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.rentable_items WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.rentable_items_id_seq', $objDatabase );
    }

}
?>