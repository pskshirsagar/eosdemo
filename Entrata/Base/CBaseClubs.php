<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClubs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClubs extends CEosPluralBase {

	/**
	 * @return CClub[]
	 */
	public static function fetchClubs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CClub::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CClub
	 */
	public static function fetchClub( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClub::class, $objDatabase );
	}

	public static function fetchClubCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clubs', $objDatabase );
	}

	public static function fetchClubByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchClub( sprintf( 'SELECT * FROM clubs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchClubsByCid( $intCid, $objDatabase ) {
		return self::fetchClubs( sprintf( 'SELECT * FROM clubs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchClubsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchClubs( sprintf( 'SELECT * FROM clubs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchClubsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchClubs( sprintf( 'SELECT * FROM clubs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchClubsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchClubs( sprintf( 'SELECT * FROM clubs WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

}
?>