<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAssets extends CEosPluralBase {

	/**
	 * @return CCustomerAsset[]
	 */
	public static function fetchCustomerAssets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerAsset::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAsset
	 */
	public static function fetchCustomerAsset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAsset::class, $objDatabase );
	}

	public static function fetchCustomerAssetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_assets', $objDatabase );
	}

	public static function fetchCustomerAssetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAsset( sprintf( 'SELECT * FROM customer_assets WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAssets( sprintf( 'SELECT * FROM customer_assets WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssets( sprintf( 'SELECT * FROM customer_assets WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssets( sprintf( 'SELECT * FROM customer_assets WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerAssetsByCustomerAssetTypeIdByCid( $intCustomerAssetTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAssets( sprintf( 'SELECT * FROM customer_assets WHERE customer_asset_type_id = %d AND cid = %d', $intCustomerAssetTypeId, $intCid ), $objDatabase );
	}

}
?>