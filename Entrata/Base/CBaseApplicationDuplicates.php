<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationDuplicates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationDuplicates extends CEosPluralBase {

	/**
	 * @return CApplicationDuplicate[]
	 */
	public static function fetchApplicationDuplicates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationDuplicate', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationDuplicate
	 */
	public static function fetchApplicationDuplicate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationDuplicate', $objDatabase );
	}

	public static function fetchApplicationDuplicateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_duplicates', $objDatabase );
	}

	public static function fetchApplicationDuplicateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicate( sprintf( 'SELECT * FROM application_duplicates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByLeadSourceIdByCid( $intLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE lead_source_id = %d AND cid = %d', ( int ) $intLeadSourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByLeaseIntervalTypeIdByCid( $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE lease_interval_type_id = %d AND cid = %d', ( int ) $intLeaseIntervalTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesBySemAdGroupIdByCid( $intSemAdGroupId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE sem_ad_group_id = %d AND cid = %d', ( int ) $intSemAdGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesBySemKeywordIdByCid( $intSemKeywordId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE sem_keyword_id = %d AND cid = %d', ( int ) $intSemKeywordId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesBySemSourceIdByCid( $intSemSourceId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE sem_source_id = %d AND cid = %d', ( int ) $intSemSourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByCraigslistPostIdByCid( $intCraigslistPostId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE craigslist_post_id = %d AND cid = %d', ( int ) $intCraigslistPostId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationDuplicatesByTrafficCookieIdByCid( $intTrafficCookieId, $intCid, $objDatabase ) {
		return self::fetchApplicationDuplicates( sprintf( 'SELECT * FROM application_duplicates WHERE traffic_cookie_id = %d AND cid = %d', ( int ) $intTrafficCookieId, ( int ) $intCid ), $objDatabase );
	}

}
?>