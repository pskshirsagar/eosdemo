<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileNotes extends CEosPluralBase {

	/**
	 * @return CFileNote[]
	 */
	public static function fetchFileNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileNote::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileNote
	 */
	public static function fetchFileNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileNote::class, $objDatabase );
	}

	public static function fetchFileNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_notes', $objDatabase );
	}

	public static function fetchFileNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileNote( sprintf( 'SELECT * FROM file_notes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileNotesByCid( $intCid, $objDatabase ) {
		return self::fetchFileNotes( sprintf( 'SELECT * FROM file_notes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFileNotesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileNotes( sprintf( 'SELECT * FROM file_notes WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

}
?>