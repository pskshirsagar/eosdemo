<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMessageBatchDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMessageBatchDetails extends CEosPluralBase {

	/**
	 * @return CMessageBatchDetail[]
	 */
	public static function fetchMessageBatchDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMessageBatchDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMessageBatchDetail
	 */
	public static function fetchMessageBatchDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageBatchDetail::class, $objDatabase );
	}

	public static function fetchMessageBatchDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_batch_details', $objDatabase );
	}

	public static function fetchMessageBatchDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetail( sprintf( 'SELECT * FROM message_batch_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetails( sprintf( 'SELECT * FROM message_batch_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetails( sprintf( 'SELECT * FROM message_batch_details WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchDetailsByMessageBatchIdByCid( $intMessageBatchId, $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetails( sprintf( 'SELECT * FROM message_batch_details WHERE message_batch_id = %d AND cid = %d', $intMessageBatchId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchDetailsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetails( sprintf( 'SELECT * FROM message_batch_details WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchDetailsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchMessageBatchDetails( sprintf( 'SELECT * FROM message_batch_details WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

}
?>