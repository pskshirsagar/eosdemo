<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTags
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileTags extends CEosPluralBase {

	/**
	 * @return CFileTag[]
	 */
	public static function fetchFileTags( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileTag::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileTag
	 */
	public static function fetchFileTag( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileTag::class, $objDatabase );
	}

	public static function fetchFileTagCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_tags', $objDatabase );
	}

	public static function fetchFileTagByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileTag( sprintf( 'SELECT * FROM file_tags WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileTagsByCid( $intCid, $objDatabase ) {
		return self::fetchFileTags( sprintf( 'SELECT * FROM file_tags WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>