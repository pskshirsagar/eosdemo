<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationDuplicate extends CEosSingularBase {

	const TABLE_NAME = 'public.application_duplicates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyEmployeeId;
	protected $m_intApplicantApplicationId;
	protected $m_intLeadSourceId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intPsProductId;
	protected $m_intSemAdGroupId;
	protected $m_intSemKeywordId;
	protected $m_intSemSourceId;
	protected $m_intCraigslistPostId;
	protected $m_intTrafficCookieId;
	protected $m_strRemotePrimaryKey;
	protected $m_strNotes;
	protected $m_strContactDatetime;
	protected $m_fltDesiredRentMin;
	protected $m_fltDesiredRentMax;
	protected $m_intTermMonth;
	protected $m_strLeaseStartDate;
	protected $m_intDesiredBedrooms;
	protected $m_intDesiredBathrooms;
	protected $m_strPhoneNumber;
	protected $m_intIsWebInitiated;
	protected $m_intIsTextViewed;
	protected $m_intIsSendText;
	protected $m_intContactAcceptedBy;
	protected $m_strContactAcceptedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsWebInitiated = '0';
		$this->m_intIsTextViewed = '0';
		$this->m_intIsSendText = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['craigslist_post_id'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistPostId', trim( $arrValues['craigslist_post_id'] ) ); elseif( isset( $arrValues['craigslist_post_id'] ) ) $this->setCraigslistPostId( $arrValues['craigslist_post_id'] );
		if( isset( $arrValues['traffic_cookie_id'] ) && $boolDirectSet ) $this->set( 'm_intTrafficCookieId', trim( $arrValues['traffic_cookie_id'] ) ); elseif( isset( $arrValues['traffic_cookie_id'] ) ) $this->setTrafficCookieId( $arrValues['traffic_cookie_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['contact_datetime'] ) && $boolDirectSet ) $this->set( 'm_strContactDatetime', trim( $arrValues['contact_datetime'] ) ); elseif( isset( $arrValues['contact_datetime'] ) ) $this->setContactDatetime( $arrValues['contact_datetime'] );
		if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMin', trim( $arrValues['desired_rent_min'] ) ); elseif( isset( $arrValues['desired_rent_min'] ) ) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
		if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->set( 'm_fltDesiredRentMax', trim( $arrValues['desired_rent_max'] ) ); elseif( isset( $arrValues['desired_rent_max'] ) ) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
		if( isset( $arrValues['term_month'] ) && $boolDirectSet ) $this->set( 'm_intTermMonth', trim( $arrValues['term_month'] ) ); elseif( isset( $arrValues['term_month'] ) ) $this->setTermMonth( $arrValues['term_month'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intDesiredBedrooms', trim( $arrValues['desired_bedrooms'] ) ); elseif( isset( $arrValues['desired_bedrooms'] ) ) $this->setDesiredBedrooms( $arrValues['desired_bedrooms'] );
		if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_intDesiredBathrooms', trim( $arrValues['desired_bathrooms'] ) ); elseif( isset( $arrValues['desired_bathrooms'] ) ) $this->setDesiredBathrooms( $arrValues['desired_bathrooms'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['is_web_initiated'] ) && $boolDirectSet ) $this->set( 'm_intIsWebInitiated', trim( $arrValues['is_web_initiated'] ) ); elseif( isset( $arrValues['is_web_initiated'] ) ) $this->setIsWebInitiated( $arrValues['is_web_initiated'] );
		if( isset( $arrValues['is_text_viewed'] ) && $boolDirectSet ) $this->set( 'm_intIsTextViewed', trim( $arrValues['is_text_viewed'] ) ); elseif( isset( $arrValues['is_text_viewed'] ) ) $this->setIsTextViewed( $arrValues['is_text_viewed'] );
		if( isset( $arrValues['is_send_text'] ) && $boolDirectSet ) $this->set( 'm_intIsSendText', trim( $arrValues['is_send_text'] ) ); elseif( isset( $arrValues['is_send_text'] ) ) $this->setIsSendText( $arrValues['is_send_text'] );
		if( isset( $arrValues['contact_accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intContactAcceptedBy', trim( $arrValues['contact_accepted_by'] ) ); elseif( isset( $arrValues['contact_accepted_by'] ) ) $this->setContactAcceptedBy( $arrValues['contact_accepted_by'] );
		if( isset( $arrValues['contact_accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strContactAcceptedOn', trim( $arrValues['contact_accepted_on'] ) ); elseif( isset( $arrValues['contact_accepted_on'] ) ) $this->setContactAcceptedOn( $arrValues['contact_accepted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setCraigslistPostId( $intCraigslistPostId ) {
		$this->set( 'm_intCraigslistPostId', CStrings::strToIntDef( $intCraigslistPostId, NULL, false ) );
	}

	public function getCraigslistPostId() {
		return $this->m_intCraigslistPostId;
	}

	public function sqlCraigslistPostId() {
		return ( true == isset( $this->m_intCraigslistPostId ) ) ? ( string ) $this->m_intCraigslistPostId : 'NULL';
	}

	public function setTrafficCookieId( $intTrafficCookieId ) {
		$this->set( 'm_intTrafficCookieId', CStrings::strToIntDef( $intTrafficCookieId, NULL, false ) );
	}

	public function getTrafficCookieId() {
		return $this->m_intTrafficCookieId;
	}

	public function sqlTrafficCookieId() {
		return ( true == isset( $this->m_intTrafficCookieId ) ) ? ( string ) $this->m_intTrafficCookieId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2030, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setContactDatetime( $strContactDatetime ) {
		$this->set( 'm_strContactDatetime', CStrings::strTrimDef( $strContactDatetime, -1, NULL, true ) );
	}

	public function getContactDatetime() {
		return $this->m_strContactDatetime;
	}

	public function sqlContactDatetime() {
		return ( true == isset( $this->m_strContactDatetime ) ) ? '\'' . $this->m_strContactDatetime . '\'' : 'NOW()';
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		$this->set( 'm_fltDesiredRentMin', CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2 ) );
	}

	public function getDesiredRentMin() {
		return $this->m_fltDesiredRentMin;
	}

	public function sqlDesiredRentMin() {
		return ( true == isset( $this->m_fltDesiredRentMin ) ) ? ( string ) $this->m_fltDesiredRentMin : 'NULL';
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		$this->set( 'm_fltDesiredRentMax', CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2 ) );
	}

	public function getDesiredRentMax() {
		return $this->m_fltDesiredRentMax;
	}

	public function sqlDesiredRentMax() {
		return ( true == isset( $this->m_fltDesiredRentMax ) ) ? ( string ) $this->m_fltDesiredRentMax : 'NULL';
	}

	public function setTermMonth( $intTermMonth ) {
		$this->set( 'm_intTermMonth', CStrings::strToIntDef( $intTermMonth, NULL, false ) );
	}

	public function getTermMonth() {
		return $this->m_intTermMonth;
	}

	public function sqlTermMonth() {
		return ( true == isset( $this->m_intTermMonth ) ) ? ( string ) $this->m_intTermMonth : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setDesiredBedrooms( $intDesiredBedrooms ) {
		$this->set( 'm_intDesiredBedrooms', CStrings::strToIntDef( $intDesiredBedrooms, NULL, false ) );
	}

	public function getDesiredBedrooms() {
		return $this->m_intDesiredBedrooms;
	}

	public function sqlDesiredBedrooms() {
		return ( true == isset( $this->m_intDesiredBedrooms ) ) ? ( string ) $this->m_intDesiredBedrooms : 'NULL';
	}

	public function setDesiredBathrooms( $intDesiredBathrooms ) {
		$this->set( 'm_intDesiredBathrooms', CStrings::strToIntDef( $intDesiredBathrooms, NULL, false ) );
	}

	public function getDesiredBathrooms() {
		return $this->m_intDesiredBathrooms;
	}

	public function sqlDesiredBathrooms() {
		return ( true == isset( $this->m_intDesiredBathrooms ) ) ? ( string ) $this->m_intDesiredBathrooms : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setIsWebInitiated( $intIsWebInitiated ) {
		$this->set( 'm_intIsWebInitiated', CStrings::strToIntDef( $intIsWebInitiated, NULL, false ) );
	}

	public function getIsWebInitiated() {
		return $this->m_intIsWebInitiated;
	}

	public function sqlIsWebInitiated() {
		return ( true == isset( $this->m_intIsWebInitiated ) ) ? ( string ) $this->m_intIsWebInitiated : '0';
	}

	public function setIsTextViewed( $intIsTextViewed ) {
		$this->set( 'm_intIsTextViewed', CStrings::strToIntDef( $intIsTextViewed, NULL, false ) );
	}

	public function getIsTextViewed() {
		return $this->m_intIsTextViewed;
	}

	public function sqlIsTextViewed() {
		return ( true == isset( $this->m_intIsTextViewed ) ) ? ( string ) $this->m_intIsTextViewed : '0';
	}

	public function setIsSendText( $intIsSendText ) {
		$this->set( 'm_intIsSendText', CStrings::strToIntDef( $intIsSendText, NULL, false ) );
	}

	public function getIsSendText() {
		return $this->m_intIsSendText;
	}

	public function sqlIsSendText() {
		return ( true == isset( $this->m_intIsSendText ) ) ? ( string ) $this->m_intIsSendText : '1';
	}

	public function setContactAcceptedBy( $intContactAcceptedBy ) {
		$this->set( 'm_intContactAcceptedBy', CStrings::strToIntDef( $intContactAcceptedBy, NULL, false ) );
	}

	public function getContactAcceptedBy() {
		return $this->m_intContactAcceptedBy;
	}

	public function sqlContactAcceptedBy() {
		return ( true == isset( $this->m_intContactAcceptedBy ) ) ? ( string ) $this->m_intContactAcceptedBy : 'NULL';
	}

	public function setContactAcceptedOn( $strContactAcceptedOn ) {
		$this->set( 'm_strContactAcceptedOn', CStrings::strTrimDef( $strContactAcceptedOn, -1, NULL, true ) );
	}

	public function getContactAcceptedOn() {
		return $this->m_strContactAcceptedOn;
	}

	public function sqlContactAcceptedOn() {
		return ( true == isset( $this->m_strContactAcceptedOn ) ) ? '\'' . $this->m_strContactAcceptedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, company_employee_id, applicant_application_id, lead_source_id, property_floorplan_id, property_unit_id, unit_space_id, lease_interval_type_id, ps_product_id, sem_ad_group_id, sem_keyword_id, sem_source_id, craigslist_post_id, traffic_cookie_id, remote_primary_key, notes, contact_datetime, desired_rent_min, desired_rent_max, term_month, lease_start_date, desired_bedrooms, desired_bathrooms, phone_number, is_web_initiated, is_text_viewed, is_send_text, contact_accepted_by, contact_accepted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlApplicantApplicationId() . ', ' .
 						$this->sqlLeadSourceId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlLeaseIntervalTypeId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlSemKeywordId() . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlCraigslistPostId() . ', ' .
 						$this->sqlTrafficCookieId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlContactDatetime() . ', ' .
 						$this->sqlDesiredRentMin() . ', ' .
 						$this->sqlDesiredRentMax() . ', ' .
 						$this->sqlTermMonth() . ', ' .
 						$this->sqlLeaseStartDate() . ', ' .
 						$this->sqlDesiredBedrooms() . ', ' .
 						$this->sqlDesiredBathrooms() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlIsWebInitiated() . ', ' .
 						$this->sqlIsTextViewed() . ', ' .
 						$this->sqlIsSendText() . ', ' .
 						$this->sqlContactAcceptedBy() . ', ' .
 						$this->sqlContactAcceptedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; } elseif( true == array_key_exists( 'CraigslistPostId', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; } elseif( true == array_key_exists( 'TrafficCookieId', $this->getChangedColumns() ) ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; } elseif( true == array_key_exists( 'ContactDatetime', $this->getChangedColumns() ) ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; } elseif( true == array_key_exists( 'DesiredRentMin', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; } elseif( true == array_key_exists( 'DesiredRentMax', $this->getChangedColumns() ) ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' term_month = ' . $this->sqlTermMonth() . ','; } elseif( true == array_key_exists( 'TermMonth', $this->getChangedColumns() ) ) { $strSql .= ' term_month = ' . $this->sqlTermMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; } elseif( true == array_key_exists( 'DesiredBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; } elseif( true == array_key_exists( 'DesiredBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_web_initiated = ' . $this->sqlIsWebInitiated() . ','; } elseif( true == array_key_exists( 'IsWebInitiated', $this->getChangedColumns() ) ) { $strSql .= ' is_web_initiated = ' . $this->sqlIsWebInitiated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_text_viewed = ' . $this->sqlIsTextViewed() . ','; } elseif( true == array_key_exists( 'IsTextViewed', $this->getChangedColumns() ) ) { $strSql .= ' is_text_viewed = ' . $this->sqlIsTextViewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_text = ' . $this->sqlIsSendText() . ','; } elseif( true == array_key_exists( 'IsSendText', $this->getChangedColumns() ) ) { $strSql .= ' is_send_text = ' . $this->sqlIsSendText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_accepted_by = ' . $this->sqlContactAcceptedBy() . ','; } elseif( true == array_key_exists( 'ContactAcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' contact_accepted_by = ' . $this->sqlContactAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_accepted_on = ' . $this->sqlContactAcceptedOn() . ','; } elseif( true == array_key_exists( 'ContactAcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' contact_accepted_on = ' . $this->sqlContactAcceptedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'sem_source_id' => $this->getSemSourceId(),
			'craigslist_post_id' => $this->getCraigslistPostId(),
			'traffic_cookie_id' => $this->getTrafficCookieId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'notes' => $this->getNotes(),
			'contact_datetime' => $this->getContactDatetime(),
			'desired_rent_min' => $this->getDesiredRentMin(),
			'desired_rent_max' => $this->getDesiredRentMax(),
			'term_month' => $this->getTermMonth(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'desired_bedrooms' => $this->getDesiredBedrooms(),
			'desired_bathrooms' => $this->getDesiredBathrooms(),
			'phone_number' => $this->getPhoneNumber(),
			'is_web_initiated' => $this->getIsWebInitiated(),
			'is_text_viewed' => $this->getIsTextViewed(),
			'is_send_text' => $this->getIsSendText(),
			'contact_accepted_by' => $this->getContactAcceptedBy(),
			'contact_accepted_on' => $this->getContactAcceptedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>