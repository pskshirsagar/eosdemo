<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackageStatusTypes
 * Do not add any new functions to this class.
 */

class CBasePackageStatusTypes extends CEosPluralBase {

	/**
	 * @return CPackageStatusType[]
	 */
	public static function fetchPackageStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPackageStatusType::class, $objDatabase );
	}

	/**
	 * @return CPackageStatusType
	 */
	public static function fetchPackageStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPackageStatusType::class, $objDatabase );
	}

	public static function fetchPackageStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'package_status_types', $objDatabase );
	}

	public static function fetchPackageStatusTypeById( $intId, $objDatabase ) {
		return self::fetchPackageStatusType( sprintf( 'SELECT * FROM package_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>