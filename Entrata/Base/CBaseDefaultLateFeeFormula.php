<?php

class CBaseDefaultLateFeeFormula extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_late_fee_formulas';

	protected $m_intId;
	protected $m_intOccupancyTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltFixedAmount;
	protected $m_fltAmountPerDay;
	protected $m_fltFixedPercent;
	protected $m_fltPercentPerDay;
	protected $m_intFirstLateDay;
	protected $m_intDailyLateFeeStartDay;
	protected $m_fltMinimumBalanceDue;
	protected $m_fltMaxMonthlyLateFeePercent;
	protected $m_fltMaxMonthlyLateFeeAmount;
	protected $m_fltLegalFeeAmount;
	protected $m_intScheduledChargesOnly;
	protected $m_intBaseChargeOnAmountDue;
	protected $m_intAutoPostNsfLateFees;
	protected $m_intPostFeesToNextPeriod;
	protected $m_intIncludePastPeriodTransactions;
	protected $m_intAllowToApplyLegalFee;
	protected $m_intIsSystem;
	protected $m_intDailyLateFeeEndDay;
	protected $m_intIncludeFutureLeases;
	protected $m_boolChargeLateFeesOnHolidays;
	protected $m_boolChargeLateFeesOnSaturdays;
	protected $m_boolChargeLateFeesOnSundays;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intGraceDays;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupancyTypeId = '1';
		$this->m_fltFixedAmount = '0';
		$this->m_fltAmountPerDay = '0';
		$this->m_fltFixedPercent = '0';
		$this->m_fltPercentPerDay = '0';
		$this->m_intFirstLateDay = '5';
		$this->m_fltMinimumBalanceDue = '0';
		$this->m_intScheduledChargesOnly = '0';
		$this->m_intBaseChargeOnAmountDue = '1';
		$this->m_intAutoPostNsfLateFees = '0';
		$this->m_intPostFeesToNextPeriod = '0';
		$this->m_intIncludePastPeriodTransactions = '0';
		$this->m_intAllowToApplyLegalFee = '0';
		$this->m_intIsSystem = '0';
		$this->m_intDailyLateFeeEndDay = '31';
		$this->m_intIncludeFutureLeases = '0';
		$this->m_boolChargeLateFeesOnHolidays = true;
		$this->m_boolChargeLateFeesOnSaturdays = true;
		$this->m_boolChargeLateFeesOnSundays = true;
		$this->m_intGraceDays = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFixedAmount', trim( $arrValues['fixed_amount'] ) ); elseif( isset( $arrValues['fixed_amount'] ) ) $this->setFixedAmount( $arrValues['fixed_amount'] );
		if( isset( $arrValues['amount_per_day'] ) && $boolDirectSet ) $this->set( 'm_fltAmountPerDay', trim( $arrValues['amount_per_day'] ) ); elseif( isset( $arrValues['amount_per_day'] ) ) $this->setAmountPerDay( $arrValues['amount_per_day'] );
		if( isset( $arrValues['fixed_percent'] ) && $boolDirectSet ) $this->set( 'm_fltFixedPercent', trim( $arrValues['fixed_percent'] ) ); elseif( isset( $arrValues['fixed_percent'] ) ) $this->setFixedPercent( $arrValues['fixed_percent'] );
		if( isset( $arrValues['percent_per_day'] ) && $boolDirectSet ) $this->set( 'm_fltPercentPerDay', trim( $arrValues['percent_per_day'] ) ); elseif( isset( $arrValues['percent_per_day'] ) ) $this->setPercentPerDay( $arrValues['percent_per_day'] );
		if( isset( $arrValues['first_late_day'] ) && $boolDirectSet ) $this->set( 'm_intFirstLateDay', trim( $arrValues['first_late_day'] ) ); elseif( isset( $arrValues['first_late_day'] ) ) $this->setFirstLateDay( $arrValues['first_late_day'] );
		if( isset( $arrValues['daily_late_fee_start_day'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeStartDay', trim( $arrValues['daily_late_fee_start_day'] ) ); elseif( isset( $arrValues['daily_late_fee_start_day'] ) ) $this->setDailyLateFeeStartDay( $arrValues['daily_late_fee_start_day'] );
		if( isset( $arrValues['minimum_balance_due'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumBalanceDue', trim( $arrValues['minimum_balance_due'] ) ); elseif( isset( $arrValues['minimum_balance_due'] ) ) $this->setMinimumBalanceDue( $arrValues['minimum_balance_due'] );
		if( isset( $arrValues['max_monthly_late_fee_percent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMonthlyLateFeePercent', trim( $arrValues['max_monthly_late_fee_percent'] ) ); elseif( isset( $arrValues['max_monthly_late_fee_percent'] ) ) $this->setMaxMonthlyLateFeePercent( $arrValues['max_monthly_late_fee_percent'] );
		if( isset( $arrValues['max_monthly_late_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMonthlyLateFeeAmount', trim( $arrValues['max_monthly_late_fee_amount'] ) ); elseif( isset( $arrValues['max_monthly_late_fee_amount'] ) ) $this->setMaxMonthlyLateFeeAmount( $arrValues['max_monthly_late_fee_amount'] );
		if( isset( $arrValues['legal_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltLegalFeeAmount', trim( $arrValues['legal_fee_amount'] ) ); elseif( isset( $arrValues['legal_fee_amount'] ) ) $this->setLegalFeeAmount( $arrValues['legal_fee_amount'] );
		if( isset( $arrValues['scheduled_charges_only'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargesOnly', trim( $arrValues['scheduled_charges_only'] ) ); elseif( isset( $arrValues['scheduled_charges_only'] ) ) $this->setScheduledChargesOnly( $arrValues['scheduled_charges_only'] );
		if( isset( $arrValues['base_charge_on_amount_due'] ) && $boolDirectSet ) $this->set( 'm_intBaseChargeOnAmountDue', trim( $arrValues['base_charge_on_amount_due'] ) ); elseif( isset( $arrValues['base_charge_on_amount_due'] ) ) $this->setBaseChargeOnAmountDue( $arrValues['base_charge_on_amount_due'] );
		if( isset( $arrValues['auto_post_nsf_late_fees'] ) && $boolDirectSet ) $this->set( 'm_intAutoPostNsfLateFees', trim( $arrValues['auto_post_nsf_late_fees'] ) ); elseif( isset( $arrValues['auto_post_nsf_late_fees'] ) ) $this->setAutoPostNsfLateFees( $arrValues['auto_post_nsf_late_fees'] );
		if( isset( $arrValues['post_fees_to_next_period'] ) && $boolDirectSet ) $this->set( 'm_intPostFeesToNextPeriod', trim( $arrValues['post_fees_to_next_period'] ) ); elseif( isset( $arrValues['post_fees_to_next_period'] ) ) $this->setPostFeesToNextPeriod( $arrValues['post_fees_to_next_period'] );
		if( isset( $arrValues['include_past_period_transactions'] ) && $boolDirectSet ) $this->set( 'm_intIncludePastPeriodTransactions', trim( $arrValues['include_past_period_transactions'] ) ); elseif( isset( $arrValues['include_past_period_transactions'] ) ) $this->setIncludePastPeriodTransactions( $arrValues['include_past_period_transactions'] );
		if( isset( $arrValues['allow_to_apply_legal_fee'] ) && $boolDirectSet ) $this->set( 'm_intAllowToApplyLegalFee', trim( $arrValues['allow_to_apply_legal_fee'] ) ); elseif( isset( $arrValues['allow_to_apply_legal_fee'] ) ) $this->setAllowToApplyLegalFee( $arrValues['allow_to_apply_legal_fee'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['daily_late_fee_end_day'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeEndDay', trim( $arrValues['daily_late_fee_end_day'] ) ); elseif( isset( $arrValues['daily_late_fee_end_day'] ) ) $this->setDailyLateFeeEndDay( $arrValues['daily_late_fee_end_day'] );
		if( isset( $arrValues['include_future_leases'] ) && $boolDirectSet ) $this->set( 'm_intIncludeFutureLeases', trim( $arrValues['include_future_leases'] ) ); elseif( isset( $arrValues['include_future_leases'] ) ) $this->setIncludeFutureLeases( $arrValues['include_future_leases'] );
		if( isset( $arrValues['charge_late_fees_on_holidays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnHolidays', trim( stripcslashes( $arrValues['charge_late_fees_on_holidays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_holidays'] ) ) $this->setChargeLateFeesOnHolidays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_holidays'] ) : $arrValues['charge_late_fees_on_holidays'] );
		if( isset( $arrValues['charge_late_fees_on_saturdays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnSaturdays', trim( stripcslashes( $arrValues['charge_late_fees_on_saturdays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_saturdays'] ) ) $this->setChargeLateFeesOnSaturdays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_saturdays'] ) : $arrValues['charge_late_fees_on_saturdays'] );
		if( isset( $arrValues['charge_late_fees_on_sundays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnSundays', trim( stripcslashes( $arrValues['charge_late_fees_on_sundays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_sundays'] ) ) $this->setChargeLateFeesOnSundays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_sundays'] ) : $arrValues['charge_late_fees_on_sundays'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['grace_days'] ) && $boolDirectSet ) $this->set( 'm_intGraceDays', trim( $arrValues['grace_days'] ) ); elseif( isset( $arrValues['grace_days'] ) ) $this->setGraceDays( $arrValues['grace_days'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFixedAmount( $fltFixedAmount ) {
		$this->set( 'm_fltFixedAmount', CStrings::strToFloatDef( $fltFixedAmount, NULL, false, 2 ) );
	}

	public function getFixedAmount() {
		return $this->m_fltFixedAmount;
	}

	public function sqlFixedAmount() {
		return ( true == isset( $this->m_fltFixedAmount ) ) ? ( string ) $this->m_fltFixedAmount : '0';
	}

	public function setAmountPerDay( $fltAmountPerDay ) {
		$this->set( 'm_fltAmountPerDay', CStrings::strToFloatDef( $fltAmountPerDay, NULL, false, 2 ) );
	}

	public function getAmountPerDay() {
		return $this->m_fltAmountPerDay;
	}

	public function sqlAmountPerDay() {
		return ( true == isset( $this->m_fltAmountPerDay ) ) ? ( string ) $this->m_fltAmountPerDay : '0';
	}

	public function setFixedPercent( $fltFixedPercent ) {
		$this->set( 'm_fltFixedPercent', CStrings::strToFloatDef( $fltFixedPercent, NULL, false, 6 ) );
	}

	public function getFixedPercent() {
		return $this->m_fltFixedPercent;
	}

	public function sqlFixedPercent() {
		return ( true == isset( $this->m_fltFixedPercent ) ) ? ( string ) $this->m_fltFixedPercent : '0';
	}

	public function setPercentPerDay( $fltPercentPerDay ) {
		$this->set( 'm_fltPercentPerDay', CStrings::strToFloatDef( $fltPercentPerDay, NULL, false, 6 ) );
	}

	public function getPercentPerDay() {
		return $this->m_fltPercentPerDay;
	}

	public function sqlPercentPerDay() {
		return ( true == isset( $this->m_fltPercentPerDay ) ) ? ( string ) $this->m_fltPercentPerDay : '0';
	}

	public function setFirstLateDay( $intFirstLateDay ) {
		$this->set( 'm_intFirstLateDay', CStrings::strToIntDef( $intFirstLateDay, NULL, false ) );
	}

	public function getFirstLateDay() {
		return $this->m_intFirstLateDay;
	}

	public function sqlFirstLateDay() {
		return ( true == isset( $this->m_intFirstLateDay ) ) ? ( string ) $this->m_intFirstLateDay : '5';
	}

	public function setDailyLateFeeStartDay( $intDailyLateFeeStartDay ) {
		$this->set( 'm_intDailyLateFeeStartDay', CStrings::strToIntDef( $intDailyLateFeeStartDay, NULL, false ) );
	}

	public function getDailyLateFeeStartDay() {
		return $this->m_intDailyLateFeeStartDay;
	}

	public function sqlDailyLateFeeStartDay() {
		return ( true == isset( $this->m_intDailyLateFeeStartDay ) ) ? ( string ) $this->m_intDailyLateFeeStartDay : 'NULL';
	}

	public function setMinimumBalanceDue( $fltMinimumBalanceDue ) {
		$this->set( 'm_fltMinimumBalanceDue', CStrings::strToFloatDef( $fltMinimumBalanceDue, NULL, false, 2 ) );
	}

	public function getMinimumBalanceDue() {
		return $this->m_fltMinimumBalanceDue;
	}

	public function sqlMinimumBalanceDue() {
		return ( true == isset( $this->m_fltMinimumBalanceDue ) ) ? ( string ) $this->m_fltMinimumBalanceDue : '0';
	}

	public function setMaxMonthlyLateFeePercent( $fltMaxMonthlyLateFeePercent ) {
		$this->set( 'm_fltMaxMonthlyLateFeePercent', CStrings::strToFloatDef( $fltMaxMonthlyLateFeePercent, NULL, false, 2 ) );
	}

	public function getMaxMonthlyLateFeePercent() {
		return $this->m_fltMaxMonthlyLateFeePercent;
	}

	public function sqlMaxMonthlyLateFeePercent() {
		return ( true == isset( $this->m_fltMaxMonthlyLateFeePercent ) ) ? ( string ) $this->m_fltMaxMonthlyLateFeePercent : 'NULL';
	}

	public function setMaxMonthlyLateFeeAmount( $fltMaxMonthlyLateFeeAmount ) {
		$this->set( 'm_fltMaxMonthlyLateFeeAmount', CStrings::strToFloatDef( $fltMaxMonthlyLateFeeAmount, NULL, false, 2 ) );
	}

	public function getMaxMonthlyLateFeeAmount() {
		return $this->m_fltMaxMonthlyLateFeeAmount;
	}

	public function sqlMaxMonthlyLateFeeAmount() {
		return ( true == isset( $this->m_fltMaxMonthlyLateFeeAmount ) ) ? ( string ) $this->m_fltMaxMonthlyLateFeeAmount : 'NULL';
	}

	public function setLegalFeeAmount( $fltLegalFeeAmount ) {
		$this->set( 'm_fltLegalFeeAmount', CStrings::strToFloatDef( $fltLegalFeeAmount, NULL, false, 2 ) );
	}

	public function getLegalFeeAmount() {
		return $this->m_fltLegalFeeAmount;
	}

	public function sqlLegalFeeAmount() {
		return ( true == isset( $this->m_fltLegalFeeAmount ) ) ? ( string ) $this->m_fltLegalFeeAmount : 'NULL';
	}

	public function setScheduledChargesOnly( $intScheduledChargesOnly ) {
		$this->set( 'm_intScheduledChargesOnly', CStrings::strToIntDef( $intScheduledChargesOnly, NULL, false ) );
	}

	public function getScheduledChargesOnly() {
		return $this->m_intScheduledChargesOnly;
	}

	public function sqlScheduledChargesOnly() {
		return ( true == isset( $this->m_intScheduledChargesOnly ) ) ? ( string ) $this->m_intScheduledChargesOnly : '0';
	}

	public function setBaseChargeOnAmountDue( $intBaseChargeOnAmountDue ) {
		$this->set( 'm_intBaseChargeOnAmountDue', CStrings::strToIntDef( $intBaseChargeOnAmountDue, NULL, false ) );
	}

	public function getBaseChargeOnAmountDue() {
		return $this->m_intBaseChargeOnAmountDue;
	}

	public function sqlBaseChargeOnAmountDue() {
		return ( true == isset( $this->m_intBaseChargeOnAmountDue ) ) ? ( string ) $this->m_intBaseChargeOnAmountDue : '1';
	}

	public function setAutoPostNsfLateFees( $intAutoPostNsfLateFees ) {
		$this->set( 'm_intAutoPostNsfLateFees', CStrings::strToIntDef( $intAutoPostNsfLateFees, NULL, false ) );
	}

	public function getAutoPostNsfLateFees() {
		return $this->m_intAutoPostNsfLateFees;
	}

	public function sqlAutoPostNsfLateFees() {
		return ( true == isset( $this->m_intAutoPostNsfLateFees ) ) ? ( string ) $this->m_intAutoPostNsfLateFees : '0';
	}

	public function setPostFeesToNextPeriod( $intPostFeesToNextPeriod ) {
		$this->set( 'm_intPostFeesToNextPeriod', CStrings::strToIntDef( $intPostFeesToNextPeriod, NULL, false ) );
	}

	public function getPostFeesToNextPeriod() {
		return $this->m_intPostFeesToNextPeriod;
	}

	public function sqlPostFeesToNextPeriod() {
		return ( true == isset( $this->m_intPostFeesToNextPeriod ) ) ? ( string ) $this->m_intPostFeesToNextPeriod : '0';
	}

	public function setIncludePastPeriodTransactions( $intIncludePastPeriodTransactions ) {
		$this->set( 'm_intIncludePastPeriodTransactions', CStrings::strToIntDef( $intIncludePastPeriodTransactions, NULL, false ) );
	}

	public function getIncludePastPeriodTransactions() {
		return $this->m_intIncludePastPeriodTransactions;
	}

	public function sqlIncludePastPeriodTransactions() {
		return ( true == isset( $this->m_intIncludePastPeriodTransactions ) ) ? ( string ) $this->m_intIncludePastPeriodTransactions : '0';
	}

	public function setAllowToApplyLegalFee( $intAllowToApplyLegalFee ) {
		$this->set( 'm_intAllowToApplyLegalFee', CStrings::strToIntDef( $intAllowToApplyLegalFee, NULL, false ) );
	}

	public function getAllowToApplyLegalFee() {
		return $this->m_intAllowToApplyLegalFee;
	}

	public function sqlAllowToApplyLegalFee() {
		return ( true == isset( $this->m_intAllowToApplyLegalFee ) ) ? ( string ) $this->m_intAllowToApplyLegalFee : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setDailyLateFeeEndDay( $intDailyLateFeeEndDay ) {
		$this->set( 'm_intDailyLateFeeEndDay', CStrings::strToIntDef( $intDailyLateFeeEndDay, NULL, false ) );
	}

	public function getDailyLateFeeEndDay() {
		return $this->m_intDailyLateFeeEndDay;
	}

	public function sqlDailyLateFeeEndDay() {
		return ( true == isset( $this->m_intDailyLateFeeEndDay ) ) ? ( string ) $this->m_intDailyLateFeeEndDay : '31';
	}

	public function setIncludeFutureLeases( $intIncludeFutureLeases ) {
		$this->set( 'm_intIncludeFutureLeases', CStrings::strToIntDef( $intIncludeFutureLeases, NULL, false ) );
	}

	public function getIncludeFutureLeases() {
		return $this->m_intIncludeFutureLeases;
	}

	public function sqlIncludeFutureLeases() {
		return ( true == isset( $this->m_intIncludeFutureLeases ) ) ? ( string ) $this->m_intIncludeFutureLeases : '0';
	}

	public function setChargeLateFeesOnHolidays( $boolChargeLateFeesOnHolidays ) {
		$this->set( 'm_boolChargeLateFeesOnHolidays', CStrings::strToBool( $boolChargeLateFeesOnHolidays ) );
	}

	public function getChargeLateFeesOnHolidays() {
		return $this->m_boolChargeLateFeesOnHolidays;
	}

	public function sqlChargeLateFeesOnHolidays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnHolidays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnHolidays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeLateFeesOnSaturdays( $boolChargeLateFeesOnSaturdays ) {
		$this->set( 'm_boolChargeLateFeesOnSaturdays', CStrings::strToBool( $boolChargeLateFeesOnSaturdays ) );
	}

	public function getChargeLateFeesOnSaturdays() {
		return $this->m_boolChargeLateFeesOnSaturdays;
	}

	public function sqlChargeLateFeesOnSaturdays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnSaturdays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnSaturdays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeLateFeesOnSundays( $boolChargeLateFeesOnSundays ) {
		$this->set( 'm_boolChargeLateFeesOnSundays', CStrings::strToBool( $boolChargeLateFeesOnSundays ) );
	}

	public function getChargeLateFeesOnSundays() {
		return $this->m_boolChargeLateFeesOnSundays;
	}

	public function sqlChargeLateFeesOnSundays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnSundays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnSundays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGraceDays( $intGraceDays ) {
		$this->set( 'm_intGraceDays', CStrings::strToIntDef( $intGraceDays, NULL, false ) );
	}

	public function getGraceDays() {
		return $this->m_intGraceDays;
	}

	public function sqlGraceDays() {
		return ( true == isset( $this->m_intGraceDays ) ) ? ( string ) $this->m_intGraceDays : '1';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'fixed_amount' => $this->getFixedAmount(),
			'amount_per_day' => $this->getAmountPerDay(),
			'fixed_percent' => $this->getFixedPercent(),
			'percent_per_day' => $this->getPercentPerDay(),
			'first_late_day' => $this->getFirstLateDay(),
			'daily_late_fee_start_day' => $this->getDailyLateFeeStartDay(),
			'minimum_balance_due' => $this->getMinimumBalanceDue(),
			'max_monthly_late_fee_percent' => $this->getMaxMonthlyLateFeePercent(),
			'max_monthly_late_fee_amount' => $this->getMaxMonthlyLateFeeAmount(),
			'legal_fee_amount' => $this->getLegalFeeAmount(),
			'scheduled_charges_only' => $this->getScheduledChargesOnly(),
			'base_charge_on_amount_due' => $this->getBaseChargeOnAmountDue(),
			'auto_post_nsf_late_fees' => $this->getAutoPostNsfLateFees(),
			'post_fees_to_next_period' => $this->getPostFeesToNextPeriod(),
			'include_past_period_transactions' => $this->getIncludePastPeriodTransactions(),
			'allow_to_apply_legal_fee' => $this->getAllowToApplyLegalFee(),
			'is_system' => $this->getIsSystem(),
			'daily_late_fee_end_day' => $this->getDailyLateFeeEndDay(),
			'include_future_leases' => $this->getIncludeFutureLeases(),
			'charge_late_fees_on_holidays' => $this->getChargeLateFeesOnHolidays(),
			'charge_late_fees_on_saturdays' => $this->getChargeLateFeesOnSaturdays(),
			'charge_late_fees_on_sundays' => $this->getChargeLateFeesOnSundays(),
			'details' => $this->getDetails(),
			'grace_days' => $this->getGraceDays()
		);
	}

}
?>