<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserPreferences extends CEosPluralBase {

	/**
	 * @return CCompanyUserPreference[]
	 */
	public static function fetchCompanyUserPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserPreference
	 */
	public static function fetchCompanyUserPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserPreference', $objDatabase );
	}

	public static function fetchCompanyUserPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_preferences', $objDatabase );
	}

	public static function fetchCompanyUserPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPreference( sprintf( 'SELECT * FROM company_user_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserPreferences( sprintf( 'SELECT * FROM company_user_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPreferences( sprintf( 'SELECT * FROM company_user_preferences WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>