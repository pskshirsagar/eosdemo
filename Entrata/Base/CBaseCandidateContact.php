<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateContact extends CEosSingularBase {

	const TABLE_NAME = 'public.candidate_contacts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCandidateId;
	protected $m_intCandidateContactTypeId;
	protected $m_strCompanyName;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strRelationship;
	protected $m_strOccupation;
	protected $m_strPhoneNumber;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_fltGrossSalary;
	protected $m_strEarningsDescription;
	protected $m_strDateStarted;
	protected $m_strDateTerminated;
	protected $m_strReasonForLeaving;
	protected $m_strRoles;
	protected $m_strDuties;
	protected $m_strNotes;
	protected $m_strCandidateApprovedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['candidate_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateId', trim( $arrValues['candidate_id'] ) ); elseif( isset( $arrValues['candidate_id'] ) ) $this->setCandidateId( $arrValues['candidate_id'] );
		if( isset( $arrValues['candidate_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateContactTypeId', trim( $arrValues['candidate_contact_type_id'] ) ); elseif( isset( $arrValues['candidate_contact_type_id'] ) ) $this->setCandidateContactTypeId( $arrValues['candidate_contact_type_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['relationship'] ) && $boolDirectSet ) $this->set( 'm_strRelationship', trim( stripcslashes( $arrValues['relationship'] ) ) ); elseif( isset( $arrValues['relationship'] ) ) $this->setRelationship( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['relationship'] ) : $arrValues['relationship'] );
		if( isset( $arrValues['occupation'] ) && $boolDirectSet ) $this->set( 'm_strOccupation', trim( stripcslashes( $arrValues['occupation'] ) ) ); elseif( isset( $arrValues['occupation'] ) ) $this->setOccupation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['occupation'] ) : $arrValues['occupation'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['gross_salary'] ) && $boolDirectSet ) $this->set( 'm_fltGrossSalary', trim( $arrValues['gross_salary'] ) ); elseif( isset( $arrValues['gross_salary'] ) ) $this->setGrossSalary( $arrValues['gross_salary'] );
		if( isset( $arrValues['earnings_description'] ) && $boolDirectSet ) $this->set( 'm_strEarningsDescription', trim( stripcslashes( $arrValues['earnings_description'] ) ) ); elseif( isset( $arrValues['earnings_description'] ) ) $this->setEarningsDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['earnings_description'] ) : $arrValues['earnings_description'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['reason_for_leaving'] ) && $boolDirectSet ) $this->set( 'm_strReasonForLeaving', trim( stripcslashes( $arrValues['reason_for_leaving'] ) ) ); elseif( isset( $arrValues['reason_for_leaving'] ) ) $this->setReasonForLeaving( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_for_leaving'] ) : $arrValues['reason_for_leaving'] );
		if( isset( $arrValues['roles'] ) && $boolDirectSet ) $this->set( 'm_strRoles', trim( stripcslashes( $arrValues['roles'] ) ) ); elseif( isset( $arrValues['roles'] ) ) $this->setRoles( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['roles'] ) : $arrValues['roles'] );
		if( isset( $arrValues['duties'] ) && $boolDirectSet ) $this->set( 'm_strDuties', trim( stripcslashes( $arrValues['duties'] ) ) ); elseif( isset( $arrValues['duties'] ) ) $this->setDuties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['duties'] ) : $arrValues['duties'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['candidate_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strCandidateApprovedOn', trim( $arrValues['candidate_approved_on'] ) ); elseif( isset( $arrValues['candidate_approved_on'] ) ) $this->setCandidateApprovedOn( $arrValues['candidate_approved_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCandidateId( $intCandidateId ) {
		$this->set( 'm_intCandidateId', CStrings::strToIntDef( $intCandidateId, NULL, false ) );
	}

	public function getCandidateId() {
		return $this->m_intCandidateId;
	}

	public function sqlCandidateId() {
		return ( true == isset( $this->m_intCandidateId ) ) ? ( string ) $this->m_intCandidateId : 'NULL';
	}

	public function setCandidateContactTypeId( $intCandidateContactTypeId ) {
		$this->set( 'm_intCandidateContactTypeId', CStrings::strToIntDef( $intCandidateContactTypeId, NULL, false ) );
	}

	public function getCandidateContactTypeId() {
		return $this->m_intCandidateContactTypeId;
	}

	public function sqlCandidateContactTypeId() {
		return ( true == isset( $this->m_intCandidateContactTypeId ) ) ? ( string ) $this->m_intCandidateContactTypeId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setRelationship( $strRelationship ) {
		$this->set( 'm_strRelationship', CStrings::strTrimDef( $strRelationship, 240, NULL, true ) );
	}

	public function getRelationship() {
		return $this->m_strRelationship;
	}

	public function sqlRelationship() {
		return ( true == isset( $this->m_strRelationship ) ) ? '\'' . addslashes( $this->m_strRelationship ) . '\'' : 'NULL';
	}

	public function setOccupation( $strOccupation ) {
		$this->set( 'm_strOccupation', CStrings::strTrimDef( $strOccupation, 240, NULL, true ) );
	}

	public function getOccupation() {
		return $this->m_strOccupation;
	}

	public function sqlOccupation() {
		return ( true == isset( $this->m_strOccupation ) ) ? '\'' . addslashes( $this->m_strOccupation ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setGrossSalary( $fltGrossSalary ) {
		$this->set( 'm_fltGrossSalary', CStrings::strToFloatDef( $fltGrossSalary, NULL, false, 4 ) );
	}

	public function getGrossSalary() {
		return $this->m_fltGrossSalary;
	}

	public function sqlGrossSalary() {
		return ( true == isset( $this->m_fltGrossSalary ) ) ? ( string ) $this->m_fltGrossSalary : 'NULL';
	}

	public function setEarningsDescription( $strEarningsDescription ) {
		$this->set( 'm_strEarningsDescription', CStrings::strTrimDef( $strEarningsDescription, 240, NULL, true ) );
	}

	public function getEarningsDescription() {
		return $this->m_strEarningsDescription;
	}

	public function sqlEarningsDescription() {
		return ( true == isset( $this->m_strEarningsDescription ) ) ? '\'' . addslashes( $this->m_strEarningsDescription ) . '\'' : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setReasonForLeaving( $strReasonForLeaving ) {
		$this->set( 'm_strReasonForLeaving', CStrings::strTrimDef( $strReasonForLeaving, 2000, NULL, true ) );
	}

	public function getReasonForLeaving() {
		return $this->m_strReasonForLeaving;
	}

	public function sqlReasonForLeaving() {
		return ( true == isset( $this->m_strReasonForLeaving ) ) ? '\'' . addslashes( $this->m_strReasonForLeaving ) . '\'' : 'NULL';
	}

	public function setRoles( $strRoles ) {
		$this->set( 'm_strRoles', CStrings::strTrimDef( $strRoles, 240, NULL, true ) );
	}

	public function getRoles() {
		return $this->m_strRoles;
	}

	public function sqlRoles() {
		return ( true == isset( $this->m_strRoles ) ) ? '\'' . addslashes( $this->m_strRoles ) . '\'' : 'NULL';
	}

	public function setDuties( $strDuties ) {
		$this->set( 'm_strDuties', CStrings::strTrimDef( $strDuties, 240, NULL, true ) );
	}

	public function getDuties() {
		return $this->m_strDuties;
	}

	public function sqlDuties() {
		return ( true == isset( $this->m_strDuties ) ) ? '\'' . addslashes( $this->m_strDuties ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setCandidateApprovedOn( $strCandidateApprovedOn ) {
		$this->set( 'm_strCandidateApprovedOn', CStrings::strTrimDef( $strCandidateApprovedOn, -1, NULL, true ) );
	}

	public function getCandidateApprovedOn() {
		return $this->m_strCandidateApprovedOn;
	}

	public function sqlCandidateApprovedOn() {
		return ( true == isset( $this->m_strCandidateApprovedOn ) ) ? '\'' . $this->m_strCandidateApprovedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, candidate_id, candidate_contact_type_id, company_name, name_first, name_last, relationship, occupation, phone_number, street_line1, street_line2, city, state_code, postal_code, gross_salary, earnings_description, date_started, date_terminated, reason_for_leaving, roles, duties, notes, candidate_approved_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCandidateId() . ', ' .
 						$this->sqlCandidateContactTypeId() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlRelationship() . ', ' .
 						$this->sqlOccupation() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlGrossSalary() . ', ' .
 						$this->sqlEarningsDescription() . ', ' .
 						$this->sqlDateStarted() . ', ' .
 						$this->sqlDateTerminated() . ', ' .
 						$this->sqlReasonForLeaving() . ', ' .
 						$this->sqlRoles() . ', ' .
 						$this->sqlDuties() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlCandidateApprovedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; } elseif( true == array_key_exists( 'CandidateId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_id = ' . $this->sqlCandidateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_contact_type_id = ' . $this->sqlCandidateContactTypeId() . ','; } elseif( true == array_key_exists( 'CandidateContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_contact_type_id = ' . $this->sqlCandidateContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relationship = ' . $this->sqlRelationship() . ','; } elseif( true == array_key_exists( 'Relationship', $this->getChangedColumns() ) ) { $strSql .= ' relationship = ' . $this->sqlRelationship() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupation = ' . $this->sqlOccupation() . ','; } elseif( true == array_key_exists( 'Occupation', $this->getChangedColumns() ) ) { $strSql .= ' occupation = ' . $this->sqlOccupation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_salary = ' . $this->sqlGrossSalary() . ','; } elseif( true == array_key_exists( 'GrossSalary', $this->getChangedColumns() ) ) { $strSql .= ' gross_salary = ' . $this->sqlGrossSalary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' earnings_description = ' . $this->sqlEarningsDescription() . ','; } elseif( true == array_key_exists( 'EarningsDescription', $this->getChangedColumns() ) ) { $strSql .= ' earnings_description = ' . $this->sqlEarningsDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; } elseif( true == array_key_exists( 'ReasonForLeaving', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' roles = ' . $this->sqlRoles() . ','; } elseif( true == array_key_exists( 'Roles', $this->getChangedColumns() ) ) { $strSql .= ' roles = ' . $this->sqlRoles() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duties = ' . $this->sqlDuties() . ','; } elseif( true == array_key_exists( 'Duties', $this->getChangedColumns() ) ) { $strSql .= ' duties = ' . $this->sqlDuties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_approved_on = ' . $this->sqlCandidateApprovedOn() . ','; } elseif( true == array_key_exists( 'CandidateApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' candidate_approved_on = ' . $this->sqlCandidateApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'candidate_id' => $this->getCandidateId(),
			'candidate_contact_type_id' => $this->getCandidateContactTypeId(),
			'company_name' => $this->getCompanyName(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'relationship' => $this->getRelationship(),
			'occupation' => $this->getOccupation(),
			'phone_number' => $this->getPhoneNumber(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'gross_salary' => $this->getGrossSalary(),
			'earnings_description' => $this->getEarningsDescription(),
			'date_started' => $this->getDateStarted(),
			'date_terminated' => $this->getDateTerminated(),
			'reason_for_leaving' => $this->getReasonForLeaving(),
			'roles' => $this->getRoles(),
			'duties' => $this->getDuties(),
			'notes' => $this->getNotes(),
			'candidate_approved_on' => $this->getCandidateApprovedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>