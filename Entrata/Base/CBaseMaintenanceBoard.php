<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceBoard extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_boards';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intGroupByProblems;
	protected $m_boolShowInspectionColumn;
	protected $m_boolShowFloorplanOnUnitHover;
	protected $m_boolDontShowEndDatesAndDaysRemaining;
	protected $m_boolShowAvailableOnDate;
	protected $m_boolDisplayCompletedUntilMoveIn;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowInspectionColumn = false;
		$this->m_boolShowFloorplanOnUnitHover = false;
		$this->m_boolDontShowEndDatesAndDaysRemaining = false;
		$this->m_boolShowAvailableOnDate = false;
		$this->m_boolDisplayCompletedUntilMoveIn = false;
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['group_by_problems'] ) && $boolDirectSet ) $this->set( 'm_intGroupByProblems', trim( $arrValues['group_by_problems'] ) ); elseif( isset( $arrValues['group_by_problems'] ) ) $this->setGroupByProblems( $arrValues['group_by_problems'] );
		if( isset( $arrValues['show_inspection_column'] ) && $boolDirectSet ) $this->set( 'm_boolShowInspectionColumn', trim( stripcslashes( $arrValues['show_inspection_column'] ) ) ); elseif( isset( $arrValues['show_inspection_column'] ) ) $this->setShowInspectionColumn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_inspection_column'] ) : $arrValues['show_inspection_column'] );
		if( isset( $arrValues['show_floorplan_on_unit_hover'] ) && $boolDirectSet ) $this->set( 'm_boolShowFloorplanOnUnitHover', trim( stripcslashes( $arrValues['show_floorplan_on_unit_hover'] ) ) ); elseif( isset( $arrValues['show_floorplan_on_unit_hover'] ) ) $this->setShowFloorplanOnUnitHover( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_floorplan_on_unit_hover'] ) : $arrValues['show_floorplan_on_unit_hover'] );
		if( isset( $arrValues['dont_show_end_dates_and_days_remaining'] ) && $boolDirectSet ) $this->set( 'm_boolDontShowEndDatesAndDaysRemaining', trim( stripcslashes( $arrValues['dont_show_end_dates_and_days_remaining'] ) ) ); elseif( isset( $arrValues['dont_show_end_dates_and_days_remaining'] ) ) $this->setDontShowEndDatesAndDaysRemaining( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_show_end_dates_and_days_remaining'] ) : $arrValues['dont_show_end_dates_and_days_remaining'] );
		if( isset( $arrValues['show_available_on_date'] ) && $boolDirectSet ) $this->set( 'm_boolShowAvailableOnDate', trim( stripcslashes( $arrValues['show_available_on_date'] ) ) ); elseif( isset( $arrValues['show_available_on_date'] ) ) $this->setShowAvailableOnDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_available_on_date'] ) : $arrValues['show_available_on_date'] );
		if( isset( $arrValues['display_completed_until_move_in'] ) && $boolDirectSet ) $this->set( 'm_boolDisplayCompletedUntilMoveIn', trim( stripcslashes( $arrValues['display_completed_until_move_in'] ) ) ); elseif( isset( $arrValues['display_completed_until_move_in'] ) ) $this->setDisplayCompletedUntilMoveIn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_completed_until_move_in'] ) : $arrValues['display_completed_until_move_in'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setGroupByProblems( $intGroupByProblems ) {
		$this->set( 'm_intGroupByProblems', CStrings::strToIntDef( $intGroupByProblems, NULL, false ) );
	}

	public function getGroupByProblems() {
		return $this->m_intGroupByProblems;
	}

	public function sqlGroupByProblems() {
		return ( true == isset( $this->m_intGroupByProblems ) ) ? ( string ) $this->m_intGroupByProblems : 'NULL';
	}

	public function setShowInspectionColumn( $boolShowInspectionColumn ) {
		$this->set( 'm_boolShowInspectionColumn', CStrings::strToBool( $boolShowInspectionColumn ) );
	}

	public function getShowInspectionColumn() {
		return $this->m_boolShowInspectionColumn;
	}

	public function sqlShowInspectionColumn() {
		return ( true == isset( $this->m_boolShowInspectionColumn ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInspectionColumn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowFloorplanOnUnitHover( $boolShowFloorplanOnUnitHover ) {
		$this->set( 'm_boolShowFloorplanOnUnitHover', CStrings::strToBool( $boolShowFloorplanOnUnitHover ) );
	}

	public function getShowFloorplanOnUnitHover() {
		return $this->m_boolShowFloorplanOnUnitHover;
	}

	public function sqlShowFloorplanOnUnitHover() {
		return ( true == isset( $this->m_boolShowFloorplanOnUnitHover ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowFloorplanOnUnitHover ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontShowEndDatesAndDaysRemaining( $boolDontShowEndDatesAndDaysRemaining ) {
		$this->set( 'm_boolDontShowEndDatesAndDaysRemaining', CStrings::strToBool( $boolDontShowEndDatesAndDaysRemaining ) );
	}

	public function getDontShowEndDatesAndDaysRemaining() {
		return $this->m_boolDontShowEndDatesAndDaysRemaining;
	}

	public function sqlDontShowEndDatesAndDaysRemaining() {
		return ( true == isset( $this->m_boolDontShowEndDatesAndDaysRemaining ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontShowEndDatesAndDaysRemaining ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowAvailableOnDate( $boolShowAvailableOnDate ) {
		$this->set( 'm_boolShowAvailableOnDate', CStrings::strToBool( $boolShowAvailableOnDate ) );
	}

	public function getShowAvailableOnDate() {
		return $this->m_boolShowAvailableOnDate;
	}

	public function sqlShowAvailableOnDate() {
		return ( true == isset( $this->m_boolShowAvailableOnDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowAvailableOnDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisplayCompletedUntilMoveIn( $boolDisplayCompletedUntilMoveIn ) {
		$this->set( 'm_boolDisplayCompletedUntilMoveIn', CStrings::strToBool( $boolDisplayCompletedUntilMoveIn ) );
	}

	public function getDisplayCompletedUntilMoveIn() {
		return $this->m_boolDisplayCompletedUntilMoveIn;
	}

	public function sqlDisplayCompletedUntilMoveIn() {
		return ( true == isset( $this->m_boolDisplayCompletedUntilMoveIn ) ) ? '\'' . ( true == ( bool ) $this->m_boolDisplayCompletedUntilMoveIn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, system_code, name, description, group_by_problems, show_inspection_column, show_floorplan_on_unit_hover, dont_show_end_dates_and_days_remaining, show_available_on_date, display_completed_until_move_in, is_disabled, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlSystemCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlGroupByProblems() . ', ' .
						$this->sqlShowInspectionColumn() . ', ' .
						$this->sqlShowFloorplanOnUnitHover() . ', ' .
						$this->sqlDontShowEndDatesAndDaysRemaining() . ', ' .
						$this->sqlShowAvailableOnDate() . ', ' .
						$this->sqlDisplayCompletedUntilMoveIn() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode(). ',' ; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_by_problems = ' . $this->sqlGroupByProblems(). ',' ; } elseif( true == array_key_exists( 'GroupByProblems', $this->getChangedColumns() ) ) { $strSql .= ' group_by_problems = ' . $this->sqlGroupByProblems() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_inspection_column = ' . $this->sqlShowInspectionColumn(). ',' ; } elseif( true == array_key_exists( 'ShowInspectionColumn', $this->getChangedColumns() ) ) { $strSql .= ' show_inspection_column = ' . $this->sqlShowInspectionColumn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_floorplan_on_unit_hover = ' . $this->sqlShowFloorplanOnUnitHover(). ',' ; } elseif( true == array_key_exists( 'ShowFloorplanOnUnitHover', $this->getChangedColumns() ) ) { $strSql .= ' show_floorplan_on_unit_hover = ' . $this->sqlShowFloorplanOnUnitHover() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_show_end_dates_and_days_remaining = ' . $this->sqlDontShowEndDatesAndDaysRemaining(). ',' ; } elseif( true == array_key_exists( 'DontShowEndDatesAndDaysRemaining', $this->getChangedColumns() ) ) { $strSql .= ' dont_show_end_dates_and_days_remaining = ' . $this->sqlDontShowEndDatesAndDaysRemaining() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_available_on_date = ' . $this->sqlShowAvailableOnDate(). ',' ; } elseif( true == array_key_exists( 'ShowAvailableOnDate', $this->getChangedColumns() ) ) { $strSql .= ' show_available_on_date = ' . $this->sqlShowAvailableOnDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_completed_until_move_in = ' . $this->sqlDisplayCompletedUntilMoveIn(). ',' ; } elseif( true == array_key_exists( 'DisplayCompletedUntilMoveIn', $this->getChangedColumns() ) ) { $strSql .= ' display_completed_until_move_in = ' . $this->sqlDisplayCompletedUntilMoveIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'group_by_problems' => $this->getGroupByProblems(),
			'show_inspection_column' => $this->getShowInspectionColumn(),
			'show_floorplan_on_unit_hover' => $this->getShowFloorplanOnUnitHover(),
			'dont_show_end_dates_and_days_remaining' => $this->getDontShowEndDatesAndDaysRemaining(),
			'show_available_on_date' => $this->getShowAvailableOnDate(),
			'display_completed_until_move_in' => $this->getDisplayCompletedUntilMoveIn(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>