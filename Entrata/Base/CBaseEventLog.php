<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEventLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.event_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intPriorEventLogId;
	protected $m_intEventTypeId;
	protected $m_intEventSubTypeId;
	protected $m_intEventResultId;
	protected $m_intDefaultEventResultId;
	protected $m_intEventId;
	protected $m_intAssociatedEventId;
	protected $m_intPsProductId;
	protected $m_intOldStageId;
	protected $m_intNewStageId;
	protected $m_intOldStatusId;
	protected $m_intNewStatusId;
	protected $m_intCompanyEmployeeId;
	protected $m_intDataReferenceId;
	protected $m_intIntegrationResultId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intCustomerId;
	protected $m_strRemotePrimaryKey;
	protected $m_strCalendarEventKey;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strLogDatetime;
	protected $m_strScheduledDatetime;
	protected $m_strScheduledEndDatetime;
	protected $m_strEventDatetime;
	protected $m_strEventHandle;
	protected $m_strTitle;
	protected $m_strNotes;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strIpAddress;
	protected $m_boolDoNotExport;
	protected $m_boolIsResident;
	protected $m_boolIsDeleted;
	protected $m_boolIsPostMonthIgnored;
	protected $m_boolIsPostDateIgnored;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_boolDoNotExport = false;
		$this->m_boolIsResident = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsPostMonthIgnored = false;
		$this->m_boolIsPostDateIgnored = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['prior_event_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorEventLogId', trim( $arrValues['prior_event_log_id'] ) ); elseif( isset( $arrValues['prior_event_log_id'] ) ) $this->setPriorEventLogId( $arrValues['prior_event_log_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventSubTypeId', trim( $arrValues['event_sub_type_id'] ) ); elseif( isset( $arrValues['event_sub_type_id'] ) ) $this->setEventSubTypeId( $arrValues['event_sub_type_id'] );
		if( isset( $arrValues['event_result_id'] ) && $boolDirectSet ) $this->set( 'm_intEventResultId', trim( $arrValues['event_result_id'] ) ); elseif( isset( $arrValues['event_result_id'] ) ) $this->setEventResultId( $arrValues['event_result_id'] );
		if( isset( $arrValues['default_event_result_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultEventResultId', trim( $arrValues['default_event_result_id'] ) ); elseif( isset( $arrValues['default_event_result_id'] ) ) $this->setDefaultEventResultId( $arrValues['default_event_result_id'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['associated_event_id'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedEventId', trim( $arrValues['associated_event_id'] ) ); elseif( isset( $arrValues['associated_event_id'] ) ) $this->setAssociatedEventId( $arrValues['associated_event_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['old_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStageId', trim( $arrValues['old_stage_id'] ) ); elseif( isset( $arrValues['old_stage_id'] ) ) $this->setOldStageId( $arrValues['old_stage_id'] );
		if( isset( $arrValues['new_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStageId', trim( $arrValues['new_stage_id'] ) ); elseif( isset( $arrValues['new_stage_id'] ) ) $this->setNewStageId( $arrValues['new_stage_id'] );
		if( isset( $arrValues['old_status_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStatusId', trim( $arrValues['old_status_id'] ) ); elseif( isset( $arrValues['old_status_id'] ) ) $this->setOldStatusId( $arrValues['old_status_id'] );
		if( isset( $arrValues['new_status_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStatusId', trim( $arrValues['new_status_id'] ) ); elseif( isset( $arrValues['new_status_id'] ) ) $this->setNewStatusId( $arrValues['new_status_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['data_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intDataReferenceId', trim( $arrValues['data_reference_id'] ) ); elseif( isset( $arrValues['data_reference_id'] ) ) $this->setDataReferenceId( $arrValues['data_reference_id'] );
		if( isset( $arrValues['integration_result_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationResultId', trim( $arrValues['integration_result_id'] ) ); elseif( isset( $arrValues['integration_result_id'] ) ) $this->setIntegrationResultId( $arrValues['integration_result_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['calendar_event_key'] ) && $boolDirectSet ) $this->set( 'm_strCalendarEventKey', trim( stripcslashes( $arrValues['calendar_event_key'] ) ) ); elseif( isset( $arrValues['calendar_event_key'] ) ) $this->setCalendarEventKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['calendar_event_key'] ) : $arrValues['calendar_event_key'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['scheduled_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledDatetime', trim( $arrValues['scheduled_datetime'] ) ); elseif( isset( $arrValues['scheduled_datetime'] ) ) $this->setScheduledDatetime( $arrValues['scheduled_datetime'] );
		if( isset( $arrValues['scheduled_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndDatetime', trim( $arrValues['scheduled_end_datetime'] ) ); elseif( isset( $arrValues['scheduled_end_datetime'] ) ) $this->setScheduledEndDatetime( $arrValues['scheduled_end_datetime'] );
		if( isset( $arrValues['event_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEventDatetime', trim( $arrValues['event_datetime'] ) ); elseif( isset( $arrValues['event_datetime'] ) ) $this->setEventDatetime( $arrValues['event_datetime'] );
		if( isset( $arrValues['event_handle'] ) && $boolDirectSet ) $this->set( 'm_strEventHandle', trim( stripcslashes( $arrValues['event_handle'] ) ) ); elseif( isset( $arrValues['event_handle'] ) ) $this->setEventHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_handle'] ) : $arrValues['event_handle'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['do_not_export'] ) && $boolDirectSet ) $this->set( 'm_boolDoNotExport', trim( stripcslashes( $arrValues['do_not_export'] ) ) ); elseif( isset( $arrValues['do_not_export'] ) ) $this->setDoNotExport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['do_not_export'] ) : $arrValues['do_not_export'] );
		if( isset( $arrValues['is_resident'] ) && $boolDirectSet ) $this->set( 'm_boolIsResident', trim( stripcslashes( $arrValues['is_resident'] ) ) ); elseif( isset( $arrValues['is_resident'] ) ) $this->setIsResident( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resident'] ) : $arrValues['is_resident'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostMonthIgnored', trim( stripcslashes( $arrValues['is_post_month_ignored'] ) ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_month_ignored'] ) : $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostDateIgnored', trim( stripcslashes( $arrValues['is_post_date_ignored'] ) ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_date_ignored'] ) : $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPriorEventLogId( $intPriorEventLogId ) {
		$this->set( 'm_intPriorEventLogId', CStrings::strToIntDef( $intPriorEventLogId, NULL, false ) );
	}

	public function getPriorEventLogId() {
		return $this->m_intPriorEventLogId;
	}

	public function sqlPriorEventLogId() {
		return ( true == isset( $this->m_intPriorEventLogId ) ) ? ( string ) $this->m_intPriorEventLogId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setEventSubTypeId( $intEventSubTypeId ) {
		$this->set( 'm_intEventSubTypeId', CStrings::strToIntDef( $intEventSubTypeId, NULL, false ) );
	}

	public function getEventSubTypeId() {
		return $this->m_intEventSubTypeId;
	}

	public function sqlEventSubTypeId() {
		return ( true == isset( $this->m_intEventSubTypeId ) ) ? ( string ) $this->m_intEventSubTypeId : 'NULL';
	}

	public function setEventResultId( $intEventResultId ) {
		$this->set( 'm_intEventResultId', CStrings::strToIntDef( $intEventResultId, NULL, false ) );
	}

	public function getEventResultId() {
		return $this->m_intEventResultId;
	}

	public function sqlEventResultId() {
		return ( true == isset( $this->m_intEventResultId ) ) ? ( string ) $this->m_intEventResultId : 'NULL';
	}

	public function setDefaultEventResultId( $intDefaultEventResultId ) {
		$this->set( 'm_intDefaultEventResultId', CStrings::strToIntDef( $intDefaultEventResultId, NULL, false ) );
	}

	public function getDefaultEventResultId() {
		return $this->m_intDefaultEventResultId;
	}

	public function sqlDefaultEventResultId() {
		return ( true == isset( $this->m_intDefaultEventResultId ) ) ? ( string ) $this->m_intDefaultEventResultId : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setAssociatedEventId( $intAssociatedEventId ) {
		$this->set( 'm_intAssociatedEventId', CStrings::strToIntDef( $intAssociatedEventId, NULL, false ) );
	}

	public function getAssociatedEventId() {
		return $this->m_intAssociatedEventId;
	}

	public function sqlAssociatedEventId() {
		return ( true == isset( $this->m_intAssociatedEventId ) ) ? ( string ) $this->m_intAssociatedEventId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setOldStageId( $intOldStageId ) {
		$this->set( 'm_intOldStageId', CStrings::strToIntDef( $intOldStageId, NULL, false ) );
	}

	public function getOldStageId() {
		return $this->m_intOldStageId;
	}

	public function sqlOldStageId() {
		return ( true == isset( $this->m_intOldStageId ) ) ? ( string ) $this->m_intOldStageId : 'NULL';
	}

	public function setNewStageId( $intNewStageId ) {
		$this->set( 'm_intNewStageId', CStrings::strToIntDef( $intNewStageId, NULL, false ) );
	}

	public function getNewStageId() {
		return $this->m_intNewStageId;
	}

	public function sqlNewStageId() {
		return ( true == isset( $this->m_intNewStageId ) ) ? ( string ) $this->m_intNewStageId : 'NULL';
	}

	public function setOldStatusId( $intOldStatusId ) {
		$this->set( 'm_intOldStatusId', CStrings::strToIntDef( $intOldStatusId, NULL, false ) );
	}

	public function getOldStatusId() {
		return $this->m_intOldStatusId;
	}

	public function sqlOldStatusId() {
		return ( true == isset( $this->m_intOldStatusId ) ) ? ( string ) $this->m_intOldStatusId : 'NULL';
	}

	public function setNewStatusId( $intNewStatusId ) {
		$this->set( 'm_intNewStatusId', CStrings::strToIntDef( $intNewStatusId, NULL, false ) );
	}

	public function getNewStatusId() {
		return $this->m_intNewStatusId;
	}

	public function sqlNewStatusId() {
		return ( true == isset( $this->m_intNewStatusId ) ) ? ( string ) $this->m_intNewStatusId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setDataReferenceId( $intDataReferenceId ) {
		$this->set( 'm_intDataReferenceId', CStrings::strToIntDef( $intDataReferenceId, NULL, false ) );
	}

	public function getDataReferenceId() {
		return $this->m_intDataReferenceId;
	}

	public function sqlDataReferenceId() {
		return ( true == isset( $this->m_intDataReferenceId ) ) ? ( string ) $this->m_intDataReferenceId : 'NULL';
	}

	public function setIntegrationResultId( $intIntegrationResultId ) {
		$this->set( 'm_intIntegrationResultId', CStrings::strToIntDef( $intIntegrationResultId, NULL, false ) );
	}

	public function getIntegrationResultId() {
		return $this->m_intIntegrationResultId;
	}

	public function sqlIntegrationResultId() {
		return ( true == isset( $this->m_intIntegrationResultId ) ) ? ( string ) $this->m_intIntegrationResultId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 40, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCalendarEventKey( $strCalendarEventKey ) {
		$this->set( 'm_strCalendarEventKey', CStrings::strTrimDef( $strCalendarEventKey, 240, NULL, true ) );
	}

	public function getCalendarEventKey() {
		return $this->m_strCalendarEventKey;
	}

	public function sqlCalendarEventKey() {
		return ( true == isset( $this->m_strCalendarEventKey ) ) ? '\'' . addslashes( $this->m_strCalendarEventKey ) . '\'' : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setScheduledDatetime( $strScheduledDatetime ) {
		$this->set( 'm_strScheduledDatetime', CStrings::strTrimDef( $strScheduledDatetime, -1, NULL, true ) );
	}

	public function getScheduledDatetime() {
		return $this->m_strScheduledDatetime;
	}

	public function sqlScheduledDatetime() {
		return ( true == isset( $this->m_strScheduledDatetime ) ) ? '\'' . $this->m_strScheduledDatetime . '\'' : 'NOW()';
	}

	public function setScheduledEndDatetime( $strScheduledEndDatetime ) {
		$this->set( 'm_strScheduledEndDatetime', CStrings::strTrimDef( $strScheduledEndDatetime, -1, NULL, true ) );
	}

	public function getScheduledEndDatetime() {
		return $this->m_strScheduledEndDatetime;
	}

	public function sqlScheduledEndDatetime() {
		return ( true == isset( $this->m_strScheduledEndDatetime ) ) ? '\'' . $this->m_strScheduledEndDatetime . '\'' : 'NULL';
	}

	public function setEventDatetime( $strEventDatetime ) {
		$this->set( 'm_strEventDatetime', CStrings::strTrimDef( $strEventDatetime, -1, NULL, true ) );
	}

	public function getEventDatetime() {
		return $this->m_strEventDatetime;
	}

	public function sqlEventDatetime() {
		return ( true == isset( $this->m_strEventDatetime ) ) ? '\'' . $this->m_strEventDatetime . '\'' : 'NOW()';
	}

	public function setEventHandle( $strEventHandle ) {
		$this->set( 'm_strEventHandle', CStrings::strTrimDef( $strEventHandle, -1, NULL, true ) );
	}

	public function getEventHandle() {
		return $this->m_strEventHandle;
	}

	public function sqlEventHandle() {
		return ( true == isset( $this->m_strEventHandle ) ) ? '\'' . addslashes( $this->m_strEventHandle ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 50, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setDoNotExport( $boolDoNotExport ) {
		$this->set( 'm_boolDoNotExport', CStrings::strToBool( $boolDoNotExport ) );
	}

	public function getDoNotExport() {
		return $this->m_boolDoNotExport;
	}

	public function sqlDoNotExport() {
		return ( true == isset( $this->m_boolDoNotExport ) ) ? '\'' . ( true == ( bool ) $this->m_boolDoNotExport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResident( $boolIsResident ) {
		$this->set( 'm_boolIsResident', CStrings::strToBool( $boolIsResident ) );
	}

	public function getIsResident() {
		return $this->m_boolIsResident;
	}

	public function sqlIsResident() {
		return ( true == isset( $this->m_boolIsResident ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResident ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostMonthIgnored( $boolIsPostMonthIgnored ) {
		$this->set( 'm_boolIsPostMonthIgnored', CStrings::strToBool( $boolIsPostMonthIgnored ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_boolIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_boolIsPostMonthIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostMonthIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostDateIgnored( $boolIsPostDateIgnored ) {
		$this->set( 'm_boolIsPostDateIgnored', CStrings::strToBool( $boolIsPostDateIgnored ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_boolIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_boolIsPostDateIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostDateIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, unit_space_id, prior_event_log_id, event_type_id, event_sub_type_id, event_result_id, default_event_result_id, event_id, associated_event_id, ps_product_id, old_stage_id, new_stage_id, old_status_id, new_status_id, company_employee_id, data_reference_id, integration_result_id, period_id, reporting_period_id, effective_period_id, original_period_id, lease_id, lease_interval_id, customer_id, remote_primary_key, calendar_event_key, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, log_datetime, scheduled_datetime, scheduled_end_datetime, event_datetime, event_handle, title, notes, details, ip_address, do_not_export, is_resident, is_deleted, is_post_month_ignored, is_post_date_ignored, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlPriorEventLogId() . ', ' .
						$this->sqlEventTypeId() . ', ' .
						$this->sqlEventSubTypeId() . ', ' .
						$this->sqlEventResultId() . ', ' .
						$this->sqlDefaultEventResultId() . ', ' .
						$this->sqlEventId() . ', ' .
						$this->sqlAssociatedEventId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlOldStageId() . ', ' .
						$this->sqlNewStageId() . ', ' .
						$this->sqlOldStatusId() . ', ' .
						$this->sqlNewStatusId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlDataReferenceId() . ', ' .
						$this->sqlIntegrationResultId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlLeaseIntervalId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlCalendarEventKey() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlScheduledDatetime() . ', ' .
						$this->sqlScheduledEndDatetime() . ', ' .
						$this->sqlEventDatetime() . ', ' .
						$this->sqlEventHandle() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlDoNotExport() . ', ' .
						$this->sqlIsResident() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_event_log_id = ' . $this->sqlPriorEventLogId(). ',' ; } elseif( true == array_key_exists( 'PriorEventLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_event_log_id = ' . $this->sqlPriorEventLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId(). ',' ; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId(). ',' ; } elseif( true == array_key_exists( 'EventSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_result_id = ' . $this->sqlEventResultId(). ',' ; } elseif( true == array_key_exists( 'EventResultId', $this->getChangedColumns() ) ) { $strSql .= ' event_result_id = ' . $this->sqlEventResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_event_result_id = ' . $this->sqlDefaultEventResultId(). ',' ; } elseif( true == array_key_exists( 'DefaultEventResultId', $this->getChangedColumns() ) ) { $strSql .= ' default_event_result_id = ' . $this->sqlDefaultEventResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId(). ',' ; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_event_id = ' . $this->sqlAssociatedEventId(). ',' ; } elseif( true == array_key_exists( 'AssociatedEventId', $this->getChangedColumns() ) ) { $strSql .= ' associated_event_id = ' . $this->sqlAssociatedEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_stage_id = ' . $this->sqlOldStageId(). ',' ; } elseif( true == array_key_exists( 'OldStageId', $this->getChangedColumns() ) ) { $strSql .= ' old_stage_id = ' . $this->sqlOldStageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_stage_id = ' . $this->sqlNewStageId(). ',' ; } elseif( true == array_key_exists( 'NewStageId', $this->getChangedColumns() ) ) { $strSql .= ' new_stage_id = ' . $this->sqlNewStageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId(). ',' ; } elseif( true == array_key_exists( 'OldStatusId', $this->getChangedColumns() ) ) { $strSql .= ' old_status_id = ' . $this->sqlOldStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId(). ',' ; } elseif( true == array_key_exists( 'NewStatusId', $this->getChangedColumns() ) ) { $strSql .= ' new_status_id = ' . $this->sqlNewStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_reference_id = ' . $this->sqlDataReferenceId(). ',' ; } elseif( true == array_key_exists( 'DataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' data_reference_id = ' . $this->sqlDataReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId(). ',' ; } elseif( true == array_key_exists( 'IntegrationResultId', $this->getChangedColumns() ) ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_event_key = ' . $this->sqlCalendarEventKey(). ',' ; } elseif( true == array_key_exists( 'CalendarEventKey', $this->getChangedColumns() ) ) { $strSql .= ' calendar_event_key = ' . $this->sqlCalendarEventKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_datetime = ' . $this->sqlScheduledDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_datetime = ' . $this->sqlScheduledEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime(). ',' ; } elseif( true == array_key_exists( 'EventDatetime', $this->getChangedColumns() ) ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_handle = ' . $this->sqlEventHandle(). ',' ; } elseif( true == array_key_exists( 'EventHandle', $this->getChangedColumns() ) ) { $strSql .= ' event_handle = ' . $this->sqlEventHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_export = ' . $this->sqlDoNotExport(). ',' ; } elseif( true == array_key_exists( 'DoNotExport', $this->getChangedColumns() ) ) { $strSql .= ' do_not_export = ' . $this->sqlDoNotExport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident = ' . $this->sqlIsResident(). ',' ; } elseif( true == array_key_exists( 'IsResident', $this->getChangedColumns() ) ) { $strSql .= ' is_resident = ' . $this->sqlIsResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'prior_event_log_id' => $this->getPriorEventLogId(),
			'event_type_id' => $this->getEventTypeId(),
			'event_sub_type_id' => $this->getEventSubTypeId(),
			'event_result_id' => $this->getEventResultId(),
			'default_event_result_id' => $this->getDefaultEventResultId(),
			'event_id' => $this->getEventId(),
			'associated_event_id' => $this->getAssociatedEventId(),
			'ps_product_id' => $this->getPsProductId(),
			'old_stage_id' => $this->getOldStageId(),
			'new_stage_id' => $this->getNewStageId(),
			'old_status_id' => $this->getOldStatusId(),
			'new_status_id' => $this->getNewStatusId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'data_reference_id' => $this->getDataReferenceId(),
			'integration_result_id' => $this->getIntegrationResultId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'customer_id' => $this->getCustomerId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'calendar_event_key' => $this->getCalendarEventKey(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'log_datetime' => $this->getLogDatetime(),
			'scheduled_datetime' => $this->getScheduledDatetime(),
			'scheduled_end_datetime' => $this->getScheduledEndDatetime(),
			'event_datetime' => $this->getEventDatetime(),
			'event_handle' => $this->getEventHandle(),
			'title' => $this->getTitle(),
			'notes' => $this->getNotes(),
			'details' => $this->getDetails(),
			'ip_address' => $this->getIpAddress(),
			'do_not_export' => $this->getDoNotExport(),
			'is_resident' => $this->getIsResident(),
			'is_deleted' => $this->getIsDeleted(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>