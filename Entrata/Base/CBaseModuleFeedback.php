<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseModuleFeedback extends CEosSingularBase {

	const TABLE_NAME = 'public.module_feedbacks';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intModuleId;
	protected $m_strSatisfactionRating;
	protected $m_arrstrImprovementAreas;
	protected $m_strFeedback;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strFeedback = '\'NULL\'::text';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['module_id'] ) && $boolDirectSet ) $this->set( 'm_intModuleId', trim( $arrValues['module_id'] ) ); elseif( isset( $arrValues['module_id'] ) ) $this->setModuleId( $arrValues['module_id'] );
		if( isset( $arrValues['satisfaction_rating'] ) && $boolDirectSet ) $this->set( 'm_strSatisfactionRating', trim( $arrValues['satisfaction_rating'] ) ); elseif( isset( $arrValues['satisfaction_rating'] ) ) $this->setSatisfactionRating( $arrValues['satisfaction_rating'] );
		if( isset( $arrValues['improvement_areas'] ) && $boolDirectSet ) $this->set( 'm_arrstrImprovementAreas', trim( $arrValues['improvement_areas'] ) ); elseif( isset( $arrValues['improvement_areas'] ) ) $this->setImprovementAreas( $arrValues['improvement_areas'] );
		if( isset( $arrValues['feedback'] ) && $boolDirectSet ) $this->set( 'm_strFeedback', trim( $arrValues['feedback'] ) ); elseif( isset( $arrValues['feedback'] ) ) $this->setFeedback( $arrValues['feedback'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setModuleId( $intModuleId ) {
		$this->set( 'm_intModuleId', CStrings::strToIntDef( $intModuleId, NULL, false ) );
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : 'NULL';
	}

	public function setSatisfactionRating( $strSatisfactionRating ) {
		$this->set( 'm_strSatisfactionRating', CStrings::strTrimDef( $strSatisfactionRating, -1, NULL, true ) );
	}

	public function getSatisfactionRating() {
		return $this->m_strSatisfactionRating;
	}

	public function sqlSatisfactionRating() {
		return ( true == isset( $this->m_strSatisfactionRating ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSatisfactionRating ) : '\'' . addslashes( $this->m_strSatisfactionRating ) . '\'' ) : 'NULL';
	}

	public function setImprovementAreas( $arrstrImprovementAreas ) {
		$this->set( 'm_arrstrImprovementAreas', CStrings::strToArrIntDef( $arrstrImprovementAreas, NULL ) );
	}

	public function getImprovementAreas() {
		return $this->m_arrstrImprovementAreas;
	}

	public function sqlImprovementAreas() {
		return ( true == isset( $this->m_arrstrImprovementAreas ) && true == valArr( $this->m_arrstrImprovementAreas ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrImprovementAreas, NULL ) . '\'' : 'NULL';
	}

	public function setFeedback( $strFeedback ) {
		$this->set( 'm_strFeedback', CStrings::strTrimDef( $strFeedback, -1, NULL, true, true ) );
	}

	public function getFeedback() {
		return $this->m_strFeedback;
	}

	public function sqlFeedback() {
		return ( true == isset( $this->m_strFeedback ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFeedback ) : '\'' . addslashes( $this->m_strFeedback ) . '\'' ) : '\'NULL\'';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, module_id, satisfaction_rating, improvement_areas, feedback, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlModuleId() . ', ' .
						$this->sqlSatisfactionRating() . ', ' .
						$this->sqlImprovementAreas() . ', ' .
						$this->sqlFeedback() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_id = ' . $this->sqlModuleId(). ',' ; } elseif( true == array_key_exists( 'ModuleId', $this->getChangedColumns() ) ) { $strSql .= ' module_id = ' . $this->sqlModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfaction_rating = ' . $this->sqlSatisfactionRating(). ',' ; } elseif( true == array_key_exists( 'SatisfactionRating', $this->getChangedColumns() ) ) { $strSql .= ' satisfaction_rating = ' . $this->sqlSatisfactionRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' improvement_areas = ' . $this->sqlImprovementAreas(). ',' ; } elseif( true == array_key_exists( 'ImprovementAreas', $this->getChangedColumns() ) ) { $strSql .= ' improvement_areas = ' . $this->sqlImprovementAreas() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feedback = ' . $this->sqlFeedback(). ',' ; } elseif( true == array_key_exists( 'Feedback', $this->getChangedColumns() ) ) { $strSql .= ' feedback = ' . $this->sqlFeedback() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'module_id' => $this->getModuleId(),
			'satisfaction_rating' => $this->getSatisfactionRating(),
			'improvement_areas' => $this->getImprovementAreas(),
			'feedback' => $this->getFeedback(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>