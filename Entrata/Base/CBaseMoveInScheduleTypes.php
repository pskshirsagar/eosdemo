<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInScheduleTypes
 * Do not add any new functions to this class.
 */

class CBaseMoveInScheduleTypes extends CEosPluralBase {

	/**
	 * @return CMoveInScheduleType[]
	 */
	public static function fetchMoveInScheduleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMoveInScheduleType::class, $objDatabase );
	}

	/**
	 * @return CMoveInScheduleType
	 */
	public static function fetchMoveInScheduleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMoveInScheduleType::class, $objDatabase );
	}

	public static function fetchMoveInScheduleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'move_in_schedule_types', $objDatabase );
	}

	public static function fetchMoveInScheduleTypeById( $intId, $objDatabase ) {
		return self::fetchMoveInScheduleType( sprintf( 'SELECT * FROM move_in_schedule_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>