<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.offer_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_arrintUnitTypeIds;
	protected $m_arrintLeaseStartWindowIds;
	protected $m_intApplicationStageStatusId;
	protected $m_strOfferTemplateTypeId;
	protected $m_intOccupancyTypeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strMoveOutStartDate;
	protected $m_strMoveOutEndDate;
	protected $m_strExpiryDate;
	protected $m_boolIsStrictDate;
	protected $m_boolIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImportOfferTemplateId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsStrictDate = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUnitTypeIds', trim( $arrValues['unit_type_ids'] ) ); elseif( isset( $arrValues['unit_type_ids'] ) ) $this->setUnitTypeIds( $arrValues['unit_type_ids'] );
		if( isset( $arrValues['lease_start_window_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintLeaseStartWindowIds', trim( $arrValues['lease_start_window_ids'] ) ); elseif( isset( $arrValues['lease_start_window_ids'] ) ) $this->setLeaseStartWindowIds( $arrValues['lease_start_window_ids'] );
		if( isset( $arrValues['application_stage_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageStatusId', trim( $arrValues['application_stage_status_id'] ) ); elseif( isset( $arrValues['application_stage_status_id'] ) ) $this->setApplicationStageStatusId( $arrValues['application_stage_status_id'] );
		if( isset( $arrValues['offer_template_type_id'] ) && $boolDirectSet ) $this->set( 'm_strOfferTemplateTypeId', trim( stripcslashes( $arrValues['offer_template_type_id'] ) ) ); elseif( isset( $arrValues['offer_template_type_id'] ) ) $this->setOfferTemplateTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['offer_template_type_id'] ) : $arrValues['offer_template_type_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['move_out_start_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutStartDate', trim( $arrValues['move_out_start_date'] ) ); elseif( isset( $arrValues['move_out_start_date'] ) ) $this->setMoveOutStartDate( $arrValues['move_out_start_date'] );
		if( isset( $arrValues['move_out_end_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutEndDate', trim( $arrValues['move_out_end_date'] ) ); elseif( isset( $arrValues['move_out_end_date'] ) ) $this->setMoveOutEndDate( $arrValues['move_out_end_date'] );
		if( isset( $arrValues['expiry_date'] ) && $boolDirectSet ) $this->set( 'm_strExpiryDate', trim( $arrValues['expiry_date'] ) ); elseif( isset( $arrValues['expiry_date'] ) ) $this->setExpiryDate( $arrValues['expiry_date'] );
		if( isset( $arrValues['is_strict_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsStrictDate', trim( stripcslashes( $arrValues['is_strict_date'] ) ) ); elseif( isset( $arrValues['is_strict_date'] ) ) $this->setIsStrictDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_strict_date'] ) : $arrValues['is_strict_date'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['import_offer_template_id'] ) && $boolDirectSet ) $this->set( 'm_intImportOfferTemplateId', trim( $arrValues['import_offer_template_id'] ) ); elseif( isset( $arrValues['import_offer_template_id'] ) ) $this->setImportOfferTemplateId( $arrValues['import_offer_template_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeIds( $arrintUnitTypeIds ) {
		$this->set( 'm_arrintUnitTypeIds', CStrings::strToArrIntDef( $arrintUnitTypeIds, NULL ) );
	}

	public function getUnitTypeIds() {
		return $this->m_arrintUnitTypeIds;
	}

	public function sqlUnitTypeIds() {
		return ( true == isset( $this->m_arrintUnitTypeIds ) && true == valArr( $this->m_arrintUnitTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUnitTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setLeaseStartWindowIds( $arrintLeaseStartWindowIds ) {
		$this->set( 'm_arrintLeaseStartWindowIds', CStrings::strToArrIntDef( $arrintLeaseStartWindowIds, NULL ) );
	}

	public function getLeaseStartWindowIds() {
		return $this->m_arrintLeaseStartWindowIds;
	}

	public function sqlLeaseStartWindowIds() {
		return ( true == isset( $this->m_arrintLeaseStartWindowIds ) && true == valArr( $this->m_arrintLeaseStartWindowIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintLeaseStartWindowIds, NULL ) . '\'' : 'NULL';
	}

	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->set( 'm_intApplicationStageStatusId', CStrings::strToIntDef( $intApplicationStageStatusId, NULL, false ) );
	}

	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	public function sqlApplicationStageStatusId() {
		return ( true == isset( $this->m_intApplicationStageStatusId ) ) ? ( string ) $this->m_intApplicationStageStatusId : 'NULL';
	}

	public function setOfferTemplateTypeId( $strOfferTemplateTypeId ) {
		$this->set( 'm_strOfferTemplateTypeId', CStrings::strTrimDef( $strOfferTemplateTypeId, -1, NULL, true ) );
	}

	public function getOfferTemplateTypeId() {
		return $this->m_strOfferTemplateTypeId;
	}

	public function sqlOfferTemplateTypeId() {
		return ( true == isset( $this->m_strOfferTemplateTypeId ) ) ? '\'' . addslashes( $this->m_strOfferTemplateTypeId ) . '\'' : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 50, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setMoveOutStartDate( $strMoveOutStartDate ) {
		$this->set( 'm_strMoveOutStartDate', CStrings::strTrimDef( $strMoveOutStartDate, -1, NULL, true ) );
	}

	public function getMoveOutStartDate() {
		return $this->m_strMoveOutStartDate;
	}

	public function sqlMoveOutStartDate() {
		return ( true == isset( $this->m_strMoveOutStartDate ) ) ? '\'' . $this->m_strMoveOutStartDate . '\'' : 'NULL';
	}

	public function setMoveOutEndDate( $strMoveOutEndDate ) {
		$this->set( 'm_strMoveOutEndDate', CStrings::strTrimDef( $strMoveOutEndDate, -1, NULL, true ) );
	}

	public function getMoveOutEndDate() {
		return $this->m_strMoveOutEndDate;
	}

	public function sqlMoveOutEndDate() {
		return ( true == isset( $this->m_strMoveOutEndDate ) ) ? '\'' . $this->m_strMoveOutEndDate . '\'' : 'NULL';
	}

	public function setExpiryDate( $strExpiryDate ) {
		$this->set( 'm_strExpiryDate', CStrings::strTrimDef( $strExpiryDate, -1, NULL, true ) );
	}

	public function getExpiryDate() {
		return $this->m_strExpiryDate;
	}

	public function sqlExpiryDate() {
		return ( true == isset( $this->m_strExpiryDate ) ) ? '\'' . $this->m_strExpiryDate . '\'' : 'NOW()';
	}

	public function setIsStrictDate( $boolIsStrictDate ) {
		$this->set( 'm_boolIsStrictDate', CStrings::strToBool( $boolIsStrictDate ) );
	}

	public function getIsStrictDate() {
		return $this->m_boolIsStrictDate;
	}

	public function sqlIsStrictDate() {
		return ( true == isset( $this->m_boolIsStrictDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStrictDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImportOfferTemplateId( $intImportOfferTemplateId ) {
		$this->set( 'm_intImportOfferTemplateId', CStrings::strToIntDef( $intImportOfferTemplateId, NULL, false ) );
	}

	public function getImportOfferTemplateId() {
		return $this->m_intImportOfferTemplateId;
	}

	public function sqlImportOfferTemplateId() {
		return ( true == isset( $this->m_intImportOfferTemplateId ) ) ? ( string ) $this->m_intImportOfferTemplateId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_ids, lease_start_window_ids, application_stage_status_id, offer_template_type_id, occupancy_type_id, lease_interval_type_id, name, description, move_out_start_date, move_out_end_date, expiry_date, is_strict_date, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, import_offer_template_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUnitTypeIds() . ', ' .
 						$this->sqlLeaseStartWindowIds() . ', ' .
 						$this->sqlApplicationStageStatusId() . ', ' .
 						$this->sqlOfferTemplateTypeId() . ', ' .
 						$this->sqlOccupancyTypeId() . ', ' .
 						$this->sqlLeaseIntervalTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlMoveOutStartDate() . ', ' .
 						$this->sqlMoveOutEndDate() . ', ' .
 						$this->sqlExpiryDate() . ', ' .
 						$this->sqlIsStrictDate() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlImportOfferTemplateId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_ids = ' . $this->sqlUnitTypeIds() . ','; } elseif( true == array_key_exists( 'UnitTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_ids = ' . $this->sqlUnitTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_ids = ' . $this->sqlLeaseStartWindowIds() . ','; } elseif( true == array_key_exists( 'LeaseStartWindowIds', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_ids = ' . $this->sqlLeaseStartWindowIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId() . ','; } elseif( true == array_key_exists( 'ApplicationStageStatusId', $this->getChangedColumns() ) ) { $strSql .= ' application_stage_status_id = ' . $this->sqlApplicationStageStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_template_type_id = ' . $this->sqlOfferTemplateTypeId() . ','; } elseif( true == array_key_exists( 'OfferTemplateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' offer_template_type_id = ' . $this->sqlOfferTemplateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_start_date = ' . $this->sqlMoveOutStartDate() . ','; } elseif( true == array_key_exists( 'MoveOutStartDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_start_date = ' . $this->sqlMoveOutStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_end_date = ' . $this->sqlMoveOutEndDate() . ','; } elseif( true == array_key_exists( 'MoveOutEndDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_end_date = ' . $this->sqlMoveOutEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; } elseif( true == array_key_exists( 'ExpiryDate', $this->getChangedColumns() ) ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_strict_date = ' . $this->sqlIsStrictDate() . ','; } elseif( true == array_key_exists( 'IsStrictDate', $this->getChangedColumns() ) ) { $strSql .= ' is_strict_date = ' . $this->sqlIsStrictDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_offer_template_id = ' . $this->sqlImportOfferTemplateId() . ','; } elseif( true == array_key_exists( 'ImportOfferTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' import_offer_template_id = ' . $this->sqlImportOfferTemplateId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_ids' => $this->getUnitTypeIds(),
			'lease_start_window_ids' => $this->getLeaseStartWindowIds(),
			'application_stage_status_id' => $this->getApplicationStageStatusId(),
			'offer_template_type_id' => $this->getOfferTemplateTypeId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'move_out_start_date' => $this->getMoveOutStartDate(),
			'move_out_end_date' => $this->getMoveOutEndDate(),
			'expiry_date' => $this->getExpiryDate(),
			'is_strict_date' => $this->getIsStrictDate(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'import_offer_template_id' => $this->getImportOfferTemplateId()
		);
	}

}
?>