<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationScheduledCharges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyCertificationScheduledCharges extends CEosPluralBase {

	/**
	 * @return CSubsidyCertificationScheduledCharge[]
	 */
	public static function fetchSubsidyCertificationScheduledCharges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyCertificationScheduledCharge', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyCertificationScheduledCharge
	 */
	public static function fetchSubsidyCertificationScheduledCharge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyCertificationScheduledCharge', $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certification_scheduled_charges', $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharge( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargesByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharges( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharges( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharges( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargesBySubsidyCertificationIdByCid( $intSubsidyCertificationId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharges( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE subsidy_certification_id = %d AND cid = %d', ( int ) $intSubsidyCertificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationScheduledChargesByScheduledChargeIdByCid( $intScheduledChargeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertificationScheduledCharges( sprintf( 'SELECT * FROM subsidy_certification_scheduled_charges WHERE scheduled_charge_id = %d AND cid = %d', ( int ) $intScheduledChargeId, ( int ) $intCid ), $objDatabase );
	}

}
?>