<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationSubmissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationSubmissions extends CEosPluralBase {

	/**
	 * @return CApplicantApplicationSubmission[]
	 */
	public static function fetchApplicantApplicationSubmissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantApplicationSubmission', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantApplicationSubmission
	 */
	public static function fetchApplicantApplicationSubmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantApplicationSubmission', $objDatabase );
	}

	public static function fetchApplicantApplicationSubmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_application_submissions', $objDatabase );
	}

	public static function fetchApplicantApplicationSubmissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationSubmission( sprintf( 'SELECT * FROM applicant_application_submissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationSubmissionsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationSubmissions( sprintf( 'SELECT * FROM applicant_application_submissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationSubmissionsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationSubmissions( sprintf( 'SELECT * FROM applicant_application_submissions WHERE applicant_application_id = %d AND cid = %d', ( int ) $intApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantApplicationSubmissionsByApplicationSubmissionTypeIdByCid( $intApplicationSubmissionTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationSubmissions( sprintf( 'SELECT * FROM applicant_application_submissions WHERE application_submission_type_id = %d AND cid = %d', ( int ) $intApplicationSubmissionTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>