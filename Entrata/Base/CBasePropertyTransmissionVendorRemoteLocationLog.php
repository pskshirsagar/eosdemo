<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransmissionVendorRemoteLocationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.property_transmission_vendor_remote_location_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyTransmissionVendorRemoteLocationId;
	protected $m_intPropertyTransmissionVendorRemoteLocationStatusId;
	protected $m_intPreviousPropertyTransmissionVendorRemoteLocationStatusId;
	protected $m_intSystemEmailId;
	protected $m_strResponseText;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_transmission_vendor_remote_location_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTransmissionVendorRemoteLocationId', trim( $arrValues['property_transmission_vendor_remote_location_id'] ) ); elseif( isset( $arrValues['property_transmission_vendor_remote_location_id'] ) ) $this->setPropertyTransmissionVendorRemoteLocationId( $arrValues['property_transmission_vendor_remote_location_id'] );
		if( isset( $arrValues['property_transmission_vendor_remote_location_status_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTransmissionVendorRemoteLocationStatusId', trim( $arrValues['property_transmission_vendor_remote_location_status_id'] ) ); elseif( isset( $arrValues['property_transmission_vendor_remote_location_status_id'] ) ) $this->setPropertyTransmissionVendorRemoteLocationStatusId( $arrValues['property_transmission_vendor_remote_location_status_id'] );
		if( isset( $arrValues['previous_property_transmission_vendor_remote_location_status_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousPropertyTransmissionVendorRemoteLocationStatusId', trim( $arrValues['previous_property_transmission_vendor_remote_location_status_id'] ) ); elseif( isset( $arrValues['previous_property_transmission_vendor_remote_location_status_id'] ) ) $this->setPreviousPropertyTransmissionVendorRemoteLocationStatusId( $arrValues['previous_property_transmission_vendor_remote_location_status_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['response_text'] ) && $boolDirectSet ) $this->set( 'm_strResponseText', trim( stripcslashes( $arrValues['response_text'] ) ) ); elseif( isset( $arrValues['response_text'] ) ) $this->setResponseText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_text'] ) : $arrValues['response_text'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyTransmissionVendorRemoteLocationId( $intPropertyTransmissionVendorRemoteLocationId ) {
		$this->set( 'm_intPropertyTransmissionVendorRemoteLocationId', CStrings::strToIntDef( $intPropertyTransmissionVendorRemoteLocationId, NULL, false ) );
	}

	public function getPropertyTransmissionVendorRemoteLocationId() {
		return $this->m_intPropertyTransmissionVendorRemoteLocationId;
	}

	public function sqlPropertyTransmissionVendorRemoteLocationId() {
		return ( true == isset( $this->m_intPropertyTransmissionVendorRemoteLocationId ) ) ? ( string ) $this->m_intPropertyTransmissionVendorRemoteLocationId : 'NULL';
	}

	public function setPropertyTransmissionVendorRemoteLocationStatusId( $intPropertyTransmissionVendorRemoteLocationStatusId ) {
		$this->set( 'm_intPropertyTransmissionVendorRemoteLocationStatusId', CStrings::strToIntDef( $intPropertyTransmissionVendorRemoteLocationStatusId, NULL, false ) );
	}

	public function getPropertyTransmissionVendorRemoteLocationStatusId() {
		return $this->m_intPropertyTransmissionVendorRemoteLocationStatusId;
	}

	public function sqlPropertyTransmissionVendorRemoteLocationStatusId() {
		return ( true == isset( $this->m_intPropertyTransmissionVendorRemoteLocationStatusId ) ) ? ( string ) $this->m_intPropertyTransmissionVendorRemoteLocationStatusId : 'NULL';
	}

	public function setPreviousPropertyTransmissionVendorRemoteLocationStatusId( $intPreviousPropertyTransmissionVendorRemoteLocationStatusId ) {
		$this->set( 'm_intPreviousPropertyTransmissionVendorRemoteLocationStatusId', CStrings::strToIntDef( $intPreviousPropertyTransmissionVendorRemoteLocationStatusId, NULL, false ) );
	}

	public function getPreviousPropertyTransmissionVendorRemoteLocationStatusId() {
		return $this->m_intPreviousPropertyTransmissionVendorRemoteLocationStatusId;
	}

	public function sqlPreviousPropertyTransmissionVendorRemoteLocationStatusId() {
		return ( true == isset( $this->m_intPreviousPropertyTransmissionVendorRemoteLocationStatusId ) ) ? ( string ) $this->m_intPreviousPropertyTransmissionVendorRemoteLocationStatusId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setResponseText( $strResponseText ) {
		$this->set( 'm_strResponseText', CStrings::strTrimDef( $strResponseText, -1, NULL, true ) );
	}

	public function getResponseText() {
		return $this->m_strResponseText;
	}

	public function sqlResponseText() {
		return ( true == isset( $this->m_strResponseText ) ) ? '\'' . addslashes( $this->m_strResponseText ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_transmission_vendor_remote_location_id, property_transmission_vendor_remote_location_status_id, previous_property_transmission_vendor_remote_location_status_id, system_email_id, response_text, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyTransmissionVendorRemoteLocationId() . ', ' .
 						$this->sqlPropertyTransmissionVendorRemoteLocationStatusId() . ', ' .
 						$this->sqlPreviousPropertyTransmissionVendorRemoteLocationStatusId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlResponseText() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_transmission_vendor_remote_location_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationId() . ','; } elseif( true == array_key_exists( 'PropertyTransmissionVendorRemoteLocationId', $this->getChangedColumns() ) ) { $strSql .= ' property_transmission_vendor_remote_location_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_transmission_vendor_remote_location_status_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationStatusId() . ','; } elseif( true == array_key_exists( 'PropertyTransmissionVendorRemoteLocationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' property_transmission_vendor_remote_location_status_id = ' . $this->sqlPropertyTransmissionVendorRemoteLocationStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_property_transmission_vendor_remote_location_status_id = ' . $this->sqlPreviousPropertyTransmissionVendorRemoteLocationStatusId() . ','; } elseif( true == array_key_exists( 'PreviousPropertyTransmissionVendorRemoteLocationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' previous_property_transmission_vendor_remote_location_status_id = ' . $this->sqlPreviousPropertyTransmissionVendorRemoteLocationStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; } elseif( true == array_key_exists( 'ResponseText', $this->getChangedColumns() ) ) { $strSql .= ' response_text = ' . $this->sqlResponseText() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_transmission_vendor_remote_location_id' => $this->getPropertyTransmissionVendorRemoteLocationId(),
			'property_transmission_vendor_remote_location_status_id' => $this->getPropertyTransmissionVendorRemoteLocationStatusId(),
			'previous_property_transmission_vendor_remote_location_status_id' => $this->getPreviousPropertyTransmissionVendorRemoteLocationStatusId(),
			'system_email_id' => $this->getSystemEmailId(),
			'response_text' => $this->getResponseText(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>