<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayees1099Property extends CEosSingularBase {

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intPropertyId;
	protected $m_intApLegalEntityId;
	protected $m_fltAmount;
	protected $m_strYear;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->m_intApPayeeId = trim( $arrValues['ap_payee_id'] ); else if( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ap_legal_entity_id'] ) && $boolDirectSet ) $this->m_intApLegalEntityId = trim( $arrValues['ap_legal_entity_id'] ); else if( isset( $arrValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrValues['ap_legal_entity_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->m_fltAmount = trim( $arrValues['amount'] ); else if( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->m_strYear = trim( $arrValues['year'] ); else if( isset( $arrValues['year'] ) ) $this->setYear( $arrValues['year'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = CStrings::strToIntDef( $intApPayeeId, NULL, false );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? (string) $this->m_intApPayeeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->m_intApLegalEntityId = CStrings::strToIntDef( $intApLegalEntityId, NULL, false );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function sqlApLegalEntityId() {
		return ( true == isset( $this->m_intApLegalEntityId ) ) ? (string) $this->m_intApLegalEntityId : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->m_fltAmount = CStrings::strToFloatDef( $fltAmount, NULL, false, 2 );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? (string) $this->m_fltAmount : 'NULL';
	}

	public function setYear( $strYear ) {
		$this->m_strYear = CStrings::strTrimDef( $strYear, -1, NULL, true );
	}

	public function getYear() {
		return $this->m_strYear;
	}

	public function sqlYear() {
		return ( true == isset( $this->m_strYear ) ) ? '\'' . $this->m_strYear . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.ap_payees_1099_properties_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.ap_payees_1099_properties
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlApLegalEntityId() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlYear() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.ap_payees_1099_properties
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApPayeeId() ) != $this->getOriginalValueByFieldName ( 'ap_payee_id' ) ) { $arrstrOriginalValueChanges['ap_payee_id'] = $this->sqlApPayeeId(); $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApLegalEntityId() ) != $this->getOriginalValueByFieldName ( 'ap_legal_entity_id' ) ) { $arrstrOriginalValueChanges['ap_legal_entity_id'] = $this->sqlApLegalEntityId(); $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlAmount() ), $this->getOriginalValueByFieldName ( 'amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['amount'] = $this->sqlAmount(); $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year = ' . $this->sqlYear() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlYear() ) != $this->getOriginalValueByFieldName ( 'year' ) ) { $arrstrOriginalValueChanges['year'] = $this->sqlYear(); $strSql .= ' year = ' . $this->sqlYear() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.ap_payees_1099_properties WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.ap_payees_1099_properties_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'property_id' => $this->getPropertyId(),
			'ap_legal_entity_id' => $this->getApLegalEntityId(),
			'amount' => $this->getAmount(),
			'year' => $this->getYear(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>