<?php

class CBaseSubsidyErrorCode extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_error_codes';

	protected $m_intId;
	protected $m_strErrorCode;
	protected $m_strDescription;
	protected $m_strRecommendedSolution;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['error_code'] ) && $boolDirectSet ) $this->set( 'm_strErrorCode', trim( stripcslashes( $arrValues['error_code'] ) ) ); elseif( isset( $arrValues['error_code'] ) ) $this->setErrorCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_code'] ) : $arrValues['error_code'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['recommended_solution'] ) && $boolDirectSet ) $this->set( 'm_strRecommendedSolution', trim( stripcslashes( $arrValues['recommended_solution'] ) ) ); elseif( isset( $arrValues['recommended_solution'] ) ) $this->setRecommendedSolution( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['recommended_solution'] ) : $arrValues['recommended_solution'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setErrorCode( $strErrorCode ) {
		$this->set( 'm_strErrorCode', CStrings::strTrimDef( $strErrorCode, 40, NULL, true ) );
	}

	public function getErrorCode() {
		return $this->m_strErrorCode;
	}

	public function sqlErrorCode() {
		return ( true == isset( $this->m_strErrorCode ) ) ? '\'' . addslashes( $this->m_strErrorCode ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setRecommendedSolution( $strRecommendedSolution ) {
		$this->set( 'm_strRecommendedSolution', CStrings::strTrimDef( $strRecommendedSolution, 4096, NULL, true ) );
	}

	public function getRecommendedSolution() {
		return $this->m_strRecommendedSolution;
	}

	public function sqlRecommendedSolution() {
		return ( true == isset( $this->m_strRecommendedSolution ) ) ? '\'' . addslashes( $this->m_strRecommendedSolution ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'error_code' => $this->getErrorCode(),
			'description' => $this->getDescription(),
			'recommended_solution' => $this->getRecommendedSolution()
		);
	}

}
?>