<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallChunkTypes
 * Do not add any new functions to this class.
 */

class CBaseCallChunkTypes extends CEosPluralBase {

	/**
	 * @return CCallChunkType[]
	 */
	public static function fetchCallChunkTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallChunkType::class, $objDatabase );
	}

	/**
	 * @return CCallChunkType
	 */
	public static function fetchCallChunkType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallChunkType::class, $objDatabase );
	}

	public static function fetchCallChunkTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_chunk_types', $objDatabase );
	}

	public static function fetchCallChunkTypeById( $intId, $objDatabase ) {
		return self::fetchCallChunkType( sprintf( 'SELECT * FROM call_chunk_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>