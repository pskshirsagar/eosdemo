<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductReports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProductReports extends CEosPluralBase {

	/**
	 * @return CProductReport[]
	 */
	public static function fetchProductReports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CProductReport', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CProductReport
	 */
	public static function fetchProductReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProductReport', $objDatabase );
	}

	public static function fetchProductReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'product_reports', $objDatabase );
	}

	public static function fetchProductReportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchProductReport( sprintf( 'SELECT * FROM product_reports WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProductReportsByCid( $intCid, $objDatabase ) {
		return self::fetchProductReports( sprintf( 'SELECT * FROM product_reports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProductReportsByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchProductReports( sprintf( 'SELECT * FROM product_reports WHERE report_id = %d AND cid = %d', ( int ) $intReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProductReportsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchProductReports( sprintf( 'SELECT * FROM product_reports WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

}
?>