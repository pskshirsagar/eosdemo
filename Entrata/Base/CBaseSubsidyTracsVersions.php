<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyTracsVersions
 * Do not add any new functions to this class.
 */

class CBaseSubsidyTracsVersions extends CEosPluralBase {

	/**
	 * @return CSubsidyTracsVersion[]
	 */
	public static function fetchSubsidyTracsVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyTracsVersion::class, $objDatabase );
	}

	/**
	 * @return CSubsidyTracsVersion
	 */
	public static function fetchSubsidyTracsVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyTracsVersion::class, $objDatabase );
	}

	public static function fetchSubsidyTracsVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_tracs_versions', $objDatabase );
	}

	public static function fetchSubsidyTracsVersionById( $intId, $objDatabase ) {
		return self::fetchSubsidyTracsVersion( sprintf( 'SELECT * FROM subsidy_tracs_versions WHERE id = %d', $intId ), $objDatabase );
	}

}
?>