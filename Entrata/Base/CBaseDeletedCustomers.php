<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDeletedCustomers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDeletedCustomers extends CEosPluralBase {

	/**
	 * @return CDeletedCustomer[]
	 */
	public static function fetchDeletedCustomers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CDeletedCustomer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDeletedCustomer
	 */
	public static function fetchDeletedCustomer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDeletedCustomer', $objDatabase );
	}

	public static function fetchDeletedCustomerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'deleted_customers', $objDatabase );
	}

	public static function fetchDeletedCustomerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDeletedCustomer( sprintf( 'SELECT * FROM deleted_customers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDeletedCustomersByCid( $intCid, $objDatabase ) {
		return self::fetchDeletedCustomers( sprintf( 'SELECT * FROM deleted_customers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDeletedCustomersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDeletedCustomers( sprintf( 'SELECT * FROM deleted_customers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDeletedCustomersByNewCustomerIdByCid( $intNewCustomerId, $intCid, $objDatabase ) {
		return self::fetchDeletedCustomers( sprintf( 'SELECT * FROM deleted_customers WHERE new_customer_id = %d AND cid = %d', ( int ) $intNewCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDeletedCustomersByOldCustomerIdByCid( $intOldCustomerId, $intCid, $objDatabase ) {
		return self::fetchDeletedCustomers( sprintf( 'SELECT * FROM deleted_customers WHERE old_customer_id = %d AND cid = %d', ( int ) $intOldCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>