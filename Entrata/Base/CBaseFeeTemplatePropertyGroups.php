<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplatePropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeTemplatePropertyGroups extends CEosPluralBase {

	/**
	 * @return CFeeTemplatePropertyGroup[]
	 */
	public static function fetchFeeTemplatePropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFeeTemplatePropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFeeTemplatePropertyGroup
	 */
	public static function fetchFeeTemplatePropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeTemplatePropertyGroup::class, $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_template_property_groups', $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplatePropertyGroup( sprintf( 'SELECT * FROM fee_template_property_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchFeeTemplatePropertyGroups( sprintf( 'SELECT * FROM fee_template_property_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplatePropertyGroups( sprintf( 'SELECT * FROM fee_template_property_groups WHERE property_group_id = %d AND cid = %d', $intPropertyGroupId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplatePropertyGroupsByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplatePropertyGroups( sprintf( 'SELECT * FROM fee_template_property_groups WHERE fee_template_id = %d AND cid = %d', $intFeeTemplateId, $intCid ), $objDatabase );
	}

}
?>