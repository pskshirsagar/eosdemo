<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorkbookCompanyUsers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorkbookCompanyUsers extends CEosPluralBase {

	/**
	 * @return CBudgetWorkbookCompanyUser[]
	 */
	public static function fetchBudgetWorkbookCompanyUsers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorkbookCompanyUser::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorkbookCompanyUser
	 */
	public static function fetchBudgetWorkbookCompanyUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorkbookCompanyUser::class, $objDatabase );
	}

	public static function fetchBudgetWorkbookCompanyUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_workbook_company_users', $objDatabase );
	}

	public static function fetchBudgetWorkbookCompanyUserByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookCompanyUser( sprintf( 'SELECT * FROM budget_workbook_company_users WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookCompanyUsersByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookCompanyUsers( sprintf( 'SELECT * FROM budget_workbook_company_users WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookCompanyUsersByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookCompanyUsers( sprintf( 'SELECT * FROM budget_workbook_company_users WHERE budget_workbook_id = %d AND cid = %d', ( int ) $intBudgetWorkbookId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookCompanyUsersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookCompanyUsers( sprintf( 'SELECT * FROM budget_workbook_company_users WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>