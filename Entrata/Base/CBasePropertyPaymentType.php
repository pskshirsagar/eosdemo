<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPaymentType extends CEosSingularBase {

	const TABLE_NAME = 'public.property_payment_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPaymentTypeId;
	protected $m_boolAllowInEntrata;
	protected $m_boolAllowInResidentPortal;
	protected $m_boolAllowInProspectPortal;
	protected $m_boolIsCertifiedFunds;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowInEntrata = true;
		$this->m_boolAllowInResidentPortal = true;
		$this->m_boolAllowInProspectPortal = true;
		$this->m_boolIsCertifiedFunds = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['allow_in_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInEntrata', trim( stripcslashes( $arrValues['allow_in_entrata'] ) ) ); elseif( isset( $arrValues['allow_in_entrata'] ) ) $this->setAllowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_entrata'] ) : $arrValues['allow_in_entrata'] );
		if( isset( $arrValues['allow_in_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInResidentPortal', trim( stripcslashes( $arrValues['allow_in_resident_portal'] ) ) ); elseif( isset( $arrValues['allow_in_resident_portal'] ) ) $this->setAllowInResidentPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_resident_portal'] ) : $arrValues['allow_in_resident_portal'] );
		if( isset( $arrValues['allow_in_prospect_portal'] ) && $boolDirectSet ) $this->set( 'm_boolAllowInProspectPortal', trim( stripcslashes( $arrValues['allow_in_prospect_portal'] ) ) ); elseif( isset( $arrValues['allow_in_prospect_portal'] ) ) $this->setAllowInProspectPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_in_prospect_portal'] ) : $arrValues['allow_in_prospect_portal'] );
		if( isset( $arrValues['is_certified_funds'] ) && $boolDirectSet ) $this->set( 'm_boolIsCertifiedFunds', trim( stripcslashes( $arrValues['is_certified_funds'] ) ) ); elseif( isset( $arrValues['is_certified_funds'] ) ) $this->setIsCertifiedFunds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_certified_funds'] ) : $arrValues['is_certified_funds'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setAllowInEntrata( $boolAllowInEntrata ) {
		$this->set( 'm_boolAllowInEntrata', CStrings::strToBool( $boolAllowInEntrata ) );
	}

	public function getAllowInEntrata() {
		return $this->m_boolAllowInEntrata;
	}

	public function sqlAllowInEntrata() {
		return ( true == isset( $this->m_boolAllowInEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowInResidentPortal( $boolAllowInResidentPortal ) {
		$this->set( 'm_boolAllowInResidentPortal', CStrings::strToBool( $boolAllowInResidentPortal ) );
	}

	public function getAllowInResidentPortal() {
		return $this->m_boolAllowInResidentPortal;
	}

	public function sqlAllowInResidentPortal() {
		return ( true == isset( $this->m_boolAllowInResidentPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInResidentPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowInProspectPortal( $boolAllowInProspectPortal ) {
		$this->set( 'm_boolAllowInProspectPortal', CStrings::strToBool( $boolAllowInProspectPortal ) );
	}

	public function getAllowInProspectPortal() {
		return $this->m_boolAllowInProspectPortal;
	}

	public function sqlAllowInProspectPortal() {
		return ( true == isset( $this->m_boolAllowInProspectPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowInProspectPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCertifiedFunds( $boolIsCertifiedFunds ) {
		$this->set( 'm_boolIsCertifiedFunds', CStrings::strToBool( $boolIsCertifiedFunds ) );
	}

	public function getIsCertifiedFunds() {
		return $this->m_boolIsCertifiedFunds;
	}

	public function sqlIsCertifiedFunds() {
		return ( true == isset( $this->m_boolIsCertifiedFunds ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCertifiedFunds ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, payment_type_id, allow_in_entrata, allow_in_resident_portal, allow_in_prospect_portal, is_certified_funds, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPaymentTypeId() . ', ' .
 						$this->sqlAllowInEntrata() . ', ' .
 						$this->sqlAllowInResidentPortal() . ', ' .
 						$this->sqlAllowInProspectPortal() . ', ' .
 						$this->sqlIsCertifiedFunds() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_in_entrata = ' . $this->sqlAllowInEntrata() . ','; } elseif( true == array_key_exists( 'AllowInEntrata', $this->getChangedColumns() ) ) { $strSql .= ' allow_in_entrata = ' . $this->sqlAllowInEntrata() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_in_resident_portal = ' . $this->sqlAllowInResidentPortal() . ','; } elseif( true == array_key_exists( 'AllowInResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' allow_in_resident_portal = ' . $this->sqlAllowInResidentPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_in_prospect_portal = ' . $this->sqlAllowInProspectPortal() . ','; } elseif( true == array_key_exists( 'AllowInProspectPortal', $this->getChangedColumns() ) ) { $strSql .= ' allow_in_prospect_portal = ' . $this->sqlAllowInProspectPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_certified_funds = ' . $this->sqlIsCertifiedFunds() . ','; } elseif( true == array_key_exists( 'IsCertifiedFunds', $this->getChangedColumns() ) ) { $strSql .= ' is_certified_funds = ' . $this->sqlIsCertifiedFunds() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'allow_in_entrata' => $this->getAllowInEntrata(),
			'allow_in_resident_portal' => $this->getAllowInResidentPortal(),
			'allow_in_prospect_portal' => $this->getAllowInProspectPortal(),
			'is_certified_funds' => $this->getIsCertifiedFunds(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>