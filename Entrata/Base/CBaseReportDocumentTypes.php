<?php

class CBaseReportDocumentTypes extends CEosPluralBase {

    public static function fetchReportDocumentTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CReportDocumentType', $objDatabase );
    }

    public static function fetchReportDocumentType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CReportDocumentType', $objDatabase );
    }

    public static function fetchReportDocumentTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'report_document_types', $objDatabase );
    }

    public static function fetchReportDocumentTypeById( $intId, $objDatabase ) {
        return self::fetchReportDocumentType( sprintf( 'SELECT * FROM report_document_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>