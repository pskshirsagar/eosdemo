<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionFormLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionFormLocations extends CEosPluralBase {

	/**
	 * @return CInspectionFormLocation[]
	 */
	public static function fetchInspectionFormLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInspectionFormLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionFormLocation
	 */
	public static function fetchInspectionFormLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspectionFormLocation::class, $objDatabase );
	}

	public static function fetchInspectionFormLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_form_locations', $objDatabase );
	}

	public static function fetchInspectionFormLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocation( sprintf( 'SELECT * FROM inspection_form_locations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocations( sprintf( 'SELECT * FROM inspection_form_locations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocations( sprintf( 'SELECT * FROM inspection_form_locations WHERE inspection_form_id = %d AND cid = %d', $intInspectionFormId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionFormLocationsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchInspectionFormLocations( sprintf( 'SELECT * FROM inspection_form_locations WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

}
?>