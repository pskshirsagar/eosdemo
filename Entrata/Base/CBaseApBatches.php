<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApBatches extends CEosPluralBase {

	/**
	 * @return CApBatch[]
	 */
	public static function fetchApBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApBatch
	 */
	public static function fetchApBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApBatch', $objDatabase );
	}

	public static function fetchApBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_batches', $objDatabase );
	}

	public static function fetchApBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApBatch( sprintf( 'SELECT * FROM ap_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchApBatches( sprintf( 'SELECT * FROM ap_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApBatchesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApBatches( sprintf( 'SELECT * FROM ap_batches WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>