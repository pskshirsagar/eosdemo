<?php

class CBaseArCodeTypeAllowances extends CEosPluralBase {

    const TABLE_AR_CODE_TYPE_ALLOWANCES = 'public.ar_code_type_allowances';

    public static function fetchArCodeTypeAllowances( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CArCodeTypeAllowance', $objDatabase );
    }

    public static function fetchArCodeTypeAllowance( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArCodeTypeAllowance', $objDatabase );
    }

    public static function fetchArCodeTypeAllowanceCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_code_type_allowances', $objDatabase );
    }

    public static function fetchArCodeTypeAllowanceById( $intId, $objDatabase ) {
        return self::fetchArCodeTypeAllowance( sprintf( 'SELECT * FROM ar_code_type_allowances WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchArCodeTypeAllowancesByArCodeTypeId( $intArCodeTypeId, $objDatabase ) {
        return self::fetchArCodeTypeAllowances( sprintf( 'SELECT * FROM ar_code_type_allowances WHERE ar_code_type_id = %d', (int) $intArCodeTypeId ), $objDatabase );
    }

    public static function fetchArCodeTypeAllowancesByReferenceId( $intReferenceId, $objDatabase ) {
        return self::fetchArCodeTypeAllowances( sprintf( 'SELECT * FROM ar_code_type_allowances WHERE reference_id = %d', (int) $intReferenceId ), $objDatabase );
    }

}
?>