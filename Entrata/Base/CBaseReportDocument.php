<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportDocument extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intReportHistoryId;
    protected $m_intDocumentId;
    protected $m_intReportDocumentTypeId;
    protected $m_strPropertyGroupIds;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['report_history_id'] ) && $boolDirectSet ) $this->m_intReportHistoryId = trim( $arrValues['report_history_id'] ); else if( isset( $arrValues['report_history_id'] ) ) $this->setReportHistoryId( $arrValues['report_history_id'] );
        if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->m_intDocumentId = trim( $arrValues['document_id'] ); else if( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
        if( isset( $arrValues['report_document_type_id'] ) && $boolDirectSet ) $this->m_intReportDocumentTypeId = trim( $arrValues['report_document_type_id'] ); else if( isset( $arrValues['report_document_type_id'] ) ) $this->setReportDocumentTypeId( $arrValues['report_document_type_id'] );
        if( isset( $arrValues['property_group_ids'] ) && $boolDirectSet ) $this->m_strPropertyGroupIds = trim( stripcslashes( $arrValues['property_group_ids'] ) ); else if( isset( $arrValues['property_group_ids'] ) ) $this->setPropertyGroupIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_group_ids'] ) : $arrValues['property_group_ids'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setReportHistoryId( $intReportHistoryId ) {
        $this->m_intReportHistoryId = CStrings::strToIntDef( $intReportHistoryId, NULL, false );
    }

    public function getReportHistoryId() {
        return $this->m_intReportHistoryId;
    }

    public function sqlReportHistoryId() {
        return ( true == isset( $this->m_intReportHistoryId ) ) ? (string) $this->m_intReportHistoryId : 'NULL';
    }

    public function setDocumentId( $intDocumentId ) {
        $this->m_intDocumentId = CStrings::strToIntDef( $intDocumentId, NULL, false );
    }

    public function getDocumentId() {
        return $this->m_intDocumentId;
    }

    public function sqlDocumentId() {
        return ( true == isset( $this->m_intDocumentId ) ) ? (string) $this->m_intDocumentId : 'NULL';
    }

    public function setReportDocumentTypeId( $intReportDocumentTypeId ) {
        $this->m_intReportDocumentTypeId = CStrings::strToIntDef( $intReportDocumentTypeId, NULL, false );
    }

    public function getReportDocumentTypeId() {
        return $this->m_intReportDocumentTypeId;
    }

    public function sqlReportDocumentTypeId() {
        return ( true == isset( $this->m_intReportDocumentTypeId ) ) ? (string) $this->m_intReportDocumentTypeId : 'NULL';
    }

    public function setPropertyGroupIds( $strPropertyGroupIds ) {
        $this->m_strPropertyGroupIds = CStrings::strTrimDef( $strPropertyGroupIds, -1, NULL, true );
    }

    public function getPropertyGroupIds() {
        return $this->m_strPropertyGroupIds;
    }

    public function sqlPropertyGroupIds() {
        return ( true == isset( $this->m_strPropertyGroupIds ) ) ? '\'' . addslashes( $this->m_strPropertyGroupIds ) . '\'' : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.report_documents_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.report_documents					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlReportHistoryId() . ', ' . 		                $this->sqlDocumentId() . ', ' . 		                $this->sqlReportDocumentTypeId() . ', ' . 		                $this->sqlPropertyGroupIds() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.report_documents
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_history_id = ' . $this->sqlReportHistoryId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlReportHistoryId() ) != $this->getOriginalValueByFieldName ( 'report_history_id' ) ) { $arrstrOriginalValueChanges['report_history_id'] = $this->sqlReportHistoryId(); $strSql .= ' report_history_id = ' . $this->sqlReportHistoryId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDocumentId() ) != $this->getOriginalValueByFieldName ( 'document_id' ) ) { $arrstrOriginalValueChanges['document_id'] = $this->sqlDocumentId(); $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_document_type_id = ' . $this->sqlReportDocumentTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlReportDocumentTypeId() ) != $this->getOriginalValueByFieldName ( 'report_document_type_id' ) ) { $arrstrOriginalValueChanges['report_document_type_id'] = $this->sqlReportDocumentTypeId(); $strSql .= ' report_document_type_id = ' . $this->sqlReportDocumentTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyGroupIds() ) != $this->getOriginalValueByFieldName ( 'property_group_ids' ) ) { $arrstrOriginalValueChanges['property_group_ids'] = $this->sqlPropertyGroupIds(); $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.report_documents WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.report_documents_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'report_history_id' => $this->getReportHistoryId(),
        	'document_id' => $this->getDocumentId(),
        	'report_document_type_id' => $this->getReportDocumentTypeId(),
        	'property_group_ids' => $this->getPropertyGroupIds(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>