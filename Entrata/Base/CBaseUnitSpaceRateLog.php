<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceRateLog extends CEosSingularBase {

	const TABLE_NAME = 'public.unit_space_rate_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitSpaceId;
	protected $m_intPriorUnitSpaceRateLogId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_intMarketRentTypeId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_intLeaseTermMonths;
	protected $m_fltBaseRent;
	protected $m_fltAmenityRent;
	protected $m_fltSpecialRent;
	protected $m_fltAddOnRent;
	protected $m_fltMarketRent;
	protected $m_fltBaseDeposit;
	protected $m_fltAmenityDeposit;
	protected $m_fltSpecialDeposit;
	protected $m_fltAddOnDeposit;
	protected $m_fltTotalDeposit;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMarketRentTypeId = '1';
		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_strLogDatetime = 'now()';
		$this->m_intLeaseTermMonths = '0';
		$this->m_fltBaseRent = '0';
		$this->m_fltAmenityRent = '0';
		$this->m_fltSpecialRent = '0';
		$this->m_fltAddOnRent = '0';
		$this->m_fltMarketRent = '0';
		$this->m_fltBaseDeposit = '0';
		$this->m_fltAmenityDeposit = '0';
		$this->m_fltSpecialDeposit = '0';
		$this->m_fltAddOnDeposit = '0';
		$this->m_fltTotalDeposit = '0';
		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['prior_unit_space_rate_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorUnitSpaceRateLogId', trim( $arrValues['prior_unit_space_rate_log_id'] ) ); elseif( isset( $arrValues['prior_unit_space_rate_log_id'] ) ) $this->setPriorUnitSpaceRateLogId( $arrValues['prior_unit_space_rate_log_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['market_rent_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketRentTypeId', trim( $arrValues['market_rent_type_id'] ) ); elseif( isset( $arrValues['market_rent_type_id'] ) ) $this->setMarketRentTypeId( $arrValues['market_rent_type_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermMonths', trim( $arrValues['lease_term_months'] ) ); elseif( isset( $arrValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrValues['lease_term_months'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['add_on_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnRent', trim( $arrValues['add_on_rent'] ) ); elseif( isset( $arrValues['add_on_rent'] ) ) $this->setAddOnRent( $arrValues['add_on_rent'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['base_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltBaseDeposit', trim( $arrValues['base_deposit'] ) ); elseif( isset( $arrValues['base_deposit'] ) ) $this->setBaseDeposit( $arrValues['base_deposit'] );
		if( isset( $arrValues['amenity_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityDeposit', trim( $arrValues['amenity_deposit'] ) ); elseif( isset( $arrValues['amenity_deposit'] ) ) $this->setAmenityDeposit( $arrValues['amenity_deposit'] );
		if( isset( $arrValues['special_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialDeposit', trim( $arrValues['special_deposit'] ) ); elseif( isset( $arrValues['special_deposit'] ) ) $this->setSpecialDeposit( $arrValues['special_deposit'] );
		if( isset( $arrValues['add_on_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltAddOnDeposit', trim( $arrValues['add_on_deposit'] ) ); elseif( isset( $arrValues['add_on_deposit'] ) ) $this->setAddOnDeposit( $arrValues['add_on_deposit'] );
		if( isset( $arrValues['total_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltTotalDeposit', trim( $arrValues['total_deposit'] ) ); elseif( isset( $arrValues['total_deposit'] ) ) $this->setTotalDeposit( $arrValues['total_deposit'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPriorUnitSpaceRateLogId( $intPriorUnitSpaceRateLogId ) {
		$this->set( 'm_intPriorUnitSpaceRateLogId', CStrings::strToIntDef( $intPriorUnitSpaceRateLogId, NULL, false ) );
	}

	public function getPriorUnitSpaceRateLogId() {
		return $this->m_intPriorUnitSpaceRateLogId;
	}

	public function sqlPriorUnitSpaceRateLogId() {
		return ( true == isset( $this->m_intPriorUnitSpaceRateLogId ) ) ? ( string ) $this->m_intPriorUnitSpaceRateLogId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setMarketRentTypeId( $intMarketRentTypeId ) {
		$this->set( 'm_intMarketRentTypeId', CStrings::strToIntDef( $intMarketRentTypeId, NULL, false ) );
	}

	public function getMarketRentTypeId() {
		return $this->m_intMarketRentTypeId;
	}

	public function sqlMarketRentTypeId() {
		return ( true == isset( $this->m_intMarketRentTypeId ) ) ? ( string ) $this->m_intMarketRentTypeId : '1';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->set( 'm_intLeaseTermMonths', CStrings::strToIntDef( $intLeaseTermMonths, NULL, false ) );
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	public function sqlLeaseTermMonths() {
		return ( true == isset( $this->m_intLeaseTermMonths ) ) ? ( string ) $this->m_intLeaseTermMonths : '0';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : '0';
	}

	public function setAmenityRent( $fltAmenityRent ) {
		$this->set( 'm_fltAmenityRent', CStrings::strToFloatDef( $fltAmenityRent, NULL, false, 2 ) );
	}

	public function getAmenityRent() {
		return $this->m_fltAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_fltAmenityRent ) ) ? ( string ) $this->m_fltAmenityRent : '0';
	}

	public function setSpecialRent( $fltSpecialRent ) {
		$this->set( 'm_fltSpecialRent', CStrings::strToFloatDef( $fltSpecialRent, NULL, false, 2 ) );
	}

	public function getSpecialRent() {
		return $this->m_fltSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_fltSpecialRent ) ) ? ( string ) $this->m_fltSpecialRent : '0';
	}

	public function setAddOnRent( $fltAddOnRent ) {
		$this->set( 'm_fltAddOnRent', CStrings::strToFloatDef( $fltAddOnRent, NULL, false, 2 ) );
	}

	public function getAddOnRent() {
		return $this->m_fltAddOnRent;
	}

	public function sqlAddOnRent() {
		return ( true == isset( $this->m_fltAddOnRent ) ) ? ( string ) $this->m_fltAddOnRent : '0';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : '0';
	}

	public function setBaseDeposit( $fltBaseDeposit ) {
		$this->set( 'm_fltBaseDeposit', CStrings::strToFloatDef( $fltBaseDeposit, NULL, false, 2 ) );
	}

	public function getBaseDeposit() {
		return $this->m_fltBaseDeposit;
	}

	public function sqlBaseDeposit() {
		return ( true == isset( $this->m_fltBaseDeposit ) ) ? ( string ) $this->m_fltBaseDeposit : '0';
	}

	public function setAmenityDeposit( $fltAmenityDeposit ) {
		$this->set( 'm_fltAmenityDeposit', CStrings::strToFloatDef( $fltAmenityDeposit, NULL, false, 2 ) );
	}

	public function getAmenityDeposit() {
		return $this->m_fltAmenityDeposit;
	}

	public function sqlAmenityDeposit() {
		return ( true == isset( $this->m_fltAmenityDeposit ) ) ? ( string ) $this->m_fltAmenityDeposit : '0';
	}

	public function setSpecialDeposit( $fltSpecialDeposit ) {
		$this->set( 'm_fltSpecialDeposit', CStrings::strToFloatDef( $fltSpecialDeposit, NULL, false, 2 ) );
	}

	public function getSpecialDeposit() {
		return $this->m_fltSpecialDeposit;
	}

	public function sqlSpecialDeposit() {
		return ( true == isset( $this->m_fltSpecialDeposit ) ) ? ( string ) $this->m_fltSpecialDeposit : '0';
	}

	public function setAddOnDeposit( $fltAddOnDeposit ) {
		$this->set( 'm_fltAddOnDeposit', CStrings::strToFloatDef( $fltAddOnDeposit, NULL, false, 2 ) );
	}

	public function getAddOnDeposit() {
		return $this->m_fltAddOnDeposit;
	}

	public function sqlAddOnDeposit() {
		return ( true == isset( $this->m_fltAddOnDeposit ) ) ? ( string ) $this->m_fltAddOnDeposit : '0';
	}

	public function setTotalDeposit( $fltTotalDeposit ) {
		$this->set( 'm_fltTotalDeposit', CStrings::strToFloatDef( $fltTotalDeposit, NULL, false, 2 ) );
	}

	public function getTotalDeposit() {
		return $this->m_fltTotalDeposit;
	}

	public function sqlTotalDeposit() {
		return ( true == isset( $this->m_fltTotalDeposit ) ) ? ( string ) $this->m_fltTotalDeposit : '0';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_space_id, prior_unit_space_rate_log_id, period_id, reporting_period_id, effective_period_id, original_period_id, market_rent_type_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, log_datetime, effective_date, lease_term_months, base_rent, amenity_rent, special_rent, add_on_rent, market_rent, base_deposit, amenity_deposit, special_deposit, add_on_deposit, total_deposit, is_post_month_ignored, is_post_date_ignored, is_opening_log, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlPriorUnitSpaceRateLogId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlReportingPeriodId() . ', ' .
 						$this->sqlEffectivePeriodId() . ', ' .
 						$this->sqlOriginalPeriodId() . ', ' .
 						$this->sqlMarketRentTypeId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlReportingPostMonth() . ', ' .
 						$this->sqlApplyThroughPostMonth() . ', ' .
 						$this->sqlReportingPostDate() . ', ' .
 						$this->sqlApplyThroughPostDate() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlEffectiveDate() . ', ' .
 						$this->sqlLeaseTermMonths() . ', ' .
 						$this->sqlBaseRent() . ', ' .
 						$this->sqlAmenityRent() . ', ' .
 						$this->sqlSpecialRent() . ', ' .
 						$this->sqlAddOnRent() . ', ' .
 						$this->sqlMarketRent() . ', ' .
 						$this->sqlBaseDeposit() . ', ' .
 						$this->sqlAmenityDeposit() . ', ' .
 						$this->sqlSpecialDeposit() . ', ' .
 						$this->sqlAddOnDeposit() . ', ' .
 						$this->sqlTotalDeposit() . ', ' .
 						$this->sqlIsPostMonthIgnored() . ', ' .
 						$this->sqlIsPostDateIgnored() . ', ' .
 						$this->sqlIsOpeningLog() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_unit_space_rate_log_id = ' . $this->sqlPriorUnitSpaceRateLogId() . ','; } elseif( true == array_key_exists( 'PriorUnitSpaceRateLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_unit_space_rate_log_id = ' . $this->sqlPriorUnitSpaceRateLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_type_id = ' . $this->sqlMarketRentTypeId() . ','; } elseif( true == array_key_exists( 'MarketRentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_type_id = ' . $this->sqlMarketRentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; } elseif( true == array_key_exists( 'LeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_months = ' . $this->sqlLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent() . ','; } elseif( true == array_key_exists( 'AddOnRent', $this->getChangedColumns() ) ) { $strSql .= ' add_on_rent = ' . $this->sqlAddOnRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; } elseif( true == array_key_exists( 'MarketRent', $this->getChangedColumns() ) ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit() . ','; } elseif( true == array_key_exists( 'BaseDeposit', $this->getChangedColumns() ) ) { $strSql .= ' base_deposit = ' . $this->sqlBaseDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit() . ','; } elseif( true == array_key_exists( 'AmenityDeposit', $this->getChangedColumns() ) ) { $strSql .= ' amenity_deposit = ' . $this->sqlAmenityDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit() . ','; } elseif( true == array_key_exists( 'SpecialDeposit', $this->getChangedColumns() ) ) { $strSql .= ' special_deposit = ' . $this->sqlSpecialDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit() . ','; } elseif( true == array_key_exists( 'AddOnDeposit', $this->getChangedColumns() ) ) { $strSql .= ' add_on_deposit = ' . $this->sqlAddOnDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit() . ','; } elseif( true == array_key_exists( 'TotalDeposit', $this->getChangedColumns() ) ) { $strSql .= ' total_deposit = ' . $this->sqlTotalDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'prior_unit_space_rate_log_id' => $this->getPriorUnitSpaceRateLogId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'market_rent_type_id' => $this->getMarketRentTypeId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'lease_term_months' => $this->getLeaseTermMonths(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'add_on_rent' => $this->getAddOnRent(),
			'market_rent' => $this->getMarketRent(),
			'base_deposit' => $this->getBaseDeposit(),
			'amenity_deposit' => $this->getAmenityDeposit(),
			'special_deposit' => $this->getSpecialDeposit(),
			'add_on_deposit' => $this->getAddOnDeposit(),
			'total_deposit' => $this->getTotalDeposit(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>