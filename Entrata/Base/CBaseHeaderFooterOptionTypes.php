<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CHeaderFooterOptionTypes
 * Do not add any new functions to this class.
 */

class CBaseHeaderFooterOptionTypes extends CEosPluralBase {

	/**
	 * @return CHeaderFooterOptionType[]
	 */
	public static function fetchHeaderFooterOptionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CHeaderFooterOptionType::class, $objDatabase );
	}

	/**
	 * @return CHeaderFooterOptionType
	 */
	public static function fetchHeaderFooterOptionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CHeaderFooterOptionType::class, $objDatabase );
	}

	public static function fetchHeaderFooterOptionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'header_footer_option_types', $objDatabase );
	}

	public static function fetchHeaderFooterOptionTypeById( $intId, $objDatabase ) {
		return self::fetchHeaderFooterOptionType( sprintf( 'SELECT * FROM header_footer_option_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>