<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetDepreciationSchedules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetDepreciationSchedules extends CEosPluralBase {

	/**
	 * @return CAssetDepreciationSchedule[]
	 */
	public static function fetchAssetDepreciationSchedules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetDepreciationSchedule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetDepreciationSchedule
	 */
	public static function fetchAssetDepreciationSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetDepreciationSchedule', $objDatabase );
	}

	public static function fetchAssetDepreciationScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_depreciation_schedules', $objDatabase );
	}

	public static function fetchAssetDepreciationScheduleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetDepreciationSchedule( sprintf( 'SELECT * FROM asset_depreciation_schedules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetDepreciationSchedulesByCid( $intCid, $objDatabase ) {
		return self::fetchAssetDepreciationSchedules( sprintf( 'SELECT * FROM asset_depreciation_schedules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>