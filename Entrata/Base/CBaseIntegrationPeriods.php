<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationPeriods
 * Do not add any new functions to this class.
 */

class CBaseIntegrationPeriods extends CEosPluralBase {

	/**
	 * @return CIntegrationPeriod[]
	 */
	public static function fetchIntegrationPeriods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationPeriod::class, $objDatabase );
	}

	/**
	 * @return CIntegrationPeriod
	 */
	public static function fetchIntegrationPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationPeriod::class, $objDatabase );
	}

	public static function fetchIntegrationPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_periods', $objDatabase );
	}

	public static function fetchIntegrationPeriodById( $intId, $objDatabase ) {
		return self::fetchIntegrationPeriod( sprintf( 'SELECT * FROM integration_periods WHERE id = %d', $intId ), $objDatabase );
	}

}
?>