<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyRequestResponseTypeId;
	protected $m_strRequestPayload;
	protected $m_strResponsePayload;
	protected $m_strRequestEndpoint;
	protected $m_intRequestPort;
	protected $m_strRequestServiceName;
	protected $m_boolIsFailed;
	protected $m_intRequestedBy;
	protected $m_strRequestedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_request_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyRequestResponseTypeId', trim( $arrValues['subsidy_request_response_type_id'] ) ); elseif( isset( $arrValues['subsidy_request_response_type_id'] ) ) $this->setSubsidyRequestResponseTypeId( $arrValues['subsidy_request_response_type_id'] );
		if( isset( $arrValues['request_payload'] ) && $boolDirectSet ) $this->set( 'm_strRequestPayload', trim( stripcslashes( $arrValues['request_payload'] ) ) ); elseif( isset( $arrValues['request_payload'] ) ) $this->setRequestPayload( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_payload'] ) : $arrValues['request_payload'] );
		if( isset( $arrValues['response_payload'] ) && $boolDirectSet ) $this->set( 'm_strResponsePayload', trim( stripcslashes( $arrValues['response_payload'] ) ) ); elseif( isset( $arrValues['response_payload'] ) ) $this->setResponsePayload( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_payload'] ) : $arrValues['response_payload'] );
		if( isset( $arrValues['request_endpoint'] ) && $boolDirectSet ) $this->set( 'm_strRequestEndpoint', trim( stripcslashes( $arrValues['request_endpoint'] ) ) ); elseif( isset( $arrValues['request_endpoint'] ) ) $this->setRequestEndpoint( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_endpoint'] ) : $arrValues['request_endpoint'] );
		if( isset( $arrValues['request_port'] ) && $boolDirectSet ) $this->set( 'm_intRequestPort', trim( $arrValues['request_port'] ) ); elseif( isset( $arrValues['request_port'] ) ) $this->setRequestPort( $arrValues['request_port'] );
		if( isset( $arrValues['request_service_name'] ) && $boolDirectSet ) $this->set( 'm_strRequestServiceName', trim( stripcslashes( $arrValues['request_service_name'] ) ) ); elseif( isset( $arrValues['request_service_name'] ) ) $this->setRequestServiceName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_service_name'] ) : $arrValues['request_service_name'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFailed', trim( stripcslashes( $arrValues['is_failed'] ) ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_failed'] ) : $arrValues['is_failed'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['requested_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestedOn', trim( $arrValues['requested_on'] ) ); elseif( isset( $arrValues['requested_on'] ) ) $this->setRequestedOn( $arrValues['requested_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyRequestResponseTypeId( $intSubsidyRequestResponseTypeId ) {
		$this->set( 'm_intSubsidyRequestResponseTypeId', CStrings::strToIntDef( $intSubsidyRequestResponseTypeId, NULL, false ) );
	}

	public function getSubsidyRequestResponseTypeId() {
		return $this->m_intSubsidyRequestResponseTypeId;
	}

	public function sqlSubsidyRequestResponseTypeId() {
		return ( true == isset( $this->m_intSubsidyRequestResponseTypeId ) ) ? ( string ) $this->m_intSubsidyRequestResponseTypeId : 'NULL';
	}

	public function setRequestPayload( $strRequestPayload ) {
		$this->set( 'm_strRequestPayload', CStrings::strTrimDef( $strRequestPayload, -1, NULL, true ) );
	}

	public function getRequestPayload() {
		return $this->m_strRequestPayload;
	}

	public function sqlRequestPayload() {
		return ( true == isset( $this->m_strRequestPayload ) ) ? '\'' . addslashes( $this->m_strRequestPayload ) . '\'' : 'NULL';
	}

	public function setResponsePayload( $strResponsePayload ) {
		$this->set( 'm_strResponsePayload', CStrings::strTrimDef( $strResponsePayload, -1, NULL, true ) );
	}

	public function getResponsePayload() {
		return $this->m_strResponsePayload;
	}

	public function sqlResponsePayload() {
		return ( true == isset( $this->m_strResponsePayload ) ) ? '\'' . addslashes( $this->m_strResponsePayload ) . '\'' : 'NULL';
	}

	public function setRequestEndpoint( $strRequestEndpoint ) {
		$this->set( 'm_strRequestEndpoint', CStrings::strTrimDef( $strRequestEndpoint, 4096, NULL, true ) );
	}

	public function getRequestEndpoint() {
		return $this->m_strRequestEndpoint;
	}

	public function sqlRequestEndpoint() {
		return ( true == isset( $this->m_strRequestEndpoint ) ) ? '\'' . addslashes( $this->m_strRequestEndpoint ) . '\'' : 'NULL';
	}

	public function setRequestPort( $intRequestPort ) {
		$this->set( 'm_intRequestPort', CStrings::strToIntDef( $intRequestPort, NULL, false ) );
	}

	public function getRequestPort() {
		return $this->m_intRequestPort;
	}

	public function sqlRequestPort() {
		return ( true == isset( $this->m_intRequestPort ) ) ? ( string ) $this->m_intRequestPort : 'NULL';
	}

	public function setRequestServiceName( $strRequestServiceName ) {
		$this->set( 'm_strRequestServiceName', CStrings::strTrimDef( $strRequestServiceName, 240, NULL, true ) );
	}

	public function getRequestServiceName() {
		return $this->m_strRequestServiceName;
	}

	public function sqlRequestServiceName() {
		return ( true == isset( $this->m_strRequestServiceName ) ) ? '\'' . addslashes( $this->m_strRequestServiceName ) . '\'' : 'NULL';
	}

	public function setIsFailed( $boolIsFailed ) {
		$this->set( 'm_boolIsFailed', CStrings::strToBool( $boolIsFailed ) );
	}

	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setRequestedOn( $strRequestedOn ) {
		$this->set( 'm_strRequestedOn', CStrings::strTrimDef( $strRequestedOn, -1, NULL, true ) );
	}

	public function getRequestedOn() {
		return $this->m_strRequestedOn;
	}

	public function sqlRequestedOn() {
		return ( true == isset( $this->m_strRequestedOn ) ) ? '\'' . $this->m_strRequestedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_request_response_type_id, request_payload, response_payload, request_endpoint, request_port, request_service_name, is_failed, requested_by, requested_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidyRequestResponseTypeId() . ', ' .
 						$this->sqlRequestPayload() . ', ' .
 						$this->sqlResponsePayload() . ', ' .
 						$this->sqlRequestEndpoint() . ', ' .
 						$this->sqlRequestPort() . ', ' .
 						$this->sqlRequestServiceName() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlRequestedBy() . ', ' .
 						$this->sqlRequestedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_request_response_type_id = ' . $this->sqlSubsidyRequestResponseTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyRequestResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_request_response_type_id = ' . $this->sqlSubsidyRequestResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_payload = ' . $this->sqlRequestPayload() . ','; } elseif( true == array_key_exists( 'RequestPayload', $this->getChangedColumns() ) ) { $strSql .= ' request_payload = ' . $this->sqlRequestPayload() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_payload = ' . $this->sqlResponsePayload() . ','; } elseif( true == array_key_exists( 'ResponsePayload', $this->getChangedColumns() ) ) { $strSql .= ' response_payload = ' . $this->sqlResponsePayload() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_endpoint = ' . $this->sqlRequestEndpoint() . ','; } elseif( true == array_key_exists( 'RequestEndpoint', $this->getChangedColumns() ) ) { $strSql .= ' request_endpoint = ' . $this->sqlRequestEndpoint() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_port = ' . $this->sqlRequestPort() . ','; } elseif( true == array_key_exists( 'RequestPort', $this->getChangedColumns() ) ) { $strSql .= ' request_port = ' . $this->sqlRequestPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_service_name = ' . $this->sqlRequestServiceName() . ','; } elseif( true == array_key_exists( 'RequestServiceName', $this->getChangedColumns() ) ) { $strSql .= ' request_service_name = ' . $this->sqlRequestServiceName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; } elseif( true == array_key_exists( 'RequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_request_response_type_id' => $this->getSubsidyRequestResponseTypeId(),
			'request_payload' => $this->getRequestPayload(),
			'response_payload' => $this->getResponsePayload(),
			'request_endpoint' => $this->getRequestEndpoint(),
			'request_port' => $this->getRequestPort(),
			'request_service_name' => $this->getRequestServiceName(),
			'is_failed' => $this->getIsFailed(),
			'requested_by' => $this->getRequestedBy(),
			'requested_on' => $this->getRequestedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>