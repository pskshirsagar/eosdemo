<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyReviewAttribute extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.company_review_attributes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultReviewAttributeId;
	protected $m_strAttributeName;
	protected $m_intIsPublished;
	protected $m_intAttributeAddedBy;
	protected $m_strAttributeAddedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';
		$this->m_intAttributeAddedBy = '0';
		$this->m_intUpdatedBy = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_review_attribute_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReviewAttributeId', trim( $arrValues['default_review_attribute_id'] ) ); elseif( isset( $arrValues['default_review_attribute_id'] ) ) $this->setDefaultReviewAttributeId( $arrValues['default_review_attribute_id'] );
		if( isset( $arrValues['attribute_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAttributeName', trim( $arrValues['attribute_name'] ) ); elseif( isset( $arrValues['attribute_name'] ) ) $this->setAttributeName( $arrValues['attribute_name'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['attribute_added_by'] ) && $boolDirectSet ) $this->set( 'm_intAttributeAddedBy', trim( $arrValues['attribute_added_by'] ) ); elseif( isset( $arrValues['attribute_added_by'] ) ) $this->setAttributeAddedBy( $arrValues['attribute_added_by'] );
		if( isset( $arrValues['attribute_added_on'] ) && $boolDirectSet ) $this->set( 'm_strAttributeAddedOn', trim( $arrValues['attribute_added_on'] ) ); elseif( isset( $arrValues['attribute_added_on'] ) ) $this->setAttributeAddedOn( $arrValues['attribute_added_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultReviewAttributeId( $intDefaultReviewAttributeId ) {
		$this->set( 'm_intDefaultReviewAttributeId', CStrings::strToIntDef( $intDefaultReviewAttributeId, NULL, false ) );
	}

	public function getDefaultReviewAttributeId() {
		return $this->m_intDefaultReviewAttributeId;
	}

	public function sqlDefaultReviewAttributeId() {
		return ( true == isset( $this->m_intDefaultReviewAttributeId ) ) ? ( string ) $this->m_intDefaultReviewAttributeId : 'NULL';
	}

	public function setAttributeName( $strAttributeName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAttributeName', CStrings::strTrimDef( $strAttributeName, 256, NULL, true ), $strLocaleCode );
	}

	public function getAttributeName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAttributeName', $strLocaleCode );
	}

	public function sqlAttributeName() {
		return ( true == isset( $this->m_strAttributeName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAttributeName ) : '\'' . addslashes( $this->m_strAttributeName ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setAttributeAddedBy( $intAttributeAddedBy ) {
		$this->set( 'm_intAttributeAddedBy', CStrings::strToIntDef( $intAttributeAddedBy, NULL, false ) );
	}

	public function getAttributeAddedBy() {
		return $this->m_intAttributeAddedBy;
	}

	public function sqlAttributeAddedBy() {
		return ( true == isset( $this->m_intAttributeAddedBy ) ) ? ( string ) $this->m_intAttributeAddedBy : '0';
	}

	public function setAttributeAddedOn( $strAttributeAddedOn ) {
		$this->set( 'm_strAttributeAddedOn', CStrings::strTrimDef( $strAttributeAddedOn, -1, NULL, true ) );
	}

	public function getAttributeAddedOn() {
		return $this->m_strAttributeAddedOn;
	}

	public function sqlAttributeAddedOn() {
		return ( true == isset( $this->m_strAttributeAddedOn ) ) ? '\'' . $this->m_strAttributeAddedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '0';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_review_attribute_id, attribute_name, is_published, attribute_added_by, attribute_added_on, updated_by, updated_on, created_by, created_on, details, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultReviewAttributeId() . ', ' .
						$this->sqlAttributeName() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlAttributeAddedBy() . ', ' .
						$this->sqlAttributeAddedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_review_attribute_id = ' . $this->sqlDefaultReviewAttributeId(). ',' ; } elseif( true == array_key_exists( 'DefaultReviewAttributeId', $this->getChangedColumns() ) ) { $strSql .= ' default_review_attribute_id = ' . $this->sqlDefaultReviewAttributeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attribute_name = ' . $this->sqlAttributeName(). ',' ; } elseif( true == array_key_exists( 'AttributeName', $this->getChangedColumns() ) ) { $strSql .= ' attribute_name = ' . $this->sqlAttributeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attribute_added_by = ' . $this->sqlAttributeAddedBy(). ',' ; } elseif( true == array_key_exists( 'AttributeAddedBy', $this->getChangedColumns() ) ) { $strSql .= ' attribute_added_by = ' . $this->sqlAttributeAddedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attribute_added_on = ' . $this->sqlAttributeAddedOn(). ',' ; } elseif( true == array_key_exists( 'AttributeAddedOn', $this->getChangedColumns() ) ) { $strSql .= ' attribute_added_on = ' . $this->sqlAttributeAddedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_review_attribute_id' => $this->getDefaultReviewAttributeId(),
			'attribute_name' => $this->getAttributeName(),
			'is_published' => $this->getIsPublished(),
			'attribute_added_by' => $this->getAttributeAddedBy(),
			'attribute_added_on' => $this->getAttributeAddedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>