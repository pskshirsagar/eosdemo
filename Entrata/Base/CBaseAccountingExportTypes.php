<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportTypes
 * Do not add any new functions to this class.
 */

class CBaseAccountingExportTypes extends CEosPluralBase {

	/**
	 * @return CAccountingExportType[]
	 */
	public static function fetchAccountingExportTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAccountingExportType::class, $objDatabase );
	}

	/**
	 * @return CAccountingExportType
	 */
	public static function fetchAccountingExportType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportType::class, $objDatabase );
	}

	public static function fetchAccountingExportTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_types', $objDatabase );
	}

	public static function fetchAccountingExportTypeById( $intId, $objDatabase ) {
		return self::fetchAccountingExportType( sprintf( 'SELECT * FROM accounting_export_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>