<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilterPropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportFilterPropertyGroups extends CEosPluralBase {

	/**
	 * @return CReportFilterPropertyGroup[]
	 */
	public static function fetchReportFilterPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CReportFilterPropertyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportFilterPropertyGroup
	 */
	public static function fetchReportFilterPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReportFilterPropertyGroup', $objDatabase );
	}

	public static function fetchReportFilterPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_filter_property_groups', $objDatabase );
	}

	public static function fetchReportFilterPropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportFilterPropertyGroup( sprintf( 'SELECT * FROM report_filter_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFilterPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchReportFilterPropertyGroups( sprintf( 'SELECT * FROM report_filter_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFilterPropertyGroupsByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		return self::fetchReportFilterPropertyGroups( sprintf( 'SELECT * FROM report_filter_property_groups WHERE report_filter_id = %d AND cid = %d', ( int ) $intReportFilterId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportFilterPropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchReportFilterPropertyGroups( sprintf( 'SELECT * FROM report_filter_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>