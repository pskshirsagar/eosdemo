<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyBadges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyBadges extends CEosPluralBase {

	/**
	 * @return CCompanyBadge[]
	 */
	public static function fetchCompanyBadges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyBadge', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyBadge
	 */
	public static function fetchCompanyBadge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyBadge', $objDatabase );
	}

	public static function fetchCompanyBadgeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_badges', $objDatabase );
	}

	public static function fetchCompanyBadgeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyBadge( sprintf( 'SELECT * FROM company_badges WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyBadgesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyBadges( sprintf( 'SELECT * FROM company_badges WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyBadgesByDefaultBadgeIdByCid( $intDefaultBadgeId, $intCid, $objDatabase ) {
		return self::fetchCompanyBadges( sprintf( 'SELECT * FROM company_badges WHERE default_badge_id = %d AND cid = %d', ( int ) $intDefaultBadgeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyBadgesByCompanyActivityPointIdByCid( $intCompanyActivityPointId, $intCid, $objDatabase ) {
		return self::fetchCompanyBadges( sprintf( 'SELECT * FROM company_badges WHERE company_activity_point_id = %d AND cid = %d', ( int ) $intCompanyActivityPointId, ( int ) $intCid ), $objDatabase );
	}

}
?>