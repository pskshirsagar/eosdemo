<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedDashboardPriorities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedDashboardPriorities extends CEosPluralBase {

	/**
	 * @return CCachedDashboardPriority[]
	 */
	public static function fetchCachedDashboardPriorities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCachedDashboardPriority::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedDashboardPriority
	 */
	public static function fetchCachedDashboardPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCachedDashboardPriority::class, $objDatabase );
	}

	public static function fetchCachedDashboardPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_dashboard_priorities', $objDatabase );
	}

	public static function fetchCachedDashboardPriorityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriority( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByCid( $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE module_id = %d AND cid = %d', $intModuleId, $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE company_employee_id = %d AND cid = %d', $intCompanyEmployeeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByPriorityIdByCid( $intPriorityId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE priority_id = %d AND cid = %d', $intPriorityId, $intCid ), $objDatabase );
	}

	public static function fetchCachedDashboardPrioritiesByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchCachedDashboardPriorities( sprintf( 'SELECT * FROM cached_dashboard_priorities WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

}
?>