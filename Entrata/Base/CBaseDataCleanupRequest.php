<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataCleanupRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.data_cleanup_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intDataCleanupTypeId;
	protected $m_strSummary;
	protected $m_strPostMonth;
	protected $m_boolIsArCleanup;
	protected $m_boolIsApCleanup;
	protected $m_boolIsJeCleanup;
	protected $m_intIsPublished;
	protected $m_intIsApproved;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strCompletionOn;
	protected $m_strFailedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_boolIsArCleanup = false;
		$this->m_boolIsApCleanup = false;
		$this->m_boolIsJeCleanup = false;
		$this->m_intIsPublished = '0';
		$this->m_intIsApproved = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['data_cleanup_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataCleanupTypeId', trim( $arrValues['data_cleanup_type_id'] ) ); elseif( isset( $arrValues['data_cleanup_type_id'] ) ) $this->setDataCleanupTypeId( $arrValues['data_cleanup_type_id'] );
		if( isset( $arrValues['summary'] ) && $boolDirectSet ) $this->set( 'm_strSummary', trim( stripcslashes( $arrValues['summary'] ) ) ); elseif( isset( $arrValues['summary'] ) ) $this->setSummary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['summary'] ) : $arrValues['summary'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['is_ar_cleanup'] ) && $boolDirectSet ) $this->set( 'm_boolIsArCleanup', trim( stripcslashes( $arrValues['is_ar_cleanup'] ) ) ); elseif( isset( $arrValues['is_ar_cleanup'] ) ) $this->setIsArCleanup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ar_cleanup'] ) : $arrValues['is_ar_cleanup'] );
		if( isset( $arrValues['is_ap_cleanup'] ) && $boolDirectSet ) $this->set( 'm_boolIsApCleanup', trim( stripcslashes( $arrValues['is_ap_cleanup'] ) ) ); elseif( isset( $arrValues['is_ap_cleanup'] ) ) $this->setIsApCleanup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ap_cleanup'] ) : $arrValues['is_ap_cleanup'] );
		if( isset( $arrValues['is_je_cleanup'] ) && $boolDirectSet ) $this->set( 'm_boolIsJeCleanup', trim( stripcslashes( $arrValues['is_je_cleanup'] ) ) ); elseif( isset( $arrValues['is_je_cleanup'] ) ) $this->setIsJeCleanup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_je_cleanup'] ) : $arrValues['is_je_cleanup'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['completion_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletionOn', trim( $arrValues['completion_on'] ) ); elseif( isset( $arrValues['completion_on'] ) ) $this->setCompletionOn( $arrValues['completion_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDataCleanupTypeId( $intDataCleanupTypeId ) {
		$this->set( 'm_intDataCleanupTypeId', CStrings::strToIntDef( $intDataCleanupTypeId, NULL, false ) );
	}

	public function getDataCleanupTypeId() {
		return $this->m_intDataCleanupTypeId;
	}

	public function sqlDataCleanupTypeId() {
		return ( true == isset( $this->m_intDataCleanupTypeId ) ) ? ( string ) $this->m_intDataCleanupTypeId : 'NULL';
	}

	public function setSummary( $strSummary ) {
		$this->set( 'm_strSummary', CStrings::strTrimDef( $strSummary, -1, NULL, true ) );
	}

	public function getSummary() {
		return $this->m_strSummary;
	}

	public function sqlSummary() {
		return ( true == isset( $this->m_strSummary ) ) ? '\'' . addslashes( $this->m_strSummary ) . '\'' : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setIsArCleanup( $boolIsArCleanup ) {
		$this->set( 'm_boolIsArCleanup', CStrings::strToBool( $boolIsArCleanup ) );
	}

	public function getIsArCleanup() {
		return $this->m_boolIsArCleanup;
	}

	public function sqlIsArCleanup() {
		return ( true == isset( $this->m_boolIsArCleanup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArCleanup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsApCleanup( $boolIsApCleanup ) {
		$this->set( 'm_boolIsApCleanup', CStrings::strToBool( $boolIsApCleanup ) );
	}

	public function getIsApCleanup() {
		return $this->m_boolIsApCleanup;
	}

	public function sqlIsApCleanup() {
		return ( true == isset( $this->m_boolIsApCleanup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApCleanup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsJeCleanup( $boolIsJeCleanup ) {
		$this->set( 'm_boolIsJeCleanup', CStrings::strToBool( $boolIsJeCleanup ) );
	}

	public function getIsJeCleanup() {
		return $this->m_boolIsJeCleanup;
	}

	public function sqlIsJeCleanup() {
		return ( true == isset( $this->m_boolIsJeCleanup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsJeCleanup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setCompletionOn( $strCompletionOn ) {
		$this->set( 'm_strCompletionOn', CStrings::strTrimDef( $strCompletionOn, -1, NULL, true ) );
	}

	public function getCompletionOn() {
		return $this->m_strCompletionOn;
	}

	public function sqlCompletionOn() {
		return ( true == isset( $this->m_strCompletionOn ) ) ? '\'' . $this->m_strCompletionOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, data_cleanup_type_id, summary, post_month, is_ar_cleanup, is_ap_cleanup, is_je_cleanup, is_published, is_approved, approved_by, approved_on, start_datetime, end_datetime, completion_on, failed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlDataCleanupTypeId() . ', ' .
 						$this->sqlSummary() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlIsArCleanup() . ', ' .
 						$this->sqlIsApCleanup() . ', ' .
 						$this->sqlIsJeCleanup() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsApproved() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlCompletionOn() . ', ' .
 						$this->sqlFailedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_cleanup_type_id = ' . $this->sqlDataCleanupTypeId() . ','; } elseif( true == array_key_exists( 'DataCleanupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_cleanup_type_id = ' . $this->sqlDataCleanupTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; } elseif( true == array_key_exists( 'Summary', $this->getChangedColumns() ) ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ar_cleanup = ' . $this->sqlIsArCleanup() . ','; } elseif( true == array_key_exists( 'IsArCleanup', $this->getChangedColumns() ) ) { $strSql .= ' is_ar_cleanup = ' . $this->sqlIsArCleanup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ap_cleanup = ' . $this->sqlIsApCleanup() . ','; } elseif( true == array_key_exists( 'IsApCleanup', $this->getChangedColumns() ) ) { $strSql .= ' is_ap_cleanup = ' . $this->sqlIsApCleanup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_je_cleanup = ' . $this->sqlIsJeCleanup() . ','; } elseif( true == array_key_exists( 'IsJeCleanup', $this->getChangedColumns() ) ) { $strSql .= ' is_je_cleanup = ' . $this->sqlIsJeCleanup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completion_on = ' . $this->sqlCompletionOn() . ','; } elseif( true == array_key_exists( 'CompletionOn', $this->getChangedColumns() ) ) { $strSql .= ' completion_on = ' . $this->sqlCompletionOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'data_cleanup_type_id' => $this->getDataCleanupTypeId(),
			'summary' => $this->getSummary(),
			'post_month' => $this->getPostMonth(),
			'is_ar_cleanup' => $this->getIsArCleanup(),
			'is_ap_cleanup' => $this->getIsApCleanup(),
			'is_je_cleanup' => $this->getIsJeCleanup(),
			'is_published' => $this->getIsPublished(),
			'is_approved' => $this->getIsApproved(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'completion_on' => $this->getCompletionOn(),
			'failed_on' => $this->getFailedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>