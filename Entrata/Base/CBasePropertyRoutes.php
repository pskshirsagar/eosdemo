<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyRoutes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyRoutes extends CEosPluralBase {

	public static function fetchPropertyRoutes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyRoute', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertyRoute( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyRoute', $objDatabase );
	}

	public static function fetchPropertyRouteCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_routes', $objDatabase );
	}

	public static function fetchPropertyRouteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoute( sprintf( 'SELECT * FROM property_routes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoutesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyRoutes( sprintf( 'SELECT * FROM property_routes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoutesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoutes( sprintf( 'SELECT * FROM property_routes WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoutesByRouteIdByCid( $intRouteId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoutes( sprintf( 'SELECT * FROM property_routes WHERE route_id = %d AND cid = %d', ( int ) $intRouteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyRoutesByRouteTypeIdByCid( $intRouteTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyRoutes( sprintf( 'SELECT * FROM property_routes WHERE route_type_id = %d AND cid = %d', ( int ) $intRouteTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>