<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoneyTransfers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMoneyTransfers extends CEosPluralBase {

	/**
	 * @return CMoneyTransfer[]
	 */
	public static function fetchMoneyTransfers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMoneyTransfer', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMoneyTransfer
	 */
	public static function fetchMoneyTransfer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMoneyTransfer', $objDatabase );
	}

	public static function fetchMoneyTransferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'money_transfers', $objDatabase );
	}

	public static function fetchMoneyTransferByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfer( sprintf( 'SELECT * FROM money_transfers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByCid( $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByApCodeTypeIdByCid( $intApCodeTypeId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE ap_code_type_id = %d AND cid = %d', ( int ) $intApCodeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByFromBankAccountIdByCid( $intFromBankAccountId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE from_bank_account_id = %d AND cid = %d', ( int ) $intFromBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByToApRemittanceIdByCid( $intToApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE to_ap_remittance_id = %d AND cid = %d', ( int ) $intToApRemittanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByInvoiceApHeaderIdByCid( $intInvoiceApHeaderId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE invoice_ap_header_id = %d AND cid = %d', ( int ) $intInvoiceApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByPaymentApHeaderIdByCid( $intPaymentApHeaderId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE payment_ap_header_id = %d AND cid = %d', ( int ) $intPaymentApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByReimbursementInvoiceApHeaderIdByCid( $intReimbursementInvoiceApHeaderId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE reimbursement_invoice_ap_header_id = %d AND cid = %d', ( int ) $intReimbursementInvoiceApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByReimbursementPaymentApHeaderIdByCid( $intReimbursementPaymentApHeaderId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE reimbursement_payment_ap_header_id = %d AND cid = %d', ( int ) $intReimbursementPaymentApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByChargeArTransactionIdByCid( $intChargeArTransactionId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE charge_ar_transaction_id = %d AND cid = %d', ( int ) $intChargeArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByPaymentArTransactionIdByCid( $intPaymentArTransactionId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE payment_ar_transaction_id = %d AND cid = %d', ( int ) $intPaymentArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByReimbursementChargeArTransactionIdByCid( $intReimbursementChargeArTransactionId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE reimbursement_charge_ar_transaction_id = %d AND cid = %d', ( int ) $intReimbursementChargeArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMoneyTransfersByReimbursementPaymentArTransactionIdByCid( $intReimbursementPaymentArTransactionId, $intCid, $objDatabase ) {
		return self::fetchMoneyTransfers( sprintf( 'SELECT * FROM money_transfers WHERE reimbursement_payment_ar_transaction_id = %d AND cid = %d', ( int ) $intReimbursementPaymentArTransactionId, ( int ) $intCid ), $objDatabase );
	}

}
?>