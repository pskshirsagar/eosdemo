<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPaymentAllowanceTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentAllowanceTypes extends CEosPluralBase {

	/**
	 * @return CPaymentAllowanceType[]
	 */
	public static function fetchPaymentAllowanceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentAllowanceType::class, $objDatabase );
	}

	/**
	 * @return CPaymentAllowanceType
	 */
	public static function fetchPaymentAllowanceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentAllowanceType::class, $objDatabase );
	}

	public static function fetchPaymentAllowanceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_allowance_types', $objDatabase );
	}

	public static function fetchPaymentAllowanceTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentAllowanceType( sprintf( 'SELECT * FROM payment_allowance_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>