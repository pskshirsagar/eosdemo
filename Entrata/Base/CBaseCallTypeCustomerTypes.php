<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallTypeCustomerTypes
 * Do not add any new functions to this class.
 */

class CBaseCallTypeCustomerTypes extends CEosPluralBase {

	/**
	 * @return CCallTypeCustomerType[]
	 */
	public static function fetchCallTypeCustomerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallTypeCustomerType', $objDatabase );
	}

	/**
	 * @return CCallTypeCustomerType
	 */
	public static function fetchCallTypeCustomerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallTypeCustomerType', $objDatabase );
	}

	public static function fetchCallTypeCustomerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_type_customer_types', $objDatabase );
	}

	public static function fetchCallTypeCustomerTypeById( $intId, $objDatabase ) {
		return self::fetchCallTypeCustomerType( sprintf( 'SELECT * FROM call_type_customer_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallTypeCustomerTypesByScheduledCallTypeId( $intScheduledCallTypeId, $objDatabase ) {
		return self::fetchCallTypeCustomerTypes( sprintf( 'SELECT * FROM call_type_customer_types WHERE scheduled_call_type_id = %d', ( int ) $intScheduledCallTypeId ), $objDatabase );
	}

	public static function fetchCallTypeCustomerTypesByLeaseStatusTypeId( $intLeaseStatusTypeId, $objDatabase ) {
		return self::fetchCallTypeCustomerTypes( sprintf( 'SELECT * FROM call_type_customer_types WHERE lease_status_type_id = %d', ( int ) $intLeaseStatusTypeId ), $objDatabase );
	}

}
?>