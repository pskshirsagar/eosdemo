<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductModules
 * Do not add any new functions to this class.
 */

class CBaseProductModules extends CEosPluralBase {

	/**
	 * @return CProductModule[]
	 */
	public static function fetchProductModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CProductModule', $objDatabase );
	}

	/**
	 * @return CProductModule
	 */
	public static function fetchProductModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProductModule', $objDatabase );
	}

	public static function fetchProductModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'product_modules', $objDatabase );
	}

	public static function fetchProductModuleById( $intId, $objDatabase ) {
		return self::fetchProductModule( sprintf( 'SELECT * FROM product_modules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProductModulesByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchProductModules( sprintf( 'SELECT * FROM product_modules WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

	public static function fetchProductModulesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchProductModules( sprintf( 'SELECT * FROM product_modules WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchProductModulesByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchProductModules( sprintf( 'SELECT * FROM product_modules WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

}
?>