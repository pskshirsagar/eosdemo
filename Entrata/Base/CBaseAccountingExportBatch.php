<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportBatch extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.accounting_export_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransmissionVendorId;
	protected $m_intAccountingExportSchedulerId;
	protected $m_intAccountingExportFileFormatTypeId;
	protected $m_intGlChartId;
	protected $m_intGlTreeId;
	protected $m_fltAmount;
	protected $m_boolIsSuccess;
	protected $m_strMessage;
	protected $m_strFileNamePrefix;
	protected $m_strFileNameSuffix;
	protected $m_strBatchMemo;
	protected $m_strLastAttemptedOn;
	protected $m_intReattemptCount;
	protected $m_strExportedThrough;
	protected $m_strPostMonthRange;
	protected $m_strBatchDatetime;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intTaxableEntityId;
	protected $m_intInvoiceNumber;
	protected $m_strExternalUuid;
	protected $m_strUploadedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSuccess = true;
		$this->m_intReattemptCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['accounting_export_scheduler_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportSchedulerId', trim( $arrValues['accounting_export_scheduler_id'] ) ); elseif( isset( $arrValues['accounting_export_scheduler_id'] ) ) $this->setAccountingExportSchedulerId( $arrValues['accounting_export_scheduler_id'] );
		if( isset( $arrValues['accounting_export_file_format_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportFileFormatTypeId', trim( $arrValues['accounting_export_file_format_type_id'] ) ); elseif( isset( $arrValues['accounting_export_file_format_type_id'] ) ) $this->setAccountingExportFileFormatTypeId( $arrValues['accounting_export_file_format_type_id'] );
		if( isset( $arrValues['gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intGlChartId', trim( $arrValues['gl_chart_id'] ) ); elseif( isset( $arrValues['gl_chart_id'] ) ) $this->setGlChartId( $arrValues['gl_chart_id'] );
		if( isset( $arrValues['gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeId', trim( $arrValues['gl_tree_id'] ) ); elseif( isset( $arrValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrValues['gl_tree_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['is_success'] ) && $boolDirectSet ) $this->set( 'm_boolIsSuccess', trim( stripcslashes( $arrValues['is_success'] ) ) ); elseif( isset( $arrValues['is_success'] ) ) $this->setIsSuccess( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_success'] ) : $arrValues['is_success'] );
		if( isset( $arrValues['message'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMessage', trim( $arrValues['message'] ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( $arrValues['message'] );
		if( isset( $arrValues['file_name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strFileNamePrefix', trim( $arrValues['file_name_prefix'] ) ); elseif( isset( $arrValues['file_name_prefix'] ) ) $this->setFileNamePrefix( $arrValues['file_name_prefix'] );
		if( isset( $arrValues['file_name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strFileNameSuffix', trim( $arrValues['file_name_suffix'] ) ); elseif( isset( $arrValues['file_name_suffix'] ) ) $this->setFileNameSuffix( $arrValues['file_name_suffix'] );
		if( isset( $arrValues['batch_memo'] ) && $boolDirectSet ) $this->set( 'm_strBatchMemo', trim( $arrValues['batch_memo'] ) ); elseif( isset( $arrValues['batch_memo'] ) ) $this->setBatchMemo( $arrValues['batch_memo'] );
		if( isset( $arrValues['last_attempted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastAttemptedOn', trim( $arrValues['last_attempted_on'] ) ); elseif( isset( $arrValues['last_attempted_on'] ) ) $this->setLastAttemptedOn( $arrValues['last_attempted_on'] );
		if( isset( $arrValues['reattempt_count'] ) && $boolDirectSet ) $this->set( 'm_intReattemptCount', trim( $arrValues['reattempt_count'] ) ); elseif( isset( $arrValues['reattempt_count'] ) ) $this->setReattemptCount( $arrValues['reattempt_count'] );
		if( isset( $arrValues['exported_through'] ) && $boolDirectSet ) $this->set( 'm_strExportedThrough', trim( $arrValues['exported_through'] ) ); elseif( isset( $arrValues['exported_through'] ) ) $this->setExportedThrough( $arrValues['exported_through'] );
		if( isset( $arrValues['post_month_range'] ) && $boolDirectSet ) $this->set( 'm_strPostMonthRange', trim( $arrValues['post_month_range'] ) ); elseif( isset( $arrValues['post_month_range'] ) ) $this->setPostMonthRange( $arrValues['post_month_range'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['taxable_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxableEntityId', trim( $arrValues['taxable_entity_id'] ) ); elseif( isset( $arrValues['taxable_entity_id'] ) ) $this->setTaxableEntityId( $arrValues['taxable_entity_id'] );
		if( isset( $arrValues['invoice_number'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceNumber', trim( $arrValues['invoice_number'] ) ); elseif( isset( $arrValues['invoice_number'] ) ) $this->setInvoiceNumber( $arrValues['invoice_number'] );
		if( isset( $arrValues['external_uuid'] ) && $boolDirectSet ) $this->set( 'm_strExternalUuid', trim( $arrValues['external_uuid'] ) ); elseif( isset( $arrValues['external_uuid'] ) ) $this->setExternalUuid( $arrValues['external_uuid'] );
		if( isset( $arrValues['uploaded_on'] ) && $boolDirectSet ) $this->set( 'm_strUploadedOn', trim( $arrValues['uploaded_on'] ) ); elseif( isset( $arrValues['uploaded_on'] ) ) $this->setUploadedOn( $arrValues['uploaded_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setAccountingExportSchedulerId( $intAccountingExportSchedulerId ) {
		$this->set( 'm_intAccountingExportSchedulerId', CStrings::strToIntDef( $intAccountingExportSchedulerId, NULL, false ) );
	}

	public function getAccountingExportSchedulerId() {
		return $this->m_intAccountingExportSchedulerId;
	}

	public function sqlAccountingExportSchedulerId() {
		return ( true == isset( $this->m_intAccountingExportSchedulerId ) ) ? ( string ) $this->m_intAccountingExportSchedulerId : 'NULL';
	}

	public function setAccountingExportFileFormatTypeId( $intAccountingExportFileFormatTypeId ) {
		$this->set( 'm_intAccountingExportFileFormatTypeId', CStrings::strToIntDef( $intAccountingExportFileFormatTypeId, NULL, false ) );
	}

	public function getAccountingExportFileFormatTypeId() {
		return $this->m_intAccountingExportFileFormatTypeId;
	}

	public function sqlAccountingExportFileFormatTypeId() {
		return ( true == isset( $this->m_intAccountingExportFileFormatTypeId ) ) ? ( string ) $this->m_intAccountingExportFileFormatTypeId : 'NULL';
	}

	public function setGlChartId( $intGlChartId ) {
		$this->set( 'm_intGlChartId', CStrings::strToIntDef( $intGlChartId, NULL, false ) );
	}

	public function getGlChartId() {
		return $this->m_intGlChartId;
	}

	public function sqlGlChartId() {
		return ( true == isset( $this->m_intGlChartId ) ) ? ( string ) $this->m_intGlChartId : 'NULL';
	}

	public function setGlTreeId( $intGlTreeId ) {
		$this->set( 'm_intGlTreeId', CStrings::strToIntDef( $intGlTreeId, NULL, false ) );
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function sqlGlTreeId() {
		return ( true == isset( $this->m_intGlTreeId ) ) ? ( string ) $this->m_intGlTreeId : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setIsSuccess( $boolIsSuccess ) {
		$this->set( 'm_boolIsSuccess', CStrings::strToBool( $boolIsSuccess ) );
	}

	public function getIsSuccess() {
		return $this->m_boolIsSuccess;
	}

	public function sqlIsSuccess() {
		return ( true == isset( $this->m_boolIsSuccess ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSuccess ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMessage', CStrings::strTrimDef( $strMessage, 500, NULL, true ), $strLocaleCode );
	}

	public function getMessage( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMessage', $strLocaleCode );
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMessage ) : '\'' . addslashes( $this->m_strMessage ) . '\'' ) : 'NULL';
	}

	public function setFileNamePrefix( $strFileNamePrefix ) {
		$this->set( 'm_strFileNamePrefix', CStrings::strTrimDef( $strFileNamePrefix, 100, NULL, true ) );
	}

	public function getFileNamePrefix() {
		return $this->m_strFileNamePrefix;
	}

	public function sqlFileNamePrefix() {
		return ( true == isset( $this->m_strFileNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileNamePrefix ) : '\'' . addslashes( $this->m_strFileNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setFileNameSuffix( $strFileNameSuffix ) {
		$this->set( 'm_strFileNameSuffix', CStrings::strTrimDef( $strFileNameSuffix, 100, NULL, true ) );
	}

	public function getFileNameSuffix() {
		return $this->m_strFileNameSuffix;
	}

	public function sqlFileNameSuffix() {
		return ( true == isset( $this->m_strFileNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileNameSuffix ) : '\'' . addslashes( $this->m_strFileNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setBatchMemo( $strBatchMemo ) {
		$this->set( 'm_strBatchMemo', CStrings::strTrimDef( $strBatchMemo, 2000, NULL, true ) );
	}

	public function getBatchMemo() {
		return $this->m_strBatchMemo;
	}

	public function sqlBatchMemo() {
		return ( true == isset( $this->m_strBatchMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchMemo ) : '\'' . addslashes( $this->m_strBatchMemo ) . '\'' ) : 'NULL';
	}

	public function setLastAttemptedOn( $strLastAttemptedOn ) {
		$this->set( 'm_strLastAttemptedOn', CStrings::strTrimDef( $strLastAttemptedOn, -1, NULL, true ) );
	}

	public function getLastAttemptedOn() {
		return $this->m_strLastAttemptedOn;
	}

	public function sqlLastAttemptedOn() {
		return ( true == isset( $this->m_strLastAttemptedOn ) ) ? '\'' . $this->m_strLastAttemptedOn . '\'' : 'NULL';
	}

	public function setReattemptCount( $intReattemptCount ) {
		$this->set( 'm_intReattemptCount', CStrings::strToIntDef( $intReattemptCount, NULL, false ) );
	}

	public function getReattemptCount() {
		return $this->m_intReattemptCount;
	}

	public function sqlReattemptCount() {
		return ( true == isset( $this->m_intReattemptCount ) ) ? ( string ) $this->m_intReattemptCount : '0';
	}

	public function setExportedThrough( $strExportedThrough ) {
		$this->set( 'm_strExportedThrough', CStrings::strTrimDef( $strExportedThrough, -1, NULL, true ) );
	}

	public function getExportedThrough() {
		return $this->m_strExportedThrough;
	}

	public function sqlExportedThrough() {
		return ( true == isset( $this->m_strExportedThrough ) ) ? '\'' . $this->m_strExportedThrough . '\'' : 'NULL';
	}

	public function setPostMonthRange( $strPostMonthRange ) {
		$this->set( 'm_strPostMonthRange', CStrings::strTrimDef( $strPostMonthRange, 500, NULL, true ) );
	}

	public function getPostMonthRange() {
		return $this->m_strPostMonthRange;
	}

	public function sqlPostMonthRange() {
		return ( true == isset( $this->m_strPostMonthRange ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostMonthRange ) : '\'' . addslashes( $this->m_strPostMonthRange ) . '\'' ) : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTaxableEntityId( $intTaxableEntityId ) {
		$this->set( 'm_intTaxableEntityId', CStrings::strToIntDef( $intTaxableEntityId, NULL, false ) );
	}

	public function getTaxableEntityId() {
		return $this->m_intTaxableEntityId;
	}

	public function sqlTaxableEntityId() {
		return ( true == isset( $this->m_intTaxableEntityId ) ) ? ( string ) $this->m_intTaxableEntityId : 'NULL';
	}

	public function setInvoiceNumber( $intInvoiceNumber ) {
		$this->set( 'm_intInvoiceNumber', CStrings::strToIntDef( $intInvoiceNumber, NULL, false ) );
	}

	public function getInvoiceNumber() {
		return $this->m_intInvoiceNumber;
	}

	public function sqlInvoiceNumber() {
		return ( true == isset( $this->m_intInvoiceNumber ) ) ? ( string ) $this->m_intInvoiceNumber : 'NULL';
	}

	public function setExternalUuid( $strExternalUuid ) {
		$this->set( 'm_strExternalUuid', CStrings::strTrimDef( $strExternalUuid, 100, NULL, true ) );
	}

	public function getExternalUuid() {
		return $this->m_strExternalUuid;
	}

	public function sqlExternalUuid() {
		return ( true == isset( $this->m_strExternalUuid ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalUuid ) : '\'' . addslashes( $this->m_strExternalUuid ) . '\'' ) : 'NULL';
	}

	public function setUploadedOn( $strUploadedOn ) {
		$this->set( 'm_strUploadedOn', CStrings::strTrimDef( $strUploadedOn, -1, NULL, true ) );
	}

	public function getUploadedOn() {
		return $this->m_strUploadedOn;
	}

	public function sqlUploadedOn() {
		return ( true == isset( $this->m_strUploadedOn ) ) ? '\'' . $this->m_strUploadedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transmission_vendor_id, accounting_export_scheduler_id, accounting_export_file_format_type_id, gl_chart_id, gl_tree_id, amount, is_success, message, file_name_prefix, file_name_suffix, batch_memo, last_attempted_on, reattempt_count, exported_through, post_month_range, batch_datetime, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, taxable_entity_id, invoice_number, external_uuid, uploaded_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlAccountingExportSchedulerId() . ', ' .
						$this->sqlAccountingExportFileFormatTypeId() . ', ' .
						$this->sqlGlChartId() . ', ' .
						$this->sqlGlTreeId() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlIsSuccess() . ', ' .
						$this->sqlMessage() . ', ' .
						$this->sqlFileNamePrefix() . ', ' .
						$this->sqlFileNameSuffix() . ', ' .
						$this->sqlBatchMemo() . ', ' .
						$this->sqlLastAttemptedOn() . ', ' .
						$this->sqlReattemptCount() . ', ' .
						$this->sqlExportedThrough() . ', ' .
						$this->sqlPostMonthRange() . ', ' .
						$this->sqlBatchDatetime() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTaxableEntityId() . ', ' .
						$this->sqlInvoiceNumber() . ', ' .
						$this->sqlExternalUuid() . ', ' .
						$this->sqlUploadedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_scheduler_id = ' . $this->sqlAccountingExportSchedulerId(). ',' ; } elseif( true == array_key_exists( 'AccountingExportSchedulerId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_scheduler_id = ' . $this->sqlAccountingExportSchedulerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_file_format_type_id = ' . $this->sqlAccountingExportFileFormatTypeId(). ',' ; } elseif( true == array_key_exists( 'AccountingExportFileFormatTypeId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_file_format_type_id = ' . $this->sqlAccountingExportFileFormatTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId(). ',' ; } elseif( true == array_key_exists( 'GlChartId', $this->getChangedColumns() ) ) { $strSql .= ' gl_chart_id = ' . $this->sqlGlChartId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess(). ',' ; } elseif( true == array_key_exists( 'IsSuccess', $this->getChangedColumns() ) ) { $strSql .= ' is_success = ' . $this->sqlIsSuccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage(). ',' ; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix(). ',' ; } elseif( true == array_key_exists( 'FileNamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix(). ',' ; } elseif( true == array_key_exists( 'FileNameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_memo = ' . $this->sqlBatchMemo(). ',' ; } elseif( true == array_key_exists( 'BatchMemo', $this->getChangedColumns() ) ) { $strSql .= ' batch_memo = ' . $this->sqlBatchMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_attempted_on = ' . $this->sqlLastAttemptedOn(). ',' ; } elseif( true == array_key_exists( 'LastAttemptedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_attempted_on = ' . $this->sqlLastAttemptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount(). ',' ; } elseif( true == array_key_exists( 'ReattemptCount', $this->getChangedColumns() ) ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_through = ' . $this->sqlExportedThrough(). ',' ; } elseif( true == array_key_exists( 'ExportedThrough', $this->getChangedColumns() ) ) { $strSql .= ' exported_through = ' . $this->sqlExportedThrough() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month_range = ' . $this->sqlPostMonthRange(). ',' ; } elseif( true == array_key_exists( 'PostMonthRange', $this->getChangedColumns() ) ) { $strSql .= ' post_month_range = ' . $this->sqlPostMonthRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime(). ',' ; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' taxable_entity_id = ' . $this->sqlTaxableEntityId(). ',' ; } elseif( true == array_key_exists( 'TaxableEntityId', $this->getChangedColumns() ) ) { $strSql .= ' taxable_entity_id = ' . $this->sqlTaxableEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber(). ',' ; } elseif( true == array_key_exists( 'InvoiceNumber', $this->getChangedColumns() ) ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_uuid = ' . $this->sqlExternalUuid(). ',' ; } elseif( true == array_key_exists( 'ExternalUuid', $this->getChangedColumns() ) ) { $strSql .= ' external_uuid = ' . $this->sqlExternalUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uploaded_on = ' . $this->sqlUploadedOn(). ',' ; } elseif( true == array_key_exists( 'UploadedOn', $this->getChangedColumns() ) ) { $strSql .= ' uploaded_on = ' . $this->sqlUploadedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'accounting_export_scheduler_id' => $this->getAccountingExportSchedulerId(),
			'accounting_export_file_format_type_id' => $this->getAccountingExportFileFormatTypeId(),
			'gl_chart_id' => $this->getGlChartId(),
			'gl_tree_id' => $this->getGlTreeId(),
			'amount' => $this->getAmount(),
			'is_success' => $this->getIsSuccess(),
			'message' => $this->getMessage(),
			'file_name_prefix' => $this->getFileNamePrefix(),
			'file_name_suffix' => $this->getFileNameSuffix(),
			'batch_memo' => $this->getBatchMemo(),
			'last_attempted_on' => $this->getLastAttemptedOn(),
			'reattempt_count' => $this->getReattemptCount(),
			'exported_through' => $this->getExportedThrough(),
			'post_month_range' => $this->getPostMonthRange(),
			'batch_datetime' => $this->getBatchDatetime(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'taxable_entity_id' => $this->getTaxableEntityId(),
			'invoice_number' => $this->getInvoiceNumber(),
			'external_uuid' => $this->getExternalUuid(),
			'uploaded_on' => $this->getUploadedOn()
		);
	}

}
?>