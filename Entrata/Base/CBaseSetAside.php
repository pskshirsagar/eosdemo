<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSetAside extends CEosSingularBase {

	const TABLE_NAME = 'public.set_asides';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intSubsidySetAsideTypeId;
	protected $m_intSubsidyIncomeLimitAreaId;
	protected $m_strName;
	protected $m_strBuildingIdentificationNumber;
	protected $m_strPlacedInServiceDate;
	protected $m_strCreditStartDate;
	protected $m_strComplianceStartDate;
	protected $m_strExtendedUseStartDate;
	protected $m_fltQualifiedUnitsPercent;
	protected $m_fltMedianIncomePercent;
	protected $m_fltFirstYearLevelPercent;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltQualifiedUnitsPercent = '0.00';
		$this->m_fltMedianIncomePercent = '0.00';
		$this->m_fltFirstYearLevelPercent = '0.00';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['subsidy_set_aside_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySetAsideTypeId', trim( $arrValues['subsidy_set_aside_type_id'] ) ); elseif( isset( $arrValues['subsidy_set_aside_type_id'] ) ) $this->setSubsidySetAsideTypeId( $arrValues['subsidy_set_aside_type_id'] );
		if( isset( $arrValues['subsidy_income_limit_area_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLimitAreaId', trim( $arrValues['subsidy_income_limit_area_id'] ) ); elseif( isset( $arrValues['subsidy_income_limit_area_id'] ) ) $this->setSubsidyIncomeLimitAreaId( $arrValues['subsidy_income_limit_area_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['building_identification_number'] ) && $boolDirectSet ) $this->set( 'm_strBuildingIdentificationNumber', trim( stripcslashes( $arrValues['building_identification_number'] ) ) ); elseif( isset( $arrValues['building_identification_number'] ) ) $this->setBuildingIdentificationNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_identification_number'] ) : $arrValues['building_identification_number'] );
		if( isset( $arrValues['placed_in_service_date'] ) && $boolDirectSet ) $this->set( 'm_strPlacedInServiceDate', trim( $arrValues['placed_in_service_date'] ) ); elseif( isset( $arrValues['placed_in_service_date'] ) ) $this->setPlacedInServiceDate( $arrValues['placed_in_service_date'] );
		if( isset( $arrValues['credit_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCreditStartDate', trim( $arrValues['credit_start_date'] ) ); elseif( isset( $arrValues['credit_start_date'] ) ) $this->setCreditStartDate( $arrValues['credit_start_date'] );
		if( isset( $arrValues['compliance_start_date'] ) && $boolDirectSet ) $this->set( 'm_strComplianceStartDate', trim( $arrValues['compliance_start_date'] ) ); elseif( isset( $arrValues['compliance_start_date'] ) ) $this->setComplianceStartDate( $arrValues['compliance_start_date'] );
		if( isset( $arrValues['extended_use_start_date'] ) && $boolDirectSet ) $this->set( 'm_strExtendedUseStartDate', trim( $arrValues['extended_use_start_date'] ) ); elseif( isset( $arrValues['extended_use_start_date'] ) ) $this->setExtendedUseStartDate( $arrValues['extended_use_start_date'] );
		if( isset( $arrValues['qualified_units_percent'] ) && $boolDirectSet ) $this->set( 'm_fltQualifiedUnitsPercent', trim( $arrValues['qualified_units_percent'] ) ); elseif( isset( $arrValues['qualified_units_percent'] ) ) $this->setQualifiedUnitsPercent( $arrValues['qualified_units_percent'] );
		if( isset( $arrValues['median_income_percent'] ) && $boolDirectSet ) $this->set( 'm_fltMedianIncomePercent', trim( $arrValues['median_income_percent'] ) ); elseif( isset( $arrValues['median_income_percent'] ) ) $this->setMedianIncomePercent( $arrValues['median_income_percent'] );
		if( isset( $arrValues['first_year_level_percent'] ) && $boolDirectSet ) $this->set( 'm_fltFirstYearLevelPercent', trim( $arrValues['first_year_level_percent'] ) ); elseif( isset( $arrValues['first_year_level_percent'] ) ) $this->setFirstYearLevelPercent( $arrValues['first_year_level_percent'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setSubsidySetAsideTypeId( $intSubsidySetAsideTypeId ) {
		$this->set( 'm_intSubsidySetAsideTypeId', CStrings::strToIntDef( $intSubsidySetAsideTypeId, NULL, false ) );
	}

	public function getSubsidySetAsideTypeId() {
		return $this->m_intSubsidySetAsideTypeId;
	}

	public function sqlSubsidySetAsideTypeId() {
		return ( true == isset( $this->m_intSubsidySetAsideTypeId ) ) ? ( string ) $this->m_intSubsidySetAsideTypeId : 'NULL';
	}

	public function setSubsidyIncomeLimitAreaId( $intSubsidyIncomeLimitAreaId ) {
		$this->set( 'm_intSubsidyIncomeLimitAreaId', CStrings::strToIntDef( $intSubsidyIncomeLimitAreaId, NULL, false ) );
	}

	public function getSubsidyIncomeLimitAreaId() {
		return $this->m_intSubsidyIncomeLimitAreaId;
	}

	public function sqlSubsidyIncomeLimitAreaId() {
		return ( true == isset( $this->m_intSubsidyIncomeLimitAreaId ) ) ? ( string ) $this->m_intSubsidyIncomeLimitAreaId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBuildingIdentificationNumber( $strBuildingIdentificationNumber ) {
		$this->set( 'm_strBuildingIdentificationNumber', CStrings::strTrimDef( $strBuildingIdentificationNumber, 9, NULL, true ) );
	}

	public function getBuildingIdentificationNumber() {
		return $this->m_strBuildingIdentificationNumber;
	}

	public function sqlBuildingIdentificationNumber() {
		return ( true == isset( $this->m_strBuildingIdentificationNumber ) ) ? '\'' . addslashes( $this->m_strBuildingIdentificationNumber ) . '\'' : 'NULL';
	}

	public function setPlacedInServiceDate( $strPlacedInServiceDate ) {
		$this->set( 'm_strPlacedInServiceDate', CStrings::strTrimDef( $strPlacedInServiceDate, -1, NULL, true ) );
	}

	public function getPlacedInServiceDate() {
		return $this->m_strPlacedInServiceDate;
	}

	public function sqlPlacedInServiceDate() {
		return ( true == isset( $this->m_strPlacedInServiceDate ) ) ? '\'' . $this->m_strPlacedInServiceDate . '\'' : 'NOW()';
	}

	public function setCreditStartDate( $strCreditStartDate ) {
		$this->set( 'm_strCreditStartDate', CStrings::strTrimDef( $strCreditStartDate, -1, NULL, true ) );
	}

	public function getCreditStartDate() {
		return $this->m_strCreditStartDate;
	}

	public function sqlCreditStartDate() {
		return ( true == isset( $this->m_strCreditStartDate ) ) ? '\'' . $this->m_strCreditStartDate . '\'' : 'NOW()';
	}

	public function setComplianceStartDate( $strComplianceStartDate ) {
		$this->set( 'm_strComplianceStartDate', CStrings::strTrimDef( $strComplianceStartDate, -1, NULL, true ) );
	}

	public function getComplianceStartDate() {
		return $this->m_strComplianceStartDate;
	}

	public function sqlComplianceStartDate() {
		return ( true == isset( $this->m_strComplianceStartDate ) ) ? '\'' . $this->m_strComplianceStartDate . '\'' : 'NOW()';
	}

	public function setExtendedUseStartDate( $strExtendedUseStartDate ) {
		$this->set( 'm_strExtendedUseStartDate', CStrings::strTrimDef( $strExtendedUseStartDate, -1, NULL, true ) );
	}

	public function getExtendedUseStartDate() {
		return $this->m_strExtendedUseStartDate;
	}

	public function sqlExtendedUseStartDate() {
		return ( true == isset( $this->m_strExtendedUseStartDate ) ) ? '\'' . $this->m_strExtendedUseStartDate . '\'' : 'NOW()';
	}

	public function setQualifiedUnitsPercent( $fltQualifiedUnitsPercent ) {
		$this->set( 'm_fltQualifiedUnitsPercent', CStrings::strToFloatDef( $fltQualifiedUnitsPercent, NULL, false, 2 ) );
	}

	public function getQualifiedUnitsPercent() {
		return $this->m_fltQualifiedUnitsPercent;
	}

	public function sqlQualifiedUnitsPercent() {
		return ( true == isset( $this->m_fltQualifiedUnitsPercent ) ) ? ( string ) $this->m_fltQualifiedUnitsPercent : '0.00';
	}

	public function setMedianIncomePercent( $fltMedianIncomePercent ) {
		$this->set( 'm_fltMedianIncomePercent', CStrings::strToFloatDef( $fltMedianIncomePercent, NULL, false, 2 ) );
	}

	public function getMedianIncomePercent() {
		return $this->m_fltMedianIncomePercent;
	}

	public function sqlMedianIncomePercent() {
		return ( true == isset( $this->m_fltMedianIncomePercent ) ) ? ( string ) $this->m_fltMedianIncomePercent : '0.00';
	}

	public function setFirstYearLevelPercent( $fltFirstYearLevelPercent ) {
		$this->set( 'm_fltFirstYearLevelPercent', CStrings::strToFloatDef( $fltFirstYearLevelPercent, NULL, false, 2 ) );
	}

	public function getFirstYearLevelPercent() {
		return $this->m_fltFirstYearLevelPercent;
	}

	public function sqlFirstYearLevelPercent() {
		return ( true == isset( $this->m_fltFirstYearLevelPercent ) ) ? ( string ) $this->m_fltFirstYearLevelPercent : '0.00';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_building_id, subsidy_set_aside_type_id, subsidy_income_limit_area_id, name, building_identification_number, placed_in_service_date, credit_start_date, compliance_start_date, extended_use_start_date, qualified_units_percent, median_income_percent, first_year_level_percent, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlSubsidySetAsideTypeId() . ', ' .
 						$this->sqlSubsidyIncomeLimitAreaId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlBuildingIdentificationNumber() . ', ' .
 						$this->sqlPlacedInServiceDate() . ', ' .
 						$this->sqlCreditStartDate() . ', ' .
 						$this->sqlComplianceStartDate() . ', ' .
 						$this->sqlExtendedUseStartDate() . ', ' .
 						$this->sqlQualifiedUnitsPercent() . ', ' .
 						$this->sqlMedianIncomePercent() . ', ' .
 						$this->sqlFirstYearLevelPercent() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_set_aside_type_id = ' . $this->sqlSubsidySetAsideTypeId() . ','; } elseif( true == array_key_exists( 'SubsidySetAsideTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_set_aside_type_id = ' . $this->sqlSubsidySetAsideTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_limit_area_id = ' . $this->sqlSubsidyIncomeLimitAreaId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeLimitAreaId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_limit_area_id = ' . $this->sqlSubsidyIncomeLimitAreaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_identification_number = ' . $this->sqlBuildingIdentificationNumber() . ','; } elseif( true == array_key_exists( 'BuildingIdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' building_identification_number = ' . $this->sqlBuildingIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' placed_in_service_date = ' . $this->sqlPlacedInServiceDate() . ','; } elseif( true == array_key_exists( 'PlacedInServiceDate', $this->getChangedColumns() ) ) { $strSql .= ' placed_in_service_date = ' . $this->sqlPlacedInServiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_start_date = ' . $this->sqlCreditStartDate() . ','; } elseif( true == array_key_exists( 'CreditStartDate', $this->getChangedColumns() ) ) { $strSql .= ' credit_start_date = ' . $this->sqlCreditStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_start_date = ' . $this->sqlComplianceStartDate() . ','; } elseif( true == array_key_exists( 'ComplianceStartDate', $this->getChangedColumns() ) ) { $strSql .= ' compliance_start_date = ' . $this->sqlComplianceStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extended_use_start_date = ' . $this->sqlExtendedUseStartDate() . ','; } elseif( true == array_key_exists( 'ExtendedUseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' extended_use_start_date = ' . $this->sqlExtendedUseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qualified_units_percent = ' . $this->sqlQualifiedUnitsPercent() . ','; } elseif( true == array_key_exists( 'QualifiedUnitsPercent', $this->getChangedColumns() ) ) { $strSql .= ' qualified_units_percent = ' . $this->sqlQualifiedUnitsPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' median_income_percent = ' . $this->sqlMedianIncomePercent() . ','; } elseif( true == array_key_exists( 'MedianIncomePercent', $this->getChangedColumns() ) ) { $strSql .= ' median_income_percent = ' . $this->sqlMedianIncomePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_year_level_percent = ' . $this->sqlFirstYearLevelPercent() . ','; } elseif( true == array_key_exists( 'FirstYearLevelPercent', $this->getChangedColumns() ) ) { $strSql .= ' first_year_level_percent = ' . $this->sqlFirstYearLevelPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'subsidy_set_aside_type_id' => $this->getSubsidySetAsideTypeId(),
			'subsidy_income_limit_area_id' => $this->getSubsidyIncomeLimitAreaId(),
			'name' => $this->getName(),
			'building_identification_number' => $this->getBuildingIdentificationNumber(),
			'placed_in_service_date' => $this->getPlacedInServiceDate(),
			'credit_start_date' => $this->getCreditStartDate(),
			'compliance_start_date' => $this->getComplianceStartDate(),
			'extended_use_start_date' => $this->getExtendedUseStartDate(),
			'qualified_units_percent' => $this->getQualifiedUnitsPercent(),
			'median_income_percent' => $this->getMedianIncomePercent(),
			'first_year_level_percent' => $this->getFirstYearLevelPercent(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>