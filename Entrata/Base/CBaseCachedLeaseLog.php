<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedLeaseLog extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_lease_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intPriorCachedLeaseLogId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_intReversalCachedLeaseLogId;
	protected $m_intOldLeaseStatusTypeId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intOldTerminationListTypeId;
	protected $m_intTerminationListTypeId;
	protected $m_intOldLeaseIntervalTypeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intActiveLeaseIntervalId;
	protected $m_intM2mLeaseIntervalId;
	protected $m_intPrimaryCustomerId;
	protected $m_intTransferLeaseId;
	protected $m_intUnitSpaceId;
	protected $m_intOccupancyTypeId;
	protected $m_intLastDelinquencyNoteEventId;
	protected $m_intSubsidyContractId;
	protected $m_intSubsidyContractTypeId;
	protected $m_intSetAsideId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strLeaseStatusType;
	protected $m_strLeaseIntervalType;
	protected $m_strCompanyName;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameMiddle;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_fltRepaymentBalance;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveInDate;
	protected $m_strNoticeDate;
	protected $m_strMoveOutDate;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_strTransferredOn;
	protected $m_strCollectionsStartDate;
	protected $m_strFmoStartedOn;
	protected $m_strFmoApprovedOn;
	protected $m_strFmoProcessedOn;
	protected $m_boolHasRepaymentAgreement;
	protected $m_intIsTransferringIn;
	protected $m_intIsMultiSlot;
	protected $m_intIsReversal;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strLeaseSubStatus;
	protected $m_intOrganizationContractId;
	protected $m_intTransferFromLeaseId;
	protected $m_boolSelectedDepositAlternative;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_fltRepaymentBalance = '0';
		$this->m_boolHasRepaymentAgreement = false;
		$this->m_intIsTransferringIn = '0';
		$this->m_intIsMultiSlot = '0';
		$this->m_intIsReversal = '0';
		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['prior_cached_lease_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCachedLeaseLogId', trim( $arrValues['prior_cached_lease_log_id'] ) ); elseif( isset( $arrValues['prior_cached_lease_log_id'] ) ) $this->setPriorCachedLeaseLogId( $arrValues['prior_cached_lease_log_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['reversal_cached_lease_log_id'] ) && $boolDirectSet ) $this->set( 'm_intReversalCachedLeaseLogId', trim( $arrValues['reversal_cached_lease_log_id'] ) ); elseif( isset( $arrValues['reversal_cached_lease_log_id'] ) ) $this->setReversalCachedLeaseLogId( $arrValues['reversal_cached_lease_log_id'] );
		if( isset( $arrValues['old_lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldLeaseStatusTypeId', trim( $arrValues['old_lease_status_type_id'] ) ); elseif( isset( $arrValues['old_lease_status_type_id'] ) ) $this->setOldLeaseStatusTypeId( $arrValues['old_lease_status_type_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['old_termination_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldTerminationListTypeId', trim( $arrValues['old_termination_list_type_id'] ) ); elseif( isset( $arrValues['old_termination_list_type_id'] ) ) $this->setOldTerminationListTypeId( $arrValues['old_termination_list_type_id'] );
		if( isset( $arrValues['termination_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationListTypeId', trim( $arrValues['termination_list_type_id'] ) ); elseif( isset( $arrValues['termination_list_type_id'] ) ) $this->setTerminationListTypeId( $arrValues['termination_list_type_id'] );
		if( isset( $arrValues['old_lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldLeaseIntervalTypeId', trim( $arrValues['old_lease_interval_type_id'] ) ); elseif( isset( $arrValues['old_lease_interval_type_id'] ) ) $this->setOldLeaseIntervalTypeId( $arrValues['old_lease_interval_type_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['active_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intActiveLeaseIntervalId', trim( $arrValues['active_lease_interval_id'] ) ); elseif( isset( $arrValues['active_lease_interval_id'] ) ) $this->setActiveLeaseIntervalId( $arrValues['active_lease_interval_id'] );
		if( isset( $arrValues['m2m_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intM2mLeaseIntervalId', trim( $arrValues['m2m_lease_interval_id'] ) ); elseif( isset( $arrValues['m2m_lease_interval_id'] ) ) $this->setM2mLeaseIntervalId( $arrValues['m2m_lease_interval_id'] );
		if( isset( $arrValues['primary_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerId', trim( $arrValues['primary_customer_id'] ) ); elseif( isset( $arrValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrValues['primary_customer_id'] );
		if( isset( $arrValues['transfer_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferLeaseId', trim( $arrValues['transfer_lease_id'] ) ); elseif( isset( $arrValues['transfer_lease_id'] ) ) $this->setTransferLeaseId( $arrValues['transfer_lease_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['last_delinquency_note_event_id'] ) && $boolDirectSet ) $this->set( 'm_intLastDelinquencyNoteEventId', trim( $arrValues['last_delinquency_note_event_id'] ) ); elseif( isset( $arrValues['last_delinquency_note_event_id'] ) ) $this->setLastDelinquencyNoteEventId( $arrValues['last_delinquency_note_event_id'] );
		if( isset( $arrValues['subsidy_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractId', trim( $arrValues['subsidy_contract_id'] ) ); elseif( isset( $arrValues['subsidy_contract_id'] ) ) $this->setSubsidyContractId( $arrValues['subsidy_contract_id'] );
		if( isset( $arrValues['subsidy_contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractTypeId', trim( $arrValues['subsidy_contract_type_id'] ) ); elseif( isset( $arrValues['subsidy_contract_type_id'] ) ) $this->setSubsidyContractTypeId( $arrValues['subsidy_contract_type_id'] );
		if( isset( $arrValues['set_aside_id'] ) && $boolDirectSet ) $this->set( 'm_intSetAsideId', trim( $arrValues['set_aside_id'] ) ); elseif( isset( $arrValues['set_aside_id'] ) ) $this->setSetAsideId( $arrValues['set_aside_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['lease_status_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStatusType', trim( $arrValues['lease_status_type'] ) ); elseif( isset( $arrValues['lease_status_type'] ) ) $this->setLeaseStatusType( $arrValues['lease_status_type'] );
		if( isset( $arrValues['lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalType', trim( $arrValues['lease_interval_type'] ) ); elseif( isset( $arrValues['lease_interval_type'] ) ) $this->setLeaseIntervalType( $arrValues['lease_interval_type'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( $arrValues['unit_number_cache'] ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['repayment_balance'] ) && $boolDirectSet ) $this->set( 'm_fltRepaymentBalance', trim( $arrValues['repayment_balance'] ) ); elseif( isset( $arrValues['repayment_balance'] ) ) $this->setRepaymentBalance( $arrValues['repayment_balance'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['notice_date'] ) && $boolDirectSet ) $this->set( 'm_strNoticeDate', trim( $arrValues['notice_date'] ) ); elseif( isset( $arrValues['notice_date'] ) ) $this->setNoticeDate( $arrValues['notice_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['transferred_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredOn', trim( $arrValues['transferred_on'] ) ); elseif( isset( $arrValues['transferred_on'] ) ) $this->setTransferredOn( $arrValues['transferred_on'] );
		if( isset( $arrValues['collections_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsStartDate', trim( $arrValues['collections_start_date'] ) ); elseif( isset( $arrValues['collections_start_date'] ) ) $this->setCollectionsStartDate( $arrValues['collections_start_date'] );
		if( isset( $arrValues['fmo_started_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoStartedOn', trim( $arrValues['fmo_started_on'] ) ); elseif( isset( $arrValues['fmo_started_on'] ) ) $this->setFmoStartedOn( $arrValues['fmo_started_on'] );
		if( isset( $arrValues['fmo_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoApprovedOn', trim( $arrValues['fmo_approved_on'] ) ); elseif( isset( $arrValues['fmo_approved_on'] ) ) $this->setFmoApprovedOn( $arrValues['fmo_approved_on'] );
		if( isset( $arrValues['fmo_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoProcessedOn', trim( $arrValues['fmo_processed_on'] ) ); elseif( isset( $arrValues['fmo_processed_on'] ) ) $this->setFmoProcessedOn( $arrValues['fmo_processed_on'] );
		if( isset( $arrValues['has_repayment_agreement'] ) && $boolDirectSet ) $this->set( 'm_boolHasRepaymentAgreement', trim( stripcslashes( $arrValues['has_repayment_agreement'] ) ) ); elseif( isset( $arrValues['has_repayment_agreement'] ) ) $this->setHasRepaymentAgreement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_repayment_agreement'] ) : $arrValues['has_repayment_agreement'] );
		if( isset( $arrValues['is_transferring_in'] ) && $boolDirectSet ) $this->set( 'm_intIsTransferringIn', trim( $arrValues['is_transferring_in'] ) ); elseif( isset( $arrValues['is_transferring_in'] ) ) $this->setIsTransferringIn( $arrValues['is_transferring_in'] );
		if( isset( $arrValues['is_multi_slot'] ) && $boolDirectSet ) $this->set( 'm_intIsMultiSlot', trim( $arrValues['is_multi_slot'] ) ); elseif( isset( $arrValues['is_multi_slot'] ) ) $this->setIsMultiSlot( $arrValues['is_multi_slot'] );
		if( isset( $arrValues['is_reversal'] ) && $boolDirectSet ) $this->set( 'm_intIsReversal', trim( $arrValues['is_reversal'] ) ); elseif( isset( $arrValues['is_reversal'] ) ) $this->setIsReversal( $arrValues['is_reversal'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['lease_sub_status'] ) && $boolDirectSet ) $this->set( 'm_strLeaseSubStatus', trim( $arrValues['lease_sub_status'] ) ); elseif( isset( $arrValues['lease_sub_status'] ) ) $this->setLeaseSubStatus( $arrValues['lease_sub_status'] );
		if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
		if( isset( $arrValues['transfer_from_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferFromLeaseId', trim( $arrValues['transfer_from_lease_id'] ) ); elseif( isset( $arrValues['transfer_from_lease_id'] ) ) $this->setTransferFromLeaseId( $arrValues['transfer_from_lease_id'] );
		if( isset( $arrValues['selected_deposit_alternative'] ) && $boolDirectSet ) $this->set( 'm_boolSelectedDepositAlternative', trim( stripcslashes( $arrValues['selected_deposit_alternative'] ) ) ); elseif( isset( $arrValues['selected_deposit_alternative'] ) ) $this->setSelectedDepositAlternative( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['selected_deposit_alternative'] ) : $arrValues['selected_deposit_alternative'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPriorCachedLeaseLogId( $intPriorCachedLeaseLogId ) {
		$this->set( 'm_intPriorCachedLeaseLogId', CStrings::strToIntDef( $intPriorCachedLeaseLogId, NULL, false ) );
	}

	public function getPriorCachedLeaseLogId() {
		return $this->m_intPriorCachedLeaseLogId;
	}

	public function sqlPriorCachedLeaseLogId() {
		return ( true == isset( $this->m_intPriorCachedLeaseLogId ) ) ? ( string ) $this->m_intPriorCachedLeaseLogId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setReversalCachedLeaseLogId( $intReversalCachedLeaseLogId ) {
		$this->set( 'm_intReversalCachedLeaseLogId', CStrings::strToIntDef( $intReversalCachedLeaseLogId, NULL, false ) );
	}

	public function getReversalCachedLeaseLogId() {
		return $this->m_intReversalCachedLeaseLogId;
	}

	public function sqlReversalCachedLeaseLogId() {
		return ( true == isset( $this->m_intReversalCachedLeaseLogId ) ) ? ( string ) $this->m_intReversalCachedLeaseLogId : 'NULL';
	}

	public function setOldLeaseStatusTypeId( $intOldLeaseStatusTypeId ) {
		$this->set( 'm_intOldLeaseStatusTypeId', CStrings::strToIntDef( $intOldLeaseStatusTypeId, NULL, false ) );
	}

	public function getOldLeaseStatusTypeId() {
		return $this->m_intOldLeaseStatusTypeId;
	}

	public function sqlOldLeaseStatusTypeId() {
		return ( true == isset( $this->m_intOldLeaseStatusTypeId ) ) ? ( string ) $this->m_intOldLeaseStatusTypeId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setOldTerminationListTypeId( $intOldTerminationListTypeId ) {
		$this->set( 'm_intOldTerminationListTypeId', CStrings::strToIntDef( $intOldTerminationListTypeId, NULL, false ) );
	}

	public function getOldTerminationListTypeId() {
		return $this->m_intOldTerminationListTypeId;
	}

	public function sqlOldTerminationListTypeId() {
		return ( true == isset( $this->m_intOldTerminationListTypeId ) ) ? ( string ) $this->m_intOldTerminationListTypeId : 'NULL';
	}

	public function setTerminationListTypeId( $intTerminationListTypeId ) {
		$this->set( 'm_intTerminationListTypeId', CStrings::strToIntDef( $intTerminationListTypeId, NULL, false ) );
	}

	public function getTerminationListTypeId() {
		return $this->m_intTerminationListTypeId;
	}

	public function sqlTerminationListTypeId() {
		return ( true == isset( $this->m_intTerminationListTypeId ) ) ? ( string ) $this->m_intTerminationListTypeId : 'NULL';
	}

	public function setOldLeaseIntervalTypeId( $intOldLeaseIntervalTypeId ) {
		$this->set( 'm_intOldLeaseIntervalTypeId', CStrings::strToIntDef( $intOldLeaseIntervalTypeId, NULL, false ) );
	}

	public function getOldLeaseIntervalTypeId() {
		return $this->m_intOldLeaseIntervalTypeId;
	}

	public function sqlOldLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intOldLeaseIntervalTypeId ) ) ? ( string ) $this->m_intOldLeaseIntervalTypeId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setActiveLeaseIntervalId( $intActiveLeaseIntervalId ) {
		$this->set( 'm_intActiveLeaseIntervalId', CStrings::strToIntDef( $intActiveLeaseIntervalId, NULL, false ) );
	}

	public function getActiveLeaseIntervalId() {
		return $this->m_intActiveLeaseIntervalId;
	}

	public function sqlActiveLeaseIntervalId() {
		return ( true == isset( $this->m_intActiveLeaseIntervalId ) ) ? ( string ) $this->m_intActiveLeaseIntervalId : 'NULL';
	}

	public function setM2mLeaseIntervalId( $intM2mLeaseIntervalId ) {
		$this->set( 'm_intM2mLeaseIntervalId', CStrings::strToIntDef( $intM2mLeaseIntervalId, NULL, false ) );
	}

	public function getM2mLeaseIntervalId() {
		return $this->m_intM2mLeaseIntervalId;
	}

	public function sqlM2mLeaseIntervalId() {
		return ( true == isset( $this->m_intM2mLeaseIntervalId ) ) ? ( string ) $this->m_intM2mLeaseIntervalId : 'NULL';
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->set( 'm_intPrimaryCustomerId', CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false ) );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function sqlPrimaryCustomerId() {
		return ( true == isset( $this->m_intPrimaryCustomerId ) ) ? ( string ) $this->m_intPrimaryCustomerId : 'NULL';
	}

	public function setTransferLeaseId( $intTransferLeaseId ) {
		$this->set( 'm_intTransferLeaseId', CStrings::strToIntDef( $intTransferLeaseId, NULL, false ) );
	}

	public function getTransferLeaseId() {
		return $this->m_intTransferLeaseId;
	}

	public function sqlTransferLeaseId() {
		return ( true == isset( $this->m_intTransferLeaseId ) ) ? ( string ) $this->m_intTransferLeaseId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setLastDelinquencyNoteEventId( $intLastDelinquencyNoteEventId ) {
		$this->set( 'm_intLastDelinquencyNoteEventId', CStrings::strToIntDef( $intLastDelinquencyNoteEventId, NULL, false ) );
	}

	public function getLastDelinquencyNoteEventId() {
		return $this->m_intLastDelinquencyNoteEventId;
	}

	public function sqlLastDelinquencyNoteEventId() {
		return ( true == isset( $this->m_intLastDelinquencyNoteEventId ) ) ? ( string ) $this->m_intLastDelinquencyNoteEventId : 'NULL';
	}

	public function setSubsidyContractId( $intSubsidyContractId ) {
		$this->set( 'm_intSubsidyContractId', CStrings::strToIntDef( $intSubsidyContractId, NULL, false ) );
	}

	public function getSubsidyContractId() {
		return $this->m_intSubsidyContractId;
	}

	public function sqlSubsidyContractId() {
		return ( true == isset( $this->m_intSubsidyContractId ) ) ? ( string ) $this->m_intSubsidyContractId : 'NULL';
	}

	public function setSubsidyContractTypeId( $intSubsidyContractTypeId ) {
		$this->set( 'm_intSubsidyContractTypeId', CStrings::strToIntDef( $intSubsidyContractTypeId, NULL, false ) );
	}

	public function getSubsidyContractTypeId() {
		return $this->m_intSubsidyContractTypeId;
	}

	public function sqlSubsidyContractTypeId() {
		return ( true == isset( $this->m_intSubsidyContractTypeId ) ) ? ( string ) $this->m_intSubsidyContractTypeId : 'NULL';
	}

	public function setSetAsideId( $intSetAsideId ) {
		$this->set( 'm_intSetAsideId', CStrings::strToIntDef( $intSetAsideId, NULL, false ) );
	}

	public function getSetAsideId() {
		return $this->m_intSetAsideId;
	}

	public function sqlSetAsideId() {
		return ( true == isset( $this->m_intSetAsideId ) ) ? ( string ) $this->m_intSetAsideId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLeaseStatusType( $strLeaseStatusType ) {
		$this->set( 'm_strLeaseStatusType', CStrings::strTrimDef( $strLeaseStatusType, 50, NULL, true ) );
	}

	public function getLeaseStatusType() {
		return $this->m_strLeaseStatusType;
	}

	public function sqlLeaseStatusType() {
		return ( true == isset( $this->m_strLeaseStatusType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseStatusType ) : '\'' . addslashes( $this->m_strLeaseStatusType ) . '\'' ) : 'NULL';
	}

	public function setLeaseIntervalType( $strLeaseIntervalType ) {
		$this->set( 'm_strLeaseIntervalType', CStrings::strTrimDef( $strLeaseIntervalType, 50, NULL, true ) );
	}

	public function getLeaseIntervalType() {
		return $this->m_strLeaseIntervalType;
	}

	public function sqlLeaseIntervalType() {
		return ( true == isset( $this->m_strLeaseIntervalType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseIntervalType ) : '\'' . addslashes( $this->m_strLeaseIntervalType ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumberCache ) : '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' ) : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setRepaymentBalance( $fltRepaymentBalance ) {
		$this->set( 'm_fltRepaymentBalance', CStrings::strToFloatDef( $fltRepaymentBalance, NULL, false, 2 ) );
	}

	public function getRepaymentBalance() {
		return $this->m_fltRepaymentBalance;
	}

	public function sqlRepaymentBalance() {
		return ( true == isset( $this->m_fltRepaymentBalance ) ) ? ( string ) $this->m_fltRepaymentBalance : '0';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setNoticeDate( $strNoticeDate ) {
		$this->set( 'm_strNoticeDate', CStrings::strTrimDef( $strNoticeDate, -1, NULL, true ) );
	}

	public function getNoticeDate() {
		return $this->m_strNoticeDate;
	}

	public function sqlNoticeDate() {
		return ( true == isset( $this->m_strNoticeDate ) ) ? '\'' . $this->m_strNoticeDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setTransferredOn( $strTransferredOn ) {
		$this->set( 'm_strTransferredOn', CStrings::strTrimDef( $strTransferredOn, -1, NULL, true ) );
	}

	public function getTransferredOn() {
		return $this->m_strTransferredOn;
	}

	public function sqlTransferredOn() {
		return ( true == isset( $this->m_strTransferredOn ) ) ? '\'' . $this->m_strTransferredOn . '\'' : 'NULL';
	}

	public function setCollectionsStartDate( $strCollectionsStartDate ) {
		$this->set( 'm_strCollectionsStartDate', CStrings::strTrimDef( $strCollectionsStartDate, -1, NULL, true ) );
	}

	public function getCollectionsStartDate() {
		return $this->m_strCollectionsStartDate;
	}

	public function sqlCollectionsStartDate() {
		return ( true == isset( $this->m_strCollectionsStartDate ) ) ? '\'' . $this->m_strCollectionsStartDate . '\'' : 'NULL';
	}

	public function setFmoStartedOn( $strFmoStartedOn ) {
		$this->set( 'm_strFmoStartedOn', CStrings::strTrimDef( $strFmoStartedOn, -1, NULL, true ) );
	}

	public function getFmoStartedOn() {
		return $this->m_strFmoStartedOn;
	}

	public function sqlFmoStartedOn() {
		return ( true == isset( $this->m_strFmoStartedOn ) ) ? '\'' . $this->m_strFmoStartedOn . '\'' : 'NULL';
	}

	public function setFmoApprovedOn( $strFmoApprovedOn ) {
		$this->set( 'm_strFmoApprovedOn', CStrings::strTrimDef( $strFmoApprovedOn, -1, NULL, true ) );
	}

	public function getFmoApprovedOn() {
		return $this->m_strFmoApprovedOn;
	}

	public function sqlFmoApprovedOn() {
		return ( true == isset( $this->m_strFmoApprovedOn ) ) ? '\'' . $this->m_strFmoApprovedOn . '\'' : 'NULL';
	}

	public function setFmoProcessedOn( $strFmoProcessedOn ) {
		$this->set( 'm_strFmoProcessedOn', CStrings::strTrimDef( $strFmoProcessedOn, -1, NULL, true ) );
	}

	public function getFmoProcessedOn() {
		return $this->m_strFmoProcessedOn;
	}

	public function sqlFmoProcessedOn() {
		return ( true == isset( $this->m_strFmoProcessedOn ) ) ? '\'' . $this->m_strFmoProcessedOn . '\'' : 'NULL';
	}

	public function setHasRepaymentAgreement( $boolHasRepaymentAgreement ) {
		$this->set( 'm_boolHasRepaymentAgreement', CStrings::strToBool( $boolHasRepaymentAgreement ) );
	}

	public function getHasRepaymentAgreement() {
		return $this->m_boolHasRepaymentAgreement;
	}

	public function sqlHasRepaymentAgreement() {
		return ( true == isset( $this->m_boolHasRepaymentAgreement ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasRepaymentAgreement ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTransferringIn( $intIsTransferringIn ) {
		$this->set( 'm_intIsTransferringIn', CStrings::strToIntDef( $intIsTransferringIn, NULL, false ) );
	}

	public function getIsTransferringIn() {
		return $this->m_intIsTransferringIn;
	}

	public function sqlIsTransferringIn() {
		return ( true == isset( $this->m_intIsTransferringIn ) ) ? ( string ) $this->m_intIsTransferringIn : '0';
	}

	public function setIsMultiSlot( $intIsMultiSlot ) {
		$this->set( 'm_intIsMultiSlot', CStrings::strToIntDef( $intIsMultiSlot, NULL, false ) );
	}

	public function getIsMultiSlot() {
		return $this->m_intIsMultiSlot;
	}

	public function sqlIsMultiSlot() {
		return ( true == isset( $this->m_intIsMultiSlot ) ) ? ( string ) $this->m_intIsMultiSlot : '0';
	}

	public function setIsReversal( $intIsReversal ) {
		$this->set( 'm_intIsReversal', CStrings::strToIntDef( $intIsReversal, NULL, false ) );
	}

	public function getIsReversal() {
		return $this->m_intIsReversal;
	}

	public function sqlIsReversal() {
		return ( true == isset( $this->m_intIsReversal ) ) ? ( string ) $this->m_intIsReversal : '0';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLeaseSubStatus( $strLeaseSubStatus ) {
		$this->set( 'm_strLeaseSubStatus', CStrings::strTrimDef( $strLeaseSubStatus, 50, NULL, true ) );
	}

	public function getLeaseSubStatus() {
		return $this->m_strLeaseSubStatus;
	}

	public function sqlLeaseSubStatus() {
		return ( true == isset( $this->m_strLeaseSubStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseSubStatus ) : '\'' . addslashes( $this->m_strLeaseSubStatus ) . '\'' ) : 'NULL';
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function sqlOrganizationContractId() {
		return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
	}

	public function setTransferFromLeaseId( $intTransferFromLeaseId ) {
		$this->set( 'm_intTransferFromLeaseId', CStrings::strToIntDef( $intTransferFromLeaseId, NULL, false ) );
	}

	public function getTransferFromLeaseId() {
		return $this->m_intTransferFromLeaseId;
	}

	public function sqlTransferFromLeaseId() {
		return ( true == isset( $this->m_intTransferFromLeaseId ) ) ? ( string ) $this->m_intTransferFromLeaseId : 'NULL';
	}

	public function setSelectedDepositAlternative( $boolSelectedDepositAlternative ) {
		$this->set( 'm_boolSelectedDepositAlternative', CStrings::strToBool( $boolSelectedDepositAlternative ) );
	}

	public function getSelectedDepositAlternative() {
		return $this->m_boolSelectedDepositAlternative;
	}

	public function sqlSelectedDepositAlternative() {
		return ( true == isset( $this->m_boolSelectedDepositAlternative ) ) ? '\'' . ( true == ( bool ) $this->m_boolSelectedDepositAlternative ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, prior_cached_lease_log_id, period_id, reporting_period_id, effective_period_id, original_period_id, reversal_cached_lease_log_id, old_lease_status_type_id, lease_status_type_id, old_termination_list_type_id, termination_list_type_id, old_lease_interval_type_id, lease_interval_type_id, active_lease_interval_id, m2m_lease_interval_id, primary_customer_id, transfer_lease_id, unit_space_id, occupancy_type_id, last_delinquency_note_event_id, subsidy_contract_id, subsidy_contract_type_id, set_aside_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, lease_status_type, lease_interval_type, company_name, name_first, name_last, name_middle, property_name, building_name, unit_number_cache, display_number, repayment_balance, lease_start_date, lease_end_date, move_in_date, notice_date, move_out_date, log_datetime, effective_date, transferred_on, collections_start_date, fmo_started_on, fmo_approved_on, fmo_processed_on, has_repayment_agreement, is_transferring_in, is_multi_slot, is_reversal, is_post_month_ignored, is_post_date_ignored, is_opening_log, updated_by, updated_on, created_by, created_on, lease_sub_status, organization_contract_id, transfer_from_lease_id, selected_deposit_alternative )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlPriorCachedLeaseLogId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlReversalCachedLeaseLogId() . ', ' .
						$this->sqlOldLeaseStatusTypeId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlOldTerminationListTypeId() . ', ' .
						$this->sqlTerminationListTypeId() . ', ' .
						$this->sqlOldLeaseIntervalTypeId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ', ' .
						$this->sqlActiveLeaseIntervalId() . ', ' .
						$this->sqlM2mLeaseIntervalId() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlTransferLeaseId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlLastDelinquencyNoteEventId() . ', ' .
						$this->sqlSubsidyContractId() . ', ' .
						$this->sqlSubsidyContractTypeId() . ', ' .
						$this->sqlSetAsideId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlLeaseStatusType() . ', ' .
						$this->sqlLeaseIntervalType() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlRepaymentBalance() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlNoticeDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlTransferredOn() . ', ' .
						$this->sqlCollectionsStartDate() . ', ' .
						$this->sqlFmoStartedOn() . ', ' .
						$this->sqlFmoApprovedOn() . ', ' .
						$this->sqlFmoProcessedOn() . ', ' .
						$this->sqlHasRepaymentAgreement() . ', ' .
						$this->sqlIsTransferringIn() . ', ' .
						$this->sqlIsMultiSlot() . ', ' .
						$this->sqlIsReversal() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLeaseSubStatus() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlTransferFromLeaseId() . ', ' .
						$this->sqlSelectedDepositAlternative() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_cached_lease_log_id = ' . $this->sqlPriorCachedLeaseLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCachedLeaseLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_cached_lease_log_id = ' . $this->sqlPriorCachedLeaseLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversal_cached_lease_log_id = ' . $this->sqlReversalCachedLeaseLogId(). ',' ; } elseif( true == array_key_exists( 'ReversalCachedLeaseLogId', $this->getChangedColumns() ) ) { $strSql .= ' reversal_cached_lease_log_id = ' . $this->sqlReversalCachedLeaseLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_lease_status_type_id = ' . $this->sqlOldLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OldLeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_lease_status_type_id = ' . $this->sqlOldLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_termination_list_type_id = ' . $this->sqlOldTerminationListTypeId(). ',' ; } elseif( true == array_key_exists( 'OldTerminationListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_termination_list_type_id = ' . $this->sqlOldTerminationListTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_list_type_id = ' . $this->sqlTerminationListTypeId(). ',' ; } elseif( true == array_key_exists( 'TerminationListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' termination_list_type_id = ' . $this->sqlTerminationListTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_lease_interval_type_id = ' . $this->sqlOldLeaseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'OldLeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_lease_interval_type_id = ' . $this->sqlOldLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' active_lease_interval_id = ' . $this->sqlActiveLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'ActiveLeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' active_lease_interval_id = ' . $this->sqlActiveLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' m2m_lease_interval_id = ' . $this->sqlM2mLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'M2mLeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' m2m_lease_interval_id = ' . $this->sqlM2mLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId(). ',' ; } elseif( true == array_key_exists( 'PrimaryCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_delinquency_note_event_id = ' . $this->sqlLastDelinquencyNoteEventId(). ',' ; } elseif( true == array_key_exists( 'LastDelinquencyNoteEventId', $this->getChangedColumns() ) ) { $strSql .= ' last_delinquency_note_event_id = ' . $this->sqlLastDelinquencyNoteEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId(). ',' ; } elseif( true == array_key_exists( 'SetAsideId', $this->getChangedColumns() ) ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusType', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalType', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache(). ',' ; } elseif( true == array_key_exists( 'UnitNumberCache', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber(). ',' ; } elseif( true == array_key_exists( 'DisplayNumber', $this->getChangedColumns() ) ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_balance = ' . $this->sqlRepaymentBalance(). ',' ; } elseif( true == array_key_exists( 'RepaymentBalance', $this->getChangedColumns() ) ) { $strSql .= ' repayment_balance = ' . $this->sqlRepaymentBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate(). ',' ; } elseif( true == array_key_exists( 'NoticeDate', $this->getChangedColumns() ) ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn(). ',' ; } elseif( true == array_key_exists( 'TransferredOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate(). ',' ; } elseif( true == array_key_exists( 'CollectionsStartDate', $this->getChangedColumns() ) ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn(). ',' ; } elseif( true == array_key_exists( 'FmoStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn(). ',' ; } elseif( true == array_key_exists( 'FmoApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn(). ',' ; } elseif( true == array_key_exists( 'FmoProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_repayment_agreement = ' . $this->sqlHasRepaymentAgreement(). ',' ; } elseif( true == array_key_exists( 'HasRepaymentAgreement', $this->getChangedColumns() ) ) { $strSql .= ' has_repayment_agreement = ' . $this->sqlHasRepaymentAgreement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn(). ',' ; } elseif( true == array_key_exists( 'IsTransferringIn', $this->getChangedColumns() ) ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_multi_slot = ' . $this->sqlIsMultiSlot(). ',' ; } elseif( true == array_key_exists( 'IsMultiSlot', $this->getChangedColumns() ) ) { $strSql .= ' is_multi_slot = ' . $this->sqlIsMultiSlot() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reversal = ' . $this->sqlIsReversal(). ',' ; } elseif( true == array_key_exists( 'IsReversal', $this->getChangedColumns() ) ) { $strSql .= ' is_reversal = ' . $this->sqlIsReversal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_sub_status = ' . $this->sqlLeaseSubStatus(). ',' ; } elseif( true == array_key_exists( 'LeaseSubStatus', $this->getChangedColumns() ) ) { $strSql .= ' lease_sub_status = ' . $this->sqlLeaseSubStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferFromLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selected_deposit_alternative = ' . $this->sqlSelectedDepositAlternative(). ',' ; } elseif( true == array_key_exists( 'SelectedDepositAlternative', $this->getChangedColumns() ) ) { $strSql .= ' selected_deposit_alternative = ' . $this->sqlSelectedDepositAlternative() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'prior_cached_lease_log_id' => $this->getPriorCachedLeaseLogId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'reversal_cached_lease_log_id' => $this->getReversalCachedLeaseLogId(),
			'old_lease_status_type_id' => $this->getOldLeaseStatusTypeId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'old_termination_list_type_id' => $this->getOldTerminationListTypeId(),
			'termination_list_type_id' => $this->getTerminationListTypeId(),
			'old_lease_interval_type_id' => $this->getOldLeaseIntervalTypeId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'active_lease_interval_id' => $this->getActiveLeaseIntervalId(),
			'm2m_lease_interval_id' => $this->getM2mLeaseIntervalId(),
			'primary_customer_id' => $this->getPrimaryCustomerId(),
			'transfer_lease_id' => $this->getTransferLeaseId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'last_delinquency_note_event_id' => $this->getLastDelinquencyNoteEventId(),
			'subsidy_contract_id' => $this->getSubsidyContractId(),
			'subsidy_contract_type_id' => $this->getSubsidyContractTypeId(),
			'set_aside_id' => $this->getSetAsideId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'lease_status_type' => $this->getLeaseStatusType(),
			'lease_interval_type' => $this->getLeaseIntervalType(),
			'company_name' => $this->getCompanyName(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'name_middle' => $this->getNameMiddle(),
			'property_name' => $this->getPropertyName(),
			'building_name' => $this->getBuildingName(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'repayment_balance' => $this->getRepaymentBalance(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'notice_date' => $this->getNoticeDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'transferred_on' => $this->getTransferredOn(),
			'collections_start_date' => $this->getCollectionsStartDate(),
			'fmo_started_on' => $this->getFmoStartedOn(),
			'fmo_approved_on' => $this->getFmoApprovedOn(),
			'fmo_processed_on' => $this->getFmoProcessedOn(),
			'has_repayment_agreement' => $this->getHasRepaymentAgreement(),
			'is_transferring_in' => $this->getIsTransferringIn(),
			'is_multi_slot' => $this->getIsMultiSlot(),
			'is_reversal' => $this->getIsReversal(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'lease_sub_status' => $this->getLeaseSubStatus(),
			'organization_contract_id' => $this->getOrganizationContractId(),
			'transfer_from_lease_id' => $this->getTransferFromLeaseId(),
			'selected_deposit_alternative' => $this->getSelectedDepositAlternative()
		);
	}

}
?>