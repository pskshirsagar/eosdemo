<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApAllocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApAllocations extends CEosPluralBase {

	/**
	 * @return CApAllocation[]
	 */
	public static function fetchApAllocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApAllocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApAllocation
	 */
	public static function fetchApAllocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApAllocation', $objDatabase );
	}

	public static function fetchApAllocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_allocations', $objDatabase );
	}

	public static function fetchApAllocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApAllocation( sprintf( 'SELECT * FROM ap_allocations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCid( $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE gl_transaction_type_id = %d AND cid = %d', ( int ) $intGlTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByOriginApAllocationIdByCid( $intOriginApAllocationId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE origin_ap_allocation_id = %d AND cid = %d', ( int ) $intOriginApAllocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByChargeApDetailIdByCid( $intChargeApDetailId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE charge_ap_detail_id = %d AND cid = %d', ( int ) $intChargeApDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCreditApDetailIdByCid( $intCreditApDetailId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE credit_ap_detail_id = %d AND cid = %d', ( int ) $intCreditApDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByApGlAccountIdByCid( $intApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE ap_gl_account_id = %d AND cid = %d', ( int ) $intApGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByPurchasesClearingGlAccountIdByCid( $intPurchasesClearingGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE purchases_clearing_gl_account_id = %d AND cid = %d', ( int ) $intPurchasesClearingGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByPendingReimbursementsGlAccountIdByCid( $intPendingReimbursementsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE pending_reimbursements_gl_account_id = %d AND cid = %d', ( int ) $intPendingReimbursementsGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByInterCoApGlAccountIdByCid( $intInterCoApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE inter_co_ap_gl_account_id = %d AND cid = %d', ( int ) $intInterCoApGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByBankGlAccountIdByCid( $intBankGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE bank_gl_account_id = %d AND cid = %d', ( int ) $intBankGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByChargeGlAccountIdByCid( $intChargeGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE charge_gl_account_id = %d AND cid = %d', ( int ) $intChargeGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCreditGlAccountIdByCid( $intCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE credit_gl_account_id = %d AND cid = %d', ( int ) $intCreditGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByChargeWipGlAccountIdByCid( $intChargeWipGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE charge_wip_gl_account_id = %d AND cid = %d', ( int ) $intChargeWipGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCreditWipGlAccountIdByCid( $intCreditWipGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE credit_wip_gl_account_id = %d AND cid = %d', ( int ) $intCreditWipGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByChargeApTransactionTypeIdByCid( $intChargeApTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE charge_ap_transaction_type_id = %d AND cid = %d', ( int ) $intChargeApTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCreditApTransactionTypeIdByCid( $intCreditApTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE credit_ap_transaction_type_id = %d AND cid = %d', ( int ) $intCreditApTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByAccrualDebitGlAccountIdByCid( $intAccrualDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE accrual_debit_gl_account_id = %d AND cid = %d', ( int ) $intAccrualDebitGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByAccrualCreditGlAccountIdByCid( $intAccrualCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE accrual_credit_gl_account_id = %d AND cid = %d', ( int ) $intAccrualCreditGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCashDebitGlAccountIdByCid( $intCashDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE cash_debit_gl_account_id = %d AND cid = %d', ( int ) $intCashDebitGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByCashCreditGlAccountIdByCid( $intCashCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE cash_credit_gl_account_id = %d AND cid = %d', ( int ) $intCashCreditGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByLumpApHeaderIdByCid( $intLumpApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE lump_ap_header_id = %d AND cid = %d', ( int ) $intLumpApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApAllocationsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchApAllocations( sprintf( 'SELECT * FROM ap_allocations WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>