<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyNotificationGroups
 * Do not add any new functions to this class.
 */

class CBasePropertyNotificationGroups extends CEosPluralBase {

	/**
	 * @return CPropertyNotificationGroup[]
	 */
	public static function fetchPropertyNotificationGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyNotificationGroup::class, $objDatabase );
	}

	/**
	 * @return CPropertyNotificationGroup
	 */
	public static function fetchPropertyNotificationGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyNotificationGroup::class, $objDatabase );
	}

	public static function fetchPropertyNotificationGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_notification_groups', $objDatabase );
	}

	public static function fetchPropertyNotificationGroupById( $intId, $objDatabase ) {
		return self::fetchPropertyNotificationGroup( sprintf( 'SELECT * FROM property_notification_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>