<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyListItem extends CEosSingularBase {

	const TABLE_NAME = 'public.property_list_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intListTypeId;
	protected $m_intListItemId;
	protected $m_intIntegrationContactTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_boolShowInEntrata;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowInEntrata = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intListTypeId', trim( $arrValues['list_type_id'] ) ); elseif( isset( $arrValues['list_type_id'] ) ) $this->setListTypeId( $arrValues['list_type_id'] );
		if( isset( $arrValues['list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intListItemId', trim( $arrValues['list_item_id'] ) ); elseif( isset( $arrValues['list_item_id'] ) ) $this->setListItemId( $arrValues['list_item_id'] );
		if( isset( $arrValues['integration_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationContactTypeId', trim( $arrValues['integration_contact_type_id'] ) ); elseif( isset( $arrValues['integration_contact_type_id'] ) ) $this->setIntegrationContactTypeId( $arrValues['integration_contact_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['show_in_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolShowInEntrata', trim( stripcslashes( $arrValues['show_in_entrata'] ) ) ); elseif( isset( $arrValues['show_in_entrata'] ) ) $this->setShowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_entrata'] ) : $arrValues['show_in_entrata'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setListTypeId( $intListTypeId ) {
		$this->set( 'm_intListTypeId', CStrings::strToIntDef( $intListTypeId, NULL, false ) );
	}

	public function getListTypeId() {
		return $this->m_intListTypeId;
	}

	public function sqlListTypeId() {
		return ( true == isset( $this->m_intListTypeId ) ) ? ( string ) $this->m_intListTypeId : 'NULL';
	}

	public function setListItemId( $intListItemId ) {
		$this->set( 'm_intListItemId', CStrings::strToIntDef( $intListItemId, NULL, false ) );
	}

	public function getListItemId() {
		return $this->m_intListItemId;
	}

	public function sqlListItemId() {
		return ( true == isset( $this->m_intListItemId ) ) ? ( string ) $this->m_intListItemId : 'NULL';
	}

	public function setIntegrationContactTypeId( $intIntegrationContactTypeId ) {
		$this->set( 'm_intIntegrationContactTypeId', CStrings::strToIntDef( $intIntegrationContactTypeId, NULL, false ) );
	}

	public function getIntegrationContactTypeId() {
		return $this->m_intIntegrationContactTypeId;
	}

	public function sqlIntegrationContactTypeId() {
		return ( true == isset( $this->m_intIntegrationContactTypeId ) ) ? ( string ) $this->m_intIntegrationContactTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setShowInEntrata( $boolShowInEntrata ) {
		$this->set( 'm_boolShowInEntrata', CStrings::strToBool( $boolShowInEntrata ) );
	}

	public function getShowInEntrata() {
		return $this->m_boolShowInEntrata;
	}

	public function sqlShowInEntrata() {
		return ( true == isset( $this->m_boolShowInEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, list_type_id, list_item_id, integration_contact_type_id, remote_primary_key, show_in_entrata, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlListTypeId() . ', ' .
 						$this->sqlListItemId() . ', ' .
 						$this->sqlIntegrationContactTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlShowInEntrata() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_type_id = ' . $this->sqlListTypeId() . ','; } elseif( true == array_key_exists( 'ListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' list_type_id = ' . $this->sqlListTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_item_id = ' . $this->sqlListItemId() . ','; } elseif( true == array_key_exists( 'ListItemId', $this->getChangedColumns() ) ) { $strSql .= ' list_item_id = ' . $this->sqlListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_contact_type_id = ' . $this->sqlIntegrationContactTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_contact_type_id = ' . $this->sqlIntegrationContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata() . ','; } elseif( true == array_key_exists( 'ShowInEntrata', $this->getChangedColumns() ) ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'list_type_id' => $this->getListTypeId(),
			'list_item_id' => $this->getListItemId(),
			'integration_contact_type_id' => $this->getIntegrationContactTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'show_in_entrata' => $this->getShowInEntrata(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>