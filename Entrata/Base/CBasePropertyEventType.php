<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEventType extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intEventTypeId;
    protected $m_intDefaultPropertyEventResultId;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->m_intEventTypeId = trim( $arrValues['event_type_id'] ); else if( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
        if( isset( $arrValues['default_property_event_result_id'] ) && $boolDirectSet ) $this->m_intDefaultPropertyEventResultId = trim( $arrValues['default_property_event_result_id'] ); else if( isset( $arrValues['default_property_event_result_id'] ) ) $this->setDefaultPropertyEventResultId( $arrValues['default_property_event_result_id'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setEventTypeId( $intEventTypeId ) {
        $this->m_intEventTypeId = CStrings::strToIntDef( $intEventTypeId, NULL, false );
    }

    public function getEventTypeId() {
        return $this->m_intEventTypeId;
    }

    public function sqlEventTypeId() {
        return ( true == isset( $this->m_intEventTypeId ) ) ? (string) $this->m_intEventTypeId : 'NULL';
    }

    public function setDefaultPropertyEventResultId( $intDefaultPropertyEventResultId ) {
        $this->m_intDefaultPropertyEventResultId = CStrings::strToIntDef( $intDefaultPropertyEventResultId, NULL, false );
    }

    public function getDefaultPropertyEventResultId() {
        return $this->m_intDefaultPropertyEventResultId;
    }

    public function sqlDefaultPropertyEventResultId() {
        return ( true == isset( $this->m_intDefaultPropertyEventResultId ) ) ? (string) $this->m_intDefaultPropertyEventResultId : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.property_event_types_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.property_event_types					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlEventTypeId() . ', ' . 		                $this->sqlDefaultPropertyEventResultId() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.property_event_types
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEventTypeId() ) != $this->getOriginalValueByFieldName ( 'event_type_id' ) ) { $arrstrOriginalValueChanges['event_type_id'] = $this->sqlEventTypeId(); $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_property_event_result_id = ' . $this->sqlDefaultPropertyEventResultId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDefaultPropertyEventResultId() ) != $this->getOriginalValueByFieldName ( 'default_property_event_result_id' ) ) { $arrstrOriginalValueChanges['default_property_event_result_id'] = $this->sqlDefaultPropertyEventResultId(); $strSql .= ' default_property_event_result_id = ' . $this->sqlDefaultPropertyEventResultId() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.property_event_types WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.property_event_types_id_seq', $objDatabase );
    }

}
?>