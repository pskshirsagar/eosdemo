<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMaintenanceLocations
 * Do not add any new functions to this class.
 */

class CBaseDefaultMaintenanceLocations extends CEosPluralBase {

	/**
	 * @return CDefaultMaintenanceLocation[]
	 */
	public static function fetchDefaultMaintenanceLocations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMaintenanceLocation::class, $objDatabase );
	}

	/**
	 * @return CDefaultMaintenanceLocation
	 */
	public static function fetchDefaultMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMaintenanceLocation::class, $objDatabase );
	}

	public static function fetchDefaultMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_maintenance_locations', $objDatabase );
	}

	public static function fetchDefaultMaintenanceLocationById( $intId, $objDatabase ) {
		return self::fetchDefaultMaintenanceLocation( sprintf( 'SELECT * FROM default_maintenance_locations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMaintenanceLocationsByMaintenanceLocationTypeId( $intMaintenanceLocationTypeId, $objDatabase ) {
		return self::fetchDefaultMaintenanceLocations( sprintf( 'SELECT * FROM default_maintenance_locations WHERE maintenance_location_type_id = %d', $intMaintenanceLocationTypeId ), $objDatabase );
	}

}
?>