<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseAssociation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intUnitSpaceId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intCustomerId;
	protected $m_intCompanyEmployeeId;
	protected $m_intArCascadeId;
	protected $m_intArCascadeReferenceId;
	protected $m_intArOriginId;
	protected $m_intArOriginReferenceId;
	protected $m_intArOriginObjectId;
	protected $m_intQuoteId;
	protected $m_intQuoteLeaseTermId;
	protected $m_strRemotePrimaryKey;
	protected $m_strAssociationDatetime;
	protected $m_strDescription;
	protected $m_strNotes;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strNoticeDate;
	protected $m_boolLockToLeaseDates;
	protected $m_boolUseLeaseStartDate;
	protected $m_boolUseLeaseMoveOutDate;
	protected $m_boolHideDescription;
	protected $m_boolIsDefault;
	protected $m_boolIsFeatured;
	protected $m_boolIsOptional;
	protected $m_boolIsPublished;
	protected $m_boolIsSelectedQuote;
	protected $m_boolHideRates;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMappingId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intLeaseStatusTypeId = '1';
		$this->m_boolLockToLeaseDates = false;
		$this->m_boolUseLeaseStartDate = true;
		$this->m_boolUseLeaseMoveOutDate = true;
		$this->m_boolHideDescription = false;
		$this->m_boolIsDefault = false;
		$this->m_boolIsFeatured = false;
		$this->m_boolIsOptional = true;
		$this->m_boolIsPublished = true;
		$this->m_boolIsSelectedQuote = true;
		$this->m_boolHideRates = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalId', trim( $arrValues['lease_interval_id'] ) ); elseif( isset( $arrValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrValues['lease_interval_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeId', trim( $arrValues['ar_cascade_id'] ) ); elseif( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
		if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeReferenceId', trim( $arrValues['ar_cascade_reference_id'] ) ); elseif( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
		if( isset( $arrValues['ar_origin_object_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginObjectId', trim( $arrValues['ar_origin_object_id'] ) ); elseif( isset( $arrValues['ar_origin_object_id'] ) ) $this->setArOriginObjectId( $arrValues['ar_origin_object_id'] );
		if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
		if( isset( $arrValues['quote_lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteLeaseTermId', trim( $arrValues['quote_lease_term_id'] ) ); elseif( isset( $arrValues['quote_lease_term_id'] ) ) $this->setQuoteLeaseTermId( $arrValues['quote_lease_term_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['association_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAssociationDatetime', trim( $arrValues['association_datetime'] ) ); elseif( isset( $arrValues['association_datetime'] ) ) $this->setAssociationDatetime( $arrValues['association_datetime'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['notice_date'] ) && $boolDirectSet ) $this->set( 'm_strNoticeDate', trim( $arrValues['notice_date'] ) ); elseif( isset( $arrValues['notice_date'] ) ) $this->setNoticeDate( $arrValues['notice_date'] );
		if( isset( $arrValues['lock_to_lease_dates'] ) && $boolDirectSet ) $this->set( 'm_boolLockToLeaseDates', trim( stripcslashes( $arrValues['lock_to_lease_dates'] ) ) ); elseif( isset( $arrValues['lock_to_lease_dates'] ) ) $this->setLockToLeaseDates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lock_to_lease_dates'] ) : $arrValues['lock_to_lease_dates'] );
		if( isset( $arrValues['use_lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseStartDate', trim( stripcslashes( $arrValues['use_lease_start_date'] ) ) ); elseif( isset( $arrValues['use_lease_start_date'] ) ) $this->setUseLeaseStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_start_date'] ) : $arrValues['use_lease_start_date'] );
		if( isset( $arrValues['use_lease_move_out_date'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseMoveOutDate', trim( stripcslashes( $arrValues['use_lease_move_out_date'] ) ) ); elseif( isset( $arrValues['use_lease_move_out_date'] ) ) $this->setUseLeaseMoveOutDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_move_out_date'] ) : $arrValues['use_lease_move_out_date'] );
		if( isset( $arrValues['hide_description'] ) && $boolDirectSet ) $this->set( 'm_boolHideDescription', trim( stripcslashes( $arrValues['hide_description'] ) ) ); elseif( isset( $arrValues['hide_description'] ) ) $this->setHideDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_description'] ) : $arrValues['hide_description'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptional', trim( stripcslashes( $arrValues['is_optional'] ) ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optional'] ) : $arrValues['is_optional'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_selected_quote'] ) && $boolDirectSet ) $this->set( 'm_boolIsSelectedQuote', trim( stripcslashes( $arrValues['is_selected_quote'] ) ) ); elseif( isset( $arrValues['is_selected_quote'] ) ) $this->setIsSelectedQuote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_selected_quote'] ) : $arrValues['is_selected_quote'] );
		if( isset( $arrValues['hide_rates'] ) && $boolDirectSet ) $this->set( 'm_boolHideRates', trim( stripcslashes( $arrValues['hide_rates'] ) ) ); elseif( isset( $arrValues['hide_rates'] ) ) $this->setHideRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_rates'] ) : $arrValues['hide_rates'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingId', trim( $arrValues['mapping_id'] ) ); elseif( isset( $arrValues['mapping_id'] ) ) $this->setMappingId( $arrValues['mapping_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->set( 'm_intLeaseIntervalId', CStrings::strToIntDef( $intLeaseIntervalId, NULL, false ) );
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function sqlLeaseIntervalId() {
		return ( true == isset( $this->m_intLeaseIntervalId ) ) ? ( string ) $this->m_intLeaseIntervalId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : '1';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->set( 'm_intArCascadeId', CStrings::strToIntDef( $intArCascadeId, NULL, false ) );
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function sqlArCascadeId() {
		return ( true == isset( $this->m_intArCascadeId ) ) ? ( string ) $this->m_intArCascadeId : 'NULL';
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->set( 'm_intArCascadeReferenceId', CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false ) );
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function sqlArCascadeReferenceId() {
		return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? ( string ) $this->m_intArCascadeReferenceId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function sqlArOriginReferenceId() {
		return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
	}

	public function setArOriginObjectId( $intArOriginObjectId ) {
		$this->set( 'm_intArOriginObjectId', CStrings::strToIntDef( $intArOriginObjectId, NULL, false ) );
	}

	public function getArOriginObjectId() {
		return $this->m_intArOriginObjectId;
	}

	public function sqlArOriginObjectId() {
		return ( true == isset( $this->m_intArOriginObjectId ) ) ? ( string ) $this->m_intArOriginObjectId : 'NULL';
	}

	public function setQuoteId( $intQuoteId ) {
		$this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function sqlQuoteId() {
		return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
	}

	public function setQuoteLeaseTermId( $intQuoteLeaseTermId ) {
		$this->set( 'm_intQuoteLeaseTermId', CStrings::strToIntDef( $intQuoteLeaseTermId, NULL, false ) );
	}

	public function getQuoteLeaseTermId() {
		return $this->m_intQuoteLeaseTermId;
	}

	public function sqlQuoteLeaseTermId() {
		return ( true == isset( $this->m_intQuoteLeaseTermId ) ) ? ( string ) $this->m_intQuoteLeaseTermId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 264, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setAssociationDatetime( $strAssociationDatetime ) {
		$this->set( 'm_strAssociationDatetime', CStrings::strTrimDef( $strAssociationDatetime, -1, NULL, true ) );
	}

	public function getAssociationDatetime() {
		return $this->m_strAssociationDatetime;
	}

	public function sqlAssociationDatetime() {
		return ( true == isset( $this->m_strAssociationDatetime ) ) ? '\'' . $this->m_strAssociationDatetime . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 240, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setNoticeDate( $strNoticeDate ) {
		$this->set( 'm_strNoticeDate', CStrings::strTrimDef( $strNoticeDate, -1, NULL, true ) );
	}

	public function getNoticeDate() {
		return $this->m_strNoticeDate;
	}

	public function sqlNoticeDate() {
		return ( true == isset( $this->m_strNoticeDate ) ) ? '\'' . $this->m_strNoticeDate . '\'' : 'NULL';
	}

	public function setLockToLeaseDates( $boolLockToLeaseDates ) {
		$this->set( 'm_boolLockToLeaseDates', CStrings::strToBool( $boolLockToLeaseDates ) );
	}

	public function getLockToLeaseDates() {
		return $this->m_boolLockToLeaseDates;
	}

	public function sqlLockToLeaseDates() {
		return ( true == isset( $this->m_boolLockToLeaseDates ) ) ? '\'' . ( true == ( bool ) $this->m_boolLockToLeaseDates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseStartDate( $boolUseLeaseStartDate ) {
		$this->set( 'm_boolUseLeaseStartDate', CStrings::strToBool( $boolUseLeaseStartDate ) );
	}

	public function getUseLeaseStartDate() {
		return $this->m_boolUseLeaseStartDate;
	}

	public function sqlUseLeaseStartDate() {
		return ( true == isset( $this->m_boolUseLeaseStartDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseStartDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseMoveOutDate( $boolUseLeaseMoveOutDate ) {
		$this->set( 'm_boolUseLeaseMoveOutDate', CStrings::strToBool( $boolUseLeaseMoveOutDate ) );
	}

	public function getUseLeaseMoveOutDate() {
		return $this->m_boolUseLeaseMoveOutDate;
	}

	public function sqlUseLeaseMoveOutDate() {
		return ( true == isset( $this->m_boolUseLeaseMoveOutDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseMoveOutDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideDescription( $boolHideDescription ) {
		$this->set( 'm_boolHideDescription', CStrings::strToBool( $boolHideDescription ) );
	}

	public function getHideDescription() {
		return $this->m_boolHideDescription;
	}

	public function sqlHideDescription() {
		return ( true == isset( $this->m_boolHideDescription ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideDescription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->set( 'm_boolIsOptional', CStrings::strToBool( $boolIsOptional ) );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_boolIsOptional ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptional ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSelectedQuote( $boolIsSelectedQuote ) {
		$this->set( 'm_boolIsSelectedQuote', CStrings::strToBool( $boolIsSelectedQuote ) );
	}

	public function getIsSelectedQuote() {
		return $this->m_boolIsSelectedQuote;
	}

	public function sqlIsSelectedQuote() {
		return ( true == isset( $this->m_boolIsSelectedQuote ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSelectedQuote ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideRates( $boolHideRates ) {
		$this->set( 'm_boolHideRates', CStrings::strToBool( $boolHideRates ) );
	}

	public function getHideRates() {
		return $this->m_boolHideRates;
	}

	public function sqlHideRates() {
		return ( true == isset( $this->m_boolHideRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMappingId( $intMappingId ) {
		$this->set( 'm_intMappingId', CStrings::strToIntDef( $intMappingId, NULL, false ) );
	}

	public function getMappingId() {
		return $this->m_intMappingId;
	}

	public function sqlMappingId() {
		return ( true == isset( $this->m_intMappingId ) ) ? ( string ) $this->m_intMappingId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, unit_space_id, lease_id, lease_interval_id, lease_status_type_id, customer_id, company_employee_id, ar_cascade_id, ar_cascade_reference_id, ar_origin_id, ar_origin_reference_id, ar_origin_object_id, quote_id, quote_lease_term_id, remote_primary_key, association_datetime, description, notes, start_date, end_date, move_in_date, move_out_date, notice_date, lock_to_lease_dates, use_lease_start_date, use_lease_move_out_date, hide_description, is_default, is_featured, is_optional, is_published, is_selected_quote, hide_rates, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, mapping_id, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyFloorplanId() . ', ' .
		          $this->sqlUnitTypeId() . ', ' .
		          $this->sqlUnitSpaceId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlLeaseIntervalId() . ', ' .
		          $this->sqlLeaseStatusTypeId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlCompanyEmployeeId() . ', ' .
		          $this->sqlArCascadeId() . ', ' .
		          $this->sqlArCascadeReferenceId() . ', ' .
		          $this->sqlArOriginId() . ', ' .
		          $this->sqlArOriginReferenceId() . ', ' .
		          $this->sqlArOriginObjectId() . ', ' .
		          $this->sqlQuoteId() . ', ' .
		          $this->sqlQuoteLeaseTermId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlAssociationDatetime() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlNotes() . ', ' .
		          $this->sqlStartDate() . ', ' .
		          $this->sqlEndDate() . ', ' .
		          $this->sqlMoveInDate() . ', ' .
		          $this->sqlMoveOutDate() . ', ' .
		          $this->sqlNoticeDate() . ', ' .
		          $this->sqlLockToLeaseDates() . ', ' .
		          $this->sqlUseLeaseStartDate() . ', ' .
		          $this->sqlUseLeaseMoveOutDate() . ', ' .
		          $this->sqlHideDescription() . ', ' .
		          $this->sqlIsDefault() . ', ' .
		          $this->sqlIsFeatured() . ', ' .
		          $this->sqlIsOptional() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlIsSelectedQuote() . ', ' .
		          $this->sqlHideRates() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlMappingId() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_id = ' . $this->sqlLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId(). ',' ; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId(). ',' ; } elseif( true == array_key_exists( 'ArOriginObjectId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_object_id = ' . $this->sqlArOriginObjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId(). ',' ; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_lease_term_id = ' . $this->sqlQuoteLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'QuoteLeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' quote_lease_term_id = ' . $this->sqlQuoteLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime(). ',' ; } elseif( true == array_key_exists( 'AssociationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate(). ',' ; } elseif( true == array_key_exists( 'NoticeDate', $this->getChangedColumns() ) ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_to_lease_dates = ' . $this->sqlLockToLeaseDates(). ',' ; } elseif( true == array_key_exists( 'LockToLeaseDates', $this->getChangedColumns() ) ) { $strSql .= ' lock_to_lease_dates = ' . $this->sqlLockToLeaseDates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_start_date = ' . $this->sqlUseLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'UseLeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_start_date = ' . $this->sqlUseLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_move_out_date = ' . $this->sqlUseLeaseMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'UseLeaseMoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_move_out_date = ' . $this->sqlUseLeaseMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_description = ' . $this->sqlHideDescription(). ',' ; } elseif( true == array_key_exists( 'HideDescription', $this->getChangedColumns() ) ) { $strSql .= ' hide_description = ' . $this->sqlHideDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured(). ',' ; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_selected_quote = ' . $this->sqlIsSelectedQuote(). ',' ; } elseif( true == array_key_exists( 'IsSelectedQuote', $this->getChangedColumns() ) ) { $strSql .= ' is_selected_quote = ' . $this->sqlIsSelectedQuote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_rates = ' . $this->sqlHideRates(). ',' ; } elseif( true == array_key_exists( 'HideRates', $this->getChangedColumns() ) ) { $strSql .= ' hide_rates = ' . $this->sqlHideRates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId(). ',' ; } elseif( true == array_key_exists( 'MappingId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'lease_id' => $this->getLeaseId(),
			'lease_interval_id' => $this->getLeaseIntervalId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'customer_id' => $this->getCustomerId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'ar_cascade_id' => $this->getArCascadeId(),
			'ar_cascade_reference_id' => $this->getArCascadeReferenceId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_origin_reference_id' => $this->getArOriginReferenceId(),
			'ar_origin_object_id' => $this->getArOriginObjectId(),
			'quote_id' => $this->getQuoteId(),
			'quote_lease_term_id' => $this->getQuoteLeaseTermId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'association_datetime' => $this->getAssociationDatetime(),
			'description' => $this->getDescription(),
			'notes' => $this->getNotes(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'notice_date' => $this->getNoticeDate(),
			'lock_to_lease_dates' => $this->getLockToLeaseDates(),
			'use_lease_start_date' => $this->getUseLeaseStartDate(),
			'use_lease_move_out_date' => $this->getUseLeaseMoveOutDate(),
			'hide_description' => $this->getHideDescription(),
			'is_default' => $this->getIsDefault(),
			'is_featured' => $this->getIsFeatured(),
			'is_optional' => $this->getIsOptional(),
			'is_published' => $this->getIsPublished(),
			'is_selected_quote' => $this->getIsSelectedQuote(),
			'hide_rates' => $this->getHideRates(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'mapping_id' => $this->getMappingId(),
			'details' => $this->getDetails()
		);
	}

}
?>