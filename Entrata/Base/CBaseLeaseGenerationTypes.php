<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseGenerationTypes
 * Do not add any new functions to this class.
 */

class CBaseLeaseGenerationTypes extends CEosPluralBase {

	/**
	 * @return CLeaseGenerationType[]
	 */
	public static function fetchLeaseGenerationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLeaseGenerationType::class, $objDatabase );
	}

	/**
	 * @return CLeaseGenerationType
	 */
	public static function fetchLeaseGenerationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseGenerationType::class, $objDatabase );
	}

	public static function fetchLeaseGenerationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_generation_types', $objDatabase );
	}

	public static function fetchLeaseGenerationTypeById( $intId, $objDatabase ) {
		return self::fetchLeaseGenerationType( sprintf( 'SELECT * FROM lease_generation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>