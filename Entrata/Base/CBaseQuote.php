<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuote extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.quotes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intUnitSpaceId;
	protected $m_intApprovalCustomerId;
	protected $m_intLeasingTierSpecialId;
	protected $m_intSelectedQuoteLeaseTermId;
	protected $m_intEmailEventId;
	protected $m_strLeaseStartDate;
	protected $m_strRemotePrimaryKey;
	protected $m_strSignature;
	protected $m_strIpAddress;
	protected $m_boolIsRenewal;
	protected $m_strEmailedOn;
	protected $m_strNotifiedOn;
	protected $m_strExpiresOn;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intCancelledBy;
	protected $m_strCancelledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMappingRenewalTierId;
	protected $m_intLeasingTierOfferId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeclinedBy;
	protected $m_strDeclinedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRenewal = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['approval_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intApprovalCustomerId', trim( $arrValues['approval_customer_id'] ) ); elseif( isset( $arrValues['approval_customer_id'] ) ) $this->setApprovalCustomerId( $arrValues['approval_customer_id'] );
		if( isset( $arrValues['leasing_tier_special_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingTierSpecialId', trim( $arrValues['leasing_tier_special_id'] ) ); elseif( isset( $arrValues['leasing_tier_special_id'] ) ) $this->setLeasingTierSpecialId( $arrValues['leasing_tier_special_id'] );
		if( isset( $arrValues['selected_quote_lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intSelectedQuoteLeaseTermId', trim( $arrValues['selected_quote_lease_term_id'] ) ); elseif( isset( $arrValues['selected_quote_lease_term_id'] ) ) $this->setSelectedQuoteLeaseTermId( $arrValues['selected_quote_lease_term_id'] );
		if( isset( $arrValues['email_event_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailEventId', trim( $arrValues['email_event_id'] ) ); elseif( isset( $arrValues['email_event_id'] ) ) $this->setEmailEventId( $arrValues['email_event_id'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( $arrValues['signature'] ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( $arrValues['signature'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['is_renewal'] ) && $boolDirectSet ) $this->set( 'm_boolIsRenewal', trim( stripcslashes( $arrValues['is_renewal'] ) ) ); elseif( isset( $arrValues['is_renewal'] ) ) $this->setIsRenewal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_renewal'] ) : $arrValues['is_renewal'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['notified_on'] ) && $boolDirectSet ) $this->set( 'm_strNotifiedOn', trim( $arrValues['notified_on'] ) ); elseif( isset( $arrValues['notified_on'] ) ) $this->setNotifiedOn( $arrValues['notified_on'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['cancelled_by'] ) && $boolDirectSet ) $this->set( 'm_intCancelledBy', trim( $arrValues['cancelled_by'] ) ); elseif( isset( $arrValues['cancelled_by'] ) ) $this->setCancelledBy( $arrValues['cancelled_by'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mapping_renewal_tier_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingRenewalTierId', trim( $arrValues['mapping_renewal_tier_id'] ) ); elseif( isset( $arrValues['mapping_renewal_tier_id'] ) ) $this->setMappingRenewalTierId( $arrValues['mapping_renewal_tier_id'] );
		if( isset( $arrValues['leasing_tier_offer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingTierOfferId', trim( $arrValues['leasing_tier_offer_id'] ) ); elseif( isset( $arrValues['leasing_tier_offer_id'] ) ) $this->setLeasingTierOfferId( $arrValues['leasing_tier_offer_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['declined_by'] ) && $boolDirectSet ) $this->set( 'm_intDeclinedBy', trim( $arrValues['declined_by'] ) ); elseif( isset( $arrValues['declined_by'] ) ) $this->setDeclinedBy( $arrValues['declined_by'] );
		if( isset( $arrValues['declined_on'] ) && $boolDirectSet ) $this->set( 'm_strDeclinedOn', trim( $arrValues['declined_on'] ) ); elseif( isset( $arrValues['declined_on'] ) ) $this->setDeclinedOn( $arrValues['declined_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setApprovalCustomerId( $intApprovalCustomerId ) {
		$this->set( 'm_intApprovalCustomerId', CStrings::strToIntDef( $intApprovalCustomerId, NULL, false ) );
	}

	public function getApprovalCustomerId() {
		return $this->m_intApprovalCustomerId;
	}

	public function sqlApprovalCustomerId() {
		return ( true == isset( $this->m_intApprovalCustomerId ) ) ? ( string ) $this->m_intApprovalCustomerId : 'NULL';
	}

	public function setLeasingTierSpecialId( $intLeasingTierSpecialId ) {
		$this->set( 'm_intLeasingTierSpecialId', CStrings::strToIntDef( $intLeasingTierSpecialId, NULL, false ) );
	}

	public function getLeasingTierSpecialId() {
		return $this->m_intLeasingTierSpecialId;
	}

	public function sqlLeasingTierSpecialId() {
		return ( true == isset( $this->m_intLeasingTierSpecialId ) ) ? ( string ) $this->m_intLeasingTierSpecialId : 'NULL';
	}

	public function setSelectedQuoteLeaseTermId( $intSelectedQuoteLeaseTermId ) {
		$this->set( 'm_intSelectedQuoteLeaseTermId', CStrings::strToIntDef( $intSelectedQuoteLeaseTermId, NULL, false ) );
	}

	public function getSelectedQuoteLeaseTermId() {
		return $this->m_intSelectedQuoteLeaseTermId;
	}

	public function sqlSelectedQuoteLeaseTermId() {
		return ( true == isset( $this->m_intSelectedQuoteLeaseTermId ) ) ? ( string ) $this->m_intSelectedQuoteLeaseTermId : 'NULL';
	}

	public function setEmailEventId( $intEmailEventId ) {
		$this->set( 'm_intEmailEventId', CStrings::strToIntDef( $intEmailEventId, NULL, false ) );
	}

	public function getEmailEventId() {
		return $this->m_intEmailEventId;
	}

	public function sqlEmailEventId() {
		return ( true == isset( $this->m_intEmailEventId ) ) ? ( string ) $this->m_intEmailEventId : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NOW()';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 200, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSignature ) : '\'' . addslashes( $this->m_strSignature ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 30, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setIsRenewal( $boolIsRenewal ) {
		$this->set( 'm_boolIsRenewal', CStrings::strToBool( $boolIsRenewal ) );
	}

	public function getIsRenewal() {
		return $this->m_boolIsRenewal;
	}

	public function sqlIsRenewal() {
		return ( true == isset( $this->m_boolIsRenewal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRenewal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setNotifiedOn( $strNotifiedOn ) {
		$this->set( 'm_strNotifiedOn', CStrings::strTrimDef( $strNotifiedOn, -1, NULL, true ) );
	}

	public function getNotifiedOn() {
		return $this->m_strNotifiedOn;
	}

	public function sqlNotifiedOn() {
		return ( true == isset( $this->m_strNotifiedOn ) ) ? '\'' . $this->m_strNotifiedOn . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NOW()';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setCancelledBy( $intCancelledBy ) {
		$this->set( 'm_intCancelledBy', CStrings::strToIntDef( $intCancelledBy, NULL, false ) );
	}

	public function getCancelledBy() {
		return $this->m_intCancelledBy;
	}

	public function sqlCancelledBy() {
		return ( true == isset( $this->m_intCancelledBy ) ) ? ( string ) $this->m_intCancelledBy : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMappingRenewalTierId( $intMappingRenewalTierId ) {
		$this->set( 'm_intMappingRenewalTierId', CStrings::strToIntDef( $intMappingRenewalTierId, NULL, false ) );
	}

	public function getMappingRenewalTierId() {
		return $this->m_intMappingRenewalTierId;
	}

	public function sqlMappingRenewalTierId() {
		return ( true == isset( $this->m_intMappingRenewalTierId ) ) ? ( string ) $this->m_intMappingRenewalTierId : 'NULL';
	}

	public function setLeasingTierOfferId( $intLeasingTierOfferId ) {
		$this->set( 'm_intLeasingTierOfferId', CStrings::strToIntDef( $intLeasingTierOfferId, NULL, false ) );
	}

	public function getLeasingTierOfferId() {
		return $this->m_intLeasingTierOfferId;
	}

	public function sqlLeasingTierOfferId() {
		return ( true == isset( $this->m_intLeasingTierOfferId ) ) ? ( string ) $this->m_intLeasingTierOfferId : 'NULL';
	}

	public function setDeclinedBy( $intDeclinedBy ) {
		$this->set( 'm_intDeclinedBy', CStrings::strToIntDef( $intDeclinedBy, NULL, false ) );
	}

	public function getDeclinedBy() {
		return $this->m_intDeclinedBy;
	}

	public function sqlDeclinedBy() {
		return ( true == isset( $this->m_intDeclinedBy ) ) ? ( string ) $this->m_intDeclinedBy : 'NULL';
	}

	public function setDeclinedOn( $strDeclinedOn ) {
		$this->set( 'm_strDeclinedOn', CStrings::strTrimDef( $strDeclinedOn, -1, NULL, true ) );
	}

	public function getDeclinedOn() {
		return $this->m_strDeclinedOn;
	}

	public function sqlDeclinedOn() {
		return ( true == isset( $this->m_strDeclinedOn ) ) ? '\'' . $this->m_strDeclinedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, unit_space_id, approval_customer_id, leasing_tier_special_id, selected_quote_lease_term_id, email_event_id, lease_start_date, remote_primary_key, signature, ip_address, is_renewal, emailed_on, notified_on, expires_on, accepted_by, accepted_on, cancelled_by, cancelled_on, updated_by, updated_on, created_by, created_on, mapping_renewal_tier_id, leasing_tier_offer_id, details, declined_by, declined_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlApprovalCustomerId() . ', ' .
						$this->sqlLeasingTierSpecialId() . ', ' .
						$this->sqlSelectedQuoteLeaseTermId() . ', ' .
						$this->sqlEmailEventId() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSignature() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlIsRenewal() . ', ' .
						$this->sqlEmailedOn() . ', ' .
						$this->sqlNotifiedOn() . ', ' .
						$this->sqlExpiresOn() . ', ' .
						$this->sqlAcceptedBy() . ', ' .
						$this->sqlAcceptedOn() . ', ' .
						$this->sqlCancelledBy() . ', ' .
						$this->sqlCancelledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMappingRenewalTierId() . ', ' .
						$this->sqlLeasingTierOfferId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeclinedBy() . ', ' .
						$this->sqlDeclinedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_customer_id = ' . $this->sqlApprovalCustomerId(). ',' ; } elseif( true == array_key_exists( 'ApprovalCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' approval_customer_id = ' . $this->sqlApprovalCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_tier_special_id = ' . $this->sqlLeasingTierSpecialId(). ',' ; } elseif( true == array_key_exists( 'LeasingTierSpecialId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_tier_special_id = ' . $this->sqlLeasingTierSpecialId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selected_quote_lease_term_id = ' . $this->sqlSelectedQuoteLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'SelectedQuoteLeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' selected_quote_lease_term_id = ' . $this->sqlSelectedQuoteLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_event_id = ' . $this->sqlEmailEventId(). ',' ; } elseif( true == array_key_exists( 'EmailEventId', $this->getChangedColumns() ) ) { $strSql .= ' email_event_id = ' . $this->sqlEmailEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature(). ',' ; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal(). ',' ; } elseif( true == array_key_exists( 'IsRenewal', $this->getChangedColumns() ) ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn(). ',' ; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn(). ',' ; } elseif( true == array_key_exists( 'NotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' notified_on = ' . $this->sqlNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn(). ',' ; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy(). ',' ; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy(). ',' ; } elseif( true == array_key_exists( 'CancelledBy', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_renewal_tier_id = ' . $this->sqlMappingRenewalTierId(). ',' ; } elseif( true == array_key_exists( 'MappingRenewalTierId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_renewal_tier_id = ' . $this->sqlMappingRenewalTierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_tier_offer_id = ' . $this->sqlLeasingTierOfferId(). ',' ; } elseif( true == array_key_exists( 'LeasingTierOfferId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_tier_offer_id = ' . $this->sqlLeasingTierOfferId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' declined_by = ' . $this->sqlDeclinedBy(). ',' ; } elseif( true == array_key_exists( 'DeclinedBy', $this->getChangedColumns() ) ) { $strSql .= ' declined_by = ' . $this->sqlDeclinedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' declined_on = ' . $this->sqlDeclinedOn(). ',' ; } elseif( true == array_key_exists( 'DeclinedOn', $this->getChangedColumns() ) ) { $strSql .= ' declined_on = ' . $this->sqlDeclinedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'approval_customer_id' => $this->getApprovalCustomerId(),
			'leasing_tier_special_id' => $this->getLeasingTierSpecialId(),
			'selected_quote_lease_term_id' => $this->getSelectedQuoteLeaseTermId(),
			'email_event_id' => $this->getEmailEventId(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'signature' => $this->getSignature(),
			'ip_address' => $this->getIpAddress(),
			'is_renewal' => $this->getIsRenewal(),
			'emailed_on' => $this->getEmailedOn(),
			'notified_on' => $this->getNotifiedOn(),
			'expires_on' => $this->getExpiresOn(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'cancelled_by' => $this->getCancelledBy(),
			'cancelled_on' => $this->getCancelledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'mapping_renewal_tier_id' => $this->getMappingRenewalTierId(),
			'leasing_tier_offer_id' => $this->getLeasingTierOfferId(),
			'details' => $this->getDetails(),
			'declined_by' => $this->getDeclinedBy(),
			'declined_on' => $this->getDeclinedOn()
		);
	}

}
?>