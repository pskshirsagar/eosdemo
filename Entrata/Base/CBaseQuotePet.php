<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuotePet extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intQuoteId;
    protected $m_intPetTypeId;
    protected $m_strBreed;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->m_intQuoteId = trim( $arrValues['quote_id'] ); else if( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
        if( isset( $arrValues['default_pet_type_id'] ) && $boolDirectSet ) $this->m_intPetTypeId = trim( $arrValues['default_pet_type_id'] ); else if( isset( $arrValues['default_pet_type_id'] ) ) $this->setPetTypeId( $arrValues['default_pet_type_id'] );
        if( isset( $arrValues['breed'] ) && $boolDirectSet ) $this->m_strBreed = trim( stripcslashes( $arrValues['breed'] ) ); else if( isset( $arrValues['breed'] ) ) $this->setBreed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['breed'] ) : $arrValues['breed'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setQuoteId( $intQuoteId ) {
        $this->m_intQuoteId = CStrings::strToIntDef( $intQuoteId, NULL, false );
    }

    public function getQuoteId() {
        return $this->m_intQuoteId;
    }

    public function sqlQuoteId() {
        return ( true == isset( $this->m_intQuoteId ) ) ? (string) $this->m_intQuoteId : 'NULL';
    }

    public function setPetTypeId( $intPetTypeId ) {
        $this->m_intPetTypeId = CStrings::strToIntDef( $intPetTypeId, NULL, false );
    }

    public function getPetTypeId() {
        return $this->m_intPetTypeId;
    }

    public function sqlPetTypeId() {
        return ( true == isset( $this->m_intPetTypeId ) ) ? (string) $this->m_intPetTypeId : 'NULL';
    }

    public function setBreed( $strBreed ) {
        $this->m_strBreed = CStrings::strTrimDef( $strBreed, 50, NULL, true );
    }

    public function getBreed() {
        return $this->m_strBreed;
    }

    public function sqlBreed() {
        return ( true == isset( $this->m_strBreed ) ) ? '\'' . addslashes( $this->m_strBreed ) . '\'' : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.quote_pets_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.quote_pets					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlQuoteId() . ', ' . 		                $this->sqlPetTypeId() . ', ' . 		                $this->sqlBreed() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.quote_pets
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlQuoteId() ) != $this->getOriginalValueByFieldName ( 'quote_id' ) ) { $arrstrOriginalValueChanges['quote_id'] = $this->sqlQuoteId(); $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_type_id = ' . $this->sqlPetTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPetTypeId() ) != $this->getOriginalValueByFieldName ( 'default_pet_type_id' ) ) { $arrstrOriginalValueChanges['default_pet_type_id'] = $this->sqlPetTypeId(); $strSql .= ' pet_type_id = ' . $this->sqlPetTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' breed = ' . $this->sqlBreed() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlBreed() ) != $this->getOriginalValueByFieldName ( 'breed' ) ) { $arrstrOriginalValueChanges['breed'] = $this->sqlBreed(); $strSql .= ' breed = ' . $this->sqlBreed() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE						id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.quote_pets WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.quote_pets_id_seq', $objDatabase );
    }

}
?>