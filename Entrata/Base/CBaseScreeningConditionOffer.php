<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningConditionOffer extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_condition_offers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningApplicationRequestId;
	protected $m_intScreeningConditionOfferStatusId;
	protected $m_intOfferDecisionBy;
	protected $m_strOfferDecisionOn;
	protected $m_strIpAddress;
	protected $m_intIsPublished;
	protected $m_strExpiredOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_application_request_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicationRequestId', trim( $arrValues['screening_application_request_id'] ) ); elseif( isset( $arrValues['screening_application_request_id'] ) ) $this->setScreeningApplicationRequestId( $arrValues['screening_application_request_id'] );
		if( isset( $arrValues['screening_condition_offer_status_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConditionOfferStatusId', trim( $arrValues['screening_condition_offer_status_id'] ) ); elseif( isset( $arrValues['screening_condition_offer_status_id'] ) ) $this->setScreeningConditionOfferStatusId( $arrValues['screening_condition_offer_status_id'] );
		if( isset( $arrValues['offer_decision_by'] ) && $boolDirectSet ) $this->set( 'm_intOfferDecisionBy', trim( $arrValues['offer_decision_by'] ) ); elseif( isset( $arrValues['offer_decision_by'] ) ) $this->setOfferDecisionBy( $arrValues['offer_decision_by'] );
		if( isset( $arrValues['offer_decision_on'] ) && $boolDirectSet ) $this->set( 'm_strOfferDecisionOn', trim( $arrValues['offer_decision_on'] ) ); elseif( isset( $arrValues['offer_decision_on'] ) ) $this->setOfferDecisionOn( $arrValues['offer_decision_on'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['expired_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiredOn', trim( $arrValues['expired_on'] ) ); elseif( isset( $arrValues['expired_on'] ) ) $this->setExpiredOn( $arrValues['expired_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningApplicationRequestId( $intScreeningApplicationRequestId ) {
		$this->set( 'm_intScreeningApplicationRequestId', CStrings::strToIntDef( $intScreeningApplicationRequestId, NULL, false ) );
	}

	public function getScreeningApplicationRequestId() {
		return $this->m_intScreeningApplicationRequestId;
	}

	public function sqlScreeningApplicationRequestId() {
		return ( true == isset( $this->m_intScreeningApplicationRequestId ) ) ? ( string ) $this->m_intScreeningApplicationRequestId : 'NULL';
	}

	public function setScreeningConditionOfferStatusId( $intScreeningConditionOfferStatusId ) {
		$this->set( 'm_intScreeningConditionOfferStatusId', CStrings::strToIntDef( $intScreeningConditionOfferStatusId, NULL, false ) );
	}

	public function getScreeningConditionOfferStatusId() {
		return $this->m_intScreeningConditionOfferStatusId;
	}

	public function sqlScreeningConditionOfferStatusId() {
		return ( true == isset( $this->m_intScreeningConditionOfferStatusId ) ) ? ( string ) $this->m_intScreeningConditionOfferStatusId : 'NULL';
	}

	public function setOfferDecisionBy( $intOfferDecisionBy ) {
		$this->set( 'm_intOfferDecisionBy', CStrings::strToIntDef( $intOfferDecisionBy, NULL, false ) );
	}

	public function getOfferDecisionBy() {
		return $this->m_intOfferDecisionBy;
	}

	public function sqlOfferDecisionBy() {
		return ( true == isset( $this->m_intOfferDecisionBy ) ) ? ( string ) $this->m_intOfferDecisionBy : 'NULL';
	}

	public function setOfferDecisionOn( $strOfferDecisionOn ) {
		$this->set( 'm_strOfferDecisionOn', CStrings::strTrimDef( $strOfferDecisionOn, -1, NULL, true ) );
	}

	public function getOfferDecisionOn() {
		return $this->m_strOfferDecisionOn;
	}

	public function sqlOfferDecisionOn() {
		return ( true == isset( $this->m_strOfferDecisionOn ) ) ? '\'' . $this->m_strOfferDecisionOn . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, -1, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setExpiredOn( $strExpiredOn ) {
		$this->set( 'm_strExpiredOn', CStrings::strTrimDef( $strExpiredOn, -1, NULL, true ) );
	}

	public function getExpiredOn() {
		return $this->m_strExpiredOn;
	}

	public function sqlExpiredOn() {
		return ( true == isset( $this->m_strExpiredOn ) ) ? '\'' . $this->m_strExpiredOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_application_request_id, screening_condition_offer_status_id, offer_decision_by, offer_decision_on, ip_address, is_published, expired_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScreeningApplicationRequestId() . ', ' .
 						$this->sqlScreeningConditionOfferStatusId() . ', ' .
 						$this->sqlOfferDecisionBy() . ', ' .
 						$this->sqlOfferDecisionOn() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlExpiredOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_condition_offer_status_id = ' . $this->sqlScreeningConditionOfferStatusId() . ','; } elseif( true == array_key_exists( 'ScreeningConditionOfferStatusId', $this->getChangedColumns() ) ) { $strSql .= ' screening_condition_offer_status_id = ' . $this->sqlScreeningConditionOfferStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_decision_by = ' . $this->sqlOfferDecisionBy() . ','; } elseif( true == array_key_exists( 'OfferDecisionBy', $this->getChangedColumns() ) ) { $strSql .= ' offer_decision_by = ' . $this->sqlOfferDecisionBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_decision_on = ' . $this->sqlOfferDecisionOn() . ','; } elseif( true == array_key_exists( 'OfferDecisionOn', $this->getChangedColumns() ) ) { $strSql .= ' offer_decision_on = ' . $this->sqlOfferDecisionOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expired_on = ' . $this->sqlExpiredOn() . ','; } elseif( true == array_key_exists( 'ExpiredOn', $this->getChangedColumns() ) ) { $strSql .= ' expired_on = ' . $this->sqlExpiredOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_application_request_id' => $this->getScreeningApplicationRequestId(),
			'screening_condition_offer_status_id' => $this->getScreeningConditionOfferStatusId(),
			'offer_decision_by' => $this->getOfferDecisionBy(),
			'offer_decision_on' => $this->getOfferDecisionOn(),
			'ip_address' => $this->getIpAddress(),
			'is_published' => $this->getIsPublished(),
			'expired_on' => $this->getExpiredOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>