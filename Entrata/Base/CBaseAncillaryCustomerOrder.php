<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAncillaryCustomerOrder extends CEosSingularBase {

	const TABLE_NAME = 'public.ancillary_customer_orders';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intAncillaryVendorId;
	protected $m_strToken;
	protected $m_intRemotePrimaryKey;
	protected $m_strConfirmationNumber;
	protected $m_intIsOrderValid;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '0';
		$this->m_intCustomerId = '0';
		$this->m_intLeaseId = '0';
		$this->m_intAncillaryVendorId = '0';
		$this->m_intIsOrderValid = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ancillary_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intAncillaryVendorId', trim( $arrValues['ancillary_vendor_id'] ) ); elseif( isset( $arrValues['ancillary_vendor_id'] ) ) $this->setAncillaryVendorId( $arrValues['ancillary_vendor_id'] );
		if( isset( $arrValues['token'] ) && $boolDirectSet ) $this->set( 'm_strToken', trim( stripcslashes( $arrValues['token'] ) ) ); elseif( isset( $arrValues['token'] ) ) $this->setToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['token'] ) : $arrValues['token'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_intRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['confirmation_number'] ) && $boolDirectSet ) $this->set( 'm_strConfirmationNumber', trim( stripcslashes( $arrValues['confirmation_number'] ) ) ); elseif( isset( $arrValues['confirmation_number'] ) ) $this->setConfirmationNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['confirmation_number'] ) : $arrValues['confirmation_number'] );
		if( isset( $arrValues['is_order_valid'] ) && $boolDirectSet ) $this->set( 'm_intIsOrderValid', trim( $arrValues['is_order_valid'] ) ); elseif( isset( $arrValues['is_order_valid'] ) ) $this->setIsOrderValid( $arrValues['is_order_valid'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '0';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : '0';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : '0';
	}

	public function setAncillaryVendorId( $intAncillaryVendorId ) {
		$this->set( 'm_intAncillaryVendorId', CStrings::strToIntDef( $intAncillaryVendorId, NULL, false ) );
	}

	public function getAncillaryVendorId() {
		return $this->m_intAncillaryVendorId;
	}

	public function sqlAncillaryVendorId() {
		return ( true == isset( $this->m_intAncillaryVendorId ) ) ? ( string ) $this->m_intAncillaryVendorId : '0';
	}

	public function setToken( $strToken ) {
		$this->set( 'm_strToken', CStrings::strTrimDef( $strToken, 50, NULL, true ) );
	}

	public function getToken() {
		return $this->m_strToken;
	}

	public function sqlToken() {
		return ( true == isset( $this->m_strToken ) ) ? '\'' . addslashes( $this->m_strToken ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->set( 'm_intRemotePrimaryKey', CStrings::strToIntDef( $intRemotePrimaryKey, NULL, false ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_intRemotePrimaryKey ) ) ? ( string ) $this->m_intRemotePrimaryKey : 'NULL';
	}

	public function setConfirmationNumber( $strConfirmationNumber ) {
		$this->set( 'm_strConfirmationNumber', CStrings::strTrimDef( $strConfirmationNumber, 50, NULL, true ) );
	}

	public function getConfirmationNumber() {
		return $this->m_strConfirmationNumber;
	}

	public function sqlConfirmationNumber() {
		return ( true == isset( $this->m_strConfirmationNumber ) ) ? '\'' . addslashes( $this->m_strConfirmationNumber ) . '\'' : 'NULL';
	}

	public function setIsOrderValid( $intIsOrderValid ) {
		$this->set( 'm_intIsOrderValid', CStrings::strToIntDef( $intIsOrderValid, NULL, false ) );
	}

	public function getIsOrderValid() {
		return $this->m_intIsOrderValid;
	}

	public function sqlIsOrderValid() {
		return ( true == isset( $this->m_intIsOrderValid ) ) ? ( string ) $this->m_intIsOrderValid : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, lease_id, ancillary_vendor_id, token, remote_primary_key, confirmation_number, is_order_valid, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlAncillaryVendorId() . ', ' .
 						$this->sqlToken() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlConfirmationNumber() . ', ' .
 						$this->sqlIsOrderValid() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ancillary_vendor_id = ' . $this->sqlAncillaryVendorId() . ','; } elseif( true == array_key_exists( 'AncillaryVendorId', $this->getChangedColumns() ) ) { $strSql .= ' ancillary_vendor_id = ' . $this->sqlAncillaryVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token = ' . $this->sqlToken() . ','; } elseif( true == array_key_exists( 'Token', $this->getChangedColumns() ) ) { $strSql .= ' token = ' . $this->sqlToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmation_number = ' . $this->sqlConfirmationNumber() . ','; } elseif( true == array_key_exists( 'ConfirmationNumber', $this->getChangedColumns() ) ) { $strSql .= ' confirmation_number = ' . $this->sqlConfirmationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_order_valid = ' . $this->sqlIsOrderValid() . ','; } elseif( true == array_key_exists( 'IsOrderValid', $this->getChangedColumns() ) ) { $strSql .= ' is_order_valid = ' . $this->sqlIsOrderValid() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'ancillary_vendor_id' => $this->getAncillaryVendorId(),
			'token' => $this->getToken(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'confirmation_number' => $this->getConfirmationNumber(),
			'is_order_valid' => $this->getIsOrderValid(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>