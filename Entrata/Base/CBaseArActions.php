<?php

class CBaseArActions extends CEosPluralBase {

    const TABLE_AR_ACTIONS = 'public.ar_actions';

    public static function fetchArActions( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CArAction', $objDatabase );
    }

    public static function fetchArAction( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CArAction', $objDatabase );
    }

    public static function fetchArActionCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'ar_actions', $objDatabase );
    }

    public static function fetchArActionById( $intId, $objDatabase ) {
        return self::fetchArAction( sprintf( 'SELECT * FROM ar_actions WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>