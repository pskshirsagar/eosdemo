<?php

class CBaseTemplateNavigationOption extends CEosSingularBase {

    protected $m_intId;
    protected $m_intWebsiteTemplateId;
    protected $m_intWebsiteTemplateNavigationId;
    protected $m_intIsDefault;
    protected $m_intIsPublished;
    protected $m_intOrderNum;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intIsDefault = '0';
        $this->m_intIsPublished = '1';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['website_template_id'] ) && $boolDirectSet ) $this->m_intWebsiteTemplateId = trim( $arrValues['website_template_id'] ); else if( isset( $arrValues['website_template_id'] ) ) $this->setWebsiteTemplateId( $arrValues['website_template_id'] );
        if( isset( $arrValues['website_template_navigation_id'] ) && $boolDirectSet ) $this->m_intWebsiteTemplateNavigationId = trim( $arrValues['website_template_navigation_id'] ); else if( isset( $arrValues['website_template_navigation_id'] ) ) $this->setWebsiteTemplateNavigationId( $arrValues['website_template_navigation_id'] );
        if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->m_intIsDefault = trim( $arrValues['is_default'] ); else if( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setWebsiteTemplateId( $intWebsiteTemplateId ) {
        $this->m_intWebsiteTemplateId = CStrings::strToIntDef( $intWebsiteTemplateId, NULL, false );
    }

    public function getWebsiteTemplateId() {
        return $this->m_intWebsiteTemplateId;
    }

    public function sqlWebsiteTemplateId() {
        return ( true == isset( $this->m_intWebsiteTemplateId ) ) ? (string) $this->m_intWebsiteTemplateId : 'NULL';
    }

    public function setWebsiteTemplateNavigationId( $intWebsiteTemplateNavigationId ) {
        $this->m_intWebsiteTemplateNavigationId = CStrings::strToIntDef( $intWebsiteTemplateNavigationId, NULL, false );
    }

    public function getWebsiteTemplateNavigationId() {
        return $this->m_intWebsiteTemplateNavigationId;
    }

    public function sqlWebsiteTemplateNavigationId() {
        return ( true == isset( $this->m_intWebsiteTemplateNavigationId ) ) ? (string) $this->m_intWebsiteTemplateNavigationId : 'NULL';
    }

    public function setIsDefault( $intIsDefault ) {
        $this->m_intIsDefault = CStrings::strToIntDef( $intIsDefault, NULL, false );
    }

    public function getIsDefault() {
        return $this->m_intIsDefault;
    }

    public function sqlIsDefault() {
        return ( true == isset( $this->m_intIsDefault ) ) ? (string) $this->m_intIsDefault : '0';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.template_navigation_options_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.template_navigation_options					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlWebsiteTemplateId() . ', ' . 		                $this->sqlWebsiteTemplateNavigationId() . ', ' . 		                $this->sqlIsDefault() . ', ' . 		                $this->sqlIsPublished() . ', ' . 		                $this->sqlOrderNum() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.template_navigation_options
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlWebsiteTemplateId() ) != $this->getOriginalValueByFieldName ( 'website_template_id' ) ) { $arrstrOriginalValueChanges['website_template_id'] = $this->sqlWebsiteTemplateId(); $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_navigation_id = ' . $this->sqlWebsiteTemplateNavigationId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlWebsiteTemplateNavigationId() ) != $this->getOriginalValueByFieldName ( 'website_template_navigation_id' ) ) { $arrstrOriginalValueChanges['website_template_navigation_id'] = $this->sqlWebsiteTemplateNavigationId(); $strSql .= ' website_template_navigation_id = ' . $this->sqlWebsiteTemplateNavigationId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsDefault() ) != $this->getOriginalValueByFieldName ( 'is_default' ) ) { $arrstrOriginalValueChanges['is_default'] = $this->sqlIsDefault(); $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName ( 'order_num' ) ) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.template_navigation_options WHERE id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.template_navigation_options_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'website_template_id' => $this->getWebsiteTemplateId(),
        	'website_template_navigation_id' => $this->getWebsiteTemplateNavigationId(),
        	'is_default' => $this->getIsDefault(),
        	'is_published' => $this->getIsPublished(),
        	'order_num' => $this->getOrderNum(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>