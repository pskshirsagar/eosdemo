<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileAssociation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.file_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFileId;
	protected $m_intPropertyId;
	protected $m_intDocumentId;
	protected $m_intApplicantId;
	protected $m_intQuoteId;
	protected $m_intApplicationId;
	protected $m_intCustomerId;
	protected $m_intCustomerPetId;
	protected $m_intLeaseId;
	protected $m_intArTransactionId;
	protected $m_intInspectionResponseId;
	protected $m_intMaintenanceRequestId;
	protected $m_intCompanyUserReportHistoryId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intApHeaderId;
	protected $m_intApPaymentId;
	protected $m_intApPaymentBatchId;
	protected $m_intCheckComponentTypeId;
	protected $m_intBankAccountId;
	protected $m_intGlHeaderId;
	protected $m_intGlReconciliationId;
	protected $m_intGlHeaderScheduleId;
	protected $m_intChoreId;
	protected $m_intVpDocumentId;
	protected $m_intCompanyEmployeeId;
	protected $m_intCompanyOwnerId;
	protected $m_intSystemMessageId;
	protected $m_intCampaignId;
	protected $m_intScheduledEmailId;
	protected $m_intScheduledTaskEmailId;
	protected $m_intScheduledChargeId;
	protected $m_intEsaFileId;
	protected $m_intCustomerDataVerificationId;
	protected $m_intBudgetChangeOrderId;
	protected $m_intSubsidyCertificationId;
	protected $m_intSubsidyHapRequestId;
	protected $m_intSubsidySpecialClaimId;
	protected $m_intMilitaryMacPeriodId;
	protected $m_strIpAddress;
	protected $m_strRemotePrimaryKey;
	protected $m_strSignature;
	protected $m_intOrderNum;
	protected $m_intRequireSign;
	protected $m_strSignInitiatedOn;
	protected $m_strFileSignedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intViolationId;
	protected $m_intResidentInsurancePolicyId;
	protected $m_intOwnerId;
	protected $m_intOrganizationId;
	protected $m_intOrganizationContractId;
	protected $m_intSubsidyRequestId;
	protected $m_intCommercialMeasureId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMaintenanceTemplateId;
	protected $m_intLeaseAccommodationId;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intRequireSign = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_pet_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerPetId', trim( $arrValues['customer_pet_id'] ) ); elseif( isset( $arrValues['customer_pet_id'] ) ) $this->setCustomerPetId( $arrValues['customer_pet_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['inspection_response_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionResponseId', trim( $arrValues['inspection_response_id'] ) ); elseif( isset( $arrValues['inspection_response_id'] ) ) $this->setInspectionResponseId( $arrValues['inspection_response_id'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['company_user_report_history_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserReportHistoryId', trim( $arrValues['company_user_report_history_id'] ) ); elseif( isset( $arrValues['company_user_report_history_id'] ) ) $this->setCompanyUserReportHistoryId( $arrValues['company_user_report_history_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['ap_payment_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentBatchId', trim( $arrValues['ap_payment_batch_id'] ) ); elseif( isset( $arrValues['ap_payment_batch_id'] ) ) $this->setApPaymentBatchId( $arrValues['ap_payment_batch_id'] );
		if( isset( $arrValues['check_component_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckComponentTypeId', trim( $arrValues['check_component_type_id'] ) ); elseif( isset( $arrValues['check_component_type_id'] ) ) $this->setCheckComponentTypeId( $arrValues['check_component_type_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderId', trim( $arrValues['gl_header_id'] ) ); elseif( isset( $arrValues['gl_header_id'] ) ) $this->setGlHeaderId( $arrValues['gl_header_id'] );
		if( isset( $arrValues['gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationId', trim( $arrValues['gl_reconciliation_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrValues['gl_reconciliation_id'] );
		if( isset( $arrValues['gl_header_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderScheduleId', trim( $arrValues['gl_header_schedule_id'] ) ); elseif( isset( $arrValues['gl_header_schedule_id'] ) ) $this->setGlHeaderScheduleId( $arrValues['gl_header_schedule_id'] );
		if( isset( $arrValues['chore_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreId', trim( $arrValues['chore_id'] ) ); elseif( isset( $arrValues['chore_id'] ) ) $this->setChoreId( $arrValues['chore_id'] );
		if( isset( $arrValues['vp_document_id'] ) && $boolDirectSet ) $this->set( 'm_intVpDocumentId', trim( $arrValues['vp_document_id'] ) ); elseif( isset( $arrValues['vp_document_id'] ) ) $this->setVpDocumentId( $arrValues['vp_document_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['company_owner_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyOwnerId', trim( $arrValues['company_owner_id'] ) ); elseif( isset( $arrValues['company_owner_id'] ) ) $this->setCompanyOwnerId( $arrValues['company_owner_id'] );
		if( isset( $arrValues['system_message_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageId', trim( $arrValues['system_message_id'] ) ); elseif( isset( $arrValues['system_message_id'] ) ) $this->setSystemMessageId( $arrValues['system_message_id'] );
		if( isset( $arrValues['campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intCampaignId', trim( $arrValues['campaign_id'] ) ); elseif( isset( $arrValues['campaign_id'] ) ) $this->setCampaignId( $arrValues['campaign_id'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailId', trim( $arrValues['scheduled_email_id'] ) ); elseif( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['scheduled_task_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskEmailId', trim( $arrValues['scheduled_task_email_id'] ) ); elseif( isset( $arrValues['scheduled_task_email_id'] ) ) $this->setScheduledTaskEmailId( $arrValues['scheduled_task_email_id'] );
		if( isset( $arrValues['scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeId', trim( $arrValues['scheduled_charge_id'] ) ); elseif( isset( $arrValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrValues['scheduled_charge_id'] );
		if( isset( $arrValues['esa_file_id'] ) && $boolDirectSet ) $this->set( 'm_intEsaFileId', trim( $arrValues['esa_file_id'] ) ); elseif( isset( $arrValues['esa_file_id'] ) ) $this->setEsaFileId( $arrValues['esa_file_id'] );
		if( isset( $arrValues['customer_data_verification_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataVerificationId', trim( $arrValues['customer_data_verification_id'] ) ); elseif( isset( $arrValues['customer_data_verification_id'] ) ) $this->setCustomerDataVerificationId( $arrValues['customer_data_verification_id'] );
		if( isset( $arrValues['budget_change_order_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetChangeOrderId', trim( $arrValues['budget_change_order_id'] ) ); elseif( isset( $arrValues['budget_change_order_id'] ) ) $this->setBudgetChangeOrderId( $arrValues['budget_change_order_id'] );
		if( isset( $arrValues['subsidy_certification_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCertificationId', trim( $arrValues['subsidy_certification_id'] ) ); elseif( isset( $arrValues['subsidy_certification_id'] ) ) $this->setSubsidyCertificationId( $arrValues['subsidy_certification_id'] );
		if( isset( $arrValues['subsidy_hap_request_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyHapRequestId', trim( $arrValues['subsidy_hap_request_id'] ) ); elseif( isset( $arrValues['subsidy_hap_request_id'] ) ) $this->setSubsidyHapRequestId( $arrValues['subsidy_hap_request_id'] );
		if( isset( $arrValues['subsidy_special_claim_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySpecialClaimId', trim( $arrValues['subsidy_special_claim_id'] ) ); elseif( isset( $arrValues['subsidy_special_claim_id'] ) ) $this->setSubsidySpecialClaimId( $arrValues['subsidy_special_claim_id'] );
		if( isset( $arrValues['military_mac_period_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryMacPeriodId', trim( $arrValues['military_mac_period_id'] ) ); elseif( isset( $arrValues['military_mac_period_id'] ) ) $this->setMilitaryMacPeriodId( $arrValues['military_mac_period_id'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( $arrValues['signature'] ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( $arrValues['signature'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['require_sign'] ) && $boolDirectSet ) $this->set( 'm_intRequireSign', trim( $arrValues['require_sign'] ) ); elseif( isset( $arrValues['require_sign'] ) ) $this->setRequireSign( $arrValues['require_sign'] );
		if( isset( $arrValues['sign_initiated_on'] ) && $boolDirectSet ) $this->set( 'm_strSignInitiatedOn', trim( $arrValues['sign_initiated_on'] ) ); elseif( isset( $arrValues['sign_initiated_on'] ) ) $this->setSignInitiatedOn( $arrValues['sign_initiated_on'] );
		if( isset( $arrValues['file_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strFileSignedOn', trim( $arrValues['file_signed_on'] ) ); elseif( isset( $arrValues['file_signed_on'] ) ) $this->setFileSignedOn( $arrValues['file_signed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['violation_id'] ) && $boolDirectSet ) $this->set( 'm_intViolationId', trim( $arrValues['violation_id'] ) ); elseif( isset( $arrValues['violation_id'] ) ) $this->setViolationId( $arrValues['violation_id'] );
		if( isset( $arrValues['resident_insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intResidentInsurancePolicyId', trim( $arrValues['resident_insurance_policy_id'] ) ); elseif( isset( $arrValues['resident_insurance_policy_id'] ) ) $this->setResidentInsurancePolicyId( $arrValues['resident_insurance_policy_id'] );
		if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerId', trim( $arrValues['owner_id'] ) ); elseif( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
		if( isset( $arrValues['organization_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationId', trim( $arrValues['organization_id'] ) ); elseif( isset( $arrValues['organization_id'] ) ) $this->setOrganizationId( $arrValues['organization_id'] );
		if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
		if( isset( $arrValues['subsidy_request_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyRequestId', trim( $arrValues['subsidy_request_id'] ) ); elseif( isset( $arrValues['subsidy_request_id'] ) ) $this->setSubsidyRequestId( $arrValues['subsidy_request_id'] );
		if( isset( $arrValues['commercial_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intCommercialMeasureId', trim( $arrValues['commercial_measure_id'] ) ); elseif( isset( $arrValues['commercial_measure_id'] ) ) $this->setCommercialMeasureId( $arrValues['commercial_measure_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceTemplateId', trim( $arrValues['maintenance_template_id'] ) ); elseif( isset( $arrValues['maintenance_template_id'] ) ) $this->setMaintenanceTemplateId( $arrValues['maintenance_template_id'] );
		if( isset( $arrValues['lease_accommodation_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseAccommodationId', trim( $arrValues['lease_accommodation_id'] ) ); elseif( isset( $arrValues['lease_accommodation_id'] ) ) $this->setLeaseAccommodationId( $arrValues['lease_accommodation_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setQuoteId( $intQuoteId ) {
		$this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function sqlQuoteId() {
		return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerPetId( $intCustomerPetId ) {
		$this->set( 'm_intCustomerPetId', CStrings::strToIntDef( $intCustomerPetId, NULL, false ) );
	}

	public function getCustomerPetId() {
		return $this->m_intCustomerPetId;
	}

	public function sqlCustomerPetId() {
		return ( true == isset( $this->m_intCustomerPetId ) ) ? ( string ) $this->m_intCustomerPetId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setInspectionResponseId( $intInspectionResponseId ) {
		$this->set( 'm_intInspectionResponseId', CStrings::strToIntDef( $intInspectionResponseId, NULL, false ) );
	}

	public function getInspectionResponseId() {
		return $this->m_intInspectionResponseId;
	}

	public function sqlInspectionResponseId() {
		return ( true == isset( $this->m_intInspectionResponseId ) ) ? ( string ) $this->m_intInspectionResponseId : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setCompanyUserReportHistoryId( $intCompanyUserReportHistoryId ) {
		$this->set( 'm_intCompanyUserReportHistoryId', CStrings::strToIntDef( $intCompanyUserReportHistoryId, NULL, false ) );
	}

	public function getCompanyUserReportHistoryId() {
		return $this->m_intCompanyUserReportHistoryId;
	}

	public function sqlCompanyUserReportHistoryId() {
		return ( true == isset( $this->m_intCompanyUserReportHistoryId ) ) ? ( string ) $this->m_intCompanyUserReportHistoryId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setApPaymentBatchId( $intApPaymentBatchId ) {
		$this->set( 'm_intApPaymentBatchId', CStrings::strToIntDef( $intApPaymentBatchId, NULL, false ) );
	}

	public function getApPaymentBatchId() {
		return $this->m_intApPaymentBatchId;
	}

	public function sqlApPaymentBatchId() {
		return ( true == isset( $this->m_intApPaymentBatchId ) ) ? ( string ) $this->m_intApPaymentBatchId : 'NULL';
	}

	public function setCheckComponentTypeId( $intCheckComponentTypeId ) {
		$this->set( 'm_intCheckComponentTypeId', CStrings::strToIntDef( $intCheckComponentTypeId, NULL, false ) );
	}

	public function getCheckComponentTypeId() {
		return $this->m_intCheckComponentTypeId;
	}

	public function sqlCheckComponentTypeId() {
		return ( true == isset( $this->m_intCheckComponentTypeId ) ) ? ( string ) $this->m_intCheckComponentTypeId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setGlHeaderId( $intGlHeaderId ) {
		$this->set( 'm_intGlHeaderId', CStrings::strToIntDef( $intGlHeaderId, NULL, false ) );
	}

	public function getGlHeaderId() {
		return $this->m_intGlHeaderId;
	}

	public function sqlGlHeaderId() {
		return ( true == isset( $this->m_intGlHeaderId ) ) ? ( string ) $this->m_intGlHeaderId : 'NULL';
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->set( 'm_intGlReconciliationId', CStrings::strToIntDef( $intGlReconciliationId, NULL, false ) );
	}

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function sqlGlReconciliationId() {
		return ( true == isset( $this->m_intGlReconciliationId ) ) ? ( string ) $this->m_intGlReconciliationId : 'NULL';
	}

	public function setGlHeaderScheduleId( $intGlHeaderScheduleId ) {
		$this->set( 'm_intGlHeaderScheduleId', CStrings::strToIntDef( $intGlHeaderScheduleId, NULL, false ) );
	}

	public function getGlHeaderScheduleId() {
		return $this->m_intGlHeaderScheduleId;
	}

	public function sqlGlHeaderScheduleId() {
		return ( true == isset( $this->m_intGlHeaderScheduleId ) ) ? ( string ) $this->m_intGlHeaderScheduleId : 'NULL';
	}

	public function setChoreId( $intChoreId ) {
		$this->set( 'm_intChoreId', CStrings::strToIntDef( $intChoreId, NULL, false ) );
	}

	public function getChoreId() {
		return $this->m_intChoreId;
	}

	public function sqlChoreId() {
		return ( true == isset( $this->m_intChoreId ) ) ? ( string ) $this->m_intChoreId : 'NULL';
	}

	public function setVpDocumentId( $intVpDocumentId ) {
		$this->set( 'm_intVpDocumentId', CStrings::strToIntDef( $intVpDocumentId, NULL, false ) );
	}

	public function getVpDocumentId() {
		return $this->m_intVpDocumentId;
	}

	public function sqlVpDocumentId() {
		return ( true == isset( $this->m_intVpDocumentId ) ) ? ( string ) $this->m_intVpDocumentId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCompanyOwnerId( $intCompanyOwnerId ) {
		$this->set( 'm_intCompanyOwnerId', CStrings::strToIntDef( $intCompanyOwnerId, NULL, false ) );
	}

	public function getCompanyOwnerId() {
		return $this->m_intCompanyOwnerId;
	}

	public function sqlCompanyOwnerId() {
		return ( true == isset( $this->m_intCompanyOwnerId ) ) ? ( string ) $this->m_intCompanyOwnerId : 'NULL';
	}

	public function setSystemMessageId( $intSystemMessageId ) {
		$this->set( 'm_intSystemMessageId', CStrings::strToIntDef( $intSystemMessageId, NULL, false ) );
	}

	public function getSystemMessageId() {
		return $this->m_intSystemMessageId;
	}

	public function sqlSystemMessageId() {
		return ( true == isset( $this->m_intSystemMessageId ) ) ? ( string ) $this->m_intSystemMessageId : 'NULL';
	}

	public function setCampaignId( $intCampaignId ) {
		$this->set( 'm_intCampaignId', CStrings::strToIntDef( $intCampaignId, NULL, false ) );
	}

	public function getCampaignId() {
		return $this->m_intCampaignId;
	}

	public function sqlCampaignId() {
		return ( true == isset( $this->m_intCampaignId ) ) ? ( string ) $this->m_intCampaignId : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->set( 'm_intScheduledEmailId', CStrings::strToIntDef( $intScheduledEmailId, NULL, false ) );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setScheduledTaskEmailId( $intScheduledTaskEmailId ) {
		$this->set( 'm_intScheduledTaskEmailId', CStrings::strToIntDef( $intScheduledTaskEmailId, NULL, false ) );
	}

	public function getScheduledTaskEmailId() {
		return $this->m_intScheduledTaskEmailId;
	}

	public function sqlScheduledTaskEmailId() {
		return ( true == isset( $this->m_intScheduledTaskEmailId ) ) ? ( string ) $this->m_intScheduledTaskEmailId : 'NULL';
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->set( 'm_intScheduledChargeId', CStrings::strToIntDef( $intScheduledChargeId, NULL, false ) );
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function sqlScheduledChargeId() {
		return ( true == isset( $this->m_intScheduledChargeId ) ) ? ( string ) $this->m_intScheduledChargeId : 'NULL';
	}

	public function setEsaFileId( $intEsaFileId ) {
		$this->set( 'm_intEsaFileId', CStrings::strToIntDef( $intEsaFileId, NULL, false ) );
	}

	public function getEsaFileId() {
		return $this->m_intEsaFileId;
	}

	public function sqlEsaFileId() {
		return ( true == isset( $this->m_intEsaFileId ) ) ? ( string ) $this->m_intEsaFileId : 'NULL';
	}

	public function setCustomerDataVerificationId( $intCustomerDataVerificationId ) {
		$this->set( 'm_intCustomerDataVerificationId', CStrings::strToIntDef( $intCustomerDataVerificationId, NULL, false ) );
	}

	public function getCustomerDataVerificationId() {
		return $this->m_intCustomerDataVerificationId;
	}

	public function sqlCustomerDataVerificationId() {
		return ( true == isset( $this->m_intCustomerDataVerificationId ) ) ? ( string ) $this->m_intCustomerDataVerificationId : 'NULL';
	}

	public function setBudgetChangeOrderId( $intBudgetChangeOrderId ) {
		$this->set( 'm_intBudgetChangeOrderId', CStrings::strToIntDef( $intBudgetChangeOrderId, NULL, false ) );
	}

	public function getBudgetChangeOrderId() {
		return $this->m_intBudgetChangeOrderId;
	}

	public function sqlBudgetChangeOrderId() {
		return ( true == isset( $this->m_intBudgetChangeOrderId ) ) ? ( string ) $this->m_intBudgetChangeOrderId : 'NULL';
	}

	public function setSubsidyCertificationId( $intSubsidyCertificationId ) {
		$this->set( 'm_intSubsidyCertificationId', CStrings::strToIntDef( $intSubsidyCertificationId, NULL, false ) );
	}

	public function getSubsidyCertificationId() {
		return $this->m_intSubsidyCertificationId;
	}

	public function sqlSubsidyCertificationId() {
		return ( true == isset( $this->m_intSubsidyCertificationId ) ) ? ( string ) $this->m_intSubsidyCertificationId : 'NULL';
	}

	public function setSubsidyHapRequestId( $intSubsidyHapRequestId ) {
		$this->set( 'm_intSubsidyHapRequestId', CStrings::strToIntDef( $intSubsidyHapRequestId, NULL, false ) );
	}

	public function getSubsidyHapRequestId() {
		return $this->m_intSubsidyHapRequestId;
	}

	public function sqlSubsidyHapRequestId() {
		return ( true == isset( $this->m_intSubsidyHapRequestId ) ) ? ( string ) $this->m_intSubsidyHapRequestId : 'NULL';
	}

	public function setSubsidySpecialClaimId( $intSubsidySpecialClaimId ) {
		$this->set( 'm_intSubsidySpecialClaimId', CStrings::strToIntDef( $intSubsidySpecialClaimId, NULL, false ) );
	}

	public function getSubsidySpecialClaimId() {
		return $this->m_intSubsidySpecialClaimId;
	}

	public function sqlSubsidySpecialClaimId() {
		return ( true == isset( $this->m_intSubsidySpecialClaimId ) ) ? ( string ) $this->m_intSubsidySpecialClaimId : 'NULL';
	}

	public function setMilitaryMacPeriodId( $intMilitaryMacPeriodId ) {
		$this->set( 'm_intMilitaryMacPeriodId', CStrings::strToIntDef( $intMilitaryMacPeriodId, NULL, false ) );
	}

	public function getMilitaryMacPeriodId() {
		return $this->m_intMilitaryMacPeriodId;
	}

	public function sqlMilitaryMacPeriodId() {
		return ( true == isset( $this->m_intMilitaryMacPeriodId ) ) ? ( string ) $this->m_intMilitaryMacPeriodId : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 250, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSignature ) : '\'' . addslashes( $this->m_strSignature ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setRequireSign( $intRequireSign ) {
		$this->set( 'm_intRequireSign', CStrings::strToIntDef( $intRequireSign, NULL, false ) );
	}

	public function getRequireSign() {
		return $this->m_intRequireSign;
	}

	public function sqlRequireSign() {
		return ( true == isset( $this->m_intRequireSign ) ) ? ( string ) $this->m_intRequireSign : '0';
	}

	public function setSignInitiatedOn( $strSignInitiatedOn ) {
		$this->set( 'm_strSignInitiatedOn', CStrings::strTrimDef( $strSignInitiatedOn, -1, NULL, true ) );
	}

	public function getSignInitiatedOn() {
		return $this->m_strSignInitiatedOn;
	}

	public function sqlSignInitiatedOn() {
		return ( true == isset( $this->m_strSignInitiatedOn ) ) ? '\'' . $this->m_strSignInitiatedOn . '\'' : 'NULL';
	}

	public function setFileSignedOn( $strFileSignedOn ) {
		$this->set( 'm_strFileSignedOn', CStrings::strTrimDef( $strFileSignedOn, -1, NULL, true ) );
	}

	public function getFileSignedOn() {
		return $this->m_strFileSignedOn;
	}

	public function sqlFileSignedOn() {
		return ( true == isset( $this->m_strFileSignedOn ) ) ? '\'' . $this->m_strFileSignedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setViolationId( $intViolationId ) {
		$this->set( 'm_intViolationId', CStrings::strToIntDef( $intViolationId, NULL, false ) );
	}

	public function getViolationId() {
		return $this->m_intViolationId;
	}

	public function sqlViolationId() {
		return ( true == isset( $this->m_intViolationId ) ) ? ( string ) $this->m_intViolationId : 'NULL';
	}

	public function setResidentInsurancePolicyId( $intResidentInsurancePolicyId ) {
		$this->set( 'm_intResidentInsurancePolicyId', CStrings::strToIntDef( $intResidentInsurancePolicyId, NULL, false ) );
	}

	public function getResidentInsurancePolicyId() {
		return $this->m_intResidentInsurancePolicyId;
	}

	public function sqlResidentInsurancePolicyId() {
		return ( true == isset( $this->m_intResidentInsurancePolicyId ) ) ? ( string ) $this->m_intResidentInsurancePolicyId : 'NULL';
	}

	public function setOwnerId( $intOwnerId ) {
		$this->set( 'm_intOwnerId', CStrings::strToIntDef( $intOwnerId, NULL, false ) );
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	public function sqlOwnerId() {
		return ( true == isset( $this->m_intOwnerId ) ) ? ( string ) $this->m_intOwnerId : 'NULL';
	}

	public function setOrganizationId( $intOrganizationId ) {
		$this->set( 'm_intOrganizationId', CStrings::strToIntDef( $intOrganizationId, NULL, false ) );
	}

	public function getOrganizationId() {
		return $this->m_intOrganizationId;
	}

	public function sqlOrganizationId() {
		return ( true == isset( $this->m_intOrganizationId ) ) ? ( string ) $this->m_intOrganizationId : 'NULL';
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function sqlOrganizationContractId() {
		return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
	}

	public function setSubsidyRequestId( $intSubsidyRequestId ) {
		$this->set( 'm_intSubsidyRequestId', CStrings::strToIntDef( $intSubsidyRequestId, NULL, false ) );
	}

	public function getSubsidyRequestId() {
		return $this->m_intSubsidyRequestId;
	}

	public function sqlSubsidyRequestId() {
		return ( true == isset( $this->m_intSubsidyRequestId ) ) ? ( string ) $this->m_intSubsidyRequestId : 'NULL';
	}

	public function setCommercialMeasureId( $intCommercialMeasureId ) {
		$this->set( 'm_intCommercialMeasureId', CStrings::strToIntDef( $intCommercialMeasureId, NULL, false ) );
	}

	public function getCommercialMeasureId() {
		return $this->m_intCommercialMeasureId;
	}

	public function sqlCommercialMeasureId() {
		return ( true == isset( $this->m_intCommercialMeasureId ) ) ? ( string ) $this->m_intCommercialMeasureId : 'NULL';
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->set( 'm_intMaintenanceTemplateId', CStrings::strToIntDef( $intMaintenanceTemplateId, NULL, false ) );
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function sqlMaintenanceTemplateId() {
		return ( true == isset( $this->m_intMaintenanceTemplateId ) ) ? ( string ) $this->m_intMaintenanceTemplateId : 'NULL';
	}

	public function setLeaseAccommodationId( $intLeaseAccommodationId ) {
		$this->set( 'm_intLeaseAccommodationId', CStrings::strToIntDef( $intLeaseAccommodationId, NULL, false ) );
	}

	public function getLeaseAccommodationId() {
		return $this->m_intLeaseAccommodationId;
	}

	public function sqlLeaseAccommodationId() {
		return ( true == isset( $this->m_intLeaseAccommodationId ) ) ? ( string ) $this->m_intLeaseAccommodationId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, file_id, property_id, document_id, applicant_id, quote_id, application_id, customer_id, customer_pet_id, lease_id, ar_transaction_id, inspection_response_id, maintenance_request_id, company_user_report_history_id, ap_payee_id, ap_payee_location_id, ap_header_id, ap_payment_id, ap_payment_batch_id, check_component_type_id, bank_account_id, gl_header_id, gl_reconciliation_id, gl_header_schedule_id, chore_id, vp_document_id, company_employee_id, company_owner_id, system_message_id, campaign_id, scheduled_email_id, scheduled_task_email_id, scheduled_charge_id, esa_file_id, customer_data_verification_id, budget_change_order_id, subsidy_certification_id, subsidy_hap_request_id, subsidy_special_claim_id, military_mac_period_id, ip_address, remote_primary_key, signature, order_num, require_sign, sign_initiated_on, file_signed_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, violation_id, resident_insurance_policy_id, owner_id, organization_id, organization_contract_id, subsidy_request_id, commercial_measure_id, details, maintenance_template_id, lease_accommodation_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlFileId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlQuoteId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerPetId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlInspectionResponseId() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlCompanyUserReportHistoryId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApHeaderId() . ', ' .
						$this->sqlApPaymentId() . ', ' .
						$this->sqlApPaymentBatchId() . ', ' .
						$this->sqlCheckComponentTypeId() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlGlHeaderId() . ', ' .
						$this->sqlGlReconciliationId() . ', ' .
						$this->sqlGlHeaderScheduleId() . ', ' .
						$this->sqlChoreId() . ', ' .
						$this->sqlVpDocumentId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlCompanyOwnerId() . ', ' .
						$this->sqlSystemMessageId() . ', ' .
						$this->sqlCampaignId() . ', ' .
						$this->sqlScheduledEmailId() . ', ' .
						$this->sqlScheduledTaskEmailId() . ', ' .
						$this->sqlScheduledChargeId() . ', ' .
						$this->sqlEsaFileId() . ', ' .
						$this->sqlCustomerDataVerificationId() . ', ' .
						$this->sqlBudgetChangeOrderId() . ', ' .
						$this->sqlSubsidyCertificationId() . ', ' .
						$this->sqlSubsidyHapRequestId() . ', ' .
						$this->sqlSubsidySpecialClaimId() . ', ' .
						$this->sqlMilitaryMacPeriodId() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSignature() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlRequireSign() . ', ' .
						$this->sqlSignInitiatedOn() . ', ' .
						$this->sqlFileSignedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlViolationId() . ', ' .
						$this->sqlResidentInsurancePolicyId() . ', ' .
						$this->sqlOwnerId() . ', ' .
						$this->sqlOrganizationId() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlSubsidyRequestId() . ', ' .
						$this->sqlCommercialMeasureId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMaintenanceTemplateId() . ', ' .
						$this->sqlLeaseAccommodationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId(). ',' ; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_pet_id = ' . $this->sqlCustomerPetId(). ',' ; } elseif( true == array_key_exists( 'CustomerPetId', $this->getChangedColumns() ) ) { $strSql .= ' customer_pet_id = ' . $this->sqlCustomerPetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_response_id = ' . $this->sqlInspectionResponseId(). ',' ; } elseif( true == array_key_exists( 'InspectionResponseId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_response_id = ' . $this->sqlInspectionResponseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_report_history_id = ' . $this->sqlCompanyUserReportHistoryId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserReportHistoryId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_report_history_id = ' . $this->sqlCompanyUserReportHistoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_batch_id = ' . $this->sqlApPaymentBatchId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_batch_id = ' . $this->sqlApPaymentBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_component_type_id = ' . $this->sqlCheckComponentTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckComponentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_component_type_id = ' . $this->sqlCheckComponentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId(). ',' ; } elseif( true == array_key_exists( 'GlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_id = ' . $this->sqlChoreId(). ',' ; } elseif( true == array_key_exists( 'ChoreId', $this->getChangedColumns() ) ) { $strSql .= ' chore_id = ' . $this->sqlChoreId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vp_document_id = ' . $this->sqlVpDocumentId(). ',' ; } elseif( true == array_key_exists( 'VpDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' vp_document_id = ' . $this->sqlVpDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_owner_id = ' . $this->sqlCompanyOwnerId(). ',' ; } elseif( true == array_key_exists( 'CompanyOwnerId', $this->getChangedColumns() ) ) { $strSql .= ' company_owner_id = ' . $this->sqlCompanyOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_id = ' . $this->sqlSystemMessageId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_id = ' . $this->sqlSystemMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' campaign_id = ' . $this->sqlCampaignId(). ',' ; } elseif( true == array_key_exists( 'CampaignId', $this->getChangedColumns() ) ) { $strSql .= ' campaign_id = ' . $this->sqlCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_email_id = ' . $this->sqlScheduledTaskEmailId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_email_id = ' . $this->sqlScheduledTaskEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' esa_file_id = ' . $this->sqlEsaFileId(). ',' ; } elseif( true == array_key_exists( 'EsaFileId', $this->getChangedColumns() ) ) { $strSql .= ' esa_file_id = ' . $this->sqlEsaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_verification_id = ' . $this->sqlCustomerDataVerificationId(). ',' ; } elseif( true == array_key_exists( 'CustomerDataVerificationId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_verification_id = ' . $this->sqlCustomerDataVerificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_change_order_id = ' . $this->sqlBudgetChangeOrderId(). ',' ; } elseif( true == array_key_exists( 'BudgetChangeOrderId', $this->getChangedColumns() ) ) { $strSql .= ' budget_change_order_id = ' . $this->sqlBudgetChangeOrderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_certification_id = ' . $this->sqlSubsidyCertificationId(). ',' ; } elseif( true == array_key_exists( 'SubsidyCertificationId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_certification_id = ' . $this->sqlSubsidyCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId(). ',' ; } elseif( true == array_key_exists( 'SubsidyHapRequestId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_special_claim_id = ' . $this->sqlSubsidySpecialClaimId(). ',' ; } elseif( true == array_key_exists( 'SubsidySpecialClaimId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_special_claim_id = ' . $this->sqlSubsidySpecialClaimId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_mac_period_id = ' . $this->sqlMilitaryMacPeriodId(). ',' ; } elseif( true == array_key_exists( 'MilitaryMacPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' military_mac_period_id = ' . $this->sqlMilitaryMacPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature(). ',' ; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_sign = ' . $this->sqlRequireSign(). ',' ; } elseif( true == array_key_exists( 'RequireSign', $this->getChangedColumns() ) ) { $strSql .= ' require_sign = ' . $this->sqlRequireSign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sign_initiated_on = ' . $this->sqlSignInitiatedOn(). ',' ; } elseif( true == array_key_exists( 'SignInitiatedOn', $this->getChangedColumns() ) ) { $strSql .= ' sign_initiated_on = ' . $this->sqlSignInitiatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_signed_on = ' . $this->sqlFileSignedOn(). ',' ; } elseif( true == array_key_exists( 'FileSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' file_signed_on = ' . $this->sqlFileSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_id = ' . $this->sqlViolationId(). ',' ; } elseif( true == array_key_exists( 'ViolationId', $this->getChangedColumns() ) ) { $strSql .= ' violation_id = ' . $this->sqlViolationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_insurance_policy_id = ' . $this->sqlResidentInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'ResidentInsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' resident_insurance_policy_id = ' . $this->sqlResidentInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId(). ',' ; } elseif( true == array_key_exists( 'OwnerId', $this->getChangedColumns() ) ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_id = ' . $this->sqlOrganizationId(). ',' ; } elseif( true == array_key_exists( 'OrganizationId', $this->getChangedColumns() ) ) { $strSql .= ' organization_id = ' . $this->sqlOrganizationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_request_id = ' . $this->sqlSubsidyRequestId(). ',' ; } elseif( true == array_key_exists( 'SubsidyRequestId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_request_id = ' . $this->sqlSubsidyRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_measure_id = ' . $this->sqlCommercialMeasureId(). ',' ; } elseif( true == array_key_exists( 'CommercialMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' commercial_measure_id = ' . $this->sqlCommercialMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_accommodation_id = ' . $this->sqlLeaseAccommodationId(). ',' ; } elseif( true == array_key_exists( 'LeaseAccommodationId', $this->getChangedColumns() ) ) { $strSql .= ' lease_accommodation_id = ' . $this->sqlLeaseAccommodationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'file_id' => $this->getFileId(),
			'property_id' => $this->getPropertyId(),
			'document_id' => $this->getDocumentId(),
			'applicant_id' => $this->getApplicantId(),
			'quote_id' => $this->getQuoteId(),
			'application_id' => $this->getApplicationId(),
			'customer_id' => $this->getCustomerId(),
			'customer_pet_id' => $this->getCustomerPetId(),
			'lease_id' => $this->getLeaseId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'inspection_response_id' => $this->getInspectionResponseId(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'company_user_report_history_id' => $this->getCompanyUserReportHistoryId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_header_id' => $this->getApHeaderId(),
			'ap_payment_id' => $this->getApPaymentId(),
			'ap_payment_batch_id' => $this->getApPaymentBatchId(),
			'check_component_type_id' => $this->getCheckComponentTypeId(),
			'bank_account_id' => $this->getBankAccountId(),
			'gl_header_id' => $this->getGlHeaderId(),
			'gl_reconciliation_id' => $this->getGlReconciliationId(),
			'gl_header_schedule_id' => $this->getGlHeaderScheduleId(),
			'chore_id' => $this->getChoreId(),
			'vp_document_id' => $this->getVpDocumentId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'company_owner_id' => $this->getCompanyOwnerId(),
			'system_message_id' => $this->getSystemMessageId(),
			'campaign_id' => $this->getCampaignId(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'scheduled_task_email_id' => $this->getScheduledTaskEmailId(),
			'scheduled_charge_id' => $this->getScheduledChargeId(),
			'esa_file_id' => $this->getEsaFileId(),
			'customer_data_verification_id' => $this->getCustomerDataVerificationId(),
			'budget_change_order_id' => $this->getBudgetChangeOrderId(),
			'subsidy_certification_id' => $this->getSubsidyCertificationId(),
			'subsidy_hap_request_id' => $this->getSubsidyHapRequestId(),
			'subsidy_special_claim_id' => $this->getSubsidySpecialClaimId(),
			'military_mac_period_id' => $this->getMilitaryMacPeriodId(),
			'ip_address' => $this->getIpAddress(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'signature' => $this->getSignature(),
			'order_num' => $this->getOrderNum(),
			'require_sign' => $this->getRequireSign(),
			'sign_initiated_on' => $this->getSignInitiatedOn(),
			'file_signed_on' => $this->getFileSignedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'violation_id' => $this->getViolationId(),
			'resident_insurance_policy_id' => $this->getResidentInsurancePolicyId(),
			'owner_id' => $this->getOwnerId(),
			'organization_id' => $this->getOrganizationId(),
			'organization_contract_id' => $this->getOrganizationContractId(),
			'subsidy_request_id' => $this->getSubsidyRequestId(),
			'commercial_measure_id' => $this->getCommercialMeasureId(),
			'details' => $this->getDetails(),
			'maintenance_template_id' => $this->getMaintenanceTemplateId(),
			'lease_accommodation_id' => $this->getLeaseAccommodationId()
		);
	}

}
?>
