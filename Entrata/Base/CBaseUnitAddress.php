<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitAddress extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.unit_addresses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intAddressTypeId;
	protected $m_intTimeZoneId;
	protected $m_strRemotePrimaryKey;
	protected $m_strMarketingName;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strCounty;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strLongitude;
	protected $m_strLatitude;
	protected $m_boolIsVerified;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVerified = false;
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';
		$this->m_arrstrPostalAddressFields = [
			'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strCounty' => 'dependentLocality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['address_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddressTypeId', trim( $arrValues['address_type_id'] ) ); elseif( isset( $arrValues['address_type_id'] ) ) $this->setAddressTypeId( $arrValues['address_type_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['marketing_name'] ) && $boolDirectSet ) $this->set( 'm_strMarketingName', trim( stripcslashes( $arrValues['marketing_name'] ) ) ); elseif( isset( $arrValues['marketing_name'] ) ) $this->setMarketingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_name'] ) : $arrValues['marketing_name'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['county'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCounty', trim( stripcslashes( $arrValues['county'] ) ) ); elseif( isset( $arrValues['county'] ) ) $this->setCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county'] ) : $arrValues['county'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( stripcslashes( $arrValues['longitude'] ) ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['longitude'] ) : $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( stripcslashes( $arrValues['latitude'] ) ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latitude'] ) : $arrValues['latitude'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerified', trim( stripcslashes( $arrValues['is_verified'] ) ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->set( 'm_intAddressTypeId', CStrings::strToIntDef( $intAddressTypeId, NULL, false ) );
	}

	public function getAddressTypeId() {
		return $this->m_intAddressTypeId;
	}

	public function sqlAddressTypeId() {
		return ( true == isset( $this->m_intAddressTypeId ) ) ? ( string ) $this->m_intAddressTypeId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setMarketingName( $strMarketingName ) {
		$this->set( 'm_strMarketingName', CStrings::strTrimDef( $strMarketingName, 50, NULL, true ) );
	}

	public function getMarketingName() {
		return $this->m_strMarketingName;
	}

	public function sqlMarketingName() {
		return ( true == isset( $this->m_strMarketingName ) ) ? '\'' . addslashes( $this->m_strMarketingName ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setCounty( $strCounty ) {
		$this->setPostalAddressField( 'dependentLocality', $strCounty, $strAddressKey = 'default', 'm_strCounty' );
	}

	public function getCounty() {
		return $this->getPostalAddressField( 'dependentLocality', 'default', 'm_strCounty' );
	}

	public function sqlCounty() {
		return ( true == isset( $this->m_strCounty ) ) ? '\'' . addslashes( $this->m_strCounty ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 25, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? '\'' . addslashes( $this->m_strLongitude ) . '\'' : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 25, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? '\'' . addslashes( $this->m_strLatitude ) . '\'' : 'NULL';
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->set( 'm_boolIsVerified', CStrings::strToBool( $boolIsVerified ) );
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, address_type_id, time_zone_id, remote_primary_key, marketing_name, street_line1, street_line2, street_line3, city, county, state_code, province, postal_code, country_code, longitude, latitude, is_verified, is_published, order_num, imported_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyUnitId() . ', ' .
		          $this->sqlAddressTypeId() . ', ' .
		          $this->sqlTimeZoneId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlMarketingName() . ', ' .
		          $this->sqlStreetLine1() . ', ' .
		          $this->sqlStreetLine2() . ', ' .
		          $this->sqlStreetLine3() . ', ' .
		          $this->sqlCity() . ', ' .
		          $this->sqlCounty() . ', ' .
		          $this->sqlStateCode() . ', ' .
		          $this->sqlProvince() . ', ' .
		          $this->sqlPostalCode() . ', ' .
		          $this->sqlCountryCode() . ', ' .
		          $this->sqlLongitude() . ', ' .
		          $this->sqlLatitude() . ', ' .
		          $this->sqlIsVerified() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlImportedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId(). ',' ; } elseif( true == array_key_exists( 'AddressTypeId', $this->getChangedColumns() ) ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId(). ',' ; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName(). ',' ; } elseif( true == array_key_exists( 'MarketingName', $this->getChangedColumns() ) ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2(). ',' ; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3(). ',' ; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county = ' . $this->sqlCounty(). ',' ; } elseif( true == array_key_exists( 'County', $this->getChangedColumns() ) ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude(). ',' ; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude(). ',' ; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified(). ',' ; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'address_type_id' => $this->getAddressTypeId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'marketing_name' => $this->getMarketingName(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'county' => $this->getCounty(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'is_verified' => $this->getIsVerified(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>