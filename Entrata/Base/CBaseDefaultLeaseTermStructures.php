<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseTermStructures
 * Do not add any new functions to this class.
 */

class CBaseDefaultLeaseTermStructures extends CEosPluralBase {

	/**
	 * @return CDefaultLeaseTermStructure[]
	 */
	public static function fetchDefaultLeaseTermStructures( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultLeaseTermStructure::class, $objDatabase );
	}

	/**
	 * @return CDefaultLeaseTermStructure
	 */
	public static function fetchDefaultLeaseTermStructure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultLeaseTermStructure::class, $objDatabase );
	}

	public static function fetchDefaultLeaseTermStructureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_lease_term_structures', $objDatabase );
	}

	public static function fetchDefaultLeaseTermStructureById( $intId, $objDatabase ) {
		return self::fetchDefaultLeaseTermStructure( sprintf( 'SELECT * FROM default_lease_term_structures WHERE id = %d', $intId ), $objDatabase );
	}

}
?>