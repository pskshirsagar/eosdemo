<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialInsurancePolicy extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.commercial_insurance_policies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intCommercialSuiteId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_strStatusType;
	protected $m_intFileId;
	protected $m_strInsurancePolicyNumber;
	protected $m_strInsuranceProviderName;
	protected $m_fltInsuranceLiabilityLimit;
	protected $m_fltInsuranceDeductible;
	protected $m_strInsurancePolicyEffectiveDate;
	protected $m_strInsurancePolicyEndDate;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intViewedBy;
	protected $m_strViewedOn;
	protected $m_intVerifiedBy;
	protected $m_strVerifiedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intInsurancePolicyTypeId = '1';
		$this->m_strInsurancePolicyNumber = NULL;
		$this->m_strInsuranceProviderName = NULL;
		$this->m_fltInsuranceLiabilityLimit = NULL;
		$this->m_fltInsuranceDeductible = NULL;
		$this->m_strViewedOn = NULL;
		$this->m_strVerifiedOn = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['commercial_suite_id'] ) && $boolDirectSet ) $this->set( 'm_intCommercialSuiteId', trim( $arrValues['commercial_suite_id'] ) ); elseif( isset( $arrValues['commercial_suite_id'] ) ) $this->setCommercialSuiteId( $arrValues['commercial_suite_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['status_type'] ) && $boolDirectSet ) $this->set( 'm_strStatusType', trim( stripcslashes( $arrValues['status_type'] ) ) ); elseif( isset( $arrValues['status_type'] ) ) $this->setStatusType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_type'] ) : $arrValues['status_type'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['insurance_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyNumber', trim( $arrValues['insurance_policy_number'] ) ); elseif( isset( $arrValues['insurance_policy_number'] ) ) $this->setInsurancePolicyNumber( $arrValues['insurance_policy_number'] );
		if( isset( $arrValues['insurance_provider_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuranceProviderName', trim( $arrValues['insurance_provider_name'] ) ); elseif( isset( $arrValues['insurance_provider_name'] ) ) $this->setInsuranceProviderName( $arrValues['insurance_provider_name'] );
		if( isset( $arrValues['insurance_liability_limit'] ) && $boolDirectSet ) $this->set( 'm_fltInsuranceLiabilityLimit', trim( $arrValues['insurance_liability_limit'] ) ); elseif( isset( $arrValues['insurance_liability_limit'] ) ) $this->setInsuranceLiabilityLimit( $arrValues['insurance_liability_limit'] );
		if( isset( $arrValues['insurance_deductible'] ) && $boolDirectSet ) $this->set( 'm_fltInsuranceDeductible', trim( $arrValues['insurance_deductible'] ) ); elseif( isset( $arrValues['insurance_deductible'] ) ) $this->setInsuranceDeductible( $arrValues['insurance_deductible'] );
		if( isset( $arrValues['insurance_policy_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyEffectiveDate', trim( $arrValues['insurance_policy_effective_date'] ) ); elseif( isset( $arrValues['insurance_policy_effective_date'] ) ) $this->setInsurancePolicyEffectiveDate( $arrValues['insurance_policy_effective_date'] );
		if( isset( $arrValues['insurance_policy_end_date'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyEndDate', trim( $arrValues['insurance_policy_end_date'] ) ); elseif( isset( $arrValues['insurance_policy_end_date'] ) ) $this->setInsurancePolicyEndDate( $arrValues['insurance_policy_end_date'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['viewed_by'] ) && $boolDirectSet ) $this->set( 'm_intViewedBy', trim( $arrValues['viewed_by'] ) ); elseif( isset( $arrValues['viewed_by'] ) ) $this->setViewedBy( $arrValues['viewed_by'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['verified_by'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedBy', trim( $arrValues['verified_by'] ) ); elseif( isset( $arrValues['verified_by'] ) ) $this->setVerifiedBy( $arrValues['verified_by'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCommercialSuiteId( $intCommercialSuiteId ) {
		$this->set( 'm_intCommercialSuiteId', CStrings::strToIntDef( $intCommercialSuiteId, NULL, false ) );
	}

	public function getCommercialSuiteId() {
		return $this->m_intCommercialSuiteId;
	}

	public function sqlCommercialSuiteId() {
		return ( true == isset( $this->m_intCommercialSuiteId ) ) ? ( string ) $this->m_intCommercialSuiteId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : '1';
	}

	public function setStatusType( $strStatusType ) {
		$this->set( 'm_strStatusType', CStrings::strTrimDef( $strStatusType, -1, NULL, true ) );
	}

	public function getStatusType() {
		return $this->m_strStatusType;
	}

	public function sqlStatusType() {
		return ( true == isset( $this->m_strStatusType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStatusType ) : '\'' . addslashes( $this->m_strStatusType ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Dec 18 2019.
	 */
	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Dec 18 2019.
	 */
	public function getFileId() {
		return $this->m_intFileId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Dec 18 2019.
	 */
	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setInsurancePolicyNumber( $strInsurancePolicyNumber ) {
		$this->set( 'm_strInsurancePolicyNumber', CStrings::strTrimDef( $strInsurancePolicyNumber, 64, NULL, true ) );
	}

	public function getInsurancePolicyNumber() {
		return $this->m_strInsurancePolicyNumber;
	}

	public function sqlInsurancePolicyNumber() {
		return ( true == isset( $this->m_strInsurancePolicyNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsurancePolicyNumber ) : '\'' . addslashes( $this->m_strInsurancePolicyNumber ) . '\'' ) : '\'NULL\'';
	}

	public function setInsuranceProviderName( $strInsuranceProviderName ) {
		$this->set( 'm_strInsuranceProviderName', CStrings::strTrimDef( $strInsuranceProviderName, 50, NULL, true ) );
	}

	public function getInsuranceProviderName() {
		return $this->m_strInsuranceProviderName;
	}

	public function sqlInsuranceProviderName() {
		return ( true == isset( $this->m_strInsuranceProviderName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsuranceProviderName ) : '\'' . addslashes( $this->m_strInsuranceProviderName ) . '\'' ) : '\'NULL\'';
	}

	public function setInsuranceLiabilityLimit( $fltInsuranceLiabilityLimit ) {
		$this->set( 'm_fltInsuranceLiabilityLimit', CStrings::strToFloatDef( $fltInsuranceLiabilityLimit, NULL, false, 2 ) );
	}

	public function getInsuranceLiabilityLimit() {
		return $this->m_fltInsuranceLiabilityLimit;
	}

	public function sqlInsuranceLiabilityLimit() {
		return ( true == isset( $this->m_fltInsuranceLiabilityLimit ) ) ? ( string ) $this->m_fltInsuranceLiabilityLimit : 'NULL::numeric';
	}

	public function setInsuranceDeductible( $fltInsuranceDeductible ) {
		$this->set( 'm_fltInsuranceDeductible', CStrings::strToFloatDef( $fltInsuranceDeductible, NULL, false, 2 ) );
	}

	public function getInsuranceDeductible() {
		return $this->m_fltInsuranceDeductible;
	}

	public function sqlInsuranceDeductible() {
		return ( true == isset( $this->m_fltInsuranceDeductible ) ) ? ( string ) $this->m_fltInsuranceDeductible : 'NULL::numeric';
	}

	public function setInsurancePolicyEffectiveDate( $strInsurancePolicyEffectiveDate ) {
		$this->set( 'm_strInsurancePolicyEffectiveDate', CStrings::strTrimDef( $strInsurancePolicyEffectiveDate, -1, NULL, true ) );
	}

	public function getInsurancePolicyEffectiveDate() {
		return $this->m_strInsurancePolicyEffectiveDate;
	}

	public function sqlInsurancePolicyEffectiveDate() {
		return ( true == isset( $this->m_strInsurancePolicyEffectiveDate ) ) ? '\'' . $this->m_strInsurancePolicyEffectiveDate . '\'' : 'NOW()';
	}

	public function setInsurancePolicyEndDate( $strInsurancePolicyEndDate ) {
		$this->set( 'm_strInsurancePolicyEndDate', CStrings::strTrimDef( $strInsurancePolicyEndDate, -1, NULL, true ) );
	}

	public function getInsurancePolicyEndDate() {
		return $this->m_strInsurancePolicyEndDate;
	}

	public function sqlInsurancePolicyEndDate() {
		return ( true == isset( $this->m_strInsurancePolicyEndDate ) ) ? '\'' . $this->m_strInsurancePolicyEndDate . '\'' : 'NULL';
	}

	public function setViewedBy( $intViewedBy ) {
		$this->set( 'm_intViewedBy', CStrings::strToIntDef( $intViewedBy, NULL, false ) );
	}

	public function getViewedBy() {
		return $this->m_intViewedBy;
	}

	public function sqlViewedBy() {
		return ( true == isset( $this->m_intViewedBy ) ) ? ( string ) $this->m_intViewedBy : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setVerifiedBy( $intVerifiedBy ) {
		$this->set( 'm_intVerifiedBy', CStrings::strToIntDef( $intVerifiedBy, NULL, false ) );
	}

	public function getVerifiedBy() {
		return $this->m_intVerifiedBy;
	}

	public function sqlVerifiedBy() {
		return ( true == isset( $this->m_intVerifiedBy ) ) ? ( string ) $this->m_intVerifiedBy : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, updated_by, updated_on, created_by, created_on, property_id, lease_id, commercial_suite_id, insurance_policy_type_id, status_type, file_id, insurance_policy_number, insurance_provider_name, insurance_liability_limit, insurance_deductible, insurance_policy_effective_date, insurance_policy_end_date, details, viewed_by, viewed_on, verified_by, verified_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlCommercialSuiteId() . ', ' .
						$this->sqlInsurancePolicyTypeId() . ', ' .
						$this->sqlStatusType() . ', ' .
						$this->sqlFileId() . ', ' .
						$this->sqlInsurancePolicyNumber() . ', ' .
						$this->sqlInsuranceProviderName() . ', ' .
						$this->sqlInsuranceLiabilityLimit() . ', ' .
						$this->sqlInsuranceDeductible() . ', ' .
						$this->sqlInsurancePolicyEffectiveDate() . ', ' .
						$this->sqlInsurancePolicyEndDate() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlViewedBy() . ', ' .
						$this->sqlViewedOn() . ', ' .
						$this->sqlVerifiedBy() . ', ' .
						$this->sqlVerifiedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_suite_id = ' . $this->sqlCommercialSuiteId(). ',' ; } elseif( true == array_key_exists( 'CommercialSuiteId', $this->getChangedColumns() ) ) { $strSql .= ' commercial_suite_id = ' . $this->sqlCommercialSuiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_type = ' . $this->sqlStatusType(). ',' ; } elseif( true == array_key_exists( 'StatusType', $this->getChangedColumns() ) ) { $strSql .= ' status_type = ' . $this->sqlStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_provider_name = ' . $this->sqlInsuranceProviderName(). ',' ; } elseif( true == array_key_exists( 'InsuranceProviderName', $this->getChangedColumns() ) ) { $strSql .= ' insurance_provider_name = ' . $this->sqlInsuranceProviderName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_liability_limit = ' . $this->sqlInsuranceLiabilityLimit(). ',' ; } elseif( true == array_key_exists( 'InsuranceLiabilityLimit', $this->getChangedColumns() ) ) { $strSql .= ' insurance_liability_limit = ' . $this->sqlInsuranceLiabilityLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_deductible = ' . $this->sqlInsuranceDeductible(). ',' ; } elseif( true == array_key_exists( 'InsuranceDeductible', $this->getChangedColumns() ) ) { $strSql .= ' insurance_deductible = ' . $this->sqlInsuranceDeductible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_effective_date = ' . $this->sqlInsurancePolicyEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_effective_date = ' . $this->sqlInsurancePolicyEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_end_date = ' . $this->sqlInsurancePolicyEndDate(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyEndDate', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_end_date = ' . $this->sqlInsurancePolicyEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy(). ',' ; } elseif( true == array_key_exists( 'ViewedBy', $this->getChangedColumns() ) ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn(). ',' ; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy(). ',' ; } elseif( true == array_key_exists( 'VerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'commercial_suite_id' => $this->getCommercialSuiteId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'status_type' => $this->getStatusType(),
			'file_id' => $this->getFileId(),
			'insurance_policy_number' => $this->getInsurancePolicyNumber(),
			'insurance_provider_name' => $this->getInsuranceProviderName(),
			'insurance_liability_limit' => $this->getInsuranceLiabilityLimit(),
			'insurance_deductible' => $this->getInsuranceDeductible(),
			'insurance_policy_effective_date' => $this->getInsurancePolicyEffectiveDate(),
			'insurance_policy_end_date' => $this->getInsurancePolicyEndDate(),
			'details' => $this->getDetails(),
			'viewed_by' => $this->getViewedBy(),
			'viewed_on' => $this->getViewedOn(),
			'verified_by' => $this->getVerifiedBy(),
			'verified_on' => $this->getVerifiedOn()
		);
	}

}
?>