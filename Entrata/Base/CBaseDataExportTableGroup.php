<?php

class CBaseDataExportTableGroup extends CEosSingularBase {

	const TABLE_NAME = 'public.data_export_table_groups';

	protected $m_intId;
	protected $m_intDataExportTableId;
	protected $m_intDataExportGroupId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['data_export_table_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportTableId', trim( $arrValues['data_export_table_id'] ) ); elseif( isset( $arrValues['data_export_table_id'] ) ) $this->setDataExportTableId( $arrValues['data_export_table_id'] );
		if( isset( $arrValues['data_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportGroupId', trim( $arrValues['data_export_group_id'] ) ); elseif( isset( $arrValues['data_export_group_id'] ) ) $this->setDataExportGroupId( $arrValues['data_export_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDataExportTableId( $intDataExportTableId ) {
		$this->set( 'm_intDataExportTableId', CStrings::strToIntDef( $intDataExportTableId, NULL, false ) );
	}

	public function getDataExportTableId() {
		return $this->m_intDataExportTableId;
	}

	public function sqlDataExportTableId() {
		return ( true == isset( $this->m_intDataExportTableId ) ) ? ( string ) $this->m_intDataExportTableId : 'NULL';
	}

	public function setDataExportGroupId( $intDataExportGroupId ) {
		$this->set( 'm_intDataExportGroupId', CStrings::strToIntDef( $intDataExportGroupId, NULL, false ) );
	}

	public function getDataExportGroupId() {
		return $this->m_intDataExportGroupId;
	}

	public function sqlDataExportGroupId() {
		return ( true == isset( $this->m_intDataExportGroupId ) ) ? ( string ) $this->m_intDataExportGroupId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'data_export_table_id' => $this->getDataExportTableId(),
			'data_export_group_id' => $this->getDataExportGroupId()
		);
	}

}
?>