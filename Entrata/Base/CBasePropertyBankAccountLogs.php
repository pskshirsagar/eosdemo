<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBankAccountLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyBankAccountLogs extends CEosPluralBase {

	/**
	 * @return CPropertyBankAccountLog[]
	 */
	public static function fetchPropertyBankAccountLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyBankAccountLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyBankAccountLog
	 */
	public static function fetchPropertyBankAccountLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyBankAccountLog', $objDatabase );
	}

	public static function fetchPropertyBankAccountLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_bank_account_logs', $objDatabase );
	}

	public static function fetchPropertyBankAccountLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccountLog( sprintf( 'SELECT * FROM property_bank_account_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountLogsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccountLogs( sprintf( 'SELECT * FROM property_bank_account_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccountLogs( sprintf( 'SELECT * FROM property_bank_account_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountLogsByBankAccountLogIdByCid( $intBankAccountLogId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccountLogs( sprintf( 'SELECT * FROM property_bank_account_logs WHERE bank_account_log_id = %d AND cid = %d', ( int ) $intBankAccountLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyBankAccountLogsByBankApCodeIdByCid( $intBankApCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyBankAccountLogs( sprintf( 'SELECT * FROM property_bank_account_logs WHERE bank_ap_code_id = %d AND cid = %d', ( int ) $intBankApCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>