<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferReasons
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransferReasons extends CEosPluralBase {

	/**
	 * @return CPropertyTransferReason[]
	 */
	public static function fetchPropertyTransferReasons( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyTransferReason', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyTransferReason
	 */
	public static function fetchPropertyTransferReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyTransferReason', $objDatabase );
	}

	public static function fetchPropertyTransferReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_transfer_reasons', $objDatabase );
	}

	public static function fetchPropertyTransferReasonByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferReason( sprintf( 'SELECT * FROM property_transfer_reasons WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferReasonsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyTransferReasons( sprintf( 'SELECT * FROM property_transfer_reasons WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferReasonsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferReasons( sprintf( 'SELECT * FROM property_transfer_reasons WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferReasonsByCompanyTransferReasonIdByCid( $intCompanyTransferReasonId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferReasons( sprintf( 'SELECT * FROM property_transfer_reasons WHERE company_transfer_reason_id = %d AND cid = %d', ( int ) $intCompanyTransferReasonId, ( int ) $intCid ), $objDatabase );
	}

}
?>