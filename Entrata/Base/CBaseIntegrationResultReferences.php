<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationResultReferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationResultReferences extends CEosPluralBase {

	/**
	 * @return CIntegrationResultReference[]
	 */
	public static function fetchIntegrationResultReferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationResultReference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationResultReference
	 */
	public static function fetchIntegrationResultReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationResultReference', $objDatabase );
	}

	public static function fetchIntegrationResultReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_result_references', $objDatabase );
	}

	public static function fetchIntegrationResultReferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResultReference( sprintf( 'SELECT * FROM integration_result_references WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultReferencesByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationResultReferences( sprintf( 'SELECT * FROM integration_result_references WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultReferencesByIntegrationResultIdByCid( $intIntegrationResultId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResultReferences( sprintf( 'SELECT * FROM integration_result_references WHERE integration_result_id = %d AND cid = %d', ( int ) $intIntegrationResultId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationResultReferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchIntegrationResultReferences( sprintf( 'SELECT * FROM integration_result_references WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>