<?php

class CBaseContractStatuses extends CEosPluralBase {

    public static function fetchContractStatuses( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CContractStatus', $objDatabase );
    }

    public static function fetchContractStatus( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CContractStatus', $objDatabase );
    }

    public static function fetchContractStatusCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'contract_statuses', $objDatabase );
    }

    public static function fetchContractStatusById( $intId, $objDatabase ) {
        return self::fetchContractStatus( sprintf( 'SELECT * FROM contract_statuses WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>