<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStages
 * Do not add any new functions to this class.
 */

class CBaseApplicationStages extends CEosPluralBase {

	/**
	 * @return CApplicationStage[]
	 */
	public static function fetchApplicationStages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationStage::class, $objDatabase );
	}

	/**
	 * @return CApplicationStage
	 */
	public static function fetchApplicationStage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationStage::class, $objDatabase );
	}

	public static function fetchApplicationStageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_stages', $objDatabase );
	}

	public static function fetchApplicationStageById( $intId, $objDatabase ) {
		return self::fetchApplicationStage( sprintf( 'SELECT * FROM application_stages WHERE id = %d', $intId ), $objDatabase );
	}

}
?>