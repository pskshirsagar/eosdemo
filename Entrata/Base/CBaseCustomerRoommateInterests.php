<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerRoommateInterests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerRoommateInterests extends CEosPluralBase {

	/**
	 * @return CCustomerRoommateInterest[]
	 */
	public static function fetchCustomerRoommateInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerRoommateInterest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerRoommateInterest
	 */
	public static function fetchCustomerRoommateInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerRoommateInterest::class, $objDatabase );
	}

	public static function fetchCustomerRoommateInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_roommate_interests', $objDatabase );
	}

	public static function fetchCustomerRoommateInterestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterest( sprintf( 'SELECT * FROM customer_roommate_interests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterests( sprintf( 'SELECT * FROM customer_roommate_interests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterests( sprintf( 'SELECT * FROM customer_roommate_interests WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterests( sprintf( 'SELECT * FROM customer_roommate_interests WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterests( sprintf( 'SELECT * FROM customer_roommate_interests WHERE roommate_interest_id = %d AND cid = %d', ( int ) $intRoommateInterestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerRoommateInterestsByRoommateInterestOptionIdByCid( $intRoommateInterestOptionId, $intCid, $objDatabase ) {
		return self::fetchCustomerRoommateInterests( sprintf( 'SELECT * FROM customer_roommate_interests WHERE roommate_interest_option_id = %d AND cid = %d', ( int ) $intRoommateInterestOptionId, ( int ) $intCid ), $objDatabase );
	}

}
?>