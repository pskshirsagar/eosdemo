<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileEventTypes
 * Do not add any new functions to this class.
 */

class CBaseFileEventTypes extends CEosPluralBase {

	/**
	 * @return CFileEventType[]
	 */
	public static function fetchFileEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFileEventType::class, $objDatabase );
	}

	/**
	 * @return CFileEventType
	 */
	public static function fetchFileEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileEventType::class, $objDatabase );
	}

	public static function fetchFileEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_event_types', $objDatabase );
	}

	public static function fetchFileEventTypeById( $intId, $objDatabase ) {
		return self::fetchFileEventType( sprintf( 'SELECT * FROM file_event_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>