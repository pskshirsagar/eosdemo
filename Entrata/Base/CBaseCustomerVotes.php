<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerVotes extends CEosPluralBase {

	/**
	 * @return CCustomerVote[]
	 */
	public static function fetchCustomerVotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerVote', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerVote
	 */
	public static function fetchCustomerVote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerVote', $objDatabase );
	}

	public static function fetchCustomerVoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_votes', $objDatabase );
	}

	public static function fetchCustomerVoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerVote( sprintf( 'SELECT * FROM customer_votes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerVotesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerVotes( sprintf( 'SELECT * FROM customer_votes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerVotesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerVotes( sprintf( 'SELECT * FROM customer_votes WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerVotesByCustomerVoteTypeIdByCid( $intCustomerVoteTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerVotes( sprintf( 'SELECT * FROM customer_votes WHERE customer_vote_type_id = %d AND cid = %d', ( int ) $intCustomerVoteTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerVotesByVoteReferenceIdByCid( $intVoteReferenceId, $intCid, $objDatabase ) {
		return self::fetchCustomerVotes( sprintf( 'SELECT * FROM customer_votes WHERE vote_reference_id = %d AND cid = %d', ( int ) $intVoteReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerVotesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerVotes( sprintf( 'SELECT * FROM customer_votes WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>