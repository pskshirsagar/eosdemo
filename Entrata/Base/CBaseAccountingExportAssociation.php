<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.accounting_export_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountingExportBatchId;
	protected $m_intGlDetailId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intArTransactionId;
	protected $m_intLeaseCustomerId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['accounting_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportBatchId', trim( $arrValues['accounting_export_batch_id'] ) ); elseif( isset( $arrValues['accounting_export_batch_id'] ) ) $this->setAccountingExportBatchId( $arrValues['accounting_export_batch_id'] );
		if( isset( $arrValues['gl_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDetailId', trim( $arrValues['gl_detail_id'] ) ); elseif( isset( $arrValues['gl_detail_id'] ) ) $this->setGlDetailId( $arrValues['gl_detail_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['lease_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCustomerId', trim( $arrValues['lease_customer_id'] ) ); elseif( isset( $arrValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrValues['lease_customer_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountingExportBatchId( $intAccountingExportBatchId ) {
		$this->set( 'm_intAccountingExportBatchId', CStrings::strToIntDef( $intAccountingExportBatchId, NULL, false ) );
	}

	public function getAccountingExportBatchId() {
		return $this->m_intAccountingExportBatchId;
	}

	public function sqlAccountingExportBatchId() {
		return ( true == isset( $this->m_intAccountingExportBatchId ) ) ? ( string ) $this->m_intAccountingExportBatchId : 'NULL';
	}

	public function setGlDetailId( $intGlDetailId ) {
		$this->set( 'm_intGlDetailId', CStrings::strToIntDef( $intGlDetailId, NULL, false ) );
	}

	public function getGlDetailId() {
		return $this->m_intGlDetailId;
	}

	public function sqlGlDetailId() {
		return ( true == isset( $this->m_intGlDetailId ) ) ? ( string ) $this->m_intGlDetailId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->set( 'm_intLeaseCustomerId', CStrings::strToIntDef( $intLeaseCustomerId, NULL, false ) );
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function sqlLeaseCustomerId() {
		return ( true == isset( $this->m_intLeaseCustomerId ) ) ? ( string ) $this->m_intLeaseCustomerId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, accounting_export_batch_id, gl_detail_id, created_by, created_on, ar_transaction_id, lease_customer_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountingExportBatchId() . ', ' .
						$this->sqlGlDetailId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlLeaseCustomerId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId(). ',' ; } elseif( true == array_key_exists( 'AccountingExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_detail_id = ' . $this->sqlGlDetailId(). ',' ; } elseif( true == array_key_exists( 'GlDetailId', $this->getChangedColumns() ) ) { $strSql .= ' gl_detail_id = ' . $this->sqlGlDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() ; } elseif( true == array_key_exists( 'LeaseCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' lease_customer_id = ' . $this->sqlLeaseCustomerId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'accounting_export_batch_id' => $this->getAccountingExportBatchId(),
			'gl_detail_id' => $this->getGlDetailId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'lease_customer_id' => $this->getLeaseCustomerId()
		);
	}

}
?>