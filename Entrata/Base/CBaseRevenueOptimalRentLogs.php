<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOptimalRentLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueOptimalRentLogs extends CEosPluralBase {

	/**
	 * @return CRevenueOptimalRentLog[]
	 */
	public static function fetchRevenueOptimalRentLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenueOptimalRentLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueOptimalRentLog
	 */
	public static function fetchRevenueOptimalRentLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueOptimalRentLog::class, $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_optimal_rent_logs', $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLog( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByRevenueOptimalRentIdByCid( $intRevenueOptimalRentId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE revenue_optimal_rent_id = %d AND cid = %d', ( int ) $intRevenueOptimalRentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE reporting_period_id = %d AND cid = %d', ( int ) $intReportingPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE effective_period_id = %d AND cid = %d', ( int ) $intEffectivePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE original_period_id = %d AND cid = %d', ( int ) $intOriginalPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentLogsByPriorRevenueOptimalRentLogIdByCid( $intPriorRevenueOptimalRentLogId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRentLogs( sprintf( 'SELECT * FROM revenue_optimal_rent_logs WHERE prior_revenue_optimal_rent_log_id = %d AND cid = %d', ( int ) $intPriorRevenueOptimalRentLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>