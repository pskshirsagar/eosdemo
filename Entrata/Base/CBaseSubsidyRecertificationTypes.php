<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRecertificationTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyRecertificationTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyRecertificationType[]
	 */
	public static function fetchSubsidyRecertificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, \CSubsidyRecertificationType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyRecertificationType
	 */
	public static function fetchSubsidyRecertificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, \CSubsidyRecertificationType::class, $objDatabase );
	}

	public static function fetchSubsidyRecertificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_recertification_types', $objDatabase );
	}

	public static function fetchSubsidyRecertificationTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyRecertificationType( sprintf( 'SELECT * FROM subsidy_recertification_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyRecertificationTypesBySubsidyTypeId( $intSubsidyTypeId, $objDatabase ) {
		return self::fetchSubsidyRecertificationTypes( sprintf( 'SELECT * FROM subsidy_recertification_types WHERE subsidy_type_id = %d', $intSubsidyTypeId ), $objDatabase );
	}

}
?>