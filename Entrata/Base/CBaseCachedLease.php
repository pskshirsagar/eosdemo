<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedLease extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_leases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intReimbursingPropertyId;
	protected $m_intOwnerId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intOccupancyTypeId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_intPrimaryCustomerId;
	protected $m_intTransferLeaseId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intTerminationListTypeId;
	protected $m_intActiveLeaseIntervalId;
	protected $m_intM2mLeaseIntervalId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intLastDelinquencyNoteEventId;
	protected $m_intSubsidyContractId;
	protected $m_intSubsidyContractTypeId;
	protected $m_intSetAsideId;
	protected $m_strRemotePrimaryKey;
	protected $m_strLeaseStatusType;
	protected $m_strLeaseIntervalType;
	protected $m_strCompanyName;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strPrimaryPhoneNumber;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveInDate;
	protected $m_strNoticeDate;
	protected $m_strMoveOutDate;
	protected $m_strCollectionsStartDate;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_fltResidentBalance;
	protected $m_fltFinancialBalance;
	protected $m_fltTotalBalance;
	protected $m_fltRepaymentBalance;
	protected $m_fltDepositHeld;
	protected $m_strMoveInReviewCompletedOn;
	protected $m_strMoveOutReviewCompletedOn;
	protected $m_strTransferredOn;
	protected $m_strFmoStartedOn;
	protected $m_strFmoApprovedOn;
	protected $m_strFmoProcessedOn;
	protected $m_strRenewalPromptMutedOn;
	protected $m_strRenewalPromptMutedUntil;
	protected $m_boolHasRepaymentAgreement;
	protected $m_intIsMonthToMonth;
	protected $m_intIsTransferringIn;
	protected $m_intIsMultiSlot;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strLeaseSubStatus;
	protected $m_intOrganizationContractId;
	protected $m_strScheduledMoveInStartTime;
	protected $m_strScheduledMoveInEndTime;
	protected $m_strNameFull;
	protected $m_strMoveInBalance;
	protected $m_intTransferFromLeaseId;
	protected $m_boolSelectedDepositAlternative;

	public function __construct() {
		parent::__construct();

		$this->m_fltResidentBalance = '0';
		$this->m_fltFinancialBalance = '0';
		$this->m_fltTotalBalance = '0';
		$this->m_fltRepaymentBalance = '0';
		$this->m_fltDepositHeld = '0';
		$this->m_boolHasRepaymentAgreement = false;
		$this->m_intIsMonthToMonth = '0';
		$this->m_intIsTransferringIn = '0';
		$this->m_intIsMultiSlot = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['reimbursing_property_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursingPropertyId', trim( $arrValues['reimbursing_property_id'] ) ); elseif( isset( $arrValues['reimbursing_property_id'] ) ) $this->setReimbursingPropertyId( $arrValues['reimbursing_property_id'] );
		if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerId', trim( $arrValues['owner_id'] ) ); elseif( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['payment_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAllowanceTypeId', trim( $arrValues['payment_allowance_type_id'] ) ); elseif( isset( $arrValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrValues['payment_allowance_type_id'] );
		if( isset( $arrValues['primary_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerId', trim( $arrValues['primary_customer_id'] ) ); elseif( isset( $arrValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrValues['primary_customer_id'] );
		if( isset( $arrValues['transfer_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferLeaseId', trim( $arrValues['transfer_lease_id'] ) ); elseif( isset( $arrValues['transfer_lease_id'] ) ) $this->setTransferLeaseId( $arrValues['transfer_lease_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['termination_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationListTypeId', trim( $arrValues['termination_list_type_id'] ) ); elseif( isset( $arrValues['termination_list_type_id'] ) ) $this->setTerminationListTypeId( $arrValues['termination_list_type_id'] );
		if( isset( $arrValues['active_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intActiveLeaseIntervalId', trim( $arrValues['active_lease_interval_id'] ) ); elseif( isset( $arrValues['active_lease_interval_id'] ) ) $this->setActiveLeaseIntervalId( $arrValues['active_lease_interval_id'] );
		if( isset( $arrValues['m2m_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intM2mLeaseIntervalId', trim( $arrValues['m2m_lease_interval_id'] ) ); elseif( isset( $arrValues['m2m_lease_interval_id'] ) ) $this->setM2mLeaseIntervalId( $arrValues['m2m_lease_interval_id'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['last_delinquency_note_event_id'] ) && $boolDirectSet ) $this->set( 'm_intLastDelinquencyNoteEventId', trim( $arrValues['last_delinquency_note_event_id'] ) ); elseif( isset( $arrValues['last_delinquency_note_event_id'] ) ) $this->setLastDelinquencyNoteEventId( $arrValues['last_delinquency_note_event_id'] );
		if( isset( $arrValues['subsidy_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractId', trim( $arrValues['subsidy_contract_id'] ) ); elseif( isset( $arrValues['subsidy_contract_id'] ) ) $this->setSubsidyContractId( $arrValues['subsidy_contract_id'] );
		if( isset( $arrValues['subsidy_contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyContractTypeId', trim( $arrValues['subsidy_contract_type_id'] ) ); elseif( isset( $arrValues['subsidy_contract_type_id'] ) ) $this->setSubsidyContractTypeId( $arrValues['subsidy_contract_type_id'] );
		if( isset( $arrValues['set_aside_id'] ) && $boolDirectSet ) $this->set( 'm_intSetAsideId', trim( $arrValues['set_aside_id'] ) ); elseif( isset( $arrValues['set_aside_id'] ) ) $this->setSetAsideId( $arrValues['set_aside_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['lease_status_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStatusType', trim( $arrValues['lease_status_type'] ) ); elseif( isset( $arrValues['lease_status_type'] ) ) $this->setLeaseStatusType( $arrValues['lease_status_type'] );
		if( isset( $arrValues['lease_interval_type'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIntervalType', trim( $arrValues['lease_interval_type'] ) ); elseif( isset( $arrValues['lease_interval_type'] ) ) $this->setLeaseIntervalType( $arrValues['lease_interval_type'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['primary_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryPhoneNumber', trim( $arrValues['primary_phone_number'] ) ); elseif( isset( $arrValues['primary_phone_number'] ) ) $this->setPrimaryPhoneNumber( $arrValues['primary_phone_number'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['notice_date'] ) && $boolDirectSet ) $this->set( 'm_strNoticeDate', trim( $arrValues['notice_date'] ) ); elseif( isset( $arrValues['notice_date'] ) ) $this->setNoticeDate( $arrValues['notice_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['collections_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsStartDate', trim( $arrValues['collections_start_date'] ) ); elseif( isset( $arrValues['collections_start_date'] ) ) $this->setCollectionsStartDate( $arrValues['collections_start_date'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( $arrValues['unit_number_cache'] ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['resident_balance'] ) && $boolDirectSet ) $this->set( 'm_fltResidentBalance', trim( $arrValues['resident_balance'] ) ); elseif( isset( $arrValues['resident_balance'] ) ) $this->setResidentBalance( $arrValues['resident_balance'] );
		if( isset( $arrValues['financial_balance'] ) && $boolDirectSet ) $this->set( 'm_fltFinancialBalance', trim( $arrValues['financial_balance'] ) ); elseif( isset( $arrValues['financial_balance'] ) ) $this->setFinancialBalance( $arrValues['financial_balance'] );
		if( isset( $arrValues['total_balance'] ) && $boolDirectSet ) $this->set( 'm_fltTotalBalance', trim( $arrValues['total_balance'] ) ); elseif( isset( $arrValues['total_balance'] ) ) $this->setTotalBalance( $arrValues['total_balance'] );
		if( isset( $arrValues['repayment_balance'] ) && $boolDirectSet ) $this->set( 'm_fltRepaymentBalance', trim( $arrValues['repayment_balance'] ) ); elseif( isset( $arrValues['repayment_balance'] ) ) $this->setRepaymentBalance( $arrValues['repayment_balance'] );
		if( isset( $arrValues['deposit_held'] ) && $boolDirectSet ) $this->set( 'm_fltDepositHeld', trim( $arrValues['deposit_held'] ) ); elseif( isset( $arrValues['deposit_held'] ) ) $this->setDepositHeld( $arrValues['deposit_held'] );
		if( isset( $arrValues['move_in_review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strMoveInReviewCompletedOn', trim( $arrValues['move_in_review_completed_on'] ) ); elseif( isset( $arrValues['move_in_review_completed_on'] ) ) $this->setMoveInReviewCompletedOn( $arrValues['move_in_review_completed_on'] );
		if( isset( $arrValues['move_out_review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutReviewCompletedOn', trim( $arrValues['move_out_review_completed_on'] ) ); elseif( isset( $arrValues['move_out_review_completed_on'] ) ) $this->setMoveOutReviewCompletedOn( $arrValues['move_out_review_completed_on'] );
		if( isset( $arrValues['transferred_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredOn', trim( $arrValues['transferred_on'] ) ); elseif( isset( $arrValues['transferred_on'] ) ) $this->setTransferredOn( $arrValues['transferred_on'] );
		if( isset( $arrValues['fmo_started_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoStartedOn', trim( $arrValues['fmo_started_on'] ) ); elseif( isset( $arrValues['fmo_started_on'] ) ) $this->setFmoStartedOn( $arrValues['fmo_started_on'] );
		if( isset( $arrValues['fmo_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoApprovedOn', trim( $arrValues['fmo_approved_on'] ) ); elseif( isset( $arrValues['fmo_approved_on'] ) ) $this->setFmoApprovedOn( $arrValues['fmo_approved_on'] );
		if( isset( $arrValues['fmo_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoProcessedOn', trim( $arrValues['fmo_processed_on'] ) ); elseif( isset( $arrValues['fmo_processed_on'] ) ) $this->setFmoProcessedOn( $arrValues['fmo_processed_on'] );
		if( isset( $arrValues['renewal_prompt_muted_on'] ) && $boolDirectSet ) $this->set( 'm_strRenewalPromptMutedOn', trim( $arrValues['renewal_prompt_muted_on'] ) ); elseif( isset( $arrValues['renewal_prompt_muted_on'] ) ) $this->setRenewalPromptMutedOn( $arrValues['renewal_prompt_muted_on'] );
		if( isset( $arrValues['renewal_prompt_muted_until'] ) && $boolDirectSet ) $this->set( 'm_strRenewalPromptMutedUntil', trim( $arrValues['renewal_prompt_muted_until'] ) ); elseif( isset( $arrValues['renewal_prompt_muted_until'] ) ) $this->setRenewalPromptMutedUntil( $arrValues['renewal_prompt_muted_until'] );
		if( isset( $arrValues['has_repayment_agreement'] ) && $boolDirectSet ) $this->set( 'm_boolHasRepaymentAgreement', trim( stripcslashes( $arrValues['has_repayment_agreement'] ) ) ); elseif( isset( $arrValues['has_repayment_agreement'] ) ) $this->setHasRepaymentAgreement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_repayment_agreement'] ) : $arrValues['has_repayment_agreement'] );
		if( isset( $arrValues['is_month_to_month'] ) && $boolDirectSet ) $this->set( 'm_intIsMonthToMonth', trim( $arrValues['is_month_to_month'] ) ); elseif( isset( $arrValues['is_month_to_month'] ) ) $this->setIsMonthToMonth( $arrValues['is_month_to_month'] );
		if( isset( $arrValues['is_transferring_in'] ) && $boolDirectSet ) $this->set( 'm_intIsTransferringIn', trim( $arrValues['is_transferring_in'] ) ); elseif( isset( $arrValues['is_transferring_in'] ) ) $this->setIsTransferringIn( $arrValues['is_transferring_in'] );
		if( isset( $arrValues['is_multi_slot'] ) && $boolDirectSet ) $this->set( 'm_intIsMultiSlot', trim( $arrValues['is_multi_slot'] ) ); elseif( isset( $arrValues['is_multi_slot'] ) ) $this->setIsMultiSlot( $arrValues['is_multi_slot'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['lease_sub_status'] ) && $boolDirectSet ) $this->set( 'm_strLeaseSubStatus', trim( $arrValues['lease_sub_status'] ) ); elseif( isset( $arrValues['lease_sub_status'] ) ) $this->setLeaseSubStatus( $arrValues['lease_sub_status'] );
		if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
		if( isset( $arrValues['scheduled_move_in_start_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInStartTime', trim( $arrValues['scheduled_move_in_start_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_start_time'] ) ) $this->setScheduledMoveInStartTime( $arrValues['scheduled_move_in_start_time'] );
		if( isset( $arrValues['scheduled_move_in_end_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMoveInEndTime', trim( $arrValues['scheduled_move_in_end_time'] ) ); elseif( isset( $arrValues['scheduled_move_in_end_time'] ) ) $this->setScheduledMoveInEndTime( $arrValues['scheduled_move_in_end_time'] );
		if( isset( $arrValues['name_full'] ) && $boolDirectSet ) $this->set( 'm_strNameFull', trim( $arrValues['name_full'] ) ); elseif( isset( $arrValues['name_full'] ) ) $this->setNameFull( $arrValues['name_full'] );
		if( isset( $arrValues['move_in_balance'] ) && $boolDirectSet ) $this->set( 'm_strMoveInBalance', trim( stripcslashes( $arrValues['move_in_balance'] ) ) ); elseif( isset( $arrValues['move_in_balance'] ) ) $this->setMoveInBalance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['move_in_balance'] ) : $arrValues['move_in_balance'] );
		if( isset( $arrValues['transfer_from_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferFromLeaseId', trim( $arrValues['transfer_from_lease_id'] ) ); elseif( isset( $arrValues['transfer_from_lease_id'] ) ) $this->setTransferFromLeaseId( $arrValues['transfer_from_lease_id'] );
		if( isset( $arrValues['selected_deposit_alternative'] ) && $boolDirectSet ) $this->set( 'm_boolSelectedDepositAlternative', trim( stripcslashes( $arrValues['selected_deposit_alternative'] ) ) ); elseif( isset( $arrValues['selected_deposit_alternative'] ) ) $this->setSelectedDepositAlternative( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['selected_deposit_alternative'] ) : $arrValues['selected_deposit_alternative'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setReimbursingPropertyId( $intReimbursingPropertyId ) {
		$this->set( 'm_intReimbursingPropertyId', CStrings::strToIntDef( $intReimbursingPropertyId, NULL, false ) );
	}

	public function getReimbursingPropertyId() {
		return $this->m_intReimbursingPropertyId;
	}

	public function sqlReimbursingPropertyId() {
		return ( true == isset( $this->m_intReimbursingPropertyId ) ) ? ( string ) $this->m_intReimbursingPropertyId : 'NULL';
	}

	public function setOwnerId( $intOwnerId ) {
		$this->set( 'm_intOwnerId', CStrings::strToIntDef( $intOwnerId, NULL, false ) );
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	public function sqlOwnerId() {
		return ( true == isset( $this->m_intOwnerId ) ) ? ( string ) $this->m_intOwnerId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
		$this->set( 'm_intPaymentAllowanceTypeId', CStrings::strToIntDef( $intPaymentAllowanceTypeId, NULL, false ) );
	}

	public function getPaymentAllowanceTypeId() {
		return $this->m_intPaymentAllowanceTypeId;
	}

	public function sqlPaymentAllowanceTypeId() {
		return ( true == isset( $this->m_intPaymentAllowanceTypeId ) ) ? ( string ) $this->m_intPaymentAllowanceTypeId : 'NULL';
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->set( 'm_intPrimaryCustomerId', CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false ) );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function sqlPrimaryCustomerId() {
		return ( true == isset( $this->m_intPrimaryCustomerId ) ) ? ( string ) $this->m_intPrimaryCustomerId : 'NULL';
	}

	public function setTransferLeaseId( $intTransferLeaseId ) {
		$this->set( 'm_intTransferLeaseId', CStrings::strToIntDef( $intTransferLeaseId, NULL, false ) );
	}

	public function getTransferLeaseId() {
		return $this->m_intTransferLeaseId;
	}

	public function sqlTransferLeaseId() {
		return ( true == isset( $this->m_intTransferLeaseId ) ) ? ( string ) $this->m_intTransferLeaseId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setTerminationListTypeId( $intTerminationListTypeId ) {
		$this->set( 'm_intTerminationListTypeId', CStrings::strToIntDef( $intTerminationListTypeId, NULL, false ) );
	}

	public function getTerminationListTypeId() {
		return $this->m_intTerminationListTypeId;
	}

	public function sqlTerminationListTypeId() {
		return ( true == isset( $this->m_intTerminationListTypeId ) ) ? ( string ) $this->m_intTerminationListTypeId : 'NULL';
	}

	public function setActiveLeaseIntervalId( $intActiveLeaseIntervalId ) {
		$this->set( 'm_intActiveLeaseIntervalId', CStrings::strToIntDef( $intActiveLeaseIntervalId, NULL, false ) );
	}

	public function getActiveLeaseIntervalId() {
		return $this->m_intActiveLeaseIntervalId;
	}

	public function sqlActiveLeaseIntervalId() {
		return ( true == isset( $this->m_intActiveLeaseIntervalId ) ) ? ( string ) $this->m_intActiveLeaseIntervalId : 'NULL';
	}

	public function setM2mLeaseIntervalId( $intM2mLeaseIntervalId ) {
		$this->set( 'm_intM2mLeaseIntervalId', CStrings::strToIntDef( $intM2mLeaseIntervalId, NULL, false ) );
	}

	public function getM2mLeaseIntervalId() {
		return $this->m_intM2mLeaseIntervalId;
	}

	public function sqlM2mLeaseIntervalId() {
		return ( true == isset( $this->m_intM2mLeaseIntervalId ) ) ? ( string ) $this->m_intM2mLeaseIntervalId : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setLastDelinquencyNoteEventId( $intLastDelinquencyNoteEventId ) {
		$this->set( 'm_intLastDelinquencyNoteEventId', CStrings::strToIntDef( $intLastDelinquencyNoteEventId, NULL, false ) );
	}

	public function getLastDelinquencyNoteEventId() {
		return $this->m_intLastDelinquencyNoteEventId;
	}

	public function sqlLastDelinquencyNoteEventId() {
		return ( true == isset( $this->m_intLastDelinquencyNoteEventId ) ) ? ( string ) $this->m_intLastDelinquencyNoteEventId : 'NULL';
	}

	public function setSubsidyContractId( $intSubsidyContractId ) {
		$this->set( 'm_intSubsidyContractId', CStrings::strToIntDef( $intSubsidyContractId, NULL, false ) );
	}

	public function getSubsidyContractId() {
		return $this->m_intSubsidyContractId;
	}

	public function sqlSubsidyContractId() {
		return ( true == isset( $this->m_intSubsidyContractId ) ) ? ( string ) $this->m_intSubsidyContractId : 'NULL';
	}

	public function setSubsidyContractTypeId( $intSubsidyContractTypeId ) {
		$this->set( 'm_intSubsidyContractTypeId', CStrings::strToIntDef( $intSubsidyContractTypeId, NULL, false ) );
	}

	public function getSubsidyContractTypeId() {
		return $this->m_intSubsidyContractTypeId;
	}

	public function sqlSubsidyContractTypeId() {
		return ( true == isset( $this->m_intSubsidyContractTypeId ) ) ? ( string ) $this->m_intSubsidyContractTypeId : 'NULL';
	}

	public function setSetAsideId( $intSetAsideId ) {
		$this->set( 'm_intSetAsideId', CStrings::strToIntDef( $intSetAsideId, NULL, false ) );
	}

	public function getSetAsideId() {
		return $this->m_intSetAsideId;
	}

	public function sqlSetAsideId() {
		return ( true == isset( $this->m_intSetAsideId ) ) ? ( string ) $this->m_intSetAsideId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setLeaseStatusType( $strLeaseStatusType ) {
		$this->set( 'm_strLeaseStatusType', CStrings::strTrimDef( $strLeaseStatusType, 25, NULL, true ) );
	}

	public function getLeaseStatusType() {
		return $this->m_strLeaseStatusType;
	}

	public function sqlLeaseStatusType() {
		return ( true == isset( $this->m_strLeaseStatusType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseStatusType ) : '\'' . addslashes( $this->m_strLeaseStatusType ) . '\'' ) : 'NULL';
	}

	public function setLeaseIntervalType( $strLeaseIntervalType ) {
		$this->set( 'm_strLeaseIntervalType', CStrings::strTrimDef( $strLeaseIntervalType, 25, NULL, true ) );
	}

	public function getLeaseIntervalType() {
		return $this->m_strLeaseIntervalType;
	}

	public function sqlLeaseIntervalType() {
		return ( true == isset( $this->m_strLeaseIntervalType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseIntervalType ) : '\'' . addslashes( $this->m_strLeaseIntervalType ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->set( 'm_strPrimaryPhoneNumber', CStrings::strTrimDef( $strPrimaryPhoneNumber, 30, NULL, true ) );
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function sqlPrimaryPhoneNumber() {
		return ( true == isset( $this->m_strPrimaryPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryPhoneNumber ) : '\'' . addslashes( $this->m_strPrimaryPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setNoticeDate( $strNoticeDate ) {
		$this->set( 'm_strNoticeDate', CStrings::strTrimDef( $strNoticeDate, -1, NULL, true ) );
	}

	public function getNoticeDate() {
		return $this->m_strNoticeDate;
	}

	public function sqlNoticeDate() {
		return ( true == isset( $this->m_strNoticeDate ) ) ? '\'' . $this->m_strNoticeDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setCollectionsStartDate( $strCollectionsStartDate ) {
		$this->set( 'm_strCollectionsStartDate', CStrings::strTrimDef( $strCollectionsStartDate, -1, NULL, true ) );
	}

	public function getCollectionsStartDate() {
		return $this->m_strCollectionsStartDate;
	}

	public function sqlCollectionsStartDate() {
		return ( true == isset( $this->m_strCollectionsStartDate ) ) ? '\'' . $this->m_strCollectionsStartDate . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumberCache ) : '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' ) : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setResidentBalance( $fltResidentBalance ) {
		$this->set( 'm_fltResidentBalance', CStrings::strToFloatDef( $fltResidentBalance, NULL, false, 2 ) );
	}

	public function getResidentBalance() {
		return $this->m_fltResidentBalance;
	}

	public function sqlResidentBalance() {
		return ( true == isset( $this->m_fltResidentBalance ) ) ? ( string ) $this->m_fltResidentBalance : '0';
	}

	public function setFinancialBalance( $fltFinancialBalance ) {
		$this->set( 'm_fltFinancialBalance', CStrings::strToFloatDef( $fltFinancialBalance, NULL, false, 2 ) );
	}

	public function getFinancialBalance() {
		return $this->m_fltFinancialBalance;
	}

	public function sqlFinancialBalance() {
		return ( true == isset( $this->m_fltFinancialBalance ) ) ? ( string ) $this->m_fltFinancialBalance : '0';
	}

	public function setTotalBalance( $fltTotalBalance ) {
		$this->set( 'm_fltTotalBalance', CStrings::strToFloatDef( $fltTotalBalance, NULL, false, 2 ) );
	}

	public function getTotalBalance() {
		return $this->m_fltTotalBalance;
	}

	public function sqlTotalBalance() {
		return ( true == isset( $this->m_fltTotalBalance ) ) ? ( string ) $this->m_fltTotalBalance : '0';
	}

	public function setRepaymentBalance( $fltRepaymentBalance ) {
		$this->set( 'm_fltRepaymentBalance', CStrings::strToFloatDef( $fltRepaymentBalance, NULL, false, 2 ) );
	}

	public function getRepaymentBalance() {
		return $this->m_fltRepaymentBalance;
	}

	public function sqlRepaymentBalance() {
		return ( true == isset( $this->m_fltRepaymentBalance ) ) ? ( string ) $this->m_fltRepaymentBalance : '0';
	}

	public function setDepositHeld( $fltDepositHeld ) {
		$this->set( 'm_fltDepositHeld', CStrings::strToFloatDef( $fltDepositHeld, NULL, false, 2 ) );
	}

	public function getDepositHeld() {
		return $this->m_fltDepositHeld;
	}

	public function sqlDepositHeld() {
		return ( true == isset( $this->m_fltDepositHeld ) ) ? ( string ) $this->m_fltDepositHeld : '0';
	}

	public function setMoveInReviewCompletedOn( $strMoveInReviewCompletedOn ) {
		$this->set( 'm_strMoveInReviewCompletedOn', CStrings::strTrimDef( $strMoveInReviewCompletedOn, -1, NULL, true ) );
	}

	public function getMoveInReviewCompletedOn() {
		return $this->m_strMoveInReviewCompletedOn;
	}

	public function sqlMoveInReviewCompletedOn() {
		return ( true == isset( $this->m_strMoveInReviewCompletedOn ) ) ? '\'' . $this->m_strMoveInReviewCompletedOn . '\'' : 'NULL';
	}

	public function setMoveOutReviewCompletedOn( $strMoveOutReviewCompletedOn ) {
		$this->set( 'm_strMoveOutReviewCompletedOn', CStrings::strTrimDef( $strMoveOutReviewCompletedOn, -1, NULL, true ) );
	}

	public function getMoveOutReviewCompletedOn() {
		return $this->m_strMoveOutReviewCompletedOn;
	}

	public function sqlMoveOutReviewCompletedOn() {
		return ( true == isset( $this->m_strMoveOutReviewCompletedOn ) ) ? '\'' . $this->m_strMoveOutReviewCompletedOn . '\'' : 'NULL';
	}

	public function setTransferredOn( $strTransferredOn ) {
		$this->set( 'm_strTransferredOn', CStrings::strTrimDef( $strTransferredOn, -1, NULL, true ) );
	}

	public function getTransferredOn() {
		return $this->m_strTransferredOn;
	}

	public function sqlTransferredOn() {
		return ( true == isset( $this->m_strTransferredOn ) ) ? '\'' . $this->m_strTransferredOn . '\'' : 'NULL';
	}

	public function setFmoStartedOn( $strFmoStartedOn ) {
		$this->set( 'm_strFmoStartedOn', CStrings::strTrimDef( $strFmoStartedOn, -1, NULL, true ) );
	}

	public function getFmoStartedOn() {
		return $this->m_strFmoStartedOn;
	}

	public function sqlFmoStartedOn() {
		return ( true == isset( $this->m_strFmoStartedOn ) ) ? '\'' . $this->m_strFmoStartedOn . '\'' : 'NULL';
	}

	public function setFmoApprovedOn( $strFmoApprovedOn ) {
		$this->set( 'm_strFmoApprovedOn', CStrings::strTrimDef( $strFmoApprovedOn, -1, NULL, true ) );
	}

	public function getFmoApprovedOn() {
		return $this->m_strFmoApprovedOn;
	}

	public function sqlFmoApprovedOn() {
		return ( true == isset( $this->m_strFmoApprovedOn ) ) ? '\'' . $this->m_strFmoApprovedOn . '\'' : 'NULL';
	}

	public function setFmoProcessedOn( $strFmoProcessedOn ) {
		$this->set( 'm_strFmoProcessedOn', CStrings::strTrimDef( $strFmoProcessedOn, -1, NULL, true ) );
	}

	public function getFmoProcessedOn() {
		return $this->m_strFmoProcessedOn;
	}

	public function sqlFmoProcessedOn() {
		return ( true == isset( $this->m_strFmoProcessedOn ) ) ? '\'' . $this->m_strFmoProcessedOn . '\'' : 'NULL';
	}

	public function setRenewalPromptMutedOn( $strRenewalPromptMutedOn ) {
		$this->set( 'm_strRenewalPromptMutedOn', CStrings::strTrimDef( $strRenewalPromptMutedOn, -1, NULL, true ) );
	}

	public function getRenewalPromptMutedOn() {
		return $this->m_strRenewalPromptMutedOn;
	}

	public function sqlRenewalPromptMutedOn() {
		return ( true == isset( $this->m_strRenewalPromptMutedOn ) ) ? '\'' . $this->m_strRenewalPromptMutedOn . '\'' : 'NULL';
	}

	public function setRenewalPromptMutedUntil( $strRenewalPromptMutedUntil ) {
		$this->set( 'm_strRenewalPromptMutedUntil', CStrings::strTrimDef( $strRenewalPromptMutedUntil, -1, NULL, true ) );
	}

	public function getRenewalPromptMutedUntil() {
		return $this->m_strRenewalPromptMutedUntil;
	}

	public function sqlRenewalPromptMutedUntil() {
		return ( true == isset( $this->m_strRenewalPromptMutedUntil ) ) ? '\'' . $this->m_strRenewalPromptMutedUntil . '\'' : 'NULL';
	}

	public function setHasRepaymentAgreement( $boolHasRepaymentAgreement ) {
		$this->set( 'm_boolHasRepaymentAgreement', CStrings::strToBool( $boolHasRepaymentAgreement ) );
	}

	public function getHasRepaymentAgreement() {
		return $this->m_boolHasRepaymentAgreement;
	}

	public function sqlHasRepaymentAgreement() {
		return ( true == isset( $this->m_boolHasRepaymentAgreement ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasRepaymentAgreement ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMonthToMonth( $intIsMonthToMonth ) {
		$this->set( 'm_intIsMonthToMonth', CStrings::strToIntDef( $intIsMonthToMonth, NULL, false ) );
	}

	public function getIsMonthToMonth() {
		return $this->m_intIsMonthToMonth;
	}

	public function sqlIsMonthToMonth() {
		return ( true == isset( $this->m_intIsMonthToMonth ) ) ? ( string ) $this->m_intIsMonthToMonth : '0';
	}

	public function setIsTransferringIn( $intIsTransferringIn ) {
		$this->set( 'm_intIsTransferringIn', CStrings::strToIntDef( $intIsTransferringIn, NULL, false ) );
	}

	public function getIsTransferringIn() {
		return $this->m_intIsTransferringIn;
	}

	public function sqlIsTransferringIn() {
		return ( true == isset( $this->m_intIsTransferringIn ) ) ? ( string ) $this->m_intIsTransferringIn : '0';
	}

	public function setIsMultiSlot( $intIsMultiSlot ) {
		$this->set( 'm_intIsMultiSlot', CStrings::strToIntDef( $intIsMultiSlot, NULL, false ) );
	}

	public function getIsMultiSlot() {
		return $this->m_intIsMultiSlot;
	}

	public function sqlIsMultiSlot() {
		return ( true == isset( $this->m_intIsMultiSlot ) ) ? ( string ) $this->m_intIsMultiSlot : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLeaseSubStatus( $strLeaseSubStatus ) {
		$this->set( 'm_strLeaseSubStatus', CStrings::strTrimDef( $strLeaseSubStatus, 50, NULL, true ) );
	}

	public function getLeaseSubStatus() {
		return $this->m_strLeaseSubStatus;
	}

	public function sqlLeaseSubStatus() {
		return ( true == isset( $this->m_strLeaseSubStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLeaseSubStatus ) : '\'' . addslashes( $this->m_strLeaseSubStatus ) . '\'' ) : 'NULL';
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function sqlOrganizationContractId() {
		return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
	}

	public function setScheduledMoveInStartTime( $strScheduledMoveInStartTime ) {
		$this->set( 'm_strScheduledMoveInStartTime', CStrings::strTrimDef( $strScheduledMoveInStartTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInStartTime() {
		return $this->m_strScheduledMoveInStartTime;
	}

	public function sqlScheduledMoveInStartTime() {
		return ( true == isset( $this->m_strScheduledMoveInStartTime ) ) ? '\'' . $this->m_strScheduledMoveInStartTime . '\'' : 'NULL';
	}

	public function setScheduledMoveInEndTime( $strScheduledMoveInEndTime ) {
		$this->set( 'm_strScheduledMoveInEndTime', CStrings::strTrimDef( $strScheduledMoveInEndTime, NULL, NULL, true ) );
	}

	public function getScheduledMoveInEndTime() {
		return $this->m_strScheduledMoveInEndTime;
	}

	public function sqlScheduledMoveInEndTime() {
		return ( true == isset( $this->m_strScheduledMoveInEndTime ) ) ? '\'' . $this->m_strScheduledMoveInEndTime . '\'' : 'NULL';
	}

	public function setNameFull( $strNameFull ) {
		$this->set( 'm_strNameFull', CStrings::strTrimDef( $strNameFull, 3000, NULL, true ) );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function sqlNameFull() {
		return ( true == isset( $this->m_strNameFull ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFull ) : '\'' . addslashes( $this->m_strNameFull ) . '\'' ) : 'NULL';
	}

	public function setMoveInBalance( $strMoveInBalance ) {
		$this->set( 'm_strMoveInBalance', CStrings::strTrimDef( $strMoveInBalance, -1, NULL, true ) );
	}

	public function getMoveInBalance() {
		return $this->m_strMoveInBalance;
	}

	public function sqlMoveInBalance() {
		return ( true == isset( $this->m_strMoveInBalance ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMoveInBalance ) : '\'' . addslashes( $this->m_strMoveInBalance ) . '\'' ) : 'NULL';
	}

	public function setTransferFromLeaseId( $intTransferFromLeaseId ) {
		$this->set( 'm_intTransferFromLeaseId', CStrings::strToIntDef( $intTransferFromLeaseId, NULL, false ) );
	}

	public function getTransferFromLeaseId() {
		return $this->m_intTransferFromLeaseId;
	}

	public function sqlTransferFromLeaseId() {
		return ( true == isset( $this->m_intTransferFromLeaseId ) ) ? ( string ) $this->m_intTransferFromLeaseId : 'NULL';
	}

	public function setSelectedDepositAlternative( $boolSelectedDepositAlternative ) {
		$this->set( 'm_boolSelectedDepositAlternative', CStrings::strToBool( $boolSelectedDepositAlternative ) );
	}

	public function getSelectedDepositAlternative() {
		return $this->m_boolSelectedDepositAlternative;
	}

	public function sqlSelectedDepositAlternative() {
		return ( true == isset( $this->m_boolSelectedDepositAlternative ) ) ? '\'' . ( true == ( bool ) $this->m_boolSelectedDepositAlternative ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, reimbursing_property_id, owner_id, property_unit_id, unit_space_id, occupancy_type_id, payment_allowance_type_id, primary_customer_id, transfer_lease_id, lease_status_type_id, lease_interval_type_id, termination_list_type_id, active_lease_interval_id, m2m_lease_interval_id, integration_database_id, last_delinquency_note_event_id, subsidy_contract_id, subsidy_contract_type_id, set_aside_id, remote_primary_key, lease_status_type, lease_interval_type, company_name, name_first, name_middle, name_last, primary_phone_number, lease_start_date, lease_end_date, move_in_date, notice_date, move_out_date, collections_start_date, property_name, building_name, unit_number_cache, display_number, resident_balance, financial_balance, total_balance, repayment_balance, deposit_held, move_in_review_completed_on, move_out_review_completed_on, transferred_on, fmo_started_on, fmo_approved_on, fmo_processed_on, renewal_prompt_muted_on, renewal_prompt_muted_until, has_repayment_agreement, is_month_to_month, is_transferring_in, is_multi_slot, updated_by, updated_on, created_by, created_on, lease_sub_status, organization_contract_id, scheduled_move_in_start_time, scheduled_move_in_end_time, name_full, move_in_balance, transfer_from_lease_id, selected_deposit_alternative )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReimbursingPropertyId() . ', ' .
						$this->sqlOwnerId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlTransferLeaseId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ', ' .
						$this->sqlTerminationListTypeId() . ', ' .
						$this->sqlActiveLeaseIntervalId() . ', ' .
						$this->sqlM2mLeaseIntervalId() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlLastDelinquencyNoteEventId() . ', ' .
						$this->sqlSubsidyContractId() . ', ' .
						$this->sqlSubsidyContractTypeId() . ', ' .
						$this->sqlSetAsideId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlLeaseStatusType() . ', ' .
						$this->sqlLeaseIntervalType() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlPrimaryPhoneNumber() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlNoticeDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlCollectionsStartDate() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlResidentBalance() . ', ' .
						$this->sqlFinancialBalance() . ', ' .
						$this->sqlTotalBalance() . ', ' .
						$this->sqlRepaymentBalance() . ', ' .
						$this->sqlDepositHeld() . ', ' .
						$this->sqlMoveInReviewCompletedOn() . ', ' .
						$this->sqlMoveOutReviewCompletedOn() . ', ' .
						$this->sqlTransferredOn() . ', ' .
						$this->sqlFmoStartedOn() . ', ' .
						$this->sqlFmoApprovedOn() . ', ' .
						$this->sqlFmoProcessedOn() . ', ' .
						$this->sqlRenewalPromptMutedOn() . ', ' .
						$this->sqlRenewalPromptMutedUntil() . ', ' .
						$this->sqlHasRepaymentAgreement() . ', ' .
						$this->sqlIsMonthToMonth() . ', ' .
						$this->sqlIsTransferringIn() . ', ' .
						$this->sqlIsMultiSlot() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLeaseSubStatus() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlScheduledMoveInStartTime() . ', ' .
						$this->sqlScheduledMoveInEndTime() . ', ' .
						$this->sqlNameFull() . ', ' .
						$this->sqlMoveInBalance() . ', ' .
						$this->sqlTransferFromLeaseId() . ', ' .
						$this->sqlSelectedDepositAlternative() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursing_property_id = ' . $this->sqlReimbursingPropertyId(). ',' ; } elseif( true == array_key_exists( 'ReimbursingPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursing_property_id = ' . $this->sqlReimbursingPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId(). ',' ; } elseif( true == array_key_exists( 'OwnerId', $this->getChangedColumns() ) ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentAllowanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_allowance_type_id = ' . $this->sqlPaymentAllowanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId(). ',' ; } elseif( true == array_key_exists( 'PrimaryCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' primary_customer_id = ' . $this->sqlPrimaryCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type_id = ' . $this->sqlLeaseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_list_type_id = ' . $this->sqlTerminationListTypeId(). ',' ; } elseif( true == array_key_exists( 'TerminationListTypeId', $this->getChangedColumns() ) ) { $strSql .= ' termination_list_type_id = ' . $this->sqlTerminationListTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' active_lease_interval_id = ' . $this->sqlActiveLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'ActiveLeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' active_lease_interval_id = ' . $this->sqlActiveLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' m2m_lease_interval_id = ' . $this->sqlM2mLeaseIntervalId(). ',' ; } elseif( true == array_key_exists( 'M2mLeaseIntervalId', $this->getChangedColumns() ) ) { $strSql .= ' m2m_lease_interval_id = ' . $this->sqlM2mLeaseIntervalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_delinquency_note_event_id = ' . $this->sqlLastDelinquencyNoteEventId(). ',' ; } elseif( true == array_key_exists( 'LastDelinquencyNoteEventId', $this->getChangedColumns() ) ) { $strSql .= ' last_delinquency_note_event_id = ' . $this->sqlLastDelinquencyNoteEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_id = ' . $this->sqlSubsidyContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_contract_type_id = ' . $this->sqlSubsidyContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId(). ',' ; } elseif( true == array_key_exists( 'SetAsideId', $this->getChangedColumns() ) ) { $strSql .= ' set_aside_id = ' . $this->sqlSetAsideId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType(). ',' ; } elseif( true == array_key_exists( 'LeaseStatusType', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type = ' . $this->sqlLeaseStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType(). ',' ; } elseif( true == array_key_exists( 'LeaseIntervalType', $this->getChangedColumns() ) ) { $strSql .= ' lease_interval_type = ' . $this->sqlLeaseIntervalType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number = ' . $this->sqlPrimaryPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PrimaryPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number = ' . $this->sqlPrimaryPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate(). ',' ; } elseif( true == array_key_exists( 'NoticeDate', $this->getChangedColumns() ) ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate(). ',' ; } elseif( true == array_key_exists( 'CollectionsStartDate', $this->getChangedColumns() ) ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache(). ',' ; } elseif( true == array_key_exists( 'UnitNumberCache', $this->getChangedColumns() ) ) { $strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber(). ',' ; } elseif( true == array_key_exists( 'DisplayNumber', $this->getChangedColumns() ) ) { $strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_balance = ' . $this->sqlResidentBalance(). ',' ; } elseif( true == array_key_exists( 'ResidentBalance', $this->getChangedColumns() ) ) { $strSql .= ' resident_balance = ' . $this->sqlResidentBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_balance = ' . $this->sqlFinancialBalance(). ',' ; } elseif( true == array_key_exists( 'FinancialBalance', $this->getChangedColumns() ) ) { $strSql .= ' financial_balance = ' . $this->sqlFinancialBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_balance = ' . $this->sqlTotalBalance(). ',' ; } elseif( true == array_key_exists( 'TotalBalance', $this->getChangedColumns() ) ) { $strSql .= ' total_balance = ' . $this->sqlTotalBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repayment_balance = ' . $this->sqlRepaymentBalance(). ',' ; } elseif( true == array_key_exists( 'RepaymentBalance', $this->getChangedColumns() ) ) { $strSql .= ' repayment_balance = ' . $this->sqlRepaymentBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_held = ' . $this->sqlDepositHeld(). ',' ; } elseif( true == array_key_exists( 'DepositHeld', $this->getChangedColumns() ) ) { $strSql .= ' deposit_held = ' . $this->sqlDepositHeld() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_review_completed_on = ' . $this->sqlMoveInReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'MoveInReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' move_in_review_completed_on = ' . $this->sqlMoveInReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_review_completed_on = ' . $this->sqlMoveOutReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'MoveOutReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' move_out_review_completed_on = ' . $this->sqlMoveOutReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn(). ',' ; } elseif( true == array_key_exists( 'TransferredOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn(). ',' ; } elseif( true == array_key_exists( 'FmoStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn(). ',' ; } elseif( true == array_key_exists( 'FmoApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn(). ',' ; } elseif( true == array_key_exists( 'FmoProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_prompt_muted_on = ' . $this->sqlRenewalPromptMutedOn(). ',' ; } elseif( true == array_key_exists( 'RenewalPromptMutedOn', $this->getChangedColumns() ) ) { $strSql .= ' renewal_prompt_muted_on = ' . $this->sqlRenewalPromptMutedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_prompt_muted_until = ' . $this->sqlRenewalPromptMutedUntil(). ',' ; } elseif( true == array_key_exists( 'RenewalPromptMutedUntil', $this->getChangedColumns() ) ) { $strSql .= ' renewal_prompt_muted_until = ' . $this->sqlRenewalPromptMutedUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_repayment_agreement = ' . $this->sqlHasRepaymentAgreement(). ',' ; } elseif( true == array_key_exists( 'HasRepaymentAgreement', $this->getChangedColumns() ) ) { $strSql .= ' has_repayment_agreement = ' . $this->sqlHasRepaymentAgreement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_month_to_month = ' . $this->sqlIsMonthToMonth(). ',' ; } elseif( true == array_key_exists( 'IsMonthToMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_month_to_month = ' . $this->sqlIsMonthToMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn(). ',' ; } elseif( true == array_key_exists( 'IsTransferringIn', $this->getChangedColumns() ) ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_multi_slot = ' . $this->sqlIsMultiSlot(). ',' ; } elseif( true == array_key_exists( 'IsMultiSlot', $this->getChangedColumns() ) ) { $strSql .= ' is_multi_slot = ' . $this->sqlIsMultiSlot() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_sub_status = ' . $this->sqlLeaseSubStatus(). ',' ; } elseif( true == array_key_exists( 'LeaseSubStatus', $this->getChangedColumns() ) ) { $strSql .= ' lease_sub_status = ' . $this->sqlLeaseSubStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId(). ',' ; } elseif( true == array_key_exists( 'OrganizationContractId', $this->getChangedColumns() ) ) { $strSql .= ' organization_contract_id = ' . $this->sqlOrganizationContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime(). ',' ; } elseif( true == array_key_exists( 'ScheduledMoveInStartTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_start_time = ' . $this->sqlScheduledMoveInStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime(). ',' ; } elseif( true == array_key_exists( 'ScheduledMoveInEndTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_move_in_end_time = ' . $this->sqlScheduledMoveInEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_full = ' . $this->sqlNameFull(). ',' ; } elseif( true == array_key_exists( 'NameFull', $this->getChangedColumns() ) ) { $strSql .= ' name_full = ' . $this->sqlNameFull() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_balance = ' . $this->sqlMoveInBalance(). ',' ; } elseif( true == array_key_exists( 'MoveInBalance', $this->getChangedColumns() ) ) { $strSql .= ' move_in_balance = ' . $this->sqlMoveInBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferFromLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selected_deposit_alternative = ' . $this->sqlSelectedDepositAlternative(). ',' ; } elseif( true == array_key_exists( 'SelectedDepositAlternative', $this->getChangedColumns() ) ) { $strSql .= ' selected_deposit_alternative = ' . $this->sqlSelectedDepositAlternative() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'reimbursing_property_id' => $this->getReimbursingPropertyId(),
			'owner_id' => $this->getOwnerId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'payment_allowance_type_id' => $this->getPaymentAllowanceTypeId(),
			'primary_customer_id' => $this->getPrimaryCustomerId(),
			'transfer_lease_id' => $this->getTransferLeaseId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'termination_list_type_id' => $this->getTerminationListTypeId(),
			'active_lease_interval_id' => $this->getActiveLeaseIntervalId(),
			'm2m_lease_interval_id' => $this->getM2mLeaseIntervalId(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'last_delinquency_note_event_id' => $this->getLastDelinquencyNoteEventId(),
			'subsidy_contract_id' => $this->getSubsidyContractId(),
			'subsidy_contract_type_id' => $this->getSubsidyContractTypeId(),
			'set_aside_id' => $this->getSetAsideId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'lease_status_type' => $this->getLeaseStatusType(),
			'lease_interval_type' => $this->getLeaseIntervalType(),
			'company_name' => $this->getCompanyName(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'primary_phone_number' => $this->getPrimaryPhoneNumber(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'notice_date' => $this->getNoticeDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'collections_start_date' => $this->getCollectionsStartDate(),
			'property_name' => $this->getPropertyName(),
			'building_name' => $this->getBuildingName(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'resident_balance' => $this->getResidentBalance(),
			'financial_balance' => $this->getFinancialBalance(),
			'total_balance' => $this->getTotalBalance(),
			'repayment_balance' => $this->getRepaymentBalance(),
			'deposit_held' => $this->getDepositHeld(),
			'move_in_review_completed_on' => $this->getMoveInReviewCompletedOn(),
			'move_out_review_completed_on' => $this->getMoveOutReviewCompletedOn(),
			'transferred_on' => $this->getTransferredOn(),
			'fmo_started_on' => $this->getFmoStartedOn(),
			'fmo_approved_on' => $this->getFmoApprovedOn(),
			'fmo_processed_on' => $this->getFmoProcessedOn(),
			'renewal_prompt_muted_on' => $this->getRenewalPromptMutedOn(),
			'renewal_prompt_muted_until' => $this->getRenewalPromptMutedUntil(),
			'has_repayment_agreement' => $this->getHasRepaymentAgreement(),
			'is_month_to_month' => $this->getIsMonthToMonth(),
			'is_transferring_in' => $this->getIsTransferringIn(),
			'is_multi_slot' => $this->getIsMultiSlot(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'lease_sub_status' => $this->getLeaseSubStatus(),
			'organization_contract_id' => $this->getOrganizationContractId(),
			'scheduled_move_in_start_time' => $this->getScheduledMoveInStartTime(),
			'scheduled_move_in_end_time' => $this->getScheduledMoveInEndTime(),
			'name_full' => $this->getNameFull(),
			'move_in_balance' => $this->getMoveInBalance(),
			'transfer_from_lease_id' => $this->getTransferFromLeaseId(),
			'selected_deposit_alternative' => $this->getSelectedDepositAlternative()
		);
	}

}
?>