<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpaceConfigurations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpaceConfigurations extends CEosPluralBase {

	/**
	 * @return CSpaceConfiguration[]
	 */
	public static function fetchSpaceConfigurations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSpaceConfiguration::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSpaceConfiguration
	 */
	public static function fetchSpaceConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSpaceConfiguration::class, $objDatabase );
	}

	public static function fetchSpaceConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'space_configurations', $objDatabase );
	}

	public static function fetchSpaceConfigurationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSpaceConfiguration( sprintf( 'SELECT * FROM space_configurations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchSpaceConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchSpaceConfigurations( sprintf( 'SELECT * FROM space_configurations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSpaceConfigurationsByDefaultSpaceConfigurationIdByCid( $intDefaultSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchSpaceConfigurations( sprintf( 'SELECT * FROM space_configurations WHERE default_space_configuration_id = %d AND cid = %d', $intDefaultSpaceConfigurationId, $intCid ), $objDatabase );
	}

}
?>