<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTrainingDatabases
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTrainingDatabases extends CEosPluralBase {

	/**
	 * @return CTrainingDatabase[]
	 */
	public static function fetchTrainingDatabases( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTrainingDatabase::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTrainingDatabase
	 */
	public static function fetchTrainingDatabase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTrainingDatabase::class, $objDatabase );
	}

	public static function fetchTrainingDatabaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_databases', $objDatabase );
	}

	public static function fetchTrainingDatabaseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTrainingDatabase( sprintf( 'SELECT * FROM training_databases WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchTrainingDatabasesByCid( $intCid, $objDatabase ) {
		return self::fetchTrainingDatabases( sprintf( 'SELECT * FROM training_databases WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>