<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetChangeOrderLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetChangeOrderLogs extends CEosPluralBase {

	/**
	 * @return CBudgetChangeOrderLog[]
	 */
	public static function fetchBudgetChangeOrderLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CBudgetChangeOrderLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetChangeOrderLog
	 */
	public static function fetchBudgetChangeOrderLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBudgetChangeOrderLog', $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_change_order_logs', $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrderLog( sprintf( 'SELECT * FROM budget_change_order_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrderLogs( sprintf( 'SELECT * FROM budget_change_order_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogsByBudgetChangeOrderIdByCid( $intBudgetChangeOrderId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrderLogs( sprintf( 'SELECT * FROM budget_change_order_logs WHERE budget_change_order_id = %d AND cid = %d', ( int ) $intBudgetChangeOrderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrderLogs( sprintf( 'SELECT * FROM budget_change_order_logs WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetChangeOrderLogsByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchBudgetChangeOrderLogs( sprintf( 'SELECT * FROM budget_change_order_logs WHERE ap_contract_id = %d AND cid = %d', ( int ) $intApContractId, ( int ) $intCid ), $objDatabase );
	}

}
?>