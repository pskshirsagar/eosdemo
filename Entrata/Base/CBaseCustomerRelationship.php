<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerRelationship extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.customer_relationships';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCustomerRelationshipId;
	protected $m_intCustomerRelationshipGroupId;
	protected $m_intCustomerTypeId;
	protected $m_strName;
	protected $m_strMarketingName;
	protected $m_boolIsDefault;
	protected $m_boolIsSystem;
	protected $m_boolShowInternally;
	protected $m_boolShowExternally;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApplicationFeeTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDefault = false;
		$this->m_boolIsSystem = false;
		$this->m_boolShowInternally = true;
		$this->m_boolShowExternally = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCustomerRelationshipId', trim( $arrValues['default_customer_relationship_id'] ) ); elseif( isset( $arrValues['default_customer_relationship_id'] ) ) $this->setDefaultCustomerRelationshipId( $arrValues['default_customer_relationship_id'] );
		if( isset( $arrValues['customer_relationship_group_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipGroupId', trim( $arrValues['customer_relationship_group_id'] ) ); elseif( isset( $arrValues['customer_relationship_group_id'] ) ) $this->setCustomerRelationshipGroupId( $arrValues['customer_relationship_group_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['marketing_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMarketingName', trim( stripcslashes( $arrValues['marketing_name'] ) ) ); elseif( isset( $arrValues['marketing_name'] ) ) $this->setMarketingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_name'] ) : $arrValues['marketing_name'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['show_internally'] ) && $boolDirectSet ) $this->set( 'm_boolShowInternally', trim( stripcslashes( $arrValues['show_internally'] ) ) ); elseif( isset( $arrValues['show_internally'] ) ) $this->setShowInternally( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_internally'] ) : $arrValues['show_internally'] );
		if( isset( $arrValues['show_externally'] ) && $boolDirectSet ) $this->set( 'm_boolShowExternally', trim( stripcslashes( $arrValues['show_externally'] ) ) ); elseif( isset( $arrValues['show_externally'] ) ) $this->setShowExternally( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_externally'] ) : $arrValues['show_externally'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['application_fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationFeeTypeId', trim( $arrValues['application_fee_type_id'] ) ); elseif( isset( $arrValues['application_fee_type_id'] ) ) $this->setApplicationFeeTypeId( $arrValues['application_fee_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultCustomerRelationshipId( $intDefaultCustomerRelationshipId ) {
		$this->set( 'm_intDefaultCustomerRelationshipId', CStrings::strToIntDef( $intDefaultCustomerRelationshipId, NULL, false ) );
	}

	public function getDefaultCustomerRelationshipId() {
		return $this->m_intDefaultCustomerRelationshipId;
	}

	public function sqlDefaultCustomerRelationshipId() {
		return ( true == isset( $this->m_intDefaultCustomerRelationshipId ) ) ? ( string ) $this->m_intDefaultCustomerRelationshipId : 'NULL';
	}

	public function setCustomerRelationshipGroupId( $intCustomerRelationshipGroupId ) {
		$this->set( 'm_intCustomerRelationshipGroupId', CStrings::strToIntDef( $intCustomerRelationshipGroupId, NULL, false ) );
	}

	public function getCustomerRelationshipGroupId() {
		return $this->m_intCustomerRelationshipGroupId;
	}

	public function sqlCustomerRelationshipGroupId() {
		return ( true == isset( $this->m_intCustomerRelationshipGroupId ) ) ? ( string ) $this->m_intCustomerRelationshipGroupId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 500, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setMarketingName( $strMarketingName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMarketingName', CStrings::strTrimDef( $strMarketingName, 500, NULL, true ), $strLocaleCode );
	}

	public function getMarketingName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMarketingName', $strLocaleCode );
	}

	public function sqlMarketingName() {
		return ( true == isset( $this->m_strMarketingName ) ) ? '\'' . addslashes( $this->m_strMarketingName ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInternally( $boolShowInternally ) {
		$this->set( 'm_boolShowInternally', CStrings::strToBool( $boolShowInternally ) );
	}

	public function getShowInternally() {
		return $this->m_boolShowInternally;
	}

	public function sqlShowInternally() {
		return ( true == isset( $this->m_boolShowInternally ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInternally ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowExternally( $boolShowExternally ) {
		$this->set( 'm_boolShowExternally', CStrings::strToBool( $boolShowExternally ) );
	}

	public function getShowExternally() {
		return $this->m_boolShowExternally;
	}

	public function sqlShowExternally() {
		return ( true == isset( $this->m_boolShowExternally ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowExternally ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApplicationFeeTypeId( $intApplicationFeeTypeId ) {
		$this->set( 'm_intApplicationFeeTypeId', CStrings::strToIntDef( $intApplicationFeeTypeId, NULL, false ) );
	}

	public function getApplicationFeeTypeId() {
		return $this->m_intApplicationFeeTypeId;
	}

	public function sqlApplicationFeeTypeId() {
		return ( true == isset( $this->m_intApplicationFeeTypeId ) ) ? ( string ) $this->m_intApplicationFeeTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_customer_relationship_id, customer_relationship_group_id, customer_type_id, name, marketing_name, is_default, is_system, show_internally, show_externally, order_num, updated_by, updated_on, created_by, created_on, application_fee_type_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCustomerRelationshipId() . ', ' .
						$this->sqlCustomerRelationshipGroupId() . ', ' .
						$this->sqlCustomerTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlMarketingName() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlShowInternally() . ', ' .
						$this->sqlShowExternally() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApplicationFeeTypeId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_customer_relationship_id = ' . $this->sqlDefaultCustomerRelationshipId(). ',' ; } elseif( true == array_key_exists( 'DefaultCustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' default_customer_relationship_id = ' . $this->sqlDefaultCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_group_id = ' . $this->sqlCustomerRelationshipGroupId(). ',' ; } elseif( true == array_key_exists( 'CustomerRelationshipGroupId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_group_id = ' . $this->sqlCustomerRelationshipGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName(). ',' ; } elseif( true == array_key_exists( 'MarketingName', $this->getChangedColumns() ) ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_internally = ' . $this->sqlShowInternally(). ',' ; } elseif( true == array_key_exists( 'ShowInternally', $this->getChangedColumns() ) ) { $strSql .= ' show_internally = ' . $this->sqlShowInternally() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_externally = ' . $this->sqlShowExternally(). ',' ; } elseif( true == array_key_exists( 'ShowExternally', $this->getChangedColumns() ) ) { $strSql .= ' show_externally = ' . $this->sqlShowExternally() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_fee_type_id = ' . $this->sqlApplicationFeeTypeId(). ',' ; } elseif( true == array_key_exists( 'ApplicationFeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' application_fee_type_id = ' . $this->sqlApplicationFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_customer_relationship_id' => $this->getDefaultCustomerRelationshipId(),
			'customer_relationship_group_id' => $this->getCustomerRelationshipGroupId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'name' => $this->getName(),
			'marketing_name' => $this->getMarketingName(),
			'is_default' => $this->getIsDefault(),
			'is_system' => $this->getIsSystem(),
			'show_internally' => $this->getShowInternally(),
			'show_externally' => $this->getShowExternally(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'application_fee_type_id' => $this->getApplicationFeeTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>