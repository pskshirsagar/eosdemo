<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatchEmails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileBatchEmails extends CEosPluralBase {

	/**
	 * @return CFileBatchEmail[]
	 */
	public static function fetchFileBatchEmails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileBatchEmail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileBatchEmail
	 */
	public static function fetchFileBatchEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileBatchEmail::class, $objDatabase );
	}

	public static function fetchFileBatchEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_batch_emails', $objDatabase );
	}

	public static function fetchFileBatchEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileBatchEmail( sprintf( 'SELECT * FROM file_batch_emails WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileBatchEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchFileBatchEmails( sprintf( 'SELECT * FROM file_batch_emails WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFileBatchEmailsByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		return self::fetchFileBatchEmails( sprintf( 'SELECT * FROM file_batch_emails WHERE file_batch_id = %d AND cid = %d', $intFileBatchId, $intCid ), $objDatabase );
	}

	public static function fetchFileBatchEmailsByFileExportStatusIdByCid( $intFileExportStatusId, $intCid, $objDatabase ) {
		return self::fetchFileBatchEmails( sprintf( 'SELECT * FROM file_batch_emails WHERE file_export_status_id = %d AND cid = %d', $intFileExportStatusId, $intCid ), $objDatabase );
	}

}
?>