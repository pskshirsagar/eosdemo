<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserAssessment extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.company_user_assessments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intCompanyUserId;
	protected $m_intHelpResourceId;
	protected $m_intCompanyGroupAssessmentId;
	protected $m_strAssignedOn;
	protected $m_strDueDate;
	protected $m_strCompletedOn;
	protected $m_intOrderNum;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intTrainingTeamAssessmentId;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['company_group_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyGroupAssessmentId', trim( $arrValues['company_group_assessment_id'] ) ); elseif( isset( $arrValues['company_group_assessment_id'] ) ) $this->setCompanyGroupAssessmentId( $arrValues['company_group_assessment_id'] );
		if( isset( $arrValues['assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strAssignedOn', trim( $arrValues['assigned_on'] ) ); elseif( isset( $arrValues['assigned_on'] ) ) $this->setAssignedOn( $arrValues['assigned_on'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['training_team_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingTeamAssessmentId', trim( $arrValues['training_team_assessment_id'] ) ); elseif( isset( $arrValues['training_team_assessment_id'] ) ) $this->setTrainingTeamAssessmentId( $arrValues['training_team_assessment_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setCompanyGroupAssessmentId( $intCompanyGroupAssessmentId ) {
		$this->set( 'm_intCompanyGroupAssessmentId', CStrings::strToIntDef( $intCompanyGroupAssessmentId, NULL, false ) );
	}

	public function getCompanyGroupAssessmentId() {
		return $this->m_intCompanyGroupAssessmentId;
	}

	public function sqlCompanyGroupAssessmentId() {
		return ( true == isset( $this->m_intCompanyGroupAssessmentId ) ) ? ( string ) $this->m_intCompanyGroupAssessmentId : 'NULL';
	}

	public function setAssignedOn( $strAssignedOn ) {
		$this->set( 'm_strAssignedOn', CStrings::strTrimDef( $strAssignedOn, -1, NULL, true ) );
	}

	public function getAssignedOn() {
		return $this->m_strAssignedOn;
	}

	public function sqlAssignedOn() {
		return ( true == isset( $this->m_strAssignedOn ) ) ? '\'' . $this->m_strAssignedOn . '\'' : 'NULL';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTrainingTeamAssessmentId( $intTrainingTeamAssessmentId ) {
		$this->set( 'm_intTrainingTeamAssessmentId', CStrings::strToIntDef( $intTrainingTeamAssessmentId, NULL, false ) );
	}

	public function getTrainingTeamAssessmentId() {
		return $this->m_intTrainingTeamAssessmentId;
	}

	public function sqlTrainingTeamAssessmentId() {
		return ( true == isset( $this->m_intTrainingTeamAssessmentId ) ) ? ( string ) $this->m_intTrainingTeamAssessmentId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, company_user_id, help_resource_id, company_group_assessment_id, assigned_on, due_date, completed_on, order_num, deleted_on, deleted_by, updated_by, updated_on, created_by, created_on, details, training_team_assessment_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlHelpResourceId() . ', ' .
						$this->sqlCompanyGroupAssessmentId() . ', ' .
						$this->sqlAssignedOn() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTrainingTeamAssessmentId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_group_assessment_id = ' . $this->sqlCompanyGroupAssessmentId(). ',' ; } elseif( true == array_key_exists( 'CompanyGroupAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_group_assessment_id = ' . $this->sqlCompanyGroupAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn(). ',' ; } elseif( true == array_key_exists( 'AssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_team_assessment_id = ' . $this->sqlTrainingTeamAssessmentId(). ',' ; } elseif( true == array_key_exists( 'TrainingTeamAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' training_team_assessment_id = ' . $this->sqlTrainingTeamAssessmentId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'help_resource_id' => $this->getHelpResourceId(),
			'company_group_assessment_id' => $this->getCompanyGroupAssessmentId(),
			'assigned_on' => $this->getAssignedOn(),
			'due_date' => $this->getDueDate(),
			'completed_on' => $this->getCompletedOn(),
			'order_num' => $this->getOrderNum(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'training_team_assessment_id' => $this->getTrainingTeamAssessmentId()
		);
	}

}
?>