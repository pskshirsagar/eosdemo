<?php

class CBaseDefaultUnitSpaceNamePattern extends CEosSingularBase {

	const TABLE_NAME = 'public.default_unit_space_name_patterns';

	protected $m_intId;
	protected $m_strUnitNumberPattern;
	protected $m_strUnitSpacePattern;
	protected $m_strUnitSpaceNameExamples;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['unit_number_pattern'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberPattern', trim( stripcslashes( $arrValues['unit_number_pattern'] ) ) ); elseif( isset( $arrValues['unit_number_pattern'] ) ) $this->setUnitNumberPattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_pattern'] ) : $arrValues['unit_number_pattern'] );
		if( isset( $arrValues['unit_space_pattern'] ) && $boolDirectSet ) $this->set( 'm_strUnitSpacePattern', trim( stripcslashes( $arrValues['unit_space_pattern'] ) ) ); elseif( isset( $arrValues['unit_space_pattern'] ) ) $this->setUnitSpacePattern( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_pattern'] ) : $arrValues['unit_space_pattern'] );
		if( isset( $arrValues['unit_space_name_examples'] ) && $boolDirectSet ) $this->set( 'm_strUnitSpaceNameExamples', trim( stripcslashes( $arrValues['unit_space_name_examples'] ) ) ); elseif( isset( $arrValues['unit_space_name_examples'] ) ) $this->setUnitSpaceNameExamples( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_name_examples'] ) : $arrValues['unit_space_name_examples'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUnitNumberPattern( $strUnitNumberPattern ) {
		$this->set( 'm_strUnitNumberPattern', CStrings::strTrimDef( $strUnitNumberPattern, 50, NULL, true ) );
	}

	public function getUnitNumberPattern() {
		return $this->m_strUnitNumberPattern;
	}

	public function sqlUnitNumberPattern() {
		return ( true == isset( $this->m_strUnitNumberPattern ) ) ? '\'' . addslashes( $this->m_strUnitNumberPattern ) . '\'' : 'NULL';
	}

	public function setUnitSpacePattern( $strUnitSpacePattern ) {
		$this->set( 'm_strUnitSpacePattern', CStrings::strTrimDef( $strUnitSpacePattern, 50, NULL, true ) );
	}

	public function getUnitSpacePattern() {
		return $this->m_strUnitSpacePattern;
	}

	public function sqlUnitSpacePattern() {
		return ( true == isset( $this->m_strUnitSpacePattern ) ) ? '\'' . addslashes( $this->m_strUnitSpacePattern ) . '\'' : 'NULL';
	}

	public function setUnitSpaceNameExamples( $strUnitSpaceNameExamples ) {
		$this->set( 'm_strUnitSpaceNameExamples', CStrings::strTrimDef( $strUnitSpaceNameExamples, -1, NULL, true ) );
	}

	public function getUnitSpaceNameExamples() {
		return $this->m_strUnitSpaceNameExamples;
	}

	public function sqlUnitSpaceNameExamples() {
		return ( true == isset( $this->m_strUnitSpaceNameExamples ) ) ? '\'' . addslashes( $this->m_strUnitSpaceNameExamples ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'unit_number_pattern' => $this->getUnitNumberPattern(),
			'unit_space_pattern' => $this->getUnitSpacePattern(),
			'unit_space_name_examples' => $this->getUnitSpaceNameExamples()
		);
	}

}
?>