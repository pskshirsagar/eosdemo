<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobGroup extends CEosSingularBase {

	const TABLE_NAME = 'public.job_groups';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobId;
	protected $m_strName;
	protected $m_strPlannedStartDate;
	protected $m_strPlannedCompletionDate;
	protected $m_strActualStartDate;
	protected $m_strActualCompletionDate;
	protected $m_boolIsUnitType;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsUnitType = true;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['planned_start_date'] ) && $boolDirectSet ) $this->set( 'm_strPlannedStartDate', trim( $arrValues['planned_start_date'] ) ); elseif( isset( $arrValues['planned_start_date'] ) ) $this->setPlannedStartDate( $arrValues['planned_start_date'] );
		if( isset( $arrValues['planned_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strPlannedCompletionDate', trim( $arrValues['planned_completion_date'] ) ); elseif( isset( $arrValues['planned_completion_date'] ) ) $this->setPlannedCompletionDate( $arrValues['planned_completion_date'] );
		if( isset( $arrValues['actual_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDate', trim( $arrValues['actual_start_date'] ) ); elseif( isset( $arrValues['actual_start_date'] ) ) $this->setActualStartDate( $arrValues['actual_start_date'] );
		if( isset( $arrValues['actual_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strActualCompletionDate', trim( $arrValues['actual_completion_date'] ) ); elseif( isset( $arrValues['actual_completion_date'] ) ) $this->setActualCompletionDate( $arrValues['actual_completion_date'] );
		if( isset( $arrValues['is_unit_type'] ) && $boolDirectSet ) $this->set( 'm_boolIsUnitType', trim( stripcslashes( $arrValues['is_unit_type'] ) ) ); elseif( isset( $arrValues['is_unit_type'] ) ) $this->setIsUnitType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_unit_type'] ) : $arrValues['is_unit_type'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setPlannedStartDate( $strPlannedStartDate ) {
		$this->set( 'm_strPlannedStartDate', CStrings::strTrimDef( $strPlannedStartDate, -1, NULL, true ) );
	}

	public function getPlannedStartDate() {
		return $this->m_strPlannedStartDate;
	}

	public function sqlPlannedStartDate() {
		return ( true == isset( $this->m_strPlannedStartDate ) ) ? '\'' . $this->m_strPlannedStartDate . '\'' : 'NOW()';
	}

	public function setPlannedCompletionDate( $strPlannedCompletionDate ) {
		$this->set( 'm_strPlannedCompletionDate', CStrings::strTrimDef( $strPlannedCompletionDate, -1, NULL, true ) );
	}

	public function getPlannedCompletionDate() {
		return $this->m_strPlannedCompletionDate;
	}

	public function sqlPlannedCompletionDate() {
		return ( true == isset( $this->m_strPlannedCompletionDate ) ) ? '\'' . $this->m_strPlannedCompletionDate . '\'' : 'NOW()';
	}

	public function setActualStartDate( $strActualStartDate ) {
		$this->set( 'm_strActualStartDate', CStrings::strTrimDef( $strActualStartDate, -1, NULL, true ) );
	}

	public function getActualStartDate() {
		return $this->m_strActualStartDate;
	}

	public function sqlActualStartDate() {
		return ( true == isset( $this->m_strActualStartDate ) ) ? '\'' . $this->m_strActualStartDate . '\'' : 'NULL';
	}

	public function setActualCompletionDate( $strActualCompletionDate ) {
		$this->set( 'm_strActualCompletionDate', CStrings::strTrimDef( $strActualCompletionDate, -1, NULL, true ) );
	}

	public function getActualCompletionDate() {
		return $this->m_strActualCompletionDate;
	}

	public function sqlActualCompletionDate() {
		return ( true == isset( $this->m_strActualCompletionDate ) ) ? '\'' . $this->m_strActualCompletionDate . '\'' : 'NULL';
	}

	public function setIsUnitType( $boolIsUnitType ) {
		$this->set( 'm_boolIsUnitType', CStrings::strToBool( $boolIsUnitType ) );
	}

	public function getIsUnitType() {
		return $this->m_boolIsUnitType;
	}

	public function sqlIsUnitType() {
		return ( true == isset( $this->m_boolIsUnitType ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUnitType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_id, name, planned_start_date, planned_completion_date, actual_start_date, actual_completion_date, is_unit_type, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlJobId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlPlannedStartDate() . ', ' .
 						$this->sqlPlannedCompletionDate() . ', ' .
 						$this->sqlActualStartDate() . ', ' .
 						$this->sqlActualCompletionDate() . ', ' .
 						$this->sqlIsUnitType() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planned_start_date = ' . $this->sqlPlannedStartDate() . ','; } elseif( true == array_key_exists( 'PlannedStartDate', $this->getChangedColumns() ) ) { $strSql .= ' planned_start_date = ' . $this->sqlPlannedStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planned_completion_date = ' . $this->sqlPlannedCompletionDate() . ','; } elseif( true == array_key_exists( 'PlannedCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' planned_completion_date = ' . $this->sqlPlannedCompletionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; } elseif( true == array_key_exists( 'ActualStartDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_completion_date = ' . $this->sqlActualCompletionDate() . ','; } elseif( true == array_key_exists( 'ActualCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_completion_date = ' . $this->sqlActualCompletionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unit_type = ' . $this->sqlIsUnitType() . ','; } elseif( true == array_key_exists( 'IsUnitType', $this->getChangedColumns() ) ) { $strSql .= ' is_unit_type = ' . $this->sqlIsUnitType() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_id' => $this->getJobId(),
			'name' => $this->getName(),
			'planned_start_date' => $this->getPlannedStartDate(),
			'planned_completion_date' => $this->getPlannedCompletionDate(),
			'actual_start_date' => $this->getActualStartDate(),
			'actual_completion_date' => $this->getActualCompletionDate(),
			'is_unit_type' => $this->getIsUnitType(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>