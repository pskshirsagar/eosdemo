<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExpenseTypes
 * Do not add any new functions to this class.
 */

class CBaseExpenseTypes extends CEosPluralBase {

	/**
	 * @return CExpenseType[]
	 */
	public static function fetchExpenseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CExpenseType::class, $objDatabase );
	}

	/**
	 * @return CExpenseType
	 */
	public static function fetchExpenseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CExpenseType::class, $objDatabase );
	}

	public static function fetchExpenseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'expense_types', $objDatabase );
	}

	public static function fetchExpenseTypeById( $intId, $objDatabase ) {
		return self::fetchExpenseType( sprintf( 'SELECT * FROM expense_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>