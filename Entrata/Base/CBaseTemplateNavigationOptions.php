<?php

class CBaseTemplateNavigationOptions extends CEosPluralBase {

    public static function fetchTemplateNavigationOptions( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CTemplateNavigationOption', $objDatabase );
    }

    public static function fetchTemplateNavigationOption( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CTemplateNavigationOption', $objDatabase );
    }

    public static function fetchTemplateNavigationOptionCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'template_navigation_options', $objDatabase );
    }

    public static function fetchTemplateNavigationOptionById( $intId, $objDatabase ) {
        return self::fetchTemplateNavigationOption( sprintf( 'SELECT * FROM template_navigation_options WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchTemplateNavigationOptionsByWebsiteTemplateId( $intWebsiteTemplateId, $objDatabase ) {
        return self::fetchTemplateNavigationOptions( sprintf( 'SELECT * FROM template_navigation_options WHERE website_template_id = %d', (int) $intWebsiteTemplateId ), $objDatabase );
    }

    public static function fetchTemplateNavigationOptionsByWebsiteTemplateNavigationId( $intWebsiteTemplateNavigationId, $objDatabase ) {
        return self::fetchTemplateNavigationOptions( sprintf( 'SELECT * FROM template_navigation_options WHERE website_template_navigation_id = %d', (int) $intWebsiteTemplateNavigationId ), $objDatabase );
    }

}
?>