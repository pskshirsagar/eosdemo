<?php

class CBaseMarket extends CEosSingularBase {

	const TABLE_NAME = 'public.markets';

	protected $m_intId;
	protected $m_strZipCode;
	protected $m_strFipsCode;
	protected $m_strMsa;
	protected $m_strMarket;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['zip_code'] ) && $boolDirectSet ) $this->set( 'm_strZipCode', trim( $arrValues['zip_code'] ) ); elseif( isset( $arrValues['zip_code'] ) ) $this->setZipCode( $arrValues['zip_code'] );
		if( isset( $arrValues['fips_code'] ) && $boolDirectSet ) $this->set( 'm_strFipsCode', trim( $arrValues['fips_code'] ) ); elseif( isset( $arrValues['fips_code'] ) ) $this->setFipsCode( $arrValues['fips_code'] );
		if( isset( $arrValues['msa'] ) && $boolDirectSet ) $this->set( 'm_strMsa', trim( $arrValues['msa'] ) ); elseif( isset( $arrValues['msa'] ) ) $this->setMsa( $arrValues['msa'] );
		if( isset( $arrValues['market'] ) && $boolDirectSet ) $this->set( 'm_strMarket', trim( $arrValues['market'] ) ); elseif( isset( $arrValues['market'] ) ) $this->setMarket( $arrValues['market'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setZipCode( $strZipCode ) {
		$this->set( 'm_strZipCode', CStrings::strTrimDef( $strZipCode, 20, NULL, true ) );
	}

	public function getZipCode() {
		return $this->m_strZipCode;
	}

	public function sqlZipCode() {
		return ( true == isset( $this->m_strZipCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strZipCode ) : '\'' . addslashes( $this->m_strZipCode ) . '\'' ) : 'NULL';
	}

	public function setFipsCode( $strFipsCode ) {
		$this->set( 'm_strFipsCode', CStrings::strTrimDef( $strFipsCode, 20, NULL, true ) );
	}

	public function getFipsCode() {
		return $this->m_strFipsCode;
	}

	public function sqlFipsCode() {
		return ( true == isset( $this->m_strFipsCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFipsCode ) : '\'' . addslashes( $this->m_strFipsCode ) . '\'' ) : 'NULL';
	}

	public function setMsa( $strMsa ) {
		$this->set( 'm_strMsa', CStrings::strTrimDef( $strMsa, 255, NULL, true ) );
	}

	public function getMsa() {
		return $this->m_strMsa;
	}

	public function sqlMsa() {
		return ( true == isset( $this->m_strMsa ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMsa ) : '\'' . addslashes( $this->m_strMsa ) . '\'' ) : 'NULL';
	}

	public function setMarket( $strMarket ) {
		$this->set( 'm_strMarket', CStrings::strTrimDef( $strMarket, 255, NULL, true ) );
	}

	public function getMarket() {
		return $this->m_strMarket;
	}

	public function sqlMarket() {
		return ( true == isset( $this->m_strMarket ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarket ) : '\'' . addslashes( $this->m_strMarket ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, zip_code, fips_code, msa, market, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlZipCode() . ', ' .
						$this->sqlFipsCode() . ', ' .
						$this->sqlMsa() . ', ' .
						$this->sqlMarket() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zip_code = ' . $this->sqlZipCode(). ',' ; } elseif( true == array_key_exists( 'ZipCode', $this->getChangedColumns() ) ) { $strSql .= ' zip_code = ' . $this->sqlZipCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fips_code = ' . $this->sqlFipsCode(). ',' ; } elseif( true == array_key_exists( 'FipsCode', $this->getChangedColumns() ) ) { $strSql .= ' fips_code = ' . $this->sqlFipsCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' msa = ' . $this->sqlMsa(). ',' ; } elseif( true == array_key_exists( 'Msa', $this->getChangedColumns() ) ) { $strSql .= ' msa = ' . $this->sqlMsa() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market = ' . $this->sqlMarket(). ',' ; } elseif( true == array_key_exists( 'Market', $this->getChangedColumns() ) ) { $strSql .= ' market = ' . $this->sqlMarket() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'zip_code' => $this->getZipCode(),
			'fips_code' => $this->getFipsCode(),
			'msa' => $this->getMsa(),
			'market' => $this->getMarket(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>