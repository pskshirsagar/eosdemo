<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableOptions
 * Do not add any new functions to this class.
 */

class CBaseComparableOptions extends CEosPluralBase {

	/**
	 * @return CComparableOption[]
	 */
	public static function fetchComparableOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CComparableOption::class, $objDatabase );
	}

	/**
	 * @return CComparableOption
	 */
	public static function fetchComparableOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CComparableOption::class, $objDatabase );
	}

	public static function fetchComparableOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'comparable_options', $objDatabase );
	}

	public static function fetchComparableOptionById( $intId, $objDatabase ) {
		return self::fetchComparableOption( sprintf( 'SELECT * FROM comparable_options WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchComparableOptionsByDefaultAmenityId( $intDefaultAmenityId, $objDatabase ) {
		return self::fetchComparableOptions( sprintf( 'SELECT * FROM comparable_options WHERE default_amenity_id = %d', $intDefaultAmenityId ), $objDatabase );
	}

	public static function fetchComparableOptionsByUtilityTypeId( $intUtilityTypeId, $objDatabase ) {
		return self::fetchComparableOptions( sprintf( 'SELECT * FROM comparable_options WHERE utility_type_id = %d', $intUtilityTypeId ), $objDatabase );
	}

}
?>