<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCustomerRelationships
 * Do not add any new functions to this class.
 */

class CBaseDefaultCustomerRelationships extends CEosPluralBase {

	/**
	 * @return CDefaultCustomerRelationship[]
	 */
	public static function fetchDefaultCustomerRelationships( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCustomerRelationship::class, $objDatabase );
	}

	/**
	 * @return CDefaultCustomerRelationship
	 */
	public static function fetchDefaultCustomerRelationship( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCustomerRelationship::class, $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_customer_relationships', $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipById( $intId, $objDatabase ) {
		return self::fetchDefaultCustomerRelationship( sprintf( 'SELECT * FROM default_customer_relationships WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipsByDefaultCustomerRelationshipGroupId( $intDefaultCustomerRelationshipGroupId, $objDatabase ) {
		return self::fetchDefaultCustomerRelationships( sprintf( 'SELECT * FROM default_customer_relationships WHERE default_customer_relationship_group_id = %d', $intDefaultCustomerRelationshipGroupId ), $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipsByCustomerTypeId( $intCustomerTypeId, $objDatabase ) {
		return self::fetchDefaultCustomerRelationships( sprintf( 'SELECT * FROM default_customer_relationships WHERE customer_type_id = %d', $intCustomerTypeId ), $objDatabase );
	}

}
?>