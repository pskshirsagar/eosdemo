<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultRoommateInterests
 * Do not add any new functions to this class.
 */

class CBaseDefaultRoommateInterests extends CEosPluralBase {

	/**
	 * @return CDefaultRoommateInterest[]
	 */
	public static function fetchDefaultRoommateInterests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultRoommateInterest::class, $objDatabase );
	}

	/**
	 * @return CDefaultRoommateInterest
	 */
	public static function fetchDefaultRoommateInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultRoommateInterest::class, $objDatabase );
	}

	public static function fetchDefaultRoommateInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_roommate_interests', $objDatabase );
	}

	public static function fetchDefaultRoommateInterestById( $intId, $objDatabase ) {
		return self::fetchDefaultRoommateInterest( sprintf( 'SELECT * FROM default_roommate_interests WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultRoommateInterestsByInputTypeId( $intInputTypeId, $objDatabase ) {
		return self::fetchDefaultRoommateInterests( sprintf( 'SELECT * FROM default_roommate_interests WHERE input_type_id = %d', $intInputTypeId ), $objDatabase );
	}

	public static function fetchDefaultRoommateInterestsByRoommateInterestCategoryId( $intRoommateInterestCategoryId, $objDatabase ) {
		return self::fetchDefaultRoommateInterests( sprintf( 'SELECT * FROM default_roommate_interests WHERE roommate_interest_category_id = %d', $intRoommateInterestCategoryId ), $objDatabase );
	}

}
?>