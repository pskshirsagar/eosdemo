<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerContact extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_contacts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intCustomerContactTypeId;
	protected $m_intAddressTypeId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strCompanyName;
	protected $m_strPhoneNumber;
	protected $m_strMobileNumber;
	protected $m_strFaxNumber;
	protected $m_strEmailAddress;
	protected $m_strRelationship;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strDependentLocality;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_fltAmountOwed;
	protected $m_fltMonthlyPaymentAmount;
	protected $m_intResidencyMonths;
	protected $m_intAge;
	protected $m_intHasAccessToUnit;
	protected $m_strNotes;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intHasAccessToUnit = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strStreetLine2' => 'addressLine2',
				'm_strStreetLine3' => 'addressLine3',
				'm_strCity' => 'locality',
				'm_strDependentLocality' => 'dependentlocality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerContactTypeId', trim( $arrValues['customer_contact_type_id'] ) ); elseif( isset( $arrValues['customer_contact_type_id'] ) ) $this->setCustomerContactTypeId( $arrValues['customer_contact_type_id'] );
		if( isset( $arrValues['address_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddressTypeId', trim( $arrValues['address_type_id'] ) ); elseif( isset( $arrValues['address_type_id'] ) ) $this->setAddressTypeId( $arrValues['address_type_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( $arrValues['mobile_number'] ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( $arrValues['mobile_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['relationship'] ) && $boolDirectSet ) $this->set( 'm_strRelationship', trim( $arrValues['relationship'] ) ); elseif( isset( $arrValues['relationship'] ) ) $this->setRelationship( $arrValues['relationship'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( $arrValues['street_line1'] ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine2', trim( $arrValues['street_line2'] ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine3', trim( $arrValues['street_line3'] ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['district'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDependentLocality', trim( $arrValues['district'] ) ); elseif( isset( $arrValues['district'] ) ) $this->setDistrict( $arrValues['district'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( $arrValues['province'] ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['amount_owed'] ) && $boolDirectSet ) $this->set( 'm_fltAmountOwed', trim( $arrValues['amount_owed'] ) ); elseif( isset( $arrValues['amount_owed'] ) ) $this->setAmountOwed( $arrValues['amount_owed'] );
		if( isset( $arrValues['monthly_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyPaymentAmount', trim( $arrValues['monthly_payment_amount'] ) ); elseif( isset( $arrValues['monthly_payment_amount'] ) ) $this->setMonthlyPaymentAmount( $arrValues['monthly_payment_amount'] );
		if( isset( $arrValues['residency_months'] ) && $boolDirectSet ) $this->set( 'm_intResidencyMonths', trim( $arrValues['residency_months'] ) ); elseif( isset( $arrValues['residency_months'] ) ) $this->setResidencyMonths( $arrValues['residency_months'] );
		if( isset( $arrValues['age'] ) && $boolDirectSet ) $this->set( 'm_intAge', trim( $arrValues['age'] ) ); elseif( isset( $arrValues['age'] ) ) $this->setAge( $arrValues['age'] );
		if( isset( $arrValues['has_access_to_unit'] ) && $boolDirectSet ) $this->set( 'm_intHasAccessToUnit', trim( $arrValues['has_access_to_unit'] ) ); elseif( isset( $arrValues['has_access_to_unit'] ) ) $this->setHasAccessToUnit( $arrValues['has_access_to_unit'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerContactTypeId( $intCustomerContactTypeId ) {
		$this->set( 'm_intCustomerContactTypeId', CStrings::strToIntDef( $intCustomerContactTypeId, NULL, false ) );
	}

	public function getCustomerContactTypeId() {
		return $this->m_intCustomerContactTypeId;
	}

	public function sqlCustomerContactTypeId() {
		return ( true == isset( $this->m_intCustomerContactTypeId ) ) ? ( string ) $this->m_intCustomerContactTypeId : 'NULL';
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->set( 'm_intAddressTypeId', CStrings::strToIntDef( $intAddressTypeId, NULL, false ) );
	}

	public function getAddressTypeId() {
		return $this->m_intAddressTypeId;
	}

	public function sqlAddressTypeId() {
		return ( true == isset( $this->m_intAddressTypeId ) ) ? ( string ) $this->m_intAddressTypeId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMobileNumber ) : '\'' . addslashes( $this->m_strMobileNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setRelationship( $strRelationship ) {
		$this->set( 'm_strRelationship', CStrings::strTrimDef( $strRelationship, 240, NULL, true ) );
	}

	public function getRelationship() {
		return $this->m_strRelationship;
	}

	public function sqlRelationship() {
		return ( true == isset( $this->m_strRelationship ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRelationship ) : '\'' . addslashes( $this->m_strRelationship ) . '\'' ) : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine1 ) : '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strStreetLine2, $strAddressKey = 'default', 'm_strStreetLine2' );
	}

	public function getStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strStreetLine2' );
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine2 ) : '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strStreetLine3, $strAddressKey = 'default', 'm_strStreetLine3' );
	}

	public function getStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strStreetLine3' );
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine3 ) : '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setDistrict( $strCity ) {
		$this->setPostalAddressField( 'dependentLocality', $strCity, $strAddressKey = 'default', 'm_strDependentLocality' );
	}

	public function getDistrict() {
		return $this->getPostalAddressField( 'dependentLocality', 'default', 'm_strDependentLocality' );
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProvince ) : '\'' . addslashes( $this->m_strProvince ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setAmountOwed( $fltAmountOwed ) {
		$this->set( 'm_fltAmountOwed', CStrings::strToFloatDef( $fltAmountOwed, NULL, false, 2 ) );
	}

	public function getAmountOwed() {
		return $this->m_fltAmountOwed;
	}

	public function sqlAmountOwed() {
		return ( true == isset( $this->m_fltAmountOwed ) ) ? ( string ) $this->m_fltAmountOwed : 'NULL';
	}

	public function setMonthlyPaymentAmount( $fltMonthlyPaymentAmount ) {
		$this->set( 'm_fltMonthlyPaymentAmount', CStrings::strToFloatDef( $fltMonthlyPaymentAmount, NULL, false, 2 ) );
	}

	public function getMonthlyPaymentAmount() {
		return $this->m_fltMonthlyPaymentAmount;
	}

	public function sqlMonthlyPaymentAmount() {
		return ( true == isset( $this->m_fltMonthlyPaymentAmount ) ) ? ( string ) $this->m_fltMonthlyPaymentAmount : 'NULL';
	}

	public function setResidencyMonths( $intResidencyMonths ) {
		$this->set( 'm_intResidencyMonths', CStrings::strToIntDef( $intResidencyMonths, NULL, false ) );
	}

	public function getResidencyMonths() {
		return $this->m_intResidencyMonths;
	}

	public function sqlResidencyMonths() {
		return ( true == isset( $this->m_intResidencyMonths ) ) ? ( string ) $this->m_intResidencyMonths : 'NULL';
	}

	public function setAge( $intAge ) {
		$this->set( 'm_intAge', CStrings::strToIntDef( $intAge, NULL, false ) );
	}

	public function getAge() {
		return $this->m_intAge;
	}

	public function sqlAge() {
		return ( true == isset( $this->m_intAge ) ) ? ( string ) $this->m_intAge : 'NULL';
	}

	public function setHasAccessToUnit( $intHasAccessToUnit ) {
		$this->set( 'm_intHasAccessToUnit', CStrings::strToIntDef( $intHasAccessToUnit, NULL, false ) );
	}

	public function getHasAccessToUnit() {
		return $this->m_intHasAccessToUnit;
	}

	public function sqlHasAccessToUnit() {
		return ( true == isset( $this->m_intHasAccessToUnit ) ) ? ( string ) $this->m_intHasAccessToUnit : '0';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, customer_contact_type_id, address_type_id, name_first, name_last, company_name, phone_number, mobile_number, fax_number, email_address, relationship, street_line1, street_line2, street_line3, city, state_code, province, postal_code, country_code, amount_owed, monthly_payment_amount, residency_months, age, has_access_to_unit, notes, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerContactTypeId() . ', ' .
						$this->sqlAddressTypeId() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlRelationship() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlStreetLine2() . ', ' .
						$this->sqlStreetLine3() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlAmountOwed() . ', ' .
						$this->sqlMonthlyPaymentAmount() . ', ' .
						$this->sqlResidencyMonths() . ', ' .
						$this->sqlAge() . ', ' .
						$this->sqlHasAccessToUnit() . ', ' .
						$this->sqlNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_contact_type_id = ' . $this->sqlCustomerContactTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_contact_type_id = ' . $this->sqlCustomerContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId(). ',' ; } elseif( true == array_key_exists( 'AddressTypeId', $this->getChangedColumns() ) ) { $strSql .= ' address_type_id = ' . $this->sqlAddressTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber(). ',' ; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relationship = ' . $this->sqlRelationship(). ',' ; } elseif( true == array_key_exists( 'Relationship', $this->getChangedColumns() ) ) { $strSql .= ' relationship = ' . $this->sqlRelationship() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2(). ',' ; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3(). ',' ; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_owed = ' . $this->sqlAmountOwed(). ',' ; } elseif( true == array_key_exists( 'AmountOwed', $this->getChangedColumns() ) ) { $strSql .= ' amount_owed = ' . $this->sqlAmountOwed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_payment_amount = ' . $this->sqlMonthlyPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residency_months = ' . $this->sqlResidencyMonths(). ',' ; } elseif( true == array_key_exists( 'ResidencyMonths', $this->getChangedColumns() ) ) { $strSql .= ' residency_months = ' . $this->sqlResidencyMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' age = ' . $this->sqlAge(). ',' ; } elseif( true == array_key_exists( 'Age', $this->getChangedColumns() ) ) { $strSql .= ' age = ' . $this->sqlAge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_access_to_unit = ' . $this->sqlHasAccessToUnit(). ',' ; } elseif( true == array_key_exists( 'HasAccessToUnit', $this->getChangedColumns() ) ) { $strSql .= ' has_access_to_unit = ' . $this->sqlHasAccessToUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'customer_contact_type_id' => $this->getCustomerContactTypeId(),
			'address_type_id' => $this->getAddressTypeId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'company_name' => $this->getCompanyName(),
			'phone_number' => $this->getPhoneNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'fax_number' => $this->getFaxNumber(),
			'email_address' => $this->getEmailAddress(),
			'relationship' => $this->getRelationship(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'district' => $this->getDistrict(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'amount_owed' => $this->getAmountOwed(),
			'monthly_payment_amount' => $this->getMonthlyPaymentAmount(),
			'residency_months' => $this->getResidencyMonths(),
			'age' => $this->getAge(),
			'has_access_to_unit' => $this->getHasAccessToUnit(),
			'notes' => $this->getNotes(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>