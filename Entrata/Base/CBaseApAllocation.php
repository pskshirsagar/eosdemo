<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApAllocation extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_allocations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intGlTransactionTypeId;
	protected $m_intOriginApAllocationId;
	protected $m_intChargeApDetailId;
	protected $m_intCreditApDetailId;
	protected $m_intApGlAccountId;
	protected $m_intPurchasesClearingGlAccountId;
	protected $m_intPendingReimbursementsGlAccountId;
	protected $m_intInterCoApGlAccountId;
	protected $m_intBankGlAccountId;
	protected $m_intChargeGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intChargeWipGlAccountId;
	protected $m_intCreditWipGlAccountId;
	protected $m_intChargeApTransactionTypeId;
	protected $m_intCreditApTransactionTypeId;
	protected $m_intAccrualDebitGlAccountId;
	protected $m_intAccrualCreditGlAccountId;
	protected $m_intCashDebitGlAccountId;
	protected $m_intCashCreditGlAccountId;
	protected $m_intLumpApHeaderId;
	protected $m_intPeriodId;
	protected $m_strAllocationDatetime;
	protected $m_fltAllocationAmount;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_boolIsDeleted;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDeleted = false;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['origin_ap_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginApAllocationId', trim( $arrValues['origin_ap_allocation_id'] ) ); elseif( isset( $arrValues['origin_ap_allocation_id'] ) ) $this->setOriginApAllocationId( $arrValues['origin_ap_allocation_id'] );
		if( isset( $arrValues['charge_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeApDetailId', trim( $arrValues['charge_ap_detail_id'] ) ); elseif( isset( $arrValues['charge_ap_detail_id'] ) ) $this->setChargeApDetailId( $arrValues['charge_ap_detail_id'] );
		if( isset( $arrValues['credit_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditApDetailId', trim( $arrValues['credit_ap_detail_id'] ) ); elseif( isset( $arrValues['credit_ap_detail_id'] ) ) $this->setCreditApDetailId( $arrValues['credit_ap_detail_id'] );
		if( isset( $arrValues['ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApGlAccountId', trim( $arrValues['ap_gl_account_id'] ) ); elseif( isset( $arrValues['ap_gl_account_id'] ) ) $this->setApGlAccountId( $arrValues['ap_gl_account_id'] );
		if( isset( $arrValues['purchases_clearing_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchasesClearingGlAccountId', trim( $arrValues['purchases_clearing_gl_account_id'] ) ); elseif( isset( $arrValues['purchases_clearing_gl_account_id'] ) ) $this->setPurchasesClearingGlAccountId( $arrValues['purchases_clearing_gl_account_id'] );
		if( isset( $arrValues['pending_reimbursements_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPendingReimbursementsGlAccountId', trim( $arrValues['pending_reimbursements_gl_account_id'] ) ); elseif( isset( $arrValues['pending_reimbursements_gl_account_id'] ) ) $this->setPendingReimbursementsGlAccountId( $arrValues['pending_reimbursements_gl_account_id'] );
		if( isset( $arrValues['inter_co_ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoApGlAccountId', trim( $arrValues['inter_co_ap_gl_account_id'] ) ); elseif( isset( $arrValues['inter_co_ap_gl_account_id'] ) ) $this->setInterCoApGlAccountId( $arrValues['inter_co_ap_gl_account_id'] );
		if( isset( $arrValues['bank_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankGlAccountId', trim( $arrValues['bank_gl_account_id'] ) ); elseif( isset( $arrValues['bank_gl_account_id'] ) ) $this->setBankGlAccountId( $arrValues['bank_gl_account_id'] );
		if( isset( $arrValues['charge_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeGlAccountId', trim( $arrValues['charge_gl_account_id'] ) ); elseif( isset( $arrValues['charge_gl_account_id'] ) ) $this->setChargeGlAccountId( $arrValues['charge_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['charge_wip_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeWipGlAccountId', trim( $arrValues['charge_wip_gl_account_id'] ) ); elseif( isset( $arrValues['charge_wip_gl_account_id'] ) ) $this->setChargeWipGlAccountId( $arrValues['charge_wip_gl_account_id'] );
		if( isset( $arrValues['credit_wip_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditWipGlAccountId', trim( $arrValues['credit_wip_gl_account_id'] ) ); elseif( isset( $arrValues['credit_wip_gl_account_id'] ) ) $this->setCreditWipGlAccountId( $arrValues['credit_wip_gl_account_id'] );
		if( isset( $arrValues['charge_ap_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeApTransactionTypeId', trim( $arrValues['charge_ap_transaction_type_id'] ) ); elseif( isset( $arrValues['charge_ap_transaction_type_id'] ) ) $this->setChargeApTransactionTypeId( $arrValues['charge_ap_transaction_type_id'] );
		if( isset( $arrValues['credit_ap_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditApTransactionTypeId', trim( $arrValues['credit_ap_transaction_type_id'] ) ); elseif( isset( $arrValues['credit_ap_transaction_type_id'] ) ) $this->setCreditApTransactionTypeId( $arrValues['credit_ap_transaction_type_id'] );
		if( isset( $arrValues['accrual_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualDebitGlAccountId', trim( $arrValues['accrual_debit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_debit_gl_account_id'] ) ) $this->setAccrualDebitGlAccountId( $arrValues['accrual_debit_gl_account_id'] );
		if( isset( $arrValues['accrual_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualCreditGlAccountId', trim( $arrValues['accrual_credit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_credit_gl_account_id'] ) ) $this->setAccrualCreditGlAccountId( $arrValues['accrual_credit_gl_account_id'] );
		if( isset( $arrValues['cash_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashDebitGlAccountId', trim( $arrValues['cash_debit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_debit_gl_account_id'] ) ) $this->setCashDebitGlAccountId( $arrValues['cash_debit_gl_account_id'] );
		if( isset( $arrValues['cash_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashCreditGlAccountId', trim( $arrValues['cash_credit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_credit_gl_account_id'] ) ) $this->setCashCreditGlAccountId( $arrValues['cash_credit_gl_account_id'] );
		if( isset( $arrValues['lump_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intLumpApHeaderId', trim( $arrValues['lump_ap_header_id'] ) ); elseif( isset( $arrValues['lump_ap_header_id'] ) ) $this->setLumpApHeaderId( $arrValues['lump_ap_header_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['allocation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAllocationDatetime', trim( $arrValues['allocation_datetime'] ) ); elseif( isset( $arrValues['allocation_datetime'] ) ) $this->setAllocationDatetime( $arrValues['allocation_datetime'] );
		if( isset( $arrValues['allocation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAllocationAmount', trim( $arrValues['allocation_amount'] ) ); elseif( isset( $arrValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrValues['allocation_amount'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setOriginApAllocationId( $intOriginApAllocationId ) {
		$this->set( 'm_intOriginApAllocationId', CStrings::strToIntDef( $intOriginApAllocationId, NULL, false ) );
	}

	public function getOriginApAllocationId() {
		return $this->m_intOriginApAllocationId;
	}

	public function sqlOriginApAllocationId() {
		return ( true == isset( $this->m_intOriginApAllocationId ) ) ? ( string ) $this->m_intOriginApAllocationId : 'NULL';
	}

	public function setChargeApDetailId( $intChargeApDetailId ) {
		$this->set( 'm_intChargeApDetailId', CStrings::strToIntDef( $intChargeApDetailId, NULL, false ) );
	}

	public function getChargeApDetailId() {
		return $this->m_intChargeApDetailId;
	}

	public function sqlChargeApDetailId() {
		return ( true == isset( $this->m_intChargeApDetailId ) ) ? ( string ) $this->m_intChargeApDetailId : 'NULL';
	}

	public function setCreditApDetailId( $intCreditApDetailId ) {
		$this->set( 'm_intCreditApDetailId', CStrings::strToIntDef( $intCreditApDetailId, NULL, false ) );
	}

	public function getCreditApDetailId() {
		return $this->m_intCreditApDetailId;
	}

	public function sqlCreditApDetailId() {
		return ( true == isset( $this->m_intCreditApDetailId ) ) ? ( string ) $this->m_intCreditApDetailId : 'NULL';
	}

	public function setApGlAccountId( $intApGlAccountId ) {
		$this->set( 'm_intApGlAccountId', CStrings::strToIntDef( $intApGlAccountId, NULL, false ) );
	}

	public function getApGlAccountId() {
		return $this->m_intApGlAccountId;
	}

	public function sqlApGlAccountId() {
		return ( true == isset( $this->m_intApGlAccountId ) ) ? ( string ) $this->m_intApGlAccountId : 'NULL';
	}

	public function setPurchasesClearingGlAccountId( $intPurchasesClearingGlAccountId ) {
		$this->set( 'm_intPurchasesClearingGlAccountId', CStrings::strToIntDef( $intPurchasesClearingGlAccountId, NULL, false ) );
	}

	public function getPurchasesClearingGlAccountId() {
		return $this->m_intPurchasesClearingGlAccountId;
	}

	public function sqlPurchasesClearingGlAccountId() {
		return ( true == isset( $this->m_intPurchasesClearingGlAccountId ) ) ? ( string ) $this->m_intPurchasesClearingGlAccountId : 'NULL';
	}

	public function setPendingReimbursementsGlAccountId( $intPendingReimbursementsGlAccountId ) {
		$this->set( 'm_intPendingReimbursementsGlAccountId', CStrings::strToIntDef( $intPendingReimbursementsGlAccountId, NULL, false ) );
	}

	public function getPendingReimbursementsGlAccountId() {
		return $this->m_intPendingReimbursementsGlAccountId;
	}

	public function sqlPendingReimbursementsGlAccountId() {
		return ( true == isset( $this->m_intPendingReimbursementsGlAccountId ) ) ? ( string ) $this->m_intPendingReimbursementsGlAccountId : 'NULL';
	}

	public function setInterCoApGlAccountId( $intInterCoApGlAccountId ) {
		$this->set( 'm_intInterCoApGlAccountId', CStrings::strToIntDef( $intInterCoApGlAccountId, NULL, false ) );
	}

	public function getInterCoApGlAccountId() {
		return $this->m_intInterCoApGlAccountId;
	}

	public function sqlInterCoApGlAccountId() {
		return ( true == isset( $this->m_intInterCoApGlAccountId ) ) ? ( string ) $this->m_intInterCoApGlAccountId : 'NULL';
	}

	public function setBankGlAccountId( $intBankGlAccountId ) {
		$this->set( 'm_intBankGlAccountId', CStrings::strToIntDef( $intBankGlAccountId, NULL, false ) );
	}

	public function getBankGlAccountId() {
		return $this->m_intBankGlAccountId;
	}

	public function sqlBankGlAccountId() {
		return ( true == isset( $this->m_intBankGlAccountId ) ) ? ( string ) $this->m_intBankGlAccountId : 'NULL';
	}

	public function setChargeGlAccountId( $intChargeGlAccountId ) {
		$this->set( 'm_intChargeGlAccountId', CStrings::strToIntDef( $intChargeGlAccountId, NULL, false ) );
	}

	public function getChargeGlAccountId() {
		return $this->m_intChargeGlAccountId;
	}

	public function sqlChargeGlAccountId() {
		return ( true == isset( $this->m_intChargeGlAccountId ) ) ? ( string ) $this->m_intChargeGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setChargeWipGlAccountId( $intChargeWipGlAccountId ) {
		$this->set( 'm_intChargeWipGlAccountId', CStrings::strToIntDef( $intChargeWipGlAccountId, NULL, false ) );
	}

	public function getChargeWipGlAccountId() {
		return $this->m_intChargeWipGlAccountId;
	}

	public function sqlChargeWipGlAccountId() {
		return ( true == isset( $this->m_intChargeWipGlAccountId ) ) ? ( string ) $this->m_intChargeWipGlAccountId : 'NULL';
	}

	public function setCreditWipGlAccountId( $intCreditWipGlAccountId ) {
		$this->set( 'm_intCreditWipGlAccountId', CStrings::strToIntDef( $intCreditWipGlAccountId, NULL, false ) );
	}

	public function getCreditWipGlAccountId() {
		return $this->m_intCreditWipGlAccountId;
	}

	public function sqlCreditWipGlAccountId() {
		return ( true == isset( $this->m_intCreditWipGlAccountId ) ) ? ( string ) $this->m_intCreditWipGlAccountId : 'NULL';
	}

	public function setChargeApTransactionTypeId( $intChargeApTransactionTypeId ) {
		$this->set( 'm_intChargeApTransactionTypeId', CStrings::strToIntDef( $intChargeApTransactionTypeId, NULL, false ) );
	}

	public function getChargeApTransactionTypeId() {
		return $this->m_intChargeApTransactionTypeId;
	}

	public function sqlChargeApTransactionTypeId() {
		return ( true == isset( $this->m_intChargeApTransactionTypeId ) ) ? ( string ) $this->m_intChargeApTransactionTypeId : 'NULL';
	}

	public function setCreditApTransactionTypeId( $intCreditApTransactionTypeId ) {
		$this->set( 'm_intCreditApTransactionTypeId', CStrings::strToIntDef( $intCreditApTransactionTypeId, NULL, false ) );
	}

	public function getCreditApTransactionTypeId() {
		return $this->m_intCreditApTransactionTypeId;
	}

	public function sqlCreditApTransactionTypeId() {
		return ( true == isset( $this->m_intCreditApTransactionTypeId ) ) ? ( string ) $this->m_intCreditApTransactionTypeId : 'NULL';
	}

	public function setAccrualDebitGlAccountId( $intAccrualDebitGlAccountId ) {
		$this->set( 'm_intAccrualDebitGlAccountId', CStrings::strToIntDef( $intAccrualDebitGlAccountId, NULL, false ) );
	}

	public function getAccrualDebitGlAccountId() {
		return $this->m_intAccrualDebitGlAccountId;
	}

	public function sqlAccrualDebitGlAccountId() {
		return ( true == isset( $this->m_intAccrualDebitGlAccountId ) ) ? ( string ) $this->m_intAccrualDebitGlAccountId : 'NULL';
	}

	public function setAccrualCreditGlAccountId( $intAccrualCreditGlAccountId ) {
		$this->set( 'm_intAccrualCreditGlAccountId', CStrings::strToIntDef( $intAccrualCreditGlAccountId, NULL, false ) );
	}

	public function getAccrualCreditGlAccountId() {
		return $this->m_intAccrualCreditGlAccountId;
	}

	public function sqlAccrualCreditGlAccountId() {
		return ( true == isset( $this->m_intAccrualCreditGlAccountId ) ) ? ( string ) $this->m_intAccrualCreditGlAccountId : 'NULL';
	}

	public function setCashDebitGlAccountId( $intCashDebitGlAccountId ) {
		$this->set( 'm_intCashDebitGlAccountId', CStrings::strToIntDef( $intCashDebitGlAccountId, NULL, false ) );
	}

	public function getCashDebitGlAccountId() {
		return $this->m_intCashDebitGlAccountId;
	}

	public function sqlCashDebitGlAccountId() {
		return ( true == isset( $this->m_intCashDebitGlAccountId ) ) ? ( string ) $this->m_intCashDebitGlAccountId : 'NULL';
	}

	public function setCashCreditGlAccountId( $intCashCreditGlAccountId ) {
		$this->set( 'm_intCashCreditGlAccountId', CStrings::strToIntDef( $intCashCreditGlAccountId, NULL, false ) );
	}

	public function getCashCreditGlAccountId() {
		return $this->m_intCashCreditGlAccountId;
	}

	public function sqlCashCreditGlAccountId() {
		return ( true == isset( $this->m_intCashCreditGlAccountId ) ) ? ( string ) $this->m_intCashCreditGlAccountId : 'NULL';
	}

	public function setLumpApHeaderId( $intLumpApHeaderId ) {
		$this->set( 'm_intLumpApHeaderId', CStrings::strToIntDef( $intLumpApHeaderId, NULL, false ) );
	}

	public function getLumpApHeaderId() {
		return $this->m_intLumpApHeaderId;
	}

	public function sqlLumpApHeaderId() {
		return ( true == isset( $this->m_intLumpApHeaderId ) ) ? ( string ) $this->m_intLumpApHeaderId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setAllocationDatetime( $strAllocationDatetime ) {
		$this->set( 'm_strAllocationDatetime', CStrings::strTrimDef( $strAllocationDatetime, -1, NULL, true ) );
	}

	public function getAllocationDatetime() {
		return $this->m_strAllocationDatetime;
	}

	public function sqlAllocationDatetime() {
		return ( true == isset( $this->m_strAllocationDatetime ) ) ? '\'' . $this->m_strAllocationDatetime . '\'' : 'NOW()';
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$this->set( 'm_fltAllocationAmount', CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 2 ) );
	}

	public function getAllocationAmount() {
		return $this->m_fltAllocationAmount;
	}

	public function sqlAllocationAmount() {
		return ( true == isset( $this->m_fltAllocationAmount ) ) ? ( string ) $this->m_fltAllocationAmount : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, gl_transaction_type_id, origin_ap_allocation_id, charge_ap_detail_id, credit_ap_detail_id, ap_gl_account_id, purchases_clearing_gl_account_id, pending_reimbursements_gl_account_id, inter_co_ap_gl_account_id, bank_gl_account_id, charge_gl_account_id, credit_gl_account_id, charge_wip_gl_account_id, credit_wip_gl_account_id, charge_ap_transaction_type_id, credit_ap_transaction_type_id, accrual_debit_gl_account_id, accrual_credit_gl_account_id, cash_debit_gl_account_id, cash_credit_gl_account_id, lump_ap_header_id, period_id, allocation_datetime, allocation_amount, post_month, post_date, is_deleted, is_posted, is_initial_import, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlOriginApAllocationId() . ', ' .
						$this->sqlChargeApDetailId() . ', ' .
						$this->sqlCreditApDetailId() . ', ' .
						$this->sqlApGlAccountId() . ', ' .
						$this->sqlPurchasesClearingGlAccountId() . ', ' .
						$this->sqlPendingReimbursementsGlAccountId() . ', ' .
						$this->sqlInterCoApGlAccountId() . ', ' .
						$this->sqlBankGlAccountId() . ', ' .
						$this->sqlChargeGlAccountId() . ', ' .
						$this->sqlCreditGlAccountId() . ', ' .
						$this->sqlChargeWipGlAccountId() . ', ' .
						$this->sqlCreditWipGlAccountId() . ', ' .
						$this->sqlChargeApTransactionTypeId() . ', ' .
						$this->sqlCreditApTransactionTypeId() . ', ' .
						$this->sqlAccrualDebitGlAccountId() . ', ' .
						$this->sqlAccrualCreditGlAccountId() . ', ' .
						$this->sqlCashDebitGlAccountId() . ', ' .
						$this->sqlCashCreditGlAccountId() . ', ' .
						$this->sqlLumpApHeaderId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlAllocationDatetime() . ', ' .
						$this->sqlAllocationAmount() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						$this->sqlIsPosted() . ', ' .
						$this->sqlIsInitialImport() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_ap_allocation_id = ' . $this->sqlOriginApAllocationId(). ',' ; } elseif( true == array_key_exists( 'OriginApAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' origin_ap_allocation_id = ' . $this->sqlOriginApAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_ap_detail_id = ' . $this->sqlChargeApDetailId(). ',' ; } elseif( true == array_key_exists( 'ChargeApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' charge_ap_detail_id = ' . $this->sqlChargeApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_ap_detail_id = ' . $this->sqlCreditApDetailId(). ',' ; } elseif( true == array_key_exists( 'CreditApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' credit_ap_detail_id = ' . $this->sqlCreditApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PurchasesClearingGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PendingReimbursementsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCoApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_gl_account_id = ' . $this->sqlBankGlAccountId(). ',' ; } elseif( true == array_key_exists( 'BankGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_gl_account_id = ' . $this->sqlBankGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_gl_account_id = ' . $this->sqlChargeGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ChargeGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' charge_gl_account_id = ' . $this->sqlChargeGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_gl_account_id = ' . $this->sqlCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_wip_gl_account_id = ' . $this->sqlChargeWipGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ChargeWipGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' charge_wip_gl_account_id = ' . $this->sqlChargeWipGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_wip_gl_account_id = ' . $this->sqlCreditWipGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CreditWipGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_wip_gl_account_id = ' . $this->sqlCreditWipGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_ap_transaction_type_id = ' . $this->sqlChargeApTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'ChargeApTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_ap_transaction_type_id = ' . $this->sqlChargeApTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_ap_transaction_type_id = ' . $this->sqlCreditApTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'CreditApTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' credit_ap_transaction_type_id = ' . $this->sqlCreditApTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lump_ap_header_id = ' . $this->sqlLumpApHeaderId(). ',' ; } elseif( true == array_key_exists( 'LumpApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' lump_ap_header_id = ' . $this->sqlLumpApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime(). ',' ; } elseif( true == array_key_exists( 'AllocationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount(). ',' ; } elseif( true == array_key_exists( 'AllocationAmount', $this->getChangedColumns() ) ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted(). ',' ; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport(). ',' ; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'origin_ap_allocation_id' => $this->getOriginApAllocationId(),
			'charge_ap_detail_id' => $this->getChargeApDetailId(),
			'credit_ap_detail_id' => $this->getCreditApDetailId(),
			'ap_gl_account_id' => $this->getApGlAccountId(),
			'purchases_clearing_gl_account_id' => $this->getPurchasesClearingGlAccountId(),
			'pending_reimbursements_gl_account_id' => $this->getPendingReimbursementsGlAccountId(),
			'inter_co_ap_gl_account_id' => $this->getInterCoApGlAccountId(),
			'bank_gl_account_id' => $this->getBankGlAccountId(),
			'charge_gl_account_id' => $this->getChargeGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'charge_wip_gl_account_id' => $this->getChargeWipGlAccountId(),
			'credit_wip_gl_account_id' => $this->getCreditWipGlAccountId(),
			'charge_ap_transaction_type_id' => $this->getChargeApTransactionTypeId(),
			'credit_ap_transaction_type_id' => $this->getCreditApTransactionTypeId(),
			'accrual_debit_gl_account_id' => $this->getAccrualDebitGlAccountId(),
			'accrual_credit_gl_account_id' => $this->getAccrualCreditGlAccountId(),
			'cash_debit_gl_account_id' => $this->getCashDebitGlAccountId(),
			'cash_credit_gl_account_id' => $this->getCashCreditGlAccountId(),
			'lump_ap_header_id' => $this->getLumpApHeaderId(),
			'period_id' => $this->getPeriodId(),
			'allocation_datetime' => $this->getAllocationDatetime(),
			'allocation_amount' => $this->getAllocationAmount(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'is_deleted' => $this->getIsDeleted(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>