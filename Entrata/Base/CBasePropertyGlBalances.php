<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGlBalances
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGlBalances extends CEosPluralBase {

	/**
	 * @return CPropertyGlBalance[]
	 */
	public static function fetchPropertyGlBalances( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyGlBalance', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGlBalance
	 */
	public static function fetchPropertyGlBalance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyGlBalance', $objDatabase );
	}

	public static function fetchPropertyGlBalanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_gl_balances', $objDatabase );
	}

	public static function fetchPropertyGlBalanceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlBalance( sprintf( 'SELECT * FROM property_gl_balances WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlBalancesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGlBalances( sprintf( 'SELECT * FROM property_gl_balances WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlBalancesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlBalances( sprintf( 'SELECT * FROM property_gl_balances WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlBalancesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlBalances( sprintf( 'SELECT * FROM property_gl_balances WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>