<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplateSlotImage extends CEosSingularBase {

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledEmailId;
	protected $m_intScheduledTaskEmailId;
	protected $m_intCampaignId;
	protected $m_intSystemMessageTemplateId;
	protected $m_intSystemMessageTemplateSlotId;
	protected $m_intCompanyMediaFileId;
	protected $m_strTitle;
	protected $m_strLink;
	protected $m_strLinkText;
	protected $m_strAlt;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->m_intScheduledEmailId = trim( $arrValues['scheduled_email_id'] ); else if( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['scheduled_task_email_id'] ) && $boolDirectSet ) $this->m_intScheduledTaskEmailId = trim( $arrValues['scheduled_task_email_id'] ); else if( isset( $arrValues['scheduled_task_email_id'] ) ) $this->setScheduledTaskEmailId( $arrValues['scheduled_task_email_id'] );
		if( isset( $arrValues['campaign_id'] ) && $boolDirectSet ) $this->m_intCampaignId = trim( $arrValues['campaign_id'] ); else if( isset( $arrValues['campaign_id'] ) ) $this->setCampaignId( $arrValues['campaign_id'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->m_intSystemMessageTemplateId = trim( $arrValues['system_message_template_id'] ); else if( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['system_message_template_slot_id'] ) && $boolDirectSet ) $this->m_intSystemMessageTemplateSlotId = trim( $arrValues['system_message_template_slot_id'] ); else if( isset( $arrValues['system_message_template_slot_id'] ) ) $this->setSystemMessageTemplateSlotId( $arrValues['system_message_template_slot_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->m_intCompanyMediaFileId = trim( $arrValues['company_media_file_id'] ); else if( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->m_strTitle = trim( stripcslashes( $arrValues['title'] ) ); else if( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['link'] ) && $boolDirectSet ) $this->m_strLink = trim( stripcslashes( $arrValues['link'] ) ); else if( isset( $arrValues['link'] ) ) $this->setLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link'] ) : $arrValues['link'] );
		if( isset( $arrValues['link_text'] ) && $boolDirectSet ) $this->m_strLinkText = trim( stripcslashes( $arrValues['link_text'] ) ); else if( isset( $arrValues['link_text'] ) ) $this->setLinkText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_text'] ) : $arrValues['link_text'] );
		if( isset( $arrValues['alt'] ) && $boolDirectSet ) $this->m_strAlt = trim( stripcslashes( $arrValues['alt'] ) ); else if( isset( $arrValues['alt'] ) ) $this->setAlt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alt'] ) : $arrValues['alt'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->m_intScheduledEmailId = CStrings::strToIntDef( $intScheduledEmailId, NULL, false );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setScheduledTaskEmailId( $intScheduledTaskEmailId ) {
		$this->m_intScheduledTaskEmailId = CStrings::strToIntDef( $intScheduledTaskEmailId, NULL, false );
	}

	public function getScheduledTaskEmailId() {
		return $this->m_intScheduledTaskEmailId;
	}

	public function sqlScheduledTaskEmailId() {
		return ( true == isset( $this->m_intScheduledTaskEmailId ) ) ? ( string ) $this->m_intScheduledTaskEmailId : 'NULL';
	}

	public function setCampaignId( $intCampaignId ) {
		$this->m_intCampaignId = CStrings::strToIntDef( $intCampaignId, NULL, false );
	}

	public function getCampaignId() {
		return $this->m_intCampaignId;
	}

	public function sqlCampaignId() {
		return ( true == isset( $this->m_intCampaignId ) ) ? ( string ) $this->m_intCampaignId : 'NULL';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->m_intSystemMessageTemplateId = CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setSystemMessageTemplateSlotId( $intSystemMessageTemplateSlotId ) {
		$this->m_intSystemMessageTemplateSlotId = CStrings::strToIntDef( $intSystemMessageTemplateSlotId, NULL, false );
	}

	public function getSystemMessageTemplateSlotId() {
		return $this->m_intSystemMessageTemplateSlotId;
	}

	public function sqlSystemMessageTemplateSlotId() {
		return ( true == isset( $this->m_intSystemMessageTemplateSlotId ) ) ? ( string ) $this->m_intSystemMessageTemplateSlotId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->m_intCompanyMediaFileId = CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = CStrings::strTrimDef( $strTitle, 50, NULL, true );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setLink( $strLink ) {
		$this->m_strLink = CStrings::strTrimDef( $strLink, 4096, NULL, true );
	}

	public function getLink() {
		return $this->m_strLink;
	}

	public function sqlLink() {
		return ( true == isset( $this->m_strLink ) ) ? '\'' . addslashes( $this->m_strLink ) . '\'' : 'NULL';
	}

	public function setLinkText( $strLinkText ) {
		$this->m_strLinkText = CStrings::strTrimDef( $strLinkText, 100, NULL, true );
	}

	public function getLinkText() {
		return $this->m_strLinkText;
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? '\'' . addslashes( $this->m_strLinkText ) . '\'' : 'NULL';
	}

	public function setAlt( $strAlt ) {
		$this->m_strAlt = CStrings::strTrimDef( $strAlt, 240, NULL, true );
	}

	public function getAlt() {
		return $this->m_strAlt;
	}

	public function sqlAlt() {
		return ( true == isset( $this->m_strAlt ) ) ? '\'' . addslashes( $this->m_strAlt ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.email_template_slot_images_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.email_template_slot_images
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScheduledEmailId() . ', ' .
 						$this->sqlScheduledTaskEmailId() . ', ' .
 						$this->sqlCampaignId() . ', ' .
 						$this->sqlSystemMessageTemplateId() . ', ' .
 						$this->sqlSystemMessageTemplateSlotId() . ', ' .
 						$this->sqlCompanyMediaFileId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlLink() . ', ' .
 						$this->sqlLinkText() . ', ' .
 						$this->sqlAlt() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.email_template_slot_images
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlScheduledEmailId() ) != $this->getOriginalValueByFieldName ( 'scheduled_email_id' ) ) { $arrstrOriginalValueChanges['scheduled_email_id'] = $this->sqlScheduledEmailId(); $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_email_id = ' . $this->sqlScheduledTaskEmailId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlScheduledTaskEmailId() ) != $this->getOriginalValueByFieldName ( 'scheduled_task_email_id' ) ) { $arrstrOriginalValueChanges['scheduled_task_email_id'] = $this->sqlScheduledTaskEmailId(); $strSql .= ' scheduled_task_email_id = ' . $this->sqlScheduledTaskEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' campaign_id = ' . $this->sqlCampaignId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCampaignId() ) != $this->getOriginalValueByFieldName ( 'campaign_id' ) ) { $arrstrOriginalValueChanges['campaign_id'] = $this->sqlCampaignId(); $strSql .= ' campaign_id = ' . $this->sqlCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSystemMessageTemplateId() ) != $this->getOriginalValueByFieldName ( 'system_message_template_id' ) ) { $arrstrOriginalValueChanges['system_message_template_id'] = $this->sqlSystemMessageTemplateId(); $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_slot_id = ' . $this->sqlSystemMessageTemplateSlotId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSystemMessageTemplateSlotId() ) != $this->getOriginalValueByFieldName ( 'system_message_template_slot_id' ) ) { $arrstrOriginalValueChanges['system_message_template_slot_id'] = $this->sqlSystemMessageTemplateSlotId(); $strSql .= ' system_message_template_slot_id = ' . $this->sqlSystemMessageTemplateSlotId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyMediaFileId() ) != $this->getOriginalValueByFieldName ( 'company_media_file_id' ) ) { $arrstrOriginalValueChanges['company_media_file_id'] = $this->sqlCompanyMediaFileId(); $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTitle() ) != $this->getOriginalValueByFieldName ( 'title' ) ) { $arrstrOriginalValueChanges['title'] = $this->sqlTitle(); $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link = ' . $this->sqlLink() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLink() ) != $this->getOriginalValueByFieldName ( 'link' ) ) { $arrstrOriginalValueChanges['link'] = $this->sqlLink(); $strSql .= ' link = ' . $this->sqlLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLinkText() ) != $this->getOriginalValueByFieldName ( 'link_text' ) ) { $arrstrOriginalValueChanges['link_text'] = $this->sqlLinkText(); $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt = ' . $this->sqlAlt() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAlt() ) != $this->getOriginalValueByFieldName ( 'alt' ) ) { $arrstrOriginalValueChanges['alt'] = $this->sqlAlt(); $strSql .= ' alt = ' . $this->sqlAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.email_template_slot_images WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.email_template_slot_images_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'scheduled_task_email_id' => $this->getScheduledTaskEmailId(),
			'campaign_id' => $this->getCampaignId(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'system_message_template_slot_id' => $this->getSystemMessageTemplateSlotId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'title' => $this->getTitle(),
			'link' => $this->getLink(),
			'link_text' => $this->getLinkText(),
			'alt' => $this->getAlt(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>