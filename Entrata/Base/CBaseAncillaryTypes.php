<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryTypes
 * Do not add any new functions to this class.
 */

class CBaseAncillaryTypes extends CEosPluralBase {

	/**
	 * @return CAncillaryType[]
	 */
	public static function fetchAncillaryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAncillaryType::class, $objDatabase );
	}

	/**
	 * @return CAncillaryType
	 */
	public static function fetchAncillaryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAncillaryType::class, $objDatabase );
	}

	public static function fetchAncillaryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ancillary_types', $objDatabase );
	}

	public static function fetchAncillaryTypeById( $intId, $objDatabase ) {
		return self::fetchAncillaryType( sprintf( 'SELECT * FROM ancillary_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>