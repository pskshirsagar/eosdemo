<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDashboardPriorities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDashboardPriorities extends CEosPluralBase {

	/**
	 * @return CDashboardPriority[]
	 */
	public static function fetchDashboardPriorities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDashboardPriority::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDashboardPriority
	 */
	public static function fetchDashboardPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDashboardPriority::class, $objDatabase );
	}

	public static function fetchDashboardPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dashboard_priorities', $objDatabase );
	}

	public static function fetchDashboardPriorityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDashboardPriority( sprintf( 'SELECT * FROM dashboard_priorities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchDashboardPrioritiesByCid( $intCid, $objDatabase ) {
		return self::fetchDashboardPriorities( sprintf( 'SELECT * FROM dashboard_priorities WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>