<?php

class CBaseSubsidyProgramType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.subsidy_program_types';

	protected $m_intId;
	protected $m_intSubsidyTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCode;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['subsidy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTypeId', trim( $arrValues['subsidy_type_id'] ) ); elseif( isset( $arrValues['subsidy_type_id'] ) ) $this->setSubsidyTypeId( $arrValues['subsidy_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['code'] ) && $boolDirectSet ) $this->set( 'm_strCode', trim( $arrValues['code'] ) ); elseif( isset( $arrValues['code'] ) ) $this->setCode( $arrValues['code'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSubsidyTypeId( $intSubsidyTypeId ) {
		$this->set( 'm_intSubsidyTypeId', CStrings::strToIntDef( $intSubsidyTypeId, NULL, false ) );
	}

	public function getSubsidyTypeId() {
		return $this->m_intSubsidyTypeId;
	}

	public function sqlSubsidyTypeId() {
		return ( true == isset( $this->m_intSubsidyTypeId ) ) ? ( string ) $this->m_intSubsidyTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setCode( $strCode ) {
		$this->set( 'm_strCode', CStrings::strTrimDef( $strCode, 1, NULL, true ) );
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function sqlCode() {
		return ( true == isset( $this->m_strCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCode ) : '\'' . addslashes( $this->m_strCode ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDetails( $jsonDetails ) {
		if( true == valObj( $jsonDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDetails', $jsonDetails );
		} elseif( true == valJsonString( $jsonDetails ) ) {
			$this->set( 'm_jsonDetails', CStrings::strToJson( $jsonDetails ) );
		} else {
			$this->set( 'm_jsonDetails', NULL ); 
		}
		unset( $this->m_strDetails );
	}

	public function getDetails() {
		if( true == isset( $this->m_strDetails ) ) {
			$this->m_jsonDetails = CStrings::strToJson( $this->m_strDetails );
			unset( $this->m_strDetails );
		}
		$this->loadUserDefinedFields();
		return $this->m_jsonDetails;
	}

	public function sqlDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'subsidy_type_id' => $this->getSubsidyTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'code' => $this->getCode(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>