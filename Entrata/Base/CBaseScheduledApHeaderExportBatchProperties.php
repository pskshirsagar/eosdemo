<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApHeaderExportBatchProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledApHeaderExportBatchProperties extends CEosPluralBase {

	/**
	 * @return CScheduledApHeaderExportBatchProperty[]
	 */
	public static function fetchScheduledApHeaderExportBatchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledApHeaderExportBatchProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledApHeaderExportBatchProperty
	 */
	public static function fetchScheduledApHeaderExportBatchProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledApHeaderExportBatchProperty', $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_ap_header_export_batch_properties', $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatchProperty( sprintf( 'SELECT * FROM scheduled_ap_header_export_batch_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatchProperties( sprintf( 'SELECT * FROM scheduled_ap_header_export_batch_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertiesByScheduledApHeaderExportBatchIdByCid( $intScheduledApHeaderExportBatchId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatchProperties( sprintf( 'SELECT * FROM scheduled_ap_header_export_batch_properties WHERE scheduled_ap_header_export_batch_id = %d AND cid = %d', ( int ) $intScheduledApHeaderExportBatchId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledApHeaderExportBatchPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScheduledApHeaderExportBatchProperties( sprintf( 'SELECT * FROM scheduled_ap_header_export_batch_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>