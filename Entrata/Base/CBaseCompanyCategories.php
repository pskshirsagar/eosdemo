<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyCategories extends CEosPluralBase {

	/**
	 * @return CCompanyCategory[]
	 */
	public static function fetchCompanyCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyCategory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyCategory
	 */
	public static function fetchCompanyCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyCategory::class, $objDatabase );
	}

	public static function fetchCompanyCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_categories', $objDatabase );
	}

	public static function fetchCompanyCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyCategory( sprintf( 'SELECT * FROM company_categories WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCategories( sprintf( 'SELECT * FROM company_categories WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>