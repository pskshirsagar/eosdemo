<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_application_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicationId;
	protected $m_intApplicationTypeId;
	protected $m_intScreeningVendorId;
	protected $m_intRemoteScreeningId;
	protected $m_intRequestStatusTypeId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intScreeningDecisionTypeId;
	protected $m_arrintScreeningDecisionReasonTypeIds;
	protected $m_intScreeningDecisionUpdatedBy;
	protected $m_strScreeningDecisionUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrstrScreeningConditionCodes;
	protected $m_strErrorDescription;

	public function __construct() {
		parent::__construct();

		$this->m_intApplicationTypeId = '1';
		$this->m_strErrorDescription = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['application_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationTypeId', trim( $arrValues['application_type_id'] ) ); elseif( isset( $arrValues['application_type_id'] ) ) $this->setApplicationTypeId( $arrValues['application_type_id'] );
		if( isset( $arrValues['screening_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningVendorId', trim( $arrValues['screening_vendor_id'] ) ); elseif( isset( $arrValues['screening_vendor_id'] ) ) $this->setScreeningVendorId( $arrValues['screening_vendor_id'] );
		if( isset( $arrValues['remote_screening_id'] ) && $boolDirectSet ) $this->set( 'm_intRemoteScreeningId', trim( $arrValues['remote_screening_id'] ) ); elseif( isset( $arrValues['remote_screening_id'] ) ) $this->setRemoteScreeningId( $arrValues['remote_screening_id'] );
		if( isset( $arrValues['request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRequestStatusTypeId', trim( $arrValues['request_status_type_id'] ) ); elseif( isset( $arrValues['request_status_type_id'] ) ) $this->setRequestStatusTypeId( $arrValues['request_status_type_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['screening_decision_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDecisionTypeId', trim( $arrValues['screening_decision_type_id'] ) ); elseif( isset( $arrValues['screening_decision_type_id'] ) ) $this->setScreeningDecisionTypeId( $arrValues['screening_decision_type_id'] );
		if( isset( $arrValues['screening_decision_reason_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintScreeningDecisionReasonTypeIds', trim( $arrValues['screening_decision_reason_type_ids'] ) ); elseif( isset( $arrValues['screening_decision_reason_type_ids'] ) ) $this->setScreeningDecisionReasonTypeIds( $arrValues['screening_decision_reason_type_ids'] );
		if( isset( $arrValues['screening_decision_updated_by'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDecisionUpdatedBy', trim( $arrValues['screening_decision_updated_by'] ) ); elseif( isset( $arrValues['screening_decision_updated_by'] ) ) $this->setScreeningDecisionUpdatedBy( $arrValues['screening_decision_updated_by'] );
		if( isset( $arrValues['screening_decision_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strScreeningDecisionUpdatedOn', trim( $arrValues['screening_decision_updated_on'] ) ); elseif( isset( $arrValues['screening_decision_updated_on'] ) ) $this->setScreeningDecisionUpdatedOn( $arrValues['screening_decision_updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_condition_codes'] ) && $boolDirectSet ) $this->set( 'm_arrstrScreeningConditionCodes', trim( $arrValues['screening_condition_codes'] ) ); elseif( isset( $arrValues['screening_condition_codes'] ) ) $this->setScreeningConditionCodes( $arrValues['screening_condition_codes'] );
		if( isset( $arrValues['error_description'] ) && $boolDirectSet ) $this->set( 'm_strErrorDescription', trim( $arrValues['error_description'] ) ); elseif( isset( $arrValues['error_description'] ) ) $this->setErrorDescription( $arrValues['error_description'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApplicationTypeId( $intApplicationTypeId ) {
		$this->set( 'm_intApplicationTypeId', CStrings::strToIntDef( $intApplicationTypeId, NULL, false ) );
	}

	public function getApplicationTypeId() {
		return $this->m_intApplicationTypeId;
	}

	public function sqlApplicationTypeId() {
		return ( true == isset( $this->m_intApplicationTypeId ) ) ? ( string ) $this->m_intApplicationTypeId : '1';
	}

	public function setScreeningVendorId( $intScreeningVendorId ) {
		$this->set( 'm_intScreeningVendorId', CStrings::strToIntDef( $intScreeningVendorId, NULL, false ) );
	}

	public function getScreeningVendorId() {
		return $this->m_intScreeningVendorId;
	}

	public function sqlScreeningVendorId() {
		return ( true == isset( $this->m_intScreeningVendorId ) ) ? ( string ) $this->m_intScreeningVendorId : 'NULL';
	}

	public function setRemoteScreeningId( $intRemoteScreeningId ) {
		$this->set( 'm_intRemoteScreeningId', CStrings::strToIntDef( $intRemoteScreeningId, NULL, false ) );
	}

	public function getRemoteScreeningId() {
		return $this->m_intRemoteScreeningId;
	}

	public function sqlRemoteScreeningId() {
		return ( true == isset( $this->m_intRemoteScreeningId ) ) ? ( string ) $this->m_intRemoteScreeningId : 'NULL';
	}

	public function setRequestStatusTypeId( $intRequestStatusTypeId ) {
		$this->set( 'm_intRequestStatusTypeId', CStrings::strToIntDef( $intRequestStatusTypeId, NULL, false ) );
	}

	public function getRequestStatusTypeId() {
		return $this->m_intRequestStatusTypeId;
	}

	public function sqlRequestStatusTypeId() {
		return ( true == isset( $this->m_intRequestStatusTypeId ) ) ? ( string ) $this->m_intRequestStatusTypeId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->set( 'm_intScreeningDecisionTypeId', CStrings::strToIntDef( $intScreeningDecisionTypeId, NULL, false ) );
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function sqlScreeningDecisionTypeId() {
		return ( true == isset( $this->m_intScreeningDecisionTypeId ) ) ? ( string ) $this->m_intScreeningDecisionTypeId : 'NULL';
	}

	public function setScreeningDecisionReasonTypeIds( $arrintScreeningDecisionReasonTypeIds ) {
		$this->set( 'm_arrintScreeningDecisionReasonTypeIds', CStrings::strToArrIntDef( $arrintScreeningDecisionReasonTypeIds, NULL ) );
	}

	public function getScreeningDecisionReasonTypeIds() {
		return $this->m_arrintScreeningDecisionReasonTypeIds;
	}

	public function sqlScreeningDecisionReasonTypeIds() {
		return ( true == isset( $this->m_arrintScreeningDecisionReasonTypeIds ) && true == valArr( $this->m_arrintScreeningDecisionReasonTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintScreeningDecisionReasonTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setScreeningDecisionUpdatedBy( $intScreeningDecisionUpdatedBy ) {
		$this->set( 'm_intScreeningDecisionUpdatedBy', CStrings::strToIntDef( $intScreeningDecisionUpdatedBy, NULL, false ) );
	}

	public function getScreeningDecisionUpdatedBy() {
		return $this->m_intScreeningDecisionUpdatedBy;
	}

	public function sqlScreeningDecisionUpdatedBy() {
		return ( true == isset( $this->m_intScreeningDecisionUpdatedBy ) ) ? ( string ) $this->m_intScreeningDecisionUpdatedBy : 'NULL';
	}

	public function setScreeningDecisionUpdatedOn( $strScreeningDecisionUpdatedOn ) {
		$this->set( 'm_strScreeningDecisionUpdatedOn', CStrings::strTrimDef( $strScreeningDecisionUpdatedOn, -1, NULL, true ) );
	}

	public function getScreeningDecisionUpdatedOn() {
		return $this->m_strScreeningDecisionUpdatedOn;
	}

	public function sqlScreeningDecisionUpdatedOn() {
		return ( true == isset( $this->m_strScreeningDecisionUpdatedOn ) ) ? '\'' . $this->m_strScreeningDecisionUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningConditionCodes( $arrstrScreeningConditionCodes ) {
		$this->set( 'm_arrstrScreeningConditionCodes', CStrings::strToArrIntDef( $arrstrScreeningConditionCodes, NULL ) );
	}

	public function getScreeningConditionCodes() {
		return $this->m_arrstrScreeningConditionCodes;
	}

	public function sqlScreeningConditionCodes() {
		return ( true == isset( $this->m_arrstrScreeningConditionCodes ) && true == valArr( $this->m_arrstrScreeningConditionCodes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrScreeningConditionCodes, NULL ) . '\'' : 'NULL';
	}

    public function setErrorDescription( $strErrorDescription ) {
        $this->set( 'm_strErrorDescription', CStrings::strTrimDef( $strErrorDescription, 250, NULL, true ) );
    }

    public function getErrorDescription() {
        return $this->m_strErrorDescription;
    }

    public function sqlErrorDescription() {
        return ( true == isset( $this->m_strErrorDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrorDescription ) : '\'' . addslashes( $this->m_strErrorDescription ) . '\'' ) : 'NULL';
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, application_id, application_type_id, screening_vendor_id, remote_screening_id, request_status_type_id, screening_recommendation_type_id, screening_decision_type_id, screening_decision_reason_type_ids, screening_decision_updated_by, screening_decision_updated_on, updated_by, updated_on, created_by, created_on, screening_condition_codes, error_description )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlApplicationTypeId() . ', ' .
						$this->sqlScreeningVendorId() . ', ' .
						$this->sqlRemoteScreeningId() . ', ' .
						$this->sqlRequestStatusTypeId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlScreeningDecisionTypeId() . ', ' .
						$this->sqlScreeningDecisionReasonTypeIds() . ', ' .
						$this->sqlScreeningDecisionUpdatedBy() . ', ' .
						$this->sqlScreeningDecisionUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningConditionCodes() . ', ' .
						$this->sqlErrorDescription() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_type_id = ' . $this->sqlApplicationTypeId(). ',' ; } elseif( true == array_key_exists( 'ApplicationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' application_type_id = ' . $this->sqlApplicationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_vendor_id = ' . $this->sqlScreeningVendorId(). ',' ; } elseif( true == array_key_exists( 'ScreeningVendorId', $this->getChangedColumns() ) ) { $strSql .= ' screening_vendor_id = ' . $this->sqlScreeningVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_screening_id = ' . $this->sqlRemoteScreeningId(). ',' ; } elseif( true == array_key_exists( 'RemoteScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' remote_screening_id = ' . $this->sqlRemoteScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_status_type_id = ' . $this->sqlRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'RequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' request_status_type_id = ' . $this->sqlRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_type_id = ' . $this->sqlScreeningDecisionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_reason_type_ids = ' . $this->sqlScreeningDecisionReasonTypeIds(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionReasonTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_reason_type_ids = ' . $this->sqlScreeningDecisionReasonTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_updated_by = ' . $this->sqlScreeningDecisionUpdatedBy(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionUpdatedBy', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_updated_by = ' . $this->sqlScreeningDecisionUpdatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_decision_updated_on = ' . $this->sqlScreeningDecisionUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'ScreeningDecisionUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' screening_decision_updated_on = ' . $this->sqlScreeningDecisionUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_condition_codes = ' . $this->sqlScreeningConditionCodes(). ',' ; } elseif( true == array_key_exists( 'ScreeningConditionCodes', $this->getChangedColumns() ) ) { $strSql .= ' screening_condition_codes = ' . $this->sqlScreeningConditionCodes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription(). ',' ; } elseif( true == array_key_exists( 'ErrorDescription', $this->getChangedColumns() ) ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'application_id' => $this->getApplicationId(),
			'application_type_id' => $this->getApplicationTypeId(),
			'screening_vendor_id' => $this->getScreeningVendorId(),
			'remote_screening_id' => $this->getRemoteScreeningId(),
			'request_status_type_id' => $this->getRequestStatusTypeId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'screening_decision_type_id' => $this->getScreeningDecisionTypeId(),
			'screening_decision_reason_type_ids' => $this->getScreeningDecisionReasonTypeIds(),
			'screening_decision_updated_by' => $this->getScreeningDecisionUpdatedBy(),
			'screening_decision_updated_on' => $this->getScreeningDecisionUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_condition_codes' => $this->getScreeningConditionCodes(),
			'error_description' => $this->getErrorDescription()
		);
	}

}
?>