<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporate extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.corporates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_strBusinessName;
	protected $m_strTaxNumberLastFour;
	protected $m_strTaxNumberEncrypted;
	protected $m_strBin;
	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_strStreetLine1;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strPrimaryContact;
	protected $m_strPrimaryContactPhone;
	protected $m_strPrimaryContactEmail;
	protected $m_intIsArchive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAdditionalDetails;
	protected $m_jsonAdditionalDetails;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCountryCode;
	protected $m_intPropertyId;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intIsArchive = '0';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strStreetLine1' => 'addressLine1',
				'm_strCity' => 'locality',
				'm_strStateCode' => 'administrativeArea',
				'm_strPostalCode' => 'postalCode',
				'm_strCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['business_name'] ) && $boolDirectSet ) $this->set( 'm_strBusinessName', trim( $arrValues['business_name'] ) ); elseif( isset( $arrValues['business_name'] ) ) $this->setBusinessName( $arrValues['business_name'] );
		if( isset( $arrValues['tax_number_last_four'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberLastFour', trim( $arrValues['tax_number_last_four'] ) ); elseif( isset( $arrValues['tax_number_last_four'] ) ) $this->setTaxNumberLastFour( $arrValues['tax_number_last_four'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['bin'] ) && $boolDirectSet ) $this->set( 'm_strBin', trim( $arrValues['bin'] ) ); elseif( isset( $arrValues['bin'] ) ) $this->setBin( $arrValues['bin'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStreetLine1', trim( $arrValues['street_line1'] ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( $arrValues['street_line1'] );
		if( isset( $arrValues['city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['primary_contact'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryContact', trim( $arrValues['primary_contact'] ) ); elseif( isset( $arrValues['primary_contact'] ) ) $this->setPrimaryContact( $arrValues['primary_contact'] );
		if( isset( $arrValues['primary_contact_phone'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryContactPhone', trim( $arrValues['primary_contact_phone'] ) ); elseif( isset( $arrValues['primary_contact_phone'] ) ) $this->setPrimaryContactPhone( $arrValues['primary_contact_phone'] );
		if( isset( $arrValues['primary_contact_email'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryContactEmail', trim( $arrValues['primary_contact_email'] ) ); elseif( isset( $arrValues['primary_contact_email'] ) ) $this->setPrimaryContactEmail( $arrValues['primary_contact_email'] );
		if( isset( $arrValues['is_archive'] ) && $boolDirectSet ) $this->set( 'm_intIsArchive', trim( $arrValues['is_archive'] ) ); elseif( isset( $arrValues['is_archive'] ) ) $this->setIsArchive( $arrValues['is_archive'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['additional_details'] ) ) $this->set( 'm_strAdditionalDetails', trim( $arrValues['additional_details'] ) );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setBusinessName( $strBusinessName ) {
		$this->set( 'm_strBusinessName', CStrings::strTrimDef( $strBusinessName, 1000, NULL, true ) );
	}

	public function getBusinessName() {
		return $this->m_strBusinessName;
	}

	public function sqlBusinessName() {
		return ( true == isset( $this->m_strBusinessName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBusinessName ) : '\'' . addslashes( $this->m_strBusinessName ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberLastFour( $strTaxNumberLastFour ) {
		$this->set( 'm_strTaxNumberLastFour', CStrings::strTrimDef( $strTaxNumberLastFour, 10, NULL, true ) );
	}

	public function getTaxNumberLastFour() {
		return $this->m_strTaxNumberLastFour;
	}

	public function sqlTaxNumberLastFour() {
		return ( true == isset( $this->m_strTaxNumberLastFour ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberLastFour ) : '\'' . addslashes( $this->m_strTaxNumberLastFour ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 255, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBin( $strBin ) {
		$this->set( 'm_strBin', CStrings::strTrimDef( $strBin, 15, NULL, true ) );
	}

	public function getBin() {
		return $this->m_strBin;
	}

	public function sqlBin() {
		return ( true == isset( $this->m_strBin ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBin ) : '\'' . addslashes( $this->m_strBin ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strStreetLine1, $strAddressKey = 'default', 'm_strStreetLine1' );
	}

	public function getStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strStreetLine1' );
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine1 ) : '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->setPostalAddressField( 'locality', $strCity, $strAddressKey = 'default', 'm_strCity' );
	}

	public function getCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCity' );
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strStateCode, $strAddressKey = 'default', 'm_strStateCode' );
	}

	public function getStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strStateCode' );
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strPostalCode, $strAddressKey = 'default', 'm_strPostalCode' );
	}

	public function getPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strPostalCode' );
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPrimaryContact( $strPrimaryContact ) {
		$this->set( 'm_strPrimaryContact', CStrings::strTrimDef( $strPrimaryContact, 100, NULL, true ) );
	}

	public function getPrimaryContact() {
		return $this->m_strPrimaryContact;
	}

	public function sqlPrimaryContact() {
		return ( true == isset( $this->m_strPrimaryContact ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryContact ) : '\'' . addslashes( $this->m_strPrimaryContact ) . '\'' ) : 'NULL';
	}

	public function setPrimaryContactPhone( $strPrimaryContactPhone ) {
		$this->set( 'm_strPrimaryContactPhone', CStrings::strTrimDef( $strPrimaryContactPhone, 30, NULL, true ) );
	}

	public function getPrimaryContactPhone() {
		return $this->m_strPrimaryContactPhone;
	}

	public function sqlPrimaryContactPhone() {
		return ( true == isset( $this->m_strPrimaryContactPhone ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryContactPhone ) : '\'' . addslashes( $this->m_strPrimaryContactPhone ) . '\'' ) : 'NULL';
	}

	public function setPrimaryContactEmail( $strPrimaryContactEmail ) {
		$this->set( 'm_strPrimaryContactEmail', CStrings::strTrimDef( $strPrimaryContactEmail, 240, NULL, true ) );
	}

	public function getPrimaryContactEmail() {
		return $this->m_strPrimaryContactEmail;
	}

	public function sqlPrimaryContactEmail() {
		return ( true == isset( $this->m_strPrimaryContactEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryContactEmail ) : '\'' . addslashes( $this->m_strPrimaryContactEmail ) . '\'' ) : 'NULL';
	}

	public function setIsArchive( $intIsArchive ) {
		$this->set( 'm_intIsArchive', CStrings::strToIntDef( $intIsArchive, NULL, false ) );
	}

	public function getIsArchive() {
		return $this->m_intIsArchive;
	}

	public function sqlIsArchive() {
		return ( true == isset( $this->m_intIsArchive ) ) ? ( string ) $this->m_intIsArchive : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAdditionalDetails( $jsonAdditionalDetails ) {
		if( true == valObj( $jsonAdditionalDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAdditionalDetails', $jsonAdditionalDetails );
		} elseif( true == valJsonString( $jsonAdditionalDetails ) ) {
			$this->set( 'm_jsonAdditionalDetails', CStrings::strToJson( $jsonAdditionalDetails ) );
		} else {
			$this->set( 'm_jsonAdditionalDetails', NULL ); 
		}
		unset( $this->m_strAdditionalDetails );
	}

	public function getAdditionalDetails() {
		if( true == isset( $this->m_strAdditionalDetails ) ) {
			$this->m_jsonAdditionalDetails = CStrings::strToJson( $this->m_strAdditionalDetails );
			unset( $this->m_strAdditionalDetails );
		}
		return $this->m_jsonAdditionalDetails;
	}

	public function sqlAdditionalDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->setPostalAddressField( 'country', $strCountryCode, $strAddressKey = 'default', 'm_strCountryCode' );
	}

	public function getCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCountryCode' );
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, business_name, tax_number_last_four, tax_number_encrypted, bin, phone_number, email_address, street_line1, city, state_code, postal_code, primary_contact, primary_contact_phone, primary_contact_email, is_archive, updated_by, updated_on, created_by, created_on, additional_details, details, country_code, property_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlBusinessName() . ', ' .
						$this->sqlTaxNumberLastFour() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlBin() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlPrimaryContact() . ', ' .
						$this->sqlPrimaryContactPhone() . ', ' .
						$this->sqlPrimaryContactEmail() . ', ' .
						$this->sqlIsArchive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAdditionalDetails() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlPropertyId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_name = ' . $this->sqlBusinessName(). ',' ; } elseif( true == array_key_exists( 'BusinessName', $this->getChangedColumns() ) ) { $strSql .= ' business_name = ' . $this->sqlBusinessName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour(). ',' ; } elseif( true == array_key_exists( 'TaxNumberLastFour', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_last_four = ' . $this->sqlTaxNumberLastFour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bin = ' . $this->sqlBin(). ',' ; } elseif( true == array_key_exists( 'Bin', $this->getChangedColumns() ) ) { $strSql .= ' bin = ' . $this->sqlBin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_contact = ' . $this->sqlPrimaryContact(). ',' ; } elseif( true == array_key_exists( 'PrimaryContact', $this->getChangedColumns() ) ) { $strSql .= ' primary_contact = ' . $this->sqlPrimaryContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_contact_phone = ' . $this->sqlPrimaryContactPhone(). ',' ; } elseif( true == array_key_exists( 'PrimaryContactPhone', $this->getChangedColumns() ) ) { $strSql .= ' primary_contact_phone = ' . $this->sqlPrimaryContactPhone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_contact_email = ' . $this->sqlPrimaryContactEmail(). ',' ; } elseif( true == array_key_exists( 'PrimaryContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' primary_contact_email = ' . $this->sqlPrimaryContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archive = ' . $this->sqlIsArchive(). ',' ; } elseif( true == array_key_exists( 'IsArchive', $this->getChangedColumns() ) ) { $strSql .= ' is_archive = ' . $this->sqlIsArchive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails(). ',' ; } elseif( true == array_key_exists( 'AdditionalDetails', $this->getChangedColumns() ) ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'business_name' => $this->getBusinessName(),
			'tax_number_last_four' => $this->getTaxNumberLastFour(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'bin' => $this->getBin(),
			'phone_number' => $this->getPhoneNumber(),
			'email_address' => $this->getEmailAddress(),
			'street_line1' => $this->getStreetLine1(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'primary_contact' => $this->getPrimaryContact(),
			'primary_contact_phone' => $this->getPrimaryContactPhone(),
			'primary_contact_email' => $this->getPrimaryContactEmail(),
			'is_archive' => $this->getIsArchive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'additional_details' => $this->getAdditionalDetails(),
			'details' => $this->getDetails(),
			'country_code' => $this->getCountryCode(),
			'property_id' => $this->getPropertyId()
		);
	}

}
?>