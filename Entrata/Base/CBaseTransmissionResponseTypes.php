<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionResponseTypes
 * Do not add any new functions to this class.
 */

class CBaseTransmissionResponseTypes extends CEosPluralBase {

	/**
	 * @return CTransmissionResponseType[]
	 */
	public static function fetchTransmissionResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransmissionResponseType::class, $objDatabase );
	}

	/**
	 * @return CTransmissionResponseType
	 */
	public static function fetchTransmissionResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransmissionResponseType::class, $objDatabase );
	}

	public static function fetchTransmissionResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transmission_response_types', $objDatabase );
	}

	public static function fetchTransmissionResponseTypeById( $intId, $objDatabase ) {
		return self::fetchTransmissionResponseType( sprintf( 'SELECT * FROM transmission_response_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTransmissionResponseTypesByTransmissionTypeId( $intTransmissionTypeId, $objDatabase ) {
		return self::fetchTransmissionResponseTypes( sprintf( 'SELECT * FROM transmission_response_types WHERE transmission_type_id = %d', $intTransmissionTypeId ), $objDatabase );
	}

}
?>