<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliation extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_reconciliations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlReconciliationStatusTypeId;
	protected $m_intBankAccountId;
	protected $m_strBeginningDate;
	protected $m_strStatementDate;
	protected $m_fltBeginningAmount;
	protected $m_fltEndingAmount;
	protected $m_fltEndingGlAmount;
	protected $m_intIsSystem;
	protected $m_intReconciledBy;
	protected $m_strReconciledOn;
	protected $m_intIsPostMonth;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSystem = '0';
		$this->m_intIsPostMonth = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_reconciliation_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationStatusTypeId', trim( $arrValues['gl_reconciliation_status_type_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_status_type_id'] ) ) $this->setGlReconciliationStatusTypeId( $arrValues['gl_reconciliation_status_type_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['beginning_date'] ) && $boolDirectSet ) $this->set( 'm_strBeginningDate', trim( $arrValues['beginning_date'] ) ); elseif( isset( $arrValues['beginning_date'] ) ) $this->setBeginningDate( $arrValues['beginning_date'] );
		if( isset( $arrValues['statement_date'] ) && $boolDirectSet ) $this->set( 'm_strStatementDate', trim( $arrValues['statement_date'] ) ); elseif( isset( $arrValues['statement_date'] ) ) $this->setStatementDate( $arrValues['statement_date'] );
		if( isset( $arrValues['beginning_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningAmount', trim( $arrValues['beginning_amount'] ) ); elseif( isset( $arrValues['beginning_amount'] ) ) $this->setBeginningAmount( $arrValues['beginning_amount'] );
		if( isset( $arrValues['ending_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEndingAmount', trim( $arrValues['ending_amount'] ) ); elseif( isset( $arrValues['ending_amount'] ) ) $this->setEndingAmount( $arrValues['ending_amount'] );
		if( isset( $arrValues['ending_gl_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEndingGlAmount', trim( $arrValues['ending_gl_amount'] ) ); elseif( isset( $arrValues['ending_gl_amount'] ) ) $this->setEndingGlAmount( $arrValues['ending_gl_amount'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['reconciled_by'] ) && $boolDirectSet ) $this->set( 'm_intReconciledBy', trim( $arrValues['reconciled_by'] ) ); elseif( isset( $arrValues['reconciled_by'] ) ) $this->setReconciledBy( $arrValues['reconciled_by'] );
		if( isset( $arrValues['reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strReconciledOn', trim( $arrValues['reconciled_on'] ) ); elseif( isset( $arrValues['reconciled_on'] ) ) $this->setReconciledOn( $arrValues['reconciled_on'] );
		if( isset( $arrValues['is_post_month'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonth', trim( $arrValues['is_post_month'] ) ); elseif( isset( $arrValues['is_post_month'] ) ) $this->setIsPostMonth( $arrValues['is_post_month'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlReconciliationStatusTypeId( $intGlReconciliationStatusTypeId ) {
		$this->set( 'm_intGlReconciliationStatusTypeId', CStrings::strToIntDef( $intGlReconciliationStatusTypeId, NULL, false ) );
	}

	public function getGlReconciliationStatusTypeId() {
		return $this->m_intGlReconciliationStatusTypeId;
	}

	public function sqlGlReconciliationStatusTypeId() {
		return ( true == isset( $this->m_intGlReconciliationStatusTypeId ) ) ? ( string ) $this->m_intGlReconciliationStatusTypeId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setBeginningDate( $strBeginningDate ) {
		$this->set( 'm_strBeginningDate', CStrings::strTrimDef( $strBeginningDate, -1, NULL, true ) );
	}

	public function getBeginningDate() {
		return $this->m_strBeginningDate;
	}

	public function sqlBeginningDate() {
		return ( true == isset( $this->m_strBeginningDate ) ) ? '\'' . $this->m_strBeginningDate . '\'' : 'NULL';
	}

	public function setStatementDate( $strStatementDate ) {
		$this->set( 'm_strStatementDate', CStrings::strTrimDef( $strStatementDate, -1, NULL, true ) );
	}

	public function getStatementDate() {
		return $this->m_strStatementDate;
	}

	public function sqlStatementDate() {
		return ( true == isset( $this->m_strStatementDate ) ) ? '\'' . $this->m_strStatementDate . '\'' : 'NOW()';
	}

	public function setBeginningAmount( $fltBeginningAmount ) {
		$this->set( 'm_fltBeginningAmount', CStrings::strToFloatDef( $fltBeginningAmount, NULL, false, 2 ) );
	}

	public function getBeginningAmount() {
		return $this->m_fltBeginningAmount;
	}

	public function sqlBeginningAmount() {
		return ( true == isset( $this->m_fltBeginningAmount ) ) ? ( string ) $this->m_fltBeginningAmount : 'NULL';
	}

	public function setEndingAmount( $fltEndingAmount ) {
		$this->set( 'm_fltEndingAmount', CStrings::strToFloatDef( $fltEndingAmount, NULL, false, 2 ) );
	}

	public function getEndingAmount() {
		return $this->m_fltEndingAmount;
	}

	public function sqlEndingAmount() {
		return ( true == isset( $this->m_fltEndingAmount ) ) ? ( string ) $this->m_fltEndingAmount : 'NULL';
	}

	public function setEndingGlAmount( $fltEndingGlAmount ) {
		$this->set( 'm_fltEndingGlAmount', CStrings::strToFloatDef( $fltEndingGlAmount, NULL, false, 2 ) );
	}

	public function getEndingGlAmount() {
		return $this->m_fltEndingGlAmount;
	}

	public function sqlEndingGlAmount() {
		return ( true == isset( $this->m_fltEndingGlAmount ) ) ? ( string ) $this->m_fltEndingGlAmount : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setReconciledBy( $intReconciledBy ) {
		$this->set( 'm_intReconciledBy', CStrings::strToIntDef( $intReconciledBy, NULL, false ) );
	}

	public function getReconciledBy() {
		return $this->m_intReconciledBy;
	}

	public function sqlReconciledBy() {
		return ( true == isset( $this->m_intReconciledBy ) ) ? ( string ) $this->m_intReconciledBy : 'NULL';
	}

	public function setReconciledOn( $strReconciledOn ) {
		$this->set( 'm_strReconciledOn', CStrings::strTrimDef( $strReconciledOn, -1, NULL, true ) );
	}

	public function getReconciledOn() {
		return $this->m_strReconciledOn;
	}

	public function sqlReconciledOn() {
		return ( true == isset( $this->m_strReconciledOn ) ) ? '\'' . $this->m_strReconciledOn . '\'' : 'NULL';
	}

	public function setIsPostMonth( $intIsPostMonth ) {
		$this->set( 'm_intIsPostMonth', CStrings::strToIntDef( $intIsPostMonth, NULL, false ) );
	}

	public function getIsPostMonth() {
		return $this->m_intIsPostMonth;
	}

	public function sqlIsPostMonth() {
		return ( true == isset( $this->m_intIsPostMonth ) ) ? ( string ) $this->m_intIsPostMonth : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_reconciliation_status_type_id, bank_account_id, beginning_date, statement_date, beginning_amount, ending_amount, ending_gl_amount, is_system, reconciled_by, reconciled_on, is_post_month, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlGlReconciliationStatusTypeId() . ', ' .
 						$this->sqlBankAccountId() . ', ' .
 						$this->sqlBeginningDate() . ', ' .
 						$this->sqlStatementDate() . ', ' .
 						$this->sqlBeginningAmount() . ', ' .
 						$this->sqlEndingAmount() . ', ' .
 						$this->sqlEndingGlAmount() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlReconciledBy() . ', ' .
 						$this->sqlReconciledOn() . ', ' .
 						$this->sqlIsPostMonth() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_status_type_id = ' . $this->sqlGlReconciliationStatusTypeId() . ','; } elseif( true == array_key_exists( 'GlReconciliationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_status_type_id = ' . $this->sqlGlReconciliationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beginning_date = ' . $this->sqlBeginningDate() . ','; } elseif( true == array_key_exists( 'BeginningDate', $this->getChangedColumns() ) ) { $strSql .= ' beginning_date = ' . $this->sqlBeginningDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' statement_date = ' . $this->sqlStatementDate() . ','; } elseif( true == array_key_exists( 'StatementDate', $this->getChangedColumns() ) ) { $strSql .= ' statement_date = ' . $this->sqlStatementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beginning_amount = ' . $this->sqlBeginningAmount() . ','; } elseif( true == array_key_exists( 'BeginningAmount', $this->getChangedColumns() ) ) { $strSql .= ' beginning_amount = ' . $this->sqlBeginningAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ending_amount = ' . $this->sqlEndingAmount() . ','; } elseif( true == array_key_exists( 'EndingAmount', $this->getChangedColumns() ) ) { $strSql .= ' ending_amount = ' . $this->sqlEndingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ending_gl_amount = ' . $this->sqlEndingGlAmount() . ','; } elseif( true == array_key_exists( 'EndingGlAmount', $this->getChangedColumns() ) ) { $strSql .= ' ending_gl_amount = ' . $this->sqlEndingGlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy() . ','; } elseif( true == array_key_exists( 'ReconciledBy', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; } elseif( true == array_key_exists( 'ReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month = ' . $this->sqlIsPostMonth() . ','; } elseif( true == array_key_exists( 'IsPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month = ' . $this->sqlIsPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_reconciliation_status_type_id' => $this->getGlReconciliationStatusTypeId(),
			'bank_account_id' => $this->getBankAccountId(),
			'beginning_date' => $this->getBeginningDate(),
			'statement_date' => $this->getStatementDate(),
			'beginning_amount' => $this->getBeginningAmount(),
			'ending_amount' => $this->getEndingAmount(),
			'ending_gl_amount' => $this->getEndingGlAmount(),
			'is_system' => $this->getIsSystem(),
			'reconciled_by' => $this->getReconciledBy(),
			'reconciled_on' => $this->getReconciledOn(),
			'is_post_month' => $this->getIsPostMonth(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>