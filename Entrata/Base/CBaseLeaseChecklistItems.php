<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseChecklistItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseChecklistItems extends CEosPluralBase {

	/**
	 * @return CLeaseChecklistItem[]
	 */
	public static function fetchLeaseChecklistItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseChecklistItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseChecklistItem
	 */
	public static function fetchLeaseChecklistItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseChecklistItem::class, $objDatabase );
	}

	public static function fetchLeaseChecklistItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_checklist_items', $objDatabase );
	}

	public static function fetchLeaseChecklistItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItem( sprintf( 'SELECT * FROM lease_checklist_items WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByLeaseChecklistIdByCid( $intLeaseChecklistId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE lease_checklist_id = %d AND cid = %d', $intLeaseChecklistId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByChecklistItemIdByCid( $intChecklistItemId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE checklist_item_id = %d AND cid = %d', $intChecklistItemId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchLeaseChecklistItems( sprintf( 'SELECT * FROM lease_checklist_items WHERE ar_transaction_id = %d AND cid = %d', $intArTransactionId, $intCid ), $objDatabase );
	}

}
?>