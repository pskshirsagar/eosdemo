<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComparableAssociations extends CEosPluralBase {

	/**
	 * @return CComparableAssociation[]
	 */
	public static function fetchComparableAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CComparableAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CComparableAssociation
	 */
	public static function fetchComparableAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CComparableAssociation', $objDatabase );
	}

	public static function fetchComparableAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'comparable_associations', $objDatabase );
	}

	public static function fetchComparableAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociation( sprintf( 'SELECT * FROM comparable_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByComparableOptionIdByCid( $intComparableOptionId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE comparable_option_id = %d AND cid = %d', ( int ) $intComparableOptionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByComparableRoomTypeIdByCid( $intComparableRoomTypeId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE comparable_room_type_id = %d AND cid = %d', ( int ) $intComparableRoomTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByDefaultAmenityIdByCid( $intDefaultAmenityId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE default_amenity_id = %d AND cid = %d', ( int ) $intDefaultAmenityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByComparableReferenceIdByCid( $intComparableReferenceId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE comparable_reference_id = %d AND cid = %d', ( int ) $intComparableReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchComparableAssociationsByUtilityTypeIdByCid( $intUtilityTypeId, $intCid, $objDatabase ) {
		return self::fetchComparableAssociations( sprintf( 'SELECT * FROM comparable_associations WHERE utility_type_id = %d AND cid = %d', ( int ) $intUtilityTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>