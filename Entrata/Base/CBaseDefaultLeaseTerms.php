<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseTerms
 * Do not add any new functions to this class.
 */

class CBaseDefaultLeaseTerms extends CEosPluralBase {

	/**
	 * @return CDefaultLeaseTerm[]
	 */
	public static function fetchDefaultLeaseTerms( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultLeaseTerm::class, $objDatabase );
	}

	/**
	 * @return CDefaultLeaseTerm
	 */
	public static function fetchDefaultLeaseTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultLeaseTerm::class, $objDatabase );
	}

	public static function fetchDefaultLeaseTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_lease_terms', $objDatabase );
	}

	public static function fetchDefaultLeaseTermById( $intId, $objDatabase ) {
		return self::fetchDefaultLeaseTerm( sprintf( 'SELECT * FROM default_lease_terms WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultLeaseTermsByDefaultLeaseTermStructureId( $intDefaultLeaseTermStructureId, $objDatabase ) {
		return self::fetchDefaultLeaseTerms( sprintf( 'SELECT * FROM default_lease_terms WHERE default_lease_term_structure_id = %d', $intDefaultLeaseTermStructureId ), $objDatabase );
	}

	public static function fetchDefaultLeaseTermsByOccupancyTypeId( $intOccupancyTypeId, $objDatabase ) {
		return self::fetchDefaultLeaseTerms( sprintf( 'SELECT * FROM default_lease_terms WHERE occupancy_type_id = %d', $intOccupancyTypeId ), $objDatabase );
	}

	public static function fetchDefaultLeaseTermsByLeaseTermTypeId( $intLeaseTermTypeId, $objDatabase ) {
		return self::fetchDefaultLeaseTerms( sprintf( 'SELECT * FROM default_lease_terms WHERE lease_term_type_id = %d', $intLeaseTermTypeId ), $objDatabase );
	}

}
?>