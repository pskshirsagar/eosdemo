<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntityTypes
 * Do not add any new functions to this class.
 */

class CBaseEntityTypes extends CEosPluralBase {

	/**
	 * @return CEntityType[]
	 */
	public static function fetchEntityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEntityType::class, $objDatabase );
	}

	/**
	 * @return CEntityType
	 */
	public static function fetchEntityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEntityType::class, $objDatabase );
	}

	public static function fetchEntityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entity_types', $objDatabase );
	}

	public static function fetchEntityTypeById( $intId, $objDatabase ) {
		return self::fetchEntityType( sprintf( 'SELECT * FROM entity_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>