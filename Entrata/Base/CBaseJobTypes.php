<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobTypes
 * Do not add any new functions to this class.
 */

class CBaseJobTypes extends CEosPluralBase {

	/**
	 * @return CJobType[]
	 */
	public static function fetchJobTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CJobType::class, $objDatabase );
	}

	/**
	 * @return CJobType
	 */
	public static function fetchJobType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobType::class, $objDatabase );
	}

	public static function fetchJobTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_types', $objDatabase );
	}

	public static function fetchJobTypeById( $intId, $objDatabase ) {
		return self::fetchJobType( sprintf( 'SELECT * FROM job_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>