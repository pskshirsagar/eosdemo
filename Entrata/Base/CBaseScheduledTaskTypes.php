<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduledTaskTypes extends CEosPluralBase {

	/**
	 * @return CScheduledTaskType[]
	 */
	public static function fetchScheduledTaskTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledTaskType::class, $objDatabase );
	}

	/**
	 * @return CScheduledTaskType
	 */
	public static function fetchScheduledTaskType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledTaskType::class, $objDatabase );
	}

	public static function fetchScheduledTaskTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_task_types', $objDatabase );
	}

	public static function fetchScheduledTaskTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledTaskType( sprintf( 'SELECT * FROM scheduled_task_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>