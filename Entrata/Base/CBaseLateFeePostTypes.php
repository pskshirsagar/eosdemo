<?php

class CBaseLateFeePostTypes extends CEosPluralBase {

	/**
	 * @return CLateFeePostType[]
	 */
	public static function fetchLateFeePostTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLateFeePostType', $objDatabase );
	}

	/**
	 * @return CLateFeePostType
	 */
	public static function fetchLateFeePostType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLateFeePostType', $objDatabase );
	}

	public static function fetchLateFeePostTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'late_fee_post_types', $objDatabase );
	}

	public static function fetchLateFeePostTypeById( $intId, $objDatabase ) {
		return self::fetchLateFeePostType( sprintf( 'SELECT * FROM late_fee_post_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>