<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultAssetConditions
 * Do not add any new functions to this class.
 */

class CBaseDefaultAssetConditions extends CEosPluralBase {

	/**
	 * @return CDefaultAssetCondition[]
	 */
	public static function fetchDefaultAssetConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultAssetCondition::class, $objDatabase );
	}

	/**
	 * @return CDefaultAssetCondition
	 */
	public static function fetchDefaultAssetCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultAssetCondition::class, $objDatabase );
	}

	public static function fetchDefaultAssetConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_asset_conditions', $objDatabase );
	}

	public static function fetchDefaultAssetConditionById( $intId, $objDatabase ) {
		return self::fetchDefaultAssetCondition( sprintf( 'SELECT * FROM default_asset_conditions WHERE id = %d', $intId ), $objDatabase );
	}

}
?>