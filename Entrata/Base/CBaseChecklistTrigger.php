<?php

class CBaseChecklistTrigger extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.checklist_triggers';

	protected $m_intId;
	protected $m_intChecklistTypeId;
	protected $m_intApplicationStageId;
	protected $m_intApplicationStatusId;
	protected $m_intEventTypeId;
	protected $m_strName;
	protected $m_boolCanShowOnPortals;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolCanShowOnPortals = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['checklist_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistTypeId', trim( $arrValues['checklist_type_id'] ) ); elseif( isset( $arrValues['checklist_type_id'] ) ) $this->setChecklistTypeId( $arrValues['checklist_type_id'] );
		if( isset( $arrValues['application_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStageId', trim( $arrValues['application_stage_id'] ) ); elseif( isset( $arrValues['application_stage_id'] ) ) $this->setApplicationStageId( $arrValues['application_stage_id'] );
		if( isset( $arrValues['application_status_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStatusId', trim( $arrValues['application_status_id'] ) ); elseif( isset( $arrValues['application_status_id'] ) ) $this->setApplicationStatusId( $arrValues['application_status_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['can_show_on_portals'] ) && $boolDirectSet ) $this->set( 'm_boolCanShowOnPortals', trim( stripcslashes( $arrValues['can_show_on_portals'] ) ) ); elseif( isset( $arrValues['can_show_on_portals'] ) ) $this->setCanShowOnPortals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['can_show_on_portals'] ) : $arrValues['can_show_on_portals'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChecklistTypeId( $intChecklistTypeId ) {
		$this->set( 'm_intChecklistTypeId', CStrings::strToIntDef( $intChecklistTypeId, NULL, false ) );
	}

	public function getChecklistTypeId() {
		return $this->m_intChecklistTypeId;
	}

	public function sqlChecklistTypeId() {
		return ( true == isset( $this->m_intChecklistTypeId ) ) ? ( string ) $this->m_intChecklistTypeId : 'NULL';
	}

	public function setApplicationStageId( $intApplicationStageId ) {
		$this->set( 'm_intApplicationStageId', CStrings::strToIntDef( $intApplicationStageId, NULL, false ) );
	}

	public function getApplicationStageId() {
		return $this->m_intApplicationStageId;
	}

	public function sqlApplicationStageId() {
		return ( true == isset( $this->m_intApplicationStageId ) ) ? ( string ) $this->m_intApplicationStageId : 'NULL';
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->set( 'm_intApplicationStatusId', CStrings::strToIntDef( $intApplicationStatusId, NULL, false ) );
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function sqlApplicationStatusId() {
		return ( true == isset( $this->m_intApplicationStatusId ) ) ? ( string ) $this->m_intApplicationStatusId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setCanShowOnPortals( $boolCanShowOnPortals ) {
		$this->set( 'm_boolCanShowOnPortals', CStrings::strToBool( $boolCanShowOnPortals ) );
	}

	public function getCanShowOnPortals() {
		return $this->m_boolCanShowOnPortals;
	}

	public function sqlCanShowOnPortals() {
		return ( true == isset( $this->m_boolCanShowOnPortals ) ) ? '\'' . ( true == ( bool ) $this->m_boolCanShowOnPortals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'checklist_type_id' => $this->getChecklistTypeId(),
			'application_stage_id' => $this->getApplicationStageId(),
			'application_status_id' => $this->getApplicationStatusId(),
			'event_type_id' => $this->getEventTypeId(),
			'name' => $this->getName(),
			'can_show_on_portals' => $this->getCanShowOnPortals(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>