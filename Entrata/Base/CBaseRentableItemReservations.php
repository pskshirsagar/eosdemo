<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRentableItemReservations extends CEosPluralBase {

    const TABLE_RENTABLE_ITEM_RESERVATIONS = 'public.rentable_item_reservations';

    public static function fetchRentableItemReservations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CRentableItemReservation', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchRentableItemReservation( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRentableItemReservation', $objDatabase );
    }

    public static function fetchRentableItemReservationCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'rentable_item_reservations', $objDatabase );
    }

    public static function fetchRentableItemReservationByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservation( sprintf( 'SELECT * FROM rentable_item_reservations WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByCid( $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByRentableItemOptionIdByCid( $intRentableItemOptionId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE rentable_item_option_id = %d AND cid = %d', (int) $intRentableItemOptionId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByRentableItemIdByCid( $intRentableItemId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE rentable_item_id = %d AND cid = %d', (int) $intRentableItemId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE lease_id = %d AND cid = %d', (int) $intLeaseId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE customer_id = %d AND cid = %d', (int) $intCustomerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE application_id = %d AND cid = %d', (int) $intApplicationId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE applicant_id = %d AND cid = %d', (int) $intApplicantId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE lease_status_type_id = %d AND cid = %d', (int) $intLeaseStatusTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE lease_interval_id = %d AND cid = %d', (int) $intLeaseIntervalId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByRentArCodeIdByCid( $intRentArCodeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE rent_ar_code_id = %d AND cid = %d', (int) $intRentArCodeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE company_employee_id = %d AND cid = %d', (int) $intCompanyEmployeeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByDepositArCodeIdByCid( $intDepositArCodeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE deposit_ar_code_id = %d AND cid = %d', (int) $intDepositArCodeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByOtherArCodeIdByCid( $intOtherArCodeId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE other_ar_code_id = %d AND cid = %d', (int) $intOtherArCodeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchRentableItemReservationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
        return self::fetchRentableItemReservations( sprintf( 'SELECT * FROM rentable_item_reservations WHERE quote_id = %d AND cid = %d', (int) $intQuoteId, (int) $intCid ), $objDatabase );
    }

}
?>