<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyDetail extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyWebsiteId;
	protected $m_intCorporateWebsiteId;
	protected $m_intRouteId;
	protected $m_intManagerialPropertyId;
	protected $m_intJobCashFlowFrequencyId;
	protected $m_intApPayeeId;
	protected $m_intCustomerId;
	protected $m_strMilitaryMacId;
	protected $m_strMilitaryInstallationMacId;
	protected $m_intMilitaryInstallationId;
	protected $m_strRemotePrimaryKey;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTaxNumberMasked;
	protected $m_strCommunityWebsiteUri;
	protected $m_strCorporateWebsiteUri;
	protected $m_intShowAppointmentResults;
	protected $m_intShowLeadOutreachResults;
	protected $m_intShowRenewalOutreachResults;
	protected $m_intShowCollectionsResults;
	protected $m_strParkingPolicy;
	protected $m_strPetPolicy;
	protected $m_strSmokingPolicy;
	protected $m_strRenterInsurancePolicy;
	protected $m_strSchoolDescription;
	protected $m_strTaxNameOnTaxReturn;
	protected $m_strTaxContactPerson;
	protected $m_strTaxStreetAddressLineOne;
	protected $m_strTaxStreetAddressLineTwo;
	protected $m_strTaxCity;
	protected $m_strTaxStateCode;
	protected $m_strTaxProvince;
	protected $m_strTaxCountryCode;
	protected $m_strTaxPostalCode;
	protected $m_strTaxPhoneNumber;
	protected $m_strTaxFax;
	protected $m_strTaxEmail;
	protected $m_intAllowsSmoking;
	protected $m_boolIsFeatured;
	protected $m_boolUpdateVacancyAmenities;
	protected $m_strChargesLastPostedOn;
	protected $m_strLateFeeLastPostedOn;
	protected $m_strLastAdminSyncOn;
	protected $m_strLateNoticesLastAutoSentOn;
	protected $m_strCollectionsNoticesLastAutoSentOn;
	protected $m_strImportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strTaxLabel;

	public function __construct() {
		parent::__construct();

		$this->m_intJobCashFlowFrequencyId = '6';
		$this->m_intShowAppointmentResults = '0';
		$this->m_intShowLeadOutreachResults = '0';
		$this->m_intShowRenewalOutreachResults = '0';
		$this->m_intShowCollectionsResults = '0';
		$this->m_intAllowsSmoking = '2';
		$this->m_boolIsFeatured = false;
		$this->m_boolUpdateVacancyAmenities = false;
		$this->m_strChargesLastPostedOn = 'now';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_website_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyWebsiteId', trim( $arrValues['property_website_id'] ) ); elseif( isset( $arrValues['property_website_id'] ) ) $this->setPropertyWebsiteId( $arrValues['property_website_id'] );
		if( isset( $arrValues['corporate_website_id'] ) && $boolDirectSet ) $this->set( 'm_intCorporateWebsiteId', trim( $arrValues['corporate_website_id'] ) ); elseif( isset( $arrValues['corporate_website_id'] ) ) $this->setCorporateWebsiteId( $arrValues['corporate_website_id'] );
		if( isset( $arrValues['route_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteId', trim( $arrValues['route_id'] ) ); elseif( isset( $arrValues['route_id'] ) ) $this->setRouteId( $arrValues['route_id'] );
		if( isset( $arrValues['managerial_property_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerialPropertyId', trim( $arrValues['managerial_property_id'] ) ); elseif( isset( $arrValues['managerial_property_id'] ) ) $this->setManagerialPropertyId( $arrValues['managerial_property_id'] );
		if( isset( $arrValues['job_cash_flow_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intJobCashFlowFrequencyId', trim( $arrValues['job_cash_flow_frequency_id'] ) ); elseif( isset( $arrValues['job_cash_flow_frequency_id'] ) ) $this->setJobCashFlowFrequencyId( $arrValues['job_cash_flow_frequency_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['military_mac_id'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryMacId', trim( stripcslashes( $arrValues['military_mac_id'] ) ) ); elseif( isset( $arrValues['military_mac_id'] ) ) $this->setMilitaryMacId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_mac_id'] ) : $arrValues['military_mac_id'] );
		if( isset( $arrValues['military_installation_mac_id'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryInstallationMacId', trim( stripcslashes( $arrValues['military_installation_mac_id'] ) ) ); elseif( isset( $arrValues['military_installation_mac_id'] ) ) $this->setMilitaryInstallationMacId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_installation_mac_id'] ) : $arrValues['military_installation_mac_id'] );
		if( isset( $arrValues['military_installation_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryInstallationId', trim( $arrValues['military_installation_id'] ) ); elseif( isset( $arrValues['military_installation_id'] ) ) $this->setMilitaryInstallationId( $arrValues['military_installation_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberMasked', trim( stripcslashes( $arrValues['tax_number_masked'] ) ) ); elseif( isset( $arrValues['tax_number_masked'] ) ) $this->setTaxNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_masked'] ) : $arrValues['tax_number_masked'] );
		if( isset( $arrValues['community_website_uri'] ) && $boolDirectSet ) $this->set( 'm_strCommunityWebsiteUri', trim( stripcslashes( $arrValues['community_website_uri'] ) ) ); elseif( isset( $arrValues['community_website_uri'] ) ) $this->setCommunityWebsiteUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['community_website_uri'] ) : $arrValues['community_website_uri'] );
		if( isset( $arrValues['corporate_website_uri'] ) && $boolDirectSet ) $this->set( 'm_strCorporateWebsiteUri', trim( stripcslashes( $arrValues['corporate_website_uri'] ) ) ); elseif( isset( $arrValues['corporate_website_uri'] ) ) $this->setCorporateWebsiteUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['corporate_website_uri'] ) : $arrValues['corporate_website_uri'] );
		if( isset( $arrValues['show_appointment_results'] ) && $boolDirectSet ) $this->set( 'm_intShowAppointmentResults', trim( $arrValues['show_appointment_results'] ) ); elseif( isset( $arrValues['show_appointment_results'] ) ) $this->setShowAppointmentResults( $arrValues['show_appointment_results'] );
		if( isset( $arrValues['show_lead_outreach_results'] ) && $boolDirectSet ) $this->set( 'm_intShowLeadOutreachResults', trim( $arrValues['show_lead_outreach_results'] ) ); elseif( isset( $arrValues['show_lead_outreach_results'] ) ) $this->setShowLeadOutreachResults( $arrValues['show_lead_outreach_results'] );
		if( isset( $arrValues['show_renewal_outreach_results'] ) && $boolDirectSet ) $this->set( 'm_intShowRenewalOutreachResults', trim( $arrValues['show_renewal_outreach_results'] ) ); elseif( isset( $arrValues['show_renewal_outreach_results'] ) ) $this->setShowRenewalOutreachResults( $arrValues['show_renewal_outreach_results'] );
		if( isset( $arrValues['show_collections_results'] ) && $boolDirectSet ) $this->set( 'm_intShowCollectionsResults', trim( $arrValues['show_collections_results'] ) ); elseif( isset( $arrValues['show_collections_results'] ) ) $this->setShowCollectionsResults( $arrValues['show_collections_results'] );
		if( isset( $arrValues['parking_policy'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strParkingPolicy', trim( stripcslashes( $arrValues['parking_policy'] ) ) ); elseif( isset( $arrValues['parking_policy'] ) ) $this->setParkingPolicy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parking_policy'] ) : $arrValues['parking_policy'] );
		if( isset( $arrValues['pet_policy'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPetPolicy', trim( stripcslashes( $arrValues['pet_policy'] ) ) ); elseif( isset( $arrValues['pet_policy'] ) ) $this->setPetPolicy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pet_policy'] ) : $arrValues['pet_policy'] );
		if( isset( $arrValues['smoking_policy'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSmokingPolicy', trim( stripcslashes( $arrValues['smoking_policy'] ) ) ); elseif( isset( $arrValues['smoking_policy'] ) ) $this->setSmokingPolicy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['smoking_policy'] ) : $arrValues['smoking_policy'] );
		if( isset( $arrValues['renter_insurance_policy'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRenterInsurancePolicy', trim( stripcslashes( $arrValues['renter_insurance_policy'] ) ) ); elseif( isset( $arrValues['renter_insurance_policy'] ) ) $this->setRenterInsurancePolicy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['renter_insurance_policy'] ) : $arrValues['renter_insurance_policy'] );
		if( isset( $arrValues['school_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strSchoolDescription', trim( stripcslashes( $arrValues['school_description'] ) ) ); elseif( isset( $arrValues['school_description'] ) ) $this->setSchoolDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['school_description'] ) : $arrValues['school_description'] );
		if( isset( $arrValues['tax_name_on_tax_return'] ) && $boolDirectSet ) $this->set( 'm_strTaxNameOnTaxReturn', trim( stripcslashes( $arrValues['tax_name_on_tax_return'] ) ) ); elseif( isset( $arrValues['tax_name_on_tax_return'] ) ) $this->setTaxNameOnTaxReturn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_name_on_tax_return'] ) : $arrValues['tax_name_on_tax_return'] );
		if( isset( $arrValues['tax_contact_person'] ) && $boolDirectSet ) $this->set( 'm_strTaxContactPerson', trim( stripcslashes( $arrValues['tax_contact_person'] ) ) ); elseif( isset( $arrValues['tax_contact_person'] ) ) $this->setTaxContactPerson( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_contact_person'] ) : $arrValues['tax_contact_person'] );
		if( isset( $arrValues['tax_street_address_line_one'] ) && $boolDirectSet ) $this->set( 'm_strTaxStreetAddressLineOne', trim( stripcslashes( $arrValues['tax_street_address_line_one'] ) ) ); elseif( isset( $arrValues['tax_street_address_line_one'] ) ) $this->setTaxStreetAddressLineOne( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_street_address_line_one'] ) : $arrValues['tax_street_address_line_one'] );
		if( isset( $arrValues['tax_street_address_line_two'] ) && $boolDirectSet ) $this->set( 'm_strTaxStreetAddressLineTwo', trim( stripcslashes( $arrValues['tax_street_address_line_two'] ) ) ); elseif( isset( $arrValues['tax_street_address_line_two'] ) ) $this->setTaxStreetAddressLineTwo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_street_address_line_two'] ) : $arrValues['tax_street_address_line_two'] );
		if( isset( $arrValues['tax_city'] ) && $boolDirectSet ) $this->set( 'm_strTaxCity', trim( stripcslashes( $arrValues['tax_city'] ) ) ); elseif( isset( $arrValues['tax_city'] ) ) $this->setTaxCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_city'] ) : $arrValues['tax_city'] );
		if( isset( $arrValues['tax_state_code'] ) && $boolDirectSet ) $this->set( 'm_strTaxStateCode', trim( stripcslashes( $arrValues['tax_state_code'] ) ) ); elseif( isset( $arrValues['tax_state_code'] ) ) $this->setTaxStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_state_code'] ) : $arrValues['tax_state_code'] );
		if( isset( $arrValues['tax_province'] ) && $boolDirectSet ) $this->set( 'm_strTaxProvince', trim( stripcslashes( $arrValues['tax_province'] ) ) ); elseif( isset( $arrValues['tax_province'] ) ) $this->setTaxProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_province'] ) : $arrValues['tax_province'] );
		if( isset( $arrValues['tax_country_code'] ) && $boolDirectSet ) $this->set( 'm_strTaxCountryCode', trim( stripcslashes( $arrValues['tax_country_code'] ) ) ); elseif( isset( $arrValues['tax_country_code'] ) ) $this->setTaxCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_country_code'] ) : $arrValues['tax_country_code'] );
		if( isset( $arrValues['tax_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strTaxPostalCode', trim( stripcslashes( $arrValues['tax_postal_code'] ) ) ); elseif( isset( $arrValues['tax_postal_code'] ) ) $this->setTaxPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_postal_code'] ) : $arrValues['tax_postal_code'] );
		if( isset( $arrValues['tax_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strTaxPhoneNumber', trim( stripcslashes( $arrValues['tax_phone_number'] ) ) ); elseif( isset( $arrValues['tax_phone_number'] ) ) $this->setTaxPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_phone_number'] ) : $arrValues['tax_phone_number'] );
		if( isset( $arrValues['tax_fax'] ) && $boolDirectSet ) $this->set( 'm_strTaxFax', trim( stripcslashes( $arrValues['tax_fax'] ) ) ); elseif( isset( $arrValues['tax_fax'] ) ) $this->setTaxFax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_fax'] ) : $arrValues['tax_fax'] );
		if( isset( $arrValues['tax_email'] ) && $boolDirectSet ) $this->set( 'm_strTaxEmail', trim( stripcslashes( $arrValues['tax_email'] ) ) ); elseif( isset( $arrValues['tax_email'] ) ) $this->setTaxEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_email'] ) : $arrValues['tax_email'] );
		if( isset( $arrValues['allows_smoking'] ) && $boolDirectSet ) $this->set( 'm_intAllowsSmoking', trim( $arrValues['allows_smoking'] ) ); elseif( isset( $arrValues['allows_smoking'] ) ) $this->setAllowsSmoking( $arrValues['allows_smoking'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['update_vacancy_amenities'] ) && $boolDirectSet ) $this->set( 'm_boolUpdateVacancyAmenities', trim( stripcslashes( $arrValues['update_vacancy_amenities'] ) ) ); elseif( isset( $arrValues['update_vacancy_amenities'] ) ) $this->setUpdateVacancyAmenities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['update_vacancy_amenities'] ) : $arrValues['update_vacancy_amenities'] );
		if( isset( $arrValues['charges_last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strChargesLastPostedOn', trim( $arrValues['charges_last_posted_on'] ) ); elseif( isset( $arrValues['charges_last_posted_on'] ) ) $this->setChargesLastPostedOn( $arrValues['charges_last_posted_on'] );
		if( isset( $arrValues['late_fee_last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLateFeeLastPostedOn', trim( $arrValues['late_fee_last_posted_on'] ) ); elseif( isset( $arrValues['late_fee_last_posted_on'] ) ) $this->setLateFeeLastPostedOn( $arrValues['late_fee_last_posted_on'] );
		if( isset( $arrValues['last_admin_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastAdminSyncOn', trim( $arrValues['last_admin_sync_on'] ) ); elseif( isset( $arrValues['last_admin_sync_on'] ) ) $this->setLastAdminSyncOn( $arrValues['last_admin_sync_on'] );
		if( isset( $arrValues['late_notices_last_auto_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strLateNoticesLastAutoSentOn', trim( $arrValues['late_notices_last_auto_sent_on'] ) ); elseif( isset( $arrValues['late_notices_last_auto_sent_on'] ) ) $this->setLateNoticesLastAutoSentOn( $arrValues['late_notices_last_auto_sent_on'] );
		if( isset( $arrValues['collections_notices_last_auto_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsNoticesLastAutoSentOn', trim( $arrValues['collections_notices_last_auto_sent_on'] ) ); elseif( isset( $arrValues['collections_notices_last_auto_sent_on'] ) ) $this->setCollectionsNoticesLastAutoSentOn( $arrValues['collections_notices_last_auto_sent_on'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['tax_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTaxLabel', trim( stripcslashes( $arrValues['tax_label'] ) ) ); elseif( isset( $arrValues['tax_label'] ) ) $this->setTaxLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_label'] ) : $arrValues['tax_label'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyWebsiteId( $intPropertyWebsiteId ) {
		$this->set( 'm_intPropertyWebsiteId', CStrings::strToIntDef( $intPropertyWebsiteId, NULL, false ) );
	}

	public function getPropertyWebsiteId() {
		return $this->m_intPropertyWebsiteId;
	}

	public function sqlPropertyWebsiteId() {
		return ( true == isset( $this->m_intPropertyWebsiteId ) ) ? ( string ) $this->m_intPropertyWebsiteId : 'NULL';
	}

	public function setCorporateWebsiteId( $intCorporateWebsiteId ) {
		$this->set( 'm_intCorporateWebsiteId', CStrings::strToIntDef( $intCorporateWebsiteId, NULL, false ) );
	}

	public function getCorporateWebsiteId() {
		return $this->m_intCorporateWebsiteId;
	}

	public function sqlCorporateWebsiteId() {
		return ( true == isset( $this->m_intCorporateWebsiteId ) ) ? ( string ) $this->m_intCorporateWebsiteId : 'NULL';
	}

	public function setRouteId( $intRouteId ) {
		$this->set( 'm_intRouteId', CStrings::strToIntDef( $intRouteId, NULL, false ) );
	}

	public function getRouteId() {
		return $this->m_intRouteId;
	}

	public function sqlRouteId() {
		return ( true == isset( $this->m_intRouteId ) ) ? ( string ) $this->m_intRouteId : 'NULL';
	}

	public function setManagerialPropertyId( $intManagerialPropertyId ) {
		$this->set( 'm_intManagerialPropertyId', CStrings::strToIntDef( $intManagerialPropertyId, NULL, false ) );
	}

	public function getManagerialPropertyId() {
		return $this->m_intManagerialPropertyId;
	}

	public function sqlManagerialPropertyId() {
		return ( true == isset( $this->m_intManagerialPropertyId ) ) ? ( string ) $this->m_intManagerialPropertyId : 'NULL';
	}

	public function setJobCashFlowFrequencyId( $intJobCashFlowFrequencyId ) {
		$this->set( 'm_intJobCashFlowFrequencyId', CStrings::strToIntDef( $intJobCashFlowFrequencyId, NULL, false ) );
	}

	public function getJobCashFlowFrequencyId() {
		return $this->m_intJobCashFlowFrequencyId;
	}

	public function sqlJobCashFlowFrequencyId() {
		return ( true == isset( $this->m_intJobCashFlowFrequencyId ) ) ? ( string ) $this->m_intJobCashFlowFrequencyId : '6';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setMilitaryMacId( $strMilitaryMacId ) {
		$this->set( 'm_strMilitaryMacId', CStrings::strTrimDef( $strMilitaryMacId, 240, NULL, true ) );
	}

	public function getMilitaryMacId() {
		return $this->m_strMilitaryMacId;
	}

	public function sqlMilitaryMacId() {
		return ( true == isset( $this->m_strMilitaryMacId ) ) ? '\'' . addslashes( $this->m_strMilitaryMacId ) . '\'' : 'NULL';
	}

	public function setMilitaryInstallationMacId( $strMilitaryInstallationMacId ) {
		$this->set( 'm_strMilitaryInstallationMacId', CStrings::strTrimDef( $strMilitaryInstallationMacId, 240, NULL, true ) );
	}

	public function getMilitaryInstallationMacId() {
		return $this->m_strMilitaryInstallationMacId;
	}

	public function sqlMilitaryInstallationMacId() {
		return ( true == isset( $this->m_strMilitaryInstallationMacId ) ) ? '\'' . addslashes( $this->m_strMilitaryInstallationMacId ) . '\'' : 'NULL';
	}

	public function setMilitaryInstallationId( $intMilitaryInstallationId ) {
		$this->set( 'm_intMilitaryInstallationId', CStrings::strToIntDef( $intMilitaryInstallationId, NULL, false ) );
	}

	public function getMilitaryInstallationId() {
		return $this->m_intMilitaryInstallationId;
	}

	public function sqlMilitaryInstallationId() {
		return ( true == isset( $this->m_intMilitaryInstallationId ) ) ? ( string ) $this->m_intMilitaryInstallationId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ), $strLocaleCode );
	}

	public function getTaxNumberEncrypted( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTaxNumberEncrypted', $strLocaleCode );
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->set( 'm_strTaxNumberMasked', CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true ) );
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function sqlTaxNumberMasked() {
		return ( true == isset( $this->m_strTaxNumberMasked ) ) ? '\'' . addslashes( $this->m_strTaxNumberMasked ) . '\'' : 'NULL';
	}

	public function setCommunityWebsiteUri( $strCommunityWebsiteUri ) {
		$this->set( 'm_strCommunityWebsiteUri', CStrings::strTrimDef( $strCommunityWebsiteUri, 4096, NULL, true ) );
	}

	public function getCommunityWebsiteUri() {
		return $this->m_strCommunityWebsiteUri;
	}

	public function sqlCommunityWebsiteUri() {
		return ( true == isset( $this->m_strCommunityWebsiteUri ) ) ? '\'' . addslashes( $this->m_strCommunityWebsiteUri ) . '\'' : 'NULL';
	}

	public function setCorporateWebsiteUri( $strCorporateWebsiteUri ) {
		$this->set( 'm_strCorporateWebsiteUri', CStrings::strTrimDef( $strCorporateWebsiteUri, 4096, NULL, true ) );
	}

	public function getCorporateWebsiteUri() {
		return $this->m_strCorporateWebsiteUri;
	}

	public function sqlCorporateWebsiteUri() {
		return ( true == isset( $this->m_strCorporateWebsiteUri ) ) ? '\'' . addslashes( $this->m_strCorporateWebsiteUri ) . '\'' : 'NULL';
	}

	public function setShowAppointmentResults( $intShowAppointmentResults ) {
		$this->set( 'm_intShowAppointmentResults', CStrings::strToIntDef( $intShowAppointmentResults, NULL, false ) );
	}

	public function getShowAppointmentResults() {
		return $this->m_intShowAppointmentResults;
	}

	public function sqlShowAppointmentResults() {
		return ( true == isset( $this->m_intShowAppointmentResults ) ) ? ( string ) $this->m_intShowAppointmentResults : '0';
	}

	public function setShowLeadOutreachResults( $intShowLeadOutreachResults ) {
		$this->set( 'm_intShowLeadOutreachResults', CStrings::strToIntDef( $intShowLeadOutreachResults, NULL, false ) );
	}

	public function getShowLeadOutreachResults() {
		return $this->m_intShowLeadOutreachResults;
	}

	public function sqlShowLeadOutreachResults() {
		return ( true == isset( $this->m_intShowLeadOutreachResults ) ) ? ( string ) $this->m_intShowLeadOutreachResults : '0';
	}

	public function setShowRenewalOutreachResults( $intShowRenewalOutreachResults ) {
		$this->set( 'm_intShowRenewalOutreachResults', CStrings::strToIntDef( $intShowRenewalOutreachResults, NULL, false ) );
	}

	public function getShowRenewalOutreachResults() {
		return $this->m_intShowRenewalOutreachResults;
	}

	public function sqlShowRenewalOutreachResults() {
		return ( true == isset( $this->m_intShowRenewalOutreachResults ) ) ? ( string ) $this->m_intShowRenewalOutreachResults : '0';
	}

	public function setShowCollectionsResults( $intShowCollectionsResults ) {
		$this->set( 'm_intShowCollectionsResults', CStrings::strToIntDef( $intShowCollectionsResults, NULL, false ) );
	}

	public function getShowCollectionsResults() {
		return $this->m_intShowCollectionsResults;
	}

	public function sqlShowCollectionsResults() {
		return ( true == isset( $this->m_intShowCollectionsResults ) ) ? ( string ) $this->m_intShowCollectionsResults : '0';
	}

	public function setParkingPolicy( $strParkingPolicy, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strParkingPolicy', CStrings::strTrimDef( $strParkingPolicy, 2000, NULL, true ), $strLocaleCode );
	}

	public function getParkingPolicy( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strParkingPolicy', $strLocaleCode );
	}

	public function sqlParkingPolicy() {
		return ( true == isset( $this->m_strParkingPolicy ) ) ? '\'' . addslashes( $this->m_strParkingPolicy ) . '\'' : 'NULL';
	}

	public function setPetPolicy( $strPetPolicy, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPetPolicy', CStrings::strTrimDef( $strPetPolicy, 2000, NULL, true ), $strLocaleCode );
	}

	public function getPetPolicy( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPetPolicy', $strLocaleCode );
	}

	public function sqlPetPolicy() {
		return ( true == isset( $this->m_strPetPolicy ) ) ? '\'' . addslashes( $this->m_strPetPolicy ) . '\'' : 'NULL';
	}

	public function setSmokingPolicy( $strSmokingPolicy, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSmokingPolicy', CStrings::strTrimDef( $strSmokingPolicy, 2000, NULL, true ), $strLocaleCode );
	}

	public function getSmokingPolicy( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSmokingPolicy', $strLocaleCode );
	}

	public function sqlSmokingPolicy() {
		return ( true == isset( $this->m_strSmokingPolicy ) ) ? '\'' . addslashes( $this->m_strSmokingPolicy ) . '\'' : 'NULL';
	}

	public function setRenterInsurancePolicy( $strRenterInsurancePolicy, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRenterInsurancePolicy', CStrings::strTrimDef( $strRenterInsurancePolicy, 2000, NULL, true ), $strLocaleCode );
	}

	public function getRenterInsurancePolicy( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRenterInsurancePolicy', $strLocaleCode );
	}

	public function sqlRenterInsurancePolicy() {
		return ( true == isset( $this->m_strRenterInsurancePolicy ) ) ? '\'' . addslashes( $this->m_strRenterInsurancePolicy ) . '\'' : 'NULL';
	}

	public function setSchoolDescription( $strSchoolDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strSchoolDescription', CStrings::strTrimDef( $strSchoolDescription, 2000, NULL, true ), $strLocaleCode );
	}

	public function getSchoolDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strSchoolDescription', $strLocaleCode );
	}

	public function sqlSchoolDescription() {
		return ( true == isset( $this->m_strSchoolDescription ) ) ? '\'' . addslashes( $this->m_strSchoolDescription ) . '\'' : 'NULL';
	}

	public function setTaxNameOnTaxReturn( $strTaxNameOnTaxReturn ) {
		$this->set( 'm_strTaxNameOnTaxReturn', CStrings::strTrimDef( $strTaxNameOnTaxReturn, 200, NULL, true ) );
	}

	public function getTaxNameOnTaxReturn() {
		return $this->m_strTaxNameOnTaxReturn;
	}

	public function sqlTaxNameOnTaxReturn() {
		return ( true == isset( $this->m_strTaxNameOnTaxReturn ) ) ? '\'' . addslashes( $this->m_strTaxNameOnTaxReturn ) . '\'' : 'NULL';
	}

	public function setTaxContactPerson( $strTaxContactPerson ) {
		$this->set( 'm_strTaxContactPerson', CStrings::strTrimDef( $strTaxContactPerson, 100, NULL, true ) );
	}

	public function getTaxContactPerson() {
		return $this->m_strTaxContactPerson;
	}

	public function sqlTaxContactPerson() {
		return ( true == isset( $this->m_strTaxContactPerson ) ) ? '\'' . addslashes( $this->m_strTaxContactPerson ) . '\'' : 'NULL';
	}

	public function setTaxStreetAddressLineOne( $strTaxStreetAddressLineOne ) {
		$this->set( 'm_strTaxStreetAddressLineOne', CStrings::strTrimDef( $strTaxStreetAddressLineOne, 100, NULL, true ) );
	}

	public function getTaxStreetAddressLineOne() {
		return $this->m_strTaxStreetAddressLineOne;
	}

	public function sqlTaxStreetAddressLineOne() {
		return ( true == isset( $this->m_strTaxStreetAddressLineOne ) ) ? '\'' . addslashes( $this->m_strTaxStreetAddressLineOne ) . '\'' : 'NULL';
	}

	public function setTaxStreetAddressLineTwo( $strTaxStreetAddressLineTwo ) {
		$this->set( 'm_strTaxStreetAddressLineTwo', CStrings::strTrimDef( $strTaxStreetAddressLineTwo, 100, NULL, true ) );
	}

	public function getTaxStreetAddressLineTwo() {
		return $this->m_strTaxStreetAddressLineTwo;
	}

	public function sqlTaxStreetAddressLineTwo() {
		return ( true == isset( $this->m_strTaxStreetAddressLineTwo ) ) ? '\'' . addslashes( $this->m_strTaxStreetAddressLineTwo ) . '\'' : 'NULL';
	}

	public function setTaxCity( $strTaxCity ) {
		$this->set( 'm_strTaxCity', CStrings::strTrimDef( $strTaxCity, 50, NULL, true ) );
	}

	public function getTaxCity() {
		return $this->m_strTaxCity;
	}

	public function sqlTaxCity() {
		return ( true == isset( $this->m_strTaxCity ) ) ? '\'' . addslashes( $this->m_strTaxCity ) . '\'' : 'NULL';
	}

	public function setTaxStateCode( $strTaxStateCode ) {
		$this->set( 'm_strTaxStateCode', CStrings::strTrimDef( $strTaxStateCode, 2, NULL, true ) );
	}

	public function getTaxStateCode() {
		return $this->m_strTaxStateCode;
	}

	public function sqlTaxStateCode() {
		return ( true == isset( $this->m_strTaxStateCode ) ) ? '\'' . addslashes( $this->m_strTaxStateCode ) . '\'' : 'NULL';
	}

	public function setTaxProvince( $strTaxProvince ) {
		$this->set( 'm_strTaxProvince', CStrings::strTrimDef( $strTaxProvince, 50, NULL, true ) );
	}

	public function getTaxProvince() {
		return $this->m_strTaxProvince;
	}

	public function sqlTaxProvince() {
		return ( true == isset( $this->m_strTaxProvince ) ) ? '\'' . addslashes( $this->m_strTaxProvince ) . '\'' : 'NULL';
	}

	public function setTaxCountryCode( $strTaxCountryCode ) {
		$this->set( 'm_strTaxCountryCode', CStrings::strTrimDef( $strTaxCountryCode, 2, NULL, true ) );
	}

	public function getTaxCountryCode() {
		return $this->m_strTaxCountryCode;
	}

	public function sqlTaxCountryCode() {
		return ( true == isset( $this->m_strTaxCountryCode ) ) ? '\'' . addslashes( $this->m_strTaxCountryCode ) . '\'' : 'NULL';
	}

	public function setTaxPostalCode( $strTaxPostalCode ) {
		$this->set( 'm_strTaxPostalCode', CStrings::strTrimDef( $strTaxPostalCode, 20, NULL, true ) );
	}

	public function getTaxPostalCode() {
		return $this->m_strTaxPostalCode;
	}

	public function sqlTaxPostalCode() {
		return ( true == isset( $this->m_strTaxPostalCode ) ) ? '\'' . addslashes( $this->m_strTaxPostalCode ) . '\'' : 'NULL';
	}

	public function setTaxPhoneNumber( $strTaxPhoneNumber ) {
		$this->set( 'm_strTaxPhoneNumber', CStrings::strTrimDef( $strTaxPhoneNumber, 30, NULL, true ) );
	}

	public function getTaxPhoneNumber() {
		return $this->m_strTaxPhoneNumber;
	}

	public function sqlTaxPhoneNumber() {
		return ( true == isset( $this->m_strTaxPhoneNumber ) ) ? '\'' . addslashes( $this->m_strTaxPhoneNumber ) . '\'' : 'NULL';
	}

	public function setTaxFax( $strTaxFax ) {
		$this->set( 'm_strTaxFax', CStrings::strTrimDef( $strTaxFax, 30, NULL, true ) );
	}

	public function getTaxFax() {
		return $this->m_strTaxFax;
	}

	public function sqlTaxFax() {
		return ( true == isset( $this->m_strTaxFax ) ) ? '\'' . addslashes( $this->m_strTaxFax ) . '\'' : 'NULL';
	}

	public function setTaxEmail( $strTaxEmail ) {
		$this->set( 'm_strTaxEmail', CStrings::strTrimDef( $strTaxEmail, 240, NULL, true ) );
	}

	public function getTaxEmail() {
		return $this->m_strTaxEmail;
	}

	public function sqlTaxEmail() {
		return ( true == isset( $this->m_strTaxEmail ) ) ? '\'' . addslashes( $this->m_strTaxEmail ) . '\'' : 'NULL';
	}

	public function setAllowsSmoking( $intAllowsSmoking ) {
		$this->set( 'm_intAllowsSmoking', CStrings::strToIntDef( $intAllowsSmoking, NULL, false ) );
	}

	public function getAllowsSmoking() {
		return $this->m_intAllowsSmoking;
	}

	public function sqlAllowsSmoking() {
		return ( true == isset( $this->m_intAllowsSmoking ) ) ? ( string ) $this->m_intAllowsSmoking : '2';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdateVacancyAmenities( $boolUpdateVacancyAmenities ) {
		$this->set( 'm_boolUpdateVacancyAmenities', CStrings::strToBool( $boolUpdateVacancyAmenities ) );
	}

	public function getUpdateVacancyAmenities() {
		return $this->m_boolUpdateVacancyAmenities;
	}

	public function sqlUpdateVacancyAmenities() {
		return ( true == isset( $this->m_boolUpdateVacancyAmenities ) ) ? '\'' . ( true == ( bool ) $this->m_boolUpdateVacancyAmenities ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargesLastPostedOn( $strChargesLastPostedOn ) {
		$this->set( 'm_strChargesLastPostedOn', CStrings::strTrimDef( $strChargesLastPostedOn, -1, NULL, true ) );
	}

	public function getChargesLastPostedOn() {
		return $this->m_strChargesLastPostedOn;
	}

	public function sqlChargesLastPostedOn() {
		return ( true == isset( $this->m_strChargesLastPostedOn ) ) ? '\'' . $this->m_strChargesLastPostedOn . '\'' : 'NOW()';
	}

	public function setLateFeeLastPostedOn( $strLateFeeLastPostedOn ) {
		$this->set( 'm_strLateFeeLastPostedOn', CStrings::strTrimDef( $strLateFeeLastPostedOn, -1, NULL, true ) );
	}

	public function getLateFeeLastPostedOn() {
		return $this->m_strLateFeeLastPostedOn;
	}

	public function sqlLateFeeLastPostedOn() {
		return ( true == isset( $this->m_strLateFeeLastPostedOn ) ) ? '\'' . $this->m_strLateFeeLastPostedOn . '\'' : 'NULL';
	}

	public function setLastAdminSyncOn( $strLastAdminSyncOn ) {
		$this->set( 'm_strLastAdminSyncOn', CStrings::strTrimDef( $strLastAdminSyncOn, -1, NULL, true ) );
	}

	public function getLastAdminSyncOn() {
		return $this->m_strLastAdminSyncOn;
	}

	public function sqlLastAdminSyncOn() {
		return ( true == isset( $this->m_strLastAdminSyncOn ) ) ? '\'' . $this->m_strLastAdminSyncOn . '\'' : 'NULL';
	}

	public function setLateNoticesLastAutoSentOn( $strLateNoticesLastAutoSentOn ) {
		$this->set( 'm_strLateNoticesLastAutoSentOn', CStrings::strTrimDef( $strLateNoticesLastAutoSentOn, -1, NULL, true ) );
	}

	public function getLateNoticesLastAutoSentOn() {
		return $this->m_strLateNoticesLastAutoSentOn;
	}

	public function sqlLateNoticesLastAutoSentOn() {
		return ( true == isset( $this->m_strLateNoticesLastAutoSentOn ) ) ? '\'' . $this->m_strLateNoticesLastAutoSentOn . '\'' : 'NULL';
	}

	public function setCollectionsNoticesLastAutoSentOn( $strCollectionsNoticesLastAutoSentOn ) {
		$this->set( 'm_strCollectionsNoticesLastAutoSentOn', CStrings::strTrimDef( $strCollectionsNoticesLastAutoSentOn, -1, NULL, true ) );
	}

	public function getCollectionsNoticesLastAutoSentOn() {
		return $this->m_strCollectionsNoticesLastAutoSentOn;
	}

	public function sqlCollectionsNoticesLastAutoSentOn() {
		return ( true == isset( $this->m_strCollectionsNoticesLastAutoSentOn ) ) ? '\'' . $this->m_strCollectionsNoticesLastAutoSentOn . '\'' : 'NULL';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTaxLabel( $strTaxLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTaxLabel', CStrings::strTrimDef( $strTaxLabel, 100, NULL, true ), $strLocaleCode );
	}

	public function getTaxLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTaxLabel', $strLocaleCode );
	}

	public function sqlTaxLabel() {
		return ( true == isset( $this->m_strTaxLabel ) ) ? '\'' . addslashes( $this->m_strTaxLabel ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_website_id, corporate_website_id, route_id, managerial_property_id, job_cash_flow_frequency_id, ap_payee_id, customer_id, military_mac_id, military_installation_mac_id, military_installation_id, remote_primary_key, tax_number_encrypted, tax_number_masked, community_website_uri, corporate_website_uri, show_appointment_results, show_lead_outreach_results, show_renewal_outreach_results, show_collections_results, parking_policy, pet_policy, smoking_policy, renter_insurance_policy, school_description, tax_name_on_tax_return, tax_contact_person, tax_street_address_line_one, tax_street_address_line_two, tax_city, tax_state_code, tax_province, tax_country_code, tax_postal_code, tax_phone_number, tax_fax, tax_email, allows_smoking, is_featured, update_vacancy_amenities, charges_last_posted_on, late_fee_last_posted_on, last_admin_sync_on, late_notices_last_auto_sent_on, collections_notices_last_auto_sent_on, imported_on, updated_by, updated_on, created_by, created_on, details, tax_label )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyWebsiteId() . ', ' .
						$this->sqlCorporateWebsiteId() . ', ' .
						$this->sqlRouteId() . ', ' .
						$this->sqlManagerialPropertyId() . ', ' .
						$this->sqlJobCashFlowFrequencyId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlMilitaryMacId() . ', ' .
						$this->sqlMilitaryInstallationMacId() . ', ' .
						$this->sqlMilitaryInstallationId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlCommunityWebsiteUri() . ', ' .
						$this->sqlCorporateWebsiteUri() . ', ' .
						$this->sqlShowAppointmentResults() . ', ' .
						$this->sqlShowLeadOutreachResults() . ', ' .
						$this->sqlShowRenewalOutreachResults() . ', ' .
						$this->sqlShowCollectionsResults() . ', ' .
						$this->sqlParkingPolicy() . ', ' .
						$this->sqlPetPolicy() . ', ' .
						$this->sqlSmokingPolicy() . ', ' .
						$this->sqlRenterInsurancePolicy() . ', ' .
						$this->sqlSchoolDescription() . ', ' .
						$this->sqlTaxNameOnTaxReturn() . ', ' .
						$this->sqlTaxContactPerson() . ', ' .
						$this->sqlTaxStreetAddressLineOne() . ', ' .
						$this->sqlTaxStreetAddressLineTwo() . ', ' .
						$this->sqlTaxCity() . ', ' .
						$this->sqlTaxStateCode() . ', ' .
						$this->sqlTaxProvince() . ', ' .
						$this->sqlTaxCountryCode() . ', ' .
						$this->sqlTaxPostalCode() . ', ' .
						$this->sqlTaxPhoneNumber() . ', ' .
						$this->sqlTaxFax() . ', ' .
						$this->sqlTaxEmail() . ', ' .
						$this->sqlAllowsSmoking() . ', ' .
						$this->sqlIsFeatured() . ', ' .
						$this->sqlUpdateVacancyAmenities() . ', ' .
						$this->sqlChargesLastPostedOn() . ', ' .
						$this->sqlLateFeeLastPostedOn() . ', ' .
						$this->sqlLastAdminSyncOn() . ', ' .
						$this->sqlLateNoticesLastAutoSentOn() . ', ' .
						$this->sqlCollectionsNoticesLastAutoSentOn() . ', ' .
						$this->sqlImportedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTaxLabel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_website_id = ' . $this->sqlPropertyWebsiteId(). ',' ; } elseif( true == array_key_exists( 'PropertyWebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' property_website_id = ' . $this->sqlPropertyWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corporate_website_id = ' . $this->sqlCorporateWebsiteId(). ',' ; } elseif( true == array_key_exists( 'CorporateWebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' corporate_website_id = ' . $this->sqlCorporateWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_id = ' . $this->sqlRouteId(). ',' ; } elseif( true == array_key_exists( 'RouteId', $this->getChangedColumns() ) ) { $strSql .= ' route_id = ' . $this->sqlRouteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' managerial_property_id = ' . $this->sqlManagerialPropertyId(). ',' ; } elseif( true == array_key_exists( 'ManagerialPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' managerial_property_id = ' . $this->sqlManagerialPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_cash_flow_frequency_id = ' . $this->sqlJobCashFlowFrequencyId(). ',' ; } elseif( true == array_key_exists( 'JobCashFlowFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' job_cash_flow_frequency_id = ' . $this->sqlJobCashFlowFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_mac_id = ' . $this->sqlMilitaryMacId(). ',' ; } elseif( true == array_key_exists( 'MilitaryMacId', $this->getChangedColumns() ) ) { $strSql .= ' military_mac_id = ' . $this->sqlMilitaryMacId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_installation_mac_id = ' . $this->sqlMilitaryInstallationMacId(). ',' ; } elseif( true == array_key_exists( 'MilitaryInstallationMacId', $this->getChangedColumns() ) ) { $strSql .= ' military_installation_mac_id = ' . $this->sqlMilitaryInstallationMacId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_installation_id = ' . $this->sqlMilitaryInstallationId(). ',' ; } elseif( true == array_key_exists( 'MilitaryInstallationId', $this->getChangedColumns() ) ) { $strSql .= ' military_installation_id = ' . $this->sqlMilitaryInstallationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked(). ',' ; } elseif( true == array_key_exists( 'TaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' community_website_uri = ' . $this->sqlCommunityWebsiteUri(). ',' ; } elseif( true == array_key_exists( 'CommunityWebsiteUri', $this->getChangedColumns() ) ) { $strSql .= ' community_website_uri = ' . $this->sqlCommunityWebsiteUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corporate_website_uri = ' . $this->sqlCorporateWebsiteUri(). ',' ; } elseif( true == array_key_exists( 'CorporateWebsiteUri', $this->getChangedColumns() ) ) { $strSql .= ' corporate_website_uri = ' . $this->sqlCorporateWebsiteUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_appointment_results = ' . $this->sqlShowAppointmentResults(). ',' ; } elseif( true == array_key_exists( 'ShowAppointmentResults', $this->getChangedColumns() ) ) { $strSql .= ' show_appointment_results = ' . $this->sqlShowAppointmentResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_lead_outreach_results = ' . $this->sqlShowLeadOutreachResults(). ',' ; } elseif( true == array_key_exists( 'ShowLeadOutreachResults', $this->getChangedColumns() ) ) { $strSql .= ' show_lead_outreach_results = ' . $this->sqlShowLeadOutreachResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_renewal_outreach_results = ' . $this->sqlShowRenewalOutreachResults(). ',' ; } elseif( true == array_key_exists( 'ShowRenewalOutreachResults', $this->getChangedColumns() ) ) { $strSql .= ' show_renewal_outreach_results = ' . $this->sqlShowRenewalOutreachResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_collections_results = ' . $this->sqlShowCollectionsResults(). ',' ; } elseif( true == array_key_exists( 'ShowCollectionsResults', $this->getChangedColumns() ) ) { $strSql .= ' show_collections_results = ' . $this->sqlShowCollectionsResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parking_policy = ' . $this->sqlParkingPolicy(). ',' ; } elseif( true == array_key_exists( 'ParkingPolicy', $this->getChangedColumns() ) ) { $strSql .= ' parking_policy = ' . $this->sqlParkingPolicy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_policy = ' . $this->sqlPetPolicy(). ',' ; } elseif( true == array_key_exists( 'PetPolicy', $this->getChangedColumns() ) ) { $strSql .= ' pet_policy = ' . $this->sqlPetPolicy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' smoking_policy = ' . $this->sqlSmokingPolicy(). ',' ; } elseif( true == array_key_exists( 'SmokingPolicy', $this->getChangedColumns() ) ) { $strSql .= ' smoking_policy = ' . $this->sqlSmokingPolicy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renter_insurance_policy = ' . $this->sqlRenterInsurancePolicy(). ',' ; } elseif( true == array_key_exists( 'RenterInsurancePolicy', $this->getChangedColumns() ) ) { $strSql .= ' renter_insurance_policy = ' . $this->sqlRenterInsurancePolicy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' school_description = ' . $this->sqlSchoolDescription(). ',' ; } elseif( true == array_key_exists( 'SchoolDescription', $this->getChangedColumns() ) ) { $strSql .= ' school_description = ' . $this->sqlSchoolDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_name_on_tax_return = ' . $this->sqlTaxNameOnTaxReturn(). ',' ; } elseif( true == array_key_exists( 'TaxNameOnTaxReturn', $this->getChangedColumns() ) ) { $strSql .= ' tax_name_on_tax_return = ' . $this->sqlTaxNameOnTaxReturn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_contact_person = ' . $this->sqlTaxContactPerson(). ',' ; } elseif( true == array_key_exists( 'TaxContactPerson', $this->getChangedColumns() ) ) { $strSql .= ' tax_contact_person = ' . $this->sqlTaxContactPerson() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_street_address_line_one = ' . $this->sqlTaxStreetAddressLineOne(). ',' ; } elseif( true == array_key_exists( 'TaxStreetAddressLineOne', $this->getChangedColumns() ) ) { $strSql .= ' tax_street_address_line_one = ' . $this->sqlTaxStreetAddressLineOne() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_street_address_line_two = ' . $this->sqlTaxStreetAddressLineTwo(). ',' ; } elseif( true == array_key_exists( 'TaxStreetAddressLineTwo', $this->getChangedColumns() ) ) { $strSql .= ' tax_street_address_line_two = ' . $this->sqlTaxStreetAddressLineTwo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_city = ' . $this->sqlTaxCity(). ',' ; } elseif( true == array_key_exists( 'TaxCity', $this->getChangedColumns() ) ) { $strSql .= ' tax_city = ' . $this->sqlTaxCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_state_code = ' . $this->sqlTaxStateCode(). ',' ; } elseif( true == array_key_exists( 'TaxStateCode', $this->getChangedColumns() ) ) { $strSql .= ' tax_state_code = ' . $this->sqlTaxStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_province = ' . $this->sqlTaxProvince(). ',' ; } elseif( true == array_key_exists( 'TaxProvince', $this->getChangedColumns() ) ) { $strSql .= ' tax_province = ' . $this->sqlTaxProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_country_code = ' . $this->sqlTaxCountryCode(). ',' ; } elseif( true == array_key_exists( 'TaxCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' tax_country_code = ' . $this->sqlTaxCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_postal_code = ' . $this->sqlTaxPostalCode(). ',' ; } elseif( true == array_key_exists( 'TaxPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' tax_postal_code = ' . $this->sqlTaxPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_phone_number = ' . $this->sqlTaxPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'TaxPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' tax_phone_number = ' . $this->sqlTaxPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_fax = ' . $this->sqlTaxFax(). ',' ; } elseif( true == array_key_exists( 'TaxFax', $this->getChangedColumns() ) ) { $strSql .= ' tax_fax = ' . $this->sqlTaxFax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_email = ' . $this->sqlTaxEmail(). ',' ; } elseif( true == array_key_exists( 'TaxEmail', $this->getChangedColumns() ) ) { $strSql .= ' tax_email = ' . $this->sqlTaxEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_smoking = ' . $this->sqlAllowsSmoking(). ',' ; } elseif( true == array_key_exists( 'AllowsSmoking', $this->getChangedColumns() ) ) { $strSql .= ' allows_smoking = ' . $this->sqlAllowsSmoking() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured(). ',' ; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' update_vacancy_amenities = ' . $this->sqlUpdateVacancyAmenities(). ',' ; } elseif( true == array_key_exists( 'UpdateVacancyAmenities', $this->getChangedColumns() ) ) { $strSql .= ' update_vacancy_amenities = ' . $this->sqlUpdateVacancyAmenities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charges_last_posted_on = ' . $this->sqlChargesLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'ChargesLastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' charges_last_posted_on = ' . $this->sqlChargesLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_last_posted_on = ' . $this->sqlLateFeeLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LateFeeLastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_last_posted_on = ' . $this->sqlLateFeeLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_admin_sync_on = ' . $this->sqlLastAdminSyncOn(). ',' ; } elseif( true == array_key_exists( 'LastAdminSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_admin_sync_on = ' . $this->sqlLastAdminSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_notices_last_auto_sent_on = ' . $this->sqlLateNoticesLastAutoSentOn(). ',' ; } elseif( true == array_key_exists( 'LateNoticesLastAutoSentOn', $this->getChangedColumns() ) ) { $strSql .= ' late_notices_last_auto_sent_on = ' . $this->sqlLateNoticesLastAutoSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_notices_last_auto_sent_on = ' . $this->sqlCollectionsNoticesLastAutoSentOn(). ',' ; } elseif( true == array_key_exists( 'CollectionsNoticesLastAutoSentOn', $this->getChangedColumns() ) ) { $strSql .= ' collections_notices_last_auto_sent_on = ' . $this->sqlCollectionsNoticesLastAutoSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_label = ' . $this->sqlTaxLabel(). ',' ; } elseif( true == array_key_exists( 'TaxLabel', $this->getChangedColumns() ) ) { $strSql .= ' tax_label = ' . $this->sqlTaxLabel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_website_id' => $this->getPropertyWebsiteId(),
			'corporate_website_id' => $this->getCorporateWebsiteId(),
			'route_id' => $this->getRouteId(),
			'managerial_property_id' => $this->getManagerialPropertyId(),
			'job_cash_flow_frequency_id' => $this->getJobCashFlowFrequencyId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'customer_id' => $this->getCustomerId(),
			'military_mac_id' => $this->getMilitaryMacId(),
			'military_installation_mac_id' => $this->getMilitaryInstallationMacId(),
			'military_installation_id' => $this->getMilitaryInstallationId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'tax_number_masked' => $this->getTaxNumberMasked(),
			'community_website_uri' => $this->getCommunityWebsiteUri(),
			'corporate_website_uri' => $this->getCorporateWebsiteUri(),
			'show_appointment_results' => $this->getShowAppointmentResults(),
			'show_lead_outreach_results' => $this->getShowLeadOutreachResults(),
			'show_renewal_outreach_results' => $this->getShowRenewalOutreachResults(),
			'show_collections_results' => $this->getShowCollectionsResults(),
			'parking_policy' => $this->getParkingPolicy(),
			'pet_policy' => $this->getPetPolicy(),
			'smoking_policy' => $this->getSmokingPolicy(),
			'renter_insurance_policy' => $this->getRenterInsurancePolicy(),
			'school_description' => $this->getSchoolDescription(),
			'tax_name_on_tax_return' => $this->getTaxNameOnTaxReturn(),
			'tax_contact_person' => $this->getTaxContactPerson(),
			'tax_street_address_line_one' => $this->getTaxStreetAddressLineOne(),
			'tax_street_address_line_two' => $this->getTaxStreetAddressLineTwo(),
			'tax_city' => $this->getTaxCity(),
			'tax_state_code' => $this->getTaxStateCode(),
			'tax_province' => $this->getTaxProvince(),
			'tax_country_code' => $this->getTaxCountryCode(),
			'tax_postal_code' => $this->getTaxPostalCode(),
			'tax_phone_number' => $this->getTaxPhoneNumber(),
			'tax_fax' => $this->getTaxFax(),
			'tax_email' => $this->getTaxEmail(),
			'allows_smoking' => $this->getAllowsSmoking(),
			'is_featured' => $this->getIsFeatured(),
			'update_vacancy_amenities' => $this->getUpdateVacancyAmenities(),
			'charges_last_posted_on' => $this->getChargesLastPostedOn(),
			'late_fee_last_posted_on' => $this->getLateFeeLastPostedOn(),
			'last_admin_sync_on' => $this->getLastAdminSyncOn(),
			'late_notices_last_auto_sent_on' => $this->getLateNoticesLastAutoSentOn(),
			'collections_notices_last_auto_sent_on' => $this->getCollectionsNoticesLastAutoSentOn(),
			'imported_on' => $this->getImportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'tax_label' => $this->getTaxLabel()
		);
	}

}
?>