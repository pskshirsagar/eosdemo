<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyOccupations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyOccupations extends CEosPluralBase {

	/**
	 * @return CPropertyOccupation[]
	 */
	public static function fetchPropertyOccupations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyOccupation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyOccupation
	 */
	public static function fetchPropertyOccupation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyOccupation', $objDatabase );
	}

	public static function fetchPropertyOccupationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_occupations', $objDatabase );
	}

	public static function fetchPropertyOccupationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyOccupation( sprintf( 'SELECT * FROM property_occupations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOccupationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyOccupations( sprintf( 'SELECT * FROM property_occupations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOccupationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyOccupations( sprintf( 'SELECT * FROM property_occupations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOccupationsByOccupationIdByCid( $intOccupationId, $intCid, $objDatabase ) {
		return self::fetchPropertyOccupations( sprintf( 'SELECT * FROM property_occupations WHERE occupation_id = %d AND cid = %d', ( int ) $intOccupationId, ( int ) $intCid ), $objDatabase );
	}

}
?>