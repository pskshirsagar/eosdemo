<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyEthnicityTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyEthnicityTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyEthnicityType[]
	 */
	public static function fetchSubsidyEthnicityTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyEthnicityType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyEthnicityType
	 */
	public static function fetchSubsidyEthnicityType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyEthnicityType::class, $objDatabase );
	}

	public static function fetchSubsidyEthnicityTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_ethnicity_types', $objDatabase );
	}

	public static function fetchSubsidyEthnicityTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyEthnicityType( sprintf( 'SELECT * FROM subsidy_ethnicity_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>