<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyOnSiteContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyOnSiteContacts extends CEosPluralBase {

	/**
	 * @return CPropertyOnSiteContact[]
	 */
	public static function fetchPropertyOnSiteContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyOnSiteContact', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyOnSiteContact
	 */
	public static function fetchPropertyOnSiteContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyOnSiteContact', $objDatabase );
	}

	public static function fetchPropertyOnSiteContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_on_site_contacts', $objDatabase );
	}

	public static function fetchPropertyOnSiteContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContact( sprintf( 'SELECT * FROM property_on_site_contacts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContacts( sprintf( 'SELECT * FROM property_on_site_contacts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContacts( sprintf( 'SELECT * FROM property_on_site_contacts WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyOnSiteContactsByOnSiteContactTypeIdByCid( $intOnSiteContactTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyOnSiteContacts( sprintf( 'SELECT * FROM property_on_site_contacts WHERE on_site_contact_type_id = %d AND cid = %d', ( int ) $intOnSiteContactTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>