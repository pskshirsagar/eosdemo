<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMetadataKeys
 * Do not add any new functions to this class.
 */

class CBaseFileMetadataKeys extends CEosPluralBase {

	/**
	 * @return CFileMetadataKey[]
	 */
	public static function fetchFileMetadataKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CFileMetadataKey', $objDatabase );
	}

	/**
	 * @return CFileMetadataKey
	 */
	public static function fetchFileMetadataKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFileMetadataKey', $objDatabase );
	}

	public static function fetchFileMetadataKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_metadata_keys', $objDatabase );
	}

	public static function fetchFileMetadataKeyById( $intId, $objDatabase ) {
		return self::fetchFileMetadataKey( sprintf( 'SELECT * FROM file_metadata_keys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>