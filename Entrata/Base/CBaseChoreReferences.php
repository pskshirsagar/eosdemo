<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreReferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreReferences extends CEosPluralBase {

	/**
	 * @return CChoreReference[]
	 */
	public static function fetchChoreReferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CChoreReference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChoreReference
	 */
	public static function fetchChoreReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChoreReference', $objDatabase );
	}

	public static function fetchChoreReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_references', $objDatabase );
	}

	public static function fetchChoreReferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChoreReference( sprintf( 'SELECT * FROM chore_references WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreReferencesByCid( $intCid, $objDatabase ) {
		return self::fetchChoreReferences( sprintf( 'SELECT * FROM chore_references WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreReferencesByChoreReferenceTypeIdByCid( $intChoreReferenceTypeId, $intCid, $objDatabase ) {
		return self::fetchChoreReferences( sprintf( 'SELECT * FROM chore_references WHERE chore_reference_type_id = %d AND cid = %d', ( int ) $intChoreReferenceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreReferencesByChoreIdByCid( $intChoreId, $intCid, $objDatabase ) {
		return self::fetchChoreReferences( sprintf( 'SELECT * FROM chore_references WHERE chore_id = %d AND cid = %d', ( int ) $intChoreId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreReferencesByTaskNoteIdByCid( $intTaskNoteId, $intCid, $objDatabase ) {
		return self::fetchChoreReferences( sprintf( 'SELECT * FROM chore_references WHERE task_note_id = %d AND cid = %d', ( int ) $intTaskNoteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreReferencesByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchChoreReferences( sprintf( 'SELECT * FROM chore_references WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>