<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOverrideRents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueOverrideRents extends CEosPluralBase {

	/**
	 * @return CRevenueOverrideRent[]
	 */
	public static function fetchRevenueOverrideRents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenueOverrideRent::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueOverrideRent
	 */
	public static function fetchRevenueOverrideRent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueOverrideRent::class, $objDatabase );
	}

	public static function fetchRevenueOverrideRentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_override_rents', $objDatabase );
	}

	public static function fetchRevenueOverrideRentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRent( sprintf( 'SELECT * FROM revenue_override_rents WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchRevenueOverrideRents( sprintf( 'SELECT * FROM revenue_override_rents WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>