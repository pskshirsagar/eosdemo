<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRepayment extends CEosSingularBase {

	const TABLE_NAME = 'public.repayments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intExternalId;
	protected $m_intArTriggerId;
	protected $m_intOffsetDays;
	protected $m_fltAgreementAmountTotal;
	protected $m_fltAgreementAmountDue;
	protected $m_fltCurrentBalance;
	protected $m_fltCurrentAmountDue;
	protected $m_strLastPaymentOn;
	protected $m_boolIsActive;
	protected $m_boolIsResident;
	protected $m_strFirstPaymentDueOn;
	protected $m_strStartsOn;
	protected $m_strEndsOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOffsetDays = '0';
		$this->m_boolIsActive = true;
		$this->m_boolIsResident = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['external_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalId', trim( $arrValues['external_id'] ) ); elseif( isset( $arrValues['external_id'] ) ) $this->setExternalId( $arrValues['external_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['offset_days'] ) && $boolDirectSet ) $this->set( 'm_intOffsetDays', trim( $arrValues['offset_days'] ) ); elseif( isset( $arrValues['offset_days'] ) ) $this->setOffsetDays( $arrValues['offset_days'] );
		if( isset( $arrValues['agreement_amount_total'] ) && $boolDirectSet ) $this->set( 'm_fltAgreementAmountTotal', trim( $arrValues['agreement_amount_total'] ) ); elseif( isset( $arrValues['agreement_amount_total'] ) ) $this->setAgreementAmountTotal( $arrValues['agreement_amount_total'] );
		if( isset( $arrValues['agreement_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltAgreementAmountDue', trim( $arrValues['agreement_amount_due'] ) ); elseif( isset( $arrValues['agreement_amount_due'] ) ) $this->setAgreementAmountDue( $arrValues['agreement_amount_due'] );
		if( isset( $arrValues['current_balance'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentBalance', trim( $arrValues['current_balance'] ) ); elseif( isset( $arrValues['current_balance'] ) ) $this->setCurrentBalance( $arrValues['current_balance'] );
		if( isset( $arrValues['current_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentAmountDue', trim( $arrValues['current_amount_due'] ) ); elseif( isset( $arrValues['current_amount_due'] ) ) $this->setCurrentAmountDue( $arrValues['current_amount_due'] );
		if( isset( $arrValues['last_payment_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPaymentOn', trim( $arrValues['last_payment_on'] ) ); elseif( isset( $arrValues['last_payment_on'] ) ) $this->setLastPaymentOn( $arrValues['last_payment_on'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_resident'] ) && $boolDirectSet ) $this->set( 'm_boolIsResident', trim( stripcslashes( $arrValues['is_resident'] ) ) ); elseif( isset( $arrValues['is_resident'] ) ) $this->setIsResident( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resident'] ) : $arrValues['is_resident'] );
		if( isset( $arrValues['first_payment_due_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstPaymentDueOn', trim( $arrValues['first_payment_due_on'] ) ); elseif( isset( $arrValues['first_payment_due_on'] ) ) $this->setFirstPaymentDueOn( $arrValues['first_payment_due_on'] );
		if( isset( $arrValues['starts_on'] ) && $boolDirectSet ) $this->set( 'm_strStartsOn', trim( $arrValues['starts_on'] ) ); elseif( isset( $arrValues['starts_on'] ) ) $this->setStartsOn( $arrValues['starts_on'] );
		if( isset( $arrValues['ends_on'] ) && $boolDirectSet ) $this->set( 'm_strEndsOn', trim( $arrValues['ends_on'] ) ); elseif( isset( $arrValues['ends_on'] ) ) $this->setEndsOn( $arrValues['ends_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setExternalId( $intExternalId ) {
		$this->set( 'm_intExternalId', CStrings::strToIntDef( $intExternalId, NULL, false ) );
	}

	public function getExternalId() {
		return $this->m_intExternalId;
	}

	public function sqlExternalId() {
		return ( true == isset( $this->m_intExternalId ) ) ? ( string ) $this->m_intExternalId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setOffsetDays( $intOffsetDays ) {
		$this->set( 'm_intOffsetDays', CStrings::strToIntDef( $intOffsetDays, NULL, false ) );
	}

	public function getOffsetDays() {
		return $this->m_intOffsetDays;
	}

	public function sqlOffsetDays() {
		return ( true == isset( $this->m_intOffsetDays ) ) ? ( string ) $this->m_intOffsetDays : '0';
	}

	public function setAgreementAmountTotal( $fltAgreementAmountTotal ) {
		$this->set( 'm_fltAgreementAmountTotal', CStrings::strToFloatDef( $fltAgreementAmountTotal, NULL, false, 2 ) );
	}

	public function getAgreementAmountTotal() {
		return $this->m_fltAgreementAmountTotal;
	}

	public function sqlAgreementAmountTotal() {
		return ( true == isset( $this->m_fltAgreementAmountTotal ) ) ? ( string ) $this->m_fltAgreementAmountTotal : 'NULL';
	}

	public function setAgreementAmountDue( $fltAgreementAmountDue ) {
		$this->set( 'm_fltAgreementAmountDue', CStrings::strToFloatDef( $fltAgreementAmountDue, NULL, false, 2 ) );
	}

	public function getAgreementAmountDue() {
		return $this->m_fltAgreementAmountDue;
	}

	public function sqlAgreementAmountDue() {
		return ( true == isset( $this->m_fltAgreementAmountDue ) ) ? ( string ) $this->m_fltAgreementAmountDue : 'NULL';
	}

	public function setCurrentBalance( $fltCurrentBalance ) {
		$this->set( 'm_fltCurrentBalance', CStrings::strToFloatDef( $fltCurrentBalance, NULL, false, 2 ) );
	}

	public function getCurrentBalance() {
		return $this->m_fltCurrentBalance;
	}

	public function sqlCurrentBalance() {
		return ( true == isset( $this->m_fltCurrentBalance ) ) ? ( string ) $this->m_fltCurrentBalance : 'NULL';
	}

	public function setCurrentAmountDue( $fltCurrentAmountDue ) {
		$this->set( 'm_fltCurrentAmountDue', CStrings::strToFloatDef( $fltCurrentAmountDue, NULL, false, 2 ) );
	}

	public function getCurrentAmountDue() {
		return $this->m_fltCurrentAmountDue;
	}

	public function sqlCurrentAmountDue() {
		return ( true == isset( $this->m_fltCurrentAmountDue ) ) ? ( string ) $this->m_fltCurrentAmountDue : 'NULL';
	}

	public function setLastPaymentOn( $strLastPaymentOn ) {
		$this->set( 'm_strLastPaymentOn', CStrings::strTrimDef( $strLastPaymentOn, -1, NULL, true ) );
	}

	public function getLastPaymentOn() {
		return $this->m_strLastPaymentOn;
	}

	public function sqlLastPaymentOn() {
		return ( true == isset( $this->m_strLastPaymentOn ) ) ? '\'' . $this->m_strLastPaymentOn . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResident( $boolIsResident ) {
		$this->set( 'm_boolIsResident', CStrings::strToBool( $boolIsResident ) );
	}

	public function getIsResident() {
		return $this->m_boolIsResident;
	}

	public function sqlIsResident() {
		return ( true == isset( $this->m_boolIsResident ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResident ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setFirstPaymentDueOn( $strFirstPaymentDueOn ) {
		$this->set( 'm_strFirstPaymentDueOn', CStrings::strTrimDef( $strFirstPaymentDueOn, -1, NULL, true ) );
	}

	public function getFirstPaymentDueOn() {
		return $this->m_strFirstPaymentDueOn;
	}

	public function sqlFirstPaymentDueOn() {
		return ( true == isset( $this->m_strFirstPaymentDueOn ) ) ? '\'' . $this->m_strFirstPaymentDueOn . '\'' : 'NOW()';
	}

	public function setStartsOn( $strStartsOn ) {
		$this->set( 'm_strStartsOn', CStrings::strTrimDef( $strStartsOn, -1, NULL, true ) );
	}

	public function getStartsOn() {
		return $this->m_strStartsOn;
	}

	public function sqlStartsOn() {
		return ( true == isset( $this->m_strStartsOn ) ) ? '\'' . $this->m_strStartsOn . '\'' : 'NOW()';
	}

	public function setEndsOn( $strEndsOn ) {
		$this->set( 'm_strEndsOn', CStrings::strTrimDef( $strEndsOn, -1, NULL, true ) );
	}

	public function getEndsOn() {
		return $this->m_strEndsOn;
	}

	public function sqlEndsOn() {
		return ( true == isset( $this->m_strEndsOn ) ) ? '\'' . $this->m_strEndsOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, external_id, ar_trigger_id, offset_days, agreement_amount_total, agreement_amount_due, current_balance, current_amount_due, last_payment_on, is_active, is_resident, first_payment_due_on, starts_on, ends_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlExternalId() . ', ' .
 						$this->sqlArTriggerId() . ', ' .
 						$this->sqlOffsetDays() . ', ' .
 						$this->sqlAgreementAmountTotal() . ', ' .
 						$this->sqlAgreementAmountDue() . ', ' .
 						$this->sqlCurrentBalance() . ', ' .
 						$this->sqlCurrentAmountDue() . ', ' .
 						$this->sqlLastPaymentOn() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlIsResident() . ', ' .
 						$this->sqlFirstPaymentDueOn() . ', ' .
 						$this->sqlStartsOn() . ', ' .
 						$this->sqlEndsOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; } elseif( true == array_key_exists( 'ExternalId', $this->getChangedColumns() ) ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offset_days = ' . $this->sqlOffsetDays() . ','; } elseif( true == array_key_exists( 'OffsetDays', $this->getChangedColumns() ) ) { $strSql .= ' offset_days = ' . $this->sqlOffsetDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_amount_total = ' . $this->sqlAgreementAmountTotal() . ','; } elseif( true == array_key_exists( 'AgreementAmountTotal', $this->getChangedColumns() ) ) { $strSql .= ' agreement_amount_total = ' . $this->sqlAgreementAmountTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_amount_due = ' . $this->sqlAgreementAmountDue() . ','; } elseif( true == array_key_exists( 'AgreementAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' agreement_amount_due = ' . $this->sqlAgreementAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_balance = ' . $this->sqlCurrentBalance() . ','; } elseif( true == array_key_exists( 'CurrentBalance', $this->getChangedColumns() ) ) { $strSql .= ' current_balance = ' . $this->sqlCurrentBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_amount_due = ' . $this->sqlCurrentAmountDue() . ','; } elseif( true == array_key_exists( 'CurrentAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' current_amount_due = ' . $this->sqlCurrentAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_payment_on = ' . $this->sqlLastPaymentOn() . ','; } elseif( true == array_key_exists( 'LastPaymentOn', $this->getChangedColumns() ) ) { $strSql .= ' last_payment_on = ' . $this->sqlLastPaymentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident = ' . $this->sqlIsResident() . ','; } elseif( true == array_key_exists( 'IsResident', $this->getChangedColumns() ) ) { $strSql .= ' is_resident = ' . $this->sqlIsResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_payment_due_on = ' . $this->sqlFirstPaymentDueOn() . ','; } elseif( true == array_key_exists( 'FirstPaymentDueOn', $this->getChangedColumns() ) ) { $strSql .= ' first_payment_due_on = ' . $this->sqlFirstPaymentDueOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' starts_on = ' . $this->sqlStartsOn() . ','; } elseif( true == array_key_exists( 'StartsOn', $this->getChangedColumns() ) ) { $strSql .= ' starts_on = ' . $this->sqlStartsOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ends_on = ' . $this->sqlEndsOn() . ','; } elseif( true == array_key_exists( 'EndsOn', $this->getChangedColumns() ) ) { $strSql .= ' ends_on = ' . $this->sqlEndsOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'external_id' => $this->getExternalId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'offset_days' => $this->getOffsetDays(),
			'agreement_amount_total' => $this->getAgreementAmountTotal(),
			'agreement_amount_due' => $this->getAgreementAmountDue(),
			'current_balance' => $this->getCurrentBalance(),
			'current_amount_due' => $this->getCurrentAmountDue(),
			'last_payment_on' => $this->getLastPaymentOn(),
			'is_active' => $this->getIsActive(),
			'is_resident' => $this->getIsResident(),
			'first_payment_due_on' => $this->getFirstPaymentDueOn(),
			'starts_on' => $this->getStartsOn(),
			'ends_on' => $this->getEndsOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>