<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseCommercialDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCommercialDetails extends CEosPluralBase {

	/**
	 * @return CLeaseCommercialDetail[]
	 */
	public static function fetchLeaseCommercialDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseCommercialDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseCommercialDetail
	 */
	public static function fetchLeaseCommercialDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseCommercialDetail', $objDatabase );
	}

	public static function fetchLeaseCommercialDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_commercial_details', $objDatabase );
	}

	public static function fetchLeaseCommercialDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetail( sprintf( 'SELECT * FROM lease_commercial_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByMasterLeaseIdByCid( $intMasterLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE master_lease_id = %d AND cid = %d', ( int ) $intMasterLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByNoticePriorCustomerAddressIdByCid( $intNoticePriorCustomerAddressId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE notice_prior_customer_address_id = %d AND cid = %d', ( int ) $intNoticePriorCustomerAddressId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCommercialDetailsByNoticeAfterCustomerAddressIdByCid( $intNoticeAfterCustomerAddressId, $intCid, $objDatabase ) {
		return self::fetchLeaseCommercialDetails( sprintf( 'SELECT * FROM lease_commercial_details WHERE notice_after_customer_address_id = %d AND cid = %d', ( int ) $intNoticeAfterCustomerAddressId, ( int ) $intCid ), $objDatabase );
	}

}
?>