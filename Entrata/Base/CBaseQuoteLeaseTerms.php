<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CQuoteLeaseTerms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseQuoteLeaseTerms extends CEosPluralBase {

	/**
	 * @return CQuoteLeaseTerm[]
	 */
	public static function fetchQuoteLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CQuoteLeaseTerm::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CQuoteLeaseTerm
	 */
	public static function fetchQuoteLeaseTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CQuoteLeaseTerm::class, $objDatabase );
	}

	public static function fetchQuoteLeaseTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'quote_lease_terms', $objDatabase );
	}

	public static function fetchQuoteLeaseTermByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerm( sprintf( 'SELECT * FROM quote_lease_terms WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByCid( $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerms( sprintf( 'SELECT * FROM quote_lease_terms WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerms( sprintf( 'SELECT * FROM quote_lease_terms WHERE quote_id = %d AND cid = %d', ( int ) $intQuoteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerms( sprintf( 'SELECT * FROM quote_lease_terms WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerms( sprintf( 'SELECT * FROM quote_lease_terms WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchQuoteLeaseTermsByMappingRenewalOfferIdByCid( $intMappingRenewalOfferId, $intCid, $objDatabase ) {
		return self::fetchQuoteLeaseTerms( sprintf( 'SELECT * FROM quote_lease_terms WHERE mapping_renewal_offer_id = %d AND cid = %d', ( int ) $intMappingRenewalOfferId, ( int ) $intCid ), $objDatabase );
	}

}
?>