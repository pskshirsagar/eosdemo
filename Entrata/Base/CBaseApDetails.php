<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApDetails extends CEosPluralBase {

	/**
	 * @return CApDetail[]
	 */
	public static function fetchApDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApDetail
	 */
	public static function fetchApDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApDetail::class, $objDatabase );
	}

	public static function fetchApDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_details', $objDatabase );
	}

	public static function fetchApDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApDetail( sprintf( 'SELECT * FROM ap_details WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_header_id = %d AND cid = %d', $intApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByOrderDetailIdByCid( $intOrderDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE order_detail_id = %d AND cid = %d', $intOrderDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApPhysicalStatusTypeIdByCid( $intApPhysicalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_physical_status_type_id = %d AND cid = %d', $intApPhysicalStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE gl_transaction_type_id = %d AND cid = %d', $intGlTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApTransactionTypeIdByCid( $intApTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_transaction_type_id = %d AND cid = %d', $intApTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApPostTypeIdByCid( $intApPostTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_post_type_id = %d AND cid = %d', $intApPostTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE bank_account_id = %d AND cid = %d', $intBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByInterCoPropertyIdByCid( $intInterCoPropertyId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE inter_co_property_id = %d AND cid = %d', $intInterCoPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApFormulaIdByCid( $intApFormulaId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_formula_id = %d AND cid = %d', $intApFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByCompanyDepartmentIdByCid( $intCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE company_department_id = %d AND cid = %d', $intCompanyDepartmentId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByGlDimensionIdByCid( $intGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE gl_dimension_id = %d AND cid = %d', $intGlDimensionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE job_phase_id = %d AND cid = %d', $intJobPhaseId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_contract_id = %d AND cid = %d', $intApContractId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByBudgetApHeaderIdByCid( $intBudgetApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE budget_ap_header_id = %d AND cid = %d', $intBudgetApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApContractApDetailIdByCid( $intApContractApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_contract_ap_detail_id = %d AND cid = %d', $intApContractApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPoApDetailIdByCid( $intPoApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE po_ap_detail_id = %d AND cid = %d', $intPoApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByReversalApDetailIdByCid( $intReversalApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE reversal_ap_detail_id = %d AND cid = %d', $intReversalApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByScheduledApDetailIdByCid( $intScheduledApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE scheduled_ap_detail_id = %d AND cid = %d', $intScheduledApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByTemplateApDetailIdByCid( $intTemplateApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE template_ap_detail_id = %d AND cid = %d', $intTemplateApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByReimbursementApDetailIdByCid( $intReimbursementApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE reimbursement_ap_detail_id = %d AND cid = %d', $intReimbursementApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByUnitOfMeasureIdByCid( $intUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE unit_of_measure_id = %d AND cid = %d', $intUnitOfMeasureId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApCatalogItemIdByCid( $intApCatalogItemId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_catalog_item_id = %d AND cid = %d', $intApCatalogItemId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_code_id = %d AND cid = %d', $intApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApPayeeSubAccountIdByCid( $intApPayeeSubAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_payee_sub_account_id = %d AND cid = %d', $intApPayeeSubAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByJobApCodeIdByCid( $intJobApCodeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE job_ap_code_id = %d AND cid = %d', $intJobApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApGlAccountIdByCid( $intApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_gl_account_id = %d AND cid = %d', $intApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByWipGlAccountIdByCid( $intWipGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE wip_gl_account_id = %d AND cid = %d', $intWipGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPurchasesClearingGlAccountIdByCid( $intPurchasesClearingGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE purchases_clearing_gl_account_id = %d AND cid = %d', $intPurchasesClearingGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPendingReimbursementsGlAccountIdByCid( $intPendingReimbursementsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE pending_reimbursements_gl_account_id = %d AND cid = %d', $intPendingReimbursementsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByInterCoApGlAccountIdByCid( $intInterCoApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE inter_co_ap_gl_account_id = %d AND cid = %d', $intInterCoApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByInterCoBankAccountIdByCid( $intInterCoBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE inter_co_bank_account_id = %d AND cid = %d', $intInterCoBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByBankGlAccountIdByCid( $intBankGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE bank_gl_account_id = %d AND cid = %d', $intBankGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByRetentionPayableGlAccountIdByCid( $intRetentionPayableGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE retention_payable_gl_account_id = %d AND cid = %d', $intRetentionPayableGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByAccrualDebitGlAccountIdByCid( $intAccrualDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE accrual_debit_gl_account_id = %d AND cid = %d', $intAccrualDebitGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByAccrualCreditGlAccountIdByCid( $intAccrualCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE accrual_credit_gl_account_id = %d AND cid = %d', $intAccrualCreditGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByCashDebitGlAccountIdByCid( $intCashDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE cash_debit_gl_account_id = %d AND cid = %d', $intCashDebitGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByCashCreditGlAccountIdByCid( $intCashCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE cash_credit_gl_account_id = %d AND cid = %d', $intCashCreditGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByInterCoArTransactionIdByCid( $intInterCoArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE inter_co_ar_transaction_id = %d AND cid = %d', $intInterCoArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByRefundArTransactionIdByCid( $intRefundArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE refund_ar_transaction_id = %d AND cid = %d', $intRefundArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE ap_export_batch_id = %d AND cid = %d', $intApExportBatchId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByFeeIdByCid( $intFeeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE fee_id = %d AND cid = %d', $intFeeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByUnclaimedApPaymentIdByCid( $intUnclaimedApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE unclaimed_ap_payment_id = %d AND cid = %d', $intUnclaimedApPaymentId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByPropertyCamPoolIdByCid( $intPropertyCamPoolId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE property_cam_pool_id = %d AND cid = %d', $intPropertyCamPoolId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByOldPoDetailIdByCid( $intOldPoDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE old_po_detail_id = %d AND cid = %d', $intOldPoDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByCcApPaymentIdByCid( $intCcApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE cc_ap_payment_id = %d AND cid = %d', $intCcApPaymentId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailsByReimbursementMethodIdByCid( $intReimbursementMethodId, $intCid, $objDatabase ) {
		return self::fetchApDetails( sprintf( 'SELECT * FROM ap_details WHERE reimbursement_method_id = %d AND cid = %d', $intReimbursementMethodId, $intCid ), $objDatabase );
	}

}
?>