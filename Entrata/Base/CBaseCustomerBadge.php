<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerBadge extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_badges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intCompanyBadgeId;
	protected $m_strEarnedDatetime;
	protected $m_intIsProAward;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsProAward = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_badge_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyBadgeId', trim( $arrValues['company_badge_id'] ) ); elseif( isset( $arrValues['company_badge_id'] ) ) $this->setCompanyBadgeId( $arrValues['company_badge_id'] );
		if( isset( $arrValues['earned_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEarnedDatetime', trim( $arrValues['earned_datetime'] ) ); elseif( isset( $arrValues['earned_datetime'] ) ) $this->setEarnedDatetime( $arrValues['earned_datetime'] );
		if( isset( $arrValues['is_pro_award'] ) && $boolDirectSet ) $this->set( 'm_intIsProAward', trim( $arrValues['is_pro_award'] ) ); elseif( isset( $arrValues['is_pro_award'] ) ) $this->setIsProAward( $arrValues['is_pro_award'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyBadgeId( $intCompanyBadgeId ) {
		$this->set( 'm_intCompanyBadgeId', CStrings::strToIntDef( $intCompanyBadgeId, NULL, false ) );
	}

	public function getCompanyBadgeId() {
		return $this->m_intCompanyBadgeId;
	}

	public function sqlCompanyBadgeId() {
		return ( true == isset( $this->m_intCompanyBadgeId ) ) ? ( string ) $this->m_intCompanyBadgeId : 'NULL';
	}

	public function setEarnedDatetime( $strEarnedDatetime ) {
		$this->set( 'm_strEarnedDatetime', CStrings::strTrimDef( $strEarnedDatetime, -1, NULL, true ) );
	}

	public function getEarnedDatetime() {
		return $this->m_strEarnedDatetime;
	}

	public function sqlEarnedDatetime() {
		return ( true == isset( $this->m_strEarnedDatetime ) ) ? '\'' . $this->m_strEarnedDatetime . '\'' : 'NOW()';
	}

	public function setIsProAward( $intIsProAward ) {
		$this->set( 'm_intIsProAward', CStrings::strToIntDef( $intIsProAward, NULL, false ) );
	}

	public function getIsProAward() {
		return $this->m_intIsProAward;
	}

	public function sqlIsProAward() {
		return ( true == isset( $this->m_intIsProAward ) ) ? ( string ) $this->m_intIsProAward : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, company_badge_id, earned_datetime, is_pro_award, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlCompanyBadgeId() . ', ' .
 						$this->sqlEarnedDatetime() . ', ' .
 						$this->sqlIsProAward() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_badge_id = ' . $this->sqlCompanyBadgeId() . ','; } elseif( true == array_key_exists( 'CompanyBadgeId', $this->getChangedColumns() ) ) { $strSql .= ' company_badge_id = ' . $this->sqlCompanyBadgeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' earned_datetime = ' . $this->sqlEarnedDatetime() . ','; } elseif( true == array_key_exists( 'EarnedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' earned_datetime = ' . $this->sqlEarnedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pro_award = ' . $this->sqlIsProAward() . ','; } elseif( true == array_key_exists( 'IsProAward', $this->getChangedColumns() ) ) { $strSql .= ' is_pro_award = ' . $this->sqlIsProAward() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'company_badge_id' => $this->getCompanyBadgeId(),
			'earned_datetime' => $this->getEarnedDatetime(),
			'is_pro_award' => $this->getIsProAward(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>