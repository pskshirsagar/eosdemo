<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpecialLeaseTerms extends CEosPluralBase {

    const TABLE_SPECIAL_LEASE_TERMS = 'public.special_lease_terms';

    public static function fetchSpecialLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CSpecialLeaseTerm', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchSpecialLeaseTerm( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CSpecialLeaseTerm', $objDatabase );
    }

    public static function fetchSpecialLeaseTermCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'special_lease_terms', $objDatabase );
    }

    public static function fetchSpecialLeaseTermByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchSpecialLeaseTerm( sprintf( 'SELECT * FROM special_lease_terms WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchSpecialLeaseTermsByCid( $intCid, $objDatabase ) {
        return self::fetchSpecialLeaseTerms( sprintf( 'SELECT * FROM special_lease_terms WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchSpecialLeaseTermsBySpecialIdByCid( $intSpecialId, $intCid, $objDatabase ) {
        return self::fetchSpecialLeaseTerms( sprintf( 'SELECT * FROM special_lease_terms WHERE special_id = %d AND cid = %d', (int) $intSpecialId, (int) $intCid ), $objDatabase );
    }

    public static function fetchSpecialLeaseTermsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
        return self::fetchSpecialLeaseTerms( sprintf( 'SELECT * FROM special_lease_terms WHERE lease_term_id = %d AND cid = %d', (int) $intLeaseTermId, (int) $intCid ), $objDatabase );
    }

    public static function fetchSpecialLeaseTermsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
        return self::fetchSpecialLeaseTerms( sprintf( 'SELECT * FROM special_lease_terms WHERE lease_start_window_id = %d AND cid = %d', (int) $intLeaseStartWindowId, (int) $intCid ), $objDatabase );
    }

}
?>