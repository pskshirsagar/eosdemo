<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLateFeeBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.late_fee_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLateFeeBatchTypeId;
	protected $m_strBatchDatetime;
	protected $m_intIsAutoRun;
	protected $m_intPostedBy;
	protected $m_strPostedThroughDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsAutoRun = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['late_fee_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeBatchTypeId', trim( $arrValues['late_fee_batch_type_id'] ) ); elseif( isset( $arrValues['late_fee_batch_type_id'] ) ) $this->setLateFeeBatchTypeId( $arrValues['late_fee_batch_type_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['is_auto_run'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoRun', trim( $arrValues['is_auto_run'] ) ); elseif( isset( $arrValues['is_auto_run'] ) ) $this->setIsAutoRun( $arrValues['is_auto_run'] );
		if( isset( $arrValues['posted_by'] ) && $boolDirectSet ) $this->set( 'm_intPostedBy', trim( $arrValues['posted_by'] ) ); elseif( isset( $arrValues['posted_by'] ) ) $this->setPostedBy( $arrValues['posted_by'] );
		if( isset( $arrValues['posted_through_date'] ) && $boolDirectSet ) $this->set( 'm_strPostedThroughDate', trim( $arrValues['posted_through_date'] ) ); elseif( isset( $arrValues['posted_through_date'] ) ) $this->setPostedThroughDate( $arrValues['posted_through_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLateFeeBatchTypeId( $intLateFeeBatchTypeId ) {
		$this->set( 'm_intLateFeeBatchTypeId', CStrings::strToIntDef( $intLateFeeBatchTypeId, NULL, false ) );
	}

	public function getLateFeeBatchTypeId() {
		return $this->m_intLateFeeBatchTypeId;
	}

	public function sqlLateFeeBatchTypeId() {
		return ( true == isset( $this->m_intLateFeeBatchTypeId ) ) ? ( string ) $this->m_intLateFeeBatchTypeId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setIsAutoRun( $intIsAutoRun ) {
		$this->set( 'm_intIsAutoRun', CStrings::strToIntDef( $intIsAutoRun, NULL, false ) );
	}

	public function getIsAutoRun() {
		return $this->m_intIsAutoRun;
	}

	public function sqlIsAutoRun() {
		return ( true == isset( $this->m_intIsAutoRun ) ) ? ( string ) $this->m_intIsAutoRun : '0';
	}

	public function setPostedBy( $intPostedBy ) {
		$this->set( 'm_intPostedBy', CStrings::strToIntDef( $intPostedBy, NULL, false ) );
	}

	public function getPostedBy() {
		return $this->m_intPostedBy;
	}

	public function sqlPostedBy() {
		return ( true == isset( $this->m_intPostedBy ) ) ? ( string ) $this->m_intPostedBy : 'NULL';
	}

	public function setPostedThroughDate( $strPostedThroughDate ) {
		$this->set( 'm_strPostedThroughDate', CStrings::strTrimDef( $strPostedThroughDate, -1, NULL, true ) );
	}

	public function getPostedThroughDate() {
		return $this->m_strPostedThroughDate;
	}

	public function sqlPostedThroughDate() {
		return ( true == isset( $this->m_strPostedThroughDate ) ) ? '\'' . $this->m_strPostedThroughDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, late_fee_batch_type_id, batch_datetime, is_auto_run, posted_by, posted_through_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLateFeeBatchTypeId() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
 						$this->sqlIsAutoRun() . ', ' .
 						$this->sqlPostedBy() . ', ' .
 						$this->sqlPostedThroughDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_batch_type_id = ' . $this->sqlLateFeeBatchTypeId() . ','; } elseif( true == array_key_exists( 'LateFeeBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_batch_type_id = ' . $this->sqlLateFeeBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_run = ' . $this->sqlIsAutoRun() . ','; } elseif( true == array_key_exists( 'IsAutoRun', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_run = ' . $this->sqlIsAutoRun() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; } elseif( true == array_key_exists( 'PostedBy', $this->getChangedColumns() ) ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_through_date = ' . $this->sqlPostedThroughDate() . ','; } elseif( true == array_key_exists( 'PostedThroughDate', $this->getChangedColumns() ) ) { $strSql .= ' posted_through_date = ' . $this->sqlPostedThroughDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'late_fee_batch_type_id' => $this->getLateFeeBatchTypeId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'is_auto_run' => $this->getIsAutoRun(),
			'posted_by' => $this->getPostedBy(),
			'posted_through_date' => $this->getPostedThroughDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>