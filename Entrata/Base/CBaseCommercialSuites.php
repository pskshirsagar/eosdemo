<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialSuites
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialSuites extends CEosPluralBase {

	/**
	 * @return CCommercialSuite[]
	 */
	public static function fetchCommercialSuites( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCommercialSuite', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialSuite
	 */
	public static function fetchCommercialSuite( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommercialSuite', $objDatabase );
	}

	public static function fetchCommercialSuiteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_suites', $objDatabase );
	}

	public static function fetchCommercialSuiteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuite( sprintf( 'SELECT * FROM commercial_suites WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuitesByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialSuites( sprintf( 'SELECT * FROM commercial_suites WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuitesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuites( sprintf( 'SELECT * FROM commercial_suites WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuitesByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuites( sprintf( 'SELECT * FROM commercial_suites WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuitesByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuites( sprintf( 'SELECT * FROM commercial_suites WHERE property_floor_id = %d AND cid = %d', ( int ) $intPropertyFloorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCommercialSuitesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCommercialSuites( sprintf( 'SELECT * FROM commercial_suites WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

}
?>