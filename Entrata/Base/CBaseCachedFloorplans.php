<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedFloorplans
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedFloorplans extends CEosPluralBase {

	/**
	 * @return CCachedFloorplan[]
	 */
	public static function fetchCachedFloorplans( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCachedFloorplan::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedFloorplan
	 */
	public static function fetchCachedFloorplan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCachedFloorplan::class, $objDatabase );
	}

	public static function fetchCachedFloorplanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_floorplans', $objDatabase );
	}

	public static function fetchCachedFloorplanByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplan( sprintf( 'SELECT * FROM cached_floorplans WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByCid( $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE ar_trigger_id = %d AND cid = %d', $intArTriggerId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE space_configuration_id = %d AND cid = %d', $intSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedFloorplansByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchCachedFloorplans( sprintf( 'SELECT * FROM cached_floorplans WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

}
?>