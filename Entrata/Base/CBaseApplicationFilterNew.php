<?php

class CBaseApplicationFilterNew extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intCompanyUserId;
    protected $m_strProperties;
    protected $m_strPropertyFloorplans;
    protected $m_strUnitKinds;
    protected $m_strApplicationStageStatuses;
    protected $m_strScreeningResponseTypes;
    protected $m_strLeaseIntervalTypes;
    protected $m_strLeadSources;
    protected $m_strPsProducts;
    protected $m_strCompanyEmployees;
    protected $m_strApplications;
    protected $m_strFilterName;
    protected $m_strQuickSearch;
    protected $m_intLastContactMinDays;
    protected $m_intLastContactMaxDays;
    protected $m_intAdDelayDays;
    protected $m_strDesiredBedrooms;
    protected $m_strDesiredBathrooms;
    protected $m_strDesiredPets;
    protected $m_fltDesiredRentMin;
    protected $m_fltDesiredRentMax;
    protected $m_strDesiredOccupants;
    protected $m_strDesiredFloors;
    protected $m_strApplicationStartDate;
    protected $m_strApplicationEndDate;
    protected $m_strMoveInDateMin;
    protected $m_strMoveInDateMax;
    protected $m_strSortBy;
    protected $m_strSortDirection;
    protected $m_intIsIntegrated;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_strSortDirection = 'DESC';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if ( isset( $arrValues['id'] )) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if ( isset( $arrValues['cid'] )) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->m_intCompanyUserId = trim( $arrValues['company_user_id'] ); else if ( isset( $arrValues['company_user_id'] )) $this->setCompanyUserId( $arrValues['company_user_id'] );
        if( isset( $arrValues['properties'] ) && $boolDirectSet ) $this->m_strProperties = trim( stripcslashes( $arrValues['properties'] ) ); else if ( isset( $arrValues['properties'] )) $this->setProperties(( true == $boolStripSlashes ) ? stripslashes( $arrValues['properties'] ) : $arrValues['properties'] );
        if( isset( $arrValues['property_floorplans'] ) && $boolDirectSet ) $this->m_strPropertyFloorplans = trim( stripcslashes( $arrValues['property_floorplans'] ) ); else if ( isset( $arrValues['property_floorplans'] )) $this->setPropertyFloorplans(( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_floorplans'] ) : $arrValues['property_floorplans'] );
        if( isset( $arrValues['unit_kinds'] ) && $boolDirectSet ) $this->m_strUnitKinds = trim( stripcslashes( $arrValues['unit_kinds'] ) ); else if ( isset( $arrValues['unit_kinds'] )) $this->setUnitKinds(( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_kinds'] ) : $arrValues['unit_kinds'] );
        if( isset( $arrValues['application_stage_statuses'] ) && $boolDirectSet ) $this->m_strApplicationStageStatuses = trim( stripcslashes( $arrValues['application_stage_statuses'] ) ); else if ( isset( $arrValues['application_stage_statuses'] )) $this->setApplicationStageStatuses(( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_stage_statuses'] ) : $arrValues['application_stage_statuses'] );
        if( isset( $arrValues['screening_response_types'] ) && $boolDirectSet ) $this->m_strScreeningResponseTypes = trim( stripcslashes( $arrValues['screening_response_types'] ) ); else if ( isset( $arrValues['screening_response_types'] )) $this->setScreeningResponseTypes(( true == $boolStripSlashes ) ? stripslashes( $arrValues['screening_response_types'] ) : $arrValues['screening_response_types'] );
        if( isset( $arrValues['lease_interval_types'] ) && $boolDirectSet ) $this->m_strLeaseIntervalTypes = trim( stripcslashes( $arrValues['lease_interval_types'] ) ); else if ( isset( $arrValues['lease_interval_types'] )) $this->setLeaseIntervalTypes(( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_interval_types'] ) : $arrValues['lease_interval_types'] );
        if( isset( $arrValues['lead_sources'] ) && $boolDirectSet ) $this->m_strLeadSources = trim( stripcslashes( $arrValues['lead_sources'] ) ); else if ( isset( $arrValues['lead_sources'] )) $this->setLeadSources(( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_sources'] ) : $arrValues['lead_sources'] );
        if( isset( $arrValues['ps_products'] ) && $boolDirectSet ) $this->m_strPsProducts = trim( stripcslashes( $arrValues['ps_products'] ) ); else if ( isset( $arrValues['ps_products'] )) $this->setPsProducts(( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_products'] ) : $arrValues['ps_products'] );
        if( isset( $arrValues['company_employees'] ) && $boolDirectSet ) $this->m_strCompanyEmployees = trim( stripcslashes( $arrValues['company_employees'] ) ); else if ( isset( $arrValues['company_employees'] )) $this->setCompanyEmployees(( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_employees'] ) : $arrValues['company_employees'] );
        if( isset( $arrValues['applications'] ) && $boolDirectSet ) $this->m_strApplications = trim( stripcslashes( $arrValues['applications'] ) ); else if ( isset( $arrValues['applications'] )) $this->setApplications(( true == $boolStripSlashes ) ? stripslashes( $arrValues['applications'] ) : $arrValues['applications'] );
        if( isset( $arrValues['filter_name'] ) && $boolDirectSet ) $this->m_strFilterName = trim( stripcslashes( $arrValues['filter_name'] ) ); else if ( isset( $arrValues['filter_name'] )) $this->setFilterName(( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_name'] ) : $arrValues['filter_name'] );
        if( isset( $arrValues['quick_search'] ) && $boolDirectSet ) $this->m_strQuickSearch = trim( stripcslashes( $arrValues['quick_search'] ) ); else if ( isset( $arrValues['quick_search'] )) $this->setQuickSearch(( true == $boolStripSlashes ) ? stripslashes( $arrValues['quick_search'] ) : $arrValues['quick_search'] );
        if( isset( $arrValues['last_contact_min_days'] ) && $boolDirectSet ) $this->m_intLastContactMinDays = trim( $arrValues['last_contact_min_days'] ); else if ( isset( $arrValues['last_contact_min_days'] )) $this->setLastContactMinDays( $arrValues['last_contact_min_days'] );
        if( isset( $arrValues['last_contact_max_days'] ) && $boolDirectSet ) $this->m_intLastContactMaxDays = trim( $arrValues['last_contact_max_days'] ); else if ( isset( $arrValues['last_contact_max_days'] )) $this->setLastContactMaxDays( $arrValues['last_contact_max_days'] );
        if( isset( $arrValues['ad_delay_days'] ) && $boolDirectSet ) $this->m_intAdDelayDays = trim( $arrValues['ad_delay_days'] ); else if ( isset( $arrValues['ad_delay_days'] )) $this->setAdDelayDays( $arrValues['ad_delay_days'] );
        if( isset( $arrValues['desired_bedrooms'] ) && $boolDirectSet ) $this->m_strDesiredBedrooms = trim( stripcslashes( $arrValues['desired_bedrooms'] ) ); else if ( isset( $arrValues['desired_bedrooms'] )) $this->setDesiredBedrooms(( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bedrooms'] ) : $arrValues['desired_bedrooms'] );
        if( isset( $arrValues['desired_bathrooms'] ) && $boolDirectSet ) $this->m_strDesiredBathrooms = trim( stripcslashes( $arrValues['desired_bathrooms'] ) ); else if ( isset( $arrValues['desired_bathrooms'] )) $this->setDesiredBathrooms(( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_bathrooms'] ) : $arrValues['desired_bathrooms'] );
        if( isset( $arrValues['desired_pets'] ) && $boolDirectSet ) $this->m_strDesiredPets = trim( stripcslashes( $arrValues['desired_pets'] ) ); else if ( isset( $arrValues['desired_pets'] )) $this->setDesiredPets(( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_pets'] ) : $arrValues['desired_pets'] );
        if( isset( $arrValues['desired_rent_min'] ) && $boolDirectSet ) $this->m_fltDesiredRentMin = trim( $arrValues['desired_rent_min'] ); else if ( isset( $arrValues['desired_rent_min'] )) $this->setDesiredRentMin( $arrValues['desired_rent_min'] );
        if( isset( $arrValues['desired_rent_max'] ) && $boolDirectSet ) $this->m_fltDesiredRentMax = trim( $arrValues['desired_rent_max'] ); else if ( isset( $arrValues['desired_rent_max'] )) $this->setDesiredRentMax( $arrValues['desired_rent_max'] );
        if( isset( $arrValues['desired_occupants'] ) && $boolDirectSet ) $this->m_strDesiredOccupants = trim( stripcslashes( $arrValues['desired_occupants'] ) ); else if ( isset( $arrValues['desired_occupants'] )) $this->setDesiredOccupants(( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_occupants'] ) : $arrValues['desired_occupants'] );
        if( isset( $arrValues['desired_floors'] ) && $boolDirectSet ) $this->m_strDesiredFloors = trim( stripcslashes( $arrValues['desired_floors'] ) ); else if ( isset( $arrValues['desired_floors'] )) $this->setDesiredFloors(( true == $boolStripSlashes ) ? stripslashes( $arrValues['desired_floors'] ) : $arrValues['desired_floors'] );
        if( isset( $arrValues['application_start_date'] ) && $boolDirectSet ) $this->m_strApplicationStartDate = trim( $arrValues['application_start_date'] ); else if ( isset( $arrValues['application_start_date'] )) $this->setApplicationStartDate( $arrValues['application_start_date'] );
        if( isset( $arrValues['application_end_date'] ) && $boolDirectSet ) $this->m_strApplicationEndDate = trim( $arrValues['application_end_date'] ); else if ( isset( $arrValues['application_end_date'] )) $this->setApplicationEndDate( $arrValues['application_end_date'] );
        if( isset( $arrValues['move_in_date_min'] ) && $boolDirectSet ) $this->m_strMoveInDateMin = trim( $arrValues['move_in_date_min'] ); else if ( isset( $arrValues['move_in_date_min'] )) $this->setMoveInDateMin( $arrValues['move_in_date_min'] );
        if( isset( $arrValues['move_in_date_max'] ) && $boolDirectSet ) $this->m_strMoveInDateMax = trim( $arrValues['move_in_date_max'] ); else if ( isset( $arrValues['move_in_date_max'] )) $this->setMoveInDateMax( $arrValues['move_in_date_max'] );
        if( isset( $arrValues['sort_by'] ) && $boolDirectSet ) $this->m_strSortBy = trim( stripcslashes( $arrValues['sort_by'] ) ); else if ( isset( $arrValues['sort_by'] )) $this->setSortBy(( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_by'] ) : $arrValues['sort_by'] );
        if( isset( $arrValues['sort_direction'] ) && $boolDirectSet ) $this->m_strSortDirection = trim( stripcslashes( $arrValues['sort_direction'] ) ); else if ( isset( $arrValues['sort_direction'] )) $this->setSortDirection(( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_direction'] ) : $arrValues['sort_direction'] );
        if( isset( $arrValues['is_integrated'] ) && $boolDirectSet ) $this->m_intIsIntegrated = trim( $arrValues['is_integrated'] ); else if ( isset( $arrValues['is_integrated'] )) $this->setIsIntegrated( $arrValues['is_integrated'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if ( isset( $arrValues['deleted_by'] )) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if ( isset( $arrValues['deleted_on'] )) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if ( isset( $arrValues['updated_by'] )) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if ( isset( $arrValues['updated_on'] )) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if ( isset( $arrValues['created_by'] )) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if ( isset( $arrValues['created_on'] )) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return  ( true == isset( $this->m_intId )) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return  ( true == isset( $this->m_intCid )) ? (string) $this->m_intCid : 'NULL';
    }

    public function setCompanyUserId( $intCompanyUserId ) {
        $this->m_intCompanyUserId = CStrings::strToIntDef( $intCompanyUserId, NULL, false );
    }

    public function getCompanyUserId() {
        return $this->m_intCompanyUserId;
    }

    public function sqlCompanyUserId() {
        return  ( true == isset( $this->m_intCompanyUserId )) ? (string) $this->m_intCompanyUserId : 'NULL';
    }

    public function setProperties( $strProperties ) {
        $this->m_strProperties = CStrings::strTrimDef( $strProperties, -1, NULL, true );
    }

    public function getProperties() {
        return $this->m_strProperties;
    }

    public function sqlProperties() {
        return  ( true == isset( $this->m_strProperties )) ? '\'' . addslashes( $this->m_strProperties) . '\'' : 'NULL';
    }

    public function setPropertyFloorplans( $strPropertyFloorplans ) {
        $this->m_strPropertyFloorplans = CStrings::strTrimDef( $strPropertyFloorplans, -1, NULL, true );
    }

    public function getPropertyFloorplans() {
        return $this->m_strPropertyFloorplans;
    }

    public function sqlPropertyFloorplans() {
        return  ( true == isset( $this->m_strPropertyFloorplans )) ? '\'' . addslashes( $this->m_strPropertyFloorplans) . '\'' : 'NULL';
    }

    public function setUnitKinds( $strUnitKinds ) {
        $this->m_strUnitKinds = CStrings::strTrimDef( $strUnitKinds, 2000, NULL, true );
    }

    public function getUnitKinds() {
        return $this->m_strUnitKinds;
    }

    public function sqlUnitKinds() {
        return  ( true == isset( $this->m_strUnitKinds )) ? '\'' . addslashes( $this->m_strUnitKinds) . '\'' : 'NULL';
    }

    public function setApplicationStageStatuses( $strApplicationStageStatuses ) {
        $this->m_strApplicationStageStatuses = CStrings::strTrimDef( $strApplicationStageStatuses, -1, NULL, true );
    }

    public function getApplicationStageStatuses() {
        return $this->m_strApplicationStageStatuses;
    }

    public function sqlApplicationStageStatuses() {
        return  ( true == isset( $this->m_strApplicationStageStatuses )) ? '\'' . addslashes( $this->m_strApplicationStageStatuses) . '\'' : 'NULL';
    }

    public function setScreeningResponseTypes( $strScreeningResponseTypes ) {
        $this->m_strScreeningResponseTypes = CStrings::strTrimDef( $strScreeningResponseTypes, -1, NULL, true );
    }

    public function getScreeningResponseTypes() {
        return $this->m_strScreeningResponseTypes;
    }

    public function sqlScreeningResponseTypes() {
        return  ( true == isset( $this->m_strScreeningResponseTypes )) ? '\'' . addslashes( $this->m_strScreeningResponseTypes) . '\'' : 'NULL';
    }

    public function setLeaseIntervalTypes( $strLeaseIntervalTypes ) {
        $this->m_strLeaseIntervalTypes = CStrings::strTrimDef( $strLeaseIntervalTypes, -1, NULL, true );
    }

    public function getLeaseIntervalTypes() {
        return $this->m_strLeaseIntervalTypes;
    }

    public function sqlLeaseIntervalTypes() {
        return  ( true == isset( $this->m_strLeaseIntervalTypes )) ? '\'' . addslashes( $this->m_strLeaseIntervalTypes) . '\'' : 'NULL';
    }

    public function setLeadSources( $strLeadSources ) {
        $this->m_strLeadSources = CStrings::strTrimDef( $strLeadSources, -1, NULL, true );
    }

    public function getLeadSources() {
        return $this->m_strLeadSources;
    }

    public function sqlLeadSources() {
        return  ( true == isset( $this->m_strLeadSources )) ? '\'' . addslashes( $this->m_strLeadSources) . '\'' : 'NULL';
    }

    public function setPsProducts( $strPsProducts ) {
        $this->m_strPsProducts = CStrings::strTrimDef( $strPsProducts, -1, NULL, true );
    }

    public function getPsProducts() {
        return $this->m_strPsProducts;
    }

    public function sqlPsProducts() {
        return  ( true == isset( $this->m_strPsProducts )) ? '\'' . addslashes( $this->m_strPsProducts) . '\'' : 'NULL';
    }

    public function setCompanyEmployees( $strCompanyEmployees ) {
        $this->m_strCompanyEmployees = CStrings::strTrimDef( $strCompanyEmployees, -1, NULL, true );
    }

    public function getCompanyEmployees() {
        return $this->m_strCompanyEmployees;
    }

    public function sqlCompanyEmployees() {
        return  ( true == isset( $this->m_strCompanyEmployees )) ? '\'' . addslashes( $this->m_strCompanyEmployees) . '\'' : 'NULL';
    }

    public function setApplications( $strApplications ) {
        $this->m_strApplications = CStrings::strTrimDef( $strApplications, -1, NULL, true );
    }

    public function getApplications() {
        return $this->m_strApplications;
    }

    public function sqlApplications() {
        return  ( true == isset( $this->m_strApplications )) ? '\'' . addslashes( $this->m_strApplications) . '\'' : 'NULL';
    }

    public function setFilterName( $strFilterName ) {
        $this->m_strFilterName = CStrings::strTrimDef( $strFilterName, 50, NULL, true );
    }

    public function getFilterName() {
        return $this->m_strFilterName;
    }

    public function sqlFilterName() {
        return  ( true == isset( $this->m_strFilterName )) ? '\'' . addslashes( $this->m_strFilterName) . '\'' : 'NULL';
    }

    public function setQuickSearch( $strQuickSearch ) {
        $this->m_strQuickSearch = CStrings::strTrimDef( $strQuickSearch, -1, NULL, true );
    }

    public function getQuickSearch() {
        return $this->m_strQuickSearch;
    }

    public function sqlQuickSearch() {
        return  ( true == isset( $this->m_strQuickSearch )) ? '\'' . addslashes( $this->m_strQuickSearch) . '\'' : 'NULL';
    }

    public function setLastContactMinDays( $intLastContactMinDays ) {
        $this->m_intLastContactMinDays = CStrings::strToIntDef( $intLastContactMinDays, NULL, false );
    }

    public function getLastContactMinDays() {
        return $this->m_intLastContactMinDays;
    }

    public function sqlLastContactMinDays() {
        return  ( true == isset( $this->m_intLastContactMinDays )) ? (string) $this->m_intLastContactMinDays : 'NULL';
    }

    public function setLastContactMaxDays( $intLastContactMaxDays ) {
        $this->m_intLastContactMaxDays = CStrings::strToIntDef( $intLastContactMaxDays, NULL, false );
    }

    public function getLastContactMaxDays() {
        return $this->m_intLastContactMaxDays;
    }

    public function sqlLastContactMaxDays() {
        return  ( true == isset( $this->m_intLastContactMaxDays )) ? (string) $this->m_intLastContactMaxDays : 'NULL';
    }

    public function setAdDelayDays( $intAdDelayDays ) {
        $this->m_intAdDelayDays = CStrings::strToIntDef( $intAdDelayDays, NULL, false );
    }

    public function getAdDelayDays() {
        return $this->m_intAdDelayDays;
    }

    public function sqlAdDelayDays() {
        return  ( true == isset( $this->m_intAdDelayDays )) ? (string) $this->m_intAdDelayDays : 'NULL';
    }

    public function setDesiredBedrooms( $strDesiredBedrooms ) {
        $this->m_strDesiredBedrooms = CStrings::strTrimDef( $strDesiredBedrooms, 240, NULL, true );
    }

    public function getDesiredBedrooms() {
        return $this->m_strDesiredBedrooms;
    }

    public function sqlDesiredBedrooms() {
        return  ( true == isset( $this->m_strDesiredBedrooms )) ? '\'' . addslashes( $this->m_strDesiredBedrooms) . '\'' : 'NULL';
    }

    public function setDesiredBathrooms( $strDesiredBathrooms ) {
        $this->m_strDesiredBathrooms = CStrings::strTrimDef( $strDesiredBathrooms, 240, NULL, true );
    }

    public function getDesiredBathrooms() {
        return $this->m_strDesiredBathrooms;
    }

    public function sqlDesiredBathrooms() {
        return  ( true == isset( $this->m_strDesiredBathrooms )) ? '\'' . addslashes( $this->m_strDesiredBathrooms) . '\'' : 'NULL';
    }

    public function setDesiredPets( $strDesiredPets ) {
        $this->m_strDesiredPets = CStrings::strTrimDef( $strDesiredPets, 240, NULL, true );
    }

    public function getDesiredPets() {
        return $this->m_strDesiredPets;
    }

    public function sqlDesiredPets() {
        return  ( true == isset( $this->m_strDesiredPets )) ? '\'' . addslashes( $this->m_strDesiredPets) . '\'' : 'NULL';
    }

    public function setDesiredRentMin( $fltDesiredRentMin ) {
        $this->m_fltDesiredRentMin = CStrings::strToFloatDef( $fltDesiredRentMin, NULL, false, 2);
    }

    public function getDesiredRentMin() {
        return $this->m_fltDesiredRentMin;
    }

    public function sqlDesiredRentMin() {
        return  ( true == isset( $this->m_fltDesiredRentMin )) ? (string) $this->m_fltDesiredRentMin : 'NULL';
    }

    public function setDesiredRentMax( $fltDesiredRentMax ) {
        $this->m_fltDesiredRentMax = CStrings::strToFloatDef( $fltDesiredRentMax, NULL, false, 2);
    }

    public function getDesiredRentMax() {
        return $this->m_fltDesiredRentMax;
    }

    public function sqlDesiredRentMax() {
        return  ( true == isset( $this->m_fltDesiredRentMax )) ? (string) $this->m_fltDesiredRentMax : 'NULL';
    }

    public function setDesiredOccupants( $strDesiredOccupants ) {
        $this->m_strDesiredOccupants = CStrings::strTrimDef( $strDesiredOccupants, 2000, NULL, true );
    }

    public function getDesiredOccupants() {
        return $this->m_strDesiredOccupants;
    }

    public function sqlDesiredOccupants() {
        return  ( true == isset( $this->m_strDesiredOccupants )) ? '\'' . addslashes( $this->m_strDesiredOccupants) . '\'' : 'NULL';
    }

    public function setDesiredFloors( $strDesiredFloors ) {
        $this->m_strDesiredFloors = CStrings::strTrimDef( $strDesiredFloors, 2000, NULL, true );
    }

    public function getDesiredFloors() {
        return $this->m_strDesiredFloors;
    }

    public function sqlDesiredFloors() {
        return  ( true == isset( $this->m_strDesiredFloors )) ? '\'' . addslashes( $this->m_strDesiredFloors) . '\'' : 'NULL';
    }

    public function setApplicationStartDate( $strApplicationStartDate ) {
        $this->m_strApplicationStartDate = CStrings::strTrimDef( $strApplicationStartDate, -1, NULL, true );
    }

    public function getApplicationStartDate() {
        return $this->m_strApplicationStartDate;
    }

    public function sqlApplicationStartDate() {
        return  ( true == isset( $this->m_strApplicationStartDate )) ? '\'' . $this->m_strApplicationStartDate . '\'' : 'NULL';
    }

    public function setApplicationEndDate( $strApplicationEndDate ) {
        $this->m_strApplicationEndDate = CStrings::strTrimDef( $strApplicationEndDate, -1, NULL, true );
    }

    public function getApplicationEndDate() {
        return $this->m_strApplicationEndDate;
    }

    public function sqlApplicationEndDate() {
        return  ( true == isset( $this->m_strApplicationEndDate )) ? '\'' . $this->m_strApplicationEndDate . '\'' : 'NULL';
    }

    public function setMoveInDateMin( $strMoveInDateMin ) {
        $this->m_strMoveInDateMin = CStrings::strTrimDef( $strMoveInDateMin, -1, NULL, true );
    }

    public function getMoveInDateMin() {
        return $this->m_strMoveInDateMin;
    }

    public function sqlMoveInDateMin() {
        return  ( true == isset( $this->m_strMoveInDateMin )) ? '\'' . $this->m_strMoveInDateMin . '\'' : 'NULL';
    }

    public function setMoveInDateMax( $strMoveInDateMax ) {
        $this->m_strMoveInDateMax = CStrings::strTrimDef( $strMoveInDateMax, -1, NULL, true );
    }

    public function getMoveInDateMax() {
        return $this->m_strMoveInDateMax;
    }

    public function sqlMoveInDateMax() {
        return  ( true == isset( $this->m_strMoveInDateMax )) ? '\'' . $this->m_strMoveInDateMax . '\'' : 'NULL';
    }

    public function setSortBy( $strSortBy ) {
        $this->m_strSortBy = CStrings::strTrimDef( $strSortBy, 240, NULL, true );
    }

    public function getSortBy() {
        return $this->m_strSortBy;
    }

    public function sqlSortBy() {
        return  ( true == isset( $this->m_strSortBy )) ? '\'' . addslashes( $this->m_strSortBy) . '\'' : 'NULL';
    }

    public function setSortDirection( $strSortDirection ) {
        $this->m_strSortDirection = CStrings::strTrimDef( $strSortDirection, 240, NULL, true );
    }

    public function getSortDirection() {
        return $this->m_strSortDirection;
    }

    public function sqlSortDirection() {
        return  ( true == isset( $this->m_strSortDirection )) ? '\'' . addslashes( $this->m_strSortDirection) . '\'' : '\'DESC\'';
    }

    public function setIsIntegrated( $intIsIntegrated ) {
        $this->m_intIsIntegrated = CStrings::strToIntDef( $intIsIntegrated, NULL, false );
    }

    public function getIsIntegrated() {
        return $this->m_intIsIntegrated;
    }

    public function sqlIsIntegrated() {
        return  ( true == isset( $this->m_intIsIntegrated )) ? (string) $this->m_intIsIntegrated : 'NULL';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return  ( true == isset( $this->m_intDeletedBy )) ? (string) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return  ( true == isset( $this->m_strDeletedOn )) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return  ( true == isset( $this->m_intUpdatedBy )) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return  ( true == isset( $this->m_strUpdatedOn )) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return  ( true == isset( $this->m_intCreatedBy )) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return  ( true == isset( $this->m_strCreatedOn )) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() )) ?  'nextval( \'public.application_filters_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.application_filters
					VALUES ( ' .
	                    $strId . ', ' .
	                    $this->sqlCid() . ', ' .
	                    $this->sqlCompanyUserId() . ', ' .
	                    $this->sqlProperties() . ', ' .
	                    $this->sqlPropertyFloorplans() . ', ' .
	                    $this->sqlUnitKinds() . ', ' .
	                    $this->sqlApplicationStageStatuses() . ', ' .
	                    $this->sqlScreeningResponseTypes() . ', ' .
	                    $this->sqlLeaseIntervalTypes() . ', ' .
	                    $this->sqlLeadSources() . ', ' .
	                    $this->sqlPsProducts() . ', ' .
	                    $this->sqlCompanyEmployees() . ', ' .
	                    $this->sqlApplications() . ', ' .
	                    $this->sqlFilterName() . ', ' .
	                    $this->sqlQuickSearch() . ', ' .
	                    $this->sqlLastContactMinDays() . ', ' .
	                    $this->sqlLastContactMaxDays() . ', ' .
	                    $this->sqlAdDelayDays() . ', ' .
	                    $this->sqlDesiredBedrooms() . ', ' .
	                    $this->sqlDesiredBathrooms() . ', ' .
	                    $this->sqlDesiredPets() . ', ' .
	                    $this->sqlDesiredRentMin() . ', ' .
	                    $this->sqlDesiredRentMax() . ', ' .
	                    $this->sqlDesiredOccupants() . ', ' .
	                    $this->sqlDesiredFloors() . ', ' .
	                    $this->sqlApplicationStartDate() . ', ' .
	                    $this->sqlApplicationEndDate() . ', ' .
	                    $this->sqlMoveInDateMin() . ', ' .
	                    $this->sqlMoveInDateMax() . ', ' .
	                    $this->sqlSortBy() . ', ' .
	                    $this->sqlSortDirection() . ', ' .
	                    $this->sqlIsIntegrated() . ', ' .
	                    $this->sqlDeletedBy() . ', ' .
	                    $this->sqlDeletedOn() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlUpdatedOn() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate()) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.application_filters
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid()) != $this->getOriginalValueByFieldName ( 'cid' )) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyUserId()) != $this->getOriginalValueByFieldName ( 'company_user_id' )) { $arrstrOriginalValueChanges['company_user_id'] = $this->sqlCompanyUserId(); $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlProperties()) != $this->getOriginalValueByFieldName ( 'properties' )) { $arrstrOriginalValueChanges['properties'] = $this->sqlProperties(); $strSql .= ' properties = ' . $this->sqlProperties() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyFloorplans()) != $this->getOriginalValueByFieldName ( 'property_floorplans' )) { $arrstrOriginalValueChanges['property_floorplans'] = $this->sqlPropertyFloorplans(); $strSql .= ' property_floorplans = ' . $this->sqlPropertyFloorplans() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kinds = ' . $this->sqlUnitKinds() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUnitKinds()) != $this->getOriginalValueByFieldName ( 'unit_kinds' )) { $arrstrOriginalValueChanges['unit_kinds'] = $this->sqlUnitKinds(); $strSql .= ' unit_kinds = ' . $this->sqlUnitKinds() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_stage_statuses = ' . $this->sqlApplicationStageStatuses() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicationStageStatuses()) != $this->getOriginalValueByFieldName ( 'application_stage_statuses' )) { $arrstrOriginalValueChanges['application_stage_statuses'] = $this->sqlApplicationStageStatuses(); $strSql .= ' application_stage_statuses = ' . $this->sqlApplicationStageStatuses() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_response_types = ' . $this->sqlScreeningResponseTypes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlScreeningResponseTypes()) != $this->getOriginalValueByFieldName ( 'screening_response_types' )) { $arrstrOriginalValueChanges['screening_response_types'] = $this->sqlScreeningResponseTypes(); $strSql .= ' screening_response_types = ' . $this->sqlScreeningResponseTypes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeaseIntervalTypes()) != $this->getOriginalValueByFieldName ( 'lease_interval_types' )) { $arrstrOriginalValueChanges['lease_interval_types'] = $this->sqlLeaseIntervalTypes(); $strSql .= ' lease_interval_types = ' . $this->sqlLeaseIntervalTypes() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_sources = ' . $this->sqlLeadSources() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLeadSources()) != $this->getOriginalValueByFieldName ( 'lead_sources' )) { $arrstrOriginalValueChanges['lead_sources'] = $this->sqlLeadSources(); $strSql .= ' lead_sources = ' . $this->sqlLeadSources() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPsProducts()) != $this->getOriginalValueByFieldName ( 'ps_products' )) { $arrstrOriginalValueChanges['ps_products'] = $this->sqlPsProducts(); $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employees = ' . $this->sqlCompanyEmployees() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyEmployees()) != $this->getOriginalValueByFieldName ( 'company_employees' )) { $arrstrOriginalValueChanges['company_employees'] = $this->sqlCompanyEmployees(); $strSql .= ' company_employees = ' . $this->sqlCompanyEmployees() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applications = ' . $this->sqlApplications() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplications()) != $this->getOriginalValueByFieldName ( 'applications' )) { $arrstrOriginalValueChanges['applications'] = $this->sqlApplications(); $strSql .= ' applications = ' . $this->sqlApplications() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_name = ' . $this->sqlFilterName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlFilterName()) != $this->getOriginalValueByFieldName ( 'filter_name' )) { $arrstrOriginalValueChanges['filter_name'] = $this->sqlFilterName(); $strSql .= ' filter_name = ' . $this->sqlFilterName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quick_search = ' . $this->sqlQuickSearch() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlQuickSearch()) != $this->getOriginalValueByFieldName ( 'quick_search' )) { $arrstrOriginalValueChanges['quick_search'] = $this->sqlQuickSearch(); $strSql .= ' quick_search = ' . $this->sqlQuickSearch() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contact_min_days = ' . $this->sqlLastContactMinDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLastContactMinDays()) != $this->getOriginalValueByFieldName ( 'last_contact_min_days' )) { $arrstrOriginalValueChanges['last_contact_min_days'] = $this->sqlLastContactMinDays(); $strSql .= ' last_contact_min_days = ' . $this->sqlLastContactMinDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contact_max_days = ' . $this->sqlLastContactMaxDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLastContactMaxDays()) != $this->getOriginalValueByFieldName ( 'last_contact_max_days' )) { $arrstrOriginalValueChanges['last_contact_max_days'] = $this->sqlLastContactMaxDays(); $strSql .= ' last_contact_max_days = ' . $this->sqlLastContactMaxDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_delay_days = ' . $this->sqlAdDelayDays() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAdDelayDays()) != $this->getOriginalValueByFieldName ( 'ad_delay_days' )) { $arrstrOriginalValueChanges['ad_delay_days'] = $this->sqlAdDelayDays(); $strSql .= ' ad_delay_days = ' . $this->sqlAdDelayDays() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDesiredBedrooms()) != $this->getOriginalValueByFieldName ( 'desired_bedrooms' )) { $arrstrOriginalValueChanges['desired_bedrooms'] = $this->sqlDesiredBedrooms(); $strSql .= ' desired_bedrooms = ' . $this->sqlDesiredBedrooms() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDesiredBathrooms()) != $this->getOriginalValueByFieldName ( 'desired_bathrooms' )) { $arrstrOriginalValueChanges['desired_bathrooms'] = $this->sqlDesiredBathrooms(); $strSql .= ' desired_bathrooms = ' . $this->sqlDesiredBathrooms() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_pets = ' . $this->sqlDesiredPets() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDesiredPets()) != $this->getOriginalValueByFieldName ( 'desired_pets' )) { $arrstrOriginalValueChanges['desired_pets'] = $this->sqlDesiredPets(); $strSql .= ' desired_pets = ' . $this->sqlDesiredPets() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; } elseif (( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlDesiredRentMin()), $this->getOriginalValueByFieldName ( 'desired_rent_min' ), 2 ))) { $arrstrOriginalValueChanges['desired_rent_min'] = $this->sqlDesiredRentMin(); $strSql .= ' desired_rent_min = ' . $this->sqlDesiredRentMin() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; } elseif (( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlDesiredRentMax()), $this->getOriginalValueByFieldName ( 'desired_rent_max' ), 2 ))) { $arrstrOriginalValueChanges['desired_rent_max'] = $this->sqlDesiredRentMax(); $strSql .= ' desired_rent_max = ' . $this->sqlDesiredRentMax() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDesiredOccupants()) != $this->getOriginalValueByFieldName ( 'desired_occupants' )) { $arrstrOriginalValueChanges['desired_occupants'] = $this->sqlDesiredOccupants(); $strSql .= ' desired_occupants = ' . $this->sqlDesiredOccupants() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_floors = ' . $this->sqlDesiredFloors() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDesiredFloors()) != $this->getOriginalValueByFieldName ( 'desired_floors' )) { $arrstrOriginalValueChanges['desired_floors'] = $this->sqlDesiredFloors(); $strSql .= ' desired_floors = ' . $this->sqlDesiredFloors() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_start_date = ' . $this->sqlApplicationStartDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicationStartDate()) != $this->getOriginalValueByFieldName ( 'application_start_date' )) { $arrstrOriginalValueChanges['application_start_date'] = $this->sqlApplicationStartDate(); $strSql .= ' application_start_date = ' . $this->sqlApplicationStartDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_end_date = ' . $this->sqlApplicationEndDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicationEndDate()) != $this->getOriginalValueByFieldName ( 'application_end_date' )) { $arrstrOriginalValueChanges['application_end_date'] = $this->sqlApplicationEndDate(); $strSql .= ' application_end_date = ' . $this->sqlApplicationEndDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMoveInDateMin()) != $this->getOriginalValueByFieldName ( 'move_in_date_min' )) { $arrstrOriginalValueChanges['move_in_date_min'] = $this->sqlMoveInDateMin(); $strSql .= ' move_in_date_min = ' . $this->sqlMoveInDateMin() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMoveInDateMax()) != $this->getOriginalValueByFieldName ( 'move_in_date_max' )) { $arrstrOriginalValueChanges['move_in_date_max'] = $this->sqlMoveInDateMax(); $strSql .= ' move_in_date_max = ' . $this->sqlMoveInDateMax() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSortBy()) != $this->getOriginalValueByFieldName ( 'sort_by' )) { $arrstrOriginalValueChanges['sort_by'] = $this->sqlSortBy(); $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSortDirection()) != $this->getOriginalValueByFieldName ( 'sort_direction' )) { $arrstrOriginalValueChanges['sort_direction'] = $this->sqlSortDirection(); $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsIntegrated()) != $this->getOriginalValueByFieldName ( 'is_integrated' )) { $arrstrOriginalValueChanges['is_integrated'] = $this->sqlIsIntegrated(); $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy()) != $this->getOriginalValueByFieldName ( 'deleted_by' )) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn()) != $this->getOriginalValueByFieldName ( 'deleted_on' )) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'
					WHERE
					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ){
			    if( true == $this->executeSql( $strSql, $this, $objDatabase )){
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.application_filters WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase ) {
        return parent::fetchNextId( 'public.application_filters_id_seq', $objDatabase );
    }
}
?>