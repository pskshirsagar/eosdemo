<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCamShare extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_cam_shares';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseCamPoolId;
	protected $m_intApCodeId;
	protected $m_intCamCalculationTypeId;
	protected $m_fltShareAmount;
	protected $m_intCapAmount;
	protected $m_boolIsBaseYearFixed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intPropertyCamBudgetId;

	public function __construct() {
		parent::__construct();

		$this->m_intCapAmount = '0';
		$this->m_boolIsBaseYearFixed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_cam_pool_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCamPoolId', trim( $arrValues['lease_cam_pool_id'] ) ); elseif( isset( $arrValues['lease_cam_pool_id'] ) ) $this->setLeaseCamPoolId( $arrValues['lease_cam_pool_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['cam_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCamCalculationTypeId', trim( $arrValues['cam_calculation_type_id'] ) ); elseif( isset( $arrValues['cam_calculation_type_id'] ) ) $this->setCamCalculationTypeId( $arrValues['cam_calculation_type_id'] );
		if( isset( $arrValues['share_amount'] ) && $boolDirectSet ) $this->set( 'm_fltShareAmount', trim( $arrValues['share_amount'] ) ); elseif( isset( $arrValues['share_amount'] ) ) $this->setShareAmount( $arrValues['share_amount'] );
		if( isset( $arrValues['cap_amount'] ) && $boolDirectSet ) $this->set( 'm_intCapAmount', trim( $arrValues['cap_amount'] ) ); elseif( isset( $arrValues['cap_amount'] ) ) $this->setCapAmount( $arrValues['cap_amount'] );
		if( isset( $arrValues['is_base_year_fixed'] ) && $boolDirectSet ) $this->set( 'm_boolIsBaseYearFixed', trim( stripcslashes( $arrValues['is_base_year_fixed'] ) ) ); elseif( isset( $arrValues['is_base_year_fixed'] ) ) $this->setIsBaseYearFixed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_base_year_fixed'] ) : $arrValues['is_base_year_fixed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['property_cam_budget_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyCamBudgetId', trim( $arrValues['property_cam_budget_id'] ) ); elseif( isset( $arrValues['property_cam_budget_id'] ) ) $this->setPropertyCamBudgetId( $arrValues['property_cam_budget_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseCamPoolId( $intLeaseCamPoolId ) {
		$this->set( 'm_intLeaseCamPoolId', CStrings::strToIntDef( $intLeaseCamPoolId, NULL, false ) );
	}

	public function getLeaseCamPoolId() {
		return $this->m_intLeaseCamPoolId;
	}

	public function sqlLeaseCamPoolId() {
		return ( true == isset( $this->m_intLeaseCamPoolId ) ) ? ( string ) $this->m_intLeaseCamPoolId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setCamCalculationTypeId( $intCamCalculationTypeId ) {
		$this->set( 'm_intCamCalculationTypeId', CStrings::strToIntDef( $intCamCalculationTypeId, NULL, false ) );
	}

	public function getCamCalculationTypeId() {
		return $this->m_intCamCalculationTypeId;
	}

	public function sqlCamCalculationTypeId() {
		return ( true == isset( $this->m_intCamCalculationTypeId ) ) ? ( string ) $this->m_intCamCalculationTypeId : 'NULL';
	}

	public function setShareAmount( $fltShareAmount ) {
		$this->set( 'm_fltShareAmount', CStrings::strToFloatDef( $fltShareAmount, NULL, false, 2 ) );
	}

	public function getShareAmount() {
		return $this->m_fltShareAmount;
	}

	public function sqlShareAmount() {
		return ( true == isset( $this->m_fltShareAmount ) ) ? ( string ) $this->m_fltShareAmount : 'NULL';
	}

	public function setCapAmount( $intCapAmount ) {
		$this->set( 'm_intCapAmount', CStrings::strToIntDef( $intCapAmount, NULL, false ) );
	}

	public function getCapAmount() {
		return $this->m_intCapAmount;
	}

	public function sqlCapAmount() {
		return ( true == isset( $this->m_intCapAmount ) ) ? ( string ) $this->m_intCapAmount : '0';
	}

	public function setIsBaseYearFixed( $boolIsBaseYearFixed ) {
		$this->set( 'm_boolIsBaseYearFixed', CStrings::strToBool( $boolIsBaseYearFixed ) );
	}

	public function getIsBaseYearFixed() {
		return $this->m_boolIsBaseYearFixed;
	}

	public function sqlIsBaseYearFixed() {
		return ( true == isset( $this->m_boolIsBaseYearFixed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBaseYearFixed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setPropertyCamBudgetId( $intPropertyCamBudgetId ) {
		$this->set( 'm_intPropertyCamBudgetId', CStrings::strToIntDef( $intPropertyCamBudgetId, NULL, false ) );
	}

	public function getPropertyCamBudgetId() {
		return $this->m_intPropertyCamBudgetId;
	}

	public function sqlPropertyCamBudgetId() {
		return ( true == isset( $this->m_intPropertyCamBudgetId ) ) ? ( string ) $this->m_intPropertyCamBudgetId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_cam_pool_id, ap_code_id, cam_calculation_type_id, share_amount, cap_amount, is_base_year_fixed, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, details, property_cam_budget_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseCamPoolId() . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlCamCalculationTypeId() . ', ' .
						$this->sqlShareAmount() . ', ' .
						$this->sqlCapAmount() . ', ' .
						$this->sqlIsBaseYearFixed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlPropertyCamBudgetId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_cam_pool_id = ' . $this->sqlLeaseCamPoolId(). ',' ; } elseif( true == array_key_exists( 'LeaseCamPoolId', $this->getChangedColumns() ) ) { $strSql .= ' lease_cam_pool_id = ' . $this->sqlLeaseCamPoolId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cam_calculation_type_id = ' . $this->sqlCamCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'CamCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cam_calculation_type_id = ' . $this->sqlCamCalculationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' share_amount = ' . $this->sqlShareAmount(). ',' ; } elseif( true == array_key_exists( 'ShareAmount', $this->getChangedColumns() ) ) { $strSql .= ' share_amount = ' . $this->sqlShareAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_amount = ' . $this->sqlCapAmount(). ',' ; } elseif( true == array_key_exists( 'CapAmount', $this->getChangedColumns() ) ) { $strSql .= ' cap_amount = ' . $this->sqlCapAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_base_year_fixed = ' . $this->sqlIsBaseYearFixed(). ',' ; } elseif( true == array_key_exists( 'IsBaseYearFixed', $this->getChangedColumns() ) ) { $strSql .= ' is_base_year_fixed = ' . $this->sqlIsBaseYearFixed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_cam_budget_id = ' . $this->sqlPropertyCamBudgetId(). ',' ; } elseif( true == array_key_exists( 'PropertyCamBudgetId', $this->getChangedColumns() ) ) { $strSql .= ' property_cam_budget_id = ' . $this->sqlPropertyCamBudgetId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_cam_pool_id' => $this->getLeaseCamPoolId(),
			'ap_code_id' => $this->getApCodeId(),
			'cam_calculation_type_id' => $this->getCamCalculationTypeId(),
			'share_amount' => $this->getShareAmount(),
			'cap_amount' => $this->getCapAmount(),
			'is_base_year_fixed' => $this->getIsBaseYearFixed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails(),
			'property_cam_budget_id' => $this->getPropertyCamBudgetId()
		);
	}

}
?>