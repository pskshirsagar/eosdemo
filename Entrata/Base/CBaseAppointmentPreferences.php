<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAppointmentPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAppointmentPreferences extends CEosPluralBase {

	/**
	 * @return CAppointmentPreference[]
	 */
	public static function fetchAppointmentPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAppointmentPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAppointmentPreference
	 */
	public static function fetchAppointmentPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAppointmentPreference', $objDatabase );
	}

	public static function fetchAppointmentPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'appointment_preferences', $objDatabase );
	}

	public static function fetchAppointmentPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAppointmentPreference( sprintf( 'SELECT * FROM appointment_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAppointmentPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchAppointmentPreferences( sprintf( 'SELECT * FROM appointment_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAppointmentPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAppointmentPreferences( sprintf( 'SELECT * FROM appointment_preferences WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAppointmentPreferencesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchAppointmentPreferences( sprintf( 'SELECT * FROM appointment_preferences WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAppointmentPreferencesByPropertyIdByPreferenceTypeByCid( $intPropertyId, $strPreferenceType, $intCid, $objDatabase ) {
		return self::fetchAppointmentPreferences( sprintf( 'SELECT * FROM appointment_preferences WHERE property_id = %d AND appointment_preference_type = \'%s\' AND cid = %d', ( int ) $intPropertyId, ( string ) $strPreferenceType, ( int ) $intCid ), $objDatabase );
	}


}
?>