<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPhoneNumbers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CPropertyPhoneNumber[]
	 */
	public static function fetchPropertyPhoneNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyPhoneNumber::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyPhoneNumber
	 */
	public static function fetchPropertyPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyPhoneNumber::class, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_phone_numbers', $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyPhoneNumber( sprintf( 'SELECT * FROM property_phone_numbers WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyPhoneNumbers( sprintf( 'SELECT * FROM property_phone_numbers WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPhoneNumbers( sprintf( 'SELECT * FROM property_phone_numbers WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPhoneNumberTypeIdByCid( $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyPhoneNumbers( sprintf( 'SELECT * FROM property_phone_numbers WHERE phone_number_type_id = %d AND cid = %d', $intPhoneNumberTypeId, $intCid ), $objDatabase );
	}

}
?>