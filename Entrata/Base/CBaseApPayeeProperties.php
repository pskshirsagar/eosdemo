<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeProperties extends CEosPluralBase {

	/**
	 * @return CApPayeeProperty[]
	 */
	public static function fetchApPayeeProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPayeeProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeProperty
	 */
	public static function fetchApPayeeProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPayeeProperty', $objDatabase );
	}

	public static function fetchApPayeePropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_properties', $objDatabase );
	}

	public static function fetchApPayeePropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeProperty( sprintf( 'SELECT * FROM ap_payee_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeProperties( sprintf( 'SELECT * FROM ap_payee_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApPayeeProperties( sprintf( 'SELECT * FROM ap_payee_properties WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertiesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApPayeeProperties( sprintf( 'SELECT * FROM ap_payee_properties WHERE ap_payee_location_id = %d AND cid = %d', ( int ) $intApPayeeLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeePropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApPayeeProperties( sprintf( 'SELECT * FROM ap_payee_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>