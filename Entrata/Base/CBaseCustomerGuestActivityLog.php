<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerGuestActivityLog extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_guest_activity_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerGuestId;
	protected $m_strCheckInTime;
	protected $m_strCheckOutTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCheckedInBy;
	protected $m_intCheckedOutBy;
	protected $m_intCustomerId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_guest_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerGuestId', trim( $arrValues['customer_guest_id'] ) ); elseif( isset( $arrValues['customer_guest_id'] ) ) $this->setCustomerGuestId( $arrValues['customer_guest_id'] );
		if( isset( $arrValues['check_in_time'] ) && $boolDirectSet ) $this->set( 'm_strCheckInTime', trim( $arrValues['check_in_time'] ) ); elseif( isset( $arrValues['check_in_time'] ) ) $this->setCheckInTime( $arrValues['check_in_time'] );
		if( isset( $arrValues['check_out_time'] ) && $boolDirectSet ) $this->set( 'm_strCheckOutTime', trim( $arrValues['check_out_time'] ) ); elseif( isset( $arrValues['check_out_time'] ) ) $this->setCheckOutTime( $arrValues['check_out_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['checked_in_by'] ) && $boolDirectSet ) $this->set( 'm_intCheckedInBy', trim( $arrValues['checked_in_by'] ) ); elseif( isset( $arrValues['checked_in_by'] ) ) $this->setCheckedInBy( $arrValues['checked_in_by'] );
		if( isset( $arrValues['checked_out_by'] ) && $boolDirectSet ) $this->set( 'm_intCheckedOutBy', trim( $arrValues['checked_out_by'] ) ); elseif( isset( $arrValues['checked_out_by'] ) ) $this->setCheckedOutBy( $arrValues['checked_out_by'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerGuestId( $intCustomerGuestId ) {
		$this->set( 'm_intCustomerGuestId', CStrings::strToIntDef( $intCustomerGuestId, NULL, false ) );
	}

	public function getCustomerGuestId() {
		return $this->m_intCustomerGuestId;
	}

	public function sqlCustomerGuestId() {
		return ( true == isset( $this->m_intCustomerGuestId ) ) ? ( string ) $this->m_intCustomerGuestId : 'NULL';
	}

	public function setCheckInTime( $strCheckInTime ) {
		$this->set( 'm_strCheckInTime', CStrings::strTrimDef( $strCheckInTime, -1, NULL, true ) );
	}

	public function getCheckInTime() {
		return $this->m_strCheckInTime;
	}

	public function sqlCheckInTime() {
		return ( true == isset( $this->m_strCheckInTime ) ) ? '\'' . $this->m_strCheckInTime . '\'' : 'NOW()';
	}

	public function setCheckOutTime( $strCheckOutTime ) {
		$this->set( 'm_strCheckOutTime', CStrings::strTrimDef( $strCheckOutTime, -1, NULL, true ) );
	}

	public function getCheckOutTime() {
		return $this->m_strCheckOutTime;
	}

	public function sqlCheckOutTime() {
		return ( true == isset( $this->m_strCheckOutTime ) ) ? '\'' . $this->m_strCheckOutTime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCheckedInBy( $intCheckedInBy ) {
		$this->set( 'm_intCheckedInBy', CStrings::strToIntDef( $intCheckedInBy, NULL, false ) );
	}

	public function getCheckedInBy() {
		return $this->m_intCheckedInBy;
	}

	public function sqlCheckedInBy() {
		return ( true == isset( $this->m_intCheckedInBy ) ) ? ( string ) $this->m_intCheckedInBy : 'NULL';
	}

	public function setCheckedOutBy( $intCheckedOutBy ) {
		$this->set( 'm_intCheckedOutBy', CStrings::strToIntDef( $intCheckedOutBy, NULL, false ) );
	}

	public function getCheckedOutBy() {
		return $this->m_intCheckedOutBy;
	}

	public function sqlCheckedOutBy() {
		return ( true == isset( $this->m_intCheckedOutBy ) ) ? ( string ) $this->m_intCheckedOutBy : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_guest_id, check_in_time, check_out_time, updated_by, updated_on, created_by, created_on, checked_in_by, checked_out_by, customer_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
		          $this->sqlCustomerGuestId() . ', ' .
		          $this->sqlCheckInTime() . ', ' .
		          $this->sqlCheckOutTime() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCheckedInBy() . ', ' .
						$this->sqlCheckedOutBy() . ', ' .
						$this->sqlCustomerId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_guest_id = ' . $this->sqlCustomerGuestId(). ',' ; } elseif( true == array_key_exists( 'CustomerGuestId', $this->getChangedColumns() ) ) { $strSql .= ' customer_guest_id = ' . $this->sqlCustomerGuestId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_in_time = ' . $this->sqlCheckInTime(). ',' ; } elseif( true == array_key_exists( 'CheckInTime', $this->getChangedColumns() ) ) { $strSql .= ' check_in_time = ' . $this->sqlCheckInTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_out_time = ' . $this->sqlCheckOutTime(). ',' ; } elseif( true == array_key_exists( 'CheckOutTime', $this->getChangedColumns() ) ) { $strSql .= ' check_out_time = ' . $this->sqlCheckOutTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checked_in_by = ' . $this->sqlCheckedInBy(). ',' ; } elseif( true == array_key_exists( 'CheckedInBy', $this->getChangedColumns() ) ) { $strSql .= ' checked_in_by = ' . $this->sqlCheckedInBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checked_out_by = ' . $this->sqlCheckedOutBy(). ',' ; } elseif( true == array_key_exists( 'CheckedOutBy', $this->getChangedColumns() ) ) { $strSql .= ' checked_out_by = ' . $this->sqlCheckedOutBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_guest_id' => $this->getCustomerGuestId(),
			'check_in_time' => $this->getCheckInTime(),
			'check_out_time' => $this->getCheckOutTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'checked_in_by' => $this->getCheckedInBy(),
			'checked_out_by' => $this->getCheckedOutBy(),
			'customer_id' => $this->getCustomerId()
		);
	}

}
?>