<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportNotificationEmails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportNotificationEmails extends CEosPluralBase {

	/**
	 * @return CAccountingExportNotificationEmail[]
	 */
	public static function fetchAccountingExportNotificationEmails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountingExportNotificationEmail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountingExportNotificationEmail
	 */
	public static function fetchAccountingExportNotificationEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportNotificationEmail::class, $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_notification_emails', $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmail( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmails( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailByAccountingExportTypeIdByCid( $intAccountingExportTypeId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmail( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE accounting_export_type_id = %d AND cid = %d', ( int ) $intAccountingExportTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailsByAccountingExportTypeIdByCid( $intAccountingExportTypeId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmails( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE accounting_export_type_id = %d AND cid = %d', ( int ) $intAccountingExportTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailBySchedulerIdByCid( $intSchedulerId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmail( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE scheduler_id = %d AND cid = %d', ( int ) $intSchedulerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportNotificationEmailsBySchedulerIdByCid( $intSchedulerId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportNotificationEmails( sprintf( 'SELECT * FROM accounting_export_notification_emails WHERE scheduler_id = %d AND cid = %d', ( int ) $intSchedulerId, ( int ) $intCid ), $objDatabase );
	}

}
?>