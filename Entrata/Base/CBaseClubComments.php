<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClubs
 * Do not add any new functions to this class.
 */

class CBaseClubComments extends CEosPluralBase {
	/**
	 * @return CClub
	 */
	public static function fetchClubComment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClubComment::class, $objDatabase );
	}

}
?>