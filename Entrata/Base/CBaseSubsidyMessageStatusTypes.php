<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessageStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyMessageStatusTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyMessageStatusType[]
	 */
	public static function fetchSubsidyMessageStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyMessageStatusType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyMessageStatusType
	 */
	public static function fetchSubsidyMessageStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyMessageStatusType::class, $objDatabase );
	}

	public static function fetchSubsidyMessageStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_message_status_types', $objDatabase );
	}

	public static function fetchSubsidyMessageStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyMessageStatusType( sprintf( 'SELECT * FROM subsidy_message_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>