<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLateFeeBatchDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.late_fee_batch_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLateFeeBatchId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intLateFeeFormulaId;
	protected $m_intLateFeeActionTypeId;
	protected $m_intArTransactionId;
	protected $m_intParentLateFeeBatchDetailId;
	protected $m_strPostDate;
	protected $m_fltPostedAmount;
	protected $m_fltSuggestedAmount;
	protected $m_strMemo;
	protected $m_intIsInitialLateFee;
	protected $m_intIsLegalFee;
	protected $m_intIsExpired;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsInitialLateFee = '0';
		$this->m_intIsLegalFee = '0';
		$this->m_intIsExpired = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['late_fee_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeBatchId', trim( $arrValues['late_fee_batch_id'] ) ); elseif( isset( $arrValues['late_fee_batch_id'] ) ) $this->setLateFeeBatchId( $arrValues['late_fee_batch_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeFormulaId', trim( $arrValues['late_fee_formula_id'] ) ); elseif( isset( $arrValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrValues['late_fee_formula_id'] );
		if( isset( $arrValues['late_fee_action_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeActionTypeId', trim( $arrValues['late_fee_action_type_id'] ) ); elseif( isset( $arrValues['late_fee_action_type_id'] ) ) $this->setLateFeeActionTypeId( $arrValues['late_fee_action_type_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['parent_late_fee_batch_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intParentLateFeeBatchDetailId', trim( $arrValues['parent_late_fee_batch_detail_id'] ) ); elseif( isset( $arrValues['parent_late_fee_batch_detail_id'] ) ) $this->setParentLateFeeBatchDetailId( $arrValues['parent_late_fee_batch_detail_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['posted_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPostedAmount', trim( $arrValues['posted_amount'] ) ); elseif( isset( $arrValues['posted_amount'] ) ) $this->setPostedAmount( $arrValues['posted_amount'] );
		if( isset( $arrValues['suggested_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSuggestedAmount', trim( $arrValues['suggested_amount'] ) ); elseif( isset( $arrValues['suggested_amount'] ) ) $this->setSuggestedAmount( $arrValues['suggested_amount'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['is_initial_late_fee'] ) && $boolDirectSet ) $this->set( 'm_intIsInitialLateFee', trim( $arrValues['is_initial_late_fee'] ) ); elseif( isset( $arrValues['is_initial_late_fee'] ) ) $this->setIsInitialLateFee( $arrValues['is_initial_late_fee'] );
		if( isset( $arrValues['is_legal_fee'] ) && $boolDirectSet ) $this->set( 'm_intIsLegalFee', trim( $arrValues['is_legal_fee'] ) ); elseif( isset( $arrValues['is_legal_fee'] ) ) $this->setIsLegalFee( $arrValues['is_legal_fee'] );
		if( isset( $arrValues['is_expired'] ) && $boolDirectSet ) $this->set( 'm_intIsExpired', trim( $arrValues['is_expired'] ) ); elseif( isset( $arrValues['is_expired'] ) ) $this->setIsExpired( $arrValues['is_expired'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLateFeeBatchId( $intLateFeeBatchId ) {
		$this->set( 'm_intLateFeeBatchId', CStrings::strToIntDef( $intLateFeeBatchId, NULL, false ) );
	}

	public function getLateFeeBatchId() {
		return $this->m_intLateFeeBatchId;
	}

	public function sqlLateFeeBatchId() {
		return ( true == isset( $this->m_intLateFeeBatchId ) ) ? ( string ) $this->m_intLateFeeBatchId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->set( 'm_intLateFeeFormulaId', CStrings::strToIntDef( $intLateFeeFormulaId, NULL, false ) );
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function sqlLateFeeFormulaId() {
		return ( true == isset( $this->m_intLateFeeFormulaId ) ) ? ( string ) $this->m_intLateFeeFormulaId : 'NULL';
	}

	public function setLateFeeActionTypeId( $intLateFeeActionTypeId ) {
		$this->set( 'm_intLateFeeActionTypeId', CStrings::strToIntDef( $intLateFeeActionTypeId, NULL, false ) );
	}

	public function getLateFeeActionTypeId() {
		return $this->m_intLateFeeActionTypeId;
	}

	public function sqlLateFeeActionTypeId() {
		return ( true == isset( $this->m_intLateFeeActionTypeId ) ) ? ( string ) $this->m_intLateFeeActionTypeId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setParentLateFeeBatchDetailId( $intParentLateFeeBatchDetailId ) {
		$this->set( 'm_intParentLateFeeBatchDetailId', CStrings::strToIntDef( $intParentLateFeeBatchDetailId, NULL, false ) );
	}

	public function getParentLateFeeBatchDetailId() {
		return $this->m_intParentLateFeeBatchDetailId;
	}

	public function sqlParentLateFeeBatchDetailId() {
		return ( true == isset( $this->m_intParentLateFeeBatchDetailId ) ) ? ( string ) $this->m_intParentLateFeeBatchDetailId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostedAmount( $fltPostedAmount ) {
		$this->set( 'm_fltPostedAmount', CStrings::strToFloatDef( $fltPostedAmount, NULL, false, 2 ) );
	}

	public function getPostedAmount() {
		return $this->m_fltPostedAmount;
	}

	public function sqlPostedAmount() {
		return ( true == isset( $this->m_fltPostedAmount ) ) ? ( string ) $this->m_fltPostedAmount : 'NULL';
	}

	public function setSuggestedAmount( $fltSuggestedAmount ) {
		$this->set( 'm_fltSuggestedAmount', CStrings::strToFloatDef( $fltSuggestedAmount, NULL, false, 2 ) );
	}

	public function getSuggestedAmount() {
		return $this->m_fltSuggestedAmount;
	}

	public function sqlSuggestedAmount() {
		return ( true == isset( $this->m_fltSuggestedAmount ) ) ? ( string ) $this->m_fltSuggestedAmount : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 2000, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setIsInitialLateFee( $intIsInitialLateFee ) {
		$this->set( 'm_intIsInitialLateFee', CStrings::strToIntDef( $intIsInitialLateFee, NULL, false ) );
	}

	public function getIsInitialLateFee() {
		return $this->m_intIsInitialLateFee;
	}

	public function sqlIsInitialLateFee() {
		return ( true == isset( $this->m_intIsInitialLateFee ) ) ? ( string ) $this->m_intIsInitialLateFee : '0';
	}

	public function setIsLegalFee( $intIsLegalFee ) {
		$this->set( 'm_intIsLegalFee', CStrings::strToIntDef( $intIsLegalFee, NULL, false ) );
	}

	public function getIsLegalFee() {
		return $this->m_intIsLegalFee;
	}

	public function sqlIsLegalFee() {
		return ( true == isset( $this->m_intIsLegalFee ) ) ? ( string ) $this->m_intIsLegalFee : '0';
	}

	public function setIsExpired( $intIsExpired ) {
		$this->set( 'm_intIsExpired', CStrings::strToIntDef( $intIsExpired, NULL, false ) );
	}

	public function getIsExpired() {
		return $this->m_intIsExpired;
	}

	public function sqlIsExpired() {
		return ( true == isset( $this->m_intIsExpired ) ) ? ( string ) $this->m_intIsExpired : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, late_fee_batch_id, lease_id, customer_id, late_fee_formula_id, late_fee_action_type_id, ar_transaction_id, parent_late_fee_batch_detail_id, post_date, posted_amount, suggested_amount, memo, is_initial_late_fee, is_legal_fee, is_expired, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLateFeeBatchId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLateFeeFormulaId() . ', ' .
 						$this->sqlLateFeeActionTypeId() . ', ' .
 						$this->sqlArTransactionId() . ', ' .
 						$this->sqlParentLateFeeBatchDetailId() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlPostedAmount() . ', ' .
 						$this->sqlSuggestedAmount() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlIsInitialLateFee() . ', ' .
 						$this->sqlIsLegalFee() . ', ' .
 						$this->sqlIsExpired() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_batch_id = ' . $this->sqlLateFeeBatchId() . ','; } elseif( true == array_key_exists( 'LateFeeBatchId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_batch_id = ' . $this->sqlLateFeeBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ','; } elseif( true == array_key_exists( 'LateFeeFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_action_type_id = ' . $this->sqlLateFeeActionTypeId() . ','; } elseif( true == array_key_exists( 'LateFeeActionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_action_type_id = ' . $this->sqlLateFeeActionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_late_fee_batch_detail_id = ' . $this->sqlParentLateFeeBatchDetailId() . ','; } elseif( true == array_key_exists( 'ParentLateFeeBatchDetailId', $this->getChangedColumns() ) ) { $strSql .= ' parent_late_fee_batch_detail_id = ' . $this->sqlParentLateFeeBatchDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_amount = ' . $this->sqlPostedAmount() . ','; } elseif( true == array_key_exists( 'PostedAmount', $this->getChangedColumns() ) ) { $strSql .= ' posted_amount = ' . $this->sqlPostedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suggested_amount = ' . $this->sqlSuggestedAmount() . ','; } elseif( true == array_key_exists( 'SuggestedAmount', $this->getChangedColumns() ) ) { $strSql .= ' suggested_amount = ' . $this->sqlSuggestedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_late_fee = ' . $this->sqlIsInitialLateFee() . ','; } elseif( true == array_key_exists( 'IsInitialLateFee', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_late_fee = ' . $this->sqlIsInitialLateFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_legal_fee = ' . $this->sqlIsLegalFee() . ','; } elseif( true == array_key_exists( 'IsLegalFee', $this->getChangedColumns() ) ) { $strSql .= ' is_legal_fee = ' . $this->sqlIsLegalFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_expired = ' . $this->sqlIsExpired() . ','; } elseif( true == array_key_exists( 'IsExpired', $this->getChangedColumns() ) ) { $strSql .= ' is_expired = ' . $this->sqlIsExpired() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'late_fee_batch_id' => $this->getLateFeeBatchId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'late_fee_formula_id' => $this->getLateFeeFormulaId(),
			'late_fee_action_type_id' => $this->getLateFeeActionTypeId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'parent_late_fee_batch_detail_id' => $this->getParentLateFeeBatchDetailId(),
			'post_date' => $this->getPostDate(),
			'posted_amount' => $this->getPostedAmount(),
			'suggested_amount' => $this->getSuggestedAmount(),
			'memo' => $this->getMemo(),
			'is_initial_late_fee' => $this->getIsInitialLateFee(),
			'is_legal_fee' => $this->getIsLegalFee(),
			'is_expired' => $this->getIsExpired(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>