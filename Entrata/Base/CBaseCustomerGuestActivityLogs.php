<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerGuestActivityLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerGuestActivityLogs extends CEosPluralBase {

	/**
	 * @return CCustomerGuestActivityLog[]
	 */
	public static function fetchCustomerGuestActivityLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerGuestActivityLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerGuestActivityLog
	 */
	public static function fetchCustomerGuestActivityLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerGuestActivityLog::class, $objDatabase );
	}

	public static function fetchCustomerGuestActivityLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_guest_activity_logs', $objDatabase );
	}

	public static function fetchCustomerGuestActivityLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuestActivityLog( sprintf( 'SELECT * FROM customer_guest_activity_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestActivityLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerGuestActivityLogs( sprintf( 'SELECT * FROM customer_guest_activity_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestActivityLogsByCustomerGuestIdByCid( $intCustomerGuestId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuestActivityLogs( sprintf( 'SELECT * FROM customer_guest_activity_logs WHERE customer_guest_id = %d AND cid = %d', $intCustomerGuestId, $intCid ), $objDatabase );
	}

}
?>