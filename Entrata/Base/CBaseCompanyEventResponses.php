<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEventResponses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyEventResponses extends CEosPluralBase {

	/**
	 * @return CCompanyEventResponse[]
	 */
	public static function fetchCompanyEventResponses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyEventResponse', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyEventResponse
	 */
	public static function fetchCompanyEventResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyEventResponse', $objDatabase );
	}

	public static function fetchCompanyEventResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_event_responses', $objDatabase );
	}

	public static function fetchCompanyEventResponseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponse( sprintf( 'SELECT * FROM company_event_responses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventResponsesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponses( sprintf( 'SELECT * FROM company_event_responses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventResponsesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponses( sprintf( 'SELECT * FROM company_event_responses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventResponsesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponses( sprintf( 'SELECT * FROM company_event_responses WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventResponsesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponses( sprintf( 'SELECT * FROM company_event_responses WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyEventResponsesByPropertyEventIdByCid( $intPropertyEventId, $intCid, $objDatabase ) {
		return self::fetchCompanyEventResponses( sprintf( 'SELECT * FROM company_event_responses WHERE property_event_id = %d AND cid = %d', ( int ) $intPropertyEventId, ( int ) $intCid ), $objDatabase );
	}

}
?>