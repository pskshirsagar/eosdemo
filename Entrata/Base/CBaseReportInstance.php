<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportInstance extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.report_instances';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReportGroupId;
	protected $m_intReportId;
	protected $m_intReportVersionId;
	protected $m_intModuleId;
	protected $m_intParentModuleId;
	protected $m_intReportFilterId;
	protected $m_intDefaultReportInstanceId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['report_group_id'] ) && $boolDirectSet ) $this->set( 'm_intReportGroupId', trim( $arrValues['report_group_id'] ) ); elseif( isset( $arrValues['report_group_id'] ) ) $this->setReportGroupId( $arrValues['report_group_id'] );
		if( isset( $arrValues['report_id'] ) && $boolDirectSet ) $this->set( 'm_intReportId', trim( $arrValues['report_id'] ) ); elseif( isset( $arrValues['report_id'] ) ) $this->setReportId( $arrValues['report_id'] );
		if( isset( $arrValues['report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intReportVersionId', trim( $arrValues['report_version_id'] ) ); elseif( isset( $arrValues['report_version_id'] ) ) $this->setReportVersionId( $arrValues['report_version_id'] );
		if( isset( $arrValues['module_id'] ) && $boolDirectSet ) $this->set( 'm_intModuleId', trim( $arrValues['module_id'] ) ); elseif( isset( $arrValues['module_id'] ) ) $this->setModuleId( $arrValues['module_id'] );
		if( isset( $arrValues['parent_module_id'] ) && $boolDirectSet ) $this->set( 'm_intParentModuleId', trim( $arrValues['parent_module_id'] ) ); elseif( isset( $arrValues['parent_module_id'] ) ) $this->setParentModuleId( $arrValues['parent_module_id'] );
		if( isset( $arrValues['report_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intReportFilterId', trim( $arrValues['report_filter_id'] ) ); elseif( isset( $arrValues['report_filter_id'] ) ) $this->setReportFilterId( $arrValues['report_filter_id'] );
		if( isset( $arrValues['default_report_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportInstanceId', trim( $arrValues['default_report_instance_id'] ) ); elseif( isset( $arrValues['default_report_instance_id'] ) ) $this->setDefaultReportInstanceId( $arrValues['default_report_instance_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->set( 'm_intReportGroupId', CStrings::strToIntDef( $intReportGroupId, NULL, false ) );
	}

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function sqlReportGroupId() {
		return ( true == isset( $this->m_intReportGroupId ) ) ? ( string ) $this->m_intReportGroupId : 'NULL';
	}

	public function setReportId( $intReportId ) {
		$this->set( 'm_intReportId', CStrings::strToIntDef( $intReportId, NULL, false ) );
	}

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function sqlReportId() {
		return ( true == isset( $this->m_intReportId ) ) ? ( string ) $this->m_intReportId : 'NULL';
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->set( 'm_intReportVersionId', CStrings::strToIntDef( $intReportVersionId, NULL, false ) );
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function sqlReportVersionId() {
		return ( true == isset( $this->m_intReportVersionId ) ) ? ( string ) $this->m_intReportVersionId : 'NULL';
	}

	public function setModuleId( $intModuleId ) {
		$this->set( 'm_intModuleId', CStrings::strToIntDef( $intModuleId, NULL, false ) );
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : 'NULL';
	}

	public function setParentModuleId( $intParentModuleId ) {
		$this->set( 'm_intParentModuleId', CStrings::strToIntDef( $intParentModuleId, NULL, false ) );
	}

	public function getParentModuleId() {
		return $this->m_intParentModuleId;
	}

	public function sqlParentModuleId() {
		return ( true == isset( $this->m_intParentModuleId ) ) ? ( string ) $this->m_intParentModuleId : 'NULL';
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->set( 'm_intReportFilterId', CStrings::strToIntDef( $intReportFilterId, NULL, false ) );
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function sqlReportFilterId() {
		return ( true == isset( $this->m_intReportFilterId ) ) ? ( string ) $this->m_intReportFilterId : 'NULL';
	}

	public function setDefaultReportInstanceId( $intDefaultReportInstanceId ) {
		$this->set( 'm_intDefaultReportInstanceId', CStrings::strToIntDef( $intDefaultReportInstanceId, NULL, false ) );
	}

	public function getDefaultReportInstanceId() {
		return $this->m_intDefaultReportInstanceId;
	}

	public function sqlDefaultReportInstanceId() {
		return ( true == isset( $this->m_intDefaultReportInstanceId ) ) ? ( string ) $this->m_intDefaultReportInstanceId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, report_group_id, report_id, report_version_id, module_id, parent_module_id, report_filter_id, default_report_instance_id, name, description, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReportGroupId() . ', ' .
						$this->sqlReportId() . ', ' .
						$this->sqlReportVersionId() . ', ' .
						$this->sqlModuleId() . ', ' .
						$this->sqlParentModuleId() . ', ' .
						$this->sqlReportFilterId() . ', ' .
						$this->sqlDefaultReportInstanceId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId(). ',' ; } elseif( true == array_key_exists( 'ReportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' report_group_id = ' . $this->sqlReportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_id = ' . $this->sqlReportId(). ',' ; } elseif( true == array_key_exists( 'ReportId', $this->getChangedColumns() ) ) { $strSql .= ' report_id = ' . $this->sqlReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId(). ',' ; } elseif( true == array_key_exists( 'ReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' report_version_id = ' . $this->sqlReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_id = ' . $this->sqlModuleId(). ',' ; } elseif( true == array_key_exists( 'ModuleId', $this->getChangedColumns() ) ) { $strSql .= ' module_id = ' . $this->sqlModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId(). ',' ; } elseif( true == array_key_exists( 'ParentModuleId', $this->getChangedColumns() ) ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId(). ',' ; } elseif( true == array_key_exists( 'ReportFilterId', $this->getChangedColumns() ) ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_instance_id = ' . $this->sqlDefaultReportInstanceId(). ',' ; } elseif( true == array_key_exists( 'DefaultReportInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_instance_id = ' . $this->sqlDefaultReportInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'report_group_id' => $this->getReportGroupId(),
			'report_id' => $this->getReportId(),
			'report_version_id' => $this->getReportVersionId(),
			'module_id' => $this->getModuleId(),
			'parent_module_id' => $this->getParentModuleId(),
			'report_filter_id' => $this->getReportFilterId(),
			'default_report_instance_id' => $this->getDefaultReportInstanceId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>