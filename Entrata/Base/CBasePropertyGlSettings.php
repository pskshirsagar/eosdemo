<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGlSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGlSettings extends CEosPluralBase {

	/**
	 * @return CPropertyGlSetting[]
	 */
	public static function fetchPropertyGlSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyGlSetting::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGlSetting
	 */
	public static function fetchPropertyGlSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyGlSetting::class, $objDatabase );
	}

	public static function fetchPropertyGlSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_gl_settings', $objDatabase );
	}

	public static function fetchPropertyGlSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSetting( sprintf( 'SELECT * FROM property_gl_settings WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSetting( sprintf( 'SELECT * FROM property_gl_settings WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE gl_tree_id = %d AND cid = %d', $intGlTreeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByRentBankAccountIdByCid( $intRentBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE rent_bank_account_id = %d AND cid = %d', $intRentBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByDepositBankAccountIdByCid( $intDepositBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE deposit_bank_account_id = %d AND cid = %d', $intDepositBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByRefundBankAccountIdByCid( $intRefundBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE refund_bank_account_id = %d AND cid = %d', $intRefundBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByApBankAccountIdByCid( $intApBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE ap_bank_account_id = %d AND cid = %d', $intApBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByApGlAccountIdByCid( $intApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE ap_gl_account_id = %d AND cid = %d', $intApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByInterCoReimbursementBankAccountIdByCid( $intInterCoReimbursementBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE inter_co_reimbursement_bank_account_id = %d AND cid = %d', $intInterCoReimbursementBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByMarketRentGlAccountIdByCid( $intMarketRentGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE market_rent_gl_account_id = %d AND cid = %d', $intMarketRentGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByGainToLeaseGlAccountIdByCid( $intGainToLeaseGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE gain_to_lease_gl_account_id = %d AND cid = %d', $intGainToLeaseGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByLossToLeaseGlAccountIdByCid( $intLossToLeaseGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE loss_to_lease_gl_account_id = %d AND cid = %d', $intLossToLeaseGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByVacancyLossGlAccountIdByCid( $intVacancyLossGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE vacancy_loss_gl_account_id = %d AND cid = %d', $intVacancyLossGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByDelinquentRentGlAccountIdByCid( $intDelinquentRentGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE delinquent_rent_gl_account_id = %d AND cid = %d', $intDelinquentRentGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPrepaidRentGlAccountIdByCid( $intPrepaidRentGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE prepaid_rent_gl_account_id = %d AND cid = %d', $intPrepaidRentGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByRetainedEarningsGlAccountIdByCid( $intRetainedEarningsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE retained_earnings_gl_account_id = %d AND cid = %d', $intRetainedEarningsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByInterCoArGlAccountIdByCid( $intInterCoArGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE inter_co_ar_gl_account_id = %d AND cid = %d', $intInterCoArGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByInterCoApGlAccountIdByCid( $intInterCoApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE inter_co_ap_gl_account_id = %d AND cid = %d', $intInterCoApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPoBalancesApGlAccountIdByCid( $intPoBalancesApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE po_balances_ap_gl_account_id = %d AND cid = %d', $intPoBalancesApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPurchasesClearingGlAccountIdByCid( $intPurchasesClearingGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE purchases_clearing_gl_account_id = %d AND cid = %d', $intPurchasesClearingGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPendingReimbursementsGlAccountIdByCid( $intPendingReimbursementsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE pending_reimbursements_gl_account_id = %d AND cid = %d', $intPendingReimbursementsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByUnclaimedPropertyGlAccountIdByCid( $intUnclaimedPropertyGlAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE unclaimed_property_gl_account_id = %d AND cid = %d', $intUnclaimedPropertyGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByAccelRentArCodeIdByCid( $intAccelRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE accel_rent_ar_code_id = %d AND cid = %d', $intAccelRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsBySkipAccelRentArCodeIdByCid( $intSkipAccelRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE skip_accel_rent_ar_code_id = %d AND cid = %d', $intSkipAccelRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByRentArCodeIdByCid( $intRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE rent_ar_code_id = %d AND cid = %d', $intRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByDepositArCodeIdByCid( $intDepositArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE deposit_ar_code_id = %d AND cid = %d', $intDepositArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByPercentOfSalesArCodeIdByCid( $intPercentOfSalesArCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE percent_of_sales_ar_code_id = %d AND cid = %d', $intPercentOfSalesArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByMarketRentTypeIdByCid( $intMarketRentTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE market_rent_type_id = %d AND cid = %d', $intMarketRentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByLeaseExpirationStructureIdByCid( $intLeaseExpirationStructureId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE lease_expiration_structure_id = %d AND cid = %d', $intLeaseExpirationStructureId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByDrawRequestApHeaderTypeIdByCid( $intDrawRequestApHeaderTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE draw_request_ap_header_type_id = %d AND cid = %d', $intDrawRequestApHeaderTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByUnclaimedPropertyApPayeeIdByCid( $intUnclaimedPropertyApPayeeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE unclaimed_property_ap_payee_id = %d AND cid = %d', $intUnclaimedPropertyApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByApReceivingTypeIdByCid( $intApReceivingTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE ap_receiving_type_id = %d AND cid = %d', $intApReceivingTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByMilitaryMacBankAccountIdByCid( $intMilitaryMacBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE military_mac_bank_account_id = %d AND cid = %d', $intMilitaryMacBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByJobCostingApBankAccountIdByCid( $intJobCostingApBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE job_costing_ap_bank_account_id = %d AND cid = %d', $intJobCostingApBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyGlSettingsByCcApBankAccountIdByCid( $intCcApBankAccountId, $intCid, $objDatabase ) {
		return self::fetchPropertyGlSettings( sprintf( 'SELECT * FROM property_gl_settings WHERE cc_ap_bank_account_id = %d AND cid = %d', $intCcApBankAccountId, $intCid ), $objDatabase );
	}

}
?>