<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationItem extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_reconciliation_items';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlReconciliationId;
	protected $m_intReversingGlReconciliationId;
	protected $m_fltAmount;
	protected $m_strNotes;
	protected $m_strReferenceNumber;
	protected $m_strDate;
	protected $m_intIsInitialItem;
	protected $m_intIsReconciled;
	protected $m_intIsBankBalance;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsInitialItem = '0';
		$this->m_intIsReconciled = '1';
		$this->m_intIsBankBalance = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationId', trim( $arrValues['gl_reconciliation_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrValues['gl_reconciliation_id'] );
		if( isset( $arrValues['reversing_gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intReversingGlReconciliationId', trim( $arrValues['reversing_gl_reconciliation_id'] ) ); elseif( isset( $arrValues['reversing_gl_reconciliation_id'] ) ) $this->setReversingGlReconciliationId( $arrValues['reversing_gl_reconciliation_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_strReferenceNumber', trim( stripcslashes( $arrValues['reference_number'] ) ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_number'] ) : $arrValues['reference_number'] );
		if( isset( $arrValues['date'] ) && $boolDirectSet ) $this->set( 'm_strDate', trim( $arrValues['date'] ) ); elseif( isset( $arrValues['date'] ) ) $this->setDate( $arrValues['date'] );
		if( isset( $arrValues['is_initial_item'] ) && $boolDirectSet ) $this->set( 'm_intIsInitialItem', trim( $arrValues['is_initial_item'] ) ); elseif( isset( $arrValues['is_initial_item'] ) ) $this->setIsInitialItem( $arrValues['is_initial_item'] );
		if( isset( $arrValues['is_reconciled'] ) && $boolDirectSet ) $this->set( 'm_intIsReconciled', trim( $arrValues['is_reconciled'] ) ); elseif( isset( $arrValues['is_reconciled'] ) ) $this->setIsReconciled( $arrValues['is_reconciled'] );
		if( isset( $arrValues['is_bank_balance'] ) && $boolDirectSet ) $this->set( 'm_intIsBankBalance', trim( $arrValues['is_bank_balance'] ) ); elseif( isset( $arrValues['is_bank_balance'] ) ) $this->setIsBankBalance( $arrValues['is_bank_balance'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->set( 'm_intGlReconciliationId', CStrings::strToIntDef( $intGlReconciliationId, NULL, false ) );
	}

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function sqlGlReconciliationId() {
		return ( true == isset( $this->m_intGlReconciliationId ) ) ? ( string ) $this->m_intGlReconciliationId : 'NULL';
	}

	public function setReversingGlReconciliationId( $intReversingGlReconciliationId ) {
		$this->set( 'm_intReversingGlReconciliationId', CStrings::strToIntDef( $intReversingGlReconciliationId, NULL, false ) );
	}

	public function getReversingGlReconciliationId() {
		return $this->m_intReversingGlReconciliationId;
	}

	public function sqlReversingGlReconciliationId() {
		return ( true == isset( $this->m_intReversingGlReconciliationId ) ) ? ( string ) $this->m_intReversingGlReconciliationId : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setReferenceNumber( $strReferenceNumber ) {
		$this->set( 'm_strReferenceNumber', CStrings::strTrimDef( $strReferenceNumber, 40, NULL, true ) );
	}

	public function getReferenceNumber() {
		return $this->m_strReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_strReferenceNumber ) ) ? '\'' . addslashes( $this->m_strReferenceNumber ) . '\'' : 'NULL';
	}

	public function setDate( $strDate ) {
		$this->set( 'm_strDate', CStrings::strTrimDef( $strDate, -1, NULL, true ) );
	}

	public function getDate() {
		return $this->m_strDate;
	}

	public function sqlDate() {
		return ( true == isset( $this->m_strDate ) ) ? '\'' . $this->m_strDate . '\'' : 'NULL';
	}

	public function setIsInitialItem( $intIsInitialItem ) {
		$this->set( 'm_intIsInitialItem', CStrings::strToIntDef( $intIsInitialItem, NULL, false ) );
	}

	public function getIsInitialItem() {
		return $this->m_intIsInitialItem;
	}

	public function sqlIsInitialItem() {
		return ( true == isset( $this->m_intIsInitialItem ) ) ? ( string ) $this->m_intIsInitialItem : '0';
	}

	public function setIsReconciled( $intIsReconciled ) {
		$this->set( 'm_intIsReconciled', CStrings::strToIntDef( $intIsReconciled, NULL, false ) );
	}

	public function getIsReconciled() {
		return $this->m_intIsReconciled;
	}

	public function sqlIsReconciled() {
		return ( true == isset( $this->m_intIsReconciled ) ) ? ( string ) $this->m_intIsReconciled : '1';
	}

	public function setIsBankBalance( $intIsBankBalance ) {
		$this->set( 'm_intIsBankBalance', CStrings::strToIntDef( $intIsBankBalance, NULL, false ) );
	}

	public function getIsBankBalance() {
		return $this->m_intIsBankBalance;
	}

	public function sqlIsBankBalance() {
		return ( true == isset( $this->m_intIsBankBalance ) ) ? ( string ) $this->m_intIsBankBalance : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_reconciliation_id, reversing_gl_reconciliation_id, amount, notes, reference_number, date, is_initial_item, is_reconciled, is_bank_balance, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlGlReconciliationId() . ', ' .
 						$this->sqlReversingGlReconciliationId() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlReferenceNumber() . ', ' .
 						$this->sqlDate() . ', ' .
 						$this->sqlIsInitialItem() . ', ' .
 						$this->sqlIsReconciled() . ', ' .
 						$this->sqlIsBankBalance() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; } elseif( true == array_key_exists( 'GlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversing_gl_reconciliation_id = ' . $this->sqlReversingGlReconciliationId() . ','; } elseif( true == array_key_exists( 'ReversingGlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' reversing_gl_reconciliation_id = ' . $this->sqlReversingGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date = ' . $this->sqlDate() . ','; } elseif( true == array_key_exists( 'Date', $this->getChangedColumns() ) ) { $strSql .= ' date = ' . $this->sqlDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_item = ' . $this->sqlIsInitialItem() . ','; } elseif( true == array_key_exists( 'IsInitialItem', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_item = ' . $this->sqlIsInitialItem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reconciled = ' . $this->sqlIsReconciled() . ','; } elseif( true == array_key_exists( 'IsReconciled', $this->getChangedColumns() ) ) { $strSql .= ' is_reconciled = ' . $this->sqlIsReconciled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bank_balance = ' . $this->sqlIsBankBalance() . ','; } elseif( true == array_key_exists( 'IsBankBalance', $this->getChangedColumns() ) ) { $strSql .= ' is_bank_balance = ' . $this->sqlIsBankBalance() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_reconciliation_id' => $this->getGlReconciliationId(),
			'reversing_gl_reconciliation_id' => $this->getReversingGlReconciliationId(),
			'amount' => $this->getAmount(),
			'notes' => $this->getNotes(),
			'reference_number' => $this->getReferenceNumber(),
			'date' => $this->getDate(),
			'is_initial_item' => $this->getIsInitialItem(),
			'is_reconciled' => $this->getIsReconciled(),
			'is_bank_balance' => $this->getIsBankBalance(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>