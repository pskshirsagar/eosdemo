<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseSignatureTypes
 * Do not add any new functions to this class.
 */

class CBaseLeaseSignatureTypes extends CEosPluralBase {

	/**
	 * @return CLeaseSignatureType[]
	 */
	public static function fetchLeaseSignatureTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLeaseSignatureType::class, $objDatabase );
	}

	/**
	 * @return CLeaseSignatureType
	 */
	public static function fetchLeaseSignatureType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseSignatureType::class, $objDatabase );
	}

	public static function fetchLeaseSignatureTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_signature_types', $objDatabase );
	}

	public static function fetchLeaseSignatureTypeById( $intId, $objDatabase ) {
		return self::fetchLeaseSignatureType( sprintf( 'SELECT * FROM lease_signature_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>