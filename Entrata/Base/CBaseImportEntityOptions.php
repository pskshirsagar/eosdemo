<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportEntityOptions
 * Do not add any new functions to this class.
 */

class CBaseImportEntityOptions extends CEosPluralBase {

	/**
	 * @return CImportEntityOption[]
	 */
	public static function fetchImportEntityOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CImportEntityOption::class, $objDatabase );
	}

	/**
	 * @return CImportEntityOption
	 */
	public static function fetchImportEntityOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CImportEntityOption::class, $objDatabase );
	}

	public static function fetchImportEntityOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_entity_options', $objDatabase );
	}

	public static function fetchImportEntityOptionById( $intId, $objDatabase ) {
		return self::fetchImportEntityOption( sprintf( 'SELECT * FROM import_entity_options WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchImportEntityOptionsByImportEntityId( $intImportEntityId, $objDatabase ) {
		return self::fetchImportEntityOptions( sprintf( 'SELECT * FROM import_entity_options WHERE import_entity_id = %d', $intImportEntityId ), $objDatabase );
	}

}
?>