<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContactSubmission extends CEosSingularBase {

	const TABLE_NAME = 'public.contact_submissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intCustomerId;
	protected $m_intApPayeeId;
	protected $m_intCompanyOwnerId;
	protected $m_intCompanyEmployeeId;
	protected $m_intResponseRequestTypeId;
	protected $m_intCustomerMessageId;
	protected $m_intContactSubmissionTypeId;
	protected $m_intListItemsId;
	protected $m_strContactDatetime;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strUnitNumber;
	protected $m_strPhoneNumber;
	protected $m_strOfficeNumber;
	protected $m_strMobileNumber;
	protected $m_strEmailAddress;
	protected $m_strSubject;
	protected $m_strMessage;
	protected $m_strMoveOutDate;
	protected $m_intClosedBy;
	protected $m_strClosedOn;
	protected $m_intViewedBy;
	protected $m_strViewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['company_owner_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyOwnerId', trim( $arrValues['company_owner_id'] ) ); elseif( isset( $arrValues['company_owner_id'] ) ) $this->setCompanyOwnerId( $arrValues['company_owner_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['response_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseRequestTypeId', trim( $arrValues['response_request_type_id'] ) ); elseif( isset( $arrValues['response_request_type_id'] ) ) $this->setResponseRequestTypeId( $arrValues['response_request_type_id'] );
		if( isset( $arrValues['customer_message_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerMessageId', trim( $arrValues['customer_message_id'] ) ); elseif( isset( $arrValues['customer_message_id'] ) ) $this->setCustomerMessageId( $arrValues['customer_message_id'] );
		if( isset( $arrValues['contact_submission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContactSubmissionTypeId', trim( $arrValues['contact_submission_type_id'] ) ); elseif( isset( $arrValues['contact_submission_type_id'] ) ) $this->setContactSubmissionTypeId( $arrValues['contact_submission_type_id'] );
		if( isset( $arrValues['list_items_id'] ) && $boolDirectSet ) $this->set( 'm_intListItemsId', trim( $arrValues['list_items_id'] ) ); elseif( isset( $arrValues['list_items_id'] ) ) $this->setListItemsId( $arrValues['list_items_id'] );
		if( isset( $arrValues['contact_datetime'] ) && $boolDirectSet ) $this->set( 'm_strContactDatetime', trim( $arrValues['contact_datetime'] ) ); elseif( isset( $arrValues['contact_datetime'] ) ) $this->setContactDatetime( $arrValues['contact_datetime'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['office_number'] ) && $boolDirectSet ) $this->set( 'm_strOfficeNumber', trim( stripcslashes( $arrValues['office_number'] ) ) ); elseif( isset( $arrValues['office_number'] ) ) $this->setOfficeNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['office_number'] ) : $arrValues['office_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( stripcslashes( $arrValues['mobile_number'] ) ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mobile_number'] ) : $arrValues['mobile_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['closed_by'] ) && $boolDirectSet ) $this->set( 'm_intClosedBy', trim( $arrValues['closed_by'] ) ); elseif( isset( $arrValues['closed_by'] ) ) $this->setClosedBy( $arrValues['closed_by'] );
		if( isset( $arrValues['closed_on'] ) && $boolDirectSet ) $this->set( 'm_strClosedOn', trim( $arrValues['closed_on'] ) ); elseif( isset( $arrValues['closed_on'] ) ) $this->setClosedOn( $arrValues['closed_on'] );
		if( isset( $arrValues['viewed_by'] ) && $boolDirectSet ) $this->set( 'm_intViewedBy', trim( $arrValues['viewed_by'] ) ); elseif( isset( $arrValues['viewed_by'] ) ) $this->setViewedBy( $arrValues['viewed_by'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setCompanyOwnerId( $intCompanyOwnerId ) {
		$this->set( 'm_intCompanyOwnerId', CStrings::strToIntDef( $intCompanyOwnerId, NULL, false ) );
	}

	public function getCompanyOwnerId() {
		return $this->m_intCompanyOwnerId;
	}

	public function sqlCompanyOwnerId() {
		return ( true == isset( $this->m_intCompanyOwnerId ) ) ? ( string ) $this->m_intCompanyOwnerId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setResponseRequestTypeId( $intResponseRequestTypeId ) {
		$this->set( 'm_intResponseRequestTypeId', CStrings::strToIntDef( $intResponseRequestTypeId, NULL, false ) );
	}

	public function getResponseRequestTypeId() {
		return $this->m_intResponseRequestTypeId;
	}

	public function sqlResponseRequestTypeId() {
		return ( true == isset( $this->m_intResponseRequestTypeId ) ) ? ( string ) $this->m_intResponseRequestTypeId : 'NULL';
	}

	public function setCustomerMessageId( $intCustomerMessageId ) {
		$this->set( 'm_intCustomerMessageId', CStrings::strToIntDef( $intCustomerMessageId, NULL, false ) );
	}

	public function getCustomerMessageId() {
		return $this->m_intCustomerMessageId;
	}

	public function sqlCustomerMessageId() {
		return ( true == isset( $this->m_intCustomerMessageId ) ) ? ( string ) $this->m_intCustomerMessageId : 'NULL';
	}

	public function setContactSubmissionTypeId( $intContactSubmissionTypeId ) {
		$this->set( 'm_intContactSubmissionTypeId', CStrings::strToIntDef( $intContactSubmissionTypeId, NULL, false ) );
	}

	public function getContactSubmissionTypeId() {
		return $this->m_intContactSubmissionTypeId;
	}

	public function sqlContactSubmissionTypeId() {
		return ( true == isset( $this->m_intContactSubmissionTypeId ) ) ? ( string ) $this->m_intContactSubmissionTypeId : 'NULL';
	}

	public function setListItemsId( $intListItemsId ) {
		$this->set( 'm_intListItemsId', CStrings::strToIntDef( $intListItemsId, NULL, false ) );
	}

	public function getListItemsId() {
		return $this->m_intListItemsId;
	}

	public function sqlListItemsId() {
		return ( true == isset( $this->m_intListItemsId ) ) ? ( string ) $this->m_intListItemsId : 'NULL';
	}

	public function setContactDatetime( $strContactDatetime ) {
		$this->set( 'm_strContactDatetime', CStrings::strTrimDef( $strContactDatetime, -1, NULL, true ) );
	}

	public function getContactDatetime() {
		return $this->m_strContactDatetime;
	}

	public function sqlContactDatetime() {
		return ( true == isset( $this->m_strContactDatetime ) ) ? '\'' . $this->m_strContactDatetime . '\'' : 'NOW()';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setOfficeNumber( $strOfficeNumber ) {
		$this->set( 'm_strOfficeNumber', CStrings::strTrimDef( $strOfficeNumber, 30, NULL, true ) );
	}

	public function getOfficeNumber() {
		return $this->m_strOfficeNumber;
	}

	public function sqlOfficeNumber() {
		return ( true == isset( $this->m_strOfficeNumber ) ) ? '\'' . addslashes( $this->m_strOfficeNumber ) . '\'' : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? '\'' . addslashes( $this->m_strMobileNumber ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 2000, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setClosedBy( $intClosedBy ) {
		$this->set( 'm_intClosedBy', CStrings::strToIntDef( $intClosedBy, NULL, false ) );
	}

	public function getClosedBy() {
		return $this->m_intClosedBy;
	}

	public function sqlClosedBy() {
		return ( true == isset( $this->m_intClosedBy ) ) ? ( string ) $this->m_intClosedBy : 'NULL';
	}

	public function setClosedOn( $strClosedOn ) {
		$this->set( 'm_strClosedOn', CStrings::strTrimDef( $strClosedOn, -1, NULL, true ) );
	}

	public function getClosedOn() {
		return $this->m_strClosedOn;
	}

	public function sqlClosedOn() {
		return ( true == isset( $this->m_strClosedOn ) ) ? '\'' . $this->m_strClosedOn . '\'' : 'NULL';
	}

	public function setViewedBy( $intViewedBy ) {
		$this->set( 'm_intViewedBy', CStrings::strToIntDef( $intViewedBy, NULL, false ) );
	}

	public function getViewedBy() {
		return $this->m_intViewedBy;
	}

	public function sqlViewedBy() {
		return ( true == isset( $this->m_intViewedBy ) ) ? ( string ) $this->m_intViewedBy : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, customer_id, ap_payee_id, company_owner_id, company_employee_id, response_request_type_id, customer_message_id, contact_submission_type_id, list_items_id, contact_datetime, name_first, name_last, unit_number, phone_number, office_number, mobile_number, email_address, subject, message, move_out_date, closed_by, closed_on, viewed_by, viewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlCompanyOwnerId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlResponseRequestTypeId() . ', ' .
 						$this->sqlCustomerMessageId() . ', ' .
 						$this->sqlContactSubmissionTypeId() . ', ' .
 						$this->sqlListItemsId() . ', ' .
 						$this->sqlContactDatetime() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlOfficeNumber() . ', ' .
 						$this->sqlMobileNumber() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlMoveOutDate() . ', ' .
 						$this->sqlClosedBy() . ', ' .
 						$this->sqlClosedOn() . ', ' .
 						$this->sqlViewedBy() . ', ' .
 						$this->sqlViewedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_owner_id = ' . $this->sqlCompanyOwnerId() . ','; } elseif( true == array_key_exists( 'CompanyOwnerId', $this->getChangedColumns() ) ) { $strSql .= ' company_owner_id = ' . $this->sqlCompanyOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_request_type_id = ' . $this->sqlResponseRequestTypeId() . ','; } elseif( true == array_key_exists( 'ResponseRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' response_request_type_id = ' . $this->sqlResponseRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_message_id = ' . $this->sqlCustomerMessageId() . ','; } elseif( true == array_key_exists( 'CustomerMessageId', $this->getChangedColumns() ) ) { $strSql .= ' customer_message_id = ' . $this->sqlCustomerMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_submission_type_id = ' . $this->sqlContactSubmissionTypeId() . ','; } elseif( true == array_key_exists( 'ContactSubmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contact_submission_type_id = ' . $this->sqlContactSubmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_items_id = ' . $this->sqlListItemsId() . ','; } elseif( true == array_key_exists( 'ListItemsId', $this->getChangedColumns() ) ) { $strSql .= ' list_items_id = ' . $this->sqlListItemsId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; } elseif( true == array_key_exists( 'ContactDatetime', $this->getChangedColumns() ) ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_number = ' . $this->sqlOfficeNumber() . ','; } elseif( true == array_key_exists( 'OfficeNumber', $this->getChangedColumns() ) ) { $strSql .= ' office_number = ' . $this->sqlOfficeNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; } elseif( true == array_key_exists( 'ClosedBy', $this->getChangedColumns() ) ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; } elseif( true == array_key_exists( 'ClosedOn', $this->getChangedColumns() ) ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; } elseif( true == array_key_exists( 'ViewedBy', $this->getChangedColumns() ) ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'customer_id' => $this->getCustomerId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'company_owner_id' => $this->getCompanyOwnerId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'response_request_type_id' => $this->getResponseRequestTypeId(),
			'customer_message_id' => $this->getCustomerMessageId(),
			'contact_submission_type_id' => $this->getContactSubmissionTypeId(),
			'list_items_id' => $this->getListItemsId(),
			'contact_datetime' => $this->getContactDatetime(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'unit_number' => $this->getUnitNumber(),
			'phone_number' => $this->getPhoneNumber(),
			'office_number' => $this->getOfficeNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'email_address' => $this->getEmailAddress(),
			'subject' => $this->getSubject(),
			'message' => $this->getMessage(),
			'move_out_date' => $this->getMoveOutDate(),
			'closed_by' => $this->getClosedBy(),
			'closed_on' => $this->getClosedOn(),
			'viewed_by' => $this->getViewedBy(),
			'viewed_on' => $this->getViewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>