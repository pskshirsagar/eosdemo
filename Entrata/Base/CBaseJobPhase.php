<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobPhase extends CEosSingularBase {

	const TABLE_NAME = 'public.job_phases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intJobId;
	protected $m_intReClassGlHeaderId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strFiscalYear;
	protected $m_intOrderNum;
	protected $m_boolIsBudgetByPeriod;
	protected $m_strOriginalStartDate;
	protected $m_strOriginalEndDate;
	protected $m_strActualStartDate;
	protected $m_strActualEndDate;
	protected $m_intContractedBy;
	protected $m_strContractedOn;
	protected $m_intStartedBy;
	protected $m_strStartedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltProFormaBudgetAmount;
	protected $m_intRenovationDaysToComplete;
	protected $m_intJobStatusId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsBudgetByPeriod = false;
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';
		$this->m_fltProFormaBudgetAmount = '0';
		$this->m_intRenovationDaysToComplete = '0';
		$this->m_intJobStatusId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['re_class_gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReClassGlHeaderId', trim( $arrValues['re_class_gl_header_id'] ) ); elseif( isset( $arrValues['re_class_gl_header_id'] ) ) $this->setReClassGlHeaderId( $arrValues['re_class_gl_header_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['fiscal_year'] ) && $boolDirectSet ) $this->set( 'm_strFiscalYear', trim( $arrValues['fiscal_year'] ) ); elseif( isset( $arrValues['fiscal_year'] ) ) $this->setFiscalYear( $arrValues['fiscal_year'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_budget_by_period'] ) && $boolDirectSet ) $this->set( 'm_boolIsBudgetByPeriod', trim( stripcslashes( $arrValues['is_budget_by_period'] ) ) ); elseif( isset( $arrValues['is_budget_by_period'] ) ) $this->setIsBudgetByPeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_budget_by_period'] ) : $arrValues['is_budget_by_period'] );
		if( isset( $arrValues['original_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalStartDate', trim( $arrValues['original_start_date'] ) ); elseif( isset( $arrValues['original_start_date'] ) ) $this->setOriginalStartDate( $arrValues['original_start_date'] );
		if( isset( $arrValues['original_end_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalEndDate', trim( $arrValues['original_end_date'] ) ); elseif( isset( $arrValues['original_end_date'] ) ) $this->setOriginalEndDate( $arrValues['original_end_date'] );
		if( isset( $arrValues['actual_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActualStartDate', trim( $arrValues['actual_start_date'] ) ); elseif( isset( $arrValues['actual_start_date'] ) ) $this->setActualStartDate( $arrValues['actual_start_date'] );
		if( isset( $arrValues['actual_end_date'] ) && $boolDirectSet ) $this->set( 'm_strActualEndDate', trim( $arrValues['actual_end_date'] ) ); elseif( isset( $arrValues['actual_end_date'] ) ) $this->setActualEndDate( $arrValues['actual_end_date'] );
		if( isset( $arrValues['contracted_by'] ) && $boolDirectSet ) $this->set( 'm_intContractedBy', trim( $arrValues['contracted_by'] ) ); elseif( isset( $arrValues['contracted_by'] ) ) $this->setContractedBy( $arrValues['contracted_by'] );
		if( isset( $arrValues['contracted_on'] ) && $boolDirectSet ) $this->set( 'm_strContractedOn', trim( $arrValues['contracted_on'] ) ); elseif( isset( $arrValues['contracted_on'] ) ) $this->setContractedOn( $arrValues['contracted_on'] );
		if( isset( $arrValues['started_by'] ) && $boolDirectSet ) $this->set( 'm_intStartedBy', trim( $arrValues['started_by'] ) ); elseif( isset( $arrValues['started_by'] ) ) $this->setStartedBy( $arrValues['started_by'] );
		if( isset( $arrValues['started_on'] ) && $boolDirectSet ) $this->set( 'm_strStartedOn', trim( $arrValues['started_on'] ) ); elseif( isset( $arrValues['started_on'] ) ) $this->setStartedOn( $arrValues['started_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['pro_forma_budget_amount'] ) && $boolDirectSet ) $this->set( 'm_fltProFormaBudgetAmount', trim( $arrValues['pro_forma_budget_amount'] ) ); elseif( isset( $arrValues['pro_forma_budget_amount'] ) ) $this->setProFormaBudgetAmount( $arrValues['pro_forma_budget_amount'] );
		if( isset( $arrValues['renovation_days_to_complete'] ) && $boolDirectSet ) $this->set( 'm_intRenovationDaysToComplete', trim( $arrValues['renovation_days_to_complete'] ) ); elseif( isset( $arrValues['renovation_days_to_complete'] ) ) $this->setRenovationDaysToComplete( $arrValues['renovation_days_to_complete'] );
		if( isset( $arrValues['job_status_id'] ) && $boolDirectSet ) $this->set( 'm_intJobStatusId', trim( $arrValues['job_status_id'] ) ); elseif( isset( $arrValues['job_status_id'] ) ) $this->setJobStatusId( $arrValues['job_status_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setReClassGlHeaderId( $intReClassGlHeaderId ) {
		$this->set( 'm_intReClassGlHeaderId', CStrings::strToIntDef( $intReClassGlHeaderId, NULL, false ) );
	}

	public function getReClassGlHeaderId() {
		return $this->m_intReClassGlHeaderId;
	}

	public function sqlReClassGlHeaderId() {
		return ( true == isset( $this->m_intReClassGlHeaderId ) ) ? ( string ) $this->m_intReClassGlHeaderId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setFiscalYear( $strFiscalYear ) {
		$this->set( 'm_strFiscalYear', CStrings::strTrimDef( $strFiscalYear, -1, NULL, true ) );
	}

	public function getFiscalYear() {
		return $this->m_strFiscalYear;
	}

	public function sqlFiscalYear() {
		return ( true == isset( $this->m_strFiscalYear ) ) ? '\'' . $this->m_strFiscalYear . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsBudgetByPeriod( $boolIsBudgetByPeriod ) {
		$this->set( 'm_boolIsBudgetByPeriod', CStrings::strToBool( $boolIsBudgetByPeriod ) );
	}

	public function getIsBudgetByPeriod() {
		return $this->m_boolIsBudgetByPeriod;
	}

	public function sqlIsBudgetByPeriod() {
		return ( true == isset( $this->m_boolIsBudgetByPeriod ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBudgetByPeriod ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOriginalStartDate( $strOriginalStartDate ) {
		$this->set( 'm_strOriginalStartDate', CStrings::strTrimDef( $strOriginalStartDate, -1, NULL, true ) );
	}

	public function getOriginalStartDate() {
		return $this->m_strOriginalStartDate;
	}

	public function sqlOriginalStartDate() {
		return ( true == isset( $this->m_strOriginalStartDate ) ) ? '\'' . $this->m_strOriginalStartDate . '\'' : 'NOW()';
	}

	public function setOriginalEndDate( $strOriginalEndDate ) {
		$this->set( 'm_strOriginalEndDate', CStrings::strTrimDef( $strOriginalEndDate, -1, NULL, true ) );
	}

	public function getOriginalEndDate() {
		return $this->m_strOriginalEndDate;
	}

	public function sqlOriginalEndDate() {
		return ( true == isset( $this->m_strOriginalEndDate ) ) ? '\'' . $this->m_strOriginalEndDate . '\'' : 'NOW()';
	}

	public function setActualStartDate( $strActualStartDate ) {
		$this->set( 'm_strActualStartDate', CStrings::strTrimDef( $strActualStartDate, -1, NULL, true ) );
	}

	public function getActualStartDate() {
		return $this->m_strActualStartDate;
	}

	public function sqlActualStartDate() {
		return ( true == isset( $this->m_strActualStartDate ) ) ? '\'' . $this->m_strActualStartDate . '\'' : 'NULL';
	}

	public function setActualEndDate( $strActualEndDate ) {
		$this->set( 'm_strActualEndDate', CStrings::strTrimDef( $strActualEndDate, -1, NULL, true ) );
	}

	public function getActualEndDate() {
		return $this->m_strActualEndDate;
	}

	public function sqlActualEndDate() {
		return ( true == isset( $this->m_strActualEndDate ) ) ? '\'' . $this->m_strActualEndDate . '\'' : 'NULL';
	}

	public function setContractedBy( $intContractedBy ) {
		$this->set( 'm_intContractedBy', CStrings::strToIntDef( $intContractedBy, NULL, false ) );
	}

	public function getContractedBy() {
		return $this->m_intContractedBy;
	}

	public function sqlContractedBy() {
		return ( true == isset( $this->m_intContractedBy ) ) ? ( string ) $this->m_intContractedBy : 'NULL';
	}

	public function setContractedOn( $strContractedOn ) {
		$this->set( 'm_strContractedOn', CStrings::strTrimDef( $strContractedOn, -1, NULL, true ) );
	}

	public function getContractedOn() {
		return $this->m_strContractedOn;
	}

	public function sqlContractedOn() {
		return ( true == isset( $this->m_strContractedOn ) ) ? '\'' . $this->m_strContractedOn . '\'' : 'NULL';
	}

	public function setStartedBy( $intStartedBy ) {
		$this->set( 'm_intStartedBy', CStrings::strToIntDef( $intStartedBy, NULL, false ) );
	}

	public function getStartedBy() {
		return $this->m_intStartedBy;
	}

	public function sqlStartedBy() {
		return ( true == isset( $this->m_intStartedBy ) ) ? ( string ) $this->m_intStartedBy : 'NULL';
	}

	public function setStartedOn( $strStartedOn ) {
		$this->set( 'm_strStartedOn', CStrings::strTrimDef( $strStartedOn, -1, NULL, true ) );
	}

	public function getStartedOn() {
		return $this->m_strStartedOn;
	}

	public function sqlStartedOn() {
		return ( true == isset( $this->m_strStartedOn ) ) ? '\'' . $this->m_strStartedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setProFormaBudgetAmount( $fltProFormaBudgetAmount ) {
		$this->set( 'm_fltProFormaBudgetAmount', CStrings::strToFloatDef( $fltProFormaBudgetAmount, NULL, false, 2 ) );
	}

	public function getProFormaBudgetAmount() {
		return $this->m_fltProFormaBudgetAmount;
	}

	public function sqlProFormaBudgetAmount() {
		return ( true == isset( $this->m_fltProFormaBudgetAmount ) ) ? ( string ) $this->m_fltProFormaBudgetAmount : '0';
	}

	public function setRenovationDaysToComplete( $intRenovationDaysToComplete ) {
		$this->set( 'm_intRenovationDaysToComplete', CStrings::strToIntDef( $intRenovationDaysToComplete, NULL, false ) );
	}

	public function getRenovationDaysToComplete() {
		return $this->m_intRenovationDaysToComplete;
	}

	public function sqlRenovationDaysToComplete() {
		return ( true == isset( $this->m_intRenovationDaysToComplete ) ) ? ( string ) $this->m_intRenovationDaysToComplete : '0';
	}

	public function setJobStatusId( $intJobStatusId ) {
		$this->set( 'm_intJobStatusId', CStrings::strToIntDef( $intJobStatusId, NULL, false ) );
	}

	public function getJobStatusId() {
		return $this->m_intJobStatusId;
	}

	public function sqlJobStatusId() {
		return ( true == isset( $this->m_intJobStatusId ) ) ? ( string ) $this->m_intJobStatusId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, job_id, re_class_gl_header_id, name, description, fiscal_year, order_num, is_budget_by_period, original_start_date, original_end_date, actual_start_date, actual_end_date, contracted_by, contracted_on, started_by, started_on, completed_by, completed_on, updated_by, updated_on, created_by, created_on, pro_forma_budget_amount, renovation_days_to_complete, job_status_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlJobId() . ', ' .
						$this->sqlReClassGlHeaderId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlFiscalYear() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlIsBudgetByPeriod() . ', ' .
						$this->sqlOriginalStartDate() . ', ' .
						$this->sqlOriginalEndDate() . ', ' .
						$this->sqlActualStartDate() . ', ' .
						$this->sqlActualEndDate() . ', ' .
						$this->sqlContractedBy() . ', ' .
						$this->sqlContractedOn() . ', ' .
						$this->sqlStartedBy() . ', ' .
						$this->sqlStartedOn() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlProFormaBudgetAmount() . ', ' .
						$this->sqlRenovationDaysToComplete() . ', ' .
						$this->sqlJobStatusId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId(). ',' ; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' re_class_gl_header_id = ' . $this->sqlReClassGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'ReClassGlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' re_class_gl_header_id = ' . $this->sqlReClassGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fiscal_year = ' . $this->sqlFiscalYear(). ',' ; } elseif( true == array_key_exists( 'FiscalYear', $this->getChangedColumns() ) ) { $strSql .= ' fiscal_year = ' . $this->sqlFiscalYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_budget_by_period = ' . $this->sqlIsBudgetByPeriod(). ',' ; } elseif( true == array_key_exists( 'IsBudgetByPeriod', $this->getChangedColumns() ) ) { $strSql .= ' is_budget_by_period = ' . $this->sqlIsBudgetByPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate(). ',' ; } elseif( true == array_key_exists( 'OriginalStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_start_date = ' . $this->sqlOriginalStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate(). ',' ; } elseif( true == array_key_exists( 'OriginalEndDate', $this->getChangedColumns() ) ) { $strSql .= ' original_end_date = ' . $this->sqlOriginalEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate(). ',' ; } elseif( true == array_key_exists( 'ActualStartDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_date = ' . $this->sqlActualStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate(). ',' ; } elseif( true == array_key_exists( 'ActualEndDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_end_date = ' . $this->sqlActualEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contracted_by = ' . $this->sqlContractedBy(). ',' ; } elseif( true == array_key_exists( 'ContractedBy', $this->getChangedColumns() ) ) { $strSql .= ' contracted_by = ' . $this->sqlContractedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contracted_on = ' . $this->sqlContractedOn(). ',' ; } elseif( true == array_key_exists( 'ContractedOn', $this->getChangedColumns() ) ) { $strSql .= ' contracted_on = ' . $this->sqlContractedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_by = ' . $this->sqlStartedBy(). ',' ; } elseif( true == array_key_exists( 'StartedBy', $this->getChangedColumns() ) ) { $strSql .= ' started_by = ' . $this->sqlStartedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_on = ' . $this->sqlStartedOn(). ',' ; } elseif( true == array_key_exists( 'StartedOn', $this->getChangedColumns() ) ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pro_forma_budget_amount = ' . $this->sqlProFormaBudgetAmount(). ',' ; } elseif( true == array_key_exists( 'ProFormaBudgetAmount', $this->getChangedColumns() ) ) { $strSql .= ' pro_forma_budget_amount = ' . $this->sqlProFormaBudgetAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renovation_days_to_complete = ' . $this->sqlRenovationDaysToComplete(). ',' ; } elseif( true == array_key_exists( 'RenovationDaysToComplete', $this->getChangedColumns() ) ) { $strSql .= ' renovation_days_to_complete = ' . $this->sqlRenovationDaysToComplete() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_status_id = ' . $this->sqlJobStatusId(). ',' ; } elseif( true == array_key_exists( 'JobStatusId', $this->getChangedColumns() ) ) { $strSql .= ' job_status_id = ' . $this->sqlJobStatusId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'job_id' => $this->getJobId(),
			're_class_gl_header_id' => $this->getReClassGlHeaderId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'fiscal_year' => $this->getFiscalYear(),
			'order_num' => $this->getOrderNum(),
			'is_budget_by_period' => $this->getIsBudgetByPeriod(),
			'original_start_date' => $this->getOriginalStartDate(),
			'original_end_date' => $this->getOriginalEndDate(),
			'actual_start_date' => $this->getActualStartDate(),
			'actual_end_date' => $this->getActualEndDate(),
			'contracted_by' => $this->getContractedBy(),
			'contracted_on' => $this->getContractedOn(),
			'started_by' => $this->getStartedBy(),
			'started_on' => $this->getStartedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'pro_forma_budget_amount' => $this->getProFormaBudgetAmount(),
			'renovation_days_to_complete' => $this->getRenovationDaysToComplete(),
			'job_status_id' => $this->getJobStatusId()
		);
	}

}
?>