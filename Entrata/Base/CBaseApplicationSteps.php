<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSteps
 * Do not add any new functions to this class.
 */

class CBaseApplicationSteps extends CEosPluralBase {

	/**
	 * @return CApplicationStep[]
	 */
	public static function fetchApplicationSteps( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApplicationStep::class, $objDatabase );
	}

	/**
	 * @return CApplicationStep
	 */
	public static function fetchApplicationStep( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApplicationStep::class, $objDatabase );
	}

	public static function fetchApplicationStepCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_steps', $objDatabase );
	}

	public static function fetchApplicationStepById( $intId, $objDatabase ) {
		return self::fetchApplicationStep( sprintf( 'SELECT * FROM application_steps WHERE id = %d', $intId ), $objDatabase );
	}

}
?>