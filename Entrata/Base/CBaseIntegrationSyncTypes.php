<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationSyncTypes
 * Do not add any new functions to this class.
 */

class CBaseIntegrationSyncTypes extends CEosPluralBase {

	/**
	 * @return CIntegrationSyncType[]
	 */
	public static function fetchIntegrationSyncTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIntegrationSyncType::class, $objDatabase );
	}

	/**
	 * @return CIntegrationSyncType
	 */
	public static function fetchIntegrationSyncType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIntegrationSyncType::class, $objDatabase );
	}

	public static function fetchIntegrationSyncTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_sync_types', $objDatabase );
	}

	public static function fetchIntegrationSyncTypeById( $intId, $objDatabase ) {
		return self::fetchIntegrationSyncType( sprintf( 'SELECT * FROM integration_sync_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>