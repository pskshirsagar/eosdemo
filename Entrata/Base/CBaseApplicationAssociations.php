<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationAssociations extends CEosPluralBase {

	/**
	 * @return CApplicationAssociation[]
	 */
	public static function fetchApplicationAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationAssociation
	 */
	public static function fetchApplicationAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationAssociation', $objDatabase );
	}

	public static function fetchApplicationAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_associations', $objDatabase );
	}

	public static function fetchApplicationAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociation( sprintf( 'SELECT * FROM application_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE lease_interval_id = %d AND cid = %d', ( int ) $intLeaseIntervalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByCustomerDataTypeIdByCid( $intCustomerDataTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE customer_data_type_id = %d AND cid = %d', ( int ) $intCustomerDataTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationAssociationsByCustomerDataTypeReferenceIdByCid( $intCustomerDataTypeReferenceId, $intCid, $objDatabase ) {
		return self::fetchApplicationAssociations( sprintf( 'SELECT * FROM application_associations WHERE customer_data_type_reference_id = %d AND cid = %d', ( int ) $intCustomerDataTypeReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>