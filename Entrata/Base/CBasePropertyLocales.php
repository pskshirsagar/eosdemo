<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLocales
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyLocales extends CEosPluralBase {

	/**
	 * @return CPropertyLocale[]
	 */
	public static function fetchPropertyLocales( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyLocale::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyLocale
	 */
	public static function fetchPropertyLocale( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyLocale::class, $objDatabase );
	}

	public static function fetchPropertyLocaleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_locales', $objDatabase );
	}

	public static function fetchPropertyLocaleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyLocale( sprintf( 'SELECT * FROM property_locales WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLocalesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyLocales( sprintf( 'SELECT * FROM property_locales WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLocalesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyLocales( sprintf( 'SELECT * FROM property_locales WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>