<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferItemLog extends CEosSingularBase {

	const TABLE_NAME = 'public.offer_item_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intOfferItemId;
	protected $m_intOfferId;
	protected $m_arrintArLookupTableIds;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intMaxItemCount;
	protected $m_intArOriginId;
	protected $m_intArOriginReferenceId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_intPriorOfferItemLogId;
	protected $m_strLogDatetime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['offer_item_id'] ) && $boolDirectSet ) $this->set( 'm_intOfferItemId', trim( $arrValues['offer_item_id'] ) ); elseif( isset( $arrValues['offer_item_id'] ) ) $this->setOfferItemId( $arrValues['offer_item_id'] );
		if( isset( $arrValues['offer_id'] ) && $boolDirectSet ) $this->set( 'm_intOfferId', trim( $arrValues['offer_id'] ) ); elseif( isset( $arrValues['offer_id'] ) ) $this->setOfferId( $arrValues['offer_id'] );
		if( isset( $arrValues['ar_lookup_table_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintArLookupTableIds', trim( $arrValues['ar_lookup_table_ids'] ) ); elseif( isset( $arrValues['ar_lookup_table_ids'] ) ) $this->setArLookupTableIds( $arrValues['ar_lookup_table_ids'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['max_item_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxItemCount', trim( $arrValues['max_item_count'] ) ); elseif( isset( $arrValues['max_item_count'] ) ) $this->setMaxItemCount( $arrValues['max_item_count'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['prior_offer_item_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorOfferItemLogId', trim( $arrValues['prior_offer_item_log_id'] ) ); elseif( isset( $arrValues['prior_offer_item_log_id'] ) ) $this->setPriorOfferItemLogId( $arrValues['prior_offer_item_log_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setOfferItemId( $intOfferItemId ) {
		$this->set( 'm_intOfferItemId', CStrings::strToIntDef( $intOfferItemId, NULL, false ) );
	}

	public function getOfferItemId() {
		return $this->m_intOfferItemId;
	}

	public function sqlOfferItemId() {
		return ( true == isset( $this->m_intOfferItemId ) ) ? ( string ) $this->m_intOfferItemId : 'NULL';
	}

	public function setOfferId( $intOfferId ) {
		$this->set( 'm_intOfferId', CStrings::strToIntDef( $intOfferId, NULL, false ) );
	}

	public function getOfferId() {
		return $this->m_intOfferId;
	}

	public function sqlOfferId() {
		return ( true == isset( $this->m_intOfferId ) ) ? ( string ) $this->m_intOfferId : 'NULL';
	}

	public function setArLookupTableIds( $arrintArLookupTableIds ) {
		$this->set( 'm_arrintArLookupTableIds', CStrings::strToArrIntDef( $arrintArLookupTableIds, NULL ) );
	}

	public function getArLookupTableIds() {
		return $this->m_arrintArLookupTableIds;
	}

	public function sqlArLookupTableIds() {
		return ( true == isset( $this->m_arrintArLookupTableIds ) && true == valArr( $this->m_arrintArLookupTableIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintArLookupTableIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 60, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setMaxItemCount( $intMaxItemCount ) {
		$this->set( 'm_intMaxItemCount', CStrings::strToIntDef( $intMaxItemCount, NULL, false ) );
	}

	public function getMaxItemCount() {
		return $this->m_intMaxItemCount;
	}

	public function sqlMaxItemCount() {
		return ( true == isset( $this->m_intMaxItemCount ) ) ? ( string ) $this->m_intMaxItemCount : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function sqlArOriginReferenceId() {
		return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setPriorOfferItemLogId( $intPriorOfferItemLogId ) {
		$this->set( 'm_intPriorOfferItemLogId', CStrings::strToIntDef( $intPriorOfferItemLogId, NULL, false ) );
	}

	public function getPriorOfferItemLogId() {
		return $this->m_intPriorOfferItemLogId;
	}

	public function sqlPriorOfferItemLogId() {
		return ( true == isset( $this->m_intPriorOfferItemLogId ) ) ? ( string ) $this->m_intPriorOfferItemLogId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, offer_item_id, offer_id, ar_lookup_table_ids, name, description, max_item_count, ar_origin_id, ar_origin_reference_id, period_id, reporting_period_id, effective_period_id, original_period_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, is_post_month_ignored, is_post_date_ignored, is_opening_log, prior_offer_item_log_id, log_datetime, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlOfferItemId() . ', ' .
 						$this->sqlOfferId() . ', ' .
 						$this->sqlArLookupTableIds() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlMaxItemCount() . ', ' .
 						$this->sqlArOriginId() . ', ' .
 						$this->sqlArOriginReferenceId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlReportingPeriodId() . ', ' .
 						$this->sqlEffectivePeriodId() . ', ' .
 						$this->sqlOriginalPeriodId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlReportingPostMonth() . ', ' .
 						$this->sqlApplyThroughPostMonth() . ', ' .
 						$this->sqlReportingPostDate() . ', ' .
 						$this->sqlApplyThroughPostDate() . ', ' .
 						$this->sqlIsPostMonthIgnored() . ', ' .
 						$this->sqlIsPostDateIgnored() . ', ' .
 						$this->sqlIsOpeningLog() . ', ' .
 						$this->sqlPriorOfferItemLogId() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_item_id = ' . $this->sqlOfferItemId() . ','; } elseif( true == array_key_exists( 'OfferItemId', $this->getChangedColumns() ) ) { $strSql .= ' offer_item_id = ' . $this->sqlOfferItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_id = ' . $this->sqlOfferId() . ','; } elseif( true == array_key_exists( 'OfferId', $this->getChangedColumns() ) ) { $strSql .= ' offer_id = ' . $this->sqlOfferId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_lookup_table_ids = ' . $this->sqlArLookupTableIds() . ','; } elseif( true == array_key_exists( 'ArLookupTableIds', $this->getChangedColumns() ) ) { $strSql .= ' ar_lookup_table_ids = ' . $this->sqlArLookupTableIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; } elseif( true == array_key_exists( 'MaxItemCount', $this->getChangedColumns() ) ) { $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_offer_item_log_id = ' . $this->sqlPriorOfferItemLogId() . ','; } elseif( true == array_key_exists( 'PriorOfferItemLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_offer_item_log_id = ' . $this->sqlPriorOfferItemLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'offer_item_id' => $this->getOfferItemId(),
			'offer_id' => $this->getOfferId(),
			'ar_lookup_table_ids' => $this->getArLookupTableIds(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'max_item_count' => $this->getMaxItemCount(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_origin_reference_id' => $this->getArOriginReferenceId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'prior_offer_item_log_id' => $this->getPriorOfferItemLogId(),
			'log_datetime' => $this->getLogDatetime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>