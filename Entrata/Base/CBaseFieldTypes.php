<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldTypes
 * Do not add any new functions to this class.
 */

class CBaseFieldTypes extends CEosPluralBase {

	/**
	 * @return CFieldType[]
	 */
	public static function fetchFieldTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFieldType::class, $objDatabase );
	}

	/**
	 * @return CFieldType
	 */
	public static function fetchFieldType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFieldType::class, $objDatabase );
	}

	public static function fetchFieldTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'field_types', $objDatabase );
	}

	public static function fetchFieldTypeById( $intId, $objDatabase ) {
		return self::fetchFieldType( sprintf( 'SELECT * FROM field_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>