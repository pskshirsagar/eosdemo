<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueUnitType extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.revenue_unit_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_strEffectiveDate;
	protected $m_strExpiryDate;
	protected $m_fltBaseRent;
	protected $m_intDemandThirtyDay;
	protected $m_intDemandSixtyDay;
	protected $m_intDemandNinetyDay;
	protected $m_fltDailyAvailabilityChangeRate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltMinExecutedRent;
	protected $m_fltMaxExecutedRent;
	protected $m_fltAvgExecutedRent;
	protected $m_fltMinAdvertisedRent;
	protected $m_fltMaxAdvertisedRent;
	protected $m_fltAvgAdvertisedRent;
	protected $m_fltMinCompetitiveRent;
	protected $m_fltMaxCompetitiveRent;
	protected $m_fltAvgCompetitiveRent;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strExpiryDate = '12/31/2099';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['expiry_date'] ) && $boolDirectSet ) $this->set( 'm_strExpiryDate', trim( $arrValues['expiry_date'] ) ); elseif( isset( $arrValues['expiry_date'] ) ) $this->setExpiryDate( $arrValues['expiry_date'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['demand_thirty_day'] ) && $boolDirectSet ) $this->set( 'm_intDemandThirtyDay', trim( $arrValues['demand_thirty_day'] ) ); elseif( isset( $arrValues['demand_thirty_day'] ) ) $this->setDemandThirtyDay( $arrValues['demand_thirty_day'] );
		if( isset( $arrValues['demand_sixty_day'] ) && $boolDirectSet ) $this->set( 'm_intDemandSixtyDay', trim( $arrValues['demand_sixty_day'] ) ); elseif( isset( $arrValues['demand_sixty_day'] ) ) $this->setDemandSixtyDay( $arrValues['demand_sixty_day'] );
		if( isset( $arrValues['demand_ninety_day'] ) && $boolDirectSet ) $this->set( 'm_intDemandNinetyDay', trim( $arrValues['demand_ninety_day'] ) ); elseif( isset( $arrValues['demand_ninety_day'] ) ) $this->setDemandNinetyDay( $arrValues['demand_ninety_day'] );
		if( isset( $arrValues['daily_availability_change_rate'] ) && $boolDirectSet ) $this->set( 'm_fltDailyAvailabilityChangeRate', trim( $arrValues['daily_availability_change_rate'] ) ); elseif( isset( $arrValues['daily_availability_change_rate'] ) ) $this->setDailyAvailabilityChangeRate( $arrValues['daily_availability_change_rate'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['min_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinExecutedRent', trim( $arrValues['min_executed_rent'] ) ); elseif( isset( $arrValues['min_executed_rent'] ) ) $this->setMinExecutedRent( $arrValues['min_executed_rent'] );
		if( isset( $arrValues['max_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxExecutedRent', trim( $arrValues['max_executed_rent'] ) ); elseif( isset( $arrValues['max_executed_rent'] ) ) $this->setMaxExecutedRent( $arrValues['max_executed_rent'] );
		if( isset( $arrValues['avg_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgExecutedRent', trim( $arrValues['avg_executed_rent'] ) ); elseif( isset( $arrValues['avg_executed_rent'] ) ) $this->setAvgExecutedRent( $arrValues['avg_executed_rent'] );
		if( isset( $arrValues['min_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinAdvertisedRent', trim( $arrValues['min_advertised_rent'] ) ); elseif( isset( $arrValues['min_advertised_rent'] ) ) $this->setMinAdvertisedRent( $arrValues['min_advertised_rent'] );
		if( isset( $arrValues['max_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAdvertisedRent', trim( $arrValues['max_advertised_rent'] ) ); elseif( isset( $arrValues['max_advertised_rent'] ) ) $this->setMaxAdvertisedRent( $arrValues['max_advertised_rent'] );
		if( isset( $arrValues['avg_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAdvertisedRent', trim( $arrValues['avg_advertised_rent'] ) ); elseif( isset( $arrValues['avg_advertised_rent'] ) ) $this->setAvgAdvertisedRent( $arrValues['avg_advertised_rent'] );
		if( isset( $arrValues['min_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinCompetitiveRent', trim( $arrValues['min_competitive_rent'] ) ); elseif( isset( $arrValues['min_competitive_rent'] ) ) $this->setMinCompetitiveRent( $arrValues['min_competitive_rent'] );
		if( isset( $arrValues['max_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxCompetitiveRent', trim( $arrValues['max_competitive_rent'] ) ); elseif( isset( $arrValues['max_competitive_rent'] ) ) $this->setMaxCompetitiveRent( $arrValues['max_competitive_rent'] );
		if( isset( $arrValues['avg_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgCompetitiveRent', trim( $arrValues['avg_competitive_rent'] ) ); elseif( isset( $arrValues['avg_competitive_rent'] ) ) $this->setAvgCompetitiveRent( $arrValues['avg_competitive_rent'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setExpiryDate( $strExpiryDate ) {
		$this->set( 'm_strExpiryDate', CStrings::strTrimDef( $strExpiryDate, -1, NULL, true ) );
	}

	public function getExpiryDate() {
		return $this->m_strExpiryDate;
	}

	public function sqlExpiryDate() {
		return ( true == isset( $this->m_strExpiryDate ) ) ? '\'' . $this->m_strExpiryDate . '\'' : 'NOW()';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : 'NULL';
	}

	public function setDemandThirtyDay( $intDemandThirtyDay ) {
		$this->set( 'm_intDemandThirtyDay', CStrings::strToIntDef( $intDemandThirtyDay, NULL, false ) );
	}

	public function getDemandThirtyDay() {
		return $this->m_intDemandThirtyDay;
	}

	public function sqlDemandThirtyDay() {
		return ( true == isset( $this->m_intDemandThirtyDay ) ) ? ( string ) $this->m_intDemandThirtyDay : 'NULL';
	}

	public function setDemandSixtyDay( $intDemandSixtyDay ) {
		$this->set( 'm_intDemandSixtyDay', CStrings::strToIntDef( $intDemandSixtyDay, NULL, false ) );
	}

	public function getDemandSixtyDay() {
		return $this->m_intDemandSixtyDay;
	}

	public function sqlDemandSixtyDay() {
		return ( true == isset( $this->m_intDemandSixtyDay ) ) ? ( string ) $this->m_intDemandSixtyDay : 'NULL';
	}

	public function setDemandNinetyDay( $intDemandNinetyDay ) {
		$this->set( 'm_intDemandNinetyDay', CStrings::strToIntDef( $intDemandNinetyDay, NULL, false ) );
	}

	public function getDemandNinetyDay() {
		return $this->m_intDemandNinetyDay;
	}

	public function sqlDemandNinetyDay() {
		return ( true == isset( $this->m_intDemandNinetyDay ) ) ? ( string ) $this->m_intDemandNinetyDay : 'NULL';
	}

	public function setDailyAvailabilityChangeRate( $fltDailyAvailabilityChangeRate ) {
		$this->set( 'm_fltDailyAvailabilityChangeRate', CStrings::strToFloatDef( $fltDailyAvailabilityChangeRate, NULL, false, 4 ) );
	}

	public function getDailyAvailabilityChangeRate() {
		return $this->m_fltDailyAvailabilityChangeRate;
	}

	public function sqlDailyAvailabilityChangeRate() {
		return ( true == isset( $this->m_fltDailyAvailabilityChangeRate ) ) ? ( string ) $this->m_fltDailyAvailabilityChangeRate : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMinExecutedRent( $fltMinExecutedRent ) {
		$this->set( 'm_fltMinExecutedRent', CStrings::strToFloatDef( $fltMinExecutedRent, NULL, false, 2 ) );
	}

	public function getMinExecutedRent() {
		return $this->m_fltMinExecutedRent;
	}

	public function sqlMinExecutedRent() {
		return ( true == isset( $this->m_fltMinExecutedRent ) ) ? ( string ) $this->m_fltMinExecutedRent : 'NULL';
	}

	public function setMaxExecutedRent( $fltMaxExecutedRent ) {
		$this->set( 'm_fltMaxExecutedRent', CStrings::strToFloatDef( $fltMaxExecutedRent, NULL, false, 2 ) );
	}

	public function getMaxExecutedRent() {
		return $this->m_fltMaxExecutedRent;
	}

	public function sqlMaxExecutedRent() {
		return ( true == isset( $this->m_fltMaxExecutedRent ) ) ? ( string ) $this->m_fltMaxExecutedRent : 'NULL';
	}

	public function setAvgExecutedRent( $fltAvgExecutedRent ) {
		$this->set( 'm_fltAvgExecutedRent', CStrings::strToFloatDef( $fltAvgExecutedRent, NULL, false, 2 ) );
	}

	public function getAvgExecutedRent() {
		return $this->m_fltAvgExecutedRent;
	}

	public function sqlAvgExecutedRent() {
		return ( true == isset( $this->m_fltAvgExecutedRent ) ) ? ( string ) $this->m_fltAvgExecutedRent : 'NULL';
	}

	public function setMinAdvertisedRent( $fltMinAdvertisedRent ) {
		$this->set( 'm_fltMinAdvertisedRent', CStrings::strToFloatDef( $fltMinAdvertisedRent, NULL, false, 2 ) );
	}

	public function getMinAdvertisedRent() {
		return $this->m_fltMinAdvertisedRent;
	}

	public function sqlMinAdvertisedRent() {
		return ( true == isset( $this->m_fltMinAdvertisedRent ) ) ? ( string ) $this->m_fltMinAdvertisedRent : 'NULL';
	}

	public function setMaxAdvertisedRent( $fltMaxAdvertisedRent ) {
		$this->set( 'm_fltMaxAdvertisedRent', CStrings::strToFloatDef( $fltMaxAdvertisedRent, NULL, false, 2 ) );
	}

	public function getMaxAdvertisedRent() {
		return $this->m_fltMaxAdvertisedRent;
	}

	public function sqlMaxAdvertisedRent() {
		return ( true == isset( $this->m_fltMaxAdvertisedRent ) ) ? ( string ) $this->m_fltMaxAdvertisedRent : 'NULL';
	}

	public function setAvgAdvertisedRent( $fltAvgAdvertisedRent ) {
		$this->set( 'm_fltAvgAdvertisedRent', CStrings::strToFloatDef( $fltAvgAdvertisedRent, NULL, false, 2 ) );
	}

	public function getAvgAdvertisedRent() {
		return $this->m_fltAvgAdvertisedRent;
	}

	public function sqlAvgAdvertisedRent() {
		return ( true == isset( $this->m_fltAvgAdvertisedRent ) ) ? ( string ) $this->m_fltAvgAdvertisedRent : 'NULL';
	}

	public function setMinCompetitiveRent( $fltMinCompetitiveRent ) {
		$this->set( 'm_fltMinCompetitiveRent', CStrings::strToFloatDef( $fltMinCompetitiveRent, NULL, false, 2 ) );
	}

	public function getMinCompetitiveRent() {
		return $this->m_fltMinCompetitiveRent;
	}

	public function sqlMinCompetitiveRent() {
		return ( true == isset( $this->m_fltMinCompetitiveRent ) ) ? ( string ) $this->m_fltMinCompetitiveRent : 'NULL';
	}

	public function setMaxCompetitiveRent( $fltMaxCompetitiveRent ) {
		$this->set( 'm_fltMaxCompetitiveRent', CStrings::strToFloatDef( $fltMaxCompetitiveRent, NULL, false, 2 ) );
	}

	public function getMaxCompetitiveRent() {
		return $this->m_fltMaxCompetitiveRent;
	}

	public function sqlMaxCompetitiveRent() {
		return ( true == isset( $this->m_fltMaxCompetitiveRent ) ) ? ( string ) $this->m_fltMaxCompetitiveRent : 'NULL';
	}

	public function setAvgCompetitiveRent( $fltAvgCompetitiveRent ) {
		$this->set( 'm_fltAvgCompetitiveRent', CStrings::strToFloatDef( $fltAvgCompetitiveRent, NULL, false, 2 ) );
	}

	public function getAvgCompetitiveRent() {
		return $this->m_fltAvgCompetitiveRent;
	}

	public function sqlAvgCompetitiveRent() {
		return ( true == isset( $this->m_fltAvgCompetitiveRent ) ) ? ( string ) $this->m_fltAvgCompetitiveRent : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_id, effective_date, expiry_date, base_rent, demand_thirty_day, demand_sixty_day, demand_ninety_day, daily_availability_change_rate, updated_by, updated_on, created_by, created_on, min_executed_rent, max_executed_rent, avg_executed_rent, min_advertised_rent, max_advertised_rent, avg_advertised_rent, min_competitive_rent, max_competitive_rent, avg_competitive_rent, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlExpiryDate() . ', ' .
						$this->sqlBaseRent() . ', ' .
						$this->sqlDemandThirtyDay() . ', ' .
						$this->sqlDemandSixtyDay() . ', ' .
						$this->sqlDemandNinetyDay() . ', ' .
						$this->sqlDailyAvailabilityChangeRate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMinExecutedRent() . ', ' .
						$this->sqlMaxExecutedRent() . ', ' .
						$this->sqlAvgExecutedRent() . ', ' .
						$this->sqlMinAdvertisedRent() . ', ' .
						$this->sqlMaxAdvertisedRent() . ', ' .
						$this->sqlAvgAdvertisedRent() . ', ' .
						$this->sqlMinCompetitiveRent() . ', ' .
						$this->sqlMaxCompetitiveRent() . ', ' .
						$this->sqlAvgCompetitiveRent() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate(). ',' ; } elseif( true == array_key_exists( 'ExpiryDate', $this->getChangedColumns() ) ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_thirty_day = ' . $this->sqlDemandThirtyDay(). ',' ; } elseif( true == array_key_exists( 'DemandThirtyDay', $this->getChangedColumns() ) ) { $strSql .= ' demand_thirty_day = ' . $this->sqlDemandThirtyDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_sixty_day = ' . $this->sqlDemandSixtyDay(). ',' ; } elseif( true == array_key_exists( 'DemandSixtyDay', $this->getChangedColumns() ) ) { $strSql .= ' demand_sixty_day = ' . $this->sqlDemandSixtyDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_ninety_day = ' . $this->sqlDemandNinetyDay(). ',' ; } elseif( true == array_key_exists( 'DemandNinetyDay', $this->getChangedColumns() ) ) { $strSql .= ' demand_ninety_day = ' . $this->sqlDemandNinetyDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_availability_change_rate = ' . $this->sqlDailyAvailabilityChangeRate(). ',' ; } elseif( true == array_key_exists( 'DailyAvailabilityChangeRate', $this->getChangedColumns() ) ) { $strSql .= ' daily_availability_change_rate = ' . $this->sqlDailyAvailabilityChangeRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_executed_rent = ' . $this->sqlMinExecutedRent(). ',' ; } elseif( true == array_key_exists( 'MinExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' min_executed_rent = ' . $this->sqlMinExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_executed_rent = ' . $this->sqlMaxExecutedRent(). ',' ; } elseif( true == array_key_exists( 'MaxExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' max_executed_rent = ' . $this->sqlMaxExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_executed_rent = ' . $this->sqlAvgExecutedRent(). ',' ; } elseif( true == array_key_exists( 'AvgExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_executed_rent = ' . $this->sqlAvgExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_advertised_rent = ' . $this->sqlMinAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'MinAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' min_advertised_rent = ' . $this->sqlMinAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_advertised_rent = ' . $this->sqlMaxAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'MaxAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' max_advertised_rent = ' . $this->sqlMaxAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_advertised_rent = ' . $this->sqlAvgAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'AvgAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_advertised_rent = ' . $this->sqlAvgAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_competitive_rent = ' . $this->sqlMinCompetitiveRent(). ',' ; } elseif( true == array_key_exists( 'MinCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' min_competitive_rent = ' . $this->sqlMinCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_competitive_rent = ' . $this->sqlMaxCompetitiveRent(). ',' ; } elseif( true == array_key_exists( 'MaxCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' max_competitive_rent = ' . $this->sqlMaxCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_competitive_rent = ' . $this->sqlAvgCompetitiveRent(). ',' ; } elseif( true == array_key_exists( 'AvgCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_competitive_rent = ' . $this->sqlAvgCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'effective_date' => $this->getEffectiveDate(),
			'expiry_date' => $this->getExpiryDate(),
			'base_rent' => $this->getBaseRent(),
			'demand_thirty_day' => $this->getDemandThirtyDay(),
			'demand_sixty_day' => $this->getDemandSixtyDay(),
			'demand_ninety_day' => $this->getDemandNinetyDay(),
			'daily_availability_change_rate' => $this->getDailyAvailabilityChangeRate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'min_executed_rent' => $this->getMinExecutedRent(),
			'max_executed_rent' => $this->getMaxExecutedRent(),
			'avg_executed_rent' => $this->getAvgExecutedRent(),
			'min_advertised_rent' => $this->getMinAdvertisedRent(),
			'max_advertised_rent' => $this->getMaxAdvertisedRent(),
			'avg_advertised_rent' => $this->getAvgAdvertisedRent(),
			'min_competitive_rent' => $this->getMinCompetitiveRent(),
			'max_competitive_rent' => $this->getMaxCompetitiveRent(),
			'avg_competitive_rent' => $this->getAvgCompetitiveRent(),
			'details' => $this->getDetails()
		);
	}

}
?>