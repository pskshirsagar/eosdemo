<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExportBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseApExportBatchTypes extends CEosPluralBase {

	/**
	 * @return CApExportBatchType[]
	 */
	public static function fetchApExportBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApExportBatchType::class, $objDatabase );
	}

	/**
	 * @return CApExportBatchType
	 */
	public static function fetchApExportBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApExportBatchType::class, $objDatabase );
	}

	public static function fetchApExportBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_export_batch_types', $objDatabase );
	}

	public static function fetchApExportBatchTypeById( $intId, $objDatabase ) {
		return self::fetchApExportBatchType( sprintf( 'SELECT * FROM ap_export_batch_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>