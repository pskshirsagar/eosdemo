<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCorporateClients
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCorporateClients extends CEosPluralBase {

	/**
	 * @return CPropertyCorporateClient[]
	 */
	public static function fetchPropertyCorporateClients( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCorporateClient', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCorporateClient
	 */
	public static function fetchPropertyCorporateClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCorporateClient', $objDatabase );
	}

	public static function fetchPropertyCorporateClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_corporate_clients', $objDatabase );
	}

	public static function fetchPropertyCorporateClientByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCorporateClient( sprintf( 'SELECT * FROM property_corporate_clients WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCorporateClientsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCorporateClients( sprintf( 'SELECT * FROM property_corporate_clients WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCorporateClientsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCorporateClients( sprintf( 'SELECT * FROM property_corporate_clients WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCorporateClientsByCorporateClientIdByCid( $intCorporateClientId, $intCid, $objDatabase ) {
		return self::fetchPropertyCorporateClients( sprintf( 'SELECT * FROM property_corporate_clients WHERE corporate_client_id = %d AND cid = %d', ( int ) $intCorporateClientId, ( int ) $intCid ), $objDatabase );
	}

}
?>