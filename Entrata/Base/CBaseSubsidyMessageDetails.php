<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessageDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyMessageDetails extends CEosPluralBase {

	/**
	 * @return CSubsidyMessageDetail[]
	 */
	public static function fetchSubsidyMessageDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyMessageDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyMessageDetail
	 */
	public static function fetchSubsidyMessageDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyMessageDetail', $objDatabase );
	}

	public static function fetchSubsidyMessageDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_message_details', $objDatabase );
	}

	public static function fetchSubsidyMessageDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetail( sprintf( 'SELECT * FROM subsidy_message_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsBySubsidyMessageSubTypeIdByCid( $intSubsidyMessageSubTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE subsidy_message_sub_type_id = %d AND cid = %d', ( int ) $intSubsidyMessageSubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsBySubmissionSubsidyMessageIdByCid( $intSubmissionSubsidyMessageId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE submission_subsidy_message_id = %d AND cid = %d', ( int ) $intSubmissionSubsidyMessageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMessageDetailsByResultSubsidyMessageIdByCid( $intResultSubsidyMessageId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMessageDetails( sprintf( 'SELECT * FROM subsidy_message_details WHERE result_subsidy_message_id = %d AND cid = %d', ( int ) $intResultSubsidyMessageId, ( int ) $intCid ), $objDatabase );
	}

}
?>