<?php

class CBaseDefaultArTriggerRule extends CEosSingularBase {

	const TABLE_NAME = 'public.default_ar_trigger_rules';

	protected $m_intId;
	protected $m_intArTriggerId;
	protected $m_boolCacheToLease;

	public function __construct() {
		parent::__construct();

		$this->m_boolCacheToLease = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['cache_to_lease'] ) && $boolDirectSet ) $this->set( 'm_boolCacheToLease', trim( stripcslashes( $arrValues['cache_to_lease'] ) ) ); elseif( isset( $arrValues['cache_to_lease'] ) ) $this->setCacheToLease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cache_to_lease'] ) : $arrValues['cache_to_lease'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setCacheToLease( $boolCacheToLease ) {
		$this->set( 'm_boolCacheToLease', CStrings::strToBool( $boolCacheToLease ) );
	}

	public function getCacheToLease() {
		return $this->m_boolCacheToLease;
	}

	public function sqlCacheToLease() {
		return ( true == isset( $this->m_boolCacheToLease ) ) ? '\'' . ( true == ( bool ) $this->m_boolCacheToLease ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'cache_to_lease' => $this->getCacheToLease()
		);
	}

}
?>