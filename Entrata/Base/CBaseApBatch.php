<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strBatchDatetime;
	protected $m_strPostMonth;
	protected $m_intControlCount;
	protected $m_fltControlTotal;
	protected $m_strMemo;
	protected $m_intIsPaused;
	protected $m_intIsAutoPosted;
	protected $m_intIsManagerial;
	protected $m_intPostedBy;
	protected $m_strPostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPaused = '0';
		$this->m_intIsAutoPosted = '0';
		$this->m_intIsManagerial = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['control_count'] ) && $boolDirectSet ) $this->set( 'm_intControlCount', trim( $arrValues['control_count'] ) ); elseif( isset( $arrValues['control_count'] ) ) $this->setControlCount( $arrValues['control_count'] );
		if( isset( $arrValues['control_total'] ) && $boolDirectSet ) $this->set( 'm_fltControlTotal', trim( $arrValues['control_total'] ) ); elseif( isset( $arrValues['control_total'] ) ) $this->setControlTotal( $arrValues['control_total'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['is_paused'] ) && $boolDirectSet ) $this->set( 'm_intIsPaused', trim( $arrValues['is_paused'] ) ); elseif( isset( $arrValues['is_paused'] ) ) $this->setIsPaused( $arrValues['is_paused'] );
		if( isset( $arrValues['is_auto_posted'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoPosted', trim( $arrValues['is_auto_posted'] ) ); elseif( isset( $arrValues['is_auto_posted'] ) ) $this->setIsAutoPosted( $arrValues['is_auto_posted'] );
		if( isset( $arrValues['is_managerial'] ) && $boolDirectSet ) $this->set( 'm_intIsManagerial', trim( $arrValues['is_managerial'] ) ); elseif( isset( $arrValues['is_managerial'] ) ) $this->setIsManagerial( $arrValues['is_managerial'] );
		if( isset( $arrValues['posted_by'] ) && $boolDirectSet ) $this->set( 'm_intPostedBy', trim( $arrValues['posted_by'] ) ); elseif( isset( $arrValues['posted_by'] ) ) $this->setPostedBy( $arrValues['posted_by'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setControlCount( $intControlCount ) {
		$this->set( 'm_intControlCount', CStrings::strToIntDef( $intControlCount, NULL, false ) );
	}

	public function getControlCount() {
		return $this->m_intControlCount;
	}

	public function sqlControlCount() {
		return ( true == isset( $this->m_intControlCount ) ) ? ( string ) $this->m_intControlCount : 'NULL';
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->set( 'm_fltControlTotal', CStrings::strToFloatDef( $fltControlTotal, NULL, false, 2 ) );
	}

	public function getControlTotal() {
		return $this->m_fltControlTotal;
	}

	public function sqlControlTotal() {
		return ( true == isset( $this->m_fltControlTotal ) ) ? ( string ) $this->m_fltControlTotal : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 240, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setIsPaused( $intIsPaused ) {
		$this->set( 'm_intIsPaused', CStrings::strToIntDef( $intIsPaused, NULL, false ) );
	}

	public function getIsPaused() {
		return $this->m_intIsPaused;
	}

	public function sqlIsPaused() {
		return ( true == isset( $this->m_intIsPaused ) ) ? ( string ) $this->m_intIsPaused : '0';
	}

	public function setIsAutoPosted( $intIsAutoPosted ) {
		$this->set( 'm_intIsAutoPosted', CStrings::strToIntDef( $intIsAutoPosted, NULL, false ) );
	}

	public function getIsAutoPosted() {
		return $this->m_intIsAutoPosted;
	}

	public function sqlIsAutoPosted() {
		return ( true == isset( $this->m_intIsAutoPosted ) ) ? ( string ) $this->m_intIsAutoPosted : '0';
	}

	public function setIsManagerial( $intIsManagerial ) {
		$this->set( 'm_intIsManagerial', CStrings::strToIntDef( $intIsManagerial, NULL, false ) );
	}

	public function getIsManagerial() {
		return $this->m_intIsManagerial;
	}

	public function sqlIsManagerial() {
		return ( true == isset( $this->m_intIsManagerial ) ) ? ( string ) $this->m_intIsManagerial : '0';
	}

	public function setPostedBy( $intPostedBy ) {
		$this->set( 'm_intPostedBy', CStrings::strToIntDef( $intPostedBy, NULL, false ) );
	}

	public function getPostedBy() {
		return $this->m_intPostedBy;
	}

	public function sqlPostedBy() {
		return ( true == isset( $this->m_intPostedBy ) ) ? ( string ) $this->m_intPostedBy : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, batch_datetime, post_month, control_count, control_total, memo, is_paused, is_auto_posted, is_managerial, posted_by, posted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlControlCount() . ', ' .
 						$this->sqlControlTotal() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlIsPaused() . ', ' .
 						$this->sqlIsAutoPosted() . ', ' .
 						$this->sqlIsManagerial() . ', ' .
 						$this->sqlPostedBy() . ', ' .
 						$this->sqlPostedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_count = ' . $this->sqlControlCount() . ','; } elseif( true == array_key_exists( 'ControlCount', $this->getChangedColumns() ) ) { $strSql .= ' control_count = ' . $this->sqlControlCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; } elseif( true == array_key_exists( 'ControlTotal', $this->getChangedColumns() ) ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused() . ','; } elseif( true == array_key_exists( 'IsPaused', $this->getChangedColumns() ) ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_posted = ' . $this->sqlIsAutoPosted() . ','; } elseif( true == array_key_exists( 'IsAutoPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_posted = ' . $this->sqlIsAutoPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_managerial = ' . $this->sqlIsManagerial() . ','; } elseif( true == array_key_exists( 'IsManagerial', $this->getChangedColumns() ) ) { $strSql .= ' is_managerial = ' . $this->sqlIsManagerial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; } elseif( true == array_key_exists( 'PostedBy', $this->getChangedColumns() ) ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'post_month' => $this->getPostMonth(),
			'control_count' => $this->getControlCount(),
			'control_total' => $this->getControlTotal(),
			'memo' => $this->getMemo(),
			'is_paused' => $this->getIsPaused(),
			'is_auto_posted' => $this->getIsAutoPosted(),
			'is_managerial' => $this->getIsManagerial(),
			'posted_by' => $this->getPostedBy(),
			'posted_on' => $this->getPostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>