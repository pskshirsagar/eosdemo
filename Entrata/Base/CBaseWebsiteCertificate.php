<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteCertificate extends CEosSingularBase {

	const TABLE_NAME = 'public.website_certificates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intDomainId;
	protected $m_intCertificateBundleId;
	protected $m_strPublicKey;
	protected $m_strPrivateKey;
	protected $m_strCertificateExpiration;
	protected $m_intCertificateType;
	protected $m_strAuthUrl;
	protected $m_strAuthResponse;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCertificateType = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['domain_id'] ) && $boolDirectSet ) $this->set( 'm_intDomainId', trim( $arrValues['domain_id'] ) ); elseif( isset( $arrValues['domain_id'] ) ) $this->setDomainId( $arrValues['domain_id'] );
		if( isset( $arrValues['certificate_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificateBundleId', trim( $arrValues['certificate_bundle_id'] ) ); elseif( isset( $arrValues['certificate_bundle_id'] ) ) $this->setCertificateBundleId( $arrValues['certificate_bundle_id'] );
		if( isset( $arrValues['public_key'] ) && $boolDirectSet ) $this->set( 'm_strPublicKey', trim( stripcslashes( $arrValues['public_key'] ) ) ); elseif( isset( $arrValues['public_key'] ) ) $this->setPublicKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['public_key'] ) : $arrValues['public_key'] );
		if( isset( $arrValues['private_key'] ) && $boolDirectSet ) $this->set( 'm_strPrivateKey', trim( stripcslashes( $arrValues['private_key'] ) ) ); elseif( isset( $arrValues['private_key'] ) ) $this->setPrivateKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['private_key'] ) : $arrValues['private_key'] );
		if( isset( $arrValues['certificate_expiration'] ) && $boolDirectSet ) $this->set( 'm_strCertificateExpiration', trim( $arrValues['certificate_expiration'] ) ); elseif( isset( $arrValues['certificate_expiration'] ) ) $this->setCertificateExpiration( $arrValues['certificate_expiration'] );
		if( isset( $arrValues['certificate_type'] ) && $boolDirectSet ) $this->set( 'm_intCertificateType', trim( $arrValues['certificate_type'] ) ); elseif( isset( $arrValues['certificate_type'] ) ) $this->setCertificateType( $arrValues['certificate_type'] );
		if( isset( $arrValues['auth_url'] ) && $boolDirectSet ) $this->set( 'm_strAuthUrl', trim( stripcslashes( $arrValues['auth_url'] ) ) ); elseif( isset( $arrValues['auth_url'] ) ) $this->setAuthUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auth_url'] ) : $arrValues['auth_url'] );
		if( isset( $arrValues['auth_response'] ) && $boolDirectSet ) $this->set( 'm_strAuthResponse', trim( stripcslashes( $arrValues['auth_response'] ) ) ); elseif( isset( $arrValues['auth_response'] ) ) $this->setAuthResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auth_response'] ) : $arrValues['auth_response'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setDomainId( $intDomainId ) {
		$this->set( 'm_intDomainId', CStrings::strToIntDef( $intDomainId, NULL, false ) );
	}

	public function getDomainId() {
		return $this->m_intDomainId;
	}

	public function sqlDomainId() {
		return ( true == isset( $this->m_intDomainId ) ) ? ( string ) $this->m_intDomainId : 'NULL';
	}

	public function setCertificateBundleId( $intCertificateBundleId ) {
		$this->set( 'm_intCertificateBundleId', CStrings::strToIntDef( $intCertificateBundleId, NULL, false ) );
	}

	public function getCertificateBundleId() {
		return $this->m_intCertificateBundleId;
	}

	public function sqlCertificateBundleId() {
		return ( true == isset( $this->m_intCertificateBundleId ) ) ? ( string ) $this->m_intCertificateBundleId : 'NULL';
	}

	public function setPublicKey( $strPublicKey ) {
		$this->set( 'm_strPublicKey', CStrings::strTrimDef( $strPublicKey, -1, NULL, true ) );
	}

	public function getPublicKey() {
		return $this->m_strPublicKey;
	}

	public function sqlPublicKey() {
		return ( true == isset( $this->m_strPublicKey ) ) ? '\'' . addslashes( $this->m_strPublicKey ) . '\'' : 'NULL';
	}

	public function setPrivateKey( $strPrivateKey ) {
		$this->set( 'm_strPrivateKey', CStrings::strTrimDef( $strPrivateKey, -1, NULL, true ) );
	}

	public function getPrivateKey() {
		return $this->m_strPrivateKey;
	}

	public function sqlPrivateKey() {
		return ( true == isset( $this->m_strPrivateKey ) ) ? '\'' . addslashes( $this->m_strPrivateKey ) . '\'' : 'NULL';
	}

	public function setCertificateExpiration( $strCertificateExpiration ) {
		$this->set( 'm_strCertificateExpiration', CStrings::strTrimDef( $strCertificateExpiration, -1, NULL, true ) );
	}

	public function getCertificateExpiration() {
		return $this->m_strCertificateExpiration;
	}

	public function sqlCertificateExpiration() {
		return ( true == isset( $this->m_strCertificateExpiration ) ) ? '\'' . $this->m_strCertificateExpiration . '\'' : 'NULL';
	}

	public function setCertificateType( $intCertificateType ) {
		$this->set( 'm_intCertificateType', CStrings::strToIntDef( $intCertificateType, NULL, false ) );
	}

	public function getCertificateType() {
		return $this->m_intCertificateType;
	}

	public function sqlCertificateType() {
		return ( true == isset( $this->m_intCertificateType ) ) ? ( string ) $this->m_intCertificateType : '1';
	}

	public function setAuthUrl( $strAuthUrl ) {
		$this->set( 'm_strAuthUrl', CStrings::strTrimDef( $strAuthUrl, 255, NULL, true ) );
	}

	public function getAuthUrl() {
		return $this->m_strAuthUrl;
	}

	public function sqlAuthUrl() {
		return ( true == isset( $this->m_strAuthUrl ) ) ? '\'' . addslashes( $this->m_strAuthUrl ) . '\'' : 'NULL';
	}

	public function setAuthResponse( $strAuthResponse ) {
		$this->set( 'm_strAuthResponse', CStrings::strTrimDef( $strAuthResponse, 255, NULL, true ) );
	}

	public function getAuthResponse() {
		return $this->m_strAuthResponse;
	}

	public function sqlAuthResponse() {
		return ( true == isset( $this->m_strAuthResponse ) ) ? '\'' . addslashes( $this->m_strAuthResponse ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, domain_id, certificate_bundle_id, public_key, private_key, certificate_expiration, certificate_type, auth_url, auth_response, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlDomainId() . ', ' .
 						$this->sqlCertificateBundleId() . ', ' .
 						$this->sqlPublicKey() . ', ' .
 						$this->sqlPrivateKey() . ', ' .
 						$this->sqlCertificateExpiration() . ', ' .
 						$this->sqlCertificateType() . ', ' .
 						$this->sqlAuthUrl() . ', ' .
 						$this->sqlAuthResponse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain_id = ' . $this->sqlDomainId() . ','; } elseif( true == array_key_exists( 'DomainId', $this->getChangedColumns() ) ) { $strSql .= ' domain_id = ' . $this->sqlDomainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certificate_bundle_id = ' . $this->sqlCertificateBundleId() . ','; } elseif( true == array_key_exists( 'CertificateBundleId', $this->getChangedColumns() ) ) { $strSql .= ' certificate_bundle_id = ' . $this->sqlCertificateBundleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' public_key = ' . $this->sqlPublicKey() . ','; } elseif( true == array_key_exists( 'PublicKey', $this->getChangedColumns() ) ) { $strSql .= ' public_key = ' . $this->sqlPublicKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey() . ','; } elseif( true == array_key_exists( 'PrivateKey', $this->getChangedColumns() ) ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certificate_expiration = ' . $this->sqlCertificateExpiration() . ','; } elseif( true == array_key_exists( 'CertificateExpiration', $this->getChangedColumns() ) ) { $strSql .= ' certificate_expiration = ' . $this->sqlCertificateExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certificate_type = ' . $this->sqlCertificateType() . ','; } elseif( true == array_key_exists( 'CertificateType', $this->getChangedColumns() ) ) { $strSql .= ' certificate_type = ' . $this->sqlCertificateType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_url = ' . $this->sqlAuthUrl() . ','; } elseif( true == array_key_exists( 'AuthUrl', $this->getChangedColumns() ) ) { $strSql .= ' auth_url = ' . $this->sqlAuthUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_response = ' . $this->sqlAuthResponse() . ','; } elseif( true == array_key_exists( 'AuthResponse', $this->getChangedColumns() ) ) { $strSql .= ' auth_response = ' . $this->sqlAuthResponse() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'domain_id' => $this->getDomainId(),
			'certificate_bundle_id' => $this->getCertificateBundleId(),
			'public_key' => $this->getPublicKey(),
			'private_key' => $this->getPrivateKey(),
			'certificate_expiration' => $this->getCertificateExpiration(),
			'certificate_type' => $this->getCertificateType(),
			'auth_url' => $this->getAuthUrl(),
			'auth_response' => $this->getAuthResponse(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>