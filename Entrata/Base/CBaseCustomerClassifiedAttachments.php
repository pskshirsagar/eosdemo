<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClassifiedAttachments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerClassifiedAttachments extends CEosPluralBase {

	/**
	 * @return CCustomerClassifiedAttachment[]
	 */
	public static function fetchCustomerClassifiedAttachments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerClassifiedAttachment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerClassifiedAttachment
	 */
	public static function fetchCustomerClassifiedAttachment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerClassifiedAttachment', $objDatabase );
	}

	public static function fetchCustomerClassifiedAttachmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_classified_attachments', $objDatabase );
	}

	public static function fetchCustomerClassifiedAttachmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedAttachment( sprintf( 'SELECT * FROM customer_classified_attachments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedAttachmentsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedAttachments( sprintf( 'SELECT * FROM customer_classified_attachments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedAttachmentsByCustomerClassifiedIdByCid( $intCustomerClassifiedId, $intCid, $objDatabase ) {
		return self::fetchCustomerClassifiedAttachments( sprintf( 'SELECT * FROM customer_classified_attachments WHERE customer_classified_id = %d AND cid = %d', ( int ) $intCustomerClassifiedId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerClassifiedAttachmentsByCustomerClassifiedIdsByCid( $arrintCustomerClassifiedIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						customer_classified_attachments
					WHERE
						customer_classified_id IN( ' . implode( ',', $arrintCustomerClassifiedIds ) . ' )
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerClassifiedAttachments( $strSql, $objDatabase );
	}

}
?>