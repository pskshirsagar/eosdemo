<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceExclusions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceExclusions extends CEosPluralBase {

	/**
	 * @return CUnitSpaceExclusion[]
	 */
	public static function fetchUnitSpaceExclusions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitSpaceExclusion::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceExclusion
	 */
	public static function fetchUnitSpaceExclusion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceExclusion::class, $objDatabase );
	}

	public static function fetchUnitSpaceExclusionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_exclusions', $objDatabase );
	}

	public static function fetchUnitSpaceExclusionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceExclusion( sprintf( 'SELECT * FROM unit_space_exclusions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceExclusionsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceExclusions( sprintf( 'SELECT * FROM unit_space_exclusions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceExclusionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceExclusions( sprintf( 'SELECT * FROM unit_space_exclusions WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceExclusionsByUnitExclusionReasonTypeIdByCid( $intUnitExclusionReasonTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceExclusions( sprintf( 'SELECT * FROM unit_space_exclusions WHERE unit_exclusion_reason_type_id = %d AND cid = %d', $intUnitExclusionReasonTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceExclusionsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceExclusions( sprintf( 'SELECT * FROM unit_space_exclusions WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

}
?>