<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultApPayees
 * Do not add any new functions to this class.
 */

class CBaseDefaultApPayees extends CEosPluralBase {

	/**
	 * @return CDefaultApPayee[]
	 */
	public static function fetchDefaultApPayees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultApPayee::class, $objDatabase );
	}

	/**
	 * @return CDefaultApPayee
	 */
	public static function fetchDefaultApPayee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultApPayee::class, $objDatabase );
	}

	public static function fetchDefaultApPayeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_ap_payees', $objDatabase );
	}

	public static function fetchDefaultApPayeeById( $intId, $objDatabase ) {
		return self::fetchDefaultApPayee( sprintf( 'SELECT * FROM default_ap_payees WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultApPayeesByApPayeeTypeId( $intApPayeeTypeId, $objDatabase ) {
		return self::fetchDefaultApPayees( sprintf( 'SELECT * FROM default_ap_payees WHERE ap_payee_type_id = %d', $intApPayeeTypeId ), $objDatabase );
	}

	public static function fetchDefaultApPayeesByDefaultApPayeeTermId( $intDefaultApPayeeTermId, $objDatabase ) {
		return self::fetchDefaultApPayees( sprintf( 'SELECT * FROM default_ap_payees WHERE default_ap_payee_term_id = %d', $intDefaultApPayeeTermId ), $objDatabase );
	}

}
?>