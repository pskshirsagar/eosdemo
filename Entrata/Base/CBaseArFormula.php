<?php

class CBaseArFormula extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ar_formulas';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsProspect;
	protected $m_boolIsRenewal;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintOccupancyTypeIds;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsProspect = false;
		$this->m_boolIsRenewal = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_prospect'] ) && $boolDirectSet ) $this->set( 'm_boolIsProspect', trim( stripcslashes( $arrValues['is_prospect'] ) ) ); elseif( isset( $arrValues['is_prospect'] ) ) $this->setIsProspect( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prospect'] ) : $arrValues['is_prospect'] );
		if( isset( $arrValues['is_renewal'] ) && $boolDirectSet ) $this->set( 'm_boolIsRenewal', trim( stripcslashes( $arrValues['is_renewal'] ) ) ); elseif( isset( $arrValues['is_renewal'] ) ) $this->setIsRenewal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_renewal'] ) : $arrValues['is_renewal'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsProspect( $boolIsProspect ) {
		$this->set( 'm_boolIsProspect', CStrings::strToBool( $boolIsProspect ) );
	}

	public function getIsProspect() {
		return $this->m_boolIsProspect;
	}

	public function sqlIsProspect() {
		return ( true == isset( $this->m_boolIsProspect ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProspect ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRenewal( $boolIsRenewal ) {
		$this->set( 'm_boolIsRenewal', CStrings::strToBool( $boolIsRenewal ) );
	}

	public function getIsRenewal() {
		return $this->m_boolIsRenewal;
	}

	public function sqlIsRenewal() {
		return ( true == isset( $this->m_boolIsRenewal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRenewal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_prospect' => $this->getIsProspect(),
			'is_renewal' => $this->getIsRenewal(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds()
		);
	}

}
?>