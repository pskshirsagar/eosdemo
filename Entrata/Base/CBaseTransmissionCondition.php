<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransmissionCondition extends CEosSingularBase {

	const TABLE_NAME = 'public.transmission_conditions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransmissionConditionTypeId;
	protected $m_intApplicationId;
	protected $m_intTransmissionId;
	protected $m_intGuarantorApplicantApplicationTransmissionId;
	protected $m_intGuarantorApplicantApplicationId;
	protected $m_intDepositArTransactionId;
	protected $m_intRentApplicationRateId;
	protected $m_intRentScheduledChargeId;
	protected $m_strConditionDatetime;
	protected $m_intSatisfiedBy;
	protected $m_strSatisfiedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transmission_condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionConditionTypeId', trim( $arrValues['transmission_condition_type_id'] ) ); elseif( isset( $arrValues['transmission_condition_type_id'] ) ) $this->setTransmissionConditionTypeId( $arrValues['transmission_condition_type_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionId', trim( $arrValues['transmission_id'] ) ); elseif( isset( $arrValues['transmission_id'] ) ) $this->setTransmissionId( $arrValues['transmission_id'] );
		if( isset( $arrValues['guarantor_applicant_application_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intGuarantorApplicantApplicationTransmissionId', trim( $arrValues['guarantor_applicant_application_transmission_id'] ) ); elseif( isset( $arrValues['guarantor_applicant_application_transmission_id'] ) ) $this->setGuarantorApplicantApplicationTransmissionId( $arrValues['guarantor_applicant_application_transmission_id'] );
		if( isset( $arrValues['guarantor_applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intGuarantorApplicantApplicationId', trim( $arrValues['guarantor_applicant_application_id'] ) ); elseif( isset( $arrValues['guarantor_applicant_application_id'] ) ) $this->setGuarantorApplicantApplicationId( $arrValues['guarantor_applicant_application_id'] );
		if( isset( $arrValues['deposit_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositArTransactionId', trim( $arrValues['deposit_ar_transaction_id'] ) ); elseif( isset( $arrValues['deposit_ar_transaction_id'] ) ) $this->setDepositArTransactionId( $arrValues['deposit_ar_transaction_id'] );
		if( isset( $arrValues['rent_application_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intRentApplicationRateId', trim( $arrValues['rent_application_rate_id'] ) ); elseif( isset( $arrValues['rent_application_rate_id'] ) ) $this->setRentApplicationRateId( $arrValues['rent_application_rate_id'] );
		if( isset( $arrValues['rent_scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intRentScheduledChargeId', trim( $arrValues['rent_scheduled_charge_id'] ) ); elseif( isset( $arrValues['rent_scheduled_charge_id'] ) ) $this->setRentScheduledChargeId( $arrValues['rent_scheduled_charge_id'] );
		if( isset( $arrValues['condition_datetime'] ) && $boolDirectSet ) $this->set( 'm_strConditionDatetime', trim( $arrValues['condition_datetime'] ) ); elseif( isset( $arrValues['condition_datetime'] ) ) $this->setConditionDatetime( $arrValues['condition_datetime'] );
		if( isset( $arrValues['satisfied_by'] ) && $boolDirectSet ) $this->set( 'm_intSatisfiedBy', trim( $arrValues['satisfied_by'] ) ); elseif( isset( $arrValues['satisfied_by'] ) ) $this->setSatisfiedBy( $arrValues['satisfied_by'] );
		if( isset( $arrValues['satisfied_on'] ) && $boolDirectSet ) $this->set( 'm_strSatisfiedOn', trim( $arrValues['satisfied_on'] ) ); elseif( isset( $arrValues['satisfied_on'] ) ) $this->setSatisfiedOn( $arrValues['satisfied_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransmissionConditionTypeId( $intTransmissionConditionTypeId ) {
		$this->set( 'm_intTransmissionConditionTypeId', CStrings::strToIntDef( $intTransmissionConditionTypeId, NULL, false ) );
	}

	public function getTransmissionConditionTypeId() {
		return $this->m_intTransmissionConditionTypeId;
	}

	public function sqlTransmissionConditionTypeId() {
		return ( true == isset( $this->m_intTransmissionConditionTypeId ) ) ? ( string ) $this->m_intTransmissionConditionTypeId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setTransmissionId( $intTransmissionId ) {
		$this->set( 'm_intTransmissionId', CStrings::strToIntDef( $intTransmissionId, NULL, false ) );
	}

	public function getTransmissionId() {
		return $this->m_intTransmissionId;
	}

	public function sqlTransmissionId() {
		return ( true == isset( $this->m_intTransmissionId ) ) ? ( string ) $this->m_intTransmissionId : 'NULL';
	}

	public function setGuarantorApplicantApplicationTransmissionId( $intGuarantorApplicantApplicationTransmissionId ) {
		$this->set( 'm_intGuarantorApplicantApplicationTransmissionId', CStrings::strToIntDef( $intGuarantorApplicantApplicationTransmissionId, NULL, false ) );
	}

	public function getGuarantorApplicantApplicationTransmissionId() {
		return $this->m_intGuarantorApplicantApplicationTransmissionId;
	}

	public function sqlGuarantorApplicantApplicationTransmissionId() {
		return ( true == isset( $this->m_intGuarantorApplicantApplicationTransmissionId ) ) ? ( string ) $this->m_intGuarantorApplicantApplicationTransmissionId : 'NULL';
	}

	public function setGuarantorApplicantApplicationId( $intGuarantorApplicantApplicationId ) {
		$this->set( 'm_intGuarantorApplicantApplicationId', CStrings::strToIntDef( $intGuarantorApplicantApplicationId, NULL, false ) );
	}

	public function getGuarantorApplicantApplicationId() {
		return $this->m_intGuarantorApplicantApplicationId;
	}

	public function sqlGuarantorApplicantApplicationId() {
		return ( true == isset( $this->m_intGuarantorApplicantApplicationId ) ) ? ( string ) $this->m_intGuarantorApplicantApplicationId : 'NULL';
	}

	public function setDepositArTransactionId( $intDepositArTransactionId ) {
		$this->set( 'm_intDepositArTransactionId', CStrings::strToIntDef( $intDepositArTransactionId, NULL, false ) );
	}

	public function getDepositArTransactionId() {
		return $this->m_intDepositArTransactionId;
	}

	public function sqlDepositArTransactionId() {
		return ( true == isset( $this->m_intDepositArTransactionId ) ) ? ( string ) $this->m_intDepositArTransactionId : 'NULL';
	}

	public function setRentApplicationRateId( $intRentApplicationRateId ) {
		$this->set( 'm_intRentApplicationRateId', CStrings::strToIntDef( $intRentApplicationRateId, NULL, false ) );
	}

	public function getRentApplicationRateId() {
		return $this->m_intRentApplicationRateId;
	}

	public function sqlRentApplicationRateId() {
		return ( true == isset( $this->m_intRentApplicationRateId ) ) ? ( string ) $this->m_intRentApplicationRateId : 'NULL';
	}

	public function setRentScheduledChargeId( $intRentScheduledChargeId ) {
		$this->set( 'm_intRentScheduledChargeId', CStrings::strToIntDef( $intRentScheduledChargeId, NULL, false ) );
	}

	public function getRentScheduledChargeId() {
		return $this->m_intRentScheduledChargeId;
	}

	public function sqlRentScheduledChargeId() {
		return ( true == isset( $this->m_intRentScheduledChargeId ) ) ? ( string ) $this->m_intRentScheduledChargeId : 'NULL';
	}

	public function setConditionDatetime( $strConditionDatetime ) {
		$this->set( 'm_strConditionDatetime', CStrings::strTrimDef( $strConditionDatetime, -1, NULL, true ) );
	}

	public function getConditionDatetime() {
		return $this->m_strConditionDatetime;
	}

	public function sqlConditionDatetime() {
		return ( true == isset( $this->m_strConditionDatetime ) ) ? '\'' . $this->m_strConditionDatetime . '\'' : 'NULL';
	}

	public function setSatisfiedBy( $intSatisfiedBy ) {
		$this->set( 'm_intSatisfiedBy', CStrings::strToIntDef( $intSatisfiedBy, NULL, false ) );
	}

	public function getSatisfiedBy() {
		return $this->m_intSatisfiedBy;
	}

	public function sqlSatisfiedBy() {
		return ( true == isset( $this->m_intSatisfiedBy ) ) ? ( string ) $this->m_intSatisfiedBy : 'NULL';
	}

	public function setSatisfiedOn( $strSatisfiedOn ) {
		$this->set( 'm_strSatisfiedOn', CStrings::strTrimDef( $strSatisfiedOn, -1, NULL, true ) );
	}

	public function getSatisfiedOn() {
		return $this->m_strSatisfiedOn;
	}

	public function sqlSatisfiedOn() {
		return ( true == isset( $this->m_strSatisfiedOn ) ) ? '\'' . $this->m_strSatisfiedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transmission_condition_type_id, application_id, transmission_id, guarantor_applicant_application_transmission_id, guarantor_applicant_application_id, deposit_ar_transaction_id, rent_application_rate_id, rent_scheduled_charge_id, condition_datetime, satisfied_by, satisfied_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTransmissionConditionTypeId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlTransmissionId() . ', ' .
 						$this->sqlGuarantorApplicantApplicationTransmissionId() . ', ' .
 						$this->sqlGuarantorApplicantApplicationId() . ', ' .
 						$this->sqlDepositArTransactionId() . ', ' .
 						$this->sqlRentApplicationRateId() . ', ' .
 						$this->sqlRentScheduledChargeId() . ', ' .
 						$this->sqlConditionDatetime() . ', ' .
 						$this->sqlSatisfiedBy() . ', ' .
 						$this->sqlSatisfiedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_condition_type_id = ' . $this->sqlTransmissionConditionTypeId() . ','; } elseif( true == array_key_exists( 'TransmissionConditionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_condition_type_id = ' . $this->sqlTransmissionConditionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId() . ','; } elseif( true == array_key_exists( 'TransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_id = ' . $this->sqlTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guarantor_applicant_application_transmission_id = ' . $this->sqlGuarantorApplicantApplicationTransmissionId() . ','; } elseif( true == array_key_exists( 'GuarantorApplicantApplicationTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' guarantor_applicant_application_transmission_id = ' . $this->sqlGuarantorApplicantApplicationTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guarantor_applicant_application_id = ' . $this->sqlGuarantorApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'GuarantorApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' guarantor_applicant_application_id = ' . $this->sqlGuarantorApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_ar_transaction_id = ' . $this->sqlDepositArTransactionId() . ','; } elseif( true == array_key_exists( 'DepositArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_ar_transaction_id = ' . $this->sqlDepositArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_application_rate_id = ' . $this->sqlRentApplicationRateId() . ','; } elseif( true == array_key_exists( 'RentApplicationRateId', $this->getChangedColumns() ) ) { $strSql .= ' rent_application_rate_id = ' . $this->sqlRentApplicationRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_scheduled_charge_id = ' . $this->sqlRentScheduledChargeId() . ','; } elseif( true == array_key_exists( 'RentScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' rent_scheduled_charge_id = ' . $this->sqlRentScheduledChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_datetime = ' . $this->sqlConditionDatetime() . ','; } elseif( true == array_key_exists( 'ConditionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' condition_datetime = ' . $this->sqlConditionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy() . ','; } elseif( true == array_key_exists( 'SatisfiedBy', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; } elseif( true == array_key_exists( 'SatisfiedOn', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transmission_condition_type_id' => $this->getTransmissionConditionTypeId(),
			'application_id' => $this->getApplicationId(),
			'transmission_id' => $this->getTransmissionId(),
			'guarantor_applicant_application_transmission_id' => $this->getGuarantorApplicantApplicationTransmissionId(),
			'guarantor_applicant_application_id' => $this->getGuarantorApplicantApplicationId(),
			'deposit_ar_transaction_id' => $this->getDepositArTransactionId(),
			'rent_application_rate_id' => $this->getRentApplicationRateId(),
			'rent_scheduled_charge_id' => $this->getRentScheduledChargeId(),
			'condition_datetime' => $this->getConditionDatetime(),
			'satisfied_by' => $this->getSatisfiedBy(),
			'satisfied_on' => $this->getSatisfiedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>