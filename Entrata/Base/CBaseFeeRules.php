<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeRules extends CEosPluralBase {

	/**
	 * @return CFeeRule[]
	 */
	public static function fetchFeeRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFeeRule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFeeRule
	 */
	public static function fetchFeeRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFeeRule', $objDatabase );
	}

	public static function fetchFeeRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_rules', $objDatabase );
	}

	public static function fetchFeeRuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFeeRule( sprintf( 'SELECT * FROM fee_rules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByCid( $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByFeeIdByCid( $intFeeId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE fee_id = %d AND cid = %d', ( int ) $intFeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE fee_template_id = %d AND cid = %d', ( int ) $intFeeTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByFeeTemplateRuleTypeIdByCid( $intFeeTemplateRuleTypeId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE fee_template_rule_type_id = %d AND cid = %d', ( int ) $intFeeTemplateRuleTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByFeeTemplateRuleIdByCid( $intFeeTemplateRuleId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE fee_template_rule_id = %d AND cid = %d', ( int ) $intFeeTemplateRuleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByGlGroupIdByCid( $intGlGroupId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE gl_group_id = %d AND cid = %d', ( int ) $intGlGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFeeRulesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchFeeRules( sprintf( 'SELECT * FROM fee_rules WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>