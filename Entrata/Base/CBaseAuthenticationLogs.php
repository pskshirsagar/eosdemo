<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAuthenticationLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAuthenticationLogs extends CEosPluralBase {

	/**
	 * @return CAuthenticationLog[]
	 */
	public static function fetchAuthenticationLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAuthenticationLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAuthenticationLog
	 */
	public static function fetchAuthenticationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAuthenticationLog', $objDatabase );
	}

	public static function fetchAuthenticationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'authentication_logs', $objDatabase );
	}

	public static function fetchAuthenticationLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLog( sprintf( 'SELECT * FROM authentication_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByCid( $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByEmployeeIdByCid( $intEmployeeId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE employee_id = %d AND cid = %d', ( int ) $intEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsByAuthenticationLogTypeIdByCid( $intAuthenticationLogTypeId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE authentication_log_type_id = %d AND cid = %d', ( int ) $intAuthenticationLogTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAuthenticationLogsBySessionIdByCid( $intSessionId, $intCid, $objDatabase ) {
		return self::fetchAuthenticationLogs( sprintf( 'SELECT * FROM authentication_logs WHERE session_id = %d AND cid = %d', ( int ) $intSessionId, ( int ) $intCid ), $objDatabase );
	}

}
?>