<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExportTypes
 * Do not add any new functions to this class.
 */

class CBaseFileExportTypes extends CEosPluralBase {

	/**
	 * @return CFileExportType[]
	 */
	public static function fetchFileExportTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFileExportType::class, $objDatabase );
	}

	/**
	 * @return CFileExportType
	 */
	public static function fetchFileExportType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileExportType::class, $objDatabase );
	}

	public static function fetchFileExportTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_export_types', $objDatabase );
	}

	public static function fetchFileExportTypeById( $intId, $objDatabase ) {
		return self::fetchFileExportType( sprintf( 'SELECT * FROM file_export_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>