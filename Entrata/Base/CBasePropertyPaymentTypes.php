<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPaymentTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPaymentTypes extends CEosPluralBase {

	/**
	 * @return CPropertyPaymentType[]
	 */
	public static function fetchPropertyPaymentTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyPaymentType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyPaymentType
	 */
	public static function fetchPropertyPaymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyPaymentType', $objDatabase );
	}

	public static function fetchPropertyPaymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_payment_types', $objDatabase );
	}

	public static function fetchPropertyPaymentTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyPaymentType( sprintf( 'SELECT * FROM property_payment_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPaymentTypesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyPaymentTypes( sprintf( 'SELECT * FROM property_payment_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPaymentTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPaymentTypes( sprintf( 'SELECT * FROM property_payment_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPaymentTypesByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyPaymentTypes( sprintf( 'SELECT * FROM property_payment_types WHERE payment_type_id = %d AND cid = %d', ( int ) $intPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>