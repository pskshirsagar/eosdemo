<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMiscellaneousRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyMiscellaneousRequests extends CEosPluralBase {

	/**
	 * @return CSubsidyMiscellaneousRequest[]
	 */
	public static function fetchSubsidyMiscellaneousRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSubsidyMiscellaneousRequest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyMiscellaneousRequest
	 */
	public static function fetchSubsidyMiscellaneousRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyMiscellaneousRequest', $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_miscellaneous_requests', $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequest( sprintf( 'SELECT * FROM subsidy_miscellaneous_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequests( sprintf( 'SELECT * FROM subsidy_miscellaneous_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequests( sprintf( 'SELECT * FROM subsidy_miscellaneous_requests WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestsBySubsidyHapRequestIdByCid( $intSubsidyHapRequestId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequests( sprintf( 'SELECT * FROM subsidy_miscellaneous_requests WHERE subsidy_hap_request_id = %d AND cid = %d', ( int ) $intSubsidyHapRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyMiscellaneousRequestsBySubsidyMiscellaneousRequestTypeIdByCid( $intSubsidyMiscellaneousRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyMiscellaneousRequests( sprintf( 'SELECT * FROM subsidy_miscellaneous_requests WHERE subsidy_miscellaneous_request_type_id = %d AND cid = %d', ( int ) $intSubsidyMiscellaneousRequestTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>