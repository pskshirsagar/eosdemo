<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicant extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.applicants';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaritalStatusTypeId;
	protected $m_intCustomerId;
	protected $m_intOriginatingCustomerId;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intSecondaryPhoneNumberTypeId;
	protected $m_intMessageOperatorId;
	protected $m_strStudentIdNumber;
	protected $m_strGuestRemotePrimaryKey;
	protected $m_strAppRemotePrimaryKey;
	protected $m_strRenewalRemotePrimaryKey;
	protected $m_strCompanyName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strNameMaiden;
	protected $m_strPhoneNumber;
	protected $m_strMobileNumber;
	protected $m_strWorkNumber;
	protected $m_strFaxNumber;
	protected $m_strEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTaxNumberMasked;
	protected $m_strBirthDate;
	protected $m_strGender;
	protected $m_intHeight;
	protected $m_intWeight;
	protected $m_intOccupationId;
	protected $m_strEyeColor;
	protected $m_strHairColor;
	protected $m_strDlNumberEncrypted;
	protected $m_strDlStateCode;
	protected $m_intCompanyIdentificationTypeId;
	protected $m_strIdentificationValue;
	protected $m_strIdentificationExpiration;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strFacebookUser;
	protected $m_strLastAccessOn;
	protected $m_intMaxLoginAttempts;
	protected $m_intLoginErrorCount;
	protected $m_intRequiresPasswordChange;
	protected $m_strRelocationReason;
	protected $m_intHasFelonyConviction;
	protected $m_intFelonyIsResolved;
	protected $m_strFelonyComments;
	protected $m_intFelonyYear;
	protected $m_intSmokes;
	protected $m_intHasPets;
	protected $m_strPetDescription;
	protected $m_intHasBeenEvicted;
	protected $m_intHasBrokenLease;
	protected $m_intHasBankruptcy;
	protected $m_intHasPreviousRentOwed;
	protected $m_intHasMiddleName;
	protected $m_intIsReturningResident;
	protected $m_intIsUnemployed;
	protected $m_intIsLegalUsResident;
	protected $m_strBankruptcyCounty;
	protected $m_strBankruptcyStateCode;
	protected $m_intBankruptcyYear;
	protected $m_intHasPublicJudgment;
	protected $m_intPublicJudgmentYear;
	protected $m_intWasSuedForRent;
	protected $m_intWasSuedForDamages;
	protected $m_strSuedForRentComments;
	protected $m_strSuedForDamageComments;
	protected $m_strEvictedComments;
	protected $m_strBrokenLeaseComments;
	protected $m_strFelonyConvictionComments;
	protected $m_strPreviousRentOwedComments;
	protected $m_strBankruptcyComments;
	protected $m_strReturningResidentComments;
	protected $m_strUnemployedComments;
	protected $m_strReasonInUsComments;
	protected $m_strGaaScreeningInitials;
	protected $m_strRelationshipToApplicant;
	protected $m_intHasRentersInsurance;
	protected $m_strInsuranceProviderName;
	protected $m_strInsurancePolicyNumber;
	protected $m_strReferenceComments;
	protected $m_intIsAlien;
	protected $m_intReferencesApprovedBy;
	protected $m_strReferencesApprovedOn;
	protected $m_strCurrentManagerNameFirst;
	protected $m_strCurrentManagerNameLast;
	protected $m_strCurrentPhoneNumber;
	protected $m_strCurrentFaxNumber;
	protected $m_strCurrentEmailAddress;
	protected $m_strPreviousManagerNameFirst;
	protected $m_strPreviousManagerNameLast;
	protected $m_strPreviousPhoneNumber;
	protected $m_strPreviousFaxNumber;
	protected $m_strPreviousEmailAddress;
	protected $m_strParentNameFirst;
	protected $m_strParentNameLast;
	protected $m_strParentEmailAddress;
	protected $m_strNotes;
	protected $m_intOrderNum;
	protected $m_strSmsActivatedOn;
	protected $m_strSmsCancelledOn;
	protected $m_strSmsApplicationUpdatesActivatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAltNameFirst;
	protected $m_strAltNameMiddle;
	protected $m_strAltNameLast;
	protected $m_intHasAltMiddleName;
	protected $m_strPreferredName;
	protected $m_strNameLastMatronymic;
	protected $m_intHasNameLastMatronymic;
	protected $m_strCitizenshipCountryCode;
	protected $m_intTaxIdTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intMaxLoginAttempts = '50';
		$this->m_intLoginErrorCount = '0';
		$this->m_intRequiresPasswordChange = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['marital_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaritalStatusTypeId', trim( $arrValues['marital_status_type_id'] ) ); elseif( isset( $arrValues['marital_status_type_id'] ) ) $this->setMaritalStatusTypeId( $arrValues['marital_status_type_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['originating_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginatingCustomerId', trim( $arrValues['originating_customer_id'] ) ); elseif( isset( $arrValues['originating_customer_id'] ) ) $this->setOriginatingCustomerId( $arrValues['originating_customer_id'] );
		if( isset( $arrValues['primary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryPhoneNumberTypeId', trim( $arrValues['primary_phone_number_type_id'] ) ); elseif( isset( $arrValues['primary_phone_number_type_id'] ) ) $this->setPrimaryPhoneNumberTypeId( $arrValues['primary_phone_number_type_id'] );
		if( isset( $arrValues['secondary_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryPhoneNumberTypeId', trim( $arrValues['secondary_phone_number_type_id'] ) ); elseif( isset( $arrValues['secondary_phone_number_type_id'] ) ) $this->setSecondaryPhoneNumberTypeId( $arrValues['secondary_phone_number_type_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['student_id_number'] ) && $boolDirectSet ) $this->set( 'm_strStudentIdNumber', trim( $arrValues['student_id_number'] ) ); elseif( isset( $arrValues['student_id_number'] ) ) $this->setStudentIdNumber( $arrValues['student_id_number'] );
		if( isset( $arrValues['guest_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strGuestRemotePrimaryKey', trim( $arrValues['guest_remote_primary_key'] ) ); elseif( isset( $arrValues['guest_remote_primary_key'] ) ) $this->setGuestRemotePrimaryKey( $arrValues['guest_remote_primary_key'] );
		if( isset( $arrValues['app_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strAppRemotePrimaryKey', trim( $arrValues['app_remote_primary_key'] ) ); elseif( isset( $arrValues['app_remote_primary_key'] ) ) $this->setAppRemotePrimaryKey( $arrValues['app_remote_primary_key'] );
		if( isset( $arrValues['renewal_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRenewalRemotePrimaryKey', trim( $arrValues['renewal_remote_primary_key'] ) ); elseif( isset( $arrValues['renewal_remote_primary_key'] ) ) $this->setRenewalRemotePrimaryKey( $arrValues['renewal_remote_primary_key'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( $arrValues['mobile_number'] ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( $arrValues['mobile_number'] );
		if( isset( $arrValues['work_number'] ) && $boolDirectSet ) $this->set( 'm_strWorkNumber', trim( $arrValues['work_number'] ) ); elseif( isset( $arrValues['work_number'] ) ) $this->setWorkNumber( $arrValues['work_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberMasked', trim( $arrValues['tax_number_masked'] ) ); elseif( isset( $arrValues['tax_number_masked'] ) ) $this->setTaxNumberMasked( $arrValues['tax_number_masked'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		if( isset( $arrValues['weight'] ) && $boolDirectSet ) $this->set( 'm_intWeight', trim( $arrValues['weight'] ) ); elseif( isset( $arrValues['weight'] ) ) $this->setWeight( $arrValues['weight'] );
		if( isset( $arrValues['occupation_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupationId', trim( $arrValues['occupation_id'] ) ); elseif( isset( $arrValues['occupation_id'] ) ) $this->setOccupationId( $arrValues['occupation_id'] );
		if( isset( $arrValues['eye_color'] ) && $boolDirectSet ) $this->set( 'm_strEyeColor', trim( $arrValues['eye_color'] ) ); elseif( isset( $arrValues['eye_color'] ) ) $this->setEyeColor( $arrValues['eye_color'] );
		if( isset( $arrValues['hair_color'] ) && $boolDirectSet ) $this->set( 'm_strHairColor', trim( $arrValues['hair_color'] ) ); elseif( isset( $arrValues['hair_color'] ) ) $this->setHairColor( $arrValues['hair_color'] );
		if( isset( $arrValues['dl_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strDlNumberEncrypted', trim( $arrValues['dl_number_encrypted'] ) ); elseif( isset( $arrValues['dl_number_encrypted'] ) ) $this->setDlNumberEncrypted( $arrValues['dl_number_encrypted'] );
		if( isset( $arrValues['dl_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDlStateCode', trim( $arrValues['dl_state_code'] ) ); elseif( isset( $arrValues['dl_state_code'] ) ) $this->setDlStateCode( $arrValues['dl_state_code'] );
		if( isset( $arrValues['company_identification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyIdentificationTypeId', trim( $arrValues['company_identification_type_id'] ) ); elseif( isset( $arrValues['company_identification_type_id'] ) ) $this->setCompanyIdentificationTypeId( $arrValues['company_identification_type_id'] );
		if( isset( $arrValues['identification_value'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationValue', trim( $arrValues['identification_value'] ) ); elseif( isset( $arrValues['identification_value'] ) ) $this->setIdentificationValue( $arrValues['identification_value'] );
		if( isset( $arrValues['identification_expiration'] ) && $boolDirectSet ) $this->set( 'm_strIdentificationExpiration', trim( $arrValues['identification_expiration'] ) ); elseif( isset( $arrValues['identification_expiration'] ) ) $this->setIdentificationExpiration( $arrValues['identification_expiration'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['facebook_user'] ) && $boolDirectSet ) $this->set( 'm_strFacebookUser', trim( $arrValues['facebook_user'] ) ); elseif( isset( $arrValues['facebook_user'] ) ) $this->setFacebookUser( $arrValues['facebook_user'] );
		if( isset( $arrValues['last_access_on'] ) && $boolDirectSet ) $this->set( 'm_strLastAccessOn', trim( $arrValues['last_access_on'] ) ); elseif( isset( $arrValues['last_access_on'] ) ) $this->setLastAccessOn( $arrValues['last_access_on'] );
		if( isset( $arrValues['max_login_attempts'] ) && $boolDirectSet ) $this->set( 'm_intMaxLoginAttempts', trim( $arrValues['max_login_attempts'] ) ); elseif( isset( $arrValues['max_login_attempts'] ) ) $this->setMaxLoginAttempts( $arrValues['max_login_attempts'] );
		if( isset( $arrValues['login_error_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginErrorCount', trim( $arrValues['login_error_count'] ) ); elseif( isset( $arrValues['login_error_count'] ) ) $this->setLoginErrorCount( $arrValues['login_error_count'] );
		if( isset( $arrValues['requires_password_change'] ) && $boolDirectSet ) $this->set( 'm_intRequiresPasswordChange', trim( $arrValues['requires_password_change'] ) ); elseif( isset( $arrValues['requires_password_change'] ) ) $this->setRequiresPasswordChange( $arrValues['requires_password_change'] );
		if( isset( $arrValues['relocation_reason'] ) && $boolDirectSet ) $this->set( 'm_strRelocationReason', trim( $arrValues['relocation_reason'] ) ); elseif( isset( $arrValues['relocation_reason'] ) ) $this->setRelocationReason( $arrValues['relocation_reason'] );
		if( isset( $arrValues['has_felony_conviction'] ) && $boolDirectSet ) $this->set( 'm_intHasFelonyConviction', trim( $arrValues['has_felony_conviction'] ) ); elseif( isset( $arrValues['has_felony_conviction'] ) ) $this->setHasFelonyConviction( $arrValues['has_felony_conviction'] );
		if( isset( $arrValues['felony_is_resolved'] ) && $boolDirectSet ) $this->set( 'm_intFelonyIsResolved', trim( $arrValues['felony_is_resolved'] ) ); elseif( isset( $arrValues['felony_is_resolved'] ) ) $this->setFelonyIsResolved( $arrValues['felony_is_resolved'] );
		if( isset( $arrValues['felony_comments'] ) && $boolDirectSet ) $this->set( 'm_strFelonyComments', trim( $arrValues['felony_comments'] ) ); elseif( isset( $arrValues['felony_comments'] ) ) $this->setFelonyComments( $arrValues['felony_comments'] );
		if( isset( $arrValues['felony_year'] ) && $boolDirectSet ) $this->set( 'm_intFelonyYear', trim( $arrValues['felony_year'] ) ); elseif( isset( $arrValues['felony_year'] ) ) $this->setFelonyYear( $arrValues['felony_year'] );
		if( isset( $arrValues['smokes'] ) && $boolDirectSet ) $this->set( 'm_intSmokes', trim( $arrValues['smokes'] ) ); elseif( isset( $arrValues['smokes'] ) ) $this->setSmokes( $arrValues['smokes'] );
		if( isset( $arrValues['has_pets'] ) && $boolDirectSet ) $this->set( 'm_intHasPets', trim( $arrValues['has_pets'] ) ); elseif( isset( $arrValues['has_pets'] ) ) $this->setHasPets( $arrValues['has_pets'] );
		if( isset( $arrValues['pet_description'] ) && $boolDirectSet ) $this->set( 'm_strPetDescription', trim( $arrValues['pet_description'] ) ); elseif( isset( $arrValues['pet_description'] ) ) $this->setPetDescription( $arrValues['pet_description'] );
		if( isset( $arrValues['has_been_evicted'] ) && $boolDirectSet ) $this->set( 'm_intHasBeenEvicted', trim( $arrValues['has_been_evicted'] ) ); elseif( isset( $arrValues['has_been_evicted'] ) ) $this->setHasBeenEvicted( $arrValues['has_been_evicted'] );
		if( isset( $arrValues['has_broken_lease'] ) && $boolDirectSet ) $this->set( 'm_intHasBrokenLease', trim( $arrValues['has_broken_lease'] ) ); elseif( isset( $arrValues['has_broken_lease'] ) ) $this->setHasBrokenLease( $arrValues['has_broken_lease'] );
		if( isset( $arrValues['has_bankruptcy'] ) && $boolDirectSet ) $this->set( 'm_intHasBankruptcy', trim( $arrValues['has_bankruptcy'] ) ); elseif( isset( $arrValues['has_bankruptcy'] ) ) $this->setHasBankruptcy( $arrValues['has_bankruptcy'] );
		if( isset( $arrValues['has_previous_rent_owed'] ) && $boolDirectSet ) $this->set( 'm_intHasPreviousRentOwed', trim( $arrValues['has_previous_rent_owed'] ) ); elseif( isset( $arrValues['has_previous_rent_owed'] ) ) $this->setHasPreviousRentOwed( $arrValues['has_previous_rent_owed'] );
		if( isset( $arrValues['has_middle_name'] ) && $boolDirectSet ) $this->set( 'm_intHasMiddleName', trim( $arrValues['has_middle_name'] ) ); elseif( isset( $arrValues['has_middle_name'] ) ) $this->setHasMiddleName( $arrValues['has_middle_name'] );
		if( isset( $arrValues['is_returning_resident'] ) && $boolDirectSet ) $this->set( 'm_intIsReturningResident', trim( $arrValues['is_returning_resident'] ) ); elseif( isset( $arrValues['is_returning_resident'] ) ) $this->setIsReturningResident( $arrValues['is_returning_resident'] );
		if( isset( $arrValues['is_unemployed'] ) && $boolDirectSet ) $this->set( 'm_intIsUnemployed', trim( $arrValues['is_unemployed'] ) ); elseif( isset( $arrValues['is_unemployed'] ) ) $this->setIsUnemployed( $arrValues['is_unemployed'] );
		if( isset( $arrValues['is_legal_us_resident'] ) && $boolDirectSet ) $this->set( 'm_intIsLegalUsResident', trim( $arrValues['is_legal_us_resident'] ) ); elseif( isset( $arrValues['is_legal_us_resident'] ) ) $this->setIsLegalUsResident( $arrValues['is_legal_us_resident'] );
		if( isset( $arrValues['bankruptcy_county'] ) && $boolDirectSet ) $this->set( 'm_strBankruptcyCounty', trim( $arrValues['bankruptcy_county'] ) ); elseif( isset( $arrValues['bankruptcy_county'] ) ) $this->setBankruptcyCounty( $arrValues['bankruptcy_county'] );
		if( isset( $arrValues['bankruptcy_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBankruptcyStateCode', trim( $arrValues['bankruptcy_state_code'] ) ); elseif( isset( $arrValues['bankruptcy_state_code'] ) ) $this->setBankruptcyStateCode( $arrValues['bankruptcy_state_code'] );
		if( isset( $arrValues['bankruptcy_year'] ) && $boolDirectSet ) $this->set( 'm_intBankruptcyYear', trim( $arrValues['bankruptcy_year'] ) ); elseif( isset( $arrValues['bankruptcy_year'] ) ) $this->setBankruptcyYear( $arrValues['bankruptcy_year'] );
		if( isset( $arrValues['has_public_judgment'] ) && $boolDirectSet ) $this->set( 'm_intHasPublicJudgment', trim( $arrValues['has_public_judgment'] ) ); elseif( isset( $arrValues['has_public_judgment'] ) ) $this->setHasPublicJudgment( $arrValues['has_public_judgment'] );
		if( isset( $arrValues['public_judgment_year'] ) && $boolDirectSet ) $this->set( 'm_intPublicJudgmentYear', trim( $arrValues['public_judgment_year'] ) ); elseif( isset( $arrValues['public_judgment_year'] ) ) $this->setPublicJudgmentYear( $arrValues['public_judgment_year'] );
		if( isset( $arrValues['was_sued_for_rent'] ) && $boolDirectSet ) $this->set( 'm_intWasSuedForRent', trim( $arrValues['was_sued_for_rent'] ) ); elseif( isset( $arrValues['was_sued_for_rent'] ) ) $this->setWasSuedForRent( $arrValues['was_sued_for_rent'] );
		if( isset( $arrValues['was_sued_for_damages'] ) && $boolDirectSet ) $this->set( 'm_intWasSuedForDamages', trim( $arrValues['was_sued_for_damages'] ) ); elseif( isset( $arrValues['was_sued_for_damages'] ) ) $this->setWasSuedForDamages( $arrValues['was_sued_for_damages'] );
		if( isset( $arrValues['sued_for_rent_comments'] ) && $boolDirectSet ) $this->set( 'm_strSuedForRentComments', trim( $arrValues['sued_for_rent_comments'] ) ); elseif( isset( $arrValues['sued_for_rent_comments'] ) ) $this->setSuedForRentComments( $arrValues['sued_for_rent_comments'] );
		if( isset( $arrValues['sued_for_damage_comments'] ) && $boolDirectSet ) $this->set( 'm_strSuedForDamageComments', trim( $arrValues['sued_for_damage_comments'] ) ); elseif( isset( $arrValues['sued_for_damage_comments'] ) ) $this->setSuedForDamageComments( $arrValues['sued_for_damage_comments'] );
		if( isset( $arrValues['evicted_comments'] ) && $boolDirectSet ) $this->set( 'm_strEvictedComments', trim( $arrValues['evicted_comments'] ) ); elseif( isset( $arrValues['evicted_comments'] ) ) $this->setEvictedComments( $arrValues['evicted_comments'] );
		if( isset( $arrValues['broken_lease_comments'] ) && $boolDirectSet ) $this->set( 'm_strBrokenLeaseComments', trim( $arrValues['broken_lease_comments'] ) ); elseif( isset( $arrValues['broken_lease_comments'] ) ) $this->setBrokenLeaseComments( $arrValues['broken_lease_comments'] );
		if( isset( $arrValues['felony_conviction_comments'] ) && $boolDirectSet ) $this->set( 'm_strFelonyConvictionComments', trim( $arrValues['felony_conviction_comments'] ) ); elseif( isset( $arrValues['felony_conviction_comments'] ) ) $this->setFelonyConvictionComments( $arrValues['felony_conviction_comments'] );
		if( isset( $arrValues['previous_rent_owed_comments'] ) && $boolDirectSet ) $this->set( 'm_strPreviousRentOwedComments', trim( $arrValues['previous_rent_owed_comments'] ) ); elseif( isset( $arrValues['previous_rent_owed_comments'] ) ) $this->setPreviousRentOwedComments( $arrValues['previous_rent_owed_comments'] );
		if( isset( $arrValues['bankruptcy_comments'] ) && $boolDirectSet ) $this->set( 'm_strBankruptcyComments', trim( $arrValues['bankruptcy_comments'] ) ); elseif( isset( $arrValues['bankruptcy_comments'] ) ) $this->setBankruptcyComments( $arrValues['bankruptcy_comments'] );
		if( isset( $arrValues['returning_resident_comments'] ) && $boolDirectSet ) $this->set( 'm_strReturningResidentComments', trim( $arrValues['returning_resident_comments'] ) ); elseif( isset( $arrValues['returning_resident_comments'] ) ) $this->setReturningResidentComments( $arrValues['returning_resident_comments'] );
		if( isset( $arrValues['unemployed_comments'] ) && $boolDirectSet ) $this->set( 'm_strUnemployedComments', trim( $arrValues['unemployed_comments'] ) ); elseif( isset( $arrValues['unemployed_comments'] ) ) $this->setUnemployedComments( $arrValues['unemployed_comments'] );
		if( isset( $arrValues['reason_in_us_comments'] ) && $boolDirectSet ) $this->set( 'm_strReasonInUsComments', trim( $arrValues['reason_in_us_comments'] ) ); elseif( isset( $arrValues['reason_in_us_comments'] ) ) $this->setReasonInUsComments( $arrValues['reason_in_us_comments'] );
		if( isset( $arrValues['gaa_screening_initials'] ) && $boolDirectSet ) $this->set( 'm_strGaaScreeningInitials', trim( $arrValues['gaa_screening_initials'] ) ); elseif( isset( $arrValues['gaa_screening_initials'] ) ) $this->setGaaScreeningInitials( $arrValues['gaa_screening_initials'] );
		if( isset( $arrValues['relationship_to_applicant'] ) && $boolDirectSet ) $this->set( 'm_strRelationshipToApplicant', trim( $arrValues['relationship_to_applicant'] ) ); elseif( isset( $arrValues['relationship_to_applicant'] ) ) $this->setRelationshipToApplicant( $arrValues['relationship_to_applicant'] );
		if( isset( $arrValues['has_renters_insurance'] ) && $boolDirectSet ) $this->set( 'm_intHasRentersInsurance', trim( $arrValues['has_renters_insurance'] ) ); elseif( isset( $arrValues['has_renters_insurance'] ) ) $this->setHasRentersInsurance( $arrValues['has_renters_insurance'] );
		if( isset( $arrValues['insurance_provider_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuranceProviderName', trim( $arrValues['insurance_provider_name'] ) ); elseif( isset( $arrValues['insurance_provider_name'] ) ) $this->setInsuranceProviderName( $arrValues['insurance_provider_name'] );
		if( isset( $arrValues['insurance_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyNumber', trim( $arrValues['insurance_policy_number'] ) ); elseif( isset( $arrValues['insurance_policy_number'] ) ) $this->setInsurancePolicyNumber( $arrValues['insurance_policy_number'] );
		if( isset( $arrValues['reference_comments'] ) && $boolDirectSet ) $this->set( 'm_strReferenceComments', trim( $arrValues['reference_comments'] ) ); elseif( isset( $arrValues['reference_comments'] ) ) $this->setReferenceComments( $arrValues['reference_comments'] );
		if( isset( $arrValues['is_alien'] ) && $boolDirectSet ) $this->set( 'm_intIsAlien', trim( $arrValues['is_alien'] ) ); elseif( isset( $arrValues['is_alien'] ) ) $this->setIsAlien( $arrValues['is_alien'] );
		if( isset( $arrValues['references_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intReferencesApprovedBy', trim( $arrValues['references_approved_by'] ) ); elseif( isset( $arrValues['references_approved_by'] ) ) $this->setReferencesApprovedBy( $arrValues['references_approved_by'] );
		if( isset( $arrValues['references_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strReferencesApprovedOn', trim( $arrValues['references_approved_on'] ) ); elseif( isset( $arrValues['references_approved_on'] ) ) $this->setReferencesApprovedOn( $arrValues['references_approved_on'] );
		if( isset( $arrValues['current_manager_name_first'] ) && $boolDirectSet ) $this->set( 'm_strCurrentManagerNameFirst', trim( $arrValues['current_manager_name_first'] ) ); elseif( isset( $arrValues['current_manager_name_first'] ) ) $this->setCurrentManagerNameFirst( $arrValues['current_manager_name_first'] );
		if( isset( $arrValues['current_manager_name_last'] ) && $boolDirectSet ) $this->set( 'm_strCurrentManagerNameLast', trim( $arrValues['current_manager_name_last'] ) ); elseif( isset( $arrValues['current_manager_name_last'] ) ) $this->setCurrentManagerNameLast( $arrValues['current_manager_name_last'] );
		if( isset( $arrValues['current_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strCurrentPhoneNumber', trim( $arrValues['current_phone_number'] ) ); elseif( isset( $arrValues['current_phone_number'] ) ) $this->setCurrentPhoneNumber( $arrValues['current_phone_number'] );
		if( isset( $arrValues['current_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strCurrentFaxNumber', trim( $arrValues['current_fax_number'] ) ); elseif( isset( $arrValues['current_fax_number'] ) ) $this->setCurrentFaxNumber( $arrValues['current_fax_number'] );
		if( isset( $arrValues['current_email_address'] ) && $boolDirectSet ) $this->set( 'm_strCurrentEmailAddress', trim( $arrValues['current_email_address'] ) ); elseif( isset( $arrValues['current_email_address'] ) ) $this->setCurrentEmailAddress( $arrValues['current_email_address'] );
		if( isset( $arrValues['previous_manager_name_first'] ) && $boolDirectSet ) $this->set( 'm_strPreviousManagerNameFirst', trim( $arrValues['previous_manager_name_first'] ) ); elseif( isset( $arrValues['previous_manager_name_first'] ) ) $this->setPreviousManagerNameFirst( $arrValues['previous_manager_name_first'] );
		if( isset( $arrValues['previous_manager_name_last'] ) && $boolDirectSet ) $this->set( 'm_strPreviousManagerNameLast', trim( $arrValues['previous_manager_name_last'] ) ); elseif( isset( $arrValues['previous_manager_name_last'] ) ) $this->setPreviousManagerNameLast( $arrValues['previous_manager_name_last'] );
		if( isset( $arrValues['previous_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPreviousPhoneNumber', trim( $arrValues['previous_phone_number'] ) ); elseif( isset( $arrValues['previous_phone_number'] ) ) $this->setPreviousPhoneNumber( $arrValues['previous_phone_number'] );
		if( isset( $arrValues['previous_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strPreviousFaxNumber', trim( $arrValues['previous_fax_number'] ) ); elseif( isset( $arrValues['previous_fax_number'] ) ) $this->setPreviousFaxNumber( $arrValues['previous_fax_number'] );
		if( isset( $arrValues['previous_email_address'] ) && $boolDirectSet ) $this->set( 'm_strPreviousEmailAddress', trim( $arrValues['previous_email_address'] ) ); elseif( isset( $arrValues['previous_email_address'] ) ) $this->setPreviousEmailAddress( $arrValues['previous_email_address'] );
		if( isset( $arrValues['parent_name_first'] ) && $boolDirectSet ) $this->set( 'm_strParentNameFirst', trim( $arrValues['parent_name_first'] ) ); elseif( isset( $arrValues['parent_name_first'] ) ) $this->setParentNameFirst( $arrValues['parent_name_first'] );
		if( isset( $arrValues['parent_name_last'] ) && $boolDirectSet ) $this->set( 'm_strParentNameLast', trim( $arrValues['parent_name_last'] ) ); elseif( isset( $arrValues['parent_name_last'] ) ) $this->setParentNameLast( $arrValues['parent_name_last'] );
		if( isset( $arrValues['parent_email_address'] ) && $boolDirectSet ) $this->set( 'm_strParentEmailAddress', trim( $arrValues['parent_email_address'] ) ); elseif( isset( $arrValues['parent_email_address'] ) ) $this->setParentEmailAddress( $arrValues['parent_email_address'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['sms_activated_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsActivatedOn', trim( $arrValues['sms_activated_on'] ) ); elseif( isset( $arrValues['sms_activated_on'] ) ) $this->setSmsActivatedOn( $arrValues['sms_activated_on'] );
		if( isset( $arrValues['sms_cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsCancelledOn', trim( $arrValues['sms_cancelled_on'] ) ); elseif( isset( $arrValues['sms_cancelled_on'] ) ) $this->setSmsCancelledOn( $arrValues['sms_cancelled_on'] );
		if( isset( $arrValues['sms_application_updates_activated_on'] ) && $boolDirectSet ) $this->set( 'm_strSmsApplicationUpdatesActivatedOn', trim( $arrValues['sms_application_updates_activated_on'] ) ); elseif( isset( $arrValues['sms_application_updates_activated_on'] ) ) $this->setSmsApplicationUpdatesActivatedOn( $arrValues['sms_application_updates_activated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['alt_name_first'] ) && $boolDirectSet ) $this->set( 'm_strAltNameFirst', trim( $arrValues['alt_name_first'] ) ); elseif( isset( $arrValues['alt_name_first'] ) ) $this->setAltNameFirst( $arrValues['alt_name_first'] );
		if( isset( $arrValues['alt_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strAltNameMiddle', trim( $arrValues['alt_name_middle'] ) ); elseif( isset( $arrValues['alt_name_middle'] ) ) $this->setAltNameMiddle( $arrValues['alt_name_middle'] );
		if( isset( $arrValues['alt_name_last'] ) && $boolDirectSet ) $this->set( 'm_strAltNameLast', trim( $arrValues['alt_name_last'] ) ); elseif( isset( $arrValues['alt_name_last'] ) ) $this->setAltNameLast( $arrValues['alt_name_last'] );
		if( isset( $arrValues['has_alt_middle_name'] ) && $boolDirectSet ) $this->set( 'm_intHasAltMiddleName', trim( $arrValues['has_alt_middle_name'] ) ); elseif( isset( $arrValues['has_alt_middle_name'] ) ) $this->setHasAltMiddleName( $arrValues['has_alt_middle_name'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_strNameLastMatronymic', trim( $arrValues['name_last_matronymic'] ) ); elseif( isset( $arrValues['name_last_matronymic'] ) ) $this->setNameLastMatronymic( $arrValues['name_last_matronymic'] );
		if( isset( $arrValues['has_name_last_matronymic'] ) && $boolDirectSet ) $this->set( 'm_intHasNameLastMatronymic', trim( $arrValues['has_name_last_matronymic'] ) ); elseif( isset( $arrValues['has_name_last_matronymic'] ) ) $this->setHasNameLastMatronymic( $arrValues['has_name_last_matronymic'] );
		if( isset( $arrValues['citizenship_country_code'] ) && $boolDirectSet ) $this->set( 'm_strCitizenshipCountryCode', trim( $arrValues['citizenship_country_code'] ) ); elseif( isset( $arrValues['citizenship_country_code'] ) ) $this->setCitizenshipCountryCode( $arrValues['citizenship_country_code'] );
		if( isset( $arrValues['tax_id_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxIdTypeId', trim( $arrValues['tax_id_type_id'] ) ); elseif( isset( $arrValues['tax_id_type_id'] ) ) $this->setTaxIdTypeId( $arrValues['tax_id_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaritalStatusTypeId( $intMaritalStatusTypeId ) {
		$this->set( 'm_intMaritalStatusTypeId', CStrings::strToIntDef( $intMaritalStatusTypeId, NULL, false ) );
	}

	public function getMaritalStatusTypeId() {
		return $this->m_intMaritalStatusTypeId;
	}

	public function sqlMaritalStatusTypeId() {
		return ( true == isset( $this->m_intMaritalStatusTypeId ) ) ? ( string ) $this->m_intMaritalStatusTypeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setOriginatingCustomerId( $intOriginatingCustomerId ) {
		$this->set( 'm_intOriginatingCustomerId', CStrings::strToIntDef( $intOriginatingCustomerId, NULL, false ) );
	}

	public function getOriginatingCustomerId() {
		return $this->m_intOriginatingCustomerId;
	}

	public function sqlOriginatingCustomerId() {
		return ( true == isset( $this->m_intOriginatingCustomerId ) ) ? ( string ) $this->m_intOriginatingCustomerId : 'NULL';
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->set( 'm_intPrimaryPhoneNumberTypeId', CStrings::strToIntDef( $intPrimaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getPrimaryPhoneNumberTypeId() {
		return $this->m_intPrimaryPhoneNumberTypeId;
	}

	public function sqlPrimaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPrimaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intPrimaryPhoneNumberTypeId : 'NULL';
	}

	public function setSecondaryPhoneNumberTypeId( $intSecondaryPhoneNumberTypeId ) {
		$this->set( 'm_intSecondaryPhoneNumberTypeId', CStrings::strToIntDef( $intSecondaryPhoneNumberTypeId, NULL, false ) );
	}

	public function getSecondaryPhoneNumberTypeId() {
		return $this->m_intSecondaryPhoneNumberTypeId;
	}

	public function sqlSecondaryPhoneNumberTypeId() {
		return ( true == isset( $this->m_intSecondaryPhoneNumberTypeId ) ) ? ( string ) $this->m_intSecondaryPhoneNumberTypeId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setStudentIdNumber( $strStudentIdNumber ) {
		$this->set( 'm_strStudentIdNumber', CStrings::strTrimDef( $strStudentIdNumber, 25, NULL, true ) );
	}

	public function getStudentIdNumber() {
		return $this->m_strStudentIdNumber;
	}

	public function sqlStudentIdNumber() {
		return ( true == isset( $this->m_strStudentIdNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStudentIdNumber ) : '\'' . addslashes( $this->m_strStudentIdNumber ) . '\'' ) : 'NULL';
	}

	public function setGuestRemotePrimaryKey( $strGuestRemotePrimaryKey ) {
		$this->set( 'm_strGuestRemotePrimaryKey', CStrings::strTrimDef( $strGuestRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getGuestRemotePrimaryKey() {
		return $this->m_strGuestRemotePrimaryKey;
	}

	public function sqlGuestRemotePrimaryKey() {
		return ( true == isset( $this->m_strGuestRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGuestRemotePrimaryKey ) : '\'' . addslashes( $this->m_strGuestRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setAppRemotePrimaryKey( $strAppRemotePrimaryKey ) {
		$this->set( 'm_strAppRemotePrimaryKey', CStrings::strTrimDef( $strAppRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getAppRemotePrimaryKey() {
		return $this->m_strAppRemotePrimaryKey;
	}

	public function sqlAppRemotePrimaryKey() {
		return ( true == isset( $this->m_strAppRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAppRemotePrimaryKey ) : '\'' . addslashes( $this->m_strAppRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setRenewalRemotePrimaryKey( $strRenewalRemotePrimaryKey ) {
		$this->set( 'm_strRenewalRemotePrimaryKey', CStrings::strTrimDef( $strRenewalRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRenewalRemotePrimaryKey() {
		return $this->m_strRenewalRemotePrimaryKey;
	}

	public function sqlRenewalRemotePrimaryKey() {
		return ( true == isset( $this->m_strRenewalRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRenewalRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRenewalRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 1000, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 1000, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 1000, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 1000, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMobileNumber ) : '\'' . addslashes( $this->m_strMobileNumber ) . '\'' ) : 'NULL';
	}

	public function setWorkNumber( $strWorkNumber ) {
		$this->set( 'm_strWorkNumber', CStrings::strTrimDef( $strWorkNumber, 30, NULL, true ) );
	}

	public function getWorkNumber() {
		return $this->m_strWorkNumber;
	}

	public function sqlWorkNumber() {
		return ( true == isset( $this->m_strWorkNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorkNumber ) : '\'' . addslashes( $this->m_strWorkNumber ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->set( 'm_strTaxNumberMasked', CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true ) );
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function sqlTaxNumberMasked() {
		return ( true == isset( $this->m_strTaxNumberMasked ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberMasked ) : '\'' . addslashes( $this->m_strTaxNumberMasked ) . '\'' ) : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function setWeight( $intWeight ) {
		$this->set( 'm_intWeight', CStrings::strToIntDef( $intWeight, NULL, false ) );
	}

	public function getWeight() {
		return $this->m_intWeight;
	}

	public function sqlWeight() {
		return ( true == isset( $this->m_intWeight ) ) ? ( string ) $this->m_intWeight : 'NULL';
	}

	public function setOccupationId( $intOccupationId ) {
		$this->set( 'm_intOccupationId', CStrings::strToIntDef( $intOccupationId, NULL, false ) );
	}

	public function getOccupationId() {
		return $this->m_intOccupationId;
	}

	public function sqlOccupationId() {
		return ( true == isset( $this->m_intOccupationId ) ) ? ( string ) $this->m_intOccupationId : 'NULL';
	}

	public function setEyeColor( $strEyeColor ) {
		$this->set( 'm_strEyeColor', CStrings::strTrimDef( $strEyeColor, 20, NULL, true ) );
	}

	public function getEyeColor() {
		return $this->m_strEyeColor;
	}

	public function sqlEyeColor() {
		return ( true == isset( $this->m_strEyeColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEyeColor ) : '\'' . addslashes( $this->m_strEyeColor ) . '\'' ) : 'NULL';
	}

	public function setHairColor( $strHairColor ) {
		$this->set( 'm_strHairColor', CStrings::strTrimDef( $strHairColor, 20, NULL, true ) );
	}

	public function getHairColor() {
		return $this->m_strHairColor;
	}

	public function sqlHairColor() {
		return ( true == isset( $this->m_strHairColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHairColor ) : '\'' . addslashes( $this->m_strHairColor ) . '\'' ) : 'NULL';
	}

	public function setDlNumberEncrypted( $strDlNumberEncrypted ) {
		$this->set( 'm_strDlNumberEncrypted', CStrings::strTrimDef( $strDlNumberEncrypted, 240, NULL, true ) );
	}

	public function getDlNumberEncrypted() {
		return $this->m_strDlNumberEncrypted;
	}

	public function sqlDlNumberEncrypted() {
		return ( true == isset( $this->m_strDlNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlNumberEncrypted ) : '\'' . addslashes( $this->m_strDlNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDlStateCode( $strDlStateCode ) {
		$this->set( 'm_strDlStateCode', CStrings::strTrimDef( $strDlStateCode, 250, NULL, true ) );
	}

	public function getDlStateCode() {
		return $this->m_strDlStateCode;
	}

	public function sqlDlStateCode() {
		return ( true == isset( $this->m_strDlStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDlStateCode ) : '\'' . addslashes( $this->m_strDlStateCode ) . '\'' ) : 'NULL';
	}

	public function setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId ) {
		$this->set( 'm_intCompanyIdentificationTypeId', CStrings::strToIntDef( $intCompanyIdentificationTypeId, NULL, false ) );
	}

	public function getCompanyIdentificationTypeId() {
		return $this->m_intCompanyIdentificationTypeId;
	}

	public function sqlCompanyIdentificationTypeId() {
		return ( true == isset( $this->m_intCompanyIdentificationTypeId ) ) ? ( string ) $this->m_intCompanyIdentificationTypeId : 'NULL';
	}

	public function setIdentificationValue( $strIdentificationValue ) {
		$this->set( 'm_strIdentificationValue', CStrings::strTrimDef( $strIdentificationValue, 240, NULL, true ) );
	}

	public function getIdentificationValue() {
		return $this->m_strIdentificationValue;
	}

	public function sqlIdentificationValue() {
		return ( true == isset( $this->m_strIdentificationValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIdentificationValue ) : '\'' . addslashes( $this->m_strIdentificationValue ) . '\'' ) : 'NULL';
	}

	public function setIdentificationExpiration( $strIdentificationExpiration ) {
		$this->set( 'm_strIdentificationExpiration', CStrings::strTrimDef( $strIdentificationExpiration, -1, NULL, true ) );
	}

	public function getIdentificationExpiration() {
		return $this->m_strIdentificationExpiration;
	}

	public function sqlIdentificationExpiration() {
		return ( true == isset( $this->m_strIdentificationExpiration ) ) ? '\'' . $this->m_strIdentificationExpiration . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	public function setFacebookUser( $strFacebookUser ) {
		$this->set( 'm_strFacebookUser', CStrings::strTrimDef( $strFacebookUser, 240, NULL, true ) );
	}

	public function getFacebookUser() {
		return $this->m_strFacebookUser;
	}

	public function sqlFacebookUser() {
		return ( true == isset( $this->m_strFacebookUser ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFacebookUser ) : '\'' . addslashes( $this->m_strFacebookUser ) . '\'' ) : 'NULL';
	}

	public function setLastAccessOn( $strLastAccessOn ) {
		$this->set( 'm_strLastAccessOn', CStrings::strTrimDef( $strLastAccessOn, -1, NULL, true ) );
	}

	public function getLastAccessOn() {
		return $this->m_strLastAccessOn;
	}

	public function sqlLastAccessOn() {
		return ( true == isset( $this->m_strLastAccessOn ) ) ? '\'' . $this->m_strLastAccessOn . '\'' : 'NULL';
	}

	public function setMaxLoginAttempts( $intMaxLoginAttempts ) {
		$this->set( 'm_intMaxLoginAttempts', CStrings::strToIntDef( $intMaxLoginAttempts, NULL, false ) );
	}

	public function getMaxLoginAttempts() {
		return $this->m_intMaxLoginAttempts;
	}

	public function sqlMaxLoginAttempts() {
		return ( true == isset( $this->m_intMaxLoginAttempts ) ) ? ( string ) $this->m_intMaxLoginAttempts : '50';
	}

	public function setLoginErrorCount( $intLoginErrorCount ) {
		$this->set( 'm_intLoginErrorCount', CStrings::strToIntDef( $intLoginErrorCount, NULL, false ) );
	}

	public function getLoginErrorCount() {
		return $this->m_intLoginErrorCount;
	}

	public function sqlLoginErrorCount() {
		return ( true == isset( $this->m_intLoginErrorCount ) ) ? ( string ) $this->m_intLoginErrorCount : '0';
	}

	public function setRequiresPasswordChange( $intRequiresPasswordChange ) {
		$this->set( 'm_intRequiresPasswordChange', CStrings::strToIntDef( $intRequiresPasswordChange, NULL, false ) );
	}

	public function getRequiresPasswordChange() {
		return $this->m_intRequiresPasswordChange;
	}

	public function sqlRequiresPasswordChange() {
		return ( true == isset( $this->m_intRequiresPasswordChange ) ) ? ( string ) $this->m_intRequiresPasswordChange : '0';
	}

	public function setRelocationReason( $strRelocationReason ) {
		$this->set( 'm_strRelocationReason', CStrings::strTrimDef( $strRelocationReason, 2000, NULL, true ) );
	}

	public function getRelocationReason() {
		return $this->m_strRelocationReason;
	}

	public function sqlRelocationReason() {
		return ( true == isset( $this->m_strRelocationReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRelocationReason ) : '\'' . addslashes( $this->m_strRelocationReason ) . '\'' ) : 'NULL';
	}

	public function setHasFelonyConviction( $intHasFelonyConviction ) {
		$this->set( 'm_intHasFelonyConviction', CStrings::strToIntDef( $intHasFelonyConviction, NULL, false ) );
	}

	public function getHasFelonyConviction() {
		return $this->m_intHasFelonyConviction;
	}

	public function sqlHasFelonyConviction() {
		return ( true == isset( $this->m_intHasFelonyConviction ) ) ? ( string ) $this->m_intHasFelonyConviction : 'NULL';
	}

	public function setFelonyIsResolved( $intFelonyIsResolved ) {
		$this->set( 'm_intFelonyIsResolved', CStrings::strToIntDef( $intFelonyIsResolved, NULL, false ) );
	}

	public function getFelonyIsResolved() {
		return $this->m_intFelonyIsResolved;
	}

	public function sqlFelonyIsResolved() {
		return ( true == isset( $this->m_intFelonyIsResolved ) ) ? ( string ) $this->m_intFelonyIsResolved : 'NULL';
	}

	public function setFelonyComments( $strFelonyComments ) {
		$this->set( 'm_strFelonyComments', CStrings::strTrimDef( $strFelonyComments, 2000, NULL, true ) );
	}

	public function getFelonyComments() {
		return $this->m_strFelonyComments;
	}

	public function sqlFelonyComments() {
		return ( true == isset( $this->m_strFelonyComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFelonyComments ) : '\'' . addslashes( $this->m_strFelonyComments ) . '\'' ) : 'NULL';
	}

	public function setFelonyYear( $intFelonyYear ) {
		$this->set( 'm_intFelonyYear', CStrings::strToIntDef( $intFelonyYear, NULL, false ) );
	}

	public function getFelonyYear() {
		return $this->m_intFelonyYear;
	}

	public function sqlFelonyYear() {
		return ( true == isset( $this->m_intFelonyYear ) ) ? ( string ) $this->m_intFelonyYear : 'NULL';
	}

	public function setSmokes( $intSmokes ) {
		$this->set( 'm_intSmokes', CStrings::strToIntDef( $intSmokes, NULL, false ) );
	}

	public function getSmokes() {
		return $this->m_intSmokes;
	}

	public function sqlSmokes() {
		return ( true == isset( $this->m_intSmokes ) ) ? ( string ) $this->m_intSmokes : 'NULL';
	}

	public function setHasPets( $intHasPets ) {
		$this->set( 'm_intHasPets', CStrings::strToIntDef( $intHasPets, NULL, false ) );
	}

	public function getHasPets() {
		return $this->m_intHasPets;
	}

	public function sqlHasPets() {
		return ( true == isset( $this->m_intHasPets ) ) ? ( string ) $this->m_intHasPets : 'NULL';
	}

	public function setPetDescription( $strPetDescription ) {
		$this->set( 'm_strPetDescription', CStrings::strTrimDef( $strPetDescription, 240, NULL, true ) );
	}

	public function getPetDescription() {
		return $this->m_strPetDescription;
	}

	public function sqlPetDescription() {
		return ( true == isset( $this->m_strPetDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPetDescription ) : '\'' . addslashes( $this->m_strPetDescription ) . '\'' ) : 'NULL';
	}

	public function setHasBeenEvicted( $intHasBeenEvicted ) {
		$this->set( 'm_intHasBeenEvicted', CStrings::strToIntDef( $intHasBeenEvicted, NULL, false ) );
	}

	public function getHasBeenEvicted() {
		return $this->m_intHasBeenEvicted;
	}

	public function sqlHasBeenEvicted() {
		return ( true == isset( $this->m_intHasBeenEvicted ) ) ? ( string ) $this->m_intHasBeenEvicted : 'NULL';
	}

	public function setHasBrokenLease( $intHasBrokenLease ) {
		$this->set( 'm_intHasBrokenLease', CStrings::strToIntDef( $intHasBrokenLease, NULL, false ) );
	}

	public function getHasBrokenLease() {
		return $this->m_intHasBrokenLease;
	}

	public function sqlHasBrokenLease() {
		return ( true == isset( $this->m_intHasBrokenLease ) ) ? ( string ) $this->m_intHasBrokenLease : 'NULL';
	}

	public function setHasBankruptcy( $intHasBankruptcy ) {
		$this->set( 'm_intHasBankruptcy', CStrings::strToIntDef( $intHasBankruptcy, NULL, false ) );
	}

	public function getHasBankruptcy() {
		return $this->m_intHasBankruptcy;
	}

	public function sqlHasBankruptcy() {
		return ( true == isset( $this->m_intHasBankruptcy ) ) ? ( string ) $this->m_intHasBankruptcy : 'NULL';
	}

	public function setHasPreviousRentOwed( $intHasPreviousRentOwed ) {
		$this->set( 'm_intHasPreviousRentOwed', CStrings::strToIntDef( $intHasPreviousRentOwed, NULL, false ) );
	}

	public function getHasPreviousRentOwed() {
		return $this->m_intHasPreviousRentOwed;
	}

	public function sqlHasPreviousRentOwed() {
		return ( true == isset( $this->m_intHasPreviousRentOwed ) ) ? ( string ) $this->m_intHasPreviousRentOwed : 'NULL';
	}

	public function setHasMiddleName( $intHasMiddleName ) {
		$this->set( 'm_intHasMiddleName', CStrings::strToIntDef( $intHasMiddleName, NULL, false ) );
	}

	public function getHasMiddleName() {
		return $this->m_intHasMiddleName;
	}

	public function sqlHasMiddleName() {
		return ( true == isset( $this->m_intHasMiddleName ) ) ? ( string ) $this->m_intHasMiddleName : 'NULL';
	}

	public function setIsReturningResident( $intIsReturningResident ) {
		$this->set( 'm_intIsReturningResident', CStrings::strToIntDef( $intIsReturningResident, NULL, false ) );
	}

	public function getIsReturningResident() {
		return $this->m_intIsReturningResident;
	}

	public function sqlIsReturningResident() {
		return ( true == isset( $this->m_intIsReturningResident ) ) ? ( string ) $this->m_intIsReturningResident : 'NULL';
	}

	public function setIsUnemployed( $intIsUnemployed ) {
		$this->set( 'm_intIsUnemployed', CStrings::strToIntDef( $intIsUnemployed, NULL, false ) );
	}

	public function getIsUnemployed() {
		return $this->m_intIsUnemployed;
	}

	public function sqlIsUnemployed() {
		return ( true == isset( $this->m_intIsUnemployed ) ) ? ( string ) $this->m_intIsUnemployed : 'NULL';
	}

	public function setIsLegalUsResident( $intIsLegalUsResident ) {
		$this->set( 'm_intIsLegalUsResident', CStrings::strToIntDef( $intIsLegalUsResident, NULL, false ) );
	}

	public function getIsLegalUsResident() {
		return $this->m_intIsLegalUsResident;
	}

	public function sqlIsLegalUsResident() {
		return ( true == isset( $this->m_intIsLegalUsResident ) ) ? ( string ) $this->m_intIsLegalUsResident : 'NULL';
	}

	public function setBankruptcyCounty( $strBankruptcyCounty ) {
		$this->set( 'm_strBankruptcyCounty', CStrings::strTrimDef( $strBankruptcyCounty, 50, NULL, true ) );
	}

	public function getBankruptcyCounty() {
		return $this->m_strBankruptcyCounty;
	}

	public function sqlBankruptcyCounty() {
		return ( true == isset( $this->m_strBankruptcyCounty ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankruptcyCounty ) : '\'' . addslashes( $this->m_strBankruptcyCounty ) . '\'' ) : 'NULL';
	}

	public function setBankruptcyStateCode( $strBankruptcyStateCode ) {
		$this->set( 'm_strBankruptcyStateCode', CStrings::strTrimDef( $strBankruptcyStateCode, 2, NULL, true ) );
	}

	public function getBankruptcyStateCode() {
		return $this->m_strBankruptcyStateCode;
	}

	public function sqlBankruptcyStateCode() {
		return ( true == isset( $this->m_strBankruptcyStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankruptcyStateCode ) : '\'' . addslashes( $this->m_strBankruptcyStateCode ) . '\'' ) : 'NULL';
	}

	public function setBankruptcyYear( $intBankruptcyYear ) {
		$this->set( 'm_intBankruptcyYear', CStrings::strToIntDef( $intBankruptcyYear, NULL, false ) );
	}

	public function getBankruptcyYear() {
		return $this->m_intBankruptcyYear;
	}

	public function sqlBankruptcyYear() {
		return ( true == isset( $this->m_intBankruptcyYear ) ) ? ( string ) $this->m_intBankruptcyYear : 'NULL';
	}

	public function setHasPublicJudgment( $intHasPublicJudgment ) {
		$this->set( 'm_intHasPublicJudgment', CStrings::strToIntDef( $intHasPublicJudgment, NULL, false ) );
	}

	public function getHasPublicJudgment() {
		return $this->m_intHasPublicJudgment;
	}

	public function sqlHasPublicJudgment() {
		return ( true == isset( $this->m_intHasPublicJudgment ) ) ? ( string ) $this->m_intHasPublicJudgment : 'NULL';
	}

	public function setPublicJudgmentYear( $intPublicJudgmentYear ) {
		$this->set( 'm_intPublicJudgmentYear', CStrings::strToIntDef( $intPublicJudgmentYear, NULL, false ) );
	}

	public function getPublicJudgmentYear() {
		return $this->m_intPublicJudgmentYear;
	}

	public function sqlPublicJudgmentYear() {
		return ( true == isset( $this->m_intPublicJudgmentYear ) ) ? ( string ) $this->m_intPublicJudgmentYear : 'NULL';
	}

	public function setWasSuedForRent( $intWasSuedForRent ) {
		$this->set( 'm_intWasSuedForRent', CStrings::strToIntDef( $intWasSuedForRent, NULL, false ) );
	}

	public function getWasSuedForRent() {
		return $this->m_intWasSuedForRent;
	}

	public function sqlWasSuedForRent() {
		return ( true == isset( $this->m_intWasSuedForRent ) ) ? ( string ) $this->m_intWasSuedForRent : 'NULL';
	}

	public function setWasSuedForDamages( $intWasSuedForDamages ) {
		$this->set( 'm_intWasSuedForDamages', CStrings::strToIntDef( $intWasSuedForDamages, NULL, false ) );
	}

	public function getWasSuedForDamages() {
		return $this->m_intWasSuedForDamages;
	}

	public function sqlWasSuedForDamages() {
		return ( true == isset( $this->m_intWasSuedForDamages ) ) ? ( string ) $this->m_intWasSuedForDamages : 'NULL';
	}

	public function setSuedForRentComments( $strSuedForRentComments ) {
		$this->set( 'm_strSuedForRentComments', CStrings::strTrimDef( $strSuedForRentComments, 240, NULL, true ) );
	}

	public function getSuedForRentComments() {
		return $this->m_strSuedForRentComments;
	}

	public function sqlSuedForRentComments() {
		return ( true == isset( $this->m_strSuedForRentComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSuedForRentComments ) : '\'' . addslashes( $this->m_strSuedForRentComments ) . '\'' ) : 'NULL';
	}

	public function setSuedForDamageComments( $strSuedForDamageComments ) {
		$this->set( 'm_strSuedForDamageComments', CStrings::strTrimDef( $strSuedForDamageComments, 240, NULL, true ) );
	}

	public function getSuedForDamageComments() {
		return $this->m_strSuedForDamageComments;
	}

	public function sqlSuedForDamageComments() {
		return ( true == isset( $this->m_strSuedForDamageComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSuedForDamageComments ) : '\'' . addslashes( $this->m_strSuedForDamageComments ) . '\'' ) : 'NULL';
	}

	public function setEvictedComments( $strEvictedComments ) {
		$this->set( 'm_strEvictedComments', CStrings::strTrimDef( $strEvictedComments, 240, NULL, true ) );
	}

	public function getEvictedComments() {
		return $this->m_strEvictedComments;
	}

	public function sqlEvictedComments() {
		return ( true == isset( $this->m_strEvictedComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEvictedComments ) : '\'' . addslashes( $this->m_strEvictedComments ) . '\'' ) : 'NULL';
	}

	public function setBrokenLeaseComments( $strBrokenLeaseComments ) {
		$this->set( 'm_strBrokenLeaseComments', CStrings::strTrimDef( $strBrokenLeaseComments, 240, NULL, true ) );
	}

	public function getBrokenLeaseComments() {
		return $this->m_strBrokenLeaseComments;
	}

	public function sqlBrokenLeaseComments() {
		return ( true == isset( $this->m_strBrokenLeaseComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBrokenLeaseComments ) : '\'' . addslashes( $this->m_strBrokenLeaseComments ) . '\'' ) : 'NULL';
	}

	public function setFelonyConvictionComments( $strFelonyConvictionComments ) {
		$this->set( 'm_strFelonyConvictionComments', CStrings::strTrimDef( $strFelonyConvictionComments, 240, NULL, true ) );
	}

	public function getFelonyConvictionComments() {
		return $this->m_strFelonyConvictionComments;
	}

	public function sqlFelonyConvictionComments() {
		return ( true == isset( $this->m_strFelonyConvictionComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFelonyConvictionComments ) : '\'' . addslashes( $this->m_strFelonyConvictionComments ) . '\'' ) : 'NULL';
	}

	public function setPreviousRentOwedComments( $strPreviousRentOwedComments ) {
		$this->set( 'm_strPreviousRentOwedComments', CStrings::strTrimDef( $strPreviousRentOwedComments, 240, NULL, true ) );
	}

	public function getPreviousRentOwedComments() {
		return $this->m_strPreviousRentOwedComments;
	}

	public function sqlPreviousRentOwedComments() {
		return ( true == isset( $this->m_strPreviousRentOwedComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousRentOwedComments ) : '\'' . addslashes( $this->m_strPreviousRentOwedComments ) . '\'' ) : 'NULL';
	}

	public function setBankruptcyComments( $strBankruptcyComments ) {
		$this->set( 'm_strBankruptcyComments', CStrings::strTrimDef( $strBankruptcyComments, 240, NULL, true ) );
	}

	public function getBankruptcyComments() {
		return $this->m_strBankruptcyComments;
	}

	public function sqlBankruptcyComments() {
		return ( true == isset( $this->m_strBankruptcyComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankruptcyComments ) : '\'' . addslashes( $this->m_strBankruptcyComments ) . '\'' ) : 'NULL';
	}

	public function setReturningResidentComments( $strReturningResidentComments ) {
		$this->set( 'm_strReturningResidentComments', CStrings::strTrimDef( $strReturningResidentComments, 240, NULL, true ) );
	}

	public function getReturningResidentComments() {
		return $this->m_strReturningResidentComments;
	}

	public function sqlReturningResidentComments() {
		return ( true == isset( $this->m_strReturningResidentComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturningResidentComments ) : '\'' . addslashes( $this->m_strReturningResidentComments ) . '\'' ) : 'NULL';
	}

	public function setUnemployedComments( $strUnemployedComments ) {
		$this->set( 'm_strUnemployedComments', CStrings::strTrimDef( $strUnemployedComments, 240, NULL, true ) );
	}

	public function getUnemployedComments() {
		return $this->m_strUnemployedComments;
	}

	public function sqlUnemployedComments() {
		return ( true == isset( $this->m_strUnemployedComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnemployedComments ) : '\'' . addslashes( $this->m_strUnemployedComments ) . '\'' ) : 'NULL';
	}

	public function setReasonInUsComments( $strReasonInUsComments ) {
		$this->set( 'm_strReasonInUsComments', CStrings::strTrimDef( $strReasonInUsComments, 240, NULL, true ) );
	}

	public function getReasonInUsComments() {
		return $this->m_strReasonInUsComments;
	}

	public function sqlReasonInUsComments() {
		return ( true == isset( $this->m_strReasonInUsComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonInUsComments ) : '\'' . addslashes( $this->m_strReasonInUsComments ) . '\'' ) : 'NULL';
	}

	public function setGaaScreeningInitials( $strGaaScreeningInitials ) {
		$this->set( 'm_strGaaScreeningInitials', CStrings::strTrimDef( $strGaaScreeningInitials, 240, NULL, true ) );
	}

	public function getGaaScreeningInitials() {
		return $this->m_strGaaScreeningInitials;
	}

	public function sqlGaaScreeningInitials() {
		return ( true == isset( $this->m_strGaaScreeningInitials ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGaaScreeningInitials ) : '\'' . addslashes( $this->m_strGaaScreeningInitials ) . '\'' ) : 'NULL';
	}

	public function setRelationshipToApplicant( $strRelationshipToApplicant ) {
		$this->set( 'm_strRelationshipToApplicant', CStrings::strTrimDef( $strRelationshipToApplicant, 50, NULL, true ) );
	}

	public function getRelationshipToApplicant() {
		return $this->m_strRelationshipToApplicant;
	}

	public function sqlRelationshipToApplicant() {
		return ( true == isset( $this->m_strRelationshipToApplicant ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRelationshipToApplicant ) : '\'' . addslashes( $this->m_strRelationshipToApplicant ) . '\'' ) : 'NULL';
	}

	public function setHasRentersInsurance( $intHasRentersInsurance ) {
		$this->set( 'm_intHasRentersInsurance', CStrings::strToIntDef( $intHasRentersInsurance, NULL, false ) );
	}

	public function getHasRentersInsurance() {
		return $this->m_intHasRentersInsurance;
	}

	public function sqlHasRentersInsurance() {
		return ( true == isset( $this->m_intHasRentersInsurance ) ) ? ( string ) $this->m_intHasRentersInsurance : 'NULL';
	}

	public function setInsuranceProviderName( $strInsuranceProviderName ) {
		$this->set( 'm_strInsuranceProviderName', CStrings::strTrimDef( $strInsuranceProviderName, 50, NULL, true ) );
	}

	public function getInsuranceProviderName() {
		return $this->m_strInsuranceProviderName;
	}

	public function sqlInsuranceProviderName() {
		return ( true == isset( $this->m_strInsuranceProviderName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsuranceProviderName ) : '\'' . addslashes( $this->m_strInsuranceProviderName ) . '\'' ) : 'NULL';
	}

	public function setInsurancePolicyNumber( $strInsurancePolicyNumber ) {
		$this->set( 'm_strInsurancePolicyNumber', CStrings::strTrimDef( $strInsurancePolicyNumber, 50, NULL, true ) );
	}

	public function getInsurancePolicyNumber() {
		return $this->m_strInsurancePolicyNumber;
	}

	public function sqlInsurancePolicyNumber() {
		return ( true == isset( $this->m_strInsurancePolicyNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsurancePolicyNumber ) : '\'' . addslashes( $this->m_strInsurancePolicyNumber ) . '\'' ) : 'NULL';
	}

	public function setReferenceComments( $strReferenceComments ) {
		$this->set( 'm_strReferenceComments', CStrings::strTrimDef( $strReferenceComments, -1, NULL, true ) );
	}

	public function getReferenceComments() {
		return $this->m_strReferenceComments;
	}

	public function sqlReferenceComments() {
		return ( true == isset( $this->m_strReferenceComments ) ) ? '\'' . $this->m_strReferenceComments . '\'' : 'NULL';
	}

	public function setIsAlien( $intIsAlien ) {
		$this->set( 'm_intIsAlien', CStrings::strToIntDef( $intIsAlien, NULL, false ) );
	}

	public function getIsAlien() {
		return $this->m_intIsAlien;
	}

	public function sqlIsAlien() {
		return ( true == isset( $this->m_intIsAlien ) ) ? ( string ) $this->m_intIsAlien : 'NULL';
	}

	public function setReferencesApprovedBy( $intReferencesApprovedBy ) {
		$this->set( 'm_intReferencesApprovedBy', CStrings::strToIntDef( $intReferencesApprovedBy, NULL, false ) );
	}

	public function getReferencesApprovedBy() {
		return $this->m_intReferencesApprovedBy;
	}

	public function sqlReferencesApprovedBy() {
		return ( true == isset( $this->m_intReferencesApprovedBy ) ) ? ( string ) $this->m_intReferencesApprovedBy : 'NULL';
	}

	public function setReferencesApprovedOn( $strReferencesApprovedOn ) {
		$this->set( 'm_strReferencesApprovedOn', CStrings::strTrimDef( $strReferencesApprovedOn, -1, NULL, true ) );
	}

	public function getReferencesApprovedOn() {
		return $this->m_strReferencesApprovedOn;
	}

	public function sqlReferencesApprovedOn() {
		return ( true == isset( $this->m_strReferencesApprovedOn ) ) ? '\'' . $this->m_strReferencesApprovedOn . '\'' : 'NULL';
	}

	public function setCurrentManagerNameFirst( $strCurrentManagerNameFirst ) {
		$this->set( 'm_strCurrentManagerNameFirst', CStrings::strTrimDef( $strCurrentManagerNameFirst, 1000, NULL, true ) );
	}

	public function getCurrentManagerNameFirst() {
		return $this->m_strCurrentManagerNameFirst;
	}

	public function sqlCurrentManagerNameFirst() {
		return ( true == isset( $this->m_strCurrentManagerNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrentManagerNameFirst ) : '\'' . addslashes( $this->m_strCurrentManagerNameFirst ) . '\'' ) : 'NULL';
	}

	public function setCurrentManagerNameLast( $strCurrentManagerNameLast ) {
		$this->set( 'm_strCurrentManagerNameLast', CStrings::strTrimDef( $strCurrentManagerNameLast, 1000, NULL, true ) );
	}

	public function getCurrentManagerNameLast() {
		return $this->m_strCurrentManagerNameLast;
	}

	public function sqlCurrentManagerNameLast() {
		return ( true == isset( $this->m_strCurrentManagerNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrentManagerNameLast ) : '\'' . addslashes( $this->m_strCurrentManagerNameLast ) . '\'' ) : 'NULL';
	}

	public function setCurrentPhoneNumber( $strCurrentPhoneNumber ) {
		$this->set( 'm_strCurrentPhoneNumber', CStrings::strTrimDef( $strCurrentPhoneNumber, 30, NULL, true ) );
	}

	public function getCurrentPhoneNumber() {
		return $this->m_strCurrentPhoneNumber;
	}

	public function sqlCurrentPhoneNumber() {
		return ( true == isset( $this->m_strCurrentPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrentPhoneNumber ) : '\'' . addslashes( $this->m_strCurrentPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setCurrentFaxNumber( $strCurrentFaxNumber ) {
		$this->set( 'm_strCurrentFaxNumber', CStrings::strTrimDef( $strCurrentFaxNumber, 30, NULL, true ) );
	}

	public function getCurrentFaxNumber() {
		return $this->m_strCurrentFaxNumber;
	}

	public function sqlCurrentFaxNumber() {
		return ( true == isset( $this->m_strCurrentFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrentFaxNumber ) : '\'' . addslashes( $this->m_strCurrentFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setCurrentEmailAddress( $strCurrentEmailAddress ) {
		$this->set( 'm_strCurrentEmailAddress', CStrings::strTrimDef( $strCurrentEmailAddress, 240, NULL, true ) );
	}

	public function getCurrentEmailAddress() {
		return $this->m_strCurrentEmailAddress;
	}

	public function sqlCurrentEmailAddress() {
		return ( true == isset( $this->m_strCurrentEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrentEmailAddress ) : '\'' . addslashes( $this->m_strCurrentEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPreviousManagerNameFirst( $strPreviousManagerNameFirst ) {
		$this->set( 'm_strPreviousManagerNameFirst', CStrings::strTrimDef( $strPreviousManagerNameFirst, 1000, NULL, true ) );
	}

	public function getPreviousManagerNameFirst() {
		return $this->m_strPreviousManagerNameFirst;
	}

	public function sqlPreviousManagerNameFirst() {
		return ( true == isset( $this->m_strPreviousManagerNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousManagerNameFirst ) : '\'' . addslashes( $this->m_strPreviousManagerNameFirst ) . '\'' ) : 'NULL';
	}

	public function setPreviousManagerNameLast( $strPreviousManagerNameLast ) {
		$this->set( 'm_strPreviousManagerNameLast', CStrings::strTrimDef( $strPreviousManagerNameLast, 1000, NULL, true ) );
	}

	public function getPreviousManagerNameLast() {
		return $this->m_strPreviousManagerNameLast;
	}

	public function sqlPreviousManagerNameLast() {
		return ( true == isset( $this->m_strPreviousManagerNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousManagerNameLast ) : '\'' . addslashes( $this->m_strPreviousManagerNameLast ) . '\'' ) : 'NULL';
	}

	public function setPreviousPhoneNumber( $strPreviousPhoneNumber ) {
		$this->set( 'm_strPreviousPhoneNumber', CStrings::strTrimDef( $strPreviousPhoneNumber, 30, NULL, true ) );
	}

	public function getPreviousPhoneNumber() {
		return $this->m_strPreviousPhoneNumber;
	}

	public function sqlPreviousPhoneNumber() {
		return ( true == isset( $this->m_strPreviousPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousPhoneNumber ) : '\'' . addslashes( $this->m_strPreviousPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPreviousFaxNumber( $strPreviousFaxNumber ) {
		$this->set( 'm_strPreviousFaxNumber', CStrings::strTrimDef( $strPreviousFaxNumber, 30, NULL, true ) );
	}

	public function getPreviousFaxNumber() {
		return $this->m_strPreviousFaxNumber;
	}

	public function sqlPreviousFaxNumber() {
		return ( true == isset( $this->m_strPreviousFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousFaxNumber ) : '\'' . addslashes( $this->m_strPreviousFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setPreviousEmailAddress( $strPreviousEmailAddress ) {
		$this->set( 'm_strPreviousEmailAddress', CStrings::strTrimDef( $strPreviousEmailAddress, 240, NULL, true ) );
	}

	public function getPreviousEmailAddress() {
		return $this->m_strPreviousEmailAddress;
	}

	public function sqlPreviousEmailAddress() {
		return ( true == isset( $this->m_strPreviousEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreviousEmailAddress ) : '\'' . addslashes( $this->m_strPreviousEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setParentNameFirst( $strParentNameFirst ) {
		$this->set( 'm_strParentNameFirst', CStrings::strTrimDef( $strParentNameFirst, 50, NULL, true ) );
	}

	public function getParentNameFirst() {
		return $this->m_strParentNameFirst;
	}

	public function sqlParentNameFirst() {
		return ( true == isset( $this->m_strParentNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentNameFirst ) : '\'' . addslashes( $this->m_strParentNameFirst ) . '\'' ) : 'NULL';
	}

	public function setParentNameLast( $strParentNameLast ) {
		$this->set( 'm_strParentNameLast', CStrings::strTrimDef( $strParentNameLast, 50, NULL, true ) );
	}

	public function getParentNameLast() {
		return $this->m_strParentNameLast;
	}

	public function sqlParentNameLast() {
		return ( true == isset( $this->m_strParentNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentNameLast ) : '\'' . addslashes( $this->m_strParentNameLast ) . '\'' ) : 'NULL';
	}

	public function setParentEmailAddress( $strParentEmailAddress ) {
		$this->set( 'm_strParentEmailAddress', CStrings::strTrimDef( $strParentEmailAddress, 240, NULL, true ) );
	}

	public function getParentEmailAddress() {
		return $this->m_strParentEmailAddress;
	}

	public function sqlParentEmailAddress() {
		return ( true == isset( $this->m_strParentEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentEmailAddress ) : '\'' . addslashes( $this->m_strParentEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setSmsActivatedOn( $strSmsActivatedOn ) {
		$this->set( 'm_strSmsActivatedOn', CStrings::strTrimDef( $strSmsActivatedOn, -1, NULL, true ) );
	}

	public function getSmsActivatedOn() {
		return $this->m_strSmsActivatedOn;
	}

	public function sqlSmsActivatedOn() {
		return ( true == isset( $this->m_strSmsActivatedOn ) ) ? '\'' . $this->m_strSmsActivatedOn . '\'' : 'NULL';
	}

	public function setSmsCancelledOn( $strSmsCancelledOn ) {
		$this->set( 'm_strSmsCancelledOn', CStrings::strTrimDef( $strSmsCancelledOn, -1, NULL, true ) );
	}

	public function getSmsCancelledOn() {
		return $this->m_strSmsCancelledOn;
	}

	public function sqlSmsCancelledOn() {
		return ( true == isset( $this->m_strSmsCancelledOn ) ) ? '\'' . $this->m_strSmsCancelledOn . '\'' : 'NULL';
	}

	public function setSmsApplicationUpdatesActivatedOn( $strSmsApplicationUpdatesActivatedOn ) {
		$this->set( 'm_strSmsApplicationUpdatesActivatedOn', CStrings::strTrimDef( $strSmsApplicationUpdatesActivatedOn, -1, NULL, true ) );
	}

	public function getSmsApplicationUpdatesActivatedOn() {
		return $this->m_strSmsApplicationUpdatesActivatedOn;
	}

	public function sqlSmsApplicationUpdatesActivatedOn() {
		return ( true == isset( $this->m_strSmsApplicationUpdatesActivatedOn ) ) ? '\'' . $this->m_strSmsApplicationUpdatesActivatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAltNameFirst( $strAltNameFirst ) {
		$this->set( 'm_strAltNameFirst', CStrings::strTrimDef( $strAltNameFirst, 1000, NULL, true ) );
	}

	public function getAltNameFirst() {
		return $this->m_strAltNameFirst;
	}

	public function sqlAltNameFirst() {
		return ( true == isset( $this->m_strAltNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameFirst ) : '\'' . addslashes( $this->m_strAltNameFirst ) . '\'' ) : 'NULL';
	}

	public function setAltNameMiddle( $strAltNameMiddle ) {
		$this->set( 'm_strAltNameMiddle', CStrings::strTrimDef( $strAltNameMiddle, 1000, NULL, true ) );
	}

	public function getAltNameMiddle() {
		return $this->m_strAltNameMiddle;
	}

	public function sqlAltNameMiddle() {
		return ( true == isset( $this->m_strAltNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameMiddle ) : '\'' . addslashes( $this->m_strAltNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setAltNameLast( $strAltNameLast ) {
		$this->set( 'm_strAltNameLast', CStrings::strTrimDef( $strAltNameLast, 1000, NULL, true ) );
	}

	public function getAltNameLast() {
		return $this->m_strAltNameLast;
	}

	public function sqlAltNameLast() {
		return ( true == isset( $this->m_strAltNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAltNameLast ) : '\'' . addslashes( $this->m_strAltNameLast ) . '\'' ) : 'NULL';
	}

	public function setHasAltMiddleName( $intHasAltMiddleName ) {
		$this->set( 'm_intHasAltMiddleName', CStrings::strToIntDef( $intHasAltMiddleName, NULL, false ) );
	}

	public function getHasAltMiddleName() {
		return $this->m_intHasAltMiddleName;
	}

	public function sqlHasAltMiddleName() {
		return ( true == isset( $this->m_intHasAltMiddleName ) ) ? ( string ) $this->m_intHasAltMiddleName : 'NULL';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 1000, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->set( 'm_strNameLastMatronymic', CStrings::strTrimDef( $strNameLastMatronymic, 1000, NULL, true ) );
	}

	public function getNameLastMatronymic() {
		return $this->m_strNameLastMatronymic;
	}

	public function sqlNameLastMatronymic() {
		return ( true == isset( $this->m_strNameLastMatronymic ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLastMatronymic ) : '\'' . addslashes( $this->m_strNameLastMatronymic ) . '\'' ) : 'NULL';
	}

	public function setHasNameLastMatronymic( $intHasNameLastMatronymic ) {
		$this->set( 'm_intHasNameLastMatronymic', CStrings::strToIntDef( $intHasNameLastMatronymic, NULL, false ) );
	}

	public function getHasNameLastMatronymic() {
		return $this->m_intHasNameLastMatronymic;
	}

	public function sqlHasNameLastMatronymic() {
		return ( true == isset( $this->m_intHasNameLastMatronymic ) ) ? ( string ) $this->m_intHasNameLastMatronymic : 'NULL';
	}

	public function setCitizenshipCountryCode( $strCitizenshipCountryCode ) {
		$this->set( 'm_strCitizenshipCountryCode', CStrings::strTrimDef( $strCitizenshipCountryCode, 2, NULL, true ) );
	}

	public function getCitizenshipCountryCode() {
		return $this->m_strCitizenshipCountryCode;
	}

	public function sqlCitizenshipCountryCode() {
		return ( true == isset( $this->m_strCitizenshipCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCitizenshipCountryCode ) : '\'' . addslashes( $this->m_strCitizenshipCountryCode ) . '\'' ) : 'NULL';
	}

	public function setTaxIdTypeId( $intTaxIdTypeId ) {
		$this->set( 'm_intTaxIdTypeId', CStrings::strToIntDef( $intTaxIdTypeId, NULL, false ) );
	}

	public function getTaxIdTypeId() {
		return $this->m_intTaxIdTypeId;
	}

	public function sqlTaxIdTypeId() {
		return ( true == isset( $this->m_intTaxIdTypeId ) ) ? ( string ) $this->m_intTaxIdTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert applicant record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaritalStatusTypeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlOriginatingCustomerId() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlGuestRemotePrimaryKey() . ', ' .
						$this->sqlAppRemotePrimaryKey() . ', ' .
						$this->sqlRenewalRemotePrimaryKey() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlHeight() . ', ' .
						$this->sqlWeight() . ', ' .
						$this->sqlOccupationId() . ', ' .
						$this->sqlEyeColor() . ', ' .
						$this->sqlHairColor() . ', ' .
						$this->sqlDlNumberEncrypted() . ', ' .
						$this->sqlDlStateCode() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlFacebookUser() . ', ' .
						$this->sqlLastAccessOn() . ', ' .
						$this->sqlMaxLoginAttempts() . ', ' .
						$this->sqlLoginErrorCount() . ', ' .
						$this->sqlRequiresPasswordChange() . ', ' .
						$this->sqlRelocationReason() . ', ' .
						$this->sqlHasFelonyConviction() . ', ' .
						$this->sqlFelonyIsResolved() . ', ' .
						$this->sqlFelonyComments() . ', ' .
						$this->sqlFelonyYear() . ', ' .
						$this->sqlSmokes() . ', ' .
						$this->sqlHasPets() . ', ' .
						$this->sqlPetDescription() . ', ' .
						$this->sqlHasBeenEvicted() . ', ' .
						$this->sqlHasBrokenLease() . ', ' .
						$this->sqlHasBankruptcy() . ', ' .
						$this->sqlHasPreviousRentOwed() . ', ' .
						$this->sqlHasMiddleName() . ', ' .
						$this->sqlIsReturningResident() . ', ' .
						$this->sqlIsUnemployed() . ', ' .
						$this->sqlIsLegalUsResident() . ', ' .
						$this->sqlBankruptcyCounty() . ', ' .
						$this->sqlBankruptcyStateCode() . ', ' .
						$this->sqlBankruptcyYear() . ', ' .
						$this->sqlHasPublicJudgment() . ', ' .
						$this->sqlPublicJudgmentYear() . ', ' .
						$this->sqlWasSuedForRent() . ', ' .
						$this->sqlWasSuedForDamages() . ', ' .
						$this->sqlSuedForRentComments() . ', ' .
						$this->sqlSuedForDamageComments() . ', ' .
						$this->sqlEvictedComments() . ', ' .
						$this->sqlBrokenLeaseComments() . ', ' .
						$this->sqlFelonyConvictionComments() . ', ' .
						$this->sqlPreviousRentOwedComments() . ', ' .
						$this->sqlBankruptcyComments() . ', ' .
						$this->sqlReturningResidentComments() . ', ' .
						$this->sqlUnemployedComments() . ', ' .
						$this->sqlReasonInUsComments() . ', ' .
						$this->sqlGaaScreeningInitials() . ', ' .
						$this->sqlRelationshipToApplicant() . ', ' .
						$this->sqlHasRentersInsurance() . ', ' .
						$this->sqlInsuranceProviderName() . ', ' .
						$this->sqlInsurancePolicyNumber() . ', ' .
						$this->sqlReferenceComments() . ', ' .
						$this->sqlIsAlien() . ', ' .
						$this->sqlReferencesApprovedBy() . ', ' .
						$this->sqlReferencesApprovedOn() . ', ' .
						$this->sqlCurrentManagerNameFirst() . ', ' .
						$this->sqlCurrentManagerNameLast() . ', ' .
						$this->sqlCurrentPhoneNumber() . ', ' .
						$this->sqlCurrentFaxNumber() . ', ' .
						$this->sqlCurrentEmailAddress() . ', ' .
						$this->sqlPreviousManagerNameFirst() . ', ' .
						$this->sqlPreviousManagerNameLast() . ', ' .
						$this->sqlPreviousPhoneNumber() . ', ' .
						$this->sqlPreviousFaxNumber() . ', ' .
						$this->sqlPreviousEmailAddress() . ', ' .
						$this->sqlParentNameFirst() . ', ' .
						$this->sqlParentNameLast() . ', ' .
						$this->sqlParentEmailAddress() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlSmsActivatedOn() . ', ' .
						$this->sqlSmsCancelledOn() . ', ' .
						$this->sqlSmsApplicationUpdatesActivatedOn() . ', ' .
						$this->sqlAltNameFirst() . ', ' .
						$this->sqlAltNameMiddle() . ', ' .
						$this->sqlAltNameLast() . ', ' .
						$this->sqlHasAltMiddleName() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlNameLastMatronymic() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
						$this->sqlCitizenshipCountryCode() . ', ' .
						$this->sqlTaxIdTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert applicant record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert applicant record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' . 
					'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaritalStatusTypeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlOriginatingCustomerId() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeId() . ', ' .
						$this->sqlSecondaryPhoneNumberTypeId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlStudentIdNumber() . ', ' .
						$this->sqlGuestRemotePrimaryKey() . ', ' .
						$this->sqlAppRemotePrimaryKey() . ', ' .
						$this->sqlRenewalRemotePrimaryKey() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMobileNumber() . ', ' .
						$this->sqlWorkNumber() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlHeight() . ', ' .
						$this->sqlWeight() . ', ' .
						$this->sqlOccupationId() . ', ' .
						$this->sqlEyeColor() . ', ' .
						$this->sqlHairColor() . ', ' .
						$this->sqlDlNumberEncrypted() . ', ' .
						$this->sqlDlStateCode() . ', ' .
						$this->sqlCompanyIdentificationTypeId() . ', ' .
						$this->sqlIdentificationValue() . ', ' .
						$this->sqlIdentificationExpiration() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlFacebookUser() . ', ' .
						$this->sqlLastAccessOn() . ', ' .
						$this->sqlMaxLoginAttempts() . ', ' .
						$this->sqlLoginErrorCount() . ', ' .
						$this->sqlRequiresPasswordChange() . ', ' .
						$this->sqlRelocationReason() . ', ' .
						$this->sqlHasFelonyConviction() . ', ' .
						$this->sqlFelonyIsResolved() . ', ' .
						$this->sqlFelonyComments() . ', ' .
						$this->sqlFelonyYear() . ', ' .
						$this->sqlSmokes() . ', ' .
						$this->sqlHasPets() . ', ' .
						$this->sqlPetDescription() . ', ' .
						$this->sqlHasBeenEvicted() . ', ' .
						$this->sqlHasBrokenLease() . ', ' .
						$this->sqlHasBankruptcy() . ', ' .
						$this->sqlHasPreviousRentOwed() . ', ' .
						$this->sqlHasMiddleName() . ', ' .
						$this->sqlIsReturningResident() . ', ' .
						$this->sqlIsUnemployed() . ', ' .
						$this->sqlIsLegalUsResident() . ', ' .
						$this->sqlBankruptcyCounty() . ', ' .
						$this->sqlBankruptcyStateCode() . ', ' .
						$this->sqlBankruptcyYear() . ', ' .
						$this->sqlHasPublicJudgment() . ', ' .
						$this->sqlPublicJudgmentYear() . ', ' .
						$this->sqlWasSuedForRent() . ', ' .
						$this->sqlWasSuedForDamages() . ', ' .
						$this->sqlSuedForRentComments() . ', ' .
						$this->sqlSuedForDamageComments() . ', ' .
						$this->sqlEvictedComments() . ', ' .
						$this->sqlBrokenLeaseComments() . ', ' .
						$this->sqlFelonyConvictionComments() . ', ' .
						$this->sqlPreviousRentOwedComments() . ', ' .
						$this->sqlBankruptcyComments() . ', ' .
						$this->sqlReturningResidentComments() . ', ' .
						$this->sqlUnemployedComments() . ', ' .
						$this->sqlReasonInUsComments() . ', ' .
						$this->sqlGaaScreeningInitials() . ', ' .
						$this->sqlRelationshipToApplicant() . ', ' .
						$this->sqlHasRentersInsurance() . ', ' .
						$this->sqlInsuranceProviderName() . ', ' .
						$this->sqlInsurancePolicyNumber() . ', ' .
						$this->sqlReferenceComments() . ', ' .
						$this->sqlIsAlien() . ', ' .
						$this->sqlReferencesApprovedBy() . ', ' .
						$this->sqlReferencesApprovedOn() . ', ' .
						$this->sqlCurrentManagerNameFirst() . ', ' .
						$this->sqlCurrentManagerNameLast() . ', ' .
						$this->sqlCurrentPhoneNumber() . ', ' .
						$this->sqlCurrentFaxNumber() . ', ' .
						$this->sqlCurrentEmailAddress() . ', ' .
						$this->sqlPreviousManagerNameFirst() . ', ' .
						$this->sqlPreviousManagerNameLast() . ', ' .
						$this->sqlPreviousPhoneNumber() . ', ' .
						$this->sqlPreviousFaxNumber() . ', ' .
						$this->sqlPreviousEmailAddress() . ', ' .
						$this->sqlParentNameFirst() . ', ' .
						$this->sqlParentNameLast() . ', ' .
						$this->sqlParentEmailAddress() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlSmsActivatedOn() . ', ' .
						$this->sqlSmsCancelledOn() . ', ' .
						$this->sqlSmsApplicationUpdatesActivatedOn() . ', ' .
						$this->sqlAltNameFirst() . ', ' .
						$this->sqlAltNameMiddle() . ', ' .
						$this->sqlAltNameLast() . ', ' .
						$this->sqlHasAltMiddleName() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlNameLastMatronymic() . ', ' .
						$this->sqlHasNameLastMatronymic() . ', ' .
						$this->sqlCitizenshipCountryCode() . ', ' .
						$this->sqlTaxIdTypeId() . ', ' .
						$this->sqlDetails() . ', ' .
						 ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			 return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update applicant record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update applicant record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_delete( ' .
		$this->sqlId() . ', ' .
		$this->sqlCid() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete applicant record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete applicant record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'marital_status_type_id' => $this->getMaritalStatusTypeId(),
			'customer_id' => $this->getCustomerId(),
			'originating_customer_id' => $this->getOriginatingCustomerId(),
			'primary_phone_number_type_id' => $this->getPrimaryPhoneNumberTypeId(),
			'secondary_phone_number_type_id' => $this->getSecondaryPhoneNumberTypeId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'student_id_number' => $this->getStudentIdNumber(),
			'guest_remote_primary_key' => $this->getGuestRemotePrimaryKey(),
			'app_remote_primary_key' => $this->getAppRemotePrimaryKey(),
			'renewal_remote_primary_key' => $this->getRenewalRemotePrimaryKey(),
			'company_name' => $this->getCompanyName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'name_maiden' => $this->getNameMaiden(),
			'phone_number' => $this->getPhoneNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'work_number' => $this->getWorkNumber(),
			'fax_number' => $this->getFaxNumber(),
			'email_address' => $this->getEmailAddress(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'tax_number_masked' => $this->getTaxNumberMasked(),
			'birth_date' => $this->getBirthDate(),
			'gender' => $this->getGender(),
			'height' => $this->getHeight(),
			'weight' => $this->getWeight(),
			'occupation_id' => $this->getOccupationId(),
			'eye_color' => $this->getEyeColor(),
			'hair_color' => $this->getHairColor(),
			'dl_number_encrypted' => $this->getDlNumberEncrypted(),
			'dl_state_code' => $this->getDlStateCode(),
			'company_identification_type_id' => $this->getCompanyIdentificationTypeId(),
			'identification_value' => $this->getIdentificationValue(),
			'identification_expiration' => $this->getIdentificationExpiration(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'facebook_user' => $this->getFacebookUser(),
			'last_access_on' => $this->getLastAccessOn(),
			'max_login_attempts' => $this->getMaxLoginAttempts(),
			'login_error_count' => $this->getLoginErrorCount(),
			'requires_password_change' => $this->getRequiresPasswordChange(),
			'relocation_reason' => $this->getRelocationReason(),
			'has_felony_conviction' => $this->getHasFelonyConviction(),
			'felony_is_resolved' => $this->getFelonyIsResolved(),
			'felony_comments' => $this->getFelonyComments(),
			'felony_year' => $this->getFelonyYear(),
			'smokes' => $this->getSmokes(),
			'has_pets' => $this->getHasPets(),
			'pet_description' => $this->getPetDescription(),
			'has_been_evicted' => $this->getHasBeenEvicted(),
			'has_broken_lease' => $this->getHasBrokenLease(),
			'has_bankruptcy' => $this->getHasBankruptcy(),
			'has_previous_rent_owed' => $this->getHasPreviousRentOwed(),
			'has_middle_name' => $this->getHasMiddleName(),
			'is_returning_resident' => $this->getIsReturningResident(),
			'is_unemployed' => $this->getIsUnemployed(),
			'is_legal_us_resident' => $this->getIsLegalUsResident(),
			'bankruptcy_county' => $this->getBankruptcyCounty(),
			'bankruptcy_state_code' => $this->getBankruptcyStateCode(),
			'bankruptcy_year' => $this->getBankruptcyYear(),
			'has_public_judgment' => $this->getHasPublicJudgment(),
			'public_judgment_year' => $this->getPublicJudgmentYear(),
			'was_sued_for_rent' => $this->getWasSuedForRent(),
			'was_sued_for_damages' => $this->getWasSuedForDamages(),
			'sued_for_rent_comments' => $this->getSuedForRentComments(),
			'sued_for_damage_comments' => $this->getSuedForDamageComments(),
			'evicted_comments' => $this->getEvictedComments(),
			'broken_lease_comments' => $this->getBrokenLeaseComments(),
			'felony_conviction_comments' => $this->getFelonyConvictionComments(),
			'previous_rent_owed_comments' => $this->getPreviousRentOwedComments(),
			'bankruptcy_comments' => $this->getBankruptcyComments(),
			'returning_resident_comments' => $this->getReturningResidentComments(),
			'unemployed_comments' => $this->getUnemployedComments(),
			'reason_in_us_comments' => $this->getReasonInUsComments(),
			'gaa_screening_initials' => $this->getGaaScreeningInitials(),
			'relationship_to_applicant' => $this->getRelationshipToApplicant(),
			'has_renters_insurance' => $this->getHasRentersInsurance(),
			'insurance_provider_name' => $this->getInsuranceProviderName(),
			'insurance_policy_number' => $this->getInsurancePolicyNumber(),
			'reference_comments' => $this->getReferenceComments(),
			'is_alien' => $this->getIsAlien(),
			'references_approved_by' => $this->getReferencesApprovedBy(),
			'references_approved_on' => $this->getReferencesApprovedOn(),
			'current_manager_name_first' => $this->getCurrentManagerNameFirst(),
			'current_manager_name_last' => $this->getCurrentManagerNameLast(),
			'current_phone_number' => $this->getCurrentPhoneNumber(),
			'current_fax_number' => $this->getCurrentFaxNumber(),
			'current_email_address' => $this->getCurrentEmailAddress(),
			'previous_manager_name_first' => $this->getPreviousManagerNameFirst(),
			'previous_manager_name_last' => $this->getPreviousManagerNameLast(),
			'previous_phone_number' => $this->getPreviousPhoneNumber(),
			'previous_fax_number' => $this->getPreviousFaxNumber(),
			'previous_email_address' => $this->getPreviousEmailAddress(),
			'parent_name_first' => $this->getParentNameFirst(),
			'parent_name_last' => $this->getParentNameLast(),
			'parent_email_address' => $this->getParentEmailAddress(),
			'notes' => $this->getNotes(),
			'order_num' => $this->getOrderNum(),
			'sms_activated_on' => $this->getSmsActivatedOn(),
			'sms_cancelled_on' => $this->getSmsCancelledOn(),
			'sms_application_updates_activated_on' => $this->getSmsApplicationUpdatesActivatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'alt_name_first' => $this->getAltNameFirst(),
			'alt_name_middle' => $this->getAltNameMiddle(),
			'alt_name_last' => $this->getAltNameLast(),
			'has_alt_middle_name' => $this->getHasAltMiddleName(),
			'preferred_name' => $this->getPreferredName(),
			'name_last_matronymic' => $this->getNameLastMatronymic(),
			'has_name_last_matronymic' => $this->getHasNameLastMatronymic(),
			'citizenship_country_code' => $this->getCitizenshipCountryCode(),
			'tax_id_type_id' => $this->getTaxIdTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>