<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlBranchRanges
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlBranchRanges extends CEosPluralBase {

	/**
	 * @return CGlBranchRange[]
	 */
	public static function fetchGlBranchRanges( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlBranchRange', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlBranchRange
	 */
	public static function fetchGlBranchRange( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlBranchRange', $objDatabase );
	}

	public static function fetchGlBranchRangeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_branch_ranges', $objDatabase );
	}

	public static function fetchGlBranchRangeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlBranchRange( sprintf( 'SELECT * FROM gl_branch_ranges WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlBranchRangesByCid( $intCid, $objDatabase ) {
		return self::fetchGlBranchRanges( sprintf( 'SELECT * FROM gl_branch_ranges WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlBranchRangesByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchGlBranchRanges( sprintf( 'SELECT * FROM gl_branch_ranges WHERE gl_tree_id = %d AND cid = %d', ( int ) $intGlTreeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlBranchRangesByGlGroupIdByCid( $intGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlBranchRanges( sprintf( 'SELECT * FROM gl_branch_ranges WHERE gl_group_id = %d AND cid = %d', ( int ) $intGlGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>