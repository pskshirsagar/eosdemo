<?php

class CBaseDefaultAmenity extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_amenities';

	protected $m_intId;
	protected $m_intAmenityTypeId;
	protected $m_strName;
	protected $m_strInternalDescription;
	protected $m_boolIsPublished;
	protected $m_boolIsMits;
	protected $m_boolIsComparable;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsAccessible;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_boolIsMits = true;
		$this->m_boolIsComparable = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsAccessible = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['amenity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityTypeId', trim( $arrValues['amenity_type_id'] ) ); elseif( isset( $arrValues['amenity_type_id'] ) ) $this->setAmenityTypeId( $arrValues['amenity_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['internal_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInternalDescription', trim( stripcslashes( $arrValues['internal_description'] ) ) ); elseif( isset( $arrValues['internal_description'] ) ) $this->setInternalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['internal_description'] ) : $arrValues['internal_description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_mits'] ) && $boolDirectSet ) $this->set( 'm_boolIsMits', trim( stripcslashes( $arrValues['is_mits'] ) ) ); elseif( isset( $arrValues['is_mits'] ) ) $this->setIsMits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_mits'] ) : $arrValues['is_mits'] );
		if( isset( $arrValues['is_comparable'] ) && $boolDirectSet ) $this->set( 'm_boolIsComparable', trim( stripcslashes( $arrValues['is_comparable'] ) ) ); elseif( isset( $arrValues['is_comparable'] ) ) $this->setIsComparable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_comparable'] ) : $arrValues['is_comparable'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_accessible'] ) && $boolDirectSet ) $this->set( 'm_boolIsAccessible', trim( stripcslashes( $arrValues['is_accessible'] ) ) ); elseif( isset( $arrValues['is_accessible'] ) ) $this->setIsAccessible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_accessible'] ) : $arrValues['is_accessible'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAmenityTypeId( $intAmenityTypeId ) {
		$this->set( 'm_intAmenityTypeId', CStrings::strToIntDef( $intAmenityTypeId, NULL, false ) );
	}

	public function getAmenityTypeId() {
		return $this->m_intAmenityTypeId;
	}

	public function sqlAmenityTypeId() {
		return ( true == isset( $this->m_intAmenityTypeId ) ) ? ( string ) $this->m_intAmenityTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setInternalDescription( $strInternalDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strInternalDescription', CStrings::strTrimDef( $strInternalDescription, 512, NULL, true ), $strLocaleCode );
	}

	public function getInternalDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strInternalDescription', $strLocaleCode );
	}

	public function sqlInternalDescription() {
		return ( true == isset( $this->m_strInternalDescription ) ) ? '\'' . addslashes( $this->m_strInternalDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMits( $boolIsMits ) {
		$this->set( 'm_boolIsMits', CStrings::strToBool( $boolIsMits ) );
	}

	public function getIsMits() {
		return $this->m_boolIsMits;
	}

	public function sqlIsMits() {
		return ( true == isset( $this->m_boolIsMits ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsComparable( $boolIsComparable ) {
		$this->set( 'm_boolIsComparable', CStrings::strToBool( $boolIsComparable ) );
	}

	public function getIsComparable() {
		return $this->m_boolIsComparable;
	}

	public function sqlIsComparable() {
		return ( true == isset( $this->m_boolIsComparable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsComparable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsAccessible( $boolIsAccessible ) {
		$this->set( 'm_boolIsAccessible', CStrings::strToBool( $boolIsAccessible ) );
	}

	public function getIsAccessible() {
		return $this->m_boolIsAccessible;
	}

	public function sqlIsAccessible() {
		return ( true == isset( $this->m_boolIsAccessible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAccessible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, amenity_type_id, name, internal_description, is_published, is_mits, is_comparable, order_num, created_by, created_on, details, is_accessible )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAmenityTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlInternalDescription() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsMits() . ', ' .
						$this->sqlIsComparable() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsAccessible() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_type_id = ' . $this->sqlAmenityTypeId(). ',' ; } elseif( true == array_key_exists( 'AmenityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_type_id = ' . $this->sqlAmenityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription(). ',' ; } elseif( true == array_key_exists( 'InternalDescription', $this->getChangedColumns() ) ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mits = ' . $this->sqlIsMits(). ',' ; } elseif( true == array_key_exists( 'IsMits', $this->getChangedColumns() ) ) { $strSql .= ' is_mits = ' . $this->sqlIsMits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_comparable = ' . $this->sqlIsComparable(). ',' ; } elseif( true == array_key_exists( 'IsComparable', $this->getChangedColumns() ) ) { $strSql .= ' is_comparable = ' . $this->sqlIsComparable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_accessible = ' . $this->sqlIsAccessible() ; } elseif( true == array_key_exists( 'IsAccessible', $this->getChangedColumns() ) ) { $strSql .= ' is_accessible = ' . $this->sqlIsAccessible() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'amenity_type_id' => $this->getAmenityTypeId(),
			'name' => $this->getName(),
			'internal_description' => $this->getInternalDescription(),
			'is_published' => $this->getIsPublished(),
			'is_mits' => $this->getIsMits(),
			'is_comparable' => $this->getIsComparable(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'is_accessible' => $this->getIsAccessible()
		);
	}

}
?>