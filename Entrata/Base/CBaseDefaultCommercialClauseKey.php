<?php

class CBaseDefaultCommercialClauseKey extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_commercial_clause_keys';

	protected $m_intId;
	protected $m_strKey;
	protected $m_strLabel;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLabel', trim( stripcslashes( $arrValues['label'] ) ) ); elseif( isset( $arrValues['label'] ) ) $this->setLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['label'] ) : $arrValues['label'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 50, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setLabel( $strLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLabel', CStrings::strTrimDef( $strLabel, 100, NULL, true ), $strLocaleCode );
	}

	public function getLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLabel', $strLocaleCode );
	}

	public function sqlLabel() {
		return ( true == isset( $this->m_strLabel ) ) ? '\'' . addslashes( $this->m_strLabel ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'key' => $this->getKey(),
			'label' => $this->getLabel(),
			'details' => $this->getDetails()
		);
	}

}
?>