<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFiles
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFiles extends CEosPluralBase {

	/**
	 * @return CFile[]
	 */
	public static function fetchFiles( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFile::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFile
	 */
	public static function fetchFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFile::class, $objDatabase );
	}

	public static function fetchFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'files', $objDatabase );
	}

	public static function fetchFileByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFile( sprintf( 'SELECT * FROM files WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByFileExtensionIdByCid( $intFileExtensionId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE file_extension_id = %d AND cid = %d', $intFileExtensionId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByCid( $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFilesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByDocumentAddendaIdByCid( $intDocumentAddendaId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE document_addenda_id = %d AND cid = %d', $intDocumentAddendaId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchFilesByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {
		return self::fetchFiles( sprintf( 'SELECT * FROM files WHERE file_type_id = %d AND cid = %d', $intFileTypeId, $intCid ), $objDatabase );
	}

}
?>