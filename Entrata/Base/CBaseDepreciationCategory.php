<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDepreciationCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.depreciation_categories';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultDepreciationCategoryId;
	protected $m_intDepreciationGlAccountId;
	protected $m_intAccumDepreciationGlAccountId;
	protected $m_intTaxRecoveryPeriodId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intUsefulLifeMonths;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsDepreciable;

	public function __construct() {
		parent::__construct();

		$this->m_intUsefulLifeMonths = '1';
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_boolIsDepreciable = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_depreciation_category_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultDepreciationCategoryId', trim( $arrValues['default_depreciation_category_id'] ) ); elseif( isset( $arrValues['default_depreciation_category_id'] ) ) $this->setDefaultDepreciationCategoryId( $arrValues['default_depreciation_category_id'] );
		if( isset( $arrValues['depreciation_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDepreciationGlAccountId', trim( $arrValues['depreciation_gl_account_id'] ) ); elseif( isset( $arrValues['depreciation_gl_account_id'] ) ) $this->setDepreciationGlAccountId( $arrValues['depreciation_gl_account_id'] );
		if( isset( $arrValues['accum_depreciation_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccumDepreciationGlAccountId', trim( $arrValues['accum_depreciation_gl_account_id'] ) ); elseif( isset( $arrValues['accum_depreciation_gl_account_id'] ) ) $this->setAccumDepreciationGlAccountId( $arrValues['accum_depreciation_gl_account_id'] );
		if( isset( $arrValues['tax_recovery_period_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRecoveryPeriodId', trim( $arrValues['tax_recovery_period_id'] ) ); elseif( isset( $arrValues['tax_recovery_period_id'] ) ) $this->setTaxRecoveryPeriodId( $arrValues['tax_recovery_period_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['useful_life_months'] ) && $boolDirectSet ) $this->set( 'm_intUsefulLifeMonths', trim( $arrValues['useful_life_months'] ) ); elseif( isset( $arrValues['useful_life_months'] ) ) $this->setUsefulLifeMonths( $arrValues['useful_life_months'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_depreciable'] ) && $boolDirectSet ) $this->set( 'm_boolIsDepreciable', trim( stripcslashes( $arrValues['is_depreciable'] ) ) ); elseif( isset( $arrValues['is_depreciable'] ) ) $this->setIsDepreciable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_depreciable'] ) : $arrValues['is_depreciable'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultDepreciationCategoryId( $intDefaultDepreciationCategoryId ) {
		$this->set( 'm_intDefaultDepreciationCategoryId', CStrings::strToIntDef( $intDefaultDepreciationCategoryId, NULL, false ) );
	}

	public function getDefaultDepreciationCategoryId() {
		return $this->m_intDefaultDepreciationCategoryId;
	}

	public function sqlDefaultDepreciationCategoryId() {
		return ( true == isset( $this->m_intDefaultDepreciationCategoryId ) ) ? ( string ) $this->m_intDefaultDepreciationCategoryId : 'NULL';
	}

	public function setDepreciationGlAccountId( $intDepreciationGlAccountId ) {
		$this->set( 'm_intDepreciationGlAccountId', CStrings::strToIntDef( $intDepreciationGlAccountId, NULL, false ) );
	}

	public function getDepreciationGlAccountId() {
		return $this->m_intDepreciationGlAccountId;
	}

	public function sqlDepreciationGlAccountId() {
		return ( true == isset( $this->m_intDepreciationGlAccountId ) ) ? ( string ) $this->m_intDepreciationGlAccountId : 'NULL';
	}

	public function setAccumDepreciationGlAccountId( $intAccumDepreciationGlAccountId ) {
		$this->set( 'm_intAccumDepreciationGlAccountId', CStrings::strToIntDef( $intAccumDepreciationGlAccountId, NULL, false ) );
	}

	public function getAccumDepreciationGlAccountId() {
		return $this->m_intAccumDepreciationGlAccountId;
	}

	public function sqlAccumDepreciationGlAccountId() {
		return ( true == isset( $this->m_intAccumDepreciationGlAccountId ) ) ? ( string ) $this->m_intAccumDepreciationGlAccountId : 'NULL';
	}

	public function setTaxRecoveryPeriodId( $intTaxRecoveryPeriodId ) {
		$this->set( 'm_intTaxRecoveryPeriodId', CStrings::strToIntDef( $intTaxRecoveryPeriodId, NULL, false ) );
	}

	public function getTaxRecoveryPeriodId() {
		return $this->m_intTaxRecoveryPeriodId;
	}

	public function sqlTaxRecoveryPeriodId() {
		return ( true == isset( $this->m_intTaxRecoveryPeriodId ) ) ? ( string ) $this->m_intTaxRecoveryPeriodId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUsefulLifeMonths( $intUsefulLifeMonths ) {
		$this->set( 'm_intUsefulLifeMonths', CStrings::strToIntDef( $intUsefulLifeMonths, NULL, false ) );
	}

	public function getUsefulLifeMonths() {
		return $this->m_intUsefulLifeMonths;
	}

	public function sqlUsefulLifeMonths() {
		return ( true == isset( $this->m_intUsefulLifeMonths ) ) ? ( string ) $this->m_intUsefulLifeMonths : '1';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsDepreciable( $boolIsDepreciable ) {
		$this->set( 'm_boolIsDepreciable', CStrings::strToBool( $boolIsDepreciable ) );
	}

	public function getIsDepreciable() {
		return $this->m_boolIsDepreciable;
	}

	public function sqlIsDepreciable() {
		return ( true == isset( $this->m_boolIsDepreciable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDepreciable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_depreciation_category_id, depreciation_gl_account_id, accum_depreciation_gl_account_id, tax_recovery_period_id, name, description, useful_life_months, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_depreciable )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDefaultDepreciationCategoryId() . ', ' .
 						$this->sqlDepreciationGlAccountId() . ', ' .
 						$this->sqlAccumDepreciationGlAccountId() . ', ' .
 						$this->sqlTaxRecoveryPeriodId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlUsefulLifeMonths() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsDepreciable() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_depreciation_category_id = ' . $this->sqlDefaultDepreciationCategoryId() . ','; } elseif( true == array_key_exists( 'DefaultDepreciationCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' default_depreciation_category_id = ' . $this->sqlDefaultDepreciationCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' depreciation_gl_account_id = ' . $this->sqlDepreciationGlAccountId() . ','; } elseif( true == array_key_exists( 'DepreciationGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' depreciation_gl_account_id = ' . $this->sqlDepreciationGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accum_depreciation_gl_account_id = ' . $this->sqlAccumDepreciationGlAccountId() . ','; } elseif( true == array_key_exists( 'AccumDepreciationGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accum_depreciation_gl_account_id = ' . $this->sqlAccumDepreciationGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_recovery_period_id = ' . $this->sqlTaxRecoveryPeriodId() . ','; } elseif( true == array_key_exists( 'TaxRecoveryPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' tax_recovery_period_id = ' . $this->sqlTaxRecoveryPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' useful_life_months = ' . $this->sqlUsefulLifeMonths() . ','; } elseif( true == array_key_exists( 'UsefulLifeMonths', $this->getChangedColumns() ) ) { $strSql .= ' useful_life_months = ' . $this->sqlUsefulLifeMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_depreciable = ' . $this->sqlIsDepreciable() . ','; } elseif( true == array_key_exists( 'IsDepreciable', $this->getChangedColumns() ) ) { $strSql .= ' is_depreciable = ' . $this->sqlIsDepreciable() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_depreciation_category_id' => $this->getDefaultDepreciationCategoryId(),
			'depreciation_gl_account_id' => $this->getDepreciationGlAccountId(),
			'accum_depreciation_gl_account_id' => $this->getAccumDepreciationGlAccountId(),
			'tax_recovery_period_id' => $this->getTaxRecoveryPeriodId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'useful_life_months' => $this->getUsefulLifeMonths(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_depreciable' => $this->getIsDepreciable()
		);
	}

}
?>