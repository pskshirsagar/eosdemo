<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSocialPostMediaFile extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.social_post_media_files';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSocialPostId;
	protected $m_intCompanyMediaFileId;
	protected $m_intMediaFileTypeId;
	protected $m_strMediaPath;
	protected $m_strMediaName;
	protected $m_strRemoteMediaUri;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['social_post_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostId', trim( $arrValues['social_post_id'] ) ); elseif( isset( $arrValues['social_post_id'] ) ) $this->setSocialPostId( $arrValues['social_post_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['media_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMediaFileTypeId', trim( $arrValues['media_file_type_id'] ) ); elseif( isset( $arrValues['media_file_type_id'] ) ) $this->setMediaFileTypeId( $arrValues['media_file_type_id'] );
		if( isset( $arrValues['media_path'] ) && $boolDirectSet ) $this->set( 'm_strMediaPath', trim( $arrValues['media_path'] ) ); elseif( isset( $arrValues['media_path'] ) ) $this->setMediaPath( $arrValues['media_path'] );
		if( isset( $arrValues['media_name'] ) && $boolDirectSet ) $this->set( 'm_strMediaName', trim( $arrValues['media_name'] ) ); elseif( isset( $arrValues['media_name'] ) ) $this->setMediaName( $arrValues['media_name'] );
		if( isset( $arrValues['remote_media_uri'] ) && $boolDirectSet ) $this->set( 'm_strRemoteMediaUri', trim( $arrValues['remote_media_uri'] ) ); elseif( isset( $arrValues['remote_media_uri'] ) ) $this->setRemoteMediaUri( $arrValues['remote_media_uri'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSocialPostId( $intSocialPostId ) {
		$this->set( 'm_intSocialPostId', CStrings::strToIntDef( $intSocialPostId, NULL, false ) );
	}

	public function getSocialPostId() {
		return $this->m_intSocialPostId;
	}

	public function sqlSocialPostId() {
		return ( true == isset( $this->m_intSocialPostId ) ) ? ( string ) $this->m_intSocialPostId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setMediaFileTypeId( $intMediaFileTypeId ) {
		$this->set( 'm_intMediaFileTypeId', CStrings::strToIntDef( $intMediaFileTypeId, NULL, false ) );
	}

	public function getMediaFileTypeId() {
		return $this->m_intMediaFileTypeId;
	}

	public function sqlMediaFileTypeId() {
		return ( true == isset( $this->m_intMediaFileTypeId ) ) ? ( string ) $this->m_intMediaFileTypeId : 'NULL';
	}

	public function setMediaPath( $strMediaPath ) {
		$this->set( 'm_strMediaPath', CStrings::strTrimDef( $strMediaPath, 2000, NULL, true ) );
	}

	public function getMediaPath() {
		return $this->m_strMediaPath;
	}

	public function sqlMediaPath() {
		return ( true == isset( $this->m_strMediaPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMediaPath ) : '\'' . addslashes( $this->m_strMediaPath ) . '\'' ) : 'NULL';
	}

	public function setMediaName( $strMediaName ) {
		$this->set( 'm_strMediaName', CStrings::strTrimDef( $strMediaName, 250, NULL, true ) );
	}

	public function getMediaName() {
		return $this->m_strMediaName;
	}

	public function sqlMediaName() {
		return ( true == isset( $this->m_strMediaName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMediaName ) : '\'' . addslashes( $this->m_strMediaName ) . '\'' ) : 'NULL';
	}

	public function setRemoteMediaUri( $strRemoteMediaUri ) {
		$this->set( 'm_strRemoteMediaUri', CStrings::strTrimDef( $strRemoteMediaUri, 2000, NULL, true ) );
	}

	public function getRemoteMediaUri() {
		return $this->m_strRemoteMediaUri;
	}

	public function sqlRemoteMediaUri() {
		return ( true == isset( $this->m_strRemoteMediaUri ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemoteMediaUri ) : '\'' . addslashes( $this->m_strRemoteMediaUri ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, social_post_id, company_media_file_id, media_file_type_id, media_path, media_name, remote_media_uri, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSocialPostId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlMediaFileTypeId() . ', ' .
						$this->sqlMediaPath() . ', ' .
						$this->sqlMediaName() . ', ' .
						$this->sqlRemoteMediaUri() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId(). ',' ; } elseif( true == array_key_exists( 'SocialPostId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_file_type_id = ' . $this->sqlMediaFileTypeId(). ',' ; } elseif( true == array_key_exists( 'MediaFileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' media_file_type_id = ' . $this->sqlMediaFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_path = ' . $this->sqlMediaPath(). ',' ; } elseif( true == array_key_exists( 'MediaPath', $this->getChangedColumns() ) ) { $strSql .= ' media_path = ' . $this->sqlMediaPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_name = ' . $this->sqlMediaName(). ',' ; } elseif( true == array_key_exists( 'MediaName', $this->getChangedColumns() ) ) { $strSql .= ' media_name = ' . $this->sqlMediaName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_media_uri = ' . $this->sqlRemoteMediaUri(). ',' ; } elseif( true == array_key_exists( 'RemoteMediaUri', $this->getChangedColumns() ) ) { $strSql .= ' remote_media_uri = ' . $this->sqlRemoteMediaUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'social_post_id' => $this->getSocialPostId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'media_file_type_id' => $this->getMediaFileTypeId(),
			'media_path' => $this->getMediaPath(),
			'media_name' => $this->getMediaName(),
			'remote_media_uri' => $this->getRemoteMediaUri(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>