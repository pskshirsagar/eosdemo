<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerEmployers extends CEosPluralBase {

    public static function fetchCustomerEmployers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CCustomerEmployer', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchCustomerEmployer( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CCustomerEmployer', $objDatabase );
    }

    public static function fetchCustomerEmployerCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'customer_employers', $objDatabase );
    }

    public static function fetchCustomerEmployerByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchCustomerEmployer( sprintf( 'SELECT * FROM customer_employers WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCustomerEmployersByCid( $intCid, $objDatabase ) {
        return self::fetchCustomerEmployers( sprintf( 'SELECT * FROM customer_employers WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchCustomerEmployersByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
        return self::fetchCustomerEmployers( sprintf( 'SELECT * FROM customer_employers WHERE customer_id = %d AND cid = %d', (int) $intCustomerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCustomerEmployersByIncomeTypeIdByCid( $intIncomeTypeId, $intCid, $objDatabase ) {
        return self::fetchCustomerEmployers( sprintf( 'SELECT * FROM customer_employers WHERE income_type_id = %d AND cid = %d', (int) $intIncomeTypeId, (int) $intCid ), $objDatabase );
    }

}
?>