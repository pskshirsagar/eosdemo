<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportHistories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportHistories extends CEosPluralBase {

	/**
	 * @return CReportHistory[]
	 */
	public static function fetchReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportHistory::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportHistory
	 */
	public static function fetchReportHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportHistory::class, $objDatabase );
	}

	public static function fetchReportHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_histories', $objDatabase );
	}

	public static function fetchReportHistoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportHistory( sprintf( 'SELECT * FROM report_histories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByCid( $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportScheduleTypeIdByCid( $intReportScheduleTypeId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_schedule_type_id = %d AND cid = %d', ( int ) $intReportScheduleTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportScheduleIdByCid( $intReportScheduleId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_schedule_id = %d AND cid = %d', ( int ) $intReportScheduleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_id = %d AND cid = %d', ( int ) $intReportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportVersionIdByCid( $intReportVersionId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_version_id = %d AND cid = %d', ( int ) $intReportVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_filter_id = %d AND cid = %d', ( int ) $intReportFilterId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportNewInstanceIdByCid( $intReportNewInstanceId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_new_instance_id = %d AND cid = %d', ( int ) $intReportNewInstanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_group_id = %d AND cid = %d', ( int ) $intReportGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByReportNewGroupIdByCid( $intReportNewGroupId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE report_new_group_id = %d AND cid = %d', ( int ) $intReportNewGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportHistoriesByCorrelationIdByCid( $intCorrelationId, $intCid, $objDatabase ) {
		return self::fetchReportHistories( sprintf( 'SELECT * FROM report_histories WHERE correlation_id = %d AND cid = %d', ( int ) $intCorrelationId, ( int ) $intCid ), $objDatabase );
	}

}
?>