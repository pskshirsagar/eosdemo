<?php

class CBaseScheduledEmailTemplate extends CEosSingularBase {

    protected $m_intId;
    protected $m_strName;
    protected $m_strPath;
    protected $m_strDescription;
    protected $m_strPreviewImageUri;
    protected $m_intIsPublished;
    protected $m_intOrderNum;

    public function __construct() {
        parent::__construct();

        $this->m_intIsPublished = '1';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['path'] ) && $boolDirectSet ) $this->m_strPath = trim( stripcslashes( $arrValues['path'] ) ); else if( isset( $arrValues['path'] ) ) $this->setPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['path'] ) : $arrValues['path'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['preview_image_uri'] ) && $boolDirectSet ) $this->m_strPreviewImageUri = trim( stripcslashes( $arrValues['preview_image_uri'] ) ); else if( isset( $arrValues['preview_image_uri'] ) ) $this->setPreviewImageUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preview_image_uri'] ) : $arrValues['preview_image_uri'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setPath( $strPath ) {
        $this->m_strPath = CStrings::strTrimDef( $strPath, 4096, NULL, true );
    }

    public function getPath() {
        return $this->m_strPath;
    }

    public function sqlPath() {
        return ( true == isset( $this->m_strPath ) ) ? '\'' . addslashes( $this->m_strPath ) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 4096, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setPreviewImageUri( $strPreviewImageUri ) {
        $this->m_strPreviewImageUri = CStrings::strTrimDef( $strPreviewImageUri, 4096, NULL, true );
    }

    public function getPreviewImageUri() {
        return $this->m_strPreviewImageUri;
    }

    public function sqlPreviewImageUri() {
        return ( true == isset( $this->m_strPreviewImageUri ) ) ? '\'' . addslashes( $this->m_strPreviewImageUri ) . '\'' : 'NULL';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'name' => $this->getName(),
        	'path' => $this->getPath(),
        	'description' => $this->getDescription(),
        	'preview_image_uri' => $this->getPreviewImageUri(),
        	'is_published' => $this->getIsPublished(),
        	'order_num' => $this->getOrderNum()
        );
    }

}
?>