<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventCategories
 * Do not add any new functions to this class.
 */

class CBaseCalendarEventCategories extends CEosPluralBase {

	/**
	 * @return CCalendarEventCategory[]
	 */
	public static function fetchCalendarEventCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCalendarEventCategory::class, $objDatabase );
	}

	/**
	 * @return CCalendarEventCategory
	 */
	public static function fetchCalendarEventCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCalendarEventCategory::class, $objDatabase );
	}

	public static function fetchCalendarEventCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calendar_event_categories', $objDatabase );
	}

	public static function fetchCalendarEventCategoryById( $intId, $objDatabase ) {
		return self::fetchCalendarEventCategory( sprintf( 'SELECT * FROM calendar_event_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>