<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationRequests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationRequests extends CEosPluralBase {

	/**
	 * @return CScreeningApplicationRequest[]
	 */
	public static function fetchScreeningApplicationRequests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicationRequest', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningApplicationRequest
	 */
	public static function fetchScreeningApplicationRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicationRequest', $objDatabase );
	}

	public static function fetchScreeningApplicationRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_requests', $objDatabase );
	}

	public static function fetchScreeningApplicationRequestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequest( sprintf( 'SELECT * FROM screening_application_requests WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByApplicationTypeIdByCid( $intApplicationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE application_type_id = %d AND cid = %d', ( int ) $intApplicationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByScreeningVendorIdByCid( $intScreeningVendorId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE screening_vendor_id = %d AND cid = %d', ( int ) $intScreeningVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByRemoteScreeningIdByCid( $intRemoteScreeningId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE remote_screening_id = %d AND cid = %d', ( int ) $intRemoteScreeningId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByRequestStatusTypeIdByCid( $intRequestStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE request_status_type_id = %d AND cid = %d', ( int ) $intRequestStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE screening_recommendation_type_id = %d AND cid = %d', ( int ) $intScreeningRecommendationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationRequestsByScreeningDecisionTypeIdByCid( $intScreeningDecisionTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationRequests( sprintf( 'SELECT * FROM screening_application_requests WHERE screening_decision_type_id = %d AND cid = %d', ( int ) $intScreeningDecisionTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>