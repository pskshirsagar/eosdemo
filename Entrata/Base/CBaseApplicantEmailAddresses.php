<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantEmailAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantEmailAddresses extends CEosPluralBase {

	/**
	 * @return CApplicantEmailAddress[]
	 */
	public static function fetchApplicantEmailAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantEmailAddress', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantEmailAddress
	 */
	public static function fetchApplicantEmailAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantEmailAddress', $objDatabase );
	}

	public static function fetchApplicantEmailAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_email_addresses', $objDatabase );
	}

	public static function fetchApplicantEmailAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantEmailAddress( sprintf( 'SELECT * FROM applicant_email_addresses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantEmailAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantEmailAddresses( sprintf( 'SELECT * FROM applicant_email_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantEmailAddressesByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantEmailAddresses( sprintf( 'SELECT * FROM applicant_email_addresses WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantEmailAddressesByEmailAddressTypeIdByCid( $intEmailAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantEmailAddresses( sprintf( 'SELECT * FROM applicant_email_addresses WHERE email_address_type_id = %d AND cid = %d', ( int ) $intEmailAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>