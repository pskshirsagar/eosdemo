<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyNumbers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyNumbers extends CEosPluralBase {

	/**
	 * @return CCompanyNumber[]
	 */
	public static function fetchCompanyNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyNumber', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyNumber
	 */
	public static function fetchCompanyNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyNumber', $objDatabase );
	}

	public static function fetchCompanyNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_numbers', $objDatabase );
	}

	public static function fetchCompanyNumberByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyNumber( sprintf( 'SELECT * FROM company_numbers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyNumbers( sprintf( 'SELECT * FROM company_numbers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>