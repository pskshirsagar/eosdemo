<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoals
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeasingGoals extends CEosPluralBase {

	/**
	 * @return CLeasingGoal[]
	 */
	public static function fetchLeasingGoals( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeasingGoal::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeasingGoal
	 */
	public static function fetchLeasingGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeasingGoal::class, $objDatabase );
	}

	public static function fetchLeasingGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'leasing_goals', $objDatabase );
	}

	public static function fetchLeasingGoalByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoal( sprintf( 'SELECT * FROM leasing_goals WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalsByCid( $intCid, $objDatabase ) {
		return self::fetchLeasingGoals( sprintf( 'SELECT * FROM leasing_goals WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeasingGoalsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeasingGoals( sprintf( 'SELECT * FROM leasing_goals WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>