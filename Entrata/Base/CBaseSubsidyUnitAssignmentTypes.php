<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyUnitAssignmentTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyUnitAssignmentTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyUnitAssignmentType[]
	 */
	public static function fetchSubsidyUnitAssignmentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyUnitAssignmentType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyUnitAssignmentType
	 */
	public static function fetchSubsidyUnitAssignmentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyUnitAssignmentType::class, $objDatabase );
	}

	public static function fetchSubsidyUnitAssignmentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_unit_assignment_types', $objDatabase );
	}

	public static function fetchSubsidyUnitAssignmentTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyUnitAssignmentType( sprintf( 'SELECT * FROM subsidy_unit_assignment_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>