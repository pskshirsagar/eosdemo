<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApDetailLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApDetailLogs extends CEosPluralBase {

	/**
	 * @return CApDetailLog[]
	 */
	public static function fetchApDetailLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApDetailLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApDetailLog
	 */
	public static function fetchApDetailLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApDetailLog::class, $objDatabase );
	}

	public static function fetchApDetailLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_detail_logs', $objDatabase );
	}

	public static function fetchApDetailLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApDetailLog( sprintf( 'SELECT * FROM ap_detail_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByCid( $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApDetailIdByCid( $intApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_detail_id = %d AND cid = %d', $intApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApHeaderLogIdByCid( $intApHeaderLogId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_header_log_id = %d AND cid = %d', $intApHeaderLogId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_header_id = %d AND cid = %d', $intApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApPhysicalStatusTypeIdByCid( $intApPhysicalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_physical_status_type_id = %d AND cid = %d', $intApPhysicalStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE gl_transaction_type_id = %d AND cid = %d', $intGlTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApTransactionTypeIdByCid( $intApTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_transaction_type_id = %d AND cid = %d', $intApTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApPostTypeIdByCid( $intApPostTypeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_post_type_id = %d AND cid = %d', $intApPostTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE bank_account_id = %d AND cid = %d', $intBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByInterCoPropertyIdByCid( $intInterCoPropertyId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE inter_co_property_id = %d AND cid = %d', $intInterCoPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApFormulaIdByCid( $intApFormulaId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_formula_id = %d AND cid = %d', $intApFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByCompanyDepartmentIdByCid( $intCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE company_department_id = %d AND cid = %d', $intCompanyDepartmentId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByGlDimensionIdByCid( $intGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE gl_dimension_id = %d AND cid = %d', $intGlDimensionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE job_phase_id = %d AND cid = %d', $intJobPhaseId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_contract_id = %d AND cid = %d', $intApContractId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByBudgetApHeaderIdByCid( $intBudgetApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE budget_ap_header_id = %d AND cid = %d', $intBudgetApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApContractApDetailIdByCid( $intApContractApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_contract_ap_detail_id = %d AND cid = %d', $intApContractApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPoApDetailIdByCid( $intPoApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE po_ap_detail_id = %d AND cid = %d', $intPoApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByReversalApDetailIdByCid( $intReversalApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE reversal_ap_detail_id = %d AND cid = %d', $intReversalApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByScheduledApDetailIdByCid( $intScheduledApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE scheduled_ap_detail_id = %d AND cid = %d', $intScheduledApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByTemplateApDetailIdByCid( $intTemplateApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE template_ap_detail_id = %d AND cid = %d', $intTemplateApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByReimbursementApDetailIdByCid( $intReimbursementApDetailId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE reimbursement_ap_detail_id = %d AND cid = %d', $intReimbursementApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByUnitOfMeasureIdByCid( $intUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE unit_of_measure_id = %d AND cid = %d', $intUnitOfMeasureId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApCatalogItemIdByCid( $intApCatalogItemId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_catalog_item_id = %d AND cid = %d', $intApCatalogItemId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_code_id = %d AND cid = %d', $intApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApPayeeSubAccountIdByCid( $intApPayeeSubAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_payee_sub_account_id = %d AND cid = %d', $intApPayeeSubAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByJobApCodeIdByCid( $intJobApCodeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE job_ap_code_id = %d AND cid = %d', $intJobApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApGlAccountIdByCid( $intApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_gl_account_id = %d AND cid = %d', $intApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByWipGlAccountIdByCid( $intWipGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE wip_gl_account_id = %d AND cid = %d', $intWipGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPurchasesClearingGlAccountIdByCid( $intPurchasesClearingGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE purchases_clearing_gl_account_id = %d AND cid = %d', $intPurchasesClearingGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPendingReimbursementsGlAccountIdByCid( $intPendingReimbursementsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE pending_reimbursements_gl_account_id = %d AND cid = %d', $intPendingReimbursementsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByInterCoApGlAccountIdByCid( $intInterCoApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE inter_co_ap_gl_account_id = %d AND cid = %d', $intInterCoApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByBankGlAccountIdByCid( $intBankGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE bank_gl_account_id = %d AND cid = %d', $intBankGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByRetentionPayableGlAccountIdByCid( $intRetentionPayableGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE retention_payable_gl_account_id = %d AND cid = %d', $intRetentionPayableGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByAccrualDebitGlAccountIdByCid( $intAccrualDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE accrual_debit_gl_account_id = %d AND cid = %d', $intAccrualDebitGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByAccrualCreditGlAccountIdByCid( $intAccrualCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE accrual_credit_gl_account_id = %d AND cid = %d', $intAccrualCreditGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByCashDebitGlAccountIdByCid( $intCashDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE cash_debit_gl_account_id = %d AND cid = %d', $intCashDebitGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByCashCreditGlAccountIdByCid( $intCashCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE cash_credit_gl_account_id = %d AND cid = %d', $intCashCreditGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByInterCoArTransactionIdByCid( $intInterCoArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE inter_co_ar_transaction_id = %d AND cid = %d', $intInterCoArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByRefundArTransactionIdByCid( $intRefundArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE refund_ar_transaction_id = %d AND cid = %d', $intRefundArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByAssetTransactionIdByCid( $intAssetTransactionId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE asset_transaction_id = %d AND cid = %d', $intAssetTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE ap_export_batch_id = %d AND cid = %d', $intApExportBatchId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByFeeIdByCid( $intFeeId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE fee_id = %d AND cid = %d', $intFeeId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByOldPoDetailLogIdByCid( $intOldPoDetailLogId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE old_po_detail_log_id = %d AND cid = %d', $intOldPoDetailLogId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByCcApPaymentIdByCid( $intCcApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE cc_ap_payment_id = %d AND cid = %d', $intCcApPaymentId, $intCid ), $objDatabase );
	}

	public static function fetchApDetailLogsByReimbursementMethodIdByCid( $intReimbursementMethodId, $intCid, $objDatabase ) {
		return self::fetchApDetailLogs( sprintf( 'SELECT * FROM ap_detail_logs WHERE reimbursement_method_id = %d AND cid = %d', $intReimbursementMethodId, $intCid ), $objDatabase );
	}

}
?>