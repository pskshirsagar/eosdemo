<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreCategories
 * Do not add any new functions to this class.
 */

class CBaseChoreCategories extends CEosPluralBase {

	/**
	 * @return CChoreCategory[]
	 */
	public static function fetchChoreCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChoreCategory::class, $objDatabase );
	}

	/**
	 * @return CChoreCategory
	 */
	public static function fetchChoreCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChoreCategory::class, $objDatabase );
	}

	public static function fetchChoreCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_categories', $objDatabase );
	}

	public static function fetchChoreCategoryById( $intId, $objDatabase ) {
		return self::fetchChoreCategory( sprintf( 'SELECT * FROM chore_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>