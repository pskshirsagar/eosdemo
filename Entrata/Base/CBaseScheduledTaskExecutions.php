<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskExecutions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskExecutions extends CEosPluralBase {

	/**
	 * @return CScheduledTaskExecution[]
	 */
	public static function fetchScheduledTaskExecutions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledTaskExecution', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledTaskExecution
	 */
	public static function fetchScheduledTaskExecution( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledTaskExecution', $objDatabase );
	}

	public static function fetchScheduledTaskExecutionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_task_executions', $objDatabase );
	}

	public static function fetchScheduledTaskExecutionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskExecution( sprintf( 'SELECT * FROM scheduled_task_executions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskExecutionsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledTaskExecutions( sprintf( 'SELECT * FROM scheduled_task_executions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskExecutionsByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskExecutions( sprintf( 'SELECT * FROM scheduled_task_executions WHERE scheduled_task_id = %d AND cid = %d', ( int ) $intScheduledTaskId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskExecutionsByEventIdByCid( $intEventId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskExecutions( sprintf( 'SELECT * FROM scheduled_task_executions WHERE event_id = %d AND cid = %d', ( int ) $intEventId, ( int ) $intCid ), $objDatabase );
	}

}
?>