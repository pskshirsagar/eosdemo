<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabs extends CEosPluralBase {

	/**
	 * @return CBudgetTab[]
	 */
	public static function fetchBudgetTabs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetTab::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetTab
	 */
	public static function fetchBudgetTab( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetTab::class, $objDatabase );
	}

	public static function fetchBudgetTabCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_tabs', $objDatabase );
	}

	public static function fetchBudgetTabByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetTab( sprintf( 'SELECT * FROM budget_tabs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetTabs( sprintf( 'SELECT * FROM budget_tabs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabs( sprintf( 'SELECT * FROM budget_tabs WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabsByBudgetWorksheetIdByCid( $intBudgetWorksheetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabs( sprintf( 'SELECT * FROM budget_tabs WHERE budget_worksheet_id = %d AND cid = %d', $intBudgetWorksheetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabs( sprintf( 'SELECT * FROM budget_tabs WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabsByBudgetWorksheetTypeIdByCid( $intBudgetWorksheetTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabs( sprintf( 'SELECT * FROM budget_tabs WHERE budget_worksheet_type_id = %d AND cid = %d', $intBudgetWorksheetTypeId, $intCid ), $objDatabase );
	}

}
?>