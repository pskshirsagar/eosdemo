<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationItems extends CEosPluralBase {

	/**
	 * @return CGlReconciliationItem[]
	 */
	public static function fetchGlReconciliationItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlReconciliationItem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlReconciliationItem
	 */
	public static function fetchGlReconciliationItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlReconciliationItem', $objDatabase );
	}

	public static function fetchGlReconciliationItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_reconciliation_items', $objDatabase );
	}

	public static function fetchGlReconciliationItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationItem( sprintf( 'SELECT * FROM gl_reconciliation_items WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationItemsByCid( $intCid, $objDatabase ) {
		return self::fetchGlReconciliationItems( sprintf( 'SELECT * FROM gl_reconciliation_items WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationItemsByGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationItems( sprintf( 'SELECT * FROM gl_reconciliation_items WHERE gl_reconciliation_id = %d AND cid = %d', ( int ) $intGlReconciliationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlReconciliationItemsByReversingGlReconciliationIdByCid( $intReversingGlReconciliationId, $intCid, $objDatabase ) {
		return self::fetchGlReconciliationItems( sprintf( 'SELECT * FROM gl_reconciliation_items WHERE reversing_gl_reconciliation_id = %d AND cid = %d', ( int ) $intReversingGlReconciliationId, ( int ) $intCid ), $objDatabase );
	}

}
?>