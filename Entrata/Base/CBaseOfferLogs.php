<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferLogs extends CEosPluralBase {

	/**
	 * @return COfferLog[]
	 */
	public static function fetchOfferLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COfferLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COfferLog
	 */
	public static function fetchOfferLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfferLog::class, $objDatabase );
	}

	public static function fetchOfferLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offer_logs', $objDatabase );
	}

	public static function fetchOfferLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOfferLog( sprintf( 'SELECT * FROM offer_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByCid( $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByOfferIdByCid( $intOfferId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE offer_id = %d AND cid = %d', ( int ) $intOfferId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE offer_template_id = %d AND cid = %d', ( int ) $intOfferTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE reporting_period_id = %d AND cid = %d', ( int ) $intReportingPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE effective_period_id = %d AND cid = %d', ( int ) $intEffectivePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE original_period_id = %d AND cid = %d', ( int ) $intOriginalPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferLogsByPriorOfferLogIdByCid( $intPriorOfferLogId, $intCid, $objDatabase ) {
		return self::fetchOfferLogs( sprintf( 'SELECT * FROM offer_logs WHERE prior_offer_log_id = %d AND cid = %d', ( int ) $intPriorOfferLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>