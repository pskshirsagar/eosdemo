<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceNamePatterns
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceNamePatterns extends CEosPluralBase {

	/**
	 * @return CUnitSpaceNamePattern[]
	 */
	public static function fetchUnitSpaceNamePatterns( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitSpaceNamePattern::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceNamePattern
	 */
	public static function fetchUnitSpaceNamePattern( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceNamePattern::class, $objDatabase );
	}

	public static function fetchUnitSpaceNamePatternCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_name_patterns', $objDatabase );
	}

	public static function fetchUnitSpaceNamePatternByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceNamePattern( sprintf( 'SELECT * FROM unit_space_name_patterns WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceNamePatternsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceNamePatterns( sprintf( 'SELECT * FROM unit_space_name_patterns WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>