<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeCategories
 * Do not add any new functions to this class.
 */

class CBaseApPayeeCategories extends CEosPluralBase {

	/**
	 * @return CApPayeeCategory[]
	 */
	public static function fetchApPayeeCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApPayeeCategory::class, $objDatabase );
	}

	/**
	 * @return CApPayeeCategory
	 */
	public static function fetchApPayeeCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeCategory::class, $objDatabase );
	}

	public static function fetchApPayeeCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_categories', $objDatabase );
	}

	public static function fetchApPayeeCategoryById( $intId, $objDatabase ) {
		return self::fetchApPayeeCategory( sprintf( 'SELECT * FROM ap_payee_categories WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchApPayeeCategoriesByApPayeeCategoryId( $intApPayeeCategoryId, $objDatabase ) {
		return self::fetchApPayeeCategories( sprintf( 'SELECT * FROM ap_payee_categories WHERE ap_payee_category_id = %d', $intApPayeeCategoryId ), $objDatabase );
	}

}
?>