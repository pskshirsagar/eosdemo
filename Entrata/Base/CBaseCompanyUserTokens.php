<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserTokens
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserTokens extends CEosPluralBase {

	/**
	 * @return CCompanyUserToken[]
	 */
	public static function fetchCompanyUserTokens( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserToken', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserToken
	 */
	public static function fetchCompanyUserToken( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserToken', $objDatabase );
	}

	public static function fetchCompanyUserTokenCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_tokens', $objDatabase );
	}

	public static function fetchCompanyUserTokenByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserToken( sprintf( 'SELECT * FROM company_user_tokens WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserTokensByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserTokens( sprintf( 'SELECT * FROM company_user_tokens WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserTokensByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserTokens( sprintf( 'SELECT * FROM company_user_tokens WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>