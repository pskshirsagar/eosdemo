<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContractApPayees extends CEosPluralBase {

	public static function fetchContractApPayees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CContractApPayee', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchContractApPayee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CContractApPayee', $objDatabase );
	}

	public static function fetchContractApPayeeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contract_ap_payees', $objDatabase );
	}

	public static function fetchContractApPayeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchContractApPayee( sprintf( 'SELECT * FROM contract_ap_payees WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchContractApPayeesByCid( $intCid, $objDatabase ) {
		return self::fetchContractApPayees( sprintf( 'SELECT * FROM contract_ap_payees WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchContractApPayeesByContractIdByCid( $intContractId, $intCid, $objDatabase ) {
		return self::fetchContractApPayees( sprintf( 'SELECT * FROM contract_ap_payees WHERE contract_id = %d AND cid = %d', (int) $intContractId, (int) $intCid ), $objDatabase );
	}

	public static function fetchContractApPayeesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchContractApPayees( sprintf( 'SELECT * FROM contract_ap_payees WHERE ap_payee_id = %d AND cid = %d', (int) $intApPayeeId, (int) $intCid ), $objDatabase );
	}

}
?>