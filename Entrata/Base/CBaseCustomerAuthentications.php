<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAuthentications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAuthentications extends CEosPluralBase {

	/**
	 * @return CCustomerAuthentication[]
	 */
	public static function fetchCustomerAuthentications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerAuthentication', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerAuthentication
	 */
	public static function fetchCustomerAuthentication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerAuthentication', $objDatabase );
	}

	public static function fetchCustomerAuthenticationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_authentications', $objDatabase );
	}

	public static function fetchCustomerAuthenticationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerAuthentication( sprintf( 'SELECT * FROM customer_authentications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAuthenticationsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerAuthentications( sprintf( 'SELECT * FROM customer_authentications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAuthenticationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerAuthentications( sprintf( 'SELECT * FROM customer_authentications WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAuthenticationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerAuthentications( sprintf( 'SELECT * FROM customer_authentications WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerAuthenticationsByCustomerAuthenticationTypeIdByCid( $intCustomerAuthenticationTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerAuthentications( sprintf( 'SELECT * FROM customer_authentications WHERE customer_authentication_type_id = %d AND cid = %d', ( int ) $intCustomerAuthenticationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>