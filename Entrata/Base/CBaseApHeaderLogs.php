<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApHeaderLogs extends CEosPluralBase {

	/**
	 * @return CApHeaderLog[]
	 */
	public static function fetchApHeaderLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApHeaderLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApHeaderLog
	 */
	public static function fetchApHeaderLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApHeaderLog::class, $objDatabase );
	}

	public static function fetchApHeaderLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_header_logs', $objDatabase );
	}

	public static function fetchApHeaderLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLog( sprintf( 'SELECT * FROM ap_header_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByCid( $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_header_id = %d AND cid = %d', $intApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApHeaderTypeIdByCid( $intApHeaderTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_header_type_id = %d AND cid = %d', $intApHeaderTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApHeaderSubTypeIdByCid( $intApHeaderSubTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_header_sub_type_id = %d AND cid = %d', $intApHeaderSubTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApHeaderModeIdByCid( $intApHeaderModeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_header_mode_id = %d AND cid = %d', $intApHeaderModeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPhysicalStatusTypeIdByCid( $intApPhysicalStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_physical_status_type_id = %d AND cid = %d', $intApPhysicalStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApFinancialStatusTypeIdByCid( $intApFinancialStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_financial_status_type_id = %d AND cid = %d', $intApFinancialStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_payee_account_id = %d AND cid = %d', $intApPayeeAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApRemittanceIdByCid( $intApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_remittance_id = %d AND cid = %d', $intApRemittanceId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPayeeTermIdByCid( $intApPayeeTermId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_payee_term_id = %d AND cid = %d', $intApPayeeTermId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE gl_transaction_type_id = %d AND cid = %d', $intGlTransactionTypeId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkJobPhaseIdByCid( $intBulkJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_job_phase_id = %d AND cid = %d', $intBulkJobPhaseId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkApContractIdByCid( $intBulkApContractId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_ap_contract_id = %d AND cid = %d', $intBulkApContractId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkBankAccountIdByCid( $intBulkBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_bank_account_id = %d AND cid = %d', $intBulkBankAccountId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkPropertyIdByCid( $intBulkPropertyId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_property_id = %d AND cid = %d', $intBulkPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkApFormulaIdByCid( $intBulkApFormulaId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_ap_formula_id = %d AND cid = %d', $intBulkApFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkUnitNumberIdByCid( $intBulkUnitNumberId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_unit_number_id = %d AND cid = %d', $intBulkUnitNumberId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkCompanyDepartmentIdByCid( $intBulkCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_company_department_id = %d AND cid = %d', $intBulkCompanyDepartmentId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkGlDimensionIdByCid( $intBulkGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_gl_dimension_id = %d AND cid = %d', $intBulkGlDimensionId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkPropertyBuildingIdByCid( $intBulkPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_property_building_id = %d AND cid = %d', $intBulkPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBulkBudgetApHeaderIdByCid( $intBulkBudgetApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE bulk_budget_ap_header_id = %d AND cid = %d', $intBulkBudgetApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByReversalApHeaderIdByCid( $intReversalApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE reversal_ap_header_id = %d AND cid = %d', $intReversalApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByPaymentApHeaderIdByCid( $intPaymentApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE payment_ap_header_id = %d AND cid = %d', $intPaymentApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByScheduledApHeaderIdByCid( $intScheduledApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE scheduled_ap_header_id = %d AND cid = %d', $intScheduledApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByTemplateApHeaderIdByCid( $intTemplateApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE template_ap_header_id = %d AND cid = %d', $intTemplateApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByReimbursementApHeaderIdByCid( $intReimbursementApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE reimbursement_ap_header_id = %d AND cid = %d', $intReimbursementApHeaderId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByLeaseCustomerIdByCid( $intLeaseCustomerId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE lease_customer_id = %d AND cid = %d', $intLeaseCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByRefundArTransactionIdByCid( $intRefundArTransactionId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE refund_ar_transaction_id = %d AND cid = %d', $intRefundArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApBatchIdByCid( $intApBatchId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_batch_id = %d AND cid = %d', $intApBatchId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE accounting_export_batch_id = %d AND cid = %d', $intAccountingExportBatchId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_payment_id = %d AND cid = %d', $intApPaymentId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByComplianceJobIdByCid( $intComplianceJobId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE compliance_job_id = %d AND cid = %d', $intComplianceJobId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByApRoutingTagIdByCid( $intApRoutingTagId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE ap_routing_tag_id = %d AND cid = %d', $intApRoutingTagId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderLogsByBudgetChangeOrderIdByCid( $intBudgetChangeOrderId, $intCid, $objDatabase ) {
		return self::fetchApHeaderLogs( sprintf( 'SELECT * FROM ap_header_logs WHERE budget_change_order_id = %d AND cid = %d', $intBudgetChangeOrderId, $intCid ), $objDatabase );
	}

}
?>